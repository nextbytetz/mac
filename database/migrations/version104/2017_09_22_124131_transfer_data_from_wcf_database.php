<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferDataFromWcfDatabase extends Migration
{

    private $source_db = "wcf";

    private $target_db;

    public function __construct()
    {
        $this->target_db = env("DB_DATABASE");
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("insert into " .  $this->target_db . ".employers (id, name, reg_no, doc, tin, region_id, district, street, plot_no, block_no, telephone, fax, phone, email, representer_id, created_by, created_at, modified_by, modified, annual_earning, approval_id, reject_reason, vote, emp_approved_by, emp_approved_date, cert_status, cert_issued_by, cert_issued_date, cert_reissued_by, cert_reissued_date) SELECT id, name, reg_no, date_of_commencement as doc, tin, region_id, district, street, plot_no, block_no, telephone, fax, phone, email, representer_id, created_by, created as created_at, modified_by, modified, annual_earning, approval_id, reject_reason, vote, emp_approved_by, emp_approved_date, cert_status, cert_issued_by, cert_issued_date, cert_reissued_by, cert_reissued_date FROM " . $this->source_db  . ".employers");

        DB::statement("update " .  $this->target_db  . ".employers set employer_category_cv_id = 36 where id in (SELECT id FROM " . $this->source_db  . ".employers WHERE employer_category_id = 1)");

        DB::statement("update " .  $this->target_db  . ".employers set employer_category_cv_id = 37 where id in (SELECT id FROM " . $this->source_db  . ".employers WHERE employer_category_id = 2)");

        DB::statement("insert into " . $this->target_db  . ".employer_sectors (business_sector_cv_id, employer_id) SELECT business_id as business_sector_cv_id, employer_id FROM " . $this->source_db . ".business_activities WHERE employer_id is not null");

        DB::statement("insert into " . $this->target_db  . ".legacy_logs (user_id, date_time, page, data) SELECT user_id, date_time, page, data FROM " . $this->source_db  . ".audit");

        //DB::statement("insert into " . $this->target_db  . ".legacy_receipts (rctno, rct_date, amount, contrib_month, employer_id, status, comments, created_at, amount_imported, member_count, user_id, modified_by, modified_date, contr_approval, reason, approved_by, approved_date) SELECT receipt_no as rctno, (CASE WHEN `date_receipt` = '0000-00-00' THEN null ELSE date_receipt END) as rct_date, amount, (CASE WHEN `contr_month` = '0000-00-00' THEN null ELSE contr_month END) as contrib_month, employer_id, contr_status as status, comments, created as created_at, total_excel as amount_imported, no_employees as member_count, created_by as user_id, modified_by, modified_date, contr_approval, reason, approved_by, approved_date  FROM " . $this->source_db  . ".contributions;");

        //DB::statement("insert into " . $this->target_db . ".legacy_dishonoured_cheques (legacy_receipt_id, status, assigned_to, assigned_date, updates) SELECT contributions_id as legacy_receipt_id, status, assigned_to, assigned_date, updates FROM " . $this->source_db . ".dishonoured_chq");

        DB::statement("insert into " . $this->target_db . ".employees (id, memberno, nid, firstname, middlename, lastname, dob, sex, job_title, emp_cate, salary, allowance, created_at, user_id) SELECT member_id as id, member_no as memberno, nid, firstname, middlename, lastname, dob, sex, job_title, emp_cate, round(salary/12,2) as salary, round(allowance/12,2) as allowance, (CASE WHEN `date_created` = '0000-00-00' THEN null ELSE date_created END) as created_at, created_by as user_id FROM " . $this->source_db  . ".members");

        DB::statement("update " . $this->target_db . ".employees set gender_id = 1 where sex = 'm'");

        DB::statement("update " . $this->target_db . ".employees set gender_id = 2 where sex = 'f'");

        DB::statement("update " . $this->target_db . ".employees set employee_category_cv_id = 85 where emp_cate = 'permanent'");

        DB::statement("update " . $this->target_db . ".employees set employee_category_cv_id = 84 where emp_cate = 'temporary'");

        DB::statement("insert into " . $this->target_db . ".employee_employers (employee_id, employer_id) SELECT `member_id` as employee_id, `employer_ID` as employer_id FROM " . $this->source_db . ".members");

        //DB::statement("insert into " . $this->target_db . ".contributions (legacy_receipt_id, employee_id, contrib_month, salary, grosspay, employer_id, created_at, user_id) SELECT a.members_contributions_id as legacy_receipt_id, b.member_id as employee_id, a.contribution_month as contrib_month, round(a.monthly_salary/12,2) as salary, round(a.monthly_salary/12, 2) + round(a.monthly_allowance/12, 2) as grosspay, a.employer_id, a.date_created as created_at, a.created_by as user_id FROM " . $this->source_db . ".members_contributions a join " . $this->source_db . ".members b on a.member_no = b.member_no");

        DB::statement("update " . $this->target_db . ".employees set employee_category_cv_id = 86 where emp_cate like '%specific%'");

        //DB::statement("update " . $this->target_db . ".contributions set employee_amount = ((1/100) * grosspay) where employer_id in (select id from " . $this->target_db . ".employers where employer_category_cv_id = 37)");

        //DB::statement("update " . $this->target_db . ".contributions set employee_amount = ((0.5/100) * grosspay) where employer_id in (select id from " . $this->target_db . ".employers where employer_category_cv_id = 36)");

        DB::statement("insert into " . $this->target_db . ".users (id, firstname, middlename, lastname, username, password, created_by, created_at, modified_by, modified, task_id) SELECT id, firstname, middlename, lastname, username, password, created_by, (CASE WHEN created = '0000-00-00 00:00:00' THEN null ELSE created END) as created_at, modified_by, (CASE WHEN modified = '0000-00-00 00:00:00' THEN null ELSE modified END) as modified, task_id FROM " . $this->source_db . ".users");

        //DB::statement("insert into " . $this->target_db . ".unregistered_employers (id, name, address, plot_number, street, postal_city, location, business_activity, last_remittance, state, phone, email, updated, updated_by, assign_to, date_assigned) SELECT id, name, address, plot_number, street, postal_city, location, business_activity, last_remittance, state, phone, email, updated, updated_by, assign_to, date_assigned FROM " . $this->source_db . ".unregistered_employers");

        DB::statement("update " . $this->target_db . ".employees set employee_category_cv_id = 86 where emp_cate like '%specific%'");

        DB::statement("insert into " . $this->target_db . ".employee_counts (employer_id, employee_category_cv_id, gender_id, count, annual_earning) SELECT employer_id, `employee_category_id` as employee_category_cv_id, `employee_bas_gender_id` as gender_id, count, annual_earning FROM " . $this->source_db . ".employee_numbers where employer_id is not null");

        DB::statement("update " . $this->target_db . ".employee_counts set employee_category_cv_id = 86 where employee_category_cv_id = 3");

        DB::statement("update " . $this->target_db . ".employee_counts set employee_category_cv_id = 84 where employee_category_cv_id = 2");

        DB::statement("update " . $this->target_db . ".employee_counts set employee_category_cv_id = 85 where employee_category_cv_id = 1");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
