<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferEmployerCertificateInformation extends Migration
{


    private $source_db = "wcf";

    private $target_db;

    public function __construct()
    {
        $this->target_db = env("DB_DATABASE");
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("insert into " . $this->target_db . ".employer_certificate_issues(user_id, created_at, employer_id) select `cert_issued_by`, `cert_issued_date`, id from " . $this->target_db . ".employers where `cert_status` = 1");

        DB::statement("insert into " . $this->target_db . ".employer_certificate_issues(user_id, created_at, employer_id, is_reissue) select `cert_reissued_by`, `cert_reissued_date`, id, 1 from " . $this->target_db . ".employers where `cert_status` = 1 and `cert_reissued_by` is not null");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
