<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAllTinyIntegerToSmallInteger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

$sql = <<<SQL
DROP PROCEDURE IF EXISTS change_datatype;
CREATE PROCEDURE change_datatype(in in_table_schema varchar(50), in in_data_type varchar(50), in_dest_data_type varchar(50))
begin
DECLARE cur_table_name varchar(50);
DECLARE cur_column_name varchar(50);
DECLARE cur_column_default SMALLINT;
DECLARE cur_column_comment varchar(300);
DECLARE cur_is_nullable varchar(5);
DECLARE col_nullable varchar(20);
DECLARE col_default varchar(50);
DECLARE done INT DEFAULT 0;
DECLARE selected_columns CURSOR FOR SELECT table_name, column_name, column_comment, column_default, is_nullable FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = in_table_schema and data_type = in_data_type;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

open selected_columns;

read_loop: loop
 IF done THEN
 LEAVE read_loop;
 END IF;
 
fetch selected_columns into cur_table_name, cur_column_name, cur_column_comment, cur_column_default, cur_is_nullable;

IF cur_is_nullable = 'YES' THEN
SET col_nullable = '';
ELSE
SET col_nullable = ' NOT NULL ';
END IF;

IF cur_column_default is null THEN
SET col_default = '';
ELSE
SET col_default = concat(' DEFAULT ', cur_column_default);
END IF;

SET @sql_text = concat('ALTER TABLE ', cur_table_name ,' MODIFY ', cur_column_name, ' ', in_dest_data_type, ' ', col_nullable, col_default ,' COMMENT ''', cur_column_comment, '''');

PREPARE stmt FROM @sql_text;
EXECUTE stmt;

end loop;

DEALLOCATE PREPARE stmt;
close selected_columns;

end
SQL;

        DB::unprepared($sql);

        DB::unprepared("call change_datatype('" . env("DB_DATABASE") . "', 'tinyint', 'smallint')");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}

