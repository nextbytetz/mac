<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastReviewEndDateOnEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_inspection_legacy', function (Blueprint $table) {
            $table->increments("id");
            $table->bigInteger("employer_id");
            $table->date("review_start_date")->nullable();
            $table->date("review_end_date");
            $table->bigInteger("user_id")->nullable();
            $table->timestamps();
        });
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            $table->date("last_review_end_date")->nullable();
        });
        DB::statement("comment on column employer_inspection_task.last_review_end_date is 'last date when the review ended, should be date of commencement if inspection has never been conducted'");
        DB::statement("comment on table employer_inspection_legacy is 'table to store the manual conducted inspection'");
        DB::statement("comment on column employer_inspection_legacy.review_start_date is 'date when the contribution review starts'");
        DB::statement("comment on column employer_inspection_legacy.review_end_date is 'date when the contribution review ends'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
