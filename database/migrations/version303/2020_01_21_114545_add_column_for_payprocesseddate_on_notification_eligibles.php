<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnForPayprocesseddateOnNotificationEligibles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            $table->date("payprocesseddate")->nullable();
        });
        DB::statement("comment on column notification_eligible_benefits.payprocesseddate is 'date when finance process payment or date when benefit is paid or date when zero payment is issued.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            //
        });
    }
}
