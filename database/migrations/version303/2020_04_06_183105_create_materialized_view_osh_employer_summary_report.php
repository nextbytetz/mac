<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewOshEmployerSummaryReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE MATERIALIZED VIEW  osh_employers_summary_report  AS     select o.fin_year,o.employer_id,o.employer,e.reg_no as RegNo,d.name as hq_district,r.name as hq_region,
sum(o.fatal) as fatals_premises,sum(o.out_fatal) as fatals_out,
sum(o.accidents) as accidents_premises,sum(o.out_accident) as accidents_out,
sum(o.disease) as disease,
sum(o.claims_total) as total_claims,
sum(o.total_lost_days) as total_days_lost, 
(sum(o.fatal)+sum(o.out_fatal)+sum(o.accidents)+sum(o.out_accident)+sum(o.disease)) as fatal_and_non_fatal,
(sum(o.fatal)+sum(o.accidents)) as on_premise_fatals_and_accidents,
(sum(o.accidents)+sum(o.out_accident)) as accidents_workplace_reported,
(sum(o.fatal)+sum(o.out_fatal)) as occupational_fatal
from (
select employer_id,employer,fin_year,sum(death) as fatal,sum(non_conv_death) as out_fatal,sum(accidents) as accidents,sum(non_conv_accident) as out_accident,sum(diseases) as disease,sum(total_claims) as claims_total,sum(t_lost_days) as total_lost_days from (
SELECT
s.employer_id,s.employer,s.fin_year,s.death,s.accidents,s.diseases,s.t_lost_days,s.total_claims,s.non_conv_death,s.non_conv_accident
FROM
(
SELECT  employer_id,employer,fin_year,
CASE    WHEN (conveyance_death=224) THEN count(conveyance_death) ELSE 0 END AS death,
CASE    WHEN (conveyance_death=225) THEN count(conveyance_death) ELSE 0 END AS non_conv_death,
CASE    WHEN (conveyance_accident=1) THEN count(conveyance_accident) ELSE 0 END AS accidents,
CASE    WHEN (conveyance_accident=2) THEN count(conveyance_accident) ELSE 0 END AS non_conv_accident,
count(diseases_name) as diseases,
sum(lost_days) as t_lost_days,
count(*) as total_claims
FROM main.osh_audit_checklist_employers group by employer_id,employer,fin_year,conveyance_death,conveyance_accident,diseases_name
) s) q group by employer_id,employer,fin_year) o
join main.employers e on o.employer_id=e.id join main.districts d on e.district_id=d.id join main.regions r on d.region_id=r.id 
group by o.employer_id,o.employer,e.reg_no,d.name,r.name,o.fin_year");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
