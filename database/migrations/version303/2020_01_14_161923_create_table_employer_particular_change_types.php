<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployerParticularChangeTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employer_particular_change_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_particular_change_id');
            $table->integer('name_change_type_cv_id')->nullable();
            $table->text('old_values')->nullable();
            $table->text('new_values')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

            /*Foreign keys*/
            $table->foreign('employer_particular_change_id')->references('id')->on('employer_particular_changes')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
        DB::statement("comment on table employer_particular_change_types is 'Table to keep track of all change types of employers particular' ");

        Schema::table('employer_particular_changes', function(Blueprint $table)
        {
            $table->date('date_received')->nullable();
        });
        DB::statement("comment on column employer_particular_changes.date_received is 'Date when change request was received' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
