<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGeneralCommentOnOshAuditPeriodEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
            $table->text("general_comments")->nullable();
            $table->string("workplace_name")->nullable();
            $table->string("workplace_address")->nullable();
            $table->string("workplace_location")->nullable();
            $table->string("contact_person")->nullable();
            $table->string("contact_person_phone")->nullable();
            $table->integer("total_employees")->nullable();
            $table->date("audit_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
            $table->dropColumn("general_comments");
            $table->dropColumn("workplace_name");
            $table->dropColumn("workplace_address");
            $table->dropColumn("workplace_location");
            $table->dropColumn("contact_person");
            $table->dropColumn("contact_person_phone");
            $table->dropColumn("total_employees");
            $table->dropColumn("audit_date");
        });
    }
}
