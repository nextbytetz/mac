<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBoookingPaymentDateReceiptDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("select deps_save_and_drop_dependencies('main', 'booking_payment_date')");
        DB::statement("DROP VIEW IF EXISTS  booking_payment_date");
        DB::statement("CREATE VIEW main.booking_payment_date AS 
SELECT b.id,
       COALESCE((SELECT min(receipts.rct_date) AS payment_date
                 FROM (main.receipt_codes
                     JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                 WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                        (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                        (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id))),
                (SELECT min(receipts.rct_date) AS sum
                 FROM (main.receipt_codes
                     JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                 WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                        (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                        (receipts.deleted_at IS NULL) AND (date_part('month' :: text, b.rcv_date) =
                                                           date_part('month' :: text, receipt_codes.contrib_month)) AND
                        (date_part('year' :: text, b.rcv_date) =
                         date_part('year' :: text, receipt_codes.contrib_month)) AND
                        (receipts.employer_id = b.employer_id)))) AS first_payment_date,
       COALESCE((SELECT min(receipts.created_at) AS receipt_date
                 FROM (main.receipt_codes
                     JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                 WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                        (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                        (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id))),
                (SELECT min(receipts.created_at) AS receipt_date
                 FROM (main.receipt_codes
                     JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                 WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                        (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                        (receipts.deleted_at IS NULL) AND (date_part('month' :: text, b.rcv_date) =
                                                           date_part('month' :: text, receipt_codes.contrib_month)) AND
                        (date_part('year' :: text, b.rcv_date) =
                         date_part('year' :: text, receipt_codes.contrib_month)) AND
                        (receipts.employer_id = b.employer_id)))) AS receipt_date,
       (select min(end_date) from fin_years where b.rcv_date >= start_date and b.rcv_date <= end_date) as end_fiscal_year
FROM main.bookings b
WHERE (b.deleted_at IS NULL);


");

        DB::statement("select deps_restore_dependencies('main', 'booking_payment_date')");





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
