<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommentOnInspectionProgressiveStageColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column inspections.progressive_stage is 'show the mandatory stages which the inspection has already gone. 1 - Inspection Created, 2 - Inspection Approved/On Progress, 3 - Declined Inspection Plan'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
