<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnChangeDateContributingCategoryEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('contributing_category_employer', function(Blueprint $table)
        {
            $table->date('change_date')->nullable();
        });

        DB::statement("comment on column contributing_category_employer.change_date is 'Date when employer change to another category'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
