<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfModuleIdOnLetters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letters', function(Blueprint $table)
        {
            $table->bigInteger("wf_module_id")->nullable();
            $table->foreign('wf_module_id')->references('id')->on('wf_modules')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column letters.wf_module_id is 'reference to the workflow module'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
