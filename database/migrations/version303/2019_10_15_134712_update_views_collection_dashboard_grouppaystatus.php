<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewsCollectionDashboardGrouppaystatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("select deps_save_and_drop_dependencies('main', 'comp_monthly_contribution_electronic')");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS comp_monthly_contribution_electronic ");
        DB::statement("CREATE MATERIALIZED VIEW main.comp_monthly_contribution_electronic AS
  SELECT to_char((((((a.year || '-' :: text) || a.month) || '-1' :: text)) :: date) :: timestamp with time zone,
                 'Mon YY ' :: text) AS period, a.year, a.month, a.fin_year_id, a.amount
  FROM (SELECT date_part('year' :: text, b.date_value)  AS year,
               date_part('month' :: text, b.date_value) AS month,
               b.fin_year_id,
               COALESCE(sum(g.amount), (0) :: numeric)  AS amount
        FROM ((main.receipts a_1
            JOIN main.receipt_codes g ON (((a_1.id = g.receipt_id) AND (g.fin_code_id = 2) AND (a_1.iscancelled = 0) AND g.grouppaystatus = 0 AND
                                           (a_1.isdishonoured = 0))))
            RIGHT JOIN main.fin_year_months b ON ((
          (date_part('month' :: text, g.contrib_month) = date_part('month' :: text, b.date_value)) AND
          (date_part('year' :: text, g.contrib_month) = date_part('year' :: text, b.date_value)))))
        GROUP BY (date_part('year' :: text, b.date_value)), (date_part('month' :: text, b.date_value)),
                 b.fin_year_id) a;

");

        DB::statement("select deps_restore_dependencies('main', 'comp_monthly_contribution_electronic')");


        /**/
        DB::statement("select deps_save_and_drop_dependencies('main', 'comp_monthly_contributing_employer_electronic')");
        DB::statement("DROP  MATERIALIZED VIEW IF EXISTS comp_monthly_contributing_employer_electronic ");
        DB::statement("CREATE  MATERIALIZED VIEW main.comp_monthly_contributing_employer_electronic AS

  SELECT to_char((((((a.year || '-' :: text) || a.month) || '-1' :: text)) :: date) :: timestamp with time zone,
                 'Mon YY ' :: text) AS period, a.year, a.month, a.fin_year_id, a.employers
  FROM (SELECT date_part('year' :: text, b.date_value)  AS year,
               date_part('month' :: text, b.date_value) AS month,
               b.fin_year_id,
               count(DISTINCT a_1.employer_id)          AS employers
        FROM ((main.receipts a_1
            JOIN main.receipt_codes g ON (((a_1.id = g.receipt_id) AND (g.fin_code_id = 2) AND (a_1.iscancelled = 0) AND  g.grouppaystatus = 0 AND
                                           (a_1.isdishonoured = 0))))
            RIGHT JOIN main.fin_year_months b ON ((
          (date_part('month' :: text, g.contrib_month) = date_part('month' :: text, b.date_value)) AND
          (date_part('year' :: text, g.contrib_month) = date_part('year' :: text, b.date_value)))))
        GROUP BY (date_part('year' :: text, b.date_value)), (date_part('month' :: text, b.date_value)),
                 b.fin_year_id) a;

");

        DB::statement("select deps_restore_dependencies('main', 'comp_monthly_contributing_employer_electronic')");



        /**/
        DB::statement("select deps_save_and_drop_dependencies('main', 'comp_yearly_contributing_employer_electronic')");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS comp_yearly_contributing_employer_electronic ");
        DB::statement("CREATE MATERIALIZED VIEW main.comp_yearly_contributing_employer_electronic AS

  SELECT a.name, a.fin_year_id, a.employers
  FROM (SELECT b.name, b.id AS fin_year_id, count(DISTINCT a_1.employer_id) AS employers
        FROM ((main.receipts a_1
            JOIN main.receipt_codes g ON (((a_1.id = g.receipt_id) AND (g.fin_code_id = 2) AND (a_1.iscancelled = 0) AND  g.grouppaystatus = 0 AND
                                           (a_1.isdishonoured = 0))))
            RIGHT JOIN main.fin_years b ON (((a_1.rct_date >= b.start_date) AND (a_1.rct_date <= b.end_date))))
        GROUP BY b.name, b.id) a;

");

        DB::statement("select deps_restore_dependencies('main', 'comp_yearly_contributing_employer_electronic')");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
