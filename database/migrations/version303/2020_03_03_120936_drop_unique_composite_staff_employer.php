<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUniqueCompositeStaffEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer', function (Blueprint $table) {
            $table->dropUnique('staff_employer_user_id_employer_id_staff_employer_allocation_id');
            $table->unique(['user_id', 'employer_id', 'staff_employer_allocation_id', 'isactive']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
