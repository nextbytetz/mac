<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDiseaseExposureAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disease_exposure_agents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("code_value_id")->unsigned()->index("code_value_id");
            $table->string('name',500)->comment("classification of diseases exposure by agents as per ILO");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('disease_exposure_agents', function(Blueprint $table)
        {
            // $table->foreign('code_value_id', 'code_value_id_foreign')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('disease_exposure_agents', function(Blueprint $table)
        {
            $table->dropColumn('code_value_id_foreign');
        });

        Schema::drop('disease_exposure_agents');
        Schema::drop('code_values');

       }
}
