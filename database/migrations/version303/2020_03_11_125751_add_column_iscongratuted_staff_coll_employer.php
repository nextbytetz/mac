<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIscongratutedStaffCollEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_collection_performances', function (Blueprint $table) {
            $table->smallInteger('iscongratulated')->default(0);

        });

        DB::statement("comment on column staff_employer_collection_performances.iscongratulated is 'Flag to specify if congratulation email has been sent to staff with above pass performance marks for this collection month i.e. 1 =. sent, 0 => not yet'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
