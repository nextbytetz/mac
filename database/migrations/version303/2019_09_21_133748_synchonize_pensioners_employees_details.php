<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SynchonizePensionersEmployeesDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        $sql = <<<SQL
update employees
set phone = (
            select pensioners.phone
            from pensioners
            where pensioners.employee_id = employees.id),
 email = (
            select pensioners.email
            from pensioners
            where pensioners.employee_id = employees.id),
 firstname = (
            select pensioners.firstname
            from pensioners
            where pensioners.employee_id = employees.id),
 middlename = (
            select pensioners.middlename
            from pensioners
            where pensioners.employee_id = employees.id),
 lastname = (
            select pensioners.lastname
            from pensioners
            where pensioners.employee_id = employees.id),
 bank_id = (
            select pensioners.bank_id
            from pensioners
            where pensioners.employee_id = employees.id),
 bank_branch_id = (
            select pensioners.bank_branch_id
            from pensioners
            where pensioners.employee_id = employees.id),
 accountno = (
            select pensioners.accountno
            from pensioners
            where pensioners.employee_id = employees.id)

where exists (
        select *
        from pensioners
        where employees.id = pensioners.employee_id
          );

SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
