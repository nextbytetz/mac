<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewIncidentsPayStatusForEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW IF EXISTS  employers_incidents_view");
        DB::statement("CREATE VIEW main.employers_incidents_view AS

select
   distinct on (n.id) n.id as notification_report_id, n.employer_id, n.employee_id, n.incident_type_id,
CASE WHEN (n.isprogressive = 0 and n.status = 1 and n.wf_done = 1 ) THEN  1 WHEN (neb.ispaid = 1) THEN 1  WHEN n.status = 2 THEN 2  ELSE 0 END as pay_status
from notification_reports n
       join incident_types t on t.id = n.incident_type_id
       left join notification_eligible_benefits neb on n.id = neb.notification_report_id
where n.deleted_at is null;


");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
