<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreatePayrollPendingWorkflowsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW IF EXISTS  payroll_pending_workflows_view");
        DB::statement("create view main.payroll_pending_workflows_view as
  SELECT b.member_name,
         b.filename,
         concat_ws('-' :: text, m2.name, v.name) AS workflow_name,
         b.member_type_id,
         mt.name                                 AS member_type_name,
         b.resource_id,
         b.pivot_id,
         b.employee_id,
         u.username                              AS initiated_by,
         definition.level                        AS pending_level,
         u2.username                             AS pending_user,
         r.created_at,
         m2.wf_module_group_id
  FROM ((((((((main.payroll_recoveries r
      JOIN main.payroll_beneficiaries_view b ON (((b.member_type_id = r.member_type_id) AND
                                                  (r.resource_id = b.resource_id))))
      JOIN main.code_values v ON ((r.recovery_type_cv_id = v.id)))
      JOIN main.users u ON ((r.user_id = u.id)))
      JOIN main.member_types mt ON ((mt.id = b.member_type_id)))
      JOIN main.wf_tracks w ON ((w.resource_id = r.id)))
      LEFT JOIN main.users u2 ON ((w.user_id = u2.id)))
      JOIN main.wf_definitions definition ON ((w.wf_definition_id = definition.id)))
      JOIN main.wf_modules m2 ON ((definition.wf_module_id = m2.id)))
  WHERE ((r.wf_done = 0) AND (w.status = 0) AND (m2.wf_module_group_id = 13))
  UNION
  SELECT b.member_name,
         b.filename,
         m2.name          AS workflow_name,
         b.member_type_id,
         mt.name          AS member_type_name,
         b.resource_id,
         b.pivot_id,
         b.employee_id,
         u.username       AS initiated_by,
         definition.level AS pending_level,
         u2.username      AS pending_user,
         r.created_at,
         m2.wf_module_group_id
  FROM (((((((main.payroll_bank_info_updates r
      JOIN main.payroll_beneficiaries_view b ON (((b.member_type_id = r.member_type_id) AND
                                                  (r.resource_id = b.resource_id))))
      JOIN main.users u ON ((r.user_id = u.id)))
      JOIN main.member_types mt ON ((mt.id = b.member_type_id)))
      JOIN main.wf_tracks w ON ((w.resource_id = r.id)))
      LEFT JOIN main.users u2 ON ((w.user_id = u2.id)))
      JOIN main.wf_definitions definition ON ((w.wf_definition_id = definition.id)))
      JOIN main.wf_modules m2 ON ((definition.wf_module_id = m2.id)))
  WHERE ((r.wf_done = 0) AND (w.status = 0) AND (m2.wf_module_group_id = 11))
  UNION
  SELECT b.member_name,
         b.filename,
         m2.name          AS workflow_name,
         b.member_type_id,
         mt.name          AS member_type_name,
         b.resource_id,
         b.pivot_id,
         b.employee_id,
         u.username       AS initiated_by,
         definition.level AS pending_level,
         u2.username      AS pending_user,
         r.created_at,
         m2.wf_module_group_id
  FROM (((((((main.payroll_beneficiary_updates r
      JOIN main.payroll_beneficiaries_view b ON (((b.member_type_id = r.member_type_id) AND
                                                  (r.resource_id = b.resource_id))))
      JOIN main.users u ON ((r.user_id = u.id)))
      JOIN main.member_types mt ON ((mt.id = b.member_type_id)))
      JOIN main.wf_tracks w ON ((w.resource_id = r.id)))
      LEFT JOIN main.users u2 ON ((w.user_id = u2.id)))
      JOIN main.wf_definitions definition ON ((w.wf_definition_id = definition.id)))
      JOIN main.wf_modules m2 ON ((definition.wf_module_id = m2.id)))
  WHERE ((r.wf_done = 0) AND (w.status = 0) AND (m2.wf_module_group_id = 15))
  UNION
  SELECT b.member_name,
         b.filename,
         concat_ws('-' :: text, m2.name, v.name) AS workflow_name,
         b.member_type_id,
         mt.name                                 AS member_type_name,
         b.resource_id,
         b.pivot_id,
         b.employee_id,
         u.username                              AS initiated_by,
         definition.level                        AS pending_level,
         u2.username                             AS pending_user,
         r.created_at,
         m2.wf_module_group_id
  FROM ((((((((main.payroll_status_changes r
      JOIN main.payroll_beneficiaries_view b ON (((b.member_type_id = r.member_type_id) AND
                                                  (r.resource_id = b.resource_id))))
      JOIN main.users u ON ((r.user_id = u.id)))
      JOIN main.member_types mt ON ((mt.id = b.member_type_id)))
      JOIN main.code_values v ON ((r.status_change_cv_id = v.id)))
      JOIN main.wf_tracks w ON ((w.resource_id = r.id)))
      LEFT JOIN main.users u2 ON ((w.user_id = u2.id)))
      JOIN main.wf_definitions definition ON ((w.wf_definition_id = definition.id)))
      JOIN main.wf_modules m2 ON ((definition.wf_module_id = m2.id)))
  WHERE ((r.wf_done = 0) AND (w.status = 0) AND (m2.wf_module_group_id = 12));

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
