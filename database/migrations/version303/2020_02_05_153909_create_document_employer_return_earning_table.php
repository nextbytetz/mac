<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentEmployerReturnEarningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_employer_return_earning', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger("employer_return_earning_id")->index();
            $table->integer("document_id")->index();
            $table->string("name", 150)->nullable();
            $table->text("description")->nullable();
            $table->string("ext", 20)->nullable();
            $table->double("size")->nullable();
            $table->string("mime", 150)->nullable();
            $table->timestamps();
            $table->foreign('employer_return_earning_id')->references('id')->on('employer_return_earnings')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_employer_return_earning');
    }
}
