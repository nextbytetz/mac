<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentInformationOnInspectionAssessmentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_assessments', function (Blueprint $table) {
            $table->date("last_pay_date")->nullable();
            $table->decimal("contrib_amount", 14)->nullable();
        });
        DB::statement("comment on column inspection_assessments.last_pay_date is 'date when the last payment was received before going for inspection'");
        DB::statement("comment on column inspection_assessments.contrib_amount is 'amount contributed to the fund from the declared information'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_assessments', function (Blueprint $table) {
            //
        });
    }
}
