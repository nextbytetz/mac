<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReceiptCodesBookingIdOrig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->integer('booking_id_orig')->nullable();

        });

        DB::statement("comment on column receipt_codes.booking_id_orig is 'Original booking id before update'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
