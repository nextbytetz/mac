<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSupplierIdBanks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('banks', function(Blueprint $table)
        {
            $table->string('erp_supplier_id')->nullable();
        });

        DB::statement("comment on column banks.erp_supplier_id is 'Supplier id as named on ERP'");
        //

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
