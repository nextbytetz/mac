<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewPayrollBeneficiairiesAddEmployeeNameMemberno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //





        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_beneficiaries_view')");
        DB::statement("DROP VIEW IF EXISTS  payroll_beneficiaries_view");
        DB::statement("CREATE VIEW main.payroll_beneficiaries_view AS 
SELECT p.id                      AS resource_id,
       5                         AS member_type_id,
       CASE
         WHEN (p.notification_report_id IS NOT NULL) THEN n.filename
         ELSE (m2.case_no) :: character varying(20)
           END                   AS filename,
       concat_ws(' ' :: text, p.firstname, COALESCE(p.middlename, '' :: character varying),
                 p.lastname)     AS member_name,
       concat_ws(' ' :: text, p.firstname, COALESCE(p.middlename, '' :: character varying),
                 p.lastname)     AS employee_name,
       cast(e.memberno as varchar(20)) as memberno,
       p.bank_id,
       p.bank_branch_id,
       p.notification_report_id,
       p.manual_notification_report_id,
       p.isactive,
       p.employee_id,
       NULL :: integer           AS pivot_id,
       CASE
         WHEN (e.gender_id = 1) THEN 'Male' :: text
         WHEN (e.gender_id = 2) THEN 'Female' :: text
         ELSE ' ' :: text
           END                   AS gender,
       NULL :: character varying AS relationship
FROM (((main.pensioners p
    JOIN main.employees e ON ((e.id = p.employee_id)))
    LEFT JOIN main.notification_reports n ON ((p.notification_report_id = n.id)))
    LEFT JOIN main.manual_notification_reports m2 ON ((p.manual_notification_report_id = m2.id)))
UNION
SELECT d.id                  AS resource_id,
       4                     AS member_type_id,
       CASE
         WHEN (de.notification_report_id IS NOT NULL) THEN n.filename
         ELSE (m2.case_no) :: character varying(20)
           END               AS filename,
       concat_ws(' ' :: text, d.firstname, COALESCE(d.middlename, '' :: character varying),
                 d.lastname) AS member_name,
       concat_ws(' ' :: text, e.firstname, COALESCE(e.middlename, '' :: character varying),
                 e.lastname) AS employee_name,
       concat_ws('_' :: text, e.memberno, de.dependent_type_id,
                 d.id) AS memberno,
       d.bank_id,
       d.bank_branch_id,
       de.notification_report_id,
       de.manual_notification_report_id,
       de.isactive,
       de.employee_id,
       de.id                 AS pivot_id,
       CASE
         WHEN (d.gender_id = 1) THEN 'Male' :: text
         WHEN (d.gender_id = 2) THEN 'Female' :: text
         ELSE ' ' :: text
           END               AS gender,
       dt.name               AS relationship
FROM ((((main.dependent_employee de
    JOIN main.dependents d ON ((d.id = de.dependent_id)))
    JOIN main.dependent_types dt ON ((dt.id = de.dependent_type_id)))
    join employees  e on de.employee_id = e.id
    LEFT JOIN main.notification_reports n ON ((de.notification_report_id = n.id)))
    LEFT JOIN main.manual_notification_reports m2 ON ((de.manual_notification_report_id = m2.id)));


");

        DB::statement("select deps_restore_dependencies('main', 'payroll_beneficiaries_view')");





        DB::statement("DROP VIEW IF EXISTS  payroll_runs_view");
        DB::statement("CREATE VIEW main.payroll_runs_view AS 
    SELECT payroll_beneficiaries_view.member_name,
  payroll_beneficiaries_view.employee_name,
    payroll_beneficiaries_view.memberno,
         payroll_runs.amount,
         payroll_runs.months_paid,
         payroll_runs.monthly_pension,
         payroll_runs.payroll_run_approval_id,
         payroll_run_approvals.wf_done,
         payroll_runs.arrears_amount,
         payroll_runs.deductions_amount,
         payroll_runs.unclaimed_amount,
         payroll_runs.employee_id,
         member_types.name           AS member_type_name,
         payroll_procs.run_date,
         bank_branches.name          AS bank_branch_name,
         banks.name                  AS bank_name,
         payroll_runs.accountno      AS run_accountno,
         payroll_runs.bank_branch_id AS run_bank_branch_id,
         payroll_runs.bank_id        AS run_bank_id,
         payroll_runs.resource_id,
         payroll_runs.member_type_id,
         payroll_runs.new_payee_flag,
         payroll_runs.isconstantcare,
         payroll_beneficiaries_view.filename,
         payroll_beneficiaries_view.gender,
         payroll_beneficiaries_view.relationship
  FROM ((((((main.payroll_runs
      JOIN main.payroll_run_approvals ON ((payroll_runs.payroll_run_approval_id = payroll_run_approvals.id)))
      JOIN main.payroll_procs ON ((payroll_run_approvals.payroll_proc_id = payroll_procs.id)))
      JOIN main.member_types ON ((member_types.id = payroll_runs.member_type_id)))
      JOIN main.payroll_beneficiaries_view ON (((payroll_beneficiaries_view.resource_id = payroll_runs.resource_id) AND
                                                (payroll_beneficiaries_view.member_type_id =
                                                 payroll_runs.member_type_id))))
      LEFT JOIN main.bank_branches ON ((bank_branches.id = payroll_runs.bank_branch_id)))
      LEFT JOIN main.banks ON ((banks.id = payroll_runs.bank_id)));

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
