<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingsViewsAgingAnalysis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("DROP VIEW IF EXISTS  bookings_view");
        DB::statement("CREATE VIEW main.bookings_view AS 
  SELECT b.employer_id,
         b.id                                                                                               AS booking_id,
         e.name                                                                                             AS employer_name,
         e.reg_no                                                                                           AS employer_reg_no,
         b.rcv_date,
         b.amount                                                                                           AS booked_amount,
         COALESCE((SELECT sum(receipt_codes.amount) AS sum
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id))),
                  (SELECT sum(receipt_codes.amount) AS sum
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (date_part('month' :: text, b.rcv_date) =
                                                             date_part('month' :: text, receipt_codes.contrib_month)) AND
                          (date_part('year' :: text, b.rcv_date) =
                           date_part('year' :: text, receipt_codes.contrib_month)) AND
                          (receipts.employer_id = b.employer_id))))                                         AS amount_paid,
         (((date_part('year' :: text,
                      (COALESCE((p.first_payment_date) :: timestamp with time zone, now())) :: timestamp without time zone) -
            date_part('year' :: text, bg.grace_period_end)) * (12) :: double precision) + ((date_part('month' :: text,
                                                                                             (COALESCE((p.first_payment_date) :: timestamp with time zone, now())) :: timestamp without time zone) -
                                                                                   date_part('month' :: text, bg.grace_period_end)) -
                                                                                  (1) :: double precision)) AS age_months,
         date_part('day' :: text,
                   ((COALESCE((p.first_payment_date) :: timestamp with time zone, now())) :: timestamp without time zone -
                    (bg.grace_period_end) :: timestamp without time zone))                                           AS age_days,
         p.first_payment_date,
         b.ispaid
  FROM ((main.bookings b
      JOIN main.employers e ON ((b.employer_id = e.id)))
      JOIN main.booking_payment_date p ON ((p.id = b.id)))
  join main.booking_grace_period_view bg on bg.booking_id = b.id
  WHERE (b.deleted_at IS NULL);

");



        DB::statement("select deps_save_and_drop_dependencies('main', 'booking_receivables')");


        DB::statement("DROP VIEW IF EXISTS  booking_receivables");
        DB::statement("CREATE VIEW main.booking_receivables AS 
  SELECT b.employer_id,
         b.id                                                                                              AS booking_id,
         e.name                                                                                            AS employer_name,
         e.reg_no                                                                                          AS employer_reg_no,
         b.rcv_date,
         b.amount                                                                                          AS booked_amount,
         0                                                                                                 AS amount_paid,
         (((date_part('year' :: text, (now()) :: timestamp without time zone) - date_part('year' :: text, b.rcv_date)) *
           (12) :: double precision) + ((date_part('month' :: text, (now()) :: timestamp without time zone) -
                                         date_part('month' :: text,  bg.grace_period_end)) -
                                        (1) :: double precision))                                          AS age_months,
         date_part('day' :: text,
                   ((now()) :: timestamp without time zone - ( bg.grace_period_end) :: timestamp without time zone)) AS age_days
  FROM (main.bookings b
      JOIN main.employers e ON ((b.employer_id = e.id)))
    join main.booking_grace_period_view bg on bg.booking_id = b.id
  WHERE ((b.deleted_at IS NULL) AND (b.ispaid = 0) AND (e.is_treasury = false) AND (e.duplicate_id IS NULL) AND
         (e.approval_id = 1) AND (e.deleted_at IS NULL))
  ORDER BY b.employer_id, b.rcv_date;

");

        DB::statement("select deps_restore_dependencies('main', 'booking_receivables')");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
