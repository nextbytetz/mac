<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewInspectionTablesDesignWithWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::table('inspections', function(Blueprint $table)
        {
            $table->dropColumn('inspection_type_id');
        });*/
        Schema::table('inspections', function(Blueprint $table)
        {
            $table->smallInteger('inspection_type_id')->nullable()->change();
            $table->smallInteger("wf_done")->default(0);
            $table->date("wf_done_date")->nullable();
            $table->bigInteger('inspection_type_cv_id');
            $table->bigInteger('inspection_trigger_cv_id');
            $table->bigInteger('trigger_category_cv_id');
            $table->bigInteger('staging_cv_id');
            $table->bigInteger('wf_module_id');
            $table->foreign('inspection_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('inspection_trigger_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('trigger_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('wf_module_id')->references('id')->on('wf_modules')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column inspections.inspection_type_cv_id is 'type of inspection e.g. routine, special etc'");
        DB::statement("comment on column inspections.inspection_trigger_cv_id is 'inspection is triggered by what reason, periodic plan, specific event, by system analytics etc'");
        DB::statement("comment on column inspections.trigger_category_cv_id is 'trigger category e.g. monthly plan, unregistered employers etc'");
        Schema::create('inspection_stages', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("inspection_id");
            $table->bigInteger("staging_cv_id");
            $table->json("comments");
            $table->smallInteger("allow_repeat")->default(0);
            $table->smallInteger("attended")->default(0);
            $table->smallInteger("require_attend")->default(1);
            $table->smallInteger("isdeleted")->default(0);
            $table->timestamps();
            $table->foreign('inspection_id')->references('id')->on('inspections')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column inspection_stages.allow_repeat is 'show whether this stage can be repeated at any time in the future process'");
        DB::statement("comment on column inspection_stages.attended is 'show whether a user has executed the next stage action. 1 - yes, 0 - no'");
        DB::statement("comment on column inspection_stages.isdeleted is 'show whether this stage has been deleted and has never been attended. Stages were frequently updated such that this stage was just logged and never to be attended. 1 - Yes, 0 - No'");
        Schema::create('employer_inspection_task_workflows', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("employer_inspection_task_id");
            $table->bigInteger("wf_module_id");
            $table->smallInteger("wf_done")->default(0);
            $table->date("wf_done_date")->nullable();
            $table->timestamps();
            $table->foreign('employer_inspection_task_id')->references('id')->on('employer_inspection_task')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('inspection_user', function(Blueprint $table)
        {
            $table->smallInteger('islead')->default(0);
            $table->smallInteger('isassociate')->default(0);
        });
        DB::statement("comment on column inspection_user.islead is 'specify whether allocated inspector is lead or not. 1 - Yes, 0 - No'");
        DB::statement("comment on column inspection_user.isassociate is 'specify whether inspector is not a staff but rather helping to gather more information. 1 - Yes, 0 - No'");
        Schema::create('allocations', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->string("name", 150);
            $table->bigInteger("unit_id");
            $table->smallInteger("isactive")->default(1);
            $table->text("description")->nullable();
            $table->timestamps();
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::create('allocation_user', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->smallInteger('allocation_id');
            $table->bigInteger("user_id");
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('allocation_id')->references('id')->on('allocations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table allocations is 'specify the allocation availability of users into different system functions e.g. Notification Incident Allocations, Inspection Masters etc'");
        DB::statement("comment on table allocation_user is 'users assigned in different allocations'");
        //TODO: Prepare to delete incident_user table and make use of allocations table.
        Schema::table('users', function(Blueprint $table)
        {
            $table->string("thirdparty_id", 20)->nullable();
        });
        DB::statement("comment on column users.thirdparty_id is 'any identification of the staff to the third party members eg Inspector Identification'");
        Schema::table('letters', function(Blueprint $table)
        {
            $table->string("dispatch_number", 50)->nullable();
            $table->date("delivery_date")->nullable();
            $table->string("received_by", 50)->nullable();
            $table->string("receiver_phone_number", 50)->nullable();
        });
        DB::statement("comment on column letters.dispatch_number is 'number used to track the letter sent eg EMS Number'");
        DB::statement("comment on column letters.delivery_date is 'date which the letter is delivered'");
        DB::statement("comment on column letters.received_by is 'a person who received the letter (recipient)'");
        DB::statement("comment on column letters.received_by is 'phone number of the recipient'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
