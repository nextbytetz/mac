<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewInspectionRemittanceFifth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
drop view if exists inspection_remittance;
create or replace view inspection_remittance as select zz.*, coalesce(nullif(abs(zz.paid_balance_actual), -1 * zz.paid_balance_actual), 0) paid_balance, coalesce(nullif(abs(zz.interest_balance_actual), -1 * zz.interest_balance_actual), 0) interest_balance from (select ccc.employer_inspection_task_id, ccc.employer_id, ccc.contrib_month, to_char(ccc.contrib_month, 'Mon-YY') contrib_month_formatted, ccc.contrib_amount paid_before, ccc.interest_amount interest_before, b.paid_after, d.interest_after, coalesce(ccc.underpaid, 0) - coalesce(paid_after, 0) paid_balance_actual, coalesce(ccc.interest_on_unpaid, 0) + coalesce(ccc.interest_on_paid, 0) - coalesce(d.interest_after, 0) interest_balance_actual from (select employer_inspection_task_id, employer_id, contrib_month, sum(contrib_amount) contrib_amount, sum(interest_amount) interest_amount, sum(underpaid) underpaid, sum(interest_on_unpaid) interest_on_unpaid, sum(interest_on_paid) interest_on_paid from inspection_assessment_template group by employer_inspection_task_id, employer_id, contrib_month) ccc left join (select aa.employer_id, bb.contrib_month, sum(bb.amount) paid_after from receipts aa join receipt_codes bb on aa.id = bb.receipt_id join employer_inspection_task cc on aa.employer_id = cc.employer_id where aa.isdishonoured = 0 and aa.iscancelled = 0 and bb.fin_code_id = 2 and aa.rct_date > cc.last_inspection_date group by aa.employer_id, bb.contrib_month) b on b.employer_id = ccc.employer_id and  date_part('month', b.contrib_month) = date_part('month', ccc.contrib_month) and date_part('year', b.contrib_month) = date_part('year', ccc.contrib_month)  left join (select aaa.employer_id, bbb.contrib_month, sum(bbb.amount) interest_after from receipts aaa join receipt_codes bbb on aaa.id = bbb.receipt_id join employer_inspection_task ccc on aaa.employer_id = ccc.employer_id where aaa.isdishonoured = 0 and aaa.iscancelled = 0 and bbb.fin_code_id = 1 and aaa.rct_date > ccc.last_inspection_date group by aaa.employer_id, bbb.contrib_month) d on d.employer_id = ccc.employer_id and date_part('month', d.contrib_month) = date_part('month', ccc.contrib_month) and date_part('year', d.contrib_month) = date_part('year', ccc.contrib_month) ) zz;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
