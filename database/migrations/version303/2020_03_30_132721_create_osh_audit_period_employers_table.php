<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOshAuditPeriodEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('osh_audit_period_employers', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger("osh_audit_period_id");
            $table->bigInteger("employer_id");
            $table->bigInteger("district_id")->nullable();
            $table->integer("number_of_employees")->nullable();
            $table->integer("total_claims")->nullable();
            $table->integer("number_of_fatal")->nullable();
            $table->integer("number_of_days_lost")->nullable();
            $table->integer("exemption_from_duty")->nullable();
            $table->integer("hospitalization")->nullable();
            $table->integer("light_duty")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('osh_audit_period_employers');
    }
}
