<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOshAuditScoreRatings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('osh_audit_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger("osh_audit_period_id");
            $table->bigInteger("osh_audit_period_employer_id");
            $table->bigInteger("employer_id");
            $table->integer("period");
            $table->string("fin_year");
            $table->float("ohsa");
            $table->float("training");
            $table->float("ergonomics");
            $table->float("personal_protective_equipment");
            $table->float("machine_use_maintainance");
            $table->float("chemicals");
            $table->float("emergency_preparedness");
            $table->float("adorr");
            $table->float("health_welfare");
            $table->float("rate");
            $table->float("total_score");
            $table->float("average_score");
            $table->float("possible_score");
            $table->timestamps();
        });

        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
            $table->boolean('is_submitted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('osh_audit_ratings');

        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
            $table->dropColumn('is_submitted');
        });
    }
}
