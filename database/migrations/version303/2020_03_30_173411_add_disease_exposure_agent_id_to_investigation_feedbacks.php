<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiseaseExposureAgentIdToInvestigationFeedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->integer("disease_exposure_agent_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->dropColumn('disease_exposure_agent_id');
        });
    }
}
