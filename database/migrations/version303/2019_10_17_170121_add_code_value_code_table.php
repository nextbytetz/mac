<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeValueCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_value_code', function(Blueprint $table)
        {
            $table->smallIncrements("id");
            $table->string("reference", 20);
            $table->bigInteger("code_id");
            $table->timestamps();
            $table->foreign('code_id')->references('id')->on('codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('reference')->references('reference')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
