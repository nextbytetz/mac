<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInspectionAssessmentTemplateSeventh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
select deps_save_and_drop_dependencies('main', 'inspection_assessment_template');
DROP MATERIALIZED VIEW IF EXISTS inspection_assessment_template;
create materialized view inspection_assessment_template as select uu.*, (coalesce(round(uu.underpaid, 2), 0) + coalesce(round(uu.interest_on_paid, 2), 0) + coalesce(round(uu.interest_on_unpaid, 2), 0)) accumulative_to_be_paid, to_char(uu.contrib_month, 'Mon-YY') contrib_month_formatted from (select ss.*, round(receivable_interest(ss.underpaid::decimal, ss.months_delayed_unpaid::smallint), 2) interest_on_unpaid, round(receivable_interest(ss.contrib_amount::decimal, ss.months_delayed_paid::smallint), 2) interest_on_paid from (select tt.*,  case when tt.difference_under < 0 then NULL else tt.difference_under end underpaid, case when tt.difference_over < 0 then NULL else tt.difference_over end overpaid, case when tt.months_diff_paid < 0 then 0 else tt.months_diff_paid end months_delayed_paid, case when tt.months_diff_unpaid < 0 then 0 else tt.months_diff_unpaid end months_delayed_unpaid  from (select a.id, a.employer_inspection_task_id, b.employer_id, a.contrib_month, a.member_count, a.member_count_verified, abs(coalesce(a.member_count_verified, 0) - a.member_count) member_count_difference, a.salary, a.fixed_allowance, a.non_fixed_allowance, round(a.contrib_amount/a.percent, 2) payroll_declared, nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0) + coalesce(a.non_fixed_allowance, 0), 0) payroll_verified, nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0) gross_verified, sys_contrib_amount, contrib_amount, round(a.percent * nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0), 2) contrib_amount_verified, round((a.percent * nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0)) - coalesce(a.contrib_amount, 0), 2) difference_under, round(coalesce(a.contrib_amount, 0) - (a.percent * nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0)), 2) difference_over, a.last_pay_date payment_date, case when (a.contrib_month::date < '2017-07-01'::date) and (a.first_pay_date < '2017-10-01'::date) then 0 when (a.contrib_month::date < '2017-07-01'::date) and (a.first_pay_date >= '2017-10-01'::date) then months_diff('2017-10-01'::date, a.first_pay_date) + 1 else months_diff(a.contrib_month::date, a.first_pay_date) - 1 end  months_diff_paid, case when (a.contrib_month::date < '2017-10-01'::date) then months_diff('2017-10-01'::date, b.last_inspection_date) + 1 else months_diff(a.contrib_month, b.last_inspection_date) - 1 end months_diff_unpaid, a.first_pay_date, a.last_pay_date, a.error_report, a.file_error, a.hascontriberror  from inspection_assessments a join employer_inspection_task b on a.employer_inspection_task_id = b.id) tt) ss) uu;
select deps_restore_dependencies('main', 'inspection_assessment_template');
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
