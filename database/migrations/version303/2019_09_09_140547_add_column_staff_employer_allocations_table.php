<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('staff_employer_allocations', function(Blueprint $table)
        {
            $table->smallInteger('islarge')->nullable()->change();
            $table->smallInteger('isbonus')->default(0);
            $table->softDeletes();
        });

        DB::statement("comment on column staff_employer_allocations.isbonus is 'Flag to specify if allocated employers are bonus employers i.e. 1 = bonus, 0 => not bonus'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
