<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewStartReviewEndOnInspectionPayrolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
update inspection_task_payrolls set review_start_date = a.review_start_date, review_end_date = a.review_end_date from employer_inspection_task a where a.id = inspection_task_payrolls.employer_inspection_task_id;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
