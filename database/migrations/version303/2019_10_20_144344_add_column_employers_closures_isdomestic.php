<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployersClosuresIsdomestic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->smallInteger('isdomestic')->default(0);

        });
        DB::statement("comment on column employer_closures.isdomestic is 'Flag to if employers is domestic i.e. 1 => domestic, 0 => not domestic'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
