<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewStaffEmployerOtherEmployersPerformance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW IF EXISTS  staff_other_employers_performance");
        DB::statement("CREATE VIEW main.staff_other_employers_performance AS
select
       (select sum(contrib_amount) from employer_contributions c
       left join staff_employer s on s.employer_id = c.employer_id and s.isbonus = 0 and s.isactive = 1
       where s.id is null and c.receipt_created_at >= se.start_date and receipt_created_at <= se.end_date
       ) as contribution,

        (select sum(booked_amount) from bookings_mview b
       left join staff_employer s on s.employer_id = b.employer_id and s.isbonus = 0 and s.isactive = 1
       where s.id is null and b.rcv_date < now() and b.rcv_date <= se.end_date and coalesce(b.receipt_date, se.end_date) >= se.start_date
       ) as receivable,

   (select count(1) from contributing_employers c
       join employers e on e.id = c.employer_id
       left join staff_employer s on s.employer_id = c.employer_id and s.isbonus = 0 and s.isactive = 1
       where s.id is null and e.isonline = 1 and e.approval_id = 1 and e.duplicate_id is null
       ) as online_users,

  (select count(1) from contributing_employers c
       join employers e on e.id = c.employer_id
       left join staff_employer s on s.employer_id = c.employer_id and s.isbonus = 0 and s.isactive = 1
       where s.id is null  and e.approval_id = 1 and e.duplicate_id is null
       ) as no_of_employers

from (select * from staff_employer where isactive = 1 and isbonus =  0 limit 1) as se;

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
