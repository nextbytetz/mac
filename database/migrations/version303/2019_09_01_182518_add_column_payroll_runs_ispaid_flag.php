<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollRunsIspaidFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->smallInteger('ispaid')->default(0);
        });

        DB::statement("comment on column payroll_run_vouchers.ispaid is 'Flag to specify if payments have been completed on erp for payroll beneficiaries i.e 1 = paid , 0 = not yet'");


        Schema::table('payroll_run_approvals', function(Blueprint $table)
        {
            $table->smallInteger('ispaid')->default(0);
        });

        DB::statement("comment on column payroll_run_approvals.ispaid is 'Flag to specify if payments have been completed on erp for payroll beneficiaries through all payroll run vouchers exported to erp i.e 1 = paid , 0 = not yet'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
