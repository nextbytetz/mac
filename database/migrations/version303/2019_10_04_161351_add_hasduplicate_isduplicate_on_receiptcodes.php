<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasduplicateIsduplicateOnReceiptcodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->dropColumn('frequency');
            $table->smallInteger('pay_status')->default(0);
        });
        DB::statement("comment on column receipt_codes.pay_status is 'check the contribution month pay status, 1 -> this contribution month has other duplicate payment(s) with other receipt(s), 2 -> this contribution is a duplicate of other payment with other receipt, 0 - Not Reconciled, 3 - No Duplicate'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
