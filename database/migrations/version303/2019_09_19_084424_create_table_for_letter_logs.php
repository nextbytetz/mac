<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForLetterLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('letter_logs', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("letter_id");
            $table->bigInteger("cv_id");
            $table->bigInteger("user_id");
            $table->timestamps();
            $table->foreign('letter_id')->references('id')->on('letters')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table letter_logs is 'table storing all the logs for letters'");
        DB::statement("comment on column letter_logs.cv_id is 'storing the logs for activities on letters'");
        DB::statement("comment on column letter_logs.user_id is 'user who performed an activity on specific letter'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
