<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerNameAsExtrasInDemandComplianceLetters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
update letters set extras = ('{"employername":"' || b.name::text || '", "noticereference":' || (extras->'noticereference')::text  || ', "noticedate":' || (extras->'noticedate')::text || '}'::text)::json from inspection_task_payrolls a join employer_inspection_task c on c.id = a.employer_inspection_task_id join employers b on c.employer_id = b.id where a.id = letters.resource_id and letters.cv_id in (select id from code_values where reference in ('CLDNDNOLTR','CLLTRFCMPLNC'));
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
