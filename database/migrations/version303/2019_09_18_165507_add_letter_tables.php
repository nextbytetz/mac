<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLetterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letters', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("cv_id");
            $table->bigInteger("resource_id");
            $table->string("resource_type", 150)->nullable();
            $table->boolean("canduplicate")->default("f");
            $table->smallInteger("wf_done")->default(0);
            $table->date("wf_done_date")->nullable();
            $table->timestamps();
            $table->foreign('cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table letters is 'table storing all the letter issued and printed'");
        DB::statement("comment on column letters.cv_id is 'reference to the type of letter to be issued'");
        DB::statement("comment on column letters.resource_id is 'reference to parent table which the letter issued relate e.g acknowledgement letter relate to notification_reports'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
