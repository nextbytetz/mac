<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewOshIncidentsByLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::statement("DROP MATERIALIZED VIEW IF EXISTS  osh_incidents_locations");
        DB::statement("CREATE MATERIALIZED VIEW  osh_incidents_locations  AS     select fin_year,sum(fatals_premises) as fpremises,sum(accidents_premises) as apremises,sum(fatals_out) as fopremises,sum(accidents_out) as aopremises from main.osh_employers_summary_report where fin_year is not null group by fin_year");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
