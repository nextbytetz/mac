<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuaterColumnOshAuditEmployerNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osh_audit_employer_notifications', function (Blueprint $table) {
            $table->integer("quater")->nullable();
            $table->integer("lost_days")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osh_audit_employer_notifications', function (Blueprint $table) {
            $table->dropColumn("quater");
            $table->dropColumn("lost_days");
        });
    }
}
