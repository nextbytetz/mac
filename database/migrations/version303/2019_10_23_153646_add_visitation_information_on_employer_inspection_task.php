<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisitationInformationOnEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table)
        {
            $table->date('exit_date')->nullable();
            $table->date('doc')->nullable();
            $table->date('last_inspection_date')->nullable();
            $table->date('review_start_date')->nullable();
            $table->date('review_end_date')->nullable();
        });
        DB::statement("comment on column employer_inspection_task.exit_date is 'date of the exit meeting at employer premises'");
        DB::statement("comment on column employer_inspection_task.doc is 'date when an employer started operations'");
        DB::statement("comment on column employer_inspection_task.doc is 'date when last inspection was conducted to the same employer'");
        DB::statement("comment on column employer_inspection_task.review_start_date is 'date when the assessment schedule start'");
        DB::statement("comment on column employer_inspection_task.review_end_date is 'date when the assessment schedule end'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
