<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollRecoveryAutoCalculation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payroll_recoveries', function(Blueprint $table)
        {

            $table->smallInteger('auto_calculation')->default(1);

        });

        DB::statement("comment on column payroll_recoveries.auto_calculation is 'Flag to specify if calculation was done automatic i.e 1 => auto, 0 => manaual' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
