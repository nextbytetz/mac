<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNonConveyanceOnOshAuditPeriodEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
          $table->dropColumn('number_of_employees');
          $table->dropColumn('total_claims');
          $table->dropColumn('exemption_from_duty');
          $table->dropColumn('hospitalization');
          $table->dropColumn('light_duty');
          $table->integer("not_conveyance_accident")->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
           $table->dropColumn('not_conveyance_accident');
           $table->integer("number_of_employees")->nullable();
           $table->integer("total_claims")->nullable();
           $table->integer("exemption_from_duty")->nullable();
           $table->integer("hospitalization")->nullable();
           $table->integer("light_duty")->nullable();
       });
    }
}
