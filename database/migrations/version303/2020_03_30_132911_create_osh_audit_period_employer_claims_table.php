<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOshAuditPeriodEmployerClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('osh_audit_employer_notifications', function (Blueprint $table) {
          $table->increments('id');
          $table->bigInteger("osh_audit_employer_id");
          $table->bigInteger("notification_report_id");
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('osh_audit_employer_notifications');
    }
}
