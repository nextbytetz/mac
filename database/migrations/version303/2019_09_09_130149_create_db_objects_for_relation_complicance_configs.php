<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbObjectsForRelationComplicanceConfigs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('staff_employer_configs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal('allocation_percent', 5)->default(70);
            $table->integer('non_large_employers_per_staff')->nullable();
            $table->integer('large_employers_per_staff')->nullable();
            $table->timestamps();
        });

        DB::statement("comment on table staff_employer_configs is 'Table to keep track of configurations for relation officer allocating to employers feature'");


        Schema::create('staff_employer_allocations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('no_of_employers');
            $table->integer('no_of_staff');
            $table->integer('user_id');
            $table->smallInteger('islarge');
            $table->date('start_date');
            $table->date('end_date');
            $table->smallInteger('isapproved')->default(0);
            $table->timestamps();
        });

        DB::statement("comment on table staff_employer_allocations is 'Table to keep track of staff employer allocations fro preview and approve'");
        DB::statement("comment on column staff_employer_allocations.no_of_staff is 'No of staff allocated'");
        DB::statement("comment on column staff_employer_allocations.no_of_employers is 'No of employers allocated'");
        DB::statement("comment on column staff_employer_allocations.islarge is 'Flag to specify if employers is large OR non large contributor i.e. 1 = large, 0 => not large'");

        Schema::table('staff_employer_allocations', function(Blueprint $table)
        {
              $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->integer('staff_employer_allocation_id')->nullable();
            $table->smallInteger('isbonus')->default(0);
        });
        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->foreign('staff_employer_allocation_id')->references('id')->on('staff_employer_allocations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column staff_employer.isbonus is 'Flag to specify if employers is bonus employer i.e. 1 = bonus, 0 => not bonus'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
