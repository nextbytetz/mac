<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMviewSummaryAgeAnalysis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("select deps_save_and_drop_dependencies('main', 'summary_contrib_rcvable_age_analysis')");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  summary_contrib_rcvable_age_analysis");
        DB::statement("CREATE MATERIALIZED VIEW main.summary_contrib_rcvable_age_analysis AS

  SELECT (SELECT sum(bookings_view.booked_amount) AS sum
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (0) :: double precision) AND
                 (bookings_view.age_days <= (30) :: double precision) AND (bookings_view.ispaid = 0))) AS receivable_30,
         (SELECT count(DISTINCT bookings_view.employer_id) AS count
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (0) :: double precision) AND
                 (bookings_view.age_days <= (30) :: double precision) AND (bookings_view.ispaid = 0))) AS employers_30,
         (SELECT sum(bookings_view.booked_amount) AS sum
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (31) :: double precision) AND
                 (bookings_view.age_days <= (90) :: double precision) AND
                 (bookings_view.ispaid = 0)))                                                          AS receivable_90,
         (SELECT count(DISTINCT bookings_view.employer_id) AS count
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (31) :: double precision) AND
                 (bookings_view.age_days <= (90) :: double precision) AND (bookings_view.ispaid = 0))) AS employers_90,
         (SELECT sum(bookings_view.booked_amount) AS sum
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (91) :: double precision) AND
                 (bookings_view.age_days <= (180) :: double precision) AND
                 (bookings_view.ispaid = 0)))                                                          AS receivable_180,
         (SELECT count(DISTINCT bookings_view.employer_id) AS count
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (31) :: double precision) AND
                 (bookings_view.age_days <= (90) :: double precision) AND
                 (bookings_view.ispaid = 0)))                                                          AS employers_180,
         (SELECT sum(bookings_view.booked_amount) AS sum
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (181) :: double precision) AND
                 (bookings_view.age_days <= (365) :: double precision) AND
                 (bookings_view.ispaid = 0)))                                                          AS receivable_365,
         (SELECT count(DISTINCT bookings_view.employer_id) AS count
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (181) :: double precision) AND
                 (bookings_view.age_days <= (365) :: double precision) AND
                 (bookings_view.ispaid = 0)))                                                          AS employers_365,
         (SELECT sum(bookings_view.booked_amount) AS sum
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (366) :: double precision) AND
                 (bookings_view.ispaid = 0)))                                AS receivable_366,
         (SELECT count(DISTINCT bookings_view.employer_id) AS count
          FROM main.bookings_view
          WHERE ((bookings_view.age_days >= (366) :: double precision) AND
                 (bookings_view.ispaid = 0)))                                                          AS employers_366;

");

        DB::statement("select deps_restore_dependencies('main', 'summary_contrib_rcvable_age_analysis')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
