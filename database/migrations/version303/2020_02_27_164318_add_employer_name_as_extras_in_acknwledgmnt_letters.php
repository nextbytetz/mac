<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerNameAsExtrasInAcknwledgmntLetters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
update letters set extras = ('{"employername":"' || b.name::text || '"}'::text)::json from notification_reports a join employers b on a.employer_id = b.id where a.id = letters.resource_id and letters.cv_id in (select id from code_values where reference = 'CLACKNOWLGMNT');
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
