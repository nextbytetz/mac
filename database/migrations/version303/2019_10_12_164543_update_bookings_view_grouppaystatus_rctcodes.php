<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingsViewGrouppaystatusRctcodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("select deps_save_and_drop_dependencies('main', 'booking_paid')");
        DB::statement("DROP VIEW IF EXISTS  booking_paid");
        DB::statement("CREATE VIEW main.booking_paid AS

SELECT b.employer_id,
       b.id                                                       AS booking_id,
       e.name                                                     AS employer_name,
       e.reg_no                                                   AS employer_reg_no,
       b.rcv_date,
       b.amount                                                   AS booked_amount,
       COALESCE((SELECT sum(c.contrib_amount) AS sum
                 FROM main.employer_contributions c
                 where c.booking_id = b.id and c.islegacy = 0),
                (SELECT sum(c.contrib_amount) AS sum
                 FROM main.employer_contributions c
                 WHERE (date_part('month' :: text, b.rcv_date) =
                                                           date_part('month' :: text, c.contrib_month)) AND
                        (date_part('year' :: text, b.rcv_date) =
                         date_part('year' :: text, c.contrib_month)) and c.islegacy = 0
                        )) AS amount_paid
FROM (main.bookings b
    JOIN main.employers e ON ((b.employer_id = e.id)))
WHERE ((b.deleted_at IS NULL) AND (b.ispaid = 1)) ;


");

        DB::statement("select deps_restore_dependencies('main', 'booking_paid')");



        DB::statement("select deps_save_and_drop_dependencies('main', 'booking_payment_date')");
        DB::statement("DROP VIEW IF EXISTS  booking_payment_date");
        DB::statement("CREATE VIEW main.booking_payment_date AS

  SELECT b.id,
         COALESCE((SELECT min(receipts.rct_date) AS payment_date
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id) and receipt_codes.grouppaystatus = 0)),
                  (SELECT min(receipts.rct_date) AS sum
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (date_part('month' :: text, b.rcv_date) =
                                                             date_part('month' :: text, receipt_codes.contrib_month)) AND
                          (date_part('year' :: text, b.rcv_date) =
                           date_part('year' :: text, receipt_codes.contrib_month)) AND
                          (receipts.employer_id = b.employer_id) and receipt_codes.grouppaystatus = 0)))                            AS first_payment_date,
         COALESCE((SELECT min(receipts.created_at) AS receipt_date
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id) and receipt_codes.grouppaystatus = 0)),
                  (SELECT min(receipts.created_at) AS receipt_date
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (date_part('month' :: text, b.rcv_date) =
                                                             date_part('month' :: text, receipt_codes.contrib_month)) AND
                          (date_part('year' :: text, b.rcv_date) =
                           date_part('year' :: text, receipt_codes.contrib_month)) AND
                          (receipts.employer_id = b.employer_id) and receipt_codes.grouppaystatus = 0)))                            AS receipt_date,
         (SELECT min(fin_years.end_date) AS min
          FROM main.fin_years
          WHERE ((b.rcv_date >= fin_years.start_date) AND (b.rcv_date <= fin_years.end_date))) AS end_fiscal_year
  FROM main.bookings b
  WHERE (b.deleted_at IS NULL);
");

        DB::statement("select deps_restore_dependencies('main', 'booking_payment_date')");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
