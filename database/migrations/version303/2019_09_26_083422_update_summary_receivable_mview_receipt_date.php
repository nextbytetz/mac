<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSummaryReceivableMviewReceiptDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  summary_contrib_rcvable_income_mview");
        DB::statement("CREATE MATERIALIZED VIEW main.summary_contrib_rcvable_income_mview AS

  SELECT f.id                                                            AS fin_year_id,
         concat_ws('/' :: text, f.start, f.\"end\")                        AS fiscal_year,
         (SELECT sum(employer_contributions.contrib_amount) AS sum
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND
                 ((employer_contributions.receipt_created_at) :: date >= f.start_date) AND
                 ((employer_contributions.receipt_created_at) :: date <= f.end_date) AND
                 (employer_contributions.contrib_month >= f.start_date) AND (employer_contributions.islegacy = 0) AND
                 (employer_contributions.contrib_month <= f.end_date)))  AS receipt_contrib,
         (SELECT count(DISTINCT employer_contributions.employer_id) AS count
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND
                 ((employer_contributions.receipt_created_at) :: date >= f.start_date) AND
                 ((employer_contributions.receipt_created_at) :: date <= f.end_date) AND
                 (employer_contributions.contrib_month >= f.start_date) AND (employer_contributions.islegacy = 0) AND
                 (employer_contributions.contrib_month <= f.end_date)))  AS receipt_contrib_employers,
         (SELECT sum(employer_contributions.contrib_amount) AS sum
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND
                 ((employer_contributions.receipt_created_at) :: date >= f.start_date) AND
                 ((employer_contributions.receipt_created_at) :: date <= f.end_date) AND
                 (employer_contributions.islegacy = 0) AND
                 (employer_contributions.contrib_month < f.start_date))) AS receipt_prev_contrib,
         (SELECT count(DISTINCT employer_contributions.employer_id) AS count
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND (employer_contributions.islegacy = 0) AND
                 (employer_contributions.contrib_month < f.start_date))) AS receipt_prev_contrib_employers,

         (SELECT sum(b.booked_amount) AS sum
          FROM (main.bookings_mview b
              JOIN main.employers e ON ((e.id = b.employer_id)))
          WHERE (   (COALESCE(b.receipt_date, (f.end_date::date + '1 day' :: interval) :: timestamp without time zone) > f.end_date) AND
                 ((b.rcv_date >= '2015-07-01' :: date) AND (b.rcv_date < f.start_date)) AND
                 (e.employer_status <> 3)))                              AS receivable_open_balance,
         (SELECT count(DISTINCT b.employer_id) AS count
          FROM (main.bookings_mview b
              JOIN main.employers e ON ((e.id = b.employer_id)))
          WHERE (   (COALESCE(b.receipt_date, (f.end_date::date + '1 day' :: interval) :: timestamp without time zone) > f.end_date) AND
                 ((b.rcv_date >= '2015-07-01' :: date) AND (b.rcv_date < f.start_date)) AND
                 (e.employer_status <> 3)))                              AS receivable_open_balance_employers
  FROM main.fin_years f;

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
