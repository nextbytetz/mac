<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerIdToWorkplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workplaces', function (Blueprint $table) {
            $table->integer("employer_id")->nullable();
            $table->renameColumn("workplace","workplace_name");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->dropColumn('employer_id');
            $table->renameColumn("workplace_name","workplace");
        });
    }
}
