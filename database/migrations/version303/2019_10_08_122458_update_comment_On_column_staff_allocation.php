<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommentOnColumnStaffAllocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("comment on column staff_employer_allocations.isbonus is 'Flag to specify if employers allocated belong to which type of bonus i.e. 0 => not bonus, 1 => Dormant bonus, 2 => Treasury bonus '");
        DB::statement("comment on column staff_employer_allocations.category is 'Category of this allocation i.e. 1 => large, 2 => Non large, 3 => Bonus, 4 => treasury '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
