<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingMviewsClone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("CREATE MATERIALIZED VIEW main.bookings_mview_clone AS 
  SELECT b.employer_id,
         b.booking_id,
         b.employer_name,
         b.employer_reg_no,
         b.rcv_date,
         b.booked_amount,
         b.amount_paid,
         b.age_months,
         b.age_days,
         b.first_payment_date,
         b.employer_registered_date,
         b.ispaid,
         e.min_payment_date AS employer_min_pay_date,
         b.receipt_date
  FROM main.bookings_view  b
join employers_min_payment_date e on e.employer_id = b.employer_id;


");


        $sql = <<<SQL
CREATE INDEX emp_clone_index1 ON bookings_mview_clone (employer_id);
CREATE INDEX booking_id_clone_index2 ON bookings_mview_clone (booking_id);
CREATE INDEX rcv_date_clone_index3 ON bookings_mview_clone (rcv_date);
CREATE INDEX first_pay_clone_index4 ON bookings_mview_clone (first_payment_date);
CREATE INDEX emp_min_pay_clone_index5 ON bookings_mview_clone (employer_min_pay_date);
 
SQL;
        DB::unprepared($sql);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
