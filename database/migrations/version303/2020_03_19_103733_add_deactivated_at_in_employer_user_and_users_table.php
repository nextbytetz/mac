<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeactivatedAtInEmployerUserAndUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::table('portal.users', function (Blueprint $table) {
        $table->integer('mac_deactivated_by')->nullable();
        $table->timestamp('mac_deactivated_at')->nullable();
        
        $table->integer('mac_activated_by')->nullable();
        $table->timestamp('mac_activated_at')->nullable();

        $table->integer('category_changed_by')->nullable();
        $table->timestamp('category_changed_at')->nullable();
    });

     Schema::table('portal.employer_user', function (Blueprint $table) {
        $table->integer('mac_deactivated_by')->nullable();
        $table->timestamp('mac_deactivated_at')->nullable();

        $table->integer('mac_added_by')->nullable();
        $table->timestamp('mac_added_at')->nullable();

        $table->integer('mac_activated_by')->nullable();
        $table->timestamp('mac_activated_at')->nullable();
    }); 

     Schema::table('portal.contribution_arrears', function (Blueprint $table) {
        $table->integer('mac_added_by')->nullable();
        $table->timestamp('mac_added_at')->nullable();

        $table->integer('mac_updated_by')->nullable();
        $table->timestamp('mac_updated_at')->nullable();

        $table->string('mac_add_reason')->nullable();
        $table->string('mac_clear_reason')->nullable();

        $table->integer('mac_uploaded_by')->nullable();
        $table->timestamp('mac_uploaded_at')->nullable();
    }); 
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.users', function (Blueprint $table) {
            $table->dropColumn('mac_deactivated_by');
            $table->dropColumn('mac_deactivated_at');
            $table->dropColumn('mac_activated_by');
            $table->dropColumn('mac_activated_at');
            $table->dropColumn('category_changed_by');
            $table->dropColumn('category_changed_at');
        });

        Schema::table('portal.employer_user', function (Blueprint $table) {
            $table->dropColumn('mac_deactivated_by');
            $table->dropColumn('mac_deactivated_at');
            $table->dropColumn('mac_activated_by');
            $table->dropColumn('mac_activated_at');
            $table->dropColumn('mac_added_by');
            $table->dropColumn('mac_added_at');

        }); 

        Schema::table('portal.contribution_arrears', function (Blueprint $table) {
            $table->dropColumn('mac_added_by');
            $table->dropColumn('mac_added_at');
            $table->dropColumn('mac_updated_by');
            $table->dropColumn('mac_updated_at');
            $table->dropColumn('mac_add_reason');
            $table->dropColumn('mac_clear_reason');
            $table->dropColumn('mac_uploaded_by');
            $table->dropColumn('mac_uploaded_at');
        }); 
    }
}
