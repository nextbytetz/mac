<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsduplicatePayrollTransVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payment_voucher_transactions', function (Blueprint $table) {
            $table->smallInteger("isduplicate")->default(0);
        });
        DB::statement("comment on column payment_voucher_transactions.isduplicate is 'Flag to specify if transaction already exist in claims payable i.e. 1 => exist, 0 => not exists'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
