<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_allocations', function(Blueprint $table)
        {
            $table->text('selected_staff')->nullable();
            $table->date('approved_date')->nullable();
        });

        DB::statement("comment on column staff_employer_allocations.selected_staff is 'Array of staff selected for this allocation'");


        Schema::table('staff_employer', function(Blueprint $table)
        {
             $table->unique(['user_id', 'employer_id', 'staff_employer_allocation_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
