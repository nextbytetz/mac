<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentLetterForLetters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_letter', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('letter_id')->index();
            $table->bigInteger('document_id')->index();
            $table->string('name', 150)->nullable();
            $table->text('description')->nullable();
            $table->string('ext', 20)->nullable();
            $table->float('size', 10, 0)->nullable();
            $table->string('mime', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('eoffice_document_id')->nullable()->unique();
            $table->date('doc_date')->nullable();
            $table->date('doc_receive_date')->nullable();
            $table->date('doc_create_date')->nullable();
            $table->integer('folio')->nullable();
            $table->smallInteger('isreferenced')->default(0);
        });
        DB::statement("comment on column document_letter.doc_date is 'date when document was prepared by the member'");
        DB::statement("comment on column document_letter.doc_receive_date is 'date when the document was received at WCF office'");
        DB::statement("comment on column document_letter.doc_create_date is 'ate when document was created in e-office'");
        DB::statement("comment on column document_letter.isreferenced is 'show whether document has already been referenced for a specific benefit or not. 1 - Yes, 0 - No'");
        Schema::table('document_letter', function (Blueprint $table) {
            $table->date("date_reference")->nullable();
        });
        DB::statement("comment on column document_letter.date_reference is 'date reference of the specific uploaded document'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
