<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialzedViewForBookingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("CREATE MATERIALIZED VIEW main.bookings_mview AS 
select b.*, e.min_payment_date as employer_min_pay_date from bookings_view b
join employers_min_payment_date e on e.employer_id = b.employer_id;


");


        $sql = <<<SQL
CREATE INDEX emp_index1 ON bookings_mview (employer_id);
CREATE INDEX booking_id_index2 ON bookings_mview (booking_id);
CREATE INDEX rcv_date_index3 ON bookings_mview (rcv_date);
CREATE INDEX first_pay_index4 ON bookings_mview (first_payment_date);
CREATE INDEX emp_min_pay_index5 ON bookings_mview (employer_min_pay_date);
 
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
