<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexesIntoReceiptsCodesBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('receipts', function(Blueprint $table)
        {
            $table->index('created_at');

        });

        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->index('booking_id_orig');

        });


        Schema::table('bookings', function(Blueprint $table)
        {
            $table->index('ispaid');

        });

        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->index('miss_month');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
