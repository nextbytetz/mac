<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileUploadControlEmployerInspectionAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            $table->text("upload_error")->nullable();
            $table->smallInteger("file_error")->default(0);
        });
        DB::statement("comment on column employer_inspection_task.upload_error is 'exception raised during the upload of file'");
        DB::statement("comment on column employer_inspection_task.file_error is 'shows whether the is an upload error of inspection assessment 1 - Generic Error, 0 - No error, 2 - Uploaded file error'");
        Schema::table('inspection_assessments', function (Blueprint $table) {
            $table->text("error_report")->nullable();
            $table->smallInteger("file_error")->default(0);
        });
        DB::statement("comment on column inspection_assessments.error_report is 'error message for this entry'");
        DB::statement("comment on column inspection_assessments.file_error is 'specify whether there is an entry error 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
