<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPrecisionForInterestAmountInspectionAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("select deps_save_and_drop_dependencies('main', 'inspection_assessments');");
        Schema::table('inspection_assessments', function (Blueprint $table) {
            $table->decimal('interest_amount', 14)->nullable()->change();
        });
        DB::statement("comment on column inspection_assessments.interest_amount is 'interest amount paid before going for inspection'");
        DB::statement("select deps_restore_dependencies('main', 'inspection_assessments');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
