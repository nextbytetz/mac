<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeparateCompliantAndOverpayStatusEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            $table->dropColumn("assessment_status");
            $table->smallInteger("iscompliant")->nullable();
            $table->smallInteger("hasoverpay")->default(0);
        });
        DB::statement("comment on column employer_inspection_task.iscompliant is 'show whether an employer is compliant or not, 1 - Compliant, 0 - Not Compliant'");
        DB::statement("comment on column employer_inspection_task.hasoverpay is 'show whether the employer has overpaid 1 - Yes Over Paid, 0 - Not Over Paid'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
