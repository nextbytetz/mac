<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffEmployerContribPerformanceMview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //


        DB::statement("select deps_save_and_drop_dependencies('main', 'booked_interests')");
        DB::statement("DROP VIEW IF EXISTS  booked_interests");
        DB::statement("CREATE VIEW main.booked_interests AS
        
        SELECT i.id                                                                          AS interest_id,
       i.booking_id,
       b.employer_id,
       e.name                                                                        AS employer_name,
       e.reg_no                                                                      AS employer_reg_no,
       e.region_id,
       i.miss_month,
       i.hasreceipt,
       i.amount                                                                      AS booked_amount,
       (SELECT sum(receipt_codes.amount) AS sum
        FROM (main.receipt_codes
            JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
        WHERE ((receipt_codes.fin_code_id = 1) AND (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
               (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id))) AS amount_paid,
       (SELECT min(receipts.created_at) AS date
        FROM (main.receipts
            JOIN main.receipt_codes ON ((receipt_codes.receipt_id = receipts.id)))
        WHERE ((receipt_codes.fin_code_id = 1) AND (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
               (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = i.booking_id))) AS receipt_created_at
FROM ((main.booking_interests i
    JOIN main.bookings b ON ((b.id = i.booking_id)))
    JOIN main.employers e ON ((b.employer_id = e.id)))
WHERE ((b.deleted_at IS NULL) AND (i.deleted_at IS NULL));

        
        ");


        DB::statement("select deps_restore_dependencies('main', 'booked_interests')");




        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
