<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewInspectionRemittanceSecond extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
drop view if exists inspection_remittance;
create or replace view inspection_remittance as select zz.*, coalesce(nullif(abs(zz.paid_balance_actual), -1 * zz.paid_balance_actual), 0) paid_balance, coalesce(nullif(abs(zz.interest_balance_actual), -1 * zz.interest_balance_actual), 0) interest_balance from (select a.id employer_inspection_task_id, a.employer_id, c.contrib_month, to_char(c.contrib_month, 'Mon-YY') contrib_month_formatted, c.contrib_amount paid_before, b.paid_after, d.interest_after, c.underpaid - coalesce(paid_after, 0) paid_balance_actual, c.interest_on_unpaid + c.interest_on_paid - coalesce(d.interest_after, 0) interest_balance_actual from employer_inspection_task a join inspection_assessment_template c on a.id = c.employer_inspection_task_id left join (select bb.employer_id, bb.contrib_month, sum(bb.amount) paid_after from receipts aa join receipt_codes bb on aa.id = bb.receipt_id join employer_inspection_task cc on bb.employer_id = cc.employer_id where aa.isdishonoured = 0 and aa.iscancelled = 0 and bb.fin_code_id = 2 and aa.created_at > cc.last_inspection_date group by bb.employer_id, bb.contrib_month) b on b.employer_id = a.employer_id and b.contrib_month = c.contrib_month left join (select bbb.employer_id, bbb.contrib_month, sum(bbb.amount) interest_after from receipts aaa join receipt_codes bbb on aaa.id = bbb.receipt_id join employer_inspection_task ccc on bbb.employer_id = ccc.employer_id where aaa.isdishonoured = 0 and aaa.iscancelled = 0 and bbb.fin_code_id = 1 and aaa.created_at > ccc.last_inspection_date group by bbb.employer_id, bbb.contrib_month) d on d.employer_id = a.employer_id and d.contrib_month = c.contrib_month) zz;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
