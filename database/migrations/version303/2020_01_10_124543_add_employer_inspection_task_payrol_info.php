<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerInspectionTaskPayrolInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_task_payrolls', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("inspection_task_id");
            $table->integer("payroll_id")->nullable();
            $table->string("name", 100);
            $table->smallInteger("isactive")->default(0);
            $table->timestamps();
            $table->foreign('inspection_task_id')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            $table->bigInteger("inspection_task_payroll_id")->nullable();
            $table->foreign('inspection_task_payroll_id')->references('id')->on('inspection_task_payrolls')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table inspection_task_payrolls is 'record all the payrolls for an employer for inspection'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
