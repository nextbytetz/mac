<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOshAuditEmployersAuditFilterFromRegistraionToReportingDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  osh_employers_summary_report");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  osh_audit_checklist_employers_summary");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  osh_audit_checklist_employers");
        DB::statement("CREATE MATERIALIZED VIEW  osh_audit_checklist_employers  AS     SELECT a.filename,
    a.employee,
    a.employer,
    a.employer_id,
    a.incident,
    a.incident_date,
    a.reporting_date,
    a.notification_date,
    a.registration_date,
    a.resource_id,
    a.district,
    a.region,
    b.name AS fin_year,
    b.id AS fin_year_id,
    a.incident_occurrence_cv_id AS conveyance_death,
    a.accident_type_id AS conveyance_accident,
    a.name as diseases_name,
    a.total_lost_working_days as lost_days
   FROM ( SELECT a_1.filename,
            concat_ws(' '::text, b_1.firstname, COALESCE(b_1.middlename, ''::character varying), b_1.lastname) AS employee,
            d.name AS employer,
            a_1.employer_id,
            c.name AS incident,
            a_1.incident_date,
            a_1.reporting_date,
            a_1.receipt_date AS notification_date,
            a_1.created_at AS registration_date,
            a_1.id AS resource_id,
            o.name AS district,
            p.name AS region,
            e.incident_occurrence_cv_id,
            f.accident_type_id,
            g.name,
            h.total_lost_working_days
           FROM main.notification_reports a_1
             JOIN main.employees b_1 ON a_1.employee_id = b_1.id
             JOIN main.incident_types c ON a_1.incident_type_id = c.id
             JOIN main.employers d ON d.id = a_1.employer_id
             LEFT JOIN main.districts o ON o.id = a_1.district_id
             LEFT JOIN main.regions p ON o.region_id = p.id
             LEFT JOIN main.deaths e ON a_1.id = e.notification_report_id
             LEFT JOIN main.accidents f ON a_1.id = f.notification_report_id
             left join main.diseases g on a_1.id=g.notification_report_id
             left join (select neb.notification_report_id,sum(nds.days) as total_lost_working_days
from main.notification_disability_states nds join main.notification_eligible_benefits neb on nds.notification_eligible_benefit_id=neb.id
join main.notification_reports nr on neb.notification_report_id=nr.id group by neb.notification_report_id Having(sum(nds.days))>30) h on a_1.id=h.notification_report_id
          WHERE a_1.isprogressive = 0 AND a_1.deleted_at IS null
        UNION ALL
         SELECT a_1.filename,
            concat_ws(' '::text, b_1.firstname, COALESCE(b_1.middlename, ''::character varying), b_1.lastname) AS employee,
            d.name AS employer,
            a_1.employer_id,
            c.name AS incident,
            a_1.incident_date,
            a_1.reporting_date,
            a_1.receipt_date AS notification_date,
            a_1.created_at AS registration_date,
            a_1.id AS resource_id,
            o.name AS district,
            p.name AS region,
            e.incident_occurrence_cv_id,
            f.accident_type_id,
             g.name,
            h.total_lost_working_days
           FROM main.notification_reports a_1
             JOIN main.employees b_1 ON a_1.employee_id = b_1.id
             JOIN main.incident_types c ON a_1.incident_type_id = c.id
             JOIN main.employers d ON d.id = a_1.employer_id
             LEFT JOIN main.districts o ON o.id = a_1.district_id
             LEFT JOIN main.regions p ON o.region_id = p.id
             LEFT JOIN main.code_values q ON a_1.notification_staging_cv_id = q.id
             LEFT JOIN main.codes r ON q.code_id = r.id
             LEFT JOIN main.deaths e ON a_1.id = e.notification_report_id
             LEFT JOIN main.accidents f ON a_1.id = f.notification_report_id
             left join main.diseases g on a_1.id=g.notification_report_id
             left join (select neb.notification_report_id,sum(nds.days) as total_lost_working_days
from main.notification_disability_states nds join main.notification_eligible_benefits neb on nds.notification_eligible_benefit_id=neb.id
join main.notification_reports nr on neb.notification_report_id=nr.id group by neb.notification_report_id Having(sum(nds.days))>30) h on a_1.id=h.notification_report_id
          WHERE a_1.isprogressive = 1 AND (r.id = ANY (ARRAY[13::bigint, 23::bigint])) AND a_1.deleted_at IS null) a
     LEFT JOIN main.fin_years b ON a.reporting_date::date >= b.start_date AND a.reporting_date::date <= b.end_date");

//employer_summary
DB::statement("CREATE MATERIALIZED VIEW  osh_audit_checklist_employers_summary  AS     select o.fin_year,o.employer,e.reg_no as RegNo,d.name as hq_district,r.name as hq_region,sum(o.fatal) as fatals_premises,sum(o.accidents) as accidents_premises,sum(o.disease) as disease,sum(o.claims_total) as total_claims,sum(o.total_lost_days) as total_days_lost from (
select employer_id,employer,fin_year,sum(death) as fatal,sum(accidents) as accidents,sum(diseases) as disease,sum(total_claims) as claims_total,sum(t_lost_days) as total_lost_days from (
SELECT
s.employer_id,s.employer,s.fin_year,s.death,s.accidents,s.diseases,s.t_lost_days,s.total_claims
FROM
(
SELECT  employer_id,employer,fin_year,
CASE    WHEN (conveyance_death=224) THEN count(conveyance_death) ELSE 0 END AS death,
CASE    WHEN (conveyance_accident=1) THEN count(conveyance_accident) ELSE 0 END AS accidents,
count(diseases_name) as diseases,
sum(lost_days) as t_lost_days,
count(*) as total_claims
FROM main.osh_audit_checklist_employers group by employer_id,employer,fin_year,conveyance_death,conveyance_accident,diseases_name
) s) q group by employer_id,employer,fin_year) o
join main.employers e on o.employer_id=e.id join main.districts d on e.district_id=d.id join main.regions r on d.region_id=r.id
group by o.employer,e.reg_no,d.name,r.name,o.fin_year");

DB::statement("CREATE MATERIALIZED VIEW  osh_employers_summary_report  AS     select o.fin_year,o.employer_id,o.employer,e.reg_no as RegNo,d.name as hq_district,r.name as hq_region,
sum(o.fatal) as fatals_premises,sum(o.out_fatal) as fatals_out,
sum(o.accidents) as accidents_premises,sum(o.out_accident) as accidents_out,
sum(o.disease) as disease,
sum(o.claims_total) as total_claims,
sum(o.total_lost_days) as total_days_lost, 
(sum(o.fatal)+sum(o.out_fatal)+sum(o.accidents)+sum(o.out_accident)+sum(o.disease)) as fatal_and_non_fatal,
(sum(o.fatal)+sum(o.accidents)) as on_premise_fatals_and_accidents,
(sum(o.accidents)+sum(o.out_accident)) as accidents_workplace_reported,
(sum(o.fatal)+sum(o.out_fatal)) as occupational_fatal
from (
select employer_id,employer,fin_year,sum(death) as fatal,sum(non_conv_death) as out_fatal,sum(accidents) as accidents,sum(non_conv_accident) as out_accident,sum(diseases) as disease,sum(total_claims) as claims_total,sum(t_lost_days) as total_lost_days from (
SELECT
s.employer_id,s.employer,s.fin_year,s.death,s.accidents,s.diseases,s.t_lost_days,s.total_claims,s.non_conv_death,s.non_conv_accident
FROM
(
SELECT  employer_id,employer,fin_year,
CASE    WHEN (conveyance_death=224) THEN count(conveyance_death) ELSE 0 END AS death,
CASE    WHEN (conveyance_death=225) THEN count(conveyance_death) ELSE 0 END AS non_conv_death,
CASE    WHEN (conveyance_accident=1) THEN count(conveyance_accident) ELSE 0 END AS accidents,
CASE    WHEN (conveyance_accident=2) THEN count(conveyance_accident) ELSE 0 END AS non_conv_accident,
count(diseases_name) as diseases,
sum(lost_days) as t_lost_days,
count(*) as total_claims
FROM main.osh_audit_checklist_employers group by employer_id,employer,fin_year,conveyance_death,conveyance_accident,diseases_name
) s) q group by employer_id,employer,fin_year) o
join main.employers e on o.employer_id=e.id join main.districts d on e.district_id=d.id join main.regions r on d.region_id=r.id 
group by o.employer_id,o.employer,e.reg_no,d.name,r.name,o.fin_year");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
