<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDocumentLetterToDocumentResource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename("document_letter", "document_resource");
        Schema::table('document_resource', function (Blueprint $table) {
            $table->renameColumn('letter_id', 'resource_id');
            $table->string("resource_type", 150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
