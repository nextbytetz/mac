<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_employer_inspection_task', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('employer_inspection_task_id')->index();
            $table->bigInteger('document_id')->index();
            $table->string('name', 150)->nullable();
            $table->text('description')->nullable();
            $table->string('ext', 20)->nullable();
            $table->float('size', 10, 0)->nullable();
            $table->string('mime', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('eoffice_document_id')->nullable()->unique();
            $table->date('doc_date')->nullable();
            $table->date('doc_receive_date')->nullable();
            $table->date('doc_create_date')->nullable();
            $table->integer('folio')->nullable();
            $table->smallInteger('isreferenced')->default(0);
        });
        DB::statement("comment on column document_employer_inspection_task.doc_date is 'date when document was prepared by the member'");
        DB::statement("comment on column document_employer_inspection_task.doc_receive_date is 'date when the document was received at WCF office'");
        DB::statement("comment on column document_employer_inspection_task.doc_create_date is 'ate when document was created in e-office'");
        DB::statement("comment on column document_employer_inspection_task.isreferenced is 'show whether document has already been referenced for a specific benefit or not. 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_employer_inspection_task', function (Blueprint $table) {
            //
        });
    }
}
