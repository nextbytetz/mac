<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployerClosureReopens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('employer_closure_reopens', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_closure_id');
            $table->date('open_date');
            $table->text('reason');
            $table->smallInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->smallInteger('status')->default(0);
            $table->integer('user_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table employer_closure_reopens is 'Table to keep track of all reopens of employer closures '");
        DB::statement("comment on column employer_closure_reopens.status is 'Flag to specify status of reopen i.e. 0 => pending, 1 => initiated, 2=> Approved  '");

        Schema::table('employer_closure_reopens', function(Blueprint $table)
        {
            $table->foreign('employer_closure_id')->references('id')->on('employer_closures')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
