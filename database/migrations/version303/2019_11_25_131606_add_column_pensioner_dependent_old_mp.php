<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPensionerDependentOldMp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*Pensioners*/
        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->decimal('old_mp', 15,2)->nullable();
        });
        DB::statement("comment on column pensioners.old_mp is 'Monthly pension amount before reassessment'");

        /*Dependents*/
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->decimal('old_mp', 15,2)->nullable();
        });
        DB::statement("comment on column dependent_employee.old_mp is 'Monthly pension amount before reassessment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
