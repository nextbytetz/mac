<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffEmployerPerformanceMview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  staff_employer_contrib_performance");
        DB::statement("CREATE MATERIALIZED VIEW main.staff_employer_contrib_performance AS 
        
  SELECT staff.user_id,
         concat_ws(' ' :: text, u1.firstname, u1.lastname)                                     AS fullname,
         (SELECT count(1) AS count
          FROM main.staff_employer scount
         join staff_employer_allocations a on scount.staff_employer_allocation_id = a.id
          WHERE ((a.isactive = 1) AND scount.isduplicate = 0 AND (scount.isbonus = 0)  AND  (scount.user_id = staff.user_id)))                  AS no_of_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM ((main.bookings_view b
              JOIN main.staff_employer starget ON ((
            (starget.employer_id = b.employer_id) AND (starget.user_id = staff.user_id) AND (starget.isduplicate = 0) AND (starget.isbonus = 0)  AND
            (starget.unit_id = 5))))
              join staff_employer_allocations at on starget.staff_employer_allocation_id = at.id
              JOIN main.booking_grace_period_view grace ON ((grace.booking_id = b.booking_id)))
          WHERE ((b.ispaid = 0) AND (grace.grace_period_end <= (now()) :: date) AND
                 (grace.grace_period_end <= starget.end_date) AND at.isactive = 1 AND
                 (COALESCE((b.receipt_date) :: date, starget.end_date) > starget.start_date))) AS target_receivable,
         (SELECT sum(contrib.contrib_amount) AS sum
          FROM (main.employer_contributions contrib
              JOIN main.staff_employer sactual ON ((
            (sactual.employer_id = contrib.employer_id) AND (sactual.user_id = staff.user_id) AND (sactual.isduplicate = 0) AND (sactual.isbonus = 0)
            AND (sactual.unit_id = 5)))
              join staff_employer_allocations ac on sactual.staff_employer_allocation_id = ac.id)
          WHERE ((contrib.islegacy = 0) AND ((contrib.receipt_created_at) :: date >= sactual.start_date) AND ac.isactive = 1 AND
                 (((contrib.receipt_created_at) :: date <= (now()) :: date) AND
                  ((contrib.receipt_created_at) :: date <= sactual.end_date)) AND
                 (contrib.contrib_month <= (now()) :: date) AND
                 (contrib.contrib_month <= sactual.end_date)))                                 AS contribution,
         (SELECT sum(i.booked_amount) AS sum
          FROM (main.booked_interests i
              JOIN main.staff_employer si ON ((
            (si.employer_id = i.employer_id) AND (si.user_id = staff.user_id) AND (si.isduplicate = 0) AND (si.isbonus = 0) AND
            (si.unit_id = 5)))
              join staff_employer_allocations ai on si.staff_employer_allocation_id = ai.id)
              WHERE (COALESCE((i.receipt_created_at) :: date, si.end_date) > si.start_date) AND ai.isactive = 1)       AS target_interest,
         (SELECT sum(ip.interest_amount) AS sum
          FROM (main.employer_interest_payments ip
              JOIN main.staff_employer sip ON ((
            (sip.employer_id = ip.employer_id) AND (sip.user_id = staff.user_id) AND (sip.isduplicate = 0) AND (sip.isbonus = 0) AND
            (sip.unit_id = 5)))
                join staff_employer_allocations aip on sip.staff_employer_allocation_id = aip.id)
          WHERE (((ip.receipt_created_at) :: date >= sip.start_date) AND  aip.isactive = 1 AND
                 ((ip.receipt_created_at) :: date <= (now()) :: date)))                        AS interest_paid
  FROM ((SELECT s.user_id
         FROM (main.users u
             JOIN main.staff_employer s ON (((u.id = s.user_id) AND (s.isactive = 1)  AND (s.isbonus = 0) AND (s.unit_id = 5))))
         GROUP BY s.user_id) staff
      JOIN main.users u1 ON ((u1.id = staff.user_id)));


");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
