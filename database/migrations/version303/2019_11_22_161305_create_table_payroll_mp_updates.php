<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrollMpUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_mp_updates', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('member_type_id');
            $table->integer('resource_id');
            $table->integer('employee_id');
            $table->decimal('old_mp', 14,2);
            $table->decimal('new_mp', 14,2);
            $table->text('remark');
            $table->smallInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->integer('user_id');
            $table->string('folionumber')->nullable();
            $table->timestamps();
            $table->softDeletes();

            /*Foreign keys*/
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table payroll_mp_updates is 'Table to keep track of all changes of payroll monthly pensions'");
        DB::statement("comment on column payroll_mp_updates.folionumber is 'Folio number of the document used to validate change of monthly pensions'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
