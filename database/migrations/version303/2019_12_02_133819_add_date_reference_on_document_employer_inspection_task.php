<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateReferenceOnDocumentEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_employer_inspection_task', function (Blueprint $table) {
            $table->date("date_reference")->nullable();
        });
        DB::statement("comment on column document_employer_inspection_task.date_reference is 'date reference of the specific uploaded document'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_employer_inspection_task', function (Blueprint $table) {
            //
        });
    }
}
