<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVerificationDatePayrollVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_verifications', function(Blueprint $table)
        {
            $table->date('verification_date')->nullable();
        });
        DB::statement("comment on column payroll_verifications.verification_date is 'Date beneficiary was verified'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
