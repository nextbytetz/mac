<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPayrollRunVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->integer('payroll_run_id')->nullable();
            $table->integer('bank_id')->nullable()->change();
            $table->smallInteger('isbatch')->default(1);
        });

        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->unique('payroll_run_id');
            $table->foreign('payroll_run_id')->references('id')->on('payroll_runs')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
