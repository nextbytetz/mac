<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsOnProgressiveStageOnInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column inspections.progressive_stage is 'show the mandatory stages which the inspection has already gone. 1 - Inspection Created, 2 - Inspection Approved/On Progress'");
        DB::statement("comment on column employer_inspection_task.progressive_stage is 'show the mandatory stages which a employer inspection task has already gone. 1 - Task Created, 2 - Task Assigned to Staff, 3 - Task on Progress'");
        Schema::table('employer_inspection_task', function(Blueprint $table)
        {
            $table->smallInteger('needreminder')->default(0);
        });
        DB::statement("comment on column employer_inspection_task.needreminder is 'specify whether this employer inspection task needs reminder or not, 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
