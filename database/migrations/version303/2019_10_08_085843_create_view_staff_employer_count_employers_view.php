<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewStaffEmployerCountEmployersView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("DROP VIEW IF EXISTS  staff_employers_count");
        DB::statement("CREATE VIEW main.staff_employers_count AS
select user_id, isbonus, isactive, count(*) as no_of_employers from staff_employer
group by user_id, isbonus, isactive
order by count(*) asc;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
