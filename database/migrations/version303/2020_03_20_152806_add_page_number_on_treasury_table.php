<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageNumberOnTreasuryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::table('main.treasury_employees_contribution', function (Blueprint $table) {
        $table->integer('page_number')->nullable();
    }); 

     Schema::table('main.employee_employer', function (Blueprint $table) {
        $table->integer('page_number')->nullable();
    }); 

 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('main.treasury_employees_contribution', function (Blueprint $table) {
        $table->dropColumn('page_number');

    }); 
       Schema::table('main.employee_employer', function (Blueprint $table) {
        $table->dropColumn('page_number');
    }); 

   }
}
