<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatatypeForPayprocesseddateOnEligibleBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            $table->dateTime("payprocesseddate")->nullable()->change();
        });
        $sql = <<<SQL
update notification_eligible_benefits set payprocesseddate = d.forward_date from (select c.id, coalesce(a.forward_date, b.forward_date) forward_date from (select * from notification_workflows where wf_module_id in (17, 18)) c left join (select resource_id, resource_type, forward_date, receive_date from wf_tracks where id in (select max(wf1.id) from wf_tracks wf1 join wf_definitions wfd1 on wf1.wf_definition_id = wfd1.id where wf_module_id in (17, 18) and level = 11 and status = 1 group by resource_id)) a on a.resource_id = c.id left join (select resource_id, resource_type, forward_date, receive_date from wf_tracks where id in (select max(wf1.id) from wf_tracks wf1 join wf_definitions wfd1 on wf1.wf_definition_id = wfd1.id where wf_module_id in (17, 18) and level = 10 and status = 1 group by resource_id)) b on c.id = b.resource_id) d where d.id = notification_eligible_benefits.notification_workflow_id;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
