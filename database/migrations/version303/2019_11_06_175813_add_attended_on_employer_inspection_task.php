<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttendedOnEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            $table->smallInteger("attended")->default(0);
        });
        DB::statement("comment on column employer_inspection_task.attended is 'show whether an officer has attended the employer inspection task or not. 1 - Yes, 0 - No.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            //
        });
    }
}
