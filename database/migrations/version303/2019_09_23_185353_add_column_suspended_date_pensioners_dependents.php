<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSuspendedDatePensionersDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->date('suspended_date')->nullable();
        });

        Schema::table('dependents', function(Blueprint $table)
        {
            $table->date('suspended_date')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
