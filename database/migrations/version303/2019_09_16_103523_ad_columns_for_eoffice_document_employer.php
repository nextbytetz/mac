<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdColumnsForEofficeDocumentEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->integer('eoffice_document_id')->nullable();
            $table->integer('eoffice_external_id')->nullable();
            $table->date('doc_date')->nullable();
            $table->date('doc_receive_date')->nullable();
            $table->date('doc_create_date')->nullable();
            $table->integer('folio')->nullable();
        });
        DB::statement("comment on column document_employer.doc_date is 'date when document was prepared by the member'");
        DB::statement("comment on column document_employer.doc_receive_date is 'date when the document was received at WCF office'");
        DB::statement("comment on column document_employer.doc_create_date is 'date when document was created in e-office'");
        DB::statement("comment on column document_employer.eoffice_document_id is 'E-office document id'");
        DB::statement("comment on column document_employer.eoffice_external_id is 'Primary key of table relates to this document i.e. letter e.g. business closure id'");

//


        Schema::create('eoffice_employer_closures', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('eoffice_document_id');
            $table->integer('eoffice_closure_id');
            $table->date('doc_date')->nullable();
            $table->date('doc_receive_date')->nullable();
            $table->date('doc_create_date')->nullable();
            $table->integer('folio')->nullable();
            $table->string('subject')->nullable();
            $table->string('from')->nullable();
            $table->integer('employer_id')->nullable();
            $table->integer('document_id')->nullable();
            $table->smallInteger('ispending')->default(1);
            $table->smallInteger('issynced')->default(0);
            $table->smallInteger('ismainfile')->default(0);
            $table->timestamps();
            $table->softDeletes();

        });

        DB::statement("comment on table eoffice_employer_closures is 'Table to keep track of business closures letters received on e-office which need to closed on MAC system'");
        DB::statement("comment on column eoffice_employer_closures.doc_receive_date is 'date when the document was received at WCF office'");
        DB::statement("comment on column eoffice_employer_closures.doc_create_date is 'date when document was created in e-office'");
        DB::statement("comment on column eoffice_employer_closures.eoffice_document_id is 'E-office document id'");
        DB::statement("comment on column eoffice_employer_closures.eoffice_closure_id is 'Primary key of table relates to this document i.e. filenumber on e-office'");
        DB::statement("comment on column eoffice_employer_closures.from is 'Entity that submitted this document to WCF i.e.  employer'");
        DB::statement("comment on column eoffice_employer_closures.ispending is 'FLag to specify if already received on MAC i.e. 1 => not yet , 0 => received for further process'");
        DB::statement("comment on column eoffice_employer_closures.issynced is 'FLag to specify if doc synced to document_employer table i.e. 0 => not yet , 1 => already synced'");
        DB::statement("comment on column eoffice_employer_closures.ismainfile is 'FLag to specify if is main closure file from e-office i.e. 0 => not  , 1 => yes '");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
