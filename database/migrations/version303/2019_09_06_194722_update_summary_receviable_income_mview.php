<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSummaryReceviableIncomeMview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  summary_contrib_rcvable_income_mview");
        DB::statement("CREATE MATERIALIZED VIEW main.summary_contrib_rcvable_income_mview AS

   SELECT f.id                                                            AS fin_year_id,
         concat_ws('/' :: text, f.start, f.\"end\")                        AS fiscal_year,
         (SELECT sum(employer_contributions.contrib_amount) AS sum
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND
                 ((employer_contributions.receipt_created_at) :: date >= f.start_date) AND
                 ((employer_contributions.receipt_created_at) :: date <= f.end_date) AND
                 (employer_contributions.contrib_month >= f.start_date) AND employer_contributions.islegacy = 0 AND
                 (employer_contributions.contrib_month <= f.end_date)))  AS receipt_contrib,
         (SELECT count(DISTINCT employer_contributions.employer_id) AS count
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND
                 ((employer_contributions.receipt_created_at) :: date >= f.start_date) AND
                 ((employer_contributions.receipt_created_at) :: date <= f.end_date) AND
                 (employer_contributions.contrib_month >= f.start_date) AND  employer_contributions.islegacy = 0 AND
                 (employer_contributions.contrib_month <= f.end_date)))  AS receipt_contrib_employers,
         (SELECT sum(employer_contributions.contrib_amount) AS sum
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND
                 ((employer_contributions.receipt_created_at) :: date >= f.start_date) AND
                 ((employer_contributions.receipt_created_at) :: date <= f.end_date) AND  employer_contributions.islegacy = 0 AND
                 (employer_contributions.contrib_month < f.start_date))) AS receipt_prev_contrib,
         (SELECT count(DISTINCT employer_contributions.employer_id) AS count
          FROM main.employer_contributions
          WHERE ((employer_contributions.rct_date >= f.start_date) AND
                 (employer_contributions.rct_date <= f.end_date) AND  employer_contributions.islegacy = 0 AND
                 (employer_contributions.contrib_month < f.start_date))) AS receipt_prev_contrib_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM ((main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
              JOIN (SELECT b1.employer_id
                    FROM main.bookings_view b1
                    WHERE ((b1.ispaid = 1) AND (b1.rcv_date >= f.start_date) AND (b1.rcv_date <= f.end_date) AND
                           (b1.receipt_date <= f.end_date))
                    GROUP BY b1.employer_id) b2 ON ((b2.employer_id = b.employer_id)))
          WHERE ((e.employer_status <> 3) AND (b.employer_registered_date < f.start_date) AND
                 (COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) > f.end_date) AND
                 (b.rcv_date >= f.start_date) AND
                 (b.rcv_date <= f.end_date)))                            AS receivable_contributors,
         (SELECT count(DISTINCT b.employer_id) AS count
          FROM ((main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
              JOIN (SELECT b1.employer_id
                    FROM main.bookings_view b1
                    WHERE ((b1.ispaid = 1) AND (b1.rcv_date >= f.start_date) AND (b1.rcv_date <= f.end_date) AND
                           (b1.receipt_date <= f.end_date))
                    GROUP BY b1.employer_id) b2 ON ((b2.employer_id = b.employer_id)))
          WHERE ((e.employer_status <> 3) AND (b.employer_registered_date < f.start_date) AND
                 (COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) > f.end_date) AND
                 (b.rcv_date >= f.start_date) AND
                 (b.rcv_date <= f.end_date)))                            AS receivable_contributors_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM ((main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
              LEFT JOIN (SELECT b1.employer_id
                         FROM main.bookings_view b1
                         WHERE ((b1.ispaid = 1) AND (b1.rcv_date >= f.start_date) AND (b1.rcv_date <= f.end_date) AND
                                (b1.receipt_date <= f.end_date))
                         GROUP BY b1.employer_id) b2 ON ((b2.employer_id = b.employer_id)))
          WHERE ((e.employer_status <> 3) AND (b.employer_registered_date < f.start_date) AND
                 (COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) > f.end_date) AND
                 (b.rcv_date >= f.start_date) AND (b.rcv_date <= f.end_date) AND
                 (b2.employer_id IS NULL)))                              AS receivable_non_contributors,
         (SELECT count(DISTINCT b.employer_id) AS count
          FROM ((main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
              LEFT JOIN (SELECT b1.employer_id
                         FROM main.bookings_view b1
                         WHERE ((b1.ispaid = 1) AND (b1.rcv_date >= f.start_date) AND (b1.rcv_date <= f.end_date) AND
                                (b1.receipt_date <= f.end_date))
                         GROUP BY b1.employer_id) b2 ON ((b2.employer_id = b.employer_id)))
          WHERE ((e.employer_status <> 3) AND (b.employer_registered_date < f.start_date) AND
                 (COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) > f.end_date) AND
                 (b.rcv_date >= f.start_date) AND (b.rcv_date <= f.end_date) AND
                 (b2.employer_id IS NULL)))                              AS receivable_non_contributors_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM (main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
          WHERE ((e.employer_status <> 3) AND
                 ((b.employer_registered_date >= f.start_date) AND (b.employer_registered_date <= f.end_date)) AND
                 (COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) > f.end_date) AND
                 (b.rcv_date > '2015-07-01' :: date) AND
                 (b.rcv_date <= f.end_date)))                            AS receivable_new_entrants,
         (SELECT count(DISTINCT b.employer_id) AS count
          FROM (main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
          WHERE ((e.employer_status <> 3) AND
                 ((b.employer_registered_date >= f.start_date) AND (b.employer_registered_date <= f.end_date)) AND
                 (COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) > f.end_date) AND
                 (b.rcv_date > '2015-07-01' :: date) AND
                 (b.rcv_date <= f.end_date)))                            AS receivable_new_entrants_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM ((main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
              JOIN main.employer_closures c2 ON ((e.id = c2.employer_id)))
          WHERE ((e.employer_status = 3) AND (b.rcv_date >= f.start_date) AND
                 (COALESCE(b.first_payment_date, (now()) :: date) > f.end_date) AND (b.rcv_date <= f.end_date) AND
                 (c2.close_date >= f.start_date) AND
                 (c2.close_date <= f.end_date)))                         AS receivable_closed_business,
         (SELECT count(DISTINCT b.employer_id) AS count
          FROM ((main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
              JOIN main.employer_closures c2 ON ((e.id = c2.employer_id)))
          WHERE ((e.employer_status = 3) AND (b.rcv_date >= f.start_date) AND
                 (COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) > f.end_date) AND
                 (b.rcv_date <= f.end_date) AND (c2.close_date >= f.start_date) AND
                 (c2.close_date <= f.end_date)))                         AS receivable_closed_business_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM (main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
          WHERE ((COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) >= f.start_date) AND
                 ((b.rcv_date >= '2015-07-01' :: date) AND (b.rcv_date < f.start_date)) AND
                 (e.employer_status <> 3)))                              AS receivable_open_balance,
         (SELECT count(DISTINCT b.employer_id) AS count
          FROM (main.bookings_view b
              JOIN main.employers e ON ((e.id = b.employer_id)))
          WHERE ((COALESCE(b.receipt_date, ((now()) :: date) :: timestamp without time zone) >= f.start_date) AND
                 ((b.rcv_date >= '2015-07-01' :: date) AND (b.rcv_date < f.start_date)) AND
                 (e.employer_status <> 3)))                              AS receivable_open_balance_employers
  FROM main.fin_years f;

");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
