<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOldEmployerIdStaffEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->integer('old_employer_id')->nullable();
        });

        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->foreign('old_employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column staff_employer.old_employer_id is 'Old employer before replacement'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
