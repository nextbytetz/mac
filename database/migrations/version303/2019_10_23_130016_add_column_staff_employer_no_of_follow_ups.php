<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerNoOfFollowUps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->integer('no_of_follow_ups')->nullable();
        });
        DB::statement("comment on column staff_employer.no_of_follow_ups is 'No of follow ups for each employer on this allocation'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
