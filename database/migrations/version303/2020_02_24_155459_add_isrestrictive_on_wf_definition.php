<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsrestrictiveOnWfDefinition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->smallInteger("isrestrictive")->default(0);
        });
        DB::statement("comment on column wf_definitions.isrestrictive is 'restrict the access of the level only to the assigned user, restrict user to self auto-assign, restrict repeated user in previous workflow from being selected'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
