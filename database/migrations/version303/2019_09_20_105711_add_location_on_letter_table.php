<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationOnLetterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letters', function(Blueprint $table)
        {
            $table->string("location", 50)->nullable();
            $table->smallInteger("isinitiated")->default(0);
        });
        DB::statement("comment on column letters.location is 'district and region of the recipient'");
        DB::statement("comment on column letters.isinitiated is 'check whether a letter has been initiated for approval or not'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
