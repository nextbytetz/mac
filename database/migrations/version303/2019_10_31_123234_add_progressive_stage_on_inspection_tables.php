<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProgressiveStageOnInspectionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function(Blueprint $table)
        {
            $table->smallInteger('progressive_stage')->default(1);
        });
        Schema::table('employer_inspection_task', function(Blueprint $table)
        {
            $table->smallInteger('progressive_stage')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
