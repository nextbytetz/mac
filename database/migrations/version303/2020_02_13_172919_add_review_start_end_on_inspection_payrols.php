<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewStartEndOnInspectionPayrols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_task_payrolls', function (Blueprint $table) {
            $table->date("review_start_date")->nullable();
            $table->date("review_end_date")->nullable();
        });
        Schema::table('checkers', function (Blueprint $table) {
            $table->smallInteger("notify")->default(0);
        });
        DB::statement("comment on column checkers.notify is 'indicate that checkers should blink for users to pay attention to the pendings'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
