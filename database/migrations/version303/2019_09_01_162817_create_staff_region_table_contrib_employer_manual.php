<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRegionTableContribEmployerManual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('staff_region', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('region_id');
            $table->integer('unit_id');
            $table->smallInteger('isactive')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table staff_region is 'Pivot table for allocating staff to regions schedules '");

        Schema::table('staff_region', function(Blueprint $table)
        {
            $table->foreign('region_id')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        Schema::create('employer_contrib_category_manual', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_id');
            $table->integer('contributing_category_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table employer_contrib_category_manual is 'Categorized contributing employers from manual'");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
