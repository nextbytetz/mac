<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntoStaffEmployerFollowUpsAssignedReviewer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_follow_ups', function(Blueprint $table)
        {
            $table->integer('assigned_reviewer')->nullable();
            $table->text('reverse_reason')->nullable();
            $table->smallInteger('needs_correction')->default(0);
        });
        DB::statement("comment on column staff_employer_follow_ups.needs_correction is 'Flag to specify if follow up needs correction i.e. 1 => needs correction, 2 => corrected, 0=> no need for correction'");
        DB::statement("comment on column staff_employer_follow_ups.reverse_reason is 'Reverse reason to the owner of follow up for correction identified.'");
            DB::statement("comment on column staff_employer_follow_ups.assigned_reviewer is 'Assigned staff to review this follow up.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
