<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSysContribAmountOnInspectionAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('inspection_assessments', function (Blueprint $table) {
            $table->decimal("sys_contrib_amount", 14)->nullable();
        });
        DB::statement("comment on column  inspection_assessments.contrib_amount is 'amount contributed to the fund as per the information from the employer'");
        DB::statement("comment on column  inspection_assessments.sys_contrib_amount is 'amount contributed to the fund as recorded by finance or compliance in MAC'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_assessments', function (Blueprint $table) {
            //
        });
    }
}
