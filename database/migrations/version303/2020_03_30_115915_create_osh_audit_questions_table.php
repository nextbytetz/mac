<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOshAuditQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('osh_audit_questions', function (Blueprint $table) {
         $table->increments('id');
         $table->bigInteger("code_value_id");
         $table->text('question');
         $table->string('data_type');
         $table->timestamps();
         $table->boolean('is_active')->default(1)->comment('Whether this question is being used for osh audit 0 => Not used, 1 => used');
         /*Foreign keys*/
         //$table->foreign('code_value_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('CASCADE');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('osh_audit_questions');
    }
}
