<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOshAuditPeriod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osh_audit_period', function (Blueprint $table) {
            $table->integer("fatal")->nullable();
            $table->integer("lost_time_injury")->nullable();
            $table->integer("non_conveyance")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osh_audit_period', function (Blueprint $table) {
            $table->dropColumn('fatal');
            $table->dropColumn('lost_time_injury');
            $table->dropColumn('non_conveyance');
        });
    }
}
