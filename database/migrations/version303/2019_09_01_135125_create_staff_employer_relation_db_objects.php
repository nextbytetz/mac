<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffEmployerRelationDbObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('staff_employer', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('employer_id');
            $table->integer('unit_id');
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->smallInteger('isactive')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table staff_employer is 'Pivot table for officer and employer for customer_relationship'");
        DB::statement("comment on column staff_employer.start_date is 'Date allocated this employer to do follow up and build customer relation'");
        DB::statement("comment on column staff_employer.end_date is 'End date of staff to deal with this employer'");

        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
                  });




        Schema::create('compliance_employer_follow_ups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('staff_employer_id');
            $table->text('description');
            $table->text('feedback')->nullable();
            $table->integer('follow_up_type_cv_id');
            $table->string('contact', 45)->nullable();
            $table->string('contact_person', 255)->nullable();
            $table->date('date_of_follow_up')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table compliance_employer_follow_ups is 'Table to keep track of follow ups of employer for contribution and compliances issues '");

        Schema::table('compliance_employer_follow_ups', function(Blueprint $table)
        {
            $table->foreign('follow_up_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staff_employer_id')->references('id')->on('staff_employer')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
