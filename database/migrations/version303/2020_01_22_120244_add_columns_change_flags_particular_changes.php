<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsChangeFlagsParticularChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_particular_changes', function(Blueprint $table)
        {

            $table->smallInteger('is_tin_changed')->default(0);
            $table->smallInteger('is_doc_changed')->default(0);
        });

        DB::statement("comment on column employer_particular_changes.is_tin_changed is 'Flag to specify if tin no was changed on this request i.e 1 => changed, 0 => not changed' ");
        DB::statement("comment on column employer_particular_changes.is_doc_changed is 'Flag to specify if doc was changed on this request i.e 1 => changed, 0 => not changed' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
