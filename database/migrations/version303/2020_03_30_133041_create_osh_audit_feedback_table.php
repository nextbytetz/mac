<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOshAuditFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('osh_audit_feedback', function (Blueprint $table) {
           $table->increments('id');
           $table->bigInteger("osh_audit_period_employer_id");
           $table->bigInteger("osh_audit_question_id");
           $table->text("comment")->nullable();
           $table->string('feedback', 20);
           $table->timestamps();
           $table->dateTime('deleted_at');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('osh_audit_feedback');
    }
}
