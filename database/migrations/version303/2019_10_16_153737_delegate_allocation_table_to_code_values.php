<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelegateAllocationTableToCodeValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('allocation_user', function(Blueprint $table)
        {
            $table->dropColumn("allocation_id");
            $table->bigInteger("allocation_cv_id");
            $table->foreign('allocation_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::drop("allocations");
        DB::statement("comment on table allocation_user is 'users assigned in different allocations as defined from the default configurations in code_values table'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
