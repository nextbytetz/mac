<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInspectionAssessmentTemplateSecond extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
DROP MATERIALIZED VIEW IF EXISTS inspection_assessment_template;
create materialized view inspection_assessment_template as select uu.*, (coalesce(uu.difference_under, 0) + coalesce(uu.interest_on_paid, 0) + coalesce(uu.interest_on_unpaid, 0)) accumulative_to_be_paid, to_char(uu.contrib_month, 'Mon-YY') contrib_month_formatted from (select ss.*, receivable_interest(ss.underpaid::decimal, ss.months_diff_unpaid::smallint) interest_on_unpaid, receivable_interest(ss.contrib_amount::decimal, ss.months_delayed_paid::smallint) interest_on_paid from (select tt.*,  case when tt.difference_under < 0 then NULL else tt.difference_under end underpaid, case when tt.difference_over < 0 then NULL else tt.difference_over end overpaid, case when tt.months_diff_paid <= 1 then 0 else tt.months_diff_paid - 1 end months_delayed_paid, case when tt.months_diff_unpaid <= 1 then 0 else tt.months_diff_unpaid - 1 end months_delayed_unpaid  from (select a.id, a.employer_inspection_task_id, b.employer_id, a.contrib_month, a.member_count, a.member_count_verified, abs(coalesce(a.member_count_verified, 0) - a.member_count) member_count_difference, a.salary, a.fixed_allowance, a.non_fixed_allowance, a.contrib_amount/a.percent payroll_declared, nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0) + coalesce(a.non_fixed_allowance, 0), 0) payroll_verified, nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0) gross_verified, contrib_amount, a.percent * nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0) contrib_amount_verified, (a.percent * nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0)) - a.contrib_amount difference_under, a.contrib_amount - (a.percent * nullif(coalesce(a.salary, 0) + coalesce(a.fixed_allowance, 0), 0)) difference_over, a.last_pay_date payment_date, case when (a.contrib_month::date < '2017-09-28'::date) then months_diff('2017-09-28'::date, a.first_pay_date) else months_diff(a.contrib_month::date, a.first_pay_date) end  months_diff_paid, case when (a.contrib_month::date < '2017-09-28'::date) then months_diff('2017-09-28'::date, b.last_inspection_date) else months_diff(a.contrib_month, b.last_inspection_date) end months_diff_unpaid, a.first_pay_date, a.last_pay_date, a.error_report, a.file_error  from inspection_assessments a join employer_inspection_task b on a.employer_inspection_task_id = b.id) tt) ss) uu;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
