<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStaffEmployerClosures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('staff_employer_follow_ups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->text('remark');
            $table->integer('feedback_cv_id')->nullable();
            $table->integer('follow_up_type_cv_id');
            $table->string('contact', 100)->nullable();
            $table->string('contact_person', 255)->nullable();
                      $table->integer('user_id');
            $table->integer('staff_employer_id');
            $table->date('date_of_follow_up')->nullable();
            $table->date('date_of_reminder')->nullable();
            $table->integer('reviewed_by')->nullable();
            $table->integer('isreviewed')->default(0);
            $table->integer('reviewed_date')->nullable();
            $table->timestamps();
        });


        DB::statement("comment on table staff_employer_follow_ups is 'Table to keep track of follow ups for a particular employer by relation officer'");
        DB::statement("comment on column staff_employer_follow_ups.user_id is 'User performed this follow up'");
        DB::statement("comment on column staff_employer_follow_ups.reviewed_by is 'User reviewed this followup i.e. applicable to bonus (dormant employers)'");
        DB::statement("comment on column staff_employer_follow_ups.isreviewed is 'Flag to specify if follow up has been reviewed i.e. 1 => reviewed, 0 => not reviewed or don need review'");


        Schema::table('staff_employer_follow_ups', function(Blueprint $table)
        {
            $table->foreign('follow_up_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('feedback_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('reviewed_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staff_employer_id')->references('id')->on('staff_employer')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
