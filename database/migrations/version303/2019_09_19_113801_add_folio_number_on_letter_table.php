<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFolioNumberOnLetterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letters', function(Blueprint $table)
        {
            $table->string("reference", 50)->nullable();
            $table->string("salutation", 40)->nullable();
            $table->string("box_no", 30)->nullable();
            $table->string("approved")->default(0);
        });
        DB::statement("comment on column letters.reference is 'reference of the letter to be sent'");
        DB::statement("comment on column letters.approved is 'whether the letter is approved or not 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
