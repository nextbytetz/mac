<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnStaffEmployerFollowups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_follow_ups', function(Blueprint $table)
        {
            $table->dropColumn('reviewed_date');

        });

        Schema::table('staff_employer_follow_ups', function(Blueprint $table)
        {
            $table->date('reviewed_date')->nullable();
            $table->text('review')->nullable();
            $table->text('remark')->nullable()->change();
            $table->smallInteger('isreminded')->default(0);
        });

        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->smallInteger('isduplicate')->default(0);

        });
        DB::statement("comment on column staff_employer.isduplicate is 'Flag to specify employer is duplicate i.e. 1 => duplicate, 0 => not duplicate'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
