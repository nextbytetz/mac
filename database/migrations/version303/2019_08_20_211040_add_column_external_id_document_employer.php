<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnExternalIdDocumentEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->integer('external_id')->nullable();

        });
//

        DB::statement("comment on column document_employer.external_id is 'Primary key of the table relate to this document of employer'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
