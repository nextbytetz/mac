<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStaffForLargeContributors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('staff_for_large_contributors', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->timestamps();
        });

        DB::statement("comment on table staff_for_large_contributors is 'Table to keep track of staff for large contributors'");

        Schema::table('staff_for_large_contributors', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        Schema::table('staff_employer_allocations', function(Blueprint $table)
        {
                      $table->smallInteger('run_status')->default(0);
                 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
