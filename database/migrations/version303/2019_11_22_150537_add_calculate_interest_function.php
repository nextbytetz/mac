<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalculateInterestFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create or replace function main.receivable_interest(amount decimal, months_delayed smallint) returns decimal
	language plpgsql
as $$
begin
    return (amount * power(1 + (10::decimal/100::decimal), months_delayed)) - amount;
end
$$
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
