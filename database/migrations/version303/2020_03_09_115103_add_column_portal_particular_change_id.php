<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPortalParticularChangeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_particular_changes', function (Blueprint $table) {
            $table->integer('portal_particular_change_id')->nullable();

        });

        DB::statement("comment on column employer_particular_changes.portal_particular_change_id is 'Primary key of change of particulars table from the portal'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
