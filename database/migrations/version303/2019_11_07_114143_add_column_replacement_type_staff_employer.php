<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReplacementTypeStaffEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer', function(Blueprint $table)
        {
            $table->smallInteger('replacement_type')->nullable();
        });
        DB::statement("comment on column staff_employer.replacement_type is 'Replacement type to specify reason for this allocation replacement i.e. 1=> small, 2=> duplicate, 3=> closed, 4=>dormant'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
