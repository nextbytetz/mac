<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStagingInformationOnLetters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_stages', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("letter_id");
            $table->bigInteger("staging_cv_id");
            $table->json("comments");
            $table->smallInteger("allow_repeat")->default(0);
            $table->smallInteger("attended")->default(0);
            $table->smallInteger("require_attend")->default(1);
            $table->smallInteger("isdeleted")->default(0);
            $table->timestamps();
            $table->foreign('letter_id')->references('id')->on('letters')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('letters', function (Blueprint $table) {
            $table->bigInteger("staging_cv_id")->nullable();
            $table->bigInteger("letter_stage_id")->nullable();
            $table->bigInteger("allocated")->nullable();
            $table->smallInteger("allocate_status")->default(1);
            $table->foreign('letter_stage_id')->references('id')->on('letter_stages')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('allocated')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letters', function (Blueprint $table) {
            //
        });
    }
}
