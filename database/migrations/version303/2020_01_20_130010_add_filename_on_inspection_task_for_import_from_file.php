<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilenameOnInspectionTaskForImportFromFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_tasks', function (Blueprint $table) {
            $table->string("filename", 20)->nullable();
        });
        DB::statement("comment on column inspection_tasks.filename is 'store the file name of the uploaded file, in case task is created from the imported file from excel'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_tasks', function (Blueprint $table) {
            //
        });
    }
}
