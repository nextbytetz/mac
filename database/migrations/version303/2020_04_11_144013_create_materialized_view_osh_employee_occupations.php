<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewOshEmployeeOccupations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    DB::statement("DROP MATERIALIZED VIEW IF EXISTS  osh_employees_occupations");
        DB::statement("CREATE MATERIALIZED VIEW  osh_employees_occupations  AS     select occupation_id,case when(name is null) then 'Unknown' else name end as occupation_name,case when(occupation_id is null) then count(ia.id) else count(occupation_id) end as idadi from main.incident_assessments ia left join main.occupations occ on ia.occupation_id=occ.id group by occupation_id,name order by idadi desc limit 5");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
