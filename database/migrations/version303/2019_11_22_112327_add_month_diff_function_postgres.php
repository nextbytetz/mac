<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonthDiffFunctionPostgres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create or replace function main.months_diff(from_date date, to_date date) returns smallint
	language plpgsql
as $$
declare difference smallint;
begin
    SELECT ((DATE_PART('year', to_date::date) - DATE_PART('year', from_date::date)) * 12 + (DATE_PART('month', to_date::date) - DATE_PART('month', from_date::date)))::smallint into difference;
    return difference;
end
$$
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
