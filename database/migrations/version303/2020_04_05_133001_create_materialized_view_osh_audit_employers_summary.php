<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewOshAuditEmployersSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE MATERIALIZED VIEW  osh_audit_checklist_employers_summary  AS     select o.fin_year,o.employer,e.reg_no as RegNo,d.name as hq_district,r.name as hq_region,sum(o.fatal) as fatals_premises,sum(o.accidents) as accidents_premises,sum(o.disease) as disease,sum(o.claims_total) as total_claims,sum(o.total_lost_days) as total_days_lost from (
select employer_id,employer,fin_year,sum(death) as fatal,sum(accidents) as accidents,sum(diseases) as disease,sum(total_claims) as claims_total,sum(t_lost_days) as total_lost_days from (
SELECT
s.employer_id,s.employer,s.fin_year,s.death,s.accidents,s.diseases,s.t_lost_days,s.total_claims
FROM
(
SELECT  employer_id,employer,fin_year,
CASE    WHEN (conveyance_death=224) THEN count(conveyance_death) ELSE 0 END AS death,
CASE    WHEN (conveyance_accident=1) THEN count(conveyance_accident) ELSE 0 END AS accidents,
count(diseases_name) as diseases,
sum(lost_days) as t_lost_days,
count(*) as total_claims
FROM main.osh_audit_checklist_employers group by employer_id,employer,fin_year,conveyance_death,conveyance_accident,diseases_name
) s) q group by employer_id,employer,fin_year) o
join main.employers e on o.employer_id=e.id join main.districts d on e.district_id=d.id join main.regions r on d.region_id=r.id
group by o.employer,e.reg_no,d.name,r.name,o.fin_year");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
