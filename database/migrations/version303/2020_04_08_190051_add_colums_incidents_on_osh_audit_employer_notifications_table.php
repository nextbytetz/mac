<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsIncidentsOnOshAuditEmployerNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osh_audit_employer_notifications', function (Blueprint $table) {
           $table->boolean('is_fatal')->default(0);
           $table->boolean('is_lost_days')->default(0);
           $table->boolean('not_conveyance_accident')->default(0);
       });

        DB::statement("comment on column osh_audit_employer_notifications.is_fatal is '1 if the incident is death o if is not'");
        DB::statement("comment on column osh_audit_employer_notifications.is_lost_days is '1 if the incident involves Exempted from Duties, Hospitalization,light Duties'");
        DB::statement("comment on column osh_audit_employer_notifications.not_conveyance_accident is '1 if the incident is not conveyance accident'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osh_audit_employer_notifications', function (Blueprint $table) {
          $table->dropColumn('is_fatal');
          $table->dropColumn('is_lost_days');
          $table->dropColumn('not_conveyance_accident');

      });
    }
}
