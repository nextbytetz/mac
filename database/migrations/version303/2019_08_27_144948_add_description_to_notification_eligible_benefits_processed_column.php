<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToNotificationEligibleBenefitsProcessedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column notification_eligible_benefits.processed is 'show the status whether the benefit has passed the assessment workflow approval. 1 - Yes, 0 - No, 2 - Declined, 3 - Cancelled by User'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
