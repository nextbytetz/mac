<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveForeignKeyIncidentClosureIdOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropForeign('notification_reports_incident_closure_id_foreign');
            $table->dropForeign('notification_reports_incident_td_id_foreign');
            $table->dropForeign("notification_reports_incident_mae_id_foreign");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
