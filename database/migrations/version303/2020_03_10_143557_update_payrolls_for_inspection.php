<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePayrollsForInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
/*1. when first_count has some value and second_count is null, all have some values of payroll_id (same, varying, unique for each)
2. when first_count - second_count is 1, everything is okay
3. when first_count is equal to second_count, all have payroll_id which is null
4. when first_count - second_count is greater than 1, all have some values of payroll_id, (same, varying, unique for each)
  Resolutions:
  Case 3 -- update one entry with 1
  Case 1,4 -- manipulate to ensure each entry has unique payroll_id
  */
  -- Clean payroll id for inspection_task_payrolls, avoid duplicates ...
DO $$
    DECLARE r record;
    declare t record;
BEGIN
    FOR r IN select a.employer_inspection_task_id, a.payroll_id, a.count first_count, b.count second_count from (select employer_inspection_task_id, count(id) count, min(payroll_id)  payroll_id from inspection_task_payrolls group by employer_inspection_task_id having count(id) > 1) a left join (select employer_inspection_task_id, count(id) count from inspection_task_payrolls where payroll_id is null group by employer_inspection_task_id having count(id) > 1) b on a.employer_inspection_task_id = b.employer_inspection_task_id
    LOOP
        if (r.first_count = r.second_count) then
            -- update one entry with payroll_id 1
            update inspection_task_payrolls set payroll_id = 1 where employer_inspection_task_id = r.employer_inspection_task_id and id = (select min(id) from inspection_task_payrolls where employer_inspection_task_id = r.employer_inspection_task_id);
        end if;

        if (r.first_count - coalesce(r.second_count, 0) > 1) then
            -- manipulate to ensure each entry has unique payroll_id
            for t in select employer_inspection_task_id, payroll_id, array_agg(id order by id) ids from inspection_task_payrolls where employer_inspection_task_id = r.employer_inspection_task_id group by employer_inspection_task_id, payroll_id having count(payroll_id) > 1
            loop
                update inspection_task_payrolls set payroll_id = t.payroll_id where id = t.ids[1];
                update inspection_task_payrolls set payroll_id = NULL where id in (select unnest(t.ids[2:]));
            end loop;
        end if;

    END LOOP;
    -- Update for default Payroll for employer inspection task with only one payroll
    update inspection_task_payrolls set payroll_id = 1 from (select employer_inspection_task_id , count(id) from inspection_task_payrolls group by employer_inspection_task_id having count(id) = 1) a where a.employer_inspection_task_id = inspection_task_payrolls.employer_inspection_task_id;
END$$;

update inspection_assessments set sys_contrib_amount = tt.contrib_amount, interest_amount = tt.interest_amount from (select a.id, a.employer_inspection_task_id, d.contrib_amount interest_amount, coalesce(e.contrib_amount, f.contrib_amount) contrib_amount from inspection_assessments a join inspection_task_payrolls b on a.inspection_task_payroll_id = b.id join employer_inspection_task c on b.employer_inspection_task_id = c.id left join (select a.employer_id, b.contrib_month, a.payroll_id, sum(coalesce(b.amount)) contrib_amount, min(a.rct_date) first_pay_date, max(a.rct_date) last_pay_date  from receipts a join receipt_codes b on a.id = b.receipt_id where b.grouppaystatus = 0 and a.iscancelled = 0 and a.isdishonoured = 0 and b.fin_code_id = 1 group by a.employer_id, b.contrib_month, a.payroll_id) d on d.employer_id = c.employer_id and  date_part('month', d.contrib_month) = date_part('month', a.contrib_month) and date_part('year', d.contrib_month) = date_part('year', a.contrib_month) and d.payroll_id = b.payroll_id and d.first_pay_date < c.last_inspection_date left join (select a.employer_id, b.contrib_month, a.payroll_id, sum(coalesce(b.amount)) contrib_amount, min(a.rct_date) first_pay_date, max(a.rct_date) last_pay_date  from receipts a join receipt_codes b on a.id = b.receipt_id where b.grouppaystatus = 0 and a.iscancelled = 0 and a.isdishonoured = 0 and b.fin_code_id = 2 group by a.employer_id, b.contrib_month, a.payroll_id) e on e.employer_id = c.employer_id and date_part('month', e.contrib_month) = date_part('month', a.contrib_month) and date_part('year', e.contrib_month) = date_part('year', a.contrib_month) and e.payroll_id = b.payroll_id and e.first_pay_date < c.last_inspection_date left join (select employer_id, contrib_month, 1 payroll_id, max(amount) contrib_amount, min(rct_date) first_pay_date, max(member_count) member_count, max(rct_date) last_pay_date from legacy_merged_contributions where iscancelled = 0 and isdishonoured = 0 and fin_code_id = 2 group by employer_id, contrib_month, 1) f on f.employer_id = c.employer_id and date_part('month', f.contrib_month) = date_part('month', a.contrib_month) and date_part('year', f.contrib_month) = date_part('year', a.contrib_month) and f.payroll_id = b.payroll_id and f.first_pay_date < c.last_inspection_date) tt where tt.id = inspection_assessments.id;

SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
