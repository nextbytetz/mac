<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStaffEmployerCollectionPerfomances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('staff_employer_collection_performances', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('no_of_employers');
            $table->decimal('percent_online_employers', 5,2);
            $table->decimal('arrears_before', 14,2);
            $table->decimal('arrears_collection', 14,2);
            $table->decimal('receivable_current', 14,2);
            $table->decimal('collection_current', 14,2);
            $table->date('collection_month');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table staff_employer_collection_performances is 'Table to keep track of the collection performances from debt management'");
        DB::statement("comment on column staff_employer_collection_performances.no_of_employers is 'No of employers allocated to staff'");
        DB::statement("comment on column staff_employer_collection_performances.percent_online_employers is 'Percent of online employers verified'");
        DB::statement("comment on column staff_employer_collection_performances.arrears_before is 'Arrears before allocation of employer to staff'");
        DB::statement("comment on column staff_employer_collection_performances.arrears_collection is 'Contribution collected for arrears before allocation of employer to staff'");
        DB::statement("comment on column staff_employer_collection_performances.receivable_current is 'Receivable of employers after allocation'");
        DB::statement("comment on column staff_employer_collection_performances.collection_current is 'Contribution collected for receivables after allocation of employer to staff'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
