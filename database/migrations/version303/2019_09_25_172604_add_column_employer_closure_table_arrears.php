<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployerClosureTableArrears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->decimal('arrears', 16)->nullable();
            $table->smallInteger('missing_months')->nullable();
        });
        DB::statement("comment on column employer_closures.arrears is 'Total amount of receivables at the time of closure'");
        DB::statement("comment on column employer_closures.missing_months is 'No of missing months at the time of closure'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
