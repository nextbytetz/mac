<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewOshAuditChecklistEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement("CREATE MATERIALIZED VIEW  osh_audit_checklist_employers  AS    SELECT a.filename,
    a.employee,
    a.employer,
    a.incident,
    a.incident_date,
    a.reporting_date,
    a.notification_date,
    a.registration_date,
    a.resource_id,
    a.district,
    a.region,
    b.name AS fin_year,
    b.id AS fin_year_id,
    a.incident_occurrence_cv_id as conveyance_death,
    a.accident_type_id as conveyance_accident
   FROM ( SELECT a_1.filename,
            concat_ws(' '::text, b_1.firstname, COALESCE(b_1.middlename, ''::character varying), b_1.lastname) AS employee,
            d.name AS employer,
            c.name AS incident,
            a_1.incident_date,
            a_1.reporting_date,
            a_1.receipt_date AS notification_date,
            a_1.created_at AS registration_date,
            a_1.id AS resource_id,
            o.name AS district,
            p.name AS region,
            e.incident_occurrence_cv_id,
            f.accident_type_id 
           FROM main.notification_reports a_1
             JOIN main.employees b_1 ON a_1.employee_id = b_1.id
             JOIN main.incident_types c ON a_1.incident_type_id = c.id
             JOIN main.employers d ON d.id = a_1.employer_id
             LEFT JOIN main.districts o ON o.id = a_1.district_id
             LEFT JOIN main.regions p ON o.region_id = p.id
             left join main.deaths e on a_1.id=e.notification_report_id
             left join main.accidents f on a_1.id=f.notification_report_id
          WHERE a_1.isprogressive = 0 AND a_1.deleted_at IS null
        UNION ALL
         SELECT a_1.filename,
            concat_ws(' '::text, b_1.firstname, COALESCE(b_1.middlename, ''::character varying), b_1.lastname) AS employee,
            d.name AS employer,
            c.name AS incident,
            a_1.incident_date,
            a_1.reporting_date,
            a_1.receipt_date AS notification_date,
            a_1.created_at AS registration_date,
            a_1.id AS resource_id,
            o.name AS district,
            p.name AS region,
            e.incident_occurrence_cv_id,
            f.accident_type_id 
           FROM main.notification_reports a_1
             JOIN main.employees b_1 ON a_1.employee_id = b_1.id
             JOIN main.incident_types c ON a_1.incident_type_id = c.id
             JOIN main.employers d ON d.id = a_1.employer_id
             LEFT JOIN main.districts o ON o.id = a_1.district_id
             LEFT JOIN main.regions p ON o.region_id = p.id
             LEFT JOIN main.code_values q ON a_1.notification_staging_cv_id = q.id
             LEFT JOIN main.codes r ON q.code_id = r.id
             left join main.deaths e on a_1.id=e.notification_report_id
             left join main.accidents f on a_1.id=f.notification_report_id
          WHERE a_1.isprogressive = 1 AND (r.id = ANY (ARRAY[13::bigint, 23::bigint])) AND a_1.deleted_at IS null) a
     LEFT JOIN main.fin_years b ON a.registration_date::date >= b.start_date AND a.registration_date::date <= b.end_date");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
