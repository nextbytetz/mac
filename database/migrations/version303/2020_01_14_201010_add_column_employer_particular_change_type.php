<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployerParticularChangeType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_particular_change_types', function(Blueprint $table)
        {

            $table->integer('particular_change_type_cv_id');


            /*Foreign keys*/
            $table->foreign('particular_change_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('employer_particular_changes', function(Blueprint $table)
        {

            $table->dropColumn('old_values');
            $table->dropColumn('new_values');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
