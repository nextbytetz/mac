<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllocateStatusOnEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            $table->smallInteger("allocate_status")->default(1);
        });
        DB::statement("comment on column employer_inspection_task.allocate_status is 'show whether the allocation has been done by system or re-allocated by the executive, 1 - done by system, 0 - re-allocated by the executive'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_inspection_task', function (Blueprint $table) {
            //
        });
    }
}
