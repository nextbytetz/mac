<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewOshEmployedEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  osh_audit_employees_contributed");
        DB::statement("CREATE MATERIALIZED VIEW  osh_audit_employees_contributed  AS select a.employer_id, b.name fin_year, max(member_count) contributed_employees from receipt_codes a join fin_years b on a.contrib_month between b.start_date and b.end_date where a.fin_code_id = 2 group by a.employer_id, b.name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
