<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllocationAttributeToInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function(Blueprint $table)
        {
            $table->bigInteger("user_id")->nullable()->change();
            $table->bigInteger("allocated")->nullable();
        });
        Schema::table('employer_inspection_task', function(Blueprint $table)
        {
            $table->bigInteger("user_id")->nullable()->change();
            $table->bigInteger("allocated")->nullable();
        });
        DB::statement("comment on column inspections.allocated is 'staff who has been handled responsibility of working on the inspection'");
        DB::statement("comment on column employer_inspection_task.allocated is 'staff who has been handled responsibility of working on the employer inspection task'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
