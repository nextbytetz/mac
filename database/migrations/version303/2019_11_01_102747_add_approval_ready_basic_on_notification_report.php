<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovalReadyBasicOnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->smallInteger('approval_ready_basic')->default(0);
            $table->date('approval_ready_basic_date')->nullable();
            $table->date('approval_ready_basic_mac_date')->nullable();
        });
        DB::statement("comment on column notification_reports.approval_ready_basic is 'show whether the file is ready for notification approval/validation process with mandatory known documents or not. This is triggered when all the mandatory necessary documents has already been uploaded, 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.approval_ready_basic_date is 'date ready considering receive date on the documents'");
        DB::statement("comment on column notification_reports.approval_ready_basic_mac_date is 'date ready considering create date of document in mac'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
