<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountForInspectionTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->integer("insprno")->default(1);
            $table->integer("inspsno")->default(1);
            $table->integer("inspbno")->default(1);
        });
        DB::statement("comment on column sysdefs.insprno is 'routine inspection sequence number pointer'");
        DB::statement("comment on column sysdefs.inspsno is 'special inspection sequence number pointer'");
        DB::statement("comment on column sysdefs.inspbno is 'blitz inspection sequence number pointer'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
