<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerReturnEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_return_earnings', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger("employer_id")->index();
            $table->integer("payroll_id");
            $table->bigInteger("user_id")->index();
            $table->string("fin_year");
            $table->string("phone")->nullable();
            $table->string("email")->nullable();
            $table->integer("submission_mode");
            $table->integer("submission_status");
            $table->smallInteger("wf_done")->default(0);
            $table->date("wf_done_date")->nullable();
            $table->string("user_type");
            $table->timestamps();
        });
        DB::statement("comment on column employer_return_earnings.submission_status is 'set the submission_status of the request, 0 => Pending for Approval, 1 => Approved, 2 => Rejected'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_return_earnings');
    }
}
