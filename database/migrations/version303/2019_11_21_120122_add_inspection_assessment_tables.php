<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInspectionAssessmentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_assessments', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("employer_inspection_task_id");
            $table->date("contrib_month");
            $table->smallInteger("member_count_verified")->nullable();
            $table->decimal("salary", 14)->nullable();
            $table->decimal("fixed_allowance", 14)->nullable();
            $table->decimal("non_fixed_allowance", 14)->nullable();
            $table->timestamps();
            $table->foreign('employer_inspection_task_id')->references('id')->on('employer_inspection_task')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table inspection_assessments is 'table to store assessment reviews for the inspection from employer'");
        DB::statement("comment on column inspection_assessments.contrib_month is 'contribution month for inspection review'");
        DB::statement("comment on column inspection_assessments.member_count_verified is 'member count verified from employer'");
        DB::statement("comment on column inspection_assessments.salary is 'salary verified from employer'");
        DB::statement("comment on column inspection_assessments.fixed_allowance is 'fixed allowance verified from employer'");
        DB::statement("comment on column inspection_assessments.non_fixed_allowance is 'non fixed allowance verified from employer'");

        Schema::table('employer_inspection_task', function(Blueprint $table)
        {
            $table->smallInteger("assessment_status")->nullable();
        });
        DB::statement("comment on column employer_inspection_task.assessment_status is 'show the outcome of the assessment, 1 - Compliant, 2 - Has Outstanding, 3 - Has Overpayment'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
