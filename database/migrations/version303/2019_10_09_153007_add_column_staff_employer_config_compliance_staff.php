<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerConfigComplianceStaff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_configs', function(Blueprint $table)
        {
            $table->text('compliance_officers')->nullable();

        });
        DB::statement("comment on column staff_employer_configs.compliance_officers is 'Compliance officers for employers allocation'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
