<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollRunVouchersClaimsPayable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->string('paymethod')->nullable();
            $table->string('payreference')->nullable();
            $table->date('payment_date')->nullable();
        });


        Schema::table('claims_payable', function(Blueprint $table)
        {
            $table->integer('payment_resource_id')->nullable();

        });
        DB::statement("comment on column claims_payable.payment_resource_id is 'Primary key of the resource exported to erp for payments'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
