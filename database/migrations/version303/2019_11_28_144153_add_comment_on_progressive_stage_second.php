<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentOnProgressiveStageSecond extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column employer_inspection_task.progressive_stage is 'show the mandatory stages which a employer inspection task has already gone. 1 - Task Created, 2 - Task Assigned to Staff, 3 - Task on Progress, 4 - Uploaded Assessment Schedule, 5 - Approved Assessment Schedule, 6 - Issued Response Letter, '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
