<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployerParticularChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employer_particular_changes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_id');
            $table->text('old_values');
            $table->text('new_values');
            $table->text('remark');
            $table->smallInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->integer('user_id');
            $table->string('folionumber')->nullable();
            $table->timestamps();
            $table->softDeletes();

            /*Foreign keys*/
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table employer_particular_changes is 'Table to keep track of all changes of employers particular'");
        DB::statement("comment on column employer_particular_changes.folionumber is 'Folio number of the document used to validate change of employer particulars'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
