<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerIsattanded extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer', function(Blueprint $table)
        {
                     $table->smallInteger('isattended')->default(0);
        });

        DB::statement("comment on column staff_employer.isattended is 'Flag to specify if employer is attended i.e. 1 => attended, 0 => not yet'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
