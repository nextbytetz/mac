<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntoStaffEmployerConfigsNextReviewer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_configs', function(Blueprint $table)
        {
            $table->integer('visitation_reviewer_pointer')->default(0);
        });
        DB::statement("comment on column staff_employer_configs.visitation_reviewer_pointer is 'Pointer of the next user to review visitation'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
