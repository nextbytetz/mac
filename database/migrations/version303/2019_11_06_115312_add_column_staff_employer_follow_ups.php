<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerFollowUps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_follow_ups', function(Blueprint $table)
        {
            $table->string('reminder_email')->nullable();
        });
        DB::statement("comment on column staff_employer_follow_ups.reminder_email is 'Employer email to be used for reminder.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
