<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsFlagsStaffEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_configs', function (Blueprint $table) {
            $table->text('intern_staff')->nullable();
        });

        Schema::table('staff_employer', function (Blueprint $table) {
            $table->smallInteger('isintern')->default(0);
        });


        DB::statement("comment on column staff_employer_allocations.category is 'Category of this allocation i.e. 1 => large, 2 => Non large, 3 => Bonus, 4 => treasury, 5 => Intern'");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
