<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewBookingGracePeriodView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
           DB::statement("CREATE VIEW main.booking_grace_period_view AS 
select b.id as booking_id, (date_trunc('MONTH', ( to_char(b.rcv_date, 'YYYYMM')||'01')::date) + INTERVAL '1 MONTH - 1 day')::DATE as booked_month, (date_trunc('MONTH', ( to_char(b.rcv_date + INTERVAL  '1 MONTH', 'YYYYMM')||'01')::date) + INTERVAL '1 MONTH - 1 day')::DATE as grace_period_end from bookings b;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
