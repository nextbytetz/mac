<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNeedsReviewStaffEmployerFollowUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_follow_ups', function(Blueprint $table)
        {
            $table->smallInteger('needs_review')->default(0);

        });
        DB::statement("comment on column staff_employer_follow_ups.needs_review is 'Flag to imply follow up need to be reviewed i.e. 1 => need review, 0 no review required'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
