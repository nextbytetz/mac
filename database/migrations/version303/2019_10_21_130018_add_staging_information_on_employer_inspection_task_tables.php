<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStagingInformationOnEmployerInspectionTaskTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_inspection_task_stages', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("employer_inspection_task_id");
            $table->bigInteger("staging_cv_id");
            $table->json("comments");
            $table->smallInteger("allow_repeat")->default(0);
            $table->smallInteger("attended")->default(0);
            $table->smallInteger("require_attend")->default(1);
            $table->smallInteger("isdeleted")->default(0);
            $table->timestamps();
            $table->foreign('employer_inspection_task_id')->references('id')->on('employer_inspection_task')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column employer_inspection_task_stages.allow_repeat is 'show whether this stage can be repeated at any time in the future process'");
        DB::statement("comment on column employer_inspection_task_stages.attended is 'show whether a user has executed the next stage action. 1 - yes, 0 - no'");
        DB::statement("comment on column employer_inspection_task_stages.isdeleted is 'show whether this stage has been deleted and has never been attended. Stages were frequently updated such that this stage was just logged and never to be attended. 1 - Yes, 0 - No'");

        Schema::table('employer_inspection_task', function(Blueprint $table)
        {
            $table->bigInteger("staging_cv_id")->nullable();
            $table->bigInteger("employer_inspection_task_stage_id")->nullable();
            $table->foreign('employer_inspection_task_stage_id')->references('id')->on('employer_inspection_task_stages')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
