<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasReconciledFlagOnReceiptCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function(Blueprint $table)
        {
            $table->smallInteger('hasreconciled')->default(0);
        });
        DB::statement("comment on column receipts.hasreconciled is 'check if the receipt has been reconciled by checking whether the contribution month has been paid for multiple time with different receipt and member count is lower than the previous month'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
