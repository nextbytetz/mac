<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasassessmentOnInspectionTaskPayrolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_task_payrolls', function (Blueprint $table) {
            $table->smallInteger("hasassessment")->default(0);
        });
        DB::statement("comment on column inspection_task_payrolls.hasassessment is 'show whether inspection assessment for the associated payroll has been uploaded or not. 1 - Uploaded, 0 - Not Uploaded.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_task_payrolls', function (Blueprint $table) {
            //
        });
    }
}
