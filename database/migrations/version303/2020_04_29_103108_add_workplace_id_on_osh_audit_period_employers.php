<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkplaceIdOnOshAuditPeriodEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
            $table->bigInteger("workplace_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osh_audit_period_employers', function (Blueprint $table) {
            $table->dropColumn("workplace_id");
        });
    }
}
