<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccidentCauseCvIdOnDeaths extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deaths', function(Blueprint $table)
        {
            $table->bigInteger('accident_cause_cv_id')->nullable();
            $table->foreign('accident_cause_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column deaths.accident_cause_cv_id is 'if death cause is occupational accident then the cause of accident is to be registered'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
