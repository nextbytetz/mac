<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnStaffEmployerAllocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_allocations', function(Blueprint $table)
        {
            $table->smallInteger('category')->nullable();
            $table->smallInteger('isactive')->default(0);
        });

        DB::statement("comment on column staff_employer_allocations.category is 'Category of this allocation i.e. 1 => large, 2 => Non large, 3 => Bonus'");
        DB::statement("comment on column staff_employer_allocations.isactive is 'Flag to specify if allocation is active i.e. 1=> active , 0 => not active'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
