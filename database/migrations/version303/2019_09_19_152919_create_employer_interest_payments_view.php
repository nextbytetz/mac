<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerInterestPaymentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("CREATE VIEW main.employer_interest_payments AS
    SELECT r.employer_id,
         e.doc,
         e.name                              AS employer_name,
         e.reg_no                            AS employer_reg_no,
         code.amount                         AS interest_amount,
         r.amount                            AS receipt_amount,
         to_date(concat_ws('-' :: text, date_part('year' :: text, code.contrib_month),
                           date_part('month' :: text, code.contrib_month), '28'),
                 'YYYY-MM-DD' :: text)       AS contrib_month,
         r.rct_date,
         r.created_at                        AS receipt_created_at,
         (r.rctno) :: character varying(100) AS rctno,
               r.pay_control_no                    AS control_no
  FROM ((main.receipt_codes code
      JOIN main.receipts r ON ((code.receipt_id = r.id)))
      JOIN main.employers e ON ((r.employer_id = e.id)))
  WHERE ((r.iscancelled = 0) AND (r.isdishonoured = 0) AND (r.deleted_at IS NULL) AND (code.deleted_at IS NULL)  AND
         ((code.fin_code_id = 1)));
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
