<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOshAuditPeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('osh_audit_period', function (Blueprint $table) {
           $table->increments('id');
           $table->bigInteger("fin_year_id")->index();
           $table->integer("period");
           $table->integer("number_of_employers")->nullable();
           $table->bigInteger("user_id")->nullable()->index();
           $table->string("fin_year");
           $table->integer("status")->nullable();
           $table->smallInteger("wf_done")->default(0);
           $table->date("wf_done_date")->nullable();
           $table->string("user_type")->default('App\Models\Auth\User');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('osh_audit_period');
    }
}
