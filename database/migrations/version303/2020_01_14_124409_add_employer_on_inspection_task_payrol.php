<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerOnInspectionTaskPayrol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_task_payrolls', function (Blueprint $table) {
            $table->dropColumn("inspection_task_id");
        });
        $sql = <<<SQL
drop view if exists inspection_remittance;
DROP MATERIALIZED VIEW IF EXISTS inspection_assessment_template;
SQL;
        DB::unprepared($sql);
        Schema::table('inspection_assessments', function (Blueprint $table) {
            $table->dropColumn("employer_inspection_task_id");
            $table->bigInteger("inspection_task_payroll_id");
            $table->foreign('inspection_task_payroll_id')->references('id')->on('inspection_task_payrolls')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_task_payrolls', function (Blueprint $table) {
            //
        });
    }
}
