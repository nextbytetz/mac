<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFrequencyAndMemberCountDifferenceOnReceiptCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->smallInteger('frequency')->default(1);
            $table->smallInteger('hasmemberdiff')->default(3);
        });
        DB::statement("comment on column receipt_codes.frequency is 'for how many times this contribution month with different receipt has been paid'");
        DB::statement("comment on column receipt_codes.hasmemberdiff is 'set if the member count is lower than the recent contribution month. 1 -> Number lowered, 0 -> No Change, 2 - Number Increased, 3 - Not Reconciled, 4 - First Entry'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
