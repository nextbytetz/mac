<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbObjectsEmployerClosuresFeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->date('date_reference')->nullable();

        });
//

        DB::statement("comment on column document_employer.date_reference is 'Date reference for a particular document e.g. For documents need to be on defined period interval'");



        Schema::create('employer_closure_follow_ups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->text('description');
            $table->text('feedback')->nullable();
            $table->integer('follow_up_type_cv_id');
            $table->string('contact', 45)->nullable();
            $table->string('contact_person', 255)->nullable();
            $table->date('date_of_follow_up')->nullable();
            $table->integer('user_id');
            $table->integer('employer_closure_id');
            $table->timestamps();
        });


        DB::statement("comment on table employer_closure_follow_ups is 'Table to keep track of follow ups for a particular employer closure of business'");
        DB::statement("comment on column employer_closure_follow_ups.description is 'Description of the follow up performed'");
        DB::statement("comment on column employer_closure_follow_ups.feedback is 'Feedback of the follow up, Deliverable achieved with this follow up.'");


        Schema::table('employer_closure_follow_ups', function(Blueprint $table)
        {
            $table->foreign('follow_up_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_closure_id')->references('id')->on('employer_closures')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->softDeletes();
        });
//

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
