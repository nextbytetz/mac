<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContributionFlagsForInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
update inspection_task_payrolls set iscompliant = case when tt.underpaid > 5 then 0 else 1 end, hasoverpay = case when tt.overpaid > 5 then 1 else 0 end from (select b.employer_inspection_task_id, b.inspection_task_payroll_id, coalesce(sum(accumulative_to_be_paid), 0) underpaid, coalesce(sum(overpaid), 0) overpaid from employer_inspection_task a join inspection_assessment_template b on a.id = b.employer_inspection_task_id where a.progressive_stage >= 4 group by b.employer_inspection_task_id, b.inspection_task_payroll_id) tt where tt.inspection_task_payroll_id = inspection_task_payrolls.id;

update employer_inspection_task set iscompliant = case when tt.underpaid > 5 then 0 else 1 end, hasoverpay = case when tt.overpaid > 5 then 1 else 0 end from (select b.employer_inspection_task_id, coalesce(sum(accumulative_to_be_paid), 0) underpaid, coalesce(sum(overpaid), 0) overpaid from employer_inspection_task a join inspection_assessment_template b on a.id = b.employer_inspection_task_id where a.progressive_stage >= 4 group by b.employer_inspection_task_id) tt where tt.employer_inspection_task_id = employer_inspection_task.id;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
