<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateContribReconciliationView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //create receonciliations
        DB::statement("select deps_save_and_drop_dependencies('main', 'employer_contribution_reconciliations')");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  employer_contribution_reconciliations");
        DB::statement("CREATE VIEW employer_contribution_reconciliations AS select   c.employer_id,  c.contrib_month,(select sum(curr.contrib_amount) from employer_contributions curr where curr.employer_id = c.employer_id and
  date_part('year' :: text, curr.contrib_month) = date_part('year' :: text, c.contrib_month) and
   date_part('month' :: text, curr.contrib_month) = date_part('month' :: text, c.contrib_month )) as current_amount, (select sum(prev.contrib_amount) from employer_contributions prev where prev.employer_id = c.employer_id and
  date_part('year' :: text, prev.contrib_month) = date_part('year' :: text, c.contrib_month + INTERVAL '-1 month' ) and
   date_part('month' :: text, prev.contrib_month) = date_part('month' :: text, c.contrib_month + INTERVAL '-1 month' )
  ) as prev_amount from employer_contributions c
GROUP BY c.employer_id, c.contrib_month;
");

        DB::statement("select deps_restore_dependencies('main', 'employer_contribution_reconciliations')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
