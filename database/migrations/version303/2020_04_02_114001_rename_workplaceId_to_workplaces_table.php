<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameWorkplaceIdToWorkplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workplaces', function (Blueprint $table) {
            if (Schema::hasColumn("workplaces","workplaceId")) {
                $table->dropColumn("workplaceId");
                $table->string("workplace_regno")->nullable();
            }else{
                $table->string("workplace_regno")->nullable();
            }
            
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
