<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceiptApprovalView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create or replace view receipt_approvals as select receipts.*, wf_tracks.forward_date, (case when fin_code_id is not null then 1 else wf_tracks.status end) as status from "receipts" left join "wf_tracks" on "wf_tracks"."resource_id" = "receipts"."id" and ("wf_definition_id" = 12)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
