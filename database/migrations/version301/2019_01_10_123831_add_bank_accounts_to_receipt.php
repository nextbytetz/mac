<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAccountsToReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->bigInteger("bank_id");
            $table->bigInteger("fin_code_id");
            $table->timestamps();
            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fin_code_id')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table bank_accounts is 'bank to store the relationship between banks and fin_codes'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
