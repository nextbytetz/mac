<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaterializedViewAgentsEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//          $sql = <<<SQL

//          /*Agents online employers count*/
//   create materialized view comp_agents_employer as select u.id,u.name as agent_name,count(eu.employer_id) as no_employers 
//     from portal.users u join portal.employer_user eu on u.id=eu.user_id 
//     join main.employers e on eu.employer_id=e.id where u.user_type_cv_id=2 and e.reg_no is not null and e.approval_id=1 group by u.name,u.id order by u.name asc;



// SQL;

//         DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
