<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErpReceiptsTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts_gl', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->bigInteger('trx_id');
            $table->string('trx_type');
            $table->bigInteger('rctno');
            $table->date('rct_date');
            $table->text('payment_description');
            $table->date('payment_date');
            $table->string('spare_three')->nullable(); //need discussion
            $table->string('hr_staff_id')->nullable();
            $table->integer('employer_id')->nullable();
            $table->string('thirdparty_id')->nullable();
            $table->string('payee_name');
            $table->string('debitaccount_bankaccount');
            $table->string('creditaccount_receivableaccount');
            $table->string('control_number',20)->nullable();
            $table->string('currency');
            $table->decimal('amount',14);
            $table->boolean('isposted');
            $table->string('status_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipts_gl', function (Blueprint $table) {
            //
        });
    }
}
