<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpClaimsPayableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims_payable', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('trx_id');
            $table->string('member_type');
            $table->string('notification_id',50);
            $table->string('payee_name');
            $table->bigInteger('supplier_id');
            $table->string('benefit_type');
            $table->string('debit_account_expense');
            $table->string('credit_account_receivable');
            $table->string('accountno');
            $table->string('bank_name');
            $table->string('swift_code');
            $table->integer('bank_branch_id');
            $table->string('bank_address');
            $table->string('country_name_bank');
            $table->string('city_of_bank');
            $table->string('state_or_region');
            $table->decimal('amount',14);
            $table->boolean('isposted');
            $table->string('status_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims_payable');
    }
}
