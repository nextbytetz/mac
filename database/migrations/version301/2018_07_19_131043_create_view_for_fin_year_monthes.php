<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewForFinYearMonthes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create materialized view fin_year_months as select a.id as fin_year_id, a.name, a.start, a.end, a.start_date, a.end_date, b.date_value from fin_years a JOIN (select ('2015-07-28'::date + (interval '1' month * generate_series(0, (select 12 * max(id) from fin_years))))::date as date_value) as b on b.date_value between a.start_date and a.end_date");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
