<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentReserveForRequesterOnEmployerVerification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column online_employer_verifications.employer_id_reserve is 'in case of requested employer_id is inactive due to being merged to other employer, record the previous requested employer_id by the user'");
        DB::statement("comment on column online_employer_verifications.reg_no_reserve is 'in case of requested reg_no is inactive due to being merged to other employer, record the previous requested reg_no by the user'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
