<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableBookingInterestRefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('booking_interest_refunds', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal('amount', 15);
            $table->integer('user_id')->index();
            $table->smallInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->integer('booking_interest_refund_id')->index()->nullable();
            $table->decimal('refund_amount', 15)->nullable();
            $table->smallInteger('isrefunded')->default(0);

        });

        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->foreign('booking_interest_refund_id')->references('id')->on('booking_interest_refunds')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
            }
}
