<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMysubstituteModuleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('user_subs', function (Blueprint $table) {
            $table->increments("id");
            $table->bigInteger("user_id")->index();
            $table->bigInteger("sub");
            $table->dateTime("from_date");
            $table->dateTime("to_date")->nullable();
            $table->text("reason")->nullable();
            $table->bigInteger("user_set_id");
            $table->timestamps();
            $table->foreign('user_set_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('sub')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column user_subs.user_set_id is 'id of the user who has set the user substitution'");
        DB::statement("comment on column user_subs.reason is 'The reason for setting a substitution'");

        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger("sub")->nullable();
            $table->integer("user_sub_id")->nullable();
            $table->foreign('sub')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_sub_id')->references('id')->on('user_subs')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column users.sub is 'id of the user who have substituted'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
