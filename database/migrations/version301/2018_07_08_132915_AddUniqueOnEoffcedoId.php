<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueOnEoffcedoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Remove unique violation from document_notification_report
        \Illuminate\Support\Facades\DB::Table('document_notification_report')->where('id', 3742)->update([ 'eoffice_document_id' => null]);


        Schema::table('document_notification_report', function (Blueprint $table) {
            $table->unique("eoffice_document_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('document_notification_report', function (Blueprint $table) {
            $table->dropUnique("document_notification_report_eoffice_document_id_unique");
        });
    }
}
