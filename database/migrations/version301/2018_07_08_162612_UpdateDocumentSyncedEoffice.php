<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDocumentSyncedEoffice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //Update source for all synced documents
        \Illuminate\Support\Facades\DB::Table('document_notification_report')->where('name', '<>', 'e-office')->whereNotNull('eoffice_document_id')->update([ 'name' => 'e-office']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
