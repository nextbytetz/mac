<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('trx_id');
            $table->string('supplier_name');
            $table->bigInteger('supplier_id');
            $table->string('vendor_site_code',30);
            $table->string('address_line1');
            $table->string('address_line2');
            $table->string('city');
            $table->string('liability_account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_suppliers');
    }
}
