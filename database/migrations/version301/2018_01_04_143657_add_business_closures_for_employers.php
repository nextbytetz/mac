<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessClosuresForEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_closures', function (Blueprint $table) {
            $table->increments('id');
            $table->date("close_date");
            $table->date("open_date")->nullable();
            $table->smallInteger("wf_done")->default(0);
            $table->dateTime("wf_done_date")->nullable();
            $table->bigInteger("user_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
