<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifySelfCountersForNotificationsOnNotificationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('diseases', function (Blueprint $table) {
            $table->integer("odsno")->nullable()->change();
        });
        Schema::table('deaths', function (Blueprint $table) {
            $table->integer("odtno")->nullable()->change();
        });
        Schema::table('accidents', function (Blueprint $table) {
            $table->integer("oacno")->nullable()->change();
        });

        DB::statement("update diseases set odsno = null;");
        DB::statement("update deaths set odtno = null;");
        DB::statement("update accidents set oacno = null;");

        Schema::table('diseases', function (Blueprint $table) {
            $table->integer("odsno")->unique()->change();
        });
        Schema::table('deaths', function (Blueprint $table) {
            $table->integer("odtno")->unique()->change();
        });
        Schema::table('accidents', function (Blueprint $table) {
            $table->integer("oacno")->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
