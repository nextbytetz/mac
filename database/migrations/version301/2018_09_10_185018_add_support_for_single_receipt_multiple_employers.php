<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupportForSingleReceiptMultipleEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_groups', function(Blueprint $table)
        {
            $table->bigInteger("employer_id")->index();
            $table->dropColumn("name");
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->string("receipt_type", 150)->default("App\Models\Finance\Receipt\Receipt");
            $table->dropForeign("receipt_codes_ibfk_1");
        });
        Schema::create('booking_group_receipts', function(Blueprint $table)
        {
            $table->increments("id");
            $table->integer("receipt_id")->index();
            $table->bigInteger("employer_id")->index();
            $table->string("payer", 200);
            $table->text("description")->nullable();
            $table->decimal("amount", 14);
            $table->timestamps();
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
