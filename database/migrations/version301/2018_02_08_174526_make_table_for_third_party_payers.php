<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTableForThirdPartyPayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('thirdparties', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->bigInteger("employer_id")->nullable();
            $table->string("firstname", 50)->nullable();
            $table->string("middlename", 50)->nullable();
            $table->string("lastname", 50)->nullable();
            $table->string("external_id", 30)->nullable();
            $table->integer("isactive")->default(1);
            $table->timestamps();
        });

        Schema::table('receipts', function (Blueprint $table) {
            $table->bigInteger("staff_id")->nullable();
            $table->smallInteger("thirdparty_id")->nullable();
            $table->foreign('staff_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('thirdparty_id')->references('id')->on('thirdparties')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
