<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserRemovedOnTraData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('closed_businesses', function (Blueprint $table) {
            $table->bigInteger("user_closed")->nullable()->index();
            $table->foreign('user_closed')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('new_taxpayers', function (Blueprint $table) {
            $table->bigInteger("user_closed")->nullable()->index();
            $table->foreign('user_closed')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
