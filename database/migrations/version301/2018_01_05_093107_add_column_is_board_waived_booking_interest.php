<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsBoardWaivedBookingInterest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->smallInteger('is_board_waived')->default(0)->index();
                  });

        DB::statement("comment on column booking_interests.is_board_waived is 'Flag to specify if interests has been waived / adjusted from Board Resolution; 1 => is waived / adjusted, 0 => Not Waived / adjusted'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->dropColumn('is_board_waived');
        });

    }
}
