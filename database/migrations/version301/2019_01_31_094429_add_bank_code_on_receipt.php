<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankCodeOnReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function (Blueprint $table) {
            $table->bigInteger('bank_code')->nullable();
            $table->foreign('bank_code')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column receipts.bank_code is 'representation of the bank account in the gl accounts in fin codes'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
