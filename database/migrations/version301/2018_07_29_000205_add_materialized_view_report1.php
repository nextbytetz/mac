<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaterializedViewReport1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
-- fin_monthly_collection_electronic
create materialized view fin_monthly_collection_electronic as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, coalesce(sum(a.amount), 0) as amount from receipts a right join fin_year_months b on date_part('month', a.rct_date::date) = date_part('month', b.date_value::date) and date_part('year', a.rct_date::date) = date_part('year', b.date_value) and iscancelled = 0 and isdishonoured = 0 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- fin_monthly_collection_manual
create materialized view fin_monthly_collection_manual as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.* from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, coalesce(sum(a.amount), 0) as amount from legacy_receipts a right join fin_year_months b on date_part('month', a.rct_date::date) = date_part('month', b.date_value::date) and date_part('year', a.rct_date::date) = date_part('year', b.date_value) and (contr_approval = 1 or contr_approval = 2) and iscancelled = 0 and isdishonoured = 0 where b.start <= 2018 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- fin_monthly_employer_interest
create materialized view fin_monthly_employer_interest as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, coalesce(sum(g.amount), 0) as amount from receipts a join receipt_codes g on a.id = g.receipt_id and g.fin_code_id = 1 and iscancelled = 0 and isdishonoured = 0 right join fin_year_months b on date_part('month', g.contrib_month::date) = date_part('month', b.date_value::date) and date_part('year', g.contrib_month::date) = date_part('year', b.date_value) group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- fin_monthly_receipt
create materialized view fin_monthly_receipt as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(a.amount) as total from receipts a right join fin_year_months b on date_part('month', a.rct_date::date) = date_part('month', b.date_value::date) and date_part('year', a.rct_date::date) = date_part('year', b.date_value) and iscancelled = 0 and isdishonoured = 0 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- fin_monthly_cancelled
create materialized view fin_monthly_cancelled as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(a.amount) as total from receipts a right join fin_year_months b on date_part('month', a.rct_date::date) = date_part('month', b.date_value::date) and date_part('year', a.rct_date::date) = date_part('year', b.date_value) and iscancelled = 1 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- fin_monthly_dishonoured
create materialized view fin_monthly_dishonoured as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(a.amount) as total from receipts a right join fin_year_months b on date_part('month', a.rct_date::date) = date_part('month', b.date_value::date) and date_part('year', a.rct_date::date) = date_part('year', b.date_value) and isdishonoured = 1 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_monthly_contribution_electronic
create materialized view comp_monthly_contribution_electronic as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, coalesce(sum(g.amount), 0) as amount from receipts a join receipt_codes g on a.id = g.receipt_id and g.fin_code_id = 2 and iscancelled = 0 and isdishonoured = 0 right join fin_year_months b on date_part('month', g.contrib_month::date) = date_part('month', b.date_value::date) and date_part('year', g.contrib_month::date) = date_part('year', b.date_value) group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_monthly_contribution_manual
create materialized view comp_monthly_contribution_manual as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, coalesce(sum(g.amount), 0) as amount from legacy_receipts a join legacy_receipt_codes g on a.id = g.legacy_receipt_id and g.fin_code_id = 2 and a.iscancelled = 0 and a.isdishonoured = 0 and (a.contr_approval = 1 or a.contr_approval = 2) right join fin_year_months b on date_part('month', g.contrib_month::date) = date_part('month', b.date_value::date) and date_part('year', g.contrib_month::date) = date_part('year', b.date_value) group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_yearly_contributing_employer_electronic
create materialized view comp_yearly_contributing_employer_electronic as select a.*  from (select b.name, b.id as fin_year_id, count(distinct(a.employer_id)) as employers from receipts a join receipt_codes g on a.id = g.receipt_id and g.fin_code_id = 2 and iscancelled = 0 and isdishonoured = 0 right join fin_years b on a.rct_date between b.start_date and b.end_date group by b.name, b.id) as a;

-- comp_yearly_contributing_employer_manual
create materialized view comp_yearly_contributing_employer_manual as select a.*  from (select b.name, b.id as fin_year_id, count(distinct(a.employer_id)) as employers from legacy_receipts a join legacy_receipt_codes g on a.id = g.legacy_receipt_id and g.fin_code_id = 2 and (a.contr_approval = 1 or a.contr_approval = 2) and a.iscancelled = 0 and a.isdishonoured = 0 right join fin_years b on a.rct_date between b.start_date and b.end_date where b.start <= 2018 group by b.name, b.id) as a;

-- comp_monthly_contributing_employer_electronic
create materialized view comp_monthly_contributing_employer_electronic as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(distinct(a.employer_id)) as employers from receipts a join receipt_codes g on a.id = g.receipt_id and g.fin_code_id = 2 and iscancelled = 0 and isdishonoured = 0 right join fin_year_months b on date_part('month', g.contrib_month::date) = date_part('month', b.date_value::date) and date_part('year', g.contrib_month::date) = date_part('year', b.date_value) group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_monthly_contributing_employer_manual
create materialized view comp_monthly_contributing_employer_manual as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(distinct(a.employer_id)) as employers from legacy_receipts a join legacy_receipt_codes g on a.id = g.legacy_receipt_id and g.fin_code_id = 2 and (a.contr_approval = 1 or a.contr_approval = 2) and a.iscancelled = 0 and a.isdishonoured = 0 right join fin_year_months b on date_part('month', g.contrib_month::date) = date_part('month', b.date_value::date) and date_part('year', g.contrib_month::date) = date_part('year', b.date_value) where b.start <= 2018 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_monthly_inspected_employers
create materialized view comp_monthly_inspected_employers as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(distinct(a.employer_id)) as employers  from employer_inspection_task a right join fin_year_months b on date_part('month', a.visit_date::date) = date_part('month', b.date_value::date) and date_part('year', a.visit_date::date) = date_part('year', b.date_value) and a.status = 1 where b.start >= 2017 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_monthly_emergence_inspection
create materialized view comp_monthly_emergence_inspection as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(a.id) as total  from inspections a right join fin_year_months b on date_part('month', a.start_date::date) = date_part('month', b.date_value::date) and date_part('year', a.start_date::date) = date_part('year', b.date_value) and a.inspection_type_id = 2 where b.start >= 2017 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_monthly_booking
create materialized view comp_monthly_booking as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, sum(a.amount) as amount  from bookings a right join fin_year_months b on date_part('month', a.rcv_date::date) = date_part('month', b.date_value::date) and date_part('year', a.rcv_date::date) = date_part('year', b.date_value) group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_yearly_employer_registration
create materialized view comp_yearly_employer_registration as select a.*  from (select b.name, b.id as fin_year_id, count(a.id) as employers from employers a right join fin_years b on a.created_at between b.start_date and b.end_date group by b.name, b.id) as a;

-- comp_monthly_employer_registration
create materialized view comp_monthly_employer_registration as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(a.id) as employers  from employers a right join fin_year_months b on date_part('month', a.created_at::date) = date_part('month', b.date_value::date) and date_part('year', a.created_at::date) = date_part('year', b.date_value) group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_employers_per_region
create materialized view comp_employers_per_region as select a.region_id, b.name, count(a.id) as employers from employers a join regions b on a.region_id = b.id group by a.region_id, b.name;

-- comp_yearly_certificate_issue
create materialized view comp_yearly_certificate_issue as select a.*  from (select b.name, b.id as fin_year_id, count(a.id) as total from employer_certificate_issues a right join fin_years b on a.created_at between b.start_date and b.end_date group by b.name, b.id) as a;

-- comp_monthly_employer_certificate
create materialized view comp_monthly_employer_certificate as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(a.id) as certificate  from employer_certificate_issues a right join fin_year_months b on date_part('month', a.created_at::date) = date_part('month', b.date_value::date) and date_part('year', a.created_at::date) = date_part('year', b.date_value) and a.is_reissue = 0 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

-- comp_yearly_employer_certificate
create materialized view comp_yearly_employer_certificate as select a.*  from (select b.name, b.id as fin_year_id, count(a.id) as certificate from employer_certificate_issues a right join fin_years b on a.is_reissue = 0 and a.created_at between b.start_date and b.end_date  group by b.name, b.id) as a;

-- comp_staff_verified_contribution
create materialized view comp_staff_verified_contribution as select c.name as fin_year, c.id as fin_year_id, a.username, a.id as user_id, count(b.id) as contribution from users a join wf_tracks b on a.id = b.user_id and a.unit_id = 15 and a.designation_id <> 5 and b.wf_definition_id = 12 and b.status = 1 right join fin_years c on b.forward_date between c.start_date and c.end_date group by c.name, c.id, a.username, a.id;

-- comp_large_contributor
create materialized view comp_large_contributor as select c.id, c.district_id, c.region_id, c.name, c.phone, c.telephone, c.email, r.name as region, c.tin, c.reg_no, d.contribution, d.member_count as employees
    from main.employers c join (select employer_id,  floor(avg(b.amount)) as contribution, floor(avg(b.member_count)) as member_count from main.receipts a join main.receipt_codes b on a.id = b.receipt_id where b.fin_code_id = 2 and a.iscancelled = 0 and a.isdishonoured = 0 and ((now()::date - a.created_at::date)/30 < (select large_contributor_month_limit from main.sysdefs)) group by a.employer_id having avg(b.amount) > (select large_contributor_amount_limit from main.sysdefs)) as d on c.id = d.employer_id left join main.regions r ON c.region_id = r.id order by d.contribution desc;

-- comp_employer_business_status
create materialized view comp_employer_business_status as select * from (select count(b.id) as active_public from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 36 and b.employer_status = 1 and b.approval_id = 1) as o cross join (select count(b.id) as active_private from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 37 and b.employer_status = 1 and b.approval_id = 1) as p CROSS JOIN (select count(b.id) as dormant_public from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 36 and b.employer_status = 2  and b.approval_id = 1) as q cross join (select count(b.id) as dormant_private from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 37 and b.employer_status = 2  and b.approval_id = 1) as r cross join (select count(b.id) as closed_public from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 36 and b.employer_status = 3 and b.approval_id = 1) as s CROSS JOIN (select count(b.id) as closed_private from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 37 and b.employer_status = 3 and b.approval_id = 1) as t cross join (select count(b.id) as all_employers from employers b where b.approval_id = 1) as u cross join (select count(*) as large_contributor from comp_large_contributor) as v;

   


SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
