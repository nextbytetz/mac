<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExternalPayAttributesOnReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function (Blueprint $table) {
            $table->text("pay_control_no")->nullable();
            $table->text("external_rctno")->nullable();
        });

        DB::statement("comment on column receipts.pay_control_no is 'Payment control number for online payments (GEPG)'");
        DB::statement("comment on column receipts.external_rctno is 'Payment receipt number for the online payment (GEPG)'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
