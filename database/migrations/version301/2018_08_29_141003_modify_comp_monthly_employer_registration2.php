<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCompMonthlyEmployerRegistration2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL

DROP MATERIALIZED VIEW comp_monthly_employer_registration;
create materialized view comp_monthly_employer_registration as select to_char((year || '-' || month || '-1')::date, 'Mon YY ') as period, a.*  from (select date_part('year', b.date_value::date) as year, date_part('month', b.date_value::date) as month, b.fin_year_id, count(a.id) as employers  from employers a right join fin_year_months b on date_part('month', a.created_at::date) = date_part('month', b.date_value::date) and date_part('year', a.created_at::date) = date_part('year', b.date_value) and a.approval_id = 1 group by date_part('year', b.date_value::date), date_part('month', b.date_value::date), b.fin_year_id) as a;

SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
