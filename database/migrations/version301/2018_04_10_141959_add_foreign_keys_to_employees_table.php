<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_archives', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('marital_status_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('location_type_id')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('job_title_id')->references('id')->on('job_titles')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('identity_id')->references('id')->on('identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gender_id')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('employee_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employees', function(Blueprint $table)
		{
			$table->dropForeign('employees_user_id_foreign');
			$table->dropForeign('employees_marital_status_cv_id_foreign');
			$table->dropForeign('employees_location_type_id_foreign');
			$table->dropForeign('employees_job_title_id_foreign');
			$table->dropForeign('employees_identity_id_foreign');
			$table->dropForeign('employees_gender_id_foreign');
			$table->dropForeign('employees_employee_category_cv_id_foreign');
			$table->dropForeign('employees_bank_branch_id_foreign');
		});
	}

}
