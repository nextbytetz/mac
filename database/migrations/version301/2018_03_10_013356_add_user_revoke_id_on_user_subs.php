<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserRevokeIdOnUserSubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_subs', function (Blueprint $table) {
            $table->bigInteger("user_revoke_id")->nullable();
            $table->foreign('user_revoke_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column user_subs.user_revoke_id is 'specify the user who have revoked the substitution'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
