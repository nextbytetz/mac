<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_refunds', function (Blueprint $table) {
            $table->increments("id");
            $table->bigInteger("receipt_id")->index();
            $table->text("description")->nullable();
            $table->smallInteger("wf_done")->default(0);
            $table->date("wf_done_date")->nullable();
            $table->smallInteger("ispaid")->default(0);
            $table->smallInteger("ssyes")->default(0);
            $table->timestamps();
            $table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table receipt_refunds is 'table to store all the refunds processed as requested by employers'");
        DB::statement("comment on column receipt_refunds.description is 'reasons for the refund requested'");
        DB::statement("comment on column receipt_refunds.ispaid is 'set the payment status whether is already paid or not, the status is received from erp. 1 - Yes, 0 - No'");
        DB::statement("comment on column receipt_refunds.ssyes is 'show whether the refund entry has been sent to erp or not. 1 - Yes, 0 - No'");
        DB::statement("comment on column receipt_refunds.wf_done is 'indicate whether the workflow approval has completed or not. 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
