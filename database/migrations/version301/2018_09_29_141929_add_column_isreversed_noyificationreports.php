<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsreversedNoyificationreports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->integer("is_reversed")->default(0);
        });

        DB::statement("comment on column notification_reports.is_reversed is 'Flag to specify if notification approval/rejection has been reversed'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->dropColumn("is_reversed");
        });
    }
}
