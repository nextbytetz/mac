<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Operation\Claim\InvestigationQuestion;
use Illuminate\Support\Facades\Artisan;
use App\Models\Operation\Claim\InvestigationFeedback;
use App\Models\Operation\Claim\NotificationReport;

class AddGeneralRemarksToAllInvestigationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Artisan::call('db:seed');
        $questions = InvestigationQuestion::select(['id'])->orderBy('id', 'asc')->get();
        $notifications = NotificationReport::select(['id'])->get();
        foreach ($notifications as $notification) {
            foreach ($questions as $question) {
                InvestigationFeedback::firstOrCreate(['notification_report_id' => $notification->id, 'investigation_question_id' => $question->id]);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
