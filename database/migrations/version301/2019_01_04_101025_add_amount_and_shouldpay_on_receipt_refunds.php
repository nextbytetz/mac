<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountAndShouldpayOnReceiptRefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_refunds', function (Blueprint $table) {
            $table->decimal("amount", 14)->nullable();
            $table->smallInteger("shouldpay")->default(1);
        });
        DB::statement("comment on column receipt_refunds.amount is 'amount eligible to be paid to the employer'");
        DB::statement("comment on column receipt_refunds.shouldpay is 'show whether the employer has decided the refund to be payed or be transferred to other contribution. 1 - should be paid, 0 - to be transferred to other contribution.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
