<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManualReceiptControlOnSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->integer("manual_receipt_file_total")->default(0);
            $table->integer("manual_receipt_file_uploaded")->default(0);
        });
        Schema::table('legacy_receipts', function(Blueprint $table)
        {
            $table->smallInteger("isuploaded")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
