<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasfileerrorOnBookingGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_groups', function (Blueprint $table) {
            $table->smallInteger("hasfileerror")->default(0);
        });
        Schema::table('booking_group_employers', function (Blueprint $table) {
            $table->smallInteger("hasfileerror")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
