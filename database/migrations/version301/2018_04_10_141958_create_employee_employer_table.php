<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeEmployerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_archive_employer', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_id')->index();
			$table->bigInteger('employer_id')->index();
			$table->timestamps();
			$table->decimal('basicpay', 14)->nullable();
			$table->decimal('grosspay', 14)->nullable();
			$table->bigInteger('employee_category_cv_id')->nullable()->index();
			$table->text('job_title')->nullable();
			$table->date('datehired')->nullable();
			$table->bigInteger('user_id')->nullable()->index();
			$table->string('user_type', 150)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_employer');
	}

}
