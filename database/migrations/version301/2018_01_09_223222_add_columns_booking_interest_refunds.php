<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsBookingInterestRefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interest_refunds', function(Blueprint $table)
        {
            $table->integer('bank_branch_id')->unsigned()->nullable()->index();
            $table->string('accountno', 45)->nullable();
        });

        Schema::table('booking_interest_refunds', function(Blueprint $table)
        {
            $table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
