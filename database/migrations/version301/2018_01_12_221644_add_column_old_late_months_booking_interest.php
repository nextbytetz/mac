<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOldLateMonthsBookingInterest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->integer("old_late_months")->nullable();
                   });

        DB::statement("comment on column booking_interests.old_late_months is 'late months of contribution before interest adjustment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
