<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfDoneAndIsAvailableOnClosedBusinesses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('closed_businesses', function (Blueprint $table) {
            $table->smallInteger("wf_done")->nullable()->default(0);
            $table->dateTime("wf_done_date")->nullable();
            $table->smallInteger("isavailable")->default(0);
        });
        DB::statement("comment on column closed_businesses.isavailable is 'check whether the closed business is available in the registered employers database'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
