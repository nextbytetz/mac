<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsPostederpToTableReceiptRefund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_refunds', function (Blueprint $table) {
            $table->boolean('is_posted_erp')->default(false)->comment('Wether it has been posted to erp or not');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_refunds', function (Blueprint $table) {
            //
            $table->dropColumn('is_postederp');

        });
    }
}
