<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnClaimTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claims', function (Blueprint $table) {
            $table->date("date_of_mmi")->nullable();
        });

        DB::statement("comment on column claims.date_of_mmi is 'Date of MMI added on claim assessment stage for claim with pd > 30, Imply date when maximum improvement has been attained by the member'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('claims', function (Blueprint $table) {
            $table->dropColumn("date_of_mmi");
        });
    }
}
