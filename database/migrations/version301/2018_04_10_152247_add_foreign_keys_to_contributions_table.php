<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contribution_archives', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('employee_id')->references('id')->on('employee_archives')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('legacy_receipt_code_id')->references('id')->on('legacy_receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('receipt_code_id')->references('id')->on('receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contributions', function(Blueprint $table)
		{
			$table->dropForeign('contributions_user_id_foreign');
			$table->dropForeign('contributions_employee_id_foreign');
			$table->dropForeign('contributions_legacy_receipt_code_id_foreign');
			$table->dropForeign('contributions_ibfk_1');
			$table->dropForeign('contributions_employer_id_foreign');
		});
	}

}
