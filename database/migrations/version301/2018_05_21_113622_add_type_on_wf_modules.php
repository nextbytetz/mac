<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeOnWfModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_modules', function (Blueprint $table) {
            $table->smallInteger("type")->default(0);
        });
        DB::statement("comment on column wf_modules.type is 'the different types of workflow modules for the same group. It is to be identified by types of specific resource.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
