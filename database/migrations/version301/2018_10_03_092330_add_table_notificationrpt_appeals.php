<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableNotificationrptAppeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('notification_report_appeals', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('notification_report_id');
            $table->string('reason');
            $table->integer('approved_by');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table notification_report_appeals is 'Table for storing all approval of rejection appeals of notification report'");

        DB::statement("comment on column notification_report_appeals.approved_by is 'User who approved rejection appeal of notification report'");

        Schema::table('notification_report_appeals', function(Blueprint $table)
        {
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('approved_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('notification_report_appeals');

    }
}
