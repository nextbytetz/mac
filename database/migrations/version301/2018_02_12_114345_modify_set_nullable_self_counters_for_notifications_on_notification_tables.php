<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifySetNullableSelfCountersForNotificationsOnNotificationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diseases', function (Blueprint $table) {
            $table->integer("odsno")->default("NULL")->change();
        });
        Schema::table('deaths', function (Blueprint $table) {
            $table->integer("odtno")->default("NULL")->change();
        });
        Schema::table('accidents', function (Blueprint $table) {
            $table->integer("oacno")->default("NULL")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
