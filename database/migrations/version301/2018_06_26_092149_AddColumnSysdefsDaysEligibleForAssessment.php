<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSysdefsDaysEligibleForAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->integer("days_eligible_for_claim_assessment")->nullable();
        });
        DB::statement("comment on column sysdefs.days_eligible_for_claim_assessment is 'No of ED / LD days eligible for claim assessment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->dropColumn("days_eligible_for_claim_assessment");
        });
    }
}
