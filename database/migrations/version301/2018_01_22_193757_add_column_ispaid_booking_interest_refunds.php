<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIspaidBookingInterestRefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interest_refunds', function (Blueprint $table) {
            $table->integer("is_paid")->default(0);
        });

        DB::statement("comment on column booking_interest_refunds.is_paid is 'Flag to specify if refund is already processed for payments; 1 => Already processed, 0 => Pending'");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
