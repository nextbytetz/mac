<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WipingCurrentEmployeeDataAndArchive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
$sql = <<<SQL
do $$ 
declare id bigint;
begin
    insert into employee_archives (id, memberno, firstname, middlename, lastname, email, phone, dob, basicpay, grosspay, nid, identity_id, job_title_id, gender_id, marital_status_cv_id, employee_category_cv_id, bank_branch_id, location_type_id, street, road, plot_no, block_no, surveyed_extra, unsurveyed_area, user_id, department, fax, accountno, sex, job_title, emp_cate, created_at, updated_at, deleted_at, id_no, external_id, employee_status, source) select employees.id, memberno, firstname, middlename, lastname, email, phone, dob, basicpay, grosspay, nid, identity_id, job_title_id, gender_id, marital_status_cv_id, employee_category_cv_id, bank_branch_id, location_type_id, street, road, plot_no, block_no, surveyed_extra, unsurveyed_area, user_id, department, fax, accountno, sex, job_title, emp_cate, created_at, updated_at, deleted_at, id_no, external_id, employee_status, source from employees where employees.id not in (select employee_id from notification_reports);
    
    insert into employee_archive_employer (employee_id, employer_id, created_at, updated_at, basicpay, grosspay, employee_category_cv_id, job_title, datehired, user_id, user_type) select employee_id, employer_id, created_at, updated_at, basicpay, grosspay, employee_category_cv_id, job_title, datehired, user_id, user_type from employee_employer where employee_employer.employee_id not in (select employee_id from notification_reports);
    
    insert into contribution_archives (receipt_code_id, legacy_receipt_id, legacy_receipt_code_id, employer_amount, employee_amount, salary, grosspay, contrib_month, employee_id, employer_id, user_id, status, isactive, contribution_temp_id, created_at, updated_at, deleted_at, legacy_contribution_temp_id) select receipt_code_id, legacy_receipt_id, legacy_receipt_code_id, employer_amount, employee_amount, salary, grosspay, contrib_month, employee_id, employer_id, user_id, status, isactive, contribution_temp_id, created_at, updated_at, deleted_at, legacy_contribution_temp_id from contributions where contributions.employee_id not in (select employee_id from notification_reports);
    
    delete from contributions where contributions.employee_id not in (select employee_id from notification_reports);
     
    delete from employee_employer where employee_employer.employee_id not in (select employee_id from notification_reports);
    
    delete from employees where employees.id not in (select employee_id from notification_reports);
    
    execute 'select coalesce(max(id),0) + 1 from contribution_archives' into id;
    execute 'alter sequence contribution_archives_id_seq restart with ' || id; 
    
    execute 'select coalesce(max(id),0) + 1 from employee_archive_employer' into id;
    execute 'alter sequence employee_archive_employer_id_seq restart with ' || id; 
    
    execute 'select coalesce(max(id),0) + 1 from employee_archives' into id;
    execute 'alter sequence employee_archives_id_seq restart with ' || id; 
    
end$$;
SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
