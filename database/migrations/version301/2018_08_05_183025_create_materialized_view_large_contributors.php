<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewLargeContributors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         
      $sql = <<<SQL

      --Certificate Issuance Status
     create materialized view comp_large_contributor_certificates as select c.id, c.district_id, c.region_id, c.name, c.phone, c.telephone, c.email, r.name as region, c.tin, c.reg_no, d.contribution, d.member_count as employees,c.isonline,reissue
    from main.employers c join (select employer_id,  floor(avg(b.amount)) as contribution, floor(avg(b.member_count)) as member_count from main.receipts a join main.receipt_codes b on a.id = b.receipt_id where b.fin_code_id = 2 and a.iscancelled = 0 and a.isdishonoured = 0 and ((now()::date - a.created_at::date)/30 < (select large_contributor_month_limit from main.sysdefs)) group by a.employer_id having avg(b.amount) > (select large_contributor_amount_limit from main.sysdefs)) as d on c.id = d.employer_id left join main.regions r ON c.region_id = r.id 
    join (select employer_id,max(is_reissue) as reissue from main.employer_certificate_issues group by employer_id) as re on c.id=re.employer_id
    order by d.contribution desc;

    --Online Registartion
    create materialized view comp_large_contributor_status as     select c.id, c.district_id, c.region_id, c.name, c.phone, c.telephone, c.email, r.name as region, c.tin, c.reg_no, d.contribution, d.member_count as employees,isonline
    from main.employers c join (select employer_id,  floor(avg(b.amount)) as contribution, floor(avg(b.member_count)) as member_count from main.receipts a join main.receipt_codes b on a.id = b.receipt_id where b.fin_code_id = 2 and a.iscancelled = 0 and a.isdishonoured = 0 and ((now()::date - a.created_at::date)/30 < (select large_contributor_month_limit from main.sysdefs)) group by a.employer_id having avg(b.amount) > (select large_contributor_amount_limit from main.sysdefs)) as d on c.id = d.employer_id left join main.regions r ON c.region_id = r.id order by d.contribution desc;

   

    --Succesful transactions gepg
create materialized view successful_transactions_gepg as  SELECT r.pay_control_no AS control_no,
    r.rctno,
    b.billed_item,
    b.bill_amount,
    p.paid_amount,
    p.payer_name,
    p.payment_date,
    p.account_number,
    p.bank_name,
    p.payment_channel,
    u.name AS generatedby,
    b.mobile_number AS contact
   FROM portal.payments p
     JOIN main.receipts r ON p.control_no::text = r.pay_control_no
     JOIN portal.bills b ON p.bill_id = b.bill_no
     JOIN portal.users u ON u.id = b.user_id
  WHERE p.paid_amount IS NOT NULL and b.bill_status=3;

   --Missing contributions large employers



SQL;

        DB::unprepared($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
