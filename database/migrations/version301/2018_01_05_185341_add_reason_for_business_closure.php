<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReasonForBusinessClosure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_closures', function (Blueprint $table) {
            $table->text("reason");
        });
        Schema::table('employers', function (Blueprint $table) {
            $table->integer("employer_closure_id")->index()->nullable();
            $table->foreign('employer_closure_id')->references('id')->on('employer_closures')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
