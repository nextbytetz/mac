<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateApprovedToRecommendedIntermidiateLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("update wf_tracks set comments = 'Recommended' where wf_definition_id in (select id from wf_definitions where (wf_module_id, level) not in (select wf_module_id, max(level) from wf_definitions group by wf_module_id)) and comments = 'Approved'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
