<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsForGepgIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function (Blueprint $table) {
            $table->bigInteger("user_id")->nullable()->change();
            $table->bigInteger("currency_id")->default(1)->change();
            $table->smallInteger("source")->default("1");
        });

        DB::statement("comment on column receipts.source is 'Source of payments, 1 - Branch Payment, 2 - Electronic/Online Payments'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
