<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErrorMessageOnLegacyReceiptTemps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legacy_receipt_temps', function(Blueprint $table)
        {
            $table->text('error')->nullable();
        });
        DB::statement("comment on column legacy_receipt_temps.error is 'error message for the uploaded data'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
