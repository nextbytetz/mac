<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsrejectedOnReceiptRefundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_refunds', function (Blueprint $table) {
            $table->smallInteger("isrejected")->default(0);
        });
        DB::statement("comment on column receipt_refunds.isrejected is 'show whether the refund request has been rejected or not. 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
