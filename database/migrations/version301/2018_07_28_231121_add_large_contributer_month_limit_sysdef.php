<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLargeContributerMonthLimitSysdef extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->renameColumn("large_contributor_limit", "large_contributor_amount_limit");
            $table->smallInteger("large_contributor_month_limit")->default(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
