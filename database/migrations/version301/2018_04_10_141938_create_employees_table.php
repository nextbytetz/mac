<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_archives', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('memberno')->index();
			$table->string('firstname', 100)->index();
			$table->string('middlename', 100)->nullable()->index();
			$table->string('lastname', 100)->index();
			$table->string('email', 45)->nullable();
			$table->string('phone', 45)->nullable();
			$table->date('dob')->nullable();
			$table->decimal('basicpay', 14)->nullable();
			$table->decimal('grosspay', 14)->nullable();
			$table->string('nid', 50)->nullable();
			$table->bigInteger('identity_id')->nullable()->index();
			$table->bigInteger('job_title_id')->nullable()->index();
			$table->bigInteger('gender_id')->nullable()->index();
			$table->bigInteger('marital_status_cv_id')->nullable()->index();
			$table->bigInteger('employee_category_cv_id')->nullable()->index();
			$table->bigInteger('bank_branch_id')->nullable()->index();
			$table->bigInteger('location_type_id')->nullable()->index();
			$table->string('street', 150)->nullable();
			$table->string('road', 150)->nullable();
			$table->string('plot_no', 150)->nullable();
			$table->string('block_no', 150)->nullable();
			$table->text('surveyed_extra')->nullable();
			$table->text('unsurveyed_area')->nullable();
			$table->bigInteger('user_id')->nullable()->index();
			$table->text('department')->nullable();
			$table->string('fax', 45)->nullable();
			$table->string('accountno', 45)->nullable();
			$table->string('sex', 10)->nullable();
			$table->string('job_title', 150)->nullable();
			$table->string('emp_cate', 150)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->string('id_no', 50)->nullable()->comment('ID Number of selected identity type');
			$table->string('external_id', 50)->nullable()->comment('Specify any specific unique data for the employee');
			$table->integer('employee_status')->default(1)->comment('Flag to specify employee status; 1 => Active, 2=> Dormant, 3=> Inactive');
			$table->smallInteger('source')->nullable()->default(1)->comment('Source of registration; 1 => WCF Offices, 2 => Self Service Online');
			$table->index(['firstname','lastname']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_archives');
	}

}
