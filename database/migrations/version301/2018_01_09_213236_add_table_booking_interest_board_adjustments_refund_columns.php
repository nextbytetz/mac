<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableBookingInterestBoardAdjustmentsRefundColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('booking_interest_board_adjustments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('description', 250);
            $table->date('adjustment_date');
            $table->timestamps();

        });

        DB::statement("comment on column booking_interest_board_adjustments.adjustment_date is 'Adjustment date / cut off date of applying interest calculation'");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('booking_interest_board_adjustments');
    }
}
