<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFinCodeOnReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
update receipts set fin_code_id = 43 where bank_id = 3;
update receipts set fin_code_id = 46 where bank_id = 4 or (bank_id not in (3,4) and employer_id is not null);
SQL;
        DB::unprepared($sql);
        DB::statement("comment on column receipts.fin_code_id is 'corresponding gl code of the payment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
