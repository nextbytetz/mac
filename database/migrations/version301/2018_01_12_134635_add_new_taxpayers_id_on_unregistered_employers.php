<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTaxpayersIdOnUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unregistered_employers', function (Blueprint $table) {
            $table->integer("new_taxpayer_id")->nullable()->index();
            $table->foreign('new_taxpayer_id')->references('id')->on('new_taxpayers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
