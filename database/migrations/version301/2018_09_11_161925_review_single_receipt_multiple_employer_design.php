<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReviewSingleReceiptMultipleEmployerDesign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('booking_group_receipt_codes');
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->bigInteger("employer_id")->index()->nullable();
            $table->dropColumn("receipt_type");
            $table->smallInteger("grouppaystatus")->default(0);
            $table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column receipt_codes.grouppaystatus is 'Specify whether this contribution is included in the receipt paid for multiple employers, 0 - Not Included, 1 - Entry for the receipt paying for the group of employers, 2 - Included in the receipt paid for multiple employers'");
        DB::statement("UPDATE receipt_codes SET employer_id = receipts.employer_id FROM receipts where receipts.id = receipt_codes.receipt_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
