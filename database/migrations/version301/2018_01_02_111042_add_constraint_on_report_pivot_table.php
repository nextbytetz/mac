<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintOnReportPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_category_report', function (Blueprint $table) {
            $table->unique(['report_id', 'report_category_id']);
            $table->foreign('report_id')->references('id')->on('reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('report_category_id')->references('id')->on('report_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
