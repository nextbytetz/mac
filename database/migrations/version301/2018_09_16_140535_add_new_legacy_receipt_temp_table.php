<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewLegacyReceiptTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legacy_receipt_temps', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger("employer_id")->nullable();
            $table->date("contrib_month")->nullable();
            $table->decimal("amount", 14)->nullable();
            $table->string("rctno",100)->nullable();
            $table->date("rct_date")->nullable();
            $table->integer("user_id")->nullable();
            $table->smallInteger("currency_id")->nullable();
            $table->smallInteger("fin_code_id")->nullable();
            $table->smallInteger("iscomplete")->nullable();
            $table->smallInteger("isuploaded")->nullable();
            $table->smallInteger("haserror")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legacy_receipt_temps');
    }
}
