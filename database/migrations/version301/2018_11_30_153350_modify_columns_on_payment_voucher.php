<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsOnPaymentVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payment_vouchers', function (Blueprint $table) {
            $table->integer('bank_branch_id')->unsigned()->nullable()->change();
            $table->string('accountno', 45)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *git
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_vouchers', function (Blueprint $table) {
            $table->integer('bank_branch_id')->unsigned()->change();
            $table->string('accountno', 45)->change();
        });
    }
}
