<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSelfCountersForNotificationsOnNotificationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diseases', function (Blueprint $table) {
            $table->integer("odsno")->default(0);
        });
        Schema::table('deaths', function (Blueprint $table) {
            $table->integer("odtno")->default(0);
        });
        Schema::table('accidents', function (Blueprint $table) {
            $table->integer("oacno")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
