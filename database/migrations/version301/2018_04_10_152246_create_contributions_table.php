<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContributionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contribution_archives', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('receipt_code_id')->nullable()->index();
			$table->bigInteger('legacy_receipt_id')->nullable()->index();
			$table->bigInteger('legacy_receipt_code_id')->nullable()->index();
			$table->decimal('employer_amount', 14)->nullable();
			$table->decimal('employee_amount', 14)->nullable();
			$table->decimal('salary', 14)->nullable();
			$table->decimal('grosspay', 14)->nullable();
			$table->date('contrib_month')->nullable();
			$table->bigInteger('employee_id')->index();
			$table->bigInteger('employer_id')->nullable()->index();
			$table->bigInteger('user_id')->nullable();
			$table->smallInteger('status')->default(1);
			$table->smallInteger('isactive')->default(1);
			$table->decimal('contribution_temp_id', 10, 0)->nullable()->index();
			$table->timestamps();
			$table->softDeletes();
			$table->bigInteger('legacy_contribution_temp_id')->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contributions');
	}

}
