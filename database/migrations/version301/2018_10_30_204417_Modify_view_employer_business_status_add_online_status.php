<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyViewEmployerBusinessStatusAddOnlineStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $sql = <<<SQL
DROP MATERIALIZED VIEW comp_employer_business_status;
create materialized view comp_employer_business_status as select * from (select count(b.id) as active_public from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 36 and b.employer_status = 1 and b.approval_id = 1 and b.deleted_at is null) as o cross join (select count(b.id) as active_private from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 37 and b.employer_status = 1 and b.approval_id = 1 and b.deleted_at is null) as p CROSS JOIN (select count(b.id) as dormant_public from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 36 and b.employer_status = 2  and b.approval_id = 1 and b.deleted_at is null) as q cross join (select count(b.id) as dormant_private from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 37 and b.employer_status = 2  and b.approval_id = 1 and b.deleted_at is null) as r cross join (select count(b.id) as closed_public from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 36 and b.employer_status = 3 and b.approval_id = 1 and b.deleted_at is null) as s CROSS JOIN (select count(b.id) as closed_private from employers b join code_values v ON b.employer_category_cv_id = v.id and b.employer_category_cv_id = 37 and b.employer_status = 3 and b.approval_id = 1 and b.deleted_at is null) as t cross join (select count(b.id) as all_employers from employers b where b.approval_id = 1 and b.deleted_at is null) as u cross join (select count(*) as large_contributor from comp_large_contributor ) as v cross join (select count(distinct(employer_id)) as online_registered from portal.employer_user eu join main.employers e on eu.employer_id=e.id where e.approval_id <> 3) as w;
SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            //
    }
}
