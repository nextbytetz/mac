<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadDetailsOnBookingGroupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_groups', function(Blueprint $table)
        {
            $table->integer("manual_receipt_file_total")->default(0);
            $table->integer("manual_receipt_file_uploaded")->default(0);
            $table->text("upload_error")->nullable();
            $table->smallInteger("isuploaded")->default(0);
            $table->smallInteger("error")->default(0);

        });
        Schema::table('booking_group_employers', function(Blueprint $table)
        {
            $table->dropColumn("employer_id");
            $table->bigInteger("receipt_id")->index();
            $table->integer("receipt_file_total")->default(0);
            $table->integer("receipt_file_uploaded")->default(0);
            $table->text("upload_error")->nullable();
            $table->smallInteger("isuploaded")->default(0);
            $table->smallInteger("error")->default(0);
            $table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
