<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUnregisteredFollowupInspectionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        //
        DB::beginTransaction();


        Schema::create('unregistered_followup_inspections', function(Blueprint $table)
        {
            $table->increments('id');
            $table->text('comments', 65535)->nullable();
            $table->date('start_date')->index()->comment('date when the inspection will be started.');
            $table->date('end_date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->smallInteger('iscancelled')->default(0)->comment('set whether the inspection has been cancelled or not, 1 - inspection cancelled, 0 - inspection not cancelled.');
            $table->text('cancel_reason', 65535)->nullable()->comment('set the reason for canceling the inspection entity.');
            $table->integer('cancel_user')->unsigned()->nullable()->comment('user who have canceled the inspection.');
            $table->dateTime('cancel_date')->nullable();
            $table->smallInteger('complete_status')->nullable()->default(0)->comment('set the complete status of the inspection, 1 - inspection has been completed, 0 - inspection has not yet been completed.');
            $table->date('complete_date')->nullable();
            $table->integer('complete_user')->unsigned()->nullable();
            $table->text('delay_reasons', 65535)->nullable()->comment('any reason which have made the inspection to delay to complete');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on column unregistered_followup_inspections.iscancelled is 'set whether the inspection has been cancelled or not, 1 - inspection cancelled, 0 - inspection not cancelled.'");

        DB::statement("comment on column unregistered_followup_inspections.cancel_reason is 'set the reason for canceling the inspection entity.'");
        DB::statement("comment on column unregistered_followup_inspections.cancel_user is 'user who have canceled the inspection.'");
        DB::statement("comment on column unregistered_followup_inspections.complete_status is 'set the complete status of the inspection, 1 - inspection has been completed, 0 - inspection has not yet been completed.'");
        DB::statement("comment on column unregistered_followup_inspections.delay_reasons is 'any reason which have made the inspection to delay to complete'");



        /* unregistered followup inspection task */

        Schema::create('unregistered_followup_inspection_tasks', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('unregistered_followup_inspection_id')->unsigned()->index('unregistered_followup_inspection_id');
            $table->text('name', 65535);
            $table->text('deliverable', 65535)->nullable();
            $table->integer('employer_count')->comment('number of employers in case the inspection task was selected from a inspection profile');
            $table->string('region', 150);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on column unregistered_followup_inspection_tasks.employer_count is 'number of employers in case the inspection task was selected from a inspection profile'");

        Schema::table('unregistered_followup_inspection_tasks', function(Blueprint $table)
        {
            $table->foreign('unregistered_followup_inspection_id', 'unregistered_followup_inspection_id_foreign_ibfk')->references('id')->on('unregistered_followup_inspections')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });



        DB::commit();




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::drop('unregistered_followup_inspections');

        Schema::table('unregistered_followup_inspection_tasks', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_followup_inspection_id_foreign_ibfk');
            $table->dropIndex('unregistered_followup_inspection_id');
        });

        Schema::drop('unregistered_followup_inspection_tasks');

        DB::commit();
    }
}
