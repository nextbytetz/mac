<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobTitleAndPayInfoOnEmployeeEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_employer', function (Blueprint $table) {
            $table->decimal("basicpay", 14)->nullable();
            $table->decimal("grosspay", 14)->nullable();
            $table->bigInteger("employee_category_cv_id")->index()->nullable();
            $table->text("job_title")->nullable();
            $table->foreign('employee_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
