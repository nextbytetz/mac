<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RenameColumnLegacyContributionTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::table('legacy_contribution_temps', function(Blueprint $table)
        {

            $table->renameColumn('receipt_code_id', 'legacy_receipt_code_id')->unsigned();

        });


        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->dropForeign('booking_id_foreign_ibfk');
        });
        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->foreign('booking_id','booking_id_foreign_ibfk')->references('id')->on('bookings')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::table('legacy_contribution_temps', function(Blueprint $table)
        {

            $table->renameColumn('legacy_receipt_code_id', 'receipt_code_id')->unsigned();

        });


        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->dropForeign('booking_id_foreign_ibfk');
        });
        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->foreign('booking_id','booking_id_foreign_ibfk')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        DB::commit();
    }
}
