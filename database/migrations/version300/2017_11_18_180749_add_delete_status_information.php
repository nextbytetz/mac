<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeleteStatusInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function (Blueprint $table) {
            $table->integer("delete_status")->default(0)->nullable();
            $table->integer("deleted_by")->nullable();
            $table->date("deleted_date")->nullable();
            $table->integer("delete_auth_by")->nullable();
            $table->date("delete_auth_date")->nullable();
            $table->text("delete_reason")->nullable();
        });
        $employers = DB::connection('legacy')->table('employers')->where('delete_status', 1)->get();
        foreach ($employers as $employer) {
            $query = \App\Models\Operation\Compliance\Member\Employer::find($employer->id);
            if (count($query)) {
                $query->delete_status = 1;
                $query->deleted_by = $employer->deleted_by;
                $query->deleted_date = $employer->deleted_date;
                $query->delete_auth_by = $employer->delete_auth_by;
                $query->delete_auth_date = $employer->delete_auth_date;
                $query->delete_reason = $employer->delete_reason;
                $query->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
