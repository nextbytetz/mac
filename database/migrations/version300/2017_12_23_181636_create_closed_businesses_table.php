<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClosedBusinessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('closed_businesses', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('tin', 30)->nullable();
			$table->string('firstname')->nullable();
			$table->string('middlename')->nullable();
			$table->string('lastname')->nullable();
			$table->string('taxpayername')->nullable();
			$table->string('tradingname')->nullable();
			$table->string('numberofemployees')->nullable();
			$table->date('dateofregistration')->nullable();
			$table->date('deregistrationdate')->nullable();
			$table->string('registrationstatuscode')->nullable();
			$table->string('registrationstatusreason')->nullable();
			$table->string('plotnumber')->nullable();
			$table->string('blocknumber')->nullable();
			$table->string('street')->nullable();
			$table->string('postaladdress')->nullable();
			$table->string('postalcity')->nullable();
			$table->string('postalcode')->nullable();
			$table->string('district')->nullable();
			$table->string('region')->nullable();
			$table->string('telephone1')->nullable();
			$table->string('telephone2')->nullable();
			$table->string('mobile')->nullable();
			$table->string('email')->nullable();
			$table->string('fax')->nullable();
			$table->string('businessactivitiescode')->nullable();
			$table->string('businessactivitiesdescription')->nullable();
			$table->string('ismain')->nullable();
			$table->string('businesssectorcode')->nullable();
			$table->string('businesssectordescription')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('closed_businesses');
	}

}
