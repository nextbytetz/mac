<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameAttributeOnOnlineEmployerVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('online_employer_verifications', function (Blueprint $table) {
            $table->bigInteger("employer_id")->nullable()->change();
            $table->string("name", 200);
        });
        DB::statement("comment on column online_employer_verifications.name is 'Employer name or Employer registration number as registered at WCF Branch.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
