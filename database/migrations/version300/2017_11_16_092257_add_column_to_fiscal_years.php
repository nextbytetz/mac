<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToFiscalYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('fiscal_years', function (Blueprint $table) {
            $table->integer('user_id')->index();
        });

        Schema::table('notification_reports', function(Blueprint $table)
        {

            $table->smallInteger('need_investigation')->default(1)->comment('set whether notification needs investigation or not, 1 - needs investigation, 0 - does not need investigation.')->change();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('fiscal_years', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::table('notification_reports', function(Blueprint $table)
        {

            $table->smallInteger('need_investigation')->default(0)->comment('set whether notification needs investigation or not, 1 - needs investigation, 0 - does not need investigation.')->change();

        });
    }
}
