<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableEmployeeTemps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

       Schema::create('employee_temps', function(Blueprint $table)
        {
            $table->bigInteger('id', true)->unsigned();
            $table->string('no', 191)->nullable()->comment('Specific number of the employee described by an employer');
            $table->string('firstname', 30)->nullable();
            $table->string('middlename', 30)->nullable();
            $table->string('lastname', 30)->nullable();
            $table->date('dob')->nullable();
            $table->string('gender', 6)->nullable();
             $table->decimal('basicpay', 14)->nullable();
            $table->decimal('allowance', 14)->nullable();
            $table->integer('employer_id')->unsigned()->index();
            $table->text('error_report', 65535)->nullable()->comment('Contain the error report for this specific row of import');
            $table->smallInteger('error')->default(0)->comment('Check whether the uploaded document has error or not ..., 0 - has no error, 1 - has error');
            $table->timestamps();
        });

        DB::statement("comment on column employee_temps.error_report is 'Contain the error report for this specific row of import'");
        DB::statement("comment on column employee_temps.error is 'Check whether the uploaded document has error or not ..., 0 - has no error, 1 - has error'");

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::drop('employee_temps');

        DB::commit();
    }
}
