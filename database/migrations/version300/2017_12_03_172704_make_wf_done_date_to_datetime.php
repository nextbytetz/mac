<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeWfDoneDateToDatetime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function(Blueprint $table)
        {
            $table->dateTime('wf_done_date')->nullable()->change();
        });
        Schema::table('online_employer_verifications', function(Blueprint $table)
        {
            $table->dateTime('wf_done_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
