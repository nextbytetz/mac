<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnParentIdEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::table('employers', function(Blueprint $table)
        {
            $table->bigInteger('parent_id')->unsigned()->nullable()->index('parent_id');
        });

        DB::statement("comment on column employers.parent_id is 'Primary key of the Parent employer of this employer'");

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
             //
        DB::beginTransaction();
        Schema::table('employers', function(Blueprint $table)
        {
            $table->dropColumn('parent_id');
        });
        DB::commit();
        //
    }
}
