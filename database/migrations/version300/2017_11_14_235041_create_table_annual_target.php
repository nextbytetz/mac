<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableAnnualTarget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('fiscal_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fiscal_year',10)->index();
            $table->integer('start_year');
            $table->integer('end_year');
            $table->timestamps();

        });

        Schema::create('fiscal_year_target_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fiscal_year_id');
            $table->integer('annual_target_type_cv_id');
            $table->decimal('target_total',16);
            $table->timestamps();
        });

        DB::statement("comment on column fiscal_year_target_type.target_total is 'Total amount / count targeted for this target type.'");

        Schema::table('fiscal_year_target_type', function(Blueprint $table)
        {
            $table->foreign('fiscal_year_id', 'fiscal_year_id_foreign_ibfk1')->references('id')->on('fiscal_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('annual_target_type_cv_id', 'annual_target_type_cv_id_foreign_ibfk1')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('fiscal_year_target_type');
        Schema::drop('fiscal_years');
    }
}
