<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->smallInteger('status')->default(0)->comment('set the status whether the employer has been approved or not, 1 - Approved, 0 - Pending, 2 - Rejected');
            $table->tinyInteger('wf_done')->default(0)->comment('specify whether the employer registration workflow has been completed or not. 1 => complete, 0 => pending');
            $table->date('wf_done_date')->nullable()->comment('date which workflow was completed');
        });

        Schema::rename('document_employer_registration', 'document_employer');

        Schema::table('document_employer', function(Blueprint $table)
        {
           /* $table->dropForeign('employer_registration_foreign_id_ibfk');*/
            $table->renameColumn('employer_registration_id', 'employer_id');

                $table->foreign('employer_id','employer_foreign_id_ibfk')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->dropColumn('status');
            $table->dropColumn('wf_done');
            $table->dropColumn('wf_done_date');
        });

        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->dropColumn('employer_foreign_id_ibfk');
            $table->renameColumn('employer_id', 'employer_registration_id');

            /*$table->foreign('employer_registration_id','employer_registration_foreign_id_ibfk')->references('id')->on('employer_registrations')->onUpdate('CASCADE')->onDelete('RESTRICT');*/
        });

        Schema::rename('document_employer', 'document_employer_registration');
        DB::commit();
    }
}
