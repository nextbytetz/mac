<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatedContributionTempLegacyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        Schema::create('legacy_contribution_temps', function(Blueprint $table)
        {
            $table->bigInteger('id', true)->unsigned();
            $table->string('employeeno', 191)->nullable()->comment('Specific number of the employee described by an employer');
            $table->string('firstname', 30)->nullable();
            $table->string('middlename', 30)->nullable();
            $table->string('lastname', 30)->nullable();
            $table->decimal('basicpay', 14)->nullable();
            $table->decimal('grosspay', 14)->nullable();
            $table->date('dob')->nullable();
            $table->integer('receipt_code_id')->unsigned()->index();
            $table->text('error_report', 65535)->nullable()->comment('Contain the error report for this specific row of import');
            $table->tinyInteger('error')->default(0)->comment('Check whether the uploaded document has error or not ..., 0 - has no error, 1 - has error');
            $table->timestamps();
        });
        DB::commit();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
        Schema::drop('legacy_contribution_temps');
        DB::commit();
    }
}
