<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnPayrollProcsRundate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_procs', function(Blueprint $table)
        {
            $table->date('run_date')->nullable()->default(NULL)->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payroll_procs', function(Blueprint $table)
        {
            $table->date('run_date')->default(\Carbon\Carbon::now())->change();

        });
    }
}
