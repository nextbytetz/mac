<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTinVerificationColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('online_employer_verifications', function(Blueprint $table)
        {
            $table->smallInteger('has_tin_verified')->nullable()->default(0);
            $table->smallInteger('has_tin_existing')->nullable()->default(0);
        });
        DB::statement("comment on column online_employer_verifications.has_tin_verified is 'Check whether employer tin has been verified or not, 1 => Verified, 0 => Not Verified'");
        DB::statement("comment on column online_employer_verifications.has_tin_existing is 'Check whether employer tin is existing with another business or not, 1 => Existing, 0 => Not Existing'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
