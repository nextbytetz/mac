<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('employees', function (Blueprint $table) {
            $table->index([ 'firstname']);
            $table->index([ 'middlename']);
            $table->index([ 'lastname']);
            $table->index([ 'memberno']);
        });


        Schema::table('legacy_receipts', function (Blueprint $table) {
            $table->index(['rct_date' ,]);
            $table->index([ 'iscancelled',]);
            $table->index(['isdishonoured' ,]);
            $table->index(['contr_approval' ,]);
        });

        Schema::table('receipts', function (Blueprint $table) {
            $table->index(['rct_date',]);
            $table->index(['iscancelled' ,]);
            $table->index([ 'isdishonoured' ,]);
        });


        Schema::table('health_providers', function(Blueprint $table)
        {
            $table->index([ 'name']);
            $table->index(['external_id',]);
            $table->index(['common_name']);
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
