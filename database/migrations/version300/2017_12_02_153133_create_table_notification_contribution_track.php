<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificationContributionTrack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('notification_contribution_tracks', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('notification_report_id')->unsigned()->index();
            $table->string('contribution_month', 20);
            $table->string('comments', 500);
            $table->smallInteger('status')->default(0)->index();
            $table->integer('unit_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('from_user_id')->unsigned()->index();
            $table->date('forward_date')->nullable();
            $table->smallInteger('isread')->default(0)->index();
            $table->timestamps();

        });


        Schema::table('notification_contribution_tracks', function(Blueprint $table)
        {
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        DB::statement("comment on column notification_contribution_tracks.status is 'Status flag implies that status of track ; 0 => pending, 1 => approved'");

        DB::statement("comment on column notification_contribution_tracks.isread is 'flag implies that track  is read or not yet; 0 => not read, 1 => read'");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('notification_contribution_tracks');

    }
}
