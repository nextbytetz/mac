<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsMedicalPractitioners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('medical_practitioners', function(Blueprint $table)
        {

            $table->string('phone', 45)->nullable()->change();
            $table->string('external_id', 150)->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('medical_practitioners', function(Blueprint $table)
        {

            $table->string('phone', 45)->change();
            $table->string('external_id', 150)->change();

        });
    }

}
