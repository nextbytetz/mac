<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnsUnregisteredEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->string('tin', 100)->nullable()->comment('TRA TIN');
            $table->string('telephone', 45)->nullable();
            $table->string('fax', 45)->nullable();
            $table->integer('district_id')->unsigned()->nullable()->index('district_id');
            $table->integer('location_type_id')->unsigned()->nullable()->index('location_type_id');
            $table->text('unsurveyed_area', 65535)->nullable();
            $table->text('surveyed_extra', 65535)->nullable();
            $table->string('road', 150)->nullable();
            $table->string('block_no', 150)->nullable();
            $table->boolean('is_registered')->default(0)->comment('Flag to show whether this unregistered employer has already been registered; 0 => Not Registered, 1 => Registered');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->foreign('location_type_id','location_type_id_foreign_id')->references('id')->on('unregistered_employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id','district_id_foreign_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->dropForeign('location_type_id_foreign_id');
            $table->dropIndex('location_type_id');
            $table->dropColumn('location_type_id');
            $table->dropColumn('tin');
            $table->dropColumn('is_registered');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->dropColumn('created_at');

            $table->dropForeign('district_id_foreign_id');
            $table->dropIndex('district_id');
            $table->dropColumn('district_id');

            $table->dropColumn('telephone');
            $table->dropColumn('fax');
            $table->dropColumn('block_no');
            $table->dropColumn('unsurveyed_area');
            $table->dropColumn('surveyed_extra');
            $table->dropColumn('road');


        });

        DB::commit();
    }
}
