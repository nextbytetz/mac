<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAppstatusOnOnlineEmployerVerificationAndEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('online_employer_verifications', function (Blueprint $table) {
            $table->smallInteger("appstatus")->default(1);
        });
        DB::statement("comment on column online_employer_verifications.appstatus is 'Shows how many times the application has been applied, especially for the case application rejection for online applications'");
        Schema::table('employers', function (Blueprint $table) {
            $table->smallInteger("appstatus")->default(1);
        });
        DB::statement("comment on column online_employer_verifications.appstatus is 'Shows how many times the application has been applied, especially for the case application rejection for online applications'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
