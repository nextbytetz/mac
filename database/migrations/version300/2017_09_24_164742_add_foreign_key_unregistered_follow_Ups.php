<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyUnregisteredFollowUps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
               $table->integer('unregistered_employer_id')->unsigned()->after('date_of_follow_up')->index();
        });


        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->foreign('unregistered_employer_id','unregistered_employer_id_foreign_id')->references('id')->on('unregistered_employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_employer_id_foreign_id');
            $table->dropIndex('unregistered_employer_id');
            $table->dropColumn('unregistered_employer_id');
        });


    }
}
