<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnEmployerRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        DB::beginTransaction();

/*        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->text('business_activity', 65535)->nullable();
            $table->string('box_no', 150)->nullable();

        });*/

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        //
        DB::beginTransaction();

/*        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->dropColumn('business_activity');

            $table->dropColumn('box_no');
        });*/

        DB::commit();
    }
}
