<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOldWfTracksTableForResourceType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update for Interest Waiving
        DB::statement("update wf_tracks set resource_type = 'App\Models\Finance\Receivable\InterestWriteOff' where wf_definition_id in (select id from wf_definitions where wf_module_id in (select id from wf_modules where wf_module_group_id = 1))");
        //Update for Interest Waiving
        DB::statement("update wf_tracks set resource_type = 'App\Models\Finance\Receivable\BookingInterest' where wf_definition_id in (select id from wf_definitions where wf_module_id in (select id from wf_modules where wf_module_group_id = 2))");
        //Update for Interest Waiving
        DB::statement("update wf_tracks set resource_type = 'App\Models\Operation\Claim\NotificationReport' where wf_definition_id in (select id from wf_definitions where wf_module_id in (select id from wf_modules where wf_module_group_id in (3,4)))");
        //Update for Interest Waiving
        DB::statement("update wf_tracks set resource_type = 'App\Models\Finance\Receipt\Receipt' where wf_definition_id in (select id from wf_definitions where wf_module_id in (select id from wf_modules where wf_module_group_id = 5))");
        //Update for Interest Waiving
        DB::statement("update wf_tracks set resource_type = 'App\Models\Operation\Compliance\Member\Employer' where wf_definition_id in (select id from wf_definitions where wf_module_id in (select id from wf_modules where wf_module_group_id = 6))");
        //Update for Interest Waiving
        DB::statement("update wf_tracks set resource_type = 'App\Models\Finance\Receipt\LegacyReceipt' where wf_definition_id in (select id from wf_definitions where wf_module_id in (select id from wf_modules where wf_module_group_id = 7))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
