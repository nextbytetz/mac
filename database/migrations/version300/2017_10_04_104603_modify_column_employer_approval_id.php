<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ModifyColumnEmployerApprovalId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->integer('approval_id')->nullable()->default(0)->comment('Employer status; 0=> pending, 1=> Approved, 2=>Rejected')->change();

        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->integer('approval_id')->nullable()->default(2)->change();

        });

        DB::commit();
    }
}
