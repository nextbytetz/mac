<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentOnApprovalIdOnEmployerAndOthers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column employers.approval_id is 'Employer status; 1 => Approved, 2 => Pending, 3 => Rejected, 4 => Initiated, 5 => Pending Online'");
        Schema::table('employers', function(Blueprint $table)
        {
            $table->smallInteger('source')->nullable()->default(1);
            $table->smallInteger('approval_id')->nullable()->default(2)->change();
        });
        DB::statement("comment on column employers.source is 'Source of registration; 1 => WCF Offices, 2 => Self Service Online'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
