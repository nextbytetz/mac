<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnsLegacyReceiptCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->softDeletes();
            $table->integer('fin_code_id')->unsigned()->index('fin_code_id');
            $table->integer('booking_id')->unsigned()->nullable()->index('booking_id');
            $table->string('linked_file', 25)->nullable()->comment('Linked file attached to a receipt code i.e. contributions');

        });


        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->foreign('fin_code_id', 'fin_code_id_foreign_ibfk')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('booking_id', 'booking_id_foreign_ibfk')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->dropForeign('fin_code_id_foreign_ibfk');
            $table->dropForeign('booking_id_foreign_ibfk');

            $table->dropIndex('booking_id');
            $table->dropIndex('fin_code_id');

            $table->dropColumn('deleted_at');
            $table->dropColumn('fin_code_id');
            $table->dropColumn('linked_file');
            $table->dropColumn('booking_id');
        });

        DB::commit();
    }
}
