<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewViewGetAllUnregRegEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("create VIEW registered_unregistered_employers AS 
                SELECT employers.id,
                employers.name,
    employers.tin,
    employers.doc,
    regions.name as region_name,
        employers.approval_id as approval_id,
    1 as status
   FROM employers
  left join regions on regions.id = employers.region_id where employers.deleted_at is null
UNION ALL
                 SELECT unregistered_employers.id,
                unregistered_employers.name,
    unregistered_employers.tin,
    unregistered_employers.doc,
    regions.name as region_name,
    0 as approval_id,
    2 as status
   FROM unregistered_employers
  left join regions on regions.id = unregistered_employers.region_id where unregistered_employers.deleted_at is null
   
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
