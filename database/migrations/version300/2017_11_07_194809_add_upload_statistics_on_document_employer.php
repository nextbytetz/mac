<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadStatisticsOnDocumentEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->smallInteger('isuploaded')->nullable()->default(0);
            $table->smallInteger('error')->nullable()->default(0);
            $table->smallInteger('rows_imported')->nullable()->default(0);
            $table->smallInteger('total_rows')->nullable()->default(0);
        });
        DB::statement("comment on column document_employer.isuploaded is 'Flag to specify whether the document is uploaded or not, isuploaded shows whether the content has been effected in the target table'");
        DB::statement("comment on column document_employer.error is 'Specify whether the uploaded file has an error. For fast checking in the application'");
        DB::statement("comment on column document_employer.rows_imported is 'Rows which has already been imported'");
        DB::statement("comment on column document_employer.rows_imported is 'Total rows of the file'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
