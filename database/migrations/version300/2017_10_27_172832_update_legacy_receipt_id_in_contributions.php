<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLegacyReceiptIdInContributions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("update contributions set legacy_receipt_id = null;");
        DB::statement("update contributions set legacy_receipt_id = legacy_receipts.id from legacy_receipts where contributions.employer_id = legacy_receipts.employer_id and date_part('month', contributions.contrib_month) = date_part('month', legacy_receipts.contrib_month) and date_part('year', contributions.contrib_month) = date_part('year', legacy_receipts.contrib_month)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
