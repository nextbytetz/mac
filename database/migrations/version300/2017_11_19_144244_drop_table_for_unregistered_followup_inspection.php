<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableForUnregisteredFollowupInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::drop('unregistered_followup_inspection_task_user');
        Schema::drop('unregistered_followup_inspection_tasks');
        Schema::drop('unregistered_followup_inspection_user');
        Schema::drop('unregistered_followup_inspections');
        Schema::drop('unregistered_inspection_profiles');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
