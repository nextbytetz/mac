<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasTinVerifiedOnEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function(Blueprint $table)
        {
            $table->smallInteger('has_tin_verified')->nullable()->default(0);
        });
        DB::statement("comment on column employers.has_tin_verified is 'Check whether employer tin has been verified or not, 1 => Verified, 0 => Not Verified'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
