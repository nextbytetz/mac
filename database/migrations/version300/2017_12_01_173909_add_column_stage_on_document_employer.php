<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStageOnDocumentEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->smallInteger('stage')->nullable()->default(0);
        });
        DB::statement("comment on column document_employer.stage is 'check the uploading stage of the document 0 => Not uploaded a file, 1 => Uploading file for checking , 2 => Uploading file to the members table.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
