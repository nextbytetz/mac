<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableUnregisteredInspectionProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::create('unregistered_inspection_profiles', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 191)->unique();
            $table->text('query', 65535);
            $table->integer('user_id')->nullable()->index();
            $table->timestamps();
        });
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::drop('unregistered_inspection_profiles');

        DB::commit();
    }
}
