<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsEmployeeTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employee_temps', function(Blueprint $table)
        {
               $table->string('job_title', 150)->nullable();
            $table->string('employment_category', 150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('employee_temps', function (Blueprint $table) {
            $table->dropColumn('job_title');
            $table->dropColumn('employment_category');
        });
    }
}
