<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnDependentEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependent_employee', function(Blueprint $table)
        {

            $table->dropColumn('suspense_flag');
                 });

        Schema::table('dependents', function(Blueprint $table)
        {

            $table->smallInteger('suspense_flag')->default(0)->comment('set whether dependent is suspended or not, 1 - suspended, 0 - not suspended.');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
