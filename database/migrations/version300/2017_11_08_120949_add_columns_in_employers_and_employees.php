<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnsInEmployersAndEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employers', function (Blueprint $table) {
            $table->integer('employer_status')->default(1);
        });

        DB::statement("comment on column employers.employer_status is 'Flag to specify employer business status; 1 => Active, 2=> Dormant, 3=> Closed Business'");


        Schema::table('employees', function (Blueprint $table) {
            $table->integer('employee_status')->default(1);
        });

        DB::statement("comment on column employees.employee_status is 'Flag to specify employee status; 1 => Active, 2=> Dormant, 3=> Inactive'");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('employers', function (Blueprint $table) {
            $table->dropColumn('employer_status');
        });


        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('employee_status');
        });

    }


}