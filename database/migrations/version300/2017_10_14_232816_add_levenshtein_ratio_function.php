<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLevenshteinRatioFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared("CREATE EXTENSION IF NOT EXISTS fuzzystrmatch");

$sql = <<<SQL
CREATE OR REPLACE FUNCTION levenshtein_ratio(
    s1 character varying,
    s2 character varying)
  RETURNS smallint AS
$$
declare s1_len integer;
declare s2_len integer;
declare max_len integer; 

begin
    s1_len := LENGTH(s1);
    s2_len := LENGTH(s2); 
    IF s1_len > s2_len THEN  
      max_len := s1_len;  
    ELSE  
      max_len := s2_len;  
    END IF; 
    RETURN ROUND((1- cast(LEVENSHTEIN(s1, s2) as float) / cast(max_len as float)) * 100); 
	END; $$
  LANGUAGE plpgsql VOLATILE
  COST 100;
SQL;

        DB::unprepared($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
