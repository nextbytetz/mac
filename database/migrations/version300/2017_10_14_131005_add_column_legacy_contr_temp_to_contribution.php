<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnLegacyContrTempToContribution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::table('contributions', function(Blueprint $table)
        {
            $table->bigInteger('legacy_contribution_temp_id')->unsigned()->nullable()->index('legacy_contribution_temp_id');

        });
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::table('contributions', function(Blueprint $table)
        {
            $table->dropIndex('legacy_contribution_temp_id');
            $table->dropColumn('legacy_contribution_temp_id');

        });
        DB::commit();
    }
}
