<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsOnEmployeeTempsForVarchar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_temps', function (Blueprint $table) {
            $table->text('firstname')->change();
            $table->text('middlename')->change();
            $table->text('lastname')->change();
            $table->text('dob')->change();
            $table->text('gender')->change();
            $table->text('basicpay')->change();
            $table->text('grosspay')->change();
            $table->text('job_title')->change();
            $table->text('employment_category')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
