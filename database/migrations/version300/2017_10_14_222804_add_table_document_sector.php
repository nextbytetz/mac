<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableDocumentSector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_sector', function(Blueprint $table)
        {
            $table->smallIncrements("id");
            $table->string("reference", 20);
            $table->bigInteger("document_id");
            $table->timestamps();
            $table->foreign('reference')->references('reference')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column document_sector.reference is 'Reference from the code_values table which refers to business sectors'");
        DB::statement("comment on table document_sector is 'Table to store specific document depending on the sector'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
