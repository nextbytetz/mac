<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMonthlyEarningNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->decimal('monthly_earning', 14)->nullable();

        });


        DB::statement("comment on column notification_reports.monthly_earning is 'Total gross pay of month before or equal to of incident date'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropColumn('monthly_earning');

        });

    }
}
