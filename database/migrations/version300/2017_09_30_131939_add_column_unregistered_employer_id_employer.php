<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnUnregisteredEmployerIdEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->integer('unregistered_employer_id')->unsigned()->nullable()->index('unregistered_employer_id');
        });


        Schema::table('employers', function(Blueprint $table)
        {
            $table->foreign('unregistered_employer_id','unregistered_employer_foreign_id_ibfk')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_employer_foreign_id_ibfk');
                 $table->dropIndex('unregistered_employer_id');
            $table->dropColumn('unregistered_employer_id');
        });


        DB::commit();
    }
}
