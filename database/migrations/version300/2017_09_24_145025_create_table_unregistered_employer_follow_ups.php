<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUnregisteredEmployerFollowUps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->text('description', 65535)->comment('Description of the follow up performed');
            $table->text('feedback', 65535)->nullable()->comment('Feedback of the follow up, Deliverable achieved with this follow up.');
            $table->integer('follow_up_type_cv_id')->unsigned()->index();
            $table->string('contact', 45)->nullable()->comment('Contacts used to communicate with employer i.e email, phone no depending on follow up type');;
            $table->string('contact_person', 255)->nullable();
            $table->dateTime('date_of_follow_up')->nullable()->comment('date of follow up');
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
                   });


        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->foreign('follow_up_type_cv_id','follow_up_type_foreign_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
                  });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->dropForeign('follow_up_type_foreign_cv_id');
        });

        Schema::drop('unregistered_employer_follow_ups');
    }
}
