<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateViewLegacyEmployerContributionsMerge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $main = pg_mac_main();
        DB::statement("create VIEW legacy_merged_contributions AS 
                SELECT legacy_receipts.employer_id,
    legacy_receipts.amount,
    2 AS fin_code_id,
    legacy_receipts.contrib_month,
    legacy_receipts.rctno,
    NULL::text AS description,
    NULL::bigint AS payment_mode_cv_id,
    legacy_receipts.member_count,
    legacy_receipts.bank_id,
    legacy_receipts.rct_date,
    legacy_receipts.payment_type_id,
    legacy_receipts.iscancelled,
    legacy_receipts.isdishonoured,
    legacy_receipts.created_at
   FROM {$main}.legacy_receipts
  WHERE legacy_receipts.contr_approval = ANY (ARRAY[1::bigint, 2::bigint]) and legacy_receipts.islegacy = 1
UNION ALL
 SELECT d.employer_id,
    c.amount,
    2 AS fin_code_id,
    c.contrib_month,
    d.rctno,
    NULL::text AS description,
    NULL::bigint AS payment_mode_cv_id,
     c.member_count,
    d.bank_id,
    d.rct_date,
    d.payment_type_id,
    d.iscancelled,
    d.isdishonoured,
    c.created_at
   FROM {$main}.legacy_receipt_codes c
     JOIN {$main}.legacy_receipts d ON d.id = c.legacy_receipt_id
  WHERE d.contr_approval = ANY (ARRAY[1::bigint, 2::bigint]) and d.islegacy = 0;
   
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("drop VIEW legacy_merged_contributions ");
    }
}
