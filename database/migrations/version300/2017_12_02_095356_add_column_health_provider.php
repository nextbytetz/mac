<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHealthProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('health_providers', function(Blueprint $table)
        {
            $table->integer('district_id')->unsigned()->nullable()->change();
            $table->smallInteger('region_id')->nullable()->unsigned()->index();
            $table->index(['external_id', 'name', 'common_name']);
            $table->dropIndex('idx_16864_name');

        });


        Schema::table('health_providers', function(Blueprint $table)
        {
            $table->foreign('region_id')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('health_providers', function(Blueprint $table)
        {
            $table->dropColumn('region_id');
            $table->integer('district_id')->unsigned()->change();
            $table->dropIndex(['external_id', 'name', 'common_name']);
        });
    }
}
