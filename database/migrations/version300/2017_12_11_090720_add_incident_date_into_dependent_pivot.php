<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncidentDateIntoDependentPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->integer('notification_report_id')->nullable()->index();
        });


        /*create foreign on pivot*/
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });







        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->integer('notification_report_id')->nullable()->index();
        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });



        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->integer('employee_id')->nullable()->index();
            $table->dropColumn('bank_id');
        });

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->dropColumn('notification_report_id');
        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->dropColumn('notification_report_id');
        });

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->integer('bank_id')->nullable()->index();
            $table->dropColumn('employee_id');
        });

    }
}
