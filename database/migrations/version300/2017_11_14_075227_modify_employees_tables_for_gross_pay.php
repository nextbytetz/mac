<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEmployeesTablesForGrossPay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->renameColumn('salary', 'basicpay');
            $table->renameColumn('allowance', 'grosspay');
        });
        Schema::table('employee_temps', function (Blueprint $table) {
            $table->renameColumn('allowance', 'grosspay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
