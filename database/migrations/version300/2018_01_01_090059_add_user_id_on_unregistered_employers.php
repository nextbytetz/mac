<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdOnUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unregistered_employers', function (Blueprint $table) {
            $table->bigInteger("user_id")->nullable()->index();
        });
        DB::statement("comment on column unregistered_employers.user_id is 'User who added this employer to the list of unregistered employers'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
