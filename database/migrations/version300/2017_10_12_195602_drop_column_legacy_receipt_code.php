<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class DropColumnLegacyReceiptCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();


        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->dropForeign('booking_id_foreign_ibfk');
            $table->dropIndex('booking_id');
            $table->dropColumn('booking_id');
        });



        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
        //
        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {
            $table->integer('booking_id')->unsigned()->nullable()->index('booking_id');


        });
        Schema::table('legacy_receipt_codes', function(Blueprint $table)
        {

            $table->foreign('booking_id','booking_id_foreign_ibfk')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::commit();
    }
}
