<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::table('employees', function(Blueprint $table)
        {

            $table->string('id_no', 50)->nullable();

        });

        DB::statement("comment on column employees.id_no is 'ID Number of selected identity type'");

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::table('employees', function(Blueprint $table)
        {

            $table->dropColumn('id_no');

        });


        DB::commit();
    }
}
