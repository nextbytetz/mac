<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RenameColumnDistrict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->renameColumn('district', 'district_name');

        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->renameColumn('district_name', 'district');

        });



        DB::commit();
    }
}
