<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnIntoUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();


        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->integer('unregistered_followup_inspection_task_id')->unsigned()->nullable()->index();
        });

        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->foreign('unregistered_followup_inspection_task_id', 'unregistered_followup_inspection_task_id_foreign_ibfk')->references('id')->on('unregistered_followup_inspection_tasks')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();


        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_followup_inspection_task_id_foreign_ibfk');
            $table->dropColumn('unregistered_followup_inspection_task_id');
        });

        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->foreign('unregistered_followup_inspection_task_id', 'unregistered_followup_inspection_user_foreign_ibfk')->references('id')->on('unregistered_followup_inspection_tasks')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        DB::commit();
    }
}
