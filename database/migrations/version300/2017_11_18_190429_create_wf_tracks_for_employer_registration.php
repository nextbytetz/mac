<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateWfTracksForEmployerRegistration extends Migration
{


    private $target_db;

    public function __construct()
    {
        $this->target_db = env("DB_DATABASE");
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*  wf_track level for all employers registered*/

        DB::statement("insert into wf_tracks (user_id, status, resource_id, comments, assigned, receive_date, forward_date, wf_definition_id, user_type,resource_type) SELECT created_by,1, id, 'Approved', 1, created_at, created_at, 22, 'App\Models\Auth\User', 'App\Models\Operation\Compliance\Member\Employer' FROM employers ");


        /*  wf_track level for all employers registered approved and have level 2 user (Supervisor)*/

        DB::statement("insert into wf_tracks (user_id, status, resource_id, comments, assigned, receive_date, forward_date, wf_definition_id, user_type,resource_type) SELECT emp_approved_by, 1, id, 'Approved', 1, created_at, emp_approved_date, 23, 'App\Models\Auth\User', 'App\Models\Operation\Compliance\Member\Employer' FROM employers  WHERE emp_approved_by IS NOT NULL  and approval_id = 1");



        /*  wf_track level for all employers registered and pending level 2*/

        DB::statement("insert into wf_tracks (user_id, status, resource_id, comments, assigned, receive_date, forward_date, wf_definition_id, user_type, resource_type) SELECT  NULL, 0, id, NULL, 0, created_at, NULL, 23, NULL, 'App\Models\Operation\Compliance\Member\Employer'  FROM employers  WHERE approval_id = 2 ");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
//        delete all employer registration wf_tracks.
        DB::statement("delete from wf_tracks  where wf_definition_id = 22 or wf_definition_id = 23");
    }

}
