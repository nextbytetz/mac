<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataCleanup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
$sql = <<<SQL
do $$ declare t record;
begin
    for t IN select column_name, table_name
            from information_schema.columns
            where data_type='timestamp with time zone' and table_schema = 'main'

    loop
        execute 'alter table ' || t.table_name || ' alter column ' || t.column_name || ' type timestamp(0) without time zone';
    end loop;
end$$;
SQL;

$sql2 = <<<SQL
do $$
DECLARE
    statements CURSOR FOR
        SELECT tablename FROM pg_tables
        WHERE tableowner = 'postgres' AND schemaname = 'main' and tablename <> 'notifications';
        declare pkey varchar(50);
        declare id bigint;

BEGIN
    FOR stmt IN statements LOOP
   
    select a.attname into pkey from pg_index i join pg_attribute a on a.attrelid = 
    i.indrelid and a.attnum = any (i.indkey) where i.indrelid = quote_ident(stmt.tablename)::regclass and i.indisprimary;

    if (pkey is not null) then
        EXECUTE 'select coalesce(max(' || pkey || '), 0) + 1  from '|| quote_ident(stmt.tablename) || ';' INTO id;
        EXECUTE 'alter sequence ' || quote_ident(stmt.tablename || '_' || pkey || '_seq') || ' restart with ' || id;
    end if;
        
    END LOOP;

END;
$$
SQL;


        DB::unprepared($sql);

        DB::unprepared($sql2);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
