<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasFileErrorHasDataErrorOnDocumentEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->smallInteger('hasfileerror')->nullable()->default(0);
            $table->smallInteger('hasinputerror')->nullable()->default(0);
        });
        DB::statement("comment on column document_employer.hasfileerror is 'Show whether the uploaded file has format mistakes'");
        DB::statement("comment on column document_employer.hasinputerror is 'Show whether the uploaded file has data mistakes'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
