<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnIsactiveUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        DB::beginTransaction();


        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->smallInteger('isactive')->default(1);
        });

        DB::statement("comment on column unregistered_employers.isactive is 'Flag to specify if employer is active or not active; 1 => active, 0 => not active'");

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();


        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->dropColumn('isactive');
        });



        DB::commit();
    }
}
