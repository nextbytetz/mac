<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnsSectorNoOfEmployeesUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();


        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->text('sector', 65535)->nullable();
            $table->integer('no_of_employees')->nullable();
        });


        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->dropColumn('sector');
            $table->dropColumn('no_of_employees');
        });

        DB::commit();
    }
}
