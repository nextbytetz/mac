<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsForeignKeyFollowUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_employer_id_foreign_id');
            $table->dropColumn('unregistered_employer_id');
        });
        DB::statement("alter table unregistered_employer_follow_ups add unregistered_employer_id int not NULL /* Foreign key for unregistered employer */");

        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->integer('unregistered_employer_id')->unsigned()->index()->change();
            $table->foreign('unregistered_employer_id','unregistered_employer_id_foreign_id')->references('id')->on('unregistered_employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_employer_id_foreign_id');
            $table->dropColumn('unregistered_employer_id');
        });

        DB::statement("alter table unregistered_employer_follow_ups add unregistered_employer_id int not NULL /* Foreign key for unregistered employer */");


        Schema::table('unregistered_employer_follow_ups', function(Blueprint $table)
        {
            $table->integer('unregistered_employer_id')->unsigned()->index()->change();
            $table->foreign('unregistered_employer_id','unregistered_employer_id_foreign_id')->references('id')->on('unregistered_employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }
}
