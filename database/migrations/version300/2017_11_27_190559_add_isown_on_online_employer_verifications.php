<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsownOnOnlineEmployerVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('online_employer_verifications', function(Blueprint $table)
        {
            $table->smallInteger('isown')->nullable()->default(0);
        });
        DB::statement("comment on column online_employer_verifications.isown is 'Check whether this user own this business or not, 1 => own this business, 0 => do not own this business'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
