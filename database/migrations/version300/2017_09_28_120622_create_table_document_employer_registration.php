<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableDocumentEmployerRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::beginTransaction();
        Schema::create('document_employer_registration', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_registration_id')->unsignred()->index('employer_registration_id');
            $table->integer('document_id')->unsigned()->index('document_id');
            $table->string('name', 150)->nullable()->comment('original name of the document');
            $table->text('description', 65535)->nullable();
            $table->string('ext', 20)->nullable()->comment('file extension of the uploaded document');
            $table->float('size', 10, 0)->nullable()->comment('size of the uploaded file');
            $table->string('mime', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::table('document_employer_registration', function(Blueprint $table)
        {
            /*$table->foreign('employer_registration_id','employer_registration_foreign_id_ibfk')->references('id')->on('employer_registrations')->onUpdate('CASCADE')->onDelete('RESTRICT');*/
            $table->foreign('document_id', 'document_foreign_id_ibfk')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::commit();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
        Schema::table('document_employer_registration', function(Blueprint $table)
        {
            $table->dropForeign('employer_registration_foreign_id_ibfk');
            $table->dropForeign( 'document_foreign_id_ibfk');
        });

        Schema::drop('document_employer_registration');

        DB::commit();
    }

}
