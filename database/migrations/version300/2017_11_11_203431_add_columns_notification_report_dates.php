<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnsNotificationReportDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->date('incident_date')->nullable();
            $table->date('reporting_date')->nullable()->comment('date of reporting accident to employer');
            $table->date('receipt_date')->nullable()->comment('date of receipt of notification by employer');
        });
        DB::statement("comment on column notification_reports.incident_date is 'Date incident occurred'");
        DB::statement("comment on column notification_reports.reporting_date is 'Date of reporting incident to employer'");
        DB::statement("comment on column notification_reports.receipt_date is 'Date of receipt of notification by employer (date notified WCF)'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropColumn('incident_date');
            $table->dropColumn('reporting_date');
            $table->dropColumn('receipt_date');
        });
    }
}
