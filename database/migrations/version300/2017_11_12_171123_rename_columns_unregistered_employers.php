<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RenameColumnsUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->dropColumn('unregistered_followup_inspection_task_id');
            $table->integer('inspection_task_id')->nullable()->index();

        });
        Schema::table('unregistered_employers', function(Blueprint $table) {

            $table->foreign('inspection_task_id', 'inspection_task_id_foreign_ibfk')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
        DB::statement("comment on column unregistered_employers.inspection_task_id is 'Primary key of inspection task assigned to this employer.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->renameColumn('inspection_task_id', 'unregistered_followup_inspection_task_id');

        });
    }
}
