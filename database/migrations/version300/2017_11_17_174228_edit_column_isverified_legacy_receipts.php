<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditColumnIsverifiedLegacyReceipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('legacy_receipts', function(Blueprint $table)
        {

            $table->smallInteger('isverified')->nullable()->default(1)->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('legacy_receipts', function(Blueprint $table)
        {

            $table->smallInteger('isverified')->nullable()->default(0)->change();

        });
    }
}
