<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexesDashboardTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('inspections', function (Blueprint $table) {
            $table->index(['start_date']);
        });
        Schema::table('code_values', function (Blueprint $table) {
            $table->index(['reference']);
        });
        Schema::table('cases', function (Blueprint $table) {
            $table->index(['created_at']);
        });
        Schema::table('case_mentions', function (Blueprint $table) {
            $table->index(['created_at']);
        });
        Schema::table('case_hearings', function (Blueprint $table) {
            $table->index(['created_at']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
