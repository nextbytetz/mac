<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuccessColumnDocumentEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_employer', function(Blueprint $table)
        {
            $table->smallInteger('success')->nullable()->default(0);
        });
        DB::statement("comment on column document_employer.success is 'Flag to specify whether the document is uploaded or not, success shows whether the content has been effected in the destination table'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
