<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnRegionIdUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::table('unregistered_employers', function (Blueprint $table) {
                    $table->integer('region_id')->unsigned()->nullable()->index('region_id');
            $table->dateTime('date_registered')->nullable()->comment('Date when unregistered was registered to be active (Date employer moved from unregistered to registered)');
        });
        Schema::table('unregistered_employers', function (Blueprint $table) {
            $table->foreign('region_id', 'region_id_foreign_ibfk')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
        DB::statement("comment on column unregistered_employers.date_registered is 'Date when unregistered was marked as registered employer'");
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('unregistered_employers', function (Blueprint $table) {
            $table->dropForeign('region_id_foreign_ibfk');
            $table->dropIndex('region_id');
            $table->dropColumn('region_id');
            $table->dropColumn('date_registered');
        });


    }
}
