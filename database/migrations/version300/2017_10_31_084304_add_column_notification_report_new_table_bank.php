<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnNotificationReportNewTableBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->integer('employer_id')->unsigned()->nullable()->index('employer_id');

        });
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->foreign('employer_id', 'employer_id_foreign_ibfk')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
        DB::statement("comment on column notification_reports.employer_id is 'Employer of this employee where incident occurred.'");



        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropForeign('employer_id_foreign_ibfk');
            $table->dropIndex('employer_id');
            $table->dropColumn('employer_id');
            ;
        });


        DB::commit();
    }
}
