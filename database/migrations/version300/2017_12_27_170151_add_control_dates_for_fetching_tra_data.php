<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddControlDatesForFetchingTraData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->char("last_taxpayer_fetch_date", 8)->nullable()->default("20171101");
            $table->char("last_closed_businesses_fetch_date", 8)->nullable()->default("20171101");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
