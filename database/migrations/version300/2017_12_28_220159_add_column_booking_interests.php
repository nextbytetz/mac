<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBookingInterests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('bookings', function (Blueprint $table) {
            $table->smallInteger('is_schedule')->default(1);
        });

        DB::statement("comment on column bookings.is_schedule is 'Flag to specify if booking was generated through'");

        DB::statement("update bookings set is_schedule = 0 where  Extract('day' from created_at) <> 1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('is_schedule');
        });
    }
}
