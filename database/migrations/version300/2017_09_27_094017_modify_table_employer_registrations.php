<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ModifyTableEmployerRegistrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
/*        DB::beginTransaction();
        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->dropForeign('countries_foreign_ibfk_2');
                   $table->dropColumn('country_id');

            $table->dropForeign('regions_foreign_ibfk_3');
                     $table->dropColumn('region_id');

            $table->date('date_of_commencement')->nullable()->comment('Date of company start operation')->change();
            $table->renameColumn('date_of_commencement', 'doc')->comment('Date of company start operation');

            $table->integer('district_id')->unsigned()->nullable()->change();

            $table->smallInteger('status')->default(0)->comment('Registration status, 1 - Approved, 0 - Pending, 2- Rejected')->change();

            $table->integer('business_sector_cv_id')->unsigned()->nullable()->index('business_sector_cv_id')->comment('Business sector as stated on Authority Docs such as Licence, TRA');

        });

        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->foreign('business_sector_cv_id', 'business_sector_cv_foreign_ibfk')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });*/

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
/*        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->dropForeign('business_sector_cv_foreign_ibfk');
            $table->dropIndex('business_sector_cv_id');
            $table->dropColumn('business_sector_cv_id');

            $table->dropForeign('regions_foreign_ibfk_3');
            $table->dropIndex('region_id');
            $table->dropColumn('region_id');

            $table->date('doc')->comment('Date of company start operation')->change();
            $table->renameColumn('doc', 'date_of_commencement');

            $table->integer('district_id')->unsigned()->change();

            $table->smallInteger('status')->comment('Registration status, 1 - Approved, 0 - Pending, 2- Rejected')->change();

        });*/

        DB::commit();




    }
}
