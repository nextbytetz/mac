<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsIntoHealthProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('health_providers', function(Blueprint $table)
        {
            $table->string('common_name',75)->nullable();
            $table->string('zone',75)->nullable();
            $table->string('region',45)->nullable();
            $table->string('district',45)->nullable();
            $table->string('council',75)->nullable();
            $table->string('ward',75)->nullable();
            $table->string('street',75)->nullable();
            $table->string('facility_type',75)->nullable();
            $table->string('ownership',75)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('health_providers', function(Blueprint $table)
        {
            $table->dropColumn('common_name');
            $table->dropColumn('zone');
            $table->dropColumn('region');
            $table->dropColumn('district');
            $table->dropColumn('council');
            $table->dropColumn('ward');
            $table->dropColumn('street');
            $table->dropColumn('facility_type');
            $table->dropColumn('ownership');
        });
    }
}
