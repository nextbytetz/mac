<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddForeignKeyEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::table('employers', function(Blueprint $table)
        {
            $table->foreign('region_id', 'region_id_foreign_ibfk')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


        Schema::create('employer_size_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',45);
            $table->integer('min_range')->unsigned()->nullable();
            $table->integer('max_range')->unsigned()->nullable();
            $table->string('description',45);
            $table->timestamps();
        });

        DB::statement("comment on column employer_size_types.min_range is 'Minimum range number of employees'");
        DB::statement("comment on column employer_size_types.max_range is 'Maximum range number of employees'");


        DB::commit();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::table('employers', function(Blueprint $table)
        {
            $table->dropForeign('region_id_foreign_ibfk');

        });

        Schema::drop('employer_size_types');
        DB::commit();
    }
}
