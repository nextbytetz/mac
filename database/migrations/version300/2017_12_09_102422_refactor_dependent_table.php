<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorDependentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*drop*/
        Schema::drop('dependents');

        /*Recreate dependents table*/
        Schema::create('dependents', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('firstname', 45);
            $table->string('middlename', 45)->nullable();
            $table->string('lastname', 45);
            $table->integer('identity_id')->unsigned()->index();
            $table->string('id_no', 45);
            $table->string('address', 150)->nullable();
            $table->date('dob');
            $table->integer('gender_id')->unsigned()->index();
            $table->integer('country_id')->unsigned()->index();
            $table->integer('location_type_id')->unsigned()->default(2)->index();
            $table->text('unsurveyed_area', 65535)->nullable();
            $table->string('street', 150)->nullable();
            $table->string('road', 150)->nullable();
            $table->string('plot_no', 150)->nullable();
            $table->string('block_no', 150)->nullable();
            $table->text('surveyed_extra', 65535)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->string('fax', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->integer('bank_branch_id')->unsigned()->nullable()->index();
            $table->string('accountno', 45)->nullable();
            $table->dateTime('lastresponse')->comment('last verified date');
            $table->timestamps();
            $table->softDeletes();
        });

/* Create foreign keys of dependents*/

        Schema::table('dependents', function(Blueprint $table)
        {

            $table->foreign('identity_id')->references('id')->on('identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('gender_id')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('location_type_id')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });




        /* create dependent_employee*/

        Schema::create('dependent_employee', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('dependent_id')->unsigned()->index();
            $table->integer('employee_id')->unsigned()->index();
            $table->integer('dependent_type_id')->unsigned()->index();
                       $table->decimal('survivor_pension_percent', 5)->default(0)->nullable();
            $table->decimal('survivor_pension_amount', 15)->default(0)->nullable()->comment('amount of pension the dependent is eligible to be paid.');
            $table->decimal('survivor_gratuity_percent', 5)->default(0)->nullable();
            $table->smallInteger('survivor_gratuity_flag')->default(0)->comment('Determine whether is receiving gratuity / lump sum or not, 1 - yes receiving, 0 - not receiving ');
            $table->smallInteger('survivor_gratuity_pay_flag')->default(0)->comment('Determine whether has already been paid gratuity / lump sum or not, 1 - processed / paid, 0 - not yet paid / processed ');
            $table->smallInteger('suspense_flag')->default(0)->comment('set whether dependent is suspended or not, 1 - suspended, 0 - not suspended.');
            $table->smallInteger('survivor_pension_flag')->default(0)->comment('	Determine whether is receiving pension or not, e.g children taken care by an administrator might not be receiving pension. 1 - yes receiving, 0 - not receiving ');
            $table->decimal('funeral_grant_percent', 5)->nullable();
            $table->decimal('funeral_grant', 14)->default(0)->nullable();
            $table->smallInteger('funeral_grant_pay_flag')->default(0)->comment('set whether dependent has already been paid funeral grant or not, 1 - paid, 0 - not paid yet');
            $table->decimal('survivor_gratuity_amount', 14)->default(0)->nullable()->comment('amount of gratuity the dependent is eligible to be paid.');
            $table->decimal('dependency_percentage', 5)->nullable()->comment('Percentage of dependency of Other dependents i.e dependents other than children and spouses');

            $table->smallInteger('isactive')->default(0)->comment('specify whether the dependent is active to receive monthly pension');
            $table->smallInteger('isresponded')->default(0)->comment('specify whether the dependent has responded to the claim compensation award to confirm bank details and his/her information');
            $table->smallInteger('firstpay_flag')->default(0)->comment('specify whether the dependent has already started to receive monthly pension');
            $table->integer('parent_id')->unsigned()->nullable()->index();
            $table->timestamps();

        });


/*create foreign on pivot*/
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->foreign('dependent_type_id')->references('id')->on('dependent_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->foreign('parent_id')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('dependent_id')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
