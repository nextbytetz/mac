<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUnregisteredFollowupInspectionUserTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();


        Schema::create('unregistered_followup_inspection_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('unregistered_followup_inspection_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('unregistered_followup_inspection_user', function(Blueprint $table)
        {
            $table->foreign('unregistered_followup_inspection_id', 'unregistered_followup_inspection_user_foreign_ibfk')->references('id')->on('unregistered_followup_inspections')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id', 'user_id_foreign_ibfk')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
        /* unregistered followup inspection task */

        Schema::create('unregistered_followup_inspection_task_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('unregistered_followup_inspection_task_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
        });


        Schema::table('unregistered_followup_inspection_task_user', function(Blueprint $table)
        {
            $table->foreign('unregistered_followup_inspection_task_id', 'unregistered_followup_inspection_task_id_foreign_ibfk')->references('id')->on('unregistered_followup_inspection_tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('user_id', 'user_id_foreign_ibfk')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();


        Schema::table('unregistered_followup_inspection_user', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_followup_inspection_id_foreign_ibfk');
            $table->dropForeign('user_id_foreign_ibfk');
        });

        Schema::table('unregistered_followup_inspection_task_user', function(Blueprint $table)
        {
            $table->dropForeign('unregistered_followup_inspection_task_id_foreign_ibfk');
            $table->dropForeign('user_id_foreign_ibfk');
        });


        Schema::drop('unregistered_followup_inspection_user');
        Schema::drop('unregistered_followup_inspection_task_user');

        DB::commit();
    }
}
