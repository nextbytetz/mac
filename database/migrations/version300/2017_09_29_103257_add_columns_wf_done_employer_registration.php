<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
class AddColumnsWfDoneEmployerRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

/*        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->tinyInteger('wf_done')->default(0)->comment('specify whether the employer registration workflow has been completed or not. 1 => complete, 0 => pending');
            $table->date('wf_done_date')->nullable()->comment('date which workflow was completed');
        });*/


        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

/*        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->dropColumn('wf_done');
            $table->dropColumn('wf_done_date');
        });*/


        DB::commit();
    }
}
