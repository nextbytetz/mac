<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRegionsFromZanzibar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('regions', function(Blueprint $table)
        {
            $table->softDeletes();
        });

/* update deleted at from zanzibar regions*/
        DB::statement("update regions set deleted_at = '2017-11-21' where id = 19 or id = 20 or id = 28 or id = 29 or id = 30 or id = 31 ");

        DB::statement("update employers set region_id = 1 where region_id = 19 or region_id = 20 or region_id = 28 or region_id = 29 or region_id = 30 or region_id = 31 ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('regions', function(Blueprint $table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}
