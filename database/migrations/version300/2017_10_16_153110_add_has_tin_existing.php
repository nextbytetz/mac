<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasTinExisting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function(Blueprint $table)
        {
            $table->smallInteger('has_tin_existing')->nullable()->default(0);
        });
        DB::statement("comment on column employers.has_tin_existing is 'Check whether employer tin is existing with another business or not, 1 => Existing, 0 => Not Existing'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
