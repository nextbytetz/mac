<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftdeleteZanzibarRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /* update deleted at from zanzibar regions*/
        DB::statement(" update regions set deleted_at = '2017-12-04 16:33:07' where id = 19 or id = 20 or id = 28 or id = 29 or id = 30 or id = 31 ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
