<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnsLegacyReceipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::table('legacy_receipts', function(Blueprint $table)
        {
            $table->softDeletes();
            $table->string('payer', 200)->nullable()->comment('any other payer who has made the payments.');
            $table->text('description', 65535)->nullable()->comment('any specific description about this payment.');
            $table->string('bank_reference', 20)->nullable()->comment('to control for double entry of the same receipt.');
            $table->integer('currency_id')->unsigned()->nullable()->index();
            $table->integer('office_id')->unsigned()->default(1)->index();
            $table->integer('fin_code_id')->unsigned()->nullable()->index();
            $table->smallInteger('iscancelled')->default(0)->comment('specify whether this receipt is cancelled or not');
            $table->text('cancel_reason', 65535)->nullable()->comment('reason for cancelling receipt');
            $table->integer('cancel_user_id')->unsigned()->nullable()->comment('user who has cancelled the receipt');
            $table->dateTime('cancel_date')->nullable()->comment('date which the receipt has been canceled');
            $table->smallInteger('ssyes')->default(0)->comment('specify whether data has been exported to the finance software');
            $table->smallInteger('isdishonoured')->default(0)->comment('Specify whether the receipt has been dishonored.');
            $table->smallInteger('iscomplete')->default(0)->comment('Specify whether the receipt has reached the final stage including contribution upload');
            $table->smallInteger('isverified')->nullable()->default(0)->comment('Set whether this receipt has already been verified or not');
            $table->smallInteger('islegacy')->nullable()->default(1)->comment('Set whether this receipt has been created using new mac or not.');


        });


        Schema::table('legacy_receipts', function(Blueprint $table)
        {
            $table->foreign('currency_id', 'currency_id_foreign_ibfk')->references('id')->on('currencies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('office_id', 'office_id_foreign_ibfk')->references('id')->on('offices')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fin_code_id', 'fin_code_id_foreign_ibfk')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('legacy_receipts', function(Blueprint $table)
        {
            $table->dropForeign('currency_id_foreign_ibfk');
            $table->dropForeign('office_id_foreign_ibfk');
            $table->dropForeign('fin_code_id_foreign_ibfk');

            $table->dropIndex('currency_id');
            $table->dropIndex('office_id');
            $table->dropIndex('fin_code_id');

            $table->dropColumn('payer');
            $table->dropColumn('description');
            $table->dropColumn('bank_reference');
            $table->dropColumn('currency_id');
            $table->dropColumn('office_id');
            $table->dropColumn('fin_code_id');
            $table->dropColumn('iscancelled');
            $table->dropColumn('cancel_reason');
            $table->dropColumn('cancel_user_id');
            $table->dropColumn('cancel_date');
            $table->dropColumn('ssyes');
            $table->dropColumn('isdishonoured');
            $table->dropColumn('iscomplete');
            $table->dropColumn('isverified');
            $table->dropColumn('islegacy');
            $table->dropColumn('deleted_at');
        });

        DB::commit();
    }
}
