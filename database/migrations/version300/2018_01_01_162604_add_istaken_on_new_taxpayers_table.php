<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIstakenOnNewTaxpayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_taxpayers', function (Blueprint $table) {
            $table->bigInteger("user_id")->nullable()->index();
            $table->smallInteger("istaken")->default(0);
        });
        DB::statement("comment on column new_taxpayers.user_id is 'user who moved the entry to the list of unregistered employers'");
        DB::statement("comment on column new_taxpayers.istaken is 'specify whether the entry has already been moved to unregistered employers list or nor. 1 - Already moved, 0 - Not yet moved'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
