<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnlineEmployerVerificationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_employer_verifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("employer_id")->index();
            $table->integer("user_id")->index();
            $table->string("user_type", 150);
            $table->date("doc");
            $table->string("reg_no", 100);
            $table->string("tin", 100);
            $table->string("email", 45);
            $table->string("phone", 45);
            $table->smallInteger("status")->default(0);
            $table->timestamps();
        });

        Schema::create('online_employer_verification_document', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("online_employer_verification_id")->index();
            $table->integer("document_id")->index();
            $table->string("name", 150)->nullable();
            $table->text("description")->nullable();
            $table->string("ext", 20)->nullable();
            $table->double("size")->nullable();
            $table->string("mime", 150)->nullable();
            $table->timestamps();
        });

        Schema::table('employers', function (Blueprint $table) {
            $table->smallInteger("isonline")->default(0);
        });

        DB::statement("comment on column online_employer_verifications.status is 'set the status of the request, 0 => Pending for Approval, 1 => Approved, 2 => Rejected'");
        DB::statement("comment on column employers.isonline is 'set whether this employer is managed online or not. 0 => Not managed online, 1 => managed online'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
