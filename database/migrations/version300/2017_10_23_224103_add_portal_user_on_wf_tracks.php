<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPortalUserOnWfTracks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();


        Schema::table('wf_tracks', function(Blueprint $table)
        {
            $table->string("user_type", 150)->nullable();
            $table->smallInteger("source")->nullable()->default(1);
        });
        DB::statement("comment on column wf_tracks.source is 'Specify where the workflow track if from online application, 1 => Internal, 2=> Online via webbrowsers'");

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
