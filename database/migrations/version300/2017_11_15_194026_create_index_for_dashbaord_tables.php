<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexForDashbaordTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employers', function (Blueprint $table) {
            $table->index(['created_at']);
        });

        Schema::table('employees', function (Blueprint $table) {
            $table->index(['created_at']);
        });

        Schema::table('notification_reports', function (Blueprint $table) {
            $table->index(['receipt_date']);
        });

        Schema::table('legacy_receipts', function (Blueprint $table) {
            $table->index(['rct_date', 'iscancelled', 'isdishonoured', 'contr_approval' ,]);
        });

        Schema::table('receipts', function (Blueprint $table) {
            $table->index(['rct_date', 'iscancelled', 'isdishonoured' ,]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
