<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateCapturedLegacyReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('legacy_receipts', function (Blueprint $table) {
            $table->date("capture_date")->nullable();
        });
        DB::statement("comment on column legacy_receipts.capture_date is 'Date when receipt was manually captured.'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('legacy_receipts', function (Blueprint $table) {
            $table->dropColumn("capture_date");
        });
    }
}
