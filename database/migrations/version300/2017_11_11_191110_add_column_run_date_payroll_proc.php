<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnRunDatePayrollProc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_procs', function(Blueprint $table)
        {
            $table->date('run_date')->default(\Carbon\Carbon::now());
        });

        DB::statement("comment on column payroll_procs.run_date is 'Date when payroll was run (Batch date e.g Monthly pension month)'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payroll_procs', function(Blueprint $table)
        {
            $table->dropColumn('run_date');
        });

    }
}
