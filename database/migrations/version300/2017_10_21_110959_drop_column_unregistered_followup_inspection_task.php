<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class DropColumnUnregisteredFollowupInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();
        Schema::table('unregistered_followup_inspection_tasks', function (Blueprint $table) {
            $table->dropColumn('region');
            $table->dropColumn('employer_count');
            $table->integer('unregistered_inspection_profile_id')->unsigned()->nullable()->index('unregistered_inspection_profile_id');
            $table->smallInteger('target_employer')->nullable()->comment('Check the target employers concerned in this inspection task, 1 - for Inspection Profile, 0 - Individual Employers, 2 - None, employers who are not registered to WCF by the time of this task where selected');
        });
        DB::statement("comment on column unregistered_followup_inspection_tasks.target_employer is 'Check the target employers concerned in this inspection task, 1 - for Inspection Profile, 0 - Individual Employers, 2 - None, employers who are not registered to WCF by the time of this task where selected'");

        Schema::table('unregistered_followup_inspection_tasks', function (Blueprint $table) {
            $table->foreign('unregistered_inspection_profile_id', 'unregistered_inspection_profile_id_foreign_ibfk')->references('id')->on('unregistered_inspection_profiles')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::table('unregistered_followup_inspection_tasks', function (Blueprint $table) {
            $table->dropForeign('unregistered_inspection_profile_id_foreign_ibfk');
            $table->dropIndex('unregistered_inspection_profile_id');
            $table->dropColumn('unregistered_inspection_profile_id');
            $table->dropColumn('target_employer');
            $table->string('region', 150)->nullable();
            $table->integer('employer_count')->nullable()->comment('number of employers in case the inspection task was selected from a inspection profile');
        });
        DB::commit();

    }
}