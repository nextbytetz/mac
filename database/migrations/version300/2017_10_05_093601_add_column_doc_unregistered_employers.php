<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnDocUnregisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        DB::beginTransaction();

        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->date('doc')->nullable()->comment('Date of company start operation');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();

        Schema::table('unregistered_employers', function(Blueprint $table)
        {
            $table->dropColumn('doc');
        });

        DB::commit();
    }
}
