<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class DropColumnEmployerStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->dropColumn('status');

        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        //
        DB::beginTransaction();

        Schema::table('employers', function(Blueprint $table)
        {
            $table->tinyInteger('status')->comment('Registration status, 1 - Approved, 0 - Pending, 2- Rejected');

        });

        DB::commit();
    }
}
