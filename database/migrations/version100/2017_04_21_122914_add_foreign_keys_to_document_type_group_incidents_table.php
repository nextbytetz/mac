<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentTypeGroupIncidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_type_group_incidents', function(Blueprint $table)
		{
			$table->foreign('document_type_group_id', 'document_type_group_incidents_ibfk_1')->references('id')->on('document_type_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('incident_type_id', 'document_type_group_incidents_ibfk_2')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_type_group_incidents', function(Blueprint $table)
		{
			$table->dropForeign('document_type_group_incidents_ibfk_1');
			$table->dropForeign('document_type_group_incidents_ibfk_2');
		});
	}

}
