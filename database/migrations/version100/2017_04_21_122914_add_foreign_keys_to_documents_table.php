<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'documents_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('document_type_id', 'documents_ibfk_2')->references('id')->on('document_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropForeign('documents_ibfk_1');
			$table->dropForeign('documents_ibfk_2');
		});
	}

}
