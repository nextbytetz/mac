<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmploymentHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employment_histories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('member_id')->unsigned()->index('member_id');
			$table->integer('job_title_id')->unsigned()->index('job_title_id');
			$table->string('department', 150)->nullable()->comment('department which user was working during working time');
			$table->text('activity', 65535)->nullable()->comment('activity performed during the time at work');
			$table->date('from_date');
			$table->date('to_date');
			$table->integer('employer_id')->unsigned()->nullable();
			$table->string('other_employer', 150)->comment('in case employer mentioned is not registered at WCF, must be filled manually');
			$table->integer('insurance_id')->unsigned()->nullable()->index('insurance_id');
			$table->string('other_insurance', 150)->nullable();
			$table->timestamps();
			$table->integer('deleted_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employment_histories');
	}

}
