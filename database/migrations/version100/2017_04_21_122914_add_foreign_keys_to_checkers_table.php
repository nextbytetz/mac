<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCheckersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('checkers', function(Blueprint $table)
		{
			$table->foreign('audit_entity_id', 'checkers_ibfk_1')->references('id')->on('audit_entities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('audit_action_id', 'checkers_ibfk_2')->references('id')->on('audit_actions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('audit_reference_id', 'checkers_ibfk_3')->references('id')->on('audit_references')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('checkers', function(Blueprint $table)
		{
			$table->dropForeign('checkers_ibfk_1');
			$table->dropForeign('checkers_ibfk_2');
			$table->dropForeign('checkers_ibfk_3');
		});
	}

}
