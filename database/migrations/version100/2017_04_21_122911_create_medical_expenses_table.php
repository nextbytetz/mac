<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicalExpensesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medical_expenses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_id')->unsigned()->index('notification_id');
			$table->integer('medical_expense_type_id')->unsigned()->index('medical_expense_type_id');
			$table->integer('employer_id')->unsigned()->nullable()->index('employer_id');
			$table->integer('member_id')->unsigned()->nullable()->index('member_id');
			$table->integer('insurance_id')->unsigned()->nullable()->index('insurance_id');
			$table->decimal('amount', 14);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medical_expenses');
	}

}
