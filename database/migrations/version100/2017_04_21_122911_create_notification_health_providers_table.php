<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationHealthProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification_health_providers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_id')->unsigned()->index('notification_id');
			$table->integer('health_provider_id')->unsigned()->index('health_provider_id');
			$table->integer('rank')->comment('beginning with 1, the first health provider to the rest, increment in numbers');
			$table->date('attend_date')->comment('date which a member was attended to the health provider');
			$table->date('dismiss_date')->comment('date which a member was dismissed from the health provider');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification_health_providers');
	}

}
