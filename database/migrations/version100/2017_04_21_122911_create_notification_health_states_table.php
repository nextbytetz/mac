<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationHealthStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification_health_states', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_id')->unsigned()->index('notification_id');
			$table->integer('health_state_checklist_id')->unsigned()->index('health_state_checklist_id');
			$table->dateTime('from_date')->comment('date from which the member has had the health state outlined.');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification_health_states');
	}

}
