<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBankBranchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bank_branches', function(Blueprint $table)
		{
			$table->foreign('bank_id', 'bank_branches_ibfk_1')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bank_branches', function(Blueprint $table)
		{
			$table->dropForeign('bank_branches_ibfk_1');
		});
	}

}
