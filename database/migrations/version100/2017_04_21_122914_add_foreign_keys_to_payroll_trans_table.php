<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPayrollTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payroll_trans', function(Blueprint $table)
		{
			$table->foreign('payroll_proc_id', 'payroll_trans_ibfk_1')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('claim_id', 'payroll_trans_ibfk_2')->references('id')->on('claims')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('insurance_id', 'payroll_trans_ibfk_3')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('benefit_type_id', 'payroll_trans_ibfk_4')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payroll_trans', function(Blueprint $table)
		{
			$table->dropForeign('payroll_trans_ibfk_1');
			$table->dropForeign('payroll_trans_ibfk_2');
			$table->dropForeign('payroll_trans_ibfk_3');
			$table->dropForeign('payroll_trans_ibfk_4');
		});
	}

}
