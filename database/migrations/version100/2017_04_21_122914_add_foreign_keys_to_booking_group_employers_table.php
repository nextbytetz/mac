<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBookingGroupEmployersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking_group_employers', function(Blueprint $table)
		{
			$table->foreign('booking_group_id', 'booking_group_employers_ibfk_1')->references('id')->on('booking_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking_group_employers', function(Blueprint $table)
		{
			$table->dropForeign('booking_group_employers_ibfk_1');
		});
	}

}
