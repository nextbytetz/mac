<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployerVerTwosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employer_ver_twos', function(Blueprint $table)
		{
			$table->integer('employer_id')->unsigned()->unique('employer_id');
			$table->integer('location_type_id')->unsigned()->index('location_type_id');
			$table->text('unsurveyed_area', 65535)->nullable();
			$table->text('surveyed_extra', 65535)->nullable();
			$table->string('road', 150)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employer_ver_twos');
	}

}
