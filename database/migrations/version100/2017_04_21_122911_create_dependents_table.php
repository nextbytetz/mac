<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDependentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dependents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('member_id')->unsigned()->index('member_id');
			$table->string('firstname', 45);
			$table->string('middlename', 45)->nullable();
			$table->string('lastname', 45);
			$table->integer('dependent_type_id')->unsigned()->index('dependent_type_id');
			$table->integer('identity_id')->unsigned()->index('identity_id');
			$table->string('address', 150)->nullable();
			$table->date('dob');
			$table->integer('gender_id')->unsigned()->index('gender_id');
			$table->integer('country_id')->unsigned()->index('country_id');
			$table->integer('location_type_id')->unsigned()->default(2)->index('location_type_id');
			$table->text('unsurveyed_area', 65535)->nullable();
			$table->string('street', 150)->nullable();
			$table->string('road', 150)->nullable();
			$table->string('plot_no', 150)->nullable();
			$table->string('block_no', 150)->nullable();
			$table->text('surveyed_extra', 65535)->nullable();
			$table->string('phone', 45)->nullable();
			$table->string('telephone', 45)->nullable();
			$table->string('fax', 45)->nullable();
			$table->string('email', 45)->nullable();
			$table->integer('bank_id')->unsigned()->nullable()->index('bank_id');
			$table->string('accountno', 45)->nullable();
			$table->integer('bank_branch_id')->unsigned()->nullable()->index('bank_branch_id');
			$table->integer('parent_id')->unsigned()->index('parent_id');
			$table->decimal('survivor_pension_percent', 4)->nullable();
			$table->decimal('survivor_gratuity_percent', 4)->nullable();
			$table->boolean('suspense_flag')->default(0)->comment('set whether dependent is suspended or not, 1 - suspended, 0 - not suspended.');
			$table->boolean('survivor_pension_flag')->default(1)->comment('determine whether is receiving pension or not, e.g children taken care by an administrator might not be receiving pension. 1 - yes receiving, 0 - not receiving');
			$table->decimal('funeral_grant_percent', 4)->nullable();
			$table->decimal('funeral_grant', 14)->nullable();
			$table->boolean('funeral_grant_flag')->default(0)->comment('set whether dependent is receiving funeral grant or not, 1 - receiving, 0 - not receiving');
			$table->decimal('survivor_gratuity_amount', 14)->nullable()->comment('amount of gratuity the dependent is eligible to be paid.');
			$table->dateTime('lastresponse')->comment('last verified date');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dependents');
	}

}
