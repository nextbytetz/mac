<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 150)->unique('name');
			$table->integer('document_type_group_id')->unsigned()->index('document_type_group_id');
			$table->text('description', 65535)->nullable();
			$table->boolean('isrecurring')->nullable()->default(0)->comment('set whether this document type is recurring for each incident or not.');
			$table->boolean('ismandatory')->nullable()->default(1)->comment('set whether the document is mandatory or not, 1 - mandatory, 0 - not mandatory');
			$table->boolean('isactive')->nullable()->default(1)->comment('set whether the document is active in use or not, 1 - active, 0 - not active.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_types');
	}

}
