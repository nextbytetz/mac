<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMemberOldCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('member_old_compensations', function(Blueprint $table)
		{
			$table->foreign('incident_type_id', 'member_old_compensations_ibfk_1')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('member_old_compensations', function(Blueprint $table)
		{
			$table->dropForeign('member_old_compensations_ibfk_1');
		});
	}

}
