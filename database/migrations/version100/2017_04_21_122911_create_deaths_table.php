<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeathsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deaths', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_id')->unsigned()->index('notification_id');
			$table->integer('death_cause_id')->unsigned()->index('death_cause_id');
			$table->date('death_date');
			$table->text('death_place', 65535);
			$table->string('certificate_number', 50)->comment('death certificate number');
			$table->integer('district_id')->unsigned()->index('district_id');
			$table->date('reporting_date')->comment('date of reporting death to employer');
			$table->date('receipt_date')->comment('date of receipt of death by employer');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deaths');
	}

}
