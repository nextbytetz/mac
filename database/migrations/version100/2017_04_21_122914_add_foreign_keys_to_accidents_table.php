<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAccidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accidents', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'accidents_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('accident_type_id', 'accidents_ibfk_2')->references('id')->on('accident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accidents', function(Blueprint $table)
		{
			$table->dropForeign('accidents_ibfk_1');
			$table->dropForeign('accidents_ibfk_2');
		});
	}

}
