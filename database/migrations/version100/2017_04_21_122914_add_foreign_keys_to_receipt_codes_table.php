<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReceiptCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receipt_codes', function(Blueprint $table)
		{
			$table->foreign('receipt_id', 'receipt_codes_ibfk_1')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fin_code_id', 'receipt_codes_ibfk_2')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('booking_id', 'receipt_codes_ibfk_3')->references('id')->on('bookings')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receipt_codes', function(Blueprint $table)
		{
			$table->dropForeign('receipt_codes_ibfk_1');
			$table->dropForeign('receipt_codes_ibfk_2');
			$table->dropForeign('receipt_codes_ibfk_3');
		});
	}

}
