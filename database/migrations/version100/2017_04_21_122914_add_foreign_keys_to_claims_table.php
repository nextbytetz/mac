<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClaimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('claims', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'claims_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('incident_type_id', 'claims_ibfk_2')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('exit_code_id', 'claims_ibfk_3')->references('id')->on('exit_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('claims', function(Blueprint $table)
		{
			$table->dropForeign('claims_ibfk_1');
			$table->dropForeign('claims_ibfk_2');
			$table->dropForeign('claims_ibfk_3');
		});
	}

}
