<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFinCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fin_codes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->unique('name');
			$table->string('gl_code', 10)->nullable()->comment('General ledger code number.');
			$table->string('dscription', 200)->nullable()->comment('extra description about finance code.');
			$table->integer('fin_code_group_id')->unsigned()->index('fin_code_group_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fin_codes');
	}

}
