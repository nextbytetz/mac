<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentTypeGroupIncidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_type_group_incidents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('document_type_group_id')->unsigned()->index('document_type_group_id');
			$table->integer('incident_type_id')->unsigned()->index('incident_type_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_type_group_incidents');
	}

}
