<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bookings', function(Blueprint $table)
		{
			$table->foreign('fin_code_id', 'bookings_ibfk_1')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('booking_group_id', 'bookings_ibfk_2')->references('id')->on('booking_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bookings', function(Blueprint $table)
		{
			$table->dropForeign('bookings_ibfk_1');
			$table->dropForeign('bookings_ibfk_2');
		});
	}

}
