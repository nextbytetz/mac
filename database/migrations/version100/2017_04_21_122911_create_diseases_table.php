<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDiseasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('diseases', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_id')->unsigned()->index('notification_id');
			$table->date('diagnosis_date');
			$table->string('name', 150)->comment('name of the occupational disease diagnosed.');
			$table->date('reporting_date')->comment('date of reporting disease to employer');
			$table->date('receipt_date')->comment('date of receipt of disease notification by employer');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('diseases');
	}

}
