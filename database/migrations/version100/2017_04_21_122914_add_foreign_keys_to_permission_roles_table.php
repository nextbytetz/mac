<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPermissionRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permission_roles', function(Blueprint $table)
		{
			$table->foreign('permission_id', 'permission_roles_ibfk_1')->references('id')->on('permissions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('role_id', 'permission_roles_ibfk_2')->references('id')->on('roles')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permission_roles', function(Blueprint $table)
		{
			$table->dropForeign('permission_roles_ibfk_1');
			$table->dropForeign('permission_roles_ibfk_2');
		});
	}

}
