<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationHealthProviderPractitionersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification_health_provider_practitioners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_health_provider_id')->unsigned()->index('notification_health_provider_id');
			$table->integer('medical_practitoner_id')->unsigned()->index('medical_practitoner_id')->comment('name of the medical practitioner who diagnosed the incident');
			$table->date('from_date');
			$table->date('to_date');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification_health_provider_practitioners');
	}

}
