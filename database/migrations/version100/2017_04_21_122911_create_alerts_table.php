<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alerts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('text', 65535)->comment('message about the alert to be displayed to the user.');
			$table->string('table_name', 50)->comment('table name which store the exact information about the alert. Table name should match the exact corresponding eloquent class name.');
			$table->integer('resource_id')->comment('primary key of the table which store enough information about the alert');
			$table->integer('alert_type_id')->unsigned()->index('alert_type_id');
			$table->timestamps();
			$table->dateTime('deleted_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alerts');
	}

}
