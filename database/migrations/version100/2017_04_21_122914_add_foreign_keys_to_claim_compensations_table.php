<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClaimCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('claim_compensations', function(Blueprint $table)
		{
			$table->foreign('claim_id', 'claim_compensations_ibfk_1')->references('id')->on('claims')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('insurance_id', 'claim_compensations_ibfk_2')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('benefit_type_id', 'claim_compensations_ibfk_3')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('claim_compensations', function(Blueprint $table)
		{
			$table->dropForeign('claim_compensations_ibfk_1');
			$table->dropForeign('claim_compensations_ibfk_2');
			$table->dropForeign('claim_compensations_ibfk_3');
		});
	}

}
