<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationHealthProviderServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_health_provider_services', function(Blueprint $table)
		{
			$table->foreign('notification_health_provider_id', 'notification_health_provider_services_ibfk_1')->references('id')->on('notification_health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('health_service_checklist_id', 'notification_health_provider_services_ibfk_2')->references('id')->on('health_service_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_health_provider_services', function(Blueprint $table)
		{
			$table->dropForeign('notification_health_provider_services_ibfk_1');
			$table->dropForeign('notification_health_provider_services_ibfk_2');
		});
	}

}
