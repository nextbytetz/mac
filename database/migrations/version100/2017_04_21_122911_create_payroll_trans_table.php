<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePayrollTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payroll_trans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id');
			$table->integer('claim_id')->unsigned()->index('claim_id');
			$table->integer('employer_id')->unsigned()->nullable()->index('employer_id');
			$table->integer('insurance_id')->unsigned()->nullable()->index('insurance_id');
			$table->decimal('amount', 14);
			$table->text('description', 65535)->nullable();
			$table->integer('benefit_type_id')->unsigned()->index('benefit_type_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payroll_trans');
	}

}
