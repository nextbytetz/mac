<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserVerTwosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_ver_twos', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned();
			$table->string('email', 45)->nullable();
			$table->string('phone', 45)->nullable();
			$table->integer('unit_id')->unsigned()->nullable()->index('unit_id');
			$table->integer('gender_id')->unsigned()->nullable()->index('gender_id');
			$table->string('external_id', 20)->nullable()->comment('any unique reference used to refer for this user');
			$table->boolean('active')->default(1)->comment('set whether a user is active or not, 1 - active, 0 - not active');
			$table->boolean('available')->default(1)->comment('set whether a user is available in an office or not, 1 available, 0 not available');
			$table->dateTime('last_login')->nullable();
			$table->dateTime('remember_token')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->unique(['user_id','email','phone','external_id'], 'user_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_ver_twos');
	}

}
