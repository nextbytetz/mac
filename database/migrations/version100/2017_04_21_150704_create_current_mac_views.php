<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrentMacViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create VIEW employer_ver_ones AS select * from wcf.employers");
        DB::statement("create VIEW member_ver_ones AS select * from wcf.members");
        DB::statement("create VIEW user_ver_ones AS select * from wcf.users");
        DB::statement("create VIEW employers AS select * from (employer_ver_ones a left join employer_ver_twos b on((a.id = b.employer_id)))");
        DB::statement("create VIEW members AS select * from (member_ver_ones a left join member_ver_twos b on((a.member_id = b.id)))");
        DB::statement("create VIEW users AS select * from (user_ver_ones a left join user_ver_twos b on((a.id = b.user_id)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
