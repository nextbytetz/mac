<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationDutiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_duties', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'notification_duties_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('duty_type_id', 'notification_duties_ibfk_2')->references('id')->on('duty_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_duties', function(Blueprint $table)
		{
			$table->dropForeign('notification_duties_ibfk_1');
			$table->dropForeign('notification_duties_ibfk_2');
		});
	}

}
