<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDeathsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('deaths', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'deaths_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('death_cause_id', 'deaths_ibfk_2')->references('id')->on('death_causes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('district_id', 'deaths_ibfk_3')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('deaths', function(Blueprint $table)
		{
			$table->dropForeign('deaths_ibfk_1');
			$table->dropForeign('deaths_ibfk_2');
			$table->dropForeign('deaths_ibfk_3');
		});
	}

}
