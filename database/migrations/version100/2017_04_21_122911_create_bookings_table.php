<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('rcv_date')->comment('Receivable date, date which the receivable is to be collected');
			$table->integer('employer_id')->unsigned()->nullable()->index('employer_id')->comment('in case that this booking is for employer');
			$table->integer('fin_code_id')->unsigned()->index('fin_code_id');
			$table->decimal('amount', 14)->comment('amount which has been booked');
			$table->boolean('ssyes')->default(0)->comment('determine whether booking entry has been exported to finance software');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->integer('booking_group_id')->unsigned()->nullable()->index('booking_group_id')->comment('in case that this booking is for a group');
			$table->integer('member_count')->comment('total number of members who are expected to be paid in receivable.');
			$table->text('description', 65535)->nullable();
			$table->boolean('iscomplete')->default(0)->comment('Set whether the booking is complete or not.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
	}

}
