<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permissions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('permission_group_id')->unsigned()->index('permission_group_id');
			$table->string('name', 100)->unique('name');
			$table->string('display_name', 100);
			$table->text('description', 65535)->nullable();
			$table->boolean('ischecker')->default(0)->comment('determine whether this permission is for checker, needs a second person approval.');
			$table->integer('checker_parent')->unsigned()->nullable()->index('checker_parent')->comment('parent permission for this maker checker');
			$table->boolean('can_maker_checker')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permissions');
	}

}
