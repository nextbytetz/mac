<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingGroupEmployersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_group_employers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('booking_group_id')->unsigned()->index('booking_group_id');
			$table->integer('employer_id')->unsigned()->index('employer_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_group_employers');
	}

}
