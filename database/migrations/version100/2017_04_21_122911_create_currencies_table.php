<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currencies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->string('code', 5)->comment('Short name for the defined currency');
			$table->decimal('exchange_rate', 5)->nullable()->comment('exchange rate to convert the specified in Tanzanian Shillings');
			$table->timestamps();
			$table->softDeletes();
			$table->unique(['name','code'], 'name');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currencies');
	}

}
