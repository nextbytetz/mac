<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestigationFeedbacksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investigation_feedbacks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('incident_id')->unsigned();
			$table->integer('investgation_question_id')->unsigned();
			$table->string('feedback', 20);
			$table->timestamps();
			$table->dateTime('deleted_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investigation_feedbacks');
	}

}
