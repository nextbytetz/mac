<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMemberOldCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('member_old_compensations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('member_id')->unsigned()->index('member_id');
			$table->integer('incident_type_id')->unsigned()->index('incident_type_id');
			$table->string('who_paid', 150)->comment('who paid compensation');
			$table->date('payment_date')->nullable();
			$table->decimal('amount', 14)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('member_old_compensations');
	}

}
