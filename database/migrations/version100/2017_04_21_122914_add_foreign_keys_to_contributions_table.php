<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contributions', function(Blueprint $table)
		{
			$table->foreign('receipt_code_id', 'contributions_ibfk_1')->references('id')->on('receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contributions', function(Blueprint $table)
		{
			$table->dropForeign('contributions_ibfk_1');
		});
	}

}
