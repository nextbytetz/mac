<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationHealthStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_health_states', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'notification_health_states_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('health_state_checklist_id', 'notification_health_states_ibfk_2')->references('id')->on('health_service_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_health_states', function(Blueprint $table)
		{
			$table->dropForeign('notification_health_states_ibfk_1');
			$table->dropForeign('notification_health_states_ibfk_2');
		});
	}

}
