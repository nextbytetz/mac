<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receipts', function(Blueprint $table)
		{
			$table->foreign('payment_type_id', 'receipts_ibfk_2')->references('id')->on('payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('office_id', 'receipts_ibfk_3')->references('id')->on('offices')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_id', 'receipts_ibfk_4')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('currency_id', 'receipts_ibfk_5')->references('id')->on('currencies')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receipts', function(Blueprint $table)
		{
			$table->dropForeign('receipts_ibfk_2');
			$table->dropForeign('receipts_ibfk_3');
			$table->dropForeign('receipts_ibfk_4');
			$table->dropForeign('receipts_ibfk_5');
		});
	}

}
