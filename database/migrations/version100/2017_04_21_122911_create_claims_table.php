<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claims', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_id')->unsigned()->index('notification_id');
			$table->integer('incident_type_id')->unsigned()->index('incident_type_id');
			$table->decimal('monthly_earning', 14)->comment('recent salary amount.');
			$table->boolean('day_hours')->comment('hours worked per day by a member');
			$table->integer('exit_code_id')->unsigned()->nullable()->index('exit_code_id');
			$table->boolean('iscomplete')->default(0)->comment('set whether is complete or not, 1 - claim process is complete, 0 - claim process is not complete');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claims');
	}

}
