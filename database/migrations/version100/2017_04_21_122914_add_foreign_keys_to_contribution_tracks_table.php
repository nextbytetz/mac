<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributionTracksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contribution_tracks', function(Blueprint $table)
		{
			$table->foreign('receipt_code_id', 'contribution_tracks_ibfk_1')->references('id')->on('receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contribution_tracks', function(Blueprint $table)
		{
			$table->dropForeign('contribution_tracks_ibfk_1');
		});
	}

}
