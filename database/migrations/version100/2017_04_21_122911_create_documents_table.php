<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_id')->unsigned()->index('notification_id');
			$table->integer('document_type_id')->unsigned()->index('document_type_id');
			$table->string('name', 150)->comment('original name of the document');
			$table->text('description', 65535)->nullable();
			$table->string('ext', 20)->comment('file extension of the uploaded document');
			$table->float('size', 10, 0)->comment('size of the uploaded file');
			$table->string('mime', 150);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}

}
