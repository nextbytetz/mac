<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccidentWitnessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accident_witnesses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('accident_id')->unsigned()->index('accident_id');
			$table->integer('witness_id')->unsigned()->index('witness_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->timestamps();
			$table->dateTime('deleted_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accident_witnesses');
	}

}
