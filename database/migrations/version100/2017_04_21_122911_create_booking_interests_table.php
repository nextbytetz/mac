<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingInterestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_interests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('booking_id')->unsigned()->index('booking_id');
			$table->date('miss_month')->comment('missing month which the interest is being charged.');
			$table->integer('late_monthes')->comment('number of months which the contributions has been delayed.');
			$table->decimal('interest_percent', 4)->comment('interest percent charged for this penalty');
			$table->decimal('amount', 14);
			$table->boolean('iswaived')->default(0)->comment('set whether this penalty is waived or not, 1 - waived, 0 - not waived.');
			$table->boolean('ispaid')->default(0)->comment('specify whether the interest amount has been paid or not.');
			$table->decimal('waive_amount', 14)->comment('Specify amount which has been waived.');
			$table->text('waive_note', 65535)->comment('Note on the waived amount');
			$table->integer('waive_user')->comment('staff who has waived the interest amount.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_interests');
	}

}
