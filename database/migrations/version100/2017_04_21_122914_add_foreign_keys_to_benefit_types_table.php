<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBenefitTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('benefit_types', function(Blueprint $table)
		{
			$table->foreign('benefit_type_group_id', 'benefit_types_ibfk_1')->references('id')->on('benefit_type_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('benefit_types', function(Blueprint $table)
		{
			$table->dropForeign('benefit_types_ibfk_1');
		});
	}

}
