<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationHealthProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_health_providers', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'notification_health_providers_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('health_provider_id', 'notification_health_providers_ibfk_2')->references('id')->on('health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_health_providers', function(Blueprint $table)
		{
			$table->dropForeign('notification_health_providers_ibfk_1');
			$table->dropForeign('notification_health_providers_ibfk_2');
		});
	}

}
