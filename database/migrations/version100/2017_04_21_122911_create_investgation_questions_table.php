<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestgationQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investgation_questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name', 65535)->comment('question itself');
			$table->string('data_type', 20)->comment('data_type of the question, whether Boolean, date, text, etc');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investgation_questions');
	}

}
