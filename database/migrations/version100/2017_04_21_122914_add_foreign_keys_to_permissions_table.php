<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permissions', function(Blueprint $table)
		{
			$table->foreign('permission_group_id', 'permissions_ibfk_1')->references('id')->on('permission_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('checker_parent', 'permissions_ibfk_2')->references('id')->on('permissions')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permissions', function(Blueprint $table)
		{
			$table->dropForeign('permissions_ibfk_1');
			$table->dropForeign('permissions_ibfk_2');
		});
	}

}
