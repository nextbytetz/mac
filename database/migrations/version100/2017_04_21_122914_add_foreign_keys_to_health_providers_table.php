<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHealthProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('health_providers', function(Blueprint $table)
		{
			$table->foreign('district_id', 'health_providers_ibfk_1')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('health_providers', function(Blueprint $table)
		{
			$table->dropForeign('health_providers_ibfk_1');
		});
	}

}
