<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMemberVerTwosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('member_ver_twos', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->unique('member_id');
			$table->integer('identity_id')->unsigned()->nullable()->index('identity_id');
			$table->integer('job_title_id')->unsigned()->nullable()->index('job_title_id');
			$table->text('department', 65535)->nullable();
			$table->integer('gender_id')->unsigned()->index('gender_id');
			$table->integer('marital_status_id')->unsigned()->index('marital_status_id');
			$table->integer('location_type_id')->unsigned()->index('location_type_id');
			$table->text('unsurveyed_area', 65535)->nullable();
			$table->string('street', 150)->nullable();
			$table->string('road', 150)->nullable();
			$table->string('plot_no', 150)->nullable();
			$table->string('block_no', 150)->nullable();
			$table->text('surveyed_extra', 65535)->nullable();
			$table->string('phone', 45)->nullable();
			$table->string('telephone', 45)->nullable();
			$table->string('fax', 45)->nullable();
			$table->string('email', 45)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('member_ver_twos');
	}

}
