<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAlertUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('alert_users', function(Blueprint $table)
		{
			$table->foreign('alert_id', 'alert_users_ibfk_1')->references('id')->on('alerts')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('alert_users', function(Blueprint $table)
		{
			$table->dropForeign('alert_users_ibfk_1');
		});
	}

}
