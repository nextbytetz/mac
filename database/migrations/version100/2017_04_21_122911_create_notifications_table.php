<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('incident_type_id')->unsigned()->index('incident_type_id');
			$table->integer('member_id')->unsigned()->index('member_id');
			$table->boolean('status')->comment('store the status of the notification, 0 - Pending, 1 - Approved, 2 - Rejected.');
			$table->boolean('is_acknowledgment_sent')->default(0)->comment('1 - sent, 0 - not sent');
			$table->boolean('is_pensioner')->default(0)->comment('0 - not pensioner, 1 - is pensioner.');
			$table->boolean('investgation_validity')->default(0)->comment('0 - not yet valid, 1 - investigation is valid');
			$table->boolean('isverified')->default(0)->comment('set whether a notification is verified or not, 1 - verified, 0 - not verified.');
			$table->boolean('need_investigation')->default(1)->comment('set whether notification needs investigation or not, 1 - needs investigation, 0 - does not need investigation.');
			$table->boolean('need_verification')->default(0)->comment('set whether the notification needs to be verified or not, 1 - needs verification, 0 - do not need verification.');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
