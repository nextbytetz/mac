<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSysdefsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sysdefs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('total_allowed_contrib_months')->nullable();
			$table->decimal('minimum_monthly_pension', 14)->nullable();
			$table->decimal('maximum_monthly_pension', 14)->nullable();
			$table->decimal('funeral_grant_amount', 14)->nullable();
			$table->decimal('percentage_of_earning', 4)->nullable();
			$table->decimal('assistant_percent', 4)->nullable();
			$table->integer('constant_factor_compensation')->nullable();
			$table->decimal('percentage_imparement_scale', 4)->nullable();
			$table->integer('permanent_partial_mp_period')->nullable();
			$table->decimal('contribution_penalty_percent', 4)->nullable();
			$table->integer('contribution_grace_period')->unsigned()->nullable();
			$table->decimal('public_contribution_percent', 4);
			$table->decimal('private_contribution_percent', 4);
			$table->integer('cancel_receipt_days')->comment('time period allowed for a user to cancel a receipt registered');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sysdefs');
	}

}
