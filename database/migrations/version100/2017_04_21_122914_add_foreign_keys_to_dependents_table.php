<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDependentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dependents', function(Blueprint $table)
		{
			$table->foreign('dependent_type_id', 'dependents_ibfk_1')->references('id')->on('dependent_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('identity_id', 'dependents_ibfk_2')->references('id')->on('identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gender_id', 'dependents_ibfk_3')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('country_id', 'dependents_ibfk_4')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('location_type_id', 'dependents_ibfk_5')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_id', 'dependents_ibfk_6')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_branch_id', 'dependents_ibfk_7')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('parent_id', 'dependents_ibfk_8')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dependents', function(Blueprint $table)
		{
			$table->dropForeign('dependents_ibfk_1');
			$table->dropForeign('dependents_ibfk_2');
			$table->dropForeign('dependents_ibfk_3');
			$table->dropForeign('dependents_ibfk_4');
			$table->dropForeign('dependents_ibfk_5');
			$table->dropForeign('dependents_ibfk_6');
			$table->dropForeign('dependents_ibfk_7');
			$table->dropForeign('dependents_ibfk_8');
		});
	}

}
