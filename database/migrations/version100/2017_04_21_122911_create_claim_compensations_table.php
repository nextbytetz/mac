<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claim_compensations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('claim_id')->unsigned()->index('claim_id');
			$table->integer('employer_id')->unsigned()->nullable()->index('employer_id')->comment('in case employer is to be paid');
			$table->integer('insurance_id')->unsigned()->nullable()->index('insurance_id')->comment('in case insurance is to be paid');
			$table->integer('benefit_type_id')->unsigned()->index('benefit_type_id');
			$table->decimal('amount', 14)->comment('amount to be compensated');
			$table->integer('user_id');
			$table->integer('pay_period')->comment('in months, number of months this compensation will be paid');
			$table->boolean('ispaid')->default(0)->comment('set the paid status, 1 - is already paid, 0 - not yet paid');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claim_compensations');
	}

}
