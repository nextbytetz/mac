<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOfficesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->unique('name');
			$table->integer('parent_id')->unsigned()->nullable()->index('parent_id');
			$table->string('external_id', 100)->nullable()->unique('external_id')->comment('Any special reference for the office.');
			$table->date('opening_date')->nullable()->comment('Date office opened.');
			$table->integer('region_id')->unsigned()->nullable()->index('region_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offices');
	}

}
