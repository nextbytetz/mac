<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmploymentHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employment_histories', function(Blueprint $table)
		{
			$table->foreign('job_title_id', 'employment_histories_ibfk_1')->references('id')->on('job_titles')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('insurance_id', 'employment_histories_ibfk_2')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employment_histories', function(Blueprint $table)
		{
			$table->dropForeign('employment_histories_ibfk_1');
			$table->dropForeign('employment_histories_ibfk_2');
		});
	}

}
