<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rctno')->unsigned()->nullable()->comment('Special Receipt number');
			$table->integer('employer_id')->unsigned()->nullable()->index('employer_id')->comment('Employer who has paid for the receipt');
			$table->string('payer', 200)->nullable()->comment('any other payer who has made the payments.');
			$table->text('description', 65535)->nullable()->comment('any specific description about this payment.');
			$table->string('chequeno', 50)->nullable()->comment('chequeno from the bank in case payment made in bank by cheque');
			$table->date('rct_date')->comment('Receipt date');
			$table->string('bank_reference', 20)->nullable()->comment('to control for double entry of the same receipt.');
			$table->decimal('amount', 14)->comment('exact amount paid');
			$table->integer('currency_id')->unsigned()->index('currency_id');
			$table->integer('payment_type_id')->unsigned()->index('payment_type_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->integer('office_id')->unsigned()->index('office_id');
			$table->integer('bank_id')->unsigned()->index('bank_id');
			$table->boolean('iscancelled')->default(0)->comment('specify whether this receipt is cancelled or not');
			$table->text('cancel_reason', 65535)->nullable()->comment('reason for cancelling receipt');
			$table->integer('cancel_user_id')->unsigned()->nullable()->comment('user who has cancelled the receipt');
			$table->dateTime('cancel_date')->nullable()->comment('date which the receipt has been canceled');
			$table->boolean('ssyes')->default(0)->comment('specify whether data has been exported to the finance software');
			$table->boolean('isdishonered')->default(0)->comment('Specify whether the receipt has been dishonored.');
			$table->boolean('iscomplete')->default(0)->comment('Specify whether the receipt has reached the final stage including contribution upload');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipts');
	}

}
