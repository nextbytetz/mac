<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWitnessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('witnesses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('firstname', 45);
			$table->string('othernames', 45);
			$table->string('phone', 45)->unique('phone');
			$table->timestamps();
			$table->dateTime('deleted_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('witnesses');
	}

}
