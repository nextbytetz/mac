<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuditTrailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('audit_trails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('resource_id')->unsigned()->comment('Primary key of the resource table, a table which is being tracked for changes.');
			$table->boolean('status')->comment('Audit status, 1 - Processed, 0 - waiting approval');
			$table->integer('user_id')->unsigned()->index('user_id')->comment('user who has initiated the change');
			$table->integer('audit_action_id')->unsigned()->index('audit_action_id');
			$table->integer('audit_reference_id')->unsigned()->index('audit_reference_id');
			$table->integer('audit_entity_id')->unsigned()->index('audit_entity_id');
			$table->string('table_name', 50)->comment('name of the table affected');
			$table->integer('office_id')->unsigned()->comment('office which the change has taken place.');
			$table->string('application_name', 40)->nullable()->comment('specify application name, e.g MAC, database system, etc');
			$table->text('command_before', 65535)->comment('original content before change');
			$table->text('command_after', 65535)->comment('contents after change has happened');
			$table->string('host_name', 100)->nullable()->comment('computer name through which the change has taken place.');
			$table->integer('affected_rows')->nullable()->comment('number of rows affected by the change');
			$table->integer('checker')->nullable()->comment('user who approved the action at the certain level if audit change requires approval.');
			$table->dateTime('checked_date')->nullable()->comment('date and time which checker approved the change');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('audit_trails');
	}

}
