<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_types', function(Blueprint $table)
		{
			$table->foreign('document_type_group_id', 'document_types_ibfk_1')->references('id')->on('document_type_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_types', function(Blueprint $table)
		{
			$table->dropForeign('document_types_ibfk_1');
		});
	}

}
