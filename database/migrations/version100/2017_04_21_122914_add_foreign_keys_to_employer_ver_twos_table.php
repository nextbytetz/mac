<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployerVerTwosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employer_ver_twos', function(Blueprint $table)
		{
			$table->foreign('location_type_id', 'employer_ver_twos_ibfk_1')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employer_ver_twos', function(Blueprint $table)
		{
			$table->dropForeign('employer_ver_twos_ibfk_1');
		});
	}

}
