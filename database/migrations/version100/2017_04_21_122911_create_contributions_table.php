<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContributionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contributions', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->date('paydate');
			$table->integer('receipt_code_id')->unsigned()->index('receipt_code_id');
			$table->decimal('employer_amount', 14)->nullable();
			$table->decimal('member_amount', 14);
			$table->decimal('grosspay', 14)->nullable();
			$table->integer('member_id')->unsigned()->index('member_id');
			$table->integer('user_id')->unsigned();
			$table->boolean('status')->default(1)->comment('Specify the status of the contribution, 1 - amount okay, 0 - amount not okay ...');
			$table->boolean('isactive')->default(1)->comment('set whether the contribution is active or not, 1 - active, 0 - not active.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contributions');
	}

}
