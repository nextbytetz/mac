<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->string('accountno', 50)->nullable();
			$table->string('pmt_channel', 30)->nullable()->comment('Payment channel which the payment is being made.');
			$table->timestamps();
			$table->softDeletes();
			$table->unique(['name','accountno'], 'name');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banks');
	}

}
