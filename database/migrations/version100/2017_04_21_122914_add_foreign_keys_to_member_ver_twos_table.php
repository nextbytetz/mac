<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMemberVerTwosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('member_ver_twos', function(Blueprint $table)
		{
			$table->foreign('identity_id', 'member_ver_twos_ibfk_1')->references('id')->on('identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('job_title_id', 'member_ver_twos_ibfk_2')->references('id')->on('job_titles')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gender_id', 'member_ver_twos_ibfk_3')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('marital_status_id', 'member_ver_twos_ibfk_4')->references('id')->on('marital_statuses')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('location_type_id', 'member_ver_twos_ibfk_5')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('member_ver_twos', function(Blueprint $table)
		{
			$table->dropForeign('member_ver_twos_ibfk_1');
			$table->dropForeign('member_ver_twos_ibfk_2');
			$table->dropForeign('member_ver_twos_ibfk_3');
			$table->dropForeign('member_ver_twos_ibfk_4');
			$table->dropForeign('member_ver_twos_ibfk_5');
		});
	}

}
