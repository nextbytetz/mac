<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFinCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fin_codes', function(Blueprint $table)
		{
			$table->foreign('fin_code_group_id', 'fin_codes_ibfk_1')->references('id')->on('fin_code_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fin_codes', function(Blueprint $table)
		{
			$table->dropForeign('fin_codes_ibfk_1');
		});
	}

}
