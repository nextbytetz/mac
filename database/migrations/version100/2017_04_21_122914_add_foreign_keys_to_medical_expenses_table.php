<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMedicalExpensesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('medical_expenses', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'medical_expenses_ibfk_1')->references('id')->on('notifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('medical_expense_type_id', 'medical_expenses_ibfk_2')->references('id')->on('medical_expense_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('insurance_id', 'medical_expenses_ibfk_3')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('medical_expenses', function(Blueprint $table)
		{
			$table->dropForeign('medical_expenses_ibfk_1');
			$table->dropForeign('medical_expenses_ibfk_2');
			$table->dropForeign('medical_expenses_ibfk_3');
		});
	}

}
