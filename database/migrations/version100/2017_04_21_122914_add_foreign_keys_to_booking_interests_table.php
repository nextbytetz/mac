<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBookingInterestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking_interests', function(Blueprint $table)
		{
			$table->foreign('booking_id', 'booking_interests_ibfk_1')->references('id')->on('bookings')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking_interests', function(Blueprint $table)
		{
			$table->dropForeign('booking_interests_ibfk_1');
		});
	}

}
