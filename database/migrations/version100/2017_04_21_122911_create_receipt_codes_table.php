<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipt_codes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('receipt_id')->unsigned();
			$table->integer('fin_code_id')->unsigned()->index('fin_code_id');
			$table->decimal('amount', 14);
			$table->integer('member_count')->nullable()->comment('total number of members being paid for in case receipt for contribution');
			$table->date('contrib_month')->nullable()->comment('month of the contribution in case receipt for contribution');
			$table->integer('booking_id')->unsigned()->nullable()->index('booking_id');
			$table->timestamps();
			$table->softDeletes();
			$table->index(['receipt_id','fin_code_id','booking_id'], 'receipt_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipt_codes');
	}

}
