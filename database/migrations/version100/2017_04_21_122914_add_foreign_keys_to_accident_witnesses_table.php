<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAccidentWitnessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accident_witnesses', function(Blueprint $table)
		{
			$table->foreign('accident_id', 'accident_witnesses_ibfk_1')->references('id')->on('accidents')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('witness_id', 'accident_witnesses_ibfk_2')->references('id')->on('witnesses')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accident_witnesses', function(Blueprint $table)
		{
			$table->dropForeign('accident_witnesses_ibfk_1');
			$table->dropForeign('accident_witnesses_ibfk_2');
		});
	}

}
