<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishonouredChequesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dishonoured_cheques', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('receipt_id')->unsigned()->index('receipt_id')->comment('Receipt associated with the cheque');
            $table->integer('new_bank_id')->unsigned()->nullable()->index('new_bank_id')->comment('Bank of the replaced cheque');
            $table->integer('old_bank_id')->unsigned()->index('old_bank_id')->comment('Bank of the dishonoured cheque');
            $table->string('new_cheque_no', 20)->nullable()->comment('Replaced cheque no');
            $table->string('old_cheque_no', 20)->comment('Dishonoured cheque no');
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->string('dishonour_reason',100)->comment('Reasons for dishonouring cheque');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::table('dishonoured_cheques', function(Blueprint $table)
        {
            $table->foreign('receipt_id', 'receipt_ibfk_1')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('dishonoured_cheques', function(Blueprint $table)
        {
            $table->dropForeign('receipt_ibfk_1');

        });
        Schema::drop('dishonoured_cheques');
    }

}
