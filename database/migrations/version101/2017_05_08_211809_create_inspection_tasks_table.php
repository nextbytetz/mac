<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inspection_tasks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name', 65535)->comment('name of the task');
			$table->text('deliverable', 65535)->nullable()->comment('deliverable of the specified task');
			$table->text('target', 65535)->nullable()->comment('targets of the task');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inspection_tasks');
	}

}
