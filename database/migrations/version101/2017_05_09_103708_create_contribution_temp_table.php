<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contribution_temps', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string("employeeno")->nullable()->comment("Specific number of the employee described by an employer");
            $table->string("firstname", 30);
            $table->string("middlename", 30)->nullable();
            $table->string("lastname", 30);
            $table->decimal("basicpay", 14)->nullable();
            $table->decimal("grosspay", 14)->nullable();
            $table->integer("receipt_code_id")->unsigned()->index("receipt_code_id");
            $table->timestamps();
            $table->foreign('receipt_code_id', 'contribution_temps_ibfk_1')->references('id')->on('receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contribution_temps');
    }
}
