<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReturnHasreceiptColBookingInterestRedo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            DB::statement("alter table booking_interests add hasreceipt boolean after ispaid ");
            $table->boolean('hasreceipt')->comment('Flag to show if booking has a receipt / paid')
                ->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->dropColumn('hasreceipt');
        });
    }
}
