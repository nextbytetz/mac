<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyBankDetailsOnInsuranceEmployeeEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employees', function(Blueprint $table)
        {
            $table->integer('bank_id')->unsigned()->nullable()->after('employee_category_id')->index('bank_id');
                       $table->integer('bank_branch_id')->unsigned()->nullable()->after('bank_id')->index('bank_branch_id');
            $table->string('accountno', 45)->nullable()->after('bank_branch_id');

        });

        Schema::table('employer_ver_twos', function(Blueprint $table)
        {
            $table->integer('bank_id')->unsigned()->nullable()->after('road')->index('bank_id');

            $table->integer('bank_branch_id')->unsigned()->nullable()->after('bank_id')->index('bank_branch_id');
            $table->string('accountno', 45)->nullable()->after('bank_branch_id');
        });


        Schema::table('insurances', function(Blueprint $table)
        {
            $table->integer('bank_id')->unsigned()->nullable()->after('external_id')->index('bank_id');

            $table->integer('bank_branch_id')->unsigned()->nullable()->after('bank_id')->index('bank_branch_id');
            $table->string('accountno', 45)->nullable()->after('bank_branch_id');
        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('bank_id')->unsigned()->after('fin_account_code_id')->index('bank_id');
             $table->integer('bank_branch_id')->unsigned()->after('bank_id')->index('bank_branch_id');
            $table->string('accountno', 45)->after('bank_branch_id');
        });


        Schema::table('employees', function(Blueprint $table)
        {

            $table->foreign('bank_id', 'employees_bank_id_foreign')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id', 'employees_bank_branch_id_foreign')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


        Schema::table('employer_ver_twos', function(Blueprint $table)
        {

            $table->foreign('bank_id', 'employer_bank_id_foreign')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id', 'employer_bank_branch_id_foreign')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('insurances', function(Blueprint $table)
        {

            $table->foreign('bank_id', 'insurance_bank_id_foreign')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id', 'insurance_bank_branch_id_foreign')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {

            $table->foreign('bank_id', 'payment_vouchers_bank_id_foreign')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id', 'payment_vouchers_bank_branch_id_foreign')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('employees', function(Blueprint $table)
        {
            $table->dropForeign('employees_bank_id_foreign');
            $table->dropForeign('employees_bank_branch_id_foreign');
            $table->dropIndex('bank_id');
            $table->dropIndex('bank_branch_id');
            $table->dropColumn('bank_branch_id');
            $table->dropColumn('bank_id');
            $table->dropColumn('accountno');

        });


        Schema::table('employer_ver_twos', function(Blueprint $table)
        {
            $table->dropForeign('employer_bank_id_foreign');
            $table->dropForeign('employer_bank_branch_id_foreign');
            $table->dropIndex('bank_id');
            $table->dropIndex('bank_branch_id');
            $table->dropColumn('bank_branch_id');
            $table->dropColumn('bank_id');
            $table->dropColumn('accountno');


        });

        Schema::table('insurances', function(Blueprint $table)
        {
            $table->dropForeign('insurance_bank_id_foreign');
            $table->dropForeign('insurance_bank_branch_id_foreign');
            $table->dropIndex('bank_id');
            $table->dropIndex('bank_branch_id');
            $table->dropColumn('bank_branch_id');
            $table->dropColumn('bank_id');
            $table->dropColumn('accountno');
             });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropForeign('payment_vouchers_bank_id_foreign');
            $table->dropForeign('payment_vouchers_bank_branch_id_foreign');
            $table->dropIndex('bank_id');
            $table->dropIndex('bank_branch_id');
            $table->dropColumn('bank_branch_id');
            $table->dropColumn('bank_id');
            $table->dropColumn('accountno');
        });
    }
}
