<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErrorReportOnContributionTemps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contribution_temps', function (Blueprint $table) {
            $table->tinyInteger("error")->default(0)->after("receipt_code_id")->comment("Check whether the uploaded document has error or not ..., 0 - has no error, 1 - has error");
            $table->text("error_report")->nullable()->after("receipt_code_id")->comment("Contain the error report for this specific row of import");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contribution_temps', function (Blueprint $table) {
            $table->dropColumn("error");
            $table->dropColumn("error_report");
        });
    }
}
