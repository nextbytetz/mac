<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnEmployeesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('id');
        });

        Schema::table('employees', function (Blueprint $table) {
            $table->increments("id");
            $table->index('firstname', 'firstname');
            $table->index('lastname', 'lastname');
            $table->index('dob', 'dob');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->increments("id");
            $table->dropIndex('firstname', 'firstname');
            $table->dropIndex('lastname', 'lastname');
            $table->dropIndex('dob', 'dob');
        });
    }
}
