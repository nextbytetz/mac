<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserOnCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('case_hearings', function(Blueprint $table)
        {
            $table->integer("user_id")->unsigned()->after("status_description")->nullable()->comment("system user who has created a case hearing");
        });
        Schema::table('cases', function(Blueprint $table)
        {
            $table->integer("user_id")->unsigned()->after("filling_date")->nullable()->comment("system user who has created a case in the system");
            $table->integer("user_closed")->unsigned()->after("user_id")->nullable()->comment("system user who has marked the case as closed");
        });
        Schema::table('case_mentions', function(Blueprint $table)
        {
            $table->integer("user_id")->unsigned()->after("description")->nullable()->comment("system user who has created a case mention");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_mentions', function(Blueprint $table)
        {
            $table->dropColumn("user_id");
        });
        Schema::table('cases', function(Blueprint $table)
        {
            $table->dropColumn("user_id");
            $table->dropColumn("user_closed");
        });
        Schema::table('case_hearings', function(Blueprint $table)
        {
            $table->dropColumn("user_id");
        });
    }
}
