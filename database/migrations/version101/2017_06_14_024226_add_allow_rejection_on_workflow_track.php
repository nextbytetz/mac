<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllowRejectionOnWorkflowTrack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function (Blueprint $table)
        {
            $table->integer('allow_rejection')->nullable()->default(1)->after("active")->comments("check whether the next level can reject to this level or not, 1 allow, 0 do not allow");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_definitions', function (Blueprint $table)
        {
            $table->dropColumn('allow_rejection');
        });
    }
}
