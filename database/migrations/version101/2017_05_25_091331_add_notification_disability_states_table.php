<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationDisabilityStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('notification_disability_states', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('notification_report_id')->unsigned()->index('notification_report_id');
            $table->integer('disability_state_checklist_id')->unsigned()->index('disability_state_checklist_id');
            $table->dateTime('from_date')->nullable()->comment('date from which the member has had the health state outlined.');
            $table->dateTime('to_date')->nullable()->comment('date from which the member has had the health state outlined.');
            $table->tinyInteger('day_hours')->nullable()->comment('hours worked per day by a member');
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('notification_report_id', 'notification_disability_states_ibfk_1')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('disability_state_checklist_id', 'notification_disability_states_ibfk_2')->references('id')->on('disability_state_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->dropColumn("from_date");
            $table->tinyInteger("state")->default(0)->after("health_state_checklist_id")->comment("boolean for the health state checklist, 1 - yes and 0 - no");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->dropColumn("state");

        });
        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->dateTime("from_date")->after("health_state_checklist_id")->comment("date from which the member has had the health state outlined.");
        });
        Schema::drop('notification_disability_states');
    }
}
