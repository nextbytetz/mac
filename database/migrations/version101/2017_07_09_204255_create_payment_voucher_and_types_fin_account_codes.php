<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentVoucherAndTypesFinAccountCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('payment_voucher_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 150);
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('fin_account_codes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('account_code', 20)->comment('Account Code.');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('payment_vouchers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('resource_id')->unsigned()->comment('Primary key of the resource table, a table which is being tracked for.');
            $table->integer('payment_voucher_type_id')->unsigned()->index('payment_voucher_type_id')->comment('Payment voucher type i.e compensations');
            $table->decimal('amount', 14);
            $table->integer('fin_account_code_id')->unsigned()->index('fin_account_code_id')->comment('Finance account codes where this voucher is allocated to for payment');
            $table->boolean('isexported')->default(0)->comment('Flag to show whether this payment voucher has already been exported to finance system');
            $table->integer('user_id')->unsigned()->index('user_id')->comment('Staff user who has created this payment voucher');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->foreign('fin_account_code_id', 'fin_account_code_foreign_key')->references('id')->on('fin_account_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('payment_voucher_type_id', 'payment_voucher_type_foreign_key')->references('id')->on('payment_voucher_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }





    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropForeign('fin_account_code_foreign_key');
            $table->dropForeign('payment_voucher_type_foreign_key');
        });


        Schema::drop('payment_voucher_types');
        Schema::drop('fin_account_codes');
        Schema::drop('payment_vouchers');
    }
}
