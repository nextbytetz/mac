<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerNameOnEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->integer("employer_id")->unsigned()->nullable()->change();
            $table->dropColumn("duration");
            $table->string("employer_name")->nullable()->after("employer_id");
            $table->date("end_date")->nullable()->after("start_date");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->dropColumn("end_date");
            $table->dropColumn("employer_name");
            $table->smallInteger("duration")->after("start_date")->nullable();
            $table->integer("employer_id")->unsigned()->change();
        });
    }
}
