<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTrackDescOnWfTracks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropColumn(['track_desc']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->text('track_desc', 65535)->comment('optional description of the tracking.');
        });
    }
}
