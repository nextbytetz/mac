<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeCancelUserNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->integer("cancel_user")->nullable()->unsigned()->change();
            $table->integer("user_id")->unsigned()->after("duration");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->integer("cancel_user")->unsigned()->change();
            $table->dropColumn("user_id");
        });
    }
}
