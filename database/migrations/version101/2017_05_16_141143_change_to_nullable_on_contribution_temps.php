<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeToNullableOnContributionTemps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contribution_temps', function (Blueprint $table) {
            $table->string("firstname", 30)->nullable()->change();
            $table->string("lastname", 30)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contribution_temps', function (Blueprint $table) {
            $table->string("firstname", 30)->change();
            $table->string("lastname", 30)->change();
        });
    }
}
