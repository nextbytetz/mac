<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteWfTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropForeign('wf_tracks_ibfk_1', 'wf_tracks_ibfk_1');
            $table->dropIndex('wf_type_id', 'wf_type_id');
            $table->dropColumn(['wf_type_id']);
        });
        Schema::drop('wf_types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('wf_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 80)->unique('name');
            $table->timestamps();
        });

        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->integer('wf_type_id')->unsigned()->index('wf_type_id');
            $table->foreign('wf_type_id', 'wf_tracks_ibfk_1')->references('id')->on('wf_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }
}
