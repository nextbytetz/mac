<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfDateOnClaimsAndUpdateOperation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims', function(Blueprint $table)
        {
            $table->date('wf_done_date')->nullable()->after("wf_done")->comment('date which workflow was completed');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims', function(Blueprint $table)
        {
            $table->dropColumn("wf_done_date");

        });
    }
}
