<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("drop VIEW members");
        DB::statement("drop VIEW member_ver_ones");
        Schema::rename("member_ver_twos", "employees");
        Schema::rename("member_old_compensations", "employee_old_compensations");
        Schema::table('contributions', function (Blueprint $table) {
            $table->renameColumn('member_id', 'employee_id');
            $table->decimal("basicpay", 14)->nullable();
        });
        Schema::table('dependents', function (Blueprint $table) {
            $table->renameColumn('member_id', 'employee_id');
        });
        Schema::table('employment_histories', function (Blueprint $table) {
            $table->renameColumn('member_id', 'employee_id');
        });
        Schema::table('employee_old_compensations', function (Blueprint $table) {
            $table->renameColumn('member_id', 'employee_id');
        });
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->renameColumn('member_id', 'employee_id');
        });
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->renameColumn('member_id', 'employee_id');
        });
        Schema::create('employee_categories', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->string('name', 150)->unique('name', 'name');
            $table->timestamps();
        });

        Schema::table('employees', function (Blueprint $table) {
            $table->string("firstname", 30);
            $table->string("middlename", 30)->nullable();
            $table->string("lastname", 30);
            $table->date('dob')->nullable()->comment("Date of birth");
            $table->string("memberno", 10);
            $table->integer("employer_id")->unsigned();
            $table->decimal("salary", 14)->nullable();
            $table->smallInteger("employee_category_id")->unsigned()->index("employee_category_id");
            $table->foreign('employee_category_id', 'employees_ibfk_6')->references('id')->on('employee_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn(['firstname', 'middlename', 'lastname', 'dob', 'memberno', 'employer_id', 'salary']);
            $table->dropForeign('employees_ibfk_6', 'employees_ibfk_6');
            $table->dropIndex('employee_category_id', 'employee_category_id');
            $table->dropColumn("employee_category_id");
        });

        Schema::drop('employee_categories');
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->renameColumn('employee_id', 'member_id');
        });
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->renameColumn('employee_id', 'member_id');
        });
        Schema::table('employee_old_compensations', function (Blueprint $table) {
            $table->renameColumn('employee_id', 'member_id');
        });
        Schema::table('employment_histories', function (Blueprint $table) {
            $table->renameColumn('employee_id', 'member_id');
        });
        Schema::table('dependents', function (Blueprint $table) {
            $table->renameColumn('employee_id', 'member_id');
        });
        Schema::table('contributions', function (Blueprint $table) {
            $table->renameColumn('employee_id', 'member_id');
            $table->dropColumn("basicpay");
        });
        Schema::rename("employees", "member_ver_twos");
        Schema::rename("employee_old_compensations", "member_old_compensations");
        DB::statement("create VIEW member_ver_ones AS select * from wcf.members");
        DB::statement("create VIEW members AS select * from (member_ver_ones a left join member_ver_twos b on((a.member_id = b.id)))");
    }
}
