<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMimeInReceiptCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->string('mime', 150)->nullable()->after("total_rows")->comment("mime type of the uploaded contribution file");
            $table->float('size')->nullable()->after("total_rows")->comment("size of the uploaded contribution file")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->dropColumn('mime');
            $table->float('size')->nullable()->change();
        });
    }
}
