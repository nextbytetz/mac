<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyClaimCompensationCreateTableDisabilityStateAssesed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->renameColumn('employer_id', 'resource_id');
            $table->dropForeign('claim_compensations_ibfk_2', 'claim_compensations_ibfk_2');
            $table->dropIndex('insurance_id', 'insurance_id');
                       $table->dropColumn('insurance_id');
        });

        Schema::create('notification_disability_state_assessments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('notification_disability_state_id')->unsigned()->index('notification_disability_state_id');
            $table->tinyInteger('days')->nullable()->comment('Days assessed for each disability state. i.e actual hospitalization days.');
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->timestamps();
            $table->softDeletes();
          });

        Schema::table('notification_disability_state_assessments', function(Blueprint $table) {
            $table->foreign('notification_disability_state_id', 'notification_disability_state_assessments_ibfk_1')->references('id')->on('notification_disability_states')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('claim_compensations', function(Blueprint $table)
    {
        $table->renameColumn('resource_id','employer_id' );
        $table->integer('insurance_id')->unsigned()->nullable()->index('insurance_id')->comment('in case insurance is to be paid');

    });

        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->foreign('insurance_id', 'claim_compensations_ibfk_2')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::drop('notification_disability_state_assessments');

    }
}
