<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnNotificationDisabilityStateAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('notification_disability_state_assessments', function(Blueprint $table)
        {

            $table->integer('days')->nullable()->comment('Days assessed for each disability state. i.e actual hospitalization days.')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('notification_disability_state_assessments', function(Blueprint $table)
        {

            $table->tinyInteger('days')->nullable()->comment('Days assessed for each disability state. i.e actual hospitalization days.')->change();

        });
    }
}
