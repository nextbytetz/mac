<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLawyersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lawyers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('firstname', 50);
			$table->string('othernames', 80);
			$table->string('external_id', 30)->unique('external_id')->comment('any special reference relating to the lawyer');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lawyers');
	}

}
