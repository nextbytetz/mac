<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErrorOnReceiptCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->tinyInteger("error")->default(0)->nullable()->after("isuploaded")->comment("show whether the associated uploaded file has error or not");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->dropColumn("error");
        });
    }
}
