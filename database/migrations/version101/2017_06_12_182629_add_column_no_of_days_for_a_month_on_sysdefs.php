<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNoOfDaysForAMonthOnSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('sysdefs', 'no_of_days_for_a_month'))
        {
            Schema::table('sysdefs', function (Blueprint $table)
            {
                $table->integer('no_of_days_for_a_month')->unsigned()->nullable()->default(30);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sysdefs', 'no_of_days_for_a_month'))
        {
            Schema::table('sysdefs', function (Blueprint $table)
            {
                $table->dropColumn('no_of_days_for_a_month');
            });
        }
    }
}
