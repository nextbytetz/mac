<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNewPaymentDateToBookingInterest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function(Blueprint $table)
        {
              $table->date('new_payment_date')->nullable()->after('adjust_user_id')->comment('New modified  payment date of receipt for contribution of this booking.');
                 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->dropColumn('new_payment_date');
        });
    }
}
