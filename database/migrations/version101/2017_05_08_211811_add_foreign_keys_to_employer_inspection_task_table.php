<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployerInspectionTaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employer_inspection_task', function(Blueprint $table)
		{
			$table->foreign('inspection_task_id', 'employer_inspection_task_ibfk_1')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employer_inspection_task', function(Blueprint $table)
		{
			$table->dropForeign('employer_inspection_task_ibfk_1');
		});
	}

}
