<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLinkedFileReceiptCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("alter table receipt_codes add linked_file varchar(20)  NULL  after booking_id ");
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->string('linked_file',25)->comment('Linked file attached to a receipt code i.e. contributions')
                ->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->dropColumn('linked_file');
        });
    }
}
