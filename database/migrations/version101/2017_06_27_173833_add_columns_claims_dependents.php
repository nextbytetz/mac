<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsClaimsDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claims', function(Blueprint $table)
        {
            $table->boolean('wf_done')->default(0)->after('user_id')->comment('specify whether the claim workflow has been compeleted or not.');

        });

        Schema::table('dependents', function(Blueprint $table)
        {
            $table->boolean('isactive')->default(0)->after('lastresponse')->comment('specify whether the dependent is active to receive monthly pension');
            $table->boolean('isresponded')->default(0)->after('isactive')->comment('specify whether the dependent has responded to the claim compensation award to confirm bank details and his/her information');
        });

        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->dropColumn('recycl');
            $table->dropColumn('pay_period');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('claims', function(Blueprint $table)
        {
            $table->dropColumn('wf_done');

        });
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropColumn('isactive');
            $table->dropColumn('isresponded');
        });

        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->integer('recycl')->after('user_id')->comment('in months, number of cycled this compensation will be paid');
            $table->integer('pay_period')->after('recycl')->comment('in months, number of months this compensation will be paid');

        });
    }
}
