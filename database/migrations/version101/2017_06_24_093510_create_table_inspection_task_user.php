<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInspectionTaskUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_task_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("inspection_task_id")->unsigned()->index("inspection_task_id");
            $table->integer("user_id")->unsigned()->index("user_id");
            $table->foreign('inspection_task_id')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
        });
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->dropForeign("employer_inspection_task_ibfk_1", "employer_inspection_task_ibfk_1");
            $table->foreign('inspection_task_id')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->bigIncrements("id")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inspection_task_user');
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->dropForeign("employer_inspection_task_ibfk_1", "employer_inspection_task_ibfk_1");
            $table->foreign('inspection_task_id')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->increments("id")->change();
        });
    }
}
