<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisitDateOnEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->dropColumn("start_date");
            $table->dropColumn("end_date");
            $table->dropColumn("delay_reasons");
            $table->date("visit_date")->nullable()->after("inspection_task_id");
            $table->text("resolutions")->nullable()->after("user_id")->comment("a formal expression of opinion or intention agreed after inspection");
            $table->renameColumn("extra_findings","findings");
        });
        Schema::table('inspections', function(Blueprint $table) {
            $table->text("delay_reasons")->after("complete_user")->nullable()->comment("any reason which have made the inspection to delay to complete");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->dropColumn("delay_reasons");
        });
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->date("start_date")->nullable()->after("inspection_task_id");
            $table->date("end_date")->nullable()->after("inspection_task_id");
            $table->text("delay_reasons")->nullable()->after("inspection_task_id");
            $table->dropColumn("visit_date");
            $table->dropColumn("resolutions");
            $table->renameColumn("findings","extra_findings");
        });
    }
}
