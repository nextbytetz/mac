<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInspectionProfileIdOnInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_tasks', function(Blueprint $table) {
            $table->integer("inspection_profile_id")->unsigned()->nullable()->index("inspection_profile_id")->after("deliverable");
            $table->foreign('inspection_profile_id')->references('id')->on('inspection_profiles')->onUpdate('CASCADE')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_tasks', function(Blueprint $table) {
            $table->dropColumn("inspection_profile_id");
        });
    }
}
