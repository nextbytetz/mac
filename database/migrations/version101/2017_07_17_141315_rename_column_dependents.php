<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
    {
        $table->renameColumn('funeral_grant_flag','funeral_grant_pay_flag');

    });
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->boolean('funeral_grant_pay_flag')->default(0)->comment('set whether dependent has already been paid funeral grant or not, 1 - paid, 0 - not paid yet')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->renameColumn('funeral_grant_pay_flag','funeral_grant_flag');

        });
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->decimal('funeral_grant', 14)->nullable();
            $table->boolean('funeral_grant_flag')->default(0)->comment('set whether dependent is receiving funeral grant or not, 1 - receiving, 0 - not receiving')->change();

        });
    }
}
