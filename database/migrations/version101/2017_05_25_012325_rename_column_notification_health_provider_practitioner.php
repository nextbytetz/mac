<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnNotificationHealthProviderPractitioner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_health_provider_practitioners', function (Blueprint $table) {
                    $table->renameColumn('medical_practitoner_id', 'medical_practitioner_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_health_provider_practitioners', function (Blueprint $table) {
            $table->renameColumn('medical_practitioner_id', 'medical_practitoner_id');

        });
    }
}
