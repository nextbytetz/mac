<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUserIdAndDurationAndStartDateNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->dateTime("start_date")->nullable()->change();
            $table->smallInteger("duration")->nullable()->change();
            $table->integer("user_id")->nullable()->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->dateTime("start_date")->change();
            $table->smallInteger("duration")->change();
            $table->integer("user_id")->unsigned()->change();
        });
    }
}
