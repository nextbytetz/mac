<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCaseMentions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_mentions', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->smallInteger('case_personnel_id')->index('case_personnel_id')->unsigned();
            $table->integer('case_id')->index('case_id')->unsigned();
            $table->integer('lawyer_id')->index('lawyer_id')->unsigned();
            $table->string("firstname", 40)->comment("first name of the case personnel during the mention");
            $table->string("middlename", 40)->comment("middle name of the case personnel during the mention");
            $table->string("lastname", 40)->comment("lastname name of the case personnel during the mention");
            $table->date("mention_date");
            $table->time("mention_time");
            $table->text("description")->nullable()->comment("Any associated description concerning the case mentioning");
            $table->timestamps();
            $table->foreign('case_personnel_id')->references('id')->on('case_personnels')->onUpdate('CASCADE')->onDelete('restrict');
            $table->foreign('case_id')->references('id')->on('cases')->onUpdate('CASCADE')->onDelete('restrict');
            $table->foreign('lawyer_id')->references('id')->on('lawyers')->onUpdate('CASCADE')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('case_mentions');
    }
}
