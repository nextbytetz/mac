<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullableColumnsOnEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->integer('gender_id')->unsigned()->nullable()->change();
            $table->integer('marital_status_id')->unsigned()->nullable()->change();
            $table->integer('location_type_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->integer('gender_id')->unsigned()->change();
            $table->integer('marital_status_id')->unsigned()->change();
            $table->integer('location_type_id')->unsigned()->change();
        });
    }
}
