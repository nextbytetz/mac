<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompleteDetailsOnInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->integer("complete_user")->unsigned()->nullable()->after("cancel_date");
            $table->date("complete_date")->nullable()->after("cancel_date");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->dropColumn("complete_user");
            $table->dropColumn("complete_date");
        });
    }
}
