<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePensioners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->boolean('firstpay_flag')->default(0)->after('isresponded')->comment('specify whether the dependent has already started to receive monthly pension');
        });


        Schema::create('pensioners', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employee_id')->unsigned()->index('employee_id');
            $table->integer('benefit_type_id')->unsigned()->index('benefit_type_id');
              $table->decimal('monthly_pension_amount', 14)->comment('amount of monthly pension the pensioner is eligible to be paid.');
            $table->integer('compensation_payment_type_id')->unsigned()->index('compensation_payment_type_id');
            $table->integer('recycl')->nullable()->comment('in months, number of cycled this compensation will be paid');
            $table->integer('pay_period')->nullable()->comment('in months, number of months this compensation will be paid');
            $table->boolean('isactive')->default(0)->comment('specify whether the pensioners is active to receive monthly pension');
            $table->boolean('isresponded')->default(0)->comment('specify whether the pensioner has responded to the claim compensation award to confirm bank details and his/her information');
            $table->boolean('firstpay_flag')->default(0)->comment('specify whether the pensioner has already started to receive monthly pension');
            $table->boolean('suspense_flag')->default(0)->comment('set whether pensioner is suspended or not, 1 - suspended, 0 - not suspended.');
            $table->dateTime('lastresponse')->comment('last verified date');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->foreign('employee_id', 'employee_id_foreign')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->foreign('benefit_type_id', 'benefit_type_id_foreign')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->foreign('compensation_payment_type_id', 'compensation_payment_type_id_pensioner_foreign')->references('id')->on('compensation_payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropColumn('firstpay_flag');
        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->dropForeign('employee_id_foreign', 'employee_id_foreign');
            $table->dropForeign('benefit_type_id_foreign', 'benefit_type_id_foreign');
            $table->dropForeign('compensation_payment_type_id_foreign', 'compensation_payment_type_id_foreign');

        });
        Schema::drop('pensioners');
    }
}
