<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyPayrollRuns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->foreign('member_type_id', 'payroll_runs_member_type_id_ibfk_4')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->dropForeign('payroll_runs_member_type_id_ibfk_4');
        });
    }
}
