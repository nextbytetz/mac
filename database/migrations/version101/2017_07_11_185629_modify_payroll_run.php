<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPayrollRun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::drop('payroll_runs');

        Schema::create('payroll_runs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id');
            $table->integer('member_type_id')->unsigned()->index('member_type_id');
            $table->integer('resource_id')->unsigned()->index('resource_id');
            $table->decimal('amount', 14);
            $table->integer('months_paid')->unsigned();
            $table->decimal('monthly_pension', 14);
            $table->integer('bank_id')->unsigned()->index('bank_id');
            $table->string('accountno', 45);
            $table->integer('bank_branch_id')->unsigned()->index('bank_branch_id');
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_id', 'payroll_runs_ibfk_1')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_id', 'payroll_runs_ibfk_2')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id', 'payroll_runs_ibfk_3')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
              });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payroll_runs');
        Schema::create('payroll_runs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id');
            $table->integer('claim_id')->unsigned()->index('claim_id');
            $table->integer('employer_id')->unsigned()->nullable()->index('employer_id');
            $table->integer('insurance_id')->unsigned()->nullable()->index('insurance_id');
            $table->decimal('amount', 14);
            $table->text('description', 65535)->nullable();
            $table->integer('benefit_type_id')->unsigned()->index('benefit_type_id');
            $table->timestamps();
            $table->softDeletes();
        });



    }
}
