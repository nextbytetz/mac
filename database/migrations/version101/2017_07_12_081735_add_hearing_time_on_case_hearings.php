<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHearingTimeOnCaseHearings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_personnels', function(Blueprint $table)
        {
            $table->smallInteger('id', true)->unsigned();
            $table->string('name', 80)->unique('name', 'name');
            $table->timestamps();
        });

        Schema::table('case_hearings', function(Blueprint $table)
        {
            $table->date("hearing_date")->change();
            $table->time("hearing_time")->after("hearing_date");
            $table->text("status_description")->after("status")->comment("More explanations about the status selected")->nullable();
            $table->smallInteger("status")->default(0)->comment("set the status whether the case has been heard or not, 1 - case has been heard, 0 case has not been heard., 2 - case has been cancelled.")->change();
            $table->smallInteger("case_personnel_id")->nullable()->unsigned()->index("case_personnel_id")->after("id");
            $table->foreign('case_personnel_id')->references('id')->on('case_personnels')->onUpdate('CASCADE')->onDelete('restrict');
            $table->string("firstname", 40)->after("case_personnel_id")->comment("first name of the case personnel during the hearing");
            $table->string("middlename", 40)->nullable()->after("firstname")->comment("middle name of the case personnel during the hearing");
            $table->string("lastname", 40)->after("middlename")->comment("lastname name of the case personnel during the hearing");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('case_hearings', function(Blueprint $table)
        {
            $table->dropColumn("lastname");
            $table->dropColumn("middlename");
            $table->dropColumn("firstname");
            $table->dropForeign("case_hearings_case_personnel_id_foreign");
            $table->dropIndex("case_personnel_id");
            $table->dropColumn("case_personnel_id");
            $table->dropColumn("status_description");
            $table->dropColumn("hearing_time");
            $table->dateTime("hearing_date")->nullable()->change();
        });

        Schema::drop('case_personnels');

    }
}
