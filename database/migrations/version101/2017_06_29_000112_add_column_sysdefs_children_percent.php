<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSysdefsChildrenPercent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->decimal('child_percent_with_spouse', 4)->nullable()->after('wife_spouse_percent')->comment('Default survivor pension percent for child when there is wife or spouse');
            $table->decimal('child_percent_without_spouse', 4)->nullable()->after('child_percent_with_spouse')->comment('Default survivor pension percent for when there is no wife or spouse');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->dropColumn('child_percent_with_spouse');
        });
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->dropColumn('child_percent_without_spouse');
        });
    }
}
