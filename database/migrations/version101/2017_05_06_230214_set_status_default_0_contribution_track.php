<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetStatusDefault0ContributionTrack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('contribution_tracks', function (Blueprint $table) {
            $table->boolean('status')->default(0)->change();
              });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('contribution_tracks', function (Blueprint $table) {
                     $table->boolean('status');
        });
    }
}
