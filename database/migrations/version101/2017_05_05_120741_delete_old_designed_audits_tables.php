<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\DisableForeignKeys;

class DeleteOldDesignedAuditsTables extends Migration
{
    use DisableForeignKeys;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->disableForeignKeys();

        Schema::table('checkers', function (Blueprint $table) {
            $table->dropForeign('checkers_ibfk_1', 'checkers_ibfk_1');
            $table->dropForeign('checkers_ibfk_2', 'checkers_ibfk_2');
            $table->dropForeign('checkers_ibfk_3', 'checkers_ibfk_3');
            $table->dropIndex('audit_entity_id', 'audit_entity_id');
            $table->dropIndex('audit_action_id', 'audit_action_id');
            $table->dropIndex('audit_reference_id', 'audit_reference_id');
            $table->dropColumn(['audit_entity_id', 'audit_action_id', 'audit_reference_id']);
        });

        Schema::drop('audit_actions');
        Schema::drop('audit_entities');
        Schema::drop('audit_references');
        Schema::drop('audit_trails');

        $this->enableForeignKeys();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->disableForeignKeys();

        Schema::create('audit_actions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 100)->unique('name');
            $table->timestamps();
        });
        Schema::create('audit_entities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 100)->unique('name');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('audit_references', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 100)->unique('name');
            $table->timestamps();
        });
        Schema::create('audit_trails', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('resource_id')->unsigned()->comment('Primary key of the resource table, a table which is being tracked for changes.');
            $table->boolean('status')->comment('Audit status, 1 - Processed, 0 - waiting approval');
            $table->integer('user_id')->unsigned()->index('user_id')->comment('user who has initiated the change');
            $table->integer('audit_action_id')->unsigned()->index('audit_action_id');
            $table->integer('audit_reference_id')->unsigned()->index('audit_reference_id');
            $table->integer('audit_entity_id')->unsigned()->index('audit_entity_id');
            $table->string('table_name', 50)->comment('name of the table affected');
            $table->integer('office_id')->unsigned()->comment('office which the change has taken place.');
            $table->string('application_name', 40)->nullable()->comment('specify application name, e.g MAC, database system, etc');
            $table->text('command_before', 65535)->comment('original content before change');
            $table->text('command_after', 65535)->comment('contents after change has happened');
            $table->string('host_name', 100)->nullable()->comment('computer name through which the change has taken place.');
            $table->integer('affected_rows')->nullable()->comment('number of rows affected by the change');
            $table->integer('checker')->nullable()->comment('user who approved the action at the certain level if audit change requires approval.');
            $table->dateTime('checked_date')->nullable()->comment('date and time which checker approved the change');
            $table->timestamps();
            $table->foreign('audit_action_id', 'audit_trails_ibfk_1')->references('id')->on('audit_actions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('audit_reference_id', 'audit_trails_ibfk_2')->references('id')->on('audit_references')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('audit_entity_id', 'audit_trails_ibfk_3')->references('id')->on('audit_entities')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('checkers', function (Blueprint $table) {
            $table->integer('audit_entity_id')->unsigned()->index('audit_entity_id');
            $table->integer('audit_action_id')->unsigned()->index('audit_action_id');
            $table->integer('audit_reference_id')->unsigned()->index('audit_reference_id');
            $table->foreign('audit_entity_id', 'checkers_ibfk_1')->references('id')->on('audit_entities')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('audit_action_id', 'checkers_ibfk_2')->references('id')->on('audit_actions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('audit_reference_id', 'checkers_ibfk_3')->references('id')->on('audit_references')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        $this->enableForeignKeys();
    }
}
