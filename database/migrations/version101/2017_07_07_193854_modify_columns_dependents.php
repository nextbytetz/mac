<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //        //
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->decimal('survivor_pension_percent', 5)->nullable()->change();
            $table->decimal('survivor_gratuity_percent', 5)->nullable()->change();
            $table->decimal('funeral_grant_percent', 5)->nullable()->change();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //


        Schema::table('dependents', function(Blueprint $table)
        {
            $table->decimal('survivor_pension_percent', 4)->nullable()->change();
            $table->decimal('survivor_gratuity_percent', 4)->nullable()->change();
            $table->decimal('funeral_grant_percent', 4)->nullable()->change();

        });
    }
}
