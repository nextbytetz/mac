<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyWfGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_groups', function (Blueprint $table) {
            $table->dropForeign('wf_groups_ibfk_1', 'wf_groups_ibfk_1');
            $table->dropIndex('wf_module_id', 'wf_module_id');
            $table->dropColumn(['wf_module_id']);
            $table->dropColumn(['level']);
            $table->integer('wf_definition_id')->unsigned()->index('wf_definition_id');
            $table->foreign('wf_definition_id', 'wf_definition_user_ibfk_1')->references('id')->on('wf_definitions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::rename('wf_groups', 'user_wf_definition');
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropForeign('wf_tracks_ibfk_2', 'wf_tracks_ibfk_2');
            $table->dropIndex('wf_module_id', 'wf_module_id');
            $table->dropColumn(['wf_module_id']);
            $table->dropColumn(['level']);
            $table->integer('wf_definition_id')->unsigned()->index('wf_definition_id');
            $table->foreign('wf_definition_id', 'wf_definition_track_ibfk_1')->references('id')->on('wf_definitions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->integer('wf_module_id')->unsigned()->index('wf_module_id');
            $table->integer('level');
            $table->foreign('wf_module_id', 'wf_tracks_ibfk_2')->references('id')->on('wf_modules')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->dropForeign('wf_definition_track_ibfk_1', 'wf_definition_track_ibfk_1');
            $table->dropIndex('wf_definition_id', 'wf_definition_id');
            $table->dropColumn(['wf_definition_id']);
        });
        Schema::rename('user_wf_definition', 'wf_groups');
        Schema::table('wf_groups', function (Blueprint $table) {
            $table->integer('wf_module_id')->unsigned()->index('wf_module_id');
            $table->integer('level');
            $table->foreign('wf_module_id', 'wf_groups_ibfk_1')->references('id')->on('wf_modules')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->dropForeign('wf_definition_user_ibfk_1', 'wf_definition_user_ibfk_1');
            $table->dropIndex('wf_definition_id', 'wf_definition_id');
            $table->dropColumn(['wf_definition_id']);
        });
    }
}
