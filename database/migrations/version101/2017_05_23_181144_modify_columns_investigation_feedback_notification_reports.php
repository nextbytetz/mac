<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsInvestigationFeedbackNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->renameColumn('investgation_validity', 'investigation_validity');
        });

        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->renameColumn('incident_id', 'notification_report_id');
        });

        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->renameColumn('investgation_question_id', 'investigation_question_id');
        });

        Schema::table('investigation_feedbacks', function(Blueprint $table)
        {
            $table->foreign('notification_report_id', 'notification_report_ibfk_1')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->renameColumn('investigation_validity', 'investgation_validity');
        });

        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'incident_id');
        });

        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->renameColumn('investigation_question_id', 'investgation_question_id');
        });

        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->dropForeign('notification_report_ibfk_1');
        });
    }
}
