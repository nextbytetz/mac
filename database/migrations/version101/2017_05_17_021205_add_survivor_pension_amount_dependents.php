<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSurvivorPensionAmountDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("alter table dependents add survivor_pension_amount decimal  NULL  after survivor_pension_percent ");
        Schema::table('dependents', function (Blueprint $table) {
            $table->decimal('survivor_pension_amount', 15)->nullable()->comment('amount of pension the dependent is eligible to be paid.')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dependents', function (Blueprint $table) {
            $table->dropColumn('survivor_pension_amount');
        });
    }
}
