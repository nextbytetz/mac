<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateRanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->dropColumn('payroll_proc_id');
                  });
    }
}
