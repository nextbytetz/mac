<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNameAllNotificationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename("notifications", "notification_reports");

        Schema::table('accidents', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('notification_health_providers', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('diseases', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('deaths', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('notification_investigators', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('notification_health_states', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

        Schema::table('claims', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::rename("notification_reports", "notifications");

        Schema::table('accidents', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('notification_health_providers', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('diseases', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('deaths', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('notification_investigators', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('notification_health_states', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

        Schema::table('claims', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });

    }
}
