<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIswrittenoffBookingInterest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->dropForeign('write_off_ibfk_1', 'write_off_ibfk_1');
            $table->dropIndex(['write_off_id']);
            $table->dropColumn(('write_off_id'));

        });

        Schema::table('booking_interests', function (Blueprint $table) {
            DB::statement("alter table booking_interests add write_off_id integer  after hasreceipt ");
            $table->integer('write_off_id')->comment('Batch id for interest written off')->unsigned()
                ->nullable()->change();
            $table->foreign('write_off_id', 'write_off_ibfk_1')->references('id')->on('interest_write_offs')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->index('write_off_id');


            DB::statement("alter table booking_interests add iswrittenoff integer   after hasreceipt ");
            $table->integer('iswrittenoff')->comment('Flag to imply interest is written off')->unsigned()->default(0)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->dropIndex(['write_off_id']);
            $table->dropForeign('write_off_ibfk_1');
            $table->dropColumn('iswrittenoff');
        });
    }
}
