<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatesOnInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->date("start_date")->change();
            $table->dateTime("cancel_date")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->dateTime("start_date")->change();
            $table->date("cancel_date")->nullable()->change();
        });
    }
}
