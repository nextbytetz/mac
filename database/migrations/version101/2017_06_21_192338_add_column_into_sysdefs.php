<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntoSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->integer('day_hours')->nullable()->comment('Hours worked per day by employee to be used in claim assessment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->dropColumn('day_hours');

        });
    }
}
