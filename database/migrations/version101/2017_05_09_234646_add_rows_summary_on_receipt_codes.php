<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRowsSummaryOnReceiptCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->smallInteger('rows_imported')->nullable()->after('linked_file')->comment("Total number of rows from the imported excel file which has already been stored in the database table (contribution_temps), if this receipt is for monthly contribution");
            $table->smallInteger("total_rows")->after("rows_imported")->nullable()->comment("Total number of rows from the imported excel file, if this receipt is for monthly contribution");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->dropColumn('rows_imported');
            $table->dropColumn('total_rows');
        });
    }
}
