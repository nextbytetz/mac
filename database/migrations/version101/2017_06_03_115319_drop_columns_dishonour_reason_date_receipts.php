<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsDishonourReasonDateReceipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('receipts', function (Blueprint $table) {
            $table->dropColumn('dishonour_date');
            $table->dropColumn('dishonour_user_id');
            $table->dropColumn('dishonour_reason');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("alter table receipts add dishonor_reason text null after isdishonered");
        DB::statement("alter table receipts add dishonor_user_id integer unsigned null after dishonor_reason");
        DB::statement("alter table receipts add dishonor_date dateTime null after dishonor_user_id");
    }
}

