<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeToDateTypeToDateNotificationHealthServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_health_provider_services', function(Blueprint $table)
        {
            $table->date('to_date')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_health_provider_services', function(Blueprint $table)
        {
            $table->integer('to_date')->nullable()->change();

        });
    }
}
