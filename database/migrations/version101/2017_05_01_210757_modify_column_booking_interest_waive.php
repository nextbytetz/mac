<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnBookingInterestWaive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->decimal('waive_amount', 14)->nullable()->change();
            $table->text('waive_note', 65535)->nullable()->change();
            $table->integer('waive_user')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->decimal('waive_amount', 14)->change();
            $table->text('waive_note', 65535)->change();
            $table->integer('waive_user')->change();
        });
    }
}
