<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBookingAdjustment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("alter table bookings add isadjusted boolean default 0 not NULL  after iscomplete ");
        DB::statement("alter table bookings add adjust_amount decimal  after isadjusted ");
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('isadjusted')->default(0)->comment('Flag to show whether this booking has already been adjusted; has finished workflow')->nullable()->change();
            $table->decimal('adjust_amount', 14)->comment('Specify amount which has been adjusted.')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('isadjusted');
            $table->dropColumn('adjust_amount');
        });
    }
}
