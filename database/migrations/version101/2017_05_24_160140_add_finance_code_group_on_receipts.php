<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinanceCodeGroupOnReceipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function (Blueprint $table) {
            $table->integer('fin_code_id')->unsigned()->after("bank_id")->nullable()->index("fin_code_id");
        });
        Schema::table('receipts', function (Blueprint $table) {
            $table->foreign('fin_code_id', 'receipts_ibfk_6')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipts', function (Blueprint $table) {
            $table->dropForeign('receipts_ibfk_6', 'receipts_ibfk_6');
            $table->dropIndex('fin_code_id', 'fin_code_id');
            $table->dropColumn('fin_code_id');
        });
    }
}
