<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnParentIdDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropForeign('dependents_ibfk_8','dependents_ibfk_8');
            $table->dropIndex('parent_id','parent_id');

            $table->dropColumn("parent_id");

        });

        Schema::table('dependents', function(Blueprint $table)
        {
                      $table->integer('parent_id')->nullable()->unsigned()->index('parent_id');
            $table->foreign('parent_id', 'dependents_ibfk_8')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropIndex('parent_id','parent_id');
            $table->dropForeign('dependents_ibfk_8','dependents_ibfk_8');
            $table->integer('parent_id')->index('parent_id')->change();
            $table->foreign('parent_id', 'dependents_ibfk_8')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }
}
