<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameDishonouredchequesColumnAndAddingNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dishonoured_cheques', function (Blueprint $table) {
            $table->renameColumn('user_id', 'dishonour_user_id')->comment('user who dishonours cheque');
            $table->integer('replacer_user_id')->nullable()->unsigned()->index('replacer_user_id')->comment('user who replaces the cheque');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dishonoured_cheques', function (Blueprint $table) {
            $table->renameColumn('user_id', 'dishonour_user_id');
            $table->integer('replacer_user_id')->nullable()->unsigned()->index('replacer_user_id');
        });
    }
}

