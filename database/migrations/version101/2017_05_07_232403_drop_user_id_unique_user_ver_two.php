<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUserIdUniqueUserVerTwo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_ver_twos', function(Blueprint $table)
        {
            $table->dropUnique('user_id');
            $table->unique('user_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_ver_twos', function(Blueprint $table)
        {
            $table->dropUnique('user_id', 'user_id');
            $table->unique(['user_id','email','phone','external_id'], 'user_id');
        });
    }
}
