<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntoPaymentVouchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id')->after('fin_account_code_id');
        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_id', 'payroll_proc_foreign_key')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');
              });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropForeign('payroll_proc_foreign_key');
            $table->dropIndex('payroll_proc_id');
            $table->dropColumn('payroll_proc_id');
        });
    }
}
