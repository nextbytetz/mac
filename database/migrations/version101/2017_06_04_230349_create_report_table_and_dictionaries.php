
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTableAndDictionaries extends Migration
{
  /**
   * Run the migrations.
    *
   * @return void
    */

   public function up()
   {
      //
       Schema::create('report_types', function(Blueprint $table)
        {
           $table->increments('id');
           $table->string('name', 100)->unique('name');
           $table->timestamps();
           $table->softDeletes();
       });
          Schema::create('report_categories', function(Blueprint $table)
     {
          $table->increments('id');
           $table->string('name', 100)->unique('name');          $table->timestamps();
          $table->softDeletes();
     });

       Schema::create('reports', function(Blueprint $table)
        {
            $table->increments('id');
          $table->string('name', 100)->unique('name')->comment('Name of the report');
           $table->string('url_name', 100)->unique('url_name')->comment('URL name field to be included in route url to call method for this report i.e /reports/finance/{url_name}');
           $table->integer('report_type_id')->unsigned()->index('report_type_id');
           $table->integer('report_category_id')->unsigned()->index('report_category_id');
           $table->text('description', 65535)->comment('description of the report and result to be retrieved');
          $table->timestamps();
          $table->softDeletes();
     });

       Schema::table('reports', function(Blueprint $table)
      {           $table->foreign('report_type_id', 'report_type_ibfk_1')->references('id')->on('report_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
           $table->foreign('report_category_id', 'report_category_ibfk_2')->references('id')->on('report_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
       });

   }

  /**
     * Reverse the migrations.
    *
     * @return void
    */
   public function down()
    {
            //
            Schema::table('reports', function(Blueprint $table)
        {
           $table->dropForeign('report_category_ibfk_2');
           $table->dropForeign('report_type_ibfk_1');
       });

        Schema::drop('report_types');
       Schema::drop('report_categories');
       Schema::drop('reports');
   }
}