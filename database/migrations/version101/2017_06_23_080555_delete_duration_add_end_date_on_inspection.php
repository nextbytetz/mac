<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteDurationAddEndDateOnInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->dropColumn("duration");
            $table->date("end_date")->nullable()->after("start_date");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->smallInteger("duration")->after("start_date");
            $table->dropColumn("end_date");
        });
    }
}
