<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnBookingInterestHasreceiptAndSysdef extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
              DB::statement("alter table booking_interests add hasreceipt boolean default 0 not NULL  after ispaid ");
            Schema::table('booking_interests', function (Blueprint $table) {
            $table->boolean('hasreceipt')->default(0)->comment('Flag to show whether this booking has already been receipted')->nullable()->change();
                   });

      }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->dropColumn('hasreceipt');
                     });

    }
}
