<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountImportedInReceiptCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->decimal('amount_imported', 14)->after("mime")->nullable()->comment("total amount computed from the gross pay in the uploaded file. After the linked file has been uploaded.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->dropColumn('amount_imported');
        });
    }
}
