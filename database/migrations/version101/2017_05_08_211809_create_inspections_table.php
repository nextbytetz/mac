<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inspections', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('inspection_type_id')->unsigned()->index('inspection_type_id');
			$table->text('comments', 65535)->nullable();
			$table->dateTime('start_date')->comment('date when the inspection will be started.');
			$table->smallInteger('duration')->comment('duration of the inspection in terms of days.');
			$table->boolean('iscancelled')->default(0)->comment('set whether the inspection has been cancelled or not, 1 - inspection cancelled, 0 - inspection not cancelled.');
			$table->text('cancel_reason', 65535)->nullable()->comment('set the reason for canceling the inspection entity.');
			$table->integer('cancel_user')->unsigned()->comment('user who have canceled the inspection.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inspections');
	}

}
