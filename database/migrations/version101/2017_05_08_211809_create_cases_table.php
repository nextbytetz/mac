<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cases', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 250)->comment('title of the title');
			$table->string('number', 50)->comment('specific number of the case');
			$table->string('complainant', 100)->comment('name of the complainant of the case');
			$table->integer('district_id')->unsigned()->index('district_id')->comment('district location of the case');
			$table->string('location', 200)->comment('specific location of the case');
			$table->smallInteger('court_category_id')->unsigned()->index('court_category_id');
			$table->string('court', 200)->nullable()->comment('name of the court which the case has been listened.');
			$table->decimal('fee', 14)->nullable()->comment('amount of money associated with the case');
			$table->text('liability', 65535)->nullable()->comment('Any liability associated with the case');
			$table->text('respondent', 65535)->nullable()->comment('Respondent of the case');
			$table->text('fact', 65535)->nullable()->comment('Any brief fact associated with the case.');
			$table->boolean('is_archived')->default(0)->comment('Set whether the case has been set in an archive mode or not, 1 - case is in archive mode, 0 case not in an archive mode.');
			$table->dateTime('filling_date')->nullable()->comment('date and time which the case has been filled.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cases');
	}

}
