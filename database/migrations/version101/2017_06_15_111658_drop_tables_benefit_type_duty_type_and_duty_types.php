<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTablesBenefitTypeDutyTypeAndDutyTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('benefit_type_duty_type');
        Schema::drop('notification_duties');
        Schema::drop('duty_types');

        Schema::table('disability_state_checklists', function (Blueprint $table) {
            $table->integer('benefit_type_id')->nullable()->unsigned()->after('name')->index('benefit_type_id');
        });
        Schema::table('disability_state_checklists', function (Blueprint $table) {
            $table->foreign('benefit_type_id', 'benefit_type_duty_type_ibfk_1')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::create('duty_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->unique('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('benefit_type_duty_type', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->integer('benefit_type_id')->unsigned()->index('benefit_type_id');
            $table->integer('duty_type_id')->unsigned()->index('duty_type_id');
            $table->timestamps();
            $table->foreign('benefit_type_id', 'benefit_type_duty_type_ibfk_1')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('duty_type_id', 'benefit_type_duty_type_ibfk_2')->references('id')->on('duty_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        Schema::table('disability_state_checklists', function (Blueprint $table) {
            $table->dropColumn('disability_state_checklists');
        });

    }
}