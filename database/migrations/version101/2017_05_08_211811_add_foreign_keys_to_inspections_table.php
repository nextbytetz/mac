<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInspectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inspections', function(Blueprint $table)
		{
			$table->foreign('inspection_type_id', 'inspections_ibfk_1')->references('id')->on('inspection_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inspections', function(Blueprint $table)
		{
			$table->dropForeign('inspections_ibfk_1');
		});
	}

}
