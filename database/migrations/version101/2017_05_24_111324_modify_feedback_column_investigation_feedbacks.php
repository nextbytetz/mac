<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyFeedbackColumnInvestigationFeedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->text('feedback')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->string('feedback',20)->change();

        });
    }
}
