<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermissionDependencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_dependencies', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('permission_id')->unsigned()->index('permission_id');
            $table->integer('dependency_id')->unsigned()->index('dependency_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('permission_id', 'permission_dependencies_ibfk_1')->references('id')->on('permissions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('dependency_id', 'permission_dependencies_ibfk_2')->references('id')->on('permissions')->onUpdate('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permission_dependencies');
    }
}
