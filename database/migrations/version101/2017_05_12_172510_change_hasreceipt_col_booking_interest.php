<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeHasreceiptColBookingInterest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            DB::statement("alter table booking_interests add billing_date datetime null after hasreceipt ");

            $table->dateTime('billing_date')->comment('date which the interest has been billed')->nullable()->change();
            $table->dropColumn('hasreceipt');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->dropColumn('billing_date');
            $table->boolean('hasreceipt')->comment('Flag to show if booking has a receipt / paid')
                ->nullable()->default(0);
        });
    }
}
