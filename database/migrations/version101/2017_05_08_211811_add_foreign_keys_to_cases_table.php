<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cases', function(Blueprint $table)
		{
			$table->foreign('court_category_id', 'cases_ibfk_1')->references('id')->on('court_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id', 'cases_ibfk_2')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cases', function(Blueprint $table)
		{
			$table->dropForeign('cases_ibfk_1');
            $table->dropForeign('cases_ibfk_2');
		});
	}

}
