<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinimumAllowedContributionSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->integer('minimum_allowed_contribution')->nullable()->default(1000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->dropColumn('minimum_allowed_contribution');
        });
    }
}
