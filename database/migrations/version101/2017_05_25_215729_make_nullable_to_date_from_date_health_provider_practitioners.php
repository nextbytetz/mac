<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullableToDateFromDateHealthProviderPractitioners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_health_provider_practitioners', function(Blueprint $table)
        {

            $table->date('from_date')->nullable()->change();
            $table->date('to_date')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('notification_health_provider_practitioners', function(Blueprint $table)
        {
        $table->date('from_date')->change();
        $table->date('to_date')->change();
        });
    }
}
