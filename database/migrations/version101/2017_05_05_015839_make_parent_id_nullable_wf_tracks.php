<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\DisableForeignKeys;

class MakeParentIdNullableWfTracks extends Migration
{
    use DisableForeignKeys;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropForeign('wf_tracks_ibfk_3', 'wf_tracks_ibfk_3');
            $table->dropIndex('parent_id', 'parent_id');
        });
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->nullable()->change();
        });
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->index('parent_id', 'parent_id');
            $table->foreign('parent_id', 'wf_tracks_ibfk_3')->references('id')->on('wf_tracks')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropForeign('wf_tracks_ibfk_3', 'wf_tracks_ibfk_3');
            $table->dropIndex('parent_id', 'parent_id');
        });
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->change();
        });
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->index('parent_id', 'parent_id');
            $table->foreign('parent_id', 'wf_tracks_ibfk_3')->references('id')->on('wf_tracks')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }
}
