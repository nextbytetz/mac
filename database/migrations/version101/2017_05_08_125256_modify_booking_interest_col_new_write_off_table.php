<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyBookingInterestColNewWriteOffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
        $table->renameColumn('iswaived', 'isadjusted')->comment('set whether this penalty is adjusted or not, 1 - adjusted, 0 - not adjusted.');
        $table->renameColumn('waive_amount', 'adjust_amount')->comment('Specify amount which has been waived.');
        $table->renameColumn('waive_note', 'adjust_note')->comment('Note on the adjusted amount');
        $table->renameColumn('waive_user', 'adjust_user_id')->comment('staff who has adjusted the interest amount.');
//            add new column
        $table->integer('write_off_id')->unsigned()->comment('Batch id for interest written off at the same time.!')->nullable();

    });

        // Create table booking_interest_wirte off
        Schema::create('interest_write_offs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('staff who has written off interest.');
            $table->text('description', 65535)->comment('Note on the reason of writing off interest');
            $table->timestamps();
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->renameColumn('isadjusted', 'iswaived');
            $table->renameColumn('adjust_amount', 'waive_amount');
            $table->renameColumn('adjust_note', 'waive_note');
            $table->renameColumn('adjust_user_id', 'waive_user');

            $table->dropColumn('write_off_id');
        });
// drop table booking_interest_wirte off
            Schema::drop('interest_write_offs');
    }
}
