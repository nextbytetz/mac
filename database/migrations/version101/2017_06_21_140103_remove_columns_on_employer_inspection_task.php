<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsOnEmployerInspectionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->dropColumn("iscancelled");
            $table->dropColumn("cancel_reason");
            $table->dropColumn("cancel_user");
            $table->dropColumn("deleted_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_inspection_task', function(Blueprint $table) {
            $table->boolean('iscancelled')->default(0)->comment('set whether the inspection has been cancelled or not, 1 - has been cancelled, 0 - has not been cancelled.');
            $table->text('cancel_reason', 65535)->nullable()->comment('reason for canceling a employer task.');
            $table->integer('cancel_user')->unsigned()->nullable()->comment('user who has cancelled the employer task.');
            $table->softDeletes();
        });
    }
}
