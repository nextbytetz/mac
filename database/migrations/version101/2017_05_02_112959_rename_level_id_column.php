<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameLevelIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->renameColumn('level_id', 'level');
        });
        Schema::table('wf_groups', function (Blueprint $table) {
            $table->renameColumn('level_id', 'level');
        });
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->renameColumn('level_id', 'level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->renameColumn('level', 'level_id');
        });
        Schema::table('wf_groups', function (Blueprint $table) {
            $table->renameColumn('level', 'level_id');
        });
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->renameColumn('level', 'level_id');
        });
    }
}
