<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusDateRanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->integer('status_id')->unsigned()->index('status_id')->comments('Status ; 0 => Pending, 1=> approved, Rejected => 2');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->dropColumn('status_id');
        });
    }
}
