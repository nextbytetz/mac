<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnInterestWriteOff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->renameColumn('write_off_id', 'interest_write_off_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function (Blueprint $table) {
            $table->renameColumn('interest_write_off_id', 'write_off_id');
        });
    }
}
