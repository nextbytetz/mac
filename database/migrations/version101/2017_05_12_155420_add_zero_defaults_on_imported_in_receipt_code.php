<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZeroDefaultsOnImportedInReceiptCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->smallInteger('rows_imported')->nullable()->after("linked_file")->comment("Total number of rows from the imported excel file which has already been stored in the database table (contribution_temps), if this receipt is for monthly contribution")->default(0)->change();
            $table->smallInteger('total_rows')->nullable()->after("rows_imported")->comment("Total number of rows from the imported excel file, if this receipt is for monthly contribution")->default(0)->change();
            $table->decimal('amount_imported', 14)->nullable()->after("total_rows")->comment("total amount computed from the gross pay in the uploaded file. After the linked file has been uploaded.")->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->smallInteger('rows_imported')->nullable()->after("linked_file")->comment("Total number of rows from the imported excel file which has already been stored in the database table (contribution_temps), if this receipt is for monthly contribution")->change();
            $table->smallInteger('total_rows')->nullable()->after("rows_imported")->comment("Total number of rows from the imported excel file, if this receipt is for monthly contribution")->change();
            $table->decimal('amount_imported', 14)->nullable()->after("total_rows")->comment("total amount computed from the gross pay in the uploaded file. After the linked file has been uploaded.")->change();
        });
    }
}
