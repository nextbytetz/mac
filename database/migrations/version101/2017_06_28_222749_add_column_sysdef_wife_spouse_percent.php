<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSysdefWifeSpousePercent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->decimal('wife_spouse_percent', 4)->nullable()->after('assistant_percent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->dropColumn('wife_spouse_percent');
        });
    }

}
