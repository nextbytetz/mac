<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerIdOnLawyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lawyers', function(Blueprint $table)
        {
            $table->renameColumn("othernames", "middlename");

        });
        Schema::table('lawyers', function(Blueprint $table)
        {
            $table->string('middlename', 50)->nullable()->change();
            $table->string("lastname", 50)->nullable()->after("middlename");
            $table->integer("employer_id")->unsigned()->index("employer_id")->nullable()->after("id");
            $table->string("firstname", 50)->nullable()->change();
            $table->string("external_id", 30)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lawyers', function(Blueprint $table)
        {
            $table->string("external_id", 30)->change();
            $table->string("firstname", 50)->change();
            $table->dropIndex("employer_id");
            $table->dropColumn("employer_id");
            $table->dropColumn("lastname");
            $table->renameColumn("middlename", "othernames");

        });
        Schema::table('lawyers', function(Blueprint $table)
        {
            $table->string("othernames", 80)->change();
        });
    }
}
