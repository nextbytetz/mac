<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaseHearingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_hearings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('case_id')->unsigned()->index('case_id');
			$table->dateTime('hearing_date')->nullable();
			$table->integer('lawyer_id')->unsigned()->index('lawyer_id');
			$table->decimal('fee', 14)->nullable();
			$table->boolean('status')->default(0)->comment('set the status whether the case has been heard or not, 1 - case has been heard, 0 case has not been heard.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_hearings');
	}

}
