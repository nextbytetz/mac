<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateRange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
                      $table->integer('incident_type_id')->unsigned()->index('incident_type_id');
            $table->integer('region_id')->unsigned()->index('region_id');
            $table->integer('business_id')->unsigned()->index('business_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->dropColumn('incident_type_id');
            $table->dropColumn('region_id');
            $table->dropColumn('business_id');
        });
    }
}
