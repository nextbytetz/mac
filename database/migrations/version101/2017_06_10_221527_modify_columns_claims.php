<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsClaims extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claims', function(Blueprint $table)
        {
            $table->boolean('day_hours')->nullable()->change();
            $table->decimal('pi', 4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('claims', function(Blueprint $table)
        {
            $table->boolean('day_hours')->change();
            $table->decimal('pi', 4)->change();
        });
    }
}
