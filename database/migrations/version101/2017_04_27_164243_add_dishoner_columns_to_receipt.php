<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDishonerColumnsToReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("alter table receipts add dishonor_reason text null after isdishonered");
        DB::statement("alter table receipts add dishonor_user_id integer unsigned null after dishonor_reason");
        DB::statement("alter table receipts add dishonor_date dateTime null after dishonor_user_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('receipts', function (Blueprint $table) {
            $table->dropColumn('dishonor_reason');
            $table->dropColumn('dishonor_user_id');
            $table->dropColumn('dishonor_date');
        });

    }
}



