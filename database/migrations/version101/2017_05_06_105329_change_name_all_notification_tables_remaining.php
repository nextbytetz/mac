<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNameAllNotificationTablesRemaining extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_duties', function (Blueprint $table) {
            $table->renameColumn('notification_id', 'notification_report_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_duties', function (Blueprint $table) {
            $table->renameColumn('notification_report_id', 'notification_id');
        });
    }
}
