<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompleteStatusOnInspection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->tinyInteger("complete_status")->nullable()->after("cancel_date")->default(0)->comment("set the complete status of the inspection, 1 - inspection has been completed, 0 - inspection has not yet been completed.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspections', function(Blueprint $table) {
            $table->dropColumn("complete_status");
        });
    }
}
