<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyBookingInterestColumnsConstrined extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->integer('adjust_user_id')->unsigned()->nullable()->change();
            $table->index('write_off_id');
            $table->foreign('write_off_id', 'write_off_ibfk_1')->references('id')->on('interest_write_offs')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('interest_write_offs', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable()->change();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->integer('adjust_user_id')->nullable()->change();
            $table->dropIndex('write_off_id');
            $table->dropForeign('write_off_ibfk_1');
        });

        Schema::table('interest_write_offs', function (Blueprint $table) {
            $table->integer('user_id')->change();
        });

    }
}
