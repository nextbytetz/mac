<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFillingDateOnCaseHearings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('case_hearings', function(Blueprint $table)
        {
            $table->date("filling_date")->after("fee")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_hearings', function(Blueprint $table)
        {
            $table->dropColumn("filling_date");
        });
    }
}
