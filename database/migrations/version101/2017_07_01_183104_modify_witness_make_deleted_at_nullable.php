<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyWitnessMakeDeletedAtNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->dropColumn('deleted_at');
        });
        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}
