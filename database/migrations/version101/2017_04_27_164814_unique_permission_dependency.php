<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UniquePermissionDependency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permission_dependencies', function (Blueprint $table) {
            $table->unique(['permission_id', 'dependency_id'], 'perm_dep_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permission_dependencies', function (Blueprint $table) {
            $table->dropUnique('perm_dep_unique');
        });
    }
}
