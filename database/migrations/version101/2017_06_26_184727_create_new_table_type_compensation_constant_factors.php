<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTableTypeCompensationConstantFactors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('compensation_payment_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('claim_compensations', function(Blueprint $table)
        {
                   $table->integer('pay_period')->after('recycl')->comment('in months, number of months this compensation will be paid');

        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('compensation_payment_types');

        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->dropColumn('pay_period');

        });

    }
}
