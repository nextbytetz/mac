<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropcolumnsRenameMedicalExpensesModifyHealthProviderService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->dropForeign('medical_expenses_ibfk_3', 'medical_expenses_ibfk_3');
            $table->dropIndex('insurance_id', 'insurance_id');
            $table->dropColumn('insurance_id');
            $table->renameColumn('employer_id', 'resource_id');
        });

        Schema::table('notification_health_provider_services', function (Blueprint $table) {
            $table->integer('medical_expense_id')->unsigned()->index('medical_expense_id');
        });
        Schema::table('notification_health_provider_services', function(Blueprint $table)
        {
            $table->foreign('medical_expense_id', 'notification_health_provider_services_medical_expense_ibfk_3')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->integer('insurance_id')->unsigned()->nullable()->index('insurance_id');
            $table->renameColumn('resource_id', 'employer_id');

        });
        Schema::table('medical_expenses', function(Blueprint $table)
        {
            $table->foreign('insurance_id', 'medical_expenses_ibfk_3')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('notification_health_provider_services', function (Blueprint $table) {
            $table->dropForeign('notification_health_provider_services_medical_expense_ibfk_3', 'notification_health_provider_services_medical_expense_ibfk_3');
               $table->dropIndex('medical_expense_id', 'medical_expense_id');
             $table->dropColumn('medical_expense_id');

        });
    }
}
