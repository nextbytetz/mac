<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnClaimCompensationsTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claim_compensations', function(Blueprint $table)
        {
                     $table->integer('compensation_payment_type_id')->unsigned()->after('ispaid')->index('compensation_payment_type_id');

        });

        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->foreign('compensation_payment_type_id', 'compensation_payment_type_id_foreign')->references('id')->on('compensation_payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->dropForeign('compensation_payment_type_id_foreign', 'compensation_payment_type_id_foreign');

        });
        Schema::table('claim_compensations', function(Blueprint $table)
        {
            $table->dropColumn('compensation_payment_type_id');

        });
    }
}
