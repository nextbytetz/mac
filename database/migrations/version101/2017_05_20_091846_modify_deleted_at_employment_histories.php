<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDeletedAtEmploymentHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employment_histories', function(Blueprint $table)
        {
$table->dropColumn('deleted_at');

        });

        Schema::table('employment_histories', function(Blueprint $table)
        {
                      $table->softDeletes();
        });

}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('employment_histories', function(Blueprint $table)
        {
            $table->integer('deleted_at')->nullable()->change();
        });
    }
}
