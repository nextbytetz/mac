<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusAndParentOnCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function(Blueprint $table)
        {
            $table->integer('parent_id')->unsigned()->index('parent_id')->after('id')->nullable();
            $table->foreign('parent_id')->references('id')->on('cases')->onUpdate('CASCADE')->onDelete('cascade');
            $table->tinyInteger('status')->after('id')->nullable()->default(0)->comment("check the status of the case whether is open or closed. 1 - Closed, 0 - Open");
            $table->text('complainant_address')->after('complainant')->nullable()->comment("Address of the complainant");
            $table->text('respondent_address')->after('respondent')->nullable()->comment("Address of the respondent");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cases', function(Blueprint $table)
        {
            $table->dropForeign("cases_parent_id_foreign");
            $table->dropIndex("parent_id");
            $table->dropColumn("parent_id");
            $table->dropColumn("status");
            $table->dropColumn("complainant_address");
            $table->dropColumn("respondent_address");
        });
    }
}
