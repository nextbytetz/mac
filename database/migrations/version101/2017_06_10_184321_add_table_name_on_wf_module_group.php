<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableNameOnWfModuleGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_module_groups', function (Blueprint $table) {
            $table->string('table_name', 50)->after('name')->nullable();
        });
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropColumn("table_name");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->string("table_name", 50)->after("resource_id");
        });
        Schema::table('wf_module_groups', function (Blueprint $table) {
            $table->dropColumn("table_name");
        });
    }
}
