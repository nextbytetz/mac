<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceiptNumberEmployeeNumberSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->tinyInteger('employee_number_length')->nullable()->default(8);
            $table->tinyInteger('receipt_number_length')->nullable()->default(8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->dropColumn('employee_number_length');
            $table->dropColumn('receipt_number_length');
        });
    }
}
