<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerIdIntoDateRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
                  $table->integer('employer_id')->unsigned()->index('employer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->dropColumn('employer_id');
        });
    }
}
