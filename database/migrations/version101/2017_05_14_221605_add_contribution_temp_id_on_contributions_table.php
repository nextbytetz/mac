<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContributionTempIdOnContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->tinyInteger('isuploaded')->after("upload_error")->nullable()->default(0)->comment("store the status whether the contributions for this receipt has already been uploaded or not yet, 1 - already updated, 0 - not yet uploaded completely");
        });
        Schema::table('contributions', function (Blueprint $table) {
            $table->bigInteger('contribution_temp_id')->after("isactive")->nullable()->unsigned()->index("contribution_temp_id");
            $table->foreign('contribution_temp_id', 'contributions_ibfk_2')->references('id')->on('contribution_temps')->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_codes', function (Blueprint $table) {
            $table->dropColumn('isuploaded');
        });
        Schema::table('contributions', function (Blueprint $table) {
            $table->dropIndex('contribution_temp_id', 'contribution_temp_id');
            $table->dropForeign('contributions_ibfk_2', 'contributions_ibfk_2');
            $table->dropColumn('contribution_temp_id');
        });
    }
}
