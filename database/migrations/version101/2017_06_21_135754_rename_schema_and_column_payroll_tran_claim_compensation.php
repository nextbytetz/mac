<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameSchemaAndColumnPayrollTranClaimCompensation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::rename('payroll_trans', 'payroll_runs');

        Schema::table('claim_compensations', function (Blueprint $table) {
            $table->renameColumn('pay_period', 'recycl');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::rename('payroll_runs', 'payroll_trans');

        Schema::table('claim_compensations', function (Blueprint $table) {
            $table->renameColumn('recycl', 'pay_period');
        });

    }
}
