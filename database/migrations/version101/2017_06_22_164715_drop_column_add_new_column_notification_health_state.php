<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnAddNewColumnNotificationHealthState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_health_states', function(Blueprint $table) {

            $table->dropForeign('notification_health_states_ibfk_1', 'notification_health_states_ibfk_1');
            $table->dropIndex('medical_expense_id','medical_expense_id');
            $table->dropColumn('medical_expense_id');
        });

        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->integer('notification_report_id')->unsigned()->after('id')->index('notification_report_id');
        });

        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->foreign('notification_report_id', 'notification_report_foreign')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->integer('medical_expense_id')->unsigned()->index('medical_expense_id')->change();
        });
        Schema::table('notification_health_states', function(Blueprint $table)
        {
            $table->foreign('medical_expense_id', 'notification_health_states_ibfk_1')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('notification_health_states', function(Blueprint $table) {

            $table->dropForeign('notification_report_foreign', 'notification_report_foreign');
            $table->dropIndex('notification_report_id','notification_report_id');
            $table->dropColumn('notification_report_id');
        });
    }
}
