<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDishonorChequeColumnsToReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('receipts', function (Blueprint $table) {
                $table->renameColumn('isdishonered', 'isdishonoured');
            $table->renameColumn('dishonor_reason', 'dishonour_reason');
            $table->renameColumn('dishonor_user_id', 'dishonour_user_id');
            $table->renameColumn('dishonor_date', 'dishonour_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('receipts', function (Blueprint $table) {
            $table->renameColumn('isdishonoured', 'isdishonered');
            $table->renameColumn('dishonour_reason', 'dishonor_reason');
            $table->renameColumn('dishonour_user_id', 'dishonor_user_id');
            $table->renameColumn('dishonour_date', 'dishonor_date');
        });
    }
}
