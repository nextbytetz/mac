<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPaymentVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('benefit_resource_id')->unsigned()->after('id')->comment('Primary key of the benefit resource table, a table which is being tracked for.');
            $table->integer('member_type_id')->unsigned()->index('member_type_id')->after('resource_id')->comment('which member type this compensation is concerned');
        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->foreign('member_type_id', 'member_type_id_foreign_key')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropForeign('member_type_id_foreign_key');
            $table->dropIndex('member_type_id');
            $table->dropColumn('member_type_id');
            $table->dropColumn('benefit_resource_id');
        });
    }
}
