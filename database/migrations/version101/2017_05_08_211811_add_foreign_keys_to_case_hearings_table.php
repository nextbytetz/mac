<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCaseHearingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('case_hearings', function(Blueprint $table)
		{
			$table->foreign('case_id', 'case_hearings_ibfk_1')->references('id')->on('cases')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('lawyer_id', 'case_hearings_ibfk_2')->references('id')->on('lawyers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('case_hearings', function(Blueprint $table)
		{
			$table->dropForeign('case_hearings_ibfk_1');
			$table->dropForeign('case_hearings_ibfk_2');
		});
	}

}
