<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployerInspectionTaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employer_inspection_task', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('employer_id')->unsigned()->index('employer_id');
			$table->integer('inspection_task_id')->unsigned()->index('inspection_task_id');
			$table->dateTime('start_date')->comment('date which this inspection task should start');
			$table->smallInteger('duration')->comment('number of days which this inspection should last in days');
			$table->integer('user_id')->unsigned()->index('user_id')->comment('Staff user who has been assigned to do the employer inspection task');
			$table->boolean('iscancelled')->default(0)->comment('set whether the inspection has been cancelled or not, 1 - has been cancelled, 0 - has not been cancelled.');
			$table->text('cancel_reason', 65535)->nullable()->comment('reason for canceling a employer task.');
			$table->integer('cancel_user')->unsigned()->nullable()->comment('user who has cancelled the employer task.');
			$table->text('delay_reasons', 65535)->nullable()->comment('Any reasons which might have caused the inspection to delay completion.');
			$table->text('extra_findings', 65535)->nullable()->comment('Extra findings that might have been discovered during inspection, which were not part pf the inspection tasks and targets.');
			$table->boolean('status')->default(0)->comment('set the status whether the inspection task has completed or not, 1 - inspection task has completed, 0 - inspection task has not yet completed.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employer_inspection_task');
	}

}
