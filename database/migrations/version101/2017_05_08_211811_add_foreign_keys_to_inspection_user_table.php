<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInspectionUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inspection_user', function(Blueprint $table)
		{
			$table->foreign('inspection_id', 'inspection_user_ibfk_1')->references('id')->on('inspections')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inspection_user', function(Blueprint $table)
		{
			$table->dropForeign('inspection_user_ibfk_1');
		});
	}

}
