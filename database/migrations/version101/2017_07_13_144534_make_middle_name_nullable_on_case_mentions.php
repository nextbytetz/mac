<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMiddleNameNullableOnCaseMentions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('case_mentions', function(Blueprint $table)
        {
            $table->string("middlename", 40)->nullable()->comment("middle name of the case personnel during the mention")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_mentions', function(Blueprint $table)
        {
            $table->string("middlename", 40)->comment("middle name of the case personnel during the mention")->change();
        });
    }
}
