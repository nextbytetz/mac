<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrollProcTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_proc_types', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 150);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('payroll_procs', function(Blueprint $table) {

            $table->integer('payroll_proc_type_id')->unsigned()->after('bquarter')->index('payroll_proc_type_id')->comment('Payment proc type i.e lupm sum, monthly pension');

        });

        Schema::table('payroll_procs', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_type_id', 'payroll_proc_type_foreign_key')->references('id')->on('payroll_proc_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payroll_procs', function(Blueprint $table)
        {
            $table->dropForeign('payroll_proc_type_id');
            $table->dropColumn('payroll_proc_type_id');
        });

        Schema::drop('payroll_proc_types');
    }
}
