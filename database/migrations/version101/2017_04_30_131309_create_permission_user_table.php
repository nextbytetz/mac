<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('permission_id')->unsigned()->index('permission_id');
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->timestamps();
            $table->foreign('permission_id', 'permission_user_ibfk_1')->references('id')->on('permissions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permission_user');
    }
}
