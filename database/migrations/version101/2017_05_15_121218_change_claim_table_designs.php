<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeClaimTableDesigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::rename("medical_expense_types", "member_types");
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->dropColumn('employee_id');
            $table->integer('user_id')->unsigned()->index('user_id')->after('amount')->comment('staff who has created this medical expense');
            $table->renameColumn('medical_expense_type_id', 'member_type_id');
        });
        Schema::table('claim_compensations', function (Blueprint $table) {
            $table->integer('member_type_id')->unsigned()->index('member_type_id')->after('claim_id')->comment('which member type this compensation is conserned');
            $table->foreign('member_type_id', 'claim_compensations_ibfk_4')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('notification_duties', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index('user_id')->after('day_hours')->comment('staff who has created this notification duty');
        });
        Schema::table('claims', function (Blueprint $table) {
            $table->decimal('pi', 4)->after('exit_code_id')->comment('Percentage of permanent impairment for this claim notification');
        });
        Schema::create('benefit_type_duty_type', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->integer('benefit_type_id')->unsigned()->index('benefit_type_id');
            $table->integer('duty_type_id')->unsigned()->index('duty_type_id');
            $table->timestamps();
            $table->foreign('benefit_type_id', 'benefit_type_duty_type_ibfk_1')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('duty_type_id', 'benefit_type_duty_type_ibfk_2')->references('id')->on('duty_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('diseases', function (Blueprint $table) {
            $table->integer('disease_know_how_id')->unsigned()->after('receipt_date')->index('disease_know_how_id')->comment('How this desease was discovered');
            $table->foreign('disease_know_how_id', 'diseases_ibfk_2')->references('id')->on('disease_know_hows')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diseases', function (Blueprint $table) {
            $table->dropIndex('disease_know_how_id','disease_know_how_id');
            $table->dropForeign('diseases_ibfk_2', 'diseases_ibfk_2');
            $table->dropColumn('disease_know_how_id');
        });
        Schema::drop('benefit_type_duty_type');
        Schema::table('claims', function (Blueprint $table) {
            $table->dropColumn('pi');
        });
        Schema::table('notification_duties', function (Blueprint $table) {
            $table->dropIndex('user_id', 'user_id');
            $table->dropColumn('user_id');
        });
        Schema::table('claim_compensations', function (Blueprint $table) {
            $table->dropIndex('member_type_id','member_type_id');
            $table->dropForeign('claim_compensations_ibfk_4', 'claim_compensations_ibfk_4');
            $table->dropColumn('member_type_id');
        });
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->integer('employee_id')->unsigned()->nullable()->index('employee_id');
            $table->dropIndex('user_id', 'user_id');
            $table->dropColumn("user_id");
            $table->renameColumn('member_type_id', 'medical_expense_type_id');
        });
        Schema::rename("member_types", "medical_expense_types");
    }
}
