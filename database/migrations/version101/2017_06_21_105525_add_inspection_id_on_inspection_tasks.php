<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInspectionIdOnInspectionTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_tasks', function(Blueprint $table) {
            $table->integer('inspection_id')->unsigned()->after("id")->index("inspection_id");
            $table->foreign('inspection_id')->references('id')->on('inspections')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_tasks', function(Blueprint $table) {
            $table->dropForeign("inspection_tasks_inspection_id_foreign");
            $table->dropIndex("inspection_id");
            $table->dropColumn("inspection_id");
        });
    }
}
