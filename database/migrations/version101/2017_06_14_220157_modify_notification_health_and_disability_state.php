<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyNotificationHealthAndDisabilityState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //notification health state

        Schema::table('notification_health_states', function(Blueprint $table) {

            $table->dropForeign('notification_health_states_ibfk_1', 'notification_health_states_ibfk_1');
            $table->dropIndex('notification_id','notification_id');
        });

        Schema::table('notification_health_states', function(Blueprint $table) {
        $table->renameColumn('notification_report_id', 'medical_expense_id');
    });
        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->integer('medical_expense_id')->unsigned()->index('medical_expense_id')->change();
        });
        Schema::table('notification_health_states', function(Blueprint $table)
        {
            $table->foreign('medical_expense_id', 'notification_health_states_ibfk_1')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


//        notification disability state
        Schema::table('notification_disability_states', function(Blueprint $table) {

            $table->dropForeign('notification_disability_states_ibfk_1', 'notification_disability_states_ibfk_1');
            $table->dropIndex('notification_report_id', 'notification_report_id');
        });
        Schema::table('notification_disability_states', function(Blueprint $table) {
            $table->renameColumn('notification_report_id', 'medical_expense_id');

        });
        Schema::table('notification_disability_states', function(Blueprint $table)
        {
            $table->integer('medical_expense_id')->unsigned()->index('medical_expense_id')->change();
            $table->foreign('medical_expense_id', 'notification_disability_states_ibfk_1')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_health_states', function(Blueprint $table) {

            $table->dropForeign('notification_health_states_ibfk_1', 'notification_health_states_ibfk_1');
            $table->dropIndex('medical_expense_id', 'medical_expense_id');
        });
        Schema::table('notification_health_states', function(Blueprint $table) {
            $table->renameColumn('medical_expense_id', 'notification_report_id');
            $table->integer('notification_report_id')->unsigned()->index('notification_report_id')->change();
        });
        Schema::table('notification_health_states', function(Blueprint $table)
        {
            $table->foreign('notification_report_id', 'notification_health_states_ibfk_1')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        //notification disability state
        Schema::table('notification_disability_states', function(Blueprint $table) {

            $table->dropForeign('notification_disability_states_ibfk_1', 'notification_disability_states_ibfk_1');
            $table->dropIndex('medical_expense_id', 'medical_expense_id');
        });
        Schema::table('notification_disability_states', function(Blueprint $table) {
            $table->renameColumn('medical_expense_id', 'notification_report_id');
            $table->integer('notification_report_id')->unsigned()->index('notification_report_id')->change();
        });
        Schema::table('notification_disability_states', function(Blueprint $table)
        {
            $table->foreign('notification_report_id', 'notification_disability_states_ibfk_1')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }
}
