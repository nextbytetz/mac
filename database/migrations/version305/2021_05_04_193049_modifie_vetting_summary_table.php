<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifieVettingSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hsp_bill_summaries', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->integer('vetting_status')->default(0);
            $table->integer('approval_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_summaries', function (Blueprint $table) {
            $table->dropColumn('vetting_status');
            $table->dropColumn('approval_status');
        });
    }
}
