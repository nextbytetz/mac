<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssessmentRegisterNotificationDisabilityState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_disability_states', function (Blueprint $table) {
            $table->smallInteger("isassessmentregister")->default(0);
        });
        DB::statement("comment on column notification_disability_states.isassessmentregister is 'show whether entry was added by assessment officer as a reference for assessment(s)'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_disability_states', function (Blueprint $table) {
            //
        });
    }
}
