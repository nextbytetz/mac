<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalPricePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_price_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('facility_code_id')->nullable();
            $table->foreign('facility_code_id')->references('id')->on('facility_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->bigInteger('item_code')->nullable();
            $table->string('price_code')->nullable();
            $table->string('level_price_code')->nullable();
            $table->string('old_item_code')->nullable();
            $table->integer('item_type_id')->nullable();
            $table->string('item_name')->nullable();
            $table->string('Strength')->nullable();
            $table->string('dosage')->nullable();
            $table->integer('package_id')->nullable();
            $table->integer('scheme_id')->nullable();
            $table->string('facility_level_code')->nullable();
            $table->float('unit_price')->nullable();
            $table->boolean('is_restricted')->nullable();
            $table->bigInteger('Maximum_quantity')->nullable();
            $table->string('Available_Levels')->nullable();
            $table->string('Practitioner_qalifications')->nullable();
            $table->boolean('Is_active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_price_packages', function (Blueprint $table) {
            Schema::dropIfExists('medical_price_packages');
        });
    }
}
