<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualMedicalExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_medical_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('member_type_id')->nullable();
            $table->foreign('member_type_id')->references('id')->on('main.member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('resource_id')->nullable();
            $table->float('amount');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->float('assessed_amount')->nullable();
            $table->integer('resource_origin')->nullable();
            $table->dateTime('receive_date')->nullable();
            $table->integer('has_subs')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_medical_expenses');
    }
}
