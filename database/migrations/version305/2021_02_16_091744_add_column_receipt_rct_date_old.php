<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReceiptRctDateOld extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('receipts', function (Blueprint $table) {
            $table->date("rct_date_old")->nullable();

        });
        DB::statement("comment on column receipts.rct_date_old is 'Rct date/ payment date old before update. Update can be due to receipt date adjustment'");

        Schema::table('booking_interests', function (Blueprint $table) {
            $table->date("payment_date_old")->nullable();

        });

        DB::statement("comment on column booking_interests.payment_date_old is 'Rct date/ payment date old before update. Update can be due to receipt date adjustment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
