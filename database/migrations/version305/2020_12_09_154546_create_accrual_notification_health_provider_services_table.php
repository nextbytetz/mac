<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationHealthProviderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_health_provider_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accrual_notification_health_provider_id')->unsigned()->index('accrual_notification_health_provider_id');
            $table->integer('health_service_checklist_id')->unsigned()->index('health_service_checklist_id');
            $table->boolean('feedback')->default(0)->comment('to be answered as yes/no, 1 - yes, 0 - no');
            $table->date('from_date')->nullable();
            $table->integer('to_date')->nullable();
            $table->decimal('amount', 14);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_health_provider_services');
    }
}
