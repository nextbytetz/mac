<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollChildExtensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_child_extensions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payroll_status_change_id');
            $table->integer('child_continuation_reason_cv_id');
            $table->tinyInteger('period_provision_type')->nullable();
            $table->date('deadline_date')->nullable();
            $table->string('school_name')->nullable();
            $table->integer('education_level_cv_id')->nullable();
            $table->string('school_grade')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('payroll_child_extensions', function (Blueprint $table) {
            $table->foreign('payroll_status_change_id')->references('id')->on('payroll_status_changes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('education_level_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column payroll_child_extensions.period_provision_type is 'Period provision type i.e. 1 => Specific, 2 => Lifetime'");

        Schema::table('payroll_status_changes', function (Blueprint $table) {
            $table->dropColumn('child_continuation_reason_cv_id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
