<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimFollowupCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_followup_calculations', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('first_week_total_files')->default(0);
            $table->integer('second_week_total_files')->default(0);
            $table->integer('third_week_total_files')->default(0);
            $table->integer('fourth_week_total_files')->default(0);

            $table->integer('first_week_current_files')->default(0);
            $table->integer('second_week_current_files')->default(0);
            $table->integer('third_week_current_files')->default(0);
            $table->integer('fourth_week_current_files')->default(0);

            $table->integer('first_week_attended_files')->default(0);
            $table->integer('second_week_attended_files')->default(0);
            $table->integer('third_week_attended_files')->default(0);
            $table->integer('fourth_week_attended_files')->default(0);

            $table->integer('first_week_missed_files')->default(0);
            $table->integer('second_week_missed_files')->default(0);
            $table->integer('third_week_missed_files')->default(0);
            $table->integer('fourth_week_missed_files')->default(0);

            $table->float('first_week_overall_files')->default(0);
            $table->float('second_week_overall_files')->default(0);
            $table->float('third_week_overall_files')->default(0);
            $table->float('fourth_week_overall_files')->default(0);

            $table->integer('overall_total')->default(0);
            $table->string('current_month')->nullable();
            $table->integer('last_quotient')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_followup_calculations', function (Blueprint $table) {
            Schema::dropIfExists('claim_followup_calculations');
        });
    }
}
