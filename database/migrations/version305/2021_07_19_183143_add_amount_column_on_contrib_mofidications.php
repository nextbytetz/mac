<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountColumnOnContribMofidications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrib_modifications', function (Blueprint $table) {
            $table->dropColumn('new_rct_description');
            $table->decimal('amount', 14)->nullable();
            $table->text('receipt_codes')->nullable(); 
            $table->text('request_attachment')->nullable(); 
            $table->text('receipt_attachment')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrib_modifications', function (Blueprint $table) {
           
        });
    }
}
