<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyViewEmployerContributionsMemberCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("select deps_save_and_drop_dependencies('main', 'employer_contributions')");
        DB::statement("DROP VIEW IF EXISTS  employer_contributions");
        DB::statement("CREATE VIEW main.employer_contributions AS


  SELECT r.employer_id,
         e.doc,
         e.name                              AS employer_name,
         e.reg_no                            AS employer_reg_no,
         code.amount                         AS contrib_amount,
         r.amount                            AS receipt_amount,
         to_date(concat_ws('-' :: text, date_part('year' :: text, code.contrib_month),
                           date_part('month' :: text, code.contrib_month), '28'),
                 'YYYY-MM-DD' :: text)       AS contrib_month,
         r.rct_date,
         r.created_at                        AS receipt_created_at,
         (r.rctno) :: character varying(100) AS rctno,
         0                                   AS islegacy,
         r.pay_control_no                    AS control_no,
         code.booking_id,
         code.member_count
  FROM ((main.receipt_codes code
      JOIN main.receipts r ON ((code.receipt_id = r.id)))
      JOIN main.employers e ON ((r.employer_id = e.id)))
  WHERE ((r.iscancelled = 0) AND (r.isdishonoured = 0) AND (r.deleted_at IS NULL) AND (code.deleted_at IS NULL) AND
         (code.grouppaystatus = 0) AND ((code.fin_code_id = 2) OR (code.fin_code_id = 63)))
  UNION
  SELECT l.employer_id,
         e.doc,
         e.name                                                                                      AS employer_name,
         e.reg_no                                                                                    AS employer_reg_no,
         l.amount                                                                                    AS contrib_amount,
         l.amount                                                                                    AS receipt_amount,
         to_date(concat_ws('-' :: text, date_part('year' :: text, l.contrib_month),
                           date_part('month' :: text, l.contrib_month), '28'), 'YYYY-MM-DD' :: text) AS contrib_month,
         l.rct_date,
         l.created_at                                                                                AS receipt_created_at,
         l.rctno,
         1                                                                                           AS islegacy,
         NULL :: text                                                                                AS control_no,
         NULL :: bigint                                                                              AS booking_id,
         l.member_count
  FROM (main.legacy_receipts l
      JOIN main.employers e ON ((l.employer_id = e.id)))
  WHERE ((l.iscancelled = 0) AND (l.isdishonoured = 0) AND (l.deleted_at IS NULL) AND (l.exist_in_receipts = 0));


");

        DB::statement("select deps_restore_dependencies('main', 'employer_contributions')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
