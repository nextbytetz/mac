<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnWaslegacyLegacyReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('legacy_receipts', function (Blueprint $table) {
            $table->tinyInteger('waslegacy')->default(0);
        });

        DB::statement("comment on column legacy_receipts.waslegacy is 'Flag to imply that this receipt was moved from legacy to non legacy (electronic legacy)'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
