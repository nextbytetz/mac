<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedAtOnContribModificationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('contrib_modification_redistributions', function (Blueprint $table) {
         $table->softDeletes();
     });

       Schema::table('contrib_modifications', function (Blueprint $table) {
        $table->boolean('iscancelled')->default(false);
        $table->text('cancel_reason')->nullable();
    });

       Schema::table('contrib_modification_months', function (Blueprint $table) {
        $table->softDeletes();
    });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
