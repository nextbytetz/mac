<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalNotificationReceivedView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create or replace view total_notification_received as select a.filename, concat_ws(' ', b.firstname, coalesce(b.middlename, ''), b.lastname) as employee, d.name employer, c.name as incident, a.incident_date, a.reporting_date, a.receipt_date as notification_date, a.created_at as registration_date, a.id resource_id, o.name district, p.name region, a.haspaidmanual from notification_reports a join employees b on a.employee_id = b.id join incident_types c on a.incident_type_id = c.id join employers d on d.id = a.employer_id left join districts o on o.id = a.district_id left join regions p on o.region_id = p.id where a.isprogressive = 0 and a.deleted_at is null union all select a.filename, case when b.id is not null then concat_ws(' ', b.firstname, coalesce(b.middlename, ''), b.lastname) else a.employee_name end as employee, case when d.id is not null then d.name else a.employer_name end employer, c.name as incident, a.incident_date, a.reporting_date, a.receipt_date as notification_date, a.created_at as registration_date, a.id resource_id, o.name district, p.name region, a.haspaidmanual from notification_reports a left join employees b on a.employee_id = b.id join incident_types c on a.incident_type_id = c.id left join employers d on d.id = a.employer_id left join districts o on o.id = a.district_id left join regions p on o.region_id = p.id left join code_values q on a.notification_staging_cv_id = q.id left join codes r on q.code_id = r.id where a.isprogressive = 1 and r.id in(13, 23) and a.deleted_at is null;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
