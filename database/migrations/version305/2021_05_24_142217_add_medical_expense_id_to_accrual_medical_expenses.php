<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicalExpenseIdToAccrualMedicalExpenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_medical_expenses', function (Blueprint $table) {
            $table->unsignedInteger('medical_expense_id')->nullable();
            $table->foreign('medical_expense_id')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_medical_expenses', function (Blueprint $table) {
            $table->dropColumn('medical_expense_id');
        });
    }
}
