<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDifferenceColumnsToBillDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('hsp_bill_details', function (Blueprint $table) {
            $table->integer('quantity_difference')->default(0);
            $table->float('price_difference')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_details', function (Blueprint $table) {
            $table->dropColumn('quantity_difference');
            $table->dropColumn('price_difference');
        });
    }
}
