<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPayrollRunsWithManualView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_runs_with_manual_view')");
        DB::statement("DROP  VIEW IF EXISTS  payroll_runs_with_manual_view");
        DB::statement("CREATE VIEW main.payroll_runs_with_manual_view AS

SELECT payroll_runs_view.run_id,
       payroll_runs_view.member_name,
       payroll_runs_view.employee_name,
       payroll_runs_view.memberno,
       payroll_runs_view.amount,
       payroll_runs_view.months_paid,
       payroll_runs_view.monthly_pension,
       payroll_runs_view.payroll_run_approval_id,
       payroll_runs_view.wf_done,
       payroll_runs_view.arrears_amount,
       payroll_runs_view.deductions_amount,
       payroll_runs_view.unclaimed_amount,
       payroll_runs_view.employee_id,
       payroll_runs_view.member_type_name,
       payroll_runs_view.run_date,
       payroll_runs_view.bank_branch_name,
       payroll_runs_view.bank_name,
       payroll_runs_view.run_accountno,
       payroll_runs_view.run_bank_branch_id,
       payroll_runs_view.run_bank_id,
       payroll_runs_view.resource_id,
       payroll_runs_view.member_type_id,
       payroll_runs_view.new_payee_flag,
       payroll_runs_view.isconstantcare,
       payroll_runs_view.filename,
       payroll_runs_view.gender,
       payroll_runs_view.relationship,
       0 AS islegacy
FROM main.payroll_runs_view
UNION ALL
SELECT manual_payroll_runs.id            AS run_id,
       payroll_beneficiaries_view.member_name,
       payroll_beneficiaries_view.employee_name,
       payroll_beneficiaries_view.memberno,
       manual_payroll_runs.net_amount    AS amount,
       1                                 AS months_paid,
       payroll_beneficiaries_view.monthly_pension_amount,
       NULL :: integer                   AS payroll_run_approval_id,
       1                                 AS wf_done,
       case WHEN (manual_payroll_runs.net_amount > payroll_beneficiaries_view.monthly_pension_amount )  then (manual_payroll_runs.net_amount -payroll_beneficiaries_view.monthly_pension_amount )  else 0 end                        AS arrears_amount,
       case WHEN (manual_payroll_runs.net_amount < payroll_beneficiaries_view.monthly_pension_amount )  then (payroll_beneficiaries_view.monthly_pension_amount - manual_payroll_runs.net_amount )  else 0 end                                            AS deductions_amount,
       0                                 AS unclaimed_amount,
       manual_payroll_runs.employee_id,
       member_types.name                 AS member_type_name,
       manual_payroll_runs.payroll_month AS run_date,
       NULL :: character varying         AS bank_branch_name,
       manual_payroll_runs.bank_name,
       manual_payroll_runs.accountno     AS run_accountno,
       NULL :: smallint                  AS run_bank_branch_id,
       NULL :: smallint                  AS run_bank_id,
       manual_payroll_runs.resource_id,
       manual_payroll_runs.member_type_id,
       0                                 AS new_payee_flag,
       0                                 AS isconstantcare,
       payroll_beneficiaries_view.filename,
       payroll_beneficiaries_view.gender,
       payroll_beneficiaries_view.relationship,
       1                                 AS islegacy
FROM ((main.manual_payroll_runs
    JOIN main.member_types ON ((member_types.id = manual_payroll_runs.member_type_id)))
    JOIN main.payroll_beneficiaries_view ON ((
  (payroll_beneficiaries_view.resource_id = manual_payroll_runs.resource_id) AND
  (payroll_beneficiaries_view.member_type_id = manual_payroll_runs.member_type_id))))
WHERE (manual_payroll_runs.deleted_at IS NULL);



");

        DB::statement("select deps_restore_dependencies('main', 'payroll_runs_with_manual_view')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
