<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndCreateEmployerPayrollMergeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('employer_payroll_merge_details');
        Schema::dropIfExists('employer_payroll_merge_receipts');
        Schema::create('employer_payroll_merge_details', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('payroll_merge_id');
          $table->unsignedInteger('payroll_id');
          $table->unsignedInteger('number_of_employees');
          $table->mediumText('employees');
          $table->unsignedInteger('number_of_contributions');
          $table->mediumText('receipts');
          $table->unsignedInteger('number_of_arrears');
          $table->mediumText('arrears');
          $table->unsignedInteger('employees_after')->nullable();
          $table->mediumText('remain_employees')->nullable();
          $table->unsignedInteger('contributions_after')->nullable();
          $table->mediumText('remain_receipts')->nullable();
          $table->unsignedInteger('arrears_after')->nullable();
          $table->mediumText('remain_arrears')->nullable();
          $table->timestamps();
          $table->foreign('payroll_merge_id')->references('id')->on('employer_payroll_merges')->onUpdate('CASCADE')->onDelete('RESTRICT');
      });
        DB::statement("comment on column employer_payroll_merge_details.employees_after is 'number of employees after payroll merging'");
        DB::statement("comment on column employer_payroll_merge_details.remain_employees is 'memberno of employees after payroll merging'");
        DB::statement("comment on column employer_payroll_merge_details.contributions_after is 'number of receipts after payroll merging'");
        DB::statement("comment on column employer_payroll_merge_details.remain_receipts is 'receipt_id after payroll merging'");
        DB::statement("comment on column employer_payroll_merge_details.arrears_after is 'number of arrears after payroll merging'");
        DB::statement("comment on column employer_payroll_merge_details.remain_arrears is 'arrear_id after payroll merging'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_payroll_merge_details');
    }
}
