<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsPayrollRecoveriesTableDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_arrears')");
        Schema::table('payroll_arrears', function (Blueprint $table) {
            $table->decimal('recycles',10,4)->change();


        });

        DB::statement("select deps_restore_dependencies('main', 'payroll_arrears')");

        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_unclaims')");
        Schema::table('payroll_unclaims', function (Blueprint $table) {
            $table->decimal('recycles',10,4)->change();


        });

        DB::statement("select deps_restore_dependencies('main', 'payroll_unclaims')");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
