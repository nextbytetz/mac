<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentCheckBeforeProceedingWithWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->smallInteger('need_check')->default(0);
        });
        DB::statement("comment on column wf_definitions.need_check is 'signify if a module needs checking, e.g document check etc'");

        Schema::create('wf_document_checks', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("wf_track_id")->index();
            $table->bigInteger("document_id")->index();
            $table->timestamps();;
            $table->foreign('wf_track_id')->references('id')->on('main.wf_tracks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('main.documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on table wf_document_checks is 'store the documents which have been viewed, during workflow track process.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
