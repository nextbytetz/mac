<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicalExpenseIdToAccrualNotificationHealthProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_notification_health_providers', function (Blueprint $table) {

            $table->unsignedInteger('accrual_medical_expense_id')->nullable();
            $table->foreign('accrual_medical_expense_id')->references('id')->on('main.accrual_medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_notification_health_providers', function (Blueprint $table) {

            $table->dropForeign('accrual_medical_expense_id');

        });
    }
}
