<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPayrollBeneficiaryViewRetireeFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payroll_retiree_mp_updates', function (Blueprint $table) {
            $table->string('doc_evidence')->nullable();
            $table->string('new_mp')->default(0);
        });



        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_beneficiaries_view')");
        DB::statement("DROP  VIEW IF EXISTS  payroll_beneficiaries_view");
        DB::statement("CREATE VIEW main.payroll_beneficiaries_view AS

SELECT p.id                                  AS resource_id,
         5                                     AS member_type_id,
         CASE
           WHEN (p.notification_report_id IS NOT NULL) THEN n.filename
           ELSE (m2.case_no) :: character varying(20)
             END                               AS filename,
         concat_ws(' ' :: text, p.firstname, COALESCE(p.middlename, '' :: character varying),
                   p.lastname)                 AS member_name,
         concat_ws(' ' :: text, p.firstname, COALESCE(p.middlename, '' :: character varying),
                   p.lastname)                 AS employee_name,
         (e.memberno) :: character varying(20) AS memberno,
         p.bank_id,
         p.bank_branch_id,
         p.notification_report_id,
         p.manual_notification_report_id,
         p.isactive,
         p.employee_id,
         NULL :: integer                       AS pivot_id,
         CASE
           WHEN (e.gender_id = 1) THEN 'Male' :: text
           WHEN (e.gender_id = 2) THEN 'Female' :: text
           ELSE ' ' :: text
             END                               AS gender,
         NULL :: character varying             AS relationship,
         NULL :: integer                       AS dependent_type_id,
         p.lastresponse,
         p.suspense_flag,
         p.suspended_date,
         p.accountno,
         p.dob,
         p.monthly_pension_amount,
         CASE
           WHEN (n.id IS NOT NULL) THEN c.pd
           WHEN (m2.id IS NOT NULL) THEN m2.man_pd
           ELSE NULL :: numeric
             END                               AS pd,
         em.name                               AS employer_name,
         em.id                                 AS employer_id,
         em.region_id                          AS employer_region_id,
         p.suspension_reason,
         p.hold_reason,
         p.hold_date,
         p.district_id                         AS member_district_id,
         p.region_id                           AS member_region_id,
         p.phone                               AS member_phone,
       p.date_next_retiree_follow_up,
       p.retiree_feedback_cv_id
  FROM (((((main.pensioners p
      JOIN main.employees e ON ((e.id = p.employee_id)))
      LEFT JOIN main.notification_reports n ON ((p.notification_report_id = n.id)))
      LEFT JOIN main.manual_notification_reports m2 ON ((p.manual_notification_report_id = m2.id)))
      LEFT JOIN main.claims c ON ((c.notification_report_id = n.id)))
      LEFT JOIN main.employers em ON (((em.id = m2.employer_id) OR (em.id = n.employer_id))))
  UNION
  SELECT d.id                                                           AS resource_id,
         4                                                              AS member_type_id,
         CASE
           WHEN (de.notification_report_id IS NOT NULL) THEN n.filename
           ELSE (m2.case_no) :: character varying(20)
             END                                                        AS filename,
         concat_ws(' ' :: text, d.firstname, COALESCE(d.middlename, '' :: character varying),
                   d.lastname)                                          AS member_name,
         concat_ws(' ' :: text, e.firstname, COALESCE(e.middlename, '' :: character varying),
                   e.lastname)                                          AS employee_name,
         concat_ws('_' :: text, e.memberno, de.dependent_type_id, d.id) AS memberno,
         d.bank_id,
         d.bank_branch_id,
         de.notification_report_id,
         de.manual_notification_report_id,
         de.isactive,
         de.employee_id,
         de.id                                                          AS pivot_id,
         CASE
           WHEN (d.gender_id = 1) THEN 'Male' :: text
           WHEN (d.gender_id = 2) THEN 'Female' :: text
           ELSE ' ' :: text
             END                                                        AS gender,
         dt.name                                                        AS relationship,
         de.dependent_type_id,
         d.lastresponse,
         d.suspense_flag,
         d.suspended_date,
         d.accountno,
         d.dob,
         de.survivor_pension_amount                                     AS monthly_pension_amount,
         NULL :: numeric                                                AS pd,
         em.name                                                        AS employer_name,
         em.id                                                          AS employer_id,
         em.region_id                                                   AS employer_region_id,
         d.suspension_reason,
         de.hold_reason,
         de.hold_date,
         d.district_id                                                  AS member_district_id,
         d.region_id                                                    AS member_region_id,
         d.phone                                                        AS member_phone,
         null as date_next_retiree_follow_up,
         null as retiree_feedback_cv_id
  FROM ((((((main.dependent_employee de
      JOIN main.dependents d ON ((d.id = de.dependent_id)))
      JOIN main.dependent_types dt ON ((dt.id = de.dependent_type_id)))
      JOIN main.employees e ON ((de.employee_id = e.id)))
      LEFT JOIN main.notification_reports n ON ((de.notification_report_id = n.id)))
      LEFT JOIN main.manual_notification_reports m2 ON ((de.manual_notification_report_id = m2.id)))
      LEFT JOIN main.employers em ON (((em.id = m2.employer_id) OR (em.id = n.employer_id))));

");

        DB::statement("select deps_restore_dependencies('main', 'payroll_beneficiaries_view')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
