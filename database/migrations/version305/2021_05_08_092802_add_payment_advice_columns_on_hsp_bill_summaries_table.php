<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentAdviceColumnsOnHspBillSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hsp_bill_summaries', function (Blueprint $table) {
            $table->integer('payment_advice')->default(0);
        });
        DB::statement("comment on column hsp_bill_summaries.payment_advice is ' status of payment advice for this bill: 0 = not created, 1 = created, 2 = printed' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_summaries', function (Blueprint $table) {

        });
    }
}
