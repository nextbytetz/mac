<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployerClosureFollowupsIststus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closure_follow_ups', function (Blueprint $table) {
            $table->tinyInteger("isstatus_followup")->default(0);

        });

        DB::statement("comment on column employer_closure_follow_ups.isstatus_followup is 'Flag to imply that this follow up if for status checking on temporary de-registration reopen'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
