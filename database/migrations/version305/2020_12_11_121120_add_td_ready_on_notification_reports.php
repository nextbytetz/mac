<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTdReadyOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
-- update notification_reports set td_ready = it.td_ready, td_refund_ready = it.td_refund_ready from incident_tds it where notification_reports.id = it.notification_report_id;
update notification_reports set td_ready = 1 where id in (select notification_report_id from incident_tds where td_ready = 1);
update notification_reports set td_refund_ready = 1 where id in (select notification_report_id from incident_tds where td_refund_ready = 1);
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
