<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerPayrollMergesReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_payroll_merge_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('receipt_id');
            $table->unsignedInteger('payroll_merge_id');
            $table->unsignedInteger('old_payroll_id');
            $table->unsignedInteger('new_payroll_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payroll_merge_id')->references('id')->on('employer_payroll_merges')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_payroll_merge_receipts');
    }
}
