<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToClaimFollowupUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('claim_followup_updates', function (Blueprint $table) {
        $table->unsignedInteger('user_id')->nullable();
        $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_followup_updates', function (Blueprint $table) {
            $table->dropForeign('user_id');
        });
    }
}
