<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_dependents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 45);
            $table->string('middlename', 45)->nullable();
            $table->string('lastname', 45);
            $table->unsignedInteger('identity_id')->nullable();
            $table->foreign('identity_id')->references('id')->on('main.identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->text('id_no');
            $table->string('address', 150)->nullable();
            $table->date('dob')->nullable();

            $table->unsignedInteger('gender_id')->nullable();
            $table->foreign('gender_id')->references('id')->on('main.genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('main.countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('location_type_id')->nullable();
            $table->foreign('location_type_id')->references('id')->on('main.location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->unsignedInteger('bank_branch_id')->nullable();
            $table->foreign('bank_branch_id')->references('id')->on('main.bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('bank_id')->nullable();
            $table->foreign('bank_id')->references('id')->on('main.banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('region_id')->nullable();
            $table->foreign('region_id')->references('id')->on('main.regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('main.districts')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->text('unsurveyed_area', 65535)->nullable();
            $table->string('street', 150)->nullable();
            $table->string('road', 150)->nullable();
            $table->string('plot_no', 150)->nullable();
            $table->string('block_no', 150)->nullable();
            $table->text('surveyed_extra', 65535)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->string('fax', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('accountno', 45)->nullable();

            $table->dateTime('lastresponse')->comment('last verified date');

            $table->date('exit_date')->nullable();
            $table->integer('isdisabled')->default(0)->comment('Flag to imply if child dependent is disabled who granted to receive pension');
            $table->integer('iseducation')->default(0);
            $table->text('birth_certificate_no')->nullable();
            $table->integer('isdmsposted')->default(0);
            $table->integer('firstpay_manual')->default(0);
            $table->date('suspended_date')->nullable();
            $table->text('next_kin')->nullable();
            $table->text('next_kin_phone')->nullable();
            $table->decimal('survivor_gratuity_amount', 14)->nullable()->comment('amount of gratuity the dependent is eligible to be paid.');

            $table->text('suspension_reason')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_dependents');
    }
}
