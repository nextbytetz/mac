<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrollRetireeMpUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_retiree_followups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_type_id');
            $table->integer('resource_id');
            $table->integer('employee_id');
            $table->integer('follow_up_type_cv_id');
            $table->string('contact')->nullable();
            $table->string('contact_person')->nullable();
            $table->date('date_of_follow_up');
            $table->integer('retiree_feedback_cv_id');
            $table->date('date_of_next_follow_up');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('payroll_retiree_followups', function (Blueprint $table) {
            $table->foreign('retiree_feedback_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('follow_up_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column payroll_retiree_followups.retiree_feedback_cv_id is 'Retiree follow ups feedback i.e. still working, retired pensioner, retired non pensioner'");

        Schema::create('payroll_retiree_mp_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payroll_retiree_followup_id');
            $table->integer('member_type_id');
            $table->integer('resource_id');
            $table->integer('employee_id');
            $table->decimal('current_mp', 12,2);
            $table->decimal('social_security_mp', 12,2);
            $table->date('retirement_date');
            $table->text('remark')->nullable();
            $table->decimal('deduction_amount', 12,2)->default(0);
            $table->text('deduction_summary')->nullable();
            $table->tinyInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('payroll_retiree_mp_updates', function (Blueprint $table) {
            $table->foreign('payroll_retiree_followup_id')->references('id')->on('payroll_retiree_followups')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column payroll_retiree_mp_updates.social_security_mp is 'Monthly pension paid at Social security for retired pensioners'");
        DB::statement("comment on column payroll_retiree_mp_updates.current_mp is 'Current monthly pension paid by WCF'");


        Schema::table('pensioners', function (Blueprint $table) {
            $table->date('date_next_retiree_follow_up')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
