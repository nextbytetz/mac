<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsToClaimsPayableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims_payable', function (Blueprint $table) {
            $table->text('source_reference')->nullable();
            $table->text('voucher_number')->nullable();
        });
        DB::statement("comment on column main.claims_payable.erms_status_code is 'The status of the claim if sent to eGA erms i.e. 200 => Successful reached but pending, 201 => Paid, 202 => Accepted, 400 => Client Error'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims_payable', function (Blueprint $table) {
            $table->dropColumn('source_reference');
            $table->dropColumn('voucher_number');
        });
    }
}
