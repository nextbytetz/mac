<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToEmployerPayrollMergesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_payroll_merges', function (Blueprint $table) {
            $table->integer('merge_type')->nullable();
        });
        DB::statement("comment on column employer_payroll_merges.merge_type is '1 => wrong verification, 2 => combined payrols'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
