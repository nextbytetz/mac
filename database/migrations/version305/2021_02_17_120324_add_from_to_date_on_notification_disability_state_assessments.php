<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFromToDateOnNotificationDisabilityStateAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_disability_state_assessments', function (Blueprint $table) {
            $table->date("from_date")->nullable();
            $table->date("to_date")->nullable();
        });
        Schema::create('assessment_documents', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_eligible_benefit_id");
            $table->bigInteger("document_notification_report_id");
            $table->timestamps();
            $table->foreign('document_notification_report_id')->references('id')->on('main.document_notification_report')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_disability_state_assessments.from_date is 'from_date assessed by doctor'");
        DB::statement("comment on column notification_disability_state_assessments.to_date is 'to_date assessed by doctor'");
        DB::statement("comment on table assessment_documents is 'store the reference of the document used for assessment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
