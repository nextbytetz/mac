<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewOnSlaAlerts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sla_alerts', function (Blueprint $table) {
            $table->smallInteger("configurable_report_id")->nullable()->index();
            $table->foreign('configurable_report_id')->references('id')->on('main.configurable_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sla_alerts', function (Blueprint $table) {
            //
        });
    }
}
