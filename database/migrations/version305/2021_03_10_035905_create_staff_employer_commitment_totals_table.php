<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffEmployerCommitmentTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_employer_commitment_totals', function (Blueprint $table) {
            $table->unsignedInteger('commitment_request_id')->nullable();
            $table->foreign('commitment_request_id')->references('id')->on('portal.commitment_requests')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('employer_id')->nullable();
            $table->foreign('employer_id')->references('id')->on('main.employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_employer_commitment_totals', function (Blueprint $table) {
            Schema::dropIfExists('staff_employer_commitment_totals');
        });
    }
}
