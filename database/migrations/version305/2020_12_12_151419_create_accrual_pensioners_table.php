<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualPensionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_pensioners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 45);
            $table->string('middlename', 45)->nullable();
            $table->string('lastname', 45);
            $table->integer('recycl')->nullable();
            $table->float('monthly_pension_amount')->nullable();
            $table->integer('pay_period')->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->date('dob')->nullable();
            $table->string('email', 45)->nullable();
            $table->string('accountno', 45)->nullable();

            $table->dateTime('lastresponse')->comment('last verified date');

            $table->date('exit_date')->nullable();
            $table->integer('isactive')->default(0);
            $table->integer('isresponded')->default(0);
            $table->integer('isdmsposted')->default(0);
            $table->integer('firstpay_flag')->default(0);
            $table->integer('suspense_flag')->default();
            $table->integer('firstpay_manual')->default(0);
            $table->date('suspended_date')->nullable();
            $table->float('old_mp')->nullable();
            $table->text('hold_reason')->nullable();
            $table->text('next_kin')->nullable();
            $table->text('next_kin_phone')->nullable();

            $table->text('suspension_reason')->nullable();

            $table->unsignedInteger('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('main.countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('compensation_payment_type_id')->nullable();
            $table->foreign('compensation_payment_type_id')->references('id')->on('main.compensation_payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('employee_id')->nullable();
            $table->foreign('employee_id')->references('id')->on('main.employees')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('notification_eligible_benefit_id')->nullable();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('manual_notification_report_id')->nullable();
            $table->foreign('manual_notification_report_id')->references('id')->on('main.manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->unsignedInteger('bank_branch_id')->nullable();
            $table->foreign('bank_branch_id')->references('id')->on('main.bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('bank_id')->nullable();
            $table->foreign('bank_id')->references('id')->on('main.banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('region_id')->nullable();
            $table->foreign('region_id')->references('id')->on('main.regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('main.districts')->onUpdate('CASCADE')->onDelete('RESTRICT');



            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_pensioners');
    }
}
