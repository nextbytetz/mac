<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToClaimFollowupCalculationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_followup_calculations', function (Blueprint $table) {
            $table->integer('tfirst_week_missed_files')->default(0);
            $table->integer('tsecond_week_missed_files')->default(0);
            $table->integer('tthird_week_missed_files')->default(0);
            $table->integer('tfourth_week_missed_files')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_followup_calculations', function (Blueprint $table) {
            $table->dropColumn('tfirst_week_missed_files');
            $table->dropColumn('tsecond_week_missed_files');
            $table->dropColumn('tthird_week_missed_files');
            $table->dropColumn('tfourth_week_missed_files');

        });
    }
}
