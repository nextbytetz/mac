<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsProviderIdNotificationWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
           $table->dropColumn('document_notification_report_id');
           $table->dropColumn('document_date');
           $table->dropColumn('health_provider_id');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
            //
        });
    }
}
