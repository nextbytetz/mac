<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToAccrualNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_notification_reports', function (Blueprint $table) {
            $table->integer('notification_wf_level')->nullable();
            DB::statement("comment on table accrual_notification_reports is 'wf level from notification before accrual, 1 => admin, 2 => assessment, 3 => finance'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_notification_reports', function (Blueprint $table) {
            $table->dropColumn('notification_wf_level');
        });
    }
}
