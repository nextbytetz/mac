<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDeductionFromRetireeMp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_recoveries', function (Blueprint $table) {
            $table->integer('entity_resource_id')->nullable();
            $table->string('entity_resource_type')->nullable();
            $table->tinyInteger('cancelled_but_paid')->default(0);
        });
        DB::statement("comment on column payroll_recoveries.entity_resource_id is 'Source entity for this recovery e.g. Mp update/changes process'");
        DB::statement("comment on column payroll_recoveries.cancelled_but_paid is 'Cancelled but payment already started'");

        Schema::table('payroll_retiree_mp_updates', function (Blueprint $table) {
            $table->decimal('deduction_per_cycle', 10,2)->default(0);
            $table->decimal('cycles_actual', 10,2)->default(0);
        });


        Schema::create('payroll_mp_change_tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_type_id');
            $table->integer('resource_id');
            $table->integer('employee_id');
            $table->decimal('mp',10,2);
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->integer('trans_resource_id')->nullable();
            $table->string('trans_resource_type')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('payroll_mp_change_tracks', function (Blueprint $table) {
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on table payroll_mp_change_tracks is 'Monthly pension change tracks whenever there is update on monthly pension of the beneficiary'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
