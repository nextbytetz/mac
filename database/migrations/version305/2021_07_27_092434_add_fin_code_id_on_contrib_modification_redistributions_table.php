<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinCodeIdOnContribModificationRedistributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrib_modification_redistributions', function (Blueprint $table) {
            $table->smallInteger("fin_code_id")->nullable();
        });

        Schema::table('contrib_modification_months', function (Blueprint $table) {
            $table->smallInteger("fin_code_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrib_modification_redistributions', function (Blueprint $table) {
            //
        });
    }
}
