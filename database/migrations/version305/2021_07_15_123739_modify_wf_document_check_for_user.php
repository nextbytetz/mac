<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyWfDocumentCheckForUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("truncate table wf_document_checks;");
        Schema::table('wf_document_checks', function (Blueprint $table) {
            $table->dropColumn("wf_track_id");
            $table->dropColumn("document_id");
            $table->bigInteger("user_id")->index();
            $table->bigInteger("document_notification_report_id")->index();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_notification_report_id')->references('id')->on('main.document_notification_report')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_document_checks', function (Blueprint $table) {
            //
        });
    }
}
