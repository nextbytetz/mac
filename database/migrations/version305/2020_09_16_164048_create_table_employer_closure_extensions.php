<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployerClosureExtensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('employer_closure_extensions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employer_closure_id');
            $table->date('prev_reopen_date')->nullable();
            $table->date('new_reopen_date')->nullable();
            $table->date('application_date');
            $table->text('remark')->nullable();
            $table->smallInteger('wf_status')->default(0);
            $table->smallInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employer_closure_id')->references('id')->on('employer_closures')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        DB::statement("comment on table employer_closure_extensions is 'Table to keep track of all employer closure (De-registration) extension requests'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
