<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHspBillServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hsp_bill_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('hsp_detail_id')->nullable();
            $table->foreign('hsp_detail_id')->references('id')->on('hsp_bill_details')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->string('item_code')->nullable();
            $table->float('price')->nullable();
            $table->integer('quantity')->nullable();
            $table->float('vetted_price')->nullable();
            $table->integer('vetted_quantity')->nullable();
            $table->integer('quantity_difference')->nullable();
            $table->float('price_defference')->nullable();
            $table->boolean('is_workrelated')->default(false);
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_services', function (Blueprint $table) {
            Schema::dropIfExists('hsp_bill_services');
        });
    }
}
