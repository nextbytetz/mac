<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerPayrollMergeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_payroll_merge_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payroll_merge_id');
            $table->unsignedInteger('payroll_id');
            $table->unsignedInteger('number_of_employees');
            $table->unsignedInteger('number_of_contributions');
            $table->unsignedInteger('number_of_arrears');
            $table->unsignedInteger('employees_after')->nullable();
            $table->unsignedInteger('contributions_after')->nullable();
            $table->unsignedInteger('arrears_after')->nullable();
            $table->timestamps();
            $table->foreign('payroll_merge_id')->references('id')->on('employer_payroll_merges')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_payroll_merge_details');
    }
}
