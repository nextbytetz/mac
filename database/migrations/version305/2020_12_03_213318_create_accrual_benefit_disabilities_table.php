<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualBenefitDisabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_benefit_disabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('benefit_type_id')->nullable();
            $table->foreign('benefit_type_id')->references('id')->on('main.benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('accrual_notification_report_id')->nullable();
            $table->foreign('accrual_notification_report_id')->references('id')->on('main.accrual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->float('pd_percent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_benefit_disabilities');
    }
}
