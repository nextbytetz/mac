<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommitmentRequestIdToClaimFollowupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff_employer_follow_ups', function (Blueprint $table) {
            $table->unsignedInteger('commitment_request_id')->nullable();
            $table->foreign('commitment_request_id')->references('id')->on('portal.commitment_requests')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_employer_follow_ups', function (Blueprint $table) {
            $table->dropColumn('commitment_request_id');
        });
    }
}
