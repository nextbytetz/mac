<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTenureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_tenure', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('years')->nullable();
        $table->integer('days')->nullable();
        $table->float('coupon_rate')->nullable();
        $table->timestamps();

       
    });

  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
