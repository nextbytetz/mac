<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnEmployerPayrollMergesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('employer_payroll_merges', function (Blueprint $table) {
       $table->text('old_users')->nullable();
       $table->text('new_user')->nullable();
       $table->integer('status')->default(0);
       $table->smallInteger('wf_done')->default(0);
       $table->date('wf_done_date')->nullable();
       $table->date('date_received')->nullable();
       $table->smallInteger('source')->nullable()->default(1);
       $table->text('attachment')->nullable();
       $table->string("user_type", 150)->default('App\Models\Auth\User');
     });
      DB::statement("comment on column employer_payroll_merges.status is 'stage of this request: 0 = pending, 1 = initiated workflow, 2 = approved, 3 = rejected, 4 = cancelled by user'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('employer_payroll_merges', function (Blueprint $table) {
            //
      });
    }
  }
