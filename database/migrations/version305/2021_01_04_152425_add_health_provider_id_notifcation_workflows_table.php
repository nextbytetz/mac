<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHealthProviderIdNotifcationWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
          $table->unsignedInteger('health_provider_id')->nullable();
          $table->foreign('health_provider_id')->references('id')->on('main.health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
            //
        });
    }
}
