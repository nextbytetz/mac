<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableOfAccrualDisabilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('accrual_notification_benefits');
        Schema::dropIfExists('accrual_benefit_disabilities');

        Schema::create('accrual_notification_benefits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('benefit_type_id')->nullable();
            $table->foreign('benefit_type_id')->references('id')->on('main.benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('accrual_notification_report_id')->nullable();
            $table->foreign('accrual_notification_report_id')->references('id')->on('main.accrual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->float('pd_percent')->nullable();

            $table->unsignedInteger('eligible_benefit_id')->nullable();
            $table->foreign('eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('type')->nullable();
            $table->integer('is_assessed')->default(0);
            $table->timestamps();
        });


        Schema::create('accrual_benefit_disabilities', function (Blueprint $table) {
         $table->increments('id');
         $table->unsignedInteger('accrual_benefit_disabilitie_id')->nullable();
         $table->foreign('accrual_benefit_disabilitie_id')->references('id')->on('main.accrual_notification_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
         $table->unsignedInteger('disability_id')->nullable();
         $table->foreign('disability_id')->references('id')->on('main.disability_state_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
         $table->integer('admin_days')->nullable();
         $table->date('admin_from_date')->nullable();
         $table->date('admin_to_date')->nullable();
         $table->integer('assessed_days')->nullable();
         $table->timestamps();
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_benefits');
        Schema::dropIfExists('accrual_benefit_disabilities');
    }
}
