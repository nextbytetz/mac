<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPayrollStatusChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_status_changes', function (Blueprint $table) {
            $table->integer('child_continuation_reason_cv_id')->nullable();
            $table->date('deadline_date')->nullable();
            $table->tinyInteger('hasarrears')->default(0);
            $table->decimal('pending_pay_months', 5,2)->default(0);
            $table->string('doc_evidence')->nullable();
        });



        DB::statement("comment on column payroll_status_changes.child_continuation_reason_cv_id is 'Child continuation reason for reactivation into payroll'");
        DB::statement("comment on column payroll_status_changes.deadline_date is 'Deadline date for payroll payment incase for child continue school'");
        DB::statement("comment on column payroll_status_changes.hasarrears is 'Flag to specify if has arrears'");
        DB::statement("comment on column payroll_status_changes.pending_pay_months is 'Months in arrears need to be paid on next payroll'");
        DB::statement("comment on column payroll_status_changes.doc_evidence is 'Array for document used for approval i.e. Primary keys of document_payroll_beneficiaries'");


        Schema::table('dependent_employee', function (Blueprint $table) {
            $table->date('deadline_date')->nullable();

        });

        DB::statement("comment on column dependent_employee.deadline_date is 'Cut off date for receiving payroll payments'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
