<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigation_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_name')->nullable();
            $table->smallInteger('investigation_type_cv_id')->nullable();
            $table->smallInteger('investigation_category_cv_id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->unsignedSmallInteger('duration');
            $table->text('investigation_reason');
            $table->unsignedSmallInteger('number_of_files');
            $table->unsignedSmallInteger('number_of_investigators');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('plan_status')->default(0);
            $table->text('cancel_reason')->nullable();
            $table->integer('cancel_user')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('investigation_type_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('investigation_category_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('cancel_user')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column investigation_plans.duration is 'number of days of investigation ==> system calculation based on start and end date'");
        DB::statement("comment on column investigation_plans.number_of_files is 'number of files to be investigated in this plan'");
        DB::statement("comment on column investigation_plans.number_of_investigators is 'number of users to do investigation in this plan'");
        DB::statement("comment on column investigation_plans.plan_status is 'stage of this plan: 0 = pending, 1 = initiated workflow, 2 = approved, 3 = rejected, 4 = cancelled by user' ");
        DB::statement("comment on column investigation_plans.cancel_reason is 'if cancelled set reason for cancelling'");
        DB::statement("comment on column investigation_plans.user_id is 'user who created this plan'");
        DB::statement("comment on column investigation_plans.cancel_user is 'if cancelled set user who canceled'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigation_plans');
    }
}
