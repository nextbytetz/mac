<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployersRcvableScheduleCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employers', function (Blueprint $table) {
            $table->tinyInteger('rcvable_schedule')->default(0);
        });

        DB::statement("comment on column employers.rcvable_schedule is 'Temporary flag to specify if schedule category when retrieving receivable reports'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
