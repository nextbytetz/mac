<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHspBillSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hsp_bill_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stakeholder_id')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->float('total_amount_claimed')->nullable();
            $table->integer('total_files_claimed')->nullable();
            $table->float('total_amount_vetted')->default(0);
            $table->integer('total_files_vetted')->default(0);
            $table->bigInteger('control_no')->nullable();
            $table->integer('status')->default(0)->comment('0 not vetted, 1 vetted');
            $table->integer('payment_status')->nullable();
            $table->integer('wf_done')->nullable();
            $table->dateTime('wf_done_date')->nullable();
            $table->string('batchno')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_summaries', function (Blueprint $table) {
            Schema::dropIfExists('hsp_bill_summaries');
        });
    }
}
