<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollVerificationsIsdisabled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_verifications', function (Blueprint $table) {
            $table->tinyInteger('isdisabled')->default(0);
        });
        DB::statement("comment on column payroll_verifications.isdisabled is 'Flag to imply that this survivor has disability'");

        Schema::table('payroll_child_extensions', function (Blueprint $table) {
            $table->tinyInteger('isletter_prepared')->default(0);
        });
        DB::statement("comment on column payroll_child_extensions.isletter_prepared is 'Flag to imply that response letter has been prepared'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
