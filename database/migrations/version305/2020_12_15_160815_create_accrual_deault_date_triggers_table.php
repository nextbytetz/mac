<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualDeaultDateTriggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_deault_date_triggers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->unsignedInteger('added_by')->nullable();
            $table->foreign('added_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('changed_by')->nullable();
            $table->foreign('changed_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('fin_year_id')->nullable();
            $table->foreign('fin_year_id')->references('id')->on('main.fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('unit_id')->nullable();
            $table->foreign('unit_id')->references('id')->on('main.units')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('accrual_deault_date_id')->nullable();
            $table->foreign('accrual_deault_date_id')->references('id')->on('main.accrual_deault_dates')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_deault_date_triggers');
    }
}
