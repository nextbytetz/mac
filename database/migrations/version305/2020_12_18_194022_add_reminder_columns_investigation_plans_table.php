<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReminderColumnsInvestigationPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_plans', function (Blueprint $table) {
            $table->boolean('is_reminder_sent')->default(0);
            $table->dateTime('reminder_sent_date')->nullable();
            $table->boolean('is_escalation_sent')->default(0);
            $table->dateTime('escalation_sent_date')->nullable();
        });

        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            $table->boolean('is_investigated')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_plans', function (Blueprint $table) {
           // $table->dropColumn('is_reminder_sent');
           // $table->dropColumn('reminder_sent_date');
           // $table->dropColumn('is_escalation_sent');
           // $table->dropColumn('escalation_sent_date');
       });

        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
          // $table->dropColumn('is_investigated');

      });
    }
}
