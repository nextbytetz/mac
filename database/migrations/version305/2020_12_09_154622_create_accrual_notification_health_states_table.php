<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationHealthStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_health_states', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('health_state_checklist_id')->unsigned()->index('health_state_checklist_id');
            $table->dateTime('from_date')->comment('date from which the member has had the health state outlined.');
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_health_states');
    }
}
