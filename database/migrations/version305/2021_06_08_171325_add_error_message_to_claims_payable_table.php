<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErrorMessageToClaimsPayableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims_payable', function (Blueprint $table) {
           $table->text('error_message')->nullable();
        });

        DB::statement("comment on column main.claims_payable.error_message is 'error messages for failed request to erms'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims_payable', function (Blueprint $table) {
            $table->dropColumn('error_message');
        });
    }
}
