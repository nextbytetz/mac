<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableErmsReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erms_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->date('receipt_date')->nullable();
            $table->string('bank_account_no')->nullable();
            $table->string('bill_number')->nullable();
            $table->string('reference_number')->nullable();
            $table->string('exchequer_no')->nullable();
            $table->string('title')->nullable();
            $table->text('client')->nullable();
            $table->string('generated_by')->nullable();
            $table->float('amount')->default(0);
            $table->integer('status_code')->nullable();
            $table->text('error_message')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('erms_receipts', function (Blueprint $table) {
            Schema::dropIfExists('erms_receipts');
        });
    }
}
