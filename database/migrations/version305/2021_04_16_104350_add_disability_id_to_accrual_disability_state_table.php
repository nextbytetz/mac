<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisabilityIdToAccrualDisabilityStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('accrual_notification_disability_states', function (Blueprint $table) {
             $table->unsignedInteger('disability_id')->nullable();
            $table->foreign('disability_id')->references('id')->on('main.notification_disability_states')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_notification_disability_states', function (Blueprint $table) {
            $table->dropColumn('disability_id');
        });
    }
}
