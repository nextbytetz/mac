<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualPayrollRuns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::drop('manual_payroll_runs');
        Schema::create('manual_payroll_runs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_type_id')->nullable();
            $table->integer('resource_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->string('payee_name');
            $table->string('employee_name');
            $table->string('employer_name');
                    $table->string('bank_name')->nullable();
            $table->string('accountno')->nullable();
            $table->date('payroll_month')->nullable();
            $table->decimal('monthly_pension', 14,2);
            $table->decimal('net_amount', 14,2);
            $table->string('incident_type')->nullable();
            $table->integer('incident_type_id')->nullable();
            $table->string('case_no')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('manual_payroll_runs', function (Blueprint $table) {
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on table manual_payroll_runs is 'Table for manual payroll pension payments for manual files'");
        DB::statement("comment on column manual_payroll_runs.payee_name is 'Beneficiary name i.e. pensioners/survivors'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
