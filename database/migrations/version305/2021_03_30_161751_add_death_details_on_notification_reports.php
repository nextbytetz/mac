<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeathDetailsOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->json("death_claim")->nullable();
        });
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->renameColumn("filename_death", "death_filename");
        });
        DB::statement("comment on column notification_reports.death_claim is 'information about death when added preparing to add death benefit'");
        DB::statement("comment on column notification_reports.death_filename is 'information about death when added preparing to add death benefit'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            //
        });
    }
}
