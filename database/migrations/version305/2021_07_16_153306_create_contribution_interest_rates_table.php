<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionInterestRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contribution_interest_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('fin_code_id')->unsigned();
            $table->integer('employer_category_cv_id')->unsigned();
            $table->float('rate',2,2);
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->smallInteger('wf_status')->default(0);
            $table->boolean('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->text('description');
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('fin_code_id')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column contribution_interest_rates.wf_status is 'Flag to show workflow status i.e. 0 => pending,1 => initiated,2 => approved, 3=> Rejected / Reversed (rejected if wf_done=1, reversed if wf_done=0),4 => Cancelled'");
        DB::statement("comment on column contribution_interest_rates.rate is 'Rate in percentage'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contribution_interest_rates');
    }
}
