<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInspenctionTemplateIdToContributionArrearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal.contribution_arrears', function (Blueprint $table) {
            $table->integer('inspection_template_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.contribution_arrears', function (Blueprint $table) {
            $table->dropColumn('inspection_template_id');
        });
    }
}
