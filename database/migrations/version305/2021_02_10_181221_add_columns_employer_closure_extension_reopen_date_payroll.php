<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsEmployerClosureExtensionReopenDatePayroll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function (Blueprint $table) {
            $table->date("extension_reopen_date")->nullable();
            $table->string("ispayroll_confirmed")->default(0);
            $table->string("payroll_ids")->nullable();
        });

        DB::statement("comment on column employer_closures.extension_reopen_date is 'Recent expected reopen date from extension request for internal use'");
        DB::statement("comment on column employer_closures.ispayroll_confirmed is 'Flag imply that employer has multiple payroll and need to confirm which payroll(s) have been closed'");
        DB::statement("comment on column employer_closures.payroll_ids is 'Portal payroll which was closed'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
