<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualDependentEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_dependent_employee', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('accrual_dependent_id')->nullable();
            $table->foreign('accrual_dependent_id')->references('id')->on('main.accrual_dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('employee_id')->nullable();
            $table->foreign('employee_id')->references('id')->on('main.employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('dependent_type_id')->nullable();
            $table->foreign('dependent_type_id')->references('id')->on('main.dependent_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->decimal('survivor_pension_percent', 5)->default(0)->nullable();
            $table->decimal('survivor_pension_amount', 15)->default(0)->nullable()->comment('amount of pension the dependent is eligible to be paid.');
            $table->decimal('survivor_gratuity_percent', 5)->default(0)->nullable();
            $table->smallInteger('survivor_gratuity_flag')->default(0)->comment('Determine whether is receiving gratuity / lump sum or not, 1 - yes receiving, 0 - not receiving ');
            $table->smallInteger('survivor_gratuity_pay_flag')->default(0)->comment('Determine whether has already been paid gratuity / lump sum or not, 1 - processed / paid, 0 - not yet paid / processed ');
            // $table->smallInteger('suspense_flag')->default(0)->comment('set whether dependent is suspended or not, 1 - suspended, 0 - not suspended.');
            $table->smallInteger('survivor_pension_flag')->default(0)->comment('    Determine whether is receiving pension or not, e.g children taken care by an administrator might not be receiving pension. 1 - yes receiving, 0 - not receiving ');
            $table->decimal('funeral_grant_percent', 5)->nullable();
            $table->decimal('funeral_grant', 14)->default(0)->nullable();
            $table->smallInteger('funeral_grant_pay_flag')->default(0)->comment('set whether dependent has already been paid funeral grant or not, 1 - paid, 0 - not paid yet');
            $table->decimal('survivor_gratuity_amount', 14)->default(0)->nullable()->comment('amount of gratuity the dependent is eligible to be paid.');
            $table->decimal('dependency_percentage', 5)->nullable()->comment('Percentage of dependency of Other dependents i.e dependents other than children and spouses');

            $table->smallInteger('isactive')->default(0)->comment('specify whether the dependent is active to receive monthly pension');
            $table->smallInteger('isresponded')->default(0)->comment('specify whether the dependent has responded to the claim compensation award to confirm bank details and his/her information');
            $table->smallInteger('firstpay_flag')->default(0)->comment('specify whether the dependent has already started to receive monthly pension');

            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('main.accrual_dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('notification_eligible_benefit_id')->nullable();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('manual_notification_report_id')->nullable();
            $table->foreign('manual_notification_report_id')->references('id')->on('main.manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->text('iserpsupplier')->default(0)->comment('Flag to specify if dependent is exported to erp as supplier i.e. 1 => ready exported, 0 => not yet');
            $table->integer('old_mp')->nullable();
            $table->integer('isoverage_eligible')->nullable();
            $table->integer('isotherdep')->nullable();
            $table->integer('other_dependency_type')->nullable();
            $table->integer('pay_period_months')->nullable();
            $table->integer('recycles_pay_period')->nullable();

            $table->text('hold_reason')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_dependent_employee');
    }
}