<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResolutionAndCommentColumnsInCommitmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal.commitment_requests', function (Blueprint $table) {
            $table->string('resolutions')->nullable();
            $table->text('comments')->nullable();
            $table->string('mac_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.commitment_requests', function (Blueprint $table) {
           $table->dropColumn('resolutions');
           $table->dropColumn('comments');
           $table->dropColumn('mac_user');
       });
    }
}
