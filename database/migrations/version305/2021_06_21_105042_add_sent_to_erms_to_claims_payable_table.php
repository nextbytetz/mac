<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSentToErmsToClaimsPayableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims_payable', function (Blueprint $table) {
           $table->boolean('sent_to_erms')->default(0);
           $table->integer('erms_status_code')->nullable();
       });
        DB::statement("comment on column main.claims_payable.sent_to_erms is 'The status of the claim if sent to eGA erms i.e. 1 => Sent, 0 => Not Sent'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims_payable', function (Blueprint $table) {
            $table->dropColumn('sent_to_erms');
            $table->dropColumn('erms_status_code');
        });
    }
}
