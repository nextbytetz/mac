<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewStatusToClaimFollowups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_followups', function (Blueprint $table) {
            $table->integer('review_status')->nullable()->comment('0 => Pending, 1 => Approved, 2 => Reversed, 3 => Resubmitted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_followups', function (Blueprint $table) {
            $table->dropColumn('review_status');
        });
    }
}
