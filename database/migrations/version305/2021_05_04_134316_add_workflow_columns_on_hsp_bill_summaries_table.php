<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkflowColumnsOnHspBillSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hsp_bill_summaries', function (Blueprint $table) {
            $table->integer('wf_initiator')->unsigned()->nullable();
            $table->integer('wf_status')->default(0);
            $table->foreign('wf_initiator')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column hsp_bill_summaries.wf_status is 'workflow status of this plan: 0 = not initiated, 1 = initiated workflow, 2 = approved, 3 = rejected (wf_done == 1) / reversed (wf_done == 0)' ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_summaries', function (Blueprint $table) {
            //
        });
    }
}
