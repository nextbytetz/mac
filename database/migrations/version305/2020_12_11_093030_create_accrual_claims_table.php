<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_claims', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('incident_type_id')->nullable();
            $table->foreign('incident_type_id')->references('id')->on('main.incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->decimal('monthly_earning', 14)->comment('recent salary amount.');
            $table->integer('day_hours')->nullable()->comment('hours worked per day by a member');
            $table->unsignedInteger('exit_code_id')->nullable();
            $table->foreign('exit_code_id')->references('id')->on('main.exit_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->boolean('iscomplete')->default(0)->comment('set whether is complete or not, 1 - claim process is complete, 0 - claim process is not complete');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->softDeletes();
            $table->integer('pd')->nullable();
            $table->date('date_of_mmi')->nullable();
            $table->integer('need_rehabilitation')->default(0);
            $table->integer('need_cca')->default(0);
            $table->integer('cca_registered')->default(0);
            $table->integer('assessed')->default(0);
            $table->text('diagnosis')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_claims');
    }
}
