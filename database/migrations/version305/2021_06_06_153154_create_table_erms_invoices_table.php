<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableErmsInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erms_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->date('receivable_date')->nullable();
            $table->string('bill_number')->nullable();
            $table->string('control_number')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('department_code')->nullable();
            $table->text('client')->nullable();
            $table->string('generated_by')->nullable();
            $table->float('amount')->default(0);
            $table->string('currency_code')->default('TZS');
            $table->float('exchange_rate')->default(0);
            $table->integer('status_code')->nullable();
            $table->text('error_message')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('erms_invoices', function (Blueprint $table) {
            Schema::dropIfExists('erms_invoices');
        });
    }
}
