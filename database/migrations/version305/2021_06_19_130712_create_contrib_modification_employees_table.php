<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContribModificationEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrib_modification_employees', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('redistribution_id')->unsigned();
            $table->integer('memberno')->nullable();
            $table->integer('employer_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->string('firstname', 100);
            $table->string('middlename', 100)->nullable();
            $table->string('lastname', 100);
            $table->date('dob')->nullable();
            $table->decimal('basicpay', 14)->nullable();
            $table->decimal('grosspay', 14)->nullable();
            $table->string('sex', 10)->nullable();
            $table->string('job_title', 150)->nullable();
            $table->string('emp_cate', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('redistribution_id')->references('id')->on('contrib_modification_redistributions')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrib_modification_employees');
    }
}
