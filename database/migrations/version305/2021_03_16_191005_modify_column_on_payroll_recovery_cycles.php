<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnOnPayrollRecoveryCycles extends Migration
{
    use \Database\DisableForeignKeys;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_recoveries')");


        Schema::table('payroll_recoveries', function (Blueprint $table) {
            // $table->decimal('cycles',10,4)->change();
        });
        DB::statement("select deps_restore_dependencies('main', 'payroll_recoveries')");


        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_deductions')");
        Schema::table('payroll_deductions', function (Blueprint $table) {
            // $table->decimal('recycles',10,4)->change();
            // $table->decimal('actual_recycles',10,4)->nullable();


        });

        \Illuminate\Support\Facades\DB::statement(" update payroll_deductions set actual_recycles = recycles");
        DB::statement("select deps_restore_dependencies('main', 'payroll_deductions')");

        Schema::table('payroll_retiree_mp_updates', function (Blueprint $table) {

            // $table->decimal('cycles_actual',10,4)->default(0)->change();


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
