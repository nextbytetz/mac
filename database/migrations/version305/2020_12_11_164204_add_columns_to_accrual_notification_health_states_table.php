<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAccrualNotificationHealthStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_notification_health_states', function (Blueprint $table) {
            $table->unsignedInteger('notification_eligible_benefit_id')->nullable();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('state')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_notification_health_states', function (Blueprint $table) {
            $table->dropForeign('notification_eligible_benefit_id');
            $table->dropColumn('state');
        });
    }
}
