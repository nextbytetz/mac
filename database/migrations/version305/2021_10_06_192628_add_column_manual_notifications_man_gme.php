<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnManualNotificationsManGme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->decimal('man_gme', 12,2)->nullable();
        });

        DB::statement("comment on column manual_notification_reports.man_gme is 'Manual gross monthly earning'");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
