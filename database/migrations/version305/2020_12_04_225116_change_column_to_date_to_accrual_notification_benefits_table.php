<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnToDateToAccrualNotificationBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('accrual_notification_benefits', 'admin_from_date'))
        {
            Schema::table('accrual_notification_benefits', function (Blueprint $table)
            {
                $table->dropColumn('admin_from_date');
            });

        }
        Schema::table('accrual_notification_benefits', function (Blueprint $table)
        {
            $table->date('admin_from_date');
        });


        if (Schema::hasColumn('accrual_notification_benefits', 'admin_to_date'))
        {
            Schema::table('accrual_notification_benefits', function (Blueprint $table)
            {
                $table->dropColumn('admin_to_date');
            });

        }
        Schema::table('accrual_notification_benefits', function (Blueprint $table)
        {
            $table->date('admin_to_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
