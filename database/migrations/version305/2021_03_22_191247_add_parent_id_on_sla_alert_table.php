<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdOnSlaAlertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sla_alerts', function (Blueprint $table) {
            $table->smallInteger("parent_id")->nullable();
            $table->smallInteger("days_accumulative")->nullable();
        });
        DB::statement("comment on column sla_alerts.days_accumulative is 'total number of days qualifying to notify this user'");
        DB::statement("comment on column sla_alerts.parent_id is 'reference to the previous sla of the same kind notifying a lower designation user'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sla_alerts', function (Blueprint $table) {
            //
        });
    }
}
