<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutodateOnNotificationDisabilityStatesObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_disability_states', function (Blueprint $table) {
            $table->smallInteger("autodate")->default(0);
        });
        DB::statement("comment on column notification_disability_states.autodate is 'specify whether the from date has been specified by user or not. 1 - Yes, 0 - Computed by System'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_disability_states', function (Blueprint $table) {
            //
        });
    }
}
