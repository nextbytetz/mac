<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommitmentColumnInStaffEmployerFollowUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('staff_employer_follow_ups', function (Blueprint $table) {
            $table->boolean('is_commitment')->default(false);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_employer_follow_ups', function (Blueprint $table) {
            $table->dropColumn('is_commitment');
        });
    }
}
