<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnInvestigationPlanNotificationReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            $table->integer('removal_reason')->nullable();
        });
        DB::statement("comment on column investigation_plan_notification_reports.removal_reason is 'removal reason: 1 => By plan creator, 2 => By manager decline'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            //
        });
    }
}
