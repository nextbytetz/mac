<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfInitiatorColumnOnInvestigationPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_plans', function (Blueprint $table) {
            $table->unsignedInteger('wf_initiator')->nullable();
            $table->integer('wf_done')->default(0)->change();
            $table->foreign('wf_initiator')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_plans', function (Blueprint $table) {
            //
        });
    }
}
