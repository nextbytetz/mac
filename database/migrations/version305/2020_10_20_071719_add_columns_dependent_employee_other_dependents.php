<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDependentEmployeeOtherDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependent_employee', function (Blueprint $table) {
            $table->smallInteger('isotherdep')->default(0);
            $table->smallInteger('other_dependency_type')->nullable();
            $table->decimal('pay_period_months',6,2)->nullable();
            $table->decimal('recycles_pay_period',6,2)->nullable();


        });

        DB::statement("comment on column dependent_employee.other_dependency_type is 'Other dependency type i.e. 1 => Full, 2 => Partial'");
        DB::statement("comment on column dependent_employee.pay_period_months is 'Eligible no of payable months depending on dependency type'");
        DB::statement("comment on column dependent_employee.recycles_pay_period is 'Remained no of payable months'");
        DB::statement("comment on column dependent_employee.isotherdep is 'Flag to specify if dependent is other i.e. 1 => other dependent, 0 = dependent'");

        Schema::table('sysdefs', function (Blueprint $table) {
//            $table->decimal('max_payable_otherdep_partial',15,2)->nullable();
            $table->decimal('max_payable_otherdep_full',15,2)->nullable();
            $table->decimal('min_payable_otherdep_full',15,2)->nullable();
            $table->decimal('max_payable_months_otherdep_full',6,2)->nullable();
            $table->decimal('max_payable_months_otherdep_partial',6,2)->nullable();
            $table->decimal('otherdep_full_pension_percent',5,2)->nullable();



        });

//        DB::statement("comment on column sysdefs.max_payable_otherdep_partial is 'Maximum gratuity amount payable for other dependents with partial dependency'");
        DB::statement("comment on column sysdefs.max_payable_otherdep_full is 'Maximum pension amount payable for other dependents with full dependency'");
        DB::statement("comment on column sysdefs.min_payable_otherdep_full is 'Minimum pension amount payable for other dependents with full dependency'");
        DB::statement("comment on column sysdefs.max_payable_months_otherdep_full is 'Maximum pension months payable for other dependents with full dependency'");
        DB::statement("comment on column sysdefs.max_payable_months_otherdep_partial is 'Maximum pension months payable for other dependents with partial dependency'");
        DB::statement("comment on column sysdefs.otherdep_full_pension_percent is 'Pension percent for other dependents with full dependency'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
