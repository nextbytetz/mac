<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMedicalExpenseToNullableInAccrualNotificationHealthProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_notification_health_providers', function (Blueprint $table) {

            // $table->unsignedInteger('medical_expense_id')->nullable();
            // $table->foreign('medical_expense_id')->references('id')->on('main.medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->unsignedInteger('medical_expense_id')->nullable()->change();
            $table->foreign('medical_expense_id')->references('id')->on('main.medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_notification_health_providers', function (Blueprint $table) {

            // $table->dropForeign('accrual_medical_expense_id');
            $table->unsignedInteger('medical_expense_id');
            $table->foreign('medical_expense_id')->references('id')->on('main.medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }
}
