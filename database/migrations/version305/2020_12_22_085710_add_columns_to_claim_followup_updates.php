<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToClaimFollowupUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_followup_updates', function (Blueprint $table) {
            $table->unsignedInteger('postcode_id')->nullable();
            $table->foreign('postcode_id')->references('id')->on('main.postcodes')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->text('location_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_followup_updates', function (Blueprint $table) {
            $table->dropForeign('postcode_id');
            $table->dropColumn('location_description');
        });
    }
}
