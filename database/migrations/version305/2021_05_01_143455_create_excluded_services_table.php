<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExcludedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('excluded_services', function (Blueprint $table) {
            $table->unsignedInteger('medical_price_package_id')->nullable();
            $table->foreign('medical_price_package_id')->references('id')->on('medical_price_packages')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('scheme_id')->nullable();
            $table->text('excluded_for_products')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('excluded_services', function (Blueprint $table) {
            Schema::dropIfExists('excluded_services');
        });
    }
}
