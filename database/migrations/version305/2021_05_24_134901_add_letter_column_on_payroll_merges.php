<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLetterColumnOnPayrollMerges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_payroll_merges', function (Blueprint $table) {
           $table->text('letter_reference')->nullable();
           $table->date('letter_date')->nullable();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_payroll_merges', function (Blueprint $table) {
            //
        });
    }
}
