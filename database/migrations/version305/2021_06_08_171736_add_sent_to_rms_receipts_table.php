<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSentToRmsReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function (Blueprint $table) {
           $table->boolean('sent_to_erms')->default(0);
       });
        DB::statement("comment on column main.receipts.sent_to_erms is 'The status of the receipt if sent to eGA erms i.e. 1 => Sent, 0 => Not Sent'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipts', function (Blueprint $table) {
            $table->dropColumn('sent_to_erms');
        });
    }
}
