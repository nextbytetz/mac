<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnInvestigationPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_plans', function (Blueprint $table) {
            $table->boolean('is_complete')->default(false);
            $table->unsignedInteger('individual_mode')->nullable();

        });
        DB::statement("comment on column investigation_plans.individual_mode is 'mode of conducting individual investigation: 1 => alone, 2 => with other investigator(s)'");
        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            $table->integer('investigation_plan_user_id')->unsigned()->nullable();
            $table->foreign('investigation_plan_user_id')->references('id')->on('main.investigation_plan_users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_plans', function (Blueprint $table) {
            // $table->dropColumn('is_complete');
            // $table->dropColumn('individual_mode');
        });

        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            // $table->dropColumn('investigation_plan_user_id');
        });
    }
}
