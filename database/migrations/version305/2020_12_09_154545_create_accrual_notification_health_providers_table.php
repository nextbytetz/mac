<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationHealthProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_health_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->unsignedInteger('health_provider_id')->nullable();
            $table->foreign('health_provider_id')->references('id')->on('main.health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->integer('medical_expense_id')->unsigned()->index('medical_expense_id');
            $table->text('remarks')->nullable();
            $table->integer('rank')->comment('beginning with 1, the first health provider to the rest, increment in numbers');
            $table->date('attend_date')->nullable()->comment('date which a member was attended to the health provider');
            $table->date('dismiss_date')->nullable()->comment('date which a member was dismissed from the health provider');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_health_providers');
    }
}
