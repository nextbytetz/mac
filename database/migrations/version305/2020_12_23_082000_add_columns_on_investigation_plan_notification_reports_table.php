<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnInvestigationPlanNotificationReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            $table->unsignedInteger('old_notifiation_investigator')->nullable();
            $table->foreign('old_notifiation_investigator')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        
        DB::statement("comment on column investigation_plan_notification_reports.old_notifiation_investigator is 'user who was assigned this investigation on table  notifiation_investigator'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            //
        });
    }
}
