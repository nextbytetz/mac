<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataObjectsForTransferToDeath extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incident_types', function (Blueprint $table) {
            $table->smallInteger("isregister")->default(1);
        });
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->string("filename_death", 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incident_types', function (Blueprint $table) {
            //
        });
    }
}
