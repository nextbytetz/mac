<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationDisabilityStateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_disability_state_assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('accrual_notification_disability_state_id')->nullable();
            $table->foreign('accrual_notification_disability_state_id')->references('id')->on('main.accrual_notification_disability_states')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('days')->nullable();
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_disability_state_assessments');
    }
}
