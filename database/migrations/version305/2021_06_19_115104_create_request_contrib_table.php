<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestContribTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('contrib_modifications', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('employer_id');
        $table->unsignedInteger('receipt_id')->nullable();
        $table->integer('rctno')->unsigned()->nullable();
        $table->text('old_rct_description');
        $table->text('new_rct_description')->nullable();
        $table->integer('request_type')->unsigned();
        $table->date('date_received')->nullable();
        $table->text('reason', 65535);
        $table->unsignedInteger('user_id');
        $table->string("user_type", 150)->default('App\Models\Auth\User');
        $table->smallInteger('source')->nullable()->default(1);
        $table->integer('status')->default(1);
        $table->integer('payroll_id');
        $table->tinyInteger('wf_done')->default(0);
        $table->date('wf_done_date')->nullable();
        $table->text('reject_reason', 65535)->nullable();
        $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        $table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        $table->timestamps();
        $table->softDeletes();
     });
     DB::statement("comment on column contrib_modifications.status is 'Shows status of this request 1=>pending for approval 2=>Approved, 3=>Rejected if wf_done = 1 or reversed if wf_done = 0'");
     DB::statement("comment on column contrib_modifications.request_type is 'Shows type of Modification requested 1=>change contribution month(s), 2=>redistribution of contribution amount, 3=>contribution underpayment'");
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('contrib_modifications');
   }
}
