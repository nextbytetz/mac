<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewPayrollRunsWithManualView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_runs_with_manual_view')");
        DB::statement("DROP  VIEW IF EXISTS  payroll_runs_with_manual_view");
        DB::statement("CREATE VIEW main.payroll_runs_with_manual_view AS


select *, 0 as islegacy from payroll_runs_view

UNION ALL

SELECT  manual_payroll_runs.id             AS run_id,
        payroll_beneficiaries_view.member_name,
        payroll_beneficiaries_view.employee_name,
        payroll_beneficiaries_view.memberno,
        manual_payroll_runs.net_amount,
        1 as months_paid,
        manual_payroll_runs.monthly_pension,
        null as payroll_run_approval_id,
        1 as wf_done,
        0 as arrears_amount,
        0 as  deductions_amount,
        0 as unclaimed_amount,
        manual_payroll_runs.employee_id,
        member_types.name           AS member_type_name,
        manual_payroll_runs.payroll_month as run_date,
        null       AS bank_branch_name,
        manual_payroll_runs.bank_name                  AS bank_name,
        manual_payroll_runs.accountno      AS run_accountno,
       null AS run_bank_branch_id,
        null AS   run_bank_id,
        manual_payroll_runs.resource_id,
        manual_payroll_runs.member_type_id,
       0 as new_payee_flag,
        0 as isconstantcare,
        payroll_beneficiaries_view.filename,
        payroll_beneficiaries_view.gender,
        payroll_beneficiaries_view.relationship,
        1 as islegacy
FROM ((main.manual_payroll_runs
    JOIN main.member_types ON ((member_types.id = manual_payroll_runs.member_type_id)))
    JOIN main.payroll_beneficiaries_view ON (((payroll_beneficiaries_view.resource_id = manual_payroll_runs.resource_id) AND
                                              (payroll_beneficiaries_view.member_type_id =
                                               manual_payroll_runs.member_type_id))))
WHERE (manual_payroll_runs.deleted_at IS NULL);




");

        DB::statement("select deps_restore_dependencies('main', 'payroll_runs_with_manual_view')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
