<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableErmsEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erms_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('gfs_code')->nullable();
            $table->bigInteger('account_code')->nullable();
            $table->string('book_side')->nullable();
            $table->boolean('service_entry')->default(false);
            $table->boolean('tax_entry')->default(false);
            $table->text('description')->nullable();
            $table->float('amount')->default(0);
            $table->integer('source')->nullable();
            $table->integer('resource_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on column main.erms_entries.resource_id is 'The parent table may be erms_receipts or erms_invoices'");
        DB::statement("comment on column main.erms_entries.source is 'The source table i.e. 1 => erms_receipts, 2 => erms_invoices'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('erms_entries', function (Blueprint $table) {
            Schema::dropIfExists('erms_entries');
        });
    }
}
