<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationHealthProviderPractitionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_health_provider_practitioners', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('medical_practitioner_id')->nullable();
            $table->foreign('medical_practitioner_id')->references('id')->on('main.medical_practitioners')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->unsignedInteger('accrual_notification_health_provider_id')->nullable();
            $table->foreign('accrual_notification_health_provider_id')->references('id')->on('main.accrual_notification_health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->date('from_date');
            $table->date('to_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_health_provider_practitioners');
    }
}
