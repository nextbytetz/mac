<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualClaimCompensationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_claim_compensations', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('accrual_claim_id')->nullable();
          $table->foreign('accrual_claim_id')->references('id')->on('main.accrual_claims')->onUpdate('CASCADE')->onDelete('RESTRICT');
          $table->unsignedInteger('member_type_id')->nullable();
          $table->foreign('member_type_id')->references('id')->on('main.member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
          $table->decimal('amount', 14)->nullable();
          $table->unsignedInteger('benefit_type_id')->nullable();
          $table->foreign('benefit_type_id')->references('id')->on('main.benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
          $table->unsignedInteger('compensation_payment_type_id')->nullable();
          $table->foreign('compensation_payment_type_id')->references('id')->on('main.compensation_payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
          $table->boolean('ispaid')->default(0)->comment('set whether is complete or not, 1 - claim process is complete, 0 - claim process is not complete');
          $table->unsignedInteger('user_id')->nullable();
          $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
          $table->unsignedInteger('notification_eligible_benefit_id')->nullable();
          $table->foreign('notification_eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
          $table->softDeletes();
          $table->integer('resource_id')->nullable();
          $table->date('resource_origin')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_claim_compensations');
    }
}
