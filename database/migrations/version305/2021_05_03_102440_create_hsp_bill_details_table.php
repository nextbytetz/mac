<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHspBillDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hsp_bill_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('hsp_summary_id')->nullable();
            $table->foreign('hsp_summary_id')->references('id')->on('hsp_bill_summaries')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->string('memberno')->nullable();
            $table->string('patient_names')->nullable();
            $table->bigInteger('wcf_authno')->nullable();
            $table->bigInteger('sh_authno')->nullable();
            $table->dateTime('visit_date')->nullable();
            $table->string('treatment_file')->nullable();
            $table->bigInteger('bill_no')->nullable();
            $table->float('bill_amount')->nullable();
            $table->string('medical_practitioner')->nullable();
            $table->string('ranking')->nullable();
            $table->string('facility_code')->nullable();
            $table->boolean('is_reviewed')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_details', function (Blueprint $table) {
            Schema::dropIfExists('hsp_bill_details');
        });
    }
}
