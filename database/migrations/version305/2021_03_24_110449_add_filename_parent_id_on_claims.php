<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilenameParentIdOnClaims extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims', function (Blueprint $table) {
            $table->bigInteger("parent_id")->nullable();
            $table->string("filename", 50)->unique()->nullable();
        });
        DB::statement("comment on column claims.parent_id is 'link to the previous claim before creating new one'");
        DB::statement("comment on column claims.filename is 'filename of the claim'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims', function (Blueprint $table) {
            //
        });
    }
}
