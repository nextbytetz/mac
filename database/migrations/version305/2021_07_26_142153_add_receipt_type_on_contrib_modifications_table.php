<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceiptTypeOnContribModificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrib_modifications', function (Blueprint $table) {
            $table->dropForeign(['receipt_id']);
            $table->integer('receipt_type')->default(1);
        });

        Schema::table('contrib_modification_months', function (Blueprint $table) {
            $table->dropForeign(['receipt_code_id']);
            $table->integer('receipt_code_type')->default(1);
        });

        DB::statement("comment on column contrib_modifications.receipt_type is 'Shows the source of receipt to edit 1=>receipts  2=>legacy'");
        DB::statement("comment on column contrib_modification_months.receipt_code_type is 'Shows the source of receipt to edit 1=>receipt_codes  2=>legacy_receipt_codes'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrib_modifications', function (Blueprint $table) {
            //
        });
    }
}
