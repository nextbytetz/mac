<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnStateAccrualNotificationHealthProviderServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_notification_health_provider_services', function (Blueprint $table) {
            $table->dropColumn('to_date');
        });

        Schema::table('accrual_notification_health_provider_services', function (Blueprint $table) {
            $table->date('to_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_notification_health_provider_services', function (Blueprint $table) {

        });
        
    }
}
