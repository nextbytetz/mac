<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationPlanNotificationReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigation_plan_notification_reports', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('investigation_plan_id')->unsigned();
         $table->integer('notification_report_id')->unsigned();
         $table->integer('assigned_to')->unsigned();
         $table->integer('added_by')->unsigned();
         $table->integer('removed_by')->unsigned()->nullable();
         $table->timestamps();
         $table->softDeletes();
         $table->foreign('investigation_plan_id')->references('id')->on('main.investigation_plans')->onUpdate('CASCADE')->onDelete('RESTRICT');
         $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
         $table->foreign('assigned_to')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
         $table->foreign('added_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
         $table->foreign('removed_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigation_plan_notification_reports');
    }
}
