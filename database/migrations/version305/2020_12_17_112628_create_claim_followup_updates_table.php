<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimFollowupUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_followup_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('claim_followup_id')->nullable();
            $table->foreign('claim_followup_id')->references('id')->on('main.claim_followups')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('follow_up_type_cv_id')->nullable();
            $table->foreign('follow_up_type_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->dateTime('week_date')->nullable();
            $table->text('remark')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('contact')->nullable();
            $table->string('email')->nullable();
            $table->dateTime('date_of_follow_up');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_followup_updates');
    }
}
