<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPensionersRetireeFeedbackCvId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pensioners', function (Blueprint $table) {
            $table->integer('retiree_feedback_cv_id')->nullable();
        });

        DB::statement("comment on column pensioners.retiree_feedback_cv_id is 'Most recent retiree followup feedback. Filled on alert if still working/ or mp update wf on complete'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
