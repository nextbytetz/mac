<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWithholdingTaxToInvestmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main.investiment_fixed_deposits', function (Blueprint $table) {
            $table->float('with_holding_tax')->nullable();
        });
         Schema::table('main.investiment_treasury_bills', function (Blueprint $table) {
            $table->float('with_holding_tax')->nullable();
        });
          Schema::table('main.investiment_treasury_bonds', function (Blueprint $table) {
            $table->float('with_holding_tax')->nullable();
        });
           Schema::table('main.investiment_corporate_bonds', function (Blueprint $table) {
            $table->float('with_holding_tax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
