<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentWfOnAccrual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('accrual_notification_reports', function (Blueprint $table) {
            $table->integer('current_wf_level')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accrual_notification_reports', function (Blueprint $table) {
            $table->dropColumn('current_wf_level');
        });
    }
}
