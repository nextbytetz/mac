<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemsToHspBillDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hsp_bill_details', function (Blueprint $table) {
            $table->integer('bill_items')->default(0);
            $table->integer('vetted_items')->default(0);
            $table->float('vetted_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_details', function (Blueprint $table) {
            $table->dropColumn('bill_items');
            $table->dropColumn('vetted_items');
            $table->dropColumn('vetted_amount');
        });
    }
}
