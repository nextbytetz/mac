<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_benefits', function (Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('accrual_benefit_disability_id')->nullable();
           $table->foreign('accrual_benefit_disability_id')->references('id')->on('main.accrual_benefit_disabilities')->onUpdate('CASCADE')->onDelete('RESTRICT');
           $table->unsignedInteger('disability_id')->nullable();
           $table->foreign('disability_id')->references('id')->on('main.disability_state_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
           $table->integer('admin_days')->nullable();
           $table->integer('admin_from_date')->nullable();
           $table->integer('admin_to_date')->nullable();
           $table->integer('assessed_days')->nullable();
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_benefits');
    }
}
