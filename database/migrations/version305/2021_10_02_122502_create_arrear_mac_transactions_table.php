<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArrearMacTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal.arrear_mac_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('arrear_id');
            $table->unsignedInteger('mac_user_id');
            $table->integer('status')->nullable();
            $table->text('reason')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->foreign('arrear_id')->references('id')->on('portal.contribution_arrears')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('portal.arrear_mac_transactions');
    }
}
