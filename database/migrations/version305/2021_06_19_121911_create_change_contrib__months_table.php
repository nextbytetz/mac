<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeContribMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('contrib_modification_months', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('contrib_modification_id')->unsigned();
            $table->date('old_month');
            $table->date('new_month');
            $table->timestamps();
            $table->foreign('contrib_modification_id')->references('id')->on('contrib_modifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrib_modification_months');
    }
}
