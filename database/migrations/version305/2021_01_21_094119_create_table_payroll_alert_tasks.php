<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrollAlertTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('payroll_alert_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_type_id');
            $table->integer('resource_id');
            $table->integer('resource_pivot_id')->nullable();
            $table->integer('staging_cv_id');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('shouldcount')->default(0);
            $table->tinyInteger('notify')->default(0);
            $table->tinyInteger('priority')->default(0);
            $table->integer('user_id');

            $table->timestamps();
        });

        Schema::table('payroll_alert_tasks', function (Blueprint $table) {
            $table->foreign('staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column payroll_alert_tasks.status is 'show whether the resource has been worked. 0 - Pending, 1 - Issued, 2 - Declined by system'");
        DB::statement("comment on column payroll_alert_tasks.priority is 'set the priority of the checker if it requires immediate issuance. 1 - High, 0 - Low. Entries with priority low can be worked over days'");
        DB::statement("comment on column payroll_alert_tasks.notify is 'indicate that checkers should blink for users to pay attention to the pendings'");
        DB::statement("comment on column payroll_alert_tasks.shouldcount is 'show whether check entry should count in the summary show'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
