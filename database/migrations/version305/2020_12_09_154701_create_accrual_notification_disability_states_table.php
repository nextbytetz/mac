<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationDisabilityStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_disability_states', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->unsignedInteger('disability_state_checklist_id')->nullable();
            $table->foreign('disability_state_checklist_id')->references('id')->on('main.disability_state_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('notification_eligible_benefit_id')->nullable();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->dateTime('from_date')->nullable()->comment('date from which the member has had the health state outlined.');
            $table->dateTime('to_date')->nullable()->comment('date from which the member has had the health state outlined.');
            $table->float('percent_of_ed_ld')->nullable();

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_disability_states');
    }
}
