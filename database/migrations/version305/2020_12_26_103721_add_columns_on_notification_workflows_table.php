<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnNotificationWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
            $table->unsignedInteger('document_notification_report_id')->nullable();
            $table->date('document_date')->nullable();
            $table->foreign('document_notification_report_id')->references('id')->on('main.document_notification_report')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
            $table->dropColumn('document_notification_report_id');
            $table->dropColumn('document_date');
        });
    }
}
