<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnInvestigationPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::table('investigation_plans', function (Blueprint $table) 
     {
        $table->decimal('total_budget', 20,2)->nullable();
        $table->text('budget_attachment')->nullable();
        $table->text('budget_uploaded_name')->nullable();
    });
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_plans', function (Blueprint $table) {
            //
        });
    }
}
