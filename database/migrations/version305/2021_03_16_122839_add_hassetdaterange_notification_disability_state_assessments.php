<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHassetdaterangeNotificationDisabilityStateAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_disability_state_assessments', function (Blueprint $table) {
            $table->smallInteger("hassetrange")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_disability_state_assessments', function (Blueprint $table) {
            //
        });
    }
}
