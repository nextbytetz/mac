<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyViewPayrollRunsDeletedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_runs_view')");
        DB::statement("DROP  VIEW IF EXISTS  payroll_runs_view");
        DB::statement("CREATE VIEW main.payroll_runs_view AS
  SELECT payroll_beneficiaries_view.member_name,
         payroll_beneficiaries_view.employee_name,
         payroll_beneficiaries_view.memberno,
         payroll_runs.amount,
         payroll_runs.months_paid,
         payroll_runs.monthly_pension,
         payroll_runs.payroll_run_approval_id,
         payroll_run_approvals.wf_done,
         payroll_runs.arrears_amount,
         payroll_runs.deductions_amount,
         payroll_runs.unclaimed_amount,
         payroll_runs.employee_id,
         member_types.name           AS member_type_name,
         payroll_procs.run_date,
         bank_branches.name          AS bank_branch_name,
         banks.name                  AS bank_name,
         payroll_runs.accountno      AS run_accountno,
         payroll_runs.bank_branch_id AS run_bank_branch_id,
         payroll_runs.bank_id        AS run_bank_id,
         payroll_runs.resource_id,
         payroll_runs.member_type_id,
         payroll_runs.new_payee_flag,
         payroll_runs.isconstantcare,
         payroll_beneficiaries_view.filename,
         payroll_beneficiaries_view.gender,
         payroll_beneficiaries_view.relationship
  FROM ((((((main.payroll_runs
      JOIN main.payroll_run_approvals ON ((payroll_runs.payroll_run_approval_id = payroll_run_approvals.id)))
      JOIN main.payroll_procs ON ((payroll_run_approvals.payroll_proc_id = payroll_procs.id)))
      JOIN main.member_types ON ((member_types.id = payroll_runs.member_type_id)))
      JOIN main.payroll_beneficiaries_view ON (((payroll_beneficiaries_view.resource_id = payroll_runs.resource_id) AND
                                                (payroll_beneficiaries_view.member_type_id =
                                                 payroll_runs.member_type_id))))
      LEFT JOIN main.bank_branches ON ((bank_branches.id = payroll_runs.bank_branch_id)))
      LEFT JOIN main.banks ON ((banks.id = payroll_runs.bank_id)))
      where payroll_run_approvals.deleted_at is null;


");

        DB::statement("select deps_restore_dependencies('main', 'payroll_runs_view')");



        DB::statement("select deps_save_and_drop_dependencies('main', 'payroll_suspended_runs_view')");
        DB::statement("DROP  VIEW IF EXISTS  payroll_suspended_runs_view");
        DB::statement("CREATE VIEW main.payroll_suspended_runs_view AS
  SELECT payroll_beneficiaries_view.member_name,
         payroll_beneficiaries_view.employee_name,
         payroll_beneficiaries_view.memberno,
         payroll_suspended_runs.amount,
         payroll_suspended_runs.months_paid,
         payroll_suspended_runs.monthly_pension,
         payroll_suspended_runs.payroll_run_approval_id,
         payroll_run_approvals.wf_done,
         payroll_suspended_runs.ispaid,
         payroll_suspended_runs.arrears_amount,
         payroll_suspended_runs.deductions_amount,
         payroll_suspended_runs.unclaimed_amount,
         payroll_suspended_runs.employee_id,
         member_types.name           AS member_type_name,
         payroll_procs.run_date,
         payroll_suspended_runs.resource_id,
         payroll_suspended_runs.member_type_id,
         payroll_suspended_runs.isconstantcare,
         payroll_beneficiaries_view.filename,
         payroll_beneficiaries_view.gender,
         payroll_beneficiaries_view.relationship
  FROM ((((((main.payroll_suspended_runs
      JOIN main.payroll_run_approvals ON ((payroll_suspended_runs.payroll_run_approval_id = payroll_run_approvals.id)))
      JOIN main.payroll_procs ON ((payroll_run_approvals.payroll_proc_id = payroll_procs.id)))
      JOIN main.member_types ON ((member_types.id = payroll_suspended_runs.member_type_id)))
      JOIN main.payroll_beneficiaries_view ON (((payroll_beneficiaries_view.resource_id = payroll_suspended_runs.resource_id) AND
                                                (payroll_beneficiaries_view.member_type_id =
                                                 payroll_suspended_runs.member_type_id))))))
      where payroll_run_approvals.deleted_at is null;

");

        DB::statement("select deps_restore_dependencies('main', 'payroll_suspended_runs_view')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
