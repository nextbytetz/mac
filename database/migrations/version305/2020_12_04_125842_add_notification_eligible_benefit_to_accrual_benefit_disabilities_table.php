<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationEligibleBenefitToAccrualBenefitDisabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_benefit_disabilities', function (Blueprint $table) {
            $table->unsignedInteger('eligible_benefit_id')->nullable();
            $table->foreign('eligible_benefit_id')->references('id')->on('main.notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
