<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPayrollRunsMemberTotalsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("select deps_restore_dependencies('main', 'payroll_beneficiaries_view')");
        DB::statement("DROP  VIEW IF EXISTS  payroll_all_runs_member_totals_view");
        DB::statement("CREATE VIEW main.payroll_all_runs_member_totals_view AS


select p.member_type_id, p.resource_id, p.employee_id, max(p.run_date) as last_payroll_date, sum(p.amount) as total_pension_paid from payroll_runs_with_manual_view p
join payroll_beneficiaries_view b on p.resource_id = b.resource_id and p.member_type_id = b.member_type_id and p.employee_id = b.employee_id
group by p.member_type_id, p.resource_id, p.employee_id


");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
