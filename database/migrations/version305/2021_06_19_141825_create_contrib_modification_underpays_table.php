<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContribModificationUnderpaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('contrib_modification_underpayments', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->integer('contrib_modification_id')->unsigned();
        $table->date('contrib_month');
        $table->decimal('amount', 14);
        $table->decimal('total_grosspay', 14)->nullable();
        $table->integer('member_count');
        $table->timestamps();
        $table->foreign('contrib_modification_id')->references('id')->on('contrib_modifications')->onUpdate('CASCADE')->onDelete('RESTRICT');
    });
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrib_modification_underpayments');
    }
}
