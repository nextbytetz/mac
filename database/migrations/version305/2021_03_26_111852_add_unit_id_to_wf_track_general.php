<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitIdToWfTrackGeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
select deps_save_and_drop_dependencies('main', 'wf_track_general');
drop view if exists wf_track_general;
create view wf_track_general
as
SELECT a.receive_date,
       main.wf_aging(ARRAY [a.receive_date], ARRAY [a.forward_date], ARRAY [a.process_dates]) AS aging,
       b.level,
       a.forward_date,
       c.name                                                                                 AS unit,
       d.name                                                                                 AS designation,
       e.name                                                                                 AS module,
       i.name                                                                                 AS modulegroup,
       f.from_date                                                                            AS archive_from_date,
       f.to_date                                                                              AS archive_to_date,
       cv.name                                                                                AS archive_reason,
       h.name                                                                                 AS wf_alert,
       f.archive_reason_cv_id,
       a.process_dates,
       a.receive_date                                                                         AS registration_date,
       CASE
           WHEN (((now())::date - f.to_date) >= 0) THEN 1
           ELSE 0
           END                                                                                AS unarchiveready,
       a.id                                                                                   AS wf_track_id,
       a.user_id,
       a.wf_definition_id,
       b.unit_id                                                                                   AS unit_id,
       b.designation_id,
       b.wf_module_id,
       a.wf_archive_id,
       e.wf_module_group_id,
       a.alert_cv_id,
       a.resource_id,
       a.status,
       a.comments,
       a.assigned,
       a.parent_id,
       a.deleted_at                                                                           AS wf_track_deleted_at,
       a.user_type,
       a.resource_type
FROM (((((((((main.wf_tracks a
    JOIN main.wf_definitions b ON ((a.wf_definition_id = b.id)))
    JOIN main.units c ON ((b.unit_id = c.id)))
    JOIN main.designations d ON ((d.id = b.designation_id)))
    JOIN main.wf_modules e ON ((b.wf_module_id = e.id)))
    LEFT JOIN main.wf_archives f ON ((a.wf_archive_id = f.id)))
    LEFT JOIN main.code_values cv ON ((f.archive_reason_cv_id = cv.id)))
    LEFT JOIN main.users g ON ((a.user_id = g.id)))
    LEFT JOIN main.code_values h ON ((h.id = a.alert_cv_id)))
         JOIN main.wf_module_groups i ON ((e.wf_module_group_id = i.id)))
WHERE (a.deleted_at IS NULL);
select deps_restore_dependencies('main', 'wf_track_general');
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
