<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActionOnSlaAlerts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sla_alerts', function (Blueprint $table) {
            $table->string("action", 50)->nullable();
        });
        DB::statement("comment on column sla_alerts.action is 'show which action this sla requires, who is to be notified'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sla_alerts', function (Blueprint $table) {
            //
        });
    }
}
