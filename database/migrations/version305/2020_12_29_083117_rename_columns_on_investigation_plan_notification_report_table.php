<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsOnInvestigationPlanNotificationReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
        //     $table->dropForeign('old_notifiation_investigator');
        // });

        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            $table->renameColumn('old_notifiation_investigator', 'old_notification_investigator');
            $table->foreign('old_notification_investigator')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investigation_plan_notification_reports', function (Blueprint $table) {
            //
        });
    }
}
