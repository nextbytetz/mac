<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHspBillDiseaseCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hsp_bill_disease_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('hsp_detail_id')->nullable();
            $table->foreign('hsp_detail_id')->references('id')->on('hsp_bill_details')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->string('disease_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hsp_bill_disease_codes', function (Blueprint $table) {
            Schema::dropIfExists('hsp_bill_disease_codes');
        });
    }
}
