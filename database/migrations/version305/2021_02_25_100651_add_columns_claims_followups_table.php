<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsClaimsFollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_followups', function (Blueprint $table) {
            $table->date("current_month")->nullable();
            $table->boolean("is_repeated")->default(false);
            $table->integer("last_quotient")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_followups', function (Blueprint $table) {
            $table->dropColumn("current_month");
            $table->dropColumn("is_repeated");
            $table->dropColumn("last_quotient");
        });
    }
}
