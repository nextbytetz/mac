<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceiptCodeIdOnContribModificationMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrib_modification_months', function (Blueprint $table) {
            $table->unsignedInteger('receipt_code_id')->nullable();
            $table->foreign('receipt_code_id')->references('id')->on('receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrib_modification_months', function (Blueprint $table) {
            //
        });
    }
}
