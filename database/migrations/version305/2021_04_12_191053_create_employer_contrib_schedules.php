<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerContribSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employer_contrib_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employer_id');
            $table->tinyInteger('scheduleid');
            $table->integer('fin_year_id');
            $table->timestamps();
        });

        Schema::table('employer_contrib_schedules', function (Blueprint $table) {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fin_year_id')->references('id')->on('fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on table employer_contrib_schedules is 'Table to track receivable schedules for contribution'");
        DB::statement("comment on column employer_contrib_schedules.scheduleid is '2 -> Employers contributed atleast once on fiscal year, 6 -> Employers have not contributed on fiscal year'");


        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  bookings_mview_clone");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
