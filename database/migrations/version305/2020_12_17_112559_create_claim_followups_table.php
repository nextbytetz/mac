<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimFollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_followups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('current_date')->nullable();
            $table->integer('followup_count')->default(0);
            $table->integer('missing_documents')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_followups');
    }
}
