<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimFollowUpReviewHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_follow_up_review_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('claim_followup_id')->nullable();
            $table->foreign('claim_followup_id')->references('id')->on('main.claim_followups')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->text('remark')->nullable();
            $table->string('status_description')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_follow_up_review_histories');
    }
}
