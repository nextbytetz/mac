<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceSentFlagToBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     $thisSchema= 'portal';
     Schema::table('portal.bills', function (Blueprint $table) {
         $table->boolean('sent_to_erms')->default(0);
     });
     DB::statement("comment on column {$thisSchema}.bills.sent_to_erms is 'The status of the invoice if sent to eGA erms i.e. 1 => Sent, 0 => Not Sent'");
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            //
        });
    }
}
