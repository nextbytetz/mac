<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFacilityCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stakeholder_id');
            $table->string('facility_code');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();

            // $table->foreign('stakeholder_id')->references('id')->on('api_stakeholders')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

         DB::statement("comment on table facility_codes is 'Table with all facility codes per hcp or hsp'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_codes');
    }
}
