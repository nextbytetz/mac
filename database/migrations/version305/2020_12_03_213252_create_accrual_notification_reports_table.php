<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualNotificationReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrual_notification_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_report_id')->nullable();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('fin_year_id')->nullable();
            $table->foreign('fin_year_id')->references('id')->on('main.fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('source');
            $table->integer('status');
            $table->integer('has_workflow')->nullable();
            $table->integer('wf_done')->nullable();
            $table->datetime('wf_done_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrual_notification_reports');
    }
}
