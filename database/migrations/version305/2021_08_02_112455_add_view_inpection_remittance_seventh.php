<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewInpectionRemittanceSeventh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
drop view if exists inspection_remittance;
create or replace view inspection_remittance as SELECT zz.employer_inspection_task_id, zz.employer_id, zz.contrib_month, zz.contrib_month_formatted, zz.overpaid, zz.underpaid, zz.paid_before, zz.interest_on_unpaid, zz.interest_on_paid, zz.interest_payable, zz.interest_before, zz.paid_after, zz.interest_after, zz.paid_balance_actual, zz.interest_balance_actual, COALESCE(NULLIF(abs(zz.paid_balance_actual),(('-1'::integer)::numeric * zz.paid_balance_actual)), (0)::numeric) AS paid_balance, COALESCE(NULLIF(abs(zz.interest_balance_actual), (('-1'::integer)::numeric * zz.interest_balance_actual)), (0)::numeric) AS interest_balance FROM (SELECT ccc.employer_inspection_task_id, ccc.employer_id, ccc.contrib_month, to_char((ccc.contrib_month)::timestamp with time zone, 'Mon-YY'::text) AS contrib_month_formatted, COALESCE(ccc.overpaid, (0)::numeric) AS overpaid, COALESCE(ccc.underpaid, (0)::numeric) AS underpaid, ccc.contrib_amount AS paid_before, COALESCE(ccc.interest_on_unpaid, (0)::numeric) AS interest_on_unpaid, COALESCE(ccc.interest_on_paid, (0)::numeric) AS interest_on_paid, (COALESCE(ccc.interest_on_unpaid, (0)::numeric) + COALESCE(ccc.interest_on_paid, (0)::numeric)) AS interest_payable, d.interest_before AS interest_before, b.paid_after, d.interest_after, (COALESCE(ccc.underpaid, (0)::numeric) - COALESCE(b.paid_after, (0)::numeric)) AS paid_balance_actual, (((COALESCE(ccc.interest_on_unpaid, (0)::numeric) + COALESCE(ccc.interest_on_paid, (0)::numeric)) - COALESCE(d.interest_after, (0)::numeric)) - COALESCE(ccc.interest_amount, (0)::numeric)) AS interest_balance_actual FROM (((SELECT inspection_assessment_template.employer_inspection_task_id, inspection_assessment_template.employer_id, inspection_assessment_template.contrib_month, sum(inspection_assessment_template.contrib_amount) AS contrib_amount, sum(inspection_assessment_template.interest_amount) AS interest_amount, sum(inspection_assessment_template.overpaid) AS overpaid, sum(inspection_assessment_template.underpaid) AS underpaid, sum(inspection_assessment_template.interest_on_unpaid) AS interest_on_unpaid, sum(inspection_assessment_template.interest_on_paid) AS interest_on_paid FROM main.inspection_assessment_template GROUP BY inspection_assessment_template.employer_inspection_task_id, inspection_assessment_template.employer_id, inspection_assessment_template.contrib_month) ccc LEFT JOIN (SELECT aa.employer_id, bb.contrib_month, sum(bb.amount) AS paid_after FROM ((main.receipts aa JOIN main.receipt_codes bb ON ((aa.id = bb.receipt_id))) JOIN main.employer_inspection_task cc ON ((aa.employer_id = cc.employer_id))) WHERE ((aa.isdishonoured = 0) AND (aa.iscancelled = 0) AND (bb.fin_code_id = 2) AND (aa.rct_date > cc.last_inspection_date)) GROUP BY aa.employer_id, bb.contrib_month) b ON (((b.employer_id = ccc.employer_id) AND (date_part('month'::text, b.contrib_month) = date_part('month'::text, ccc.contrib_month)) AND (date_part('year'::text, b.contrib_month) = date_part('year'::text, ccc.contrib_month))))) LEFT JOIN (SELECT aaa.employer_id, bbb.contrib_month, sum(case when aaa.rct_date > ccc_1.last_inspection_date then bbb.amount else 0 end) AS interest_after, sum(case when aaa.rct_date <= ccc_1.last_inspection_date then bbb.amount else 0 end) AS interest_before FROM ((main.receipts aaa JOIN main.receipt_codes bbb ON ((aaa.id = bbb.receipt_id))) JOIN main.employer_inspection_task ccc_1 ON ((aaa.employer_id = ccc_1.employer_id))) WHERE ((aaa.isdishonoured = 0) AND (aaa.iscancelled = 0) AND (bbb.fin_code_id = 1)) GROUP BY aaa.employer_id, bbb.contrib_month) d ON (((d.employer_id = ccc.employer_id) AND (date_part('month'::text, d.contrib_month) = date_part('month'::text, ccc.contrib_month)) AND (date_part('year'::text, d.contrib_month) = date_part('year'::text, ccc.contrib_month)))))) zz;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
