<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableErmsCanceledInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erms_cancelled_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_reference_no')->nullable();
            $table->string('receipt_reference_no')->nullable();
            $table->integer('status_code')->nullable();
            $table->text('error_message')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('erms_cancelled_invoices', function (Blueprint $table) {
            Schema::dropIfExists('erms_cancelled_invoices');
        });
    }
}
