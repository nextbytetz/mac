<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffEmployerCommitmentFollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_employer_commitment_followups', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->string('fullname')->nullable();
            $table->integer('no_of_commitments')->default(0)->nullable();
            $table->integer('attended_commitments')->default(0)->nullable();
            $table->integer('missed_commitments')->default(0)->nullable();

            $table->float('perfomance')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_employer_commitment_followups', function (Blueprint $table) {
            Schema::dropIfExists('staff_employer_commitment_followups');
        });
    }
}
