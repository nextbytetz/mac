<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateMaterializedViewSuccessfullyTransactionsGepg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
    $sql = <<<SQL
    DROP MATERIALIZED VIEW IF EXISTS  successful_transactions_gepg;
    create materialized view successful_transactions_gepg as  SELECT r.pay_control_no AS control_no,
    r.rctno,
    b.billed_item,
    b.bill_amount,
    p.paid_amount,
    p.payer_name,
    p.payment_date,
    p.account_number,
    p.bank_name,
    p.payment_channel,
    u.name AS generatedby,
    b.mobile_number AS contact
   FROM portal.payments p
     JOIN main.receipts r ON p.control_no::text = r.pay_control_no
     JOIN portal.bills b ON p.bill_id = b.bill_no
     JOIN portal.users u ON u.id = b.user_id
  WHERE p.paid_amount IS NOT NULL and b.bill_status=3;

SQL;

   DB::unprepared($sql);
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
