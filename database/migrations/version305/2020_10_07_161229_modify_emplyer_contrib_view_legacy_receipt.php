<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEmplyerContribViewLegacyReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*drop staff performance dashboard*/
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  staff_employer_contrib_performance_intern");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  staff_employer_contrib_performance");


        DB::statement("select deps_save_and_drop_dependencies('main', 'employer_contributions')");
        DB::statement("DROP VIEW IF EXISTS  employer_contributions");
        DB::statement("CREATE VIEW main.employer_contributions AS

  SELECT r.employer_id,
         e.doc,
         e.name                              AS employer_name,
         e.reg_no                            AS employer_reg_no,
         code.amount                         AS contrib_amount,
         r.amount                            AS receipt_amount,
         to_date(concat_ws('-' :: text, date_part('year' :: text, code.contrib_month),
                           date_part('month' :: text, code.contrib_month), '28'),
                 'YYYY-MM-DD' :: text)       AS contrib_month,
         r.rct_date,
         r.created_at                        AS receipt_created_at,
         (r.rctno) :: character varying(100) AS rctno,
         0                                   AS islegacy,
         r.pay_control_no                    AS control_no,
         code.booking_id,
         code.member_count
  FROM ((main.receipt_codes code
      JOIN main.receipts r ON ((code.receipt_id = r.id)))
      JOIN main.employers e ON ((r.employer_id = e.id)))
  WHERE ((r.iscancelled = 0) AND (r.isdishonoured = 0) AND (r.deleted_at IS NULL) AND (code.deleted_at IS NULL) AND
         (code.grouppaystatus = 0) AND ((code.fin_code_id = 2) OR (code.fin_code_id = 63)))
  UNION
  SELECT l.employer_id,
         e.doc,
         e.name                                                                                      AS employer_name,
         e.reg_no                                                                                    AS employer_reg_no,
         l.amount                                                                                    AS contrib_amount,
         l.amount                                                                                    AS receipt_amount,
         to_date(concat_ws('-' :: text, date_part('year' :: text, codes.contrib_month),
                           date_part('month' :: text, codes.contrib_month), '28'), 'YYYY-MM-DD' :: text) AS contrib_month,
         l.rct_date,
         l.created_at                                                                                AS receipt_created_at,
         l.rctno,
         1                                                                                           AS islegacy,
         NULL :: text                                                                                AS control_no,
         NULL :: bigint                                                                              AS booking_id,
         l.member_count
  FROM (main.legacy_receipt_codes codes
      join legacy_receipts l on codes.legacy_receipt_id = l.id
      JOIN main.employers e ON ((l.employer_id = e.id)))
  WHERE ((l.iscancelled = 0) AND (l.isdishonoured = 0) AND (l.deleted_at IS NULL) AND (l.exist_in_receipts = 0));


");

        DB::statement("select deps_restore_dependencies('main', 'employer_contributions')");




        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  staff_employer_contrib_performance");
        DB::statement("CREATE MATERIALIZED VIEW main.staff_employer_contrib_performance AS

  SELECT staff.user_id,
         concat_ws(' ' :: text, u1.firstname, u1.lastname)                                      AS fullname,
         (SELECT count(1) AS count
          FROM (main.staff_employer scount
              JOIN main.staff_employer_allocations a ON ((scount.staff_employer_allocation_id = a.id)))
          WHERE ((a.isactive = 1) AND scount.isactive = 1 AND (scount.isduplicate = 0) AND (scount.replacement_type IS NULL) AND
                 (scount.isbonus = 0) AND
                 (scount.user_id = staff.user_id)))                                             AS no_of_employers,
         (SELECT count(1) AS count
          FROM ((main.staff_employer scount
              JOIN main.employers e ON ((scount.employer_id = e.id)))
              JOIN main.staff_employer_allocations a ON ((scount.staff_employer_allocation_id = a.id)))
          WHERE ((e.isonline = 1) AND scount.isactive = 1 AND (a.isactive = 1) AND (scount.isduplicate = 0) AND
                 (scount.replacement_type IS NULL) AND (scount.isbonus = 0) AND
                 (scount.user_id = staff.user_id)))                                             AS online_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM (((main.bookings_view b
              JOIN main.staff_employer starget ON ((
            (starget.employer_id = b.employer_id) AND (starget.user_id = staff.user_id) AND (starget.isduplicate = 0)
            AND (starget.replacement_type IS NULL) AND (starget.isbonus = 0) AND (starget.unit_id = 5))))
              JOIN main.staff_employer_allocations at ON ((starget.staff_employer_allocation_id = at.id)))
              JOIN main.booking_grace_period_view grace ON ((grace.booking_id = b.booking_id)))
          WHERE ((b.rcv_date < starget.start_date) AND (at.isactive = 1) AND starget.isactive = 1 AND
                 (COALESCE((b.receipt_date) :: date, starget.end_date) >= starget.start_date))) AS arrears_before,
         (SELECT sum(contrib.contrib_amount) AS sum
          FROM ((main.employer_contributions contrib
              JOIN main.staff_employer sactual ON ((
            (sactual.employer_id = contrib.employer_id) AND (sactual.user_id = staff.user_id) AND
            (sactual.isduplicate = 0) AND (sactual.replacement_type IS NULL) AND (sactual.isbonus = 0) AND
            (sactual.unit_id = 5))))
              JOIN main.staff_employer_allocations ac ON ((sactual.staff_employer_allocation_id = ac.id)))
          WHERE ((contrib.islegacy = 0) AND ((contrib.receipt_created_at) :: date >= sactual.start_date) AND
                 (ac.isactive = 1) AND sactual.isactive = 1 AND (((contrib.receipt_created_at) :: date <= (now()) :: date) AND
                                        ((contrib.receipt_created_at) :: date <= sactual.end_date)) AND
                 (contrib.contrib_month < sactual.start_date)))                                 AS arrears_contribution,
         (SELECT sum(b.booked_amount) AS sum
          FROM (((main.bookings_view b
              JOIN main.staff_employer starget ON ((
            (starget.employer_id = b.employer_id) AND (starget.user_id = staff.user_id) AND (starget.isduplicate = 0)
            AND (starget.replacement_type IS NULL) AND (starget.isbonus = 0) AND (starget.unit_id = 5))))
              JOIN main.staff_employer_allocations at ON ((starget.staff_employer_allocation_id = at.id)))
              JOIN main.booking_grace_period_view grace ON ((grace.booking_id = b.booking_id)))
          WHERE ((b.rcv_date >= starget.start_date) AND (at.isactive = 1) AND starget.isactive = 1 AND
                 ((grace.grace_period_end + '-1 mons +8 days' :: interval) <= (now()) :: date) AND
                 (grace.grace_period_end <= starget.end_date) AND
                 (COALESCE((b.receipt_date) :: date, starget.end_date) >= starget.start_date))) AS receivable_current,
         (SELECT sum(contrib.contrib_amount) AS sum
          FROM (((main.employer_contributions contrib
              JOIN main.staff_employer sactual ON ((
            (sactual.employer_id = contrib.employer_id) AND (sactual.user_id = staff.user_id) AND
            (sactual.isduplicate = 0) AND (sactual.replacement_type IS NULL) AND (sactual.isbonus = 0) AND
            (sactual.unit_id = 5))))
              JOIN main.staff_employer_allocations ac ON ((sactual.staff_employer_allocation_id = ac.id)))
              JOIN main.booking_grace_period_view grace ON ((grace.booking_id = contrib.booking_id)))
          WHERE ((contrib.islegacy = 0) AND ((contrib.receipt_created_at) :: date >= sactual.start_date) AND
                 (ac.isactive = 1) AND sactual.isactive = 1 AND (((contrib.receipt_created_at) :: date <= (now()) :: date) AND
                                        ((contrib.receipt_created_at) :: date <= sactual.end_date)) AND
                 (contrib.contrib_month >= sactual.start_date) AND
                 ((grace.grace_period_end + '-1 mons +8 days' :: interval) <= (now()) :: date) AND
                 (contrib.contrib_month <= sactual.end_date)))                                  AS contribution_current
  FROM ((SELECT s.user_id
         FROM (main.users u
             JOIN main.staff_employer s ON ((
           (u.id = s.user_id) AND (s.isactive = 1) AND (s.isbonus = 0) AND (s.isintern = 0) AND (s.unit_id = 5))))
         GROUP BY s.user_id) staff
      JOIN main.users u1 ON ((u1.id = staff.user_id)));
");


        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  staff_employer_contrib_performance_intern");
        DB::statement("CREATE MATERIALIZED VIEW main.staff_employer_contrib_performance_intern AS

  SELECT staff.user_id,
         concat_ws(' ' :: text, u1.firstname, u1.lastname)                                      AS fullname,
         (SELECT count(1) AS count
          FROM (main.staff_employer scount
              JOIN main.staff_employer_allocations a ON ((scount.staff_employer_allocation_id = a.id)))
          WHERE ((a.isactive = 1) AND scount.isactive = 1 AND (scount.isduplicate = 0) AND (scount.replacement_type IS NULL) AND
                 (scount.isbonus = 0) AND
                 (scount.user_id = staff.user_id)))                                             AS no_of_employers,
         (SELECT count(1) AS count
          FROM ((main.staff_employer scount
              JOIN main.employers e ON ((scount.employer_id = e.id)))
              JOIN main.staff_employer_allocations a ON ((scount.staff_employer_allocation_id = a.id)))
          WHERE ((e.isonline = 1) AND scount.isactive = 1  AND (a.isactive = 1) AND (scount.isduplicate = 0) AND
                 (scount.replacement_type IS NULL) AND (scount.isbonus = 0) AND
                 (scount.user_id = staff.user_id)))                                             AS online_employers,
         (SELECT sum(b.booked_amount) AS sum
          FROM (((main.bookings_view b
              JOIN main.staff_employer starget ON ((
            (starget.employer_id = b.employer_id) AND (starget.user_id = staff.user_id) AND (starget.isduplicate = 0)
            AND (starget.replacement_type IS NULL) AND (starget.isbonus = 0) AND (starget.unit_id = 5))))
              JOIN main.staff_employer_allocations at ON ((starget.staff_employer_allocation_id = at.id)))
              JOIN main.booking_grace_period_view grace ON ((grace.booking_id = b.booking_id)))
          WHERE ((b.rcv_date < starget.start_date) AND (at.isactive = 1) AND starget.isactive = 1  AND
                 (COALESCE((b.receipt_date) :: date, starget.end_date) >= starget.start_date))) AS arrears_before,
         (SELECT sum(contrib.contrib_amount) AS sum
          FROM ((main.employer_contributions contrib
              JOIN main.staff_employer sactual ON ((
            (sactual.employer_id = contrib.employer_id) AND (sactual.user_id = staff.user_id) AND
            (sactual.isduplicate = 0) AND (sactual.replacement_type IS NULL) AND (sactual.isbonus = 0)  AND
            (sactual.unit_id = 5))))
              JOIN main.staff_employer_allocations ac ON ((sactual.staff_employer_allocation_id = ac.id)))
          WHERE ((contrib.islegacy = 0) AND ((contrib.receipt_created_at) :: date >= sactual.start_date) AND
                 (ac.isactive = 1) AND sactual.isactive = 1 AND (((contrib.receipt_created_at) :: date <= (now()) :: date) AND
                                        ((contrib.receipt_created_at) :: date <= sactual.end_date)) AND
                 (contrib.contrib_month < sactual.start_date)))                                 AS arrears_contribution,
         (SELECT sum(b.booked_amount) AS sum
          FROM (((main.bookings_view b
              JOIN main.staff_employer starget ON ((
            (starget.employer_id = b.employer_id) AND (starget.user_id = staff.user_id) AND (starget.isduplicate = 0)
            AND (starget.replacement_type IS NULL) AND (starget.isbonus = 0) AND (starget.unit_id = 5))))
              JOIN main.staff_employer_allocations at ON ((starget.staff_employer_allocation_id = at.id)))
              JOIN main.booking_grace_period_view grace ON ((grace.booking_id = b.booking_id)))
          WHERE ((b.rcv_date >= starget.start_date) AND (at.isactive = 1) AND starget.isactive = 1 AND
                 ((grace.grace_period_end + '-1 mons +8 days' :: interval) <= (now()) :: date) AND
                 (grace.grace_period_end <= starget.end_date) AND
                 (COALESCE((b.receipt_date) :: date, starget.end_date) >= starget.start_date))) AS receivable_current,
         (SELECT sum(contrib.contrib_amount) AS sum
          FROM (((main.employer_contributions contrib
              JOIN main.staff_employer sactual ON ((
            (sactual.employer_id = contrib.employer_id) AND (sactual.user_id = staff.user_id) AND
            (sactual.isduplicate = 0) AND (sactual.replacement_type IS NULL) AND (sactual.isbonus = 0) AND
            (sactual.unit_id = 5))))
              JOIN main.staff_employer_allocations ac ON ((sactual.staff_employer_allocation_id = ac.id)))
              JOIN main.booking_grace_period_view grace ON ((grace.booking_id = contrib.booking_id)))
          WHERE ((contrib.islegacy = 0) AND ((contrib.receipt_created_at) :: date >= sactual.start_date) AND
                 (ac.isactive = 1) AND sactual.isactive = 1 AND (((contrib.receipt_created_at) :: date <= (now()) :: date) AND
                                        ((contrib.receipt_created_at) :: date <= sactual.end_date)) AND
                 (contrib.contrib_month >= sactual.start_date) AND
                 ((grace.grace_period_end + '-1 mons +8 days' :: interval) <= (now()) :: date) AND
                 (contrib.contrib_month <= sactual.end_date)))                                  AS contribution_current
  FROM ((SELECT s.user_id
         FROM (main.users u
             JOIN main.staff_employer s ON ((
           (u.id = s.user_id) AND (s.isactive = 1) AND (s.isbonus = 0) AND (s.isintern = 1) AND (s.unit_id = 5))))
         GROUP BY s.user_id) staff
      JOIN main.users u1 ON ((u1.id = staff.user_id)));
  
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
