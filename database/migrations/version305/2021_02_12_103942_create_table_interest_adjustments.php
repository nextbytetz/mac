<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInterestAdjustments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('interest_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('receipt_id');
            $table->date('payment_date');
            $table->date('new_payment_date');
            $table->integer('reason_type_cv_id');
            $table->text('remark');
            $table->text('booking_interest_ids');
            $table->tinyInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::table('interest_adjustments', function (Blueprint $table) {
            $table->foreign('reason_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column interest_adjustments.reason_type_cv_id is 'Reason for adjusting payment date'");
        DB::statement("comment on column interest_adjustments.booking_interest_ids is 'Booking interest which will be adjusted'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
