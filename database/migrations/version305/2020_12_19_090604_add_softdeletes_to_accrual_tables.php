<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftdeletesToAccrualTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accrual_notification_reports', function (Blueprint $table) {
            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->softDeletes();
        });
        Schema::table('accrual_benefit_disabilities', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('accrual_notification_benefits', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('accrual_claims', function (Blueprint $table) {
            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('accrual_notification_reports', function (Blueprint $table) {
        $table->dropSoftDeletes();
        $table->dropForeign('user_id');
    });
       Schema::table('accrual_claims', function (Blueprint $table) {
         $table->dropForeign('user_id');
     });
       Schema::table('accrual_benefit_disabilities', function (Blueprint $table) {
        $table->dropSoftDeletes();
    });
       Schema::table('accrual_notification_benefits', function (Blueprint $table) {
        $table->dropSoftDeletes();
    });
   }
}
