<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersDeactivationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     // $portalSchema = portal_schema();
     Schema::create('portal.user_deactivations', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('user_id');
        $table->unsignedInteger('mac_user_id');
        $table->integer('status')->default(0);
        $table->text('reason')->nullable();
        $table->text('remarks')->nullable();
        $table->unsignedInteger('parent_id')->nullable();
        $table->timestamps();
    });
     // DB::statement("comment on column {$portalSchema}.user_deactivations.status is ' status : 0 = deactivated, 1 = activated' ");
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal.user_deactivations');
    }
}
