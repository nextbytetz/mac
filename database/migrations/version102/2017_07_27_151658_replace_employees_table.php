<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create or replace VIEW employee_ver_ones AS select member_id as id, firstname, middlename, lastname, dob, sex, job_title, emp_cate, round(salary/12, 2) as basicpay, allowance, employer_id, member_no as memberno from wcf.members");
        DB::statement("create or replace VIEW employees AS select * from (employee_ver_ones a left join employee_ver_twos b on((a.id = b.employee_id)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
