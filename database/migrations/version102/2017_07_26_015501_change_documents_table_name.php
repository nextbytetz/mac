<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDocumentsTableName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table)
        {
            $table->string("name", 150)->nullable()->change();
            $table->string("ext", 20)->nullable()->change();
            $table->string("mime", 150)->nullable()->change();
            $table->float("size")->nullable()->change();
        });
        Schema::rename("documents", "document_type_notification_report");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename("document_type_notification_report", "documents");
        Schema::table('documents', function (Blueprint $table)
        {
            $table->string("name", 150)->change();
            $table->string("ext", 20)->change();
            $table->string("mime", 150)->change();
            $table->float("size")->change();
        });
    }
}
