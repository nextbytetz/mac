<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPaymentVoucherTran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->decimal('amount', 14)->after('fin_account_code_id');
            $table->boolean('is_processed')->default(0)->after('is_exported')->comment('Flag to show whether this payment voucher transaction has already been processed to Payment Voucher');
            $table->integer('payment_voucher_id')->unsigned()->nullable()->after('is_processed');

            $table->dropForeign('payroll_proc_pv_tran_foreign_key');
            $table->dropIndex('payroll_proc_id');
            $table->dropColumn('payroll_proc_id');
        });



        Schema::table('payment_vouchers', function(Blueprint $table)
        {

            $table->dropColumn('benefit_resource_id');
            $table->dropColumn('resource_id');
            $table->dropForeign('member_type_id_pv_foreign_key');
            $table->dropColumn('member_type_id');



            $table->integer('payroll_proc_id')->unsigned()->after('is_paid')->index('payroll_proc_id');

        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_id', 'payroll_proc_pv_foreign_key')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {

            $table->dropColumn('is_processed');
            $table->dropColumn('amount');
        });


        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('benefit_resource_id')->unsigned()->comment('Primary key of the benefit resource table, a table which is being tracked for.');
            $table->integer('resource_id')->unsigned()->comment('Primary key of the resource table, a table which is being tracked for.');
            $table->integer('member_type_id')->unsigned()->index('member_type_id')->comment('which member type this compensation is concerned');

            $table->dropForeign('payroll_proc_pv_foreign_key');
            $table->dropIndex('payroll_proc_id');
            $table->dropColumn('payroll_proc_id');
        });


        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {

            $table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id');

        });


        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_id', 'payroll_proc_pv_tran_foreign_key')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');


        });
    }
}
