<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCodeValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('code_values', function(Blueprint $table)
        {

            $table->string('reference', 20)->nullable()->after('description')->comment('Code value reference id which will be unique for every code value');

        });

        Schema::table('code_values', function(Blueprint $table)
        {
            $table->unique(['code_id', 'reference']);
        });


    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('code_values', function(Blueprint $table)
        {
            $table->dropUnique(['code_id', 'reference']);
            $table->dropColumn('reference');
        });

    }
}
