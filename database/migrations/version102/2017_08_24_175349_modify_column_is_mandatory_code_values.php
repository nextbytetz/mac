<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Operation\Claim\NotificationReport;

class ModifyColumnIsMandatoryCodeValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('code_values', function(Blueprint $table)
        {

            $table->boolean("is_mandatory")->default(0)->comment("1 - is mandatory, 0 - is not mandatory (System defined flag)")->change();

        });


        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->integer("incident_exposure_cv_id")->unsigned()->nullable()->change();
        });

        NotificationReport::where("incident_exposure_cv_id", 0)->update(['incident_exposure_cv_id' => NULL]);
        NotificationReport::where("body_part_injury_cv_id", 0)->update(['body_part_injury_cv_id' => NULL]);
        NotificationReport::where("nature_of_incident_cv_id", 0)->update(['nature_of_incident_cv_id' => NULL]);

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->foreign('body_part_injury_cv_id', 'body_part_injury_code_value_foreign')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->foreign('incident_exposure_cv_id', 'incident_exposure_code_value_foreign')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->foreign('nature_of_incident_cv_id', 'nature_of_incident_code_value_foreign')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('code_values', function(Blueprint $table)
        {

            $table->tinyInteger("is_mandatory")->default(1)->comment("1 - is mandatory, 0 - is not mandatory (System defined flag)");

        });

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropForeign('body_part_injury_code_value_foreign');
            $table->dropForeign('incident_exposure_code_value_foreign');
            $table->dropForeign('nature_of_incident_code_value_foreign');

        });

    }
}
