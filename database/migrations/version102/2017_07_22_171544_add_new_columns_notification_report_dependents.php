<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsNotificationReportDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->integer('body_part_injury_cv_id')->unsigned()->index('body_part_injury_cv_id')->after('user_id');
            $table->integer('incident_exposure_cv_id')->unsigned()->index('incident_exposure_id')->after('body_part_injury_cv_id');
            $table->integer('nature_of_incident_cv_id')->unsigned()->index('nature_of_incident_cv_id')->after('incident_exposure_cv_id');
        });

        //dependents
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->string('id_no',45)->after('identity_id');
            $table->decimal('dependency_percentage',5)->unsigned()->index('survivor_gratuity_amount')->after('survivor_gratuity_amount')->comment('Percentage of dependency of Other dependents i.e dependents other than children and spouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropIndex('body_part_injury_cv_id');
            $table->dropIndex('incident_exposure_cv_id');
            $table->dropIndex('nature_of_incident_cv_id');

            $table->dropColumn('body_part_injury_cv_id');
            $table->dropColumn('incident_exposure_cv_id');
            $table->dropColumn('nature_of_incident_cv_id');
        });


        //dependents
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropColumn('id_no');
            $table->dropColumn('dependency_percentage');
        });

    }
}
