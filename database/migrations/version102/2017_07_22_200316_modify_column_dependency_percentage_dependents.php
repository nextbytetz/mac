<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnDependencyPercentageDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //dependents
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropIndex('survivor_gratuity_amount');
            $table->decimal('dependency_percentage',5)->unsigned()->nullable()->comment('Percentage of dependency of Other dependents i.e dependents other than children and spouses')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        //dependents
        Schema::table('dependents', function(Blueprint $table)
        {

            $table->decimal('dependency_percentage',5)->unsigned()->nullable()->comment('Percentage of dependency of Other dependents i.e dependents other than children and spouses')->change();
        });
    }
}
