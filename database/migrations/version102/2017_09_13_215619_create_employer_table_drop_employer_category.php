<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerTableDropEmployerCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


//        Registered employers

/*        Schema::table('registered_employers', function(Blueprint $table)
        {
            $table->dropForeign('employer_categories_foreign_ibfk_1');
            $table->dropIndex('employer_category_id');
            $table->dropColumn('employer_category_id');

            $table->integer('employer_category_cv_id')->unsigned()->nullable()->index('employer_category_cv_id');
        });

        Schema::table('registered_employers', function(Blueprint $table)
        {
            $table->foreign('employer_category_cv_id', 'employer_categories_foreign_ibfk_1')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });*/


        //
        Schema::drop('employer_categories');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::create('employer_categories', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->string('name', 150)->unique('name', 'name');
            $table->timestamps();
        });


        Schema::table('registered_employers', function(Blueprint $table)
        {
            $table->dropForeign('employer_categories_foreign_ibfk_1');
            $table->dropIndex('employer_category_cv_id');
            $table->dropColumn('employer_category_cv_id');
        });



    }
}
