<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayrollProcIdIntoPaymentVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id')->after('fin_account_code_id');
        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_id', 'payroll_proc_pv_foreign_key')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('bank_id')->unsigned()->after('fin_account_code_id')->index('bank_id');
            $table->integer('bank_branch_id')->unsigned()->after('bank_id')->index('bank_branch_id');
            $table->string('accountno', 45)->after('bank_branch_id');
        });


        Schema::table('payment_vouchers', function(Blueprint $table)
        {

            $table->foreign('bank_id', 'payment_vouchers_bank_id_foreign')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id', 'payment_vouchers_bank_branch_id_foreign')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropForeign('payroll_proc_pv_foreign_key');
            $table->dropIndex('payroll_proc_id');
            $table->dropColumn('payroll_proc_id');
        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropForeign('payment_vouchers_bank_id_foreign');
            $table->dropForeign('payment_vouchers_bank_branch_id_foreign');
            $table->dropIndex('bank_id');
            $table->dropIndex('bank_branch_id');
            $table->dropColumn('bank_branch_id');
            $table->dropColumn('bank_id');
            $table->dropColumn('accountno');
        });
    }
}
