<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployeeDateRange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->integer('employee_id')->unsigned()->after('employer_id')->index('employee_id');
            $table->integer('insurance_id')->unsigned()->after('employee_id')->index('insurance_id');
            $table->integer('dependent_id')->unsigned()->after('insurance_id')->index('dependent_id');
            $table->integer('pensioner_id')->unsigned()->after('dependent_id')->index('pensioner_id');

            $table->integer('member_type_id')->unsigned()->after('pensioner_id')->index('member_type_id');
            $table->integer('resource_id')->unsigned()->after('member_type_id')->index('resource_id');
            $table->integer('status_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->dropIndex('employee_id');
            $table->dropColumn('employee_id');

            $table->dropIndex('insurance_id');
            $table->dropColumn('insurance_id');

            $table->dropIndex('dependent_id');
            $table->dropColumn('dependent_id');

            $table->dropIndex('pensioner_id');
            $table->dropColumn('pensioner_id');


            $table->dropIndex('member_type_id');
            $table->dropColumn('member_type_id');


            $table->dropIndex('resource_id');
            $table->dropColumn('resource_id');


            $table->integer('status_id')->unsigned()->change();
    });
    }
}
