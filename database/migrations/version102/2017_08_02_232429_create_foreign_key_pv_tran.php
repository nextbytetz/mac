<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyPvTran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {

            $table->foreign('payment_voucher_id', 'payment_vouchers_pv_tran_foreign')->references('id')->on('payment_vouchers')->onUpdate('CASCADE')->onDelete('RESTRICT');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {

            $table->dropForeign('payment_vouchers_pv_tran_foreign');


        });
    }
}
