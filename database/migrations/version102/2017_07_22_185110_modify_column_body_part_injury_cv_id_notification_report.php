<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnBodyPartInjuryCvIdNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->integer('body_part_injury_cv_id')->unsigned()->nullable()->change();
            $table->integer('nature_of_incident_cv_id')->unsigned()->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->integer('body_part_injury_cv_id')->unsigned()->change();
            $table->integer('nature_of_incident_cv_id')->unsigned()->change();
        });

    }
}
