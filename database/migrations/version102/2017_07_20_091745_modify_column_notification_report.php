<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->boolean('need_investigation')->default(0)->comment('set whether notification needs investigation or not, 1 - needs investigation, 0 - does not need investigation.')->change();
            $table->boolean('need_verification')->default(1)->comment('set whether the notification needs to be verified or not, 1 - needs verification, 0 - do not need verification.')->change();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->boolean('need_investigation')->default(1)->comment('set whether notification needs investigation or not, 1 - needs investigation, 0 - does not need investigation.')->change();
            $table->boolean('need_verification')->default(0)->comment('set whether the notification needs to be verified or not, 1 - needs verification, 0 - do not need verification.')->change();
        });
    }
}
