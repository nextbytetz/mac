<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnContribMonthIntoClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claims', function(Blueprint $table)
    {
        $table->date('contrib_month')->after("monthly_earning")->comment('Contribution month of gross monthly earning before incident date');
        $table->decimal('pi',10,7)->comment('Percentage of permanent disability for this claim notification')->change();

    });

        Schema::table('claims', function(Blueprint $table)
        {
            $table->renameColumn('pi','pd');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('claims', function(Blueprint $table)
        {
            $table->dropColumn('contrib_month');
            $table->renameColumn('pd','pi');

        });
    }
}
