<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReferMembersFromOldDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->dropForeign("employee_id_foreign", "employee_id_foreign");
        });
        Schema::table('employees', function(Blueprint $table)
        {
            $table->dropColumn('firstname');
            $table->dropColumn('middlename');
            $table->dropColumn('lastname');
            $table->dropColumn('dob');
            $table->dropColumn('memberno');
            $table->dropColumn('employer_id');
            $table->dropColumn('basicpay');
            $table->renameColumn("id", "employee_id");
        });
        Schema::rename("employees", "employee_ver_twos");
        DB::statement("create VIEW employee_ver_ones AS select member_id as id, firstname, middlename, lastname, dob, sex, job_title, emp_cate, round(salary/12, 2) as basicpay, allowance, employer_id, member_no as member_no from wcf.members");
        DB::statement("create VIEW employees AS select * from (employee_ver_ones a left join employee_ver_twos b on((a.id = b.employee_id)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
