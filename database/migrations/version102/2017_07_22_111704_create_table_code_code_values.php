<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCodeCodeValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('codes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 50)->unique();
            $table->tinyInteger('is_system_defined')->nullable()->default(1)->comment("Set whether this code is system defined or not, 1 - system defined, 0 - not system defined");
            $table->timestamps();
        });
        Schema::create('code_values', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("code_id")->unsigned()->index("code_id");
            $table->string('name', 50);
            $table->text("description")->nullable();
            $table->tinyInteger("sort")->nullable()->comment("The order which the code value appear on the user view");
            $table->tinyInteger("is_active")->default(1)->comment("set whether this code value is active or not, 1 code value is active, 0 code value is not active");
            $table->tinyInteger("is_mandatory")->default(1)->comment("1 - is mandatory, 0 - is not mandatory");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('code_values', function(Blueprint $table)
        {
            $table->foreign('code_id', 'code_id_foreign')->references('id')->on('codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('code_values', function(Blueprint $table)
        {
            $table->dropColumn('code_id_foreign');
        });

        Schema::drop('code_values');
        Schema::drop('codes');

       }
}
