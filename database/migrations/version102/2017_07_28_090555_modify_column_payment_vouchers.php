<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnPaymentVouchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
             $table->integer('fin_account_code_id')->unsigned()->nullable()->comment('Finance account codes where this voucher is allocated to for payment')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('fin_account_code_id')->unsigned()->comment('Finance account codes where this voucher is allocated to for payment')->change();

        });
    }
}
