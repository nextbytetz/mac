<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnNotificationDisabilityState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //       Schema::table('notification_disability_states', function (Blueprint $table) {
        Schema::table('notification_disability_states', function (Blueprint $table) {
            $table->dropColumn('percent_of_ed_ld');

        });

        Schema::table('notification_disability_states', function (Blueprint $table) {

            $table->decimal('percent_of_ed_ld',6)->nullable()->after('days')->comment('Percentage of Excuse duty (ED) / Light duties (LD)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_disability_states', function (Blueprint $table) {
            $table->dropColumn('percent_of_ed_ld');
        });
    }
}
