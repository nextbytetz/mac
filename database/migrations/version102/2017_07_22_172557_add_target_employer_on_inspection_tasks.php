<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTargetEmployerOnInspectionTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_tasks', function (Blueprint $table)
        {
            $table->smallInteger("target_employer")->nullable()->after("last_id")->comment("Check the target employers concerned in this inspection task, 1 - for Inspection Profile, 0 - Individual Employers, 2 - None, employers who are not registered to WCF by the time of this task where selected");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_tasks', function (Blueprint $table)
        {
            $table->dropColumn("target_employer");
        });
    }
}
