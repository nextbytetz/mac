<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDependentsSurvivorGratuityFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
        {

            $table->boolean('survivor_pension_flag')->default(0)->comment('	Determine whether is receiving pension or not, e.g children taken care by an administrator might not be receiving pension. 1 - yes receiving, 0 - not receiving ')->change();
            $table->boolean('survivor_gratuity_flag')->default(0)->comment('Determine whether is receiving gratuity / lump sum or not, 1 - yes receiving, 0 - not receiving ')->after('survivor_gratuity_percent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
        {

            $table->boolean('survivor_pension_flag')->default(1)->comment('	determine whether is receiving pension or not, e.g children taken care by an administrator might not be receiving pension. 1 - yes receiving, 0 - not receiving ')->change();
            $table->dropColumn('survivor_gratuity_flag');
        });
    }
}
