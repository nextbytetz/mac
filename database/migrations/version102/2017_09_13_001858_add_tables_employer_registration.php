<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablesEmployerRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/*        Schema::create('registered_employers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 150);
            $table->date('date_of_commencement')->comment('Date of company start operation');
            $table->integer('employer_category_id')->unsigned()->nullable()->index('employer_category_id');
            $table->string('tin', 45)->nullable();
            $table->string('vote', 15)->nullable();
            $table->decimal('annual_earning', 14)->default(0)->comment('Total Annual earning of all employees');
            $table->string('phone', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->string('fax', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->integer('country_id')->unsigned()->index('country_id');
            $table->integer('region_id')->unsigned()->index('region_id');
            $table->integer('district_id')->unsigned()->index('district_id');
            $table->integer('location_type_id')->unsigned()->default(2)->index('location_type_id');
            $table->text('unsurveyed_area', 65535)->nullable();
            $table->text('surveyed_extra', 65535)->nullable();
            $table->string('street', 150)->nullable();
            $table->string('road', 150)->nullable();
            $table->string('plot_no', 150)->nullable();
            $table->string('block_no', 150)->nullable();
            $table->integer('user_id')->unsigned()->index('user_id')->comment('Staff registered this employer');
            $table->tinyInteger('status')->comment('Registration status, 1 - Approved, 0 - Pending, 2- Rejected');
            $table->timestamps();
            $table->softDeletes();
        });*/
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('registered_employers');
    }
}
