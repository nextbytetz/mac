<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfDoneIntoNotificationReportRemoveFromClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claims', function(Blueprint $table)
        {
            $table->dropColumn('wf_done');
            $table->dropColumn('wf_done_date');

        });

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->boolean('wf_done')->default(0)->after('nature_of_incident_cv_id')->comment('specify whether the notification report workflow has been completed or not.');
            $table->date('wf_done_date')->nullable()->after("wf_done")->comment('date which workflow was completed');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropColumn('wf_done');
            $table->dropColumn('wf_done_date');

        });

        Schema::table('claims', function(Blueprint $table)
        {
            $table->boolean('wf_done')->default(0)->after('nature_of_incident_cv_id')->comment('specify whether the claim workflow has been completed or not.');
            $table->date('wf_done_date')->nullable()->after("wf_done")->comment('date which workflow was completed');

        });
    }
}
