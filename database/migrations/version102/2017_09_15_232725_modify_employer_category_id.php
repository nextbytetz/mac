<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEmployerCategoryId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
/*        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->dropForeign('employer_categories_foreign_ibfk_1');
            $table->dropIndex('employer_category_cv_id');
            $table->dropColumn('employer_category_cv_id');

        });

        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->integer('employer_category_cv_id')->unsigned()->nullable()->index('employer_category_cv_id')->after('annual_earning');

        });

        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->foreign('employer_category_cv_id', 'employer_categories_foreign_ibfk_1')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });*/


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->dropForeign('employer_categories_foreign_ibfk_1');
            $table->dropIndex('employer_category_cv_id');
            $table->dropColumn('employer_category_cv_id');

        });

        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->integer('employer_category_cv_id')->unsigned()->nullable()->index('employer_category_cv_id')->after('annual_earning');

        });

        Schema::table('employer_registrations', function(Blueprint $table)
        {
            $table->foreign('employer_category_cv_id', 'employer_categories_foreign_ibfk_1')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }
}
