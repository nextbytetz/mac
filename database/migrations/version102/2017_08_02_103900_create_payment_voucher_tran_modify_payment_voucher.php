<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentVoucherTranModifyPaymentVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropForeign('benefit_type_foreign_key');
            $table->dropIndex('benefit_type_id');
            $table->dropColumn('benefit_type_id');

            $table->dropForeign('fin_account_code_foreign_key');
            $table->dropIndex('fin_account_code_id');
            $table->dropColumn('fin_account_code_id');

            $table->dropForeign('payroll_proc_pv_foreign_key');
            $table->dropIndex('payroll_proc_id');
            $table->dropColumn('payroll_proc_id');

            $table->dropColumn('is_exported');

            $table->boolean('is_paid')->default(0)->after('accountno')->comment('Flag to show whether this payment voucher has already been dispatched / paid to payee');

        });
//
//
        Schema::create('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('benefit_resource_id')->unsigned()->comment('Primary key of the benefit resource table, a table which is being tracked for.');
            $table->integer('member_type_id')->unsigned()->index('member_type_id')->comment('which member type this compensation is concerned');
            $table->integer('resource_id')->unsigned()->comment('Primary key of the resource table, a table which is being tracked for.');
            $table->integer('fin_account_code_id')->unsigned()->index('fin_account_code_id')->comment('Finance account codes where this voucher is allocated to for payment');
            $table->boolean('is_exported')->default(0)->comment('Flag to show whether this payment voucher has already been exported to finance system');
            $table->integer('payroll_proc_id')->unsigned()->index('payroll_proc_id');
            $table->integer('user_id')->unsigned()->index('user_id')->comment('Staff user who has created this payment voucher');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_id', 'payroll_proc_pv_tran_foreign_key')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');

            $table->foreign('fin_account_code_id', 'fin_account_code_pv_tran_foreign_key')->references('id')->on('fin_account_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::drop('payment_voucher_types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->dropColumn('is_paid');

        });

        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->dropForeign('payroll_proc_pv_tran_foreign_key');
            $table->dropForeign('fin_account_code_pv_tran_foreign_key');

        });
        Schema::drop('payment_voucher_transactions');
    }
}
