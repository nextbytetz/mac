<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function (Blueprint $table)
        {
            $table->decimal('no_of_days_for_a_month_in_assessment',14, 4)->unsigned()->nullable()->after('no_of_days_for_a_month')->comment('No of days in a month to be used in claim assessment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sysdefs', function (Blueprint $table)
        {
            $table->dropColumn('no_of_days_for_a_month_in_assessment');
        });
    }
}
