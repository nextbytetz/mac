<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MmodifyColumnMedicalPractitioner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('medical_practitioners', function(Blueprint $table)
        {
            $table->string('middlename', 45)->nullable()->change();

        });

        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->dropUnique('phone');
          });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('medical_practitioners', function(Blueprint $table)
        {
            $table->string('middlename', 45)->change();

        });

        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->string('phone', 45)->unique('phone')->change();
        });
    }
}
