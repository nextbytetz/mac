<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegacyContributions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create or replace VIEW legacy_employee_contributions AS SELECT a.members_contributions_id as id, a.contribution_month, a.employer_id, round(monthly_salary/12, 2) as salary, round(monthly_allowance/12,2) as allowance, b.member_id as employee_id FROM wcf.members_contributions a join wcf.members b on a.member_no = b.member_no");
        DB::statement("create or replace VIEW legacy_employer_contributions AS select * from wcf.contributions");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
