<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBenefitTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('benefit_types', function(Blueprint $table)
        {
            $table->integer('fin_account_code_id')->unsigned()->nullable()->after('shortcode')->index('fin_account_code_id');

        });

        Schema::table('benefit_types', function(Blueprint $table)
        {
            $table->foreign('fin_account_code_id', 'fin_account_code_benefit_type_foreign_key')->references('id')->on('fin_account_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('benefit_types', function(Blueprint $table)
        {
            $table->dropForeign('fin_account_code_benefit_type_foreign_key');
            $table->dropIndex('fin_account_code_id');
            $table->dropColumn('fin_account_code_id');
        });
    }
}
