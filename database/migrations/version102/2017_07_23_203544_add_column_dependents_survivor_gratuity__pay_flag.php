<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDependentsSurvivorGratuityPayFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependents', function (Blueprint $table) {

            $table->boolean('survivor_gratuity_pay_flag')->default(0)->comment('Determine whether has already been paid gratuity / lump sum or not, 1 - processed / paid, 0 - not yet paid / processed ')->after('survivor_gratuity_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dependents', function (Blueprint $table) {

            $table->dropColumn('survivor_gratuity_pay_flag');
        });
    }
}