<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnPaymentVoucherAddNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('benefit_type_id')->unsigned()->nullable()->after('member_type_id')->index('benefit_type_id');

            $table->dropForeign('payment_voucher_type_foreign_key');
        });

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->foreign('benefit_type_id', 'benefit_type_foreign_key')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->dropColumn('payment_voucher_type_id');
        });

        Schema::drop('payment_vouchers');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('payment_vouchers', function(Blueprint $table)
        {
                 $table->dropForeign('benefit_type_foreign_key');
            $table->dropIndex('benefit_type_id');
            $table->dropColumn('benefit_type_id');
        });




    }
}
