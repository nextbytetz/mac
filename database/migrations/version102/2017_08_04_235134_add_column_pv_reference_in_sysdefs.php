<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPvReferenceInSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function (Blueprint $table)
        {
            $table->string('payment_voucher_reference',45)->nullable()->comment('Payment Voucher reference');
        });
        Schema::table('payment_vouchers', function (Blueprint $table)
        {
            $table->string('reference',45)->nullable()->after('accountno')->comment('Payment Voucher reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sysdefs', function (Blueprint $table)
        {
            $table->dropColumn('payment_voucher_reference');
    });

        Schema::table('payment_vouchers', function (Blueprint $table)
        {
            $table->dropColumn('reference');
        });
    }
}
