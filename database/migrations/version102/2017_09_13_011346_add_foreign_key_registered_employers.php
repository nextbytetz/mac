<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyRegisteredEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

/*        Schema::table('registered_employers', function(Blueprint $table) {

            $table->smallInteger('employer_category_id')->unsigned()->change();
        });




        Schema::table('registered_employers', function(Blueprint $table)
        {
            $table->foreign('employer_category_id', 'employer_categories_foreign_ibfk_1')->references('id')->on('employer_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('country_id', 'countries_foreign_ibfk_2')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('region_id', 'regions_foreign_ibfk_3')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id', 'district_foreign_ibfk_4')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('location_type_id', 'location_types_foreign_ibfk_5')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
                Schema::table('registered_employers', function(Blueprint $table)
        {
            $table->dropForeign('employer_categories_foreign_ibfk_1');
            $table->dropForeign('countries_foreign_ibfk_2');
            $table->dropForeign('regions_foreign_ibfk_3');
            $table->dropForeign('district_foreign_ibfk_4');
            $table->dropForeign('location_types_foreign_ibfk_5');
        });
    }
}
