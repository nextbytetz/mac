<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMinimumMaxmumLumpsumSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->decimal('minimum_lumpsum_amount', 14)->nullable()->after('maximum_monthly_pension');
            $table->decimal('maximum_lumpsum_amount', 14)->nullable()->after('minimum_lumpsum_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->dropColumn('minimum_lumpsum_amount');
            $table->dropColumn('maximum_lumpsum_amount');
        });
    }
}
