<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerCategoryForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employer_categories', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->string('name', 150)->unique('name', 'name');
            $table->timestamps();
        });


//



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('employee_categories');
//

    }
}
