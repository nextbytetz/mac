<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IndexDatesForDashboardSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function(Blueprint $table)
        {
            $table->index(['rct_date']);
        });
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->index(['contrib_month']);
        });
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->index(['created_at']);
        });
        Schema::table('inspections', function(Blueprint $table)
        {
            $table->index(['start_date']);
        });
        Schema::table('employer_inspection_task', function(Blueprint $table)
        {
            $table->index(['visit_date']);
        });
        Schema::table('cases', function(Blueprint $table)
        {
            $table->index(['created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
