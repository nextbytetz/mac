<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInspectionProfileOnInspectionTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_tasks', function (Blueprint $table)
        {
            $table->integer("inspection_profile_id")->nullable()->unsigned()->after("deliverable")->comment("in case this task was selected for inspection profile")->change();
            $table->integer("employer_count")->nullable()->after("inspection_profile_id")->comment("number of employers in case the inspection task was selected from a inspection profile");
            $table->integer("last_id")->after("employer_count")->nullable()->comment("last employer_id of the selected count of employers in case inspection profile was selected for this inspection task");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_tasks', function (Blueprint $table)
        {
            $table->dropColumn("last_id");
            $table->dropColumn("employer_count");
        });
    }
}
