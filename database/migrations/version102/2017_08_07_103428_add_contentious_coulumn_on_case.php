<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentiousCoulumnOnCase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function(Blueprint $table)
        {
            $table->tinyInteger("liability_type")->nullable()->comment("State whether a case is contentious or non contentious. In case of contentious, either you owe or owed. 1 Contentious, 2 Non contentious");
            $table->tinyInteger("contentious_type")->nullable()->comment(" 1 - we owe money, 2 - we owed money");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cases', function(Blueprint $table)
        {
            $table->dropColumn("liability_type");
            $table->dropColumn("contentious_type");
        });
    }
}
