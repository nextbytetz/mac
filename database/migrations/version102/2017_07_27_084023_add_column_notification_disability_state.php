<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNotificationDisabilityState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_disability_states', function(Blueprint $table)
        {
            $table->Integer('days')->nullable()->after('day_hours')->comment('No of Days for each disability state. i.e actual hospitalization days.');

        });


        Schema::table('sysdefs', function(Blueprint $table)
        {

            $table->decimal('minimum_monthly_pension', 14)->nullable()->comment('Minimum payable amount per month i.e pension, ttd, tpd')->change();
            $table->decimal('maximum_monthly_pension', 14)->nullable()->comment('Maximum payable amount per month i.e pension, ttd, tpd')->change();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_disability_states', function(Blueprint $table)
        {

            $table->dropColumn('days');

        });
    }
}
