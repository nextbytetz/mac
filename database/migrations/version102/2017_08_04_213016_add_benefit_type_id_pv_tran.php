<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBenefitTypeIdPvTran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->integer('benefit_type_id')->unsigned()->nullable()->after('resource_id')->index('benefit_type_id');

            $table->dropForeign('fin_account_code_pv_tran_foreign_key');
            $table->dropIndex('fin_account_code_id');
            $table->dropColumn('fin_account_code_id');

        });

        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->foreign('benefit_type_id', 'benefit_type_pv_tran_foreign_key')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {

            $table->dropForeign('benefit_type_pv_tran_foreign_key');
            $table->dropIndex('benefit_type_id');
            $table->dropColumn('benefit_type_id');

            $table->integer('fin_account_code_id')->unsigned()->nullable()->after('resource_id')->index('fin_account_code_id');

        });



        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->foreign('fin_account_code_id', 'fin_account_code_pv_tran_foreign_key')->references('id')->on('fin_account_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

    }
}
