<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsPractitionerWitnessSysdefs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
//        wITNESS
        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->string('lastname', 45)->after('othernames');
            $table->renameColumn('othernames', 'middlename');
            $table->text('address')->after('phone')->nullable()->comment("Address of the witness");
            $table->string('national_id', 45)->nullable()->after('address');
        });

//        medical practitioners
        Schema::table('medical_practitioners', function(Blueprint $table)
        {
            $table->string('lastname', 45)->after('othernames');
            $table->renameColumn('othernames', 'middlename');
            $table->string('national_id', 45)->nullable()->after('address');
        });

//        sysdef
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->integer('min_days_for_ttd_tpd')->nullable()->after('no_of_days_for_a_month_in_assessment')->comment('Minimum Days eligible to compensate Temporary Disablement');
        });

//  claims
        Schema::table('claims', function(Blueprint $table)
        {
            $table->decimal('pd',10,7)->default(0)->comment('Percentage of permanent disability for this claim notification')->change();

        });


//        accidents
        Schema::table('accidents', function(Blueprint $table)
        {
            $table->time('accident_time')->after('accident_place')->comment('Time when accident occured');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->dropColumn('lastname');
            $table->renameColumn('middlename', 'othernames');
            $table->dropColumn('address');
            $table->dropColumn('national_id');
        });


        Schema::table('medical_practitioners', function(Blueprint $table)
        {
            $table->dropColumn('lastname');
            $table->renameColumn('middlename', 'othernames');
            $table->dropColumn('national_id');
        });

        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->dropColumn('min_days_for_ttd_tpd');

        });


        Schema::table('claims', function(Blueprint $table)
        {
            $table->decimal('pd',10,7)->comment('Percentage of permanent disability for this claim notification')->change();

        });


        Schema::table('accidents', function(Blueprint $table)
        {
            $table->dropColumn('accident_time');

        });

    }
}
