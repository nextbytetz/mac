<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsNotificationReportInitialExpense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->integer('initial_expense_member_type_id')->unsigned()->index('initial_expense_member_type_id')->nullable()->after('user_id')->comment('Member type who paid initial expense for this notification report');
            $table->integer('initial_expense_resource_id')->unsigned()->nullable()->after('initial_expense_member_type_id')->comment('Primary key of member type who paid initial expense for this notification report');
            $table->integer('pay_through_insurance_id')->unsigned()->nullable()->index('pay_through_insurance_id')->after('initial_expense_resource_id')->comment('Insurance used to pay initial expense for this notification report');

            $table->boolean('is_cash')->default(0)->after('pay_through_insurance_id')->comment('Flag to show whether initial expense was paid by member i.e employee or employer.');
        });

        Schema::table('notification_reports', function (Blueprint $table) {
             $table->foreign('initial_expense_member_type_id', 'member_type_id_foreign')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('pay_through_insurance_id', 'pay_through_insurance_id_foreign')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->dropForeign('member_type_id_foreign');
            $table->dropForeign('pay_through_insurance_id_foreign');
            $table->dropIndex('initial_expense_member_type_id');
            $table->dropIndex('pay_through_insurance_id');

            $table->dropColumn('initial_expense_member_type_id');
            $table->dropColumn('initial_expense_resource_id');
            $table->dropColumn('pay_through_insurance_id');
            $table->dropColumn('is_cash');
        });
    }
}
