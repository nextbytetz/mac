<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsverifiedOnReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function (Blueprint $table)
        {
            $table->tinyInteger("isverified")->nullable()->after("iscomplete")->default(0)->comment("Set whether this receipt has already been verified or not");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipts', function (Blueprint $table)
        {
            $table->dropColumn("isverified");
        });
    }
}
