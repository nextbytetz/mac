<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnNotificationDisabilityState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('notification_disability_states', function (Blueprint $table) {
            $table->dropColumn('day_hours');
            $table->decimal('percent_of_ed_ld',10,7)->nullable()->comment('Percentage of Excuse duty (ED) / Light duties (LD)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('notification_disability_states', function (Blueprint $table) {
            $table->tinyInteger('day_hours')->nullable()->comment('hours worked per day by a member');
            $table->dropColumn('day_hours');
        });
    }
}
