<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDocumentsTableStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename("document_type_groups", "document_groups");
        Schema::rename("document_type_group_incident_type", "document_group_incident_type");
        Schema::table('document_group_incident_type', function (Blueprint $table)
        {
            $table->renameColumn('document_type_group_id', 'document_group_id');;
        });
        Schema::rename("document_types", "documents");
        Schema::rename("document_type_notification_report", "document_notification_report");
        Schema::table('document_notification_report', function (Blueprint $table)
        {
            $table->renameColumn('document_type_id', 'document_id');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_notification_report', function (Blueprint $table)
        {
            $table->renameColumn('document_id', 'document_type_id');;
        });
        Schema::rename("document_notification_report", "document_type_notification_report");
        Schema::rename("documents", "document_types");
        Schema::table('document_group_incident_type', function (Blueprint $table)
        {
            $table->renameColumn('document_group_id', 'document_type_group_id');;
        });
        Schema::rename("document_group_incident_type", "document_type_group_incident_type");
        Schema::rename("document_groups", "document_type_groups");
    }
}
