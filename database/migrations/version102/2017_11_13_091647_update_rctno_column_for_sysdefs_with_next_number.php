<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Sysdef\Sysdef;

class UpdateRctnoColumnForSysdefsWithNextNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DB::getDriverName() == "pgsql") {
            $receipt = DB::select("SELECT right(rctno::text, 8)::int as rctno FROM receipts a ORDER BY right(rctno::text, 8)::int DESC limit 1");
        } else {
            $receipt = DB::select("SELECT cast(right(rctno, 8) as signed int) as rctno FROM `receipts` a ORDER BY cast(right(rctno, 8) as signed int) DESC limit 1");
        }

        $sysdef = Sysdef::find(1);
        $sysdef->rctno = $receipt[0]->rctno + 1;
        $sysdef->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
