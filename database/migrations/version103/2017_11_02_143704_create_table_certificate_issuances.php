<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableCertificateIssuances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::beginTransaction();

        Schema::create('employer_certificate_issues', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->integer('employer_id')->unsigned()->index();
            $table->smallInteger('is_reissue')->unsigned()->default(0)->comment("Flag to specify if employer has been reissued registration certificate");
            $table->timestamps();
        });

        Schema::table('employer_certificate_issues', function(Blueprint $table) {

            $table->foreign('employer_id', 'employer_id_foreign_ibfk')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
//
//        Schema::table('employers', function(Blueprint $table)
//        {
//            $table->smallInteger('is_certified')->unsigned()->nullable();
//        });
//        DB::statement("comment on column employers.is_certified is 'Flag to specify if employer has been issued registration certificate'");


        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::beginTransaction();
        Schema::table('employer_certificate_issues', function(Blueprint $table)
        {
            $table->dropForeign('employer_id_foreign_ibfk');
            $table->dropIndex('employer_certificate_issues_employer_id_index');
            $table->dropColumn('employer_id');
        });

        Schema::drop('employer_certificate_issues');

//        Schema::table('employers', function(Blueprint $table)
//        {
//                 $table->dropColumn('is_certified');
//        });

        DB::commit();
    }
}
