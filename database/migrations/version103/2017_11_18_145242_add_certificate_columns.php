<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCertificateColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employers', function(Blueprint $table)
        {
            $table->integer("cert_status")->default(0);
            $table->integer("cert_issued_by")->nullable();
            $table->date("cert_issued_date")->nullable();
            $table->integer("cert_reissued_by")->nullable();
            $table->date("cert_reissued_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
