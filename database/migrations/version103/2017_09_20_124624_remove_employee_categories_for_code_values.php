<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEmployeeCategoriesForCodeValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('employee_counts', function(Blueprint $table)
        {
            $table->dropForeign("employee_category_id");
            $table->dropIndex("employee_category_id");
        });

        Schema::table('employee_counts', function(Blueprint $table)
        {
            $table->renameColumn("employee_category_id", "employee_category_cv_id");
        });

        Schema::table('employee_counts', function(Blueprint $table)
        {
            $table->index("employee_category_cv_id", "employee_category_cv_id");
        });

        Schema::table('employee_counts', function(Blueprint $table)
        {
            $table->integer("employee_category_cv_id")->unsigned()->change();
            $table->foreign('employee_category_cv_id', 'employee_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        Schema::table('employees', function(Blueprint $table)
        {
            $table->dropForeign("employees_ibfk_6");
            $table->dropIndex("employee_category_id");
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->renameColumn("employee_category_id", "employee_category_cv_id");
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->index("employee_category_cv_id", "employee_category_cv_id");
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->integer("employee_category_cv_id")->unsigned()->change();
        });



        Schema::table('employees', function(Blueprint $table)
        {
            $table->foreign('employee_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::dropIfExists('employee_categories');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
