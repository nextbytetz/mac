<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\DisableForeignKeys;

class ModifyColumnOrderAndForeignKeys extends Migration
{

    use DisableForeignKeys;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->disableForeignKeys();

        Schema::dropIfExists('employees');

        Schema::create('employees', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('memberno');
            $table->string('firstname', 100);
            $table->string('middlename', 100)->nullable();
            $table->string('lastname', 100);
            $table->string('email', 45)->nullable();
            $table->string('phone', 45)->nullable();
            $table->date('dob')->nullable();
            $table->decimal('salary', 14)->nullable();
            $table->decimal('allowance', 14)->nullable();
            $table->string('nid', 50)->nullable()->comment('national identity card');
            $table->integer('identity_id')->unsigned()->nullable()->index();
            $table->integer('job_title_id')->unsigned()->nullable()->index();
            $table->integer('gender_id')->unsigned()->nullable()->index();
            $table->integer('marital_status_cv_id')->unsigned()->nullable()->index();
            $table->integer('employee_category_cv_id')->unsigned()->nullable()->index();
            $table->integer('bank_branch_id')->unsigned()->nullable()->index();
            $table->integer('location_type_id')->unsigned()->nullable()->index();
            $table->string('street', 150)->nullable();
            $table->string('road', 150)->nullable();
            $table->string('plot_no', 150)->nullable();
            $table->string('block_no', 150)->nullable();
            $table->text('surveyed_extra', 65535)->nullable();
            $table->text('unsurveyed_area', 65535)->nullable();
            $table->integer('user_id')->nullable()->unsigned()->index("user_id");
            $table->text('department', 65535)->nullable();
            $table->string('fax', 45)->nullable();
            $table->string('accountno', 45)->nullable();
            $table->string('sex', 10)->nullable();
            $table->string('job_title', 150)->nullable();
            $table->string('emp_cate', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('identity_id')->references('id')->on('identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('job_title_id')->references('id')->on('job_titles')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('gender_id')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('location_type_id')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('marital_status_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::dropIfExists('employers');

        Schema::create('employers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('reg_no', 500)->nullable();
            $table->dateTime('doc')->nullable()->comment('date of commencement of the business');
            $table->string('tin', 100)->nullable()->comment('TRA TIN');
            $table->string('phone', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('fax', 45)->nullable();
            $table->string('vote', 100)->nullable();
            $table->decimal('annual_earning', 18)->nullable();
            $table->integer('district_id')->unsigned()->nullable()->index();
            $table->integer('employer_category_cv_id')->unsigned()->nullable()->index();
            $table->integer('bank_branch_id')->unsigned()->nullable()->index();
            $table->string('accountno', 45)->nullable();
            $table->integer('location_type_id')->unsigned()->nullable()->index();
            $table->string('road', 150)->nullable();
            $table->string('street', 45)->nullable();
            $table->string('plot_no', 45)->nullable();
            $table->string('block_no', 45)->nullable();
            $table->text('surveyed_extra', 65535)->nullable();
            $table->text('unsurveyed_area', 65535)->nullable();
            $table->integer('region_id')->nullable();
            $table->string('district', 45)->nullable();
            $table->integer('representer_id')->unsigned()->nullable()->index();
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified')->nullable();
            $table->integer('approval_id')->nullable()->default(2)->comment(" 1 - Approved, 2 - Rejected");
            $table->text('reject_reason', 65535)->nullable();
            $table->integer('emp_approved_by')->nullable();
            $table->date('emp_approved_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('employers', function(Blueprint $table)
        {
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('location_type_id')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('representer_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('employer_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::dropIfExists('users');

        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('username', 45)->nullable()->unique();
            $table->string('samaccountname', 45)->nullable()->unique()->comment("Microsoft Active Directory Username");
            $table->string('password', 100)->nullable();
            $table->string('firstname', 45)->nullable();
            $table->string('lastname', 45)->nullable();
            $table->string('middlename', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('phone', 45)->nullable();
            $table->integer('unit_id')->unsigned()->nullable()->index();
            $table->integer('designation_id')->unsigned()->nullable()->index();
            $table->integer('gender_id')->unsigned()->nullable()->index();
            $table->string('external_id', 20)->nullable()->comment('any unique reference used to refer for this user');
            $table->tinyInteger('active')->default(1)->comment('set whether a user is active or not, 1 - active, 0 - not active');
            $table->tinyInteger('available')->default(1)->comment('set whether a user is available in an office or not, 1 available, 0 not available');
            $table->dateTime('last_login')->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->integer('created_by')->nullable();
            $table->dateTime('created')->nullable();
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified')->nullable();
            $table->integer('task_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->foreign('designation_id')->references('id')->on('designations')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('gender_id')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('insurances', function(Blueprint $table)
        {
            $table->dropForeign("insurance_bank_id_foreign");
            $table->dropIndex("bank_id");
        });

        Schema::table('insurances', function(Blueprint $table)
        {
            $table->dropColumn("bank_id");
        });

        $this->enableForeignKeys();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
