<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerIdToContribution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contributions', function(Blueprint $table)
        {
            $table->dropColumn('basicpay');
        });

        Schema::table('contributions', function(Blueprint $table)
        {
            $table->decimal('salary', 14)->nullable()->after("employee_amount");
            $table->integer("employer_id")->nullable()->after("employee_id")->unsigned()->index("employer_id");
        });

        Schema::table('contributions', function(Blueprint $table)
        {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
