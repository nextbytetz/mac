<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContributionMonthOnContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contributions', function(Blueprint $table)
        {
            $table->date('contrib_month')->nullable()->after("grosspay");
        });

        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropForeign("dependents_ibfk_6");
            $table->dropIndex("bank_id");
        });

        Schema::table('dependents', function(Blueprint $table)
        {
            $table->dropColumn("bank_id");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
