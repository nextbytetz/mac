<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IntegratingOldMacIntoNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::dropIfExists('registered_employers');

        Schema::table('code_values', function(Blueprint $table)
        {
            $table->unique(['reference']);
        });

        DB::statement("drop view businesses");
        DB::statement("drop view business_activities");

        Schema::create('employer_sectors', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_id')->unsigned()->index('employer_id');
            $table->integer('business_sector_cv_id')->unsigned()->index('business_sector_cv_id');
            $table->timestamps();
            $table->foreign('business_sector_cv_id', 'business_sector_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("drop view legacy_employer_contributions");
        DB::statement("drop view legacy_employee_contributions");

        Schema::table('code_values', function(Blueprint $table)
        {
            $table->decimal('allowance', 14)->nullable();
        });

        Schema::create('legacy_dishonoured_cheques', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('legacy_receipt_id')->nullable();
            $table->integer('status')->default(0);
            $table->integer('assigned_to')->nullable();
            $table->date('assigned_date')->nullable();
            $table->text('updates', 65535)->nullable();
            $table->timestamps();
        });

        DB::statement("drop view employee_ver_ones");
        DB::statement("drop view employees");

        Schema::rename("employee_ver_twos", "employees");



        Schema::create('legacy_logs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->dateTime('date_time')->nullable();
            $table->string('page', 150)->nullable();
            $table->string('data', 150)->nullable();
        });

        Schema::create('legacy_receipts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('rctno', 100)->nullable();
            $table->date('rct_date')->nullable();
            $table->decimal('amount', 14)->nullable();
            $table->string('chequeno', 50)->nullable();
            $table->integer('bank_id')->nullable()->unsigned()->index('bank_id');
            $table->integer('payment_type_id')->nullable()->unsigned()->index('payment_type_id');
            $table->date('contrib_month')->nullable();
            $table->integer('employer_id')->unsigned()->nullable()->index("employer_id");
            $table->integer('status')->nullable()->default(0);
            $table->text('comments', 65535)->nullable();
            $table->decimal('amount_imported', 14)->nullable()->comment("total amount computed from the gross pay in the uploaded file. After the linked file has been uploaded.");
            $table->integer('member_count')->nullable()->comment("total number of members being paid for contribution");
            $table->integer('user_id')->nullable()->unsigned()->index("user_id");
            $table->integer('modified_by')->nullable();
            $table->date('modified_date')->nullable();
            $table->integer('contr_approval')->nullable()->default(0)->comment("0 - Pending, 1 - Approved, 2 - Rejected");
            $table->text('reason', 65535)->nullable();
            $table->integer('approved_by')->nullable();
            $table->date('approved_date')->nullable();
            $table->timestamps();
            $table->foreign('payment_type_id', 'payment_type_id')->references('id')->on('payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::create('legacy_receipts_codes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("legacy_receipt_id")->unsigned()->index("legacy_receipt_id");
            $table->decimal("amount", 14)->comment("contribution amount");
            $table->date('contrib_month')->nullable();
            $table->integer('member_count')->nullable()->comment("total number of members being paid for in case receipt for contribution");
            $table->integer('rows_imported')->nullable()->comment("Total number of rows from the imported excel file which has already been stored in the database table (contribution_temps), if this receipt is for monthly contribution");
            $table->integer('total_rows')->nullable()->comment("Total number of rows from the imported excel file, if this receipt is for monthly contribution");
            $table->decimal('amount_imported', 14)->nullable()->comment("total amount computed from the gross pay in the uploaded file. After the linked file has been uploaded.");
            $table->string('mime', 150)->nullable()->comment("mime type of the uploaded contribution file");
            $table->text("upload_error")->nullable()->comment("store error report when the file upload failed from the queue jobs");
            $table->smallInteger("isuploaded")->nullable()->default(0)->comment("store the status whether the contributions for this receipt has already been uploaded or not yet, 1 - already updated, 0 - not yet uploaded completely");
            $table->smallInteger("error")->nullable()->default(0)->comment("show whether the associated uploaded file has error or not");
            $table->double("size")->nullable()->comment("size of the uploaded contribution file");
            $table->timestamps();
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->renameColumn('employee_id', 'id');
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned()->change();
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->string('firstname', 100)->nullable();
            $table->string('middlename', 100)->nullable();
            $table->string('lastname', 100)->nullable();
            $table->date('dob')->nullable();
            $table->string('sex', 10)->nullable();
            $table->string('job_title', 150)->nullable();
            $table->string('emp_cate', 150)->nullable();
            $table->decimal('salary', 14)->nullable();
            $table->decimal('allowance', 14)->nullable();
            $table->integer('memberno');
            $table->string('nid', 50)->nullable()->comment("national identity card");
        });

        Schema::create('employee_employers', function(Blueprint $table)
        {
            $table->bigIncrements("id");
            $table->integer("employee_id")->unsigned()->index("employee_id");
            $table->integer("employer_id")->unsigned()->index("employer_id");
            $table->timestamps();
        });



        Schema::table('employees', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned()->change();
        });


        Schema::table('employee_employers', function(Blueprint $table)
        {
            $table->foreign('employee_id', 'employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::create('employee_counts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_id')->nullable()->index('employer_id');
            $table->integer('employee_category_id')->nullable()->index('employee_category_id');
            $table->integer('gender_id')->nullable()->index('gender_id');
            $table->integer('count')->nullable();
            $table->decimal('annual_earning', 14)->nullable();
            $table->timestamps();

        });

        Schema::table('employee_counts', function(Blueprint $table)
        {
            $table->integer('employer_id')->nullable()->unsigned()->change();
            $table->smallInteger('employee_category_id')->nullable()->unsigned()->change();
            $table->integer('gender_id')->nullable()->unsigned()->change();
        });

        Schema::table('employee_counts', function(Blueprint $table)
        {
            $table->foreign('employee_category_id', 'employee_category_id')->references('id')->on('employee_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('gender_id', 'gender_id')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("drop view user_ver_ones");
        DB::statement("drop view users");

        Schema::rename("user_ver_twos", "users");


        Schema::table('users', function(Blueprint $table)
        {
            $table->renameColumn('user_id', 'id');
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned()->change();
        });

        DB::statement("TRUNCATE TABLE users;");

        Schema::table('users', function(Blueprint $table)
        {
            $table->string('firstname', 45)->nullable();
            $table->string('lastname', 45)->nullable();
            $table->string('middlename', 45)->nullable();
            $table->string('username', 45)->unique()->nullable();
            $table->string('password', 100)->nullable();
            $table->integer('created_by')->nullable();
            $table->dateTime('created')->nullable();
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified')->nullable();
            $table->integer('task_id')->default(0);
        });

        DB::statement("drop view employer_ver_ones");
        DB::statement("drop view employers");

        Schema::rename("employer_ver_twos", "employers");

        Schema::table('employers', function(Blueprint $table)
        {
            $table->renameColumn('employer_id', 'id');
        });

        Schema::table('employers', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned()->change();
        });

        Schema::table('employers', function(Blueprint $table)
        {
            $table->string('name', 150)->unique('name');
            $table->string('reg_no', 500)->nullable();
            $table->dateTime('doc')->nullable()->comment("date of commencement of the business");
            $table->integer('region_id')->nullable()->index('region_id');
            $table->integer('district_id')->nullable()->unsigned()->index('district_id');
            $table->string('district', 45)->nullable();
            $table->string('street', 45)->nullable();
            $table->string('plot_no', 45)->nullable();
            $table->string('block_no', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->string('fax', 45)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->integer('representer_id')->nullable()->index('representer_id');
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified')->nullable();
            $table->decimal('annual_earning', 14)->nullable();
            $table->integer('employer_category_id')->nullable();
            $table->integer('approval_id')->nullable()->default(2);
            $table->text('reject_reason', 65535)->nullable();
            $table->string('vote', 100)->nullable();
            $table->string('tin', 100)->nullable()->comment("TRA TIN");
            $table->integer('emp_approved_by')->nullable();
            $table->date('emp_approved_date')->nullable();
            $table->foreign('district_id', 'district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('employers', function(Blueprint $table)
        {
            $table->integer('representer_id')->nullable()->unsigned()->change();
        });

        Schema::create('unregistered_employers', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->text('name', 65535)->nullable();
            $table->text('address', 65535)->nullable();
            $table->text('plot_number', 65535)->nullable();
            $table->text('street', 65535)->nullable();
            $table->text('postal_city', 65535)->nullable();
            $table->text('location', 65535)->nullable();
            $table->text('business_activity', 65535)->nullable();
            $table->text('last_remittance', 65535)->nullable();
            $table->integer('state')->default(0);
            $table->string('phone', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->date('updated')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('assign_to')->default(0);
            $table->date('date_assigned')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_numbers');
        Schema::drop('legacy_receipts');
        Schema::drop('legacy_logs');
    }
}
