<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegacyContributionReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::rename("legacy_receipts_codes", "legacy_receipt_codes");

        Schema::table('contributions', function(Blueprint $table)
        {
            $table->integer('receipt_code_id')->unsigned()->nullable()->change();
            $table->integer('legacy_receipt_code_id')->unsigned()->nullable()->index("legacy_receipt_code_id")->after("receipt_code_id");
            $table->integer('legacy_receipt_id')->unsigned()->nullable()->index("legacy_receipt_id")->after("receipt_code_id");
        });

        Schema::table('contributions', function(Blueprint $table)
        {
            $table->foreign('legacy_receipt_code_id')->references('id')->on('legacy_receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            //$table->foreign('legacy_receipt_id')->references('id')->on('legacy_receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
