<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteMaritalStatusTableForCodeValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('employees', function(Blueprint $table)
        {
            $table->dropForeign("employees_ibfk_4");
            $table->dropIndex("marital_status_id");
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->renameColumn("marital_status_id", "marital_status_cv_id");
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->index("marital_status_cv_id", "marital_status_cv_id");
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->integer("marital_status_cv_id")->unsigned()->change();
        });

        Schema::table('employees', function(Blueprint $table)
        {
            $table->foreign('marital_status_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::dropIfExists('marital_statuses');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
