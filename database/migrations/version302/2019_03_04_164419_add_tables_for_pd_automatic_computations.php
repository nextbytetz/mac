<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablesForPdAutomaticComputations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pd_injuries', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->string("name", 200);
            $table->smallInteger("rank");
            $table->timestamps();
        });
        DB::statement("comment on table pd_injuries is 'list of body part injuries which the doctor will choose as the highest body injury of the incident for assessment'");

        Schema::create('pd_impairments', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->string("name", 200);
            $table->timestamps();
        });
        DB::statement("comment on table pd_impairments is 'comprehensive list of body part injured which a doctor select the most preferably injury for the incident to be used in evaluating occupational characteristic value (letter)'");

        Schema::create('pd_amputations', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->string("name", 200);
            $table->decimal("percent", 5);
            $table->timestamps();
        });
        DB::statement("comment on table pd_amputations is 'list of lost body parts with the corresponding percentage for pd'");

        Schema::create('pd_fecs', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->smallInteger("wpi");
            $table->smallInteger("rank");
            $table->smallInteger("rating");
            $table->timestamps();
        });
        DB::statement("comment on table pd_fecs is 'disability rating table/future earning capacity'");
        DB::statement("comment on column pd_fecs.wpi is 'whole person impairment determined by doctors during assessment'");
        DB::statement("comment on column pd_fecs.rank is 'rank from the pd_injuries table'");

        Schema::create('pd_occupation_characteristics', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->smallInteger("group");
            $table->smallInteger("pd_impairment_id");
            $table->char("characteristic", 1);
            $table->timestamps();
            $table->foreign('pd_impairment_id')->references('id')->on('pd_impairments')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table pd_occupation_characteristics is 'evaluate the characteristic value of the disablement from the occupation group and occupational impairment'");
        DB::statement("comment on column pd_occupation_characteristics.group is 'occupational group from the occupations table'");

        Schema::create('pd_occupation_adjustments', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->char("characteristic", 1);
            $table->smallInteger("prev_rating");
            $table->smallInteger("rating");
            $table->timestamps();
        });
        DB::statement("comment on table pd_occupation_adjustments is 'Rating adjustment for occupation'");
        DB::statement("comment on column pd_occupation_adjustments.characteristic is 'characteristic evaluated from pd_occupation_characteristics table'");
        DB::statement("comment on column pd_occupation_adjustments.prev_rating is 'previous rating evaluated from pd_fecs table'");

        Schema::create('pd_age_ranges', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->smallInteger("from");
            $table->smallInteger("to");
            $table->timestamps();
        });
        DB::statement("comment on table pd_age_ranges is 'Age range criteria for pd computation'");

        Schema::create('pd_age_adjustments', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->smallInteger("pd_age_range_id");
            $table->smallInteger("prev_rating");
            $table->smallInteger("rating");
            $table->timestamps();
            $table->foreign('pd_age_range_id')->references('id')->on('pd_age_ranges')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table pd_age_adjustments is 'Rating adjustment for age'");
        DB::statement("comment on column pd_age_adjustments.prev_rating is 'previous rating evaluated from pd_occupation_adjustments table'");

        Schema::create('incident_assessments', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->date("dob")->nullable();
            $table->smallInteger("age")->nullable();
            $table->char("characteristic", 1)->nullable();
            $table->smallInteger("pd_impairment_id")->nullable();
            $table->smallInteger("group")->nullable();
            $table->smallInteger("wpi")->nullable();
            $table->smallInteger("pd_injury_id")->nullable();
            $table->smallInteger("pd_amputation_id")->nullable();
            $table->smallInteger("rating");
            $table->smallInteger("method");
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('pd_impairment_id')->references('id')->on('pd_impairments')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('pd_injury_id')->references('id')->on('pd_injuries')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('pd_amputation_id')->references('id')->on('pd_amputations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table incident_assessments is 'keep the track of the assessment parameters used per pd computation'");
        DB::statement("comment on column incident_assessments.rating is 'percentage disablement rating evaluated after first and/or second schedule procedure'");
        DB::statement("comment on column incident_assessments.method is 'method used for computation of permanent disability rating/percentage, 1 - first schedule, 2 - second schedule, 3 - both methods were used. '");

        Schema::table('notification_letters', function (Blueprint $table) {
            $table->text("rejection")->nullable();
            $table->bigInteger("rejection_user")->nullable();
            $table->text("acceptance")->nullable();
            $table->bigInteger("acceptance_user")->nullable();
            $table->foreign('rejection_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('acceptance_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
