<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewEmployersContribtuions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
//        DB::statement("DROP VIEW employer_contributions");
        DB::statement("CREATE VIEW  employer_contributions  AS select r.employer_id, e.doc, e.name as employer_name, e.reg_no as employer_reg_no,code.amount as contrib_amount, r.amount as receipt_amount, code.contrib_month, r.rct_date, r.created_at as receipt_created_at, r.rctno
from receipt_codes code
join receipts r on code.receipt_id = r.id
join employers e on r.employer_id = e.id
where r.iscancelled = 0 and r.isdishonoured = 0 and r.deleted_at is null
and (code.fin_code_id = 2 or code.fin_code_id = 63)
order by e.id, code.contrib_month
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
