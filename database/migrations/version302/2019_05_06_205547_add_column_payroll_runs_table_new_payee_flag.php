<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollRunsTableNewPayeeFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_runs', function(Blueprint $table)
        {
                $table->smallInteger('new_payee_flag')->default(0);
        });
        DB::statement("comment on column payroll_runs.new_payee_flag is 'SHow whether this payroll is the first to this beneficiary i.e. 1 => first pay, 0 => not first pay'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
