<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoIncidentClosuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('incident_tds', function(Blueprint $table)
        {
            $table->smallInteger("td_refund_ready")->default(0);
            $table->date("td_refund_ready_date")->nullable();
        });

        $sql = <<<SQL

insert into incident_closures(notification_report_id, register_date, close_date)  select id, created_at, wf_done_date from notification_reports where isprogressive = 0 union all select id, created_at, NULL from notification_reports where isprogressive = 1;

update notification_reports set incident_closure_id = incident_closures.id from incident_closures where incident_closures.notification_report_id = notification_reports.id;

insert into incident_tds(notification_report_id, td_ready, td_refund_ready) select id, td_ready, td_refund_ready from notification_reports where isprogressive = 1 and (td_ready = 1 or td_refund_ready = 1);

update notification_reports set incident_td_id = incident_tds.id from incident_tds where incident_tds.notification_report_id = notification_reports.id;

SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
