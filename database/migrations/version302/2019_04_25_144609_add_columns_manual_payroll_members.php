<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsManualPayrollMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('manual_payroll_members', function(Blueprint $table)
        {
            $table->smallInteger('ispaid')->default(1);
            $table->smallInteger('hasarrears')->default(0);
            $table->smallInteger('pending_pay_months')->default(0);
            $table->text('remark')->nullable();
            $table->integer('resource_id')->nullable();
        });
        DB::statement("comment on column manual_payroll_members.ispaid is 'Show whether beneficiary already been paid at least once  through manual process . 1 - Yes, 0 - No'");
        DB::statement("comment on column manual_payroll_members.hasarrears is 'Show whether beneficiary already been paid at least once  through manual process and have arrears, pending unpaid months . 1 - Yes, 0 - No, 2 - Has arrears and already recovered through system'");
        DB::statement("comment on column manual_payroll_members.pending_pay_months is 'No of months pending for payment (arrears) which will be paid on through system. 1 - Yes, 0 - No'");
        DB::statement("comment on column manual_payroll_members.remark is 'Remark concerning the beneficiary'");
        DB::statement("comment on column manual_payroll_members.resource_id is 'Primary key of the table of member type specified i.e. dependent and pensioner'");

        /*Payroll recoveries*/
        Schema::table('payroll_recoveries', function(Blueprint $table)
        {
            $table->smallInteger('ismanual')->default(0);
        });
        DB::statement("comment on column payroll_recoveries.ismanual is 'Show whether recovery is from manual process . 1 - Yes, 0 - No'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
