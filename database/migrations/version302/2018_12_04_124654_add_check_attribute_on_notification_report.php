<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckAttributeOnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column notification_reports.isverified is 'for progressive notification, it shows whether notification checklist has been filled. for non-progressive notification,  1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.investigation_validity is 'show whether the investigation has completed or not,  1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
