<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentInvestigationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_investigation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("notification_report_id")->index();
            $table->integer("document_id")->index();
            $table->string("name", 150)->nullable();
            $table->text("description")->nullable();
            $table->string("ext", 20)->nullable();
            $table->double("size")->nullable();
            $table->string("mime", 150)->nullable();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table document_investigation is 'table to store investigation feedback images for notification reports'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
