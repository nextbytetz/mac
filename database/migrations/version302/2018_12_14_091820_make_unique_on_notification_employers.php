<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUniqueOnNotificationEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_employers', function (Blueprint $table) {
            $table->unique(['notification_report_id', 'employer_id']);
        });
        Schema::table('notification_investigators', function (Blueprint $table) {
            $table->smallInteger("recycle")->default(0);
        });
        DB::statement("comment on column notification_investigators.recycle is 'frequency of filling the investigation report for the same user/staff'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
