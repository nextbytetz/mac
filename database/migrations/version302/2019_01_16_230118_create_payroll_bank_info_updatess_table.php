<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollBankInfoUpdatessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('resource_id');
            $table->integer('member_type_id');
            $table->integer('old_bank_branch_id');
            $table->string('old_accountno', 45);
            $table->integer('new_bank_branch_id');
            $table->string('new_accountno', 45);
            $table->integer('user_id');
            $table->smallInteger('wf_done')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_bank_info_updates is 'Table to keep track of all changes on bank details of payroll beneficiary'");
        DB::statement("comment on column payroll_bank_info_updates.resource_id is 'Primary key of the resource table'");

        Schema::table('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->foreign('old_bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('new_bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
