<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeOnNotificationInvestigators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_investigators', function (Blueprint $table) {
            $table->smallInteger("type")->default(2);
        });
        DB::statement("comment on column notification_investigators.type is 'show whether it is physical or phone call investigation i.e. 1 - Phone Call Investigation, 2 - Physical Investigation'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
