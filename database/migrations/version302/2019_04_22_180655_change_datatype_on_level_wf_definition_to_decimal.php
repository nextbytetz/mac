<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatatypeOnLevelWfDefinitionToDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("select deps_save_and_drop_dependencies('main', 'wf_definitions')");
        DB::statement("alter table wf_definitions alter column level type numeric(4,2) using level::numeric");
        DB::statement("select deps_restore_dependencies('main', 'wf_definitions')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
