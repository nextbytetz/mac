<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollSuspendedRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payroll_run_approval_id');
            $table->integer('member_type_id');
            $table->integer('resource_id');
            $table->decimal('amount', 14);
            $table->smallInteger('months_paid');
            $table->decimal('monthly_pension', 14);
            $table->integer('notification_report_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table payroll_suspended_runs is 'Table to keep track of all suspended payments for all suspended beneficiary during payroll run' ");

        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('payroll_run_approval_id')->references('id')->on('payroll_run_approvals')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
