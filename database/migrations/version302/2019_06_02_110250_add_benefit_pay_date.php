<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBenefitPayDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incident_tds', function(Blueprint $table)
        {
            $table->date("td_refund_pay_date")->nullable();
            $table->date("td_pay_date")->nullable();
        });
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->date("pd_pay_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
