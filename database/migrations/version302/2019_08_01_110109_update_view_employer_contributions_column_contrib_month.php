<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewEmployerContributionsColumnContribMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW employer_contributions");
        DB::statement("CREATE VIEW employer_contributions AS SELECT r.employer_id,
         e.doc,
         e.name       AS employer_name,
         e.reg_no     AS employer_reg_no,
         code.amount  AS contrib_amount,
         r.amount     AS receipt_amount,
         to_date(concat_ws('-', date_part('year' :: text, contrib_month), date_part('month' :: text, contrib_month), '28'), 'YYYY-MM-DD') as contrib_month,
         r.rct_date,
         r.created_at AS receipt_created_at,
         r.rctno
  FROM ((main.receipt_codes code
      JOIN main.receipts r ON ((code.receipt_id = r.id)))
      JOIN main.employers e ON ((r.employer_id = e.id)))
  WHERE ((r.iscancelled = 0) AND (r.isdishonoured = 0) AND (r.deleted_at IS NULL) AND
         ((code.fin_code_id = 2) OR (code.fin_code_id = 63)))
  ORDER BY e.id, code.contrib_month;
");


        //create receonciliations
        DB::statement("CREATE VIEW employer_contribution_reconciliations AS select   c.employer_id,  c.contrib_month,(select sum(curr.contrib_amount) from employer_contributions curr where curr.employer_id = c.employer_id and
  date_part('year' :: text, curr.contrib_month) = date_part('year' :: text, c.contrib_month) and
   date_part('month' :: text, curr.contrib_month) = date_part('month' :: text, c.contrib_month )) as current_amount, (select sum(prev.contrib_amount) from employer_contributions prev where prev.employer_id = c.employer_id and
  date_part('year' :: text, prev.contrib_month) = date_part('year' :: text, c.contrib_month + INTERVAL '-1 month' ) and
   date_part('month' :: text, prev.contrib_month) = date_part('month' :: text, c.contrib_month + INTERVAL '-1 month' )
  ) as prev_amount from employer_contributions c
GROUP BY c.employer_id, c.contrib_month;
");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
