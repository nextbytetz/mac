<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDocumentPayrollBeneficiaryTableIspending extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('document_payroll_beneficiary', function(Blueprint $table)
        {
            $table->smallInteger('ispending')->default(1);
        });

        DB::statement("comment on column document_payroll_beneficiary.ispending is 'SHow whether this document is pending for action (initiate update approval of beneficiary) i.e. 1 => pending, 0 => already worked on'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
