<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentOnStatusColumnInNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column wf_tracks.status is 'status of the track, 0 - Pending for action, 1 - Recommended/Approved, 2 - Rejected to previous level, 3 - Declined/Approval Denied'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
