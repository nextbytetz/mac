<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDocumentPayrollBeneficiary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('document_payroll_beneficiary', function(Blueprint $table)
        {
            $table->dateTime('document_date')->nullable();
            $table->dateTime('receive_date')->nullable();
            $table->dateTime('document_create_date')->nullable();
            $table->string('folionumber')->nullable();
        });

        DB::statement("comment on column document_payroll_beneficiary.document_date is 'Date on the document'");
        DB::statement("comment on column document_payroll_beneficiary.receive_date is 'Date document was received at WCF'");
        DB::statement("comment on column document_payroll_beneficiary.document_create_date is 'Date document was uploaded on E-office'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
