<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployerCompliance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('contributing_category_employer', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_id');
            $table->integer('contributing_category_id');
            $table->integer('previous_contributing_category_id')->nullable();
            $table->smallInteger('isasigned')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table contributing_category_employer is 'Table to keep track of employer relation to contribution category'");
        DB::statement("comment on column contributing_category_employer.previous_contributing_category_id is 'Column to keep track of previous category id of employer.'");

        Schema::table('contributing_category_employer', function(Blueprint $table)
        {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('contributing_category_id')->references('id')->on('employer_contributing_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('previous_contributing_category_id')->references('id')->on('employer_contributing_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        Schema::create('employer_compliance_officer', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('employer_id');
            $table->integer('user_id');
            $table->integer('contributing_category_id')->nullable();
            $table->smallInteger('isactive')->default(0);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('assigned_by')->nullable();
            $table->integer('issystem')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('employer_compliance_officer', function(Blueprint $table)
        {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('contributing_category_id')->references('id')->on('employer_contributing_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('assigned_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on table employer_compliance_officer is 'Table to keep track of employer relation to compliance officers'");
        DB::statement("comment on column employer_compliance_officer.assigned_by is 'User who assigned employer to this compliance officer.'");
        DB::statement("comment on column employer_compliance_officer.issystem is 'Flag to specify if officer was assigned by user or system i.e. 1 => system , 0 => user'");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
