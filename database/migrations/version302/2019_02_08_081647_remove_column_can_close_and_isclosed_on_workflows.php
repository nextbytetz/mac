<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnCanCloseAndIsclosedOnWorkflows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->dropColumn("can_close");
        });
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropColumn("isclosed");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
