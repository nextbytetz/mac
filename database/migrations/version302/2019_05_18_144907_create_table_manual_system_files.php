<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManualSystemFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('manual_system_files', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('notification_report_id');
            $table->dateTime('date_of_mmi')->nullable();
            $table->decimal('pd', 5,2)->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table manual_system_files is 'Table to keep track of all system notification which started from the system but processed manually'");

        Schema::table('manual_system_files', function(Blueprint $table)
        {
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
