<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBenefitTypeIdPayrollRunVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->integer('benefit_type_id')->nullable();
        });


        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->foreign('benefit_type_id')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
