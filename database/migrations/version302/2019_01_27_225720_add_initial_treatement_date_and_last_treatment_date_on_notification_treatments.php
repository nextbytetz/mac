<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInitialTreatementDateAndLastTreatmentDateOnNotificationTreatments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diseases', function (Blueprint $table) {
            $table->dropColumn("initial_treatment_date");
            $table->dropColumn("last_treatment_date");
        });
        Schema::table('accidents', function (Blueprint $table) {
            $table->dropColumn("initial_treatment_date");
            $table->dropColumn("last_treatment_date");
        });
        Schema::table('notification_treatments', function (Blueprint $table) {
            $table->date("initial_treatment_date")->nullable();
            $table->date("last_treatment_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
