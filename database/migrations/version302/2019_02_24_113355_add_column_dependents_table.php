<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependents', function (Blueprint $table) {
            $table->smallInteger("isdisabled")->default(0);
            $table->smallInteger("iseducation")->default(0);
        });
        DB::statement("comment on column dependents.isdisabled is 'Flag to imply if child dependent is disabled who granted to receive pension'");
        DB::statement("comment on column dependents.iseducation is 'Flag to imply if child dependent above 18yrs is granted education benefit where he/she  will receive pension until finishes education'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
