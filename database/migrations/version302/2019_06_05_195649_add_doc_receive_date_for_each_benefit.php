<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocReceiveDateForEachBenefit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL

-- Insert for mae
update incident_mae set mae_refund_member_ready_date = vv.receive_date from  (select b.id, coalesce(max(a.doc_receive_date), max(a.created_at::date)) receive_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where benefit_type_id in (select id from benefit_types where benefit_type_claim_id = 1)) and b.isprogressive = 1 group by b.id) vv where vv.id = incident_mae.notification_report_id;

-- Insert for td
update incident_tds set td_refund_ready_date = vv.receive_date from  (select b.id, coalesce(max(a.doc_receive_date), max(a.created_at::date)) receive_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where benefit_type_id in (select id from benefit_types where benefit_type_claim_id = 3)) and b.isprogressive = 1 group by b.id) vv where vv.id = incident_tds.notification_report_id and incident_tds.td_refund_ready = 1;

-- Insert for td_refund
update incident_tds set td_ready_date = vv.receive_date from  (select b.id, coalesce(max(a.doc_receive_date), max(a.created_at::date)) receive_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where benefit_type_id in (select id from benefit_types where benefit_type_claim_id = 3)) and b.isprogressive = 1 group by b.id) vv where vv.id = incident_tds.notification_report_id and incident_tds.td_ready = 1;

-- Insert for pd
 update notification_reports set pd_ready_date = vv.receive_date from  (select b.id, coalesce(max(a.doc_receive_date), max(a.created_at::date)) receive_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where benefit_type_id in (select id from benefit_types where benefit_type_claim_id = 5)) and b.isprogressive = 1 and b.pd_ready = 1 group by b.id) vv where vv.id = notification_reports.id and pd_ready = 1;
 
 -- Insert for survivor
 update notification_reports set survivor_ready_date = vv.receive_date from  (select b.id, coalesce(max(a.doc_receive_date), max(a.created_at::date)) receive_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where benefit_type_id in (select id from benefit_types where benefit_type_claim_id = 7)) and b.isprogressive = 1 and b.survivor_ready = 1 group by b.id) vv where vv.id = notification_reports.id and survivor_ready = 1;

-- Insert to close for already initiated benefit
-- Update for MAE
update incident_mae set notification_eligible_benefit_id = vv.eligible_id from (select a.id eligible_id, a.notification_report_id from notification_eligible_benefits a join incident_mae b on a.notification_report_id = b.notification_report_id and a.benefit_type_claim_id = 1 and a.parent_id is null) vv where vv.notification_report_id = incident_mae.notification_report_id;
update notification_reports set incident_mae_id = NULL from (select a.id eligible_id, a.notification_report_id from notification_eligible_benefits a join incident_mae b on a.notification_report_id = b.notification_report_id and a.benefit_type_claim_id = 1 and a.parent_id is null) vv where vv.notification_report_id = notification_reports.id;
-- Update for TD
update incident_tds set notification_eligible_benefit_id = vv.eligible_id from (select a.id eligible_id, a.notification_report_id from notification_eligible_benefits a join incident_tds b on a.notification_report_id = b.notification_report_id and a.benefit_type_claim_id in (3,4) and a.parent_id is null) vv where vv.notification_report_id = incident_tds.notification_report_id;
update notification_reports set incident_td_id = NULL from (select a.id eligible_id, a.notification_report_id from notification_eligible_benefits a join incident_tds b on a.notification_report_id = b.notification_report_id and a.benefit_type_claim_id in (3,4) and a.parent_id is null) vv where vv.notification_report_id = notification_reports.id;



SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
