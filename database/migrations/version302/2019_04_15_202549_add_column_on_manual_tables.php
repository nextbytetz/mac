<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnManualTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Beneficiaries table
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->integer('manual_notification_report_id')->nullable();
        });

        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->integer('manual_notification_report_id')->nullable();
        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

// Payroll tables

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->integer('manual_notification_report_id')->nullable();
        });

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->integer('notification_report_id')->nullable()->change();
            $table->integer('manual_notification_report_id')->nullable();
        });

        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
