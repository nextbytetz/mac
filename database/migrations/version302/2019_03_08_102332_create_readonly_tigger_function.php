<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadonlyTiggerFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
$sql = <<<SQL
create or replace function readonly_trigger_function() returns trigger as $$
begin
  raise exception 'The "%" table is read only!', TG_TABLE_NAME using hint = 'Look at tables that inherit from this table and write to them instead.';
  return null;
end;
$$ language 'plpgsql';
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
