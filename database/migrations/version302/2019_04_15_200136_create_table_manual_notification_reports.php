<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManualNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('manual_notification_reports', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('employee_name')->nullable();
            $table->smallInteger('incident_type_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->integer('employer_id')->nullable();
            $table->integer('case_no');
            $table->dateTime('incident_date')->nullable();
            $table->dateTime('reporting_date')->nullable();
            $table->dateTime('notification_date')->nullable();
            $table->dateTime('date_of_mmi')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table manual_notification_reports is 'Table to keep track of all notification registered manually before Mac system'");
        DB::statement("comment on column manual_notification_reports.employee_name is 'Employee name as used on manual file before synchronize'");

        Schema::table('manual_notification_reports', function(Blueprint $table)
        {
            $table->foreign('incident_type_id')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
