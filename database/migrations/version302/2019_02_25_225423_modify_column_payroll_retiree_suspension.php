<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnPayrollRetireeSuspension extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_retiree_suspensions', function(Blueprint $table)
        {
            $table->integer('pensioner_id');
            $table->dropColumn('dependent_id');

        });

               Schema::table('payroll_retiree_suspensions', function(Blueprint $table)
        {
            $table->foreign('pensioner_id')->references('id')->on('pensioners')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
