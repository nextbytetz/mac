<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeEmployeeEmployerNullableOnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("drop view if exists missing_document_death;");
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->bigInteger("employee_id")->nullable()->change();
        });
        $sql = <<<SQL
CREATE VIEW missing_document_death AS SELECT
    n.filename AS notificationno,
    n.incident_date,
    n.reporting_date,
    n.receipt_date,
    e.firstname,
    e.middlename,
    e.lastname,
    r.name     AS employername,
    re.name    AS regionname,
    d.document_id,
    v.name,
    n.incident_type_id,
    t.name     AS incident_type,
    n.investigation_validity,
    n.wf_done_date,
    n.id
  FROM ((((((main.document_notification_report d
    JOIN main.notification_reports n ON ((d.notification_report_id = n.id)))
    JOIN main.documents v ON ((d.document_id = v.id)))
    JOIN main.employees e ON ((n.employee_id = e.id)))
    JOIN main.employers r ON ((n.employer_id = r.id)))
    JOIN main.regions re ON ((r.region_id = re.id)))
    JOIN main.incident_types t ON ((n.incident_type_id = t.id)))
  WHERE (n.incident_type_id = 3)
  ORDER BY d.document_id;
SQL;
        DB::unprepared($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
