<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManualPayrollBeneficiaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('manual_payroll_members', function(Blueprint $table)
        {
            $table->increments('id');
            $table->smallInteger('member_type_id');
            $table->string('name');
            $table->integer('case_no');
            $table->dateTime('dob');
            $table->decimal('monthly_pension', 14);
            $table->string('bank_name',100)->nullable();
            $table->smallInteger('bank_id')->nullable();
            $table->string('accountno', 50)->nullable();
            $table->smallInteger('issynced')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table manual_payroll_members is 'Table to keep track of all payroll members (survivors, employee) processed manually i.e. do not exist on MAC'");

        Schema::table('manual_payroll_members', function(Blueprint $table)
        {
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
         });


/*add userid column*/
        Schema::table('manual_payroll_members', function(Blueprint $table)
        {

            $table->integer('user_id');

        });

        Schema::table('manual_payroll_members', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


        /*bank id on pensioners and dependent*/
        Schema::table('pensioners', function(Blueprint $table)
        {
             $table->smallInteger('bank_id')->nullable();
        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->smallInteger('bank_id')->nullable();
        });

        Schema::table('dependents', function(Blueprint $table)
        {
            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
