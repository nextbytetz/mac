<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsPayrollVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_verifications', function(Blueprint $table)
        {
            $table->dropColumn(['identity_id', 'id_no']);
            $table->smallInteger('iseducation')->default(0);
        });
        DB::statement("comment on column payroll_verifications.iseducation is 'SHow whether child dependent still attending school'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
