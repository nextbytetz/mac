<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSurvivorAndMaeReady extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->date("survivor_ready_date")->nullable();
            $table->date("survivor_pay_date")->nullable();
        });
        Schema::table('document_notification_report', function(Blueprint $table)
        {
            $table->smallInteger("isreferenced")->default(0);
        });
        DB::statement("comment on column document_notification_report.isreferenced is 'show whether document has already been referenced for a specific benefit or not. 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
