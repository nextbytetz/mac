<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProgressiveStageOnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->smallInteger("progressive_stage")->default(1);
        });
        DB::statement("comment on column notification_reports.progressive_stage is 'show the mandatory stages which a notification has already gone. 1 - Checklist, 2 - Physical Investigation, 3 - Approval, 4 - Benefit Process'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
