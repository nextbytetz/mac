<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTreasuryFundingSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treasury_funding_summary', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('total_votes');
          $table->integer('total_employees');
          $table->decimal('total_grosspay',14);
          $table->decimal('total_contribution_amount',14);
          $table->string('funding_source');
          $table->integer('treasury_contribution_summary_id')->unsigned();
          $table->date('check_date');
          $table->integer('bill_id')->unsigned()->nullable();
          $table->timestamps();
          $table->foreign('treasury_contribution_summary_id')->references('id')->on('main.treasury_contribution_summary')->onUpdate('CASCADE')->onDelete('RESTRICT');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('treasury_funding_summary');

   }
}
