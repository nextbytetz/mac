<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewAllBookingsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("CREATE VIEW  booking_payment_date  AS   select b.id, COALESCE((SELECT min(receipts.rct_date) AS payment_date
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id))),
                  (SELECT min(receipts.rct_date) AS sum
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (date_part('month' :: text, b.rcv_date) =
                                                             date_part('month' :: text, receipt_codes.contrib_month)) AND
                          (date_part('year' :: text, b.rcv_date) =
                           date_part('year' :: text, receipt_codes.contrib_month)) AND
                          (receipts.employer_id = b.employer_id)))) as first_payment_date  from bookings b
  WHERE (b.deleted_at IS NULL);

");

//        DB::statement("DROP VIEW bookings_view CASCADE");
        DB::statement("CREATE VIEW  bookings_view  AS     SELECT b.employer_id,
         b.id                                                       AS booking_id,
         e.name                                                     AS employer_name,
         e.reg_no                                                   AS employer_reg_no,
         b.rcv_date,
         b.amount                                                   AS booked_amount,
         COALESCE((SELECT sum(receipt_codes.amount) AS sum
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id = b.id))),
                  (SELECT sum(receipt_codes.amount) AS sum
                   FROM (main.receipt_codes
                       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
                   WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND
                          (receipts.iscancelled = 0) AND (receipts.isdishonoured = 0) AND
                          (receipts.deleted_at IS NULL) AND (date_part('month' :: text, b.rcv_date) =
                                                             date_part('month' :: text, receipt_codes.contrib_month)) AND
                          (date_part('year' :: text, b.rcv_date) =
                           date_part('year' :: text, receipt_codes.contrib_month)) AND
                          (receipts.employer_id = b.employer_id)))) AS amount_paid,
           (((date_part('year' :: text, (coalesce(p.first_payment_date, now())) :: timestamp without time zone) - date_part('year' :: text, b.rcv_date)) *
           (12) :: double precision) + ((date_part('month' :: text, (coalesce(p.first_payment_date, now())) :: timestamp without time zone) -
                                         date_part('month' :: text, b.rcv_date)) -
                                        (1) :: double precision))                                          AS age_months,
         date_part('day' :: text,
                   ((coalesce(p.first_payment_date, now())) :: timestamp without time zone - (b.rcv_date) :: timestamp without time zone)) AS age_days,
  p.first_payment_date,
         b.ispaid
  FROM (main.bookings b
      JOIN main.employers e ON ((b.employer_id = e.id)))
  JOIN booking_payment_date p on p.id = b.id
  WHERE (b.deleted_at IS NULL);
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
