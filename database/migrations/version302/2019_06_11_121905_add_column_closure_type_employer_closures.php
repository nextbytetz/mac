<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnClosureTypeEmployerClosures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->smallInteger('closure_type')->nullable();

        });

        DB::statement("comment on column employer_closures.closure_type is 'Flag to specify if business is closed temporary or permanent i.e. 1 =? temporary, 2 => Permanent'");


        /*Update existing data*/
        /*Temporary*/
        \Illuminate\Support\Facades\DB::table('employer_closures')->where('reason', '~*', 'temp')->update([
            'closure_type' => 1
        ]);
        /*Permanent*/
        \Illuminate\Support\Facades\DB::table('employer_closures')->where('reason', '~*', 'perm')->update([
            'closure_type' => 2
        ]);
        /*Others set - temporary*/
        \Illuminate\Support\Facades\DB::table('employer_closures')->whereNull('closure_type')->update([
            'closure_type' => 1
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
