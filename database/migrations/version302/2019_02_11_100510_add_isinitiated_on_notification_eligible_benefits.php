<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsinitiatedOnNotificationEligibleBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            $table->smallInteger("isinitiated")->default(0);
        });
        DB::statement("comment on column notification_eligible_benefits.isinitiated is 'whether notification approval workflow has been initiated for this benefit or not, 1 - Yes, 0 - Not Yet'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
