<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollRunsAndApprovals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payroll_run_approvals', function(Blueprint $table)
        {
            $table->integer('no_of_constant_cares')->default(0);
            $table->integer('no_of_suspended_constant_cares')->default(0);
        });

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->smallInteger('isconstantcare')->default(0);
        });
        DB::statement("comment on column payroll_runs.isconstantcare is 'Flag to specify if beneficiary is constant care assistant i.e. 1 => constant care , 0 => not '");

        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->smallInteger('isconstantcare')->default(0);
        });
        DB::statement("comment on column payroll_suspended_runs.isconstantcare is 'Flag to specify if beneficiary is constant care assistant i.e. 1 => constant care , 0 => not '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
