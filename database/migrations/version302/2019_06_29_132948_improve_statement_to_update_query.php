<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImproveStatementToUpdateQuery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*reset flag*/
        DB::statement(" update employers set is_treasury = false ");
        /*update flag*/
        DB::statement(" update employers set is_treasury = true where id in (select employer_id from legacy_receipts where rctno = '700158148') and employer_category_cv_id = 36 ");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
