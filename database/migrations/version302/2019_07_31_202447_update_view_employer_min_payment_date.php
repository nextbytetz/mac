<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewEmployerMinPaymentDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW employers_min_payment_date");
        DB::statement("CREATE VIEW employers_min_payment_date AS SELECT e.employer_id, (CASE WHEN min(COALESCE(l.rct_date, NOW())) < min(COALESCE(r.rct_date, NOW())) THEN min(l.rct_date) ELSE min(r.rct_date) END) AS min_payment_date
  FROM ((main.contributing_employers e
      LEFT JOIN main.legacy_receipts l ON ((l.employer_id = e.employer_id)))
      LEFT JOIN main.receipts r ON ((r.employer_id = e.employer_id)))
  GROUP BY e.employer_id;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
