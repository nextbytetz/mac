<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterilizedViewSummaryAgingAnalysis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("CREATE MATERIALIZED VIEW  summary_contrib_rcvable_age_analysis  AS   select
(select sum(booked_amount) from booking_receivables where age_days <= 30) as receivable_30,
(select count(distinct(employer_id)) from booking_receivables where age_days <= 30) as employers_30,
(select sum(booked_amount) from booking_receivables where age_days >= 31 and age_days <= 90) as receivable_90,
 (select count(distinct(employer_id)) from booking_receivables where age_days >= 31 and age_days <= 90) as employers_90,
(select sum(booked_amount) from booking_receivables where age_days >= 91 and age_days <= 180) as receivable_180,
(select count(distinct(employer_id)) from booking_receivables where age_days >= 31 and age_days <= 90) as employers_180,
(select sum(booked_amount) from booking_receivables where age_days >= 181 and age_days <= 365) as receivable_365,
 (select count(distinct(employer_id)) from booking_receivables where age_days >= 181 and age_days <= 365) as employers_365,
(select sum(booked_amount) from booking_receivables where age_days >= 366) as receivable_366,
(select count(distinct(employer_id)) from booking_receivables where age_days >= 366) as employers_366

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
