<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('date_ranges', function(Blueprint $table)
        {
            $table->smallInteger('date_type')->nullable();
            $table->smallInteger('category')->nullable();;
            $table->smallInteger('office_zone_id')->nullable();;

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
