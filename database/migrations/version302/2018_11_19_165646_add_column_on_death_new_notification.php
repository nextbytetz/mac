<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnDeathNewNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deaths', function(Blueprint $table)
        {
            $table->text("description")->nullable();
            $table->text("activity_performed")->nullable();
            $table->bigInteger("incident_occurrence_cv_id")->nullable();
            $table->bigInteger("health_provider_id")->nullable();
            $table->bigInteger("medical_practitioner_id")->nullable();
            $table->string("certificate_number", 50)->nullable()->change();
            $table->foreign('incident_occurrence_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('health_provider_id')->references('id')->on('health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('medical_practitioner_id')->references('id')->on('medical_practitioners')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column deaths.incident_occurrence_cv_id is 'Incident occurred on premises or not'");
        DB::statement("comment on column deaths.health_provider_id is 'Name of hospital where death was confirmed'");
        DB::statement("comment on column deaths.medical_practitioner_id is 'Name of Medical practitioner who confirmed death'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
