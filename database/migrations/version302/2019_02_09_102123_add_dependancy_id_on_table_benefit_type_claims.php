<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDependancyIdOnTableBenefitTypeClaims extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefit_type_claims', function (Blueprint $table) {
            $table->smallInteger("dependency_id")->nullable();
            $table->foreign('dependency_id')->references('id')->on('benefit_type_claims')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column benefit_type_claims.dependency_id is 'show whether this benefit is assessed via another benefit.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
