<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssessedAmountOnMedicalExpenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->decimal("assessed_amount", 14)->nullable();
        });
        DB::statement("comment on column medical_expenses.assessed_amount is 'amount derived from notification_health_provider_services registered during the assessment of the medical aid expense'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
