<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentPayroll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('document_payroll', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('payroll_run_approval_id');
            $table->integer('document_id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('eoffice_document_id');
            $table->dateTime('document_date')->nullable();
            $table->dateTime('receive_date')->nullable();
            $table->dateTime('document_create_date')->nullable();
            $table->string('folionumber')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table document_payroll is 'Table to keep track of all documents attached to monthly payroll files'");

        DB::statement("comment on column document_payroll.document_date is 'Date on the document'");
        DB::statement("comment on column document_payroll.receive_date is 'Date document was received at WCF'");
        DB::statement("comment on column document_payroll.document_create_date is 'Date document was uploaded on E-office'");

        Schema::table('document_payroll', function(Blueprint $table)
        {
            $table->foreign('payroll_run_approval_id')->references('id')->on('payroll_run_approvals')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        Schema::table('document_payroll_beneficiary', function(Blueprint $table)
        {
            $table->string('name')->nullable()->change();
            $table->text('description')->nullable()->change();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
