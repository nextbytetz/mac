<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllocateStatusOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->smallInteger("allocate_status")->default(1);
        });
        DB::statement("comment on column notification_reports.allocate_status is 'show whether the allocation has been done by system or re-allocated by the executive, 1 - done by system, 0 - re-allocated by the executive'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
