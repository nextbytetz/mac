<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCanCloseOnWfDefinition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function(Blueprint $table)
        {
            $table->smallInteger('can_close')->default(0);
        });
        Schema::table('wf_tracks', function(Blueprint $table)
        {
            $table->smallInteger('isclosed')->default(0);
        });
        DB::statement("comment on column wf_definitions.can_close is 'show whether the level is allowed to close the workflow prematurely'");
        DB::statement("comment on column wf_tracks.isclosed is 'show whether the workflow has been closed prematurely.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
