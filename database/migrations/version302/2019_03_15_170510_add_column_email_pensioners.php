<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmailPensioners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->string('email',45)->nullable();

        });
        $sql = <<<SQL
update pensioners
set email = (
  select employees.email
  from employees
  where employees.id = pensioners.employee_id
)
  where exists (
      select *
      from employees
      where employees.id = pensioners.employee_id
  );
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
