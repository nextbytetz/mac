<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentIsactivePensionersDependents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->smallInteger('isactive')->default(0)->comment('specify whether the pensioners is active to receive monthly pension; 0 - inactive, 1 => Active, 2 => Deactivated')->change();
        });


        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->smallInteger('isactive')->default(0)->comment('specify whether the dependents is active to receive monthly pension; 0 - inactive, 1 => Active, 2 => Deactivated')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
