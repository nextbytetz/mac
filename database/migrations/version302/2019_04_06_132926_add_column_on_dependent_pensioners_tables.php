<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnDependentPensionersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->smallInteger('isdmsposted')->default(0);
        });

        DB::statement("comment on column pensioners.isdmsposted is 'Flag to specify if pensioner is posted to e-office yet, i.e. 1 => posted, 0 => not yet'");

        Schema::table('dependents', function(Blueprint $table)
        {
            $table->smallInteger('isdmsposted')->default(0);
        });

        DB::statement("comment on column dependents.isdmsposted is 'Flag to specify if pensioner is posted to e-office yet, i.e. 1 => posted, 0 => not yet'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
