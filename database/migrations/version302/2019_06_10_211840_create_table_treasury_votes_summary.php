<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTreasuryVotesSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treasury_votes_summary', function (Blueprint $table) {
           $table->increments('id');
           $table->string('vote_code');
           $table->integer('total_employees');
           $table->decimal('total_grosspay',14);
           $table->decimal('total_contribution_amount',14);
           $table->integer('treasury_funding_summary_id')->unsigned();
           $table->date('check_date');
           $table->integer('bill_id')->unsigned()->nullable();
           $table->timestamps();
           $table->foreign('treasury_funding_summary_id')->references('id')->on('main.treasury_funding_summary')->onUpdate('CASCADE')->onDelete('RESTRICT');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('treasury_votes_summary');
 }
}
