<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAlternativeApprovers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_alternatives', function(Blueprint $table)
        {
            $table->bigIncrements("id");
            $table->bigInteger("user_id")->index();
            $table->bigInteger("alternative");
            $table->bigInteger("user_set_id");
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('alternative')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_set_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table user_alternatives is 'table to store users who will be alternative approvers in workflows in case a user is substituting another user'");
        DB::statement("comment on column user_alternatives.user_id is 'user who when substituting other user, other alternative approvers will work on his workflows'");
        DB::statement("comment on column user_alternatives.alternative is 'user who is alternative approver of user who is substituting another user'");
        DB::statement("comment on column user_alternatives.user_set_id is 'user who has applied the alternative approver'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
