<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBenefitReadyFlagsOnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->smallInteger("td_ready")->default(0);
            $table->smallInteger("td_refund_ready")->default(0);
            $table->smallInteger("pd_ready")->default(0);
            $table->smallInteger("survivor_ready")->default(0);
            $table->smallInteger("mae_refund_member_ready")->default(0);
            $table->smallInteger("mae_refund_thirdparty_ready")->default(0);
        });
        DB::statement("comment on column notification_reports.td_ready is 'if Temporary Disablement can be started based on relevant documents submitted, 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.td_refund_ready is 'if Temporary Disablement Refund can be started based on relevant documents submitted, 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.pd_ready is 'if Permanent Disablement can be started based on relevant documents submitted, 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.survivor_ready is 'if Survivor Benefits can be started based on relevant documents submitted, 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.mae_refund_member_ready is 'if MAE Refund for employee/employer can be started based on relevant documents submitted, 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.mae_refund_thirdparty_ready is 'if MAE refund for hcp/hsp can be started based on relevant documents submitted, 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
