<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBodyPartyInjuryIdOnNotificationInjuries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_injuries', function (Blueprint $table) {
            $table->smallInteger("body_part_injury_id")->nullable();
            $table->bigInteger("body_part_injury_cv_id")->nullable()->change();
            $table->foreign('body_part_injury_id')->references('id')->on('body_part_injuries')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('notification_lost_body_parts', function (Blueprint $table) {
            $table->smallInteger("body_part_injury_id")->nullable();
            $table->bigInteger("lost_body_part_cv_id")->nullable()->change();
            $table->foreign('body_part_injury_id')->references('id')->on('body_part_injuries')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
