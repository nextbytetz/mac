<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBodyPartInjuriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_part_injury_groups', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->string("name", 100);
            $table->timestamps();
        });

        Schema::create('body_part_injuries', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->smallInteger("body_part_injury_group_id");
            $table->string("name", 100);
            $table->timestamps();
            $table->foreign('body_part_injury_group_id')->references('id')->on('body_part_injury_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::create('occupations', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->smallInteger("groupno");
            $table->string("name", 200);
            $table->string("industry", 100);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
