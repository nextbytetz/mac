<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComplianceTableContributionEmloyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*Employer categories*/
        Schema::create('employer_categories', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->decimal('min_contribution', 14, 2);
            $table->decimal('max_contribution', 14, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table employer_categories is 'Table to keep track of employer contribution categories'");
        DB::statement("comment on column employer_categories.min_contribution is 'Average minimum contribution amount contributed by employer'");
        DB::statement("comment on column employer_categories.max_contribution is 'Average maximum contribution amount contributed by employer'");
        /*office zones*/

        Schema::create('office_zones', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
                  $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table office_zones is 'Table to keep track of all wcf office zones'");
        /*add office zone id into regions*/

        Schema::table('regions', function(Blueprint $table)
        {
            $table->smallInteger('office_zone_id')->nullable();
        });

        Schema::table('regions', function(Blueprint $table)
        {
            $table->foreign('office_zone_id')->references('id')->on('office_zones')->onUpdate('CASCADE')->onDelete('RESTRICT');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
