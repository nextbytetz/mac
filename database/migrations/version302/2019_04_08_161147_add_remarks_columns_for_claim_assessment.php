<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarksColumnsForClaimAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mae_subs', function(Blueprint $table)
        {
            $table->string("remarks")->nullable();
        });
        DB::statement("comment on column mae_subs.remarks is 'additional comments for in case the registered amount is not the same as assessed amount.'");

        Schema::table('notification_disability_state_assessments', function(Blueprint $table)
        {
            $table->string("remarks")->nullable();
        });
        DB::statement("comment on column notification_disability_state_assessments.remarks is 'additional comments for in case the registered days is not the same as assessed days.'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
