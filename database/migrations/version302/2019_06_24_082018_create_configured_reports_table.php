<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguredReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurable_reports', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->string("name", 200);
            $table->string("description", 200);
            $table->text("data_query");
            $table->json("filter");
            $table->smallInteger("unit_id")->nullable();
            $table->timestamps();
        });

        DB::statement("comment on table configurable_reports is 'store queries for reports with specified configurations'");
        DB::statement("comment on column configurable_reports.filter is 'specified configurations for the registered query'");
        DB::statement("comment on column configurable_reports.data_query is 'sql query for the report'");
        DB::statement("comment on column configurable_reports.description is 'long about the report'");
        DB::statement("comment on column configurable_reports.name is 'short about the report'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
