<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEligibleIdOnStateAndDisabilityInNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_health_states', function (Blueprint $table) {
            $table->bigInteger("notification_eligible_benefit_id")->nullable()->index();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('notification_disability_states', function (Blueprint $table) {
            $table->bigInteger("notification_eligible_benefit_id")->nullable()->index();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
