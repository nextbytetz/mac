<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerOnlineProfileMaterializedView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $sql = <<<SQL

         /*online employers contribution arrears*/
      create materialized view online_contribution_arrears as select id as arrear_id,employer_id,contrib_month,EXTRACT(YEAR FROM AGE(current_date,contrib_month)) * 12 +
              EXTRACT(MONTH FROM AGE(current_date,contrib_month)) as delayed_month,payroll_id,status from 
              portal.contribution_arrears where status=0;

  /*Online employer*/



SQL;

        DB::unprepared($sql);  


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
