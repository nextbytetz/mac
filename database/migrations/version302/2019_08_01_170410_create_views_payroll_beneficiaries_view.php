<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewsPayrollBeneficiariesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
//                DB::statement("DROP VIEW payroll_runs_view");
//        DB::statement("DROP VIEW payroll_beneficiaries_view");
        DB::statement("CREATE VIEW payroll_beneficiaries_view AS select p.id as resource_id, 5 as member_type_id, CASE WHEN p.notification_report_id is not null THEN n.filename ELSE cast(m2.case_no as varchar(20)) END as filename,
concat_ws(' ', p.firstname, coalesce(p.middlename, ''), p.lastname) as member_name, p.bank_id, p.bank_branch_id, p.notification_report_id, p.manual_notification_report_id, p.isactive, null as pivot_id
from pensioners p
left join notification_reports n on p.notification_report_id = n.id
left join manual_notification_reports m2 on p.manual_notification_report_id = m2.id
union
select d.id as resource_id, 4 as member_type_id, CASE WHEN de.notification_report_id is not null THEN n.filename ELSE cast(m2.case_no as varchar(20)) END as filename,
concat_ws(' ', d.firstname, coalesce(d.middlename, ''), d.lastname) as member_name, d.bank_id, d.bank_branch_id, de.notification_report_id, de.manual_notification_report_id, de.isactive, de.id as pivot_id
from dependent_employee de
join dependents d on d.id = de.dependent_id
left join notification_reports n on de.notification_report_id = n.id
left join manual_notification_reports m2 on de.manual_notification_report_id = m2.id
");


        //Payroll_runs_views

        DB::statement("CREATE VIEW payroll_runs_view AS 
select payroll_beneficiaries_view.member_name,
       payroll_runs.amount as amount,
       payroll_runs.months_paid,
       payroll_runs.monthly_pension,
       payroll_runs.payroll_run_approval_id ,
       payroll_run_approvals.wf_done,
       payroll_runs.arrears_amount,
       payroll_runs.deductions_amount,
       payroll_runs.unclaimed_amount,
       payroll_runs.employee_id,
       member_types.name as member_type_name,
       payroll_procs.run_date as run_date,
       bank_branches.name as bank_branch_name,
       banks.name as bank_name,
       payroll_runs.accountno as run_accountno,
       payroll_runs.bank_branch_id as run_bank_branch_id,
       payroll_runs.bank_id as run_bank_id,
       payroll_runs.resource_id as resource_id,
       payroll_runs.member_type_id as member_type_id,
       payroll_runs.new_payee_flag as new_payee_flag,
           payroll_runs.isconstantcare as isconstantcare,
       payroll_beneficiaries_view.filename

from payroll_runs
       join payroll_run_approvals on payroll_runs.payroll_run_approval_id = payroll_run_approvals.id
       join payroll_procs on payroll_run_approvals.payroll_proc_id = payroll_procs.id
       join member_types on member_types.id = payroll_runs.member_type_id
       join payroll_beneficiaries_view on payroll_beneficiaries_view.resource_id = payroll_runs.resource_id and payroll_beneficiaries_view.member_type_id = payroll_runs.member_type_id
       left join bank_branches on bank_branches.id = payroll_runs.bank_branch_id
       left join banks on banks.id = payroll_runs.bank_id;
");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
