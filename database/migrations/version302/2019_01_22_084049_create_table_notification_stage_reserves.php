<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificationStageReserves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->bigInteger("notification_staging_reserve_cv_id")->nullable();
            $table->foreign('notification_staging_reserve_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_reports.notification_staging_reserve_cv_id is 'column to reserve the previous notification stage before the interrupting stage, and finally used to restore the reserved stage after the interrupting stage is complete'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
