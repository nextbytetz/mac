<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerAdvancePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_advance_payments', function(Blueprint $table)
        {
            $table->increments("id");
            $table->bigInteger("employer_id")->index();
            $table->date("start_month");
            $table->smallInteger("period");
            $table->smallInteger("employee_count")->nullable();
            $table->decimal("amount", 14);
            $table->bigInteger("receipt_id")->index()->nullable();
            $table->integer("payroll_id")->nullable();
            $table->bigInteger("user_id")->index();
            $table->smallInteger("wf_done")->default(0);
            $table->date("wf_done_date")->nullable();
            $table->timestamps();
        });
        DB::statement("comment on column employer_advance_payments.start_month is 'the first month paid'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
