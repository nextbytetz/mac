<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollRecoveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_recoveries', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('resource_id');
            $table->integer('member_type_id');
            $table->integer('notification_report_id');
            $table->integer('recovery_type_cv_id');
            $table->text('remark');
            $table->decimal('total_amount', 14);
            $table->smallInteger('cycles')->default(1);
            $table->integer('user_id');
            $table->smallInteger('wf_done')->default(0);
            $table->dateTime('wf_done_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_recoveries is 'Table to keep track of all payment recoveries captured for payroll beneficiaries i.e. overpayment, underpayment, unclaimed'");
        DB::statement("comment on column payroll_recoveries.resource_id is 'Primary key of the resource table'");
        DB::statement("comment on column payroll_recoveries.total_amount is 'Total amount to be paid'");
        DB::statement("comment on column payroll_recoveries.cycles is 'Total number of cycles specified to be paid to beneficiary'");

        Schema::table('payroll_recoveries', function(Blueprint $table)
        {

            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('recovery_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
