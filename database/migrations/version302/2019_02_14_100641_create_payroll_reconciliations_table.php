<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollReconciliationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        Schema::create('payroll_reconciliations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('resource_id');
            $table->integer('member_type_id');
            $table->integer('notification_report_id');
            $table->text('remark');
            $table->decimal('amount', 14);
                  $table->smallInteger('type');
            $table->smallInteger('status')->default(0);
            $table->smallInteger('wf_done')->default(0);
            $table->dateTime('wf_done_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_reconciliations is 'Table to keep track of all beneficiary need to be reconciled'");
        DB::statement("comment on column payroll_reconciliations.type is 'Flag to specify if entry is allowances or deductions i.e. 1 => allowances/arrears 2 => Deductions'");
        DB::statement("comment on column payroll_reconciliations.status is 'Flag to specify status of the entry i.e. 0 - pending, 1 => initiated, 2 - Rejected'");

        Schema::table('payroll_reconciliations', function(Blueprint $table)
        {
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
                  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
