<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcedureCheckForeignReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create or replace function check_foreign_reference(table_ character varying, attribute_ character varying, value_ bigint) returns table(status smallint)
LANGUAGE plpgsql
as $$
declare t record;
declare return_ smallint;
declare rcount smallint;
begin
    return_ := 0;
    for t in select R.TABLE_NAME, R.column_name, r.constraint_schema from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE u inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS FK on U.CONSTRAINT_CATALOG = FK.UNIQUE_CONSTRAINT_CATALOG and U.CONSTRAINT_SCHEMA = FK.UNIQUE_CONSTRAINT_SCHEMA and U.CONSTRAINT_NAME = FK.UNIQUE_CONSTRAINT_NAME inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE R ON R.CONSTRAINT_CATALOG = FK.CONSTRAINT_CATALOG AND R.CONSTRAINT_SCHEMA = FK.CONSTRAINT_SCHEMA AND R.CONSTRAINT_NAME = FK.CONSTRAINT_NAME WHERE U.COLUMN_NAME = attribute_ AND U.TABLE_CATALOG = current_database() AND U.TABLE_SCHEMA in ('portal', 'main') AND U.TABLE_NAME = table_
        loop
            execute 'select 1 from ' || t.constraint_schema || '.' || t.table_name || ' where ' || t.column_name || ' = ' || value_ || ' limit 1';
            get diagnostics rcount := ROW_COUNT;
            if (rcount > 0) then
                return_ := 1;
                exit;
            end if;
        end loop;
    return query select return_ as status;
end$$;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
