<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingReceivablesViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW booking_receivables");
        DB::statement("CREATE VIEW booking_receivables AS       SELECT
    b.employer_id,
    b.id     AS booking_id,
    e.name   AS employer_name,
    e.reg_no AS employer_reg_no,
    b.rcv_date,
    b.amount AS booked_amount,
    0        AS amount_paid
  FROM (main.bookings b
    JOIN main.employers e ON ((b.employer_id = e.id)))
  WHERE ((b.deleted_at IS NULL) AND (b.ispaid = 0) AND (e.is_treasury = false) AND (e.duplicate_id is null) AND (e.approval_id = 1) AND (e.deleted_at is null))
ORDER BY b.employer_id, b.rcv_date;
");

        //update contributing employers view
        DB::statement("DROP VIEW contributing_employers");
        DB::statement("CREATE VIEW contributing_employers AS
   SELECT DISTINCT employers.id AS employer_id
  FROM ((main.employers
    LEFT JOIN main.receipts ON ((employers.id = receipts.employer_id)))
    LEFT JOIN main.legacy_receipts ON ((employers.id = legacy_receipts.employer_id)))
  WHERE (((receipts.id IS NOT NULL) OR (legacy_receipts.id IS NOT NULL)) AND (employers.duplicate_id IS NULL) AND
         (employers.approval_id = 1) AND (employers.deleted_at IS NULL) AND ((receipts.deleted_at IS NULL) OR
         (legacy_receipts.deleted_at IS NULL)) AND ((receipts.iscancelled = 0) OR (legacy_receipts.iscancelled = 0)));

");

        //Legacy contributors
        DB::statement("CREATE VIEW legacy_contributing_employers AS
SELECT DISTINCT employers.id AS employer_id
  FROM main.employers
     JOIN main.legacy_receipts ON (employers.id = legacy_receipts.employer_id)
  WHERE ((employers.duplicate_id IS NULL) AND
         (employers.approval_id = 1) AND (employers.deleted_at IS NULL)  AND
         (legacy_receipts.deleted_at IS NULL) AND (legacy_receipts.iscancelled = 0));

");


        //Booking receivables for contributing employers

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
