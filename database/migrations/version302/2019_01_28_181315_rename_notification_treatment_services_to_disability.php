<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameNotificationTreatmentServicesToDisability extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename("notification_treatment_services", "notification_treatment_disabilities");
        Schema::table('notification_treatment_disabilities', function (Blueprint $table) {
            $table->dropColumn("health_service_checklist_id");
            $table->bigInteger("disability_state_checklist_id");
            $table->foreign('disability_state_checklist_id')->references('id')->on('disability_state_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
