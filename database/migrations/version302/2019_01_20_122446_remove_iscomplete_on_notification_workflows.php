<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveIscompleteOnNotificationWorkflows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //shows whether the workflow completed its round trip or not. 1 - Yes, 0 - No, 2 - Partially completed
        Schema::table('notification_workflows', function (Blueprint $table) {
            $table->dropColumn("iscomplete");
        });
        DB::statement("comment on column notification_workflows.wf_done is 'shows whether the workflow completed its round trip or not. 1 - Yes, 0 - No, 2 - Partially completed'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
