<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePyrollRunVouchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payroll_run_approval_id');
            $table->integer('bank_id');
            $table->decimal('amount', 14);
            $table->integer('user_id');
            $table->smallInteger('is_exported')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_run_vouchers is 'Table to keep track of all payment vouchers for monthly pension payroll'");



        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
            $table->unique(['bank_id', 'payroll_run_approval_id']);
            $table->foreign('payroll_run_approval_id')->references('id')->on('payroll_run_approvals')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
