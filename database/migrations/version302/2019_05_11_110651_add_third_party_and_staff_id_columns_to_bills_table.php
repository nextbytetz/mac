<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThirdPartyAndStaffIdColumnsToBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal.bills', function (Blueprint $table) {
            //
            $table->integer('staff_id')->nullable();
            $table->integer('third_party_id')->nullable();
            $table->integer('gfs_code')->default(120102);
            $table->integer('employer_id')->nullable()->change();
            $table->integer('member_count')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.bills', function (Blueprint $table) {
            //


        });
    }
}
