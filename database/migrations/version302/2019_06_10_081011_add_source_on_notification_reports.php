<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourceOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->smallInteger("source")->default(1);
        });
        DB::statement("comment on column notification_reports.source is 'Source of notification registration, 1 - Branch Registered, 2 - Online Registration'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
