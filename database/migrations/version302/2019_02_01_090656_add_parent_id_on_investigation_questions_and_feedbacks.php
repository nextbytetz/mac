<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdOnInvestigationQuestionsAndFeedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_questions', function (Blueprint $table) {
            $table->bigInteger("parent_id")->nullable();
            $table->foreign('parent_id')->references('id')->on('investigation_questions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->bigInteger("parent_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
