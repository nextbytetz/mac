<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferenceToOnlineNotificationsToNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->bigInteger('incident_id')->nullable();
            $table->smallInteger("haspaidmanual")->default(0);
            $table->smallInteger("paidmanual_reserve_flag")->nullable();
            $table->bigInteger("user_set_manual_pay")->nullable();
            $table->decimal("man_mae", 14)->nullable();
            $table->decimal("man_pd", 5)->nullable();
            $table->decimal("man_ppd", 14)->nullable();
            $table->decimal("man_ptd", 14)->nullable();
            $table->decimal("man_ttd", 14)->nullable();
            $table->decimal("man_tpd", 14)->nullable();
            $table->decimal("man_funeral_grant", 14)->nullable();
        });
        Schema::table('configurable_reports', function(Blueprint $table)
        {
            $table->smallInteger("istable")->default(0);
        });
        Schema::table('documents', function(Blueprint $table)
        {
            $table->smallInteger("anysource")->default(0);
        });
        DB::statement("comment on column notification_reports.haspaidmanual is 'specify whether a file has been created through a system but has been processed and paid manually'");
        DB::statement("comment on column notification_reports.incident_id is 'link to the online application registered by employer'");
        DB::statement("comment on column notification_reports.user_set_manual_pay is 'user who has set the file as paid manually'");
        DB::statement("comment on column configurable_reports.istable is 'show whether this report has to be shown as a datatable report with options to download excel in the report list'");
        DB::statement("comment on column documents.anysource is 'specify whether a document can be uploaded from any source, i.e. from internal staff and online members through portal'");

        DB::statement("comment on column notification_reports.man_mae is 'mae amount paid manually'");
        DB::statement("comment on column notification_reports.man_pd is 'percentage disability paid manually'");
        DB::statement("comment on column notification_reports.man_ppd is 'ppd amount paid manually'");
        DB::statement("comment on column notification_reports.man_ptd is 'ptd amount paid manually'");
        DB::statement("comment on column notification_reports.man_ttd is 'ttd amount paid manually'");
        DB::statement("comment on column notification_reports.man_tpd is 'tpd amount paid manually'");
        DB::statement("comment on column notification_reports.man_funeral_grant is 'funeral grant amount paid manually'");

        DB::statement("comment on column notification_reports.paidmanual_reserve_flag is 'reserved stage in case of progressive notification or status in case of non progressive notification when we update manual payment, to be used when undoing manual payments'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
