<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeForSupportingProgressiveNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("drop view if exists missing_documents_claims;");
        DB::statement("drop view if exists missing_document_death;");
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->smallInteger("isprogressive")->default(0);
            $table->bigInteger("employer_id")->nullable()->change();
            $table->bigInteger("notification_staging_cv_id")->nullable();
            $table->string("employee", 100)->nullable();
            $table->string("employer", 100)->nullable();
        });
        DB::statement("CREATE VIEW missing_documents_claims AS SELECT n.filename AS notificationno,n.incident_date,n.reporting_date,n.receipt_date,e.firstname,e.middlename,e.lastname,r.name AS employername,re.name AS regionname,d.document_id,v.name,n.incident_type_id,t.name AS incident_type,n.investigation_validity FROM ((((((main.document_notification_report d JOIN main.notification_reports n ON ((d.notification_report_id = n.id))) JOIN main.documents v ON ((d.document_id = v.id))) JOIN main.employees e ON ((n.employee_id = e.id))) JOIN main.employers r ON ((n.employer_id = r.id))) JOIN main.regions re ON ((r.region_id = re.id))) JOIN main.incident_types t ON ((n.incident_type_id = t.id))) WHERE (n.incident_type_id <> 3) ORDER BY d.document_id;");

        $sql = <<<SQL
CREATE VIEW missing_document_death AS SELECT
    n.filename AS notificationno,
    n.incident_date,
    n.reporting_date,
    n.receipt_date,
    e.firstname,
    e.middlename,
    e.lastname,
    r.name     AS employername,
    re.name    AS regionname,
    d.document_id,
    v.name,
    n.incident_type_id,
    t.name     AS incident_type,
    n.investigation_validity,
    n.wf_done_date,
    n.id
  FROM ((((((main.document_notification_report d
    JOIN main.notification_reports n ON ((d.notification_report_id = n.id)))
    JOIN main.documents v ON ((d.document_id = v.id)))
    JOIN main.employees e ON ((n.employee_id = e.id)))
    JOIN main.employers r ON ((n.employer_id = r.id)))
    JOIN main.regions re ON ((r.region_id = re.id)))
    JOIN main.incident_types t ON ((n.incident_type_id = t.id)))
  WHERE (n.incident_type_id = 3)
  ORDER BY d.document_id;
SQL;
        DB::unprepared($sql);

        DB::statement("comment on column notification_reports.status is 'status for non-progressive notification report with only one workflow, 0 - Pending Incident, 1 - Approved to Claim, 2 - Rejected incident, 3 - Progressive Notification Report'");
        DB::statement("comment on column notification_reports.isprogressive is 'Show whether incident is registered for progressive notification report (with multiple benefit workflows). 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_reports.employee is 'name of the employee in case incident is for unregistered employee'");
        DB::statement("comment on column notification_reports.employer is 'name of the employer in case incident is for unregistered employee'");
        Schema::create('notification_workflows', function(Blueprint $table)
        {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->bigInteger("wf_module_id")->index();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
