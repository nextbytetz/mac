<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnDocumentInvestigation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_investigation', function (Blueprint $table) {
            $table->smallInteger("index")->nullable();
        });
        DB::statement("comment on column document_investigation.index is 'the zero-based sequential index of the loaded file in the preview list '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
