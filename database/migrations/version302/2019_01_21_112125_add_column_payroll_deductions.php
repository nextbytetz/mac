<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollDeductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_deductions', function(Blueprint $table)
        {
            $table->decimal('last_cycle_amount', 14);
        });
        DB::statement("comment on column payroll_deductions.last_cycle_amount is 'Amount to be recovered on the last cycle'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
