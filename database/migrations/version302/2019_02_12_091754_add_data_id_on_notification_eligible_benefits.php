<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataIdOnNotificationEligibleBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            $table->bigInteger("data_id")->nullable()->index();
        });
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->dropColumn("notification_eligible_benefit_id");
        });
        DB::statement("comment on column notification_eligible_benefits.data_id is 'reference to the data entry concerning this benefit. e.g for MAE reference to the table medical_expenses'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
