<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployeeIdPayrollTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->integer('employee_id')->nullable();
        });
        Schema::table('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('payroll_beneficiary_updates', function(Blueprint $table)
        {
            $table->integer('employee_id')->nullable();
        });
        Schema::table('payroll_beneficiary_updates', function(Blueprint $table)
        {
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
