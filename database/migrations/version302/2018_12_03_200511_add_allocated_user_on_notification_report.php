<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllocatedUserOnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->bigInteger("allocated")->nullable();
            $table->foreign('allocated')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_reports.allocated is 'staff who has been handled responsibility of working on checklist'");
        Schema::table('notification_investigators', function (Blueprint $table) {
            $table->smallInteger("iscomplete")->default(1);
        });
        DB::statement("comment on column notification_investigators.iscomplete is 'set whether a staff completed the investigation or not. 1 - Yes, 0 - No'");
        Schema::create('notification_stages', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->bigInteger("notification_staging_cv_id")->index();
            $table->text("comments")->nullable();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('notification_staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_stages is 'store all the stages which a notification incident is passing through'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
