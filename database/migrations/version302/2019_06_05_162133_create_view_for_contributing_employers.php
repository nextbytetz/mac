<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewForContributingEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("CREATE VIEW contributing_employers AS SELECT DISTINCT employers.name
  FROM employers
      LEFT JOIN regions
    ON regions.id = employers.id
    LEFT JOIN receipts
    ON employers.id = receipts.employer_id
    LEFT JOIN legacy_receipts
    ON employers.id = legacy_receipts.employer_id
        LEFT JOIN receipt_codes
    ON receipt_codes.id = receipts.id
           LEFT JOIN legacy_receipt_codes
    ON legacy_receipt_codes.id = legacy_receipts.id
            LEFT JOIN office_zones
    ON office_zones.id = regions.office_zone_id
  LEFT JOIN contributing_category_employer
    on employers.id = contributing_category_employer.employer_id
   where (receipts.id IS NOT NULL or legacy_receipts.id IS not NULL) AND employers.duplicate_id is NULL AND employers.approval_id = 1
AND employers.deleted_at is NULL AND receipts.deleted_at is NULL AND legacy_receipts.deleted_at is NULL
");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
