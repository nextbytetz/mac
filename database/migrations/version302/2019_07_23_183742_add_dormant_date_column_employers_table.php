<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDormantDateColumnEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employers', function(Blueprint $table)
        {
            $table->date('dormant_date')->nullable();

        });
        DB::statement("comment on column employers.dormant_date is 'Dormant date is a date when employers became dormant'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
