<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingDuplicateView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW booking_duplicates");
        DB::statement("CREATE VIEW booking_duplicates AS     SELECT
    bookings.employer_id,
   DATE_PART('month', bookings.rcv_date::date) as booking_month,
  DATE_PART('year', bookings.rcv_date::date) as booking_year,
    count(*) AS count
  FROM main.bookings
  WHERE (bookings.deleted_at IS NULL)
  GROUP BY bookings.employer_id, DATE_PART('month', bookings.rcv_date::date),DATE_PART('year', bookings.rcv_date::date)
  HAVING (count(*) > 1);
");


        DB::statement("DROP VIEW booking_paid");
        DB::statement("CREATE VIEW booking_paid AS       SELECT
    b.employer_id,
    b.id                                                                                                           AS booking_id,
    e.name                                                                                                         AS employer_name,
    e.reg_no                                                                                                       AS employer_reg_no,
    b.rcv_date,
    b.amount                                                                                                       AS booked_amount,
    coalesce(
    (SELECT sum(receipt_codes.amount) AS sum
     FROM (main.receipt_codes
       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
     WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND (receipts.iscancelled = 0) AND
            (receipts.isdishonoured = 0) AND (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id =
                                                                                b.id)))  ,
        (SELECT sum(receipt_codes.amount) AS sum
     FROM (main.receipt_codes
       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
     WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND (receipts.iscancelled = 0) AND
            (receipts.isdishonoured = 0) AND (receipts.deleted_at IS NULL)  AND DATE_PART('month', b.rcv_date::date) = DATE_PART('month', receipt_codes.contrib_month::date) AND DATE_PART('year', b.rcv_date::date) = DATE_PART('year', receipt_codes.contrib_month::date)  AND (receipts.employer_id = b.employer_id))) )                           AS amount_paid
  FROM (main.bookings b
    JOIN main.employers e ON ((b.employer_id = e.id)))
  WHERE ((b.deleted_at IS NULL) AND (b.ispaid = 1));
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
