<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatatypeOnLevelWfDefinitionToIncreasePrecision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("select deps_save_and_drop_dependencies('main', 'wf_definitions')");
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->decimal("level",4,2)->change();
        });
        DB::statement("select deps_restore_dependencies('main', 'wf_definitions')");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
