<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPayrollSuspendedRuns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->smallInteger('ispaid')->default(0);
            $table->integer('paid_payroll_run_approval_id')->nullable();
            $table->decimal('arrears_amount', 14)->default(0)->comment('Total arrears amount paid on this payroll i.e. underpayment, unclaimed');
            $table->decimal('deductions_amount', 14)->default(0)->comment('Total deductions amount deducted on this payroll i.e. over-payments');
        });


        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->foreign('paid_payroll_run_approval_id')->references('id')->on('payroll_run_approvals')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
