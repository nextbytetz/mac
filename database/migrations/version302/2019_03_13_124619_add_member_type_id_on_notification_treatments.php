<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMemberTypeIdOnNotificationTreatments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_treatments', function (Blueprint $table) {
            $table->bigInteger("member_type_id")->nullable();
            $table->bigInteger("member_id")->nullable();
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_treatments.member_type_id is 'member type who paid for the treatment from member_types table'");
        DB::statement("comment on column notification_treatments.member_id is 'resource id of the member type specified to the respective table'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
