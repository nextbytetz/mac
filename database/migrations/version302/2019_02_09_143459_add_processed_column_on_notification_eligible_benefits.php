<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessedColumnOnNotificationEligibleBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            $table->smallInteger("processed")->default(0);
        });
        DB::statement("comment on column notification_eligible_benefits.processed is 'show the status whether the benefit has passed the assessment workflow approval. 1 - Yes, 0 - No, 2 - Declined'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
