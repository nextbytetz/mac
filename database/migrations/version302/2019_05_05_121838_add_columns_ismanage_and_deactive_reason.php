<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsIsmanageAndDeactiveReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal.employer_user', function (Blueprint $table) {
             $table->boolean('ismanage')->default(1);
            $table->string('deactivate_reason')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.employer_user', function (Blueprint $table) {
            //
        });
    }
}
