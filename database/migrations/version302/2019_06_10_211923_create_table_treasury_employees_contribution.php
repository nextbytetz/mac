<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTreasuryEmployeesContribution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treasury_employees_contribution', function (Blueprint $table) {
         $table->increments('id');
         $table->string('check_number');
         $table->string('vote_code');
         $table->string('vote_name');
         $table->string('firstname');
         $table->string('middlename')->nullable();
         $table->string('lastname');
         $table->decimal('grosspay',14);
         $table->decimal('employee_contribution',14)->nullable();
         $table->decimal('employer_contribution',14);
         $table->string('funding_source');
         $table->date('check_date');
         $table->integer('treasury_contribution_summary_id')->unsigned();
         $table->boolean('ismoved')->default(0);
         $table->timestamps();
         $table->foreign('treasury_contribution_summary_id')->references('id')->on('main.treasury_contribution_summary')->onUpdate('CASCADE')->onDelete('RESTRICT');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treasury_employees_contribution');
    }
}
