<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasChildOnPdImpairments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pd_impairments', function (Blueprint $table) {
            $table->smallInteger("has_child")->default(0);
        });
        DB::statement("comment on column pd_impairments.has_child is 'determine whether entry has sub-entries in this table, 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
