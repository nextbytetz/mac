<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsprocessedPayrollRunApprova extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_approvals', function(Blueprint $table)
        {
            $table->smallInteger('ispayprocessed')->default(0);
        });

        DB::statement("comment on column payroll_run_approvals.ispayprocessed is 'show whether the payroll run payment has been processed at level of finance officer. 1 - yes, 0 - no'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
