<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsTreasuryOnBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal.bills', function (Blueprint $table) {
            $table->boolean('is_treasury')->default(false);
            $table->string('vote_code')->nullable();
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.bills', function (Blueprint $table) {
         $table->dropColumn('is_treasury');
         $table->dropColumn('vote_code');
     });
    }
}
