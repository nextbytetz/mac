<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllowRepeatOnNotificationWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
            $table->smallInteger("allow_repeat")->default(0);
        });
        DB::statement("comment on column notification_workflows.allow_repeat is 'show whether this stage can be repeated at any time in the future process'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
