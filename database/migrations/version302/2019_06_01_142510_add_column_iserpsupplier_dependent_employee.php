<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIserpsupplierDependentEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->text('iserpsupplier')->default(0);
        });

        DB::statement("comment on column dependent_employee.iserpsupplier is 'Flag to specify if dependent is exported to erp as supplier i.e. 1 => ready exported, 0 => not yet'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
