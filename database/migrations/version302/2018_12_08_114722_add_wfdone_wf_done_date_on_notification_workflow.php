<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfdoneWfDoneDateOnNotificationWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_workflows', function (Blueprint $table) {
            $table->smallInteger("wf_done")->default(0);
            $table->date("wf_done_date")->nullable();
            $table->smallInteger("iscomplete")->default(0);
        });
        DB::statement("comment on column notification_workflows.iscomplete is 'shows whether the workflow completed its round trip or not. 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
