<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollRetireeSuspensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_child_suspensions', function(Blueprint $table)
        {
            $table->foreign('dependent_id')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        Schema::create('payroll_retiree_suspensions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('dependent_id');
            $table->smallInteger('isvoluntary')->default('0');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table payroll_retiree_suspensions is 'Table to keep track of all retiree limit age suspensions by system i.e. 55yrs/60yrs'");

        DB::statement("comment on column payroll_retiree_suspensions.isvoluntary is 'Flag to imply if dependent is approaching voluntary retirement age / compulsory retirement age'");

        Schema::table('payroll_retiree_suspensions', function(Blueprint $table)
        {
            $table->foreign('dependent_id')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
