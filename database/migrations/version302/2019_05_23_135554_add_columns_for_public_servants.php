<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForPublicServants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            //
            $table->string("CheckNumber")->nullable();
            $table->string("DeptCode")->nullable();
            $table->string("DeptName")->nullable();
            $table->string("VoteCode")->nullable();
            $table->string("PayGrade")->nullable();
            $table->string("PayStep")->nullable();
            $table->string("SalaryScale")->nullable();
            $table->string("FundingSource")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            //
        });
    }
}
