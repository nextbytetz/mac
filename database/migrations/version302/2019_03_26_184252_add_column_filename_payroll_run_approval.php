<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFilenamePayrollRunApproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_approvals', function(Blueprint $table)
        {
            $table->string('filename')->nullable();
            $table->smallInteger('isdmsposted')->default(0);
        });

        DB::statement("comment on column payroll_run_approvals.isdmsposted is 'Flag to imply if file has been posted into e-office i.e 1=> posted, 0 -> pending'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
