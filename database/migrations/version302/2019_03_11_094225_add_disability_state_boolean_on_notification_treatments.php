<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisabilityStateBooleanOnNotificationTreatments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_treatments', function (Blueprint $table) {
            $table->smallInteger("has_ed")->default(0);
            $table->smallInteger("has_hospitalization")->default(0);
            $table->smallInteger("has_ld")->default(0);
        });
        DB::statement("comment on column notification_treatments.has_ed is 'specify whether a member had excuse from duty days'");
        DB::statement("comment on column notification_treatments.has_hospitalization is 'specify whether a member has hospitalization days'");
        DB::statement("comment on column notification_treatments.has_ld is 'specify whether a member has light duties days'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
