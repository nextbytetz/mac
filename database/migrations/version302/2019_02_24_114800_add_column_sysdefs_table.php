<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSysdefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->smallInteger("payroll_child_limit_age")->nullable();
            $table->smallInteger("payroll_child_education_limit_age")->nullable();
            $table->smallInteger("payroll_suspension_alert_months")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
