<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReassessmentParentIszeropaidOnEligibles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function(Blueprint $table)
        {
            $table->smallInteger('iszeropaid')->default(0);
            $table->bigInteger('reassessment_parent')->nullable();
            $table->foreign('reassessment_parent')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('incident_assessments', function(Blueprint $table)
        {
            $table->dropColumn('notification_report_id');
            $table->bigInteger('notification_eligible_benefit_id')->index();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_eligible_benefits.iszeropaid is 'show whether the benefit payment has zero payment or not, 1 - Yes, 0 - No'");
        DB::statement("comment on column notification_eligible_benefits.reassessment_parent is 'on doing reassessment this column will refer to the parent eligible payment which reassessment is referring to'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
