<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollUnclaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_unclaims', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payroll_recovery_id');
            $table->decimal('amount', 14);
            $table->smallInteger('recycles')->default(1);
            $table->smallInteger('isactive')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_unclaims is 'Table to keep track of all arrears / unclaimed payment captured for payroll beneficiaries'");
        DB::statement("comment on column payroll_unclaims.amount is 'Amount to be paid per cycle'");
        DB::statement("comment on column payroll_unclaims.recycles is 'Number of cycles remained to pay beneficiary'");


        Schema::table('payroll_unclaims', function(Blueprint $table)
        {
            $table->foreign('payroll_recovery_id')->references('id')->on('payroll_recoveries')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
