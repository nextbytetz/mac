<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsPayrollRunApprovals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_approvals', function(Blueprint $table)
        {
            $table->renameColumn('number_of_pensioners', 'no_of_pensioners');
            $table->renameColumn('number_of_dependents', 'no_of_dependents');
            $table->integer('no_of_suspended_pensioners')->default(0);
            $table->integer('no_of_suspended_dependents')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
