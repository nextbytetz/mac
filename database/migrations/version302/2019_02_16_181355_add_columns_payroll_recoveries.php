<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPayrollRecoveries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_recoveries', function (Blueprint $table) {
            $table->integer("payroll_reconciliation_id")->nullable();
        });

        Schema::table('payroll_recoveries', function(Blueprint $table)
        {
            $table->foreign('payroll_reconciliation_id')->references('id')->on('payroll_reconciliations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('payroll_reconciliations', function (Blueprint $table) {
            $table->integer("user_id")->nullable();
        });

        Schema::table('payroll_reconciliations', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
