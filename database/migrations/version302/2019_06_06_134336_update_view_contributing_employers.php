<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewContributingEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW contributing_employers");

        DB::statement("CREATE VIEW contributing_employers AS SELECT DISTINCT employers.id AS employer_id
  FROM ((main.employers
    LEFT JOIN main.receipts ON ((employers.id = receipts.employer_id)))
    LEFT JOIN main.legacy_receipts ON ((employers.id = legacy_receipts.employer_id)))
  WHERE (((receipts.id IS NOT NULL) OR (legacy_receipts.id IS NOT NULL)) AND (employers.duplicate_id IS NULL) AND
         (employers.approval_id = 1) AND (employers.deleted_at IS NULL) AND (receipts.deleted_at IS NULL) AND
         (legacy_receipts.deleted_at IS NULL) AND receipts.iscancelled = 0 AND legacy_receipts.iscancelled = 0);
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
