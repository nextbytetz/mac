<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewMinPaymentDateForEmployers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
              DB::statement("CREATE VIEW employers_min_payment_date AS      select e.employer_id, min(coalesce(l.rct_date, r.rct_date)) as min_payment_date from contributing_employers e
left join legacy_receipts l on l.employer_id = e.employer_id
left join receipts r on r.employer_id = e.employer_id
GROUP BY   e.employer_id;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
