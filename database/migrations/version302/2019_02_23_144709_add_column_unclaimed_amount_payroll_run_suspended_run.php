<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUnclaimedAmountPayrollRunSuspendedRun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_runs', function (Blueprint $table) {
            $table->decimal("unclaimed_amount", 14)->default(0);
        });
        Schema::table('payroll_suspended_runs', function (Blueprint $table) {
            $table->decimal("unclaimed_amount", 14)->default(0);
        });

        Schema::table('pensioners', function (Blueprint $table) {
            $table->string('phone', 45)->nullable();
        });

        $sql = <<<SQL
update pensioners
set phone = (
  select employees.phone
  from employees
  where employees.id = pensioners.employee_id
)
  where exists (
      select *
      from employees
      where employees.id = pensioners.employee_id
  );
SQL;
        DB::unprepared($sql);
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
