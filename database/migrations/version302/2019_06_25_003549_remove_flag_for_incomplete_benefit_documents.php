<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFlagForIncompleteBenefitDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL

update document_notification_report set isreferenced = 0 where notification_report_id not in (select notification_report_id from notification_eligible_benefits) and notification_report_id in (select notification_report_id from document_notification_report where isreferenced = 1);

SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
