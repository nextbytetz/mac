<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeIsownOnEmployerAdvancePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_advance_payments', function (Blueprint $table) {
            $table->smallInteger('isown')->default(1);
            $table->smallInteger("status")->default(0);
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column employer_advance_payments.isown is 'Check whether this user own this business or not, 1 => own this business, 0 => do not own this business'");
        DB::statement("comment on column employer_advance_payments.status is 'set the status of the request, 0 => Pending for Approval, 1 => Approved, 2 => Rejected'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
