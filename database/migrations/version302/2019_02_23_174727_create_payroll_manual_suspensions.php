<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollManualSuspensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_manual_suspensions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->dateTime('start_date');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_manual_suspensions is 'Table to keep track of all manual suspension triggers after verification of beneficiary'");
        DB::statement("comment on column payroll_manual_suspensions.start_date is 'Start date of the verification quarter'");

        Schema::table('payroll_manual_suspensions', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



        Schema::table('payroll_system_suspensions', function(Blueprint $table)
        {
            $table->dropColumn('user_id');
            $table->integer('payroll_manual_suspension_id')->nullable();
        });

        Schema::table('payroll_system_suspensions', function(Blueprint $table)
        {
            $table->foreign('payroll_manual_suspension_id')->references('id')->on('payroll_manual_suspensions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
