<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttendedOnNotificationStages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_stages', function (Blueprint $table) {
            $table->smallInteger("attended")->default(0);
        });
        DB::statement("comment on column notification_stages.attended is 'show whether a user has executed the next stage action. 1 - yes, 0 - no'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
