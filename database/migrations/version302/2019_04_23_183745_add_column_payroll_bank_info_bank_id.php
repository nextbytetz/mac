<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollBankInfoBankId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->smallInteger('old_bank_branch_id')->nullable()->change();
            $table->smallInteger('old_bank_id')->nullable();
            $table->smallInteger('new_bank_branch_id')->nullable()->change();
            $table->smallInteger('new_bank_id')->nullable();
        });

        Schema::table('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->foreign('old_bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('new_bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
