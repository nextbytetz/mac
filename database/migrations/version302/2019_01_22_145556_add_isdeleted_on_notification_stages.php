<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsdeletedOnNotificationStages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_stages', function (Blueprint $table) {
            $table->smallInteger("isdeleted")->default(0);
        });
        DB::statement("comment on column notification_stages.isdeleted is 'show whether this stage has been deleted and has never been attended. Stages were frequently updated such that this stage was just logged and never to be attended. 1 - Yes, 0 - No'");
        DB::statement("comment on column checkers.status is 'show whether the resource has been worked. 0 - Pending, 1 - Issued, 2 - Declined by system'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
