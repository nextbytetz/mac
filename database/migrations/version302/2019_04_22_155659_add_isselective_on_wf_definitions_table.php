<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsselectiveOnWfDefinitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function(Blueprint $table)
        {
            $table->smallInteger('isselective')->default(0);
        });
        DB::statement("comment on column wf_definitions.isselective is 'show whether this level can choose the next level to jump or not. 1 - Yes, 0 - No'");

        Schema::table('designations', function(Blueprint $table)
        {
            $table->smallInteger('level')->nullable();
        });
        DB::statement("comment on column designations.level is 'show the designations from different units with the same hierarchy of administration'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
