<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPayrollRecoveriesIscancelled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_recoveries', function(Blueprint $table)
        {
            $table->smallInteger('iscancelled')->default(0);
            $table->string('cancel_reason')->nullable();
        });

        DB::statement("comment on column payroll_recoveries.iscancelled is 'Flag to specify if recovery which is pending for payment is cancelled i.e. 1 => cancelled , 0 => not '");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
