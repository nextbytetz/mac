<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeRankNullabmeOnNotificationHealthProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_health_providers', function (Blueprint $table) {
            $table->bigInteger("rank")->nullable()->change();
        });
        Schema::table('medical_expenses', function (Blueprint $table) {
            $table->bigInteger("notification_eligible_benefit_id")->nullable()->index();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
