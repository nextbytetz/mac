<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnApprovalReadyOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->smallInteger('approval_ready')->default(0);
        });
        DB::statement("comment on column notification_reports.approval_ready is 'show whether the file is ready for notification approval/validation process or not. This is triggered when all the necessary documents has already been uploaded, 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
