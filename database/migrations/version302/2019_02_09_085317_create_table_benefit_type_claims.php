<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBenefitTypeClaims extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('benefit_type_claims', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->string("name", 100);
            $table->timestamps();
        });

        DB::statement("comment on table benefit_type_claims is 'store the general benefits processed during assessment'");

        Schema::table('benefit_types', function (Blueprint $table) {
            $table->smallInteger("benefit_type_claim_id")->nullable();
            $table->foreign('benefit_type_claim_id')->references('id')->on('benefit_type_claims')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
