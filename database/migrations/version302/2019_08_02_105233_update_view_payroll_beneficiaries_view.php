<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewPayrollBeneficiariesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW payroll_beneficiaries_view CASCADE");
        DB::statement("CREATE VIEW payroll_beneficiaries_view AS SELECT p.id                  AS resource_id,
       5                     AS member_type_id,
       CASE
              WHEN (p.notification_report_id IS NOT NULL) THEN n.filename
              ELSE (m2.case_no) :: character varying(20)
           END               AS filename,
       concat_ws(' ' :: text, p.firstname, COALESCE(p.middlename, '' :: character varying),
                 p.lastname) AS member_name,
       p.bank_id,
       p.bank_branch_id,
       p.notification_report_id,
       p.manual_notification_report_id,
       p.isactive,
       p.employee_id,
       NULL :: integer       AS pivot_id
FROM ((main.pensioners p
    LEFT JOIN main.notification_reports n ON ((p.notification_report_id = n.id)))
    LEFT JOIN main.manual_notification_reports m2 ON ((p.manual_notification_report_id = m2.id)))
UNION
SELECT d.id                  AS resource_id,
       4                     AS member_type_id,
       CASE
              WHEN (de.notification_report_id IS NOT NULL) THEN n.filename
              ELSE (m2.case_no) :: character varying(20)
           END               AS filename,
       concat_ws(' ' :: text, d.firstname, COALESCE(d.middlename, '' :: character varying),
                 d.lastname) AS member_name,
       d.bank_id,
       d.bank_branch_id,
       de.notification_report_id,
       de.manual_notification_report_id,
       de.isactive,
       de.employee_id,
       de.id                 AS pivot_id
FROM (((main.dependent_employee de
    JOIN main.dependents d ON ((d.id = de.dependent_id)))
    LEFT JOIN main.notification_reports n ON ((de.notification_report_id = n.id)))
    LEFT JOIN main.manual_notification_reports m2 ON ((de.manual_notification_report_id = m2.id)));
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
