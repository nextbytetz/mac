<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContribMonthOrigOnReceiptCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->date("contrib_month_orig")->nullable();
        });
        DB::statement("comment on column receipt_codes.contrib_month_orig is 'original registered contribution month before changes'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
