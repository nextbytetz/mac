<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonthlyEarningChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->decimal("monthly_earning_checklist", 14)->nullable();
        });
        DB::statement("comment on column notification_reports.monthly_earning_checklist is 'monthly earning recorded during notification checklist, this may be very helpful in raising incorrect contribution workflow in case contribution does not match with the retrieved earning from contributions.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
