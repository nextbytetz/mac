<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewBookingDuplicate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("CREATE VIEW booking_duplicates AS  SELECT
    employer_id, rcv_date, COUNT(*)
FROM
    bookings
  where deleted_at is null
GROUP BY
     employer_id, rcv_date
HAVING
    COUNT(*) > 1
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
