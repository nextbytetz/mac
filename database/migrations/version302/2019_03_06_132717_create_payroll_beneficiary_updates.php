<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollBeneficiaryUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_beneficiary_updates', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('resource_id');
            $table->integer('member_type_id');
            $table->text('old_values');
            $table->text('new_values');
            $table->smallInteger('wf_done')->default(0);
            $table->dateTime('wf_done_date')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table payroll_beneficiary_updates is 'Table to keep track of all payroll beneficiary information modifications'");


        Schema::table('payroll_beneficiary_updates', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
