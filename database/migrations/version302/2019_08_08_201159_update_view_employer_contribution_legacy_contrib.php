<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewEmployerContributionLegacyContrib extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

                DB::statement("DROP VIEW employer_contributions CASCADE");
        DB::statement("CREATE VIEW  employer_contributions  AS SELECT r.employer_id,
       e.doc,
       e.name                        AS employer_name,
       e.reg_no                      AS employer_reg_no,
       code.amount                   AS contrib_amount,
       r.amount                      AS receipt_amount,
       to_date(concat_ws('-' :: text, date_part('year' :: text, code.contrib_month),
                         date_part('month' :: text, code.contrib_month), '28'),
               'YYYY-MM-DD' :: text) AS contrib_month,
       r.rct_date,
       r.created_at                  AS receipt_created_at,
       CAST(r.rctno as VARCHAR(100)),
       0 as islegacy
FROM ((main.receipt_codes code
    JOIN main.receipts r ON ((code.receipt_id = r.id)))
    JOIN main.employers e ON ((r.employer_id = e.id)))
WHERE ((r.iscancelled = 0) AND (r.isdishonoured = 0) AND (r.deleted_at IS NULL) AND
       ((code.fin_code_id = 2) OR (code.fin_code_id = 63)))

UNION

SELECT l.employer_id,
       e.doc,
       e.name                        AS employer_name,
       e.reg_no                      AS employer_reg_no,
       l.amount                   AS contrib_amount,
       l.amount                      AS receipt_amount,
       to_date(concat_ws('-' :: text, date_part('year' :: text, l.contrib_month),
                         date_part('month' :: text, l.contrib_month), '28'),
               'YYYY-MM-DD' :: text) AS contrib_month,
       l.rct_date,
       l.created_at                  AS receipt_created_at,
       l.rctno,
       1 as islegacy
FROM main.legacy_receipts l
       JOIN main.employers e ON l.employer_id = e.id
WHERE l.iscancelled = 0 AND l.isdishonoured = 0 AND l.deleted_at IS NULL
");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
