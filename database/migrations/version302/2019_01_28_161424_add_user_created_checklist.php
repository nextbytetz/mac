<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserCreatedChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diseases', function (Blueprint $table) {
            $table->bigInteger("checklist_user")->nullable();
            $table->foreign('checklist_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column diseases.checklist_user is 'user who filled checklist'");
        Schema::table('accidents', function (Blueprint $table) {
            $table->bigInteger("checklist_user")->nullable();
            $table->foreign('checklist_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column accidents.checklist_user is 'user who filled checklist'");
        Schema::table('deaths', function (Blueprint $table) {
            $table->bigInteger("checklist_user")->nullable();
            $table->foreign('checklist_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column deaths.checklist_user is 'user who filled checklist'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
