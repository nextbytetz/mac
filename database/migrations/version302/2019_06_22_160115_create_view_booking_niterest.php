<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewBookingNiterest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement("CREATE VIEW booked_interests AS SELECT i.id as interest_id, i.booking_id, b.employer_id, e.name as employer_name, e.reg_no as employer_reg_no, e.region_id,i.miss_month,i.hasreceipt,i.amount as booked_amount,  (select sum(receipt_codes.amount) from receipt_codes JOIN receipts  on receipt_codes.receipt_id = receipts.id
where (receipt_codes.fin_code_id = 1) AND
      receipts.iscancelled = 0 and receipts.isdishonoured = 0 and receipts.deleted_at is null AND receipt_codes.booking_id = b.id) as amount_paid
FROM main.booking_interests as i
  JOIN main.bookings as b on b.id = i.booking_id
  JOIN main.employers as e on b.employer_id = e.id
where
  b.deleted_at is null and i.deleted_at is null

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
