<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptionForRehabilitationAndCca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims', function(Blueprint $table)
        {
            $table->smallInteger('need_rehabilitation')->default(0);
            $table->smallInteger('need_cca')->default(0);
        });
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->smallInteger('minimum_allowed_cca')->default(85);
        });
        DB::statement("comment on column claims.need_rehabilitation is 'show whether this claim need rehabilitation or not, 1 - Yes, 0 - No'");
        DB::statement("comment on column claims.need_cca is 'show whether this claim need constant care attendant or not, 1 - Yes, 0 - No'");
        DB::statement("comment on column sysdefs.minimum_allowed_cca is 'minimum value of percentage disability for a claim to be eligible for being opted for constant care attendant'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
