<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManualPayrollRuns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('manual_payroll_runs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->smallInteger('member_type_id');
            $table->integer('resource_id')->nullable();
            $table->decimal('monthly_pension', 14);
            $table->smallInteger('months_paid');
            $table->decimal('amount', 14);
            $table->decimal('arrears_amount', 14);
            $table->integer('manual_notification_report_id')->nullable();
            $table->integer('notification_report_id')->nullable();
            $table->string('payee_name');
            $table->dateTime('payroll_month');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table manual_payroll_runs is 'Table to keep track of all monthly pension payroll processed manually before the system'");

        Schema::table('manual_payroll_runs', function(Blueprint $table)
        {
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
