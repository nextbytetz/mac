<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriorityOnCheckers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkers', function (Blueprint $table) {
            $table->smallInteger("priority")->default(1);
        });
        DB::statement("comment on column checkers.priority is 'set the priority of the checker if it requires immediate issuance. 1 - High, 0 - Low. Entries with priority low can be worked over days.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
