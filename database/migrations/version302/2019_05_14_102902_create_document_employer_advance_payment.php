<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentEmployerAdvancePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_employer_advance_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("employer_advance_payment_id")->index();
            $table->integer("document_id")->index();
            $table->string("name", 150)->nullable();
            $table->text("description")->nullable();
            $table->string("ext", 20)->nullable();
            $table->double("size")->nullable();
            $table->string("mime", 150)->nullable();
            $table->timestamps();
            $table->foreign('employer_advance_payment_id')->references('id')->on('employer_advance_payments')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
