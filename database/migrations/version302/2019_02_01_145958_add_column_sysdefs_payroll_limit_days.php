<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSysdefsPayrollLimitDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sysdefs', function(Blueprint $table)
        {
            $table->smallInteger('pension_verification_limit_days')->nullable()->comment('Number of days for verification period for pensioners and dependents');
            $table->smallInteger('periodical_verification_limit_days')->nullable()->comment('Number of days for verification period for periodical payments i.e. constant care and Temporary disablement');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
