<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewPayrollPendingWorkflowView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
//        DB::statement("DROP VIEW payroll_pending_workflows_view");
        DB::statement("CREATE VIEW payroll_pending_workflows_view AS  
      select b.member_name,b.filename, concat_ws('-', m2.name, v.name) as workflow_name, b.member_type_id, mt.name as member_type_name, b.resource_id, b.pivot_id, b.employee_id, u.username as initiated_by, definition.level as pending_level, u2.username as pending_user, r.created_at, m2.wf_module_group_id  from payroll_recoveries r
join payroll_beneficiaries_view b on b.member_type_id = r.member_type_id and r.resource_id = b.resource_id
join code_values v on r.recovery_type_cv_id = v.id
join users u on r.user_id = u.id
    join member_types mt on mt.id = b.member_type_id
join wf_tracks w on w.resource_id = r.id
left join users u2 on w.user_id = u2.id
join wf_definitions definition on w.wf_definition_id = definition.id
join wf_modules m2 on definition.wf_module_id = m2.id
where r.wf_done = 0 and w.status = 0 and m2.wf_module_group_id = 13

UNION

select b.member_name,b.filename,  m2.name as workflow_name, b.member_type_id, mt.name as member_type_name, b.resource_id, b.pivot_id, b.employee_id, u.username as initiated_by, definition.level as pending_level, u2.username as pending_user, r.created_at, m2.wf_module_group_id   from payroll_bank_info_updates r
join payroll_beneficiaries_view b on b.member_type_id = r.member_type_id and r.resource_id = b.resource_id
join users u on r.user_id = u.id
join member_types mt on mt.id = b.member_type_id
join wf_tracks w on w.resource_id = r.id
left join users u2 on w.user_id = u2.id
join wf_definitions definition on w.wf_definition_id = definition.id
join wf_modules m2 on definition.wf_module_id = m2.id
where r.wf_done = 0 and w.status = 0 and m2.wf_module_group_id = 11

UNION

select b.member_name,b.filename, m2.name  as workflow_name, b.member_type_id, mt.name as member_type_name, b.resource_id, b.pivot_id, b.employee_id, u.username as initiated_by, definition.level as pending_level, u2.username as pending_user, r.created_at, m2.wf_module_group_id  from payroll_beneficiary_updates r
join payroll_beneficiaries_view b on b.member_type_id = r.member_type_id and r.resource_id = b.resource_id
join users u on r.user_id = u.id
join member_types mt on mt.id = b.member_type_id
join wf_tracks w on w.resource_id = r.id
left join users u2 on w.user_id = u2.id
join wf_definitions definition on w.wf_definition_id = definition.id
join wf_modules m2 on definition.wf_module_id = m2.id
where r.wf_done = 0 and w.status = 0 and m2.wf_module_group_id = 15

UNION

select b.member_name,b.filename,  concat_ws('-', m2.name, v.name) as workflow_name, b.member_type_id, mt.name as member_type_name, b.resource_id, b.pivot_id, b.employee_id, u.username as initiated_by, definition.level as pending_level, u2.username as pending_user, r.created_at, m2.wf_module_group_id  from payroll_status_changes r
join payroll_beneficiaries_view b on b.member_type_id = r.member_type_id and r.resource_id = b.resource_id
join users u on r.user_id = u.id
join member_types mt on mt.id = b.member_type_id
join code_values v on r.status_change_cv_id = v.id
join wf_tracks w on w.resource_id = r.id
left join users u2 on w.user_id = u2.id
join wf_definitions definition on w.wf_definition_id = definition.id
join wf_modules m2 on definition.wf_module_id = m2.id
where r.wf_done = 0 and w.status = 0 and m2.wf_module_group_id = 12

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
