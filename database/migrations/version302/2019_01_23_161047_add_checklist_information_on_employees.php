<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChecklistInformationOnEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->bigInteger("country_id")->nullable();
            $table->string("passport_no", 50)->nullable();
            $table->string("work_permit_no", 50)->nullable();
            $table->string("residence_permit_no", 50)->nullable();
            $table->smallInteger("no_of_children")->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('employee_employer', function (Blueprint $table) {
            $table->string("department", 50)->nullable();
        });
        DB::statement("comment on column employees.country_id is 'nationality of an employee, country of origin'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
