<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaidDateOnDashboardReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_codes', function(Blueprint $table)
        {
            $table->boolean("is_treasury")->default("f");
        });
        DB::statement("comment on column receipt_codes.is_treasury is 'signifies whether this monthly contribution has been paid by treasury or not, true or false. corresponding receipt linked by receipt_id'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
