<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasEmployerEmployeeInNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->smallInteger("has_employee")->default(1);
            $table->smallInteger("has_employer")->default(1);
        });
        DB::statement("comment on column notification_reports.has_employee is 'specify whether during registration of the notification, employee was present in database'");
        DB::statement("comment on column notification_reports.has_employer is 'specify whether during registration of the notification, employer was present in database'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
