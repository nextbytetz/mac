<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFolioPayrollApprovalTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->string('folionumber')->nullable();

        });
        DB::statement("comment on column payroll_bank_info_updates.folionumber is 'Folio number of document to verify updated information'");

        Schema::table('payroll_beneficiary_updates', function(Blueprint $table)
        {
            $table->string('folionumber')->nullable();

        });
        DB::statement("comment on column payroll_beneficiary_updates.folionumber is 'Folio number of document to verify updated information'");

        Schema::table('payroll_verifications', function(Blueprint $table)
        {
            $table->string('folionumber')->nullable();

        });
        DB::statement("comment on column payroll_verifications.folionumber is 'Folio number of document to verify updated information'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
