<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnExitDateForBeneficiaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pensioners', function (Blueprint $table) {
            $table->dateTime('exit_date')->nullable();
        });

        DB::statement("comment on column pensioners.exit_date is 'Exit date from payroll i.e. death date, deactivated_date'");

        Schema::table('dependents', function (Blueprint $table) {
            $table->dateTime('exit_date')->nullable();
        });

        DB::statement("comment on column dependents.exit_date is 'Exit date from payroll i.e. death date, deactivated_date'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
