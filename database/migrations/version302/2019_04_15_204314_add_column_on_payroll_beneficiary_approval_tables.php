<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOnPayrollBeneficiaryApprovalTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payroll_recoveries', function(Blueprint $table)
        {
            $table->integer('notification_report_id')->nullable()->change();
            $table->integer('manual_notification_report_id')->nullable();
        });

        Schema::table('payroll_recoveries', function(Blueprint $table)
        {
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });



        Schema::table('payroll_status_changes', function(Blueprint $table)
        {
            $table->integer('notification_report_id')->nullable()->change();
            $table->integer('manual_notification_report_id')->nullable();
        });

        Schema::table('payroll_status_changes', function(Blueprint $table)
        {
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
