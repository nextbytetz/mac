<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsertIntoMae extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL

insert into incident_mae(notification_report_id, mae_refund_member_ready) select id, mae_refund_member_ready from notification_reports where isprogressive = 1 and mae_refund_member_ready = 1;

update notification_reports set incident_mae_id = incident_mae.id from incident_mae where incident_mae.notification_report_id = notification_reports.id;

SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
