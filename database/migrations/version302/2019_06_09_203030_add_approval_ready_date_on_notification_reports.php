<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovalReadyDateOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->date("approval_ready_date")->nullable();
            $table->date("approval_ready_mac_date")->nullable();
            $table->date("pd_ready_mac_date")->nullable();
            $table->date("survivor_ready_mac_date")->nullable();
        });
        Schema::table('incident_tds', function(Blueprint $table)
        {
            $table->date("td_ready_mac_date")->nullable();
            $table->date("td_refund_ready_mac_date")->nullable();
        });
        Schema::table('incident_mae', function(Blueprint $table)
        {
            $table->date("mae_refund_member_ready_mac_date")->nullable();
        });

        $sql = <<<SQL

-- Insert for approval_ready
-- (Accident)
 update notification_reports set approval_ready_mac_date = vv.create_date from  (select b.id, coalesce(max(a.created_at::date)) create_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where member_type_id = 2 and b.incident_type_id = 1) and b.isprogressive = 1 and b.approval_ready = 1 group by b.id) vv where vv.id = notification_reports.id and approval_ready = 1;

-- (Disease)
 update notification_reports set approval_ready_mac_date = vv.create_date from  (select b.id, coalesce(max(a.created_at::date)) create_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where member_type_id = 2 and b.incident_type_id = 2) and b.isprogressive = 1 and b.approval_ready = 1 group by b.id) vv where vv.id = notification_reports.id and approval_ready = 1;

-- (Death)
 update notification_reports set approval_ready_mac_date = vv.create_date from  (select b.id, coalesce(max(a.created_at::date)) create_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where member_type_id = 2 and b.incident_type_id = 3) and b.isprogressive = 1 and b.approval_ready = 1 group by b.id) vv where vv.id = notification_reports.id and approval_ready = 1;

-- (Accident)
 update notification_reports set approval_ready_date = vv.create_date from  (select b.id, coalesce(max(a.doc_receive_date::date), max(a.created_at::date)) create_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where member_type_id = 2 and b.incident_type_id = 1) and b.isprogressive = 1 and b.approval_ready = 1 group by b.id) vv where vv.id = notification_reports.id and approval_ready = 1;

-- (Disease)
 update notification_reports set approval_ready_date = vv.create_date from  (select b.id, coalesce(max(a.doc_receive_date::date), max(a.created_at::date)) create_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where member_type_id = 2 and b.incident_type_id = 2) and b.isprogressive = 1 and b.approval_ready = 1 group by b.id) vv where vv.id = notification_reports.id and approval_ready = 1;

-- (Death)
 update notification_reports set approval_ready_date = vv.create_date from  (select b.id, coalesce(max(a.doc_receive_date::date), max(a.created_at::date)) create_date from document_notification_report a join notification_reports b on b.id = a.notification_report_id where a.document_id in (select document_id from document_benefits where member_type_id = 2 and b.incident_type_id = 3) and b.isprogressive = 1 and b.approval_ready = 1 group by b.id) vv where vv.id = notification_reports.id and approval_ready = 1;

SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
