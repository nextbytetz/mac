<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIssyncedManualFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('manual_payroll_runs', function(Blueprint $table)
        {
            $table->smallInteger('issynced')->default(0);
            $table->integer('user_id');

        });

        Schema::table('manual_payroll_runs', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        Schema::table('manual_notification_reports', function(Blueprint $table)
        {
            $table->smallInteger('issynced')->default(0);
            $table->string('employer_name')->nullable();
            $table->integer('user_id');
        });

        DB::statement("comment on column manual_notification_reports.employer_name is 'Employer name as used on manual file before synchronize'");

        Schema::table('manual_notification_reports', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
