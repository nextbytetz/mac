<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentDateInfoColumnsOnDocumentNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_notification_report', function(Blueprint $table)
        {
            $table->date('doc_date')->nullable();
            $table->date('doc_receive_date')->nullable();
            $table->date('doc_create_date')->nullable();
            $table->integer('folio')->nullable();
        });
        DB::statement("comment on column document_notification_report.doc_date is 'date when document was prepared by the member'");
        DB::statement("comment on column document_notification_report.doc_receive_date is 'date when the document was received at WCF office'");
        DB::statement("comment on column document_notification_report.doc_create_date is 'date when document was created in e-office'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
