<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCommentOnColumnNeedInvestigationNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column notification_reports.need_investigation is 'set whether notification needs physical investigation or not, 1 - needs physical investigation, 0 - does not need physical investigation/only need phone call investigation.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
