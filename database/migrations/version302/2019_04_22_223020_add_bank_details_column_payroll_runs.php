<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankDetailsColumnPayrollRuns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->smallInteger('bank_branch_id')->nullable()->change();
            $table->smallInteger('bank_id')->nullable();
                   });

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
