<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssessmentCheckOnClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims', function(Blueprint $table)
        {
            $table->smallInteger('cca_registered')->default(0);
            $table->smallInteger('assessed')->default(0);
        });
        DB::statement("comment on column claims.cca_registered is 'show whether constant care attendant for a pensioner has been registered, 1 - Yes, 0 - No'");
        DB::statement("comment on column claims.assessed is 'show whether the assessment for permanent disablement percentage has been done, 1 - Yes, 0 - No '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
