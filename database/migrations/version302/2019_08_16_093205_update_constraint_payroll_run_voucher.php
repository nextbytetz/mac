<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateConstraintPayrollRunVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_run_vouchers', function(Blueprint $table)
        {
           $table->dropUnique('payroll_run_vouchers_bank_id_payroll_run_approval_id_unique');
           $table->unique(['bank_id', 'payroll_run_approval_id', 'benefit_type_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
