<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResourceAndBenefitTypeClaimidOnNotificationEligibleBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            $table->smallInteger("benefit_type_claim_id")->nullable();
            $table->bigInteger("resource_id")->nullable();
            $table->bigInteger("user_id")->nullable();
            $table->bigInteger("notification_workflow_id")->nullable();
            $table->dropColumn("benefit_type_id");
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('benefit_type_claim_id')->references('id')->on('benefit_type_claims')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('notification_workflow_id')->references('id')->on('notification_workflows')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_eligible_benefits.benefit_type_claim_id is 'associated accessible benefit type claim id'");
        DB::statement("comment on column notification_eligible_benefits.resource_id is 'beneficiary to be paid the compensations'");
        DB::statement("comment on column notification_eligible_benefits.user_id is 'staff who created the request'");
        DB::statement("comment on column notification_eligible_benefits.notification_workflow_id is 'associated workflow for this benefit processing'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
