<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewSummaryRecevableIncomeAnalysis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
//        DB::statement("DROP MATERIALIZED VIEW  summary_contrib_rcvable_income_mview");
        DB::statement("CREATE MATERIALIZED VIEW  summary_contrib_rcvable_income_mview  AS   
select
f.id as fin_year_id,
  concat_ws('/', f.start, f.\"end\") as fiscal_year,
  (select sum(contrib_amount) from employer_contributions where rct_date::date >= f.start_date::date and rct_date::date <= f.end_date::date and contrib_month >= f.start_date and contrib_month <= f.end_date)  as receipt_contrib,
(select count(distinct employer_id) from employer_contributions where rct_date::date >= f.start_date::date and rct_date::date <= f.end_date::date and contrib_month >= f.start_date and contrib_month <= f.end_date)  as receipt_contrib_employers,
  (select sum(contrib_amount) from employer_contributions where rct_date::date >= f.start_date::date and rct_date::date <= f.end_date::date and contrib_month < f.start_date)  as receipt_prev_contrib,
         (select count(distinct employer_id) from employer_contributions where rct_date::date >= f.start_date::date and rct_date::date <= f.end_date::date and contrib_month < f.start_date)  as receipt_prev_contrib_employers,
  (select sum(b.booked_amount) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
   join ( select b1.employer_id from bookings b1 where b1.ispaid = 1 and deleted_at IS NULL and  b1.rcv_date::date >= f.start_date::date and b1.rcv_date::date <= f.end_date::date group by b1.employer_id) b2 on b2.employer_id = b.employer_id
   where e.employer_status <> 3 and
  p.min_payment_date::date < f.start_date::date  and b.rcv_date::date >= f.start_date::date and b.rcv_date::date <= f.end_date::date
  ) as receivable_contributors,
         (select count(distinct b.employer_id) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
   join ( select b1.employer_id from bookings b1 where b1.ispaid = 1 and deleted_at IS NULL and  b1.rcv_date::date >= f.start_date::date and b1.rcv_date::date <= f.end_date::date group by b1.employer_id) b2 on b2.employer_id = b.employer_id
   where e.employer_status <> 3 and
  p.min_payment_date::date < f.start_date::date  and b.rcv_date::date >= f.start_date::date and b.rcv_date::date <= f.end_date::date
  ) as receivable_contributors_employers,
  (select sum(b.booked_amount) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
  left join ( select b1.employer_id from bookings b1 where b1.ispaid = 1 and deleted_at IS NULL and  b1.rcv_date::date >= f.start_date::date and b1.rcv_date::date <= f.end_date::date group by b1.employer_id) b2 on b2.employer_id = b.employer_id
   where e.employer_status <> 3 and
  p.min_payment_date::date < f.start_date::date  and b.rcv_date::date >= f.start_date::date and b.rcv_date::date <= f.end_date::date and b2.employer_id is null
  ) as receivable_non_contributors,
         (select count(distinct b.employer_id) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
  left join ( select b1.employer_id from bookings b1 where b1.ispaid = 1 and deleted_at IS NULL and  b1.rcv_date::date >= f.start_date::date and b1.rcv_date::date <= f.end_date::date group by b1.employer_id) b2 on b2.employer_id = b.employer_id
   where e.employer_status <> 3 and
  p.min_payment_date::date < f.start_date::date  and b.rcv_date::date >= f.start_date::date and b.rcv_date::date <= f.end_date::date and b2.employer_id is null
  ) as receivable_non_contributors_employers,
    (select sum(b.booked_amount) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
   where e.employer_status <> 3 and
  p.min_payment_date::date >= f.start_date::date and  p.min_payment_date::date <= f.end_date::date and b.rcv_date <= f.end_date
  ) as receivable_new_entrants,
           (select count(distinct b.employer_id) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
   where e.employer_status <> 3 and
  p.min_payment_date::date >= f.start_date::date and  p.min_payment_date::date <= f.end_date::date and b.rcv_date <= f.end_date
  ) as receivable_new_entrants_employers,
           (select sum(b.booked_amount) from booking_receivables b  join employers e on e.id = b.employer_id
           join employer_closures c2 on e.id = c2.employer_id
   where e.employer_status = 3 and b.rcv_date >= f.start_date and b.rcv_date <= f.end_date and c2.close_date >= f.start_date and c2.close_date <= f.end_date
  ) as receivable_closed_business,
                  (select count(distinct b.employer_id) from booking_receivables b join employers e on e.id = b.employer_id
           join employer_closures c2 on e.id = c2.employer_id
   where e.employer_status = 3 and b.rcv_date >= f.start_date and b.rcv_date <= f.end_date and c2.close_date >= f.start_date and c2.close_date <= f.end_date
  ) as receivable_closed_business_employers,
           (select sum(b.booked_amount) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
      where e.employer_status <> 3 and p.min_payment_date < f.start_date and b.rcv_date < f.start_date
  ) as receivable_open_balance,
           (select count(distinct b.employer_id) from booking_receivables b join employers_min_payment_date p on b.employer_id = p.employer_id join employers e on e.id = p.employer_id
   where e.employer_status <> 3 and p.min_payment_date < f.start_date and b.rcv_date < f.start_date 
  ) as receivable_open_balance_employers
from fin_years f;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
