<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollRunsIsExported extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->smallInteger('isexported')->default(0);
        });

        DB::statement("comment on column payroll_runs.isexported is 'Flag to specify if pension is already exported to erp i.e. 1 => ready exported, 0 => not yet'");

        /*update column is claim*/
        DB::statement("comment on column claims_payable.isclaim is 'Flag to specify if transaction source i.e. 0 => non-claim, 1 => Claim benefits, 2 => Claim - Pension payroll'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
