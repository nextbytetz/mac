<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsContribOnContribBeforeOnNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->smallInteger("contrib_before")->default(0);
            $table->smallInteger("contrib_on")->default(0);
        });
        DB::statement("comment on column notification_reports.contrib_before is 'Whether one month before incident contribution were remitted '");
        DB::statement("comment on column notification_reports.contrib_on is 'Whether the contribution in the month of incident was remitted'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
