<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComplianceEmployerStatusView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \Illuminate\Support\Facades\DB::statement("DROP MATERIALIZED VIEW comp_employer_business_status");
        \Illuminate\Support\Facades\DB::statement("CREATE MATERIALIZED VIEW comp_employer_business_status AS
  SELECT
    o.active_public,
    p.active_private,
    q.dormant_public,
    r.dormant_private,
    s.closed_public,
    t.closed_private,
    u.all_employers,
    v.large_contributor,
    w.online_registered
  FROM (((((((((SELECT count(b.id) AS active_public
                FROM (main.employers b
                  JOIN main.code_values v_1 ON ((
                    (b.employer_category_cv_id = v_1.id) AND (b.employer_category_cv_id = 36) AND
                    (b.employer_status = 1) AND (b.approval_id = 1) AND (b.deleted_at IS NULL))))) o
    CROSS JOIN (SELECT count(b.id) AS active_private
                FROM (main.employers b
                  JOIN main.code_values v_1 ON ((
                    (b.employer_category_cv_id = v_1.id) AND (b.employer_category_cv_id = 37) AND
                    (b.employer_status = 1) AND (b.approval_id = 1) AND (b.deleted_at IS NULL))))) p)
    CROSS JOIN (SELECT count(b.id) AS dormant_public
                FROM (main.employers b
                  JOIN main.code_values v_1 ON ((
                    (b.employer_category_cv_id = v_1.id) AND (b.employer_category_cv_id = 36) AND
                    (b.employer_status = 2) AND (b.approval_id = 1) AND (b.deleted_at IS NULL))))) q)
    CROSS JOIN (SELECT count(b.id) AS dormant_private
                FROM (main.employers b
                  JOIN main.code_values v_1 ON ((
                    (b.employer_category_cv_id = v_1.id) AND (b.employer_category_cv_id = 37) AND
                    (b.employer_status = 2) AND (b.approval_id = 1) AND (b.deleted_at IS NULL))))) r)
    CROSS JOIN (SELECT count(b.id) AS closed_public
                FROM (main.employers b
                  JOIN main.code_values v_1 ON ((
                    (b.employer_category_cv_id = v_1.id) AND (b.employer_category_cv_id = 36) AND
                    (b.employer_status = 3) AND (b.approval_id = 1) AND (b.deleted_at IS NULL))))) s)
    CROSS JOIN (SELECT count(b.id) AS closed_private
                FROM (main.employers b
                  JOIN main.code_values v_1 ON ((
                    (b.employer_category_cv_id = v_1.id) AND (b.employer_category_cv_id = 37) AND
                    (b.employer_status = 3) AND (b.approval_id = 1) AND (b.deleted_at IS NULL))))) t)
    CROSS JOIN (SELECT count(b.id) AS all_employers
                FROM main.employers b
                WHERE ((b.approval_id = 1) AND (b.deleted_at IS NULL))) u)
    CROSS JOIN (SELECT count(*) AS large_contributor
                FROM main.comp_large_contributor) v)
    CROSS JOIN (SELECT count(DISTINCT eu.employer_id) AS online_registered
                FROM (portal.employer_user eu
                  JOIN main.employers e ON ((eu.employer_id = e.id)))
                WHERE (e.approval_id = 1)) w);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
