<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCommandToCommentsOnCheckers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkers', function (Blueprint $table) {
            $table->renameColumn("command", "comments");
        });
        /*Schema::table('notification_stages', function (Blueprint $table) {
            $table->json("comments")->nullable()->change();
        });*/
        DB::statement('alter table notification_stages alter column comments type json using comments::json');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
