<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesOnHealthProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('health_providers', function(Blueprint $table)
        {
            $table->bigInteger("health_provider_facility_type_cv_id")->nullable();
            $table->bigInteger("health_provider_ownership_cv_id")->nullable();
            $table->bigInteger("is_contracted")->default(0);
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('health_provider_facility_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('health_provider_ownership_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column health_providers.is_contracted is 'Show whether the health service provider has been contracted by WCF or not. 1 - Yes, 0 - No'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
