<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmDateColumnAndOtherOnDeaths extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deaths', function (Blueprint $table) {
            $table->date("confirm_date")->nullable();
            $table->date("incident_date")->nullable();
        });
        DB::statement("comment on column deaths.confirm_date is 'date when the death was confirmed by medical practitioner'");
        DB::statement("comment on column deaths.incident_date is 'date of the incident, if cause of death is occupation accident, then accident date. if cause of death is occupational disease, then date of diagnosis'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
