<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTreasuryContributionSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treasury_contribution_summary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('total_votes')->nullable();
            $table->integer('total_employees');
            $table->integer('total_pages');
            $table->date('check_date');
            $table->integer('loaded_employees')->nullable();
            $table->integer('loaded_pages')->nullable();
            $table->date('next_check_date')->nullable();
            $table->timestamp('start_date');
            $table->boolean('is_complete')->default(false);
            $table->string('status')->nullable();
            $table->timestamp('finish_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treasury_contribution_summary');
    }
}
