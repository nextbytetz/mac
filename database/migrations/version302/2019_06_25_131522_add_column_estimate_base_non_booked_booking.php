<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEstimateBaseNonBookedBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('non_booked_bookings', function(Blueprint $table)
        {

            $table->date('rcv_date')->change();
            $table->string('estimate_base')->nullable();
            $table->unique(['rcv_date', 'employer_id']);

        });

        Schema::table('non_booked_booking_interests', function(Blueprint $table)
        {
            $table->date('rcv_date')->change();
        });
    }

    /**
     *
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
