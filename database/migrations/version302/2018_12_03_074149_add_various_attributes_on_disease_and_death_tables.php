<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVariousAttributesOnDiseaseAndDeathTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diseases', function (Blueprint $table) {
            $table->bigInteger("user_id")->nullable();
            $table->bigInteger("health_provider_id")->nullable();
            $table->bigInteger("medical_practitioner_id")->nullable();
            $table->text("description")->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('health_provider_id')->references('id')->on('health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('medical_practitioner_id')->references('id')->on('medical_practitioners')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('witnesses', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('deaths', function (Blueprint $table) {
            $table->bigInteger("user_id")->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
