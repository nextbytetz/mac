<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsPayrollRecoveryTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_recovery_transactions', function(Blueprint $table)
        {
            $table->dropColumn('resource_id');
            $table->dropColumn('payroll_recovery_type_cv_id');
            $table->integer('payroll_recovery_id');
        });


        Schema::table('payroll_recovery_transactions', function(Blueprint $table)
        {
            $table->foreign('payroll_recovery_id')->references('id')->on('payroll_recoveries')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
