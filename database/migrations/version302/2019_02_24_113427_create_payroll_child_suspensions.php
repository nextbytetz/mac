<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollChildSuspensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_child_suspensions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('dependent_id');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_child_suspensions is 'Table to keep track of all child limit age suspensions by system'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
