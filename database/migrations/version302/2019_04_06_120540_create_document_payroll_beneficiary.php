<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentPayrollBeneficiary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('document_payroll_beneficiary', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('resource_id');
            $table->integer('member_type_id');
            $table->integer('document_id');
            $table->string('name')->nullale();
            $table->text('description')->nullale();
            $table->integer('eoffice_document-id');
            $table->smallInteger('isused')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table document_payroll_beneficiary is 'Table to keep track of all documents attached to payroll beneficiary'");
        DB::statement("comment on column document_payroll_beneficiary.isused is 'Flag to imply if document is already used for validation on i.e. 0 => pending , 1 => worked on'");

        Schema::table('document_payroll_beneficiary', function(Blueprint $table)
        {
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
