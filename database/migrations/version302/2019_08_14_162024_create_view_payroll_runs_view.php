<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewPayrollRunsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW IF EXISTS  payroll_runs_view");
        DB::statement("CREATE VIEW payroll_runs_view AS 
select payroll_beneficiaries_view.member_name,
       payroll_runs.amount as amount,
       payroll_runs.months_paid,
       payroll_runs.monthly_pension,
       payroll_runs.payroll_run_approval_id ,
       payroll_run_approvals.wf_done,
       payroll_runs.arrears_amount,
       payroll_runs.deductions_amount,
       payroll_runs.unclaimed_amount,
       payroll_runs.employee_id,
       member_types.name as member_type_name,
       payroll_procs.run_date as run_date,
       bank_branches.name as bank_branch_name,
       banks.name as bank_name,
       payroll_runs.accountno as run_accountno,
       payroll_runs.bank_branch_id as run_bank_branch_id,
       payroll_runs.bank_id as run_bank_id,
       payroll_runs.resource_id as resource_id,
       payroll_runs.member_type_id as member_type_id,
       payroll_runs.new_payee_flag as new_payee_flag,
           payroll_runs.isconstantcare as isconstantcare,
       payroll_beneficiaries_view.filename

from payroll_runs
       join payroll_run_approvals on payroll_runs.payroll_run_approval_id = payroll_run_approvals.id
       join payroll_procs on payroll_run_approvals.payroll_proc_id = payroll_procs.id
       join member_types on member_types.id = payroll_runs.member_type_id
       join payroll_beneficiaries_view on payroll_beneficiaries_view.resource_id = payroll_runs.resource_id and payroll_beneficiaries_view.member_type_id = payroll_runs.member_type_id
       left join bank_branches on bank_branches.id = payroll_runs.bank_branch_id
       left join banks on banks.id = payroll_runs.bank_id;

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
