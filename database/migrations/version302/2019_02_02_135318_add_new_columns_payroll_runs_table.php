<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsPayrollRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->dropColumn('employee_id');
            $table->dropColumn('payroll_proc_id');
            $table->integer('payroll_run_approval_id')->nullable();
            $table->decimal('arrears_amount', 14)->default(0)->comment('Total arrears amount paid on this payroll i.e. underpayment, unclaimed and suspended payments');
            $table->decimal('deductions_amount', 14)->default(0)->comment('Total deductions amount deducted on this payroll i.e. over-payments');
            $table->integer('notification_report_id')->nullable();
        });

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('payroll_run_approval_id')->references('id')->on('payroll_run_approvals')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
