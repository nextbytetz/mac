<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FlagoffAlreadyReferenceBenefitDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL

-- flag off already used documents to initiate benefit documents
-- for medical expense employee/employer
update document_notification_report set isreferenced = 1 where notification_report_id in (select notification_report_id from notification_eligible_benefits where benefit_type_claim_id = 1) and document_id in (30);
-- for td payment
update document_notification_report set isreferenced = 1 where notification_report_id in (select notification_report_id from notification_eligible_benefits where benefit_type_claim_id = 3) and document_id in (19, 21);
--for td refund payment
update document_notification_report set isreferenced = 1 where notification_report_id in (select notification_report_id from notification_eligible_benefits where benefit_type_claim_id = 4) and document_id in (18, 20);

SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
