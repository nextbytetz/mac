<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForWorkflowAutoAlocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_allocations', function(Blueprint $table)
        {
            $table->increments("id");
            $table->bigInteger("wf_definition_id");
            $table->smallInteger("pointer")->default(0);
            $table->timestamps();
            $table->foreign('wf_definition_id')->references('id')->on('wf_definitions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table wf_allocations is 'table to store allocation pointer for the wf definitions of workflows'");
        DB::statement("comment on column wf_allocations.pointer is 'pointer to the next person to be allocated for the workflow'");

        Schema::table('wf_module_groups', function(Blueprint $table)
        {
            $table->smallInteger("autolocate")->default(0);
        });

        DB::statement("comment on column wf_module_groups.autolocate is 'specify whether the workflow group has automatic user assignment at workflow levels using wf_allocations table 1 - Yes, 0 - No'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
