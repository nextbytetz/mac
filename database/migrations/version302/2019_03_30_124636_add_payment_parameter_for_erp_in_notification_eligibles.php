<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentParameterForErpInNotificationEligibles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function(Blueprint $table)
        {
            $table->smallInteger('ispayprocessed')->default(0);
            $table->smallInteger('ispaid')->default(0);
            $table->string("chequeno")->nullable();
            $table->string("paymethod")->nullable();
            $table->string("payreference")->nullable();
        });
        DB::statement("comment on column notification_eligible_benefits.ispayprocessed is 'show whether the payment for this benefit has already been posted to claim compensations table ready for being paid. This column is updated when finance officer is ready to pay during benefit workflow'");
        DB::statement("comment on column notification_eligible_benefits.ispaid is 'show whether the payment has already been paid in erp application'");
        DB::statement("comment on column notification_eligible_benefits.chequeno is 'Chequeno posted from ERP application'");
        DB::statement("comment on column notification_eligible_benefits.paymethod is 'Pay method posted from ERP application'");
        DB::statement("comment on column notification_eligible_benefits.payreference is 'Pay reference posted from ERP application'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
