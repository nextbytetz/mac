<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNonBookedBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('non_booked_bookings', function(Blueprint $table)
        {
            $table->increments('id');
            $table->dateTime('rcv_date');
            $table->integer('employer_id');
            $table->smallInteger('fin_code_id');
            $table->decimal('amount', 14,2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('non_booked_bookings', function(Blueprint $table)
        {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fin_code_id')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });



        Schema::create('non_booked_booking_interests', function(Blueprint $table)
        {
            $table->increments('id');
            $table->dateTime('rcv_date');
            $table->integer('non_booked_booking_id');
            $table->integer('late_months');
            $table->decimal('interest_percent');
            $table->decimal('amount', 14,2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('non_booked_booking_interests', function(Blueprint $table)
        {
            $table->foreign('non_booked_booking_id')->references('id')->on('non_booked_bookings')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
