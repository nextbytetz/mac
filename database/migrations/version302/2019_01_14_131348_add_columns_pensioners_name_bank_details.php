<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPensionersNameBankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->string('firstname', 100)->nullable();
            $table->string('middlename', 100)->nullable();
            $table->string('lastname', 100)->nullable();;

            $table->integer('bank_branch_id')->nullable();
            $table->string('accountno', 45)->nullable();
        });

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        $sql = <<<SQL
update pensioners
set firstname = (
  select employees.firstname
  from employees
  where employees.id = pensioners.employee_id
),
  middlename = (
    SELECT employees.middlename
    FROM employees
    WHERE employees.id = pensioners.employee_id
  ),
  lastname = (
    SELECT employees.lastname
    FROM employees
    WHERE employees.id = pensioners.employee_id
  ),
  bank_branch_id = (
    SELECT employees.bank_branch_id
    FROM employees
    WHERE employees.id = pensioners.employee_id
  ),
  accountno = (
    SELECT employees.accountno
    FROM employees
    WHERE employees.id = pensioners.employee_id
  )
  where exists (
      select *
      from employees
      where employees.id = pensioners.employee_id
  );
SQL;
        DB::unprepared($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
