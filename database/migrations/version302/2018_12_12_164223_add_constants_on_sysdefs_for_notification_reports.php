<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstantsOnSysdefsForNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->smallInteger("max_allowed_reporting_notification")->default(12);
            $table->smallInteger("notification_cad_pointer")->default(1);
            $table->smallInteger("investigate_cas_pointer")->default(1);
            $table->smallInteger("investigate_cad_pointer")->default(1);
            $table->smallInteger("max_telephone_investigation_score")->default(4);
        });
        DB::statement("comment on column sysdefs.max_allowed_reporting_notification is 'maximum number of months allowed for reporting notification incident. Difference between incident date and receipt date'");
        DB::statement("comment on column sysdefs.notification_cad_pointer is 'allocate control pointer for the created notification incidents.'");
        DB::statement("comment on column sysdefs.investigate_cas_pointer is 'allocate control pointer for the staff in investigation from claim assessment department'");
        DB::statement("comment on column sysdefs.investigate_cad_pointer is 'allocate control pointer for the staff in investigation from claim administration department'");
        DB::statement("comment on column sysdefs.max_telephone_investigation_score is 'maximum score for the investigation to be regarded as telephone'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
