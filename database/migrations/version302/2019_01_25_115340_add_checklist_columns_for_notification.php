<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChecklistColumnsForNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->date("last_work_date")->nullable();
            $table->smallInteger("paid_month_before")->nullable();
        });
        DB::statement("comment on column notification_reports.last_work_date is 'date last worked'");
        DB::statement("comment on column notification_reports.paid_month_before is 'paid salary for the last month before incident'");

        Schema::table('accidents', function (Blueprint $table) {
            $table->smallInteger("salary_being_continued")->nullable();
            $table->smallInteger("salary_full_continued")->nullable();
            $table->bigInteger("accident_cause_cv_id")->nullable();
            $table->smallInteger("lost_body_part")->nullable();
            $table->bigInteger("accident_incident_exposure_cv_id")->nullable();
            $table->date("initial_treatment_date")->nullable();
            $table->smallInteger("on_treatment")->nullable();
            $table->date("last_treatment_date")->nullable();
            $table->smallInteger("return_to_work")->nullable();
            $table->date("return_to_work_date")->nullable();
            $table->bigInteger("job_title_id")->nullable();
            $table->foreign('accident_incident_exposure_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('accident_cause_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('job_title_id')->references('id')->on('job_titles')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column accidents.salary_being_continued is 'salary being continued after the accident'");
        DB::statement("comment on column accidents.salary_full_continued is 'salary being continued in full'");
        DB::statement("comment on column accidents.accident_cause_cv_id is 'cause of accident'");
        DB::statement("comment on column accidents.lost_body_part is 'does any part of the body has been lost. 1 - Yes, 0 - No'");
        DB::statement("comment on column accidents.on_treatment is 'show whether the member is still on treatment or not. 1 - Yes, 0 - No'");
        DB::statement("comment on column accidents.return_to_work is 'show whether the member has returned to work or not. 1 - Yes, 0 - No'");
        DB::statement("comment on column accidents.job_title_id is 'job title of the member on returning to work'");
        DB::statement("comment on column accidents.initial_treatment_date is 'first date to start receiving treatment'");
        DB::statement("comment on column accidents.last_treatment_date is 'last date attended hospital for treatment'");

        Schema::table('diseases', function (Blueprint $table) {
            $table->smallInteger("salary_being_continued")->nullable();
            $table->smallInteger("salary_full_continued")->nullable();
            $table->bigInteger("disease_cause_cv_id")->nullable();
            $table->smallInteger("lost_body_part")->nullable();
            $table->bigInteger("disease_incident_exposure_cv_id")->nullable();
            $table->date("initial_treatment_date")->nullable();
            $table->smallInteger("on_treatment")->nullable();
            $table->date("last_treatment_date")->nullable();
            $table->smallInteger("return_to_work")->nullable();
            $table->date("return_to_work_date")->nullable();
            $table->bigInteger("job_title_id")->nullable();
            $table->foreign('disease_incident_exposure_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('disease_cause_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('job_title_id')->references('id')->on('job_titles')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column diseases.salary_being_continued is 'salary being continued after the accident'");
        DB::statement("comment on column diseases.salary_full_continued is 'salary being continued in full'");
        DB::statement("comment on column diseases.disease_cause_cv_id is 'cause of disease'");
        DB::statement("comment on column diseases.lost_body_part is 'does any part of the body has been lost. 1 - Yes, 0 - No'");
        DB::statement("comment on column diseases.on_treatment is 'show whether the member is still on treatment or not. 1 - Yes, 0 - No'");
        DB::statement("comment on column diseases.return_to_work is 'show whether the member has returned to work or not. 1 - Yes, 0 - No'");
        DB::statement("comment on column diseases.job_title_id is 'job title of the member on returning to work'");
        DB::statement("comment on column diseases.initial_treatment_date is 'first date to start receiving treatment'");
        DB::statement("comment on column diseases.last_treatment_date is 'last date attended hospital for treatment'");

        Schema::table('deaths', function (Blueprint $table) {
            $table->bigInteger("death_incident_exposure_cv_id")->nullable();
            $table->string("burial_permit_no")->nullable();
            $table->string("death_certificate_no")->nullable();
            $table->foreign('death_incident_exposure_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::create('notification_injuries', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->bigInteger("body_part_injury_cv_id");
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('body_part_injury_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_injuries is 'multiple body part injuries for a particular incident'");

        Schema::create('notification_lost_body_parts', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->bigInteger("lost_body_part_cv_id");
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('lost_body_part_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_lost_body_parts is 'multiple lost body parts for a particular incident'");

        Schema::create('notification_treatments', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->bigInteger("health_provider_id")->index();
            $table->smallInteger("rank");
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('health_provider_id')->references('id')->on('health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_treatments is 'record the number of medical treatments recorded during checklist'");
        DB::statement("comment on column notification_treatments.rank is 'the order of treatment from first to last'");

        Schema::create('notification_treatment_services', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_treatment_id")->index();
            $table->bigInteger("health_service_checklist_id")->index();
            $table->smallInteger("days");
            $table->timestamps();
            $table->foreign('notification_treatment_id')->references('id')->on('notification_treatments')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('health_service_checklist_id')->references('id')->on('health_service_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_treatment_services is 'record the list of services rendered for a particular notification treatment'");
        DB::statement("comment on column notification_treatment_services.days is 'number of days a member spent receiving the health service'");

        Schema::create('notification_treatment_practitioners', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_treatment_id")->index();
            $table->bigInteger("medical_practitioner_id")->index();
            $table->timestamps();
            $table->foreign('notification_treatment_id')->references('id')->on('notification_treatments')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('medical_practitioner_id')->references('id')->on('medical_practitioners')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_treatment_practitioners is 'record the list of medical practitioners involved in a particular notification treatment'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
