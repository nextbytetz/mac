<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingReceivableViewAmountPaid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
                DB::statement("DROP VIEW booking_receivables");
        DB::statement("CREATE VIEW booking_receivables AS   SELECT
    b.employer_id,
    b.id                                                                                                           AS booking_id,
    e.name                                                                                                         AS employer_name,
    e.reg_no                                                                                                       AS employer_reg_no,
    b.rcv_date,
    b.amount                                                                                                       AS booked_amount,
coalesce(    (SELECT sum(receipt_codes.amount) AS sum
     FROM (main.receipt_codes
       JOIN main.receipts ON ((receipt_codes.receipt_id = receipts.id)))
     WHERE (((receipt_codes.fin_code_id = 2) OR (receipt_codes.fin_code_id = 63)) AND (receipts.iscancelled = 0) AND
            (receipts.isdishonoured = 0) AND (receipts.deleted_at IS NULL) AND (receipt_codes.booking_id =
                                                                                b.id)))   , 0)                         AS amount_paid
  FROM (main.bookings b
    JOIN main.employers e ON ((b.employer_id = e.id)))
  WHERE (b.deleted_at IS NULL);
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
