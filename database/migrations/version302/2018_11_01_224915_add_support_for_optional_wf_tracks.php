<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupportForOptionalWfTracks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function(Blueprint $table)
        {
            $table->dropColumn('select_next_start_optional');
        });
        Schema::table('wf_tracks', function(Blueprint $table)
        {
            $table->smallInteger('select_next_start_optional')->default(0);
        });

        DB::statement("comment on column wf_tracks.select_next_start_optional is 'show whether the next optional workflow has been chosen'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
