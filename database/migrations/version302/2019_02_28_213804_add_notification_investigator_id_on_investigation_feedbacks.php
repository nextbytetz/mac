<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationInvestigatorIdOnInvestigationFeedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investigation_feedbacks', function (Blueprint $table) {
            $table->bigInteger("notification_investigator_id")->nullable()->index();
            $table->foreign('notification_investigator_id')->references('id')->on('notification_investigators')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
