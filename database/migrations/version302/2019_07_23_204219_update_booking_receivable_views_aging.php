<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingReceivableViewsAging extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW booking_receivables");
        DB::statement("CREATE VIEW booking_receivables AS       SELECT
    b.employer_id,
    b.id     AS booking_id,
    e.name   AS employer_name,
    e.reg_no AS employer_reg_no,
    b.rcv_date,
    b.amount AS booked_amount,
    0        AS amount_paid,
   (DATE_PART('year', NOW()::TIMESTAMP) - DATE_PART('year', b.rcv_date::date)) * 12 +
   ((DATE_PART('month', NOW()::TIMESTAMP) - DATE_PART('month', b.rcv_date::date)) - 1) AS age_months,
   DATE_PART('day', NOW()::timestamp - b.rcv_date::timestamp) AS age_days
  FROM (main.bookings b
    JOIN main.employers e ON ((b.employer_id = e.id)))
  WHERE ((b.deleted_at IS NULL) AND (b.ispaid = 0) AND (e.is_treasury = FALSE) AND (e.duplicate_id IS NULL) AND
         (e.approval_id = 1) AND (e.deleted_at IS NULL))
  ORDER BY b.employer_id, b.rcv_date;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
