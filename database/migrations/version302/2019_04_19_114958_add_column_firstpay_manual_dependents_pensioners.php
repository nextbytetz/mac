<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFirstpayManualDependentsPensioners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dependents', function(Blueprint $table)
        {
            $table->smallInteger('firstpay_manual')->default(2);

        });

        DB::statement("comment on column dependents.firstpay_manual is 'Flag to imply if dependent was first paid from manual process of payroll i.e. 1 => Started from manual, 0=> started from system, 2  => pending for merging'");

        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->smallInteger('firstpay_manual')->default(2);

        });

        DB::statement("comment on column pensioners.firstpay_manual is 'Flag to imply if pensioner was first paid from manual process of payroll i.e. 1 => Started from manual, 0=> started from system,  2  => pending for merging'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
