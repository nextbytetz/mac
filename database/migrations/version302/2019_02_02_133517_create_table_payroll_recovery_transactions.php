<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrollRecoveryTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_recovery_transactions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payroll_run_approval_id');
            $table->integer('resource_id')->comment('Primary key of the table o payroll recoveries i.e. payroll_arrears, payroll_deductions, payroll_unclaimed');
            $table->integer('payroll_recovery_type_cv_id');
            $table->decimal('amount', 14)->default(0)->comment('Total amount eligible to be recovered ');
               $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table payroll_recovery_transactions is 'Table to keep track of all recovery of all individuals' ");


        Schema::table('payroll_recovery_transactions', function(Blueprint $table)
        {
            $table->foreign('payroll_run_approval_id')->references('id')->on('payroll_run_approvals')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('payroll_recovery_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
