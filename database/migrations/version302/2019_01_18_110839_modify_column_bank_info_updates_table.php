<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnBankInfoUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_bank_info_updates', function(Blueprint $table)
        {
            $table->integer('old_bank_branch_id')->nullable()->change();
            $table->string('old_accountno', 45)->nullable()->change();
            $table->dateTime('wf_done_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
