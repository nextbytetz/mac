<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasnoteOnWfDefinition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->smallInteger("has_note")->default(0);
        });
        DB::statement("comment on column wf_definitions.has_note is 'this is for approval workflows, indicate whether a level should display a summary statement for the approving personnel to review for decision making'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
