<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingReceivableView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        DB::statement("DROP VIEW booking_receivables");
        DB::statement("CREATE VIEW booking_receivables AS SELECT  b.employer_id as employer_id,  b.id as booking_id, e.name as employer_name, e.reg_no as employer_reg_no, b.rcv_date, b.amount as booked_amount, (select sum(receipt_codes.amount) from receipt_codes JOIN receipts  on receipt_codes.receipt_id = receipts.id
where (receipt_codes.fin_code_id = 2 or receipt_codes.fin_code_id = 63) AND
receipts.iscancelled = 0 and receipts.isdishonoured = 0 and receipts.deleted_at is null AND receipt_codes.booking_id = b.id) as amount_paid
FROM main.bookings as b
JOIN main.employers as e on b.employer_id = e.id
where
  b.deleted_at is null

");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
