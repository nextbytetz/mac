<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaePaymentReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_mae', function(Blueprint $table)
        {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->bigInteger("notification_eligible_benefit_id")->nullable()->index();
            $table->smallInteger("mae_refund_member_ready")->default(0);
            $table->date("mae_refund_member_ready_date")->nullable();
            $table->date("mae_refund_member_pay_date")->nullable();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('incident_tds', function(Blueprint $table)
        {
            $table->bigInteger("notification_eligible_benefit_id")->nullable()->index();
            $table->foreign('notification_eligible_benefit_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table incident_mae is 'store the information of mae benefit initiation and payment dates.'");
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->bigInteger("incident_mae_id")->nullable()->index();
            $table->foreign('incident_mae_id')->references('id')->on('incident_mae')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
