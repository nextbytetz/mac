<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicalExpenseSubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_expenses', function(Blueprint $table)
        {
            $table->date('receive_date')->nullable();
            $table->text("remarks")->nullable();
        });
        DB::statement("comment on column medical_expenses.receive_date is 'date when the medical expense bill has been received at the fund.'");
        DB::statement("comment on column medical_expenses.remarks is 'any additional information about this medical expense'");

        Schema::create('mae_subs', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("medical_expense_id")->index();
            $table->string("rctno", 100);
            $table->decimal("amount", 14);
            $table->bigInteger("health_provider_id")->nullable();
            $table->decimal("assessed_amount")->nullable();
            $table->timestamps();
            $table->foreign('medical_expense_id')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table mae_subs is 'table to store sub items of the medical expenses'");
        DB::statement("comment on column mae_subs.rctno is 'receipt number of the sub payment'");

        Schema::create('mae_services', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("mae_sub_id")->index();
            $table->bigInteger("health_service_checklist_id");
            $table->timestamps();
            $table->foreign('mae_sub_id')->references('id')->on('mae_subs')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('health_service_checklist_id')->references('id')->on('health_service_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table mae_services is 'record the list of health service checklists per each mae_subs entry'");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
