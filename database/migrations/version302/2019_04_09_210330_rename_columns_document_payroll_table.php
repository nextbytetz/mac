<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsDocumentPayrollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('document_payroll', function(Blueprint $table)
        {
            $table->renameColumn( 'document_date','doc_date');
            $table->renameColumn('receive_date', 'doc_receive_date');
            $table->renameColumn('document_create_date',  'doc_create_date');
            $table->renameColumn('folionumber', 'folio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
