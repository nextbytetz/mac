<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationEligibleBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_eligible_benefits', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id");
            $table->bigInteger("benefit_type_id");
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('benefit_type_id')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_eligible_benefits is 'store all the eligible benefits which a specific incident is entitled to be paid'");

        Schema::table('notification_workflows', function (Blueprint $table) {
            $table->bigInteger("benefit_type_id")->nullable();
            $table->foreign('benefit_type_id')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('claim_compensations', function (Blueprint $table) {
            $table->bigInteger("notification_workflow_id")->nullable();
            $table->foreign('notification_workflow_id')->references('id')->on('notification_workflows')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
