<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrollRunApprovals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_run_approvals', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('payroll_proc_id');
            $table->decimal('total_net_amount', 14)->default(0)->comment('Tota net amount eligible to be paid');
            $table->decimal('total_arrears_amount', 14)->default(0)->comment('Total arrears amount eligible to be paid i.e.underpayments, unclaimed, suspended payments');
            $table->decimal('total_deductions_amount', 14)->default(0)->comment('Total deductions amount eligible to be deducted i.e.over-payments');
            $table->decimal('total_suspended_amount', 14)->default(0)->comment('Total suspended amount from suspended pensioners and dependents');
            $table->integer('number_of_pensioners')->default(0)->comment('Number of pensioners eligible for payroll');
            $table->integer('number_of_dependents')->default(0)->comment('Number of dependents eligible for payroll');
            $table->smallInteger('status')->default(0)->comment('Status of the payroll run ; 0 => pending, 1 => Initiated, 2 => Rejected');
            $table->smallInteger('wf_done')->default(0);
            $table->dateTime('wf_done_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table payroll_run_approvals is 'Table to keep track of all monthy payroll approvals' ");


        Schema::table('payroll_run_approvals', function(Blueprint $table)
        {
            $table->foreign('payroll_proc_id')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
