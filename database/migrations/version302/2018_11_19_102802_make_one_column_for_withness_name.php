<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeOneColumnForWithnessName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('witnesses', function(Blueprint $table)
        {
            $table->string("lastname", 45)->nullable()->change();
            $table->string("firstname", 45)->nullable()->change();
            $table->string("name", 45)->nullable();
        });
        DB::statement("update witnesses set name = concat_ws(' ', firstname, coalesce(middlename, ''), lastname);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
