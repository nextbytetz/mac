<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllowRepeatOnWfModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_modules', function (Blueprint $table) {
            $table->smallInteger("allow_repeat")->default(0);
        });
        Schema::table('notification_workflows', function (Blueprint $table) {
            $table->dropColumn("allow_repeat");
        });
        DB::statement("comment on column wf_modules.allow_repeat is 'show whether for this module, a specific resource can have more than one workflow trip. 1 - Yes, 0 - No'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
