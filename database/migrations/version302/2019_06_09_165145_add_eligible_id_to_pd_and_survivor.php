<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEligibleIdToPdAndSurvivor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->bigInteger("pd_eligible_id")->nullable();
            $table->bigInteger("survivor_eligible_id")->nullable();
            $table->foreign('pd_eligible_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('survivor_eligible_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column notification_reports.pd_eligible_id is 'reference to the notification_eligible_id when the benefit approval process is initiated. Help to flag off the initiated pd from pending to be initiated'");
        DB::statement("comment on column notification_reports.survivor_eligible_id is 'reference to the notification_eligible_id when the benefit approval process is initiated. Help to flag off the initiated survivor from pending to be initiated'");

        $sql = <<<SQL

-- Insert to close for already initiated benefit for pd and survivor
-- Update for PD
update notification_reports set pd_eligible_id = vv.eligible_id from (select a.id eligible_id, a.notification_report_id from notification_eligible_benefits a where a.benefit_type_claim_id = 5 and a.parent_id is null) vv where vv.notification_report_id = notification_reports.id;
-- Update for Survivor
update notification_reports set survivor_eligible_id = vv.eligible_id from (select a.id eligible_id, a.notification_report_id from notification_eligible_benefits a where a.benefit_type_claim_id = 7 and a.parent_id is null) vv where vv.notification_report_id = notification_reports.id;


SQL;

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
