<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupportForOptionalWorkflowLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function(Blueprint $table)
        {
            $table->smallInteger('has_next_start_optional')->default(0);
            $table->smallInteger('is_optional')->default(0);
            $table->smallInteger('select_next_start_optional')->default(0);
        });
        DB::statement("comment on column wf_definitions.has_next_start_optional is 'Show whether the next level is optional. Help to enable user to decide whether the next level should be chosen is preferred of not'");
        DB::statement("comment on column wf_definitions.is_optional is 'show whether this level optional and can be skipped'");
        DB::statement("comment on column wf_definitions.select_next_start_optional is 'show whether the next optional workflow has been chosen'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
