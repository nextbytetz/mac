<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTreasuryFailedPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('treasury_failed_pages', function (Blueprint $table) {
       $table->increments('id');
       $table->integer('treasury_contribution_summary_id')->unsigned();
       $table->integer('page_number');
       $table->string('error_code');
       $table->text('reason');
       $table->boolean('is_rerun')->default(false);
       $table->timestamps();
       $table->foreign('treasury_contribution_summary_id')->references('id')->on('main.treasury_contribution_summary')->onUpdate('CASCADE')->onDelete('RESTRICT');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('treasury_failed_pages');

    }
  }
