<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUploadErrorManualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('manual_payroll_runs', function(Blueprint $table)
        {
            $table->text('upload_error')->nullable();
        });

        Schema::table('manual_notification_reports', function(Blueprint $table)
        {
            $table->text('upload_error')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
