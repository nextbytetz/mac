<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnPaymentVouchersPayrollProcId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payment_vouchers', function(Blueprint $table)
        {
            $table->integer('payroll_proc_id')->nullable()->change();
        });


        Schema::table('payment_voucher_transactions', function(Blueprint $table)
        {
            $table->integer('claim_compensation_id')->nullable();
        });

        Schema::table('payment_voucher_transactions', function (Blueprint $table) {
                 $table->foreign('claim_compensation_id')->references('id')->on('claim_compensations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
