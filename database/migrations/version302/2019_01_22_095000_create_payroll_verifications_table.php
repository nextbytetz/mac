<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_verifications', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('resource_id');
            $table->integer('member_type_id');
            $table->integer('identity_id')->nullable();
            $table->string('id_no',100)->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_verifications is 'Table to keep track of all beneficiary verifications'");

        Schema::table('payroll_verifications', function(Blueprint $table)
        {
            $table->foreign('identity_id')->references('id')->on('identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_verifications');
    }
}
