<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsderivedOnNotificationEligibleNenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_eligible_benefits', function (Blueprint $table) {
            $table->smallInteger("parent_id")->nullable();
            $table->foreign('parent_id')->references('id')->on('notification_eligible_benefits')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_eligible_benefits.parent_id is 'show whether this benefit is derived from another benefit and thus does not require approval'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
