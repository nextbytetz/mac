<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAttributesOnCheckerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->smallInteger("iscomplete_register")->default(1);
        });
        DB::statement("comment on column notification_reports.iscomplete_register is 'show whether the incident has a complete registered notification or not. 1 - Yes, 0 - No'");
        Schema::table('checkers', function (Blueprint $table) {
            $table->dropColumn("table_name");
            $table->dropColumn("command");
        });
        Schema::table('checkers', function (Blueprint $table) {
            $table->json("command");
            $table->string("resource_type", 150);
            $table->bigInteger("user_id");
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column checkers.command is 'list instructions to be performed and additional information for this checker resource'");
        DB::statement("comment on column checkers.status is 'show whether the resource has been worked. 0 - Pending, 1 - Issued'");
        Schema::create('notification_letters', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id");
            $table->text("acknowledgement")->nullable();
            $table->bigInteger("acknowledgement_user")->nullable();
            $table->text("reminder")->nullable();
            $table->bigInteger("reminder_user")->nullable();
            $table->text("award")->nullable();
            $table->bigInteger("award_user")->nullable();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('acknowledgement_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('reminder_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('award_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column notification_letters.acknowledgement_user is 'user who issued acknowledgement letter'");
        DB::statement("comment on column notification_letters.reminder_user is 'user who issued reminder letter'");
        DB::statement("comment on column notification_letters.award_user is 'user who issues award letter'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
