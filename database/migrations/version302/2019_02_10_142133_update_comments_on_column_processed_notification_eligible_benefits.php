<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommentsOnColumnProcessedNotificationEligibleBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column notification_reports.wf_done is 'shows whether the incident has been processed to closure. For the case of progressive incident, until all the benefits has been processed. 1 - Yes, 0 - No, 2 - Declined'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
