<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollRemainderPensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_remainder_pensions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_type_id');
            $table->integer('resource_id');
            $table->decimal('amount', 14);
            $table->smallInteger('status')->default(0)->comment('Flag to specify status of this payment process i.e 0 => pending, 1 => Initiated, 2=> Rejected ');
            $table->smallInteger('wf_done')->default(0);
            $table->dateTime('wf_done_date');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("comment on table payroll_remainder_pensions is 'Table to keep track of all approvals of remained pension amount when removing beneficiary from payroll. Remained amount after reconciliation'");

        Schema::table('payroll_remainder_pensions', function(Blueprint $table)
        {
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
