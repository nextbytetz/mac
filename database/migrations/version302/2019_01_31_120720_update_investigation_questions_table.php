<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvestigationQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigation_question_incident_type', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->bigInteger("investigation_question_id");
            $table->bigInteger("incident_type_id");
            $table->timestamps();
            $table->foreign('investigation_question_id')->references('id')->on('investigation_questions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('incident_type_id')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table investigation_question_incident_type is 'relate the investigation questions to the incident types'");

        Schema::table('investigation_questions', function (Blueprint $table) {
            $table->string("data_type_source")->nullable();
        });
        DB::statement("comment on column investigation_questions.data_type_source is 'associated source of data for the data_type select'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
