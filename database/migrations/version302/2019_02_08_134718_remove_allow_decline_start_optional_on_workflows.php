<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAllowDeclineStartOptionalOnWorkflows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_modules', function (Blueprint $table) {
            $table->dropColumn("allow_decline");
        });
        Schema::table('wf_tracks', function (Blueprint $table) {
            $table->dropColumn("select_next_start_optional");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
