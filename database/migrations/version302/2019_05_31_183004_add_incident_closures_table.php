<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncidentClosuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_closures', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("notification_report_id")->index();
            $table->date("register_date");
            $table->date("close_date")->nullable();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::create('incident_tds', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("notification_report_id")->index();
            $table->smallInteger("td_ready")->default(0);
            $table->date("td_ready_date")->nullable();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->bigInteger('incident_closure_id')->nullable();
            $table->bigInteger("incident_td_id")->nullable();
            $table->date("pd_ready_date")->nullable();
            $table->date("td_refund_ready_date")->nullable();
            $table->foreign('incident_closure_id')->references('id')->on('incident_closures')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('incident_td_id')->references('id')->on('incident_tds')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on table incident_tds is 'store the frequency of paying temporary disablement benefit'");
        DB::statement("comment on column incident_tds.td_ready is 'show whether td is ready ready to be paid or not'");
        DB::statement("comment on column incident_tds.td_ready_date is 'date when temporary disablement is ready to be processed, after submission of complete documentation'");
        DB::statement("comment on column notification_reports.td_refund_ready_date is 'date when temporary disablement refund is ready to be processed, after submission of complete documentation'");
        DB::statement("comment on column notification_reports.pd_ready_date is 'date when permanent disablement is ready to be processed, after submission of complete documentation'");
        DB::statement("comment on table incident_closures is 'for tracking opening and closing of notification file'");
        DB::statement("comment on column notification_reports.incident_closure_id is 'store the current opening and closing of file status'");
        DB::statement("comment on column incident_closures.register_date is 'date when notification has been registered'");
        DB::statement("comment on column incident_closures.close_date is 'date when notification has been closed'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
