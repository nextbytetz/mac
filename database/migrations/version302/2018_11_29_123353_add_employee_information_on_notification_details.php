<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeInformationOnNotificationDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_reports', function (Blueprint $table) {
            $table->date("datehired")->nullable();
            $table->text("job_title")->nullable();
        });
        DB::statement("comment on column notification_reports.employer_id is 'The employer which the employee had an incident'");
        DB::statement("comment on column notification_reports.datehired is 'date hired for the employer at the time of an incident'");
        DB::statement("comment on column notification_reports.job_title is 'job title for the employer at the time of an incident'");
        Schema::create('notification_employers', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("notification_report_id")->index();
            $table->bigInteger("employer_id")->index();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table notification_employers is 'store all the active employers during the registration of an incident'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
