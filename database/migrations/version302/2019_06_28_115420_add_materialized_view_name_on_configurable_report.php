<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaterializedViewNameOnConfigurableReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurable_reports', function(Blueprint $table)
        {
            $table->string("materialized_view")->nullable()->unique();
        });
        DB::statement("comment on column configurable_reports.materialized_view is 'name of the materialized view with the financial year status'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
