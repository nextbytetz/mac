<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsPayrollSupportTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*payrolls status change*/
        Schema::table('payroll_status_changes', function(Blueprint $table)
        {
            $table->integer('employee_id')->nullable();
            $table->dropColumn('notification_report_id');
            $table->dropColumn('manual_notification_report_id');
        });


        Schema::table('payroll_status_changes', function(Blueprint $table)
        {
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('payroll_recoveries', function(Blueprint $table)
        {
            $table->integer('employee_id')->nullable();
            $table->dropColumn('notification_report_id');
            $table->dropColumn('manual_notification_report_id');
        });


        Schema::table('payroll_recoveries', function(Blueprint $table)
        {
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->integer('employee_id')->nullable();
            $table->dropColumn('notification_report_id');
            $table->dropColumn('manual_notification_report_id');
        });


        Schema::table('payroll_runs', function(Blueprint $table)
        {
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->integer('employee_id')->nullable();
            $table->dropColumn('notification_report_id');
            $table->dropColumn('manual_notification_report_id');
        });


        Schema::table('payroll_suspended_runs', function(Blueprint $table)
        {
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
