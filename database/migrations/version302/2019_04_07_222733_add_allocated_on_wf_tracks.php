<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllocatedOnWfTracks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_tracks', function(Blueprint $table)
        {
            $table->bigInteger("allocated")->nullable();
        });
        DB::statement("comment on column wf_tracks.allocated is 'user who were automatically allocated to attend the workflow by auto assignment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
