<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnClaimPayable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('claims_payable', function(Blueprint $table)
        {
            $table->integer('pv_tran_id')->nullable();
        });

        Schema::table('claims_payable', function(Blueprint $table)
        {
            $table->foreign('pv_tran_id')->references('id')->on('payment_voucher_transactions')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
