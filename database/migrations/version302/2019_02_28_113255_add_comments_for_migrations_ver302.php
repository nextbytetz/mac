<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsForMigrationsVer302 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*sysdefs*/
        DB::statement("comment on column sysdefs.pension_verification_limit_days is 'Number of days for verification period for pensioners and dependents'");
        DB::statement("comment on column sysdefs.periodical_verification_limit_days is 'Number of days for verification period for periodical payments i.e. constant care and Temporary disablement'");
        /*pensioners*/
        DB::statement("comment on column pensioners.isactive is 'Specify whether the pensioners is active to receive monthly pension; 0 - inactive, 1 => Active, 2 => Deactivated'");
        /*dependent_employee*/
        DB::statement("comment on column dependent_employee.isactive is 'Specify whether the dependents is active to receive monthly pension; 0 - inactive, 1 => Active, 2 => Deactivated'");
        /*payroll run approvals*/
        DB::statement("comment on column payroll_run_approvals.total_net_amount is 'Total net amount eligible to be paid'");
        DB::statement("comment on column payroll_run_approvals.total_arrears_amount is 'Total arrears amount eligible to be paid i.e.underpayments, unclaimed, suspended payments'");
        DB::statement("comment on column payroll_run_approvals.total_deductions_amount is 'Total deductions amount eligible to be deducted i.e.over-payments'");
        DB::statement("comment on column payroll_run_approvals.total_suspended_amount is 'Total suspended amount from suspended pensioners and dependents'");
        DB::statement("comment on column payroll_run_approvals.no_of_pensioners is 'Number of pensioners eligible for payroll'");
        DB::statement("comment on column payroll_run_approvals.no_of_dependents is 'Number of dependents eligible for payroll'");
        DB::statement("comment on column payroll_run_approvals.status is 'Status of the payroll run ; 0 => pending, 1 => Initiated, 2 => Rejected'");
        DB::statement("comment on column payroll_run_approvals.run_status is 'Flag to specify if the payroll run job is complete'");
        /*payroll_suspended_runs*/
        DB::statement("comment on column payroll_suspended_runs.arrears_amount is 'Total arrears amount paid on this payroll i.e. underpayment, unclaimed'");
        DB::statement("comment on column payroll_suspended_runs.deductions_amount is 'Total deductions amount deducted on this payroll i.e. over-payments'");
        /*payroll_runs*/
        DB::statement("comment on column payroll_runs.arrears_amount is 'Total arrears amount paid on this payroll i.e. underpayment, unclaimed and suspended payments'");
        DB::statement("comment on column payroll_runs.deductions_amount is 'Total deductions amount deducted on this payroll i.e. over-payments'");
        /*payroll_remainder_pensions*/
        DB::statement("comment on column payroll_remainder_pensions.status is 'Flag to specify status of this payment process i.e 0 => pending, 1 => Initiated, 2=> Rejected'");
        /*Payroll recovery transactions*/
            DB::statement("comment on column payroll_recovery_transactions.amount is 'Total amount eligible to be recovered'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
