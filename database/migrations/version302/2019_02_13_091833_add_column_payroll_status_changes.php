<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollStatusChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_status_changes', function (Blueprint $table) {
            $table->dateTime("exit_date")->nullable();
        });
        DB::statement("comment on column payroll_status_changes.exit_date is 'Exit date from payroll i.e. death date, deactivated_date when deactivating member/beneficiary'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
