<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentBenefitTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //document_benefits
        Schema::create('document_benefits', function (Blueprint $table) {
            $table->smallIncrements("id");
            $table->bigInteger("document_id");
            $table->bigInteger("incident_type_id")->nullable();
            $table->bigInteger("benefit_type_id");
            $table->bigInteger("member_type_id")->nullable();
            $table->timestamps();
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('incident_type_id')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table document_benefits is 'document required for specific incident_type and benefit_type and member type'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
