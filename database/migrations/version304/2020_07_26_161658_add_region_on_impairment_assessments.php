<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegionOnImpairmentAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('impairment_assessments', function (Blueprint $table) {
            $table->bigInteger('region_id')->nullable();
            $table->foreign('region_id')->references('id')->on('main.regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('impairment_assessments', function (Blueprint $table) {
            //
        });
    }
}
