<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeHasPayrollWithDefault1OnManualNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->smallInteger('has_payroll')->default(1)->change();
        });
        $sql = <<<SQL
update manual_notification_reports set has_payroll = 1;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
