<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateForPendingApprovedNotificationRejection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
-- Rejection from Notification Approval
update notification_reports set notification_staging_cv_id = (select id from code_values where reference = 'APRDNSRINCW' and code_id = 23) where id in (select c.notification_report_id from (select * from notification_workflows where wf_module_id in (23,62)) c join (select resource_id, resource_type, forward_date, receive_date from wf_tracks where id in (select max(wf1.id) from wf_tracks wf1 join wf_definitions wfd1 on wf1.wf_definition_id = wfd1.id where wf_module_id in (23,62) and level = 5 and status = 1 group by resource_id)) a on a.resource_id = c.id ) and notification_staging_cv_id = (select id from code_values where reference = 'ONSRINCW' and code_id = 23);

-- System Rejected Notification
update notification_reports set notification_staging_cv_id = (select id from code_values where reference = 'NSAPRDNREJSYW' and code_id = 23) where id in (select c.notification_report_id from (select * from notification_workflows where wf_module_id in (33)) c join (select resource_id, resource_type, forward_date, receive_date from wf_tracks where id in (select max(wf1.id) from wf_tracks wf1 join wf_definitions wfd1 on wf1.wf_definition_id = wfd1.id where wf_module_id in (33) and level = 6 and status = 1 group by resource_id)) a on a.resource_id = c.id ) and notification_staging_cv_id = (select id from code_values where reference = 'NSONNREJSYW' and code_id = 23);
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
