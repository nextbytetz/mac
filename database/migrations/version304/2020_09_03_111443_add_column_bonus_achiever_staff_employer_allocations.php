<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBonusAchieverStaffEmployerAllocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_allocations', function (Blueprint $table) {

            $table->text('bonus_achiever')->nullable();

        });

        DB::statement("comment on column staff_employer_allocations.bonus_achiever is 'All users/staff who have achieved bonus/dormant 100% activation on this allocation'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
