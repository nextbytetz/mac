<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAttendedByManualFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->integer('attended_by')->nullable();
            $table->integer('last_updated_by')->nullable();
        });

        DB::statement("comment on column manual_notification_reports.attended_by is 'User who attended this file and updated information'");
        DB::statement("comment on column manual_notification_reports.last_updated_by is 'User who last updated this file'");

        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->foreign('attended_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('last_updated_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
