<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RectifyManualFilesNotUploaded extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        $sql = <<<SQL
update manual_notification_reports set incident_type = CASE WHEN incident_type_id = 1 THEN 'Accident' WHEN incident_type_id = 2 THEN 'Disease' WHEN incident_type_id = 3 then 'Death' ELSE incident_type END where has_payroll = 1;

--Update employee_name
UPDATE manual_notification_reports m
SET employee_name = concat_ws(' ', e.firstname, e.middlename, e.lastname)
FROM employees e
WHERE e.id = m.employee_id AND m.has_payroll = 1;

--Update employer_name
UPDATE manual_notification_reports m
SET employer_name = concat_ws(' ', e.name)
FROM employers e
WHERE e.id = m.employee_id AND m.has_payroll = 1;

SQL;
        DB::unprepared($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
