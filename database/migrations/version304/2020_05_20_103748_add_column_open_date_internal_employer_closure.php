<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOpenDateInternalEmployerClosure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->date('open_date_internal')->nullable();
            $table->integer('dormant_source_cv_id')->nullable();
        });

        DB::statement("comment on column employer_closures.open_date_internal is 'Date of re-open formatted for internal use i.e. start of the month'");
        DB::statement("comment on column employer_closures.dormant_source_cv_id is 'Authority source for dormant de-registrations'");


        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->foreign('dormant_source_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
