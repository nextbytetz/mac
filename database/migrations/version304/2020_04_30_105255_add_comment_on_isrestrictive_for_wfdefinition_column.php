<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentOnIsrestrictiveForWfdefinitionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column wf_definitions.isrestrictive is 'restrict the access of the level only to the assigned user, restrict user to self auto-assign, restrict repeated user in previous workflow from being selected, return the workflow to the same last person participated in the same level in case there was rejection'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
