<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsBookingsTableDuplicateId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('bookings', function(Blueprint $table)
        {
            $table->integer('duplicate_id')->nullable();

        });

        Schema::table('booking_interests', function(Blueprint $table)
        {
            $table->decimal('old_interest_amount', 16, 2)->nullable();
            $table->integer('duplicate_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
