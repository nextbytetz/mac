<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquityDividendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equity_dividends', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('investment_equity_id');
            $table->bigInteger('number_of_shares');
            $table->decimal('dividend_per_share', 22,4);
            $table->decimal('dividend_amount', 22,4);
            $table->decimal('dividend_income', 22,4);
            $table->decimal('tax_rate', 5,2);
            $table->date('declaration_date');
            $table->timestamps();

            $table->foreign('investment_equity_id')->references('id')->on('investment_equities')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        DB::statement("comment on column equity_dividends.dividend_income is ' = (number of shares * dividend per share) - 10% of (number of shares * dividend per sharer)'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equity_dividends');
    }
}
