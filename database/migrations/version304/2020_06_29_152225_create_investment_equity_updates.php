<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentEquityUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_equity_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('equity_issuer_id');
            $table->unsignedInteger('equity_lot_id')->nullable();
            $table->string('lot_number')->nullable();
            $table->bigInteger('number_of_share');
            $table->decimal('share_value', 22,4);
            $table->date('value_date')->nullable();
            $table->decimal('total_share_value', 22,4);
            $table->boolean('is_update')->default(0);
            $table->bigInteger('total_share_available')->nullable();
            $table->date('selling_date')->nullable();
            $table->string('selling_broker')->nullable();
            $table->integer('selling_broker_regno')->nullable();
            $table->decimal('commission', 22,4)->nullable();
            $table->boolean('is_sell')->default(0);
            $table->decimal('dividend_receivable', 22,4)->nullable();
            $table->date('dividend_declaration_date')->nullable();
            $table->date('dividend_payment_date')->nullable();
            $table->boolean('is_dividend')->default(0);
            $table->boolean('is_dividend_paid')->default(0);
            $table->timestamps();

            $table->foreign('equity_issuer_id')->references('id')->on('investment_equity_issuers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('equity_lot_id')->references('id')->on('investment_equity_lots')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_equity_updates');
    }
}
