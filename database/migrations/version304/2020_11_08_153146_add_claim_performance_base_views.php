<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaimPerformanceBaseViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
drop view if exists incident_performance_base;
create or replace view incident_performance_base as select concat_ws(' ', usr.firstname, usr.lastname) as staff, it.name as incident_type, concat_ws(' ', emp.firstname, coalesce(emp.middlename, ''), emp.lastname) as employee, e.name as employer, nr.incident_date, nr.reporting_date, nr.receipt_date, nr.created_at::date as registration_date, d.name as district, r.name as region, cv.name as incident_stage, case when nr.isprogressive = 1 then 'Progressive' else 'Non-Progressive' end as category, case when nr.source = 1 then 'Branch Registered' else 'Branch Registered' end as registration_source, cv.reference, nr.investigation_validity, is_acknowledgment_sent, cv.code_id, nr.source, usr.id as attribute, usr.id as user_id, nr.incident_type_id, nr.district_id, d.region_id, nr.notification_staging_cv_id, nr.isprogressive, nr.progressive_stage, nr.allocated, nr.employer_id, nr.employee_id from notification_reports nr left join users usr on nr.allocated = usr.id join code_values cv on nr.notification_staging_cv_id = cv.id join employers e on nr.employer_id = e.id join employees emp on emp.id = nr.employee_id join incident_types it on nr.incident_type_id = it.id left join districts d on nr.district_id = d.id left join regions r on d.region_id = r.id where nr.isprogressive = 1;
drop view if exists online_incident_performance_base;
create or replace view online_incident_performance_base as select concat_ws(' ', usr.firstname, usr.lastname) as staff, concat_ws(' ', usrb.firstname, usrb.lastname) as incident_staff, it.name as incident_type, concat_ws(' ', emp.firstname, coalesce(emp.middlename, ''), emp.lastname) as employee, e.name as employer, nr.incident_date, nr.reporting_date, mnr.receipt_date as receipt_date, nr.created_at::date as registration_date, d.name as district, r.name as region, cv.name as incident_stage, cv.reference, cv.code_id, usr.id as attribute, usr.id as user_id, usrb.id as incident_attribute, nr.incident_type_id, nr.district_id, d.region_id, nr.incident_staging_cv_id, nr.allocated, nr.employer_id, nr.employee_id from portal.incidents nr left join main.users usr on nr.allocated = usr.id join portal.code_values cv on nr.incident_staging_cv_id = cv.id join main.employers e on nr.employer_id = e.id join main.employees emp on emp.id = nr.employee_id join incident_types it on nr.incident_type_id = it.id left join main.districts d on nr.district_id = d.id left join main.regions r on d.region_id = r.id left join main.notification_reports mnr on mnr.incident_id = nr.id left join main.users usrb on usrb.id = mnr.allocated;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
