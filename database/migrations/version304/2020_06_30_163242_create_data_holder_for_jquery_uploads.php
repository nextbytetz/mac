<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataHolderForJqueryUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_attributes', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->json("data")->nullable();
            $table->bigInteger('reference');
            $table->bigInteger('file_upload_type_cv_id');
            $table->timestamps();
        });
        DB::statement("comment on table upload_attributes is 'table to hold the information of uploaded excel'");
        DB::statement("comment on column upload_attributes.reference is 'reference of the manipulated file before being referred to the sent sms'");
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->smallInteger('has_payroll')->default(0);
        });
        DB::statement("comment on column manual_notification_reports.has_payroll is 'specify whether this notification is in payroll'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
