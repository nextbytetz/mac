<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifieHoldingNumberToTreasuryBillsBondsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investiment_treasury_bonds', function (Blueprint $table) {
            if (Schema::hasColumn('investiment_treasury_bonds', 'holding_number'))
            {
            Schema::table('investiment_treasury_bonds', function (Blueprint $table)
            {
                 $table->dropColumn('holding_number');
            });
            }
            $table->string('holding_number')->nullable();
        });

         Schema::table('investiment_treasury_bills', function (Blueprint $table) {
            if (Schema::hasColumn('investiment_treasury_bills', 'holding_number'))
            {
            Schema::table('investiment_treasury_bills', function (Blueprint $table)
            {
                 $table->dropColumn('holding_number');
            });
            }
            $table->string('holding_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
