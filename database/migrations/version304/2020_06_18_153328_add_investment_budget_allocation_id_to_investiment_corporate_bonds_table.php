<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvestmentBudgetAllocationIdToInvestimentCorporateBondsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investiment_corporate_bonds', function (Blueprint $table) {
            $table->unsignedInteger('investment_budget_allocation_id')->nullable();
            $table->foreign('investment_budget_allocation_id')->references('id')->on('investment_budget_allocation')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_corporate_bonds', function (Blueprint $table) {
            $table->dropColumn('investment_budget_allocation_id');
        });
    }
}
