<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedDepositUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_deposit_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->float('interest_received')->nullable();
            $table->float('principal_redeemed')->nullable();
            $table->float('penalty_received')->nullable();
            $table->date('payment_date_received')->nullable();
            $table->float('interest_receivable')->nullable();
            $table->float('interest_income')->nullable();
            $table->float('weighted_average_return')->nullable();
            $table->float('penalty_income')->nullable();
            $table->float('penalty_receivable')->nullable();
            $table->float('overpaid_interest')->nullable();
            $table->unsignedInteger('investment_fixed_deposit_id')->nullable();
            $table->foreign('investment_fixed_deposit_id')->references('id')->on('investiment_fixed_deposits')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_deposit_updates');
    }
}
