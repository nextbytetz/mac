<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvestmentCodeTypeIdOnInvestmentBudgetAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investment_budget_allocation', function (Blueprint $table) {
            $table->unsignedInteger('investment_code_type_id')->nullable();
            $table->string('code_value_reference')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investment_budget_allocation', function (Blueprint $table) {
            $table->dropColumn('investment_code_type_id');
            $table->dropColumn('code_value_reference');
        });
    }
}
