<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourceToInvestimentBillsAndBonds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investiment_treasury_bills', function (Blueprint $table) {
            $table->integer('source')->nullable();
        });
        DB::statement("comment on column investiment_treasury_bills.source is 'Source of market 1 => primary , 2 => secondary'");
        
        Schema::table('investiment_treasury_bonds', function (Blueprint $table) {
            $table->integer('source')->nullable();
        });
        DB::statement("comment on column investiment_treasury_bonds.source is 'Source of market 1 => primary , 2 => secondary'");

        Schema::table('investiment_corporate_bonds', function (Blueprint $table) {
            $table->integer('source')->nullable();
        });
        DB::statement("comment on column investiment_corporate_bonds.source is 'Source of market 1 => primary , 2 => secondary'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_treasury_bills', function (Blueprint $table) {
            $table->dropColumn('source');
        });

        Schema::table('investiment_treasury_bonds', function (Blueprint $table) {
            $table->dropColumn('source');
        });

        Schema::table('investiment_corporate_bonds', function (Blueprint $table) {
            $table->dropColumn('source');
        });
    }
}
