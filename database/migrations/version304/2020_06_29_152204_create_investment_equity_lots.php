<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentEquityLots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_equity_lots', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('budget_allocation_id');
            $table->unsignedInteger('fin_year_id');
            $table->unsignedInteger('equity_issuer_id');
            $table->unsignedInteger('category_id');
            $table->string('category_name');
            $table->string('lot_number');
            $table->bigInteger('number_of_share');
            $table->bigInteger('total_issued_share');
            $table->decimal('price_per_share', 22,4);
            $table->decimal('total_price', 22,4);
            $table->date('purchase_date');
            $table->bigInteger('remaining_share');
            $table->date('disbursment_date');
            $table->integer('broker_id')->nullable();
            $table->string('broker_name')->nullable();
            $table->date('subscription_date')->nullable();
            $table->date('allotment_date')->nullable();
            $table->decimal('commission', 22,4)->nullable();
            $table->decimal('total_cost', 22,4);
            $table->timestamps();

            $table->foreign('equity_issuer_id')->references('id')->on('investment_equity_issuers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('budget_allocation_id')->references('id')->on('investment_budget_allocation')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fin_year_id')->references('id')->on('fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        DB::statement("comment on column investment_equity_lots.category_id is 'Listed or unlisted'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_equity_lots');
    }
}
