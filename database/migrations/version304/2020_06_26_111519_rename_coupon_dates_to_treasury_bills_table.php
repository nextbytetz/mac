<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCouponDatesToTreasuryBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('investiment_treasury_bills', function(Blueprint $table)
        {
            $table->date('first_coupon_date')->nullable();
            $table->date('next_coupon_date')->nullable();
            $table->float('coupon_rate')->nullable();
            $table->float('dirty_price')->nullable();
            $table->float('discount')->nullable();
            $table->float('fair_value')->nullable();
            $table->float('amortization')->nullable();
            $table->float('interest_income')->nullable();
            $table->float('interest_receivable')->nullable();
            $table->float('expected_income')->nullable();
            $table->float('balance_sheet_value')->nullable();
            $table->float('carrying_amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_treasury_bills', function(Blueprint $table)
        {
            $table->dropColumn('first_coupon_date');
            $table->dropColumn('next_coupon_date');
            $table->dropColumn('coupon_rate');
            $table->dropColumn('dirty_price');
            $table->dropColumn('discount');
            $table->dropColumn('fair_value');
            $table->dropColumn('amortization');
            $table->dropColumn('interest_income');
            $table->dropColumn('interest_receivable');
            $table->dropColumn('expected_income');
            $table->dropColumn('balance_sheet_value');
            $table->dropColumn('carrying_amount');
        });
    }
}
