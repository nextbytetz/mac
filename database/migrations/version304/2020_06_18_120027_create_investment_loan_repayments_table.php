<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentLoanRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_loan_repayments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('investment_loan_id');
            $table->decimal('received_repayment',20,2)->nullable();
            $table->decimal('amount_disbursed',20,2)->nullable();
            $table->decimal('interest_received',20,2)->nullable();
            $table->decimal('principal_redeemed',20,2)->nullable();
            $table->date('interest_received_date')->nullable();
            $table->date('received_repayment_date')->nullable();
            $table->timestamps();

            $table->foreign('investment_loan_id')->references('id')->on('investment_loans')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_loan_repayments');
    }
}
