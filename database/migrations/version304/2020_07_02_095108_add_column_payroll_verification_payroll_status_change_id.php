<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollVerificationPayrollStatusChangeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_verifications', function (Blueprint $table) {
            $table->integer('payroll_status_change_id')->nullable();
        });

        Schema::table('payroll_verifications', function (Blueprint $table) {
            $table->foreign('payroll_status_change_id')->references('id')->on('payroll_status_changes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
