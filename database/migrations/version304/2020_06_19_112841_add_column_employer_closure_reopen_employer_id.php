<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployerClosureReopenEmployerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('employer_closure_reopens', function (Blueprint $table) {
            $table->integer('employer_id')->nullable();
            $table->integer('employer_closure_id')->nullable()->change();
        });


        Schema::table('employer_closure_reopens', function(Blueprint $table) {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
