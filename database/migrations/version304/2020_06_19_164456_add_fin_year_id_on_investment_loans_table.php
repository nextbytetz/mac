<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinYearIdOnInvestmentLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investment_loans', function (Blueprint $table) {
         $table->unsignedInteger('fin_year_id')->nullable();
         $table->foreign('fin_year_id')->references('id')->on('fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investment_loans', function (Blueprint $table) {
            //
        });
    }
}
