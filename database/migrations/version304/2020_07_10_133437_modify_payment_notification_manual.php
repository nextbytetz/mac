<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPaymentNotificationManual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  cr30");
        Schema::table('manual_notification_reports', function(Blueprint $table)
        {
            $table->renameColumn("mae", 'man_mae');
            $table->renameColumn("pd", 'man_pd');
            $table->renameColumn("ppd", 'man_ppd');
            $table->renameColumn("ptd", 'man_ptd');
            $table->renameColumn("ttd", 'man_ttd');
            $table->renameColumn("tpd", 'man_tpd');
            $table->renameColumn("funeral_grant",'man_funeral_grant');
        });

        DB::statement("select deps_save_and_drop_dependencies('main', 'cr30')");
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS  cr30");
        DB::statement("CREATE MATERIALIZED VIEW main.cr30 AS
  SELECT a.employee,
         a.employer,
         a.case_no,
         a.incident_type,
         a.incident_date,
         a.reporting_date,
         a.notification_date,
         a.registration_date,
         a.date_of_mmi,
         a.created_at,
         a.updated_at,
         a.district,
         a.region,
         a.pd,
         a.claim_status,
         a.reject_reason,
         a.issynced,
         a.isupdated,
         a.ismembersynced,
         a.status_cv_id,
         a.reject_reason_cv_id,
         a.employee_id,
         a.employer_id,
         a.resourceid,
         b.name AS fin_year,
         b.id   AS fin_year_id
  FROM ((SELECT CASE
                  WHEN (c.id IS NULL) THEN (a_1.employee_name) :: text
                  ELSE concat_ws(' ' :: text, c.firstname, c.lastname)
                    END                                                          AS employee,
                CASE
                  WHEN (d.id IS NULL) THEN a_1.employer_name
                  ELSE d.name
                    END                                                          AS employer,
                a_1.case_no,
                CASE
                  WHEN (b_1.id IS NULL) THEN a_1.incident_type
                  ELSE b_1.name
                    END                                                          AS incident_type,
                (a_1.incident_date) :: date                                      AS incident_date,
                (a_1.reporting_date) :: date                                     AS reporting_date,
                (a_1.notification_date) :: date                                  AS notification_date,
                COALESCE(a_1.registration_date, (a_1.notification_date) :: date) AS registration_date,
                (a_1.date_of_mmi) :: date                                        AS date_of_mmi,
                a_1.created_at,
                a_1.updated_at,
                CASE
                  WHEN (e.id IS NULL) THEN a_1.district
                  ELSE e.name
                    END                                                          AS district,
                f.name                                                           AS region,
                a_1.man_pd as pd,
                CASE
                  WHEN (cv.id IS NULL) THEN a_1.claim_status
                  ELSE cv.name
                    END                                                          AS claim_status,
                CASE
                  WHEN (cvr.id IS NULL) THEN (a_1.reject_reason) :: character varying
                  ELSE cvr.name
                    END                                                          AS reject_reason,
                a_1.issynced,
                a_1.isupdated,
                a_1.ismembersynced,
                a_1.status_cv_id,
                a_1.reject_reason_cv_id,
                a_1.employee_id,
                a_1.employer_id,
                a_1.id                                                           AS resourceid
         FROM (((((((main.manual_notification_reports a_1
             LEFT JOIN main.incident_types b_1 ON ((a_1.incident_type_id = b_1.id)))
             LEFT JOIN main.employees c ON ((c.id = a_1.employee_id)))
             LEFT JOIN main.employers d ON ((d.id = a_1.employer_id)))
             LEFT JOIN main.districts e ON ((e.id = a_1.district_id)))
             LEFT JOIN main.regions f ON ((f.id = e.region_id)))
             LEFT JOIN main.code_values cv ON ((cv.id = a_1.status_cv_id)))
             LEFT JOIN main.code_values cvr ON ((cvr.id = a_1.reject_reason_cv_id)))) a
      LEFT JOIN main.fin_years b ON (((a.registration_date >= b.start_date) AND (a.registration_date <= b.end_date))));
");

        DB::statement("select deps_restore_dependencies('main', 'cr30')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
