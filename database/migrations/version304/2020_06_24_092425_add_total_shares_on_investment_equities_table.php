<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalSharesOnInvestmentEquitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investment_equities', function (Blueprint $table) {
            $table->bigInteger('remain_shares')->nullable();
        });
        DB::statement("comment on column investment_equities.remain_shares is ' total current number of shares'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investment_equities', function (Blueprint $table) {
            $table->dropColumn('remain_shares');
        });
    }
}
