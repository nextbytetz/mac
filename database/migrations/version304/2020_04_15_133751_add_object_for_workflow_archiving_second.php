<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObjectForWorkflowArchivingSecond extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_archives', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('wf_track_id');
            $table->date('from_date');
            $table->date('to_date')->nullable();
            $table->date('restore_date')->nullable();
            $table->smallInteger('status')->default(0);
            $table->text('description');
            $table->bigInteger('ref_user');
            $table->smallInteger('instore')->default(0);
            $table->bigInteger('receiver_user')->nullable();
            $table->timestamps();
            $table->foreign('wf_track_id')->references('id')->on('main.wf_tracks')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('ref_user')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('receiver_user')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column wf_archives.ref_user is 'user who had the workflow before archiving'");
        DB::statement("comment on column wf_archives.restore_date is 'date when workflow is restored to workflow process'");
        DB::statement("comment on column wf_archives.instore is 'show whether the file is to be sent to record department of not, 0 - Not, 1 - Yes'");
        DB::statement("comment on column wf_archives.receiver_user is 'user who have received a file in records department in case file has been specified to be sent to record department'");
        DB::statement("comment on column wf_archives.description is 'description as to why the file is being archived between the referred from_date to to_date'");
        DB::statement("comment on column wf_archives.status is 'show the status of the archive process, 0 - Pending for receipt in case file is to be sent to records department, 1 - archived successful, 2 - restored to the workflow process'");
        DB::statement("comment on column wf_tracks.status is 'status of the track, 0 - Pending for action, 1 - Recommended/Approved, 2 - Rejected to previous level, 3 - Declined/Approval Denied, 4 - Seek Advice, 5 - Cancelled by Initiator/User, 6 - Archived'");

        Schema::table('wf_module_groups', function(Blueprint $table)
        {
            $table->smallInteger('can_archive')->default(0);
        });

        Schema::table('wf_tracks', function(Blueprint $table)
        {
            $table->json('process_dates')->nullable();
            $table->bigInteger('wf_archive_id')->nullable();
        });
        DB::statement("comment on column wf_tracks.process_dates is 'reserve the workflow process dates including all receive and forward dates, skipping the period for archive'");
        DB::statement("comment on column wf_tracks.wf_archive_id is 'in case workflow track is archived.'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
