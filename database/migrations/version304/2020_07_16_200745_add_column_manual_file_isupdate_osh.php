<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnManualFileIsupdateOsh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("update manual_notification_reports set man_pd = pd where man_pd is null;  ");

        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->smallInteger('isupdate_osh')->default(0);
            $table->integer('accident_cause_agency_cv_id')->nullable();
            $table->integer('accident_cause_type_cv_id')->nullable();
            $table->dropColumn('accident_cause_type');
            $table->dropColumn('accident_cause_agency');
            $table->dropColumn('pd');
              });

        DB::statement("comment on column manual_notification_reports.isupdate_osh is 'Flag to specify if osh data are updated i.e. 1 => updated, 0 => not yet'");

        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->foreign('accident_cause_agency_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('accident_cause_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
