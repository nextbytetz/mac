<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestimentTreasuryBondsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investiment_treasury_bonds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('holding_number');
            $table->date('settlement_date');
            $table->date('maturity_date');
            $table->date('coupon_date');
            $table->float('auction_date');
            $table->string('auction_number');
            $table->string('tenure');
            $table->float('amount_invested');
            $table->float('tax_rate');
            $table->float('coupon_rate');
            $table->float('income')->nullable();
            $table->float('receivable')->nullable();
            $table->float('yield')->nullable();
            $table->float('weighted_average_return')->nullable();
            $table->float('clean_price')->nullable();
            $table->float('dirty_price')->nullable();
            $table->float('cost')->nullable();
            $table->float('carrying_amount')->nullable();
            $table->float('balance_sheet_value')->nullable();
            $table->float('discount')->nullable();
            $table->float('fair_value')->nullable();
            $table->float('amortization')->nullable();
            $table->float('interest_income')->nullable();
            $table->float('interest_receivable')->nullable();
            $table->float('minimum_selling_price')->nullable();
            $table->float('expected_income')->nullable();
            $table->float('gain_loss')->nullable();
            $table->float('realized_yield')->nullable();
            $table->float('effective_interest_rate')->nullable();
            $table->unsignedInteger('investment_budget_id')->nullable();
            $table->foreign('investment_budget_id')->references('id')->on('investment_budgets')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investiment_treasury_bonds');
    }
}
