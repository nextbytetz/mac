<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFromHiddensToDisplayOnConfigurableReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurable_reports', function(Blueprint $table)
        {
            $table->renameColumn('hiddens', 'display');
            $table->json('aliases')->nullable();
        });
        DB::statement("comment on column configurable_reports.display is 'columns from the report which should be shown on the display, but can be dynamically hidden when needed'");
        DB::statement("comment on column configurable_reports.aliases is 'transform the report attributes to the human readable headers on the report'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
