<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLegacyBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('legacy_bookings', function(Blueprint $table)
        {
            $table->bigInteger('id', true);
            $table->date('rcv_date')->index();
            $table->bigInteger('employer_id')->nullable()->index('idx_lb_employer_id');
            $table->bigInteger('fin_code_id')->index('idx_lb_fin_code_id');
            $table->decimal('amount', 14);
            $table->bigInteger('user_id')->nullable()->index('idx_lb_user_id');
            $table->bigInteger('booking_group_id')->nullable()->index('idx_lb_booking_group_id');
            $table->bigInteger('member_count');
            $table->text('description')->nullable();
            $table->smallInteger('iscomplete')->default(0);
            $table->smallInteger('isadjusted')->nullable()->default(0);
            $table->decimal('adjust_amount', 14)->nullable();
            $table->smallInteger('is_schedule')->default(1)->comment('Flag to specify if booking was generated through');
            $table->bigInteger('employer_orig')->nullable();
            $table->decimal('old_booked_amount', 16)->default(0);
            $table->decimal('paid_amount', 16)->default(0);
            $table->smallInteger('ispaid')->default(1)->index();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('legacy_bookings', function(Blueprint $table)
        {
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fin_code_id', 'bookings_ilbfk_1')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('booking_group_id', 'bookings_ilbfk_2')->references('id')->on('booking_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
