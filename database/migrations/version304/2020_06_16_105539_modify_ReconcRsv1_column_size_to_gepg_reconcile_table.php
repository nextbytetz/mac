<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyReconcRsv1ColumnSizeToGepgReconcileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal.gepgreconcile', function (Blueprint $table) {
            $table->string('ReconcRsv1',150)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.gepgreconcile', function (Blueprint $table) {
            //
        });
    }
}
