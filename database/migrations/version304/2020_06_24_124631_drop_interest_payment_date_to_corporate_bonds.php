<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropInterestPaymentDateToCorporateBonds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('investiment_corporate_bonds', 'interest_payment_date'))
            {
            Schema::table('investiment_corporate_bonds', function (Blueprint $table)
            {
                 $table->dropColumn('interest_payment_date');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
