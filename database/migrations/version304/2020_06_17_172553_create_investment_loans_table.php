<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('investment_loans');
        
        Schema::create('investment_loans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('budget_allocation_id');
            $table->integer('loan_type');
            $table->string('loan_type_name');
            $table->integer('reg_no')->nullable();
            $table->string('loanee_name');
            $table->decimal('loan_amount', 20,2);
            $table->date('disbursement_date');
            $table->date('maturity_date');
            $table->integer('tenure');
            $table->decimal('interest_rate');
            $table->decimal('penalty_rate');
            $table->decimal('tax_rate');

            $table->decimal('loan_balance',20,2)->nullable();
            $table->decimal('interest_receivable',20,2)->nullable();
            $table->decimal('interest_income',20,2)->nullable();
            $table->decimal('principle_recovered',20,2)->nullable();

            $table->decimal('amount_due',20,2)->nullable();
            $table->decimal('penalty_due',20,2)->nullable();
            $table->decimal('penalty_paid',20,2)->nullable();
            $table->decimal('portfolio_return',20,2)->nullable();

            $table->timestamps();

            $table->foreign('budget_allocation_id')->references('id')->on('investment_budget_allocation')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });


        DB::statement("comment on column investment_loans.loan_type is 'type of loan 1 - Government, 2 => Cooperate, 3 => Cooperative Union'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_loans');
    }
}
