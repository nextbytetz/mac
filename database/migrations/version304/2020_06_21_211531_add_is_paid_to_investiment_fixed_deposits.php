<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPaidToInvestimentFixedDeposits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('investiment_fixed_deposits', function (Blueprint $table) {
            $table->integer('is_paid')->nullable();
        });
        DB::statement("comment on column investiment_fixed_deposits.is_paid is 'Status of payment 1 - paid , 0 => not paid'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_fixed_deposits', function (Blueprint $table) {
            $table->dropColumn('is_paid');
        });
    }
}
