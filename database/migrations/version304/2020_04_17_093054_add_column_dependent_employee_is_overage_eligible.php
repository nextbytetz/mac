<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDependentEmployeeIsOverageEligible extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //'
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->smallInteger('is_overage_eligible')->nullable();
        });
        DB::statement("comment on column dependent_employee.is_overage_eligible is 'Flag to specify if child is eligible for payroll at the time for processing payroll but already overage (Was underage at time of death)'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
