<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAllocationParameterTargetIntern extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_configs', function (Blueprint $table) {

            $table->smallInteger('followup_weekly_target_intern')->default(10);

        });

        DB::statement("comment on column staff_employer_configs.followup_weekly_target_intern is 'Weekly followup target for intern staff'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
