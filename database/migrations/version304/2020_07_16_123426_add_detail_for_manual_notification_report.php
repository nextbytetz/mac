<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailForManualNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->string('hcp_name', 150)->nullable();
            $table->text('accident_cause_type')->nullable();
            $table->text('accident_cause_agency')->nullable();
            $table->text('injury_nature')->nullable();
            $table->text('bodily_location')->nullable();
            $table->text('disease_diagnosed')->nullable();
            $table->string('disease_type', 120)->nullable();
            $table->string('disease_agent', 120)->nullable();
            $table->string('disease_target_organ', 120)->nullable();
            $table->smallInteger('day_off')->nullable();
            $table->smallInteger('light_duties')->nullable();
            $table->decimal('pd', 3)->nullable();
            $table->text('rehabilitation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
