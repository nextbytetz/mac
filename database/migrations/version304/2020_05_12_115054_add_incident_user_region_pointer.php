<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncidentUserRegionPointer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_user_region_allocations', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->bigInteger('region_id')->nullable();
            $table->bigInteger('district_id')->nullable();
            $table->smallInteger('pointer');
            $table->foreign('region_id')->references('id')->on('main.regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id')->references('id')->on('main.districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
