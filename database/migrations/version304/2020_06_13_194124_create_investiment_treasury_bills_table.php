<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestimentTreasuryBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investiment_treasury_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->string('holding_number');
            $table->date('settlement_date');
            $table->date('maturity_date');
            $table->float('auction_date');
            $table->string('tenure');
            $table->float('amount_invested');
            $table->float('price');
            $table->float('tax_rate');
            $table->float('income')->nullable();
            $table->float('receivable')->nullable();
            $table->float('yield')->nullable();
            $table->float('weighted_average_return')->nullable();
            $table->float('cost')->nullable();
            $table->unsignedInteger('investment_budget_id')->nullable();
            $table->foreign('investment_budget_id')->references('id')->on('investment_budgets')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investiment_treasury_bills');
    }
}
