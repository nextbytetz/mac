<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOldestStartDateFollowupreminders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_followup_reminders', function (Blueprint $table) {

                   $table->date('oldest_start_date')->nullable();

        });
        DB::statement("comment on column staff_followup_reminders.oldest_start_date is 'Start date of the oldest week with missing target'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
