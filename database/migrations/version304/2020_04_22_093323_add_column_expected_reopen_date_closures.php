<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnExpectedReopenDateClosures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->date('specified_reopen_date')->nullable();
            $table->integer('tempo_type_cv_id')->nullable();
            $table->decimal('interest_amount', 15,2)->nullable();
        });
        DB::statement("comment on column employer_closures.specified_reopen_date is 'Specified reopen date by employer'");
        DB::statement("comment on column employer_closures.tempo_type_cv_id is 'Temporary closure types (optional)'");
        DB::statement("comment on column employer_closures.interest_amount is 'Interest amount at time of closure'");

        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->foreign('tempo_type_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
