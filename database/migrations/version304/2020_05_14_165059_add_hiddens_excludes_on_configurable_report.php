<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHiddensExcludesOnConfigurableReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurable_reports', function(Blueprint $table)
        {
            $table->json('hiddens')->nullable();
            $table->json('excludes')->nullable();
        });
        DB::statement("comment on column configurable_reports.hiddens is 'columns from the report which should be hidden from the display, but can be dynamically shown when needed'");
        DB::statement("comment on column configurable_reports.excludes is 'columns from the report which should be completely excluded from the display and export'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
