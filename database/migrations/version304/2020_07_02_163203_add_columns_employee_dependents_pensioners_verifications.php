<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsEmployeeDependentsPensionersVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employees', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
            $table->integer('district_id')->nullable();
        });
        Schema::table('employees', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('pensioners', function (Blueprint $table) {
            $table->string('next_kin')->nullable();
            $table->string('next_kin_phone')->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('district_id')->nullable();
        });
        Schema::table('pensioners', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        Schema::table('dependents', function (Blueprint $table) {
            $table->string('next_kin')->nullable();
            $table->string('next_kin_phone')->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('district_id')->nullable();
        });
        Schema::table('dependents', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('payroll_verifications', function (Blueprint $table) {
            $table->string('next_kin')->nullable();
            $table->string('next_kin_phone')->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('district_id')->nullable();
        });


        Schema::table('payroll_verifications', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
