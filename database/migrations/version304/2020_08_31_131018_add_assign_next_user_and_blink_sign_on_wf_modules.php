<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssignNextUserAndBlinkSignOnWfModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_definitions', function (Blueprint $table) {
            $table->smallInteger('assign_next_user')->default(0);
        });
        DB::statement("comment on column wf_definitions.assign_next_user is 'Check if level need to specify next user to assign'");
        Schema::table('wf_modules', function (Blueprint $table) {
            $table->smallInteger('notify')->default(0);
        });
        DB::statement("comment on column wf_modules.notify is 'show whether this module should show a blink sign on user dashboard pending list'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
