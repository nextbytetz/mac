<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInterestTypeDescriptionToInvestimentFixedDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('investiment_fixed_deposits', function (Blueprint $table) {
            $table->string('interest_type_description')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_fixed_deposits', function (Blueprint $table) {
            $table->dropColumn('interest_type_description');
        });
    }
}
