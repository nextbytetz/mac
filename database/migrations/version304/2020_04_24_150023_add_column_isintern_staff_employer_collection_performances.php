<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsinternStaffEmployerCollectionPerformances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_collection_performances', function(Blueprint $table)
        {
            $table->smallInteger('isintern')->default(0);

        });
        DB::statement("comment on column staff_employer_collection_performances.isintern is 'Flag to specify this performance is for intern i.e. 1 => intern, 0 => not intern.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
