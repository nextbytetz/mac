<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReversedByOnAnnualReturnSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal.annual_returns_summary', function (Blueprint $table) {
            $table->bigInteger('reversed_by')->nullable();
            $table->text('reverse_reason')->nullable();
            $table->timestamp('reversed_at')->nullable();

            // $table->foreign('reversed_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal.annual_returns_summary', function (Blueprint $table) {
         $table->dropColumn('reversed_by');
         $table->dropColumn('reversed_at');
         $table->dropColumn('reverse_reason');
     });
    }
}
