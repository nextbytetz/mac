<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentEquitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_equities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('allocation_id')->nullable();
            $table->unsignedInteger('code_type_id')->nullable();
            $table->unsignedInteger('code_value_id')->nullable();
            $table->string('code_value_reference');
            $table->string('category');
            $table->string('issuer');
            $table->integer('issuer_reg_no')->nullable();
            $table->decimal('price_per_share', 22,4);
            $table->bigInteger('number_of_shares');
            $table->bigInteger('total_issued_shares');
            $table->date('purchase_date');
            $table->date('disbursement_date');
            $table->decimal('total_share_values', 22,4);

            $table->string('broker')->nullable();
            $table->integer('broker_reg_no')->nullable();
            $table->date('allotment_date')->nullable();
            $table->date('subscription_date')->nullable();
            $table->decimal('commission', 22,4)->nullable();
            $table->timestamps();
        });

        DB::statement("comment on column investment_equities.issuer is 'share owner example Vodacom, TCC'");
        DB::statement("comment on column investment_equities.broker is 'broker selling shares on behalf of issuer example Orbit security'");
        DB::statement("comment on column investment_equities.total_issued_shares is 'number of authorized shares sold'");
        DB::statement("comment on column investment_equities.disbursement_date is 'date of paying out or disbursing money'");
        DB::statement("comment on column investment_equities.total_share_values is ' is = price_per_share times number_of_shares'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_equities');
    }
}
