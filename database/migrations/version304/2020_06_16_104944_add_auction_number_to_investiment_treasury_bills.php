<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuctionNumberToInvestimentTreasuryBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('investiment_treasury_bills', function (Blueprint $table) {
        $table->string('auction_number');
    });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_treasury_bills', function (Blueprint $table) {
            $table->dropColumn('auction_number');
        });
    }
}
