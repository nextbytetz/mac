<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObjectsForInstallmentDefaultersErrorneousContribution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_stages', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger("resource_id");
            $table->bigInteger("staging_cv_id");
            $table->json("comments");
            $table->smallInteger("allow_repeat")->default(0);
            $table->smallInteger("attended")->default(0);
            $table->smallInteger("require_attend")->default(1);
            $table->smallInteger('isdeleted')->default(0);
            $table->string('resource_type', 150)->nullable();
            $table->timestamps();
            $table->foreign('staging_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column resource_stages.allow_repeat is 'show whether this stage can be repeated at any time in the future process'");
        DB::statement("comment on column resource_stages.attended is 'show whether a user has executed the next stage action. 1 - yes, 0 - no'");
        DB::statement("comment on column resource_stages.isdeleted is 'show whether this stage has been deleted and has never been attended. Stages were frequently updated such that this stage was just logged and never to be attended. 1 - Yes, 0 - No'");
        DB::statement("comment on table resource_stages is 'log the stages of different resources stages'");

        //Contribution Liability
        Schema::rename('receipt_refunds', 'refunds');
        Schema::table('refunds', function(Blueprint $table)
        {
            $table->bigInteger('staging_cv_id')->nullable();
            $table->smallInteger('progressive_stage')->default(1);
            $table->bigInteger('resource_stage_id')->nullable();
            $table->dropColumn('receipt_id');
            $table->bigInteger('source_cv_id');
            $table->bigInteger('allocated')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('employer_inspection_task_id')->nullable();
            $table->smallInteger('ispayprocessed')->default(0);
            $table->bigInteger('employer_id');
            $table->foreign('source_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_inspection_task_id')->references('id')->on('main.employer_inspection_task')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('staging_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('resource_stage_id')->references('id')->on('main.resource_stages')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('allocated')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('main.employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column refunds.progressive_stage is 'show the mandatory stages which a refund has already gone. 1 - Refund Request Created, 2 - Received Refund Supporting Documents, 3. Actual Contribution Paid, 4. Approved Request 5. Refund Paid by the Fund'");
        DB::statement("comment on table refunds is 'table to store all erroneous contribution entries to be paid to employer'");


        Schema::create('refund_receipts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('refund_id');
            $table->bigInteger('receipt_code_orig');
            $table->bigInteger('receipt_code_repl')->nullable();
            $table->smallInteger('remitted')->default(0);
            $table->decimal('actual_amount', 14);
            $table->timestamps();
            $table->foreign('refund_id')->references('id')->on('main.refunds')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('receipt_code_orig')->references('id')->on('main.receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('receipt_code_repl')->references('id')->on('main.receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column refund_receipts.remitted is 'show whether the correct contribution/actual amount for the corresponding month have already been paid.'");
        DB::statement("comment on column refund_receipts.receipt_code_orig is 'reference to the original paid monthly contribution'");
        DB::statement("comment on column refund_receipts.receipt_code_repl is 'reference to the new remitted monthly contribution'");


        //Contribution Receivable
        Schema::create('installments', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('employer_id');
            $table->smallInteger('payroll_id')->nullable();
            $table->smallInteger('status');
            $table->bigInteger('user_id')->nullable();
            $table->text('description')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->foreign('employer_id')->references('id')->on('main.employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table installments is 'table to store all commitment of the employer to pay arrears'");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
