<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAuctionDateDatatypeInInvestimentTreasuryBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('investiment_treasury_bills', 'auction_date'))
        {
            Schema::table('investiment_treasury_bills', function (Blueprint $table)
            {
                $table->dropColumn('auction_date');
            });

        }
        Schema::table('investiment_treasury_bills', function (Blueprint $table)
        {
                $table->date('auction_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('investiment_treasury_bills', 'auction_date'))
        {
            Schema::table('investiment_treasury_bills', function (Blueprint $table)
            {
                $table->dropColumn('auction_date');
            });

        }
        Schema::table('investiment_treasury_bills', function (Blueprint $table)
        {
            $table->float('auction_date');
        });
    }
}
