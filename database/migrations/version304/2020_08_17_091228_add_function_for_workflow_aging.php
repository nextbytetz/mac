<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFunctionForWorkflowAging extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create or replace function main.wf_aging(receive_date timestamp[0][], forward_date timestamp[0][], process_dates json[] default NULL) returns integer
	language plpgsql
as $$
declare i smallint;
diff bigint := 0;
datediff interval;
begin
    FOR i IN 1 .. array_upper(receive_date, 1)
LOOP
  datediff := COALESCE((forward_date[i]), now()) - (receive_date[i]);
  diff := diff + ((DATE_PART('day', datediff) * 24 +
                DATE_PART('hour', datediff)) * 60 +
                DATE_PART('minute', datediff)) * 60 +
                DATE_PART('second', datediff);
END LOOP;

return floor(diff/86400);
    
end
$$
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
