<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnArrearsSummaryPayrollStatusChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_status_changes', function (Blueprint $table) {
            $table->text('arrears_summary')->nullable();
        });
        DB::statement("comment on column payroll_status_changes.arrears_summary is 'Arrears summary for beneficiary activated for the first time and not paid yet.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
