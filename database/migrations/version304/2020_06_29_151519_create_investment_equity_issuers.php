<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentEquityIssuers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_equity_issuers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('issuer_id')->nullable();
            $table->string('issuer_name');
            $table->string('issuer_reference');
            $table->timestamps();
            $table->foreign('issuer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_equity_issuers');
    }
}
