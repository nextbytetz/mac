<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsdashboardFlagOnConfigurableReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurable_reports', function (Blueprint $table) {
            $table->smallInteger('isdashboard')->default(1);
        });
        Schema::table('sysdefs', function (Blueprint $table) {
            $table->smallInteger("wf_mode")->default(2);
        });
        DB::statement("comment on column configurable_reports.isdashboard is 'show whether the report should be shown on the unit report dashboard or not. 1 - Yes, 0 - No'");
        DB::statement("comment on column sysdefs.wf_mode is 'mode of displaying the workflow, 1 - Datatable using lazy loading queries, 2 - Configurable Stored Queries'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurable_reports', function (Blueprint $table) {
            //
        });
    }
}
