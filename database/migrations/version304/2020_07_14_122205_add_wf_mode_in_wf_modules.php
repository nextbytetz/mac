<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfModeInWfModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_modules', function (Blueprint $table) {
            $table->smallInteger('wf_mode')->default(1);
        });
        DB::statement("comment on column wf_modules.wf_mode is 'mode of displaying the workflow, 1 - Datatable using lazy loading queries, 2 - Configurable Stored Queries'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
