<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsIntoEmployerClosuresFollowUps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->date('followup_ref_date')->nullable();
            $table->smallInteger('followup_outcome')->nullable();
        });
        DB::statement("comment on column employer_closures.followup_ref_date is 'Date when follow up was made fot temporary closure to check status of business'");
        DB::statement("comment on column employer_closures.followup_outcome is 'Followup outcome i.e. 1 => Still closed, 2 => Reopen/resume business'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
