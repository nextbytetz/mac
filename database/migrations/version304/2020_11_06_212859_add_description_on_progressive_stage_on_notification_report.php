<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionOnProgressiveStageOnNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("comment on column notification_reports.progressive_stage is 'show the mandatory stages which a notification has already gone. 1 - Checklist, 2 - Physical Investigation, 3 - Approval, 4 - Benefit Process, 5 - Benefit Process (Paid)'");
        $sql = <<<SQL
update notification_reports set progressive_stage = 5 where progressive_stage = 4 and id in (select notification_report_id from notification_eligible_benefits where ispaid = 1 and processed = 1);
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
