<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsupdatedOnManualNotificationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->smallInteger('isupdated')->default(0);
        });
        DB::statement("comment on column manual_notification_reports.isupdated is 'set status if other information are updated, 1  -Yes, 0 - No'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
