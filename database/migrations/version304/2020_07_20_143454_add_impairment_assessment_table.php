<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImpairmentAssessmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impairment_assessments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('notification_report_id')->index();
            $table->text('comments');
            $table->date('date');
            $table->bigInteger('assessor');
            $table->bigInteger('user_id');
            $table->json('edits')->nullable();
            $table->text('location')->nullable();
            $table->timestamps();
            $table->foreign('notification_report_id')->references('id')->on('main.notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('assessor')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column impairment_assessments.edits is 'track the number of edits of the specified information'");
        DB::statement("comment on column impairment_assessments.assessor is 'name of the officer who did assessment'");
        DB::statement("comment on column impairment_assessments.date is 'date when assessment was performed'");
        DB::statement("comment on column impairment_assessments.location is 'location where the assessment was performed.'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('impairment_assessments', function (Blueprint $table) {
            //
        });
    }
}
