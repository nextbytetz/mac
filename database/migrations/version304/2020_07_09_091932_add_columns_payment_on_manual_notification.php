<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPaymentOnManualNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('manual_notification_reports', function(Blueprint $table)
        {
            $table->decimal("mae", 14)->nullable();
                    $table->decimal("ppd", 14)->nullable();
            $table->decimal("ptd", 14)->nullable();
            $table->decimal("ttd", 14)->nullable();
            $table->decimal("tpd", 14)->nullable();
            $table->decimal("funeral_grant", 14)->nullable();
        });

        Schema::table('notification_reports', function(Blueprint $table)
        {
            $table->dropColumn("mae");
            $table->dropColumn("pd");
            $table->dropColumn("ppd");
            $table->dropColumn("ptd");
            $table->dropColumn("ttd");
            $table->dropColumn("tpd");
            $table->dropColumn("funeral_grant");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
