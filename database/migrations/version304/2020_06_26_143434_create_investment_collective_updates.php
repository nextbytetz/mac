<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentCollectiveUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_collective_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('scheme_id');
            $table->unsignedInteger('lot_id')->nullable();
            $table->string('lot_number')->nullable();
            $table->integer('category_id')->nullable();
            $table->bigInteger('number_of_unit');
            $table->decimal('unit_value', 22,4)->nullable();
            $table->decimal('total_unit_amount', 22,4);
            $table->date('value_date')->nullable();
            $table->boolean('is_update')->default(0);

            $table->decimal('selling_price', 22,4)->nullable();
            $table->bigInteger('total_unit');
            $table->date('selling_date')->nullable();
            $table->boolean('is_sell')->default(0);


            $table->decimal('dividend_nav', 22,4)->nullable();
            $table->decimal('dividend_income', 22,4)->nullable();
            $table->decimal('dividend_receivable', 22,4)->nullable();
            $table->date('dividend_declaration_date')->nullable();
            $table->date('dividend_payment_date')->nullable();
            $table->boolean('is_paid')->default(0);
            $table->boolean('is_dividend')->default(0);
            $table->timestamps();
            
            $table->foreign('scheme_id')->references('id')->on('investment_collective_schemes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('lot_id')->references('id')->on('investment_collective_lots')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });

        DB::statement("comment on column investment_collective_updates.is_update is 'true=> updating price false=>not'");
        DB::statement("comment on column investment_collective_updates.is_sell is 'true=> selling units false=>not'");
        DB::statement("comment on column investment_collective_updates.is_dividend is 'true=> is dividend false=>not'");
        DB::statement("comment on column investment_collective_updates.lot_id is 'not null when selling| refer to which lot are we selling'");
        DB::statement("comment on column investment_collective_updates.category_id is '1=> IPO , 2=>Secondary'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_collective_updates');
    }
}
