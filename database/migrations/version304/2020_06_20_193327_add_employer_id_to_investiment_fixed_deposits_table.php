<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployerIdToInvestimentFixedDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investiment_fixed_deposits', function (Blueprint $table) {
            $table->unsignedInteger('employer_id')->nullable();
            $table->foreign('employer_id')->references('id')->on('main.employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_fixed_deposits', function (Blueprint $table) {
            $table->dropColumn('employer_id');
        });
    }
}
