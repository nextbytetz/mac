<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNullabkeNotificationStageOnIncidents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
update notification_reports set notification_staging_cv_id = ns.notification_staging_cv_id from notification_stages ns where notification_reports.notification_stage_id = ns.id and notification_reports.isprogressive = 1 and notification_reports.notification_staging_cv_id is null;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
