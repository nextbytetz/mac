<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHcpNotificationReportOshData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('hcp_notification_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('health_provider_id');
            $table->integer('notification_report_id');
            $table->timestamps();
        });

        Schema::table('hcp_notification_report', function (Blueprint $table) {
            $table->foreign('health_provider_id')->references('id')->on('health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('notification_reports', function (Blueprint $table) {
            $table->integer('accident_cause_agency_cv_id')->nullable();
            $table->integer('accident_cause_type_cv_id')->nullable();
        });

        Schema::table('notification_reports', function (Blueprint $table) {
            $table->foreign('accident_cause_agency_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('accident_cause_type_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
