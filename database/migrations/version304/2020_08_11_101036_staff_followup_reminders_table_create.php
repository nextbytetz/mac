<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffFollowupRemindersTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('staff_followup_reminders', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->smallInteger('weekly_missing_target')->default(0);
            $table->smallInteger('weekly_target')->default(0);
            $table->date('week_start_date');
            $table->date('week_end_date');
            $table->smallInteger('prev_missing_target')->default(0);
            $table->smallInteger('total_missing_target')->default(0);
            $table->integer('parent_reminder_id')->nullable();
            $table->smallInteger('isnotified')->default(0);
            $table->smallInteger('elevation_level')->default(1);
            $table->smallInteger('isactive')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("comment on table staff_followup_reminders is 'Table to keep track of all missing weekly target - reminders in debt management for compliances'");

        DB::statement("comment on column staff_followup_reminders.weekly_missing_target is 'Weekly missing target - no of followups not done on that week'");
        DB::statement("comment on column staff_followup_reminders.prev_missing_target is 'No. of followups not done on previous week(s) which has active reminders'");
        DB::statement("comment on column staff_followup_reminders.total_missing_target is 'Total missing target current and previous weeks'");
        DB::statement("comment on column staff_followup_reminders.elevation_level is 'Elevation levels for notification email forwarding i.e. 1 => CM, 2 => CM , 3 => DO, CM'");


        Schema::table('staff_followup_reminders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('parent_reminder_id')->references('id')->on('staff_followup_reminders')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });


        Schema::table('staff_employer_configs', function (Blueprint $table) {

            $table->smallInteger('followup_weekly_target')->default(25);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
