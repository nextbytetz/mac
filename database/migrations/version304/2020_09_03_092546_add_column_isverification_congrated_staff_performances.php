<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsverificationCongratedStaffPerformances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_collection_performances', function (Blueprint $table) {

            $table->smallInteger('isverification_congrated')->default(0);

        });

        DB::statement("comment on column staff_employer_collection_performances.isverification_congrated is 'Flag to imply if congrated for 100% verification achievement i.e. 1 => yes, 0 => not yet'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
