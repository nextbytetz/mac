<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentCollectiveLots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('investment_collective_lots', function (Blueprint $table) {
       $table->increments('id');
       $table->unsignedInteger('scheme_id');
       $table->integer('category_id');
       $table->string('lot_number');
       $table->bigInteger('purchased_unit');
       $table->decimal('purchase_nav', 22,4);
       $table->decimal('purchase_amount', 22,4);
       $table->date('purchase_date');
       $table->bigInteger('remaining_unit');
       $table->date('disbursment_date');
       $table->timestamps();

       $table->foreign('scheme_id')->references('id')->on('investment_collective_schemes')->onUpdate('CASCADE')->onDelete('RESTRICT');

     });

      DB::statement("comment on column investment_collective_lots.category_id is '1=> IPO , 2=>Secondary'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('investment_collective_lots');
    }
  }
