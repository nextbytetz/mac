<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModifieTypeToInvestimentCorporateBonds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investiment_corporate_bonds', function (Blueprint $table) {
            if (Schema::hasColumn('investiment_corporate_bonds', 'type'))
            {
            Schema::table('investiment_corporate_bonds', function (Blueprint $table)
            {
                 $table->dropColumn('type');
            });
            }
            $table->unsignedInteger('type')->nullable();
            $table->foreign('type')->references('id')->on('investment_code_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_corporate_bonds', function (Blueprint $table) {
            
        });
    }
}
