<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCouponDateToCorporateTreasuryBonds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investiment_corporate_bonds', function(Blueprint $table)
        {
            $table->renameColumn('coupon_date', 'first_coupon_date');
            $table->date('next_coupon_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_corporate_bonds', function(Blueprint $table)
        {
            // $table->renameColumn('first_coupon_date', 'coupon_date');
            // $table->dropColumn('next_coupon_date')->nullable();
        });
    }
}
