<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnManualFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('manual_notification_reports', function (Blueprint $table) {
                      $table->integer('bodily_location_cv_id')->nullable();
            $table->integer('injury_nature_cv_id')->nullable();
            $table->dropColumn('injury_nature');
            $table->dropColumn('bodily_location');
        });


        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->foreign('bodily_location_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('injury_nature_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
