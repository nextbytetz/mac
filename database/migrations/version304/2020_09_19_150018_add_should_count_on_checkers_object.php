<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShouldCountOnCheckersObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkers', function (Blueprint $table) {
            $table->smallInteger('shouldcount')->default(1);
        });
        DB::statement("comment on column checkers.shouldcount is 'show whether check entry should count in the summary show'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkers', function (Blueprint $table) {
            //
        });
    }
}
