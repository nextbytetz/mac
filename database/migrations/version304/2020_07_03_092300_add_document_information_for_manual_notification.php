<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentInformationForManualNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->smallInteger('isdmsposted')->default(0);
        });
        Schema::create('document_manual_notification_report', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('manual_notification_report_id')->index();
            $table->bigInteger('document_id')->index();
            $table->string('name', 150)->nullable();
            $table->text('description')->nullable();
            $table->string('ext', 20)->nullable();
            $table->float('size', 10, 0)->nullable();
            $table->string('mime', 150)->nullable();
            $table->integer('eoffice_document_id')->nullable()->unique();
            $table->date('doc_date')->nullable()->comment('date when document was prepared by the member');
            $table->date('doc_receive_date')->nullable()->comment('date when the document was received at WCF office');
            $table->date('doc_create_date')->nullable()->comment('date when document was created in e-office');
            $table->integer('folio')->nullable();
            $table->smallInteger('isreferenced')->default(0)->comment('show whether document has already been referenced for a specific benefit or not. 1 - Yes, 0 - No');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
