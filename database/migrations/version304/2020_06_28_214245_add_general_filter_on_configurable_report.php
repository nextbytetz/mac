<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGeneralFilterOnConfigurableReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurable_reports', function (Blueprint $table) {
            $table->json('query_filter')->nullable();
        });
        DB::statement("comment on column configurable_reports.query_filter is 'general query filter from the report applied by the system, not available for selection as filter. '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
