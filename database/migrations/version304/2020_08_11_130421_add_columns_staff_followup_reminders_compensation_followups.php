<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStaffFollowupRemindersCompensationFollowups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_followup_reminders', function (Blueprint $table) {

            $table->smallInteger('compensation_followups')->default(0);
            $table->date('completed_date')->nullable();
            $table->smallInteger('days_worked')->default(0);
        });


        DB::statement("comment on column staff_followup_reminders.compensation_followups is 'Compensation followups from future work week to decrease missing targets'");
        DB::statement("comment on column staff_followup_reminders.completed_date is 'Date this reminder was deactivated'");
        DB::statement("comment on column staff_followup_reminders.days_worked is 'Days, staff worked on this week'");



        Schema::table('staff_employer_configs', function (Blueprint $table) {

            $table->date('followup_reminder_next_date')->nullable();

        });

        DB::statement("comment on column staff_employer_configs.followup_reminder_next_date is 'Next date is start of week to run job to post missing target reminders'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
