<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArchiveAlertOnWfTrack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_tracks', function(Blueprint $table)
        {
            $table->bigInteger('alert_cv_id')->nullable();
            $table->foreign('alert_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column wf_tracks.alert_cv_id is 'specify different alerts for workflow as specified in code_values table'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
