<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFunctionForWorkflowAgingIi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create or replace function main.wf_aging(receive_date timestamp[0][], forward_date timestamp[0][], process_dates json[] default NULL) returns integer
	language plpgsql
as $$
declare i smallint;
diff bigint := 0;
datediff interval;
archivediff bigint := 0;
datearchivediff interval;
ij_key bigint;
ij_value json;
max_forward_date timestamp(0);
    
begin
    FOR i IN 1 .. array_upper(receive_date, 1)
LOOP
  max_forward_date := COALESCE((forward_date[i]), now());
  datediff := max_forward_date - receive_date[i];
  diff := diff + ((DATE_PART('day', datediff) * 24 + DATE_PART('hour', datediff)) * 60 + DATE_PART('minute', datediff)) * 60 + DATE_PART('second', datediff);
END LOOP;
    
if (process_dates is not null) then
    for i in 1 .. array_upper(process_dates, 1)
    loop
    FOR ij_key, ij_value IN SELECT * FROM json_each(process_dates[i])
      LOOP
        if ((ij_value->>'to_date')::timestamp > max_forward_date) then
            datearchivediff = max_forward_date - (ij_value->>'from_date')::timestamp;
            else
            datearchivediff = (ij_value->>'to_date')::timestamp - (ij_value->>'from_date')::timestamp;
        end if;
        archivediff := archivediff + ((DATE_PART('day', datearchivediff) * 24 + DATE_PART('hour', datearchivediff)) * 60 + DATE_PART('minute', datearchivediff)) * 60 + DATE_PART('second', datearchivediff);
        -- RAISE NOTICE 'output from space %', ij->>'from_date';
        -- RAISE NOTICE '%: %', ij_key, ij_value;
      END LOOP;
    end loop;
end if;

-- RAISE NOTICE '%', diff;
-- RAISE NOTICE '%', archivediff;
-- RAISE NOTICE '%', round((diff - archivediff)/86400);
    
return floor((diff - archivediff)/86400);
    
end
$$
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
