<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquityUpdateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('equity_updates', function (Blueprint $table) {
       $table->increments('id');
       $table->unsignedInteger('investment_equity_id');
       $table->decimal('price_per_share', 22,4);
       $table->bigInteger('number_of_shares');
       $table->date('share_price_date')->nullable();
       $table->decimal('total_share_values', 22,4)->nullable();
       $table->date('selling_date')->nullable();
       $table->string('selling_broker')->nullable();
       $table->integer('selling_broker_regno')->nullable();
       $table->decimal('commission', 22,4)->nullable();
       $table->boolean('is_sell')->default(0);
       $table->timestamps();

       $table->foreign('investment_equity_id')->references('id')->on('investment_equities')->onUpdate('CASCADE')->onDelete('RESTRICT');

     });
      DB::statement("comment on column equity_updates.total_share_values is ' = price_per_share times number_of_shares'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('equity_updates');
    }
  }
