<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuspenseFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('payroll_status_changes', function (Blueprint $table) {
            $table->smallInteger('old_suspense_flag')->default(0);
        });
        DB::statement("comment on column payroll_status_changes.old_suspense_flag is 'Suspense flag at the time of status change initiation'");

        Schema::table('payroll_bank_info_updates', function (Blueprint $table) {
            $table->smallInteger('old_suspense_flag')->default(0);
        });
        DB::statement("comment on column payroll_bank_info_updates.old_suspense_flag is 'Suspense flag at the time of initiating bank updates'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
