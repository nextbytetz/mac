<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckForFinYearOnConfigurableReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurable_reports', function (Blueprint $table) {
            $table->boolean('has_fin_year')->default('t');
        });
        DB::statement("comment on column configurable_reports.has_fin_year is 'specify whether report has integrated financial years columns with table '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurable_reports', function (Blueprint $table) {
            //
        });
    }
}
