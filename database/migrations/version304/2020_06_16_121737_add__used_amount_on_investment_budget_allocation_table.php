<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsedAmountOnInvestmentBudgetAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investment_budget_allocation', function (Blueprint $table) {
            $table->decimal('amount_spent', 20,2)->nullable();
            $table->unsignedInteger('fin_year_id')->nullable();
            $table->foreign('fin_year_id')->references('id')->on('fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investment_budget_allocation', function (Blueprint $table) {
            //
        });
    }
}
