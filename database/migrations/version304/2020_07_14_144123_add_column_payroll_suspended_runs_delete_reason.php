<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPayrollSuspendedRunsDeleteReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_suspended_runs', function (Blueprint $table) {
            $table->text('delete_reason')->nullable();
        });


        DB::statement("comment on column payroll_suspended_runs.delete_reason is 'Reason for deactivating this payment'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
