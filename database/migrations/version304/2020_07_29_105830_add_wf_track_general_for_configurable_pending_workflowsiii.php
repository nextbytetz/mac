<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfTrackGeneralForConfigurablePendingWorkflowsiii extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
select deps_save_and_drop_dependencies('main', 'wf_track_general');
drop view if exists wf_track_general;
create or replace view wf_track_general as select a.id as wf_track_id, a.user_id, a.wf_definition_id, b.id as unit_id, b.designation_id, b.wf_module_id, a.wf_archive_id, e.wf_module_group_id, a.alert_cv_id, a.resource_id, a.status, a.comments, a.assigned, a.parent_id, a.receive_date, a.forward_date, DATE_PART('day', coalesce(a.forward_date, now()) - a.receive_date) as aging, a.deleted_at as wf_track_deleted_at, a.user_type, a.resource_type, b.level, c.name as unit, d.name as designation, e.name module, i.name modulegroup, f.from_date as archive_from_date, f.to_date as archive_to_date, cv.name as archive_reason, h.name as wf_alert, f.archive_reason_cv_id, a.receive_date as registration_date from wf_tracks a join wf_definitions b on a.wf_definition_id = b.id join units c on b.unit_id = c.id join designations d on d.id = b.designation_id join wf_modules e on b.wf_module_id = e.id left join wf_archives f on a.wf_archive_id = f.id left join code_values cv on f.archive_reason_cv_id = cv.id left join users g on a.user_id = g.id left join code_values h on h.id = a.alert_cv_id join wf_module_groups i on e.wf_module_group_id = i.id;
select deps_restore_dependencies('main', 'wf_track_general');
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
