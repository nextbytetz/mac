<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestimentFixedDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investiment_fixed_deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->date('opening_date');
            $table->date('maturity_date');
            $table->float('interest_rate');
            $table->string('interest_type');
            $table->string('penalty_rate');
            $table->string('tenure');
            $table->float('amount_invested');
            $table->string('institution_name');
            $table->float('tax_rate');
            $table->string('certificate_number')->nullable();
            $table->float('interest_received')->nullable();
            $table->float('principal_redeemed')->nullable();
            $table->float('penalty_received')->nullable();
            $table->date('payment_date_received')->nullable();
            $table->float('interest_receivable')->nullable();
            $table->float('interest_income')->nullable();
            $table->float('weighted_average_return')->nullable();
            $table->float('penalty_income')->nullable();
            $table->float('penalty_receivable')->nullable();
            $table->float('overpaid_interest')->nullable();
            $table->unsignedInteger('investment_budget_id')->nullable();
            $table->foreign('investment_budget_id')->references('id')->on('investment_budgets')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investiment_fixed_deposits');
    }
}
