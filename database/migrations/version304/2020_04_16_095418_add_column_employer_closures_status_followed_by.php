<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployerClosuresStatusFollowedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->integer('followup_by')->nullable();
        });
        DB::statement("comment on column employer_closures.followup_by is 'User who made followed up to check status of business for temporary closed'");

        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->foreign('followup_by')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
