<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrenyIdAndCurrencyExchangeToFixedDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investiment_fixed_deposits', function(Blueprint $table)
        {
            $table->unsignedInteger('currency_id')->nullable();
            $table->foreign('currency_id')->references('id')->on('currencies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->float('exchange_rate')->nullable();
        });

        Schema::table('fixed_deposit_updates', function(Blueprint $table)
        {
            $table->unsignedInteger('currency_id')->nullable();
            $table->foreign('currency_id')->references('id')->on('currencies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->float('exchange_rate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investiment_fixed_deposits', function(Blueprint $table)
        {
            $table->dropColumn('currency_id');
            $table->dropColumn('exchange_rate');
        });
        
         Schema::table('fixed_deposit_updates', function(Blueprint $table)
        {
            $table->dropColumn('currency_id');
            $table->dropColumn('exchange_rate');
        });
    }
}
