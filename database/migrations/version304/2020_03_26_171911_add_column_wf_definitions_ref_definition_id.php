<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnWfDefinitionsRefDefinitionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('wf_definitions', function(Blueprint $table)
        {
            $table->integer('ref_level')->nullable();

        });

        DB::statement("comment on column wf_definitions.ref_level is 'Reference wf definition level to pick user last participated on that level e.g. reference to initiator (assign this level to initiator) '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
