<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotTableHspManualNotificationReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('hcp_manual_notification_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('health_provider_id');
            $table->integer('manual_notification_report_id');
            $table->timestamps();
        });

        Schema::table('hcp_manual_notification_report', function (Blueprint $table) {
            $table->foreign('health_provider_id')->references('id')->on('health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('manual_notification_report_id')->references('id')->on('manual_notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
