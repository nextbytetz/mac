<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHoldPayrollBenefiriariesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //dependents
        Schema::table('dependent_employee', function(Blueprint $table)
        {
            $table->text('hold_reason')->nullable();
        });
        DB::statement("comment on column dependent_employee.hold_reason is 'Reason for holding this dependent from enrolling to payroll'");
        DB::statement("comment on column dependent_employee.isactive is 'Specify whether the dependents is active to receive monthly pension; 0 - inactive, 1 => Active, 2 => Deactivated, 3 => on hold (Before payroll enrollment)'");


        //pensioners
        Schema::table('pensioners', function(Blueprint $table)
        {
            $table->text('hold_reason')->nullable();
        });
        DB::statement("comment on column pensioners.hold_reason is 'Reason for holding this dependent from enrolling to payroll'");
        DB::statement("comment on column pensioners.isactive is 'Specify whether the pensioners is active to receive monthly pension; 0 - inactive, 1 => Active, 2 => Deactivated, 3 => on hold (Before payroll enrollment) '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
