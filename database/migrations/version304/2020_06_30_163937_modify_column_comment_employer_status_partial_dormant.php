<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnCommentEmployerStatusPartialDormant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("comment on column employers.employer_status is 'Flag to specify employer business status; 1 => Active, 2=> Dormant, 3=> Closed Business, 4=> Partial Dormant (Dormant through de-registration)' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
