<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeModificationOnIncidentPerformanceBaseViewv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
select deps_save_and_drop_dependencies('main', 'incident_performance_base');
drop view if exists incident_performance_base;
create or replace view incident_performance_base as select d.name as district, r.name as region, concat_ws(' ', usr.firstname, usr.lastname) as staff, it.name as incident_type, concat_ws(' ', emp.firstname, coalesce(emp.middlename, ''), emp.lastname) as employee, e.name as employer, nr.incident_date, nr.reporting_date, nr.receipt_date, nr.created_at::date as registration_date, cv.name as incident_stage, case when nr.isprogressive = 1 then 'Progressive' else 'Non-Progressive' end as category, case when nr.source = 1 then 'Branch Registered' else 'Branch Registered' end as registration_source, cv.reference, nr.investigation_validity, is_acknowledgment_sent, is_acknowledgment_initiated, case when nr.status = 3 then cv.code_id when nr.status = 2 then 23 else 13 end as code_id, nr.source, usr.id as attribute, usr.id as user_id, nr.incident_type_id, nr.district_id, d.region_id, nr.notification_staging_cv_id, nr.isprogressive, nr.status, case when nr.status = 3 then nr.progressive_stage when nr.status = 1 and nr.wf_done = 0 then 4 when nr.status = 1 and nr.wf_done = 1 then 5 else 2 end as progressive_stage, nr.allocated, nr.employer_id, nr.employee_id, nr.id as incident_id from notification_reports nr left join users usr on nr.allocated = usr.id left join code_values cv on nr.notification_staging_cv_id = cv.id join employers e on nr.employer_id = e.id join employees emp on emp.id = nr.employee_id join incident_types it on nr.incident_type_id = it.id left join districts d on nr.district_id = d.id left join regions r on d.region_id = r.id;
select deps_restore_dependencies('main', 'incident_performance_base');
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
