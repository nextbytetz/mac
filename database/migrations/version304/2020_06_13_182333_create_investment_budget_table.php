<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fin_year_id');
            $table->decimal('amount', 20,2);
            $table->timestamps();
            $table->foreign('fin_year_id')->references('id')->on('fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_budgets');
    }
}
