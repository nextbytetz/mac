<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCommentOnColumnStaffEmployer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("comment on column staff_employer.replacement_type is 'Replacement type to specify reason for this allocation replacement i.e. 1=> small, 2=> duplicate, 3=> closed, 4=>dormant. 5=> others active, 6 => Revoked/removed'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
