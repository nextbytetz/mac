<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArchiveReasonTableForWorkflows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_archives', function(Blueprint $table)
        {
            $table->bigInteger('archive_reason_cv_id')->nullable();
            $table->foreign('archive_reason_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::create('wf_archive_reason_codes', function(Blueprint $table)
        {
            $table->smallIncrements("id");
            $table->bigInteger("wf_module_group_id");
            $table->bigInteger("code_id");
            $table->timestamps();
            $table->foreign('wf_module_group_id')->references('id')->on('wf_module_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('code_id')->references('id')->on('codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
