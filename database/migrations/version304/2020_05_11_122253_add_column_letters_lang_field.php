<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLettersLangField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('letters', function(Blueprint $table)
        {
            $table->string('lang')->default('en');
        });
        DB::statement("comment on column letters.lang is 'Language for letters template i.e. en - English, sw - Swahili'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
