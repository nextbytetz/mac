<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForManualNotificationReportsPartIi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->string('external_id', 50)->nullable();
            $table->string('incident_type', 50)->nullable();
            $table->date('registration_date')->nullable();
            $table->string('district', 50)->nullable();
            $table->bigInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
