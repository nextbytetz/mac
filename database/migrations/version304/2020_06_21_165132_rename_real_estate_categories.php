<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRealEstateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('real_estate_categories');
        Schema::create('investment_code_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('code_value_id');
            $table->string('code_value_reference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_code_types');
    }
}
