<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployerClosuresLastContribCloseDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->decimal('last_contribution',16,2)->nullable();
        });

        DB::statement("comment on column employer_closures.last_contribution is 'Last contribution at the time of this dormant de-registration'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
