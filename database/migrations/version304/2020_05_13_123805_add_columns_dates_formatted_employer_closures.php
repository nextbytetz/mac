<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDatesFormattedEmployerClosures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function(Blueprint $table)
        {
            $table->date('close_date_internal')->nullable();
            $table->date('specified_reopen_date_internal')->nullable();
        });

        DB::statement("comment on column employer_closures.close_date_internal is 'Date of closure formatted for internal use i.e. 1-15 day will be rounded to start of the month, 16-31 will be rounded to end of the month'");
        DB::statement("comment on column employer_closures.specified_reopen_date_internal is 'Date of reopening business which will be rounded to start of the month '");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
