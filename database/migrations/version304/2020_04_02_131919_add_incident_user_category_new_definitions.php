<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncidentUserCategoryNewDefinitions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incident_users', function(Blueprint $table)
        {
            $table->bigInteger('iuc_cv_id')->nullable();
            $table->foreign('iuc_cv_id')->references('id')->on('main.code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table incident_users is 'store the selected allocation of users into different category of applications. e.g. Notification & Claim checklist users'");
        Schema::rename('region_user', 'incident_user_regions');
        DB::statement("comment on table incident_user_regions is 'allocation of incident users into regions'");
        Schema::table('incident_user_regions', function(Blueprint $table)
        {
            $table->dropColumn('user_id');
            $table->smallInteger('incident_user_id');
            $table->foreign('incident_user_id')->references('id')->on('main.incident_users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
