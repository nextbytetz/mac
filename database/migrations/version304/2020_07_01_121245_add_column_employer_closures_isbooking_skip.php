<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmployerClosuresIsbookingSkip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employer_closures', function (Blueprint $table) {
            $table->smallInteger('isbookingskip')->default(1);

        });

        DB::statement("comment on column employer_closures.isbookingskip is 'Flag to specify if this closure qualifies for booking skipping i.e. 1 => skip, 0 => not skipping' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
