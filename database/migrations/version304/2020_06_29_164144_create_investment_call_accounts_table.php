<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentCallAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('investment_call_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('financial_year_id')->nullable();
            $table->float('balance_amount');
            $table->float('interest_rate');
            $table->float('tax_rate');
            $table->float('received_interest');
            $table->float('interest_income');
            $table->float('interest_receivable');
            $table->date('balance_date');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_call_accounts');
    }
}
