<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('region_user');
        Schema::create('region_user', function(Blueprint $table)
        {
            $table->smallIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('region_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('main.users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('region_id')->references('id')->on('main.regions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on table region_user is 'hold the allocation of user into region'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
