<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClaimPerformanceBaseViewsii extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create or replace view incident_performance_base as select concat_ws(' ', usr.firstname, usr.lastname) as staff, it.name as incident_type, concat_ws(' ', emp.firstname, coalesce(emp.middlename, ''), emp.lastname) as employee, e.name as employer, nr.incident_date, nr.reporting_date, nr.receipt_date, nr.created_at::date as registration_date, d.name as district, r.name as region, cv.name as incident_stage, case when nr.isprogressive = 1 then 'Progressive' else 'Non-Progressive' end as category, case when nr.source = 1 then 'Branch Registered' else 'Branch Registered' end as registration_source, cv.reference, nr.investigation_validity, is_acknowledgment_sent, cv.code_id, nr.source, usr.id as attribute, usr.id as user_id, nr.incident_type_id, nr.district_id, d.region_id, nr.notification_staging_cv_id, nr.isprogressive, nr.progressive_stage, nr.allocated, nr.employer_id, nr.employee_id, nr.id as incident_id from notification_reports nr left join users usr on nr.allocated = usr.id join code_values cv on nr.notification_staging_cv_id = cv.id join employers e on nr.employer_id = e.id join employees emp on emp.id = nr.employee_id join incident_types it on nr.incident_type_id = it.id left join districts d on nr.district_id = d.id left join regions r on d.region_id = r.id where nr.isprogressive = 1;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
