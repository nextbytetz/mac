<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFirstpayFlagStatusChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('payroll_status_changes', function (Blueprint $table) {
            $table->smallInteger('firstpay_flag')->nullable();
        });


        DB::statement("comment on column payroll_status_changes.firstpay_flag is 'Flag to imply if beneficiary is already paid i.e. 1 => already paid, 2 => Not paid yet'");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
