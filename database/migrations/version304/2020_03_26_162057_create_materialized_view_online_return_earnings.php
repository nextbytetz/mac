<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterializedViewOnlineReturnEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("CREATE MATERIALIZED VIEW  online_return_earnings  AS   select e.reg_no,eu.payroll_id,e.name as employer_name,a.status as submission_status,a.created_at as uploaded_date,u.name as user_submitted,a.submitted_date,u.email,u.phone from portal.annual_returns_summary a 
join main.employers e on a.employer_id=e.id
join portal.users u on a.user_id=u.id
join portal.employer_user eu on eu.employer_id=a.employer_id
  "); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
