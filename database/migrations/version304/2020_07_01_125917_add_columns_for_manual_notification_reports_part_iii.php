<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForManualNotificationReportsPartIii extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_notification_reports', function (Blueprint $table) {
            $table->string('claim_status', 150)->nullable();
            $table->text('reject_reason')->nullable();
            $table->bigInteger('status_cv_id')->nullable();
            $table->bigInteger('reject_reason_cv_id')->nullable();
            $table->foreign('status_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('reject_reason_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
