<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreasuryBillsUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treasury_bills_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->date('payment_date');
            $table->float('interest_earned');
            $table->unsignedInteger('treasury_bill_id')->nullable();
            $table->foreign('treasury_bill_id')->references('id')->on('investiment_treasury_bills')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treasury_bills_updates');
    }
}
