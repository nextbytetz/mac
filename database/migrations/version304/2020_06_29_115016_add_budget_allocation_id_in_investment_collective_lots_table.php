<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBudgetAllocationIdInInvestmentCollectiveLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investment_collective_lots', function (Blueprint $table) {
            $table->unsignedInteger('budget_allocation_id')->nullable();
            $table->unsignedInteger('fin_year_id')->nullable();
            $table->foreign('fin_year_id')->references('id')->on('fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('budget_allocation_id')->references('id')->on('investment_budget_allocation')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investment_collective_lots', function (Blueprint $table) {
            //
        });
    }
}
