<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigurableReportCategoryObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurable_report_types', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name', 150);
            $table->smallInteger('isbalance')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurable_report_types', function (Blueprint $table) {
            //
        });
    }
}
