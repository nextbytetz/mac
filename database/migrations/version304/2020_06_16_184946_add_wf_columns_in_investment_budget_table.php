<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWfColumnsInInvestmentBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investment_budgets', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('status')->nullable();
            $table->smallInteger('wf_done')->default(0);
            $table->date('wf_done_date')->nullable();
        });

        DB::statement("comment on column investment_budgets.status is 'status of the workflow 1 - initiated/pending for approval, 2 - Approved, 3 - Rejected'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investment_budgets', function (Blueprint $table) {
            //
        });
    }
}
