<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentBudgetMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_budget_months', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('investment_budget_category_id');
            $table->unsignedInteger('fin_year_id');
            $table->unsignedInteger('code_value_id');
            $table->string('code_value_reference');
            $table->string('investment_type_name');
            $table->unsignedInteger('investment_code_type_id')->nullable();
            $table->decimal('july', 20,2);
            $table->decimal('august', 20,2);
            $table->decimal('september', 20,2);
            $table->decimal('october', 20,2);
            $table->decimal('november', 20,2);
            $table->decimal('december', 20,2);
            $table->decimal('january', 20,2);
            $table->decimal('february', 20,2);
            $table->decimal('march', 20,2);
            $table->decimal('april', 20,2);
            $table->decimal('may', 20,2);
            $table->decimal('june', 20,2);
            $table->decimal('total', 20,2);
            $table->timestamps();
           
            $table->foreign('code_value_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fin_year_id')->references('id')->on('fin_years')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_budget_months');
    }
}
