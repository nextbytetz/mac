<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlaRulesObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sla_alerts', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('reference', 20);
            $table->bigInteger('designation_id');
            $table->bigInteger('unit_id');
            $table->smallInteger('days_after');
            $table->smallInteger('sort');
            $table->smallInteger('isactive')->default(1);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->unique(['reference', 'sort']);
            $table->foreign('designation_id')->references('id')->on('designations')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('reference')->references('reference')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        DB::statement("comment on column sla_alerts.days_after is 'is the number of days after which users under the specified designation should be notified.'");
        DB::statement("comment on column sla_alerts.sort is 'sequence of alerts to officer under the specified designations concerning a specified alert reference.'");
        DB::statement("comment on table sla_alerts is 'holds information about alerting officers in the specified designations on delays of working on files'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
