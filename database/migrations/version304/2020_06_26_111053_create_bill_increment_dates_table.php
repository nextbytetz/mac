<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillIncrementDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_increment_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->date('interest_date');
            $table->float('interest_amount');
            $table->date('payment_date')->nullable();
            $table->float('interest_earned')->nullable();
            $table->unsignedInteger('treasury_bill_id')->nullable();
            $table->foreign('treasury_bill_id')->references('id')->on('investiment_treasury_bills')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_increment_dates');
    }
}
