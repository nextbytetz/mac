<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBondIncrementDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bond_increment_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->date('interest_date');
            $table->float('interest_amount');
            $table->date('payment_date')->nullable();
            $table->float('interest_earned')->nullable();
            $table->unsignedInteger('treasury_bond_id')->nullable();
            $table->foreign('treasury_bond_id')->references('id')->on('investiment_treasury_bonds')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->unsignedInteger('corporate_bond_id')->nullable();
            $table->foreign('corporate_bond_id')->references('id')->on('investiment_corporate_bonds')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bond_increment_dates');
    }
}
