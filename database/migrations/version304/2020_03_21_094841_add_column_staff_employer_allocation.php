<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerAllocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_allocations', function(Blueprint $table)
        {
            $table->smallInteger('status')->default(1);

        });

        DB::statement("comment on column staff_employer_allocations.status is 'Status of allocation 1 - on progress/active, 2 => complete'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
