<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentBudgetAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_budget_allocation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('investment_budget_id');
            $table->unsignedInteger('code_value_id');
            $table->decimal('amount', 20,2);
            $table->decimal('percent');
            $table->timestamps();
            $table->foreign('investment_budget_id')->references('id')->on('investment_budgets')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('code_value_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_budget_allocation');
    }
}
