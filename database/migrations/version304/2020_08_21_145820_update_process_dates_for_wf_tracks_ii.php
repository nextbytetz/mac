<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProcessDatesForWfTracksIi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
do $$
    declare t record;
    _process_dates json;
begin
CREATE TEMPORARY TABLE archives (
  id bigint,
  from_date date,
  to_date date
);

    for t IN select wf_track_id, array_agg(id order by from_date) as id, array_agg(from_date order by from_date) as from_date, array_agg(to_date order by from_date) as to_date from wf_archives group by wf_track_id

    loop
        insert into archives(id, from_date, to_date) select id, from_date, to_date from wf_archives where wf_track_id = t.wf_track_id;

/*
 Update Algorithm
+--------------+-----------+--------------+--------------+------------------------------------------------------------+
|from_date     |to_date    |Outcome                                                                                   |
+--------------+--------------+--------------+--------------+---------------------------------------------------------+
|1             |1          |Skip/Delete                                                                               |
|1             |0          |update from_date of match on the matched, to_date of this on the matched and skip/Delete  |
|0             |1          |update from_date of this on the matched, to_date of match on the matched and skip/Delete  |
|0             |0          |Take the entry                                                                            |
+--------------+-----------+--------------+--------------+------------------------------------------------------------+
 */
    FOR i IN 1 .. array_upper(t.from_date, 1)
    LOOP
        if exists(select 1 from archives where t.from_date[i] not between from_date and to_date and t.to_date[i] between from_date and to_date and to_date <> t.to_date[i]) then
            -- update from_date of this on the matched, to_date of match on the matched and skip/Delete
            update archives set from_date = t.from_date[i] where t.from_date[i] not between from_date and to_date and t.to_date[i] between from_date and to_date and to_date <> t.to_date[i];
            delete from archives where id = t.id[i];
        elseif exists(select 1 from archives where t.from_date[i] between from_date and to_date and t.to_date[i] not between from_date and to_date and from_date <> t.from_date[i] ) then
            -- update from_date of match on the matched, to_date of this on the matched and skip/Delete
            update archives set to_date = t.to_date[i] where t.from_date[i] between from_date and to_date and t.to_date[i] not between from_date and to_date and from_date <> t.from_date[i];
            delete from archives where id = t.id[i];
        elseif exists(select 1 from archives where t.from_date[i] between from_date and to_date and t.to_date[i] between from_date and to_date and to_date <> t.to_date[i]) then
            -- Skip/Delete
            delete from archives where id = t.id[i];
        end if;
        -- RAISE NOTICE '%', t.from_date[i];
        -- RAISE NOTICE '%', t.id[i];
    END LOOP;
         -- collect result from archives into json
        select json_object_agg(wf.id, json_build_object('from_date', wf.from_date, 'to_date', wf.to_date)) into _process_dates from (select id, from_date, to_date from archives order by from_date) wf;
        -- RAISE NOTICE '%', _process_dates;
        update wf_tracks set process_dates = _process_dates where id = t.wf_track_id;

        delete from archives;
    end loop;
    DROP TABLE archives;
end$$;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
