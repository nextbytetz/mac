<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStaffEmployerAllocationIsintern extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('staff_employer_allocations', function(Blueprint $table)
        {
            $table->smallInteger('isintern')->default(0);

        });

        DB::statement("comment on column staff_employer_allocations.isintern is 'Flag to specify if allocation is for intern staff i.e. 1 > intern, 0 => staff'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
