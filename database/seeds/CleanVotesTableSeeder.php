<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class CleanVotesTableSeeder extends Seeder
{
use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clean_votes')->delete();
        
        \DB::table('clean_votes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'vote' => '2',
                'vote_name' => 'Tanzania Teachers Commission',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            1 => 
            array (
                'id' => 2,
                'vote' => '3',
                'vote_name' => 'Land Use Planning Commission',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            2 => 
            array (
                'id' => 3,
                'vote' => '4',
                'vote_name' => 'Records & Archives Management',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            3 => 
            array (
                'id' => 4,
                'vote' => '5',
                'vote_name' => 'National Irrigation Commission',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            4 => 
            array (
                'id' => 5,
                'vote' => '7',
                'vote_name' => 'Treasury Registrars Office',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            5 => 
            array (
                'id' => 6,
                'vote' => '9',
                'vote_name' => 'Secretariat of the Public Remunaration Board',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            6 => 
            array (
                'id' => 7,
                'vote' => '10',
                'vote_name' => 'Joint Finance Commission',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            7 => 
            array (
                'id' => 8,
                'vote' => '100',
                'vote_name' => 'Ministry of Minerals',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            8 => 
            array (
                'id' => 9,
                'vote' => '11',
                'vote_name' => 'Oil and Gas Bureau',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            9 => 
            array (
                'id' => 10,
                'vote' => '12',
                'vote_name' => 'Judiciary Service Commission',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            10 => 
            array (
                'id' => 11,
                'vote' => '13',
                'vote_name' => 'Baraza la Taifa la Uwezeshaji',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            11 => 
            array (
                'id' => 12,
                'vote' => '14',
                'vote_name' => 'Jeshi la Zimamoto na Uokoaji',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            12 => 
            array (
                'id' => 13,
                'vote' => '15',
                'vote_name' => 'Commission for Mediation and Arbitration',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            13 => 
            array (
                'id' => 14,
                'vote' => '16',
                'vote_name' => 'Attorney General\'s Office',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            14 => 
            array (
                'id' => 15,
                'vote' => '18',
                'vote_name' => 'High Court',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            15 => 
            array (
                'id' => 16,
                'vote' => '19',
                'vote_name' => 'Solicitor Gerenal',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            16 => 
            array (
                'id' => 17,
                'vote' => '21',
                'vote_name' => 'The Treasury',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            17 => 
            array (
                'id' => 18,
                'vote' => '22',
                'vote_name' => 'Public Debt & General Services',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            18 => 
            array (
                'id' => 19,
                'vote' => '23',
                'vote_name' => 'Accountant General\'s Dept',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            19 => 
            array (
                'id' => 20,
                'vote' => '24',
                'vote_name' => 'Cooperative Development Commision',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            20 => 
            array (
                'id' => 21,
                'vote' => '25',
                'vote_name' => 'Prime Minister-Private Office',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            21 => 
            array (
                'id' => 22,
                'vote' => '26',
                'vote_name' => 'Vice President',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            22 => 
            array (
                'id' => 23,
                'vote' => '27',
                'vote_name' => 'Registrar of Political Parties',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            23 => 
            array (
                'id' => 24,
                'vote' => '28',
                'vote_name' => 'Ministry Home Affairs-Police Force',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            24 => 
            array (
                'id' => 25,
                'vote' => '29',
                'vote_name' => 'Min Home Affairs-Prison Services',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            25 => 
            array (
                'id' => 26,
                'vote' => '31',
                'vote_name' => 'Vice Presidents Office',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            26 => 
            array (
                'id' => 27,
                'vote' => '32',
                'vote_name' => 'President Office-Public Service Management',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            27 => 
            array (
                'id' => 28,
                'vote' => '33',
                'vote_name' => 'Ethics Secretariat',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            28 => 
            array (
                'id' => 29,
                'vote' => '34',
                'vote_name' => 'Foreign Affairs and International Co-operation',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            29 => 
            array (
                'id' => 30,
                'vote' => '35',
                'vote_name' => 'Division of Public Prosecution',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            30 => 
            array (
                'id' => 31,
                'vote' => '36',
                'vote_name' => 'Katavi Region',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            31 => 
            array (
                'id' => 32,
                'vote' => '36AA2',
                'vote_name' => 'Nsimbo District Council',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            32 => 
            array (
                'id' => 33,
                'vote' => '36AA4',
                'vote_name' => 'Mlele District Council',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            33 => 
            array (
                'id' => 34,
                'vote' => '36AA5',
                'vote_name' => 'Mpimbwe District Council',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            34 => 
            array (
                'id' => 35,
                'vote' => '37',
                'vote_name' => 'Prime Minister\'s Office',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            35 => 
            array (
                'id' => 36,
                'vote' => '38',
                'vote_name' => 'Defence Unit',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            36 => 
            array (
                'id' => 37,
                'vote' => '39',
                'vote_name' => 'The National Service',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            37 => 
            array (
                'id' => 38,
                'vote' => '40',
                'vote_name' => 'Judiciary Department',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            38 => 
            array (
                'id' => 39,
                'vote' => '41',
                'vote_name' => 'Justice & Constitutional Affrs',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            39 => 
            array (
                'id' => 40,
                'vote' => '42',
                'vote_name' => 'NATIONAL ASSEMBLY FUND',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            40 => 
            array (
                'id' => 41,
                'vote' => '43',
                'vote_name' => 'Agricultural Food Security and Co-operation',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            41 => 
            array (
                'id' => 42,
                'vote' => '44',
                'vote_name' => 'Ministry of Industry and Trade',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            42 => 
            array (
                'id' => 43,
                'vote' => '45',
                'vote_name' => 'National Audit Office',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            43 => 
            array (
                'id' => 44,
                'vote' => '46',
                'vote_name' => 'Ministry of Education and Vocational Training',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            44 => 
            array (
                'id' => 45,
                'vote' => '47',
                'vote_name' => 'Simiyu Region',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            45 => 
            array (
                'id' => 46,
                'vote' => '47BB1',
                'vote_name' => 'Bariadi Town Council',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            46 => 
            array (
                'id' => 47,
                'vote' => '47BB5',
                'vote_name' => 'Busega District Council',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            47 => 
            array (
                'id' => 48,
                'vote' => '47BB6',
                'vote_name' => 'Itilima District Council',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            48 => 
            array (
                'id' => 49,
                'vote' => '48',
                'vote_name' => 'Lands, Housing and Human Settlement',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            49 => 
            array (
                'id' => 50,
                'vote' => '47',
                'vote_name' => 'Simiyu Region',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
            50 => 
            array (
                'id' => 51,
                'vote' => '47',
                'vote_name' => 'Simiyu Region',
                'created_at' => '2019-05-25 15:09:12',
                'updated_at' => '2019-05-25 15:09:12',
            ),
        ));
        
        
    }
}