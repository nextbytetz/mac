<?php

use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

/**
 * Class AccessTableSeeder.
 */
class Version100TableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        DB::beginTransaction();

          /*Facility codes*/
        $this->call(PaymentRatesSeeder::class);
        /*end*/

        $this->call(AccidentTypesTableSeeder::class);
        /**
        $this->call(AlertTypesTableSeeder::class);
        $this->call(AuditActionsTableSeeder::class);
        $this->call(AuditEntitiesTableSeeder::class);
        $this->call(AuditReferencesTableSeeder::class);
         */
        $this->call(BanksTableSeeder::class);
        $this->call(BankAccountsTableSeeder::class);
        $this->call(BankBranchesTableSeeder::class);
        $this->call(FinCodeGroupsTableSeeder::class);
        $this->call(FinCodesTableSeeder::class);
        $this->call(FinAccountCodesTableSeeder::class);
        $this->call(BenefitTypeGroupsTableSeeder::class);
        $this->call(BenefitTypesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(DeathCausesTableSeeder::class);
        $this->call(DependentTypesTableSeeder::class);
        $this->call(DesignationsTableSeeder::class);
        $this->call(DisabilityStateChecklistsTableSeeder::class);
        $this->call(DiseaseKnowHowsTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(DocumentGroupsTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(IncidentTypesTableSeeder::class);
        $this->call(DocumentsTableSeeder::class);
        $this->call(DocumentGroupIncidentTypeTableSeeder::class);
        /* $this->call(DutyTypesTableSeeder::class); //Dropped */
        $this->call(ExitCodesTableSeeder::class);
        $this->call(GendersTableSeeder::class);
        $this->call(HealthServiceChecklistsTableSeeder::class);
        $this->call(HealthStateChecklistsTableSeeder::class);
        $this->call(IdentitiesTableSeeder::class);
        $this->call(JobTitlesTableSeeder::class);
        $this->call(LocationTypesTableSeeder::class);
        /*$this->call(MaritalStatusesTableSeeder::class);*/
        $this->call(MemberTypesTableSeeder::class);
        $this->call(OfficesTableSeeder::class);
        $this->call(PaymentTypesTableSeeder::class);
        $this->call(PermissionGroupsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(SysdefsTableSeeder::class);
        $this->call(UnitGroupsTableSeeder::class);
        $this->call(UnitsTableSeeder::class);
        $this->call(InspectionTypesTableSeeder::class);
        /*$this->call(EmployeeCategoriesTableSeeder::class);*/
        /* $this->call(BenefitTypeDutyTypeTableSeeder::class); //Dropped */
        /* $this->call(WfTypesTableSeeder::class); */
        $this->call(PermissionDependenciesTableSeeder::class);
        /*Reports*/
        $this->call(ReportTypesTableSeeder::class);
        $this->call(ReportCategoriesTableSeeder::class);
        $this->call(ReportsTableSeeder::class);
        $this->call(ReportCategoryReportTableSeeder::class);
        /*Reports*/
        $this->call(ConfigurableReportTypesTableSeeder::class);
        $this->call(ConfigurableReportTableSeeder::class);
        $this->call(CompensationPaymentTypesTableSeeder::class);
        $this->call(PayrollProcTypesTableSeeder::class);
        $this->call(CourtCategoriesTableSeeder::class);
        $this->call(CasePersonnelsTableSeeder::class);
        $this->call(CodesTableSeeder::class);
        $this->call(CodeValuesTableSeeder::class);
        $this->call(CodeValueCodeTableSeeder::class);
        $this->call(WfModuleGroupsTableSeeder::class);
        $this->call(WfModulesTableSeeder::class);
        $this->call(WfDefinitionsTableSeeder::class);
        $this->call(WfArchiveReasonCodesTableSeeder::class);
        //$this->call(InsurancesTableSeeder::class);
        $this->call(InvestigationQuestionsTableSeeder::class);
        $this->call(InvestigationQuestionIncidentTypeTableSeeder::class);
        $this->call(DocumentBenefitTableSeeder::class);
        $this->call(BenefitTypeClaimsTableSeeder::class);
        $this->call(BodyPartInjuryGroupTableSeeder::class);
        $this->call(BodyPartInjuryTableSeeder::class);
        $this->call(OccupationTableSeeder::class);

        /* start: PD Computation Tables */
        $this->call(PdAmputationsTableSeeder::class);
        $this->call(PdAgeRangesTableSeeder::class);
        $this->call(PdInjuriesTableSeeder::class);
        $this->call(PdImpairmentsTableSeeder::class);
        $this->call(PdFecsTableSeeder::class);
        $this->call(PdOccupationCharacteristicsTableSeeder::class);
        $this->call(PdOccupationAdjustmentsTableSeeder::class);
        $this->call(PdAgeAdjustmentsTableSeeder::class);
        $this->call(EmployerContributingCategoriesTableSeeder::class);
        $this->call(OfficeZoneTableSeeder::class);
        /* end: PD Computation Tables */

        /*postcodes*/
        $this->call(PostCodesSeeder::class);

        /*end*/

        /*Occupational diseases caused by exposure to agents arising   from work activities*/
        $this->call(DiseaseExposureAgentsTableSeeder::class);
        /*end*/


        // OSH Audit Questions
        $this->call(OshAuditQuestionsTableSeeder::class);
        $this->call(OshAuditPeriodSeeder::class);
        
        // end Audit Questions

        if (DB::getDriverName() == "pgsql") {
            $this->call(EmployerSizeTypeTableSeeder::class);
            $this->call(DocumentSectorTableSeeder::class);
        }
         //Real Estate Categories seeder
        $this->call(RealEstateCategoriesTableSeeder::class);
        $this->call(InvestmentBudgetCategorySeeder::class);
        $this->call(SlaAlertsTableSeeder::class);

        /*coupon rate and tenure seeder*/
        $this->call(CouponTenureTableSeeder::class);
        /*end*/

         /*Facility codes*/
        $this->call(FacilityCodes::class);
        /*end*/

        DB::commit();

    }
}
