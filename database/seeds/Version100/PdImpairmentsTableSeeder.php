<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PdImpairmentsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $count = \DB::table('pd_impairments')->limit(1)->count();

        if (!$count) {
            $this->disableForeignKeys('pd_impairments');
            $this->delete('pd_impairments');

            \DB::table('pd_impairments')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'name' => 'CARDIO-HEART',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'name' => 'HYPERTENSION',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'name' => 'AORTIC DISEASE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'name' => 'PERIPH - UE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'name' => 'PERIPH - LE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'name' => 'PULM CIRC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'name' => 'RESPIRATORY',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'name' => 'UPPER DIGEST',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'name' => 'COLON, RECTUM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'name' => 'FISTULAS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'name' => 'LIVER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'name' => 'HERNIA',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'name' => 'URINARY',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'name' => 'REPRODUCTIVE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'name' => 'SKIN-SCARS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'name' => 'DERMATITIS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'name' => 'SKIN CANCER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'name' => 'HEMATOPOIETIC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'name' => 'DIABETES',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'name' => 'EAR-HEARING',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'name' => 'VESTIBULAR',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'name' => 'FACE-COSMETIC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'name' => 'FACE-EYE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'name' => 'NOSE - SMELL',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'name' => 'MASTICATION',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'name' => 'TASTE-SMELL',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'name' => 'VOICE-SPEECH',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => 43,
                        'has_child' => 0,
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'name' => 'VISION',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'name' => 'CONSCIOUSNESS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'name' => 'EPISODIC NEURO',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'name' => 'AROUSAL',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'name' => 'COGNITIVE IMP',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'name' => 'LANGUAGE DISOR',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'name' => 'BEHAV-EMOT',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                34 =>
                    array (
                        'id' => 35,
                        'name' => 'CRANIAL-OLFACTORY',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                35 =>
                    array (
                        'id' => 36,
                        'name' => 'CRANIAL-OPTIC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                36 =>
                    array (
                        'id' => 37,
                        'name' => 'CRANIAL-OCULO',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                37 =>
                    array (
                        'id' => 38,
                        'name' => 'CRANIAL-TRIGEM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                38 =>
                    array (
                        'id' => 39,
                        'name' => 'CRANIAL-FACIAL',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                39 =>
                    array (
                        'id' => 40,
                        'name' => 'CRANIAL-VERTIGO',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                40 =>
                    array (
                        'id' => 41,
                        'name' => 'CRANIAL-TINNITUS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                41 =>
                    array (
                        'id' => 42,
                        'name' => 'CRANIAL-GLOSSO',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                42 =>
                    array (
                        'id' => 43,
                        'name' => 'CRANIAL-SPINAL ACC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 1,
                    ),
                43 =>
                    array (
                        'id' => 44,
                        'name' => 'CRANIAL-HYPOGLOS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                44 =>
                    array (
                        'id' => 45,
                        'name' => 'STATION GAIT',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                45 =>
                    array (
                        'id' => 46,
                        'name' => 'UPPER EXTREM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                46 =>
                    array (
                        'id' => 47,
                        'name' => 'SPINAL-RESPIR',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                47 =>
                    array (
                        'id' => 48,
                        'name' => 'SPINAL-URINARY',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                48 =>
                    array (
                        'id' => 49,
                        'name' => 'SPINAL-ANORECT',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                49 =>
                    array (
                        'id' => 50,
                        'name' => 'SPINAL-SEXUAL',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                50 =>
                    array (
                        'id' => 51,
                        'name' => 'PAIN-UE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                51 =>
                    array (
                        'id' => 52,
                        'name' => 'PAIN-LE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                52 =>
                    array (
                        'id' => 53,
                        'name' => 'PERIPH-SPINE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                53 =>
                    array (
                        'id' => 54,
                        'name' => 'PERIPH-UE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                54 =>
                    array (
                        'id' => 55,
                        'name' => 'PERIPH-LE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                55 =>
                    array (
                        'id' => 56,
                        'name' => 'PSYCHIACTRIC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                56 =>
                    array (
                        'id' => 57,
                        'name' => 'SPINE-DRE-ROM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => 43,
                        'has_child' => 0,
                    ),
                57 =>
                    array (
                        'id' => 58,
                        'name' => 'CORTIC-ONE UE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                58 =>
                    array (
                        'id' => 59,
                        'name' => 'CORTIC-TWO UE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                59 =>
                    array (
                        'id' => 60,
                        'name' => 'CORTIC-GAIT',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                60 =>
                    array (
                        'id' => 61,
                        'name' => 'CORTIC-BLADDER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                61 =>
                    array (
                        'id' => 62,
                        'name' => 'CORTIC-ANOREC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                62 =>
                    array (
                        'id' => 63,
                        'name' => 'CORTIC-SEXUAL',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                63 =>
                    array (
                        'id' => 64,
                        'name' => 'CORTIC-RESPIR',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                64 =>
                    array (
                        'id' => 65,
                        'name' => 'PELVIC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                65 =>
                    array (
                        'id' => 66,
                        'name' => 'ARM-AMPUT',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                66 =>
                    array (
                        'id' => 67,
                        'name' => 'BRACHIAL PLEX',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                67 =>
                    array (
                        'id' => 68,
                        'name' => 'CARPAL TUNNEL',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                68 =>
                    array (
                        'id' => 69,
                        'name' => 'ENTRAP-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                69 =>
                    array (
                        'id' => 70,
                        'name' => 'CRPS I',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                70 =>
                    array (
                        'id' => 71,
                        'name' => 'CRPS II',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                71 =>
                    array (
                        'id' => 72,
                        'name' => 'PERIPH VASC',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                72 =>
                    array (
                        'id' => 73,
                        'name' => 'ARM-GRIP/PINCH',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                73 =>
                    array (
                        'id' => 74,
                        'name' => 'ARM-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                74 =>
                    array (
                        'id' => 75,
                        'name' => 'SHOULER-ROM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => 43,
                        'has_child' => 0,
                    ),
                75 =>
                    array (
                        'id' => 76,
                        'name' => 'SHOULDER-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                76 =>
                    array (
                        'id' => 77,
                        'name' => 'ELBOW-ROM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                77 =>
                    array (
                        'id' => 78,
                        'name' => 'ELBOW-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                78 =>
                    array (
                        'id' => 79,
                        'name' => 'WRIST-ROM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                79 =>
                    array (
                        'id' => 80,
                        'name' => 'WRIST-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                80 =>
                    array (
                        'id' => 81,
                        'name' => 'HAND',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                81 =>
                    array (
                        'id' => 82,
                        'name' => 'THUMB',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                82 =>
                    array (
                        'id' => 83,
                        'name' => 'INDEX',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                83 =>
                    array (
                        'id' => 84,
                        'name' => 'MIDDLE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                84 =>
                    array (
                        'id' => 85,
                        'name' => 'RING',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                85 =>
                    array (
                        'id' => 86,
                        'name' => 'LITTLE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                86 =>
                    array (
                        'id' => 87,
                        'name' => 'LEG-LENGTH',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                87 =>
                    array (
                        'id' => 88,
                        'name' => 'LEG-AMPUT',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                88 =>
                    array (
                        'id' => 89,
                        'name' => 'LEG-SKIN LOSS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                89 =>
                    array (
                        'id' => 90,
                        'name' => 'LEG-PERIPH NRV',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                90 =>
                    array (
                        'id' => 91,
                        'name' => 'LEG-VASCULAR',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                91 =>
                    array (
                        'id' => 92,
                        'name' => 'LEG-CAUSALGIA',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                92 =>
                    array (
                        'id' => 93,
                        'name' => 'LEG-GAIT',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                93 =>
                    array (
                        'id' => 94,
                        'name' => 'LEG-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                94 =>
                    array (
                        'id' => 95,
                        'name' => 'PELVIS-FX.',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                95 =>
                    array (
                        'id' => 96,
                        'name' => 'HIP',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                96 =>
                    array (
                        'id' => 97,
                        'name' => 'FEMUR-FX',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                97 =>
                    array (
                        'id' => 98,
                        'name' => 'KNEE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                98 =>
                    array (
                        'id' => 99,
                        'name' => 'TIBIA-FX',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                99 =>
                    array (
                        'id' => 100,
                        'name' => 'ANKLE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                100 =>
                    array (
                        'id' => 101,
                        'name' => 'FOOT-ATROPHY',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                101 =>
                    array (
                        'id' => 102,
                        'name' => 'FOOT-ANKYLOSIS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                102 =>
                    array (
                        'id' => 103,
                        'name' => 'FOOT-ARTHRITIS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                103 =>
                    array (
                        'id' => 104,
                        'name' => 'FOOT-ROM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                104 =>
                    array (
                        'id' => 105,
                        'name' => 'FOOT-STRENGTH',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                105 =>
                    array (
                        'id' => 106,
                        'name' => 'FOOT-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                106 =>
                    array (
                        'id' => 107,
                        'name' => 'FOOT-DBE',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                107 =>
                    array (
                        'id' => 108,
                        'name' => 'TOE-ATROPHY',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                108 =>
                    array (
                        'id' => 109,
                        'name' => 'TOE-ANKYLOSIS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                109 =>
                    array (
                        'id' => 110,
                        'name' => 'TOE-ARTHRITIS',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                110 =>
                    array (
                        'id' => 111,
                        'name' => 'TOE-ROM',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                111 =>
                    array (
                        'id' => 112,
                        'name' => 'TOE-STRENGTH',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                112 =>
                    array (
                        'id' => 113,
                        'name' => 'TOE-AMPUTATION',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
                113 =>
                    array (
                        'id' => 114,
                        'name' => 'TOE-OTHER',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'parent_id' => NULL,
                        'has_child' => 0,
                    ),
            ));

            $this->enableForeignKeys('pd_impairments');

$sql = <<<SQL
DROP TRIGGER IF EXISTS pd_impairments_ro ON pd_impairments;
create trigger pd_impairments_ro before insert or update or delete or truncate on pd_impairments for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);

        }

        
    }
}