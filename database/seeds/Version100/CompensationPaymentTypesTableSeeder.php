<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;;

class CompensationPaymentTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('compensation_payment_types');
        $this->delete('compensation_payment_types');

        \DB::table('compensation_payment_types')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Lump Sum',
                    'created_at' => '2017-06-26 11:39:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Monthly Pension over period of time',
                    'created_at' => '2017-06-26 11:39:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Monthly Pension For Life',
                    'created_at' => '2017-06-26 11:39:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

        ));

        $this->enableForeignKeys('compensation_payment_types');
    }

}
