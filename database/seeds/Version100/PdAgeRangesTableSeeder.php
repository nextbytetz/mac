<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PdAgeRangesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $count = \DB::table('pd_age_ranges')->limit(1)->count();

        if (!$count) {
            $this->disableForeignKeys('pd_age_ranges');
            $this->delete('pd_age_ranges');

            \DB::table('pd_age_ranges')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'from' => 0,
                        'to' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'from' => 22,
                        'to' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'from' => 27,
                        'to' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'from' => 32,
                        'to' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'from' => 37,
                        'to' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'from' => 42,
                        'to' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'from' => 47,
                        'to' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'from' => 52,
                        'to' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'from' => 57,
                        'to' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'from' => 62,
                        'to' => 500,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));

            $this->enableForeignKeys('pd_age_ranges');

$sql = <<<SQL
DROP TRIGGER IF EXISTS pd_age_ranges_ro ON pd_age_ranges;
create trigger pd_age_ranges_ro before insert or update or delete or truncate on pd_age_ranges for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);
        }
        
    }
}