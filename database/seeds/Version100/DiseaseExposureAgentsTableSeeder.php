<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DiseaseExposureAgentsTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->disableForeignKeys('disease_exposure_agents');
        $this->delete('disease_exposure_agents');
        
        \DB::table('disease_exposure_agents')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code_value_id' => 311,
                'name' => 'Diseases caused by beryllium or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code_value_id' => 311,
                'name' => 'Diseases caused by cadmium or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
           2 => 
            array (
                'id' => 3,
                'code_value_id' => 311,
                'name' => 'Diseases caused by phosphorus or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

            3 => 
            array (
                'id' => 4,
                'code_value_id' => 311,
                'name' => 'Diseases caused by chromium or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code_value_id' => 311,
                'name' => 'Diseases caused by manganese or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code_value_id' => 311,
                'name' => 'Diseases caused by arsenic or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code_value_id' => 311,
                'name' => 'Diseases caused by mercury or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code_value_id' => 311,
                'name' => 'Diseases caused by lead or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              8 => 
            array (
                'id' => 9,
                'code_value_id' => 311,
                'name' => 'Diseases caused by fluorine or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code_value_id' => 311,
                'name' => 'Diseases caused by carbon disulfi de',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'code_value_id' => 311,
                'name' => 'Diseases caused by halogen derivatives of aliphatic or aromatic hydrocarbons',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'code_value_id' => 311,
                'name' => 'Diseases caused by benzene or its homologues',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               12 => 
            array (
                'id' => 13,
                'code_value_id' => 311,
                'name' => 'Diseases caused by nitro- and amino-derivatives of benzene or its homologues',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
                 13 => 
            array (
                'id' => 14,
                'code_value_id' => 311,
                'name' => 'Diseases caused by nitroglycerine or other nitric acid esters',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               14 => 
            array (
                'id' => 15,
                'code_value_id' => 311,
                'name' => 'Diseases caused by alcohols, glycols or ketones',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
                15 => 
            array (
                'id' => 16,
                'code_value_id' => 311,
                'name' => 'Diseases caused by asphyxiants like carbon monoxide, hydrogen sulfi de, hydrogen cyanide  or its derivatives',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
                16 => 
            array (
                'id' => 17,
                'code_value_id' => 311,
                'name' => 'Diseases caused by acrylonitrile',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
                 17 => 
            array (
                'id' => 18,
                'code_value_id' => 311,
                'name' => 'Diseases caused by oxides of nitrogen',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
           18 => 
            array (
                'id' => 19,
                'code_value_id' => 311,
                'name' => 'Diseases caused by vanadium or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'code_value_id' => 311,
                'name' => 'Diseases caused by antimony or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'code_value_id' => 311,
                'name' => 'Diseases caused by hexane',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             21 => 
            array (
                'id' => 22,
                'code_value_id' => 311,
                'name' => 'Diseases caused by mineral acids',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             22 => 
            array (
                'id' => 23,
                'code_value_id' => 311,
                'name' => 'Diseases caused by pharmaceutical agents',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              23 => 
            array (
                'id' => 24,
                'code_value_id' => 311,
                'name' => 'Diseases caused by nickel or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              24 => 
            array (
                'id' => 25,
                'code_value_id' => 311,
                'name' => 'Diseases caused by thallium or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
                25 => 
            array (
                'id' => 26,
                'code_value_id' => 311,
                'name' => 'Diseases caused by osmium or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               26 => 
            array (
                'id' => 27,
                'code_value_id' => 311,
                'name' => 'Diseases caused by selenium or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'code_value_id' => 311,
                'name' => 'Diseases caused by copper or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              28 => 
            array (
                'id' => 29,
                'code_value_id' => 311,
                'name' => 'Diseases caused by platinum or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              29 => 
            array (
                'id' => 30,
                'code_value_id' => 311,
                'name' => 'Diseases caused by tin or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
                 30 => 
            array (
                'id' => 31,
                'code_value_id' => 311,
                'name' => 'Diseases caused by zinc or its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
                31 => 
            array (
                'id' => 32,
                'code_value_id' => 311,
                'name' => 'Diseases caused by phosgene',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             32 => 
            array (
                'id' => 33,
                'code_value_id' => 311,
                'name' => 'Diseases caused by corneal irritants like benzoquinone',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'code_value_id' => 311,
                'name' => 'Diseases caused by ammonia',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               34 => 
            array (
                'id' => 35,
                'code_value_id' => 311,
                'name' => 'Diseases caused by isocyanates',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             35 => 
            array (
                'id' => 36,
                'code_value_id' => 311,
                'name' => 'Diseases caused by pesticides',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'code_value_id' => 311,
                'name' => 'Diseases caused by sulphur oxides',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'code_value_id' => 311,
                'name' => 'Diseases caused by organic solvents',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'code_value_id' => 311,
                'name' => 'Diseases caused by latex or latex-containing products',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             39 => 
            array (
                'id' => 40,
                'code_value_id' => 311,
                'name' => 'Diseases caused by chlorine',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'code_value_id' => 311,
                'name' => 'Diseases caused by other chemical agents at work not mentioned in the preceding items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              41 => 
            array (
                'id' => 42,
                'code_value_id' => 312,
                'name' => 'Hearing impairment caused by noise',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               42 => 
            array (
                'id' => 43,
                'code_value_id' => 312,
                'name' => 'Diseases caused by vibration (disorders of muscles, tendons, bones, joints, peripheral blood   vessels or peripheral nerves)',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               43 => 
            array (
                'id' => 44,
                'code_value_id' => 312,
                'name' => 'Diseases caused by compressed or decompressed air',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               44 => 
            array (
                'id' => 45,
                'code_value_id' => 312,
                'name' => 'Diseases caused by ionizing radiations',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             45 => 
            array (
                'id' => 46,
                'code_value_id' => 312,
                'name' => 'Diseases caused by optical (ultraviolet, visible light, infrared) radiations including laser',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             46 => 
            array (
                'id' => 47,
                'code_value_id' => 312,
                'name' => 'Diseases caused by exposure to extreme temperatures',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
               47 => 
            array (
                'id' => 48,
                'code_value_id' => 312,
                'name' => 'Diseases caused by other physical agents at work not mentioned in the preceding items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              48 => 
            array (
                'id' => 49,
                'code_value_id' => 313,
                'name' => 'Brucellosis',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'code_value_id' => 313,
                'name' => 'Hepatitis viruses',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'code_value_id' => 313,
                'name' => 'Human immunodefi ciency virus (HIV)',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'code_value_id' => 313,
                'name' => 'Tetanus',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'code_value_id' => 313,
                'name' => 'Tuberculosis',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'code_value_id' => 313,
                'name' => 'Toxic or infl ammatory syndromes associated with bacterial or fungal contaminants',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             54 => 
            array (
                'id' => 55,
                'code_value_id' => 313,
                'name' => 'Anthrax',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'code_value_id' => 313,
                'name' => 'Leptospirosis',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'code_value_id' => 313,
                'name' => 'Diseases caused by other biological agents at work not mentioned in the preceding items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'code_value_id' => 313,
                'name' => 'Diseases caused by other biological agents at work not mentioned in the preceding items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             58 => 
            array (
                'id' => 59,
                'code_value_id' => 314,
                'name' => 'Respiratory diseases',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'code_value_id' => 314,
                'name' => 'Pneumoconioses caused by fi brogenic mineral dust (silicosis, anthraco-silicosis, asbestosis)',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'code_value_id' => 314,
                'name' => 'Silicotuberculosis',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'code_value_id' => 314,
                'name' => 'Pneumoconioses caused by non-fi brogenic mineral dust',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'code_value_id' => 314,
                'name' => 'Siderosis',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'code_value_id' => 314,
                'name' => 'Bronchopulmonary diseases caused by hard-metal dust',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'code_value_id' => 314,
                'name' => 'Bronchopulmonary diseases',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'code_value_id' => 314,
                'name' => 'Asthma caused by recognized sensitizing agents or irritants inherent to the work process',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'code_value_id' => 314,
                'name' => 'Extrinsic allergic alveolitis',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'code_value_id' => 314,
                'name' => 'Chronic obstructive pulmonary diseases',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'code_value_id' => 314,
                'name' => 'Diseases of the lung caused by aluminium',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'code_value_id' => 314,
                'name' => 'Upper airways disorders',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'code_value_id' => 314,
                'name' => 'Other respiratory diseases not mentioned in the preceding items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'code_value_id' => 315,
                'name' => 'Allergic contact dermatoses and contact urticaria',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'code_value_id' => 315,
                'name' => 'Irritant contact dermatoses',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'code_value_id' => 315,
                'name' => 'Vitiligo caused by other recognized agents arising from work activities not included in other items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             74 => 
            array (
                'id' => 75,
                'code_value_id' => 315,
                'name' => 'Other skin diseases caused by physical, chemical or biological agents at work not included under other items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'code_value_id' => 316,
                'name' => 'Radial styloid tenosynovitis due to repetitive movements, forceful exertions and extreme  postures of the wrist',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'code_value_id' => 316,
                'name' => 'Chronic tenosynovitis of hand and wrist due to repetitive movements, forceful exertions and extreme postures of the wrist',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'code_value_id' => 316,
                'name' => 'Olecranon bursitis due to prolonged pressure of the elbow region',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'code_value_id' => 316,
                'name' => 'Prepatellar bursitis due to prolonged stay in kneeling position',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'code_value_id' => 316,
                'name' => 'Epicondylitis due to repetitive forceful work',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'code_value_id' => 316,
                'name' => 'Meniscus lesions following extended periods of work in a kneeling or squatting position',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'code_value_id' => 316,
                'name' => 'Carpal tunnel syndrome',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'code_value_id' => 316,
                'name' => 'Other musculoskeletal disorders not mentioned in the preceding items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'code_value_id' => 317,
                'name' => 'Post-traumatic stress disorder',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'code_value_id' => 317,
                'name' => 'Other mental or behavioural disorders not mentioned in the preceding item',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'code_value_id' => 318,
                'name' => 'Asbestos',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'code_value_id' => 318,
                'name' => 'Benzidine and its salts',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'code_value_id' => 318,
                'name' => 'Bis-chloromethyl ether (BCME)',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'code_value_id' => 318,
                'name' => 'Chromium VI compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'code_value_id' => 318,
                'name' => 'Coal tars, coal tar pitches or soots',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'code_value_id' => 318,
                'name' => 'Beta-naphthylamine',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'code_value_id' => 318,
                'name' => 'Vinyl chloride',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'code_value_id' => 318,
                'name' => 'Benzene',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'code_value_id' => 318,
                'name' => 'Toxic nitro- and amino-derivatives of benzene or its homologues',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'code_value_id' => 318,
                'name' => 'Ionizing radiations',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

            95 => 
            array (
                'id' => 96,
                'code_value_id' => 318,
                'name' => 'Tar, pitch, bitumen, mineral oil, anthracene, or the compounds, products or residues of  these substances',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'code_value_id' => 318,
                'name' => 'Coke oven emissions',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'code_value_id' => 318,
                'name' => 'Nickel compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'code_value_id' => 318,
                'name' => 'Wood dust',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'code_value_id' => 318,
                'name' => 'Arsenic and its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'code_value_id' => 318,
                'name' => 'Beryllium and its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'code_value_id' => 318,
                'name' => 'Cadmium and its compounds',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'code_value_id' => 318,
                'name' => 'Erionite',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'code_value_id' => 318,
                'name' => 'Ethylene oxide',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'code_value_id' => 318,
                'name' => 'Hepatitis B virus (HBV) and hepatitis C virus (HCV)',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

            105 => 
            array (
                'id' => 106,
                'code_value_id' => 318,
                'name' => 'Cancers caused by other agents at work not mentioned in the preceding items',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

            106 => 
            array (
                'id' => 107,
                'code_value_id' => 485,
                'name' => 'Miners’ nystagmus',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

            107 => 
            array (
                'id' => 108,
                'code_value_id' => 485,
                'name' => 'Other specific diseases caused by occupations or processes not mentioned in this list ',
                'created_at' => '2020-03-29 16:39:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

        ));

        $this->enableForeignKeys('disease_exposure_agents');
    }
}
