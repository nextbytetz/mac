<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DutyTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('duty_types');
        
        \DB::table('duty_types')->insert(array (
            0 => 
            array (
                'name' => 'Hospitalization Days',
                'created_at' => '2017-04-19 11:09:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'name' => 'Excuse from Duty',
                'created_at' => '2017-04-19 11:09:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'name' => 'Light Duty',
                'created_at' => '2017-04-19 11:09:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
    }
}