<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class IncidentTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('incident_types');
        $this->delete('incident_types');
        
        \DB::table('incident_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Accident',
                'isregister' => 1,
                'created_at' => '2017-04-18 17:46:12',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Disease',
                'isregister' => 1,
                'created_at' => '2017-04-18 17:46:12',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Death',
                'isregister' => 1,
                'created_at' => '2017-04-18 17:46:20',
                'updated_at' => NULL,
            ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Accident - Death',
                    'isregister' => 0,
                    'created_at' => '2017-04-18 17:46:20',
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Disease - Death',
                    'isregister' => 0,
                    'created_at' => '2017-04-18 17:46:20',
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('incident_types');
    }
}