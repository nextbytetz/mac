<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class EmployeeCategoriesTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('employee_categories');
        
        \DB::table('employee_categories')->insert(array (
            0 => 
            array (
                'name' => 'Permanent',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'name' => 'Temporary',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
        
    }
}