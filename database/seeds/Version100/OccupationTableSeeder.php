<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class OccupationTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $count = \DB::table('occupations')->limit(1)->count();

        if ($count) {
            $this->disableForeignKeys('occupations');
            $this->delete('occupations');

            \DB::table('occupations')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'groupno' => 111,
                        'name' => 'Abstractor',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'groupno' => 110,
                        'name' => 'Academic Dean',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'groupno' => 110,
                        'name' => 'Account Executive',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'groupno' => 111,
                        'name' => 'Account Information Clerk',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'groupno' => 111,
                        'name' => 'Accountant',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'groupno' => 111,
                        'name' => 'Accountant, Property',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'groupno' => 111,
                        'name' => 'Accounting Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'groupno' => 590,
                        'name' => 'Acrobat',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'groupno' => 210,
                        'name' => 'Actor',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'groupno' => 310,
                        'name' => 'Acupressurist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'groupno' => 211,
                        'name' => 'Addressing Machine Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'groupno' => 111,
                        'name' => 'Administrative Analyst',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'groupno' => 211,
                        'name' => 'Administrative Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'groupno' => 212,
                        'name' => 'Administrator, Health Care Facility',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'groupno' => 111,
                        'name' => 'Admissions Evaluator',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'groupno' => 212,
                        'name' => 'Air Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'groupno' => 481,
                        'name' => 'Air Conditioning Installer Serv., Window Unit',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'groupno' => 480,
                        'name' => 'Air Hammer Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'groupno' => 212,
                        'name' => 'Air Traffic Control Specialist, Tower',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'groupno' => 380,
                        'name' => 'Aircraft Body Repairer',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'groupno' => 380,
                        'name' => 'Aircraft Bonded Structures Repairer',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'groupno' => 341,
                        'name' => 'Aircraft Service Attendant',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'groupno' => 460,
                        'name' => 'Aircraft Service Worker',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'groupno' => 380,
                        'name' => 'Airframe And Power Plant Mechanic',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'groupno' => 213,
                        'name' => 'Airline Transportation Agent',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'groupno' => 322,
                        'name' => 'Airplane Flight Attendant',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'groupno' => 213,
                        'name' => 'Airplane Inspector',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'groupno' => 250,
                        'name' => 'Airplane Pilot, Commercial',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'groupno' => 380,
                        'name' => 'Alarm Service Technician',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'groupno' => 111,
                        'name' => 'Alarm Signal Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'groupno' => 560,
                        'name' => 'Ambulance Attendant',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'groupno' => 560,
                        'name' => 'Ambulance Driver',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'groupno' => 340,
                        'name' => 'Amusement Park Attendant',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'groupno' => 210,
                        'name' => 'Amusement Park Entertainer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                34 =>
                    array (
                        'id' => 35,
                        'groupno' => 220,
                        'name' => 'Anesthesiologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                35 =>
                    array (
                        'id' => 36,
                        'groupno' => 310,
                        'name' => 'Angiogram Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                36 =>
                    array (
                        'id' => 37,
                        'groupno' => 491,
                        'name' => 'Animal Keeper',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                37 =>
                    array (
                        'id' => 38,
                        'groupno' => 491,
                        'name' => 'Animal Ride Attendant',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                38 =>
                    array (
                        'id' => 39,
                        'groupno' => 390,
                        'name' => 'Animal Trainer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                39 =>
                    array (
                        'id' => 40,
                        'groupno' => 210,
                        'name' => 'Announcer',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                40 =>
                    array (
                        'id' => 41,
                        'groupno' => 460,
                        'name' => 'Anodizer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                41 =>
                    array (
                        'id' => 42,
                        'groupno' => 380,
                        'name' => 'Antenna Installer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                42 =>
                    array (
                        'id' => 43,
                        'groupno' => 380,
                        'name' => 'Antenna Installer, Satellite Communications',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                43 =>
                    array (
                        'id' => 44,
                        'groupno' => 110,
                        'name' => 'Appeals Referee',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                44 =>
                    array (
                        'id' => 45,
                        'groupno' => 111,
                        'name' => 'Appointment Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                45 =>
                    array (
                        'id' => 46,
                        'groupno' => 212,
                        'name' => 'Appraiser, Art',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                46 =>
                    array (
                        'id' => 47,
                        'groupno' => 212,
                        'name' => 'Appraiser, Business Eqpt.',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                47 =>
                    array (
                        'id' => 48,
                        'groupno' => 213,
                        'name' => 'Appraiser, Real Estate',
                        'industry' => 'Real Estate',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                48 =>
                    array (
                        'id' => 49,
                        'groupno' => 330,
                        'name' => 'Arbor Press Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                49 =>
                    array (
                        'id' => 50,
                        'groupno' => 370,
                        'name' => 'Arc Cutter',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                50 =>
                    array (
                        'id' => 51,
                        'groupno' => 212,
                        'name' => 'Architect',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                51 =>
                    array (
                        'id' => 52,
                        'groupno' => 111,
                        'name' => 'Archivist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                52 =>
                    array (
                        'id' => 53,
                        'groupno' => 320,
                        'name' => 'Armature Bander',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                53 =>
                    array (
                        'id' => 54,
                        'groupno' => 350,
                        'name' => 'Armored Car Driver',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                54 =>
                    array (
                        'id' => 55,
                        'groupno' => 390,
                        'name' => 'Armored Car Guard',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                55 =>
                    array (
                        'id' => 56,
                        'groupno' => 111,
                        'name' => 'Art Director',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                56 =>
                    array (
                        'id' => 57,
                        'groupno' => 221,
                        'name' => 'Artificial Flower Maker',
                        'industry' => 'Button And Notion',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                57 =>
                    array (
                        'id' => 58,
                        'groupno' => 220,
                        'name' => 'Artificial Plastic Eye Maker',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                58 =>
                    array (
                        'id' => 59,
                        'groupno' => 351,
                        'name' => 'Asphalt Distributor Tender',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                59 =>
                    array (
                        'id' => 60,
                        'groupno' => 351,
                        'name' => 'Asphalt Paving Machine Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                60 =>
                    array (
                        'id' => 61,
                        'groupno' => 480,
                        'name' => 'Asphalt Raker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                61 =>
                    array (
                        'id' => 62,
                        'groupno' => 351,
                        'name' => 'Asphalt Surface Heater Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                62 =>
                    array (
                        'id' => 63,
                        'groupno' => 221,
                        'name' => 'Assembler',
                        'industry' => 'Household Appliances',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                63 =>
                    array (
                        'id' => 64,
                        'groupno' => 120,
                        'name' => 'Assembler J',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                64 =>
                    array (
                        'id' => 65,
                        'groupno' => 380,
                        'name' => 'Assembler-Installer, General',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                65 =>
                    array (
                        'id' => 66,
                        'groupno' => 221,
                        'name' => 'Assembler, Electric Motor',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                66 =>
                    array (
                        'id' => 67,
                        'groupno' => 370,
                        'name' => 'Assembler, Internal Combustion Engine',
                        'industry' => 'Engine-Turbine',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                67 =>
                    array (
                        'id' => 68,
                        'groupno' => 370,
                        'name' => 'Assembler, Motor Vehicle',
                        'industry' => 'Automotive Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                68 =>
                    array (
                        'id' => 69,
                        'groupno' => 221,
                        'name' => 'Assembler, Musical Instruments',
                        'industry' => 'Musical Instruments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                69 =>
                    array (
                        'id' => 70,
                        'groupno' => 320,
                        'name' => 'Assembler, Office Machines',
                        'industry' => 'Office Machines',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                70 =>
                    array (
                        'id' => 71,
                        'groupno' => 221,
                        'name' => 'Assembler, Production',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                71 =>
                    array (
                        'id' => 72,
                        'groupno' => 120,
                        'name' => 'Assembler, Semiconductor',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                72 =>
                    array (
                        'id' => 73,
                        'groupno' => 221,
                        'name' => 'Assembler, Small Products',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                73 =>
                    array (
                        'id' => 74,
                        'groupno' => 380,
                        'name' => 'Assembler, Subassembly',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                74 =>
                    array (
                        'id' => 75,
                        'groupno' => 590,
                        'name' => 'Athlete, Professional',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                75 =>
                    array (
                        'id' => 76,
                        'groupno' => 390,
                        'name' => 'Athletic Trainer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                76 =>
                    array (
                        'id' => 77,
                        'groupno' => 111,
                        'name' => 'Attendance Clerk',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                77 =>
                    array (
                        'id' => 78,
                        'groupno' => 111,
                        'name' => 'Auction Clerk',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                78 =>
                    array (
                        'id' => 79,
                        'groupno' => 210,
                        'name' => 'Auctioneer',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                79 =>
                    array (
                        'id' => 80,
                        'groupno' => 212,
                        'name' => 'Audio Operator',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                80 =>
                    array (
                        'id' => 81,
                        'groupno' => 221,
                        'name' => 'Audio Video Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                81 =>
                    array (
                        'id' => 82,
                        'groupno' => 251,
                        'name' => 'Audiovisual Production Specialist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                82 =>
                    array (
                        'id' => 83,
                        'groupno' => 111,
                        'name' => 'Audit Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                83 =>
                    array (
                        'id' => 84,
                        'groupno' => 111,
                        'name' => 'Auditor',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                84 =>
                    array (
                        'id' => 85,
                        'groupno' => 251,
                        'name' => 'Auditor, Field',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                85 =>
                    array (
                        'id' => 86,
                        'groupno' => 321,
                        'name' => 'Auto Painter',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                86 =>
                    array (
                        'id' => 87,
                        'groupno' => 330,
                        'name' => 'Autoclave Operator',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                87 =>
                    array (
                        'id' => 88,
                        'groupno' => 370,
                        'name' => 'Automated Equipment Installer',
                        'industry' => 'Machinery Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                88 =>
                    array (
                        'id' => 89,
                        'groupno' => 370,
                        'name' => 'Automobile Accessories Installer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                89 =>
                    array (
                        'id' => 90,
                        'groupno' => 370,
                        'name' => 'Automobile Assembler',
                        'industry' => 'Automotive Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                90 =>
                    array (
                        'id' => 91,
                        'groupno' => 370,
                        'name' => 'Automobile Body Repairer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                91 =>
                    array (
                        'id' => 92,
                        'groupno' => 340,
                        'name' => 'Automobile Detailer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                92 =>
                    array (
                        'id' => 93,
                        'groupno' => 111,
                        'name' => 'Automobile Locator',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                93 =>
                    array (
                        'id' => 94,
                        'groupno' => 214,
                        'name' => 'Automobile Repair Service Estimator',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                94 =>
                    array (
                        'id' => 95,
                        'groupno' => 370,
                        'name' => 'Automobile Service Station Mechanic',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                95 =>
                    array (
                        'id' => 96,
                        'groupno' => 321,
                        'name' => 'Automobile Upholsterer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                96 =>
                    array (
                        'id' => 97,
                        'groupno' => 340,
                        'name' => 'Automobile Washer & Polisher',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                97 =>
                    array (
                        'id' => 98,
                        'groupno' => 460,
                        'name' => 'Automobile Wrecker',
                        'industry' => 'Wholesale Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                98 =>
                    array (
                        'id' => 99,
                        'groupno' => 380,
                        'name' => 'Awning Maker',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                99 =>
                    array (
                        'id' => 100,
                        'groupno' => 240,
                        'name' => 'Babysitter',
                        'industry' => 'Domestic Services; Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                100 =>
                    array (
                        'id' => 101,
                        'groupno' => 460,
                        'name' => 'Baggage Handler',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                101 =>
                    array (
                        'id' => 102,
                        'groupno' => 212,
                        'name' => 'Baggage Screener, Airpo',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                102 =>
                    array (
                        'id' => 103,
                        'groupno' => 214,
                        'name' => 'Bagger',
                        'industry' => 'Retail Trade; Groceries',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                103 =>
                    array (
                        'id' => 104,
                        'groupno' => 490,
                        'name' => 'Bailiff',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                104 =>
                    array (
                        'id' => 105,
                        'groupno' => 322,
                        'name' => 'Baker',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                105 =>
                    array (
                        'id' => 106,
                        'groupno' => 420,
                        'name' => 'Baker',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                106 =>
                    array (
                        'id' => 107,
                        'groupno' => 460,
                        'name' => 'Baker Helper',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                107 =>
                    array (
                        'id' => 108,
                        'groupno' => 322,
                        'name' => 'Bakery Supervisor',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                108 =>
                    array (
                        'id' => 109,
                        'groupno' => 230,
                        'name' => 'Band Sawmill Operator',
                        'industry' => 'Sawmill And Planer Mill',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                109 =>
                    array (
                        'id' => 110,
                        'groupno' => 330,
                        'name' => 'Band-Sawing Machine Operator',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                110 =>
                    array (
                        'id' => 111,
                        'groupno' => 211,
                        'name' => 'Bank Clerk',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                111 =>
                    array (
                        'id' => 112,
                        'groupno' => 290,
                        'name' => 'Barber',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                112 =>
                    array (
                        'id' => 113,
                        'groupno' => 330,
                        'name' => 'Barrel Assembler',
                        'industry' => 'Wooden Container',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                113 =>
                    array (
                        'id' => 114,
                        'groupno' => 460,
                        'name' => 'Barrel Filler',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                114 =>
                    array (
                        'id' => 115,
                        'groupno' => 322,
                        'name' => 'Bartender',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                115 =>
                    array (
                        'id' => 116,
                        'groupno' => 221,
                        'name' => 'Basket Maker',
                        'industry' => 'Wooden Container',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                116 =>
                    array (
                        'id' => 117,
                        'groupno' => 230,
                        'name' => 'Batch Still Operator',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                117 =>
                    array (
                        'id' => 118,
                        'groupno' => 321,
                        'name' => 'Battery Assembler, Dry Cell',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                118 =>
                    array (
                        'id' => 119,
                        'groupno' => 321,
                        'name' => 'Battery Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                119 =>
                    array (
                        'id' => 120,
                        'groupno' => 290,
                        'name' => 'Beautician',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                120 =>
                    array (
                        'id' => 121,
                        'groupno' => 230,
                        'name' => 'Bed Laster',
                        'industry' => 'Boot And Shoe',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                121 =>
                    array (
                        'id' => 122,
                        'groupno' => 491,
                        'name' => 'Beekeeper',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                122 =>
                    array (
                        'id' => 123,
                        'groupno' => 360,
                        'name' => 'Bellhop',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                123 =>
                    array (
                        'id' => 124,
                        'groupno' => 221,
                        'name' => 'Bench Worker',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                124 =>
                    array (
                        'id' => 125,
                        'groupno' => 330,
                        'name' => 'Bending Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                125 =>
                    array (
                        'id' => 126,
                        'groupno' => 493,
                        'name' => 'Bicycle Messenger',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                126 =>
                    array (
                        'id' => 127,
                        'groupno' => 320,
                        'name' => 'Bicycle Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                127 =>
                    array (
                        'id' => 128,
                        'groupno' => 480,
                        'name' => 'Billboard & Sign Erector',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                128 =>
                    array (
                        'id' => 129,
                        'groupno' => 480,
                        'name' => 'Billboard Erector Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                129 =>
                    array (
                        'id' => 130,
                        'groupno' => 112,
                        'name' => 'Billing Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                130 =>
                    array (
                        'id' => 131,
                        'groupno' => 213,
                        'name' => 'Billposter',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                131 =>
                    array (
                        'id' => 132,
                        'groupno' => 230,
                        'name' => 'Bindery Worker',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                132 =>
                    array (
                        'id' => 133,
                        'groupno' => 212,
                        'name' => 'Biochemist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                133 =>
                    array (
                        'id' => 134,
                        'groupno' => 110,
                        'name' => 'Biology Specimen Technician',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                134 =>
                    array (
                        'id' => 135,
                        'groupno' => 320,
                        'name' => 'Biomedical Equipment Technician',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                135 =>
                    array (
                        'id' => 136,
                        'groupno' => 430,
                        'name' => 'Blacksmith',
                        'industry' => 'Forging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                136 =>
                    array (
                        'id' => 137,
                        'groupno' => 460,
                        'name' => 'Blacksmith Helper',
                        'industry' => 'Forging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                137 =>
                    array (
                        'id' => 138,
                        'groupno' => 480,
                        'name' => 'Blaster',
                        'industry' => 'Mining; Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                138 =>
                    array (
                        'id' => 139,
                        'groupno' => 332,
                        'name' => 'Blender',
                        'industry' => 'Petrol Refinery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                139 =>
                    array (
                        'id' => 140,
                        'groupno' => 240,
                        'name' => 'Blind Aide',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                140 =>
                    array (
                        'id' => 141,
                        'groupno' => 330,
                        'name' => 'Blister Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                141 =>
                    array (
                        'id' => 142,
                        'groupno' => 220,
                        'name' => 'Blocker And Cutter, Contact Lens',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                142 =>
                    array (
                        'id' => 143,
                        'groupno' => 221,
                        'name' => 'Blocker, Hand',
                        'industry' => 'Hat And Cap',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                143 =>
                    array (
                        'id' => 144,
                        'groupno' => 230,
                        'name' => 'Blueprinting Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                144 =>
                    array (
                        'id' => 145,
                        'groupno' => 380,
                        'name' => 'Boat Repairer',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                145 =>
                    array (
                        'id' => 146,
                        'groupno' => 380,
                        'name' => 'Boat Rigger',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                146 =>
                    array (
                        'id' => 147,
                        'groupno' => 380,
                        'name' => 'Boatbuilder, Wood',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                147 =>
                    array (
                        'id' => 148,
                        'groupno' => 390,
                        'name' => 'Bodyguard',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                148 =>
                    array (
                        'id' => 149,
                        'groupno' => 332,
                        'name' => 'Boiler Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                149 =>
                    array (
                        'id' => 150,
                        'groupno' => 332,
                        'name' => 'Boiler Tender',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                150 =>
                    array (
                        'id' => 151,
                        'groupno' => 430,
                        'name' => 'Boilermaker',
                        'industry' => 'Structured Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                151 =>
                    array (
                        'id' => 152,
                        'groupno' => 460,
                        'name' => 'Boilermaker Helper',
                        'industry' => 'Structured Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                152 =>
                    array (
                        'id' => 153,
                        'groupno' => 111,
                        'name' => 'Bonding Agent',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                153 =>
                    array (
                        'id' => 154,
                        'groupno' => 322,
                        'name' => 'Boner, Meat',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                154 =>
                    array (
                        'id' => 155,
                        'groupno' => 221,
                        'name' => 'Book Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                155 =>
                    array (
                        'id' => 156,
                        'groupno' => 320,
                        'name' => 'Bookbinder',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                156 =>
                    array (
                        'id' => 157,
                        'groupno' => 112,
                        'name' => 'Bookkeeper',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                157 =>
                    array (
                        'id' => 158,
                        'groupno' => 112,
                        'name' => 'Bookkeeper, General Ledger',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                158 =>
                    array (
                        'id' => 159,
                        'groupno' => 351,
                        'name' => 'Boom Conveyor Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                159 =>
                    array (
                        'id' => 160,
                        'groupno' => 330,
                        'name' => 'Boring Machine Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                160 =>
                    array (
                        'id' => 161,
                        'groupno' => 230,
                        'name' => 'Bottle Packer',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                161 =>
                    array (
                        'id' => 162,
                        'groupno' => 390,
                        'name' => 'Bouncer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                162 =>
                    array (
                        'id' => 163,
                        'groupno' => 390,
                        'name' => 'Bounty Hunter',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                163 =>
                    array (
                        'id' => 164,
                        'groupno' => 221,
                        'name' => 'Bow Maker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                164 =>
                    array (
                        'id' => 165,
                        'groupno' => 493,
                        'name' => 'Bowler, Professional',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                165 =>
                    array (
                        'id' => 166,
                        'groupno' => 331,
                        'name' => 'Bowling Ball Molder',
                        'industry' => 'Toy-Sport Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                166 =>
                    array (
                        'id' => 167,
                        'groupno' => 230,
                        'name' => 'Box Blank Machine Operator',
                        'industry' => 'Wooden Container',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                167 =>
                    array (
                        'id' => 168,
                        'groupno' => 460,
                        'name' => 'Box Folding Machine Operator',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                168 =>
                    array (
                        'id' => 169,
                        'groupno' => 321,
                        'name' => 'Box Maker, Paperboard',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                169 =>
                    array (
                        'id' => 170,
                        'groupno' => 321,
                        'name' => 'Box Maker, Wood',
                        'industry' => 'Wooden Container',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                170 =>
                    array (
                        'id' => 171,
                        'groupno' => 230,
                        'name' => 'Box Printing Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                171 =>
                    array (
                        'id' => 172,
                        'groupno' => 321,
                        'name' => 'Box Spring Maker',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                172 =>
                    array (
                        'id' => 173,
                        'groupno' => 211,
                        'name' => 'Braille Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                173 =>
                    array (
                        'id' => 174,
                        'groupno' => 111,
                        'name' => 'Braille Proofreader',
                        'industry' => 'Nonprofit Organization',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                174 =>
                    array (
                        'id' => 175,
                        'groupno' => 330,
                        'name' => 'Brake Press Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                175 =>
                    array (
                        'id' => 176,
                        'groupno' => 370,
                        'name' => 'Brake Repairer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                176 =>
                    array (
                        'id' => 177,
                        'groupno' => 330,
                        'name' => 'Brazing Machine Operator',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                177 =>
                    array (
                        'id' => 178,
                        'groupno' => 330,
                        'name' => 'Bread Wrapping Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                178 =>
                    array (
                        'id' => 179,
                        'groupno' => 332,
                        'name' => 'Brewery Cellar Worker',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                179 =>
                    array (
                        'id' => 180,
                        'groupno' => 331,
                        'name' => 'Brick And Tile Making Machine Operator',
                        'industry' => 'Brick And Tile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                180 =>
                    array (
                        'id' => 181,
                        'groupno' => 481,
                        'name' => 'Bricklayer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                181 =>
                    array (
                        'id' => 182,
                        'groupno' => 481,
                        'name' => 'Bricklayer Apprentice',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                182 =>
                    array (
                        'id' => 183,
                        'groupno' => 480,
                        'name' => 'Bricklayer Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                183 =>
                    array (
                        'id' => 184,
                        'groupno' => 482,
                        'name' => 'Bridge Maintenance Worker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                184 =>
                    array (
                        'id' => 185,
                        'groupno' => 482,
                        'name' => 'Bridge Worker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                185 =>
                    array (
                        'id' => 186,
                        'groupno' => 331,
                        'name' => 'Briquette Machine Operator',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                186 =>
                    array (
                        'id' => 187,
                        'groupno' => 330,
                        'name' => 'Broaching Machine Operator, Production',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                187 =>
                    array (
                        'id' => 188,
                        'groupno' => 321,
                        'name' => 'Broom Stitcher',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                188 =>
                    array (
                        'id' => 189,
                        'groupno' => 492,
                        'name' => 'Bucker',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                189 =>
                    array (
                        'id' => 190,
                        'groupno' => 111,
                        'name' => 'Budget Analyst',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                190 =>
                    array (
                        'id' => 191,
                        'groupno' => 321,
                        'name' => 'Buffer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                191 =>
                    array (
                        'id' => 192,
                        'groupno' => 230,
                        'name' => 'Buffing Machine Tender, Automatic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                192 =>
                    array (
                        'id' => 193,
                        'groupno' => 480,
                        'name' => 'Building Cleaner, Outside',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                193 =>
                    array (
                        'id' => 194,
                        'groupno' => 213,
                        'name' => 'Building Inspector',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                194 =>
                    array (
                        'id' => 195,
                        'groupno' => 213,
                        'name' => 'Building Inspector',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                195 =>
                    array (
                        'id' => 196,
                        'groupno' => 380,
                        'name' => 'Building Maintenance Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                196 =>
                    array (
                        'id' => 197,
                        'groupno' => 351,
                        'name' => 'Bulldozer Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                197 =>
                    array (
                        'id' => 198,
                        'groupno' => 380,
                        'name' => 'Burglar Alarm Installer/Repairer',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                198 =>
                    array (
                        'id' => 199,
                        'groupno' => 330,
                        'name' => 'Burning Machine Operator',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                199 =>
                    array (
                        'id' => 200,
                        'groupno' => 250,
                        'name' => 'Bus Driver',
                        'industry' => 'Motor Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                200 =>
                    array (
                        'id' => 201,
                        'groupno' => 322,
                        'name' => 'Bus Person',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                201 =>
                    array (
                        'id' => 202,
                        'groupno' => 110,
                        'name' => 'Business Manager',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                202 =>
                    array (
                        'id' => 203,
                        'groupno' => 111,
                        'name' => 'Business Representative, Labor Union',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                203 =>
                    array (
                        'id' => 204,
                        'groupno' => 420,
                        'name' => 'Butcher, All-Round',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                204 =>
                    array (
                        'id' => 205,
                        'groupno' => 322,
                        'name' => 'Butcher, Meat',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                205 =>
                    array (
                        'id' => 206,
                        'groupno' => 240,
                        'name' => 'Butler',
                        'industry' => 'Domestic Services; Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                206 =>
                    array (
                        'id' => 207,
                        'groupno' => 460,
                        'name' => 'Buttermaker',
                        'industry' => 'Dairy Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                207 =>
                    array (
                        'id' => 208,
                        'groupno' => 230,
                        'name' => 'Buttonhole And Button Sewing Machine Operator',
                        'industry' => 'Garment',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                208 =>
                    array (
                        'id' => 209,
                        'groupno' => 320,
                        'name' => 'Cabinetmaker',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                209 =>
                    array (
                        'id' => 210,
                        'groupno' => 320,
                        'name' => 'Cable Assembler And Swager',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                210 =>
                    array (
                        'id' => 211,
                        'groupno' => 350,
                        'name' => 'Cable Car Operator',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                211 =>
                    array (
                        'id' => 212,
                        'groupno' => 380,
                        'name' => 'Cable Installer-Repairer',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                212 =>
                    array (
                        'id' => 213,
                        'groupno' => 380,
                        'name' => 'Cable Maintainer',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                213 =>
                    array (
                        'id' => 214,
                        'groupno' => 480,
                        'name' => 'Cable Puller',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                214 =>
                    array (
                        'id' => 215,
                        'groupno' => 380,
                        'name' => 'Cable Splicer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                215 =>
                    array (
                        'id' => 216,
                        'groupno' => 481,
                        'name' => 'Cable Television Installer',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                216 =>
                    array (
                        'id' => 217,
                        'groupno' => 380,
                        'name' => 'Cable Tester',
                        'industry' => 'Telephone And Telegraph',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                217 =>
                    array (
                        'id' => 218,
                        'groupno' => 120,
                        'name' => 'Cad Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                218 =>
                    array (
                        'id' => 219,
                        'groupno' => 360,
                        'name' => 'Caddie',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                219 =>
                    array (
                        'id' => 220,
                        'groupno' => 322,
                        'name' => 'Cafeteria Attendant',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                220 =>
                    array (
                        'id' => 221,
                        'groupno' => 480,
                        'name' => 'Cager',
                        'industry' => 'Mine And Quarry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                221 =>
                    array (
                        'id' => 222,
                        'groupno' => 221,
                        'name' => 'Cake Decorator',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                222 =>
                    array (
                        'id' => 223,
                        'groupno' => 120,
                        'name' => 'Calligrapher',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                223 =>
                    array (
                        'id' => 224,
                        'groupno' => 360,
                        'name' => 'Camera Operator',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                224 =>
                    array (
                        'id' => 225,
                        'groupno' => 220,
                        'name' => 'Camera Repairer',
                        'industry' => 'Photography Apparel',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                225 =>
                    array (
                        'id' => 226,
                        'groupno' => 390,
                        'name' => 'Camp Counselor',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                226 =>
                    array (
                        'id' => 227,
                        'groupno' => 340,
                        'name' => 'Campground Attendant',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                227 =>
                    array (
                        'id' => 228,
                        'groupno' => 230,
                        'name' => 'Can-Filling And Closing Machine Tender',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                228 =>
                    array (
                        'id' => 229,
                        'groupno' => 221,
                        'name' => 'Candlemaker',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                229 =>
                    array (
                        'id' => 230,
                        'groupno' => 331,
                        'name' => 'Candy Maker',
                        'industry' => 'Sugar And Confectionery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                230 =>
                    array (
                        'id' => 231,
                        'groupno' => 221,
                        'name' => 'Caner',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                231 =>
                    array (
                        'id' => 232,
                        'groupno' => 230,
                        'name' => 'Cannery Worker, Hand Or Machine',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                232 =>
                    array (
                        'id' => 233,
                        'groupno' => 420,
                        'name' => 'Canvas Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                233 =>
                    array (
                        'id' => 234,
                        'groupno' => 230,
                        'name' => 'Cap-Lining Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                234 =>
                    array (
                        'id' => 235,
                        'groupno' => 320,
                        'name' => 'Capacitor Assembler',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                235 =>
                    array (
                        'id' => 236,
                        'groupno' => 322,
                        'name' => 'Car Hop',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                236 =>
                    array (
                        'id' => 237,
                        'groupno' => 460,
                        'name' => 'Carbide Powder Processor',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                237 =>
                    array (
                        'id' => 238,
                        'groupno' => 211,
                        'name' => 'Card Dealer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                238 =>
                    array (
                        'id' => 239,
                        'groupno' => 110,
                        'name' => 'Cardiac Monitor Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                239 =>
                    array (
                        'id' => 240,
                        'groupno' => 212,
                        'name' => 'Cardiopulmonary Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                240 =>
                    array (
                        'id' => 241,
                        'groupno' => 240,
                        'name' => 'Cardroom Attendant',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                241 =>
                    array (
                        'id' => 242,
                        'groupno' => 360,
                        'name' => 'Cargo Agent',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                242 =>
                    array (
                        'id' => 243,
                        'groupno' => 380,
                        'name' => 'Carpenter',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                243 =>
                    array (
                        'id' => 244,
                        'groupno' => 380,
                        'name' => 'Carpenter Apprentice',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                244 =>
                    array (
                        'id' => 245,
                        'groupno' => 480,
                        'name' => 'Carpenter Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                245 =>
                    array (
                        'id' => 246,
                        'groupno' => 380,
                        'name' => 'Carpenter, Acoustical',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                246 =>
                    array (
                        'id' => 247,
                        'groupno' => 380,
                        'name' => 'Carpenter, Maintenance',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                247 =>
                    array (
                        'id' => 248,
                        'groupno' => 380,
                        'name' => 'Carpenter, Railcar',
                        'industry' => 'Railroad Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                248 =>
                    array (
                        'id' => 249,
                        'groupno' => 481,
                        'name' => 'Carpenter, Rough',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                249 =>
                    array (
                        'id' => 250,
                        'groupno' => 380,
                        'name' => 'Carpenter, Ship',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                250 =>
                    array (
                        'id' => 251,
                        'groupno' => 480,
                        'name' => 'Carpet Cutter',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                251 =>
                    array (
                        'id' => 252,
                        'groupno' => 481,
                        'name' => 'Carpet Layer',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                252 =>
                    array (
                        'id' => 253,
                        'groupno' => 480,
                        'name' => 'Carpet Layer Helper',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                253 =>
                    array (
                        'id' => 254,
                        'groupno' => 321,
                        'name' => 'Carpet Sewer',
                        'industry' => 'Carpet And Rug',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                254 =>
                    array (
                        'id' => 255,
                        'groupno' => 230,
                        'name' => 'Carpet Weaver',
                        'industry' => 'Carpet And Rug',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                255 =>
                    array (
                        'id' => 256,
                        'groupno' => 120,
                        'name' => 'Cartographer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                256 =>
                    array (
                        'id' => 257,
                        'groupno' => 330,
                        'name' => 'Carton-Forming Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                257 =>
                    array (
                        'id' => 258,
                        'groupno' => 460,
                        'name' => 'Carton-Forming Machine Tender',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                258 =>
                    array (
                        'id' => 259,
                        'groupno' => 120,
                        'name' => 'Cartoonist, Motion Pictures',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                259 =>
                    array (
                        'id' => 260,
                        'groupno' => 111,
                        'name' => 'Caseworker',
                        'industry' => 'Social Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                260 =>
                    array (
                        'id' => 261,
                        'groupno' => 320,
                        'name' => 'Cash Register Servicer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                261 =>
                    array (
                        'id' => 262,
                        'groupno' => 111,
                        'name' => 'Cashier',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                262 =>
                    array (
                        'id' => 263,
                        'groupno' => 214,
                        'name' => 'Cashier-Checker',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                263 =>
                    array (
                        'id' => 264,
                        'groupno' => 230,
                        'name' => 'Casing Machine Operator',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                264 =>
                    array (
                        'id' => 265,
                        'groupno' => 330,
                        'name' => 'Caster',
                        'industry' => 'Smelting And Refining',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                265 =>
                    array (
                        'id' => 266,
                        'groupno' => 331,
                        'name' => 'Caster',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                266 =>
                    array (
                        'id' => 267,
                        'groupno' => 320,
                        'name' => 'Casting Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                267 =>
                    array (
                        'id' => 268,
                        'groupno' => 322,
                        'name' => 'Caterer',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                268 =>
                    array (
                        'id' => 269,
                        'groupno' => 491,
                        'name' => 'Cattle Herder',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                269 =>
                    array (
                        'id' => 270,
                        'groupno' => 480,
                        'name' => 'Caulker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                270 =>
                    array (
                        'id' => 271,
                        'groupno' => 330,
                        'name' => 'Cellophane Bag Machine Operator',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                271 =>
                    array (
                        'id' => 272,
                        'groupno' => 481,
                        'name' => 'Cement Mason',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                272 =>
                    array (
                        'id' => 273,
                        'groupno' => 480,
                        'name' => 'Cement Mason Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                273 =>
                    array (
                        'id' => 274,
                        'groupno' => 480,
                        'name' => 'Cement Sprayer, Nozzle',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                274 =>
                    array (
                        'id' => 275,
                        'groupno' => 480,
                        'name' => 'Cementer, Oilwell',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                275 =>
                    array (
                        'id' => 276,
                        'groupno' => 331,
                        'name' => 'Center Machine Operator',
                        'industry' => 'Sugar And Confectionery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                276 =>
                    array (
                        'id' => 277,
                        'groupno' => 380,
                        'name' => 'Central Office Repairer T',
                        'industry' => 'Telephone And Telegraph',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                277 =>
                    array (
                        'id' => 278,
                        'groupno' => 331,
                        'name' => 'Centrifugal Extractor Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                278 =>
                    array (
                        'id' => 279,
                        'groupno' => 230,
                        'name' => 'Centrifuge Operator, Plasma Processing',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                279 =>
                    array (
                        'id' => 280,
                        'groupno' => 230,
                        'name' => 'Centrifuge Separator Operator',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                280 =>
                    array (
                        'id' => 281,
                        'groupno' => 110,
                        'name' => 'Cephalometric Analyst',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                281 =>
                    array (
                        'id' => 282,
                        'groupno' => 331,
                        'name' => 'Ceramic Coater, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                282 =>
                    array (
                        'id' => 283,
                        'groupno' => 460,
                        'name' => 'Chain Offbearer',
                        'industry' => 'Sawmill And Planer Mill',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                283 =>
                    array (
                        'id' => 284,
                        'groupno' => 492,
                        'name' => 'Chain Saw Operator',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                284 =>
                    array (
                        'id' => 285,
                        'groupno' => 331,
                        'name' => 'Char Conveyor Tender',
                        'industry' => 'Sugar And Confectionery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                285 =>
                    array (
                        'id' => 286,
                        'groupno' => 230,
                        'name' => 'Charge Preparation Technician',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                286 =>
                    array (
                        'id' => 287,
                        'groupno' => 492,
                        'name' => 'Chaser',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                287 =>
                    array (
                        'id' => 288,
                        'groupno' => 250,
                        'name' => 'Chauffeur',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                288 =>
                    array (
                        'id' => 289,
                        'groupno' => 111,
                        'name' => 'Check Cashier',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                289 =>
                    array (
                        'id' => 290,
                        'groupno' => 360,
                        'name' => 'Checker',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                290 =>
                    array (
                        'id' => 291,
                        'groupno' => 214,
                        'name' => 'Checker, Grocery',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                291 =>
                    array (
                        'id' => 292,
                        'groupno' => 360,
                        'name' => 'Checker, Unloader',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                292 =>
                    array (
                        'id' => 293,
                        'groupno' => 360,
                        'name' => 'Checker, Warehouse',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                293 =>
                    array (
                        'id' => 294,
                        'groupno' => 240,
                        'name' => 'Checkroom Attendant',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                294 =>
                    array (
                        'id' => 295,
                        'groupno' => 322,
                        'name' => 'Cheese Cutter',
                        'industry' => 'Dairy Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                295 =>
                    array (
                        'id' => 296,
                        'groupno' => 322,
                        'name' => 'Cheesemaker',
                        'industry' => 'Dairy Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                296 =>
                    array (
                        'id' => 297,
                        'groupno' => 322,
                        'name' => 'Chef De Froid',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                297 =>
                    array (
                        'id' => 298,
                        'groupno' => 212,
                        'name' => 'Chemical Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                298 =>
                    array (
                        'id' => 299,
                        'groupno' => 212,
                        'name' => 'Chemical Laboratory Technicianm',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                299 =>
                    array (
                        'id' => 300,
                        'groupno' => 230,
                        'name' => 'Chemical Preparer',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                300 =>
                    array (
                        'id' => 301,
                        'groupno' => 212,
                        'name' => 'Chemist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                301 =>
                    array (
                        'id' => 302,
                        'groupno' => 240,
                        'name' => 'Child Monitor',
                        'industry' => 'Domestic Services; Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                302 =>
                    array (
                        'id' => 303,
                        'groupno' => 111,
                        'name' => 'Child Support Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                303 =>
                    array (
                        'id' => 304,
                        'groupno' => 340,
                        'name' => 'Child-Care Attendant, Handicapped',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                304 =>
                    array (
                        'id' => 305,
                        'groupno' => 340,
                        'name' => 'Children\'S Institution Attendant',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                305 =>
                    array (
                        'id' => 306,
                        'groupno' => 341,
                        'name' => 'Chimney Sweep',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                306 =>
                    array (
                        'id' => 307,
                        'groupno' => 460,
                        'name' => 'Chipper, Rough',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                307 =>
                    array (
                        'id' => 308,
                        'groupno' => 311,
                        'name' => 'Chiropractor',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                308 =>
                    array (
                        'id' => 309,
                        'groupno' => 311,
                        'name' => 'Chiropractor Assistant',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                309 =>
                    array (
                        'id' => 310,
                        'groupno' => 460,
                        'name' => 'Chocolate Production Machine Operator',
                        'industry' => 'Sugar And Confectionery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                310 =>
                    array (
                        'id' => 311,
                        'groupno' => 560,
                        'name' => 'Choke Setter',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                311 =>
                    array (
                        'id' => 312,
                        'groupno' => 492,
                        'name' => 'Chopper',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                312 =>
                    array (
                        'id' => 313,
                        'groupno' => 491,
                        'name' => 'Christmas Tree Farm Worker',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                313 =>
                    array (
                        'id' => 314,
                        'groupno' => 320,
                        'name' => 'Chucking Lathe Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                314 =>
                    array (
                        'id' => 315,
                        'groupno' => 330,
                        'name' => 'Circular Sawyer, Stone',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                315 =>
                    array (
                        'id' => 316,
                        'groupno' => 212,
                        'name' => 'Civil Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                316 =>
                    array (
                        'id' => 317,
                        'groupno' => 251,
                        'name' => 'Claim Adjuster, Field',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                317 =>
                    array (
                        'id' => 318,
                        'groupno' => 111,
                        'name' => 'Claim Adjuster, Inside',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                318 =>
                    array (
                        'id' => 319,
                        'groupno' => 111,
                        'name' => 'Claims Clerk',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                319 =>
                    array (
                        'id' => 320,
                        'groupno' => 221,
                        'name' => 'Clay Modeler',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                320 =>
                    array (
                        'id' => 321,
                        'groupno' => 340,
                        'name' => 'Cleaner, Commercial Or Institutional',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                321 =>
                    array (
                        'id' => 322,
                        'groupno' => 340,
                        'name' => 'Cleaner, Equipment',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                322 =>
                    array (
                        'id' => 323,
                        'groupno' => 340,
                        'name' => 'Cleaner, Hospital',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                323 =>
                    array (
                        'id' => 324,
                        'groupno' => 340,
                        'name' => 'Cleaner, Laboratory Equipment',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                324 =>
                    array (
                        'id' => 325,
                        'groupno' => 341,
                        'name' => 'Cleaner, Window',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                325 =>
                    array (
                        'id' => 326,
                        'groupno' => 210,
                        'name' => 'Clergy Member',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                326 =>
                    array (
                        'id' => 327,
                        'groupno' => 112,
                        'name' => 'Clerk-Typist',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                327 =>
                    array (
                        'id' => 328,
                        'groupno' => 111,
                        'name' => 'Clerk, Advertising Space',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                328 =>
                    array (
                        'id' => 329,
                        'groupno' => 111,
                        'name' => 'Clerk, Animal Hospital',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                329 =>
                    array (
                        'id' => 330,
                        'groupno' => 112,
                        'name' => 'Clerk, Billing',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                330 =>
                    array (
                        'id' => 331,
                        'groupno' => 111,
                        'name' => 'Clerk, Collection',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                331 =>
                    array (
                        'id' => 332,
                        'groupno' => 111,
                        'name' => 'Clerk, Contract, Automobile',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                332 =>
                    array (
                        'id' => 333,
                        'groupno' => 111,
                        'name' => 'Clerk, Court',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                333 =>
                    array (
                        'id' => 334,
                        'groupno' => 111,
                        'name' => 'Clerk, Credit',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                334 =>
                    array (
                        'id' => 335,
                        'groupno' => 111,
                        'name' => 'Clerk, Election',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                335 =>
                    array (
                        'id' => 336,
                        'groupno' => 214,
                        'name' => 'Clerk, File',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                336 =>
                    array (
                        'id' => 337,
                        'groupno' => 211,
                        'name' => 'Clerk, General',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                337 =>
                    array (
                        'id' => 338,
                        'groupno' => 211,
                        'name' => 'Clerk, Inventory Control',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',

                    ),
                338 =>
                    array (
                        'id' => 339,
                        'groupno' => 214,
                        'name' => 'Clerk, Sales',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                339 =>
                    array (
                        'id' => 340,
                        'groupno' => 360,
                        'name' => 'Clerk, Shipping',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                340 =>
                    array (
                        'id' => 341,
                        'groupno' => 112,
                        'name' => 'Clerk, Statistical',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                341 =>
                    array (
                        'id' => 342,
                        'groupno' => 111,
                        'name' => 'Clerk, Wire Transfer',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                342 =>
                    array (
                        'id' => 343,
                        'groupno' => 110,
                        'name' => 'Clinical Psychologist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                343 =>
                    array (
                        'id' => 344,
                        'groupno' => 330,
                        'name' => 'Cloth Printer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                344 =>
                    array (
                        'id' => 345,
                        'groupno' => 221,
                        'name' => 'Cloth Tester, Quality',
                        'industry' => 'Textile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                345 =>
                    array (
                        'id' => 346,
                        'groupno' => 390,
                        'name' => 'Coach, Professional Athletes',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                346 =>
                    array (
                        'id' => 347,
                        'groupno' => 331,
                        'name' => 'Coater Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                347 =>
                    array (
                        'id' => 348,
                        'groupno' => 331,
                        'name' => 'Coating Machine Operator',
                        'industry' => 'Paper And Pulp',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                348 =>
                    array (
                        'id' => 349,
                        'groupno' => 320,
                        'name' => 'Cobbler',
                        'industry' => 'Boot And Shoe',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                349 =>
                    array (
                        'id' => 350,
                        'groupno' => 230,
                        'name' => 'Coffee Roaster',
                        'industry' => 'Food Preparation, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                350 =>
                    array (
                        'id' => 351,
                        'groupno' => 322,
                        'name' => 'Coffeemaker',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                351 =>
                    array (
                        'id' => 352,
                        'groupno' => 230,
                        'name' => 'Coil Winder',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                352 =>
                    array (
                        'id' => 353,
                        'groupno' => 221,
                        'name' => 'Coil Winder, Repair',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                353 =>
                    array (
                        'id' => 354,
                        'groupno' => 214,
                        'name' => 'Coin Counter And Wrapper',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                354 =>
                    array (
                        'id' => 355,
                        'groupno' => 251,
                        'name' => 'Coin Machine Collector',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                355 =>
                    array (
                        'id' => 356,
                        'groupno' => 370,
                        'name' => 'Coin Machine Service Repairer',
                        'industry' => 'Service Individual Machine',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                356 =>
                    array (
                        'id' => 357,
                        'groupno' => 111,
                        'name' => 'Collection Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                357 =>
                    array (
                        'id' => 358,
                        'groupno' => 251,
                        'name' => 'Collector, Outside',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                358 =>
                    array (
                        'id' => 359,
                        'groupno' => 230,
                        'name' => 'Color Printer Operator',
                        'industry' => 'Photofinishing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                359 =>
                    array (
                        'id' => 360,
                        'groupno' => 111,
                        'name' => 'Columnist/Commentator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                360 =>
                    array (
                        'id' => 361,
                        'groupno' => 111,
                        'name' => 'Community Organization Worker',
                        'industry' => 'Social Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                361 =>
                    array (
                        'id' => 362,
                        'groupno' => 250,
                        'name' => 'Community Service Officer, Patrol',
                        'industry' => 'Social Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                362 =>
                    array (
                        'id' => 363,
                        'groupno' => 240,
                        'name' => 'Companion',
                        'industry' => 'Domestic Services; Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                363 =>
                    array (
                        'id' => 364,
                        'groupno' => 221,
                        'name' => 'Compositor, Typesetter',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                364 =>
                    array (
                        'id' => 365,
                        'groupno' => 230,
                        'name' => 'Compounder',
                        'industry' => 'Petrol Refinery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                365 =>
                    array (
                        'id' => 366,
                        'groupno' => 360,
                        'name' => 'Compressed Gas Plant Worker',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                366 =>
                    array (
                        'id' => 367,
                        'groupno' => 332,
                        'name' => 'Compressor Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                367 =>
                    array (
                        'id' => 368,
                        'groupno' => 112,
                        'name' => 'Computer Keyboard Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                368 =>
                    array (
                        'id' => 369,
                        'groupno' => 230,
                        'name' => 'Computer Operator, Mainframe',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                369 =>
                    array (
                        'id' => 370,
                        'groupno' => 111,
                        'name' => 'Computer Processing Scheduler',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                370 =>
                    array (
                        'id' => 371,
                        'groupno' => 112,
                        'name' => 'Computer Programmer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                371 =>
                    array (
                        'id' => 372,
                        'groupno' => 320,
                        'name' => 'Computer Repairer',
                        'industry' => 'Office Machines',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                372 =>
                    array (
                        'id' => 373,
                        'groupno' => 111,
                        'name' => 'Computer Security Specialist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                373 =>
                    array (
                        'id' => 374,
                        'groupno' => 320,
                        'name' => 'Computer Set-Up Person',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                374 =>
                    array (
                        'id' => 375,
                        'groupno' => 111,
                        'name' => 'Computer Support Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                375 =>
                    array (
                        'id' => 376,
                        'groupno' => 351,
                        'name' => 'Concrete Paving Machine Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                376 =>
                    array (
                        'id' => 377,
                        'groupno' => 480,
                        'name' => 'Concrete Stone Finisher',
                        'industry' => 'Concrete Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                377 =>
                    array (
                        'id' => 378,
                        'groupno' => 480,
                        'name' => 'Concrete Vibrator Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                378 =>
                    array (
                        'id' => 379,
                        'groupno' => 340,
                        'name' => 'Conductor, All Rails',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                379 =>
                    array (
                        'id' => 380,
                        'groupno' => 213,
                        'name' => 'Conductor, Passenger Car',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                380 =>
                    array (
                        'id' => 381,
                        'groupno' => 370,
                        'name' => 'Construction Equipment Mechanic',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                381 =>
                    array (
                        'id' => 382,
                        'groupno' => 110,
                        'name' => 'Consultant, Education',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                382 =>
                    array (
                        'id' => 383,
                        'groupno' => 230,
                        'name' => 'Contact Lens Molder',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                383 =>
                    array (
                        'id' => 384,
                        'groupno' => 330,
                        'name' => 'Contour Band Saw Operator, Vertical',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                384 =>
                    array (
                        'id' => 385,
                        'groupno' => 213,
                        'name' => 'Contractor',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                385 =>
                    array (
                        'id' => 386,
                        'groupno' => 120,
                        'name' => 'Controls Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                386 =>
                    array (
                        'id' => 387,
                        'groupno' => 360,
                        'name' => 'Conveyor Feeder-Offbearer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                387 =>
                    array (
                        'id' => 388,
                        'groupno' => 370,
                        'name' => 'Conveyor Maintenance Mechanic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                388 =>
                    array (
                        'id' => 389,
                        'groupno' => 360,
                        'name' => 'Conveyor System Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                389 =>
                    array (
                        'id' => 390,
                        'groupno' => 360,
                        'name' => 'Conveyor Tender',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                390 =>
                    array (
                        'id' => 391,
                        'groupno' => 322,
                        'name' => 'Cook',
                        'industry' => 'Domestic Services; Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                391 =>
                    array (
                        'id' => 392,
                        'groupno' => 322,
                        'name' => 'Cook',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                392 =>
                    array (
                        'id' => 393,
                        'groupno' => 322,
                        'name' => 'Cook Assistant',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                393 =>
                    array (
                        'id' => 394,
                        'groupno' => 322,
                        'name' => 'Cook, Chief',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                394 =>
                    array (
                        'id' => 395,
                        'groupno' => 322,
                        'name' => 'Cook, Fast Food',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                395 =>
                    array (
                        'id' => 396,
                        'groupno' => 322,
                        'name' => 'Cook, Pastry',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                396 =>
                    array (
                        'id' => 397,
                        'groupno' => 322,
                        'name' => 'Cook, Specialty',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                397 =>
                    array (
                        'id' => 398,
                        'groupno' => 110,
                        'name' => 'Coordinator, Skill-Training Program',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                398 =>
                    array (
                        'id' => 399,
                        'groupno' => 111,
                        'name' => 'Copy Reader',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                399 =>
                    array (
                        'id' => 400,
                        'groupno' => 111,
                        'name' => 'Copy Writer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                400 =>
                    array (
                        'id' => 401,
                        'groupno' => 112,
                        'name' => 'Copyist',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                401 =>
                    array (
                        'id' => 402,
                        'groupno' => 480,
                        'name' => 'Core Drill Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                402 =>
                    array (
                        'id' => 403,
                        'groupno' => 330,
                        'name' => 'Coremaker',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                403 =>
                    array (
                        'id' => 404,
                        'groupno' => 331,
                        'name' => 'Coremaker, Floor',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                404 =>
                    array (
                        'id' => 405,
                        'groupno' => 490,
                        'name' => 'Correction Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                405 =>
                    array (
                        'id' => 406,
                        'groupno' => 290,
                        'name' => 'Cosmetologist',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                406 =>
                    array (
                        'id' => 407,
                        'groupno' => 110,
                        'name' => 'Counselor',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                407 =>
                    array (
                        'id' => 408,
                        'groupno' => 390,
                        'name' => 'Counselor, Camp',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                408 =>
                    array (
                        'id' => 409,
                        'groupno' => 322,
                        'name' => 'Counter Attendant, Cafeteria',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                409 =>
                    array (
                        'id' => 410,
                        'groupno' => 250,
                        'name' => 'Courier',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                410 =>
                    array (
                        'id' => 411,
                        'groupno' => 111,
                        'name' => 'Court Clerk',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                411 =>
                    array (
                        'id' => 412,
                        'groupno' => 112,
                        'name' => 'Court Reporter',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                412 =>
                    array (
                        'id' => 413,
                        'groupno' => 491,
                        'name' => 'Cowpuncher',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                413 =>
                    array (
                        'id' => 414,
                        'groupno' => 360,
                        'name' => 'Crane Follower',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                414 =>
                    array (
                        'id' => 415,
                        'groupno' => 360,
                        'name' => 'Crane Hooker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                415 =>
                    array (
                        'id' => 416,
                        'groupno' => 351,
                        'name' => 'Crane Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                416 =>
                    array (
                        'id' => 417,
                        'groupno' => 360,
                        'name' => 'Crate Maker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                417 =>
                    array (
                        'id' => 418,
                        'groupno' => 111,
                        'name' => 'Credit Authorizer',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                418 =>
                    array (
                        'id' => 419,
                        'groupno' => 111,
                        'name' => 'Credit Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                419 =>
                    array (
                        'id' => 420,
                        'groupno' => 111,
                        'name' => 'Credit Counselor',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                420 =>
                    array (
                        'id' => 421,
                        'groupno' => 460,
                        'name' => 'Cremator',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                421 =>
                    array (
                        'id' => 422,
                        'groupno' => 111,
                        'name' => 'Crew Scheduler',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                422 =>
                    array (
                        'id' => 423,
                        'groupno' => 230,
                        'name' => 'Crimping Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                423 =>
                    array (
                        'id' => 424,
                        'groupno' => 330,
                        'name' => 'Crossband Layer',
                        'industry' => 'Millwork-Plywood',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                424 =>
                    array (
                        'id' => 425,
                        'groupno' => 460,
                        'name' => 'Crusher Operator',
                        'industry' => 'Concrete Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                425 =>
                    array (
                        'id' => 426,
                        'groupno' => 112,
                        'name' => 'Cryptographic Machine Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                426 =>
                    array (
                        'id' => 427,
                        'groupno' => 330,
                        'name' => 'Crystal Grower',
                        'industry' => 'Communication Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                427 =>
                    array (
                        'id' => 428,
                        'groupno' => 330,
                        'name' => 'Crystal Slicer',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                428 =>
                    array (
                        'id' => 429,
                        'groupno' => 212,
                        'name' => 'Curator',
                        'industry' => 'Museums',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                429 =>
                    array (
                        'id' => 430,
                        'groupno' => 211,
                        'name' => 'Currency Counter',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                430 =>
                    array (
                        'id' => 431,
                        'groupno' => 340,
                        'name' => 'Custodian',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                431 =>
                    array (
                        'id' => 432,
                        'groupno' => 360,
                        'name' => 'Custodian, Athletic Equipment',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                432 =>
                    array (
                        'id' => 433,
                        'groupno' => 211,
                        'name' => 'Custodian, Property',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                433 =>
                    array (
                        'id' => 434,
                        'groupno' => 211,
                        'name' => 'Customer Service Clerk',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                434 =>
                    array (
                        'id' => 435,
                        'groupno' => 213,
                        'name' => 'Customer Service Representative',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                435 =>
                    array (
                        'id' => 436,
                        'groupno' => 112,
                        'name' => 'Customer Service Representative � Inside',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                436 =>
                    array (
                        'id' => 437,
                        'groupno' => 212,
                        'name' => 'Customs Broker',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                437 =>
                    array (
                        'id' => 438,
                        'groupno' => 330,
                        'name' => 'Cut-Off Saw Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                438 =>
                    array (
                        'id' => 439,
                        'groupno' => 330,
                        'name' => 'Cut-Off Saw Operator, Metal',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                439 =>
                    array (
                        'id' => 440,
                        'groupno' => 230,
                        'name' => 'Cutter',
                        'industry' => 'Photofinishing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                440 =>
                    array (
                        'id' => 441,
                        'groupno' => 330,
                        'name' => 'Cutter Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                441 =>
                    array (
                        'id' => 442,
                        'groupno' => 230,
                        'name' => 'Cutter, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                442 =>
                    array (
                        'id' => 443,
                        'groupno' => 460,
                        'name' => 'Cutting Machine Operator',
                        'industry' => 'Textile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                443 =>
                    array (
                        'id' => 444,
                        'groupno' => 230,
                        'name' => 'Cutting Machine Operator, Automated',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                444 =>
                    array (
                        'id' => 445,
                        'groupno' => 330,
                        'name' => 'Cutting Machine Tender',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                445 =>
                    array (
                        'id' => 446,
                        'groupno' => 460,
                        'name' => 'Cylinder Filler',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                446 =>
                    array (
                        'id' => 447,
                        'groupno' => 460,
                        'name' => 'Cylinder Press Feeder',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                447 =>
                    array (
                        'id' => 448,
                        'groupno' => 120,
                        'name' => 'Cytotechnologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                448 =>
                    array (
                        'id' => 449,
                        'groupno' => 460,
                        'name' => 'Dairy Processing Equipment Operator',
                        'industry' => 'Dairy Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                449 =>
                    array (
                        'id' => 670,
                        'groupno' => 332,
                        'name' => 'Firer, High Pressure',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                450 =>
                    array (
                        'id' => 450,
                        'groupno' => 590,
                        'name' => 'Dancer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                451 =>
                    array (
                        'id' => 451,
                        'groupno' => 111,
                        'name' => 'Data Base Administrator',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                452 =>
                    array (
                        'id' => 452,
                        'groupno' => 380,
                        'name' => 'Data Communications Installer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                453 =>
                    array (
                        'id' => 453,
                        'groupno' => 112,
                        'name' => 'Data Entry Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                454 =>
                    array (
                        'id' => 454,
                        'groupno' => 221,
                        'name' => 'Decal Applier',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                455 =>
                    array (
                        'id' => 455,
                        'groupno' => 491,
                        'name' => 'Deckhand',
                        'industry' => 'Water Transport, Fishing And Hunt.',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                456 =>
                    array (
                        'id' => 456,
                        'groupno' => 331,
                        'name' => 'Decontaminator, Radioactive Material',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                457 =>
                    array (
                        'id' => 457,
                        'groupno' => 221,
                        'name' => 'Decorator',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                458 =>
                    array (
                        'id' => 458,
                        'groupno' => 380,
                        'name' => 'Decorator, Special Event',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                459 =>
                    array (
                        'id' => 459,
                        'groupno' => 480,
                        'name' => 'Decorator, Street And Building',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                460 =>
                    array (
                        'id' => 460,
                        'groupno' => 322,
                        'name' => 'Deli Cutter-Slicer',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                461 =>
                    array (
                        'id' => 461,
                        'groupno' => 250,
                        'name' => 'Deliverer, Car Rental',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                462 =>
                    array (
                        'id' => 462,
                        'groupno' => 250,
                        'name' => 'Deliverer, Floral Arrangements',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                463 =>
                    array (
                        'id' => 463,
                        'groupno' => 213,
                        'name' => 'Deliverer, Non-Driving',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                464 =>
                    array (
                        'id' => 464,
                        'groupno' => 250,
                        'name' => 'Deliverer, Pizza',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                465 =>
                    array (
                        'id' => 465,
                        'groupno' => 212,
                        'name' => 'Demonstrator',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                466 =>
                    array (
                        'id' => 466,
                        'groupno' => 212,
                        'name' => 'Dental Assistant',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                467 =>
                    array (
                        'id' => 467,
                        'groupno' => 220,
                        'name' => 'Dental Hygienist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                468 =>
                    array (
                        'id' => 468,
                        'groupno' => 220,
                        'name' => 'Dental Laboratory Technician',
                        'industry' => 'Protective Development',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                469 =>
                    array (
                        'id' => 469,
                        'groupno' => 220,
                        'name' => 'Dentist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                470 =>
                    array (
                        'id' => 470,
                        'groupno' => 490,
                        'name' => 'Deputy, Court',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                471 =>
                    array (
                        'id' => 471,
                        'groupno' => 480,
                        'name' => 'Derrick Worker, Well Service',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                472 =>
                    array (
                        'id' => 472,
                        'groupno' => 230,
                        'name' => 'Design Printer, Balloon',
                        'industry' => 'Rubber Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                473 =>
                    array (
                        'id' => 473,
                        'groupno' => 490,
                        'name' => 'Detective',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                474 =>
                    array (
                        'id' => 474,
                        'groupno' => 390,
                        'name' => 'Detective, Store',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                475 =>
                    array (
                        'id' => 475,
                        'groupno' => 212,
                        'name' => 'Dialysis Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                476 =>
                    array (
                        'id' => 476,
                        'groupno' => 330,
                        'name' => 'Die Casting Machine Operator',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                477 =>
                    array (
                        'id' => 477,
                        'groupno' => 330,
                        'name' => 'Die Cutter',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                478 =>
                    array (
                        'id' => 478,
                        'groupno' => 120,
                        'name' => 'Die Designer',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                479 =>
                    array (
                        'id' => 479,
                        'groupno' => 320,
                        'name' => 'Die Maker',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                480 =>
                    array (
                        'id' => 480,
                        'groupno' => 320,
                        'name' => 'Die Sinker',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                481 =>
                    array (
                        'id' => 481,
                        'groupno' => 322,
                        'name' => 'Dietary Aide, Hospital Services',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                482 =>
                    array (
                        'id' => 482,
                        'groupno' => 212,
                        'name' => 'Dietitian, Clinical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                483 =>
                    array (
                        'id' => 483,
                        'groupno' => 322,
                        'name' => 'Dining Room Attendant',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                484 =>
                    array (
                        'id' => 484,
                        'groupno' => 351,
                        'name' => 'Dinkey Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                485 =>
                    array (
                        'id' => 485,
                        'groupno' => 221,
                        'name' => 'Dipper',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                486 =>
                    array (
                        'id' => 486,
                        'groupno' => 331,
                        'name' => 'Dipper',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                487 =>
                    array (
                        'id' => 487,
                        'groupno' => 110,
                        'name' => 'Director, Fundraising',
                        'industry' => 'Nonprofit Organization',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                488 =>
                    array (
                        'id' => 488,
                        'groupno' => 110,
                        'name' => 'Director, Motion Picture',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                489 =>
                    array (
                        'id' => 489,
                        'groupno' => 212,
                        'name' => 'Director, Recreation Center',
                        'industry' => 'Social Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                490 =>
                    array (
                        'id' => 490,
                        'groupno' => 110,
                        'name' => 'Director, Regulatory Agency',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                491 =>
                    array (
                        'id' => 491,
                        'groupno' => 110,
                        'name' => 'Director, Research And Development',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                492 =>
                    array (
                        'id' => 492,
                        'groupno' => 110,
                        'name' => 'Director, Service',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                493 =>
                    array (
                        'id' => 493,
                        'groupno' => 210,
                        'name' => 'Director, Social',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                494 =>
                    array (
                        'id' => 494,
                        'groupno' => 112,
                        'name' => 'Directory Assistance Operator',
                        'industry' => 'Telephone And Telegraph',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                495 =>
                    array (
                        'id' => 495,
                        'groupno' => 322,
                        'name' => 'Dishwasher, Hand Or Machine',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                496 =>
                    array (
                        'id' => 496,
                        'groupno' => 111,
                        'name' => 'Dispatcher, Motor Vehicle',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                497 =>
                    array (
                        'id' => 497,
                        'groupno' => 380,
                        'name' => 'Display Maker',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                498 =>
                    array (
                        'id' => 498,
                        'groupno' => 330,
                        'name' => 'Display Screen Fabricator',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                499 =>
                    array (
                        'id' => 499,
                        'groupno' => 360,
                        'name' => 'Displayer, Merchandise',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
            ));
            \DB::table('occupations')->insert(array (
                0 =>
                    array (
                        'id' => 500,
                        'groupno' => 460,
                        'name' => 'Distillery Worker, General',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',

                    ),
                1 =>
                    array (
                        'id' => 501,
                        'groupno' => 221,
                        'name' => 'Distresser',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',

                    ),
                2 =>
                    array (
                        'id' => 502,
                        'groupno' => 480,
                        'name' => 'Ditch Digger',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',

                    ),
                3 =>
                    array (
                        'id' => 503,
                        'groupno' => 492,
                        'name' => 'Diver',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                4 =>
                    array (
                        'id' => 504,
                        'groupno' => 230,
                        'name' => 'Dividing Machine Operator',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                5 =>
                    array (
                        'id' => 505,
                        'groupno' => 111,
                        'name' => 'Document Preparer, Microfilming',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                6 =>
                    array (
                        'id' => 506,
                        'groupno' => 491,
                        'name' => 'Dog Catcher',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                7 =>
                    array (
                        'id' => 507,
                        'groupno' => 491,
                        'name' => 'Dog Groomer',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                8 =>
                    array (
                        'id' => 508,
                        'groupno' => 251,
                        'name' => 'Dog Licenser',
                        'industry' => 'Nonprofit Organization',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                9 =>
                    array (
                        'id' => 509,
                        'groupno' => 560,
                        'name' => 'Dolly Pusher',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                10 =>
                    array (
                        'id' => 510,
                        'groupno' => 390,
                        'name' => 'Double',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                11 =>
                    array (
                        'id' => 511,
                        'groupno' => 460,
                        'name' => 'Dough Brake Machine Operator',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                12 =>
                    array (
                        'id' => 512,
                        'groupno' => 322,
                        'name' => 'Dough Molder, Hand',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                13 =>
                    array (
                        'id' => 513,
                        'groupno' => 322,
                        'name' => 'Doughnut Maker',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                14 =>
                    array (
                        'id' => 514,
                        'groupno' => 330,
                        'name' => 'Dowel Machine Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                15 =>
                    array (
                        'id' => 515,
                        'groupno' => 120,
                        'name' => 'Drafter, Architectural',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                16 =>
                    array (
                        'id' => 516,
                        'groupno' => 120,
                        'name' => 'Drafter, Assistant',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                17 =>
                    array (
                        'id' => 517,
                        'groupno' => 120,
                        'name' => 'Drafter, Civil',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                18 =>
                    array (
                        'id' => 518,
                        'groupno' => 120,
                        'name' => 'Drafter, Electrical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                19 =>
                    array (
                        'id' => 519,
                        'groupno' => 120,
                        'name' => 'Drafter, Electromechanisms Design',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                20 =>
                    array (
                        'id' => 520,
                        'groupno' => 120,
                        'name' => 'Drafter, Landscape',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                21 =>
                    array (
                        'id' => 521,
                        'groupno' => 120,
                        'name' => 'Drafter, Mechanical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                22 =>
                    array (
                        'id' => 522,
                        'groupno' => 351,
                        'name' => 'Dragline Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                23 =>
                    array (
                        'id' => 523,
                        'groupno' => 380,
                        'name' => 'Drapery Hanger',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                24 =>
                    array (
                        'id' => 524,
                        'groupno' => 110,
                        'name' => 'Drawings Checker, Engineering',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                25 =>
                    array (
                        'id' => 525,
                        'groupno' => 221,
                        'name' => 'Dressmaker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                26 =>
                    array (
                        'id' => 526,
                        'groupno' => 230,
                        'name' => 'Drier Operator',
                        'industry' => 'Food Preparation, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                27 =>
                    array (
                        'id' => 527,
                        'groupno' => 331,
                        'name' => 'Drier Operator',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                28 =>
                    array (
                        'id' => 528,
                        'groupno' => 330,
                        'name' => 'Drill Press Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                29 =>
                    array (
                        'id' => 529,
                        'groupno' => 330,
                        'name' => 'Drill Press Operator, Numerical Control',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                30 =>
                    array (
                        'id' => 530,
                        'groupno' => 321,
                        'name' => 'Driller, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                31 =>
                    array (
                        'id' => 531,
                        'groupno' => 240,
                        'name' => 'Drive-In Theater Attendant',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                32 =>
                    array (
                        'id' => 532,
                        'groupno' => 350,
                        'name' => 'Driver, Newspaper Delivery',
                        'industry' => 'Wholesale Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                33 =>
                    array (
                        'id' => 533,
                        'groupno' => 251,
                        'name' => 'Driver\'S License Examiner',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                34 =>
                    array (
                        'id' => 534,
                        'groupno' => 430,
                        'name' => 'Drophammer Operator',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                35 =>
                    array (
                        'id' => 535,
                        'groupno' => 430,
                        'name' => 'Drum Straightener',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                36 =>
                    array (
                        'id' => 536,
                        'groupno' => 340,
                        'name' => 'Dry Cleaner',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                37 =>
                    array (
                        'id' => 537,
                        'groupno' => 380,
                        'name' => 'Dry Wall Applicator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                38 =>
                    array (
                        'id' => 538,
                        'groupno' => 331,
                        'name' => 'Dry-Press Operator',
                        'industry' => 'Brick And Tile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                39 =>
                    array (
                        'id' => 539,
                        'groupno' => 481,
                        'name' => 'Duct Installer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                40 =>
                    array (
                        'id' => 540,
                        'groupno' => 330,
                        'name' => 'Dynamite Packing Machine Operator',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                41 =>
                    array (
                        'id' => 541,
                        'groupno' => 212,
                        'name' => 'Echocardiograph Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                42 =>
                    array (
                        'id' => 542,
                        'groupno' => 110,
                        'name' => 'Editor, Managing, Newspaper',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                43 =>
                    array (
                        'id' => 543,
                        'groupno' => 111,
                        'name' => 'Editor, Newspaper',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                44 =>
                    array (
                        'id' => 544,
                        'groupno' => 111,
                        'name' => 'Editor, Publications',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                45 =>
                    array (
                        'id' => 545,
                        'groupno' => 112,
                        'name' => 'Editorial Writer',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                46 =>
                    array (
                        'id' => 546,
                        'groupno' => 221,
                        'name' => 'Egg Candler',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                47 =>
                    array (
                        'id' => 547,
                        'groupno' => 380,
                        'name' => 'Electric Meter Installer',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                48 =>
                    array (
                        'id' => 548,
                        'groupno' => 221,
                        'name' => 'Electric Motor Assembler',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                49 =>
                    array (
                        'id' => 549,
                        'groupno' => 320,
                        'name' => 'Electric Motor Control Unit Assembler',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                50 =>
                    array (
                        'id' => 550,
                        'groupno' => 320,
                        'name' => 'Electric Sign Assembler',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                51 =>
                    array (
                        'id' => 551,
                        'groupno' => 221,
                        'name' => 'Electrical Appliance Repairer, Small',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                52 =>
                    array (
                        'id' => 552,
                        'groupno' => 370,
                        'name' => 'Electrical Appliance Servicer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                53 =>
                    array (
                        'id' => 553,
                        'groupno' => 460,
                        'name' => 'Electrical Appliance Uncrater',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                54 =>
                    array (
                        'id' => 554,
                        'groupno' => 212,
                        'name' => 'Electrical Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                55 =>
                    array (
                        'id' => 555,
                        'groupno' => 221,
                        'name' => 'Electrical Instrument Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                56 =>
                    array (
                        'id' => 556,
                        'groupno' => 212,
                        'name' => 'Electrical Technician',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                57 =>
                    array (
                        'id' => 557,
                        'groupno' => 380,
                        'name' => 'Electrician',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                58 =>
                    array (
                        'id' => 558,
                        'groupno' => 380,
                        'name' => 'Electrician',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                59 =>
                    array (
                        'id' => 559,
                        'groupno' => 380,
                        'name' => 'Electrician Apprentice',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                60 =>
                    array (
                        'id' => 560,
                        'groupno' => 380,
                        'name' => 'Electrician Helper',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                61 =>
                    array (
                        'id' => 561,
                        'groupno' => 370,
                        'name' => 'Electrician, Automotive',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                62 =>
                    array (
                        'id' => 562,
                        'groupno' => 380,
                        'name' => 'Electrician, Maintenance',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                63 =>
                    array (
                        'id' => 563,
                        'groupno' => 380,
                        'name' => 'Electrician, Powerhouse',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                64 =>
                    array (
                        'id' => 564,
                        'groupno' => 460,
                        'name' => 'Electroless Plater, Printed Circuit Board Panels',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                65 =>
                    array (
                        'id' => 565,
                        'groupno' => 290,
                        'name' => 'Electrologist',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                66 =>
                    array (
                        'id' => 566,
                        'groupno' => 220,
                        'name' => 'Electromechanical Technician',
                        'industry' => 'Instalation And Application',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                67 =>
                    array (
                        'id' => 567,
                        'groupno' => 320,
                        'name' => 'Electromedical Equipment Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                68 =>
                    array (
                        'id' => 568,
                        'groupno' => 212,
                        'name' => 'Electromyographic Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                69 =>
                    array (
                        'id' => 569,
                        'groupno' => 221,
                        'name' => 'Electronic Component Processor',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                70 =>
                    array (
                        'id' => 570,
                        'groupno' => 221,
                        'name' => 'Electronics Assembler',
                        'industry' => 'Communication Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                71 =>
                    array (
                        'id' => 571,
                        'groupno' => 212,
                        'name' => 'Electronics Design Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                72 =>
                    array (
                        'id' => 572,
                        'groupno' => 212,
                        'name' => 'Electronics Technician',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                73 =>
                    array (
                        'id' => 573,
                        'groupno' => 221,
                        'name' => 'Electronics Tester',
                        'industry' => 'Communication Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                74 =>
                    array (
                        'id' => 574,
                        'groupno' => 351,
                        'name' => 'Elevating Grader Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                75 =>
                    array (
                        'id' => 575,
                        'groupno' => 482,
                        'name' => 'Elevator Constructor',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                76 =>
                    array (
                        'id' => 576,
                        'groupno' => 380,
                        'name' => 'Elevator Examiner And Adjuster',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                77 =>
                    array (
                        'id' => 577,
                        'groupno' => 460,
                        'name' => 'Elevator Operator, Freight',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                78 =>
                    array (
                        'id' => 578,
                        'groupno' => 380,
                        'name' => 'Elevator Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                79 =>
                    array (
                        'id' => 579,
                        'groupno' => 111,
                        'name' => 'Eligibility Worker',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                80 =>
                    array (
                        'id' => 580,
                        'groupno' => 420,
                        'name' => 'Embalmer',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                81 =>
                    array (
                        'id' => 581,
                        'groupno' => 331,
                        'name' => 'Embosser',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                82 =>
                    array (
                        'id' => 582,
                        'groupno' => 230,
                        'name' => 'Embossing Press Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                83 =>
                    array (
                        'id' => 583,
                        'groupno' => 460,
                        'name' => 'Emergency Medical Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                84 =>
                    array (
                        'id' => 584,
                        'groupno' => 111,
                        'name' => 'Employee Relations Specialist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                85 =>
                    array (
                        'id' => 585,
                        'groupno' => 111,
                        'name' => 'Employment Interviewer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                86 =>
                    array (
                        'id' => 586,
                        'groupno' => 320,
                        'name' => 'Engine Lathe Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                87 =>
                    array (
                        'id' => 587,
                        'groupno' => 111,
                        'name' => 'Engineer, Aeronautical Design',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                88 =>
                    array (
                        'id' => 588,
                        'groupno' => 213,
                        'name' => 'Engineer, Aeronautical Test',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                89 =>
                    array (
                        'id' => 589,
                        'groupno' => 212,
                        'name' => 'Engineer, Agricultural',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                90 =>
                    array (
                        'id' => 590,
                        'groupno' => 212,
                        'name' => 'Engineer, Automotive',
                        'industry' => 'Automotive Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                91 =>
                    array (
                        'id' => 591,
                        'groupno' => 111,
                        'name' => 'Engineer, Biomedical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                92 =>
                    array (
                        'id' => 592,
                        'groupno' => 212,
                        'name' => 'Engineer, Chemical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                93 =>
                    array (
                        'id' => 593,
                        'groupno' => 212,
                        'name' => 'Engineer, Civil',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                94 =>
                    array (
                        'id' => 594,
                        'groupno' => 111,
                        'name' => 'Engineer, Electro-Optical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                95 =>
                    array (
                        'id' => 595,
                        'groupno' => 212,
                        'name' => 'Engineer, Electronics Design',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                96 =>
                    array (
                        'id' => 596,
                        'groupno' => 212,
                        'name' => 'Engineer, Factory Lay-Out',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                97 =>
                    array (
                        'id' => 597,
                        'groupno' => 213,
                        'name' => 'Engineer, Field Service',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                98 =>
                    array (
                        'id' => 598,
                        'groupno' => 212,
                        'name' => 'Engineer, Mechanical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                99 =>
                    array (
                        'id' => 599,
                        'groupno' => 111,
                        'name' => 'Engineer, Nuclear',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                100 =>
                    array (
                        'id' => 600,
                        'groupno' => 111,
                        'name' => 'Engineer, Packaging',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                101 =>
                    array (
                        'id' => 601,
                        'groupno' => 111,
                        'name' => 'Engineer, Power Distribution',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                102 =>
                    array (
                        'id' => 602,
                        'groupno' => 111,
                        'name' => 'Engineer, Product Safety',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                103 =>
                    array (
                        'id' => 603,
                        'groupno' => 212,
                        'name' => 'Engineer, Railroad',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                104 =>
                    array (
                        'id' => 604,
                        'groupno' => 213,
                        'name' => 'Engineer, Soils',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                105 =>
                    array (
                        'id' => 605,
                        'groupno' => 320,
                        'name' => 'Engraver, Hand, Hard Metals',
                        'industry' => 'Engraving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                106 =>
                    array (
                        'id' => 606,
                        'groupno' => 120,
                        'name' => 'Engraver, Hand, Soft Metals',
                        'industry' => 'Engraving; Jewelry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                107 =>
                    array (
                        'id' => 607,
                        'groupno' => 230,
                        'name' => 'Engraver, Machine',
                        'industry' => 'Engraving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                108 =>
                    array (
                        'id' => 608,
                        'groupno' => 213,
                        'name' => 'Environmental Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                109 =>
                    array (
                        'id' => 609,
                        'groupno' => 111,
                        'name' => 'Equal Opportunity Representative',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                110 =>
                    array (
                        'id' => 610,
                        'groupno' => 340,
                        'name' => 'Equipment Cleaner',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                111 =>
                    array (
                        'id' => 611,
                        'groupno' => 370,
                        'name' => 'Equipment Installer, Vehicles',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                112 =>
                    array (
                        'id' => 612,
                        'groupno' => 111,
                        'name' => 'Escrow Officer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                113 =>
                    array (
                        'id' => 613,
                        'groupno' => 111,
                        'name' => 'Estate Planner',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                114 =>
                    array (
                        'id' => 614,
                        'groupno' => 213,
                        'name' => 'Estimator/Cruiser',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                115 =>
                    array (
                        'id' => 615,
                        'groupno' => 221,
                        'name' => 'Etched Circuit Processor',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                116 =>
                    array (
                        'id' => 616,
                        'groupno' => 221,
                        'name' => 'Etcher',
                        'industry' => 'Engraving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                117 =>
                    array (
                        'id' => 617,
                        'groupno' => 320,
                        'name' => 'Etcher, Hand',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                118 =>
                    array (
                        'id' => 618,
                        'groupno' => 370,
                        'name' => 'Evaporative Cooler Installer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                119 =>
                    array (
                        'id' => 619,
                        'groupno' => 111,
                        'name' => 'Examiner',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                120 =>
                    array (
                        'id' => 620,
                        'groupno' => 390,
                        'name' => 'Exercise Physiologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                121 =>
                    array (
                        'id' => 621,
                        'groupno' => 491,
                        'name' => 'Exerciser, Horse',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                122 =>
                    array (
                        'id' => 622,
                        'groupno' => 380,
                        'name' => 'Exhibit Builder',
                        'industry' => 'Museums',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                123 =>
                    array (
                        'id' => 623,
                        'groupno' => 111,
                        'name' => 'Expediter',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                124 =>
                    array (
                        'id' => 624,
                        'groupno' => 360,
                        'name' => 'Expediter, Material',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                125 =>
                    array (
                        'id' => 625,
                        'groupno' => 380,
                        'name' => 'Experimental Aircraft Mechanic',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                126 =>
                    array (
                        'id' => 626,
                        'groupno' => 213,
                        'name' => 'Exterminator',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                127 =>
                    array (
                        'id' => 627,
                        'groupno' => 480,
                        'name' => 'Exterminator, Termite',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                128 =>
                    array (
                        'id' => 628,
                        'groupno' => 213,
                        'name' => 'Extra, Actor',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                129 =>
                    array (
                        'id' => 629,
                        'groupno' => 330,
                        'name' => 'Extruder Operator',
                        'industry' => 'Rubber Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                130 =>
                    array (
                        'id' => 630,
                        'groupno' => 220,
                        'name' => 'Eyeglass Lens Cutter',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                131 =>
                    array (
                        'id' => 631,
                        'groupno' => 230,
                        'name' => 'Fabric Stretcher',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                132 =>
                    array (
                        'id' => 632,
                        'groupno' => 320,
                        'name' => 'Fabricating Machine Operator, Metal',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                133 =>
                    array (
                        'id' => 633,
                        'groupno' => 221,
                        'name' => 'Fabricator, Foam Rubber',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                134 =>
                    array (
                        'id' => 634,
                        'groupno' => 330,
                        'name' => 'Fabricator/Assembler, Metal Products',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                135 =>
                    array (
                        'id' => 635,
                        'groupno' => 210,
                        'name' => 'Faculty Member, College Or University',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                136 =>
                    array (
                        'id' => 636,
                        'groupno' => 492,
                        'name' => 'Faller',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                137 =>
                    array (
                        'id' => 637,
                        'groupno' => 492,
                        'name' => 'Faller, Timber',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                138 =>
                    array (
                        'id' => 638,
                        'groupno' => 491,
                        'name' => 'Farm Laborer, General',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                139 =>
                    array (
                        'id' => 639,
                        'groupno' => 351,
                        'name' => 'Farm Machine Operator',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                140 =>
                    array (
                        'id' => 640,
                        'groupno' => 491,
                        'name' => 'Farmer, General',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                141 =>
                    array (
                        'id' => 641,
                        'groupno' => 491,
                        'name' => 'Farmworker, Fruit',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                142 =>
                    array (
                        'id' => 642,
                        'groupno' => 491,
                        'name' => 'Farmworker, Vegetable',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                143 =>
                    array (
                        'id' => 643,
                        'groupno' => 120,
                        'name' => 'Fashion Artist',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                144 =>
                    array (
                        'id' => 644,
                        'groupno' => 251,
                        'name' => 'Fashion Coordinator',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                145 =>
                    array (
                        'id' => 645,
                        'groupno' => 212,
                        'name' => 'Fashion Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                146 =>
                    array (
                        'id' => 646,
                        'groupno' => 322,
                        'name' => 'Fast Foods Worker',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                147 =>
                    array (
                        'id' => 647,
                        'groupno' => 460,
                        'name' => 'Feeder',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                148 =>
                    array (
                        'id' => 648,
                        'groupno' => 331,
                        'name' => 'Felting Machine Operator',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                149 =>
                    array (
                        'id' => 649,
                        'groupno' => 481,
                        'name' => 'Fence Erector',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                150 =>
                    array (
                        'id' => 650,
                        'groupno' => 330,
                        'name' => 'Fiberglass Laminator',
                        'industry' => 'Ship-Boat Manufacturing; Vehicles Nec.',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                151 =>
                    array (
                        'id' => 651,
                        'groupno' => 330,
                        'name' => 'Fiberglass Machine Operator',
                        'industry' => 'Glass Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                152 =>
                    array (
                        'id' => 652,
                        'groupno' => 213,
                        'name' => 'Field Engineer',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                153 =>
                    array (
                        'id' => 653,
                        'groupno' => 214,
                        'name' => 'File Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                154 =>
                    array (
                        'id' => 654,
                        'groupno' => 221,
                        'name' => 'Filler',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                155 =>
                    array (
                        'id' => 655,
                        'groupno' => 230,
                        'name' => 'Film Developer',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                156 =>
                    array (
                        'id' => 656,
                        'groupno' => 214,
                        'name' => 'Film Or Tape Librarian',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                157 =>
                    array (
                        'id' => 657,
                        'groupno' => 230,
                        'name' => 'Film Or Videotape Editor',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                158 =>
                    array (
                        'id' => 658,
                        'groupno' => 230,
                        'name' => 'Film Printer',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                159 =>
                    array (
                        'id' => 659,
                        'groupno' => 331,
                        'name' => 'Filter Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',

                    ),
                160 =>
                    array (
                        'id' => 660,
                        'groupno' => 460,
                        'name' => 'Filter Press Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                161 =>
                    array (
                        'id' => 661,
                        'groupno' => 320,
                        'name' => 'Final Assembler',
                        'industry' => 'Office Machines',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                162 =>
                    array (
                        'id' => 662,
                        'groupno' => 110,
                        'name' => 'Financial Aids Officer',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                163 =>
                    array (
                        'id' => 663,
                        'groupno' => 110,
                        'name' => 'Financial Planner',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                164 =>
                    array (
                        'id' => 664,
                        'groupno' => 120,
                        'name' => 'Fingernail Former',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                165 =>
                    array (
                        'id' => 665,
                        'groupno' => 320,
                        'name' => 'Fire Extinguisher Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                166 =>
                    array (
                        'id' => 666,
                        'groupno' => 490,
                        'name' => 'Fire Fighter',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                167 =>
                    array (
                        'id' => 667,
                        'groupno' => 490,
                        'name' => 'Fire Inspector',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                168 =>
                    array (
                        'id' => 668,
                        'groupno' => 490,
                        'name' => 'Fire Lookout',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                169 =>
                    array (
                        'id' => 669,
                        'groupno' => 490,
                        'name' => 'Fire Ranger',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                170 =>
                    array (
                        'id' => 671,
                        'groupno' => 320,
                        'name' => 'Firesetter',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                171 =>
                    array (
                        'id' => 672,
                        'groupno' => 360,
                        'name' => 'Fireworks Display Specialist',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                172 =>
                    array (
                        'id' => 673,
                        'groupno' => 490,
                        'name' => 'Fish And Game Warden',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                173 =>
                    array (
                        'id' => 674,
                        'groupno' => 322,
                        'name' => 'Fish Cleaner',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                174 =>
                    array (
                        'id' => 675,
                        'groupno' => 491,
                        'name' => 'Fish Farmer',
                        'industry' => 'Fishing And Hunting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                175 =>
                    array (
                        'id' => 676,
                        'groupno' => 491,
                        'name' => 'Fish Hatchery Laborer',
                        'industry' => 'Fishing And Hunting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                176 =>
                    array (
                        'id' => 677,
                        'groupno' => 492,
                        'name' => 'Fisher, Diving',
                        'industry' => 'Fishing And Hunting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                177 =>
                    array (
                        'id' => 678,
                        'groupno' => 491,
                        'name' => 'Fisher, Line F',
                        'industry' => 'Fishing And Hunting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                178 =>
                    array (
                        'id' => 679,
                        'groupno' => 491,
                        'name' => 'Fisher, Net',
                        'industry' => 'Fishing And Hunting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                179 =>
                    array (
                        'id' => 680,
                        'groupno' => 481,
                        'name' => 'Fitter',
                        'industry' => 'Construction, Pipe Lines',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                180 =>
                    array (
                        'id' => 681,
                        'groupno' => 430,
                        'name' => 'Fitter, Metal',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                181 =>
                    array (
                        'id' => 682,
                        'groupno' => 320,
                        'name' => 'Fixture Repairer-Fabricator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                182 =>
                    array (
                        'id' => 683,
                        'groupno' => 213,
                        'name' => 'Flagger, Traffic Control',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                183 =>
                    array (
                        'id' => 684,
                        'groupno' => 230,
                        'name' => 'Flatwork Finisher',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                184 =>
                    array (
                        'id' => 685,
                        'groupno' => 322,
                        'name' => 'Flight Attendant',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                185 =>
                    array (
                        'id' => 686,
                        'groupno' => 212,
                        'name' => 'Flight Engineer',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                186 =>
                    array (
                        'id' => 687,
                        'groupno' => 211,
                        'name' => 'Flight Information Expediter',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                187 =>
                    array (
                        'id' => 688,
                        'groupno' => 480,
                        'name' => 'Floor Finisher Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                188 =>
                    array (
                        'id' => 689,
                        'groupno' => 380,
                        'name' => 'Floor Layer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                189 =>
                    array (
                        'id' => 690,
                        'groupno' => 221,
                        'name' => 'Florist',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                190 =>
                    array (
                        'id' => 691,
                        'groupno' => 460,
                        'name' => 'Flour Blender',
                        'industry' => 'Grain-Feed Mills',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                191 =>
                    array (
                        'id' => 692,
                        'groupno' => 230,
                        'name' => 'Folder Seamer, Automatic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                192 =>
                    array (
                        'id' => 693,
                        'groupno' => 230,
                        'name' => 'Folding Machine Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                193 =>
                    array (
                        'id' => 694,
                        'groupno' => 330,
                        'name' => 'Folding Machine Operator',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                194 =>
                    array (
                        'id' => 695,
                        'groupno' => 330,
                        'name' => 'Folding Machine Operator T',
                        'industry' => 'Extile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                195 =>
                    array (
                        'id' => 696,
                        'groupno' => 322,
                        'name' => 'Food Assembler, Kitchen',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                196 =>
                    array (
                        'id' => 697,
                        'groupno' => 490,
                        'name' => 'Forest Fire Fighter',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                197 =>
                    array (
                        'id' => 698,
                        'groupno' => 492,
                        'name' => 'Forest Worker',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                198 =>
                    array (
                        'id' => 699,
                        'groupno' => 213,
                        'name' => 'Forester',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                199 =>
                    array (
                        'id' => 700,
                        'groupno' => 491,
                        'name' => 'Forester Aide',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                200 =>
                    array (
                        'id' => 701,
                        'groupno' => 460,
                        'name' => 'Forge Helper',
                        'industry' => 'Forging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                201 =>
                    array (
                        'id' => 702,
                        'groupno' => 430,
                        'name' => 'Forging Press Operator',
                        'industry' => 'Forging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                202 =>
                    array (
                        'id' => 703,
                        'groupno' => 351,
                        'name' => 'Forklift Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                203 =>
                    array (
                        'id' => 704,
                        'groupno' => 481,
                        'name' => 'Form Builder',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                204 =>
                    array (
                        'id' => 705,
                        'groupno' => 480,
                        'name' => 'Form Stripper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                205 =>
                    array (
                        'id' => 706,
                        'groupno' => 480,
                        'name' => 'Form Tamper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                206 =>
                    array (
                        'id' => 707,
                        'groupno' => 480,
                        'name' => 'Form Tamper Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                207 =>
                    array (
                        'id' => 708,
                        'groupno' => 320,
                        'name' => 'Former, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                208 =>
                    array (
                        'id' => 709,
                        'groupno' => 331,
                        'name' => 'Forming Machine Operator',
                        'industry' => 'Glass Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                209 =>
                    array (
                        'id' => 710,
                        'groupno' => 111,
                        'name' => 'Forms Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                210 =>
                    array (
                        'id' => 711,
                        'groupno' => 331,
                        'name' => 'Fourdrinier Machine Operator',
                        'industry' => 'Paper And Pulp',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                211 =>
                    array (
                        'id' => 712,
                        'groupno' => 470,
                        'name' => 'Frame Repairer',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                212 =>
                    array (
                        'id' => 713,
                        'groupno' => 370,
                        'name' => 'Frame Straightener',
                        'industry' => 'Motor-Bicycles',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                213 =>
                    array (
                        'id' => 714,
                        'groupno' => 230,
                        'name' => 'Freezer Operator',
                        'industry' => 'Dairy Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                214 =>
                    array (
                        'id' => 715,
                        'groupno' => 360,
                        'name' => 'Fruit Buying Inspector',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                215 =>
                    array (
                        'id' => 716,
                        'groupno' => 331,
                        'name' => 'Fruit Grader Operator',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                216 =>
                    array (
                        'id' => 717,
                        'groupno' => 491,
                        'name' => 'Fruit Picker',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                217 =>
                    array (
                        'id' => 718,
                        'groupno' => 332,
                        'name' => 'Fuel Attendant, Plant',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                218 =>
                    array (
                        'id' => 719,
                        'groupno' => 480,
                        'name' => 'Fumigator',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                219 =>
                    array (
                        'id' => 720,
                        'groupno' => 212,
                        'name' => 'Fund Raiser',
                        'industry' => 'Nonprofit Organization',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                220 =>
                    array (
                        'id' => 721,
                        'groupno' => 340,
                        'name' => 'Funeral Attendant',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                221 =>
                    array (
                        'id' => 722,
                        'groupno' => 560,
                        'name' => 'Funeral Car Chauffeur',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                222 =>
                    array (
                        'id' => 723,
                        'groupno' => 212,
                        'name' => 'Funeral Director',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                223 =>
                    array (
                        'id' => 724,
                        'groupno' => 341,
                        'name' => 'Furnace Cleaner',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                224 =>
                    array (
                        'id' => 725,
                        'groupno' => 380,
                        'name' => 'Furnace Installer And Repairer, Hot Air',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                225 =>
                    array (
                        'id' => 726,
                        'groupno' => 321,
                        'name' => 'Furniture Assembler',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                226 =>
                    array (
                        'id' => 727,
                        'groupno' => 470,
                        'name' => 'Furniture Assembler/Heavy',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                227 =>
                    array (
                        'id' => 728,
                        'groupno' => 360,
                        'name' => 'Furniture Crater',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                228 =>
                    array (
                        'id' => 729,
                        'groupno' => 221,
                        'name' => 'Furniture Finisher',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                229 =>
                    array (
                        'id' => 730,
                        'groupno' => 560,
                        'name' => 'Furniture Mover',
                        'industry' => 'Motor Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                230 =>
                    array (
                        'id' => 731,
                        'groupno' => 321,
                        'name' => 'Furniture Upholsterer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                231 =>
                    array (
                        'id' => 732,
                        'groupno' => 221,
                        'name' => 'Furrier',
                        'industry' => 'Fur Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                232 =>
                    array (
                        'id' => 733,
                        'groupno' => 370,
                        'name' => 'Garage Servicer, Transportation Equipment',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                233 =>
                    array (
                        'id' => 734,
                        'groupno' => 560,
                        'name' => 'Garbage Collector, Manual',
                        'industry' => 'Motor Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                234 =>
                    array (
                        'id' => 735,
                        'groupno' => 491,
                        'name' => 'Gardener',
                        'industry' => 'Domestic Services; Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                235 =>
                    array (
                        'id' => 736,
                        'groupno' => 221,
                        'name' => 'Garment Cutter, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                236 =>
                    array (
                        'id' => 737,
                        'groupno' => 321,
                        'name' => 'Garment Cutter, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                237 =>
                    array (
                        'id' => 738,
                        'groupno' => 332,
                        'name' => 'Gas Compressor Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                238 =>
                    array (
                        'id' => 739,
                        'groupno' => 332,
                        'name' => 'Gas Engine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                239 =>
                    array (
                        'id' => 740,
                        'groupno' => 320,
                        'name' => 'Gas Meter Adjuster',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                240 =>
                    array (
                        'id' => 741,
                        'groupno' => 212,
                        'name' => 'Gate Agent',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                241 =>
                    array (
                        'id' => 742,
                        'groupno' => 213,
                        'name' => 'Geologist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                242 =>
                    array (
                        'id' => 743,
                        'groupno' => 221,
                        'name' => 'Gift Wrapper',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                243 =>
                    array (
                        'id' => 744,
                        'groupno' => 221,
                        'name' => 'Gilder, Metal Leaf',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                244 =>
                    array (
                        'id' => 745,
                        'groupno' => 230,
                        'name' => 'Ginner',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                245 =>
                    array (
                        'id' => 746,
                        'groupno' => 221,
                        'name' => 'Glass Blower, Hand',
                        'industry' => 'Glass Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                246 =>
                    array (
                        'id' => 747,
                        'groupno' => 420,
                        'name' => 'Glass Cutter',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                247 =>
                    array (
                        'id' => 748,
                        'groupno' => 221,
                        'name' => 'Glass Finisher',
                        'industry' => 'Glass Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                248 =>
                    array (
                        'id' => 749,
                        'groupno' => 370,
                        'name' => 'Glass Installer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                249 =>
                    array (
                        'id' => 750,
                        'groupno' => 370,
                        'name' => 'Glass Installer',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                250 =>
                    array (
                        'id' => 751,
                        'groupno' => 321,
                        'name' => 'Glass Polisher',
                        'industry' => 'Glass Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                251 =>
                    array (
                        'id' => 752,
                        'groupno' => 380,
                        'name' => 'Glazier',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                252 =>
                    array (
                        'id' => 753,
                        'groupno' => 330,
                        'name' => 'Gluer',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                253 =>
                    array (
                        'id' => 754,
                        'groupno' => 251,
                        'name' => 'Golf Course Ranger',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                254 =>
                    array (
                        'id' => 755,
                        'groupno' => 390,
                        'name' => 'Golf Instructor',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                255 =>
                    array (
                        'id' => 756,
                        'groupno' => 340,
                        'name' => 'Golf Range Attendant',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                256 =>
                    array (
                        'id' => 757,
                        'groupno' => 493,
                        'name' => 'Golfer, Professional',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                257 =>
                    array (
                        'id' => 758,
                        'groupno' => 360,
                        'name' => 'Grainer, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                258 =>
                    array (
                        'id' => 759,
                        'groupno' => 110,
                        'name' => 'Grant Coordinator',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                259 =>
                    array (
                        'id' => 760,
                        'groupno' => 230,
                        'name' => 'Granulator Operator',
                        'industry' => 'Sugar And Confectionery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                260 =>
                    array (
                        'id' => 761,
                        'groupno' => 120,
                        'name' => 'Graphic Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                261 =>
                    array (
                        'id' => 762,
                        'groupno' => 480,
                        'name' => 'Grave Digger',
                        'industry' => 'Real Estate',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                262 =>
                    array (
                        'id' => 763,
                        'groupno' => 340,
                        'name' => 'Greaser',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                263 =>
                    array (
                        'id' => 764,
                        'groupno' => 460,
                        'name' => 'Green Chain Offbearer',
                        'industry' => 'Millwork-Plywood',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                264 =>
                    array (
                        'id' => 765,
                        'groupno' => 331,
                        'name' => 'Grinder Operator',
                        'industry' => 'Grain-Feed Mills',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                265 =>
                    array (
                        'id' => 766,
                        'groupno' => 320,
                        'name' => 'Grinder Operator, Precision',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                266 =>
                    array (
                        'id' => 767,
                        'groupno' => 330,
                        'name' => 'Grinder Set-Up Operator, Centerless',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                267 =>
                    array (
                        'id' => 768,
                        'groupno' => 460,
                        'name' => 'Grinder-Chipper, Rough',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                268 =>
                    array (
                        'id' => 769,
                        'groupno' => 330,
                        'name' => 'Grinder, Bench',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                269 =>
                    array (
                        'id' => 770,
                        'groupno' => 321,
                        'name' => 'Grinder, Disk, Belt Or Wheel',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                270 =>
                    array (
                        'id' => 771,
                        'groupno' => 330,
                        'name' => 'Grinder, Tool',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                271 =>
                    array (
                        'id' => 772,
                        'groupno' => 330,
                        'name' => 'Grinding Machine Tender',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                272 =>
                    array (
                        'id' => 773,
                        'groupno' => 482,
                        'name' => 'Grip',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                273 =>
                    array (
                        'id' => 774,
                        'groupno' => 482,
                        'name' => 'Grip, Property Handler',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                274 =>
                    array (
                        'id' => 775,
                        'groupno' => 482,
                        'name' => 'Grip, Stage Construction',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                275 =>
                    array (
                        'id' => 776,
                        'groupno' => 214,
                        'name' => 'Grocery Checker',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                276 =>
                    array (
                        'id' => 777,
                        'groupno' => 230,
                        'name' => 'Grommet Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                277 =>
                    array (
                        'id' => 778,
                        'groupno' => 491,
                        'name' => 'Groom',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                278 =>
                    array (
                        'id' => 779,
                        'groupno' => 491,
                        'name' => 'Groundskeeper',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                279 =>
                    array (
                        'id' => 780,
                        'groupno' => 490,
                        'name' => 'Group Supervisor',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                280 =>
                    array (
                        'id' => 781,
                        'groupno' => 490,
                        'name' => 'Guard, Correctional Facility',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                281 =>
                    array (
                        'id' => 782,
                        'groupno' => 240,
                        'name' => 'Guard, School-Crossing',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                282 =>
                    array (
                        'id' => 783,
                        'groupno' => 590,
                        'name' => 'Guide, Alpine',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                283 =>
                    array (
                        'id' => 784,
                        'groupno' => 213,
                        'name' => 'Guide, Establishment',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                284 =>
                    array (
                        'id' => 785,
                        'groupno' => 491,
                        'name' => 'Guide, Hunting And Fishing',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                285 =>
                    array (
                        'id' => 786,
                        'groupno' => 220,
                        'name' => 'Gunsmith',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                286 =>
                    array (
                        'id' => 787,
                        'groupno' => 290,
                        'name' => 'Hair Stylist',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                287 =>
                    array (
                        'id' => 788,
                        'groupno' => 211,
                        'name' => 'Hand Labeler',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                288 =>
                    array (
                        'id' => 789,
                        'groupno' => 380,
                        'name' => 'Handyperson',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                289 =>
                    array (
                        'id' => 790,
                        'groupno' => 110,
                        'name' => 'Harbor Master',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                290 =>
                    array (
                        'id' => 791,
                        'groupno' => 380,
                        'name' => 'Hardwood Floor Layer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                291 =>
                    array (
                        'id' => 792,
                        'groupno' => 320,
                        'name' => 'Harness Maker',
                        'industry' => 'Leather Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                292 =>
                    array (
                        'id' => 793,
                        'groupno' => 230,
                        'name' => 'Hat And Cap Sewer',
                        'industry' => 'Hat And Cap',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                293 =>
                    array (
                        'id' => 794,
                        'groupno' => 110,
                        'name' => 'Hazardous Waste Management Specialist',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                294 =>
                    array (
                        'id' => 795,
                        'groupno' => 110,
                        'name' => 'Hearing Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                295 =>
                    array (
                        'id' => 796,
                        'groupno' => 112,
                        'name' => 'Hearing Reporter',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                296 =>
                    array (
                        'id' => 797,
                        'groupno' => 330,
                        'name' => 'Heat Treater',
                        'industry' => 'Heat Treating',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                297 =>
                    array (
                        'id' => 798,
                        'groupno' => 430,
                        'name' => 'Heater',
                        'industry' => 'Forging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                298 =>
                    array (
                        'id' => 799,
                        'groupno' => 380,
                        'name' => 'Heating And Air Conditioning Installer-Servicer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                299 =>
                    array (
                        'id' => 800,
                        'groupno' => 230,
                        'name' => 'Hemmer, Automatic',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                300 =>
                    array (
                        'id' => 801,
                        'groupno' => 420,
                        'name' => 'Hide Puller',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                301 =>
                    array (
                        'id' => 802,
                        'groupno' => 480,
                        'name' => 'Hod Carrier',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                302 =>
                    array (
                        'id' => 803,
                        'groupno' => 351,
                        'name' => 'Hoisting Engineer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                303 =>
                    array (
                        'id' => 804,
                        'groupno' => 111,
                        'name' => 'Holter Scanning Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                304 =>
                    array (
                        'id' => 805,
                        'groupno' => 340,
                        'name' => 'Home Attendant',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                305 =>
                    array (
                        'id' => 806,
                        'groupno' => 491,
                        'name' => 'Horseshoer',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                306 =>
                    array (
                        'id' => 807,
                        'groupno' => 213,
                        'name' => 'Horticulturist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                307 =>
                    array (
                        'id' => 808,
                        'groupno' => 111,
                        'name' => 'Hospital Admitting Clerk',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                308 =>
                    array (
                        'id' => 809,
                        'groupno' => 240,
                        'name' => 'Host/Hostess',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                309 =>
                    array (
                        'id' => 810,
                        'groupno' => 211,
                        'name' => 'Hotel Clerk',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                310 =>
                    array (
                        'id' => 811,
                        'groupno' => 470,
                        'name' => 'Household Appliance Installer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                311 =>
                    array (
                        'id' => 812,
                        'groupno' => 340,
                        'name' => 'Housekeeper, Domestic',
                        'industry' => 'Domestic Services; Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                312 =>
                    array (
                        'id' => 813,
                        'groupno' => 332,
                        'name' => 'Hydroelectric Station Operator',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                313 =>
                    array (
                        'id' => 814,
                        'groupno' => 331,
                        'name' => 'Ice Cream Maker',
                        'industry' => 'Dairy Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                314 =>
                    array (
                        'id' => 815,
                        'groupno' => 460,
                        'name' => 'Ice Cutter',
                        'industry' => 'Food Preparation, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                315 =>
                    array (
                        'id' => 816,
                        'groupno' => 120,
                        'name' => 'Illustrator',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                316 =>
                    array (
                        'id' => 817,
                        'groupno' => 110,
                        'name' => 'Import-Export Agent',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                317 =>
                    array (
                        'id' => 818,
                        'groupno' => 111,
                        'name' => 'Industrial Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                318 =>
                    array (
                        'id' => 819,
                        'groupno' => 213,
                        'name' => 'Industrial Hygienist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                319 =>
                    array (
                        'id' => 820,
                        'groupno' => 111,
                        'name' => 'Information And Referral Aide',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                320 =>
                    array (
                        'id' => 821,
                        'groupno' => 111,
                        'name' => 'Information Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                321 =>
                    array (
                        'id' => 822,
                        'groupno' => 230,
                        'name' => 'Injection Molding Machine Tender',
                        'industry' => 'Plastic Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                322 =>
                    array (
                        'id' => 823,
                        'groupno' => 230,
                        'name' => 'Injection Wax Molder',
                        'industry' => 'Foundry; Jewelrysilver.',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                323 =>
                    array (
                        'id' => 824,
                        'groupno' => 330,
                        'name' => 'Inker',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                324 =>
                    array (
                        'id' => 825,
                        'groupno' => 460,
                        'name' => 'Inmate, Laborer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                325 =>
                    array (
                        'id' => 826,
                        'groupno' => 120,
                        'name' => 'Inspector',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                326 =>
                    array (
                        'id' => 827,
                        'groupno' => 221,
                        'name' => 'Inspector',
                        'industry' => 'Plastic Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                327 =>
                    array (
                        'id' => 828,
                        'groupno' => 221,
                        'name' => 'Inspector',
                        'industry' => 'Pharmaceutical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                328 =>
                    array (
                        'id' => 829,
                        'groupno' => 213,
                        'name' => 'Inspector, Agricultural Commodities',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                329 =>
                    array (
                        'id' => 830,
                        'groupno' => 213,
                        'name' => 'Inspector, Air Carrier',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                330 =>
                    array (
                        'id' => 831,
                        'groupno' => 213,
                        'name' => 'Inspector, Airplane',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                331 =>
                    array (
                        'id' => 832,
                        'groupno' => 221,
                        'name' => 'Inspector, Canned Food Reconditioning',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                332 =>
                    array (
                        'id' => 833,
                        'groupno' => 320,
                        'name' => 'Inspector, Eddy Current',
                        'industry' => 'Steel And Relay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                333 =>
                    array (
                        'id' => 834,
                        'groupno' => 221,
                        'name' => 'Inspector, Electronics',
                        'industry' => 'Communication Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                334 =>
                    array (
                        'id' => 835,
                        'groupno' => 221,
                        'name' => 'Inspector, Fabric',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                335 =>
                    array (
                        'id' => 836,
                        'groupno' => 251,
                        'name' => 'Inspector, Food And Drug',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                336 =>
                    array (
                        'id' => 837,
                        'groupno' => 321,
                        'name' => 'Inspector, Furniture',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                337 =>
                    array (
                        'id' => 838,
                        'groupno' => 221,
                        'name' => 'Inspector, Garment',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                338 =>
                    array (
                        'id' => 839,
                        'groupno' => 221,
                        'name' => 'Inspector, Glass',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                339 =>
                    array (
                        'id' => 840,
                        'groupno' => 251,
                        'name' => 'Inspector, Health Care Facilities',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                340 =>
                    array (
                        'id' => 841,
                        'groupno' => 120,
                        'name' => 'Inspector, Jewel',
                        'industry' => 'Clock And Watch',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',

                    ),
                341 =>
                    array (
                        'id' => 842,
                        'groupno' => 213,
                        'name' => 'Inspector, Metal Fabricating',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                342 =>
                    array (
                        'id' => 843,
                        'groupno' => 221,
                        'name' => 'Inspector, Metal Finish',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                343 =>
                    array (
                        'id' => 844,
                        'groupno' => 221,
                        'name' => 'Inspector, Printed Circuit Boards',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                344 =>
                    array (
                        'id' => 845,
                        'groupno' => 251,
                        'name' => 'Inspector, Quality Assurance',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                345 =>
                    array (
                        'id' => 846,
                        'groupno' => 251,
                        'name' => 'Inspector, Transportation',
                        'industry' => 'Motor Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                346 =>
                    array (
                        'id' => 847,
                        'groupno' => 213,
                        'name' => 'Inspector, Weigh Station',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                347 =>
                    array (
                        'id' => 848,
                        'groupno' => 493,
                        'name' => 'Instructor, Aerobics',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                348 =>
                    array (
                        'id' => 849,
                        'groupno' => 251,
                        'name' => 'Instructor, Driving',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                349 =>
                    array (
                        'id' => 850,
                        'groupno' => 390,
                        'name' => 'Instructor, Physical Education',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                350 =>
                    array (
                        'id' => 851,
                        'groupno' => 390,
                        'name' => 'Instructor, Sports',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                351 =>
                    array (
                        'id' => 852,
                        'groupno' => 214,
                        'name' => 'Instructor, Vocational Training',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                352 =>
                    array (
                        'id' => 853,
                        'groupno' => 220,
                        'name' => 'Instrument Maker And Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                353 =>
                    array (
                        'id' => 854,
                        'groupno' => 320,
                        'name' => 'Instrument Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                354 =>
                    array (
                        'id' => 855,
                        'groupno' => 380,
                        'name' => 'Insulation Worker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                355 =>
                    array (
                        'id' => 856,
                        'groupno' => 221,
                        'name' => 'Integrated Circuit Fabricator',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                356 =>
                    array (
                        'id' => 857,
                        'groupno' => 120,
                        'name' => 'Integrated Circuit Layout Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                357 =>
                    array (
                        'id' => 858,
                        'groupno' => 214,
                        'name' => 'Interior Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                358 =>
                    array (
                        'id' => 859,
                        'groupno' => 220,
                        'name' => 'Internist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                359 =>
                    array (
                        'id' => 860,
                        'groupno' => 210,
                        'name' => 'Interpreter',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                360 =>
                    array (
                        'id' => 861,
                        'groupno' => 212,
                        'name' => 'Interpreter, Deaf',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                361 =>
                    array (
                        'id' => 862,
                        'groupno' => 111,
                        'name' => 'Interviewer, Employment',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                362 =>
                    array (
                        'id' => 863,
                        'groupno' => 212,
                        'name' => 'Interviewer/Survey Worker',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                363 =>
                    array (
                        'id' => 864,
                        'groupno' => 360,
                        'name' => 'Inventory Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                364 =>
                    array (
                        'id' => 865,
                        'groupno' => 251,
                        'name' => 'Investigator',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                365 =>
                    array (
                        'id' => 866,
                        'groupno' => 111,
                        'name' => 'Investigator, Credit Fraud',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                366 =>
                    array (
                        'id' => 867,
                        'groupno' => 251,
                        'name' => 'Investigator, Inside/Outside',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                367 =>
                    array (
                        'id' => 868,
                        'groupno' => 490,
                        'name' => 'Investigator, Vice',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                368 =>
                    array (
                        'id' => 869,
                        'groupno' => 110,
                        'name' => 'Investment Analyst',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                369 =>
                    array (
                        'id' => 870,
                        'groupno' => 111,
                        'name' => 'Invoice Control Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                370 =>
                    array (
                        'id' => 871,
                        'groupno' => 491,
                        'name' => 'Irrigator, Gravity Flow',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                371 =>
                    array (
                        'id' => 872,
                        'groupno' => 491,
                        'name' => 'Irrigator, Sprinkling System',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                372 =>
                    array (
                        'id' => 873,
                        'groupno' => 480,
                        'name' => 'Jackhammer Operator',
                        'industry' => 'Mine And Quarry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                373 =>
                    array (
                        'id' => 874,
                        'groupno' => 490,
                        'name' => 'Jailer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                374 =>
                    array (
                        'id' => 875,
                        'groupno' => 340,
                        'name' => 'Janitor',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                375 =>
                    array (
                        'id' => 876,
                        'groupno' => 120,
                        'name' => 'Jeweler',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                376 =>
                    array (
                        'id' => 877,
                        'groupno' => 320,
                        'name' => 'Jig Maker',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                377 =>
                    array (
                        'id' => 878,
                        'groupno' => 330,
                        'name' => 'Jig-Boring Machine Operator, Numerical Control',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                378 =>
                    array (
                        'id' => 879,
                        'groupno' => 330,
                        'name' => 'Jigsaw Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                379 =>
                    array (
                        'id' => 880,
                        'groupno' => 212,
                        'name' => 'Job Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                380 =>
                    array (
                        'id' => 881,
                        'groupno' => 110,
                        'name' => 'Job Development Specialist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',

                    ),
                381 =>
                    array (
                        'id' => 882,
                        'groupno' => 320,
                        'name' => 'Job Setter, Honing',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                382 =>
                    array (
                        'id' => 883,
                        'groupno' => 590,
                        'name' => 'Jockey',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                383 =>
                    array (
                        'id' => 884,
                        'groupno' => 380,
                        'name' => 'Joiner',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                384 =>
                    array (
                        'id' => 885,
                        'groupno' => 330,
                        'name' => 'Jointer Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                385 =>
                    array (
                        'id' => 886,
                        'groupno' => 110,
                        'name' => 'Judge',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                386 =>
                    array (
                        'id' => 887,
                        'groupno' => 221,
                        'name' => 'Key Cutter',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                387 =>
                    array (
                        'id' => 888,
                        'groupno' => 230,
                        'name' => 'Kick Press Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                388 =>
                    array (
                        'id' => 889,
                        'groupno' => 230,
                        'name' => 'Kiln Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                389 =>
                    array (
                        'id' => 890,
                        'groupno' => 360,
                        'name' => 'Kiln Worker',
                        'industry' => 'Pottery And Porcelain',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                390 =>
                    array (
                        'id' => 891,
                        'groupno' => 322,
                        'name' => 'Kitchen Helper',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                391 =>
                    array (
                        'id' => 892,
                        'groupno' => 330,
                        'name' => 'Knitting Machine Operator',
                        'industry' => 'Knitting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                392 =>
                    array (
                        'id' => 893,
                        'groupno' => 230,
                        'name' => 'Knitting Machine Operator, Hosiery',
                        'industry' => 'Knitting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                393 =>
                    array (
                        'id' => 894,
                        'groupno' => 492,
                        'name' => 'Knot Bumper',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                394 =>
                    array (
                        'id' => 895,
                        'groupno' => 212,
                        'name' => 'Laboratory Assistant, Blood And Plasma',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                395 =>
                    array (
                        'id' => 896,
                        'groupno' => 340,
                        'name' => 'Laboratory Equipment Cleaner',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                396 =>
                    array (
                        'id' => 897,
                        'groupno' => 220,
                        'name' => 'Laboratory Tester',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                397 =>
                    array (
                        'id' => 898,
                        'groupno' => 460,
                        'name' => 'Laborer',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                398 =>
                    array (
                        'id' => 899,
                        'groupno' => 460,
                        'name' => 'Laborer',
                        'industry' => 'Pharmaceutical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                399 =>
                    array (
                        'id' => 900,
                        'groupno' => 460,
                        'name' => 'Laborer, Chemical Processing',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                400 =>
                    array (
                        'id' => 901,
                        'groupno' => 480,
                        'name' => 'Laborer, Concrete Mixing Plant',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                401 =>
                    array (
                        'id' => 902,
                        'groupno' => 480,
                        'name' => 'Laborer, Concrete Paving',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                402 =>
                    array (
                        'id' => 903,
                        'groupno' => 480,
                        'name' => 'Laborer, Construction',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                403 =>
                    array (
                        'id' => 904,
                        'groupno' => 491,
                        'name' => 'Laborer, Farm',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                404 =>
                    array (
                        'id' => 905,
                        'groupno' => 360,
                        'name' => 'Laborer, General',
                        'industry' => 'Plastic Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                405 =>
                    array (
                        'id' => 906,
                        'groupno' => 460,
                        'name' => 'Laborer, General',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                406 =>
                    array (
                        'id' => 907,
                        'groupno' => 460,
                        'name' => 'Laborer, General',
                        'industry' => 'Non-Ferrous Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                407 =>
                    array (
                        'id' => 908,
                        'groupno' => 460,
                        'name' => 'Laborer, General',
                        'industry' => 'Steel And Relay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                408 =>
                    array (
                        'id' => 909,
                        'groupno' => 460,
                        'name' => 'Laborer, Mill',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                409 =>
                    array (
                        'id' => 910,
                        'groupno' => 460,
                        'name' => 'Laborer, Petroleum Refinery',
                        'industry' => 'Petrol Refinery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                410 =>
                    array (
                        'id' => 911,
                        'groupno' => 480,
                        'name' => 'Laborer, Road',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                411 =>
                    array (
                        'id' => 912,
                        'groupno' => 460,
                        'name' => 'Laborer, Shipyard',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                412 =>
                    array (
                        'id' => 913,
                        'groupno' => 480,
                        'name' => 'Laborer, Wrecking & Salvaging',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                413 =>
                    array (
                        'id' => 914,
                        'groupno' => 460,
                        'name' => 'Laborer, Yard',
                        'industry' => 'Paper And Pulp',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                414 =>
                    array (
                        'id' => 915,
                        'groupno' => 331,
                        'name' => 'Lacquerer',
                        'industry' => 'Plastic Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                415 =>
                    array (
                        'id' => 916,
                        'groupno' => 330,
                        'name' => 'Laminating Machine Feeder',
                        'industry' => 'Wood Production, Nec.',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                416 =>
                    array (
                        'id' => 917,
                        'groupno' => 330,
                        'name' => 'Laminating Machine Operator',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                417 =>
                    array (
                        'id' => 918,
                        'groupno' => 430,
                        'name' => 'Laminating Press Operator',
                        'industry' => 'Plastic Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                418 =>
                    array (
                        'id' => 919,
                        'groupno' => 330,
                        'name' => 'Laminator',
                        'industry' => 'Ship-Boat Manufacturing; Vehicles Nec.',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                419 =>
                    array (
                        'id' => 920,
                        'groupno' => 213,
                        'name' => 'Land Surveyor',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                420 =>
                    array (
                        'id' => 921,
                        'groupno' => 491,
                        'name' => 'Landscape Gardener',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                421 =>
                    array (
                        'id' => 922,
                        'groupno' => 230,
                        'name' => 'Laser Beam Machine Operator',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                422 =>
                    array (
                        'id' => 923,
                        'groupno' => 230,
                        'name' => 'Laser Beam Trim Operator',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                423 =>
                    array (
                        'id' => 924,
                        'groupno' => 370,
                        'name' => 'Laser Technician/Repairer',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                424 =>
                    array (
                        'id' => 925,
                        'groupno' => 330,
                        'name' => 'Lathe Operator, Numerical Control',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                425 =>
                    array (
                        'id' => 926,
                        'groupno' => 330,
                        'name' => 'Lathe Operator, Swing-Type',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                426 =>
                    array (
                        'id' => 927,
                        'groupno' => 330,
                        'name' => 'Lathe Operator, Wood-Turning',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                427 =>
                    array (
                        'id' => 928,
                        'groupno' => 460,
                        'name' => 'Lathe Spotter',
                        'industry' => 'Millwork-Plywood',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                428 =>
                    array (
                        'id' => 929,
                        'groupno' => 330,
                        'name' => 'Lathe Tender',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                429 =>
                    array (
                        'id' => 930,
                        'groupno' => 380,
                        'name' => 'Lather, Metal Or Wood',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                430 =>
                    array (
                        'id' => 931,
                        'groupno' => 340,
                        'name' => 'Launderer, Hand',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                431 =>
                    array (
                        'id' => 932,
                        'groupno' => 491,
                        'name' => 'Lawn Service Worker',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                432 =>
                    array (
                        'id' => 933,
                        'groupno' => 110,
                        'name' => 'Lawyer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                433 =>
                    array (
                        'id' => 934,
                        'groupno' => 320,
                        'name' => 'Lay-Out Maker',
                        'industry' => 'Sheet Metal; Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                434 =>
                    array (
                        'id' => 935,
                        'groupno' => 120,
                        'name' => 'Lay-Out Technician',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                435 =>
                    array (
                        'id' => 936,
                        'groupno' => 491,
                        'name' => 'Lead Pony Rider, Racetrack',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                436 =>
                    array (
                        'id' => 937,
                        'groupno' => 221,
                        'name' => 'Leather Cutter',
                        'industry' => 'Leather Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                437 =>
                    array (
                        'id' => 938,
                        'groupno' => 230,
                        'name' => 'Leather Garment Presser',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                438 =>
                    array (
                        'id' => 939,
                        'groupno' => 320,
                        'name' => 'Leather Worker',
                        'industry' => 'Leather Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                439 =>
                    array (
                        'id' => 940,
                        'groupno' => 110,
                        'name' => 'Legislative Assistant',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                440 =>
                    array (
                        'id' => 941,
                        'groupno' => 220,
                        'name' => 'Lens Examiner',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                441 =>
                    array (
                        'id' => 942,
                        'groupno' => 220,
                        'name' => 'Lens Fabricating Machine Tender',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                442 =>
                    array (
                        'id' => 943,
                        'groupno' => 230,
                        'name' => 'Lens Hardener',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                443 =>
                    array (
                        'id' => 944,
                        'groupno' => 320,
                        'name' => 'Lens Mounter, Optical',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                444 =>
                    array (
                        'id' => 945,
                        'groupno' => 220,
                        'name' => 'Lens Polisher, Hand',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                445 =>
                    array (
                        'id' => 946,
                        'groupno' => 214,
                        'name' => 'Librarian',
                        'industry' => 'Library',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                446 =>
                    array (
                        'id' => 947,
                        'groupno' => 212,
                        'name' => 'Librarian, Catalog',
                        'industry' => 'Library',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                447 =>
                    array (
                        'id' => 948,
                        'groupno' => 214,
                        'name' => 'Library Assistant',
                        'industry' => 'Library',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                448 =>
                    array (
                        'id' => 949,
                        'groupno' => 211,
                        'name' => 'License Clerk',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                449 =>
                    array (
                        'id' => 950,
                        'groupno' => 590,
                        'name' => 'Lifeguard',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                450 =>
                    array (
                        'id' => 951,
                        'groupno' => 341,
                        'name' => 'Light Fixture Servicer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                451 =>
                    array (
                        'id' => 952,
                        'groupno' => 250,
                        'name' => 'Light Rail Car Operator',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                452 =>
                    array (
                        'id' => 953,
                        'groupno' => 482,
                        'name' => 'Line Installer-Repairer',
                        'industry' => 'Telephone And Telegraph',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',

                    ),
                453 =>
                    array (
                        'id' => 954,
                        'groupno' => 341,
                        'name' => 'Line Service Attendant',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                454 =>
                    array (
                        'id' => 955,
                        'groupno' => 213,
                        'name' => 'Line Walker',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                455 =>
                    array (
                        'id' => 956,
                        'groupno' => 360,
                        'name' => 'Linen Room Clerk',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                456 =>
                    array (
                        'id' => 957,
                        'groupno' => 110,
                        'name' => 'Literary Agent',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                457 =>
                    array (
                        'id' => 958,
                        'groupno' => 491,
                        'name' => 'Livestock Yard Attendant',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                458 =>
                    array (
                        'id' => 959,
                        'groupno' => 460,
                        'name' => 'Loader/Unloader',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                459 =>
                    array (
                        'id' => 960,
                        'groupno' => 110,
                        'name' => 'Loan Officer',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                460 =>
                    array (
                        'id' => 961,
                        'groupno' => 212,
                        'name' => 'Location Manager',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                461 =>
                    array (
                        'id' => 962,
                        'groupno' => 120,
                        'name' => 'Lock Assembler',
                        'industry' => 'Cutlery-Hardware',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                462 =>
                    array (
                        'id' => 963,
                        'groupno' => 221,
                        'name' => 'Locksmith',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                463 =>
                    array (
                        'id' => 964,
                        'groupno' => 250,
                        'name' => 'Locomotive Engineer',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                464 =>
                    array (
                        'id' => 965,
                        'groupno' => 213,
                        'name' => 'Log Scaler',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                465 =>
                    array (
                        'id' => 966,
                        'groupno' => 491,
                        'name' => 'Log Sorter',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                466 =>
                    array (
                        'id' => 967,
                        'groupno' => 492,
                        'name' => 'Logger, All-Round',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                467 =>
                    array (
                        'id' => 968,
                        'groupno' => 351,
                        'name' => 'Logging Tractor Operator',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                468 =>
                    array (
                        'id' => 969,
                        'groupno' => 370,
                        'name' => 'Loom Fixer',
                        'industry' => 'Narrow Fabrics',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                469 =>
                    array (
                        'id' => 970,
                        'groupno' => 340,
                        'name' => 'Lubrication Servicer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                470 =>
                    array (
                        'id' => 971,
                        'groupno' => 320,
                        'name' => 'Luggage Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                471 =>
                    array (
                        'id' => 972,
                        'groupno' => 221,
                        'name' => 'Lumber Grader',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                472 =>
                    array (
                        'id' => 973,
                        'groupno' => 460,
                        'name' => 'Lumber Handler/Stacker',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                473 =>
                    array (
                        'id' => 974,
                        'groupno' => 360,
                        'name' => 'Lumber Sorter',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                474 =>
                    array (
                        'id' => 975,
                        'groupno' => 350,
                        'name' => 'Lunch Truck Driver',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                475 =>
                    array (
                        'id' => 976,
                        'groupno' => 370,
                        'name' => 'Machine Assembler/Builder',
                        'industry' => 'Machinery Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                476 =>
                    array (
                        'id' => 977,
                        'groupno' => 360,
                        'name' => 'Machine Feeder',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                477 =>
                    array (
                        'id' => 978,
                        'groupno' => 460,
                        'name' => 'Machine Feeder, Raw Stock',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                478 =>
                    array (
                        'id' => 979,
                        'groupno' => 330,
                        'name' => 'Machine Molder',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                479 =>
                    array (
                        'id' => 980,
                        'groupno' => 230,
                        'name' => 'Machine Operator, Roofing Materials',
                        'industry' => 'Building Material., Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                480 =>
                    array (
                        'id' => 981,
                        'groupno' => 320,
                        'name' => 'Machine Set-Up Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                481 =>
                    array (
                        'id' => 982,
                        'groupno' => 221,
                        'name' => 'Machine Tester',
                        'industry' => 'Office Machines',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                482 =>
                    array (
                        'id' => 983,
                        'groupno' => 320,
                        'name' => 'Machinist',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                483 =>
                    array (
                        'id' => 984,
                        'groupno' => 320,
                        'name' => 'Machinist, Automotive',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                484 =>
                    array (
                        'id' => 985,
                        'groupno' => 370,
                        'name' => 'Machinist, Bench',
                        'industry' => 'Machinery Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                485 =>
                    array (
                        'id' => 986,
                        'groupno' => 112,
                        'name' => 'Magnetic Tape Composer Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                486 =>
                    array (
                        'id' => 987,
                        'groupno' => 211,
                        'name' => 'Mail Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                487 =>
                    array (
                        'id' => 988,
                        'groupno' => 230,
                        'name' => 'Mailing Machine Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                488 =>
                    array (
                        'id' => 989,
                        'groupno' => 370,
                        'name' => 'Maintenance Machinist',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                489 =>
                    array (
                        'id' => 990,
                        'groupno' => 470,
                        'name' => 'Maintenance Mechanic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                490 =>
                    array (
                        'id' => 991,
                        'groupno' => 380,
                        'name' => 'Maintenance Repairer, Building',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                491 =>
                    array (
                        'id' => 992,
                        'groupno' => 470,
                        'name' => 'Maintenance Repairer, Indus. Machines & Plants',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                492 =>
                    array (
                        'id' => 993,
                        'groupno' => 480,
                        'name' => 'Maintenance Worker, Municipal',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                493 =>
                    array (
                        'id' => 994,
                        'groupno' => 311,
                        'name' => 'Make-Up Artist, Body',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                494 =>
                    array (
                        'id' => 995,
                        'groupno' => 110,
                        'name' => 'Management Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                495 =>
                    array (
                        'id' => 996,
                        'groupno' => 212,
                        'name' => 'Management Trainee',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                496 =>
                    array (
                        'id' => 997,
                        'groupno' => 212,
                        'name' => 'Manager, Advertising Agency',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                497 =>
                    array (
                        'id' => 998,
                        'groupno' => 212,
                        'name' => 'Manager, Apartment House',
                        'industry' => 'Real Estate',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                498 =>
                    array (
                        'id' => 999,
                        'groupno' => 213,
                        'name' => 'Manager, Automobile Service Station',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                499 =>
                    array (
                        'id' => 1000,
                        'groupno' => 110,
                        'name' => 'Manager, Benefits',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
            ));
            \DB::table('occupations')->insert(array (
                0 =>
                    array (
                        'id' => 1001,
                        'groupno' => 110,
                        'name' => 'Manager, Bus Transportation',
                        'industry' => 'Motor Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                1 =>
                    array (
                        'id' => 1002,
                        'groupno' => 212,
                        'name' => 'Manager, Convention',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                2 =>
                    array (
                        'id' => 1003,
                        'groupno' => 212,
                        'name' => 'Manager, Customer Services',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                3 =>
                    array (
                        'id' => 1004,
                        'groupno' => 213,
                        'name' => 'Manager, Dairy Farm',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                4 =>
                    array (
                        'id' => 1005,
                        'groupno' => 110,
                        'name' => 'Manager, Data Processing',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                5 =>
                    array (
                        'id' => 1006,
                        'groupno' => 110,
                        'name' => 'Manager, Department',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                6 =>
                    array (
                        'id' => 1007,
                        'groupno' => 212,
                        'name' => 'Manager, Fast Food Services',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                7 =>
                    array (
                        'id' => 1008,
                        'groupno' => 110,
                        'name' => 'Manager, Hotel Or Motel',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                8 =>
                    array (
                        'id' => 1009,
                        'groupno' => 212,
                        'name' => 'Manager, Hotel Recreational Facilities',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                9 =>
                    array (
                        'id' => 1010,
                        'groupno' => 212,
                        'name' => 'Manager, Labor Relations',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                10 =>
                    array (
                        'id' => 1011,
                        'groupno' => 212,
                        'name' => 'Manager, Mobile Home Park',
                        'industry' => 'Real Estate',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                11 =>
                    array (
                        'id' => 1012,
                        'groupno' => 213,
                        'name' => 'Manager, Nursery',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                12 =>
                    array (
                        'id' => 1013,
                        'groupno' => 111,
                        'name' => 'Manager, Office',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                13 =>
                    array (
                        'id' => 1014,
                        'groupno' => 212,
                        'name' => 'Manager, Parts',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                14 =>
                    array (
                        'id' => 1015,
                        'groupno' => 111,
                        'name' => 'Manager, Personnel',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                15 =>
                    array (
                        'id' => 1016,
                        'groupno' => 213,
                        'name' => 'Manager, Property',
                        'industry' => 'Real Estate',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                16 =>
                    array (
                        'id' => 1017,
                        'groupno' => 212,
                        'name' => 'Manager, Quality Control',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                17 =>
                    array (
                        'id' => 1018,
                        'groupno' => 212,
                        'name' => 'Manager, Retail Store',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                18 =>
                    array (
                        'id' => 1019,
                        'groupno' => 212,
                        'name' => 'Manager, Stage',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                19 =>
                    array (
                        'id' => 1020,
                        'groupno' => 212,
                        'name' => 'Manager, Theater',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                20 =>
                    array (
                        'id' => 1021,
                        'groupno' => 110,
                        'name' => 'Manager, Traffic',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                21 =>
                    array (
                        'id' => 1022,
                        'groupno' => 212,
                        'name' => 'Manager, Vehicle Leasing And Rental',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                22 =>
                    array (
                        'id' => 1023,
                        'groupno' => 212,
                        'name' => 'Manager, Warehouse',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                23 =>
                    array (
                        'id' => 1024,
                        'groupno' => 120,
                        'name' => 'Manicurist',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                24 =>
                    array (
                        'id' => 1025,
                        'groupno' => 330,
                        'name' => 'Marble Polisher, Machine',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                25 =>
                    array (
                        'id' => 1026,
                        'groupno' => 481,
                        'name' => 'Marble Setter',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                26 =>
                    array (
                        'id' => 1027,
                        'groupno' => 480,
                        'name' => 'Marble Setter Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                27 =>
                    array (
                        'id' => 1028,
                        'groupno' => 211,
                        'name' => 'Marker',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                28 =>
                    array (
                        'id' => 1029,
                        'groupno' => 111,
                        'name' => 'Market Research Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                29 =>
                    array (
                        'id' => 1030,
                        'groupno' => 221,
                        'name' => 'Masker, Parts',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                30 =>
                    array (
                        'id' => 1031,
                        'groupno' => 311,
                        'name' => 'Masseur/Masseuse',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                31 =>
                    array (
                        'id' => 1032,
                        'groupno' => 212,
                        'name' => 'Master Control Operator',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                32 =>
                    array (
                        'id' => 1033,
                        'groupno' => 221,
                        'name' => 'Mat Cutter, Picture Frames',
                        'industry' => 'Wood Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                33 =>
                    array (
                        'id' => 1034,
                        'groupno' => 360,
                        'name' => 'Material Expediter',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                34 =>
                    array (
                        'id' => 1035,
                        'groupno' => 460,
                        'name' => 'Material Stacker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                35 =>
                    array (
                        'id' => 1036,
                        'groupno' => 321,
                        'name' => 'Mattress Maker',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                36 =>
                    array (
                        'id' => 1037,
                        'groupno' => 322,
                        'name' => 'Meat Carver, Display',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                37 =>
                    array (
                        'id' => 1038,
                        'groupno' => 322,
                        'name' => 'Meat Clerk',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                38 =>
                    array (
                        'id' => 1039,
                        'groupno' => 322,
                        'name' => 'Meat Cutter',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                39 =>
                    array (
                        'id' => 1040,
                        'groupno' => 331,
                        'name' => 'Meat Grinder',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                40 =>
                    array (
                        'id' => 1041,
                        'groupno' => 380,
                        'name' => 'Mechanic, Aircraft',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                41 =>
                    array (
                        'id' => 1042,
                        'groupno' => 370,
                        'name' => 'Mechanic, Automobile',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                42 =>
                    array (
                        'id' => 1043,
                        'groupno' => 470,
                        'name' => 'Mechanic, Diesel',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                43 =>
                    array (
                        'id' => 1044,
                        'groupno' => 370,
                        'name' => 'Mechanic, Front-End',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                44 =>
                    array (
                        'id' => 1045,
                        'groupno' => 481,
                        'name' => 'Mechanic, Powerhouse',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                45 =>
                    array (
                        'id' => 1046,
                        'groupno' => 380,
                        'name' => 'Mechanic, Radar',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                46 =>
                    array (
                        'id' => 1047,
                        'groupno' => 370,
                        'name' => 'Mechanic, Radiator',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                47 =>
                    array (
                        'id' => 1048,
                        'groupno' => 481,
                        'name' => 'Mechanic, Refrigeration',
                        'industry' => 'Service Individual Machine',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                48 =>
                    array (
                        'id' => 1049,
                        'groupno' => 370,
                        'name' => 'Mechanic, Rocket Engine Component',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                49 =>
                    array (
                        'id' => 1050,
                        'groupno' => 470,
                        'name' => 'Mechanic, Safe And Vault',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                50 =>
                    array (
                        'id' => 1051,
                        'groupno' => 370,
                        'name' => 'Mechanic, Small Engine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                51 =>
                    array (
                        'id' => 1052,
                        'groupno' => 370,
                        'name' => 'Mechanic, Tractor',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                52 =>
                    array (
                        'id' => 1053,
                        'groupno' => 370,
                        'name' => 'Mechanic, Transmission',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                53 =>
                    array (
                        'id' => 1054,
                        'groupno' => 370,
                        'name' => 'Mechanic, Tune-Up',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                54 =>
                    array (
                        'id' => 1055,
                        'groupno' => 214,
                        'name' => 'Media Specialist, School Library',
                        'industry' => 'Library',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                55 =>
                    array (
                        'id' => 1056,
                        'groupno' => 212,
                        'name' => 'Medical Assistant, Office',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                56 =>
                    array (
                        'id' => 1057,
                        'groupno' => 470,
                        'name' => 'Medical Equipment Repairer',
                        'industry' => 'Protective Development',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                57 =>
                    array (
                        'id' => 1058,
                        'groupno' => 212,
                        'name' => 'Medical Laboratory Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                58 =>
                    array (
                        'id' => 1059,
                        'groupno' => 220,
                        'name' => 'Medical Laboratory Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                59 =>
                    array (
                        'id' => 1060,
                        'groupno' => 211,
                        'name' => 'Medical Record Clerk',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                60 =>
                    array (
                        'id' => 1061,
                        'groupno' => 321,
                        'name' => 'Melter',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                61 =>
                    array (
                        'id' => 1062,
                        'groupno' => 340,
                        'name' => 'Mental Retardation Aide, Institution',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                62 =>
                    array (
                        'id' => 1063,
                        'groupno' => 213,
                        'name' => 'Messenger, Non-Driving',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                63 =>
                    array (
                        'id' => 1064,
                        'groupno' => 331,
                        'name' => 'Metal Cleaner, Immersion',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                64 =>
                    array (
                        'id' => 1065,
                        'groupno' => 430,
                        'name' => 'Metal Fabricator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                65 =>
                    array (
                        'id' => 1066,
                        'groupno' => 321,
                        'name' => 'Metal Grinder And Finisher',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                66 =>
                    array (
                        'id' => 1067,
                        'groupno' => 321,
                        'name' => 'Metal Sprayer, Production',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                67 =>
                    array (
                        'id' => 1068,
                        'groupno' => 230,
                        'name' => 'Metallization Equipment Tender, Semiconductors',
                        'industry' => 'Communication Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                68 =>
                    array (
                        'id' => 1069,
                        'groupno' => 212,
                        'name' => 'Metallurgical Tester',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                69 =>
                    array (
                        'id' => 1070,
                        'groupno' => 213,
                        'name' => 'Meter Reader',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                70 =>
                    array (
                        'id' => 1071,
                        'groupno' => 320,
                        'name' => 'Meter Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                71 =>
                    array (
                        'id' => 1072,
                        'groupno' => 220,
                        'name' => 'Microelectronics Technician',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',

                    ),
                72 =>
                    array (
                        'id' => 1073,
                        'groupno' => 230,
                        'name' => 'Microfilm Processor',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                73 =>
                    array (
                        'id' => 1074,
                        'groupno' => 212,
                        'name' => 'Microphone Boom Operator',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                74 =>
                    array (
                        'id' => 1075,
                        'groupno' => 491,
                        'name' => 'Milker, Machine',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                75 =>
                    array (
                        'id' => 1076,
                        'groupno' => 331,
                        'name' => 'Mill Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                76 =>
                    array (
                        'id' => 1077,
                        'groupno' => 320,
                        'name' => 'Milling Machine Operator, Numerical Control',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                77 =>
                    array (
                        'id' => 1078,
                        'groupno' => 481,
                        'name' => 'Millwright',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                78 =>
                    array (
                        'id' => 1079,
                        'groupno' => 480,
                        'name' => 'Millwright Helper',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                79 =>
                    array (
                        'id' => 1080,
                        'groupno' => 213,
                        'name' => 'Mine Inspector',
                        'industry' => 'Mine And Quarry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                80 =>
                    array (
                        'id' => 1081,
                        'groupno' => 560,
                        'name' => 'Miner',
                        'industry' => 'Mine And Quarry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                81 =>
                    array (
                        'id' => 1082,
                        'groupno' => 560,
                        'name' => 'Miner Helper',
                        'industry' => 'Mine And Quarry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                82 =>
                    array (
                        'id' => 1083,
                        'groupno' => 221,
                        'name' => 'Miniature Set Constructor',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                83 =>
                    array (
                        'id' => 1084,
                        'groupno' => 460,
                        'name' => 'Mixer',
                        'industry' => 'Paint And Varnish',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                84 =>
                    array (
                        'id' => 1085,
                        'groupno' => 460,
                        'name' => 'Mixer, Clay',
                        'industry' => 'Brick And Tile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                85 =>
                    array (
                        'id' => 1086,
                        'groupno' => 480,
                        'name' => 'Mixer, Concrete',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                86 =>
                    array (
                        'id' => 1087,
                        'groupno' => 460,
                        'name' => 'Mixer, Dough',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                87 =>
                    array (
                        'id' => 1088,
                        'groupno' => 460,
                        'name' => 'Mixer, Flour',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                88 =>
                    array (
                        'id' => 1089,
                        'groupno' => 480,
                        'name' => 'Mixer, Mortar',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                89 =>
                    array (
                        'id' => 1090,
                        'groupno' => 221,
                        'name' => 'Mixer, Paint (Hand)',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                90 =>
                    array (
                        'id' => 1091,
                        'groupno' => 460,
                        'name' => 'Mixer, Paint (Machine)',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                91 =>
                    array (
                        'id' => 1092,
                        'groupno' => 331,
                        'name' => 'Mixer, Sand (Machine)',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                92 =>
                    array (
                        'id' => 1093,
                        'groupno' => 331,
                        'name' => 'Mixing Machine Operator',
                        'industry' => 'Food Preparation, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                93 =>
                    array (
                        'id' => 1094,
                        'groupno' => 460,
                        'name' => 'Mixing Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                94 =>
                    array (
                        'id' => 1095,
                        'groupno' => 380,
                        'name' => 'Mobile Home Assembler',
                        'industry' => 'Mfd. Buildings',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                95 =>
                    array (
                        'id' => 1096,
                        'groupno' => 212,
                        'name' => 'Mobile Home Park Manager',
                        'industry' => 'Real Estate',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                96 =>
                    array (
                        'id' => 1097,
                        'groupno' => 240,
                        'name' => 'Model',
                        'industry' => 'Garment',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                97 =>
                    array (
                        'id' => 1098,
                        'groupno' => 221,
                        'name' => 'Model Maker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                98 =>
                    array (
                        'id' => 1099,
                        'groupno' => 240,
                        'name' => 'Model, Artists\'',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                99 =>
                    array (
                        'id' => 1100,
                        'groupno' => 213,
                        'name' => 'Model, Photographers\'',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                100 =>
                    array (
                        'id' => 1101,
                        'groupno' => 221,
                        'name' => 'Mold And Model Maker, Plaster',
                        'industry' => 'Concrete Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                101 =>
                    array (
                        'id' => 1102,
                        'groupno' => 321,
                        'name' => 'Mold Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                102 =>
                    array (
                        'id' => 1103,
                        'groupno' => 330,
                        'name' => 'Molder',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                103 =>
                    array (
                        'id' => 1104,
                        'groupno' => 420,
                        'name' => 'Molder, Hand',
                        'industry' => 'Brick And Tile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                104 =>
                    array (
                        'id' => 1105,
                        'groupno' => 320,
                        'name' => 'Molder, Pattern',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                105 =>
                    array (
                        'id' => 1106,
                        'groupno' => 230,
                        'name' => 'Molding Machine Tender, Compression',
                        'industry' => 'Plastic Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                106 =>
                    array (
                        'id' => 1107,
                        'groupno' => 340,
                        'name' => 'Morgue Attendant',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                107 =>
                    array (
                        'id' => 1108,
                        'groupno' => 230,
                        'name' => 'Motion Picture Projectionist',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                108 =>
                    array (
                        'id' => 1109,
                        'groupno' => 351,
                        'name' => 'Motor-Grader Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                109 =>
                    array (
                        'id' => 1110,
                        'groupno' => 351,
                        'name' => 'Motorboat Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                110 =>
                    array (
                        'id' => 1111,
                        'groupno' => 370,
                        'name' => 'Motorcycle Assembler',
                        'industry' => 'Motor-Bicycles',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                111 =>
                    array (
                        'id' => 1112,
                        'groupno' => 250,
                        'name' => 'Motorcycle Driver, Delivery',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                112 =>
                    array (
                        'id' => 1113,
                        'groupno' => 490,
                        'name' => 'Motorcycle Police Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations ',
                    ),
                113 =>
                    array (
                        'id' => 1114,
                        'groupno' => 370,
                        'name' => 'Motorcycle Repairer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                114 =>
                    array (
                        'id' => 1115,
                        'groupno' => 120,
                        'name' => 'Mounter, Hand',
                        'industry' => 'Photofinishing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                115 =>
                    array (
                        'id' => 1116,
                        'groupno' => 310,
                        'name' => 'Mri Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                116 =>
                    array (
                        'id' => 1117,
                        'groupno' => 370,
                        'name' => 'Muffler Installer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                117 =>
                    array (
                        'id' => 1118,
                        'groupno' => 460,
                        'name' => 'Munitions Handler',
                        'industry' => 'Ordnance',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                118 =>
                    array (
                        'id' => 1119,
                        'groupno' => 212,
                        'name' => 'Museum Attendant & Guide',
                        'industry' => 'Museums',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                119 =>
                    array (
                        'id' => 1120,
                        'groupno' => 380,
                        'name' => 'Museum Preparator',
                        'industry' => 'Museums',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                120 =>
                    array (
                        'id' => 1121,
                        'groupno' => 220,
                        'name' => 'Musician, Instrumental',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                121 =>
                    array (
                        'id' => 1122,
                        'groupno' => 330,
                        'name' => 'Nailing Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                122 =>
                    array (
                        'id' => 1123,
                        'groupno' => 111,
                        'name' => 'Navigator',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                123 =>
                    array (
                        'id' => 1124,
                        'groupno' => 360,
                        'name' => 'News Gathering Technician',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                124 =>
                    array (
                        'id' => 1125,
                        'groupno' => 210,
                        'name' => 'Newscaster',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                125 =>
                    array (
                        'id' => 1126,
                        'groupno' => 330,
                        'name' => 'Nibbler Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                126 =>
                    array (
                        'id' => 1127,
                        'groupno' => 111,
                        'name' => 'Night Auditor',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                127 =>
                    array (
                        'id' => 1128,
                        'groupno' => 460,
                        'name' => 'Nitroglycerin Distributor',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                128 =>
                    array (
                        'id' => 1129,
                        'groupno' => 310,
                        'name' => 'Nuclear Medicine Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                129 =>
                    array (
                        'id' => 1130,
                        'groupno' => 330,
                        'name' => 'Numerical Control Machine Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                130 =>
                    array (
                        'id' => 1131,
                        'groupno' => 340,
                        'name' => 'Nurse Aide',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                131 =>
                    array (
                        'id' => 1132,
                        'groupno' => 220,
                        'name' => 'Nurse Anesthetist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                132 =>
                    array (
                        'id' => 1133,
                        'groupno' => 212,
                        'name' => 'Nurse Case Manager',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                133 =>
                    array (
                        'id' => 1134,
                        'groupno' => 311,
                        'name' => 'Nurse-Midwife',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                134 =>
                    array (
                        'id' => 1135,
                        'groupno' => 311,
                        'name' => 'Nurse, General Duty',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                135 =>
                    array (
                        'id' => 1136,
                        'groupno' => 311,
                        'name' => 'Nurse, Licensed Vocational',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                136 =>
                    array (
                        'id' => 1137,
                        'groupno' => 311,
                        'name' => 'Nurse, Private Duty',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                137 =>
                    array (
                        'id' => 1138,
                        'groupno' => 212,
                        'name' => 'Nurse, School',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                138 =>
                    array (
                        'id' => 1139,
                        'groupno' => 460,
                        'name' => 'Nut Roaster',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                139 =>
                    array (
                        'id' => 1140,
                        'groupno' => 212,
                        'name' => 'Occupational Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                140 =>
                    array (
                        'id' => 1141,
                        'groupno' => 213,
                        'name' => 'Occupational Safety And Health Inspector',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                141 =>
                    array (
                        'id' => 1142,
                        'groupno' => 311,
                        'name' => 'Occupational Therapist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                142 =>
                    array (
                        'id' => 1143,
                        'groupno' => 340,
                        'name' => 'Occupational Therapy Aide',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                143 =>
                    array (
                        'id' => 1144,
                        'groupno' => 211,
                        'name' => 'Office Clerk, General',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                144 =>
                    array (
                        'id' => 1145,
                        'groupno' => 320,
                        'name' => 'Office Machine Servicer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                145 =>
                    array (
                        'id' => 1146,
                        'groupno' => 230,
                        'name' => 'Offset Duplicating Machine Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                146 =>
                    array (
                        'id' => 1147,
                        'groupno' => 330,
                        'name' => 'Offset Press Helper',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                147 =>
                    array (
                        'id' => 1148,
                        'groupno' => 230,
                        'name' => 'Offset Press Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                148 =>
                    array (
                        'id' => 1149,
                        'groupno' => 480,
                        'name' => 'Oil Well Driller',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                149 =>
                    array (
                        'id' => 1150,
                        'groupno' => 340,
                        'name' => 'Oiler',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                150 =>
                    array (
                        'id' => 1151,
                        'groupno' => 332,
                        'name' => 'Operating Engineer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                151 =>
                    array (
                        'id' => 1152,
                        'groupno' => 332,
                        'name' => 'Operating Engineer, Refrigeration',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                152 =>
                    array (
                        'id' => 1153,
                        'groupno' => 111,
                        'name' => 'Optical Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                153 =>
                    array (
                        'id' => 1154,
                        'groupno' => 220,
                        'name' => 'Optician, Dispensing',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                154 =>
                    array (
                        'id' => 1155,
                        'groupno' => 220,
                        'name' => 'Optician, Lens Grinder',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                155 =>
                    array (
                        'id' => 1156,
                        'groupno' => 220,
                        'name' => 'Optometrist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                156 =>
                    array (
                        'id' => 1157,
                        'groupno' => 491,
                        'name' => 'Orchard Sprayer, Hand',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                157 =>
                    array (
                        'id' => 1158,
                        'groupno' => 360,
                        'name' => 'Order Checker',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                158 =>
                    array (
                        'id' => 1159,
                        'groupno' => 111,
                        'name' => 'Order Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                159 =>
                    array (
                        'id' => 1160,
                        'groupno' => 214,
                        'name' => 'Order Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                160 =>
                    array (
                        'id' => 1161,
                        'groupno' => 214,
                        'name' => 'Order Filler, Catalog Sales',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                161 =>
                    array (
                        'id' => 1162,
                        'groupno' => 460,
                        'name' => 'Orderly',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                162 =>
                    array (
                        'id' => 1163,
                        'groupno' => 481,
                        'name' => 'Ornamental Iron Worker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                163 =>
                    array (
                        'id' => 1164,
                        'groupno' => 120,
                        'name' => 'Orthodontic Technician',
                        'industry' => 'Protective Development',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                164 =>
                    array (
                        'id' => 1165,
                        'groupno' => 320,
                        'name' => 'Orthotics Technician',
                        'industry' => 'Protective Development',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                165 =>
                    array (
                        'id' => 1166,
                        'groupno' => 310,
                        'name' => 'Orthotist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                166 =>
                    array (
                        'id' => 1167,
                        'groupno' => 331,
                        'name' => 'Oven Tender',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                167 =>
                    array (
                        'id' => 1168,
                        'groupno' => 351,
                        'name' => 'Overhead Crane Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                168 =>
                    array (
                        'id' => 1169,
                        'groupno' => 331,
                        'name' => 'Oxidized Finish Plater',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                169 =>
                    array (
                        'id' => 1170,
                        'groupno' => 221,
                        'name' => 'Oxidizer',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                170 =>
                    array (
                        'id' => 1171,
                        'groupno' => 330,
                        'name' => 'Package Sealer, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                171 =>
                    array (
                        'id' => 1172,
                        'groupno' => 330,
                        'name' => 'Packager, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                172 =>
                    array (
                        'id' => 1173,
                        'groupno' => 360,
                        'name' => 'Packer, Agricultural Produce',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                173 =>
                    array (
                        'id' => 1174,
                        'groupno' => 360,
                        'name' => 'Packer, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                174 =>
                    array (
                        'id' => 1175,
                        'groupno' => 380,
                        'name' => 'Painter',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                175 =>
                    array (
                        'id' => 1176,
                        'groupno' => 480,
                        'name' => 'Painter Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                176 =>
                    array (
                        'id' => 1177,
                        'groupno' => 221,
                        'name' => 'Painter, Airbrush',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                177 =>
                    array (
                        'id' => 1178,
                        'groupno' => 482,
                        'name' => 'Painter, Bridge, Structural Steel',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                178 =>
                    array (
                        'id' => 1179,
                        'groupno' => 321,
                        'name' => 'Painter, Brush',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                179 =>
                    array (
                        'id' => 1180,
                        'groupno' => 120,
                        'name' => 'Painter, Hand, Decorative',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                180 =>
                    array (
                        'id' => 1181,
                        'groupno' => 380,
                        'name' => 'Painter, Sign',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                181 =>
                    array (
                        'id' => 1182,
                        'groupno' => 321,
                        'name' => 'Painter, Spray Gun',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                182 =>
                    array (
                        'id' => 1183,
                        'groupno' => 321,
                        'name' => 'Painter, Touch-Up',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                183 =>
                    array (
                        'id' => 1184,
                        'groupno' => 350,
                        'name' => 'Painter, Traffic Line',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                184 =>
                    array (
                        'id' => 1185,
                        'groupno' => 380,
                        'name' => 'Painter, Transportation Equipment',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                185 =>
                    array (
                        'id' => 1186,
                        'groupno' => 230,
                        'name' => 'Palletizer Operator, Automatic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                186 =>
                    array (
                        'id' => 1187,
                        'groupno' => 230,
                        'name' => 'Paper Cutter, Machine',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                187 =>
                    array (
                        'id' => 1188,
                        'groupno' => 460,
                        'name' => 'Paper-Baling Machine Tender',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                188 =>
                    array (
                        'id' => 1189,
                        'groupno' => 331,
                        'name' => 'Paper-Making Machine Operator',
                        'industry' => 'Paper And Pulp',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                189 =>
                    array (
                        'id' => 1190,
                        'groupno' => 460,
                        'name' => 'Papercutting Machine Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                190 =>
                    array (
                        'id' => 1191,
                        'groupno' => 380,
                        'name' => 'Paperhanger',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                191 =>
                    array (
                        'id' => 1192,
                        'groupno' => 321,
                        'name' => 'Parachute Rigger',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                192 =>
                    array (
                        'id' => 1193,
                        'groupno' => 211,
                        'name' => 'Paralegal',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                193 =>
                    array (
                        'id' => 1194,
                        'groupno' => 490,
                        'name' => 'Paramedic',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                194 =>
                    array (
                        'id' => 1195,
                        'groupno' => 211,
                        'name' => 'Parimutuel Ticket Seller',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                195 =>
                    array (
                        'id' => 1196,
                        'groupno' => 490,
                        'name' => 'Park Ranger',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                196 =>
                    array (
                        'id' => 1197,
                        'groupno' => 250,
                        'name' => 'Parking Enforcement Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                197 =>
                    array (
                        'id' => 1198,
                        'groupno' => 214,
                        'name' => 'Parking Lot Attendant',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                198 =>
                    array (
                        'id' => 1199,
                        'groupno' => 240,
                        'name' => 'Parking Lot Attendant, Booth',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                199 =>
                    array (
                        'id' => 1200,
                        'groupno' => 490,
                        'name' => 'Parole Officer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                200 =>
                    array (
                        'id' => 1201,
                        'groupno' => 214,
                        'name' => 'Parts Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                201 =>
                    array (
                        'id' => 1202,
                        'groupno' => 214,
                        'name' => 'Parts Order And Stock Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                202 =>
                    array (
                        'id' => 1203,
                        'groupno' => 460,
                        'name' => 'Pasteurizer',
                        'industry' => 'Dairy Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                203 =>
                    array (
                        'id' => 1204,
                        'groupno' => 250,
                        'name' => 'Patrol Officer, Volunteer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                204 =>
                    array (
                        'id' => 1205,
                        'groupno' => 230,
                        'name' => 'Pattern-Punching Machine Operator',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                205 =>
                    array (
                        'id' => 1206,
                        'groupno' => 320,
                        'name' => 'Patternmaker, All-Around',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                206 =>
                    array (
                        'id' => 1207,
                        'groupno' => 320,
                        'name' => 'Patternmaker, Metal',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                207 =>
                    array (
                        'id' => 1208,
                        'groupno' => 320,
                        'name' => 'Patternmaker, Wood',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                208 =>
                    array (
                        'id' => 1209,
                        'groupno' => 221,
                        'name' => 'Peeler, Hand',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                209 =>
                    array (
                        'id' => 1210,
                        'groupno' => 230,
                        'name' => 'Peeler, Machine',
                        'industry' => 'Canning And Preserving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                210 =>
                    array (
                        'id' => 1211,
                        'groupno' => 320,
                        'name' => 'Percussion Instrument Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                211 =>
                    array (
                        'id' => 1212,
                        'groupno' => 310,
                        'name' => 'Perfusionist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                212 =>
                    array (
                        'id' => 1213,
                        'groupno' => 390,
                        'name' => 'Personal Trainer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                213 =>
                    array (
                        'id' => 1214,
                        'groupno' => 111,
                        'name' => 'Personnel Records Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                214 =>
                    array (
                        'id' => 1215,
                        'groupno' => 111,
                        'name' => 'Personnel Recruiter',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                215 =>
                    array (
                        'id' => 1216,
                        'groupno' => 220,
                        'name' => 'Pharmacist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                216 =>
                    array (
                        'id' => 1217,
                        'groupno' => 220,
                        'name' => 'Phlebotomist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                217 =>
                    array (
                        'id' => 1218,
                        'groupno' => 211,
                        'name' => 'Photocopying Machine Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                218 =>
                    array (
                        'id' => 1219,
                        'groupno' => 221,
                        'name' => 'Photoengraver',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                219 =>
                    array (
                        'id' => 1220,
                        'groupno' => 221,
                        'name' => 'Photofinishing Laboratory Worker',
                        'industry' => 'Photofinishing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                220 =>
                    array (
                        'id' => 1221,
                        'groupno' => 213,
                        'name' => 'Photographer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                221 =>
                    array (
                        'id' => 1222,
                        'groupno' => 212,
                        'name' => 'Photographer, Still',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                222 =>
                    array (
                        'id' => 1223,
                        'groupno' => 221,
                        'name' => 'Photographic Plate Maker',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                223 =>
                    array (
                        'id' => 1224,
                        'groupno' => 213,
                        'name' => 'Photojournalist',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                224 =>
                    array (
                        'id' => 1225,
                        'groupno' => 230,
                        'name' => 'Phototypesetter Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                225 =>
                    array (
                        'id' => 1226,
                        'groupno' => 310,
                        'name' => 'Physiatrist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                226 =>
                    array (
                        'id' => 1227,
                        'groupno' => 311,
                        'name' => 'Physical Therapist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                227 =>
                    array (
                        'id' => 1228,
                        'groupno' => 340,
                        'name' => 'Physical Therapy Aide',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                228 =>
                    array (
                        'id' => 1229,
                        'groupno' => 212,
                        'name' => 'Physician Assistant',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                229 =>
                    array (
                        'id' => 1230,
                        'groupno' => 220,
                        'name' => 'Physician, General Practitioner',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                230 =>
                    array (
                        'id' => 1231,
                        'groupno' => 320,
                        'name' => 'Piano Technician',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                231 =>
                    array (
                        'id' => 1232,
                        'groupno' => 221,
                        'name' => 'Piano Tuner',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                232 =>
                    array (
                        'id' => 1233,
                        'groupno' => 491,
                        'name' => 'Picker, Fruit',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                233 =>
                    array (
                        'id' => 1234,
                        'groupno' => 330,
                        'name' => 'Picking Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                234 =>
                    array (
                        'id' => 1235,
                        'groupno' => 221,
                        'name' => 'Picture Framer',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                235 =>
                    array (
                        'id' => 1236,
                        'groupno' => 351,
                        'name' => 'Pile-Driver Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                236 =>
                    array (
                        'id' => 1237,
                        'groupno' => 370,
                        'name' => 'Pinsetter Adjuster, Automatic',
                        'industry' => 'Toy-Sport Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                237 =>
                    array (
                        'id' => 1238,
                        'groupno' => 380,
                        'name' => 'Pinsetter Mechanic, Automatic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                238 =>
                    array (
                        'id' => 1239,
                        'groupno' => 380,
                        'name' => 'Pipe Coverer And Insulator',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                239 =>
                    array (
                        'id' => 1240,
                        'groupno' => 481,
                        'name' => 'Pipe Fitter',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                240 =>
                    array (
                        'id' => 1241,
                        'groupno' => 481,
                        'name' => 'Pipe Fitter Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                241 =>
                    array (
                        'id' => 1242,
                        'groupno' => 480,
                        'name' => 'Pipe Layer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                242 =>
                    array (
                        'id' => 1243,
                        'groupno' => 480,
                        'name' => 'Pipe Layer Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                243 =>
                    array (
                        'id' => 1244,
                        'groupno' => 380,
                        'name' => 'Pipe Organ Tuner And Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                244 =>
                    array (
                        'id' => 1245,
                        'groupno' => 480,
                        'name' => 'Pipeliner',
                        'industry' => 'Pipe Lines',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                245 =>
                    array (
                        'id' => 1246,
                        'groupno' => 214,
                        'name' => 'Pit Boss/Floor Person',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Pit Boss/Floor Person',
                    ),
                246 =>
                    array (
                        'id' => 1247,
                        'groupno' => 330,
                        'name' => 'Planer Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                247 =>
                    array (
                        'id' => 1248,
                        'groupno' => 430,
                        'name' => 'Planer Operator, Metal Castings',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                248 =>
                    array (
                        'id' => 1249,
                        'groupno' => 212,
                        'name' => 'Plant Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                249 =>
                    array (
                        'id' => 1250,
                        'groupno' => 420,
                        'name' => 'Plaster Die Maker',
                        'industry' => 'Pottery And Porcelain',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                250 =>
                    array (
                        'id' => 1251,
                        'groupno' => 321,
                        'name' => 'Plaster Maker',
                        'industry' => 'Non-Ferrous Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                251 =>
                    array (
                        'id' => 1252,
                        'groupno' => 320,
                        'name' => 'Plaster Molder',
                        'industry' => 'Foundry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                252 =>
                    array (
                        'id' => 1253,
                        'groupno' => 380,
                        'name' => 'Plasterer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                253 =>
                    array (
                        'id' => 1254,
                        'groupno' => 480,
                        'name' => 'Plasterer Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                254 =>
                    array (
                        'id' => 1255,
                        'groupno' => 230,
                        'name' => 'Platen Press Feeder',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                255 =>
                    array (
                        'id' => 1256,
                        'groupno' => 230,
                        'name' => 'Platen Press Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                256 =>
                    array (
                        'id' => 1257,
                        'groupno' => 330,
                        'name' => 'Plater',
                        'industry' => 'Electroplating',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                257 =>
                    array (
                        'id' => 1258,
                        'groupno' => 460,
                        'name' => 'Plater, Electroless, Printed Circuit Boards',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                258 =>
                    array (
                        'id' => 1259,
                        'groupno' => 460,
                        'name' => 'Plater, Hot Dip',
                        'industry' => 'Galvanizing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                259 =>
                    array (
                        'id' => 1260,
                        'groupno' => 460,
                        'name' => 'Plater, Printed Circuit Board Panels',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                260 =>
                    array (
                        'id' => 1261,
                        'groupno' => 221,
                        'name' => 'Plater, Semiconductor Wafers & Components',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                261 =>
                    array (
                        'id' => 1262,
                        'groupno' => 230,
                        'name' => 'Pleating Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                262 =>
                    array (
                        'id' => 1263,
                        'groupno' => 481,
                        'name' => 'Plumber',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                263 =>
                    array (
                        'id' => 1264,
                        'groupno' => 481,
                        'name' => 'Plumber Apprentice',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                264 =>
                    array (
                        'id' => 1265,
                        'groupno' => 481,
                        'name' => 'Plumber Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                265 =>
                    array (
                        'id' => 1266,
                        'groupno' => 370,
                        'name' => 'Pneumatic Tool Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                266 =>
                    array (
                        'id' => 1267,
                        'groupno' => 380,
                        'name' => 'Pneumatic Tube Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                267 =>
                    array (
                        'id' => 1268,
                        'groupno' => 220,
                        'name' => 'Podiatrist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                268 =>
                    array (
                        'id' => 1269,
                        'groupno' => 251,
                        'name' => 'Police Artist',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                269 =>
                    array (
                        'id' => 1270,
                        'groupno' => 490,
                        'name' => 'Police Captain',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                270 =>
                    array (
                        'id' => 1271,
                        'groupno' => 111,
                        'name' => 'Police Clerk',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                271 =>
                    array (
                        'id' => 1272,
                        'groupno' => 490,
                        'name' => 'Police Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                272 =>
                    array (
                        'id' => 1273,
                        'groupno' => 490,
                        'name' => 'Police Officer, State Highway',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                273 =>
                    array (
                        'id' => 1274,
                        'groupno' => 120,
                        'name' => 'Polisher, Eyeglass Frames',
                        'industry' => 'Optical Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                274 =>
                    array (
                        'id' => 1275,
                        'groupno' => 321,
                        'name' => 'Polisher/Buffer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                275 =>
                    array (
                        'id' => 1276,
                        'groupno' => 330,
                        'name' => 'Polishing Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                276 =>
                    array (
                        'id' => 1277,
                        'groupno' => 212,
                        'name' => 'Polygraph Examiner',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                277 =>
                    array (
                        'id' => 1278,
                        'groupno' => 360,
                        'name' => 'Porter',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                278 =>
                    array (
                        'id' => 1279,
                        'groupno' => 360,
                        'name' => 'Porter, Baggage',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                279 =>
                    array (
                        'id' => 1280,
                        'groupno' => 330,
                        'name' => 'Pottery Machine Operator',
                        'industry' => 'Pottery And Porcelain',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                280 =>
                    array (
                        'id' => 1281,
                        'groupno' => 322,
                        'name' => 'Poultry Dresser',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                281 =>
                    array (
                        'id' => 1282,
                        'groupno' => 230,
                        'name' => 'Power Barker Operator',
                        'industry' => 'Paper And Pulp',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                282 =>
                    array (
                        'id' => 1283,
                        'groupno' => 430,
                        'name' => 'Power Brake Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                283 =>
                    array (
                        'id' => 1284,
                        'groupno' => 332,
                        'name' => 'Power Plant Operator',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                284 =>
                    array (
                        'id' => 1285,
                        'groupno' => 330,
                        'name' => 'Power Press Tender',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                285 =>
                    array (
                        'id' => 1286,
                        'groupno' => 332,
                        'name' => 'Power Reactor Operator',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                286 =>
                    array (
                        'id' => 1287,
                        'groupno' => 351,
                        'name' => 'Power Shovel Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                287 =>
                    array (
                        'id' => 1288,
                        'groupno' => 481,
                        'name' => 'Powerhouse Mechanic',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                288 =>
                    array (
                        'id' => 1289,
                        'groupno' => 370,
                        'name' => 'Precision Assembler & Repairer',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                289 =>
                    array (
                        'id' => 1290,
                        'groupno' => 320,
                        'name' => 'Precision Assembler, Bench',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                290 =>
                    array (
                        'id' => 1291,
                        'groupno' => 110,
                        'name' => 'President',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                291 =>
                    array (
                        'id' => 1292,
                        'groupno' => 230,
                        'name' => 'Press Operator',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                292 =>
                    array (
                        'id' => 1293,
                        'groupno' => 330,
                        'name' => 'Press Operator, Cylinder',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                293 =>
                    array (
                        'id' => 1294,
                        'groupno' => 430,
                        'name' => 'Press Operator, Heavy Duty',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                294 =>
                    array (
                        'id' => 1295,
                        'groupno' => 331,
                        'name' => 'Press Operator, Meat',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                295 =>
                    array (
                        'id' => 1296,
                        'groupno' => 230,
                        'name' => 'Press Operator, Offset',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                296 =>
                    array (
                        'id' => 1297,
                        'groupno' => 330,
                        'name' => 'Press Operator, Rotogravure',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                297 =>
                    array (
                        'id' => 1298,
                        'groupno' => 321,
                        'name' => 'Presser, All-Around',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                298 =>
                    array (
                        'id' => 1299,
                        'groupno' => 221,
                        'name' => 'Presser, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                299 =>
                    array (
                        'id' => 1300,
                        'groupno' => 321,
                        'name' => 'Presser, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                300 =>
                    array (
                        'id' => 1301,
                        'groupno' => 230,
                        'name' => 'Print Developer, Automatic',
                        'industry' => 'Photofinishing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                301 =>
                    array (
                        'id' => 1302,
                        'groupno' => 221,
                        'name' => 'Printed Circuit Board Assembler, Hand',
                        'industry' => 'Communication Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                302 =>
                    array (
                        'id' => 1303,
                        'groupno' => 120,
                        'name' => 'Printed Circuit Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                303 =>
                    array (
                        'id' => 1304,
                        'groupno' => 320,
                        'name' => 'Printer, Job',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                304 =>
                    array (
                        'id' => 1305,
                        'groupno' => 390,
                        'name' => 'Probation Officer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                305 =>
                    array (
                        'id' => 1306,
                        'groupno' => 251,
                        'name' => 'Process Server',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                306 =>
                    array (
                        'id' => 1307,
                        'groupno' => 360,
                        'name' => 'Produce Clerk, Retail',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                307 =>
                    array (
                        'id' => 1308,
                        'groupno' => 212,
                        'name' => 'Producer',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                308 =>
                    array (
                        'id' => 1309,
                        'groupno' => 212,
                        'name' => 'Prompter',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                309 =>
                    array (
                        'id' => 1310,
                        'groupno' => 211,
                        'name' => 'Proofreader',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                310 =>
                    array (
                        'id' => 1311,
                        'groupno' => 111,
                        'name' => 'Proofreader, Production',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                311 =>
                    array (
                        'id' => 1312,
                        'groupno' => 380,
                        'name' => 'Prop Maker',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                312 =>
                    array (
                        'id' => 1313,
                        'groupno' => 320,
                        'name' => 'Prosthetics Technician',
                        'industry' => 'Protective Development',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                313 =>
                    array (
                        'id' => 1314,
                        'groupno' => 310,
                        'name' => 'Prosthetist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                314 =>
                    array (
                        'id' => 1315,
                        'groupno' => 311,
                        'name' => 'Psychiatric Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                315 =>
                    array (
                        'id' => 1316,
                        'groupno' => 340,
                        'name' => 'Psychiatric Ward Attendant',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                316 =>
                    array (
                        'id' => 1317,
                        'groupno' => 110,
                        'name' => 'Psychologist, Clinical',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                317 =>
                    array (
                        'id' => 1318,
                        'groupno' => 110,
                        'name' => 'Psychologist, Counseling',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                318 =>
                    array (
                        'id' => 1319,
                        'groupno' => 380,
                        'name' => 'Public Address Setter-Up & Servicer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                319 =>
                    array (
                        'id' => 1320,
                        'groupno' => 110,
                        'name' => 'Public Health Service Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                320 =>
                    array (
                        'id' => 1321,
                        'groupno' => 111,
                        'name' => 'Public Relations Representative',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                321 =>
                    array (
                        'id' => 1322,
                        'groupno' => 212,
                        'name' => 'Pulmonary Function Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                322 =>
                    array (
                        'id' => 1323,
                        'groupno' => 470,
                        'name' => 'Pump Installer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                323 =>
                    array (
                        'id' => 1324,
                        'groupno' => 330,
                        'name' => 'Pump Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                324 =>
                    array (
                        'id' => 1325,
                        'groupno' => 370,
                        'name' => 'Pump Servicer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                325 =>
                    array (
                        'id' => 1326,
                        'groupno' => 332,
                        'name' => 'Pump Station Operator, Waterworks',
                        'industry' => 'Waterworks',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                326 =>
                    array (
                        'id' => 1327,
                        'groupno' => 330,
                        'name' => 'Punch Press Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                327 =>
                    array (
                        'id' => 1328,
                        'groupno' => 430,
                        'name' => 'Punch Press Operator, Automatic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                328 =>
                    array (
                        'id' => 1329,
                        'groupno' => 251,
                        'name' => 'Purchasing Agent',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                329 =>
                    array (
                        'id' => 1330,
                        'groupno' => 111,
                        'name' => 'Purser',
                        'industry' => 'Water Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                330 =>
                    array (
                        'id' => 1331,
                        'groupno' => 321,
                        'name' => 'Putty Glazer, Pottery',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                331 =>
                    array (
                        'id' => 1332,
                        'groupno' => 221,
                        'name' => 'Quality Assurance Monitor',
                        'industry' => 'Automotive Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                332 =>
                    array (
                        'id' => 1333,
                        'groupno' => 212,
                        'name' => 'Quality Control Technician',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                333 =>
                    array (
                        'id' => 1334,
                        'groupno' => 480,
                        'name' => 'Quarry Worker',
                        'industry' => 'Mine And Quarry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                334 =>
                    array (
                        'id' => 1335,
                        'groupno' => 120,
                        'name' => 'Quick Sketch Artist',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Quick Sketch Artist',
                    ),
                335 =>
                    array (
                        'id' => 1336,
                        'groupno' => 221,
                        'name' => 'Racket Stringer',
                        'industry' => 'Toy-Sport Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                336 =>
                    array (
                        'id' => 1337,
                        'groupno' => 330,
                        'name' => 'Radial Arm Saw Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                337 =>
                    array (
                        'id' => 1338,
                        'groupno' => 320,
                        'name' => 'Radial Drill Press Setup',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                338 =>
                    array (
                        'id' => 1339,
                        'groupno' => 310,
                        'name' => 'Radiation Therapy Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                339 =>
                    array (
                        'id' => 1340,
                        'groupno' => 212,
                        'name' => 'Radiographer, Industrial',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                340 =>
                    array (
                        'id' => 1341,
                        'groupno' => 310,
                        'name' => 'Radiologic Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                341 =>
                    array (
                        'id' => 1568,
                        'groupno' => 360,
                        'name' => 'Stock Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                342 =>
                    array (
                        'id' => 1342,
                        'groupno' => 380,
                        'name' => 'Radiological Equipment Specialist',
                        'industry' => 'Instalation And Application',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                343 =>
                    array (
                        'id' => 1343,
                        'groupno' => 212,
                        'name' => 'Radiotelephone Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                344 =>
                    array (
                        'id' => 1344,
                        'groupno' => 481,
                        'name' => 'Railroad Car Builder',
                        'industry' => 'Railroad Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                345 =>
                    array (
                        'id' => 1345,
                        'groupno' => 481,
                        'name' => 'Railway Car Repairer',
                        'industry' => 'Railroad Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                346 =>
                    array (
                        'id' => 1346,
                        'groupno' => 460,
                        'name' => 'Ramp Attendant',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                347 =>
                    array (
                        'id' => 1347,
                        'groupno' => 111,
                        'name' => 'Rater',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                348 =>
                    array (
                        'id' => 1348,
                        'groupno' => 251,
                        'name' => 'Real Estate Agent',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                349 =>
                    array (
                        'id' => 1349,
                        'groupno' => 321,
                        'name' => 'Reamer, Hand',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                350 =>
                    array (
                        'id' => 1350,
                        'groupno' => 330,
                        'name' => 'Reaming Machine Tender',
                        'industry' => 'Non-Ferrous Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                351 =>
                    array (
                        'id' => 1351,
                        'groupno' => 111,
                        'name' => 'Receptionist',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                352 =>
                    array (
                        'id' => 1352,
                        'groupno' => 212,
                        'name' => 'Recording Engineer',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                353 =>
                    array (
                        'id' => 1353,
                        'groupno' => 360,
                        'name' => 'Recording Studio Set-Up Worker',
                        'industry' => 'Recording',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                354 =>
                    array (
                        'id' => 1354,
                        'groupno' => 230,
                        'name' => 'Recordist',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                355 =>
                    array (
                        'id' => 1355,
                        'groupno' => 214,
                        'name' => 'Recreation Aide',
                        'industry' => 'Social Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                356 =>
                    array (
                        'id' => 1356,
                        'groupno' => 310,
                        'name' => 'Recreational Therapist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                357 =>
                    array (
                        'id' => 1357,
                        'groupno' => 111,
                        'name' => 'Recruiter, Personnel',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                358 =>
                    array (
                        'id' => 1358,
                        'groupno' => 111,
                        'name' => 'Registration Clerk',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                359 =>
                    array (
                        'id' => 1359,
                        'groupno' => 212,
                        'name' => 'Rehabilitation Center Manager',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                360 =>
                    array (
                        'id' => 1360,
                        'groupno' => 481,
                        'name' => 'Reinforcing Iron Worker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                361 =>
                    array (
                        'id' => 1361,
                        'groupno' => 221,
                        'name' => 'Repairer',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                362 =>
                    array (
                        'id' => 1362,
                        'groupno' => 220,
                        'name' => 'Repairer, Art Objects',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                363 =>
                    array (
                        'id' => 1363,
                        'groupno' => 320,
                        'name' => 'Repairer, Office Machines',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                364 =>
                    array (
                        'id' => 1364,
                        'groupno' => 320,
                        'name' => 'Repairer, Salvaged Parts',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                365 =>
                    array (
                        'id' => 1365,
                        'groupno' => 320,
                        'name' => 'Repairer, Small Appliance',
                        'industry' => 'Household Appliances',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                366 =>
                    array (
                        'id' => 1366,
                        'groupno' => 320,
                        'name' => 'Repairer, Wind Instrument',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                367 =>
                    array (
                        'id' => 1367,
                        'groupno' => 220,
                        'name' => 'Repairer/Adjuster',
                        'industry' => 'Office Machines',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                368 =>
                    array (
                        'id' => 1368,
                        'groupno' => 251,
                        'name' => 'Reporter',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                369 =>
                    array (
                        'id' => 1369,
                        'groupno' => 110,
                        'name' => 'Reports Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                370 =>
                    array (
                        'id' => 1370,
                        'groupno' => 213,
                        'name' => 'Repossessor',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                371 =>
                    array (
                        'id' => 1371,
                        'groupno' => 460,
                        'name' => 'Resaw Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                372 =>
                    array (
                        'id' => 1372,
                        'groupno' => 111,
                        'name' => 'Researcher',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                373 =>
                    array (
                        'id' => 1373,
                        'groupno' => 111,
                        'name' => 'Reservation Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                374 =>
                    array (
                        'id' => 1374,
                        'groupno' => 111,
                        'name' => 'Reservations Agent',
                        'industry' => 'Air Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                375 =>
                    array (
                        'id' => 1375,
                        'groupno' => 311,
                        'name' => 'Respiratory Therapist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                376 =>
                    array (
                        'id' => 1376,
                        'groupno' => 340,
                        'name' => 'Respiratory Therapy Aide',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                377 =>
                    array (
                        'id' => 1377,
                        'groupno' => 240,
                        'name' => 'Rest Room Attendant',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                378 =>
                    array (
                        'id' => 1378,
                        'groupno' => 380,
                        'name' => 'Restoration Technician',
                        'industry' => 'Museums',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                379 =>
                    array (
                        'id' => 1379,
                        'groupno' => 214,
                        'name' => 'Retail Clerk',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                380 =>
                    array (
                        'id' => 1380,
                        'groupno' => 111,
                        'name' => 'Reviewer, Final Application',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                381 =>
                    array (
                        'id' => 1381,
                        'groupno' => 330,
                        'name' => 'Rewinder Operator',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                382 =>
                    array (
                        'id' => 1382,
                        'groupno' => 230,
                        'name' => 'Rice Grader',
                        'industry' => 'Grain-Feed Mills',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                383 =>
                    array (
                        'id' => 1383,
                        'groupno' => 240,
                        'name' => 'Ride Operator',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                384 =>
                    array (
                        'id' => 1384,
                        'groupno' => 482,
                        'name' => 'Rigger',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                385 =>
                    array (
                        'id' => 1385,
                        'groupno' => 482,
                        'name' => 'Rigger, High',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                386 =>
                    array (
                        'id' => 1386,
                        'groupno' => 481,
                        'name' => 'Rigger/Slinger',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                387 =>
                    array (
                        'id' => 1387,
                        'groupno' => 330,
                        'name' => 'Ripsaw Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                388 =>
                    array (
                        'id' => 1388,
                        'groupno' => 230,
                        'name' => 'Rivet And Bolt Maker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                389 =>
                    array (
                        'id' => 1389,
                        'groupno' => 330,
                        'name' => 'Riveter, Hydraulic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                390 =>
                    array (
                        'id' => 1390,
                        'groupno' => 481,
                        'name' => 'Riveter, Pneumatic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                391 =>
                    array (
                        'id' => 1391,
                        'groupno' => 330,
                        'name' => 'Riveting Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                392 =>
                    array (
                        'id' => 1392,
                        'groupno' => 330,
                        'name' => 'Riveting Machine Operator, Automatic',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                393 =>
                    array (
                        'id' => 1393,
                        'groupno' => 351,
                        'name' => 'Road Roller Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                394 =>
                    array (
                        'id' => 1394,
                        'groupno' => 330,
                        'name' => 'Robotic Machine Operator',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                395 =>
                    array (
                        'id' => 1395,
                        'groupno' => 470,
                        'name' => 'Robotics Service Technician',
                        'industry' => 'Machinery Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                396 =>
                    array (
                        'id' => 1396,
                        'groupno' => 351,
                        'name' => 'Rock Drill Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                397 =>
                    array (
                        'id' => 1397,
                        'groupno' => 560,
                        'name' => 'Roll Tender/Setter',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                398 =>
                    array (
                        'id' => 1398,
                        'groupno' => 330,
                        'name' => 'Roller Machine Operator',
                        'industry' => 'Metal Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                399 =>
                    array (
                        'id' => 1399,
                        'groupno' => 230,
                        'name' => 'Rolling Mill Attendant',
                        'industry' => 'Steel And Relay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                400 =>
                    array (
                        'id' => 1400,
                        'groupno' => 380,
                        'name' => 'Roofer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                401 =>
                    array (
                        'id' => 1401,
                        'groupno' => 480,
                        'name' => 'Roofer Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                402 =>
                    array (
                        'id' => 1402,
                        'groupno' => 322,
                        'name' => 'Room Service Clerk',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                403 =>
                    array (
                        'id' => 1403,
                        'groupno' => 480,
                        'name' => 'Rotary Driller',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                404 =>
                    array (
                        'id' => 1404,
                        'groupno' => 480,
                        'name' => 'Rotary Driller Helper',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                405 =>
                    array (
                        'id' => 1405,
                        'groupno' => 230,
                        'name' => 'Rougher, Bar Mill',
                        'industry' => 'Steel And Relay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                406 =>
                    array (
                        'id' => 1406,
                        'groupno' => 480,
                        'name' => 'Roughneck',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                407 =>
                    array (
                        'id' => 1407,
                        'groupno' => 480,
                        'name' => 'Roustabout',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                408 =>
                    array (
                        'id' => 1408,
                        'groupno' => 211,
                        'name' => 'Router',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                409 =>
                    array (
                        'id' => 1409,
                        'groupno' => 330,
                        'name' => 'Router Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                410 =>
                    array (
                        'id' => 1410,
                        'groupno' => 330,
                        'name' => 'Router Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                411 =>
                    array (
                        'id' => 1411,
                        'groupno' => 460,
                        'name' => 'Rubber Cutter',
                        'industry' => 'Rubber Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                412 =>
                    array (
                        'id' => 1412,
                        'groupno' => 230,
                        'name' => 'Rubber Mill Operator',
                        'industry' => 'Plastic-Synthetic',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                413 =>
                    array (
                        'id' => 1413,
                        'groupno' => 340,
                        'name' => 'Rug Cleaner, Hand Or Machine',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                414 =>
                    array (
                        'id' => 1414,
                        'groupno' => 321,
                        'name' => 'Rug Repairer',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                415 =>
                    array (
                        'id' => 1415,
                        'groupno' => 420,
                        'name' => 'Saddle Maker',
                        'industry' => 'Leather Production',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                416 =>
                    array (
                        'id' => 1416,
                        'groupno' => 212,
                        'name' => 'Safety Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                417 =>
                    array (
                        'id' => 1417,
                        'groupno' => 212,
                        'name' => 'Safety Manager',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                418 =>
                    array (
                        'id' => 1418,
                        'groupno' => 380,
                        'name' => 'Sail Maker',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                419 =>
                    array (
                        'id' => 1419,
                        'groupno' => 322,
                        'name' => 'Salad Maker',
                        'industry' => 'Water Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                420 =>
                    array (
                        'id' => 1420,
                        'groupno' => 212,
                        'name' => 'Sales Agent, Insurance',
                        'industry' => 'Insurance; Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                421 =>
                    array (
                        'id' => 1421,
                        'groupno' => 214,
                        'name' => 'Sales Clerk',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                422 =>
                    array (
                        'id' => 1422,
                        'groupno' => 212,
                        'name' => 'Sales Rep, Advertising',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                423 =>
                    array (
                        'id' => 1423,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Computers And Edp Systems',
                        'industry' => 'Wholesale Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                424 =>
                    array (
                        'id' => 1424,
                        'groupno' => 212,
                        'name' => 'Sales Rep, Data Processing Services',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                425 =>
                    array (
                        'id' => 1425,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Door-To-Door',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                426 =>
                    array (
                        'id' => 1426,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Farm, Garden Eqpt. & Supplies',
                        'industry' => 'Wholesale Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                427 =>
                    array (
                        'id' => 1427,
                        'groupno' => 212,
                        'name' => 'Sales Rep, Financial Services',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                428 =>
                    array (
                        'id' => 1428,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Livestock',
                        'industry' => 'Wholesale Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                429 =>
                    array (
                        'id' => 1429,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Office Machines',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                430 =>
                    array (
                        'id' => 1430,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Recreation, Sporting Goods',
                        'industry' => 'Wholesale Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                431 =>
                    array (
                        'id' => 1431,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Security Systems',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                432 =>
                    array (
                        'id' => 1432,
                        'groupno' => 212,
                        'name' => 'Sales Rep, Upholstery, Furniture Repair',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                433 =>
                    array (
                        'id' => 1433,
                        'groupno' => 251,
                        'name' => 'Sales Rep, Women\'S And Girls\' Apparel',
                        'industry' => 'Wholesale Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                434 =>
                    array (
                        'id' => 1434,
                        'groupno' => 251,
                        'name' => 'Salesperson, Automobiles',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                435 =>
                    array (
                        'id' => 1435,
                        'groupno' => 214,
                        'name' => 'Salesperson, General Merchandise',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                436 =>
                    array (
                        'id' => 1436,
                        'groupno' => 214,
                        'name' => 'Salesperson, Parts',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                437 =>
                    array (
                        'id' => 1437,
                        'groupno' => 214,
                        'name' => 'Salesperson, Shoes',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                438 =>
                    array (
                        'id' => 1438,
                        'groupno' => 430,
                        'name' => 'Salvage Cutter',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                439 =>
                    array (
                        'id' => 1439,
                        'groupno' => 480,
                        'name' => 'Sandblaster',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                440 =>
                    array (
                        'id' => 1440,
                        'groupno' => 330,
                        'name' => 'Sander, Machine',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                441 =>
                    array (
                        'id' => 1441,
                        'groupno' => 322,
                        'name' => 'Sandwich Maker',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                442 =>
                    array (
                        'id' => 1442,
                        'groupno' => 331,
                        'name' => 'Sausage Maker',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                443 =>
                    array (
                        'id' => 1443,
                        'groupno' => 331,
                        'name' => 'Sausage Stuffer',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                444 =>
                    array (
                        'id' => 1444,
                        'groupno' => 321,
                        'name' => 'Saw Blade Filer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                445 =>
                    array (
                        'id' => 1445,
                        'groupno' => 360,
                        'name' => 'Sawmill Worker',
                        'industry' => 'Sawmill And Planer Mill',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                446 =>
                    array (
                        'id' => 1446,
                        'groupno' => 330,
                        'name' => 'Sawyer',
                        'industry' => 'Plastic Production; Plastic-Synth.',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                447 =>
                    array (
                        'id' => 1447,
                        'groupno' => 230,
                        'name' => 'Sawyer, Circular Head',
                        'industry' => 'Sawmill And Planer Mill',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                448 =>
                    array (
                        'id' => 1448,
                        'groupno' => 230,
                        'name' => 'Sawyer, Cork Slabs',
                        'industry' => 'Wood Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                449 =>
                    array (
                        'id' => 1449,
                        'groupno' => 330,
                        'name' => 'Sawyer, Trimmer',
                        'industry' => 'Sawmill And Planer Mill',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                450 =>
                    array (
                        'id' => 1450,
                        'groupno' => 111,
                        'name' => 'Scheduler',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                451 =>
                    array (
                        'id' => 1451,
                        'groupno' => 212,
                        'name' => 'School Principal',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                452 =>
                    array (
                        'id' => 1452,
                        'groupno' => 111,
                        'name' => 'Scoreboard Operator',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                453 =>
                    array (
                        'id' => 1569,
                        'groupno' => 360,
                        'name' => 'Stock Clerk',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                454 =>
                    array (
                        'id' => 1453,
                        'groupno' => 251,
                        'name' => 'Scout, Professional Sports',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                455 =>
                    array (
                        'id' => 1454,
                        'groupno' => 460,
                        'name' => 'Scrap Handler',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                456 =>
                    array (
                        'id' => 1455,
                        'groupno' => 320,
                        'name' => 'Screen Maker, Photographic Process',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                457 =>
                    array (
                        'id' => 1456,
                        'groupno' => 221,
                        'name' => 'Screen Maker, Wallpaper',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                458 =>
                    array (
                        'id' => 1457,
                        'groupno' => 330,
                        'name' => 'Screw Machine Operator, Multiple Spindle',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                459 =>
                    array (
                        'id' => 1458,
                        'groupno' => 330,
                        'name' => 'Scroll Machine Operator',
                        'industry' => 'Structured Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                460 =>
                    array (
                        'id' => 1459,
                        'groupno' => 321,
                        'name' => 'Sculptor',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                461 =>
                    array (
                        'id' => 1460,
                        'groupno' => 112,
                        'name' => 'Secretary',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                462 =>
                    array (
                        'id' => 1461,
                        'groupno' => 112,
                        'name' => 'Secretary, Legal',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                463 =>
                    array (
                        'id' => 1462,
                        'groupno' => 112,
                        'name' => 'Secretary, Medical',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                464 =>
                    array (
                        'id' => 1463,
                        'groupno' => 112,
                        'name' => 'Secretary, Social',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                465 =>
                    array (
                        'id' => 1464,
                        'groupno' => 212,
                        'name' => 'Security Guard, Gate',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                466 =>
                    array (
                        'id' => 1465,
                        'groupno' => 213,
                        'name' => 'Security Guard, Plant',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                467 =>
                    array (
                        'id' => 1466,
                        'groupno' => 390,
                        'name' => 'Security Officer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                468 =>
                    array (
                        'id' => 1467,
                        'groupno' => 230,
                        'name' => 'Seed Pelleter',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                469 =>
                    array (
                        'id' => 1468,
                        'groupno' => 212,
                        'name' => 'Seismologist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                470 =>
                    array (
                        'id' => 1469,
                        'groupno' => 330,
                        'name' => 'Semiconductor Processor',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                471 =>
                    array (
                        'id' => 1470,
                        'groupno' => 380,
                        'name' => 'Septic Tank Installer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                472 =>
                    array (
                        'id' => 1471,
                        'groupno' => 480,
                        'name' => 'Septic Tank Servicer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                473 =>
                    array (
                        'id' => 1472,
                        'groupno' => 214,
                        'name' => 'Service Manager',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                474 =>
                    array (
                        'id' => 1473,
                        'groupno' => 213,
                        'name' => 'Service Representative',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                475 =>
                    array (
                        'id' => 1474,
                        'groupno' => 340,
                        'name' => 'Service Station Attendant',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                476 =>
                    array (
                        'id' => 1475,
                        'groupno' => 213,
                        'name' => 'Set Designer',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                477 =>
                    array (
                        'id' => 1476,
                        'groupno' => 360,
                        'name' => 'Set-Up Person, Trade Show',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                478 =>
                    array (
                        'id' => 1477,
                        'groupno' => 320,
                        'name' => 'Setter, Automatic Spinning Lathe',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                479 =>
                    array (
                        'id' => 1478,
                        'groupno' => 480,
                        'name' => 'Sewage Disposal Worker',
                        'industry' => 'Sanitary Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                480 =>
                    array (
                        'id' => 1479,
                        'groupno' => 480,
                        'name' => 'Sewer Line Repairer',
                        'industry' => 'Sanitary Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                481 =>
                    array (
                        'id' => 1480,
                        'groupno' => 341,
                        'name' => 'Sewer Pipe Cleaner',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                482 =>
                    array (
                        'id' => 1481,
                        'groupno' => 221,
                        'name' => 'Sewer, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                483 =>
                    array (
                        'id' => 1482,
                        'groupno' => 230,
                        'name' => 'Sewing Machine Operator',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                484 =>
                    array (
                        'id' => 1483,
                        'groupno' => 370,
                        'name' => 'Sewing Machine Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                485 =>
                    array (
                        'id' => 1484,
                        'groupno' => 330,
                        'name' => 'Shaper Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                486 =>
                    array (
                        'id' => 1485,
                        'groupno' => 330,
                        'name' => 'Shaping Machine Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                487 =>
                    array (
                        'id' => 1486,
                        'groupno' => 430,
                        'name' => 'Shear Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                488 =>
                    array (
                        'id' => 1487,
                        'groupno' => 320,
                        'name' => 'Sheetmetal Fabricating Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                489 =>
                    array (
                        'id' => 1488,
                        'groupno' => 370,
                        'name' => 'Sheetmetal Mechanic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                490 =>
                    array (
                        'id' => 1489,
                        'groupno' => 491,
                        'name' => 'Shellfish Grower',
                        'industry' => 'Fishing And Hunting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                491 =>
                    array (
                        'id' => 1490,
                        'groupno' => 490,
                        'name' => 'Sheriff, Deputy',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                492 =>
                    array (
                        'id' => 1491,
                        'groupno' => 481,
                        'name' => 'Shipfitter',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                493 =>
                    array (
                        'id' => 1492,
                        'groupno' => 480,
                        'name' => 'Shipfitter Helper',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                494 =>
                    array (
                        'id' => 1493,
                        'groupno' => 360,
                        'name' => 'Shipping And Receiving Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                495 =>
                    array (
                        'id' => 1494,
                        'groupno' => 214,
                        'name' => 'Shipping Checker',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                496 =>
                    array (
                        'id' => 1495,
                        'groupno' => 380,
                        'name' => 'Shipwright',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                497 =>
                    array (
                        'id' => 1496,
                        'groupno' => 221,
                        'name' => 'Shoe Repairer',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                498 =>
                    array (
                        'id' => 1497,
                        'groupno' => 214,
                        'name' => 'Shop Estimator',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                499 =>
                    array (
                        'id' => 1498,
                        'groupno' => 210,
                        'name' => 'Show Host/Hostess',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
            ));
            \DB::table('occupations')->insert(array (
                0 =>
                    array (
                        'id' => 1499,
                        'groupno' => 250,
                        'name' => 'Shuttle Bus Driver',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                1 =>
                    array (
                        'id' => 1500,
                        'groupno' => 380,
                        'name' => 'Sider',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                2 =>
                    array (
                        'id' => 1501,
                        'groupno' => 341,
                        'name' => 'Sign Poster',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                3 =>
                    array (
                        'id' => 1502,
                        'groupno' => 120,
                        'name' => 'Sign Writer, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                4 =>
                    array (
                        'id' => 1503,
                        'groupno' => 221,
                        'name' => 'Silk Screen Etcher',
                        'industry' => 'Engraving',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                5 =>
                    array (
                        'id' => 1504,
                        'groupno' => 221,
                        'name' => 'Silk Screen Frame Assembler',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                6 =>
                    array (
                        'id' => 1505,
                        'groupno' => 221,
                        'name' => 'Silk Screen Printer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                7 =>
                    array (
                        'id' => 1506,
                        'groupno' => 220,
                        'name' => 'Silversmith',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                8 =>
                    array (
                        'id' => 1507,
                        'groupno' => 210,
                        'name' => 'Singer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                9 =>
                    array (
                        'id' => 1508,
                        'groupno' => 493,
                        'name' => 'Ski Instructor',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                10 =>
                    array (
                        'id' => 1509,
                        'groupno' => 240,
                        'name' => 'Ski Lift Operator',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                11 =>
                    array (
                        'id' => 1510,
                        'groupno' => 590,
                        'name' => 'Ski Patroller',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                12 =>
                    array (
                        'id' => 1511,
                        'groupno' => 221,
                        'name' => 'Ski Repairer, Production',
                        'industry' => 'Toy-Sport Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                13 =>
                    array (
                        'id' => 1512,
                        'groupno' => 420,
                        'name' => 'Skinner',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Skilled Agricultural, Forestry and Fishery Workers',
                    ),
                14 =>
                    array (
                        'id' => 1513,
                        'groupno' => 480,
                        'name' => 'Skip Tender',
                        'industry' => 'Mine And Quarry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                15 =>
                    array (
                        'id' => 1514,
                        'groupno' => 111,
                        'name' => 'Skip Tracer',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                16 =>
                    array (
                        'id' => 1515,
                        'groupno' => 460,
                        'name' => 'Slasher Tender',
                        'industry' => 'Textile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                17 =>
                    array (
                        'id' => 1516,
                        'groupno' => 230,
                        'name' => 'Slicing Machine Operator',
                        'industry' => 'Bakery Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                18 =>
                    array (
                        'id' => 1517,
                        'groupno' => 460,
                        'name' => 'Slitting Machine Operator Helper',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                19 =>
                    array (
                        'id' => 1518,
                        'groupno' => 331,
                        'name' => 'Slurry Blender',
                        'industry' => 'Cement',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                20 =>
                    array (
                        'id' => 1519,
                        'groupno' => 370,
                        'name' => 'Smog Technician',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                21 =>
                    array (
                        'id' => 1520,
                        'groupno' => 590,
                        'name' => 'Smoke Jumper',
                        'industry' => 'Forestry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                22 =>
                    array (
                        'id' => 1521,
                        'groupno' => 351,
                        'name' => 'Snowplow Operator',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                23 =>
                    array (
                        'id' => 1522,
                        'groupno' => 230,
                        'name' => 'Soap Maker',
                        'industry' => 'Soap And Related',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                24 =>
                    array (
                        'id' => 1523,
                        'groupno' => 111,
                        'name' => 'Social Worker',
                        'industry' => 'Social Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                25 =>
                    array (
                        'id' => 1524,
                        'groupno' => 111,
                        'name' => 'Software Engineer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                26 =>
                    array (
                        'id' => 1525,
                        'groupno' => 213,
                        'name' => 'Soil Conservationist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                27 =>
                    array (
                        'id' => 1526,
                        'groupno' => 481,
                        'name' => 'Solar Energy System Installer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                28 =>
                    array (
                        'id' => 1527,
                        'groupno' => 470,
                        'name' => 'Solar Fabrication Technician',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                29 =>
                    array (
                        'id' => 1528,
                        'groupno' => 120,
                        'name' => 'Solderer',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                30 =>
                    array (
                        'id' => 1529,
                        'groupno' => 111,
                        'name' => 'Sorter',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                31 =>
                    array (
                        'id' => 1530,
                        'groupno' => 214,
                        'name' => 'Sorter-Pricer',
                        'industry' => 'Nonprofit Organization',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                32 =>
                    array (
                        'id' => 1531,
                        'groupno' => 221,
                        'name' => 'Sorter, Agricultural Produce',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                33 =>
                    array (
                        'id' => 1532,
                        'groupno' => 221,
                        'name' => 'Sorter, Remnant',
                        'industry' => 'Textile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                34 =>
                    array (
                        'id' => 1533,
                        'groupno' => 212,
                        'name' => 'Sound Effects Technician',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                35 =>
                    array (
                        'id' => 1534,
                        'groupno' => 212,
                        'name' => 'Sound Mixer',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                36 =>
                    array (
                        'id' => 1535,
                        'groupno' => 322,
                        'name' => 'Sous Chef',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                37 =>
                    array (
                        'id' => 1536,
                        'groupno' => 490,
                        'name' => 'Special Agent',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                38 =>
                    array (
                        'id' => 1537,
                        'groupno' => 390,
                        'name' => 'Special Policeman',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations',
                    ),
                39 =>
                    array (
                        'id' => 1538,
                        'groupno' => 212,
                        'name' => 'Speech Pathologist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                40 =>
                    array (
                        'id' => 1539,
                        'groupno' => 331,
                        'name' => 'Spinner',
                        'industry' => 'Sugar And Confectionery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                41 =>
                    array (
                        'id' => 1540,
                        'groupno' => 430,
                        'name' => 'Spinner, Hydraulic',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                42 =>
                    array (
                        'id' => 1541,
                        'groupno' => 330,
                        'name' => 'Spinning Lathe Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                43 =>
                    array (
                        'id' => 1542,
                        'groupno' => 221,
                        'name' => 'Sports Equipment Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                44 =>
                    array (
                        'id' => 1543,
                        'groupno' => 221,
                        'name' => 'Spot Cleaner',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                45 =>
                    array (
                        'id' => 1544,
                        'groupno' => 111,
                        'name' => 'Spotter, Photographic',
                        'industry' => 'Photofinishing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                46 =>
                    array (
                        'id' => 1545,
                        'groupno' => 330,
                        'name' => 'Spray Painting Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                47 =>
                    array (
                        'id' => 1546,
                        'groupno' => 460,
                        'name' => 'Spreader Machine, Cloth',
                        'industry' => 'Textile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                48 =>
                    array (
                        'id' => 1547,
                        'groupno' => 491,
                        'name' => 'Stable Attendant',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                49 =>
                    array (
                        'id' => 1548,
                        'groupno' => 230,
                        'name' => 'Stamping Press Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                50 =>
                    array (
                        'id' => 1549,
                        'groupno' => 390,
                        'name' => 'Stand-In',
                        'industry' => 'Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                51 =>
                    array (
                        'id' => 1550,
                        'groupno' => 330,
                        'name' => 'Stapling Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                52 =>
                    array (
                        'id' => 1551,
                        'groupno' => 380,
                        'name' => 'Station Installer And Repairer',
                        'industry' => 'Telephone And Telegraph',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                53 =>
                    array (
                        'id' => 1552,
                        'groupno' => 332,
                        'name' => 'Stationary Engineer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                54 =>
                    array (
                        'id' => 1553,
                        'groupno' => 111,
                        'name' => 'Statistician, Applied',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                55 =>
                    array (
                        'id' => 1554,
                        'groupno' => 340,
                        'name' => 'Steam Cleaner',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                56 =>
                    array (
                        'id' => 1555,
                        'groupno' => 482,
                        'name' => 'Steel Erector',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                57 =>
                    array (
                        'id' => 1556,
                        'groupno' => 380,
                        'name' => 'Steel Plate Caulker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                58 =>
                    array (
                        'id' => 1557,
                        'groupno' => 482,
                        'name' => 'Steeple Jack',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                59 =>
                    array (
                        'id' => 1558,
                        'groupno' => 112,
                        'name' => 'Stenocaptioner',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                60 =>
                    array (
                        'id' => 1559,
                        'groupno' => 112,
                        'name' => 'Stenographer',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                61 =>
                    array (
                        'id' => 1560,
                        'groupno' => 112,
                        'name' => 'Stenotype Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                62 =>
                    array (
                        'id' => 1561,
                        'groupno' => 330,
                        'name' => 'Stereotype Caster & Molder',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                63 =>
                    array (
                        'id' => 1562,
                        'groupno' => 230,
                        'name' => 'Sterilizer',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                64 =>
                    array (
                        'id' => 1563,
                        'groupno' => 351,
                        'name' => 'Stevedore',
                        'industry' => 'Water Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                65 =>
                    array (
                        'id' => 1564,
                        'groupno' => 230,
                        'name' => 'Still Tender',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                66 =>
                    array (
                        'id' => 1565,
                        'groupno' => 230,
                        'name' => 'Stitcher, Standard Machine',
                        'industry' => 'Boot And Shoe',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                67 =>
                    array (
                        'id' => 1566,
                        'groupno' => 230,
                        'name' => 'Stitcher, Wire, Saddle And Side',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                68 =>
                    array (
                        'id' => 1567,
                        'groupno' => 214,
                        'name' => 'Stock Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                69 =>
                    array (
                        'id' => 1570,
                        'groupno' => 214,
                        'name' => 'Stock Clerk, Automotive Eqpt.',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                70 =>
                    array (
                        'id' => 1571,
                        'groupno' => 321,
                        'name' => 'Stone Carver',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                71 =>
                    array (
                        'id' => 1572,
                        'groupno' => 480,
                        'name' => 'Stone Driller',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                72 =>
                    array (
                        'id' => 1573,
                        'groupno' => 220,
                        'name' => 'Stone Setter',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                73 =>
                    array (
                        'id' => 1574,
                        'groupno' => 480,
                        'name' => 'Stone Splitter Operator',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                74 =>
                    array (
                        'id' => 1575,
                        'groupno' => 321,
                        'name' => 'Stonecutter, Hand',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                75 =>
                    array (
                        'id' => 1576,
                        'groupno' => 330,
                        'name' => 'Stonecutter, Machine',
                        'industry' => 'Stonework',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                76 =>
                    array (
                        'id' => 1577,
                        'groupno' => 380,
                        'name' => 'Stonemason',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                77 =>
                    array (
                        'id' => 1578,
                        'groupno' => 120,
                        'name' => 'Stoner',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                78 =>
                    array (
                        'id' => 1579,
                        'groupno' => 470,
                        'name' => 'Stove Refinisher',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                79 =>
                    array (
                        'id' => 1580,
                        'groupno' => 321,
                        'name' => 'Straightener, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                80 =>
                    array (
                        'id' => 1581,
                        'groupno' => 330,
                        'name' => 'Straightening Press Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                81 =>
                    array (
                        'id' => 1582,
                        'groupno' => 330,
                        'name' => 'Stranding Machine Operator',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                82 =>
                    array (
                        'id' => 1583,
                        'groupno' => 460,
                        'name' => 'Strapping Machine Operator',
                        'industry' => 'Wooden Container',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                83 =>
                    array (
                        'id' => 1584,
                        'groupno' => 340,
                        'name' => 'Street Cleaner/Sweeper, Manual',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                84 =>
                    array (
                        'id' => 1585,
                        'groupno' => 380,
                        'name' => 'Street Light Servicer',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                85 =>
                    array (
                        'id' => 1586,
                        'groupno' => 351,
                        'name' => 'Street Sweeper Operator',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                86 =>
                    array (
                        'id' => 1587,
                        'groupno' => 111,
                        'name' => 'Stress Analyst',
                        'industry' => 'Aircraft Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                87 =>
                    array (
                        'id' => 1588,
                        'groupno' => 212,
                        'name' => 'Stress Test Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                88 =>
                    array (
                        'id' => 1589,
                        'groupno' => 230,
                        'name' => 'Stretching Machine Tender, Frame',
                        'industry' => 'Leather Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                89 =>
                    array (
                        'id' => 1590,
                        'groupno' => 221,
                        'name' => 'Striper & Letterer, Hand, Motorcycles',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                90 =>
                    array (
                        'id' => 1591,
                        'groupno' => 331,
                        'name' => 'Stripper-Etcher, Printed Circuit Boards',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                91 =>
                    array (
                        'id' => 1592,
                        'groupno' => 482,
                        'name' => 'Structural Steel Worker',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                92 =>
                    array (
                        'id' => 1593,
                        'groupno' => 482,
                        'name' => 'Structural Steel Worker Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                93 =>
                    array (
                        'id' => 1594,
                        'groupno' => 380,
                        'name' => 'Stucco Mason',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                94 =>
                    array (
                        'id' => 1595,
                        'groupno' => 590,
                        'name' => 'Stunt Performer',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                95 =>
                    array (
                        'id' => 1596,
                        'groupno' => 320,
                        'name' => 'Subassembler',
                        'industry' => 'Machinery Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                96 =>
                    array (
                        'id' => 1597,
                        'groupno' => 332,
                        'name' => 'Substation Operator',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                97 =>
                    array (
                        'id' => 1598,
                        'groupno' => 250,
                        'name' => 'Subway Car Operator',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                98 =>
                    array (
                        'id' => 1599,
                        'groupno' => 332,
                        'name' => 'Supercalender Operator',
                        'industry' => 'Paper And Pulp',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                99 =>
                    array (
                        'id' => 1600,
                        'groupno' => 212,
                        'name' => 'Superintendent, Building',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                100 =>
                    array (
                        'id' => 1601,
                        'groupno' => 213,
                        'name' => 'Superintendent, Construction',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                101 =>
                    array (
                        'id' => 1602,
                        'groupno' => 212,
                        'name' => 'Superintendent, Plant Protection',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                102 =>
                    array (
                        'id' => 1603,
                        'groupno' => 360,
                        'name' => 'Supply Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                103 =>
                    array (
                        'id' => 1604,
                        'groupno' => 220,
                        'name' => 'Surgeon',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                104 =>
                    array (
                        'id' => 1605,
                        'groupno' => 230,
                        'name' => 'Surgical Dressing Maker, Machine',
                        'industry' => 'Protective Development',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                105 =>
                    array (
                        'id' => 1606,
                        'groupno' => 212,
                        'name' => 'Surgical Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                106 =>
                    array (
                        'id' => 1607,
                        'groupno' => 213,
                        'name' => 'Surveyor',
                        'industry' => 'Surveying/Cartographic',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                107 =>
                    array (
                        'id' => 1608,
                        'groupno' => 360,
                        'name' => 'Surveyor Helper',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                108 =>
                    array (
                        'id' => 1609,
                        'groupno' => 340,
                        'name' => 'Swimming Pool Servicer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                109 =>
                    array (
                        'id' => 1610,
                        'groupno' => 111,
                        'name' => 'Switchboard Operator, Police District',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Armed forces occupations ',
                    ),
                110 =>
                    array (
                        'id' => 1611,
                        'groupno' => 331,
                        'name' => 'Syrup Maker',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                111 =>
                    array (
                        'id' => 1612,
                        'groupno' => 111,
                        'name' => 'Systems Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                112 =>
                    array (
                        'id' => 1613,
                        'groupno' => 111,
                        'name' => 'Systems Programmer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                113 =>
                    array (
                        'id' => 1614,
                        'groupno' => 230,
                        'name' => 'Tacking Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                114 =>
                    array (
                        'id' => 1615,
                        'groupno' => 221,
                        'name' => 'Tailor, Alteration',
                        'industry' => 'Garment',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                115 =>
                    array (
                        'id' => 1616,
                        'groupno' => 221,
                        'name' => 'Tailor, Custom',
                        'industry' => 'Garment',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                116 =>
                    array (
                        'id' => 1617,
                        'groupno' => 460,
                        'name' => 'Tank Cleaner',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                117 =>
                    array (
                        'id' => 1618,
                        'groupno' => 380,
                        'name' => 'Taper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                118 =>
                    array (
                        'id' => 1619,
                        'groupno' => 120,
                        'name' => 'Taper, Printed Circuit Layout',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                119 =>
                    array (
                        'id' => 1620,
                        'groupno' => 330,
                        'name' => 'Tapping Machine Tender',
                        'industry' => 'Nut And Bolt',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                120 =>
                    array (
                        'id' => 1621,
                        'groupno' => 111,
                        'name' => 'Tax Clerk',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                121 =>
                    array (
                        'id' => 1622,
                        'groupno' => 111,
                        'name' => 'Tax Preparer',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                122 =>
                    array (
                        'id' => 1623,
                        'groupno' => 250,
                        'name' => 'Taxi Driver',
                        'industry' => 'Motor Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                123 =>
                    array (
                        'id' => 1624,
                        'groupno' => 311,
                        'name' => 'Taxidermist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                124 =>
                    array (
                        'id' => 1625,
                        'groupno' => 214,
                        'name' => 'Teacher Aide',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                125 =>
                    array (
                        'id' => 1626,
                        'groupno' => 212,
                        'name' => 'Teacher, Adult Education',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                126 =>
                    array (
                        'id' => 1627,
                        'groupno' => 214,
                        'name' => 'Teacher, Elementary School',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                127 =>
                    array (
                        'id' => 1628,
                        'groupno' => 214,
                        'name' => 'Teacher, Industrial Arts',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                128 =>
                    array (
                        'id' => 1629,
                        'groupno' => 214,
                        'name' => 'Teacher, Learning Disabled',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                129 =>
                    array (
                        'id' => 1630,
                        'groupno' => 214,
                        'name' => 'Teacher, Music',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                130 =>
                    array (
                        'id' => 1631,
                        'groupno' => 390,
                        'name' => 'Teacher, Physical Education',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                131 =>
                    array (
                        'id' => 1632,
                        'groupno' => 214,
                        'name' => 'Teacher, Physically Impaired',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                132 =>
                    array (
                        'id' => 1633,
                        'groupno' => 214,
                        'name' => 'Teacher, Preschool/Kindergarten',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                133 =>
                    array (
                        'id' => 1634,
                        'groupno' => 212,
                        'name' => 'Teacher, Secondary School',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                134 =>
                    array (
                        'id' => 1635,
                        'groupno' => 214,
                        'name' => 'Teacher, Vocational Training',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                135 =>
                    array (
                        'id' => 1636,
                        'groupno' => 120,
                        'name' => 'Technical Illustrator',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                136 =>
                    array (
                        'id' => 1637,
                        'groupno' => 112,
                        'name' => 'Telegraph Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                137 =>
                    array (
                        'id' => 1638,
                        'groupno' => 112,
                        'name' => 'Telephone Answering Service Operator',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                138 =>
                    array (
                        'id' => 1639,
                        'groupno' => 350,
                        'name' => 'Telephone Directory Deliverer',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                139 =>
                    array (
                        'id' => 1640,
                        'groupno' => 112,
                        'name' => 'Telephone Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                140 =>
                    array (
                        'id' => 1641,
                        'groupno' => 320,
                        'name' => 'Television And Radio Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                141 =>
                    array (
                        'id' => 1642,
                        'groupno' => 111,
                        'name' => 'Television Console Monitor',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                142 =>
                    array (
                        'id' => 1643,
                        'groupno' => 380,
                        'name' => 'Television Receiver/Antenna Installer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                143 =>
                    array (
                        'id' => 1644,
                        'groupno' => 470,
                        'name' => 'Television Technician',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                144 =>
                    array (
                        'id' => 1645,
                        'groupno' => 211,
                        'name' => 'Teller',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                145 =>
                    array (
                        'id' => 1646,
                        'groupno' => 214,
                        'name' => 'Teller, Vault',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                146 =>
                    array (
                        'id' => 1647,
                        'groupno' => 320,
                        'name' => 'Template Maker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                147 =>
                    array (
                        'id' => 1648,
                        'groupno' => 380,
                        'name' => 'Terrazzo Installer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                148 =>
                    array (
                        'id' => 1649,
                        'groupno' => 480,
                        'name' => 'Terrazzo Installer Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                149 =>
                    array (
                        'id' => 1650,
                        'groupno' => 220,
                        'name' => 'Test Tech, Semiconductor Processing Equipment',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                150 =>
                    array (
                        'id' => 1651,
                        'groupno' => 320,
                        'name' => 'Tester, Nondestructive',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                151 =>
                    array (
                        'id' => 1652,
                        'groupno' => 212,
                        'name' => 'Testing Machine Operator, Metal',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                152 =>
                    array (
                        'id' => 1653,
                        'groupno' => 370,
                        'name' => 'Thermal Cutter, Hand',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                153 =>
                    array (
                        'id' => 1654,
                        'groupno' => 330,
                        'name' => 'Thermal Cutting-Machine Operator',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                154 =>
                    array (
                        'id' => 1655,
                        'groupno' => 320,
                        'name' => 'Thermostat Repairer',
                        'industry' => 'Instalation And Application',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                155 =>
                    array (
                        'id' => 1656,
                        'groupno' => 221,
                        'name' => 'Thread Cutter, Hand Or Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                156 =>
                    array (
                        'id' => 1657,
                        'groupno' => 330,
                        'name' => 'Threading Machine Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                157 =>
                    array (
                        'id' => 1658,
                        'groupno' => 321,
                        'name' => 'Thrower',
                        'industry' => 'Pottery And Porcelain',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                158 =>
                    array (
                        'id' => 1659,
                        'groupno' => 212,
                        'name' => 'Ticket Agent',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                159 =>
                    array (
                        'id' => 1660,
                        'groupno' => 213,
                        'name' => 'Ticket Inspector, Transportation',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                160 =>
                    array (
                        'id' => 1661,
                        'groupno' => 230,
                        'name' => 'Ticket Printer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                161 =>
                    array (
                        'id' => 1662,
                        'groupno' => 240,
                        'name' => 'Ticket Taker',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                162 =>
                    array (
                        'id' => 1663,
                        'groupno' => 330,
                        'name' => 'Tile Maker',
                        'industry' => 'Brick And Tile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                163 =>
                    array (
                        'id' => 1664,
                        'groupno' => 380,
                        'name' => 'Tile Setter',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                164 =>
                    array (
                        'id' => 1665,
                        'groupno' => 480,
                        'name' => 'Tile Setter Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                165 =>
                    array (
                        'id' => 1666,
                        'groupno' => 330,
                        'name' => 'Timber-Sizer Operator',
                        'industry' => 'Sawmill And Planer Mill',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                166 =>
                    array (
                        'id' => 1667,
                        'groupno' => 212,
                        'name' => 'Time And Motion Study Analyst',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                167 =>
                    array (
                        'id' => 1668,
                        'groupno' => 321,
                        'name' => 'Tire Builder, Automobile',
                        'industry' => 'Rubber Tire',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                168 =>
                    array (
                        'id' => 1669,
                        'groupno' => 460,
                        'name' => 'Tire Changer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                169 =>
                    array (
                        'id' => 1670,
                        'groupno' => 460,
                        'name' => 'Tire Molder',
                        'industry' => 'Rubber Tire',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                170 =>
                    array (
                        'id' => 1671,
                        'groupno' => 321,
                        'name' => 'Tire Recapper',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                171 =>
                    array (
                        'id' => 1672,
                        'groupno' => 460,
                        'name' => 'Tire Repairer',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                172 =>
                    array (
                        'id' => 1673,
                        'groupno' => 420,
                        'name' => 'Tire Trimmer, Hand',
                        'industry' => 'Rubber Tire',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                173 =>
                    array (
                        'id' => 1674,
                        'groupno' => 211,
                        'name' => 'Title Searcher',
                        'industry' => 'Real Estate',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                174 =>
                    array (
                        'id' => 1675,
                        'groupno' => 211,
                        'name' => 'Toll Collector',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Managers',
                    ),
                175 =>
                    array (
                        'id' => 1676,
                        'groupno' => 360,
                        'name' => 'Tool And Equipment Rental Clerk',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                176 =>
                    array (
                        'id' => 1677,
                        'groupno' => 360,
                        'name' => 'Tool Crib Attendant',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                177 =>
                    array (
                        'id' => 1678,
                        'groupno' => 220,
                        'name' => 'Tool Designer',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                178 =>
                    array (
                        'id' => 1679,
                        'groupno' => 330,
                        'name' => 'Tool Dresser',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                179 =>
                    array (
                        'id' => 1680,
                        'groupno' => 320,
                        'name' => 'Tool Maker',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                180 =>
                    array (
                        'id' => 1681,
                        'groupno' => 320,
                        'name' => 'Tool Maker, Bench',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                181 =>
                    array (
                        'id' => 1682,
                        'groupno' => 120,
                        'name' => 'Tool Programmer, Numerical Control',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                182 =>
                    array (
                        'id' => 1683,
                        'groupno' => 430,
                        'name' => 'Torch Straightener And Heater',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                183 =>
                    array (
                        'id' => 1684,
                        'groupno' => 221,
                        'name' => 'Touch-Up Painter, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                184 =>
                    array (
                        'id' => 1685,
                        'groupno' => 482,
                        'name' => 'Tower Erector',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                185 =>
                    array (
                        'id' => 1686,
                        'groupno' => 212,
                        'name' => 'Toxicologist',
                        'industry' => 'Pharmaceutical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                186 =>
                    array (
                        'id' => 1687,
                        'groupno' => 221,
                        'name' => 'Toy Assembler',
                        'industry' => 'Toy-Sport Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                187 =>
                    array (
                        'id' => 1688,
                        'groupno' => 351,
                        'name' => 'Tractor Crane Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                188 =>
                    array (
                        'id' => 1689,
                        'groupno' => 351,
                        'name' => 'Tractor Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                189 =>
                    array (
                        'id' => 1690,
                        'groupno' => 111,
                        'name' => 'Traffic Clerk',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                190 =>
                    array (
                        'id' => 1691,
                        'groupno' => 212,
                        'name' => 'Traffic Engineer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                191 =>
                    array (
                        'id' => 1692,
                        'groupno' => 490,
                        'name' => 'Traffic Officer',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                192 =>
                    array (
                        'id' => 1693,
                        'groupno' => 111,
                        'name' => 'Train Dispatcher',
                        'industry' => 'Road And Rail Transportation',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                193 =>
                    array (
                        'id' => 1694,
                        'groupno' => 112,
                        'name' => 'Transcribing Machine Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                194 =>
                    array (
                        'id' => 1695,
                        'groupno' => 370,
                        'name' => 'Transformer Assembler',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                195 =>
                    array (
                        'id' => 1696,
                        'groupno' => 111,
                        'name' => 'Translator, Documents',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                196 =>
                    array (
                        'id' => 1697,
                        'groupno' => 492,
                        'name' => 'Tree Cutter',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                197 =>
                    array (
                        'id' => 1698,
                        'groupno' => 491,
                        'name' => 'Tree Pruner, Low Level/Bucket',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                198 =>
                    array (
                        'id' => 1699,
                        'groupno' => 482,
                        'name' => 'Tree Surgeon',
                        'industry' => 'Agriculture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                199 =>
                    array (
                        'id' => 1700,
                        'groupno' => 482,
                        'name' => 'Tree Trimmer',
                        'industry' => 'Telephone And Telegraph',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                200 =>
                    array (
                        'id' => 1701,
                        'groupno' => 230,
                        'name' => 'Trimmer, Machine',
                        'industry' => 'Garment',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                201 =>
                    array (
                        'id' => 1702,
                        'groupno' => 322,
                        'name' => 'Trimmer, Meat',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                202 =>
                    array (
                        'id' => 1703,
                        'groupno' => 221,
                        'name' => 'Trophy Assembler',
                        'industry' => 'Jewelry-Silver Clay',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                203 =>
                    array (
                        'id' => 1704,
                        'groupno' => 350,
                        'name' => 'Truck Driver',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                204 =>
                    array (
                        'id' => 1705,
                        'groupno' => 460,
                        'name' => 'Truck Driver Helper',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                205 =>
                    array (
                        'id' => 1706,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Concrete Mixing',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                206 =>
                    array (
                        'id' => 1707,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Dump Truck',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                207 =>
                    array (
                        'id' => 1708,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Garbage',
                        'industry' => 'Motor Transport',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                208 =>
                    array (
                        'id' => 1709,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Logs',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                209 =>
                    array (
                        'id' => 1710,
                        'groupno' => 351,
                        'name' => 'Truck Driver, Road Oiling',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                210 =>
                    array (
                        'id' => 1711,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Sales Route',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                211 =>
                    array (
                        'id' => 1712,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Tank Truck',
                        'industry' => 'Petrol Refinery',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                212 =>
                    array (
                        'id' => 1713,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Tow Truck',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                213 =>
                    array (
                        'id' => 1714,
                        'groupno' => 350,
                        'name' => 'Truck Driver, Tractor-Trailer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                214 =>
                    array (
                        'id' => 1715,
                        'groupno' => 460,
                        'name' => 'Truck Loader',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                215 =>
                    array (
                        'id' => 1716,
                        'groupno' => 380,
                        'name' => 'Truss Builder, Construction',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                216 =>
                    array (
                        'id' => 1717,
                        'groupno' => 320,
                        'name' => 'Tube Assembler, Cathode Ray',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                217 =>
                    array (
                        'id' => 1718,
                        'groupno' => 221,
                        'name' => 'Tube Bender, Hand',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                218 =>
                    array (
                        'id' => 1719,
                        'groupno' => 341,
                        'name' => 'Tube Cleaner',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                219 =>
                    array (
                        'id' => 1720,
                        'groupno' => 330,
                        'name' => 'Tubular Furniture Maker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                220 =>
                    array (
                        'id' => 1721,
                        'groupno' => 111,
                        'name' => 'Tumor Registrar',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                221 =>
                    array (
                        'id' => 1722,
                        'groupno' => 332,
                        'name' => 'Turbine Attendant',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                222 =>
                    array (
                        'id' => 1723,
                        'groupno' => 332,
                        'name' => 'Turbine Operator',
                        'industry' => 'Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                223 =>
                    array (
                        'id' => 1724,
                        'groupno' => 330,
                        'name' => 'Turret Lathe Operator',
                        'industry' => 'Machine Shop',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                224 =>
                    array (
                        'id' => 1725,
                        'groupno' => 212,
                        'name' => 'Tutor',
                        'industry' => 'Education',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                225 =>
                    array (
                        'id' => 1726,
                        'groupno' => 221,
                        'name' => 'Typesetter/Compositor',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                226 =>
                    array (
                        'id' => 1727,
                        'groupno' => 230,
                        'name' => 'Typesetting Machine Tender',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                227 =>
                    array (
                        'id' => 1728,
                        'groupno' => 112,
                        'name' => 'Typist',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                228 =>
                    array (
                        'id' => 1729,
                        'groupno' => 212,
                        'name' => 'Ultrasound Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                229 =>
                    array (
                        'id' => 1730,
                        'groupno' => 214,
                        'name' => 'Umpire',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                230 =>
                    array (
                        'id' => 1731,
                        'groupno' => 110,
                        'name' => 'Underwriter, Mortgage Loan',
                        'industry' => 'Financial',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                231 =>
                    array (
                        'id' => 1732,
                        'groupno' => 321,
                        'name' => 'Upholstery Repairer',
                        'industry' => 'Furniture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                232 =>
                    array (
                        'id' => 1733,
                        'groupno' => 110,
                        'name' => 'Urban Planner',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                233 =>
                    array (
                        'id' => 1734,
                        'groupno' => 370,
                        'name' => 'Used Car Renovator',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                234 =>
                    array (
                        'id' => 1735,
                        'groupno' => 240,
                        'name' => 'Usher',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                235 =>
                    array (
                        'id' => 1736,
                        'groupno' => 330,
                        'name' => 'Utility Operator',
                        'industry' => 'Sawmill And Planer Mill',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                236 =>
                    array (
                        'id' => 1737,
                        'groupno' => 351,
                        'name' => 'Vacuum Cleaner Operator, Industrial',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                237 =>
                    array (
                        'id' => 1738,
                        'groupno' => 320,
                        'name' => 'Vacuum Cleaner Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                238 =>
                    array (
                        'id' => 1739,
                        'groupno' => 250,
                        'name' => 'Valet, Parking',
                        'industry' => 'Automotive Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                239 =>
                    array (
                        'id' => 1740,
                        'groupno' => 330,
                        'name' => 'Variety Saw Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                240 =>
                    array (
                        'id' => 1741,
                        'groupno' => 112,
                        'name' => 'Varitype Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                241 =>
                    array (
                        'id' => 1742,
                        'groupno' => 214,
                        'name' => 'Vault Cashier',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                242 =>
                    array (
                        'id' => 1743,
                        'groupno' => 213,
                        'name' => 'Vendor',
                        'industry' => 'Amusement And Recreation; Motion Picture',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                243 =>
                    array (
                        'id' => 1744,
                        'groupno' => 340,
                        'name' => 'Venetian Blind Cleaner And Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                244 =>
                    array (
                        'id' => 1745,
                        'groupno' => 311,
                        'name' => 'Veterinarian',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Professionals',
                    ),
                245 =>
                    array (
                        'id' => 1746,
                        'groupno' => 311,
                        'name' => 'Veterinarian, Laboratory Animal Care',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                246 =>
                    array (
                        'id' => 1747,
                        'groupno' => 311,
                        'name' => 'Veterinary Technician',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                247 =>
                    array (
                        'id' => 1748,
                        'groupno' => 212,
                        'name' => 'Videotape Operator, Studio',
                        'industry' => 'Radio-Tv Broadcasting',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                248 =>
                    array (
                        'id' => 1749,
                        'groupno' => 110,
                        'name' => 'Vocational Rehabilitation Consultant',
                        'industry' => 'Government Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                249 =>
                    array (
                        'id' => 1750,
                        'groupno' => 212,
                        'name' => 'Voice Pathologist',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                250 =>
                    array (
                        'id' => 1751,
                        'groupno' => 221,
                        'name' => 'Wafer Fab Operator',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                251 =>
                    array (
                        'id' => 1752,
                        'groupno' => 322,
                        'name' => 'Waiter/Waitress',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                252 =>
                    array (
                        'id' => 1753,
                        'groupno' => 480,
                        'name' => 'Wallpaper Remover, Steam',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                253 =>
                    array (
                        'id' => 1754,
                        'groupno' => 360,
                        'name' => 'Warehouse Worker',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                254 =>
                    array (
                        'id' => 1755,
                        'groupno' => 331,
                        'name' => 'Washer, Machine',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                255 =>
                    array (
                        'id' => 1756,
                        'groupno' => 340,
                        'name' => 'Washer, Machine',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                256 =>
                    array (
                        'id' => 1757,
                        'groupno' => 460,
                        'name' => 'Washing Machine Loader And Puller',
                        'industry' => 'Laundry And Related Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                257 =>
                    array (
                        'id' => 1758,
                        'groupno' => 460,
                        'name' => 'Waste Disposal Attendant, Radioactive',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                258 =>
                    array (
                        'id' => 1759,
                        'groupno' => 332,
                        'name' => 'Waste Treatment Operator',
                        'industry' => 'Chemical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                259 =>
                    array (
                        'id' => 1760,
                        'groupno' => 332,
                        'name' => 'Wastewater Treatment Plant Operator',
                        'industry' => 'Sanitary Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                260 =>
                    array (
                        'id' => 1761,
                        'groupno' => 220,
                        'name' => 'Watch Repairer',
                        'industry' => 'Clock And Watch',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                261 =>
                    array (
                        'id' => 1762,
                        'groupno' => 380,
                        'name' => 'Water Meter Installer',
                        'industry' => 'Waterworks',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                262 =>
                    array (
                        'id' => 1763,
                        'groupno' => 332,
                        'name' => 'Water Pump Tender',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                263 =>
                    array (
                        'id' => 1764,
                        'groupno' => 460,
                        'name' => 'Water Softener Servicer And Installer',
                        'industry' => 'Business Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                264 =>
                    array (
                        'id' => 1765,
                        'groupno' => 332,
                        'name' => 'Water Treatment Plant Operator',
                        'industry' => 'Waterworks',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                265 =>
                    array (
                        'id' => 1766,
                        'groupno' => 481,
                        'name' => 'Waysman',
                        'industry' => 'Ship-Boat Manufacturing',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                266 =>
                    array (
                        'id' => 1767,
                        'groupno' => 230,
                        'name' => 'Weaver, Textile',
                        'industry' => 'Non-Ferrous Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                267 =>
                    array (
                        'id' => 1768,
                        'groupno' => 330,
                        'name' => 'Web Press Operator',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                268 =>
                    array (
                        'id' => 1769,
                        'groupno' => 330,
                        'name' => 'Web Press Operator Helper, Offset',
                        'industry' => 'Printing And Publication',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                269 =>
                    array (
                        'id' => 1770,
                        'groupno' => 360,
                        'name' => 'Weigher, Production',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                270 =>
                    array (
                        'id' => 1771,
                        'groupno' => 214,
                        'name' => 'Weigher, Shipping And Receiving',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                271 =>
                    array (
                        'id' => 1772,
                        'groupno' => 240,
                        'name' => 'Weight Reduction Specialist',
                        'industry' => 'Personal Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                272 =>
                    array (
                        'id' => 1773,
                        'groupno' => 460,
                        'name' => 'Welder Helper',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                273 =>
                    array (
                        'id' => 1774,
                        'groupno' => 380,
                        'name' => 'Welder-Fitter',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                274 =>
                    array (
                        'id' => 1775,
                        'groupno' => 430,
                        'name' => 'Welder, Arc',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                275 =>
                    array (
                        'id' => 1776,
                        'groupno' => 370,
                        'name' => 'Welder, Combination',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                276 =>
                    array (
                        'id' => 1777,
                        'groupno' => 370,
                        'name' => 'Welder, Gas',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                277 =>
                    array (
                        'id' => 1778,
                        'groupno' => 370,
                        'name' => 'Welder, Gun',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                278 =>
                    array (
                        'id' => 1779,
                        'groupno' => 370,
                        'name' => 'Welder, Production Line',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                279 =>
                    array (
                        'id' => 1780,
                        'groupno' => 430,
                        'name' => 'Welder, Tack',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                280 =>
                    array (
                        'id' => 1781,
                        'groupno' => 330,
                        'name' => 'Welding Machine Operator, Arc',
                        'industry' => 'Welding',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                281 =>
                    array (
                        'id' => 1782,
                        'groupno' => 480,
                        'name' => 'Well Digger',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                282 =>
                    array (
                        'id' => 1783,
                        'groupno' => 480,
                        'name' => 'Well Drill Operator',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                283 =>
                    array (
                        'id' => 1784,
                        'groupno' => 480,
                        'name' => 'Well Drill Operator Helper',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                284 =>
                    array (
                        'id' => 1785,
                        'groupno' => 480,
                        'name' => 'Well Puller',
                        'industry' => 'Petrol. And Gas',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                285 =>
                    array (
                        'id' => 1786,
                        'groupno' => 320,
                        'name' => 'Wheel Lacer And Truer',
                        'industry' => 'Motor-Bicycles',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                286 =>
                    array (
                        'id' => 1787,
                        'groupno' => 482,
                        'name' => 'Wind Generating Electric Power Installer',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                287 =>
                    array (
                        'id' => 1788,
                        'groupno' => 482,
                        'name' => 'Wind Turbine Technician',
                        'industry' => 'Construction; Utilities',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                288 =>
                    array (
                        'id' => 1789,
                        'groupno' => 330,
                        'name' => 'Winder',
                        'industry' => 'Paper Goods',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                289 =>
                    array (
                        'id' => 1790,
                        'groupno' => 460,
                        'name' => 'Winder Operator, Floor Coverings',
                        'industry' => 'Fabrication, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                290 =>
                    array (
                        'id' => 1791,
                        'groupno' => 230,
                        'name' => 'Winder, Magnetic Tape',
                        'industry' => 'Recording',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                291 =>
                    array (
                        'id' => 1792,
                        'groupno' => 330,
                        'name' => 'Winder, Yarn',
                        'industry' => 'Textile Production, Nec',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                292 =>
                    array (
                        'id' => 1793,
                        'groupno' => 330,
                        'name' => 'Winding-Machine Operator, Cloth',
                        'industry' => 'Textile',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                293 =>
                    array (
                        'id' => 1794,
                        'groupno' => 341,
                        'name' => 'Window Cleaner',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Elementary Occupations',
                    ),
                294 =>
                    array (
                        'id' => 1795,
                        'groupno' => 380,
                        'name' => 'Window Repairer',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                295 =>
                    array (
                        'id' => 1796,
                        'groupno' => 213,
                        'name' => 'Wine Maker',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                296 =>
                    array (
                        'id' => 1797,
                        'groupno' => 240,
                        'name' => 'Wine Steward/Stewardess',
                        'industry' => 'Hotel And Restaurant',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                297 =>
                    array (
                        'id' => 1798,
                        'groupno' => 332,
                        'name' => 'Winery Worker',
                        'industry' => 'Beverage',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                298 =>
                    array (
                        'id' => 1799,
                        'groupno' => 330,
                        'name' => 'Wire Drawing Machine Tender',
                        'industry' => 'Non-Ferrous Metal',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                299 =>
                    array (
                        'id' => 1800,
                        'groupno' => 221,
                        'name' => 'Wire Harness Assembler',
                        'industry' => 'Electricity Equipments',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                300 =>
                    array (
                        'id' => 1801,
                        'groupno' => 230,
                        'name' => 'Wire Wrapping Machine Operator',
                        'industry' => 'Electronic Computer',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                301 =>
                    array (
                        'id' => 1802,
                        'groupno' => 330,
                        'name' => 'Wood-Carving Machine Operator',
                        'industry' => 'Woodworking',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                302 =>
                    array (
                        'id' => 1803,
                        'groupno' => 321,
                        'name' => 'Wool And Pelt Grader',
                        'industry' => 'Meat Products',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Craft and Related Trades Workers',
                    ),
                303 =>
                    array (
                        'id' => 1804,
                        'groupno' => 112,
                        'name' => 'Word Processing Machine Operator',
                        'industry' => 'Clerical',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                304 =>
                    array (
                        'id' => 1805,
                        'groupno' => 330,
                        'name' => 'Wrapping Machine Operator',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                305 =>
                    array (
                        'id' => 1806,
                        'groupno' => 480,
                        'name' => 'Wrecker, Construction',
                        'industry' => 'Construction',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                306 =>
                    array (
                        'id' => 1807,
                        'groupno' => 112,
                        'name' => 'Writer, Prose, Fiction And Nonfiction',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Clerical Support Workers',
                    ),
                307 =>
                    array (
                        'id' => 1808,
                        'groupno' => 112,
                        'name' => 'Writer, Technical Publications',
                        'industry' => 'Professional And Kindred',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                308 =>
                    array (
                        'id' => 1809,
                        'groupno' => 212,
                        'name' => 'Xray Operator, Industrial',
                        'industry' => 'Any Industry',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
                309 =>
                    array (
                        'id' => 1810,
                        'groupno' => 310,
                        'name' => 'Xray Technologist',
                        'industry' => 'Medical Services',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Technicians and Associate Professionals',
                    ),
                310 =>
                    array (
                        'id' => 1811,
                        'groupno' => 460,
                        'name' => 'Yard Attendant, Building Materials',
                        'industry' => 'Retail Trade',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Services and Sales Workers',
                    ),
                311 =>
                    array (
                        'id' => 1812,
                        'groupno' => 351,
                        'name' => 'Yarder Operator, Fixed/Portable',
                        'industry' => 'Logging',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                        'tasco'=>'Plant and Machine Operators and Assemblers',
                    ),
            ));

            $this->enableForeignKeys('occupations');

$sql = <<<SQL
DROP TRIGGER IF EXISTS occupations_ro ON occupations;
create trigger occupations_ro before insert or update or delete or truncate on occupations for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);

        }

    }

}