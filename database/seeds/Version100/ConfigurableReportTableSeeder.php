<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Reporting\ConfigurableReport;

class ConfigurableReportTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    public function __construct()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    public function run()
    {

        include 'resources/views/backend/report/configurable/scripts/t1/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t2/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t3/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t4/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t5/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t6/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t7/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t8/Seeder.php';
        include 'resources/views/backend/report/configurable/scripts/t9/Seeder.php';

    }

}