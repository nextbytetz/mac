<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class HealthStateChecklistsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('health_state_checklists');
        $this->delete('health_state_checklists');

        \DB::table('health_state_checklists')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'name' => 'Completely Recovered (Returned to work with no LD)',
                    'created_at' => '2017-04-19 09:58:00',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 1,
                ),
            1 =>
                array(
                    'id' => 2,
                    'name' => 'Attending Hospital/Specialized Clinic (Hospitalized)',
                    'created_at' => '2017-04-19 09:58:00',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 1,
                ),
            2 =>
                array(
                    'id' => 3,
                    'name' => 'Permanent loss of body part or function(s)',
                    'created_at' => '2017-04-19 09:58:50',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 0,
                ),
            3 =>
                array(
                    'id' => 4,
                    'name' => 'Returned to Work with no LD',
                    'created_at' => '2017-04-19 09:58:50',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 1,
                ),
            4 =>
                array(
                    'id' => 5,
                    'name' => 'Refused to continue with treatment',
                    'created_at' => '2017-04-19 09:58:50',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 1,
                ),
            5 =>
                array(
                    'id' => 6,
                    'name' => 'Administration of estate, the case is still on progress (Court has not given decision)',
                    'created_at' => '2017-04-19 09:58:50',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 1,
                ),
            6 =>
                array(
                    'id' => 7,
                    'name' => 'Returned to Work with LD  and attending hospital/specialized clinic',
                    'created_at' => '2017-04-19 09:58:50',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 1,
                ),
            7 =>
                array(
                    'id' => 8,
                    'name' => 'Not returned to work on ED',
                    'created_at' => '2017-04-19 09:58:50',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'isactive' => 1,
                ),
        ));

        $this->enableForeignKeys('health_state_checklists');
    }
}