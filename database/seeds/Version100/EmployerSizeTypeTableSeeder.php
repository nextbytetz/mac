<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Operation\Compliance\Member\EmployerSizeType;

class EmployerSizeTypeTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    public function run()
    {

//        1
        $code= EmployerSizeType::updateOrCreate(
            ['id' => 1],
            ['name' => 'Empty Number',
                'min_range' => null,
                'max_range' => null,
                'description' => 'Not Declared',
                'created_at' => '2017-10-29 17:30:35',
            ]
        );

//        2
        $code= EmployerSizeType::updateOrCreate(
            ['id' => 2],
            ['name' => 'Micro Employers',
                'min_range' => 0,
                'max_range' => 0,
                'description' => 'Zero Employee',
                'created_at' => '2017-10-29 17:30:35',
            ]
        );


        //        3
        $code= EmployerSizeType::updateOrCreate(
            ['id' => 3],
            ['name' => 'Small Employers',
                'min_range' => 1,
                'max_range' => 100,
                'description' => '1-100',
                'created_at' => '2017-10-29 17:30:36',
            ]
        );


//        4
        $code= EmployerSizeType::updateOrCreate(
            ['id' => 4],
            ['name' => 'Medium Employers',
                'min_range' => 101,
                'max_range' => 1000,
                'description' => '101-1000',
                'created_at' => '2017-10-29 17:30:36',
            ]
        );

//        5
        $code= EmployerSizeType::updateOrCreate(
            ['id' => 5],
            ['name' => 'Large Employers',
                'min_range' => 1001,
                'max_range' => null,
                'description' => 'Greater than 1000',
                'created_at' => '2017-10-29 17:30:36',
            ]
        );

    }

}