<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Sysdef\Sysdef;

class SysdefsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        //$this->disableForeignKeys('sysdefs');
        //$this->delete('sysdefs');

        $code = Sysdef::query()->updateOrCreate(
            ['id' => 1],
            [
                'total_allowed_contrib_months' => '12',
                'minimum_monthly_pension' => '275702.83',
                'maximum_monthly_pension' => '3685852.69',
                'minimum_lumpsum_amount' => '551405.66',
                'maximum_lumpsum_amount' => '7371705.38',
                'funeral_grant_amount' => '400000.00',
                'percentage_of_earning' => '70.00',
                'assistant_percent' => '40.00',
                'wife_spouse_percent' => '40.00',
                'child_percent_with_spouse' => '20.00',
                'child_percent_without_spouse' => '40.00',
                'constant_factor_compensation' => '84',
                'percentage_imparement_scale' => '30.00',
                'permanent_partial_mp_period' => '-1',
                'contribution_penalty_percent' => '10.00',
                'contribution_grace_period' => '1',
                'public_contribution_percent' => '0.50',
                'private_contribution_percent' => '1.00',
                'cancel_receipt_days' => '90',
                'created_at' => '2017-04-18 14:32:05',
                'updated_at' => NULL,
                'no_of_days_for_a_month' => 30,
                'no_of_days_for_a_month_in_assessment' => 30.3333,
                'min_days_for_ttd_tpd' => 3,
                'employee_number_length' => 8,
                'receipt_number_length' => 8,
                'minimum_allowed_contribution' => 50,
                'day_hours' => 8,
                'payment_voucher_reference' => 'WCF.HA.263/477/01/',
                'notification_report_reference' => 'AB1/457/886/',
                'days_eligible_for_claim_assessment' => 4,
                'pension_verification_limit_days' => 180,
                'periodical_verification_limit_days' => 30,
                'payroll_child_limit_age' => 18,
                'payroll_child_education_limit_age' => 21,
                'payroll_suspension_alert_months' => 2,
                'payroll_voluntary_retire_age' => 55,
                'payroll_compulsory_retire_age' => 60,
                'minimum_allowed_cca' => 85,
                'min_payable_otherdep_full' => 110281.13 ,
                'max_payable_otherdep_full' => 1474341.08,
                'max_payable_months_otherdep_full' => 24,
                'otherdep_full_pension_percent' => 40,

            ]
        );

        //$this->enableForeignKeys('sysdefs');
    }
}