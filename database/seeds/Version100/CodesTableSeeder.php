<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Sysdef\Code;

class CodesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    public function run()
    {
        //$this->disableForeignKeys();


        //        1
        $code= Code::query()->updateOrCreate(
            ['id' => 1],
            ['name' => 'Incident Exposures', 'is_system_defined' => 0]
        );

        //       2
        $code= Code::query()->updateOrCreate(
            ['id' => 2],
            ['name' => 'Body Part Injuries', 'is_system_defined' => 0]
        );


        //       3
        $code= Code::query()->updateOrCreate(
            ['id' => 3],
            ['name' => 'Nature Of Incident', 'is_system_defined' => 0]
        );

        //       4
        $code= Code::query()->updateOrCreate(
            ['id' => 4],
            ['name' => 'Employer Categories', 'is_system_defined' => 0]
        );

        //      5
        $code = Code::query()->updateOrCreate(
            ['id' => 5],
            ['name' => 'Business Sectors', 'is_system_defined' => 0]
        );

        //      6
        $code = Code::query()->updateOrCreate(
            ['id' => 6],
            ['name' => 'Authorities', 'is_system_defined' => 0]
        );

        //      7
        $code = Code::query()->updateOrCreate(
            ['id' => 7],
            ['name' => 'Employee Categories', 'is_system_defined' => 0]
        );

        //      8
        $code = Code::query()->updateOrCreate(
            ['id' => 8],
            ['name' => 'Marital Status', 'is_system_defined' => 0]
        );


        //      9
        $code = Code::query()->updateOrCreate(
            ['id' => 9],
            ['name' => 'Follow Up Types', 'is_system_defined' => 0]
        );

        //      10
        $code = Code::query()->updateOrCreate(
            ['id' => 10],
            ['name' => 'Payment Modes', 'is_system_defined' => 0]
        );

        //      11
        $code = Code::query()->updateOrCreate(
            ['id' => 11],
            ['name' => 'Annual Target Types', 'is_system_defined' => 0]
        );

        //      12
        $code = Code::query()->updateOrCreate(
            ['id' => 12],
            ['name' => 'Incident Occurrence', 'is_system_defined' => 0]
        );

        //      13
        $code = Code::query()->updateOrCreate(
            ['id' => 13],
            ['name' => 'Notification on Progress', 'is_system_defined' => 0]
        );

        //    14
        $code = Code::query()->updateOrCreate(
            ['id' => 14],
            ['name' => 'Checker Categories', 'is_system_defined' => 1]
        );
        //    15
        $code = Code::query()->updateOrCreate(
            ['id' => 15],
            ['name' => 'Accident Causes', 'is_system_defined' => 1]
        );
        //    16
        $code = Code::query()->updateOrCreate(
            ['id' => 16],
            ['name' => 'Disease Causes', 'is_system_defined' => 1]
        );
        //    17
        $code = Code::query()->updateOrCreate(
            ['id' => 17],
            ['name' => 'Accident Incident Exposures', 'is_system_defined' => 1]
        );
        //    18
        $code = Code::query()->updateOrCreate(
            ['id' => 18],
            ['name' => 'Disease Incident Exposures', 'is_system_defined' => 1]
        );
        //    19
        $code = Code::query()->updateOrCreate(
            ['id' => 19],
            ['name' => 'Death Incident Exposures', 'is_system_defined' => 1]
        );
        //    20
        $code = Code::query()->updateOrCreate(
            ['id' => 20],
            ['name' => 'Lost Body Parts', 'is_system_defined' => 1]
        );
        //      15-->21
        $code = Code::query()->updateOrCreate(
            ['id' => 21],
            ['name' => 'Payroll Status Changes', 'is_system_defined' => 1]
        );
        //      16-->22
        $code = Code::query()->updateOrCreate(
            ['id' => 22],
            ['name' => 'Payroll Recovery types', 'is_system_defined' => 1]
        );
        //      23
        $code = Code::query()->updateOrCreate(
            ['id' => 23],
            ['name' => 'Notification Rejection', 'is_system_defined' => 0]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 24],
            ['name' => 'Notification Cancellation', 'is_system_defined' => 0]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 25],
            ['name' => 'Staff Employer follow ups feedback', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 26],
            ['name' => 'Notification & Claim Letters', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 27],
            ['name' => 'Compliance Letters', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 28],
            ['name' => 'Official Letter Logs', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 29],
            ['name' => 'Inspection Types', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 30],
            ['name' => 'Inspection Triggers', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 31],
            ['name' => 'Special Inspection Category', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 32],
            ['name' => 'Routine Inspection Category', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 33],
            ['name' => 'Inspection on Progress', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 34],
            ['name' => 'Inspection Rejection', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 35],
            ['name' => 'Inspection Cancellation', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 36],
            ['name' => 'Compliance Defaults', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 37],
            ['name' => 'Notification Defaults', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 38],
            ['name' => 'Employer Inspection Task On Progress', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 39],
            ['name' => 'Employer Inspection Task Cancellation', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 40],
            ['name' => 'Official Letter on Progress', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 41],
            ['name' => 'Checker Priority', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 42],
            ['name' => 'Document Resources', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 43],
            ['name' => 'Employer type of Particular Changes', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 44],
            ['name' => 'Employer name change types', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 45],
            ['name' => 'Registration Authorities', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(

            ['id' => 46],
            ['name' => 'Incident User Categories', 'is_system_defined' => 1]
        );


        $code = Code::query()->updateOrCreate(
            ['id' => 48],
            ['name' => 'Workflow Alerts', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 49],
            ['name' => 'Employer De-registration Types', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 50],
            ['name' => 'Erroneous Contribution Sources', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 51],
            ['name' => 'Erroneous Contribution on Progress', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 52],
            ['name' => 'Authorities Source for Dormant De-Registration', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 53],
            ['name' => 'Claim Archive Reasons', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 54],
            ['name' => 'Manual Notification Status', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 55],
            ['name' => 'Manual Notification Rejection Reason', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 56],
            ['name' => 'File Uploads Types', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 57],
            ['name' => 'Cause Of Accident According to Agency', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(

            ['id' => 58],
            ['name' => 'Causes of accident according to type of accident', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 59],
            ['name' => 'OSH Audit Checklist', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 60],
            ['name' => 'Investment types', 'is_system_defined' => 0]
        );


        $code = Code::query()->updateOrCreate(
            ['id' => 61],
            ['name' => 'Interest Adjustment Reasons', 'is_system_defined' => 0]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 62],
            ['name' => 'SLA Alerts', 'is_system_defined' => 0]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 63],
            ['name' => 'Employer De-Registration on Progress', 'is_system_defined' => 1]
        );
        $code = Code::query()->updateOrCreate(
            ['id' => 64],
            ['name' => 'Payroll Alert on Progress', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 65],
            ['name' => 'Payroll Defaults', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 66],
            ['name' => 'Payroll Child Continuation Reasons', 'is_system_defined' => 1]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 67],
            ['name' => 'Investigation types', 'is_system_defined' => 0]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 68],
            ['name' => 'Investigation categories', 'is_system_defined' => 0]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 69],
            ['name' => 'Education Level', 'is_system_defined' => 0]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 70],
            ['name' => 'Payroll Retiree Followup Feedbacks', 'is_system_defined' => 0]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 71],
            ['name' => 'Arrears Commitment Options', 'is_system_defined' => 0]
        );
        //$this->enableForeignKeys();
        $code = Code::query()->updateOrCreate(
            ['id' => 72],
            ['name' => 'Employer type of Contribution Modifications', 'is_system_defined' => 0]
        );

        $code = Code::query()->updateOrCreate(
            ['id' => 73],
            ['name' => 'Instalment MoU Upload', 'is_system_defined' => 0]
        );

    }

}