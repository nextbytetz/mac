<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
class RealEstateCategoriesTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;
    /**
     * real estate categories seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys('investment_code_types');
        $this->delete('investment_code_types');
        
        \DB::table('investment_code_types')->insert(array (
            0 => 
            array (
                'name' => 'Construction',
                'code_value_id'=>533,
                'code_value_reference' =>'INTRESTATE',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            1 => 
            array (
                'name' => 'Purchase',
                'code_value_id'=>533,
                'code_value_reference' =>'INTRESTATE',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            2 => 
            array (
                'name' => 'Lease',
                'code_value_id'=>533,
                'code_value_reference' =>'INTRESTATE',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            3 => 
            array (
                'name' => 'Bare plot',
                'code_value_id'=>533,
                'code_value_reference' =>'INTRESTATE',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            4 => 
            array (
                'name' => 'Unlisted Corporate Bonds',
                'code_value_id'=>530,
                'code_value_reference' =>'INTCBONDS',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            5 => 
            array (
                'name' => 'Listed Corporate Bonds',
                'code_value_id'=>530,
                'code_value_reference' =>'INTCBONDS',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            6 => 
            array (
                'name' => 'Unlisted Equities',
                'code_value_id'=>531,
                'code_value_reference' =>'INTEQUITIES',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            7 => 
            array (
                'name' => 'Listed Equities',
                'code_value_id'=>531,
                'code_value_reference' =>'INTEQUITIES',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            8 => 
            array (
                'name' => 'Loan to Corporates',
                'code_value_id'=>534,
                'code_value_reference' =>'INTLOAN',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            9 => 
            array (
                'name' => 'Loan to Cooperative unions',
                'code_value_id'=>534,
                'code_value_reference' =>'INTLOAN',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
            10 => 
            array (
                'name' => 'Direct Loan to Government',
                'code_value_id'=>534,
                'code_value_reference' =>'INTLOAN',
                'created_at' => '2020-06-12 10:53:00',
                'updated_at' => '2020-06-12 10:53:00',
            ),
        ));

        $this->enableForeignKeys('investment_code_types');
    }
}
