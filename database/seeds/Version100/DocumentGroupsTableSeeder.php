<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DocumentGroupsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('document_groups');
        $this->delete('document_groups');

        \DB::table('document_groups')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Claim Administrative',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Claim Administrative Optional',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Claim Supportive',
                    'created_at' => '2017-04-19 07:58:12',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Disease Administrative',
                    'created_at' => '2017-04-19 07:58:12',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Accident Supportive',
                    'created_at' => '2017-04-19 07:58:33',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Death Supportive',
                    'created_at' => '2017-04-19 07:58:33',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'name' => 'Accident & Disease Supportive',
                    'created_at' => '2017-04-19 07:59:04',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'name' => 'Accident & Disease Optional Supportive',
                    'created_at' => '2017-04-19 07:59:04',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'name' => 'Employment History',
                    'created_at' => '2017-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            9 =>
                array (
                    'id' => 10,
                    'name' => 'Old Compensation',
                    'created_at' => '2017-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            10 =>
                array (
                    'id' => 11,
                    'name' => 'Employer Registration',
                    'created_at' => '2017-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            11 =>
                array (
                    'id' => 12,
                    'name' => 'Employer Claim Report',
                    'created_at' => '2017-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'name' => 'Employer Documents',
                    'created_at' => '2017-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'name' => 'Receipt',
                    'created_at' => '2017-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            14 =>
                array (
                    'id' => 15,
                    'name' => 'HCP/HSP',
                    'created_at' => '2017-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            15 =>
                array (
                    'id' => 16,
                    'name' => 'Payroll Beneficiary',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            16 =>
                array (
                    'id' => 17,
                    'name' => 'Employer Online Claim',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            17 =>
                array (
                    'id' => 18,
                    'name' => 'Employer Business De-Registration/Closure Supporting Documents',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            18 =>
                array (
                    'id' => 19,
                    'name' => 'Employer Debt Collection Documents',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            19 =>
                array (
                    'id' => 20,
                    'name' => 'Employer Inspection',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            20 =>
                array (
                    'id' => 21,
                    'name' => 'Employer Business Closure Follow Ups Documents',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            21 =>
                array (
                    'id' => 22,
                    'name' => 'Employer Particular Changes Documents',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            22 =>
                array (
                    'id' => 23,
                    'name' => 'Letters',
                    'created_at' => '2019-04-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            23 =>
                array (
                    'id' => 24,
                    'name' => 'Return of Earnings',
                    'created_at' => '2020-02-05 14:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            24 =>
                array (
                    'id' => 25,
                    'name' => 'Erroneous Contribution',
                    'created_at' => '2020-02-05 14:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),


            25 =>
                array (
                    'id' => 26,
                    'name' => 'Employer Business Closure Reopen',
                    'created_at' => '2020-07-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),


            26 =>
                array (
                    'id' => 27,
                    'name' => 'Employer Business Closure Extension',
                    'created_at' => '2020-07-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            27 =>
                array (
                    'id' => 28,
                    'name' => 'Police Report',
                    'created_at' => '2020-07-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            28 =>
                array (
                    'id' => 29,
                    'name' => 'Medical Report',
                    'created_at' => '2020-07-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),


            29 =>
                array (
                    'id' => 30,
                    'name' => 'Interest Adjustment',
                    'created_at' => '2020-07-19 07:59:24',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('document_groups');
    }
}