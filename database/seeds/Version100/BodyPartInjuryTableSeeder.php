<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class BodyPartInjuryTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys('body_part_injuries');
        $this->delete('body_part_injuries');

        \DB::table('body_part_injuries')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Scalp',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Left Eye (including orbit and optic nerve)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Right Eye (including orbit and optic nerve)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Right Ear',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Left Ear',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Mouth (including lips, teeth and tongue)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Nose',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'body_part_injury_group_id' => 1,
                    'name' => 'Face',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'body_part_injury_group_id' => 2,
                    'name' => 'Neck (Anterior)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            9 =>
                array (
                    'id' => 10,
                    'body_part_injury_group_id' => 2,
                    'name' => 'Neck (Posterior)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            10 =>
                array (
                    'id' => 11,
                    'body_part_injury_group_id' => 2,
                    'name' => 'Neck (Left Lateral)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            11 =>
                array (
                    'id' => 12,
                    'body_part_injury_group_id' => 2,
                    'name' => 'Neck (Right Lateral)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'body_part_injury_group_id' => 3,
                    'name' => 'Lower Back',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'body_part_injury_group_id' => 3,
                    'name' => 'Upper Back',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            14 =>
                array (
                    'id' => 15,
                    'body_part_injury_group_id' => 3,
                    'name' => 'Chest',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            15 =>
                array (
                    'id' => 16,
                    'body_part_injury_group_id' => 3,
                    'name' => 'Abdomen',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            16 =>
                array (
                    'id' => 17,
                    'body_part_injury_group_id' => 3,
                    'name' => 'Pelvis',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            17 =>
                array (
                    'id' => 18,
                    'body_part_injury_group_id' => 3,
                    'name' => 'Groin',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            18 =>
                array (
                    'id' => 19,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Shoulder (including clavicle and shoulder blade)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            19 =>
                array (
                    'id' => 20,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Shoulder (including clavicle and shoulder blade)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            20 =>
                array (
                    'id' => 21,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Arm - above elbow (Between Shoulder and Elbow)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            21 =>
                array (
                    'id' => 22,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Arm - above elbow (Between Shoulder and Elbow)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            22 =>
                array (
                    'id' => 23,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Arm - below elbow (Between wrist and Elbow)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            23 =>
                array (
                    'id' => 24,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Arm - below elbow (Between wrist and Elbow)',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            24 =>
                array (
                    'id' => 25,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Elbow',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            25 =>
                array (
                    'id' => 26,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Elbow',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            26 =>
                array (
                    'id' => 27,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Forearm',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            27 =>
                array (
                    'id' => 28,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Forearm',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            28 =>
                array (
                    'id' => 29,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Wrist',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            29 =>
                array (
                    'id' => 30,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Wrist',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            30 =>
                array (
                    'id' => 31,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Hand',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            31 =>
                array (
                    'id' => 32,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Hand',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            32 =>
                array (
                    'id' => 33,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left thumb',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            33 =>
                array (
                    'id' => 34,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right thumb',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            34 =>
                array (
                    'id' => 35,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Index Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            35 =>
                array (
                    'id' => 36,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Index Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            36 =>
                array (
                    'id' => 37,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Middle Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            37 =>
                array (
                    'id' => 38,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Middle Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            38 =>
                array (
                    'id' => 39,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Ring Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            39 =>
                array (
                    'id' => 40,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Ring Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            40 =>
                array (
                    'id' => 41,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Left Little Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            41 =>
                array (
                    'id' => 42,
                    'body_part_injury_group_id' => 4,
                    'name' => 'Right Little Finger',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            42 =>
                array (
                    'id' => 43,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Hip',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            43 =>
                array (
                    'id' => 44,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Hip',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            44 =>
                array (
                    'id' => 45,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Thigh',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            45 =>
                array (
                    'id' => 46,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Thigh',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            46 =>
                array (
                    'id' => 47,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Knee',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            47 =>
                array (
                    'id' => 48,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Knee',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            48 =>
                array (
                    'id' => 49,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Leg',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            49 =>
                array (
                    'id' => 50,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Leg',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            50 =>
                array (
                    'id' => 51,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Ankle',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            51 =>
                array (
                    'id' => 52,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Ankle',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            52 =>
                array (
                    'id' => 53,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Foot',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            53 =>
                array (
                    'id' => 54,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Foot Right',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            54 =>
                array (
                    'id' => 55,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Big Toe',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            55 =>
                array (
                    'id' => 56,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Big Toe',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            56 =>
                array (
                    'id' => 57,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Left Other Toes',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            57 =>
                array (
                    'id' => 58,
                    'body_part_injury_group_id' => 5,
                    'name' => 'Right Other Toes',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            58 =>
                array (
                    'id' => 59,
                    'body_part_injury_group_id' => 6,
                    'name' => 'Circulatory system',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            59 =>
                array (
                    'id' => 60,
                    'body_part_injury_group_id' => 6,
                    'name' => 'Respiratory system',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            60 =>
                array (
                    'id' => 61,
                    'body_part_injury_group_id' => 6,
                    'name' => 'Digestive system',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            61 =>
                array (
                    'id' => 62,
                    'body_part_injury_group_id' => 6,
                    'name' => 'Nervous system',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            62 =>
                array (
                    'id' => 63,
                    'body_part_injury_group_id' => 6,
                    'name' => 'Musculo-skeletal system',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('body_part_injuries');

    }
}