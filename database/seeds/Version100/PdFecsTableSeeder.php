<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PdFecsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $count = \DB::table('pd_fecs')->limit(1)->count();

        if (!$count) {
            $this->disableForeignKeys('pd_fecs');
            $this->delete('pd_fecs');

            \DB::table('pd_fecs')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'wpi' => 1,
                        'rank' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'wpi' => 2,
                        'rank' => 1,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'wpi' => 3,
                        'rank' => 1,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'wpi' => 4,
                        'rank' => 1,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'wpi' => 5,
                        'rank' => 1,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'wpi' => 6,
                        'rank' => 1,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'wpi' => 7,
                        'rank' => 1,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'wpi' => 8,
                        'rank' => 1,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'wpi' => 9,
                        'rank' => 1,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'wpi' => 10,
                        'rank' => 1,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'wpi' => 11,
                        'rank' => 1,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'wpi' => 12,
                        'rank' => 1,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'wpi' => 13,
                        'rank' => 1,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'wpi' => 14,
                        'rank' => 1,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'wpi' => 15,
                        'rank' => 1,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'wpi' => 16,
                        'rank' => 1,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'wpi' => 17,
                        'rank' => 1,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'wpi' => 18,
                        'rank' => 1,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'wpi' => 19,
                        'rank' => 1,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'wpi' => 20,
                        'rank' => 1,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'wpi' => 1,
                        'rank' => 2,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'wpi' => 2,
                        'rank' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'wpi' => 3,
                        'rank' => 2,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'wpi' => 4,
                        'rank' => 2,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'wpi' => 5,
                        'rank' => 2,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'wpi' => 6,
                        'rank' => 2,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'wpi' => 7,
                        'rank' => 2,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'wpi' => 8,
                        'rank' => 2,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'wpi' => 9,
                        'rank' => 2,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'wpi' => 10,
                        'rank' => 2,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'wpi' => 11,
                        'rank' => 2,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'wpi' => 12,
                        'rank' => 2,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'wpi' => 13,
                        'rank' => 2,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'wpi' => 14,
                        'rank' => 2,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                34 =>
                    array (
                        'id' => 35,
                        'wpi' => 15,
                        'rank' => 2,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                35 =>
                    array (
                        'id' => 36,
                        'wpi' => 16,
                        'rank' => 2,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                36 =>
                    array (
                        'id' => 37,
                        'wpi' => 17,
                        'rank' => 2,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                37 =>
                    array (
                        'id' => 38,
                        'wpi' => 18,
                        'rank' => 2,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                38 =>
                    array (
                        'id' => 39,
                        'wpi' => 19,
                        'rank' => 2,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                39 =>
                    array (
                        'id' => 40,
                        'wpi' => 20,
                        'rank' => 2,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                40 =>
                    array (
                        'id' => 41,
                        'wpi' => 1,
                        'rank' => 3,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                41 =>
                    array (
                        'id' => 42,
                        'wpi' => 2,
                        'rank' => 3,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                42 =>
                    array (
                        'id' => 43,
                        'wpi' => 3,
                        'rank' => 3,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                43 =>
                    array (
                        'id' => 44,
                        'wpi' => 4,
                        'rank' => 3,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                44 =>
                    array (
                        'id' => 45,
                        'wpi' => 5,
                        'rank' => 3,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                45 =>
                    array (
                        'id' => 46,
                        'wpi' => 6,
                        'rank' => 3,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                46 =>
                    array (
                        'id' => 47,
                        'wpi' => 7,
                        'rank' => 3,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                47 =>
                    array (
                        'id' => 48,
                        'wpi' => 8,
                        'rank' => 3,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                48 =>
                    array (
                        'id' => 49,
                        'wpi' => 9,
                        'rank' => 3,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                49 =>
                    array (
                        'id' => 50,
                        'wpi' => 10,
                        'rank' => 3,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                50 =>
                    array (
                        'id' => 51,
                        'wpi' => 11,
                        'rank' => 3,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                51 =>
                    array (
                        'id' => 52,
                        'wpi' => 12,
                        'rank' => 3,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                52 =>
                    array (
                        'id' => 53,
                        'wpi' => 13,
                        'rank' => 3,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                53 =>
                    array (
                        'id' => 54,
                        'wpi' => 14,
                        'rank' => 3,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                54 =>
                    array (
                        'id' => 55,
                        'wpi' => 15,
                        'rank' => 3,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                55 =>
                    array (
                        'id' => 56,
                        'wpi' => 16,
                        'rank' => 3,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                56 =>
                    array (
                        'id' => 57,
                        'wpi' => 17,
                        'rank' => 3,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                57 =>
                    array (
                        'id' => 58,
                        'wpi' => 18,
                        'rank' => 3,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                58 =>
                    array (
                        'id' => 59,
                        'wpi' => 19,
                        'rank' => 3,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                59 =>
                    array (
                        'id' => 60,
                        'wpi' => 20,
                        'rank' => 3,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                60 =>
                    array (
                        'id' => 61,
                        'wpi' => 1,
                        'rank' => 4,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                61 =>
                    array (
                        'id' => 62,
                        'wpi' => 2,
                        'rank' => 4,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                62 =>
                    array (
                        'id' => 63,
                        'wpi' => 3,
                        'rank' => 4,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                63 =>
                    array (
                        'id' => 64,
                        'wpi' => 4,
                        'rank' => 4,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                64 =>
                    array (
                        'id' => 65,
                        'wpi' => 5,
                        'rank' => 4,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                65 =>
                    array (
                        'id' => 66,
                        'wpi' => 6,
                        'rank' => 4,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                66 =>
                    array (
                        'id' => 67,
                        'wpi' => 7,
                        'rank' => 4,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                67 =>
                    array (
                        'id' => 68,
                        'wpi' => 8,
                        'rank' => 4,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                68 =>
                    array (
                        'id' => 69,
                        'wpi' => 9,
                        'rank' => 4,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                69 =>
                    array (
                        'id' => 70,
                        'wpi' => 10,
                        'rank' => 4,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                70 =>
                    array (
                        'id' => 71,
                        'wpi' => 11,
                        'rank' => 4,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                71 =>
                    array (
                        'id' => 72,
                        'wpi' => 12,
                        'rank' => 4,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                72 =>
                    array (
                        'id' => 73,
                        'wpi' => 13,
                        'rank' => 4,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                73 =>
                    array (
                        'id' => 74,
                        'wpi' => 14,
                        'rank' => 4,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                74 =>
                    array (
                        'id' => 75,
                        'wpi' => 15,
                        'rank' => 4,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                75 =>
                    array (
                        'id' => 76,
                        'wpi' => 16,
                        'rank' => 4,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                76 =>
                    array (
                        'id' => 77,
                        'wpi' => 17,
                        'rank' => 4,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                77 =>
                    array (
                        'id' => 78,
                        'wpi' => 18,
                        'rank' => 4,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                78 =>
                    array (
                        'id' => 79,
                        'wpi' => 19,
                        'rank' => 4,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                79 =>
                    array (
                        'id' => 80,
                        'wpi' => 20,
                        'rank' => 4,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                80 =>
                    array (
                        'id' => 81,
                        'wpi' => 1,
                        'rank' => 5,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                81 =>
                    array (
                        'id' => 82,
                        'wpi' => 2,
                        'rank' => 5,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                82 =>
                    array (
                        'id' => 83,
                        'wpi' => 3,
                        'rank' => 5,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                83 =>
                    array (
                        'id' => 84,
                        'wpi' => 4,
                        'rank' => 5,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                84 =>
                    array (
                        'id' => 85,
                        'wpi' => 5,
                        'rank' => 5,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                85 =>
                    array (
                        'id' => 86,
                        'wpi' => 6,
                        'rank' => 5,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                86 =>
                    array (
                        'id' => 87,
                        'wpi' => 7,
                        'rank' => 5,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                87 =>
                    array (
                        'id' => 88,
                        'wpi' => 8,
                        'rank' => 5,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                88 =>
                    array (
                        'id' => 89,
                        'wpi' => 9,
                        'rank' => 5,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                89 =>
                    array (
                        'id' => 90,
                        'wpi' => 10,
                        'rank' => 5,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                90 =>
                    array (
                        'id' => 91,
                        'wpi' => 11,
                        'rank' => 5,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                91 =>
                    array (
                        'id' => 92,
                        'wpi' => 12,
                        'rank' => 5,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                92 =>
                    array (
                        'id' => 93,
                        'wpi' => 13,
                        'rank' => 5,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                93 =>
                    array (
                        'id' => 94,
                        'wpi' => 14,
                        'rank' => 5,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                94 =>
                    array (
                        'id' => 95,
                        'wpi' => 15,
                        'rank' => 5,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                95 =>
                    array (
                        'id' => 96,
                        'wpi' => 16,
                        'rank' => 5,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                96 =>
                    array (
                        'id' => 97,
                        'wpi' => 17,
                        'rank' => 5,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                97 =>
                    array (
                        'id' => 98,
                        'wpi' => 18,
                        'rank' => 5,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                98 =>
                    array (
                        'id' => 99,
                        'wpi' => 19,
                        'rank' => 5,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                99 =>
                    array (
                        'id' => 100,
                        'wpi' => 20,
                        'rank' => 5,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                100 =>
                    array (
                        'id' => 101,
                        'wpi' => 1,
                        'rank' => 6,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                101 =>
                    array (
                        'id' => 102,
                        'wpi' => 2,
                        'rank' => 6,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                102 =>
                    array (
                        'id' => 103,
                        'wpi' => 3,
                        'rank' => 6,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                103 =>
                    array (
                        'id' => 104,
                        'wpi' => 4,
                        'rank' => 6,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                104 =>
                    array (
                        'id' => 105,
                        'wpi' => 5,
                        'rank' => 6,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                105 =>
                    array (
                        'id' => 106,
                        'wpi' => 6,
                        'rank' => 6,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                106 =>
                    array (
                        'id' => 107,
                        'wpi' => 7,
                        'rank' => 6,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                107 =>
                    array (
                        'id' => 108,
                        'wpi' => 8,
                        'rank' => 6,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                108 =>
                    array (
                        'id' => 109,
                        'wpi' => 9,
                        'rank' => 6,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                109 =>
                    array (
                        'id' => 110,
                        'wpi' => 10,
                        'rank' => 6,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                110 =>
                    array (
                        'id' => 111,
                        'wpi' => 11,
                        'rank' => 6,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                111 =>
                    array (
                        'id' => 112,
                        'wpi' => 12,
                        'rank' => 6,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                112 =>
                    array (
                        'id' => 113,
                        'wpi' => 13,
                        'rank' => 6,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                113 =>
                    array (
                        'id' => 114,
                        'wpi' => 14,
                        'rank' => 6,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                114 =>
                    array (
                        'id' => 115,
                        'wpi' => 15,
                        'rank' => 6,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                115 =>
                    array (
                        'id' => 116,
                        'wpi' => 16,
                        'rank' => 6,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                116 =>
                    array (
                        'id' => 117,
                        'wpi' => 17,
                        'rank' => 6,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                117 =>
                    array (
                        'id' => 118,
                        'wpi' => 18,
                        'rank' => 6,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                118 =>
                    array (
                        'id' => 119,
                        'wpi' => 19,
                        'rank' => 6,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                119 =>
                    array (
                        'id' => 120,
                        'wpi' => 20,
                        'rank' => 6,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                120 =>
                    array (
                        'id' => 121,
                        'wpi' => 1,
                        'rank' => 7,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                121 =>
                    array (
                        'id' => 122,
                        'wpi' => 2,
                        'rank' => 7,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                122 =>
                    array (
                        'id' => 123,
                        'wpi' => 3,
                        'rank' => 7,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                123 =>
                    array (
                        'id' => 124,
                        'wpi' => 4,
                        'rank' => 7,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                124 =>
                    array (
                        'id' => 125,
                        'wpi' => 5,
                        'rank' => 7,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                125 =>
                    array (
                        'id' => 126,
                        'wpi' => 6,
                        'rank' => 7,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                126 =>
                    array (
                        'id' => 127,
                        'wpi' => 7,
                        'rank' => 7,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                127 =>
                    array (
                        'id' => 128,
                        'wpi' => 8,
                        'rank' => 7,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                128 =>
                    array (
                        'id' => 129,
                        'wpi' => 9,
                        'rank' => 7,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                129 =>
                    array (
                        'id' => 130,
                        'wpi' => 10,
                        'rank' => 7,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                130 =>
                    array (
                        'id' => 131,
                        'wpi' => 11,
                        'rank' => 7,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                131 =>
                    array (
                        'id' => 132,
                        'wpi' => 12,
                        'rank' => 7,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                132 =>
                    array (
                        'id' => 133,
                        'wpi' => 13,
                        'rank' => 7,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                133 =>
                    array (
                        'id' => 134,
                        'wpi' => 14,
                        'rank' => 7,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                134 =>
                    array (
                        'id' => 135,
                        'wpi' => 15,
                        'rank' => 7,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                135 =>
                    array (
                        'id' => 136,
                        'wpi' => 16,
                        'rank' => 7,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                136 =>
                    array (
                        'id' => 137,
                        'wpi' => 17,
                        'rank' => 7,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                137 =>
                    array (
                        'id' => 138,
                        'wpi' => 18,
                        'rank' => 7,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                138 =>
                    array (
                        'id' => 139,
                        'wpi' => 19,
                        'rank' => 7,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                139 =>
                    array (
                        'id' => 140,
                        'wpi' => 20,
                        'rank' => 7,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                140 =>
                    array (
                        'id' => 141,
                        'wpi' => 1,
                        'rank' => 8,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                141 =>
                    array (
                        'id' => 142,
                        'wpi' => 2,
                        'rank' => 8,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                142 =>
                    array (
                        'id' => 143,
                        'wpi' => 3,
                        'rank' => 8,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                143 =>
                    array (
                        'id' => 144,
                        'wpi' => 4,
                        'rank' => 8,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                144 =>
                    array (
                        'id' => 145,
                        'wpi' => 5,
                        'rank' => 8,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                145 =>
                    array (
                        'id' => 146,
                        'wpi' => 6,
                        'rank' => 8,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                146 =>
                    array (
                        'id' => 147,
                        'wpi' => 7,
                        'rank' => 8,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                147 =>
                    array (
                        'id' => 148,
                        'wpi' => 8,
                        'rank' => 8,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                148 =>
                    array (
                        'id' => 149,
                        'wpi' => 9,
                        'rank' => 8,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                149 =>
                    array (
                        'id' => 150,
                        'wpi' => 10,
                        'rank' => 8,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                150 =>
                    array (
                        'id' => 151,
                        'wpi' => 11,
                        'rank' => 8,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                151 =>
                    array (
                        'id' => 152,
                        'wpi' => 12,
                        'rank' => 8,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                152 =>
                    array (
                        'id' => 153,
                        'wpi' => 13,
                        'rank' => 8,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                153 =>
                    array (
                        'id' => 154,
                        'wpi' => 14,
                        'rank' => 8,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                154 =>
                    array (
                        'id' => 155,
                        'wpi' => 15,
                        'rank' => 8,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                155 =>
                    array (
                        'id' => 156,
                        'wpi' => 16,
                        'rank' => 8,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                156 =>
                    array (
                        'id' => 157,
                        'wpi' => 17,
                        'rank' => 8,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                157 =>
                    array (
                        'id' => 158,
                        'wpi' => 18,
                        'rank' => 8,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                158 =>
                    array (
                        'id' => 159,
                        'wpi' => 19,
                        'rank' => 8,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                159 =>
                    array (
                        'id' => 160,
                        'wpi' => 20,
                        'rank' => 8,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                160 =>
                    array (
                        'id' => 161,
                        'wpi' => 21,
                        'rank' => 1,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                161 =>
                    array (
                        'id' => 162,
                        'wpi' => 22,
                        'rank' => 1,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                162 =>
                    array (
                        'id' => 163,
                        'wpi' => 23,
                        'rank' => 1,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                163 =>
                    array (
                        'id' => 164,
                        'wpi' => 24,
                        'rank' => 1,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                164 =>
                    array (
                        'id' => 165,
                        'wpi' => 25,
                        'rank' => 1,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                165 =>
                    array (
                        'id' => 166,
                        'wpi' => 26,
                        'rank' => 1,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                166 =>
                    array (
                        'id' => 167,
                        'wpi' => 27,
                        'rank' => 1,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                167 =>
                    array (
                        'id' => 168,
                        'wpi' => 28,
                        'rank' => 1,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                168 =>
                    array (
                        'id' => 169,
                        'wpi' => 29,
                        'rank' => 1,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                169 =>
                    array (
                        'id' => 170,
                        'wpi' => 30,
                        'rank' => 1,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                170 =>
                    array (
                        'id' => 171,
                        'wpi' => 31,
                        'rank' => 1,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                171 =>
                    array (
                        'id' => 172,
                        'wpi' => 32,
                        'rank' => 1,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                172 =>
                    array (
                        'id' => 173,
                        'wpi' => 33,
                        'rank' => 1,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                173 =>
                    array (
                        'id' => 174,
                        'wpi' => 34,
                        'rank' => 1,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                174 =>
                    array (
                        'id' => 175,
                        'wpi' => 35,
                        'rank' => 1,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                175 =>
                    array (
                        'id' => 176,
                        'wpi' => 36,
                        'rank' => 1,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                176 =>
                    array (
                        'id' => 177,
                        'wpi' => 37,
                        'rank' => 1,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                177 =>
                    array (
                        'id' => 178,
                        'wpi' => 38,
                        'rank' => 1,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                178 =>
                    array (
                        'id' => 179,
                        'wpi' => 39,
                        'rank' => 1,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                179 =>
                    array (
                        'id' => 180,
                        'wpi' => 40,
                        'rank' => 1,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                180 =>
                    array (
                        'id' => 181,
                        'wpi' => 21,
                        'rank' => 2,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                181 =>
                    array (
                        'id' => 182,
                        'wpi' => 22,
                        'rank' => 2,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                182 =>
                    array (
                        'id' => 183,
                        'wpi' => 23,
                        'rank' => 2,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                183 =>
                    array (
                        'id' => 184,
                        'wpi' => 24,
                        'rank' => 2,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                184 =>
                    array (
                        'id' => 185,
                        'wpi' => 25,
                        'rank' => 2,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                185 =>
                    array (
                        'id' => 186,
                        'wpi' => 26,
                        'rank' => 2,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                186 =>
                    array (
                        'id' => 187,
                        'wpi' => 27,
                        'rank' => 2,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                187 =>
                    array (
                        'id' => 188,
                        'wpi' => 28,
                        'rank' => 2,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                188 =>
                    array (
                        'id' => 189,
                        'wpi' => 29,
                        'rank' => 2,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                189 =>
                    array (
                        'id' => 190,
                        'wpi' => 30,
                        'rank' => 2,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                190 =>
                    array (
                        'id' => 191,
                        'wpi' => 31,
                        'rank' => 2,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                191 =>
                    array (
                        'id' => 192,
                        'wpi' => 32,
                        'rank' => 2,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                192 =>
                    array (
                        'id' => 193,
                        'wpi' => 33,
                        'rank' => 2,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                193 =>
                    array (
                        'id' => 194,
                        'wpi' => 34,
                        'rank' => 2,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                194 =>
                    array (
                        'id' => 195,
                        'wpi' => 35,
                        'rank' => 2,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                195 =>
                    array (
                        'id' => 196,
                        'wpi' => 36,
                        'rank' => 2,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                196 =>
                    array (
                        'id' => 197,
                        'wpi' => 37,
                        'rank' => 2,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                197 =>
                    array (
                        'id' => 198,
                        'wpi' => 38,
                        'rank' => 2,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                198 =>
                    array (
                        'id' => 199,
                        'wpi' => 39,
                        'rank' => 2,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                199 =>
                    array (
                        'id' => 200,
                        'wpi' => 40,
                        'rank' => 2,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                200 =>
                    array (
                        'id' => 201,
                        'wpi' => 21,
                        'rank' => 3,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                201 =>
                    array (
                        'id' => 202,
                        'wpi' => 22,
                        'rank' => 3,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                202 =>
                    array (
                        'id' => 203,
                        'wpi' => 23,
                        'rank' => 3,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                203 =>
                    array (
                        'id' => 204,
                        'wpi' => 24,
                        'rank' => 3,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                204 =>
                    array (
                        'id' => 205,
                        'wpi' => 25,
                        'rank' => 3,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                205 =>
                    array (
                        'id' => 206,
                        'wpi' => 26,
                        'rank' => 3,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                206 =>
                    array (
                        'id' => 207,
                        'wpi' => 27,
                        'rank' => 3,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                207 =>
                    array (
                        'id' => 208,
                        'wpi' => 28,
                        'rank' => 3,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                208 =>
                    array (
                        'id' => 209,
                        'wpi' => 29,
                        'rank' => 3,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                209 =>
                    array (
                        'id' => 210,
                        'wpi' => 30,
                        'rank' => 3,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                210 =>
                    array (
                        'id' => 211,
                        'wpi' => 31,
                        'rank' => 3,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                211 =>
                    array (
                        'id' => 212,
                        'wpi' => 32,
                        'rank' => 3,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                212 =>
                    array (
                        'id' => 213,
                        'wpi' => 33,
                        'rank' => 3,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                213 =>
                    array (
                        'id' => 214,
                        'wpi' => 34,
                        'rank' => 3,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                214 =>
                    array (
                        'id' => 215,
                        'wpi' => 35,
                        'rank' => 3,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                215 =>
                    array (
                        'id' => 216,
                        'wpi' => 36,
                        'rank' => 3,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                216 =>
                    array (
                        'id' => 217,
                        'wpi' => 37,
                        'rank' => 3,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                217 =>
                    array (
                        'id' => 218,
                        'wpi' => 38,
                        'rank' => 3,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                218 =>
                    array (
                        'id' => 219,
                        'wpi' => 39,
                        'rank' => 3,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                219 =>
                    array (
                        'id' => 220,
                        'wpi' => 40,
                        'rank' => 3,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                220 =>
                    array (
                        'id' => 221,
                        'wpi' => 21,
                        'rank' => 4,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                221 =>
                    array (
                        'id' => 222,
                        'wpi' => 22,
                        'rank' => 4,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                222 =>
                    array (
                        'id' => 223,
                        'wpi' => 23,
                        'rank' => 4,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                223 =>
                    array (
                        'id' => 224,
                        'wpi' => 24,
                        'rank' => 4,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                224 =>
                    array (
                        'id' => 225,
                        'wpi' => 25,
                        'rank' => 4,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                225 =>
                    array (
                        'id' => 226,
                        'wpi' => 26,
                        'rank' => 4,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                226 =>
                    array (
                        'id' => 227,
                        'wpi' => 27,
                        'rank' => 4,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                227 =>
                    array (
                        'id' => 228,
                        'wpi' => 28,
                        'rank' => 4,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                228 =>
                    array (
                        'id' => 229,
                        'wpi' => 29,
                        'rank' => 4,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                229 =>
                    array (
                        'id' => 230,
                        'wpi' => 30,
                        'rank' => 4,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                230 =>
                    array (
                        'id' => 231,
                        'wpi' => 31,
                        'rank' => 4,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                231 =>
                    array (
                        'id' => 232,
                        'wpi' => 32,
                        'rank' => 4,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                232 =>
                    array (
                        'id' => 233,
                        'wpi' => 33,
                        'rank' => 4,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                233 =>
                    array (
                        'id' => 234,
                        'wpi' => 34,
                        'rank' => 4,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                234 =>
                    array (
                        'id' => 235,
                        'wpi' => 35,
                        'rank' => 4,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                235 =>
                    array (
                        'id' => 236,
                        'wpi' => 36,
                        'rank' => 4,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                236 =>
                    array (
                        'id' => 237,
                        'wpi' => 37,
                        'rank' => 4,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                237 =>
                    array (
                        'id' => 238,
                        'wpi' => 38,
                        'rank' => 4,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                238 =>
                    array (
                        'id' => 239,
                        'wpi' => 39,
                        'rank' => 4,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                239 =>
                    array (
                        'id' => 240,
                        'wpi' => 40,
                        'rank' => 4,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                240 =>
                    array (
                        'id' => 241,
                        'wpi' => 21,
                        'rank' => 5,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                241 =>
                    array (
                        'id' => 242,
                        'wpi' => 22,
                        'rank' => 5,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                242 =>
                    array (
                        'id' => 243,
                        'wpi' => 23,
                        'rank' => 5,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                243 =>
                    array (
                        'id' => 244,
                        'wpi' => 24,
                        'rank' => 5,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                244 =>
                    array (
                        'id' => 245,
                        'wpi' => 25,
                        'rank' => 5,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                245 =>
                    array (
                        'id' => 246,
                        'wpi' => 26,
                        'rank' => 5,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                246 =>
                    array (
                        'id' => 247,
                        'wpi' => 27,
                        'rank' => 5,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                247 =>
                    array (
                        'id' => 248,
                        'wpi' => 28,
                        'rank' => 5,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                248 =>
                    array (
                        'id' => 249,
                        'wpi' => 29,
                        'rank' => 5,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                249 =>
                    array (
                        'id' => 250,
                        'wpi' => 30,
                        'rank' => 5,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                250 =>
                    array (
                        'id' => 251,
                        'wpi' => 31,
                        'rank' => 5,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                251 =>
                    array (
                        'id' => 252,
                        'wpi' => 32,
                        'rank' => 5,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                252 =>
                    array (
                        'id' => 253,
                        'wpi' => 33,
                        'rank' => 5,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                253 =>
                    array (
                        'id' => 254,
                        'wpi' => 34,
                        'rank' => 5,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                254 =>
                    array (
                        'id' => 255,
                        'wpi' => 35,
                        'rank' => 5,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                255 =>
                    array (
                        'id' => 256,
                        'wpi' => 36,
                        'rank' => 5,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                256 =>
                    array (
                        'id' => 257,
                        'wpi' => 37,
                        'rank' => 5,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                257 =>
                    array (
                        'id' => 258,
                        'wpi' => 38,
                        'rank' => 5,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                258 =>
                    array (
                        'id' => 259,
                        'wpi' => 39,
                        'rank' => 5,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                259 =>
                    array (
                        'id' => 260,
                        'wpi' => 40,
                        'rank' => 5,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                260 =>
                    array (
                        'id' => 261,
                        'wpi' => 21,
                        'rank' => 6,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                261 =>
                    array (
                        'id' => 262,
                        'wpi' => 22,
                        'rank' => 6,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                262 =>
                    array (
                        'id' => 263,
                        'wpi' => 23,
                        'rank' => 6,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                263 =>
                    array (
                        'id' => 264,
                        'wpi' => 24,
                        'rank' => 6,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                264 =>
                    array (
                        'id' => 265,
                        'wpi' => 25,
                        'rank' => 6,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                265 =>
                    array (
                        'id' => 266,
                        'wpi' => 26,
                        'rank' => 6,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                266 =>
                    array (
                        'id' => 267,
                        'wpi' => 27,
                        'rank' => 6,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                267 =>
                    array (
                        'id' => 268,
                        'wpi' => 28,
                        'rank' => 6,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                268 =>
                    array (
                        'id' => 269,
                        'wpi' => 29,
                        'rank' => 6,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                269 =>
                    array (
                        'id' => 270,
                        'wpi' => 30,
                        'rank' => 6,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                270 =>
                    array (
                        'id' => 271,
                        'wpi' => 31,
                        'rank' => 6,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                271 =>
                    array (
                        'id' => 272,
                        'wpi' => 32,
                        'rank' => 6,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                272 =>
                    array (
                        'id' => 273,
                        'wpi' => 33,
                        'rank' => 6,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                273 =>
                    array (
                        'id' => 274,
                        'wpi' => 34,
                        'rank' => 6,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                274 =>
                    array (
                        'id' => 275,
                        'wpi' => 35,
                        'rank' => 6,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                275 =>
                    array (
                        'id' => 276,
                        'wpi' => 36,
                        'rank' => 6,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                276 =>
                    array (
                        'id' => 277,
                        'wpi' => 37,
                        'rank' => 6,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                277 =>
                    array (
                        'id' => 278,
                        'wpi' => 38,
                        'rank' => 6,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                278 =>
                    array (
                        'id' => 279,
                        'wpi' => 39,
                        'rank' => 6,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                279 =>
                    array (
                        'id' => 280,
                        'wpi' => 40,
                        'rank' => 6,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                280 =>
                    array (
                        'id' => 281,
                        'wpi' => 21,
                        'rank' => 7,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                281 =>
                    array (
                        'id' => 282,
                        'wpi' => 22,
                        'rank' => 7,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                282 =>
                    array (
                        'id' => 283,
                        'wpi' => 23,
                        'rank' => 7,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                283 =>
                    array (
                        'id' => 284,
                        'wpi' => 24,
                        'rank' => 7,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                284 =>
                    array (
                        'id' => 285,
                        'wpi' => 25,
                        'rank' => 7,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                285 =>
                    array (
                        'id' => 286,
                        'wpi' => 26,
                        'rank' => 7,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                286 =>
                    array (
                        'id' => 287,
                        'wpi' => 27,
                        'rank' => 7,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                287 =>
                    array (
                        'id' => 288,
                        'wpi' => 28,
                        'rank' => 7,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                288 =>
                    array (
                        'id' => 289,
                        'wpi' => 29,
                        'rank' => 7,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                289 =>
                    array (
                        'id' => 290,
                        'wpi' => 30,
                        'rank' => 7,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                290 =>
                    array (
                        'id' => 291,
                        'wpi' => 31,
                        'rank' => 7,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                291 =>
                    array (
                        'id' => 292,
                        'wpi' => 32,
                        'rank' => 7,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                292 =>
                    array (
                        'id' => 293,
                        'wpi' => 33,
                        'rank' => 7,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                293 =>
                    array (
                        'id' => 294,
                        'wpi' => 34,
                        'rank' => 7,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                294 =>
                    array (
                        'id' => 295,
                        'wpi' => 35,
                        'rank' => 7,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                295 =>
                    array (
                        'id' => 296,
                        'wpi' => 36,
                        'rank' => 7,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                296 =>
                    array (
                        'id' => 297,
                        'wpi' => 37,
                        'rank' => 7,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                297 =>
                    array (
                        'id' => 298,
                        'wpi' => 38,
                        'rank' => 7,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                298 =>
                    array (
                        'id' => 299,
                        'wpi' => 39,
                        'rank' => 7,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                299 =>
                    array (
                        'id' => 300,
                        'wpi' => 40,
                        'rank' => 7,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                300 =>
                    array (
                        'id' => 301,
                        'wpi' => 21,
                        'rank' => 8,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                301 =>
                    array (
                        'id' => 302,
                        'wpi' => 22,
                        'rank' => 8,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                302 =>
                    array (
                        'id' => 303,
                        'wpi' => 23,
                        'rank' => 8,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                303 =>
                    array (
                        'id' => 304,
                        'wpi' => 24,
                        'rank' => 8,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                304 =>
                    array (
                        'id' => 305,
                        'wpi' => 25,
                        'rank' => 8,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                305 =>
                    array (
                        'id' => 306,
                        'wpi' => 26,
                        'rank' => 8,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                306 =>
                    array (
                        'id' => 307,
                        'wpi' => 27,
                        'rank' => 8,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                307 =>
                    array (
                        'id' => 308,
                        'wpi' => 28,
                        'rank' => 8,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                308 =>
                    array (
                        'id' => 309,
                        'wpi' => 29,
                        'rank' => 8,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                309 =>
                    array (
                        'id' => 310,
                        'wpi' => 30,
                        'rank' => 8,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                310 =>
                    array (
                        'id' => 311,
                        'wpi' => 31,
                        'rank' => 8,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                311 =>
                    array (
                        'id' => 312,
                        'wpi' => 32,
                        'rank' => 8,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                312 =>
                    array (
                        'id' => 313,
                        'wpi' => 33,
                        'rank' => 8,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                313 =>
                    array (
                        'id' => 314,
                        'wpi' => 34,
                        'rank' => 8,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                314 =>
                    array (
                        'id' => 315,
                        'wpi' => 35,
                        'rank' => 8,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                315 =>
                    array (
                        'id' => 316,
                        'wpi' => 36,
                        'rank' => 8,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                316 =>
                    array (
                        'id' => 317,
                        'wpi' => 37,
                        'rank' => 8,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                317 =>
                    array (
                        'id' => 318,
                        'wpi' => 38,
                        'rank' => 8,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                318 =>
                    array (
                        'id' => 319,
                        'wpi' => 39,
                        'rank' => 8,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                319 =>
                    array (
                        'id' => 320,
                        'wpi' => 40,
                        'rank' => 8,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                320 =>
                    array (
                        'id' => 321,
                        'wpi' => 41,
                        'rank' => 1,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                321 =>
                    array (
                        'id' => 322,
                        'wpi' => 42,
                        'rank' => 1,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                322 =>
                    array (
                        'id' => 323,
                        'wpi' => 43,
                        'rank' => 1,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                323 =>
                    array (
                        'id' => 324,
                        'wpi' => 44,
                        'rank' => 1,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                324 =>
                    array (
                        'id' => 325,
                        'wpi' => 45,
                        'rank' => 1,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                325 =>
                    array (
                        'id' => 326,
                        'wpi' => 46,
                        'rank' => 1,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                326 =>
                    array (
                        'id' => 327,
                        'wpi' => 47,
                        'rank' => 1,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                327 =>
                    array (
                        'id' => 328,
                        'wpi' => 48,
                        'rank' => 1,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                328 =>
                    array (
                        'id' => 329,
                        'wpi' => 49,
                        'rank' => 1,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                329 =>
                    array (
                        'id' => 330,
                        'wpi' => 50,
                        'rank' => 1,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                330 =>
                    array (
                        'id' => 331,
                        'wpi' => 51,
                        'rank' => 1,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                331 =>
                    array (
                        'id' => 332,
                        'wpi' => 52,
                        'rank' => 1,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                332 =>
                    array (
                        'id' => 333,
                        'wpi' => 53,
                        'rank' => 1,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                333 =>
                    array (
                        'id' => 334,
                        'wpi' => 54,
                        'rank' => 1,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                334 =>
                    array (
                        'id' => 335,
                        'wpi' => 55,
                        'rank' => 1,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                335 =>
                    array (
                        'id' => 336,
                        'wpi' => 56,
                        'rank' => 1,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                336 =>
                    array (
                        'id' => 337,
                        'wpi' => 57,
                        'rank' => 1,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                337 =>
                    array (
                        'id' => 338,
                        'wpi' => 58,
                        'rank' => 1,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                338 =>
                    array (
                        'id' => 339,
                        'wpi' => 59,
                        'rank' => 1,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                339 =>
                    array (
                        'id' => 340,
                        'wpi' => 60,
                        'rank' => 1,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                340 =>
                    array (
                        'id' => 341,
                        'wpi' => 41,
                        'rank' => 2,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                341 =>
                    array (
                        'id' => 342,
                        'wpi' => 42,
                        'rank' => 2,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                342 =>
                    array (
                        'id' => 343,
                        'wpi' => 43,
                        'rank' => 2,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                343 =>
                    array (
                        'id' => 344,
                        'wpi' => 44,
                        'rank' => 2,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                344 =>
                    array (
                        'id' => 345,
                        'wpi' => 45,
                        'rank' => 2,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                345 =>
                    array (
                        'id' => 346,
                        'wpi' => 46,
                        'rank' => 2,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                346 =>
                    array (
                        'id' => 347,
                        'wpi' => 47,
                        'rank' => 2,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                347 =>
                    array (
                        'id' => 348,
                        'wpi' => 48,
                        'rank' => 2,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                348 =>
                    array (
                        'id' => 349,
                        'wpi' => 49,
                        'rank' => 2,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                349 =>
                    array (
                        'id' => 350,
                        'wpi' => 50,
                        'rank' => 2,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                350 =>
                    array (
                        'id' => 351,
                        'wpi' => 51,
                        'rank' => 2,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                351 =>
                    array (
                        'id' => 352,
                        'wpi' => 52,
                        'rank' => 2,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                352 =>
                    array (
                        'id' => 353,
                        'wpi' => 53,
                        'rank' => 2,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                353 =>
                    array (
                        'id' => 354,
                        'wpi' => 54,
                        'rank' => 2,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                354 =>
                    array (
                        'id' => 355,
                        'wpi' => 55,
                        'rank' => 2,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                355 =>
                    array (
                        'id' => 356,
                        'wpi' => 56,
                        'rank' => 2,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                356 =>
                    array (
                        'id' => 357,
                        'wpi' => 57,
                        'rank' => 2,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                357 =>
                    array (
                        'id' => 358,
                        'wpi' => 58,
                        'rank' => 2,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                358 =>
                    array (
                        'id' => 359,
                        'wpi' => 59,
                        'rank' => 2,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                359 =>
                    array (
                        'id' => 360,
                        'wpi' => 60,
                        'rank' => 2,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                360 =>
                    array (
                        'id' => 361,
                        'wpi' => 41,
                        'rank' => 3,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                361 =>
                    array (
                        'id' => 362,
                        'wpi' => 42,
                        'rank' => 3,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                362 =>
                    array (
                        'id' => 363,
                        'wpi' => 43,
                        'rank' => 3,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                363 =>
                    array (
                        'id' => 364,
                        'wpi' => 44,
                        'rank' => 3,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                364 =>
                    array (
                        'id' => 365,
                        'wpi' => 45,
                        'rank' => 3,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                365 =>
                    array (
                        'id' => 366,
                        'wpi' => 46,
                        'rank' => 3,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                366 =>
                    array (
                        'id' => 367,
                        'wpi' => 47,
                        'rank' => 3,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                367 =>
                    array (
                        'id' => 368,
                        'wpi' => 48,
                        'rank' => 3,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                368 =>
                    array (
                        'id' => 369,
                        'wpi' => 49,
                        'rank' => 3,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                369 =>
                    array (
                        'id' => 370,
                        'wpi' => 50,
                        'rank' => 3,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                370 =>
                    array (
                        'id' => 371,
                        'wpi' => 51,
                        'rank' => 3,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                371 =>
                    array (
                        'id' => 372,
                        'wpi' => 52,
                        'rank' => 3,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                372 =>
                    array (
                        'id' => 373,
                        'wpi' => 53,
                        'rank' => 3,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                373 =>
                    array (
                        'id' => 374,
                        'wpi' => 54,
                        'rank' => 3,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                374 =>
                    array (
                        'id' => 375,
                        'wpi' => 55,
                        'rank' => 3,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                375 =>
                    array (
                        'id' => 376,
                        'wpi' => 56,
                        'rank' => 3,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                376 =>
                    array (
                        'id' => 377,
                        'wpi' => 57,
                        'rank' => 3,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                377 =>
                    array (
                        'id' => 378,
                        'wpi' => 58,
                        'rank' => 3,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                378 =>
                    array (
                        'id' => 379,
                        'wpi' => 59,
                        'rank' => 3,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                379 =>
                    array (
                        'id' => 380,
                        'wpi' => 60,
                        'rank' => 3,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                380 =>
                    array (
                        'id' => 381,
                        'wpi' => 41,
                        'rank' => 4,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                381 =>
                    array (
                        'id' => 382,
                        'wpi' => 42,
                        'rank' => 4,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                382 =>
                    array (
                        'id' => 383,
                        'wpi' => 43,
                        'rank' => 4,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                383 =>
                    array (
                        'id' => 384,
                        'wpi' => 44,
                        'rank' => 4,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                384 =>
                    array (
                        'id' => 385,
                        'wpi' => 45,
                        'rank' => 4,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                385 =>
                    array (
                        'id' => 386,
                        'wpi' => 46,
                        'rank' => 4,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                386 =>
                    array (
                        'id' => 387,
                        'wpi' => 47,
                        'rank' => 4,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                387 =>
                    array (
                        'id' => 388,
                        'wpi' => 48,
                        'rank' => 4,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                388 =>
                    array (
                        'id' => 389,
                        'wpi' => 49,
                        'rank' => 4,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                389 =>
                    array (
                        'id' => 390,
                        'wpi' => 50,
                        'rank' => 4,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                390 =>
                    array (
                        'id' => 391,
                        'wpi' => 51,
                        'rank' => 4,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                391 =>
                    array (
                        'id' => 392,
                        'wpi' => 52,
                        'rank' => 4,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                392 =>
                    array (
                        'id' => 393,
                        'wpi' => 53,
                        'rank' => 4,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                393 =>
                    array (
                        'id' => 394,
                        'wpi' => 54,
                        'rank' => 4,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                394 =>
                    array (
                        'id' => 395,
                        'wpi' => 55,
                        'rank' => 4,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                395 =>
                    array (
                        'id' => 396,
                        'wpi' => 56,
                        'rank' => 4,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                396 =>
                    array (
                        'id' => 397,
                        'wpi' => 57,
                        'rank' => 4,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                397 =>
                    array (
                        'id' => 398,
                        'wpi' => 58,
                        'rank' => 4,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                398 =>
                    array (
                        'id' => 399,
                        'wpi' => 59,
                        'rank' => 4,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                399 =>
                    array (
                        'id' => 400,
                        'wpi' => 60,
                        'rank' => 4,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                400 =>
                    array (
                        'id' => 401,
                        'wpi' => 41,
                        'rank' => 5,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                401 =>
                    array (
                        'id' => 402,
                        'wpi' => 42,
                        'rank' => 5,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                402 =>
                    array (
                        'id' => 403,
                        'wpi' => 43,
                        'rank' => 5,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                403 =>
                    array (
                        'id' => 404,
                        'wpi' => 44,
                        'rank' => 5,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                404 =>
                    array (
                        'id' => 405,
                        'wpi' => 45,
                        'rank' => 5,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                405 =>
                    array (
                        'id' => 406,
                        'wpi' => 46,
                        'rank' => 5,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                406 =>
                    array (
                        'id' => 407,
                        'wpi' => 47,
                        'rank' => 5,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                407 =>
                    array (
                        'id' => 408,
                        'wpi' => 48,
                        'rank' => 5,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                408 =>
                    array (
                        'id' => 409,
                        'wpi' => 49,
                        'rank' => 5,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                409 =>
                    array (
                        'id' => 410,
                        'wpi' => 50,
                        'rank' => 5,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                410 =>
                    array (
                        'id' => 411,
                        'wpi' => 51,
                        'rank' => 5,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                411 =>
                    array (
                        'id' => 412,
                        'wpi' => 52,
                        'rank' => 5,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                412 =>
                    array (
                        'id' => 413,
                        'wpi' => 53,
                        'rank' => 5,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                413 =>
                    array (
                        'id' => 414,
                        'wpi' => 54,
                        'rank' => 5,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                414 =>
                    array (
                        'id' => 415,
                        'wpi' => 55,
                        'rank' => 5,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                415 =>
                    array (
                        'id' => 416,
                        'wpi' => 56,
                        'rank' => 5,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                416 =>
                    array (
                        'id' => 417,
                        'wpi' => 57,
                        'rank' => 5,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                417 =>
                    array (
                        'id' => 418,
                        'wpi' => 58,
                        'rank' => 5,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                418 =>
                    array (
                        'id' => 419,
                        'wpi' => 59,
                        'rank' => 5,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                419 =>
                    array (
                        'id' => 420,
                        'wpi' => 60,
                        'rank' => 5,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                420 =>
                    array (
                        'id' => 421,
                        'wpi' => 41,
                        'rank' => 6,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                421 =>
                    array (
                        'id' => 422,
                        'wpi' => 42,
                        'rank' => 6,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                422 =>
                    array (
                        'id' => 423,
                        'wpi' => 43,
                        'rank' => 6,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                423 =>
                    array (
                        'id' => 424,
                        'wpi' => 44,
                        'rank' => 6,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                424 =>
                    array (
                        'id' => 425,
                        'wpi' => 45,
                        'rank' => 6,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                425 =>
                    array (
                        'id' => 426,
                        'wpi' => 46,
                        'rank' => 6,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                426 =>
                    array (
                        'id' => 427,
                        'wpi' => 47,
                        'rank' => 6,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                427 =>
                    array (
                        'id' => 428,
                        'wpi' => 48,
                        'rank' => 6,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                428 =>
                    array (
                        'id' => 429,
                        'wpi' => 49,
                        'rank' => 6,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                429 =>
                    array (
                        'id' => 430,
                        'wpi' => 50,
                        'rank' => 6,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                430 =>
                    array (
                        'id' => 431,
                        'wpi' => 51,
                        'rank' => 6,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                431 =>
                    array (
                        'id' => 432,
                        'wpi' => 52,
                        'rank' => 6,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                432 =>
                    array (
                        'id' => 433,
                        'wpi' => 53,
                        'rank' => 6,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                433 =>
                    array (
                        'id' => 434,
                        'wpi' => 54,
                        'rank' => 6,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                434 =>
                    array (
                        'id' => 435,
                        'wpi' => 55,
                        'rank' => 6,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                435 =>
                    array (
                        'id' => 436,
                        'wpi' => 56,
                        'rank' => 6,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                436 =>
                    array (
                        'id' => 437,
                        'wpi' => 57,
                        'rank' => 6,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                437 =>
                    array (
                        'id' => 438,
                        'wpi' => 58,
                        'rank' => 6,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                438 =>
                    array (
                        'id' => 439,
                        'wpi' => 59,
                        'rank' => 6,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                439 =>
                    array (
                        'id' => 440,
                        'wpi' => 60,
                        'rank' => 6,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                440 =>
                    array (
                        'id' => 441,
                        'wpi' => 41,
                        'rank' => 7,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                441 =>
                    array (
                        'id' => 442,
                        'wpi' => 42,
                        'rank' => 7,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                442 =>
                    array (
                        'id' => 443,
                        'wpi' => 43,
                        'rank' => 7,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                443 =>
                    array (
                        'id' => 444,
                        'wpi' => 44,
                        'rank' => 7,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                444 =>
                    array (
                        'id' => 445,
                        'wpi' => 45,
                        'rank' => 7,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                445 =>
                    array (
                        'id' => 446,
                        'wpi' => 46,
                        'rank' => 7,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                446 =>
                    array (
                        'id' => 447,
                        'wpi' => 47,
                        'rank' => 7,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                447 =>
                    array (
                        'id' => 448,
                        'wpi' => 48,
                        'rank' => 7,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                448 =>
                    array (
                        'id' => 449,
                        'wpi' => 49,
                        'rank' => 7,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                449 =>
                    array (
                        'id' => 450,
                        'wpi' => 50,
                        'rank' => 7,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                450 =>
                    array (
                        'id' => 451,
                        'wpi' => 51,
                        'rank' => 7,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                451 =>
                    array (
                        'id' => 452,
                        'wpi' => 52,
                        'rank' => 7,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                452 =>
                    array (
                        'id' => 453,
                        'wpi' => 53,
                        'rank' => 7,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                453 =>
                    array (
                        'id' => 454,
                        'wpi' => 54,
                        'rank' => 7,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                454 =>
                    array (
                        'id' => 455,
                        'wpi' => 55,
                        'rank' => 7,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                455 =>
                    array (
                        'id' => 456,
                        'wpi' => 56,
                        'rank' => 7,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                456 =>
                    array (
                        'id' => 457,
                        'wpi' => 57,
                        'rank' => 7,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                457 =>
                    array (
                        'id' => 458,
                        'wpi' => 58,
                        'rank' => 7,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                458 =>
                    array (
                        'id' => 459,
                        'wpi' => 59,
                        'rank' => 7,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                459 =>
                    array (
                        'id' => 460,
                        'wpi' => 60,
                        'rank' => 7,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                460 =>
                    array (
                        'id' => 461,
                        'wpi' => 41,
                        'rank' => 8,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                461 =>
                    array (
                        'id' => 462,
                        'wpi' => 42,
                        'rank' => 8,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                462 =>
                    array (
                        'id' => 463,
                        'wpi' => 43,
                        'rank' => 8,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                463 =>
                    array (
                        'id' => 464,
                        'wpi' => 44,
                        'rank' => 8,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                464 =>
                    array (
                        'id' => 465,
                        'wpi' => 45,
                        'rank' => 8,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                465 =>
                    array (
                        'id' => 466,
                        'wpi' => 46,
                        'rank' => 8,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                466 =>
                    array (
                        'id' => 467,
                        'wpi' => 47,
                        'rank' => 8,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                467 =>
                    array (
                        'id' => 468,
                        'wpi' => 48,
                        'rank' => 8,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                468 =>
                    array (
                        'id' => 469,
                        'wpi' => 49,
                        'rank' => 8,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                469 =>
                    array (
                        'id' => 470,
                        'wpi' => 50,
                        'rank' => 8,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                470 =>
                    array (
                        'id' => 471,
                        'wpi' => 51,
                        'rank' => 8,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                471 =>
                    array (
                        'id' => 472,
                        'wpi' => 52,
                        'rank' => 8,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                472 =>
                    array (
                        'id' => 473,
                        'wpi' => 53,
                        'rank' => 8,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                473 =>
                    array (
                        'id' => 474,
                        'wpi' => 54,
                        'rank' => 8,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                474 =>
                    array (
                        'id' => 475,
                        'wpi' => 55,
                        'rank' => 8,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                475 =>
                    array (
                        'id' => 476,
                        'wpi' => 56,
                        'rank' => 8,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                476 =>
                    array (
                        'id' => 477,
                        'wpi' => 57,
                        'rank' => 8,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                477 =>
                    array (
                        'id' => 478,
                        'wpi' => 58,
                        'rank' => 8,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                478 =>
                    array (
                        'id' => 479,
                        'wpi' => 59,
                        'rank' => 8,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                479 =>
                    array (
                        'id' => 480,
                        'wpi' => 60,
                        'rank' => 8,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                480 =>
                    array (
                        'id' => 481,
                        'wpi' => 61,
                        'rank' => 1,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                481 =>
                    array (
                        'id' => 482,
                        'wpi' => 62,
                        'rank' => 1,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                482 =>
                    array (
                        'id' => 483,
                        'wpi' => 63,
                        'rank' => 1,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                483 =>
                    array (
                        'id' => 484,
                        'wpi' => 64,
                        'rank' => 1,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                484 =>
                    array (
                        'id' => 485,
                        'wpi' => 65,
                        'rank' => 1,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                485 =>
                    array (
                        'id' => 486,
                        'wpi' => 66,
                        'rank' => 1,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                486 =>
                    array (
                        'id' => 487,
                        'wpi' => 67,
                        'rank' => 1,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                487 =>
                    array (
                        'id' => 488,
                        'wpi' => 68,
                        'rank' => 1,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                488 =>
                    array (
                        'id' => 489,
                        'wpi' => 69,
                        'rank' => 1,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                489 =>
                    array (
                        'id' => 490,
                        'wpi' => 70,
                        'rank' => 1,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                490 =>
                    array (
                        'id' => 491,
                        'wpi' => 71,
                        'rank' => 1,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                491 =>
                    array (
                        'id' => 492,
                        'wpi' => 72,
                        'rank' => 1,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                492 =>
                    array (
                        'id' => 493,
                        'wpi' => 73,
                        'rank' => 1,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                493 =>
                    array (
                        'id' => 494,
                        'wpi' => 74,
                        'rank' => 1,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                494 =>
                    array (
                        'id' => 495,
                        'wpi' => 75,
                        'rank' => 1,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                495 =>
                    array (
                        'id' => 496,
                        'wpi' => 76,
                        'rank' => 1,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                496 =>
                    array (
                        'id' => 497,
                        'wpi' => 77,
                        'rank' => 1,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                497 =>
                    array (
                        'id' => 498,
                        'wpi' => 78,
                        'rank' => 1,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                498 =>
                    array (
                        'id' => 499,
                        'wpi' => 79,
                        'rank' => 1,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                499 =>
                    array (
                        'id' => 500,
                        'wpi' => 80,
                        'rank' => 1,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));
            \DB::table('pd_fecs')->insert(array (
                0 =>
                    array (
                        'id' => 501,
                        'wpi' => 61,
                        'rank' => 2,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 502,
                        'wpi' => 62,
                        'rank' => 2,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 503,
                        'wpi' => 63,
                        'rank' => 2,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 504,
                        'wpi' => 64,
                        'rank' => 2,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 505,
                        'wpi' => 65,
                        'rank' => 2,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 506,
                        'wpi' => 66,
                        'rank' => 2,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 507,
                        'wpi' => 67,
                        'rank' => 2,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 508,
                        'wpi' => 68,
                        'rank' => 2,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 509,
                        'wpi' => 69,
                        'rank' => 2,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 510,
                        'wpi' => 70,
                        'rank' => 2,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 511,
                        'wpi' => 71,
                        'rank' => 2,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 512,
                        'wpi' => 72,
                        'rank' => 2,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 513,
                        'wpi' => 73,
                        'rank' => 2,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 514,
                        'wpi' => 74,
                        'rank' => 2,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 515,
                        'wpi' => 75,
                        'rank' => 2,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 516,
                        'wpi' => 76,
                        'rank' => 2,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 517,
                        'wpi' => 77,
                        'rank' => 2,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 518,
                        'wpi' => 78,
                        'rank' => 2,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 519,
                        'wpi' => 79,
                        'rank' => 2,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 520,
                        'wpi' => 80,
                        'rank' => 2,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 521,
                        'wpi' => 61,
                        'rank' => 3,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 522,
                        'wpi' => 62,
                        'rank' => 3,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 523,
                        'wpi' => 63,
                        'rank' => 3,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 524,
                        'wpi' => 64,
                        'rank' => 3,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 525,
                        'wpi' => 65,
                        'rank' => 3,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 526,
                        'wpi' => 66,
                        'rank' => 3,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 527,
                        'wpi' => 67,
                        'rank' => 3,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 528,
                        'wpi' => 68,
                        'rank' => 3,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 529,
                        'wpi' => 69,
                        'rank' => 3,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 530,
                        'wpi' => 70,
                        'rank' => 3,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 531,
                        'wpi' => 71,
                        'rank' => 3,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 532,
                        'wpi' => 72,
                        'rank' => 3,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 533,
                        'wpi' => 73,
                        'rank' => 3,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 534,
                        'wpi' => 74,
                        'rank' => 3,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                34 =>
                    array (
                        'id' => 535,
                        'wpi' => 75,
                        'rank' => 3,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                35 =>
                    array (
                        'id' => 536,
                        'wpi' => 76,
                        'rank' => 3,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                36 =>
                    array (
                        'id' => 537,
                        'wpi' => 77,
                        'rank' => 3,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                37 =>
                    array (
                        'id' => 538,
                        'wpi' => 78,
                        'rank' => 3,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                38 =>
                    array (
                        'id' => 539,
                        'wpi' => 79,
                        'rank' => 3,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                39 =>
                    array (
                        'id' => 540,
                        'wpi' => 80,
                        'rank' => 3,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                40 =>
                    array (
                        'id' => 541,
                        'wpi' => 61,
                        'rank' => 4,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                41 =>
                    array (
                        'id' => 542,
                        'wpi' => 62,
                        'rank' => 4,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                42 =>
                    array (
                        'id' => 543,
                        'wpi' => 63,
                        'rank' => 4,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                43 =>
                    array (
                        'id' => 544,
                        'wpi' => 64,
                        'rank' => 4,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                44 =>
                    array (
                        'id' => 545,
                        'wpi' => 65,
                        'rank' => 4,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                45 =>
                    array (
                        'id' => 546,
                        'wpi' => 66,
                        'rank' => 4,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                46 =>
                    array (
                        'id' => 547,
                        'wpi' => 67,
                        'rank' => 4,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                47 =>
                    array (
                        'id' => 548,
                        'wpi' => 68,
                        'rank' => 4,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                48 =>
                    array (
                        'id' => 549,
                        'wpi' => 69,
                        'rank' => 4,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                49 =>
                    array (
                        'id' => 550,
                        'wpi' => 70,
                        'rank' => 4,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                50 =>
                    array (
                        'id' => 551,
                        'wpi' => 71,
                        'rank' => 4,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                51 =>
                    array (
                        'id' => 552,
                        'wpi' => 72,
                        'rank' => 4,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                52 =>
                    array (
                        'id' => 553,
                        'wpi' => 73,
                        'rank' => 4,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                53 =>
                    array (
                        'id' => 554,
                        'wpi' => 74,
                        'rank' => 4,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                54 =>
                    array (
                        'id' => 555,
                        'wpi' => 75,
                        'rank' => 4,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                55 =>
                    array (
                        'id' => 556,
                        'wpi' => 76,
                        'rank' => 4,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                56 =>
                    array (
                        'id' => 557,
                        'wpi' => 77,
                        'rank' => 4,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                57 =>
                    array (
                        'id' => 558,
                        'wpi' => 78,
                        'rank' => 4,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                58 =>
                    array (
                        'id' => 559,
                        'wpi' => 79,
                        'rank' => 4,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                59 =>
                    array (
                        'id' => 560,
                        'wpi' => 80,
                        'rank' => 4,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                60 =>
                    array (
                        'id' => 561,
                        'wpi' => 61,
                        'rank' => 5,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                61 =>
                    array (
                        'id' => 562,
                        'wpi' => 62,
                        'rank' => 5,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                62 =>
                    array (
                        'id' => 563,
                        'wpi' => 63,
                        'rank' => 5,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                63 =>
                    array (
                        'id' => 564,
                        'wpi' => 64,
                        'rank' => 5,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                64 =>
                    array (
                        'id' => 565,
                        'wpi' => 65,
                        'rank' => 5,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                65 =>
                    array (
                        'id' => 566,
                        'wpi' => 66,
                        'rank' => 5,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                66 =>
                    array (
                        'id' => 567,
                        'wpi' => 67,
                        'rank' => 5,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                67 =>
                    array (
                        'id' => 568,
                        'wpi' => 68,
                        'rank' => 5,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                68 =>
                    array (
                        'id' => 569,
                        'wpi' => 69,
                        'rank' => 5,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                69 =>
                    array (
                        'id' => 570,
                        'wpi' => 70,
                        'rank' => 5,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                70 =>
                    array (
                        'id' => 571,
                        'wpi' => 71,
                        'rank' => 5,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                71 =>
                    array (
                        'id' => 572,
                        'wpi' => 72,
                        'rank' => 5,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                72 =>
                    array (
                        'id' => 573,
                        'wpi' => 73,
                        'rank' => 5,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                73 =>
                    array (
                        'id' => 574,
                        'wpi' => 74,
                        'rank' => 5,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                74 =>
                    array (
                        'id' => 575,
                        'wpi' => 75,
                        'rank' => 5,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                75 =>
                    array (
                        'id' => 576,
                        'wpi' => 76,
                        'rank' => 5,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                76 =>
                    array (
                        'id' => 577,
                        'wpi' => 77,
                        'rank' => 5,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                77 =>
                    array (
                        'id' => 578,
                        'wpi' => 78,
                        'rank' => 5,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                78 =>
                    array (
                        'id' => 579,
                        'wpi' => 79,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                79 =>
                    array (
                        'id' => 580,
                        'wpi' => 80,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                80 =>
                    array (
                        'id' => 581,
                        'wpi' => 61,
                        'rank' => 6,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                81 =>
                    array (
                        'id' => 582,
                        'wpi' => 62,
                        'rank' => 6,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                82 =>
                    array (
                        'id' => 583,
                        'wpi' => 63,
                        'rank' => 6,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                83 =>
                    array (
                        'id' => 584,
                        'wpi' => 64,
                        'rank' => 6,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                84 =>
                    array (
                        'id' => 585,
                        'wpi' => 65,
                        'rank' => 6,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                85 =>
                    array (
                        'id' => 586,
                        'wpi' => 66,
                        'rank' => 6,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                86 =>
                    array (
                        'id' => 587,
                        'wpi' => 67,
                        'rank' => 6,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                87 =>
                    array (
                        'id' => 588,
                        'wpi' => 68,
                        'rank' => 6,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                88 =>
                    array (
                        'id' => 589,
                        'wpi' => 69,
                        'rank' => 6,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                89 =>
                    array (
                        'id' => 590,
                        'wpi' => 70,
                        'rank' => 6,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                90 =>
                    array (
                        'id' => 591,
                        'wpi' => 71,
                        'rank' => 6,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                91 =>
                    array (
                        'id' => 592,
                        'wpi' => 72,
                        'rank' => 6,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                92 =>
                    array (
                        'id' => 593,
                        'wpi' => 73,
                        'rank' => 6,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                93 =>
                    array (
                        'id' => 594,
                        'wpi' => 74,
                        'rank' => 6,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                94 =>
                    array (
                        'id' => 595,
                        'wpi' => 75,
                        'rank' => 6,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                95 =>
                    array (
                        'id' => 596,
                        'wpi' => 76,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                96 =>
                    array (
                        'id' => 597,
                        'wpi' => 77,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                97 =>
                    array (
                        'id' => 598,
                        'wpi' => 78,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                98 =>
                    array (
                        'id' => 599,
                        'wpi' => 79,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                99 =>
                    array (
                        'id' => 600,
                        'wpi' => 80,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                100 =>
                    array (
                        'id' => 601,
                        'wpi' => 61,
                        'rank' => 7,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                101 =>
                    array (
                        'id' => 602,
                        'wpi' => 62,
                        'rank' => 7,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                102 =>
                    array (
                        'id' => 603,
                        'wpi' => 63,
                        'rank' => 7,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                103 =>
                    array (
                        'id' => 604,
                        'wpi' => 64,
                        'rank' => 7,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                104 =>
                    array (
                        'id' => 605,
                        'wpi' => 65,
                        'rank' => 7,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                105 =>
                    array (
                        'id' => 606,
                        'wpi' => 66,
                        'rank' => 7,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                106 =>
                    array (
                        'id' => 607,
                        'wpi' => 67,
                        'rank' => 7,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                107 =>
                    array (
                        'id' => 608,
                        'wpi' => 68,
                        'rank' => 7,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                108 =>
                    array (
                        'id' => 609,
                        'wpi' => 69,
                        'rank' => 7,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                109 =>
                    array (
                        'id' => 610,
                        'wpi' => 70,
                        'rank' => 7,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                110 =>
                    array (
                        'id' => 611,
                        'wpi' => 71,
                        'rank' => 7,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                111 =>
                    array (
                        'id' => 612,
                        'wpi' => 72,
                        'rank' => 7,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                112 =>
                    array (
                        'id' => 613,
                        'wpi' => 73,
                        'rank' => 7,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                113 =>
                    array (
                        'id' => 614,
                        'wpi' => 74,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                114 =>
                    array (
                        'id' => 615,
                        'wpi' => 75,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                115 =>
                    array (
                        'id' => 616,
                        'wpi' => 76,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                116 =>
                    array (
                        'id' => 617,
                        'wpi' => 77,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                117 =>
                    array (
                        'id' => 618,
                        'wpi' => 78,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                118 =>
                    array (
                        'id' => 619,
                        'wpi' => 79,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                119 =>
                    array (
                        'id' => 620,
                        'wpi' => 80,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                120 =>
                    array (
                        'id' => 621,
                        'wpi' => 61,
                        'rank' => 8,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                121 =>
                    array (
                        'id' => 622,
                        'wpi' => 62,
                        'rank' => 8,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                122 =>
                    array (
                        'id' => 623,
                        'wpi' => 63,
                        'rank' => 8,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                123 =>
                    array (
                        'id' => 624,
                        'wpi' => 64,
                        'rank' => 8,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                124 =>
                    array (
                        'id' => 625,
                        'wpi' => 65,
                        'rank' => 8,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                125 =>
                    array (
                        'id' => 626,
                        'wpi' => 66,
                        'rank' => 8,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                126 =>
                    array (
                        'id' => 627,
                        'wpi' => 67,
                        'rank' => 8,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                127 =>
                    array (
                        'id' => 628,
                        'wpi' => 68,
                        'rank' => 8,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                128 =>
                    array (
                        'id' => 629,
                        'wpi' => 69,
                        'rank' => 8,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                129 =>
                    array (
                        'id' => 630,
                        'wpi' => 70,
                        'rank' => 8,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                130 =>
                    array (
                        'id' => 631,
                        'wpi' => 71,
                        'rank' => 8,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                131 =>
                    array (
                        'id' => 632,
                        'wpi' => 72,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                132 =>
                    array (
                        'id' => 633,
                        'wpi' => 73,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                133 =>
                    array (
                        'id' => 634,
                        'wpi' => 74,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                134 =>
                    array (
                        'id' => 635,
                        'wpi' => 75,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                135 =>
                    array (
                        'id' => 636,
                        'wpi' => 76,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                136 =>
                    array (
                        'id' => 637,
                        'wpi' => 77,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                137 =>
                    array (
                        'id' => 638,
                        'wpi' => 78,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                138 =>
                    array (
                        'id' => 639,
                        'wpi' => 79,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                139 =>
                    array (
                        'id' => 640,
                        'wpi' => 80,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                140 =>
                    array (
                        'id' => 641,
                        'wpi' => 81,
                        'rank' => 1,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                141 =>
                    array (
                        'id' => 642,
                        'wpi' => 82,
                        'rank' => 1,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                142 =>
                    array (
                        'id' => 643,
                        'wpi' => 83,
                        'rank' => 1,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                143 =>
                    array (
                        'id' => 644,
                        'wpi' => 84,
                        'rank' => 1,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                144 =>
                    array (
                        'id' => 645,
                        'wpi' => 85,
                        'rank' => 1,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                145 =>
                    array (
                        'id' => 646,
                        'wpi' => 86,
                        'rank' => 1,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                146 =>
                    array (
                        'id' => 647,
                        'wpi' => 87,
                        'rank' => 1,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                147 =>
                    array (
                        'id' => 648,
                        'wpi' => 88,
                        'rank' => 1,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                148 =>
                    array (
                        'id' => 649,
                        'wpi' => 89,
                        'rank' => 1,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                149 =>
                    array (
                        'id' => 650,
                        'wpi' => 90,
                        'rank' => 1,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                150 =>
                    array (
                        'id' => 651,
                        'wpi' => 91,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                151 =>
                    array (
                        'id' => 652,
                        'wpi' => 92,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                152 =>
                    array (
                        'id' => 653,
                        'wpi' => 93,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                153 =>
                    array (
                        'id' => 654,
                        'wpi' => 94,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                154 =>
                    array (
                        'id' => 655,
                        'wpi' => 95,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                155 =>
                    array (
                        'id' => 656,
                        'wpi' => 96,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                156 =>
                    array (
                        'id' => 657,
                        'wpi' => 97,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                157 =>
                    array (
                        'id' => 658,
                        'wpi' => 98,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                158 =>
                    array (
                        'id' => 659,
                        'wpi' => 99,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                159 =>
                    array (
                        'id' => 660,
                        'wpi' => 100,
                        'rank' => 1,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                160 =>
                    array (
                        'id' => 661,
                        'wpi' => 81,
                        'rank' => 2,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                161 =>
                    array (
                        'id' => 662,
                        'wpi' => 82,
                        'rank' => 2,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                162 =>
                    array (
                        'id' => 663,
                        'wpi' => 83,
                        'rank' => 2,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                163 =>
                    array (
                        'id' => 664,
                        'wpi' => 84,
                        'rank' => 2,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                164 =>
                    array (
                        'id' => 665,
                        'wpi' => 85,
                        'rank' => 2,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                165 =>
                    array (
                        'id' => 666,
                        'wpi' => 86,
                        'rank' => 2,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                166 =>
                    array (
                        'id' => 667,
                        'wpi' => 87,
                        'rank' => 2,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                167 =>
                    array (
                        'id' => 668,
                        'wpi' => 88,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                168 =>
                    array (
                        'id' => 669,
                        'wpi' => 89,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                169 =>
                    array (
                        'id' => 670,
                        'wpi' => 90,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                170 =>
                    array (
                        'id' => 671,
                        'wpi' => 91,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                171 =>
                    array (
                        'id' => 672,
                        'wpi' => 92,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                172 =>
                    array (
                        'id' => 673,
                        'wpi' => 93,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                173 =>
                    array (
                        'id' => 674,
                        'wpi' => 94,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                174 =>
                    array (
                        'id' => 675,
                        'wpi' => 95,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                175 =>
                    array (
                        'id' => 676,
                        'wpi' => 96,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                176 =>
                    array (
                        'id' => 677,
                        'wpi' => 97,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                177 =>
                    array (
                        'id' => 678,
                        'wpi' => 98,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                178 =>
                    array (
                        'id' => 679,
                        'wpi' => 99,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                179 =>
                    array (
                        'id' => 680,
                        'wpi' => 100,
                        'rank' => 2,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                180 =>
                    array (
                        'id' => 681,
                        'wpi' => 81,
                        'rank' => 3,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                181 =>
                    array (
                        'id' => 682,
                        'wpi' => 82,
                        'rank' => 3,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                182 =>
                    array (
                        'id' => 683,
                        'wpi' => 83,
                        'rank' => 3,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                183 =>
                    array (
                        'id' => 684,
                        'wpi' => 84,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                184 =>
                    array (
                        'id' => 685,
                        'wpi' => 85,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                185 =>
                    array (
                        'id' => 686,
                        'wpi' => 86,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                186 =>
                    array (
                        'id' => 687,
                        'wpi' => 87,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                187 =>
                    array (
                        'id' => 688,
                        'wpi' => 88,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                188 =>
                    array (
                        'id' => 689,
                        'wpi' => 89,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                189 =>
                    array (
                        'id' => 690,
                        'wpi' => 90,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                190 =>
                    array (
                        'id' => 691,
                        'wpi' => 91,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                191 =>
                    array (
                        'id' => 692,
                        'wpi' => 92,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                192 =>
                    array (
                        'id' => 693,
                        'wpi' => 93,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                193 =>
                    array (
                        'id' => 694,
                        'wpi' => 94,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                194 =>
                    array (
                        'id' => 695,
                        'wpi' => 95,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                195 =>
                    array (
                        'id' => 696,
                        'wpi' => 96,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                196 =>
                    array (
                        'id' => 697,
                        'wpi' => 97,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                197 =>
                    array (
                        'id' => 698,
                        'wpi' => 98,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                198 =>
                    array (
                        'id' => 699,
                        'wpi' => 99,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                199 =>
                    array (
                        'id' => 700,
                        'wpi' => 100,
                        'rank' => 3,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                200 =>
                    array (
                        'id' => 701,
                        'wpi' => 81,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                201 =>
                    array (
                        'id' => 702,
                        'wpi' => 82,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                202 =>
                    array (
                        'id' => 703,
                        'wpi' => 83,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                203 =>
                    array (
                        'id' => 704,
                        'wpi' => 84,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                204 =>
                    array (
                        'id' => 705,
                        'wpi' => 85,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                205 =>
                    array (
                        'id' => 706,
                        'wpi' => 86,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                206 =>
                    array (
                        'id' => 707,
                        'wpi' => 87,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                207 =>
                    array (
                        'id' => 708,
                        'wpi' => 88,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                208 =>
                    array (
                        'id' => 709,
                        'wpi' => 89,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                209 =>
                    array (
                        'id' => 710,
                        'wpi' => 90,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                210 =>
                    array (
                        'id' => 711,
                        'wpi' => 91,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                211 =>
                    array (
                        'id' => 712,
                        'wpi' => 92,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                212 =>
                    array (
                        'id' => 713,
                        'wpi' => 93,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                213 =>
                    array (
                        'id' => 714,
                        'wpi' => 94,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                214 =>
                    array (
                        'id' => 715,
                        'wpi' => 95,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                215 =>
                    array (
                        'id' => 716,
                        'wpi' => 96,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                216 =>
                    array (
                        'id' => 717,
                        'wpi' => 97,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                217 =>
                    array (
                        'id' => 718,
                        'wpi' => 98,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                218 =>
                    array (
                        'id' => 719,
                        'wpi' => 99,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                219 =>
                    array (
                        'id' => 720,
                        'wpi' => 100,
                        'rank' => 4,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                220 =>
                    array (
                        'id' => 721,
                        'wpi' => 81,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                221 =>
                    array (
                        'id' => 722,
                        'wpi' => 82,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                222 =>
                    array (
                        'id' => 723,
                        'wpi' => 83,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                223 =>
                    array (
                        'id' => 724,
                        'wpi' => 84,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                224 =>
                    array (
                        'id' => 725,
                        'wpi' => 85,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                225 =>
                    array (
                        'id' => 726,
                        'wpi' => 86,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                226 =>
                    array (
                        'id' => 727,
                        'wpi' => 87,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                227 =>
                    array (
                        'id' => 728,
                        'wpi' => 88,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                228 =>
                    array (
                        'id' => 729,
                        'wpi' => 89,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                229 =>
                    array (
                        'id' => 730,
                        'wpi' => 90,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                230 =>
                    array (
                        'id' => 731,
                        'wpi' => 91,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                231 =>
                    array (
                        'id' => 732,
                        'wpi' => 92,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                232 =>
                    array (
                        'id' => 733,
                        'wpi' => 93,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                233 =>
                    array (
                        'id' => 734,
                        'wpi' => 94,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                234 =>
                    array (
                        'id' => 735,
                        'wpi' => 95,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                235 =>
                    array (
                        'id' => 736,
                        'wpi' => 96,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                236 =>
                    array (
                        'id' => 737,
                        'wpi' => 97,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                237 =>
                    array (
                        'id' => 738,
                        'wpi' => 98,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                238 =>
                    array (
                        'id' => 739,
                        'wpi' => 99,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                239 =>
                    array (
                        'id' => 740,
                        'wpi' => 100,
                        'rank' => 5,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                240 =>
                    array (
                        'id' => 741,
                        'wpi' => 81,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                241 =>
                    array (
                        'id' => 742,
                        'wpi' => 82,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                242 =>
                    array (
                        'id' => 743,
                        'wpi' => 83,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                243 =>
                    array (
                        'id' => 744,
                        'wpi' => 84,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                244 =>
                    array (
                        'id' => 745,
                        'wpi' => 85,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                245 =>
                    array (
                        'id' => 746,
                        'wpi' => 86,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                246 =>
                    array (
                        'id' => 747,
                        'wpi' => 87,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                247 =>
                    array (
                        'id' => 748,
                        'wpi' => 88,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                248 =>
                    array (
                        'id' => 749,
                        'wpi' => 89,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                249 =>
                    array (
                        'id' => 750,
                        'wpi' => 90,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                250 =>
                    array (
                        'id' => 751,
                        'wpi' => 91,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                251 =>
                    array (
                        'id' => 752,
                        'wpi' => 92,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                252 =>
                    array (
                        'id' => 753,
                        'wpi' => 93,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                253 =>
                    array (
                        'id' => 754,
                        'wpi' => 94,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                254 =>
                    array (
                        'id' => 755,
                        'wpi' => 95,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                255 =>
                    array (
                        'id' => 756,
                        'wpi' => 96,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                256 =>
                    array (
                        'id' => 757,
                        'wpi' => 97,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                257 =>
                    array (
                        'id' => 758,
                        'wpi' => 98,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                258 =>
                    array (
                        'id' => 759,
                        'wpi' => 99,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                259 =>
                    array (
                        'id' => 760,
                        'wpi' => 100,
                        'rank' => 6,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                260 =>
                    array (
                        'id' => 761,
                        'wpi' => 81,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                261 =>
                    array (
                        'id' => 762,
                        'wpi' => 82,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                262 =>
                    array (
                        'id' => 763,
                        'wpi' => 83,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                263 =>
                    array (
                        'id' => 764,
                        'wpi' => 84,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                264 =>
                    array (
                        'id' => 765,
                        'wpi' => 85,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                265 =>
                    array (
                        'id' => 766,
                        'wpi' => 86,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                266 =>
                    array (
                        'id' => 767,
                        'wpi' => 87,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                267 =>
                    array (
                        'id' => 768,
                        'wpi' => 88,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                268 =>
                    array (
                        'id' => 769,
                        'wpi' => 89,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                269 =>
                    array (
                        'id' => 770,
                        'wpi' => 90,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                270 =>
                    array (
                        'id' => 771,
                        'wpi' => 91,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                271 =>
                    array (
                        'id' => 772,
                        'wpi' => 92,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                272 =>
                    array (
                        'id' => 773,
                        'wpi' => 93,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                273 =>
                    array (
                        'id' => 774,
                        'wpi' => 94,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                274 =>
                    array (
                        'id' => 775,
                        'wpi' => 95,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                275 =>
                    array (
                        'id' => 776,
                        'wpi' => 96,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                276 =>
                    array (
                        'id' => 777,
                        'wpi' => 97,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                277 =>
                    array (
                        'id' => 778,
                        'wpi' => 98,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                278 =>
                    array (
                        'id' => 779,
                        'wpi' => 99,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                279 =>
                    array (
                        'id' => 780,
                        'wpi' => 100,
                        'rank' => 7,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                280 =>
                    array (
                        'id' => 781,
                        'wpi' => 81,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                281 =>
                    array (
                        'id' => 782,
                        'wpi' => 82,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                282 =>
                    array (
                        'id' => 783,
                        'wpi' => 83,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                283 =>
                    array (
                        'id' => 784,
                        'wpi' => 84,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                284 =>
                    array (
                        'id' => 785,
                        'wpi' => 85,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                285 =>
                    array (
                        'id' => 786,
                        'wpi' => 86,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                286 =>
                    array (
                        'id' => 787,
                        'wpi' => 87,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                287 =>
                    array (
                        'id' => 788,
                        'wpi' => 88,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                288 =>
                    array (
                        'id' => 789,
                        'wpi' => 89,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                289 =>
                    array (
                        'id' => 790,
                        'wpi' => 90,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                290 =>
                    array (
                        'id' => 791,
                        'wpi' => 91,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                291 =>
                    array (
                        'id' => 792,
                        'wpi' => 92,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                292 =>
                    array (
                        'id' => 793,
                        'wpi' => 93,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                293 =>
                    array (
                        'id' => 794,
                        'wpi' => 94,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                294 =>
                    array (
                        'id' => 795,
                        'wpi' => 95,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                295 =>
                    array (
                        'id' => 796,
                        'wpi' => 96,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                296 =>
                    array (
                        'id' => 797,
                        'wpi' => 97,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                297 =>
                    array (
                        'id' => 798,
                        'wpi' => 98,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                298 =>
                    array (
                        'id' => 799,
                        'wpi' => 99,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                299 =>
                    array (
                        'id' => 800,
                        'wpi' => 100,
                        'rank' => 8,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));

            $this->enableForeignKeys('pd_fecs');

$sql = <<<SQL
DROP TRIGGER IF EXISTS pd_fecs_ro ON pd_fecs;
create trigger pd_fecs_ro before insert or update or delete or truncate on pd_fecs for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);
        }

    }
}