<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class MemberTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('member_types');
        $this->delete('member_types');
        
        \DB::table('member_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Employer',
                'created_at' => '2017-04-19 09:53:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Employee',
                'created_at' => '2017-04-19 09:53:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Insurance/Health Care Provider',
                'created_at' => '2017-04-19 09:53:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Dependent',
                    'created_at' => '2017-07-11 09:53:33',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Pensioner',
                    'created_at' => '2017-07-16 09:53:33',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'WCF',
                    'created_at' => '2017-07-25 09:53:33',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'name' => 'HSP/Health Service Provider',
                    'created_at' => '2017-07-25 09:53:33',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('member_types');
    }
}