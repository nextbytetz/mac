<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class BodyPartInjuryGroupTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */

    public function run()
    {
        $this->disableForeignKeys('body_part_injury_groups');
        $this->delete('body_part_injury_groups');

        \DB::table('body_part_injury_groups')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Head',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Neck (including throat and cervical vertebrae)',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Trunk',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Upper Limb',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Lower Limb',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Systemic injuries',
                    'created_at' => '2017-04-19 07:57:52',
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('body_part_injury_groups');

    }

}