<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class AlertTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('alert_types');
        
        \DB::table('alert_types')->insert(array (
            0 => 
            array (
                'name' => 'create_receipt',
                'display_name' => 'Create Receipt',
                'icon' => NULL,
                'created_at' => '2017-04-18 17:01:04',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'name' => 'approve_notification',
                'display_name' => 'Approve Notifications',
                'icon' => NULL,
                'created_at' => '2017-04-18 17:01:04',
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
    }
}