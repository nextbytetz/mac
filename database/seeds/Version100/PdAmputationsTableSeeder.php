<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PdAmputationsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $count = \DB::table('pd_amputations')->limit(1)->count();
        //$count = 0;

        if (!$count) {
            $this->disableForeignKeys('pd_amputations');
            $this->delete('pd_amputations');

            \DB::table('pd_amputations')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'name' => 'Loss of two limbs',
                        'percent' => '100.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'name' => 'Loss of both hands, or of all fingers and both thumbs',
                        'percent' => '100.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'name' => 'Total loss of sigh',
                        'percent' => '100.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'name' => 'Total paralysis',
                        'percent' => '100.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'name' => 'Injuries resulting in employee being permanent bedridden',
                        'percent' => '100.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'name' => 'Any other injury causing permanent total disablement',
                        'percent' => '100.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'name' => 'Loss of arm at shoulder',
                        'percent' => '70.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'name' => 'Loss of arm between elbow and shoulder',
                        'percent' => '68.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'name' => 'Loss of arm at elbow',
                        'percent' => '67.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'name' => 'Loss of arm between wrist and elbow',
                        'percent' => '65.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'name' => 'Loss of hand at wrist',
                        'percent' => '60.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'name' => 'Loss of four fingers and thumb of one hand',
                        'percent' => '60.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'name' => 'Loss of four fingers',
                        'percent' => '40.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'name' => 'Loss of thumb - both phalanges',
                        'percent' => '40.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'name' => 'Loss of thumb - One phalanx',
                        'percent' => '15.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'name' => 'Loss of index finger - Three phalanges',
                        'percent' => '10.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'name' => 'Loss of index finger - Two phalanges',
                        'percent' => '8.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'name' => 'Loss of index finger - One phalanx',
                        'percent' => '5.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'name' => 'Loss of middle finger - Three phalanges',
                        'percent' => '8.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'name' => 'Loss of middle finger -Two phalanges',
                        'percent' => '6.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'name' => 'Loss of middle finger - One phalanx',
                        'percent' => '4.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'name' => 'Loss of ring finger - three phalanges',
                        'percent' => '6.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'name' => 'Loss of ring finger - Two phalanges',
                        'percent' => '5.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'name' => 'Loss of ring finger - One phalanx',
                        'percent' => '3.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'name' => 'Loss of little finger - three phalanges',
                        'percent' => '4.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'name' => 'Loss of little finger - two phalanges',
                        'percent' => '3.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'name' => 'Loss of little finger - one phalanx',
                        'percent' => '2.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'name' => 'Loss of metacarpals - first, second or third (additional)',
                        'percent' => '4.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'name' => 'Loss of metacarpals - fourth or fifth (additional)',
                        'percent' => '2.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'name' => 'Loss of toes (limb) - at hip',
                        'percent' => '70.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'name' => 'Loss of toes (limb) - Between knee and hp',
                        'percent' => '70.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'name' => 'Loss of toes (limb) - below knee',
                        'percent' => '45.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'name' => 'Loss of toes - all',
                        'percent' => '15.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'name' => 'Loss of toes - Big, both phalanges',
                        'percent' => '7.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                34 =>
                    array (
                        'id' => 35,
                        'name' => 'Loss of toes - Big, one phalanx',
                        'percent' => '3.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                35 =>
                    array (
                        'id' => 36,
                        'name' => 'Toes other than big one - Loss of four toes',
                        'percent' => '7.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                36 =>
                    array (
                        'id' => 37,
                        'name' => 'Toes other than big one - Loss of three toes',
                        'percent' => '5.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                37 =>
                    array (
                        'id' => 38,
                        'name' => 'Toes other than big one - Loss of two toes',
                        'percent' => '3.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                38 =>
                    array (
                        'id' => 39,
                        'name' => 'Toes other than big one - Loss of one toe',
                        'percent' => '1.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                39 =>
                    array (
                        'id' => 40,
                        'name' => 'Loss of eye - whole eye',
                        'percent' => '30.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                40 =>
                    array (
                        'id' => 41,
                        'name' => 'Loss of eye - sight',
                        'percent' => '30.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                41 =>
                    array (
                        'id' => 42,
                        'name' => 'Loss of eye - sight except perception of light',
                        'percent' => '30.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                42 =>
                    array (
                        'id' => 43,
                        'name' => 'Loss of hearing - both ears',
                        'percent' => '50.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                43 =>
                    array (
                        'id' => 44,
                        'name' => 'Loss of hearing - one ear',
                        'percent' => '7.00',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));

            $this->enableForeignKeys('pd_amputations');

$sql = <<<SQL
DROP TRIGGER IF EXISTS pd_amputations_ro ON pd_amputations;
create trigger pd_amputations_ro before insert or update or delete or truncate on pd_amputations for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);
        }

        
    }
}