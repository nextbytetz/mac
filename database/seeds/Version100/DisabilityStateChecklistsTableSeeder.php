<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DisabilityStateChecklistsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('disability_state_checklists');
        $this->delete('disability_state_checklists');
        
        \DB::table('disability_state_checklists')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Hospitalization',
                'benefit_type_id' => 8,
                'percent_of_ed_ld' => 100,
                'created_at' => '2017-04-19 10:10:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Day off(ED)',
                'benefit_type_id' => 8,
                'percent_of_ed_ld' => 100,
                'created_at' => '2017-04-19 10:10:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Light Duties',
                'benefit_type_id' => 7,
                'percent_of_ed_ld' => 50,
                'created_at' => '2017-04-19 10:10:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Bedridden',
                'benefit_type_id' => 8,
                'percent_of_ed_ld' => 0,
                'created_at' => '2017-04-19 10:10:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('disability_state_checklists');
    }
}