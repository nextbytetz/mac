<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PdAgeAdjustmentsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $count = \DB::table('pd_age_adjustments')->limit(1)->count();

        if (!$count) {
            $this->disableForeignKeys('pd_age_adjustments');
            $this->delete('pd_age_adjustments');

            \DB::table('pd_age_adjustments')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 3,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 4,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 5,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 6,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 7,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 8,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 9,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 10,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 11,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 12,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 13,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 14,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 15,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 16,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 17,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 18,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 19,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 20,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 21,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 22,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 23,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 24,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 25,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 26,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 27,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 28,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 29,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 30,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 31,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 32,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 33,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 34,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                34 =>
                    array (
                        'id' => 35,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 35,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                35 =>
                    array (
                        'id' => 36,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 36,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                36 =>
                    array (
                        'id' => 37,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 37,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                37 =>
                    array (
                        'id' => 38,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 38,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                38 =>
                    array (
                        'id' => 39,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 39,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                39 =>
                    array (
                        'id' => 40,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 40,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                40 =>
                    array (
                        'id' => 41,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 41,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                41 =>
                    array (
                        'id' => 42,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 42,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                42 =>
                    array (
                        'id' => 43,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 43,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                43 =>
                    array (
                        'id' => 44,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 44,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                44 =>
                    array (
                        'id' => 45,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 45,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                45 =>
                    array (
                        'id' => 46,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 46,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                46 =>
                    array (
                        'id' => 47,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 47,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                47 =>
                    array (
                        'id' => 48,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 48,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                48 =>
                    array (
                        'id' => 49,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 49,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                49 =>
                    array (
                        'id' => 50,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 50,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                50 =>
                    array (
                        'id' => 51,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 51,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                51 =>
                    array (
                        'id' => 52,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 52,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                52 =>
                    array (
                        'id' => 53,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 53,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                53 =>
                    array (
                        'id' => 54,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 54,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                54 =>
                    array (
                        'id' => 55,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 55,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                55 =>
                    array (
                        'id' => 56,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 56,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                56 =>
                    array (
                        'id' => 57,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 57,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                57 =>
                    array (
                        'id' => 58,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 58,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                58 =>
                    array (
                        'id' => 59,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 59,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                59 =>
                    array (
                        'id' => 60,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 60,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                60 =>
                    array (
                        'id' => 61,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 61,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                61 =>
                    array (
                        'id' => 62,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 62,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                62 =>
                    array (
                        'id' => 63,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 63,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                63 =>
                    array (
                        'id' => 64,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 64,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                64 =>
                    array (
                        'id' => 65,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 65,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                65 =>
                    array (
                        'id' => 66,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 66,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                66 =>
                    array (
                        'id' => 67,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 67,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                67 =>
                    array (
                        'id' => 68,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 68,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                68 =>
                    array (
                        'id' => 69,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 69,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                69 =>
                    array (
                        'id' => 70,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 70,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                70 =>
                    array (
                        'id' => 71,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 71,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                71 =>
                    array (
                        'id' => 72,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 72,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                72 =>
                    array (
                        'id' => 73,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 73,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                73 =>
                    array (
                        'id' => 74,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 74,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                74 =>
                    array (
                        'id' => 75,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 75,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                75 =>
                    array (
                        'id' => 76,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 76,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                76 =>
                    array (
                        'id' => 77,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 77,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                77 =>
                    array (
                        'id' => 78,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 78,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                78 =>
                    array (
                        'id' => 79,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 79,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                79 =>
                    array (
                        'id' => 80,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 80,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                80 =>
                    array (
                        'id' => 81,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 81,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                81 =>
                    array (
                        'id' => 82,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 82,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                82 =>
                    array (
                        'id' => 83,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 83,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                83 =>
                    array (
                        'id' => 84,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 84,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                84 =>
                    array (
                        'id' => 85,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 85,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                85 =>
                    array (
                        'id' => 86,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 86,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                86 =>
                    array (
                        'id' => 87,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 87,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                87 =>
                    array (
                        'id' => 88,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 88,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                88 =>
                    array (
                        'id' => 89,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 89,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                89 =>
                    array (
                        'id' => 90,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 90,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                90 =>
                    array (
                        'id' => 91,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 91,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                91 =>
                    array (
                        'id' => 92,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 92,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                92 =>
                    array (
                        'id' => 93,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 93,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                93 =>
                    array (
                        'id' => 94,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 94,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                94 =>
                    array (
                        'id' => 95,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 95,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                95 =>
                    array (
                        'id' => 96,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 96,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                96 =>
                    array (
                        'id' => 97,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 97,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                97 =>
                    array (
                        'id' => 98,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 98,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                98 =>
                    array (
                        'id' => 99,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                99 =>
                    array (
                        'id' => 100,
                        'pd_age_range_id' => 1,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                100 =>
                    array (
                        'id' => 101,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                101 =>
                    array (
                        'id' => 102,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                102 =>
                    array (
                        'id' => 103,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 3,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                103 =>
                    array (
                        'id' => 104,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 4,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                104 =>
                    array (
                        'id' => 105,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 5,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                105 =>
                    array (
                        'id' => 106,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 6,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                106 =>
                    array (
                        'id' => 107,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 7,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                107 =>
                    array (
                        'id' => 108,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 8,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                108 =>
                    array (
                        'id' => 109,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 9,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                109 =>
                    array (
                        'id' => 110,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 10,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                110 =>
                    array (
                        'id' => 111,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 11,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                111 =>
                    array (
                        'id' => 112,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 12,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                112 =>
                    array (
                        'id' => 113,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 13,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                113 =>
                    array (
                        'id' => 114,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 14,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                114 =>
                    array (
                        'id' => 115,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 15,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                115 =>
                    array (
                        'id' => 116,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 16,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                116 =>
                    array (
                        'id' => 117,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 17,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                117 =>
                    array (
                        'id' => 118,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 18,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                118 =>
                    array (
                        'id' => 119,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 19,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                119 =>
                    array (
                        'id' => 120,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 20,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                120 =>
                    array (
                        'id' => 121,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 21,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                121 =>
                    array (
                        'id' => 122,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 22,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                122 =>
                    array (
                        'id' => 123,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 23,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                123 =>
                    array (
                        'id' => 124,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 24,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                124 =>
                    array (
                        'id' => 125,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 25,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                125 =>
                    array (
                        'id' => 126,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 26,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                126 =>
                    array (
                        'id' => 127,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 27,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                127 =>
                    array (
                        'id' => 128,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 28,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                128 =>
                    array (
                        'id' => 129,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 29,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                129 =>
                    array (
                        'id' => 130,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 30,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                130 =>
                    array (
                        'id' => 131,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 31,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                131 =>
                    array (
                        'id' => 132,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 32,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                132 =>
                    array (
                        'id' => 133,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 33,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                133 =>
                    array (
                        'id' => 134,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 34,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                134 =>
                    array (
                        'id' => 135,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 35,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                135 =>
                    array (
                        'id' => 136,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 36,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                136 =>
                    array (
                        'id' => 137,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 37,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                137 =>
                    array (
                        'id' => 138,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 38,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                138 =>
                    array (
                        'id' => 139,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 39,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                139 =>
                    array (
                        'id' => 140,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 40,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                140 =>
                    array (
                        'id' => 141,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 41,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                141 =>
                    array (
                        'id' => 142,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 42,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                142 =>
                    array (
                        'id' => 143,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 43,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                143 =>
                    array (
                        'id' => 144,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 44,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                144 =>
                    array (
                        'id' => 145,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 45,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                145 =>
                    array (
                        'id' => 146,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 46,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                146 =>
                    array (
                        'id' => 147,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 47,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                147 =>
                    array (
                        'id' => 148,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 48,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                148 =>
                    array (
                        'id' => 149,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 49,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                149 =>
                    array (
                        'id' => 150,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 50,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                150 =>
                    array (
                        'id' => 151,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 51,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                151 =>
                    array (
                        'id' => 152,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 52,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                152 =>
                    array (
                        'id' => 153,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 53,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                153 =>
                    array (
                        'id' => 154,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 54,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                154 =>
                    array (
                        'id' => 155,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 55,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                155 =>
                    array (
                        'id' => 156,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 56,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                156 =>
                    array (
                        'id' => 157,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 57,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                157 =>
                    array (
                        'id' => 158,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 58,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                158 =>
                    array (
                        'id' => 159,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 59,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                159 =>
                    array (
                        'id' => 160,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 60,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                160 =>
                    array (
                        'id' => 161,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 61,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                161 =>
                    array (
                        'id' => 162,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 62,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                162 =>
                    array (
                        'id' => 163,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 63,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                163 =>
                    array (
                        'id' => 164,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 64,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                164 =>
                    array (
                        'id' => 165,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 65,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                165 =>
                    array (
                        'id' => 166,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 66,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                166 =>
                    array (
                        'id' => 167,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 67,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                167 =>
                    array (
                        'id' => 168,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 68,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                168 =>
                    array (
                        'id' => 169,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 69,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                169 =>
                    array (
                        'id' => 170,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 70,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                170 =>
                    array (
                        'id' => 171,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 71,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                171 =>
                    array (
                        'id' => 172,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 72,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                172 =>
                    array (
                        'id' => 173,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 73,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                173 =>
                    array (
                        'id' => 174,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 74,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                174 =>
                    array (
                        'id' => 175,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 75,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                175 =>
                    array (
                        'id' => 176,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 76,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                176 =>
                    array (
                        'id' => 177,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 77,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                177 =>
                    array (
                        'id' => 178,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 78,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                178 =>
                    array (
                        'id' => 179,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 79,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                179 =>
                    array (
                        'id' => 180,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 80,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                180 =>
                    array (
                        'id' => 181,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 81,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                181 =>
                    array (
                        'id' => 182,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 82,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                182 =>
                    array (
                        'id' => 183,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 83,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                183 =>
                    array (
                        'id' => 184,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 84,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                184 =>
                    array (
                        'id' => 185,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 85,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                185 =>
                    array (
                        'id' => 186,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 86,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                186 =>
                    array (
                        'id' => 187,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 87,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                187 =>
                    array (
                        'id' => 188,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 88,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                188 =>
                    array (
                        'id' => 189,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 89,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                189 =>
                    array (
                        'id' => 190,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 90,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                190 =>
                    array (
                        'id' => 191,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 91,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                191 =>
                    array (
                        'id' => 192,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 92,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                192 =>
                    array (
                        'id' => 193,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 93,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                193 =>
                    array (
                        'id' => 194,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 94,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                194 =>
                    array (
                        'id' => 195,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 95,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                195 =>
                    array (
                        'id' => 196,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 96,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                196 =>
                    array (
                        'id' => 197,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 97,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                197 =>
                    array (
                        'id' => 198,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                198 =>
                    array (
                        'id' => 199,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                199 =>
                    array (
                        'id' => 200,
                        'pd_age_range_id' => 2,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                200 =>
                    array (
                        'id' => 201,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                201 =>
                    array (
                        'id' => 202,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                202 =>
                    array (
                        'id' => 203,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 3,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                203 =>
                    array (
                        'id' => 204,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 4,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                204 =>
                    array (
                        'id' => 205,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 5,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                205 =>
                    array (
                        'id' => 206,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 6,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                206 =>
                    array (
                        'id' => 207,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 7,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                207 =>
                    array (
                        'id' => 208,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 8,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                208 =>
                    array (
                        'id' => 209,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 9,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                209 =>
                    array (
                        'id' => 210,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 10,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                210 =>
                    array (
                        'id' => 211,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 11,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                211 =>
                    array (
                        'id' => 212,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 12,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                212 =>
                    array (
                        'id' => 213,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 13,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                213 =>
                    array (
                        'id' => 214,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 14,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                214 =>
                    array (
                        'id' => 215,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 15,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                215 =>
                    array (
                        'id' => 216,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 16,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                216 =>
                    array (
                        'id' => 217,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 17,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                217 =>
                    array (
                        'id' => 218,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 18,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                218 =>
                    array (
                        'id' => 219,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 19,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                219 =>
                    array (
                        'id' => 220,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 20,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                220 =>
                    array (
                        'id' => 221,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 21,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                221 =>
                    array (
                        'id' => 222,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 22,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                222 =>
                    array (
                        'id' => 223,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 23,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                223 =>
                    array (
                        'id' => 224,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 24,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                224 =>
                    array (
                        'id' => 225,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 25,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                225 =>
                    array (
                        'id' => 226,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 26,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                226 =>
                    array (
                        'id' => 227,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 27,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                227 =>
                    array (
                        'id' => 228,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 28,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                228 =>
                    array (
                        'id' => 229,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 29,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                229 =>
                    array (
                        'id' => 230,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 30,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                230 =>
                    array (
                        'id' => 231,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 31,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                231 =>
                    array (
                        'id' => 232,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 32,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                232 =>
                    array (
                        'id' => 233,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 33,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                233 =>
                    array (
                        'id' => 234,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 34,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                234 =>
                    array (
                        'id' => 235,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 35,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                235 =>
                    array (
                        'id' => 236,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 36,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                236 =>
                    array (
                        'id' => 237,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 37,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                237 =>
                    array (
                        'id' => 238,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 38,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                238 =>
                    array (
                        'id' => 239,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 39,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                239 =>
                    array (
                        'id' => 240,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 40,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                240 =>
                    array (
                        'id' => 241,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 41,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                241 =>
                    array (
                        'id' => 242,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 42,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                242 =>
                    array (
                        'id' => 243,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 43,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                243 =>
                    array (
                        'id' => 244,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 44,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                244 =>
                    array (
                        'id' => 245,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 45,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                245 =>
                    array (
                        'id' => 246,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 46,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                246 =>
                    array (
                        'id' => 247,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 47,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                247 =>
                    array (
                        'id' => 248,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 48,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                248 =>
                    array (
                        'id' => 249,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 49,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                249 =>
                    array (
                        'id' => 250,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 50,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                250 =>
                    array (
                        'id' => 251,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 51,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                251 =>
                    array (
                        'id' => 252,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 52,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                252 =>
                    array (
                        'id' => 253,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 53,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                253 =>
                    array (
                        'id' => 254,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 54,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                254 =>
                    array (
                        'id' => 255,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 55,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                255 =>
                    array (
                        'id' => 256,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 56,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                256 =>
                    array (
                        'id' => 257,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 57,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                257 =>
                    array (
                        'id' => 258,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 58,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                258 =>
                    array (
                        'id' => 259,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 59,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                259 =>
                    array (
                        'id' => 260,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 60,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                260 =>
                    array (
                        'id' => 261,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 61,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                261 =>
                    array (
                        'id' => 262,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 62,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                262 =>
                    array (
                        'id' => 263,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 63,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                263 =>
                    array (
                        'id' => 264,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 64,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                264 =>
                    array (
                        'id' => 265,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 65,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                265 =>
                    array (
                        'id' => 266,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 66,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                266 =>
                    array (
                        'id' => 267,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 67,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                267 =>
                    array (
                        'id' => 268,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 68,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                268 =>
                    array (
                        'id' => 269,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 69,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                269 =>
                    array (
                        'id' => 270,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 70,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                270 =>
                    array (
                        'id' => 271,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 71,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                271 =>
                    array (
                        'id' => 272,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 72,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                272 =>
                    array (
                        'id' => 273,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 73,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                273 =>
                    array (
                        'id' => 274,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 74,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                274 =>
                    array (
                        'id' => 275,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 75,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                275 =>
                    array (
                        'id' => 276,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 76,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                276 =>
                    array (
                        'id' => 277,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 77,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                277 =>
                    array (
                        'id' => 278,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 78,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                278 =>
                    array (
                        'id' => 279,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 79,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                279 =>
                    array (
                        'id' => 280,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 80,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                280 =>
                    array (
                        'id' => 281,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 81,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                281 =>
                    array (
                        'id' => 282,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 82,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                282 =>
                    array (
                        'id' => 283,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 83,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                283 =>
                    array (
                        'id' => 284,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 84,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                284 =>
                    array (
                        'id' => 285,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 85,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                285 =>
                    array (
                        'id' => 286,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 86,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                286 =>
                    array (
                        'id' => 287,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 87,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                287 =>
                    array (
                        'id' => 288,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 88,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                288 =>
                    array (
                        'id' => 289,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 89,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                289 =>
                    array (
                        'id' => 290,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 90,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                290 =>
                    array (
                        'id' => 291,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 91,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                291 =>
                    array (
                        'id' => 292,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 92,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                292 =>
                    array (
                        'id' => 293,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 93,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                293 =>
                    array (
                        'id' => 294,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 94,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                294 =>
                    array (
                        'id' => 295,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 95,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                295 =>
                    array (
                        'id' => 296,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 96,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                296 =>
                    array (
                        'id' => 297,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 97,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                297 =>
                    array (
                        'id' => 298,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                298 =>
                    array (
                        'id' => 299,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                299 =>
                    array (
                        'id' => 300,
                        'pd_age_range_id' => 3,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                300 =>
                    array (
                        'id' => 301,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                301 =>
                    array (
                        'id' => 302,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                302 =>
                    array (
                        'id' => 303,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 3,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                303 =>
                    array (
                        'id' => 304,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 4,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                304 =>
                    array (
                        'id' => 305,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 5,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                305 =>
                    array (
                        'id' => 306,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 6,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                306 =>
                    array (
                        'id' => 307,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 7,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                307 =>
                    array (
                        'id' => 308,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 8,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                308 =>
                    array (
                        'id' => 309,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 9,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                309 =>
                    array (
                        'id' => 310,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 10,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                310 =>
                    array (
                        'id' => 311,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 11,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                311 =>
                    array (
                        'id' => 312,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 12,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                312 =>
                    array (
                        'id' => 313,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 13,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                313 =>
                    array (
                        'id' => 314,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 14,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                314 =>
                    array (
                        'id' => 315,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 15,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                315 =>
                    array (
                        'id' => 316,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 16,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                316 =>
                    array (
                        'id' => 317,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 17,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                317 =>
                    array (
                        'id' => 318,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 18,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                318 =>
                    array (
                        'id' => 319,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 19,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                319 =>
                    array (
                        'id' => 320,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 20,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                320 =>
                    array (
                        'id' => 321,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 21,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                321 =>
                    array (
                        'id' => 322,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 22,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                322 =>
                    array (
                        'id' => 323,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 23,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                323 =>
                    array (
                        'id' => 324,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 24,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                324 =>
                    array (
                        'id' => 325,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 25,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                325 =>
                    array (
                        'id' => 326,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 26,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                326 =>
                    array (
                        'id' => 327,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 27,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                327 =>
                    array (
                        'id' => 328,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 28,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                328 =>
                    array (
                        'id' => 329,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 29,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                329 =>
                    array (
                        'id' => 330,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 30,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                330 =>
                    array (
                        'id' => 331,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 31,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                331 =>
                    array (
                        'id' => 332,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 32,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                332 =>
                    array (
                        'id' => 333,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 33,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                333 =>
                    array (
                        'id' => 334,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 34,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                334 =>
                    array (
                        'id' => 335,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 35,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                335 =>
                    array (
                        'id' => 336,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 36,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                336 =>
                    array (
                        'id' => 337,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 37,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                337 =>
                    array (
                        'id' => 338,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 38,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                338 =>
                    array (
                        'id' => 339,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 39,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                339 =>
                    array (
                        'id' => 340,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 40,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                340 =>
                    array (
                        'id' => 341,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 41,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                341 =>
                    array (
                        'id' => 342,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 42,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                342 =>
                    array (
                        'id' => 343,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 43,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                343 =>
                    array (
                        'id' => 344,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 44,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                344 =>
                    array (
                        'id' => 345,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 45,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                345 =>
                    array (
                        'id' => 346,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 46,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                346 =>
                    array (
                        'id' => 347,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 47,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                347 =>
                    array (
                        'id' => 348,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 48,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                348 =>
                    array (
                        'id' => 349,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 49,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                349 =>
                    array (
                        'id' => 350,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 50,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                350 =>
                    array (
                        'id' => 351,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 51,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                351 =>
                    array (
                        'id' => 352,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 52,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                352 =>
                    array (
                        'id' => 353,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 53,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                353 =>
                    array (
                        'id' => 354,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 54,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                354 =>
                    array (
                        'id' => 355,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 55,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                355 =>
                    array (
                        'id' => 356,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 56,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                356 =>
                    array (
                        'id' => 357,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 57,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                357 =>
                    array (
                        'id' => 358,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 58,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                358 =>
                    array (
                        'id' => 359,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 59,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                359 =>
                    array (
                        'id' => 360,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 60,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                360 =>
                    array (
                        'id' => 361,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 61,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                361 =>
                    array (
                        'id' => 362,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 62,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                362 =>
                    array (
                        'id' => 363,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 63,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                363 =>
                    array (
                        'id' => 364,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 64,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                364 =>
                    array (
                        'id' => 365,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 65,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                365 =>
                    array (
                        'id' => 366,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 66,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                366 =>
                    array (
                        'id' => 367,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 67,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                367 =>
                    array (
                        'id' => 368,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 68,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                368 =>
                    array (
                        'id' => 369,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 69,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                369 =>
                    array (
                        'id' => 370,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 70,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                370 =>
                    array (
                        'id' => 371,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 71,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                371 =>
                    array (
                        'id' => 372,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 72,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                372 =>
                    array (
                        'id' => 373,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 73,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                373 =>
                    array (
                        'id' => 374,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 74,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                374 =>
                    array (
                        'id' => 375,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 75,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                375 =>
                    array (
                        'id' => 376,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 76,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                376 =>
                    array (
                        'id' => 377,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 77,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                377 =>
                    array (
                        'id' => 378,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 78,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                378 =>
                    array (
                        'id' => 379,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 79,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                379 =>
                    array (
                        'id' => 380,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 80,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                380 =>
                    array (
                        'id' => 381,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 81,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                381 =>
                    array (
                        'id' => 382,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 82,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                382 =>
                    array (
                        'id' => 383,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 83,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                383 =>
                    array (
                        'id' => 384,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 84,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                384 =>
                    array (
                        'id' => 385,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 85,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                385 =>
                    array (
                        'id' => 386,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 86,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                386 =>
                    array (
                        'id' => 387,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 87,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                387 =>
                    array (
                        'id' => 388,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 88,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                388 =>
                    array (
                        'id' => 389,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 89,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                389 =>
                    array (
                        'id' => 390,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 90,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                390 =>
                    array (
                        'id' => 391,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 91,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                391 =>
                    array (
                        'id' => 392,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 92,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                392 =>
                    array (
                        'id' => 393,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 93,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                393 =>
                    array (
                        'id' => 394,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 94,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                394 =>
                    array (
                        'id' => 395,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 95,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                395 =>
                    array (
                        'id' => 396,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 96,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                396 =>
                    array (
                        'id' => 397,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 97,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                397 =>
                    array (
                        'id' => 398,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                398 =>
                    array (
                        'id' => 399,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                399 =>
                    array (
                        'id' => 400,
                        'pd_age_range_id' => 4,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                400 =>
                    array (
                        'id' => 401,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                401 =>
                    array (
                        'id' => 402,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                402 =>
                    array (
                        'id' => 403,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 3,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                403 =>
                    array (
                        'id' => 404,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 4,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                404 =>
                    array (
                        'id' => 405,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 5,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                405 =>
                    array (
                        'id' => 406,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 6,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                406 =>
                    array (
                        'id' => 407,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 7,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                407 =>
                    array (
                        'id' => 408,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 8,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                408 =>
                    array (
                        'id' => 409,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 9,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                409 =>
                    array (
                        'id' => 410,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 10,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                410 =>
                    array (
                        'id' => 411,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 11,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                411 =>
                    array (
                        'id' => 412,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 12,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                412 =>
                    array (
                        'id' => 413,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 13,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                413 =>
                    array (
                        'id' => 414,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 14,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                414 =>
                    array (
                        'id' => 415,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 15,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                415 =>
                    array (
                        'id' => 416,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 16,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                416 =>
                    array (
                        'id' => 417,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 17,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                417 =>
                    array (
                        'id' => 418,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 18,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                418 =>
                    array (
                        'id' => 419,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 19,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                419 =>
                    array (
                        'id' => 420,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 20,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                420 =>
                    array (
                        'id' => 421,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 21,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                421 =>
                    array (
                        'id' => 422,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 22,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                422 =>
                    array (
                        'id' => 423,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 23,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                423 =>
                    array (
                        'id' => 424,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 24,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                424 =>
                    array (
                        'id' => 425,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 25,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                425 =>
                    array (
                        'id' => 426,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 26,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                426 =>
                    array (
                        'id' => 427,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 27,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                427 =>
                    array (
                        'id' => 428,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 28,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                428 =>
                    array (
                        'id' => 429,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 29,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                429 =>
                    array (
                        'id' => 430,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 30,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                430 =>
                    array (
                        'id' => 431,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 31,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                431 =>
                    array (
                        'id' => 432,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 32,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                432 =>
                    array (
                        'id' => 433,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 33,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                433 =>
                    array (
                        'id' => 434,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 34,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                434 =>
                    array (
                        'id' => 435,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 35,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                435 =>
                    array (
                        'id' => 436,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 36,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                436 =>
                    array (
                        'id' => 437,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 37,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                437 =>
                    array (
                        'id' => 438,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 38,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                438 =>
                    array (
                        'id' => 439,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 39,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                439 =>
                    array (
                        'id' => 440,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 40,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                440 =>
                    array (
                        'id' => 441,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 41,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                441 =>
                    array (
                        'id' => 442,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 42,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                442 =>
                    array (
                        'id' => 443,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 43,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                443 =>
                    array (
                        'id' => 444,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 44,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                444 =>
                    array (
                        'id' => 445,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 45,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                445 =>
                    array (
                        'id' => 446,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 46,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                446 =>
                    array (
                        'id' => 447,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 47,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                447 =>
                    array (
                        'id' => 448,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 48,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                448 =>
                    array (
                        'id' => 449,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 49,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                449 =>
                    array (
                        'id' => 450,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 50,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                450 =>
                    array (
                        'id' => 451,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 51,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                451 =>
                    array (
                        'id' => 452,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 52,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                452 =>
                    array (
                        'id' => 453,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 53,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                453 =>
                    array (
                        'id' => 454,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 54,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                454 =>
                    array (
                        'id' => 455,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 55,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                455 =>
                    array (
                        'id' => 456,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 56,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                456 =>
                    array (
                        'id' => 457,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 57,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                457 =>
                    array (
                        'id' => 458,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 58,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                458 =>
                    array (
                        'id' => 459,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 59,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                459 =>
                    array (
                        'id' => 460,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 60,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                460 =>
                    array (
                        'id' => 461,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 61,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                461 =>
                    array (
                        'id' => 462,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 62,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                462 =>
                    array (
                        'id' => 463,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 63,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                463 =>
                    array (
                        'id' => 464,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 64,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                464 =>
                    array (
                        'id' => 465,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 65,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                465 =>
                    array (
                        'id' => 466,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 66,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                466 =>
                    array (
                        'id' => 467,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 67,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                467 =>
                    array (
                        'id' => 468,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 68,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                468 =>
                    array (
                        'id' => 469,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 69,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                469 =>
                    array (
                        'id' => 470,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 70,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                470 =>
                    array (
                        'id' => 471,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 71,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                471 =>
                    array (
                        'id' => 472,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 72,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                472 =>
                    array (
                        'id' => 473,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 73,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                473 =>
                    array (
                        'id' => 474,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 74,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                474 =>
                    array (
                        'id' => 475,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 75,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                475 =>
                    array (
                        'id' => 476,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 76,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                476 =>
                    array (
                        'id' => 477,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 77,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                477 =>
                    array (
                        'id' => 478,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 78,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                478 =>
                    array (
                        'id' => 479,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 79,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                479 =>
                    array (
                        'id' => 480,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 80,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                480 =>
                    array (
                        'id' => 481,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 81,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                481 =>
                    array (
                        'id' => 482,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 82,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                482 =>
                    array (
                        'id' => 483,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 83,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                483 =>
                    array (
                        'id' => 484,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 84,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                484 =>
                    array (
                        'id' => 485,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 85,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                485 =>
                    array (
                        'id' => 486,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 86,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                486 =>
                    array (
                        'id' => 487,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 87,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                487 =>
                    array (
                        'id' => 488,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 88,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                488 =>
                    array (
                        'id' => 489,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 89,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                489 =>
                    array (
                        'id' => 490,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 90,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                490 =>
                    array (
                        'id' => 491,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 91,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                491 =>
                    array (
                        'id' => 492,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 92,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                492 =>
                    array (
                        'id' => 493,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 93,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                493 =>
                    array (
                        'id' => 494,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 94,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                494 =>
                    array (
                        'id' => 495,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 95,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                495 =>
                    array (
                        'id' => 496,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 96,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                496 =>
                    array (
                        'id' => 497,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 97,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                497 =>
                    array (
                        'id' => 498,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                498 =>
                    array (
                        'id' => 499,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                499 =>
                    array (
                        'id' => 500,
                        'pd_age_range_id' => 5,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));
            \DB::table('pd_age_adjustments')->insert(array (
                0 =>
                    array (
                        'id' => 501,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 502,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 503,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 3,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 504,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 4,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 505,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 5,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 506,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 6,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 507,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 7,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 508,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 8,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 509,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 9,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 510,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 10,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 511,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 11,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 512,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 12,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 513,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 13,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 514,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 14,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 515,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 15,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 516,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 16,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 517,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 17,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 518,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 18,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 519,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 19,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 520,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 20,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 521,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 21,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 522,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 22,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 523,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 23,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 524,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 24,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 525,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 25,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 526,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 26,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 527,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 27,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 528,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 28,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 529,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 29,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 530,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 30,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 531,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 31,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 532,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 32,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 533,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 33,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 534,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 34,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                34 =>
                    array (
                        'id' => 535,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 35,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                35 =>
                    array (
                        'id' => 536,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 36,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                36 =>
                    array (
                        'id' => 537,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 37,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                37 =>
                    array (
                        'id' => 538,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 38,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                38 =>
                    array (
                        'id' => 539,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 39,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                39 =>
                    array (
                        'id' => 540,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 40,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                40 =>
                    array (
                        'id' => 541,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 41,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                41 =>
                    array (
                        'id' => 542,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 42,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                42 =>
                    array (
                        'id' => 543,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 43,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                43 =>
                    array (
                        'id' => 544,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 44,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                44 =>
                    array (
                        'id' => 545,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 45,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                45 =>
                    array (
                        'id' => 546,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 46,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                46 =>
                    array (
                        'id' => 547,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 47,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                47 =>
                    array (
                        'id' => 548,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 48,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                48 =>
                    array (
                        'id' => 549,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 49,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                49 =>
                    array (
                        'id' => 550,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 50,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                50 =>
                    array (
                        'id' => 551,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 51,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                51 =>
                    array (
                        'id' => 552,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 52,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                52 =>
                    array (
                        'id' => 553,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 53,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                53 =>
                    array (
                        'id' => 554,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 54,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                54 =>
                    array (
                        'id' => 555,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 55,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                55 =>
                    array (
                        'id' => 556,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 56,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                56 =>
                    array (
                        'id' => 557,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 57,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                57 =>
                    array (
                        'id' => 558,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 58,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                58 =>
                    array (
                        'id' => 559,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 59,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                59 =>
                    array (
                        'id' => 560,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 60,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                60 =>
                    array (
                        'id' => 561,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 61,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                61 =>
                    array (
                        'id' => 562,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 62,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                62 =>
                    array (
                        'id' => 563,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 63,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                63 =>
                    array (
                        'id' => 564,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 64,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                64 =>
                    array (
                        'id' => 565,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 65,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                65 =>
                    array (
                        'id' => 566,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 66,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                66 =>
                    array (
                        'id' => 567,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 67,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                67 =>
                    array (
                        'id' => 568,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 68,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                68 =>
                    array (
                        'id' => 569,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 69,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                69 =>
                    array (
                        'id' => 570,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 70,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                70 =>
                    array (
                        'id' => 571,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 71,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                71 =>
                    array (
                        'id' => 572,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 72,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                72 =>
                    array (
                        'id' => 573,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 73,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                73 =>
                    array (
                        'id' => 574,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 74,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                74 =>
                    array (
                        'id' => 575,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 75,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                75 =>
                    array (
                        'id' => 576,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 76,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                76 =>
                    array (
                        'id' => 577,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 77,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                77 =>
                    array (
                        'id' => 578,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 78,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                78 =>
                    array (
                        'id' => 579,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 79,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                79 =>
                    array (
                        'id' => 580,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 80,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                80 =>
                    array (
                        'id' => 581,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 81,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                81 =>
                    array (
                        'id' => 582,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 82,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                82 =>
                    array (
                        'id' => 583,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 83,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                83 =>
                    array (
                        'id' => 584,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 84,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                84 =>
                    array (
                        'id' => 585,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 85,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                85 =>
                    array (
                        'id' => 586,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 86,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                86 =>
                    array (
                        'id' => 587,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 87,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                87 =>
                    array (
                        'id' => 588,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 88,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                88 =>
                    array (
                        'id' => 589,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 89,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                89 =>
                    array (
                        'id' => 590,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 90,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                90 =>
                    array (
                        'id' => 591,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 91,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                91 =>
                    array (
                        'id' => 592,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 92,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                92 =>
                    array (
                        'id' => 593,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 93,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                93 =>
                    array (
                        'id' => 594,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 94,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                94 =>
                    array (
                        'id' => 595,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 95,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                95 =>
                    array (
                        'id' => 596,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 96,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                96 =>
                    array (
                        'id' => 597,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 97,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                97 =>
                    array (
                        'id' => 598,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                98 =>
                    array (
                        'id' => 599,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                99 =>
                    array (
                        'id' => 600,
                        'pd_age_range_id' => 6,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                100 =>
                    array (
                        'id' => 601,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                101 =>
                    array (
                        'id' => 602,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                102 =>
                    array (
                        'id' => 603,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 3,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                103 =>
                    array (
                        'id' => 604,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 4,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                104 =>
                    array (
                        'id' => 605,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 5,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                105 =>
                    array (
                        'id' => 606,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 6,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                106 =>
                    array (
                        'id' => 607,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 7,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                107 =>
                    array (
                        'id' => 608,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 8,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                108 =>
                    array (
                        'id' => 609,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 9,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                109 =>
                    array (
                        'id' => 610,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 10,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                110 =>
                    array (
                        'id' => 611,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 11,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                111 =>
                    array (
                        'id' => 612,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 12,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                112 =>
                    array (
                        'id' => 613,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 13,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                113 =>
                    array (
                        'id' => 614,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 14,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                114 =>
                    array (
                        'id' => 615,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 15,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                115 =>
                    array (
                        'id' => 616,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 16,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                116 =>
                    array (
                        'id' => 617,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 17,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                117 =>
                    array (
                        'id' => 618,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 18,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                118 =>
                    array (
                        'id' => 619,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 19,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                119 =>
                    array (
                        'id' => 620,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 20,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                120 =>
                    array (
                        'id' => 621,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 21,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                121 =>
                    array (
                        'id' => 622,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 22,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                122 =>
                    array (
                        'id' => 623,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 23,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                123 =>
                    array (
                        'id' => 624,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 24,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                124 =>
                    array (
                        'id' => 625,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 25,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                125 =>
                    array (
                        'id' => 626,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 26,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                126 =>
                    array (
                        'id' => 627,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 27,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                127 =>
                    array (
                        'id' => 628,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 28,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                128 =>
                    array (
                        'id' => 629,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 29,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                129 =>
                    array (
                        'id' => 630,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 30,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                130 =>
                    array (
                        'id' => 631,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 31,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                131 =>
                    array (
                        'id' => 632,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 32,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                132 =>
                    array (
                        'id' => 633,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 33,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                133 =>
                    array (
                        'id' => 634,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 34,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                134 =>
                    array (
                        'id' => 635,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 35,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                135 =>
                    array (
                        'id' => 636,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 36,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                136 =>
                    array (
                        'id' => 637,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 37,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                137 =>
                    array (
                        'id' => 638,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 38,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                138 =>
                    array (
                        'id' => 639,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 39,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                139 =>
                    array (
                        'id' => 640,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 40,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                140 =>
                    array (
                        'id' => 641,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 41,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                141 =>
                    array (
                        'id' => 642,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 42,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                142 =>
                    array (
                        'id' => 643,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 43,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                143 =>
                    array (
                        'id' => 644,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 44,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                144 =>
                    array (
                        'id' => 645,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 45,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                145 =>
                    array (
                        'id' => 646,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 46,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                146 =>
                    array (
                        'id' => 647,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 47,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                147 =>
                    array (
                        'id' => 648,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 48,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                148 =>
                    array (
                        'id' => 649,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 49,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                149 =>
                    array (
                        'id' => 650,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 50,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                150 =>
                    array (
                        'id' => 651,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 51,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                151 =>
                    array (
                        'id' => 652,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 52,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                152 =>
                    array (
                        'id' => 653,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 53,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                153 =>
                    array (
                        'id' => 654,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 54,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                154 =>
                    array (
                        'id' => 655,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 55,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                155 =>
                    array (
                        'id' => 656,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 56,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                156 =>
                    array (
                        'id' => 657,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 57,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                157 =>
                    array (
                        'id' => 658,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 58,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                158 =>
                    array (
                        'id' => 659,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 59,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                159 =>
                    array (
                        'id' => 660,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 60,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                160 =>
                    array (
                        'id' => 661,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 61,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                161 =>
                    array (
                        'id' => 662,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 62,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                162 =>
                    array (
                        'id' => 663,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 63,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                163 =>
                    array (
                        'id' => 664,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 64,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                164 =>
                    array (
                        'id' => 665,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 65,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                165 =>
                    array (
                        'id' => 666,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 66,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                166 =>
                    array (
                        'id' => 667,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 67,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                167 =>
                    array (
                        'id' => 668,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 68,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                168 =>
                    array (
                        'id' => 669,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 69,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                169 =>
                    array (
                        'id' => 670,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 70,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                170 =>
                    array (
                        'id' => 671,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 71,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                171 =>
                    array (
                        'id' => 672,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 72,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                172 =>
                    array (
                        'id' => 673,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 73,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                173 =>
                    array (
                        'id' => 674,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 74,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                174 =>
                    array (
                        'id' => 675,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 75,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                175 =>
                    array (
                        'id' => 676,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 76,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                176 =>
                    array (
                        'id' => 677,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 77,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                177 =>
                    array (
                        'id' => 678,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 78,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                178 =>
                    array (
                        'id' => 679,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 79,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                179 =>
                    array (
                        'id' => 680,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 80,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                180 =>
                    array (
                        'id' => 681,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 81,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                181 =>
                    array (
                        'id' => 682,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 82,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                182 =>
                    array (
                        'id' => 683,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 83,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                183 =>
                    array (
                        'id' => 684,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 84,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                184 =>
                    array (
                        'id' => 685,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 85,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                185 =>
                    array (
                        'id' => 686,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 86,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                186 =>
                    array (
                        'id' => 687,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 87,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                187 =>
                    array (
                        'id' => 688,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 88,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                188 =>
                    array (
                        'id' => 689,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 89,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                189 =>
                    array (
                        'id' => 690,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 90,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                190 =>
                    array (
                        'id' => 691,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 91,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                191 =>
                    array (
                        'id' => 692,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 92,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                192 =>
                    array (
                        'id' => 693,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 93,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                193 =>
                    array (
                        'id' => 694,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 94,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                194 =>
                    array (
                        'id' => 695,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 95,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                195 =>
                    array (
                        'id' => 696,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 96,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                196 =>
                    array (
                        'id' => 697,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 97,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                197 =>
                    array (
                        'id' => 698,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                198 =>
                    array (
                        'id' => 699,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                199 =>
                    array (
                        'id' => 700,
                        'pd_age_range_id' => 7,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                200 =>
                    array (
                        'id' => 701,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                201 =>
                    array (
                        'id' => 702,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 2,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                202 =>
                    array (
                        'id' => 703,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 3,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                203 =>
                    array (
                        'id' => 704,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 4,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                204 =>
                    array (
                        'id' => 705,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 5,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                205 =>
                    array (
                        'id' => 706,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 6,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                206 =>
                    array (
                        'id' => 707,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 7,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                207 =>
                    array (
                        'id' => 708,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 8,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                208 =>
                    array (
                        'id' => 709,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 9,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                209 =>
                    array (
                        'id' => 710,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 10,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                210 =>
                    array (
                        'id' => 711,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 11,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                211 =>
                    array (
                        'id' => 712,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 12,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                212 =>
                    array (
                        'id' => 713,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 13,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                213 =>
                    array (
                        'id' => 714,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 14,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                214 =>
                    array (
                        'id' => 715,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 15,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                215 =>
                    array (
                        'id' => 716,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 16,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                216 =>
                    array (
                        'id' => 717,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 17,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                217 =>
                    array (
                        'id' => 718,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 18,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                218 =>
                    array (
                        'id' => 719,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 19,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                219 =>
                    array (
                        'id' => 720,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 20,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                220 =>
                    array (
                        'id' => 721,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 21,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                221 =>
                    array (
                        'id' => 722,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 22,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                222 =>
                    array (
                        'id' => 723,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 23,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                223 =>
                    array (
                        'id' => 724,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 24,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                224 =>
                    array (
                        'id' => 725,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 25,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                225 =>
                    array (
                        'id' => 726,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 26,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                226 =>
                    array (
                        'id' => 727,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 27,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                227 =>
                    array (
                        'id' => 728,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 28,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                228 =>
                    array (
                        'id' => 729,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 29,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                229 =>
                    array (
                        'id' => 730,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 30,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                230 =>
                    array (
                        'id' => 731,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 31,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                231 =>
                    array (
                        'id' => 732,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 32,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                232 =>
                    array (
                        'id' => 733,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 33,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                233 =>
                    array (
                        'id' => 734,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 34,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                234 =>
                    array (
                        'id' => 735,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 35,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                235 =>
                    array (
                        'id' => 736,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 36,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                236 =>
                    array (
                        'id' => 737,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 37,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                237 =>
                    array (
                        'id' => 738,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 38,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                238 =>
                    array (
                        'id' => 739,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 39,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                239 =>
                    array (
                        'id' => 740,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 40,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                240 =>
                    array (
                        'id' => 741,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 41,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                241 =>
                    array (
                        'id' => 742,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 42,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                242 =>
                    array (
                        'id' => 743,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 43,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                243 =>
                    array (
                        'id' => 744,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 44,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                244 =>
                    array (
                        'id' => 745,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 45,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                245 =>
                    array (
                        'id' => 746,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 46,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                246 =>
                    array (
                        'id' => 747,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 47,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                247 =>
                    array (
                        'id' => 748,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 48,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                248 =>
                    array (
                        'id' => 749,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 49,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                249 =>
                    array (
                        'id' => 750,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 50,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                250 =>
                    array (
                        'id' => 751,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 51,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                251 =>
                    array (
                        'id' => 752,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 52,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                252 =>
                    array (
                        'id' => 753,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 53,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                253 =>
                    array (
                        'id' => 754,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 54,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                254 =>
                    array (
                        'id' => 755,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 55,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                255 =>
                    array (
                        'id' => 756,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 56,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                256 =>
                    array (
                        'id' => 757,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 57,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                257 =>
                    array (
                        'id' => 758,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 58,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                258 =>
                    array (
                        'id' => 759,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 59,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                259 =>
                    array (
                        'id' => 760,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 60,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                260 =>
                    array (
                        'id' => 761,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 61,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                261 =>
                    array (
                        'id' => 762,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 62,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                262 =>
                    array (
                        'id' => 763,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 63,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                263 =>
                    array (
                        'id' => 764,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 64,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                264 =>
                    array (
                        'id' => 765,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 65,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                265 =>
                    array (
                        'id' => 766,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 66,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                266 =>
                    array (
                        'id' => 767,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 67,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                267 =>
                    array (
                        'id' => 768,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 68,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                268 =>
                    array (
                        'id' => 769,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 69,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                269 =>
                    array (
                        'id' => 770,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 70,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                270 =>
                    array (
                        'id' => 771,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 71,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                271 =>
                    array (
                        'id' => 772,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 72,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                272 =>
                    array (
                        'id' => 773,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 73,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                273 =>
                    array (
                        'id' => 774,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 74,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                274 =>
                    array (
                        'id' => 775,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 75,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                275 =>
                    array (
                        'id' => 776,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 76,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                276 =>
                    array (
                        'id' => 777,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 77,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                277 =>
                    array (
                        'id' => 778,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 78,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                278 =>
                    array (
                        'id' => 779,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 79,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                279 =>
                    array (
                        'id' => 780,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 80,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                280 =>
                    array (
                        'id' => 781,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 81,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                281 =>
                    array (
                        'id' => 782,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 82,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                282 =>
                    array (
                        'id' => 783,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 83,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                283 =>
                    array (
                        'id' => 784,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 84,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                284 =>
                    array (
                        'id' => 785,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 85,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                285 =>
                    array (
                        'id' => 786,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 86,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                286 =>
                    array (
                        'id' => 787,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 87,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                287 =>
                    array (
                        'id' => 788,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 88,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                288 =>
                    array (
                        'id' => 789,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 89,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                289 =>
                    array (
                        'id' => 790,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 90,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                290 =>
                    array (
                        'id' => 791,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 91,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                291 =>
                    array (
                        'id' => 792,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 92,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                292 =>
                    array (
                        'id' => 793,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 93,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                293 =>
                    array (
                        'id' => 794,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 94,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                294 =>
                    array (
                        'id' => 795,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 95,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                295 =>
                    array (
                        'id' => 796,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 96,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                296 =>
                    array (
                        'id' => 797,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 97,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                297 =>
                    array (
                        'id' => 798,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                298 =>
                    array (
                        'id' => 799,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                299 =>
                    array (
                        'id' => 800,
                        'pd_age_range_id' => 8,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                300 =>
                    array (
                        'id' => 801,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                301 =>
                    array (
                        'id' => 802,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 2,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                302 =>
                    array (
                        'id' => 803,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 3,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                303 =>
                    array (
                        'id' => 804,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 4,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                304 =>
                    array (
                        'id' => 805,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 5,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                305 =>
                    array (
                        'id' => 806,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 6,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                306 =>
                    array (
                        'id' => 807,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 7,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                307 =>
                    array (
                        'id' => 808,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 8,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                308 =>
                    array (
                        'id' => 809,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 9,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                309 =>
                    array (
                        'id' => 810,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 10,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                310 =>
                    array (
                        'id' => 811,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 11,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                311 =>
                    array (
                        'id' => 812,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 12,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                312 =>
                    array (
                        'id' => 813,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 13,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                313 =>
                    array (
                        'id' => 814,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 14,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                314 =>
                    array (
                        'id' => 815,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 15,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                315 =>
                    array (
                        'id' => 816,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 16,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                316 =>
                    array (
                        'id' => 817,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 17,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                317 =>
                    array (
                        'id' => 818,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 18,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                318 =>
                    array (
                        'id' => 819,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 19,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                319 =>
                    array (
                        'id' => 820,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 20,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                320 =>
                    array (
                        'id' => 821,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 21,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                321 =>
                    array (
                        'id' => 822,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 22,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                322 =>
                    array (
                        'id' => 823,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 23,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                323 =>
                    array (
                        'id' => 824,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 24,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                324 =>
                    array (
                        'id' => 825,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 25,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                325 =>
                    array (
                        'id' => 826,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 26,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                326 =>
                    array (
                        'id' => 827,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 27,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                327 =>
                    array (
                        'id' => 828,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 28,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                328 =>
                    array (
                        'id' => 829,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 29,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                329 =>
                    array (
                        'id' => 830,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 30,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                330 =>
                    array (
                        'id' => 831,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 31,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                331 =>
                    array (
                        'id' => 832,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 32,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                332 =>
                    array (
                        'id' => 833,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 33,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                333 =>
                    array (
                        'id' => 834,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 34,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                334 =>
                    array (
                        'id' => 835,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 35,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                335 =>
                    array (
                        'id' => 836,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 36,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                336 =>
                    array (
                        'id' => 837,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 37,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                337 =>
                    array (
                        'id' => 838,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 38,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                338 =>
                    array (
                        'id' => 839,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 39,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                339 =>
                    array (
                        'id' => 840,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 40,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                340 =>
                    array (
                        'id' => 841,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 41,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                341 =>
                    array (
                        'id' => 842,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 42,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                342 =>
                    array (
                        'id' => 843,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 43,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                343 =>
                    array (
                        'id' => 844,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 44,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                344 =>
                    array (
                        'id' => 845,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 45,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                345 =>
                    array (
                        'id' => 846,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 46,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                346 =>
                    array (
                        'id' => 847,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 47,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                347 =>
                    array (
                        'id' => 848,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 48,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                348 =>
                    array (
                        'id' => 849,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 49,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                349 =>
                    array (
                        'id' => 850,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 50,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                350 =>
                    array (
                        'id' => 851,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 51,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                351 =>
                    array (
                        'id' => 852,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 52,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                352 =>
                    array (
                        'id' => 853,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 53,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                353 =>
                    array (
                        'id' => 854,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 54,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                354 =>
                    array (
                        'id' => 855,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 55,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                355 =>
                    array (
                        'id' => 856,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 56,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                356 =>
                    array (
                        'id' => 857,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 57,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                357 =>
                    array (
                        'id' => 858,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 58,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                358 =>
                    array (
                        'id' => 859,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 59,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                359 =>
                    array (
                        'id' => 860,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 60,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                360 =>
                    array (
                        'id' => 861,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 61,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                361 =>
                    array (
                        'id' => 862,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 62,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                362 =>
                    array (
                        'id' => 863,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 63,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                363 =>
                    array (
                        'id' => 864,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 64,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                364 =>
                    array (
                        'id' => 865,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 65,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                365 =>
                    array (
                        'id' => 866,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 66,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                366 =>
                    array (
                        'id' => 867,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 67,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                367 =>
                    array (
                        'id' => 868,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 68,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                368 =>
                    array (
                        'id' => 869,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 69,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                369 =>
                    array (
                        'id' => 870,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 70,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                370 =>
                    array (
                        'id' => 871,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 71,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                371 =>
                    array (
                        'id' => 872,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 72,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                372 =>
                    array (
                        'id' => 873,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 73,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                373 =>
                    array (
                        'id' => 874,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 74,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                374 =>
                    array (
                        'id' => 875,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 75,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                375 =>
                    array (
                        'id' => 876,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 76,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                376 =>
                    array (
                        'id' => 877,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 77,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                377 =>
                    array (
                        'id' => 878,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 78,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                378 =>
                    array (
                        'id' => 879,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 79,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                379 =>
                    array (
                        'id' => 880,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 80,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                380 =>
                    array (
                        'id' => 881,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 81,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                381 =>
                    array (
                        'id' => 882,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 82,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                382 =>
                    array (
                        'id' => 883,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 83,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                383 =>
                    array (
                        'id' => 884,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 84,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                384 =>
                    array (
                        'id' => 885,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 85,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                385 =>
                    array (
                        'id' => 886,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 86,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                386 =>
                    array (
                        'id' => 887,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 87,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                387 =>
                    array (
                        'id' => 888,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 88,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                388 =>
                    array (
                        'id' => 889,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 89,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                389 =>
                    array (
                        'id' => 890,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 90,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                390 =>
                    array (
                        'id' => 891,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 91,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                391 =>
                    array (
                        'id' => 892,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 92,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                392 =>
                    array (
                        'id' => 893,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 93,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                393 =>
                    array (
                        'id' => 894,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 94,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                394 =>
                    array (
                        'id' => 895,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 95,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                395 =>
                    array (
                        'id' => 896,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 96,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                396 =>
                    array (
                        'id' => 897,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 97,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                397 =>
                    array (
                        'id' => 898,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 98,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                398 =>
                    array (
                        'id' => 899,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                399 =>
                    array (
                        'id' => 900,
                        'pd_age_range_id' => 9,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                400 =>
                    array (
                        'id' => 901,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                401 =>
                    array (
                        'id' => 902,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 2,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                402 =>
                    array (
                        'id' => 903,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 3,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                403 =>
                    array (
                        'id' => 904,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 4,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                404 =>
                    array (
                        'id' => 905,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 5,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                405 =>
                    array (
                        'id' => 906,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 6,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                406 =>
                    array (
                        'id' => 907,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 7,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                407 =>
                    array (
                        'id' => 908,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 8,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                408 =>
                    array (
                        'id' => 909,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 9,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                409 =>
                    array (
                        'id' => 910,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 10,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                410 =>
                    array (
                        'id' => 911,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 11,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                411 =>
                    array (
                        'id' => 912,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 12,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                412 =>
                    array (
                        'id' => 913,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 13,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                413 =>
                    array (
                        'id' => 914,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 14,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                414 =>
                    array (
                        'id' => 915,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 15,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                415 =>
                    array (
                        'id' => 916,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 16,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                416 =>
                    array (
                        'id' => 917,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 17,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                417 =>
                    array (
                        'id' => 918,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 18,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                418 =>
                    array (
                        'id' => 919,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 19,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                419 =>
                    array (
                        'id' => 920,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 20,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                420 =>
                    array (
                        'id' => 921,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 21,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                421 =>
                    array (
                        'id' => 922,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 22,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                422 =>
                    array (
                        'id' => 923,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 23,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                423 =>
                    array (
                        'id' => 924,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 24,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                424 =>
                    array (
                        'id' => 925,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 25,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                425 =>
                    array (
                        'id' => 926,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 26,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                426 =>
                    array (
                        'id' => 927,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 27,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                427 =>
                    array (
                        'id' => 928,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 28,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                428 =>
                    array (
                        'id' => 929,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 29,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                429 =>
                    array (
                        'id' => 930,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 30,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                430 =>
                    array (
                        'id' => 931,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 31,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                431 =>
                    array (
                        'id' => 932,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 32,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                432 =>
                    array (
                        'id' => 933,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 33,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                433 =>
                    array (
                        'id' => 934,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 34,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                434 =>
                    array (
                        'id' => 935,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 35,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                435 =>
                    array (
                        'id' => 936,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 36,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                436 =>
                    array (
                        'id' => 937,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 37,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                437 =>
                    array (
                        'id' => 938,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 38,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                438 =>
                    array (
                        'id' => 939,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 39,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                439 =>
                    array (
                        'id' => 940,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 40,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                440 =>
                    array (
                        'id' => 941,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 41,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                441 =>
                    array (
                        'id' => 942,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 42,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                442 =>
                    array (
                        'id' => 943,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 43,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                443 =>
                    array (
                        'id' => 944,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 44,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                444 =>
                    array (
                        'id' => 945,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 45,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                445 =>
                    array (
                        'id' => 946,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 46,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                446 =>
                    array (
                        'id' => 947,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 47,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                447 =>
                    array (
                        'id' => 948,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 48,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                448 =>
                    array (
                        'id' => 949,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 49,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                449 =>
                    array (
                        'id' => 950,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 50,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                450 =>
                    array (
                        'id' => 951,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 51,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                451 =>
                    array (
                        'id' => 952,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 52,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                452 =>
                    array (
                        'id' => 953,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 53,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                453 =>
                    array (
                        'id' => 954,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 54,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                454 =>
                    array (
                        'id' => 955,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 55,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                455 =>
                    array (
                        'id' => 956,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 56,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                456 =>
                    array (
                        'id' => 957,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 57,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                457 =>
                    array (
                        'id' => 958,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 58,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                458 =>
                    array (
                        'id' => 959,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 59,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                459 =>
                    array (
                        'id' => 960,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 60,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                460 =>
                    array (
                        'id' => 961,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 61,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                461 =>
                    array (
                        'id' => 962,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 62,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                462 =>
                    array (
                        'id' => 963,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 63,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                463 =>
                    array (
                        'id' => 964,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 64,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                464 =>
                    array (
                        'id' => 965,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 65,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                465 =>
                    array (
                        'id' => 966,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 66,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                466 =>
                    array (
                        'id' => 967,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 67,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                467 =>
                    array (
                        'id' => 968,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 68,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                468 =>
                    array (
                        'id' => 969,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 69,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                469 =>
                    array (
                        'id' => 970,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 70,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                470 =>
                    array (
                        'id' => 971,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 71,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                471 =>
                    array (
                        'id' => 972,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 72,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                472 =>
                    array (
                        'id' => 973,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 73,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                473 =>
                    array (
                        'id' => 974,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 74,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                474 =>
                    array (
                        'id' => 975,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 75,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                475 =>
                    array (
                        'id' => 976,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 76,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                476 =>
                    array (
                        'id' => 977,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 77,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                477 =>
                    array (
                        'id' => 978,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 78,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                478 =>
                    array (
                        'id' => 979,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 79,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                479 =>
                    array (
                        'id' => 980,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 80,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                480 =>
                    array (
                        'id' => 981,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 81,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                481 =>
                    array (
                        'id' => 982,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 82,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                482 =>
                    array (
                        'id' => 983,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 83,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                483 =>
                    array (
                        'id' => 984,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 84,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                484 =>
                    array (
                        'id' => 985,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 85,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                485 =>
                    array (
                        'id' => 986,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 86,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                486 =>
                    array (
                        'id' => 987,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 87,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                487 =>
                    array (
                        'id' => 988,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 88,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                488 =>
                    array (
                        'id' => 989,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 89,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                489 =>
                    array (
                        'id' => 990,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 90,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                490 =>
                    array (
                        'id' => 991,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 91,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                491 =>
                    array (
                        'id' => 992,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 92,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                492 =>
                    array (
                        'id' => 993,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 93,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                493 =>
                    array (
                        'id' => 994,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 94,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                494 =>
                    array (
                        'id' => 995,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 95,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                495 =>
                    array (
                        'id' => 996,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 96,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                496 =>
                    array (
                        'id' => 997,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 97,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                497 =>
                    array (
                        'id' => 998,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 98,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                498 =>
                    array (
                        'id' => 999,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                499 =>
                    array (
                        'id' => 1000,
                        'pd_age_range_id' => 10,
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));

            $this->enableForeignKeys('pd_age_adjustments');

$sql = <<<SQL
DROP TRIGGER IF EXISTS pd_age_adjustments_ro ON pd_age_adjustments;
create trigger pd_age_adjustments_ro before insert or update or delete or truncate on pd_age_adjustments for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);

        }
        
    }
}