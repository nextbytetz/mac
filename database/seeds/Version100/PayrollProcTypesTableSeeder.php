<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PayrollProcTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('payroll_proc_types');
        $this->delete('payroll_proc_types');

        \DB::table('payroll_proc_types')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Lump Sum',
                    'created_at' => '2017-07-13 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            1 =>
                array (
                    'id' => 2,
                    'name' => 'Monthly Pension',
                    'created_at' => '2017-07-13 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Lump Sum - Monthly Batch',
                    'created_at' => '2017-07-13 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('payroll_proc_types');
    }
}