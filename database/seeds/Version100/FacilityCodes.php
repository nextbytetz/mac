<?php
use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class FacilityCodes extends Seeder
{

	 use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys('facility_codes');
        $this->delete('facility_codes');

        \DB::table('facility_codes')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'stakeholder_id' => 4,
                    'facility_code' => '01099',
                    'name' => 'Muhimbili National Hospital',
                    'created_at' => '2021-04-30 20:24:35',
                    'updated_at' => '2021-04-30 20:24:35',
                ),
            1 =>
                array (
                    'id' => 2,
                    'stakeholder_id' => 4,
                    'facility_code' => '01100',
                    'name' => 'Muhimbili Orthopaedic Institute',
                    'created_at' => '2021-04-30 20:24:35',
                    'updated_at' => '2021-04-30 20:24:35',
                ),
            2 =>
                array (
                    'id' => 3,
                    'stakeholder_id' => 4,
                    'facility_code' => '05447',
                    'name' => 'Muhimbili Ocean Road Clinic',
                    'created_at' => '2021-04-30 20:24:35',
                    'updated_at' => '2021-04-30 20:24:35',
                ),
            3 =>
                array (
                   'id' => 4,
                    'stakeholder_id' => 4,
                    'facility_code' => '06936',
                    'name' => 'JAKAYA KIKWETE CARDIAC INSTITUTE-MUHIMBILI',
                    'created_at' => '2021-04-30 20:24:35',
                    'updated_at' => '2021-04-30 20:24:35',
                ),
            4 =>
                array (
                    'id' => 5,
                    'stakeholder_id' => 4,
                    'facility_code' => '07229',
                    'name' => 'Jakaya Kikwete Cardiac Clinic',
                    'created_at' => '2021-04-30 20:24:35',
                    'updated_at' => '2021-04-30 20:24:35',
                ),
            5 =>
                array (
                    'id' => 6,
                    'stakeholder_id' => 4,
                    'facility_code' => '05004',
                    'name' => 'Ocean Road Cancer Institute',
                    'created_at' => '2021-04-30 20:24:35',
                    'updated_at' => '2021-04-30 20:24:35',
                ),
            6 =>
            array (
	                'id' => 7,
	                'stakeholder_id' => 4,
	                'facility_code' => '01334',
	                'name' => 'Kibong\'oto Special Hospital',
	                'created_at' => '2021-04-30 20:24:35',
	                'updated_at' => '2021-04-30 20:24:35',
            ),
            7 =>
            array (
	                'id' => 8,
	                'stakeholder_id' => 4,
	                'facility_code' => '00947',
	                'name' => 'Mirembe Mental Health Hospital',
	                'created_at' => '2021-04-30 20:24:35',
	                'updated_at' => '2021-04-30 20:24:35',
            ),
        ));
     $this->enableForeignKeys('facility_codes');  
    }
}
