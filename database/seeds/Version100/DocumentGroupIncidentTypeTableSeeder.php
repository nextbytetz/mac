<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DocumentGroupIncidentTypeTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('document_group_incident_type');
        $this->delete('document_group_incident_type');
        
        \DB::table('document_group_incident_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'document_group_id' => '1',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'document_group_id' => '1',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'document_group_id' => '1',
                'incident_type_id' => '3',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'document_group_id' => '2',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'document_group_id' => '2',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'document_group_id' => '2',
                'incident_type_id' => '3',
                'created_at' => '2017-04-19 18:26:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'document_group_id' => '3',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:28:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'document_group_id' => '3',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:28:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'document_group_id' => '3',
                'incident_type_id' => '3',
                'created_at' => '2017-04-19 18:28:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'document_group_id' => '4',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:28:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'document_group_id' => '5',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:29:15',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'document_group_id' => '6',
                'incident_type_id' => '3',
                'created_at' => '2017-04-19 18:31:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'document_group_id' => '7',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:31:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'document_group_id' => '7',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:31:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'document_group_id' => '8',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:33:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'document_group_id' => '8',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:33:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('document_group_incident_type');
    }
}