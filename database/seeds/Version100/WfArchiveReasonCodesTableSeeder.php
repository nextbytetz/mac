<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Workflow\WfArchiveReasonCode;

class WfArchiveReasonCodesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    public function run()
    {
        WfArchiveReasonCode::query()->updateOrCreate(
            ['id' => 1],
            [
                'wf_module_group_id' => 3,
                'code_id' => 53,
            ]
        );
        WfArchiveReasonCode::query()->updateOrCreate(
            ['id' => 2],
            [
                'wf_module_group_id' => 4,
                'code_id' => 53,
            ]
        );
    }

}