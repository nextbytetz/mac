<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class ExitCodesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('exit_codes');
        $this->delete('exit_codes');
        
        \DB::table('exit_codes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Death',
                'created_at' => '2017-04-19 09:15:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Permanent Total Disablement',
                'created_at' => '2017-04-19 09:15:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('exit_codes');
    }
}