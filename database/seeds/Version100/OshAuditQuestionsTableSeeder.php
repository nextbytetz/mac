<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class OshAuditQuestionsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('osh_audit_questions');
        $this->delete('osh_audit_questions');
        
        \DB::table('osh_audit_questions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code_value_id' => 500,
                'question'=> 'Is the workplace registered with OSHA?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            1 => 
            array (
                'id' => 2,
                'code_value_id' => 500,
                'question'=> 'Do you have OSH Policy for the workplace?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            2 => 
            array (
                'id' => 3,
                'code_value_id' => 500,
                'question'=> 'Are there designated Safety and Health Representative?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            3 => 
            array (
                'id' => 4,
                'code_value_id' => 500,
                'question'=> 'Is there a Safety and Health Committee?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            4 => 
            array (
                'id' => 5,
                'code_value_id' => 500,
                'question'=> 'Are there designated first aiders?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            5 => 
            array (
                'id' => 6,
                'code_value_id' => 500,
                'question'=> 'Do you have risk assessments document?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            6 => 
            array (
                'id' => 7,
                'code_value_id' => 500,
                'question'=> 'Are key safety rules displayed in work areas?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            7 => 
            array (
                'id' => 8,
                'code_value_id' => 500,
                'question'=> 'Do you conduct periodic medical surveillance to your employees?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            8 => 
            array (
                'id' => 9,
                'code_value_id' => 502,
                'question'=> 'Are safety and Health representatives trained?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            9 => 
            array (
                'id' => 10,
                'code_value_id' => 502,
                'question'=> 'Are first aiders trained?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            10 => 
            array (
                'id' => 11,
                'code_value_id' => 502,
                'question'=> 'Are workers provided general workplace safety trainings?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            12 => 
            array (
                'id' => 13,
                'code_value_id' => 503,
                'question'=> 'Is working station arrangement suitable for tasks?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            13 => 
            array (
                'id' => 14,
                'code_value_id' => 503,
                'question'=> 'Are job rotations considered in performing duties?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            14 => 
            array (
                'id' => 15,
                'code_value_id' => 503,
                'question'=> 'Is there appropriate mechanical handling/lifting equipment provided?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            16 => 
            array (
                'id' => 17,
                'code_value_id' => 505,
                'question'=> 'Are required PPEs provided?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            17 => 
            array (
                'id' => 18,
                'code_value_id' => 505,
                'question'=> 'Is training provided on the use of PPE?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            18 => 
            array (
                'id' => 19,
                'code_value_id' => 505,
                'question'=> 'Do you have PPE distribution register?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            19 => 
            array (
                'id' => 20,
                'code_value_id' => 506,
                'question'=> 'Is there a formal training program to instruct employees on safe methods of machine operation?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            20 => 
            array (
                'id' => 21,
                'code_value_id' => 506,
                'question'=> 'Do you have machine maintenance procedure?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            21 => 
            array (
                'id' => 22,
                'code_value_id' => 506,
                'question'=> 'Is machine lock out and tag out procedures are followed?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            22 => 
            array (
                'id' => 23,
                'code_value_id' => 506,
                'question'=> 'Are all moving chains and gears properly guarded?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            23 => 
            array (
                'id' => 24,
                'code_value_id' => 507,
                'question'=> 'Is there a formal training on chemical handling?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            24 => 
            array (
                'id' => 25,
                'code_value_id' => 507,
                'question'=> 'Chemicals are in good storage?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            25 => 
            array (
                'id' => 26,
                'code_value_id' => 508,
                'question'=> 'Do you have emergency plan/procedure?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            26 => 
            array (
                'id' => 27,
                'code_value_id' => 508,
                'question'=> 'Do you conduct emergency evacuation drills?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            27 => 
            array (
                'id' => 28,
                'code_value_id' => 508,
                'question'=> 'Do have a designated/trained emergency team?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            28=> 
            array (
                'id' => 29,
                'code_value_id' => 501,
                'question'=> 'Is there an accident reporting form?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            29 => 
            array (
                'id' => 30,
                'code_value_id' => 501,
                'question'=> 'Is there dangerous occurrences reporting form?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            31 => 
            array (
                'id' => 32,
                'code_value_id' => 501,
                'question'=> 'Do you have accident and dangerous occurrences register?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            32 => 
            array (
                'id' => 33,
                'code_value_id' => 501,
                'question'=> 'Is reporting and recording procedures followed?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            33 => 
            array (
                'id' => 34,
                'code_value_id' => 504,
                'question'=> 'Is drinking water to employees accessible all the time?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
            34 => 
            array (
                'id' => 35,
                'code_value_id' => 504,
                'question'=> 'Are the washing facilities provided?',
                'data_type' =>  'traffic',
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
                
            ),
        ));

$this->enableForeignKeys('osh_audit_questions');
}
}
