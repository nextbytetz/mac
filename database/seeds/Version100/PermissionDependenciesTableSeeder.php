<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PermissionDependenciesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        $this->disableForeignKeys('permission_dependencies');
        $this->delete('permission_dependencies');
        
        \DB::table('permission_dependencies')->insert(array (
            0 => 
            array (
                'id' => '1',
                'permission_id' => '6',
                'dependency_id' => '4',
                'created_at' => '2017-04-27 00:00:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => '2',
                'permission_id' => '5',
                'dependency_id' => '4',
                'created_at' => '2017-04-27 00:00:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 =>
                array (
                    'id' => '3',
                    'permission_id' => '37',
                    'dependency_id' => '4',
                    'created_at' => '2017-04-27 00:00:00',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            3 =>
                array (
                    'id' => '4',
                    'permission_id' => '81',
                    'dependency_id' => '21',
                    'created_at' => '2017-11-20 00:00:00',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('permission_dependencies');
        
    }
}