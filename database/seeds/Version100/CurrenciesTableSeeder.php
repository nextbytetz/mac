<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class CurrenciesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('currencies');
        $this->delete('currencies');
        
        \DB::table('currencies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Tanzania Shillings',
                'code' => 'TZS',
                'exchange_rate' => '1.00',
                'created_at' => '2017-04-18 08:07:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'US Dollar',
                'code' => 'USD',
                'exchange_rate' => NULL,
                'created_at' => '2017-04-18 08:07:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('currencies');
    }
}