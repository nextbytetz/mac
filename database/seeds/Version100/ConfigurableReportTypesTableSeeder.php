<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Reporting\ConfigurableReportType;

class ConfigurableReportTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 1],
            [
                'name' => 'Processed Claims',
                'isbalance' => 1
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 2],
            [
                'name' => 'General Notification & Claim',
                'isbalance' => 0
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 3],
            [
                'name' => 'Workflow',
                'isbalance' => 0
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 4],
            [
                'name' => 'Claim Administration Regional Performance',
                'isbalance' => 0
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 5],
            [
                'name' => 'Claim Assessment Individual Performance',
                'isbalance' => 0
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 6],
            [
                'name' => 'General Finance',
                'isbalance' => 0
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 7],
            [
                'name' => 'General Claim Assessment',
                'isbalance' => 0
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 8],
            [
                'name' => 'Claim Administration Individual Performance',
                'isbalance' => 0
            ]
        );
        $crtype = ConfigurableReportType::query()->updateOrCreate(
            ['id' => 9],
            [
                'name' => 'Compliance',
                'isbalance' => 0
            ]
        );
    }

}