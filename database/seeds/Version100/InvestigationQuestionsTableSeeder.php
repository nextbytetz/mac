<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class InvestigationQuestionsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('investigation_questions');
        $this->delete('investigation_questions');

        \DB::table('investigation_questions')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Investigation date',
                    'data_type' => 'date',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Whether the employee has a written contract of employment ',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Whether incident was notified to the Director General on Time',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Whether the incident occurred in the actual discharge of employee\'s duties',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Whether the incident is specifically attributable to the nature of the employees duty unless proved otherwise',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Whether the employee sustained injuries as a result of the occupational accident',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'name' => 'Whether there was a serious or willful misconduct on the part of employee',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'name' => 'Whether the employee has been disabled as the result of occurrence of an incident',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'name' => 'Whether past due employees contributions were remitted by the employer',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '0',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            9 =>
                array (
                    'id' => 10,
                    'name' => 'Whether the employer is registered by OSHA',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            10 =>
                array (
                    'id' => 11,
                    'name' => 'Heath care provider who treated the employee ',
                    'data_type' => 'select',
                    'data_type_source' => 'health_providers',
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            11 =>
                array (
                    'id' => 12,
                    'name' => 'Cost incurred',
                    'data_type' => 'text',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'name' => 'Who paid for medical cost?',
                    'data_type' => 'text',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'name' => 'Was the employee given sick leave (Yes or No?)',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '0',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            14 =>
                array (
                    'id' => 15,
                    'name' => 'How long was the sick leave?',
                    'data_type' => 'text',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '0',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            15 =>
                array (
                    'id' => 16,
                    'name' => 'Reason for Your Decision',
                    'data_type' => 'bigtext',
                    'data_type_source' => NULL,
                    'sort' => '2',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            16 =>
                array (
                    'id' => 17,
                    'name' => 'Whether the employee sustained injuries as a result of the incident',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            17 =>
                array (
                    'id' => 18,
                    'name' => 'State the cause of Death',
                    'data_type' => 'select',
                    'data_type_source' => "death_causes",
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            18 =>
                array (
                    'id' => 19,
                    'name' => 'Date of Accident/ Diagnosis',
                    'data_type' => 'date',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            19 =>
                array (
                    'id' => 20,
                    'name' => 'Date of Death',
                    'data_type' => 'date',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            20 =>
                array (
                    'id' => 21,
                    'name' => 'Whether the employer has OSHA compliance licence',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => '10',
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            21 =>
                array (
                    'id' => 22,
                    'name' => 'Was the accident reported to police?',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            22 =>
                array (
                    'id' => 23,
                    'name' => 'What police station?',
                    'data_type' => 'text',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => '22',
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            23 =>
                array (
                    'id' => 24,
                    'name' => 'Number of EDs provided by medical practitioner',
                    'data_type' => 'text',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            24 =>
                array (
                    'id' => 25,
                    'name' => 'Number of LDs provided by Medical Practitioner',
                    'data_type' => 'text',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            25 =>
                array (
                    'id' => 26,
                    'name' => 'Has the employee reached Maximum Medical Improvement?',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            26 =>
                array (
                    'id' => 27,
                    'name' => 'Date of Maximum Medical Improvement?',
                    'data_type' => 'date',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => '26',
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            27 =>
                array (
                    'id' => 28,
                    'name' => 'Upload image of the injured employee',
                    'data_type' => 'image',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '0',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            28 =>
                array (
                    'id' => 29,
                    'name' => 'Is the incident occupational?',
                    'data_type' => 'boolean',
                    'data_type_source' => NULL,
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            29 =>
                array (
                    'id' => 30,
                    'name' => 'Name of work place',
                    'data_type' => 'select_type',
                    'data_type_source' => 'district_wards',
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            30 =>
                array (
                    'id' => 31,
                    'name' => 'What is the cause of Accident?',
                    'data_type' => 'select_type',
                    'data_type_source' => 'accident_cause',
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            31 =>
                array (
                    'id' => 32,
                    'name' => 'What is the type of Occupational Disease?',
                    'data_type' => 'select_type',
                    'data_type_source' => 'disease_type',
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            32 =>
                array (
                    'id' => 33,
                    'name' => 'What is the Agent of Disease?',
                    'data_type' => 'select_type',
                    'data_type_source' => 'disease_agent',
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
            33 =>
                array (
                    'id' => 34,
                    'name' => 'What is the Target Organ?',
                    'data_type' => 'select_type',
                    'data_type_source' => 'target_organ',
                    'sort' => '1',
                    "isactive" => '1',
                    "parent_id" => NULL,
                    'description' => NULL,
                    'created_at' => '2017-07-26 17:37:06',
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('investigation_questions');
    }
}