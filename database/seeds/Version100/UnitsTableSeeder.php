<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class UnitsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('units');
        $this->delete('units');

        \DB::table('units')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Public Relations',
                    'unit_group_id' => '1',
                    'created_at' => '2017-04-18 08:49:17',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'FA',
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Human Resource Management and Administration',
                    'unit_group_id' => '1',
                    'created_at' => '2017-04-18 08:49:17',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'HA',
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Information, Communication and Technology',
                    'unit_group_id' => '1',
                    'created_at' => '2017-04-18 08:50:43',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'EA',
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Actuarial, Statistics and Risk Management',
                    'unit_group_id' => '1',
                    'created_at' => '2017-04-18 08:50:43',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'JA',
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Procurement Management',
                    'unit_group_id' => '1',
                    'created_at' => '2017-04-18 08:51:26',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'GA',
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Legal Services',
                    'unit_group_id' => '1',
                    'created_at' => '2017-04-18 08:51:26',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'DA',
                ),
            6 =>
                array (
                    'id' => 7,
                    'name' => 'Internal Audit',
                    'unit_group_id' => '1',
                    'created_at' => '2017-04-18 08:51:44',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'KA',
                ),
            7 =>
                array (
                    'id' => 8,
                    'name' => 'Finance, Planning & Investment',
                    'unit_group_id' => '3',
                    'created_at' => '2017-04-18 08:52:32',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'BA',
                ),
            8 =>
                array (
                    'id' => 9,
                    'name' => 'Operations',
                    'unit_group_id' => '3',
                    'created_at' => '2017-04-18 08:52:32',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'A',
                ),
            9 =>
                array (
                    'id' => 10,
                    'name' => 'Medical & Assessment Services',
                    'unit_group_id' => '3',
                    'created_at' => '2017-04-18 08:53:07',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 11,
                    'shortcode'=>'CA',
                ),
            10 =>
                array (
                    'id' => 11,
                    'name' => 'Director General',
                    'unit_group_id' => '3',
                    'created_at' => '2017-04-18 08:53:07',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => NULL,
                    'shortcode'=>'H',
                ),
            11 =>
                array (
                    'id' => 12,
                    'name' => 'Finance',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:54:13',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 8,
                    'shortcode'=>'BA',
                ),
            12 =>
                array (
                    'id' => 13,
                    'name' => 'Planning and Research',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:54:13',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 8,
                    'shortcode'=>'BC',
                ),
            13 =>
                array (
                    'id' => 14,
                    'name' => 'Claim Administration',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:54:35',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 9,
                    'shortcode'=>'AB',
                ),
            14 =>
                array (
                    'id' => 15,
                    'name' => 'Compliance',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:54:35',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 9,
                    'shortcode'=>'AC',
                ),
            15 =>
                array (
                    'id' => 16,
                    'name' => 'Records Management ',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:55:10',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 9,
                    'shortcode'=>'AD',
                ),
            16 =>
                array (
                    'id' => 17,
                    'name' => 'Claim Assessment',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:55:10',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 10,
                    'shortcode'=>'CA',
                ),
            17 =>
                array (
                    'id' => 18,
                    'name' => 'Workplace Risk Assessment',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:55:36',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 10,
                    'shortcode'=>'CB',
                ),
            18 =>
                array (
                    'id' => 19,
                    'name' => 'Health and Safety',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:55:36',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 10,
                    'shortcode'=>'CD',
                ),
            19 =>
                array (
                    'id' => 20,
                    'name' => 'Portal',
                    'unit_group_id' => '4',
                    'created_at' => '2017-04-18 08:55:36',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => NULL,
                    'shortcode'=>NULL,
                ),
            20 =>
                array (
                    'id' => 21,
                    'name' => 'Investment',
                    'unit_group_id' => '2',
                    'created_at' => '2017-04-18 08:54:13',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'parent_id' => 8,
                    'shortcode'=>'BD',
                ),
        ));

        $this->enableForeignKeys('units');
    }
}