<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PermissionGroupsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('permission_groups');
        $this->delete('permission_groups');
        
        \DB::table('permission_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Special',
                'created_at' => '2017-04-18 14:54:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Finance',
                'created_at' => '2017-04-21 10:15:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Records',
                'created_at' => '2017-04-21 10:15:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Compliance',
                'created_at' => '2017-04-21 10:17:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Notification & Claim',
                'created_at' => '2017-04-21 10:17:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Reports',
                'created_at' => '2017-04-21 10:17:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Admin',
                'created_at' => '2017-04-21 10:17:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Legal',
                'created_at' => '2017-04-21 10:18:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'Payroll',
                'created_at' => '2017-07-31 10:18:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

            9 =>
            array (
                'id' => 10,
                'name' => 'Workplace Risk Assessment',
                'created_at' => '2017-07-31 10:18:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'Investment',
                'created_at' => '2021-01-30 10:18:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),

        ));

        $this->enableForeignKeys('permission_groups');
    }
}