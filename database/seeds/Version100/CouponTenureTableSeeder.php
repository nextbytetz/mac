<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class CouponTenureTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        // $this->disableForeignKeys();
        $this->truncate('main.coupon_tenure');
        
        \DB::table('main.coupon_tenure')->insert(array (
            0 => 
            array (
                'years' => 2,
                'days' => 730,
                'coupon_rate' => 7.82,
                'created_at' => '2020-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'years' => 5,
                'days' => 1826,
                'coupon_rate' => 9.18,
                'created_at' => '2020-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'years' => 7,
                'days' => 2556,
                'coupon_rate' => 10.08,
                'created_at' => '2020-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'years' => 10,
                'days' => 3652,
                'coupon_rate' => 11.44,
                'created_at' => '2020-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'years' => 15,
                'days' => 5478,
                'coupon_rate' => 13.50,
                'created_at' => '2020-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'years' => 20,
                'days' => 7305,
                'coupon_rate' => 15.49,
                'created_at' => '2020-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'years' => 25,
                'days' => 9125,
                'coupon_rate' => 15.95,
                'created_at' => '2021-09-13 08:53:00',
                'updated_at' => NULL,
            ),
        ));

        // $this->enableForeignKeys();
    }
}