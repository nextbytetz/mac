<?php


use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;
use Database\TruncateTable;

class ReportsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->disableForeignKeys('reports');
        $this->delete('reports');

        \DB::table('reports')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Cancelled Receipts',
                    'url_name' => 'cancelled_receipts',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => '',
                    'created_at' => '2017-06-01 6:12',
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Receipting Staff Performance',
                    'url_name' => 'receipting_staff_performance',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => '',
                    'created_at' => '2017-06-01 6:12',
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Receipts: Cash Book',
                    'url_name' => 'cr/93',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => '',
                    'created_at' => '2017-06-01 6:12',
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Receipt Finance Codes',
                    'url_name' => 'receipt_finance_codes',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => '',
                    'created_at' => '2017-06-01 6:12',
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'General Receipts Summary',
                    'url_name' => 'general_receipt_summary',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => '',
                    'created_at' => '2017-06-01 6:12',
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Dishonoured Cheques',
                    'url_name' => 'dishonoured_cheques',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),

//            6 =>
//                array (
//                    'id' => 7,
//                    'name' => 'Load Contribution Staff Performance',
//                    'url_name' => 'load_contribution_staff_performance',
//                    'report_type_id' => 1,
//                    'report_category_id' => 3,
//                    'description' => 'Compliance staff performance on loading contribution activity',
//                    'created_at' => '2017-06-03 6:12',
//                    'updated_at' => NULL,
//                ),


            7 =>
                array (
                    'id' => 8,
                    'name' => 'Load Contribution Aging',
                    'url_name' => 'load_contribution_aging',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Aging of receipting until to loading contribution into system',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),

            8 =>
                array (
                    'id' => 9,
                    'name' => 'Booking-Contribution Variance',
                    'url_name' => 'booking_contribution_variance',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Booked amount against Amount paid',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),


            9 =>
                array (
                    'id' => 10,
                    'name' => 'Interest Raised - Amount Paid Variance',
                    'url_name' => 'interest_raised_paid_variance',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Interest Raised against Amount Paid',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),

            10 =>
                array (
                    'id' => 11,
                    'name' => 'New Employees Registered',
                    'url_name' => 'new_employees_registered',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'New employees registered',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),

            11 =>
                array (
                    'id' => 12,
                    'name' => 'Notification Reports Received',
                    //'url_name' => 'notification_report_received',
                    'url_name' => 'cr/56',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => 'Notification Reports Received over period of time',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'name' => 'Monthly Pension Payroll',
                    'url_name' => 'monthly_pension_payroll',
                    'report_type_id' => 1,
                    'report_category_id' => 4,
                    'description' => 'Monthly pension payroll for dependents and pensioners report',
                    'created_at' => '2017-07-17 6:12',
                    'updated_at' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'name' => 'Processed Claims',
                    'url_name' => 'processed_claims',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => 'Claims which have been processed upto final stage (Payments)',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            14 =>
                array (
                    'id' => 15,
                    'name' => 'Payment Vouchers',
                    'url_name' => 'payment_vouchers',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => '',
                    'created_at' => '2017-06-01 6:12',
                    'updated_at' => NULL,
                ),

            15 =>
                array (
                    'id' => 16,
                    'name' => 'Notification Reports Initial Expense',
                    'url_name' => 'notification_report_initial_expense',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => 'Initial expense for each notification report i.e who paid the first initial expense',
                    'created_at' => '2017-08-06 6:12',
                    'updated_at' => NULL,
                ),

            16 =>
                array (
                    'id' => 17,
                    'name' => 'Employer Registrations',
                    'url_name' => 'employer_registration',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employer report filtered by status, category and region and date range.',
                    'created_at' => '2017-10-29 6:12',
                    'updated_at' => NULL,
                ),

            17 =>
                array (
                    'id' => 18,
                    'name' => 'Registration Certificates Issued Report',
                    'url_name' => 'employer_registration_certificate_issue',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employer Registration Certificate issues',
                    'created_at' => '2017-10-29 6:12',
                    'updated_at' => NULL,
                ),

            18 =>
                array (
                    'id' => 19,
                    'name' => 'Unregistered Employers',
                    'url_name' => 'unregistered_employers_report',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Unregistered employers history.',
                    'created_at' => '2017-10-29 6:12',
                    'updated_at' => NULL,
                ),




            19 =>
                array (
                    'id' => 20,
                    'name' => 'Employer General Report',
                    'url_name' => 'employer_overall_report',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employer general report filtered by status, category and region.',
                    'created_at' => '2017-10-29 6:12',
                    'updated_at' => NULL,
                ),



            20 =>
                array (
                    'id' => 21,
                    'name' => 'Registered Employees Report',
                    'url_name' => 'employees_by_employer',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employees report by employer.Filter by employer',
                    'created_at' => '2017-12-04 6:12',
                    'updated_at' => NULL,
                ),

            21 =>
                array (
                    'id' => 22,
                    'name' => 'Employers with Interest Overpayment',
                    'url_name' => 'employer_interest_overpayment',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employers already paid interests billed to them before Sep 2017 Interest Adjustment',
                    'created_at' => '2017-12-31 6:12',
                    'updated_at' => NULL,
                ),
            22 =>
                array (
                    'id' => 23,
                    'name' => 'Contribution General Report',
                    'url_name' => 'receipt_general_report',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => 'All Receipt reports with status (Cancelled, Approved, Pending, Dishonoured)',
                    'created_at' => '2017-12-31 6:12',
                    'updated_at' => NULL,
                ),
            23 =>
                array (
                    'id' => 24,
                    'name' => 'Large Contributors',
                    'url_name' => 'large_contributor',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'All Receipt reports with status (Cancelled, Approved, Pending, Dishonoured)',
                    'created_at' => '2017-12-31 6:12',
                    'updated_at' => NULL,
                ),
            24 =>
                array (
                    'id' => 25,
                    'name' => 'Unsuccessfully Control Numbers',
                    'url_name' => 'Unsuccessfully_cn_month',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Unsuccessfully (expired, cancelled) control numbers on which their contribution months are not paid by the employers in MAC',
                    'created_at' => '2018-12-21 6:12',
                    'updated_at' => NULL,
                ),

            25 =>
                array (
                    'id' => 26,
                    'name' => 'WCFePG Agents and their employers',
                    'url_name' => 'agents_employers',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Agents and number of employers their managing.',
                    'created_at' => '2018-12-21 6:12',
                    'updated_at' => NULL,
                ),

            26 =>
                array (
                    'id' => 27,
                    'name' => 'Successfully Control Numbers',
                    'url_name' => 'successfullControlNo_trx',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Successfully control numbers report by date range',
                    'created_at' => '2018-12-26 14:56',
                    'updated_at' => NULL,
                ),

            27 =>
                array (
                    'id' => 28,
                    'name' => 'Successfully Control Numbers from GePG Daily Reconciliation',
                    'url_name' => 'daily_reconciliation',
                    'report_type_id' => 1,
                    'report_category_id' =>2,
                    'description' => 'Successfully control numbers reconciliation report by date range',
                    'created_at' => '2018-12-29 14:00',
                    'updated_at' => NULL,
                ),
            28 =>
                array (
                    'id' => 29,
                    'name' => 'Registered But not contributing',
                    'url_name' => 'registered_not_contributing',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Registered Employers but have not contributed',
                    'created_at' => '2019-05-08 23:23',
                    'updated_at' => NULL,
                ),

            29 =>
                array (
                    'id' => 30,
                    'name' => 'Online but did not request control number',
                    'url_name' => 'online_not_control_number',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Online employers have not requested for control number',
                    'created_at' => '2019-05-08 23:23',
                    'updated_at' => NULL,
                ),
            30 =>
                array (
                    'id' => 31,
                    'name' => 'Claims processed through online contributions',
                    'url_name' => 'claims_employee_online_contribution',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Claims processed through online contributions',
                    'created_at' => '2019-05-08 23:32',
                    'updated_at' => NULL,
                ),

            31 =>
                array (
                    'id' => 32,
                    'name' => 'Advance contributions register',
                    'url_name' => 'advance_contribution_register',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'List of contributions paid on advance',
                    'created_at' => '2019-05-08 23:37',
                    'updated_at' => NULL,
                ),

            32 =>
                array (
                    'id' => 33,
                    'name' => 'Contribution Receivable for Contributing Employers',
                    'url_name' => 'contribution_receivable',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Contribution Receivable for employers contributed on specified dates',
                    'created_at' => '2019-05-08 23:40',
                    'updated_at' => NULL,
                ),

            33 =>
                array (
                    'id' => 34,
                    'name' => 'Contribution Receivable Age Analysis Report',
                    'url_name' => 'contribution_age_analysis',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Age Analysis Report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),


            34 =>
                array (
                    'id' => 35,
                    'name' => 'Contributing Employers Report',
                    'url_name' => 'contributing_employers',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Report for contributing employers',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            35 =>
                array (
                    'id' => 36,
                    'name' => 'Closed Business Report',
                    'url_name' => 'closed_business',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employers business closure',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            36 =>
                array (
                    'id' => 37,
                    'name' => 'Dormant Employers Report',
                    'url_name' => 'dormant_employers',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Dormant employers report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),
//
            37 =>
                array (
                    'id' => 38,
                    'name' => 'Employer Verifications Report',
                    'url_name' => 'employer_verifications',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employer verifications report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            38 =>
                array (
                    'id' => 39,
                    'name' => 'Contribution reconciliations report',
                    'url_name' => 'contribution_reconciliations',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Contribution reconciliations report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            39 =>
                array (
                    'id' => 40,
                    'name' => 'Contribution Review Report',
                    'url_name' => 'approved_pending_contributions',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Contribution Review report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

//            40 =>
//                array (
//                    'id' => 41,
//                    'name' => 'Staff contribution Collection performance report',
//                    'url_name' => 'staff_contrib_collection_performance',
//                    'report_type_id' => 1,
//                    'report_category_id' => 3,
//                    'description' => 'Staff  collection performance report',
//                    'created_at' => '2019-05-08 23:41',
//                    'updated_at' => NULL,
//                ),

            41 =>
                array (
                    'id' => 42,
                    'name' => 'Booked Interest Report',
                    'url_name' => 'booked_interests',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Booked interest report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),
            42 =>
                array (
                    'id' => 43,
                    'name' => 'Unpaid Interest Report',
                    'url_name' => 'unpaid_interests',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Unpaid interest report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),
            43 =>
                array (
                    'id' => 44,
                    'name' => 'Non Contributing Employers Report',
                    'url_name' => 'contrib_receivable_non_contributors',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Non contributors employers report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),
//            44 =>
//                array (
//                    'id' => 45,
//                    'name' => 'Booked interest - non contributors',
//                    'url_name' => 'booked_interest_non_contributors',
//                    'report_type_id' => 1,
//                    'report_category_id' => 3,
//                    'description' => 'Booked interest for non contributors employers report',
//                    'created_at' => '2019-05-08 23:41',
//                    'updated_at' => NULL,
//                ),
            45 =>
                array (
                    'id' => 46,
                    'name' => 'Contribution Receivable for New Employers',
                    'url_name' => 'previous_receivables_new_employers',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Contribt',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            46 =>
                array (
                    'id' => 47,
                    'name' => 'Contribution Receivable for Non Contributing Employers',
                    'url_name' => 'rcvable_non_contrib_whole_year',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Contribution receivable for non contributor on this fiscal year',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            47 =>
                array (
                    'id' => 48,
                    'name' => 'Employer Monthly Contributions',
                    'url_name' => 'employer_contributions',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employer monthly contributions',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),


            48 =>
                array (
                    'id' => 49,
                    'name' => 'Contribution Receivable for Closed Businesses',
                    'url_name' => 'contrib_rcvable_closed_business',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Contribution receivable for closed businesses',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            49 =>
                array (
                    'id' => 50,
                    'name' => 'Contribution Receivable General Report',
                    'url_name' => 'contrib_rcvable_general',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Contribution receivable for general report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            50 =>
                array (
                    'id' => 51,
                    'name' => 'Contribution Receivable Open Balance Report',
                    'url_name' => 'contrib_rcvable_open_balance',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Contribution receivable opening balance for report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),



//            51 =>
//                array (
//                    'id' => 52,
//                    'name' => 'Erroneous Refund Payment Report',
//                    'url_name' => 'erroneous_refund_payment',
//                    'report_type_id' => 1,
//                    'report_category_id' => 3,
//                    'description' => 'Erroneous refund payment report',
//                    'created_at' => '2019-05-08 23:41',
//                    'updated_at' => NULL,
//                ),


            52 =>
                array (
                    'id' => 53,
                    'name' => 'Receipt Contributions Report',
                    'url_name' => 'receipt_contributions',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Receipt Contributions Report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),


            53 =>
                array (
                    'id' => 54,
                    'name' => 'Change of Monthly Earning Report',
                    'url_name' => 'change_of_monthly_earning',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Change of Monthly Earning Report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            54 =>
                array (
                    'id' => 55,
                    'name' => 'Summary of Contribution Receivable Age Analysis Report',
                    'url_name' => 'summary_contrib_age_analysis',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Summary of Contribution Receivable Age Analysis Report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            55 =>
                array (
                    'id' => 56,
                    'name' => 'Summary of Contribution Income - Receivable Report',
                    'url_name' => 'summary_contrib_income_rcvable',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Summary of Contribution Income - Receivable Report',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),


            56 =>
                array (
                    'id' => 57,
                    'name' => 'Receipts for contributions from Summary income',
                    'url_name' => 'receipts_contrib_summary_income',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Receipts for contributions from Summary income',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            57 =>
                array (
                    'id' => 58,
                    'name' => 'Receipts for Previous contributions Not Booked',
                    'url_name' => 'receipts_prev_contrib_summary_income',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Receipts for previous contributions from Summary income',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            58 =>
                array (
                    'id' => 59,
                    'name' => 'Receipts for Previous Contributions Booked',
                    'url_name' => 'receipts_prev_contrib_booked',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Receipts for previous contributions booked',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),


            59 =>
                array (
                    'id' => 60,
                    'name' => 'Staff Monthly Collection Performance (Debt Mgt)',
                    'url_name' => 'staff_monthly_collection_performance',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Staff monthly collection performances for debt management',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),


            60 =>
                array (
                    'id' => 61,
                    'name' => 'Staff Employer Follow ups (Debt Mgt)',
                    'url_name' => 'staff_employer_follow_ups',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Staff employer follow ups for debt management',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            61 =>
                array (
                    'id' => 62,
                    'name' => 'Payroll Beneficiaries',
                    'url_name' => 'payroll_beneficiaries',
                    'report_type_id' => 1,
                    'report_category_id' => 4,
                    'description' => 'Payroll beneficiaries report',
                    'created_at' => '2019-11-08 23:41',
                    'updated_at' => NULL,
                ),

            62 =>
                array (
                    'id' => 63,
                    'name' => 'Payroll Verifications',
                    'url_name' => 'payroll_verifications',
                    'report_type_id' => 1,
                    'report_category_id' => 4,
                    'description' => 'Payroll verifications report',
                    'created_at' => '2019-11-08 23:41',
                    'updated_at' => NULL,
                ),

            63 =>
                array (
                    'id' => 64,
                    'name' => 'Summary Contribution Collection Quarterly',
                    'url_name' => 'summary_contrib_quarterly',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Summary Contribution Collection Quarterly report',
                    'created_at' => '2020-01-06 23:41',
                    'updated_at' => NULL,
                ),


            64 =>
                array (
                    'id' => 65,
                    'name' => 'Employer Change of Particulars',
                    'url_name' => 'employer_change_request',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employers change of particulars report',
                    'created_at' => '2020-01-06 23:41',
                    'updated_at' => NULL,
                ),
            65 =>
                array (
                    'id' => 66,
                    'name' => 'Online Employers Return of Earnings Submission',
                    'url_name' => 'employer_return_earnings',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Online Employers Return of Earnings report',
                    'created_at' => '2020-03-26 11:41',
                    'updated_at' => NULL,
                ),
            66 =>
                array (
                    'id' => 67,
                    'name' => 'Paid Claims [First Paid Benefit(s)]',
                    'url_name' => 'cr/2',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => 'Claims paid at least one benefit',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            67=>
                array (
                    'id' => 68,
                    'name' => 'OSH Audit Employer summary report',
                    'url_name' => 'osh_audit_employer_summary',
                    'report_type_id' => 1,
                    'report_category_id' =>6,
                    'description' => 'OSH Audit Employer summary report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            68=>
                array (
                    'id' => 69,
                    'name' => 'OSH Employer summary report',
                    'url_name' => 'osh_employer_summary',
                    'report_type_id' => 1,
                    'report_category_id' =>6,
                    'description' => 'OSH Employer summary report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            69=>
                array (
                    'id' => 70,
                    'name' => 'OSH Workplace summary report',
                    'url_name' => 'osh_workplace_employer_summary',
                    'report_type_id' => 1,
                    'report_category_id' =>6,
                    'description' => 'OSH Workplace summary report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            70=>
                array (
                    'id' => 71,
                    'name' => 'OSH Sector summary report',
                    'url_name' => 'osh_sector_employer_summary',
                    'report_type_id' => 1,
                    'report_category_id' =>6,
                    'description' => 'OSH Workplace summary report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            71=>
                array (
                    'id' => 72,
                    'name' => 'OSH Employer Category summary report',
                    'url_name' => 'osh_category_employer_summary',
                    'report_type_id' => 1,
                    'report_category_id' =>6,
                    'description' => 'OSH Employer Category summary report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            72=>
                array (
                    'id' => 73,
                    'name' => 'Corporate Bond Schedule',
                    'url_name' => 'corporate_bond_schedule',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Corporate Bond Schedule report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            73=>
                array (
                    'id' => 74,
                    'name' => 'Divident Schedule',
                    'url_name' => 'divident_schedule',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Divident Schedule report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            74=>
                array (
                    'id' => 75,
                    'name' => 'Fixed Deposits Schedule',
                    'url_name' => 'fixed_deposits_schedule',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Fixed Deposits Schedule report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            75=>
                array (
                    'id' => 76,
                    'name' => 'Interest On Call Account',
                    'url_name' => 'interest_call_account',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Interest On Call Account report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            76=>
                array (
                    'id' => 77,
                    'name' => 'Listed Equities',
                    'url_name' => 'listed_equities',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Listed Equities report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            77=>
                array (
                    'id' => 78,
                    'name' => 'Treasury Bond Schedule',
                    'url_name' => 'treasury_bond_schedule',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Treasury Bond Schedule report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            78=>
                array (
                    'id' => 79,
                    'name' => 'UTT Bond Fund Investment',
                    'url_name' => 'utt_bond_fund',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'UTT Bond Fund Investment report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),

            79=>
                array (
                    'id' => 80,
                    'name' => 'Tresury Bill Schedule',
                    'url_name' => 'treasury_bill_schedule',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Treasury Bill Schedule',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            80=>
                array (
                    'id' => 81,
                    'name' => 'Change In Market Values Of Investment',
                    'url_name' => 'market_value_changes',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Change In Market Values Of Investment report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            81=>
                array (
                    'id' => 82,
                    'name' => 'Investment Portfolio',
                    'url_name' => 'investment_portfolio',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Investment Portfolio report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            82=>
                array (
                    'id' => 83,
                    'name' => 'Compliance on listed stock',
                    'url_name' => 'compliance_listed_stock',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Compliance on listed stock report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            83=>
                array (
                    'id' => 84,
                    'name' => 'Compliance on Deposits Holdings with the Banks',
                    'url_name' => 'compliance_deposit_holding',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Compliance on Deposits Holdings with the Banks report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            84=>
                array (
                    'id' => 85,
                    'name' => 'Budget Versus Actual Amount Invested ',
                    'url_name' => 'budget_actual_amount',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Budget Versus Actual Amount Invested report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            85=>
                array (
                    'id' => 86,
                    'name' => 'Maturing actual investments vs. Expected maturing investment',
                    'url_name' => 'maturing_actual_expected_maturing',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Maturing actual investments vs. Expected maturing investment report',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            86=>
                array (
                    'id' => 87,
                    'name' => 'Income from Investments for the quarter ',
                    'url_name' => 'income_from_investment',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'Income from Investments for the quarter report ',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            87=>
                array (
                    'id' => 88,
                    'name' => 'The Weighted Average Return of the Fund Investment',
                    'url_name' => 'average_return_fund',
                    'report_type_id' => 1,
                    'report_category_id' =>7,
                    'description' => 'The Weighted Average Return of the Fund Investment report ',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),

            88=>
                array (
                    'id' => 89,
                    'name' => 'Staff Employer Monthly Collection (Debt Mgt)',
                    'url_name' => 'staff_employer_monthly_collection',
                    'report_type_id' => 1,
                    'report_category_id' =>3,
                    'description' => 'Staff Employer Monthly Collection',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),

            89=>
                array (
                    'id' => 90,
                    'name' => 'Employer Contributions Receipted',
                    'url_name' => 'employer_contrib_ext_audit',
                    'report_type_id' => 1,
                    'report_category_id' =>8,
                    'description' => 'Employer Contribution receipted',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),

            90=>
                array (
                    'id' => 91,
                    'name' => 'Pension Statement',
                    'url_name' => 'pension_statement',
                    'report_type_id' => 1,
                    'report_category_id' =>4,
                    'description' => 'Pension Statement',
                    'created_at' => '2020-04-05 15:10',
                    'updated_at' => NULL,
                ),
            91 =>
                array (
                    'id' => 92,
                    'name' => 'Processed Claim (Paid & Rejected)',
                    'url_name' => 'cr/61',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            92 =>
                array (
                    'id' => 93,
                    'name' => 'Un-Processed Progressive Claim',
                    'url_name' => 'cr/62',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            93 =>
                array (
                    'id' => 94,
                    'name' => 'Un-Processed Rejected Claim',
                    'url_name' => 'cr/63',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            94 =>
                array (
                    'id' => 95,
                    'name' => 'Investigated (UnProcessed Progressive Claims)',
                    'url_name' => 'cr/64',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            95 =>
                array (
                    'id' => 96,
                    'name' => 'UnInvestigated (Un-Processed Progressive Claims)',
                    'url_name' => 'cr/65',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            96 =>
                array (
                    'id' => 97,
                    'name' => 'Validated (Un-Processed Progressive Claims, Investigated)',
                    'url_name' => 'cr/66',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            97 =>
                array (
                    'id' => 98,
                    'name' => 'UnValidated (Un-Processed Progressive, Investigated)',
                    'url_name' => 'cr/67',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            98 =>
                array (
                    'id' => 99,
                    'name' => 'Acknowledged Notification Reports',
                    'url_name' => 'cr/68',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            99 =>
                array (
                    'id' => 100,
                    'name' => 'Unacknowledged Notification Reports',
                    'url_name' => 'cr/69',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            100 =>
                array (
                    'id' => 101,
                    'name' => 'ONS (Employers Assigned)',
                    'url_name' => 'cr/70',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            101 =>
                array (
                    'id' => 102,
                    'name' => 'ONS (Employers Registered)',
                    'url_name' => 'cr/71',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            102 =>
                array (
                    'id' => 103,
                    'name' => 'ONS (Employers Not Registered)',
                    'url_name' => 'cr/79',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            103 =>
                array (
                    'id' => 104,
                    'name' => 'Paid Claims [All Benefits]',
                    'url_name' => 'cr/80',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),

            104 =>
                array (
                    'id' => 105,
                    'name' => 'Payroll Recoveries',
                    'url_name' => 'payroll_recoveries',
                    'report_type_id' => 1,
                    'report_category_id' => 4,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),



            105 =>
            array (
                'id' => 106,
                'name' => 'All accrued claims with their Respective amount for benefit(After assessment by DAS)',
                'url_name' => 'accrued_assessment',
                'report_type_id' => 1,
                'report_category_id' => 1,
                'description' => 'All accrued claims with their respective amount for each benefit (After assessment by DAS)',
                'created_at' => '2017-06-01 6:12',
                'updated_at' => NULL,
            ),
        106 =>
            array (
                'id' => 107,
                'name' => 'General Claim Accrual Reports',
                'url_name' => 'claim_accrual_report',
                'report_type_id' => 1,
                'report_category_id' => 1,
                'description' => 'All paid claims once accrued with their respective amounts each financial year',
                'created_at' => '2017-06-01 6:12',
                'updated_at' => NULL,
            ),
            107 =>
            array (
                'id' => 108,
                'name' => 'All accrued claims with their respective amount for benefit(After booking by DFPI)',
                'url_name' => 'accrued_booking',
                'report_type_id' => 1,
                'report_category_id' => 1,
                'description' => 'All accrued claims with their respective amount for each benefit (After booking by DFPI)',
                'created_at' => '2017-06-03 6:12',
                'updated_at' => NULL,
            ),
             108 =>
            array (
                'id' => 109,
                'name' => 'General Investigation Plan Reports',
                'url_name' => 'investigation_reports',
                'report_type_id' => 1,
                'report_category_id' => 1,
                'description' => 'General reports for investigation plan',
                'created_at' => '2017-06-01 6:12',
                'updated_at' => NULL,
            ),
            109 =>
            array (
                'id' => 110,
                'name' => 'General Notification Follow Ups',
                'url_name' => 'notification_follow_ups_report',
                'report_type_id' => 1,
                'report_category_id' => 1,
                'description' => 'General reports for Notification Follow Ups',
                'created_at' => '2017-06-01 6:12',
                'updated_at' => NULL,
            ),
            110 =>
                array (
                    'id' => 111,
                    'name' => 'Notification Requiring Rehabilitation',
                    'url_name' => 'cr/85',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => '',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),


            111 =>
                array (
                    'id' => 112,
                    'name' => 'Business De-registration Extension Report',
                    'url_name' => 'closed_business_extension',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Employers business de-registration extension',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),

            112 =>
                array (
                    'id' => 113,
                    'name' => 'Temporary De-registration Need Action after Status Followup',
                    'url_name' => 'deregistration_need_action_after_followup',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Temporary De-registration Need Action after Status Followup',
                    'created_at' => '2019-05-08 23:41',
                    'updated_at' => NULL,
                ),


            113 =>
                array (
                    'id' => 114,
                    'name' => 'Payroll Child Extensions',
                    'url_name' => 'payroll_child_extensions',
                    'report_type_id' => 1,
                    'report_category_id' => 4,
                    'description' => 'Payroll child granted extensions',
                    'created_at' => '2021-03-20 23:41',
                    'updated_at' => NULL,
                ),

            114 =>
                array (
                    'id' => 115,
                    'name' => 'Payroll Bank Updates',
                    'url_name' => 'payroll_bank_updates',
                    'report_type_id' => 1,
                    'report_category_id' => 4,
                    'description' => 'Payroll bank updates',
                    'created_at' => '2021-03-20 23:41',
                    'updated_at' => NULL,
                ),

            115 =>
                array (
                    'id' => 116,
                    'name' => 'Payroll Payments Period Summary',
                    'url_name' => 'payroll_payments_summary',
                    'report_type_id' => 1,
                    'report_category_id' => 4,
                    'description' => 'Payroll payments summary on perido i.e. monthly, quarterly and annually',
                    'created_at' => '2021-03-20 23:41',
                    'updated_at' => NULL,
                ),


            116 =>
                array (
                    'id' => 117,
                    'name' => 'Legacy Contribution',
                    'url_name' => 'cr/101',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Legacy contribution',
                    'created_at' => '2021-04-28 23:41',
                    'updated_at' => NULL,
                ),

            117 =>
                array (
                    'id' => 118,
                    'name' => 'Pending Payment: Claim Files ',
                    'url_name' => 'cr/20',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => 'Pending Payment: Claim Files',
                    'created_at' => '2021-04-28 23:41',
                    'updated_at' => NULL,
                ),

            118 =>
                array (
                    'id' => 119,
                    'name' => 'Receipts ',
                    'url_name' => 'cr/102',
                    'report_type_id' => 1,
                    'report_category_id' => 2,
                    'description' => 'Receipts',
                    'created_at' => '2021-04-28 23:41',
                    'updated_at' => NULL,
                ),
            119 =>
                array (
                    'id' => 120,
                    'name' => 'Total Notification Received With Processed Stage',
                    //'url_name' => 'notification_report_received',
                    'url_name' => 'cr/94',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => 'Total Notification Received With Processed Stage',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
            120 =>
                array (
                    'id' => 121,
                    'name' => 'Total Notification Received With TAT',
                    //'url_name' => 'notification_report_received',
                    'url_name' => 'cr/96',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => 'Total Notification Received With TAT',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),

                 121 =>
                array (
                    'id' => 122,
                    'name' => 'Total Notification Received With HCP Details',
                    //'url_name' => 'notification_report_received',
                    'url_name' => 'cr/97',
                    'report_type_id' => 1,
                    'report_category_id' => 1,
                    'description' => 'Total Notification Received With HCP Details',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),
                   122 =>
                array (
                    'id' => 123,
                    'name' => 'Inspection Profile',
                    //'url_name' => 'notification_report_received',
                    'url_name' => 'employer_inspection_profile',
                    'report_type_id' => 1,
                    'report_category_id' => 3,
                    'description' => 'Inspection Profile report',
                    'created_at' => '2017-06-03 6:12',
                    'updated_at' => NULL,
                ),

        ));

        $this->enableForeignKeys('reports');
    }
}
