<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class InspectionTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('inspection_types');
        $this->delete('inspection_types');
        
        \DB::table('inspection_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Routine Inspection',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Surprise Inspection',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Follow-up Inspection',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Survey Inspection',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('inspection_types');
    }
}