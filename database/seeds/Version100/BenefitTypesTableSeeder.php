<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class BenefitTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('benefit_types');
        $this->delete('benefit_types');
        
        \DB::table('benefit_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'benefit_type_group_id' => '1',
                'name' => 'Lump Sum',
                'shortcode' => '',
                'fin_account_code_id' => NULL,
                'benefit_type_claim_id' => NULL,
                'created_at' => '2017-04-19 11:39:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'benefit_type_group_id' => '1',
                'name' => 'Funeral Grants',
                'shortcode' => '',
                'fin_account_code_id' => 4,
                'benefit_type_claim_id' => 6,
                'created_at' => '2017-04-19 11:39:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 30,
            ),
            2 => 
            array (
                'id' => 3,
                'benefit_type_group_id' => '1',
                'name' => 'Permanent Partial Disablement',
                'shortcode' => 'PPD',
                'fin_account_code_id' => 2,
                'benefit_type_claim_id' => 5,
                'created_at' => '2017-04-19 11:40:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 33,
            ),
            3 => 
            array (
                'id' => 4,
                'benefit_type_group_id' => '2',
                'name' => 'Permanent Total Disablement',
                'shortcode' => 'PTD',
                'fin_account_code_id' => 2,
                'benefit_type_claim_id' => 5,
                'created_at' => '2017-04-19 11:40:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 34,
            ),
            4 => 
            array (
                'id' => 5,
                'benefit_type_group_id' => '2',
                'name' => 'Constant Attendance Care Grant',
                'shortcode' => 'CACG',
                'fin_account_code_id' => NULL,
                'benefit_type_claim_id' => 8,
                'created_at' => '2017-04-19 11:41:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 37,
            ),
            5 => 
            array (
                'id' => 6,
                'benefit_type_group_id' => '2',
                'name' => 'Dependent Monthly Pension',
                'shortcode' => 'DMP',
                'fin_account_code_id' => NULL,
                'benefit_type_claim_id' => 7,
                'created_at' => '2017-04-19 11:41:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 27,
            ),
            6 => 
            array (
                'id' => 7,
                'benefit_type_group_id' => '3',
                'name' => 'Temporary Partial Disablement',
                'shortcode' => 'TPD',
                'fin_account_code_id' => 1,
                'benefit_type_claim_id' => 3,
                'created_at' => '2017-04-19 11:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 32,
            ),
            7 => 
            array (
                'id' => 8,
                'benefit_type_group_id' => '3',
                'name' => 'Temporary Total Disablement',
                'shortcode' => 'TTD',
                'fin_account_code_id' => 1,
                'benefit_type_claim_id' => 3,
                'created_at' => '2017-04-19 11:42:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 31,
            ),
            8 => 
            array (
                'id' => 9,
                'benefit_type_group_id' => '3',
                'name' => 'Rehabilitation',
                'shortcode' => '',
                'fin_account_code_id' => NULL,
                'benefit_type_claim_id' => 9,
                'created_at' => '2017-04-19 11:42:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'fin_code_id' => 36,
            ),
            9 =>
                array (
                    'id' => 10,
                    'benefit_type_group_id' => '3',
                    'name' => 'Medical Aid Expenses (Employee/Employer)',
                    'shortcode' => 'MAE',
                    'fin_account_code_id' => 3,
                    'benefit_type_claim_id' => NULL,
                    'created_at' => '2017-04-19 11:42:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => 35,
                ),
            10 =>
                array (
                    'id' => 11,
                    'benefit_type_group_id' => '3',
                    'name' => 'Survival Benefits',
                    'shortcode' => '',
                    'fin_account_code_id' => 5,
                    'benefit_type_claim_id' => 7,
                    'created_at' => '2017-07-31 11:42:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => 28,
                ),
            11 =>
                array (
                    'id' => 12,
                    'benefit_type_group_id' => '4',
                    'name' => 'Interest Refund',
                    'shortcode' => '',
                    'fin_account_code_id' => Null,
                    'benefit_type_claim_id' => NULL,
                    'created_at' => '2018-01-22 11:42:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'benefit_type_group_id' => '1',
                    'name' => 'Dependant Grants',
                    'shortcode' => '',
                    'fin_account_code_id' => 5,
                    'benefit_type_claim_id' => 6,
                    'created_at' => '2018-01-22 11:42:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'benefit_type_group_id' => '3',
                    'name' => 'Temporary Partial Disablement Refund',
                    'shortcode' => 'TPD Refund',
                    'fin_account_code_id' => 1,
                    'benefit_type_claim_id' => 4,
                    'created_at' => '2017-04-19 11:42:05',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => 31,

                ),
            14 =>
                array (
                    'id' => 15,
                    'benefit_type_group_id' => '3',
                    'name' => 'Temporary Total Disablement Refund',
                    'shortcode' => 'TTD Refund',
                    'fin_account_code_id' => 1,
                    'benefit_type_claim_id' => 4,
                    'created_at' => '2017-04-19 11:42:05',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => 31,

                ),
            15 =>
                array (
                    'id' => 16,
                    'benefit_type_group_id' => '3',
                    'name' => 'Medical Aid Expenses Refund (Employee/Employer)',
                    'shortcode' => 'MAE Refund',
                    'fin_account_code_id' => 3,
                    'benefit_type_claim_id' => 1,
                    'created_at' => '2017-04-19 11:42:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => 35,
                    ),
            16 =>
                array (
                    'id' => 17,
                    'benefit_type_group_id' => '3',
                    'name' => 'Medical Aid Expenses Refund (HCP/HSP)',
                    'shortcode' => 'MAE Refund',
                    'fin_account_code_id' => 3,
                    'benefit_type_claim_id' => 2,
                    'created_at' => '2017-04-19 11:42:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => 35,
                ),

            17 =>
                array (
                    'id' => 18,
                    'benefit_type_group_id' => '2',
                    'name' => 'PTD AND PPD Pension',
                    'shortcode' => 'PTD AND PPD Pension',
                    'fin_account_code_id' => 2,
                    'benefit_type_claim_id' => 5,
                    'created_at' => '2019-05-25 11:42:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                    'fin_code_id' => 29,
                ),


        ));

        $this->enableForeignKeys('benefit_types');
    }
}