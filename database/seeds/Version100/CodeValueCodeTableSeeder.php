<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Sysdef\CodeValueCode;

class CodeValueCodeTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    public function run()
    {
        $value = CodeValueCode::query()->updateOrCreate(
            ['code_id' => '32', 'reference' => "ITROUTINE"],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['code_id' => '31', 'reference' => "ITSPECIAL"],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CHCNOTN", 'code_id' => '13'],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CHCNOTN", 'code_id' => '23'],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CCAEMPINSPCTNTSK", 'code_id' => '38'],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CCAEMPINSPCTNTSK", 'code_id' => '39'],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CCAEMPLYINSPCTN", 'code_id' => '33'],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CCAEMPLYINSPCTN", 'code_id' => '34'],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CCAEMPLYINSPCTN", 'code_id' => '35'],
            []
        );
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CCALETTER", 'code_id' => '40'],
            []
        );

        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CHCEMPDEREG", 'code_id' => '63'],
            []
        );

        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "CHCPAYALERT", 'code_id' => '64'],
            []
        );
        
        $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "ARREARCMT", 'code_id' => '71'],
            []
        );

         $value = CodeValueCode::query()->updateOrCreate(
            ['reference' => "INSTMOUUPLOAD", 'code_id' => '73'],
            []
        );


        
    }
}