<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class AuditReferencesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('audit_references');
        
        \DB::table('audit_references')->insert(array (
            0 => 
            array (
                'name' => 'Approve',
                'created_at' => '2017-04-18 12:12:13',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'name' => 'Reject',
                'created_at' => '2017-04-18 12:12:13',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'name' => 'Create',
                'created_at' => '2017-04-18 12:12:39',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'name' => 'Cancel',
                'created_at' => '2017-04-18 12:12:39',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'name' => 'Activate',
                'created_at' => '2017-04-18 12:12:57',
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
    }
}