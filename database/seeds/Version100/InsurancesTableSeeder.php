<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class InsurancesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('insurances');

        \DB::table('insurances')->insert(array (
            0 =>
                array (
                    'name' => 'AAR',
                    'external_id'=> NULL,
                                        'bank_branch_id'=> NULL,
                    'accountno'=> NULL,
                    'created_at' => '2017-07-25 09:53:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            1 =>
                array (
                    'name' => 'Jubilee',
                    'external_id'=> NULL,
                                  'bank_branch_id'=> NULL,
                    'accountno'=> NULL,
                    'created_at' => '2017-07-25 09:53:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'name' => 'Strategies',
                    'external_id'=> NULL,
                                       'bank_branch_id'=> NULL,
                    'accountno'=> NULL,
                    'created_at' => '2017-07-25 09:53:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            3 =>
                array (
                    'name' => 'NHIF',
                    'external_id'=> NULL,
                                     'bank_branch_id'=> NULL,
                    'accountno'=> NULL,
                    'created_at' => '2017-07-25 09:53:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            4 =>
                array (
                    'name' => 'MOI',
                    'external_id'=> NULL,
                                      'bank_branch_id'=> NULL,
                    'accountno'=> NULL,
                    'created_at' => '2017-07-25 09:53:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            5 =>
                array (
                    'name' => 'Ocean Road',
                    'external_id'=> NULL,
                                 'bank_branch_id'=> NULL,
                    'accountno'=> NULL,
                    'created_at' => '2017-07-25 09:53:21',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

        ));

        $this->enableForeignKeys();
    }
}