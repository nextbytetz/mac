<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class CasePersonnelsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    public function run()
    {
        $this->disableForeignKeys('case_personnels');
        $this->delete('case_personnels');

        \DB::table('case_personnels')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Judge',
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Magistrate',
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Chairman',
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Registrar',
                ),
        ));

        $this->enableForeignKeys('case_personnels');
    }

}