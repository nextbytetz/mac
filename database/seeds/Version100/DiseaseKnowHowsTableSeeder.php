<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DiseaseKnowHowsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('disease_know_hows');
        $this->delete('disease_know_hows');
        
        \DB::table('disease_know_hows')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Statutory medical examination',
                'created_at' => '2017-04-19 09:49:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Follow up for the illness',
                'created_at' => '2017-04-19 09:49:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('disease_know_hows');
    }
}