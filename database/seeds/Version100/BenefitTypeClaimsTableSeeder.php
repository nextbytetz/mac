<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class BenefitTypeClaimsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('benefit_type_claims');
        $this->delete('benefit_type_claims');

        \DB::table('benefit_type_claims')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'MAE Refund (Employee/Employer)',
                    'shortcode' => 'MAE Refund (Employee/Employer)',
                    'dependency_id' => NULL,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'MAE Refund (HCP/HSP)',
                    'shortcode' => 'MAE Refund (HCP/HSP)',
                    'dependency_id' => NULL,
                    'isactive' => 0,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Temporary Disablement',
                    'shortcode' => 'TD',
                    'dependency_id' => NULL,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Temporary Disablement Refund',
                    'shortcode' => 'TD Refund',
                    'dependency_id' => NULL,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Permanent Disablement',
                    'shortcode' => 'TD',
                    'dependency_id' => NULL,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Funeral Grant',
                    'shortcode' => 'FG',
                    'dependency_id' => 7,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'name' => 'Survivors',
                    'shortcode' => 'Survivors',
                    'dependency_id' => NULL,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'name' => 'Constant Care Attendant',
                    'shortcode' => 'CCA',
                    'dependency_id' => 5,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'name' => 'Rehabilitation',
                    'shortcode' => 'Rehabilitation',
                    'dependency_id' => 5,
                    'isactive' => 1,
                    'created_at' => '2017-04-19 11:39:21',
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('benefit_type_claims');

    }

}