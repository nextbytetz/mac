<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class AccidentTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('accident_types');
        $this->delete('accident_types');

        \DB::table('accident_types')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'On premises',
                    'created_at' => '2017-04-18 17:44:11',
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Out of premises',
                    'created_at' => '2017-04-18 17:44:11',
                    'updated_at' => NULL,
                ),
        ));
        $this->enableForeignKeys('accident_types');

    }
}