<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class InvestigationQuestionIncidentTypeTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys('investigation_question_incident_type');
        $this->delete('investigation_question_incident_type');

        \DB::table('investigation_question_incident_type')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'investigation_question_id' => '1',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'investigation_question_id' => '1',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'investigation_question_id' => '1',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'investigation_question_id' => '2',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'investigation_question_id' => '2',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'investigation_question_id' => '2',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'investigation_question_id' => '3',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'investigation_question_id' => '3',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'investigation_question_id' => '3',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            9 =>
                array (
                    'id' => 10,
                    'investigation_question_id' => '4',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            10 =>
                array (
                    'id' => 11,
                    'investigation_question_id' => '4',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            11 =>
                array (
                    'id' => 12,
                    'investigation_question_id' => '4',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'investigation_question_id' => '5',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'investigation_question_id' => '5',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            14 =>
                array (
                    'id' => 15,
                    'investigation_question_id' => '5',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            15 =>
                array (
                    'id' => 16,
                    'investigation_question_id' => '17',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            16 =>
                array (
                    'id' => 17,
                    'investigation_question_id' => '17',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            17 =>
                array (
                    'id' => 18,
                    'investigation_question_id' => '18',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            18 =>
                array (
                    'id' => 19,
                    'investigation_question_id' => '19',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            19 =>
                array (
                    'id' => 20,
                    'investigation_question_id' => '20',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            20 =>
                array (
                    'id' => 21,
                    'investigation_question_id' => '7',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            21 =>
                array (
                    'id' => 22,
                    'investigation_question_id' => '7',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            22 =>
                array (
                    'id' => 23,
                    'investigation_question_id' => '7',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            23 =>
                array (
                    'id' => 24,
                    'investigation_question_id' => '8',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            24 =>
                array (
                    'id' => 25,
                    'investigation_question_id' => '8',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            25 =>
                array (
                    'id' => 26,
                    'investigation_question_id' => '10',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            26 =>
                array (
                    'id' => 27,
                    'investigation_question_id' => '10',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            27 =>
                array (
                    'id' => 28,
                    'investigation_question_id' => '10',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            28 =>
                array (
                    'id' => 29,
                    'investigation_question_id' => '21',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            29 =>
                array (
                    'id' => 30,
                    'investigation_question_id' => '21',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            30 =>
                array (
                    'id' => 31,
                    'investigation_question_id' => '21',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            31 =>
                array (
                    'id' => 32,
                    'investigation_question_id' => '22',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            32 =>
                array (
                    'id' => 33,
                    'investigation_question_id' => '22',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            33 =>
                array (
                    'id' => 34,
                    'investigation_question_id' => '23',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            34 =>
                array (
                    'id' => 35,
                    'investigation_question_id' => '23',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            35 =>
                array (
                    'id' => 36,
                    'investigation_question_id' => '11',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            36 =>
                array (
                    'id' => 37,
                    'investigation_question_id' => '11',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            37 =>
                array (
                    'id' => 38,
                    'investigation_question_id' => '11',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            38 =>
                array (
                    'id' => 39,
                    'investigation_question_id' => '12',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            39 =>
                array (
                    'id' => 40,
                    'investigation_question_id' => '12',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            40 =>
                array (
                    'id' => 41,
                    'investigation_question_id' => '12',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            41 =>
                array (
                    'id' => 42,
                    'investigation_question_id' => '13',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            42 =>
                array (
                    'id' => 43,
                    'investigation_question_id' => '13',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            43 =>
                array (
                    'id' => 44,
                    'investigation_question_id' => '13',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            44 =>
                array (
                    'id' => 45,
                    'investigation_question_id' => '24',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            45 =>
                array (
                    'id' => 46,
                    'investigation_question_id' => '24',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            46 =>
                array (
                    'id' => 47,
                    'investigation_question_id' => '25',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            47 =>
                array (
                    'id' => 48,
                    'investigation_question_id' => '25',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            48 =>
                array (
                    'id' => 49,
                    'investigation_question_id' => '26',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            49 =>
                array (
                    'id' => 50,
                    'investigation_question_id' => '26',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            50 =>
                array (
                    'id' => 51,
                    'investigation_question_id' => '27',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            51 =>
                array (
                    'id' => 52,
                    'investigation_question_id' => '27',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            52 =>
                array (
                    'id' => 53,
                    'investigation_question_id' => '28',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            53 =>
                array (
                    'id' => 54,
                    'investigation_question_id' => '28',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            54 =>
                array (
                    'id' => 55,
                    'investigation_question_id' => '29',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            55 =>
                array (
                    'id' => 56,
                    'investigation_question_id' => '29',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            56 =>
                array (
                    'id' => 57,
                    'investigation_question_id' => '29',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            57 =>
                array (
                    'id' => 58,
                    'investigation_question_id' => '16',
                    'incident_type_id' => '1',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            58 =>
                array (
                    'id' => 59,
                    'investigation_question_id' => '16',
                    'incident_type_id' => '2',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            59 =>
                array (
                    'id' => 60,
                    'investigation_question_id' => '16',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            60 =>
                array (
                    'id' => 61,
                    'investigation_question_id' => '30',
                    'incident_type_id' => '3',
                    'created_at' => '2017-04-19 18:25:52',
                    'updated_at' => NULL,
                ),
            61 =>
            array (
                'id' => 62,
                'investigation_question_id' => '30',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
            ),
            62 =>
            array (
                'id' => 63,
                'investigation_question_id' => '30',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
            ),
            63 =>
            array (
                'id' => 64,
                'investigation_question_id' => '31',
                'incident_type_id' => '3',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
            ),
            64 =>
            array (
                'id' => 65,
                'investigation_question_id' => '32',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
            ),
            65 =>
            array (
                'id' => 66,
                'investigation_question_id' => '33',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
            ),
            66 =>
            array (
                'id' => 67,
                'investigation_question_id' => '34',
                'incident_type_id' => '2',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
            ),
            67 =>
            array (
                'id' => 68,
                'investigation_question_id' => '31',
                'incident_type_id' => '1',
                'created_at' => '2017-04-19 18:25:52',
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('investigation_question_incident_type');
    }

}