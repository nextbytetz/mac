<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class WfModulesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('wf_modules');
        $this->delete('wf_modules');

        \DB::table('wf_modules')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Interest Waiving',
                'wf_module_group_id' => '1',
                'created_at' => '2017-04-20 12:02:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Interest Adjustment',
                'wf_module_group_id' => '2',
                'created_at' => '2017-04-20 12:02:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Claim & Notification Processing',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '0',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Rejected Occupation Accident, Disease & Death',
                'wf_module_group_id' => '4',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2', /* Because it look like the procedure for incident type => 2 for the diseases */
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Contribution Receipt',
                'wf_module_group_id' => '5',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Employer Registration 2L',
                'wf_module_group_id' => '6',
                'created_at' => '2017-09-12 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'Contribution Receipt Before Electronic',
                'wf_module_group_id' => '7',
                'created_at' => '2017-10-10 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'Online Employer Verification',
                'wf_module_group_id' => '8',
                'created_at' => '2017-10-10 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'Interest Refund',
                'wf_module_group_id' => '9',
                'created_at' => '2018-01-10 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'Occupation Accident, Disease & Death',
                'wf_module_group_id' => '3',
                'created_at' => '2018-01-10 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'Notification Rejection',
                'wf_module_group_id' => '4',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'Notification Incident Approval.',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '4', /*started 4 by assuming to skip the three types of incidents*/
                'description' => 'Determining Whether It Is Occupational (Accident, Disease & Death)',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'Accident Medical Aid Expense (MAE) Special Approval',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '5',
                'description' => 'HCP/HSP or Insurance Refund for Accident, For Out of scope or exceed MAE maximum limit, late submission (more than 30 days). They are paid in bulk from collection of MAE approvals ( + Special) periodically',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            13 =>
            array (
                'id' => 14,
                'name' => 'Disease Medical Aid Expense (MAE) Special Approval',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '6',
                'description' => 'HCP/HSP or Insurance Refund for Disease, For Out of scope or exceed MAE maximum limit, late submission (more than 30 days). They are paid in bulk from collection of MAE approvals ( + Special) periodically',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            14 =>
            array (
                'id' => 15,
                'name' => 'Medical Aid Expense Approval for HCP/HSP',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '7',
                'description' => 'HCP/HSP or Insurance approval which comply to the regulations. They are paid in bulk from collection of MAE approvals ( + Special) for HCP/HSP periodically',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            15 =>
            array (
                'id' => 16,
                'name' => 'Medical Aid Expense Refund for HCP/HSP',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '8',
                'description' => 'Medical Aid Expense Refund for HCP/HSP or Insurance. To be processed periodical by considering all Medical Aid Expense Approvals for HCP/HSP and all special approvals',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            16 =>
            array (
                'id' => 17,
                'name' => 'Medical Aid Expense Refund for Employee/Employer',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '9',
                'description' => 'Medical Aid Expense Refund for Employee/Employer',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            17 =>
            array (
                'id' => 18,
                'name' => 'Claim Benefits',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '10',
                'description' => 'Claim Benefits for Accident & Disease & Death',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'System Rejected Notification.',
                'wf_module_group_id' => '4',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '4',
                'description' => 'All incidents which has been rejected automatically by system. By checking whether the incident date is before the start of the period (WCF Start Date) and if the incident is out of time (Exceeding 12 months after the date of incident)',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'Unregistered Member Notification.',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '11',
                'description' => 'Incident report with the member not registered in the system',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'Missing Contribution Notification.',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '12',
                'description' => 'Incident report with the member missing contributions in the system',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            21 =>
            array (
                'id' => 22,
                'name' => 'Incorrect Contribution Notification',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '13',
                'description' => 'Incident report with the employee having the incorrect last contribution amount',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            22 =>
            array (
                'id' => 23,
                'name' => 'Rejection from Notification Approval',
                'wf_module_group_id' => '4',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '5',
                'description' => 'Rejected notification from notification approval workflow (Determining whether incident is occupation). ',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            23 =>
            array (
                'id' => 24,
                'name' => 'Incorrect Employee Info',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '14',
                'description' => 'Incident report with the employee having some incorrect registered information mainly (Employee name, gender and date of birth) ',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            24 =>
            array (
                'id' => 25,
                'name' => 'Payroll Bank Details Modification',
                'wf_module_group_id' => '11',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Modification of bank details of Beneficiary',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            25 =>
            array (
                'id' => 26,
                'name' => 'Payroll Status Changes',
                'wf_module_group_id' => '12',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => 'Payroll Beneficiary status changes',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),

            26 =>
            array (
                'id' => 27,
                'name' => 'Payroll Recovery Processing',
                'wf_module_group_id' => '13',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Payroll recovery processing',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),

            27 =>
            array (
                'id' => 28,
                'name' => 'Payroll Run Processing - 12L',
                'wf_module_group_id' => '10',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '1',
                'description' => 'Processing monthly pension payroll',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),


            28 =>
            array (
                'id' => 29,
                'name' => 'Payroll Reconciliation Processing',
                'wf_module_group_id' => '14',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval for payroll beneficiaries need to be reconciled',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            29 =>
            array (
                'id' => 30,
                'name' => 'Payroll Beneficiary Details modification',
                'wf_module_group_id' => '15',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval for payroll beneficiary details modification',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            30 =>
            array (
                'id' => 31,
                'name' => 'Missing Contribution Notification',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '12',
                'description' => 'Incident report with the member missing contributions in the system',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            31 =>
            array (
                'id' => 32,
                'name' => 'Unregistered Member Notification',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '11',
                'description' => 'Incident report with the member not registered in the system',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            32 =>
            array (
                'id' => 33,
                'name' => 'System Rejected Notification',
                'wf_module_group_id' => '4',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '4',
                'description' => 'All incidents which has been rejected automatically by system. By checking whether the incident date is before the start of the period (WCF Start Date) and if the incident is out of time (Exceeding 12 months after the date of incident)',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            33 =>
            array (
                'id' => 34,
                'name' => 'Notification Incident Approval',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '4', /*started 4 by assuming to skip the three types of incidents*/
                'description' => 'Determining Whether It Is Occupational (Accident, Disease & Death)',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            34 =>
            array (
                'id' => 35,
                'name' => 'Employer Advance Payment',
                'wf_module_group_id' => '16',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval for employer advance payments',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            35 =>
            array (
                'id' => 36,
                'name' => 'Online Notification Application',
                'wf_module_group_id' => '17',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval for employer/employee online incident notification',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 1
            ),
            36 =>
            array (
                'id' => 37,
                'name' => 'Temporary Business De-Registrations',
                'wf_module_group_id' => '18',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => 'Approval for Employer Temporary Business De-Registration',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0
            ),

            37 =>
            array (
                'id' => 38,
                'name' => 'Permanent Business De-Registrations',
                'wf_module_group_id' => '18',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => 'Approval for Employer Permanent Business De-Registration',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            38 =>
            array (
                'id' => 39,
                'name' => 'Void Notification',
                'wf_module_group_id' => '3',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '15',
                'description' => 'Void notification files which has gone past investigation stage.',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            39 =>
            array (
                'id' => 40,
                'name' => 'Notification Incident Approval_',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '16',
                'description' => 'Determining Whether It Is Occupational (Accident, Disease & Death) including all staffs from DAS',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            40 =>
            array (
                'id' => 41,
                'name' => 'Undo Notification Rejection',
                'wf_module_group_id' => '4',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '6',
                'description' => 'Undo the notification rejection, and restore the notification to the normal process',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            41 =>
            array (
                'id' => 42,
                'name' => 'Receipt Verification',
                'wf_module_group_id' => '5',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            42 =>
            array (
                'id' => 43,
                'name' => 'Receipt Verification with Advance',
                'wf_module_group_id' => '5',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '3',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            43 =>
            array (
                'id' => 44,
                'name' => 'Acknowledgement Letter Issuance',
                'wf_module_group_id' => '19',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            44 =>
            array (
                'id' => 45,
                'name' => 'Employer Registration 3L',
                'wf_module_group_id' => '6',
                'created_at' => '2017-09-12 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            45 =>
            array (
                'id' => 46,
                'name' => 'Inspection Plan Approval',
                'wf_module_group_id' => '20',
                'created_at' => '2017-09-12 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            46 =>
            array (
                'id' => 47,
                'name' => 'Inspection Assessment Review',
                'wf_module_group_id' => '21',
                'created_at' => '2017-09-12 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            47 =>
            array (
                'id' => 48,
                'name' => 'Compliance Letter Issuance',
                'wf_module_group_id' => '19',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            48 =>
            array (
                'id' => 49,
                'name' => 'Defaulters (Employer)',
                'wf_module_group_id' => '21',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            49 =>
            array (
                'id' => 50,
                'name' => 'Instalment Payment Request (Employer)',
                'wf_module_group_id' => '21',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '3',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            50 =>
            array (
                'id' => 51,
                'name' => 'Employer De-registration Re-Open',
                'wf_module_group_id' => '22',
                'created_at' => '2019-10-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            51 =>
            array (
                'id' => 52,
                'name' => 'Employer Inspection Task Cancellation',
                'wf_module_group_id' => '21',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '4',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            52 =>
            array (
                'id' => 53,
                'name' => 'Benefit Award Letter Issuance',
                'wf_module_group_id' => '19',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '3',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            53 =>
            array (
                'id' => 54,
                'name' => 'Refunding Employer Overpayment',
                'wf_module_group_id' => '21',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '0',
                'type' => '5',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),

            54 =>
            array (
                'id' => 55,
                'name' => 'Employer Particular Changes - Internal',
                'wf_module_group_id' => '23',
                'created_at' => '2019-11-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            55 =>
            array (
                'id' => 56,
                'name' => 'Payroll Monthly Pension Updates',
                'wf_module_group_id' => '24',
                'created_at' => '2019-11-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            56 =>
            array (
                'id' => 57,
                'name' => 'Inspection Plan Approval II',
                'wf_module_group_id' => '20',
                'created_at' => '2017-09-12 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),
            57 =>
            array (
                'id' => 58,
                'name' => 'Employer Return of Earnings',
                'wf_module_group_id' => '25',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval for employer Return of Earnings',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),

            58 =>
            array (
                'id' => 59,
                'name' => 'Employer Particular Changes - Portal',
                'wf_module_group_id' => '23',
                'created_at' => '2019-11-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),

            59 =>
            array (
                'id' => 60,
                'name' => 'Payment by Installment',
                'wf_module_group_id' => '26',
                'created_at' => '2019-11-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            60 =>
            array (
                'id' => 61,
                'name' => 'Erroneous Contribution',
                'wf_module_group_id' => '27',
                'created_at' => '2019-11-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            61 =>
            array (
                'id' => 62,
                'name' => 'Rejection from Notification Approval II',
                'wf_module_group_id' => '4',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '7',
                'description' => 'Rejected notification from notification approval workflow (Determining whether incident is occupation). ',
                'allow_repeat' => 1,
                'wf_mode' => 2,
                'notify' => 0

            ),
            62 =>
            array (
                'id' => 63,
                'name' => 'Payroll Run Processing - 9L',
                'wf_module_group_id' => '10',
                'created_at' => '2020-06-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => 'Processing monthly pension payroll',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            63 =>
            array (
                'id' => 64,
                'name' => 'OSH Audit List Of Employers',
                'wf_module_group_id' => '28',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval of OSH Audit List Of Employers',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            64 =>
            array (
                'id' => 65,
                'name' => 'Investment Budget',
                'wf_module_group_id' => '29',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval of Investment Budget for specific financial year',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            65 =>
            array (
                'id' => 66,
                'name' => 'Employer De-Registration Extension',
                'wf_module_group_id' => '30',
                'created_at' => '2020-09-16 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Approval of extension requests of employer de-registration period',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            66 =>
            array (
                'id' => 67,
                'name' => 'Incorrect Contribution Notification II',
                'wf_module_group_id' => '3',
                'created_at' => '2017-04-20 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '17',
                'description' => 'Incident report with the employee having the incorrect last contribution amount',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),

            67 =>
            array (
                'id' => 68,
                'name' => 'Interest Adjustment (Receipt) - 6L',
                'wf_module_group_id' => '33',
                'created_at' => '2020-09-16 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => 'Approval of interest adjustment on receipt date update',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            68 =>
            array (
                'id' => 69,
                'name' => 'Payroll Child Reinstate: School',
                'wf_module_group_id' => '12',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => 'Payroll Beneficiary status changes for child dependent for school continuation',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            69 =>
            array (
                'id' => 70,
                'name' => 'Payroll Child Reinstate: Disability',
                'wf_module_group_id' => '12',
                'created_at' => '2019-01-23 12:03:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '3',
                'description' => 'Payroll Beneficiary status changes for child dependent for disability',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            70 =>
            array (
                'id' => 71,
                'name' => 'Claim Accrual At Administration Level',
                'wf_module_group_id' => '31',
                'created_at' => '2020-12-03 11:07:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => 'Accrual of Claims that are still at administration level',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            71 =>
            array (
                'id' => 72,
                'name' => 'Claim Accrual At Assesment Level',
                'wf_module_group_id' => '31',
                'created_at' => '2020-12-03 11:07:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => 'Accrual of Claims that are still at assesment level',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            72 =>
            array (
                'id' => 73,
                'name' => 'Investigation Plan Without Budget',
                'wf_module_group_id' => '32',
                'created_at' => '2020-12-11 17:38:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => 'Claim Investigation Plan',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            73=>
            array (
                'id' => 74,
                'name' => 'Investigation Plan With Budget',
                'wf_module_group_id' => '32',
                'created_at' => '2020-12-11 17:38:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '2',
                'description' => 'Claim Investigation Plan That Require Budget',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            74=>
            array (
                'id' => 75,
                'name' => 'Payroll Retiree Monthly Pension Update',
                'wf_module_group_id' => '34',
                'created_at' => '2020-12-11 17:38:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '1',
                'description' => 'Approval for updating monthly pension of retired pensioners',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            75=>
            array (
                'id' => 76,
                'name' => 'Claim Benefits II',
                'wf_module_group_id' => '3',
                'created_at' => '2020-12-11 17:38:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '18',
                'description' => 'Claim Benefits for Accident & Disease & Death',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),
            76=>
            array (
                'id' => 77,
                'name' => 'Approve Notification Transfer to Death',
                'wf_module_group_id' => '3',
                'created_at' => '2020-12-11 17:38:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '19',
                'description' => 'Accident & Disease Notifications which are added to Death',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),
            77 =>
            array (
                'id' => 78,
                'name' => 'Reminder Letter Issuance',
                'wf_module_group_id' => '19',
                'created_at' => '2017-04-20 12:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '4',
                'description' => '',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0

            ),

            78=>
            array (
             'id' => 79,
             'name' => 'Multiple Payrolls Merging',
             'wf_module_group_id' => '35',
             'created_at' => '2018-01-10 16:43:20',
             'updated_at' => NULL,
             'deleted_at' => NULL,
             'isactive' => '1',
             'type' => '0',
             'description' => 'Requesting for merging of employer\'s non-existence multiple payrolls',
             'allow_repeat' => 0,
             'wf_mode' => 1,
             'notify' => 0

         ),
            79 =>
            array (
                'id' => 80,
                'name' => 'HCP / HSP Bills Vetting',
                'wf_module_group_id' => '36',
                'created_at' => '2021-05-01 08:00:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Health Care and Health Service Provider Bills Vetting',
                'allow_repeat' => 0,
                'wf_mode' => 2,
                'notify' => 0
            ),

            80 =>
            array (
                'id' => 81,
                'name' => 'Employer\'s Contribution Modification',
                'wf_module_group_id' => '37',
                'created_at' => '2021-07-20 08:00:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Modification / adjustment of employers\'s contribution',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

            81 =>
            array (
                'id' => 82,
                'name' => 'Contribution / Interest Rate Adjustment',
                'wf_module_group_id' => '38',
                'created_at' => '2021-07-20 08:00:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'isactive' => '1',
                'type' => '0',
                'description' => 'Contribution / Interest Rate Adjustment',
                'allow_repeat' => 0,
                'wf_mode' => 1,
                'notify' => 0
            ),

        ));

$this->enableForeignKeys('wf_modules');
}
}