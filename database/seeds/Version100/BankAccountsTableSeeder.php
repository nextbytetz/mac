<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;


class BankAccountsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys("bank_accounts");
        $this->delete('bank_accounts');

        \DB::table('bank_accounts')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'bank_id' => 3,
                    'fin_code_id' => 43,
                    'isonline' => 0,
                ),
            1 =>
                array (
                    'id' => 2,
                    'bank_id' => 3,
                    'fin_code_id' => 44,
                    'isonline' => 0,
                ),
            2 =>
                array (
                    'id' => 3,
                    'bank_id' => 3,
                    'fin_code_id' => 45,
                    'isonline' => 1,
                ),
            3 =>
                array (
                    'id' => 4,
                    'bank_id' => 4,
                    'fin_code_id' => 46,
                    'isonline' => 0,
                ),
            4 =>
                array (
                    'id' => 5,
                    'bank_id' => 4,
                    'fin_code_id' => 47,
                    'isonline' => 0,
                ),
            5 =>
                array (
                    'id' => 6,
                    'bank_id' => 4,
                    'fin_code_id' => 48,
                    'isonline' => 1,
                ),
        ));

        $this->enableForeignKeys("bank_accounts");

    }


}