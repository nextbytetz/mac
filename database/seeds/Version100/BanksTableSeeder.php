<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class BanksTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys("banks");
        $this->delete('banks');
        
        \DB::table('banks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'STANDARD CHARTERED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 777,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'POST GIRO',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => NULL,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'NMB',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 588,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'CRDB',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 1378,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'NOT_APPLICABLE',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => NULL,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'WITHDRAWAL',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => NULL,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'BARCLAYS',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 3006,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'TANZANIA POSTAL BANK',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 6418,
                'erp_supplier_id' => null,
                'created_at' => '2017-04-21 11:51:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'NATIONAL BANK OF COMMERCE',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 2973,
                'erp_supplier_id' => null,
                'created_at' => '2021-02-18 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'ABSA BANK TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 3006,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'ACCESS BANK (TANZANIA) LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 549,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
              11 => 
            array (
                'id' => 12,
                'name' => 'AKIBA COMMERCIAL BANK PLC',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 443,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'AMANA BANK LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 1627,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'AZANIA BANK LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 1031,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'BANK OF BARODA TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 6213,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'BANK OF INDIA TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 172,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'CITIBANK TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 1037,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'DCB COMMERCIAL BANK PLC',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 4806,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'DIAMOND TRUST BANK TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 1559,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'ECOBANK TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 18125,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'EQUITY BANK TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 3777,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'EXIM BANK TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 53,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'I & M BANK TANZANIA LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 1783,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'MKOMBOZI COMMERCIAL BANK PLC',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 2631,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'MWALIMU COMMERCIAL BANK PLC',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 5344,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'PEOPLES BANK OF ZANZIBAR LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => NULL,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'STANBIC BANK TANZANIA LTD',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 2162,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'TIB DEVELOPMENT BANK LIMITED',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 542,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
             28 => 
            array (
                'id' => 29,
                'name' => 'BANK OF TANZANIA',
                'accountno' => NULL,
                'pmt_channel' => NULL,
                'employer_id' => 6433,
                'erp_supplier_id' => null,
                'created_at' => '2021-08-13 14:33:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys("banks");
    }
}