<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Sysdef\CodeValue;

class CodeValuesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    public function run()
    {
        //$this->disableForeignKeys();

        /*  start : Business Sectors */
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSAFF'],
            [
                'code_id' => 5,
                'name' => 'Agriculture, Forestry and Fishing',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSTSG'],
            [
                'code_id' => 5,
                'name' => 'Transportation and Storage',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSPADC'],
            [
                'code_id' => 5,
                'name' => 'Public Administration and Defense; Compulsory',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSMQR'],
            [
                'code_id' => 5,
                'name' => 'Mining and Quarrying',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSAFBSA'],
            [
                'code_id' => 5,
                'name' => 'Accommodation, Food and Beverage Service Activities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSEDC'],
            [
                'code_id' => 5,
                'name' => 'Education',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSMFC'],
            [
                'code_id' => 5,
                'name' => 'Manufacturing',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSICT'],
            [
                'code_id' => 5,
                'name' => 'Information and Technology',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSHHSWA'],
            [
                'code_id' => 5,
                'name' => 'Human Health and Social Work Activities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSEGSAC'],
            [
                'code_id' => 5,
                'name' => 'Electricity, Gas, Steam and Air Conditioning',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSFIA'],
            [
                'code_id' => 5,
                'name' => 'Financial and Insurance Activities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSOSA'],
            [
                'code_id' => 5,
                'name' => 'Other Service Activities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSCOTR'],
            [
                'code_id' => 5,
                'name' => 'Construction',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSPSTA'],
            [
                'code_id' => 5,
                'name' => 'Professional, Scientific and Technical Activities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSAHEL'],
            [
                'code_id' => 5,
                'name' => 'Activities of Household as Employers',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSWRTD'],
            [
                'code_id' => 5,
                'name' => 'Wholesale and Retail Trade',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSASSAT'],
            [
                'code_id' => 5,
                'name' => 'Administrative and Support Service Activities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSAEON'],
            [
                'code_id' => 5,
                'name' => 'Activities of Extraterritorial Organizations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSOTHRS'],
            [
                'code_id' => 5,
                'name' => 'Others (Please mention)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSSGDF'],
            [
                'code_id' => 5,
                'name' => 'Security Groups - Defences',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSRES'],
            [
                'code_id' => 5,
                'name' => 'Real Estates',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSSTPR'],
            [
                'code_id' => 5,
                'name' => 'Stationary and Printing',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSTUN'],
            [
                'code_id' => 5,
                'name' => 'Trade Unions',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSNGO'],
            [
                'code_id' => 5,
                'name' => 'NGO',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSCWS'],
            [
                'code_id' => 5,
                'name' => 'Car Wash',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSHOSP'],
            [
                'code_id' => 5,
                'name' => 'Hospital',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSGMBL'],
            [
                'code_id' => 5,
                'name' => 'Gambling',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSTURS'],
            [
                'code_id' => 5,
                'name' => 'Tourism',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BSWSWMR'],
            [
                'code_id' => 5,
                'name' => 'Water Supply;Sewerage,waste management and remediation activities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /*  end : Business Sectors */

        /*  start : Authorities */

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUTRA'],
            [
                'code_id' => 6,
                'name' => 'Tanzania Revenue Authorities (TRA)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUOSHA'],
            [
                'code_id' => 6,
                'name' => 'Occupational Safety and Health Authority (OSHA)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUBRELA'],
            [
                'code_id' => 6,
                'name' => 'Business Registration & Licencing Agency(BRELA)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUCRB'],
            [
                'code_id' => 6,
                'name' => 'Contractors Registration Board (CRB)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUFRSC'],
            [
                'code_id' => 6,
                'name' => 'Fire and Rescue',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSUMT'],
            [
                'code_id' => 6,
                'name' => 'Surface and Marine Transport Regulatory Authority (SUMATRA)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUOTHR'],
            [
                'code_id' => 6,
                'name' => 'Other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /*  end : Authorities */

        /*  start : Employer Categories */

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPUB'],
            [
                'code_id' => 4,
                'name' => 'Public',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPRI'],
            [
                'code_id' => 4,
                'name' => 'Private',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECMAN'],
            [
                'code_id' => 4,
                'name' => 'Mandate/Facilitator',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /*  end : Employer Categories */

        /*  start : Nature Of Incident */

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIMUI'],
            [
                'code_id' => 3,
                'name' => 'Multiple Injuries',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NICOAB'],
            [
                'code_id' => 3,
                'name' => 'Contusions and abrasions',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIBUSC'],
            [
                'code_id' => 3,
                'name' => 'Burns and scalds',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NICONC'],
            [
                'code_id' => 3,
                'name' => 'Concussions',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NICULAC'],
            [
                'code_id' => 3,
                'name' => 'Cuts and laceration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIPUWOU'],
            [
                'code_id' => 3,
                'name' => 'Punctured wounds',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIAMPU'],
            [
                'code_id' => 3,
                'name' => 'Amputation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIDISL'],
            [
                'code_id' => 3,
                'name' => 'Dislocations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIFRACT'],
            [
                'code_id' => 3,
                'name' => 'Fractures',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NISPSTR'],
            [
                'code_id' => 3,
                'name' => 'Sprains and strains',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NISPSTR'],
            [
                'code_id' => 3,
                'name' => 'Asphyxiation or drowning',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NITRIOR'],
            [
                'code_id' => 3,
                'name' => 'Tearing of internal organs',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIELSHO'],
            [
                'code_id' => 3,
                'name' => 'Electric shock',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIEYIES'],
            [
                'code_id' => 3,
                'name' => 'Eye injuries',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIOTHIN'],
            [
                'code_id' => 3,
                'name' => 'Other injuries',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIOCDIS'],
            [
                'code_id' => 3,
                'name' => 'Occupational diseases (all)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NINTSTA'],
            [
                'code_id' => 3,
                'name' => 'Not Stated',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /*  end : Nature Of Incident */

        /*  start : Body Part Injuries */

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPIHEYE'],
            [
                'code_id' => 2,
                'name' => 'Head eyes',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPIHOTHR'],
            [
                'code_id' => 2,
                'name' => 'Head other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPIUEFI'],
            [
                'code_id' => 2,
                'name' => 'Upper extremities - finger',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPIUEOTHR'],
            [
                'code_id' => 2,
                'name' => 'Upper extremities - other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPITRUN'],
            [
                'code_id' => 2,
                'name' => 'Trunk',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPILOEXT'],
            [
                'code_id' => 2,
                'name' => 'Lower extremities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPIMULT'],
            [
                'code_id' => 2,
                'name' => 'Multiple',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPINSTTD'],
            [
                'code_id' => 2,
                'name' => 'Not Stated',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /*  end : Body Part Injuries */

        /*  start : Incident Exposures */

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEPRMO'],
            [
                'code_id' => 1,
                'name' => 'Machinery - Power Driven - Prime Mover',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IETRANSMN'],
            [
                'code_id' => 1,
                'name' => 'Machinery - Power Driven - Transmission',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IELIFTN'],
            [
                'code_id' => 1,
                'name' => 'Machinery - Power Driven - Lifting',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEOTHR'],
            [
                'code_id' => 1,
                'name' => 'Machinery - Power Driven - Other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IENPLIFTN'],
            [
                'code_id' => 1,
                'name' => 'Machinery - Not Power Driven - Lifting',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IENPOTHR'],
            [
                'code_id' => 1,
                'name' => 'Machinery - Not Power Driven - Other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEHAWIMA'],
            [
                'code_id' => 1,
                'name' => 'Handling Without Machinery',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IETPDMVE'],
            [
                'code_id' => 1,
                'name' => 'Transport - Power Driven - Motor Vehicles',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IETPDOTHR'],
            [
                'code_id' => 1,
                'name' => 'Transport - Power Driven - Other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IETNPDRVN'],
            [
                'code_id' => 1,
                'name' => 'Transport - Not Power Driven',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEFIRE'],
            [
                'code_id' => 1,
                'name' => 'Fire',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEEXPLSN'],
            [
                'code_id' => 1,
                'name' => 'Explosion',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEOHCSUB'],
            [
                'code_id' => 1,
                'name' => 'Other Hot or Corrosive Substance',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEGASPOIS'],
            [
                'code_id' => 1,
                'name' => 'Gassing, Poisoning etc',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEELSHOC'],
            [
                'code_id' => 1,
                'name' => 'Electric Shock',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEFAPOB'],
            [
                'code_id' => 1,
                'name' => 'Falls of Person or Object',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IESOOSAOB'],
            [
                'code_id' => 1,
                'name' => 'Stepping On Or Striking Against Objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEUSHTO'],
            [
                'code_id' => 1,
                'name' => 'Use of Hand Tools',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEMISCAN'],
            [
                'code_id' => 1,
                'name' => 'Miscellaneous - Animals',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IETHROBBR'],
            [
                'code_id' => 1,
                'name' => 'Thugs, Robbery',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IEOTHR'],
            [
                'code_id' => 1,
                'name' => 'Other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IENTSTTD'],
            [
                'code_id' => 1,
                'name' => 'Not Stated',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /*  end : Incident Exposures */

        /*  start : Employee Categories */

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECTEMP'],
            [
                'code_id' => 7,
                'name' => 'Specified Period',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPERM'],
            [
                'code_id' => 7,
                'name' => 'Unspecified Period',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECSPECTS'],
            [
                'code_id' => 7,
                'name' => 'Specific Task/Casual',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /*  end : Employee Categories */

        /*  start : Marital Status */

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MSMARYD'],
            [
                'code_id' => 8,
                'name' => 'Married',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MSWIDOWD'],
            [
                'code_id' => 8,
                'name' => 'Widowed',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MSSPRTD'],
            [
                'code_id' => 8,
                'name' => 'Separated',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MSDIVCD'],
            [
                'code_id' => 8,
                'name' => 'Divorced',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        /*  end : Marital Status */



        /* Follow up types */


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'FUPHONC'],
            [
                'code_id' => 9,
                'name' => 'Phone Call',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );



        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'FUEMAIL'],
            [
                'code_id' => 9,
                'name' => 'Email',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'FUVSIT'],
            [
                'code_id' => 9,
                'name' => 'Visit',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        /* end : Follow up types */

        /* start : Payment Modes */
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PMWCBRN'],
            [
                'code_id' => 10,
                'name' => 'WCF Branch',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        /* end : Payment Modes */


        /* start : Annual Target Types  */
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTCONT'],
            [
                'code_id' => 11,
                'name' => 'Contribution Amount',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTINSP'],
            [
                'code_id' => 11,
                'name' => 'Inspection',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTEMPLYR'],
            [
                'code_id' => 11,
                'name' => 'Employers Registration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTEMPL'],
            [
                'code_id' => 11,
                'name' => 'Employees Registration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTCERT'],
            [
                'code_id' => 11,
                'name' => 'Employer Registration Certificates',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        /* end : Payment Modes */


        /* Marital Status - Single */
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MSSINGL'],
            [
                'code_id' => 8,
                'name' =>'Single',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOONPRM'],
            [
                'code_id' => 12,
                'name' =>'On Premises',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOOTPRM'],
            [
                'code_id' => 12,
                'name' =>'Out of Premises',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSNREJSYBS'],
            [
                'code_id' => 23,
                'name' => 'Rejection by System (Before WCF Statutory Period)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSNREJSYOT'],
            [
                'code_id' => 23,
                'name' => 'Rejection by System (Out of Time)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSONNREJSYW'],
            [
                'code_id' => 23,
                'name' => 'On (Rejection by System) Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSAPRDNREJSYW'],
            [
                'code_id' => 23,
                'name' => 'Approved (Rejection by System) Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NCOVDNOTWRK'],
            [
                'code_id' => 24,
                'name' => 'On Void Notification Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NCVOIDNOTFCN'],
            [
                'code_id' => 24,
                'name' => 'Void Notification',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NRONUNDORJWRK'],
            [
                'code_id' => 23,
                'name' => 'On Undo Rejection Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 10,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSPPRUM'],
            [
                'code_id' => 13,
                'name' => 'Pending Partial Registration (Unregistered Member)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ONSPPRUMW'],
            [
                'code_id' => 13,
                'name' => 'On Pending Partial Registration (Unregistered Member) Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSPPRMC'],
            [
                'code_id' => 13,
                'name' =>'Pending Partial Registration (Missing Contribution)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ONSPPRMCW'],
            [
                'code_id' => 13,
                'name' =>'On Pending Partial Registration (Missing Contribution) Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ONSPPRICW'],
            [
                'code_id' => 13,
                'name' =>'On (Incorrect Contribution Amount) Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 13,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ONSPPRIEIFW'],
            [
                'code_id' => 13,
                'name' =>'On Pending Partial Registration (Incorrect Employee Info) Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 12,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSPCRFC'],
            [
                'code_id' => 13,
                'name' =>'Pending Complete Registration (Filling Checklist)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSPCRFI'],
            [
                'code_id' => 13,
                'name' =>'Pending Complete Registration (Filling Investigation)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSPCRFNFI'],
            [
                'code_id' => 13,
                'name' =>'Pending Complete Registration (Finished Filling Investigation)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 7,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSOPNAP'],
            [
                'code_id' => 13,
                'name' => 'On Progress (Notification Approval Workflow)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 8,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSOTRTDEWRK'],
            [
                'code_id' => 13,
                'name' => 'On Approve Notification Transfer to Death Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 8,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSOPIA'],
            [
                'code_id' => 13,
                'name' => 'On Progress (Incident Approved)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 9,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSOPRFLI'],
            [
                'code_id' => 13,
                'name' => 'On Progress (Re-Filling Investigation)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 10,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSOPFRFLI'],
            [
                'code_id' => 13,
                'name' => 'On Progress (Finished Re-Filling Investigation)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 11,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSRINC'],
            [
                'code_id' => 23,
                'name' => 'Rejected Incident from Notification Approval',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ONSRINCW'],
            [
                'code_id' => 23,
                'name' => 'On Rejection from Notification Approval Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 7,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'APRDNSRINCW'],
            [
                'code_id' => 23,
                'name' => 'Approved Rejection from Notification Approval Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 8,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSOCBW'],
            [
                'code_id' => 13,
                'name' => 'On Claim Benefit Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 14,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSOPALP'],
            [
                'code_id' => 13,
                'name' => 'On Progress (Paid)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 15,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSCINC'],
            [
                'code_id' => 13,
                'name' => 'Closed Incident',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 22,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSRASSM'],
            [
                'code_id' => 13,
                'name' => 'On Reassessment',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 23,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSAMAESAW'],
            [
                'code_id' => 13,
                'name' => 'On Accident MAE Special Approval Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 18,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSDMAESAW'],
            [
                'code_id' => 13,
                'name' => 'On Disease MAE Special Approval Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 21,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSMAEAHCHSW'],
            [
                'code_id' => 13,
                'name' => 'On MAE Approval for HCP/HSP Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 0,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSMAERHCHSW'],
            [
                'code_id' => 13,
                'name' => 'On MAE Refund for HCP/HSP Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 0,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSMAERMEMW'],
            [
                'code_id' => 13,
                'name' => 'On MAE Refund for Employee/Employer Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 16,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSATLPMAN'],
            [
                'code_id' => 13,
                'name' => 'Paid Manually',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 19,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSARCHIVED'],
            [
                'code_id' => 13,
                'name' => 'Archived',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 20,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSARCHVDNDROPN'],
            [
                'code_id' => 13,
                'name' => 'Archived, Needs Re-open',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 17,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHCNOTN'],
            [
                'code_id' => 14,
                'name' => 'Notification & Claim',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CCAEMPLYARRER'],
            [
                'code_id' => 14,
                'name' => 'Employer Arrears',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CCAEMPLYINSPCTN'],
            [
                'code_id' => 14,
                'name' => 'Inspection Plan',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CCAEMPINSPCTNTSK'],
            [
                'code_id' => 14,
                'name' => 'Employer Inspection Task',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CCALETTER'],
            [
                'code_id' => 14,
                'name' => 'Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CCONTFCAPPCTN'],
            [
                'code_id' => 14,
                'name' => 'Online Notification Application',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACLIFT'],
            [
                'code_id' => 15,
                'name' => 'Falls of persons from heights (trees, buildings, scaffolds, ladders, machines,vehicles)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACFATG'],
            [
                'code_id' => 15,
                'name' => 'Falls of persons into depths (wells, ditches, excavations, holes in the ground)',
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PSCACT'],
            [
                'code_id' => 21,
                'name' =>'Activation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACDEHYD'],
            [
                'code_id' => 15,
                'name' => 'Falls of persons on the same level',
            ]);

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PSCDEACT'],
            [
                'code_id' => 21,
                'name' =>'Deactivation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACPOLIG'],
            [
                'code_id' => 15,
                'name' => 'Slides and cave-ins (earth, rocks, stones, snow)',
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PSCREIN'],
            [
                'code_id' => 21,
                'name' =>'Reinstate',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACHAZMAT'],
            [
                'code_id' => 15,
                'name' => 'Collapse (buildings, walls, scaffolds, ladders, piles of goods)',
            ]);

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PSCMASU'],
            [
                'code_id' => 21,
                'name' =>'Manual Suspension',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACAWVI'],
            [
                'code_id' => 15,
                'name' => 'Struck by falling objects during handling',
            ]);

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PSCREINR'],
            [
                'code_id' => 21,
                'name' =>'Reinstate Terminated',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACTRFAL'],
            [
                'code_id' => 15,
                'name' => 'Struck by falling objects, not classified',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACSTRESS'],
            [
                'code_id' => 15,
                'name' => 'Stepping on objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACSASOBJ'],
            [
                'code_id' => 15,
                'name' => 'Striking against stationary objects (except impacts due to a previous fall)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACSAMOBJ'],
            [
                'code_id' => 15,
                'name' => 'Striking against moving objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACSBMOFO'],
            [
                'code_id' => 15,
                'name' => 'Struck by moving objects (including flying fragments and particles) excluding falling objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACCIAOBJ'],
            [
                'code_id' => 15,
                'name' => 'Caught in an object',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACCBSOMO'],
            [
                'code_id' => 15,
                'name' => 'Caught between a stationary object and a moving object',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACCBMOFO'],
            [
                'code_id' => 15,
                'name' => 'Caught between moving objects (except flying or falling objects)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACOVILOBJ'],
            [
                'code_id' => 15,
                'name' => 'Overexertion in lifting objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACOEIPPOBJ'],
            [
                'code_id' => 15,
                'name' => 'Overexertion in pushing or pulling objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACOEIHATHOBJ'],
            [
                'code_id' => 15,
                'name' => 'Overexertion in handling or throwing objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACSTRMOVNT'],
            [
                'code_id' => 15,
                'name' => 'Strenuous movements',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACETHAE'],
            [
                'code_id' => 15,
                'name' => 'Exposure to heat (atmosphere or environment)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACETCAE'],
            [
                'code_id' => 15,
                'name' => 'Exposure to cold (atmosphere or environment)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACCWHSUOBJ'],
            [
                'code_id' => 15,
                'name' => 'Contact with hot substances or objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACCWVCSUOBJ'],
            [
                'code_id' => 15,
                'name' => 'Contact with very cold substances or objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACETOCECU'],
            [
                'code_id' => 15,
                'name' => 'Exposure to or contact with electric current',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACCIIAHSUB'],
            [
                'code_id' => 15,
                'name' => 'Contact by inhalation, ingestion or absorption of harmful substances',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACEXPIORAD'],
            [
                'code_id' => 15,
                'name' => 'Exposure to ionizing radiations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACEXPTRDIORAD'],
            [
                'code_id' => 15,
                'name' => 'Exposure to radiations other than ionizing radiations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACMTAPASS'],
            [
                'code_id' => 15,
                'name' => 'MTA as Passanger',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACMTADRIV'],
            [
                'code_id' => 15,
                'name' => 'MTA as Driver',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACMTAPEDSTR'],
            [
                'code_id' => 15,
                'name' => 'MTA as Pedestrian',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACMECHEXPLO'],
            [
                'code_id' => 15,
                'name' => 'Mechanical Explosion',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACCHEMIEXPLO'],
            [
                'code_id' => 15,
                'name' => 'Chemical Explosion',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACNUCLEEXPLO'],
            [
                'code_id' => 15,
                'name' => 'Nuclear Explosion',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACELECTREXPLO'],
            [
                'code_id' => 15,
                'name' => 'Electrical Explosion',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACDOGBITE'],
            [
                'code_id' => 15,
                'name' => 'Dog Bite',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACINSCTBITE'],
            [
                'code_id' => 15,
                'name' => 'Insect Bite',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACVENOM'],
            [
                'code_id' => 15,
                'name' => 'Venom',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCDGFUM'],
            [
                'code_id' => 16,
                'name' => 'Dust, Gases, or Fumes',
            ]);

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PRTUNDP'],
            [
                'code_id' => 22,
                'name' =>'Underpayments (Arrears)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCNOISE'],
            [
                'code_id' => 16,
                'name' => 'Noise',
            ]);

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PRTOVEP'],
            [
                'code_id' => 22,
                'name' =>'Overpayment (Deductions)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCTSUBP'],
            [
                'code_id' => 16,
                'name' => 'Toxic Substances (Poisons)',
            ]);

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PRTUNCLP'],
            [
                'code_id' => 22,
                'name' =>'Unclaimed Payments',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCVIBRT'],
            [
                'code_id' => 16,
                'name' => 'Vibration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCRADTN'],
            [
                'code_id' => 16,
                'name' => 'Radiation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCIGVIR'],
            [
                'code_id' => 16,
                'name' => 'Infectious Germs or Viruses',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCEHCTEM'],
            [
                'code_id' => 16,
                'name' => 'Extreme Hot or Cold Temperatures',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DCEHLAP'],
            [
                'code_id' => 16,
                'name' => 'Extremely High or Low Air Pressure',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPISPIN'],
            [
                'code_id' => 2,
                'name' => 'Spine Injury',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'BPIPTRIN'],
            [
                'code_id' => 2,
                'name' => 'Penetrating Truck Injury',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AEOVREX'],
            [
                'code_id' => 17,
                'name' => 'Overexertion',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AEOVREX'],
            [
                'code_id' => 17,
                'name' => 'Slips, Trips, and Falls',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AEPOHKP'],
            [
                'code_id' => 17,
                'name' => 'Poor Housekeeping',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AETKSHCT'],
            [
                'code_id' => 17,
                'name' => 'Taking Shortcuts',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DELEPOIS'],
            [
                'code_id' => 18,
                'name' => 'Diseases caused by chemical agents',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DERADTN'],
            [
                'code_id' => 18,
                'name' => 'Diseases caused by physical agents',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DECTSYN'],
            [
                'code_id' => 18,
                'name' => 'Biological agents and infectious or parasitic diseases',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DECVISYN'],
            [
                'code_id' => 18,
                'name' => 'Respiratory diseases',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DESKNDIS'],
            [
                'code_id' => 18,
                'name' => 'Skin diseases',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DEMUSDISO'],
            [
                'code_id' => 18,
                'name' => 'Musculoskeletal disorders',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DEMEBEHDIS'],
            [
                'code_id' => 18,
                'name' => 'Mental and behavioural disorders',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DEOCCPCANC'],
            [
                'code_id' => 18,
                'name' => 'Occupational cancer',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DTEBOWRK'],
            [
                'code_id' => 19,
                'name' => 'Boiler Workers',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DTEFRFGHT'],
            [
                'code_id' => 19,
                'name' => 'Firefighters',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DTEINSLT'],
            [
                'code_id' => 19,
                'name' => 'Insulation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'LBPNAIL'],
            [
                'code_id' => 20,
                'name' => 'Nail',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'LBPEYE'],
            [
                'code_id' => 20,
                'name' => 'Eye',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'LBPPALM'],
            [
                'code_id' => 20,
                'name' => 'Palm',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'LBPKNEE'],
            [
                'code_id' => 20,
                'name' => 'Knee',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'LBPLEG'],
            [
                'code_id' => 20,
                'name' => 'Leg',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSCSYSREJ'],
            [
                'code_id' => 23,
                'name' => 'Closed Rejected Incident By System',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NSCAPPRREJ'],
            [
                'code_id' => 23,
                'name' => 'Closed Rejection Incident from Notification Approval',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 9,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ABBRSN'],
            [
                'code_id' => 3,
                'name' => 'Abbrasion',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CRSHWOND'],
            [
                'code_id' => 3,
                'name' => 'Crush Wound',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CRSHNGS'],
            [
                'code_id' => 3,
                'name' => 'Crushings',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DGBITE'],
            [
                'code_id' => 3,
                'name' => 'Dog Bite',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EFFCORADTN'],
            [
                'code_id' => 3,
                'name' => 'Effects Of Radiations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EFFWEARC'],
            [
                'code_id' => 3,
                'name' => 'Effects Of Weather Exposure, And Related Conditions',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ENUCLETN'],
            [
                'code_id' => 3,
                'name' => 'Enucleations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INSCTBIT'],
            [
                'code_id' => 3,
                'name' => 'Insect Bite',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'LACRTION'],
            [
                'code_id' => 3,
                'name' => 'Laceration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PIERCNG'],
            [
                'code_id' => 3,
                'name' => 'Piercing',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'POISONG'],
            [
                'code_id' => 3,
                'name' => 'Poisonings',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NISTINJRY'],
            [
                'code_id' => 3,
                'name' => 'Soft Tissue Injury',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NISPASTR'],
            [
                'code_id' => 3,
                'name' => 'Sprains And Strains',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NISTABNG'],
            [
                'code_id' => 3,
                'name' => 'Stabbing',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NISUPRINJR'],
            [
                'code_id' => 3,
                'name' => 'Superficial Injuries',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIVENOM'],
            [
                'code_id' => 3,
                'name' => 'Venom',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NIVISCEINJR'],
            [
                'code_id' => 3,
                'name' => 'Visceral Injuries',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ACTHGSROBBRY'],
            [
                'code_id' => 15,
                'name' => 'Thugs, Robbery',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFFWM'],
            [
                'code_id' => 25,
                'name' => 'Not Cooperating',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLACKNOWLGMNT'],
            [
                'code_id' => 26,
                'name' => 'Notification Acknowledgement Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLBNAWLTTR'],
            [
                'code_id' => 26,
                'name' => 'Benefit Award Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLRMNDRLETTER'],
            [
                'code_id' => 26,
                'name' => 'Notification Reminder Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OLLLTRCRTD'],
            [
                'code_id' => 28,
                'name' => 'Letter Created',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OLLLTREDTD'],
            [
                'code_id' => 28,
                'name' => 'Letter Edited',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OLLLTRPRNTD'],
            [
                'code_id' => 28,
                'name' => 'Letter Printed',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OLLLTROPEND'],
            [
                'code_id' => 28,
                'name' => 'Letter Opened',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFMPP'],
            [
                'code_id' => 25,
                'name' => 'Make partial payment',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFSTA'],
            [
                'code_id' => 25,
                'name' => 'Settle the outstanding amount',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFPIBI'],
            [
                'code_id' => 25,
                'name' => 'Request to pay by installment (MoU)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFPVWO'],
            [
                'code_id' => 25,
                'name' => 'Visit WCF office',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLINNOLTR'],
            [
                'code_id' => 27,
                'name' => 'Inspection Notice Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLDNDNOLTR'],
            [
                'code_id' => 27,
                'name' => 'Demand Notice Letter (Inspection)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLLTRFCMPLNC'],
            [
                'code_id' => 27,
                'name' => 'Letter of Compliance (Inspection)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLEMPRMDLTR'],
            [
                'code_id' => 27,
                'name' => 'Employer Reminder Letter (Inspection)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLRFNDOVRPYMNT'],
            [
                'code_id' => 27,
                'name' => 'Refund Overpayment Confirmation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLEMPMOUAGRMNT'],
            [
                'code_id' => 27,
                'name' => 'Employer MoU Payment Agreement',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLPYINSTCONF'],
            [
                'code_id' => 27,
                'name' => 'Payment by Installment Confirmation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ITROUTINE'],
            [
                'code_id' => 29,
                'name' => 'Routine',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ITBLITZ'],
            [
                'code_id' => 29,
                'name' => 'Blitz',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ITSPECIAL'],
            [
                'code_id' => 29,
                'name' => 'Special',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ITRGPLNDINSPCTN'],
            [
                'code_id' => 30,
                'name' => 'Planned Inspection',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ITRGSPCEVNT'],
            [
                'code_id' => 30,
                'name' => 'Specific Event',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ITRGTRGBSYST'],
            [
                'code_id' => 30,
                'name' => 'Triggered By System',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SICUNRGRDEMPLY'],
            [
                'code_id' => 31,
                'name' => 'Frequent Change on Number of Employees',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SICNCONTREMPLY'],
            [
                'code_id' => 31,
                'name' => 'Non Contributing Employer',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SICERRNSCONTRBTN'],
            [
                'code_id' => 31,
                'name' => 'Erroneous Contribution',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RICMONTHLY'],
            [
                'code_id' => 32,
                'name' => 'Monthly',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RICQUARTRLY'],
            [
                'code_id' => 32,
                'name' => 'Quarterly',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RICYEARLY'],
            [
                'code_id' => 32,
                'name' => 'Yearly',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOPISPCRTD'],
            [
                'code_id' => 33,
                'name' => 'Inspection Created',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOPOISPPAW'],
            [
                'code_id' => 33,
                'name' => 'On Inspection Plan Approval Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOPAPPRVDIPLN'],
            [
                'code_id' => 33,
                'name' => 'Approved Inspection Plan, Planning Visit',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOPPNDGIASCDL'],
            [
                'code_id' => 33,
                'name' => 'Pending Inspection Assessment Schedule',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOPPLNVIDNLTR'],
            [
                'code_id' => 33,
                'name' => 'Issuing Inspection Response Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IRJDCLNDIPLN'],
            [
                'code_id' => 34,
                'name' => 'Declined Inspection Plan',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IOPCOMPLTDIPCN'],
            [
                'code_id' => 33,
                'name' => 'Completed Inspection',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ICNCANCLDISPPLN'],
            [
                'code_id' => 35,
                'name' => 'Cancelled Inspection Plan',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CDEMPLYIMSR'],
            [
                'code_id' => 36,
                'name' => 'Employer Inspection Master',
                'is_mandatory' => 1,
                'description' => "Compliance officer responsible for creating, reviewing and initiating inspection",
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CDAUTHRSDINSPCTR'],
            [
                'code_id' => 36,
                'name' => 'Authorised Inspectors',
                'is_mandatory' => 1,
                'description' => "Compliance officer who can visit employer for inspection",
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'NDNTFCTCUSR'],
            [
                'code_id' => 37,
                'name' => 'Notification Checklist Users',
                'is_mandatory' => 1,
                'description' => "Claim officers who has been assigned responsibility to manage notification files",
                'sort' => NULL,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPWAISPLAP'],
            [
                'code_id' => 38,
                'name' => 'Waiting Inspection Plan Approval',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPPLVDAT'],
            [
                'code_id' => 38,
                'name' => 'Planing Visit Date',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPISINNTLT'],
            [
                'code_id' => 38,
                'name' => 'Issuing Inspection Notice Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPINNTLTCRTD'],
            [
                'code_id' => 38,
                'name' => 'Created Inspection Notice Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPWTIASCHD'],
            [
                'code_id' => 38,
                'name' => 'Inspection Notice Letter Sent',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPVDPWTIASCHD'],
            [
                'code_id' => 38,
                'name' => 'Visit Date Postponed, Waiting Inspection Assessment Schedule',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPUPIASSC'],
            [
                'code_id' => 38,
                'name' => 'Uploaded Inspection Assessment Schedule',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 7,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPOIASCHRVW'],
            [
                'code_id' => 38,
                'name' => 'On Inspection Assessment Schedule Review Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 8,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPCMIASCRVW'],
            [
                'code_id' => 38,
                'name' => 'Completed Inspection Assessment Schedule Review',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 9,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPISSIRESLTR'],
            [
                'code_id' => 38,
                'name' => 'Issuing Inspection Response Letter',
                'is_mandatory' => 1,
                'description' => "Issuing Demand Notice Letter or Letter of Compliance to employer",
                'sort' => 10,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPIRESLTRPRTLCRTD'],
            [
                'code_id' => 38,
                'name' => 'Partially Created Inspection Response Letter',
                'is_mandatory' => 1,
                'description' => "Demand Notice Letter or Letter of Compliance to employer partially created",
                'sort' => 11,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPIRESLTRCRTD'],
            [
                'code_id' => 38,
                'name' => 'Created Inspection Response Letter',
                'is_mandatory' => 1,
                'description' => "Demand Notice Letter or Letter of Compliance to employer created",
                'sort' => 12,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPISUEDIRSLR'],
            [
                'code_id' => 38,
                'name' => 'Inspection Response Letter Sent',
                'is_mandatory' => 1,
                'description' => "Demand Notice Letter or Letter of Compliance to employer has been sent",
                'sort' => 13,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPISSREMLTR'],
            [
                'code_id' => 38,
                'name' => 'Issuing Reminder Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 14,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPREMLTRCRTD'],
            [
                'code_id' => 38,
                'name' => 'Created Reminder Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 15,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPREMLTRSNT'],
            [
                'code_id' => 38,
                'name' => 'Reminder Letter Sent',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 16,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPDFLTDEMPLY'],
            [
                'code_id' => 38,
                'name' => 'Defaulted Employer',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 17,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPODFTDEMWFW'],
            [
                'code_id' => 38,
                'name' => 'On Defaulted Employer Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 18,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPOINSTPRWFW'],
            [
                'code_id' => 38,
                'name' => 'On Instalment Payment Request Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 19,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPEPBYINSTLMNT'],
            [
                'code_id' => 38,
                'name' => 'Employer Paying By Installment',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 20,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPEMPPDOUTSTNDN'],
            [
                'code_id' => 38,
                'name' => 'Employer Paid Outstanding',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 21,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPWTHOVRPYMNT'],
            [
                'code_id' => 38,
                'name' => 'With Overpayment (Needs Refund)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 22,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITOROVRWKFLW'],
            [
                'code_id' => 38,
                'name' => 'On Overpayment Refunding Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 23,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITEMPLYREFOVP'],
            [
                'code_id' => 38,
                'name' => 'Employer Overpayment Refunded',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 24,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITPCLSDEMPINSPCTN'],
            [
                'code_id' => 38,
                'name' => 'Closed Employer Inspection',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 25,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITOCNEMPLYITSW'],
            [
                'code_id' => 39,
                'name' => 'On Cancelled Employer Inspection Task Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITCANEMPLYITS'],
            [
                'code_id' => 39,
                'name' => 'Cancelled Employer Inspection Task',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EITDCLNEMPLYITS'],
            [
                'code_id' => 39,
                'name' => 'Declined Employer Inspection Task',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OLPLTTRCTRTD'],
            [
                'code_id' => 40,
                'name' => 'Letter Created',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OLPPDINFUPDT'],
            [
                'code_id' => 40,
                'name' => 'Pending Dispatch Information Update',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OLPUPDTDDPINFRM'],
            [
                'code_id' => 40,
                'name' => 'Updated Dispatch Information',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHKCRITICAL'],
            [
                'code_id' => 41,
                'name' => 'Critical',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHKPHIGH'],
            [
                'code_id' => 41,
                'name' => 'High',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHKPMEDIUM'],
            [
                'code_id' => 41,
                'name' => 'Medium',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHKPLOW'],
            [
                'code_id' => 41,
                'name' => 'Low',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'FULETT'],
            [
                'code_id' => 9,
                'name' => 'Correspondence letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFEOP'],
            [
                'code_id' => 25,
                'name' => 'Enroll for Online Portal',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFAOA'],
            [
                'code_id' => 25,
                'name' => 'Adjustment of Arrears',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFCDC'],
            [
                'code_id' => 25,
                'name' => 'Confirmation of Date of Commencement',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFRDC'],
            [
                'code_id' => 25,
                'name' => 'Replacement of dishonoured cheques',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFCOMP'],
            [
                'code_id' => 25,
                'name' => 'Compliment',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFOTH'],
            [
                'code_id' => 25,
                'name' => 'Others',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SEFOTH'],
            [
                'code_id' => 25,
                'name' => 'Others',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DRLETTER'],
            [
                'code_id' => 42,
                'name' => 'Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DRNOTIFCNINCDNT'],
            [
                'code_id' => 42,
                'name' => 'Notification & Claim',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'DRCLMBNFT'],
            [
                'code_id' => 42,
                'name' => 'Claim Benefit',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMTCNAME'],
            [
                'code_id' => 43,
                'name' => 'Employer Name',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMTCADDR'],
            [
                'code_id' => 43,
                'name' => 'Employer Address',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMTCREGA'],
            [
                'code_id' => 43,
                'name' => 'Registration Authority',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMNCTME'],
            [
                'code_id' => 44,
                'name' => 'Merger/Acquisition',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );



        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMNCTSPL'],
            [
                'code_id' => 44,
                'name' => 'Splitting',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMNCTNA'],
            [
                'code_id' => 44,
                'name' => 'Name Change',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMNCTRN'],
            [
                'code_id' => 44,
                'name' => 'Transfer',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 0,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMNCOTH'],
            [
                'code_id' => 44,
                'name' => 'Other',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 0,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RABRELA'],
            [
                'code_id' => 45,
                'name' => 'BRELA',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RATRA'],
            [
                'code_id' => 45,
                'name' => 'TRA',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RAMHA'],
            [
                'code_id' => 45,
                'name' => 'Ministry of Home Affairs',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RAMOE'],
            [
                'code_id' => 45,
                'name' => 'Ministry of Education',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'RAMHSW'],
            [
                'code_id' => 45,
                'name' => 'Ministry of Health & Social Welfare',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMTCCONT'],
            [
                'code_id' => 43,
                'name' => 'Contact Change',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMTCCONT'],
            [
                'code_id' => 43,
                'name' => 'Contact Change',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMTCGEN'],
            [
                'code_id' => 43,
                'name' => 'General Info Change',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTLARGCO'],
            [
                'code_id' => 11,
                'name' => 'Large Employers Contribution',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTMEDIO'],
            [
                'code_id' => 11,
                'name' => 'Medium Employers Contribution',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTSMALCO'],
            [
                'code_id' => 11,
                'name' => 'Small Employers Contribution',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTPUBCO'],
            [
                'code_id' => 11,
                'name' => 'Public Employers Contribution',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ATTPRIVCO'],
            [
                'code_id' => 11,
                'name' => 'Private Employers Contribution',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'IUCNOTFCTN'],
            [
                'code_id' => 46,
                'name' => 'Notification & Claim Checklist',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLIEMPCLOSURE'],
            [
                'code_id' => 27,
                'name' => 'Employer Closure Response Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
//        $this->enableForeignKeys();

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PRAROCH'],
            [
                'code_id' => 22,
                'name' =>'Arrears for Eligible Overage Child',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLIEMPCLOSSETT'],
            [
                'code_id' => 27,
                'name' => 'Employer Closure - Debt Settlement',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ETCLPERMSETTL'],
            [
                'code_id' => 49,
                'name' => 'Permanent Settlement De-Registration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ETCLDORMANT'],
            [
                'code_id' => 49,
                'name' => 'Dormant De-Registration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'WAUARCHWRK'],
            [
                'code_id' => 48,
                'name' => 'Un-Archive Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECSINSPECTN'],
            [
                'code_id' => 50,
                'name' => 'Inspection',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECSEMREQST'],
            [
                'code_id' => 50,
                'name' => 'Employer Request',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECSEIDBYSTF'],
            [
                'code_id' => 50,
                'name' => 'Identified by Staff',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPRQSTCRTD'],
            [
                'code_id' => 51,
                'name' => 'Request Created',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPUPLDNFRM'],
            [
                'code_id' => 51,
                'name' => 'Uploading Forms',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPWTNCNTRRM'],
            [
                'code_id' => 51,
                'name' => 'Waiting Contribution Remittance',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPREADFRAPVL'],
            [
                'code_id' => 51,
                'name' => 'Ready for Approval',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPOENCNTWRK'],
            [
                'code_id' => 51,
                'name' => 'On Erroneous Contribution Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ECPCLSDRQST'],
            [
                'code_id' => 51,
                'name' => 'Closed Erroneous Contribution Request',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDSUTYFRMEM'],
            [
                'code_id' => 49,
                'name' => 'Employer Request (Default)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGBOT'],
            [
                'code_id' => 52,
                'name' => 'BOT',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGRIT'],
            [
                'code_id' => 52,
                'name' => 'RITA',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGWCF'],
            [
                'code_id' => 52,
                'name' => 'WCF (Internal)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CARNTRCDMI'],
            [
                'code_id' => 53,
                'name' => 'Not Reached MMI',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CARINCMPLTDOC'],
            [
                'code_id' => 53,
                'name' => 'Incomplete Document',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CARINSFCNTINF'],
            [
                'code_id' => 53,
                'name' => 'Insufficient Information',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CARIMPRTASSMT'],
            [
                'code_id' => 53,
                'name' => 'Impairment Assessment',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CARMEDCADVPAN'],
            [
                'code_id' => 53,
                'name' => 'Medical Advisory Panel',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CARFRADLNTCLM'],
            [
                'code_id' => 53,
                'name' => 'Fraudulent Claim',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST01'],
            [
                'code_id' => 54,
                'name' => 'Paid',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST01'],
            [
                'code_id' => 54,
                'name' => 'Paid',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST02'],
            [
                'code_id' => 54,
                'name' => 'Rejected/Closed',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST03'],
            [
                'code_id' => 54,
                'name' => 'DG',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST04'],
            [
                'code_id' => 54,
                'name' => 'DAS',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST05'],
            [
                'code_id' => 54,
                'name' => 'DFPI',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST06'],
            [
                'code_id' => 54,
                'name' => 'DO',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST07'],
            [
                'code_id' => 54,
                'name' => 'CADM',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST08'],
            [
                'code_id' => 54,
                'name' => 'PCADO',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST09'],
            [
                'code_id' => 54,
                'name' => 'CADO',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST10'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & contribution only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST11'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & Medical & contribution only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST12'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & Medical & police only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST13'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & Medical & police & contribution only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST14'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & Medical only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST15'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & police & contribution only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST16'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & Police report only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST17'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & succession document & contribution & police only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST18'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & succession document & contribution only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST19'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & succession document & police only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST20'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin & succession document only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST21'],
            [
                'code_id' => 54,
                'name' => 'Missing Admin documents only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST22'],
            [
                'code_id' => 54,
                'name' => 'Missing contribution & police report only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST23'],
            [
                'code_id' => 54,
                'name' => 'Missing Contribution only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST24'],
            [
                'code_id' => 54,
                'name' => 'Missing Medical and police report only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST25'],
            [
                'code_id' => 54,
                'name' => 'Missing Medical & contribution only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST26'],
            [
                'code_id' => 54,
                'name' => 'Missing Medical & police report only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST27'],
            [
                'code_id' => 54,
                'name' => 'Missing medical & Contribution & Police report only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST28'],
            [
                'code_id' => 54,
                'name' => 'Missing Medical reports only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST29'],
            [
                'code_id' => 54,
                'name' => 'Missing Police reports only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST30'],
            [
                'code_id' => 54,
                'name' => 'Missing succession & police report only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTST31'],
            [
                'code_id' => 54,
                'name' => 'Missing succession document only',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTRERS01'],
            [
                'code_id' => 55,
                'name' => 'Not Work related',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTRERS02'],
            [
                'code_id' => 55,
                'name' => 'Fall outside statutory period',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTRERS03'],
            [
                'code_id' => 55,
                'name' => 'Submitted outside 12 months from the date of incident',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'MANNOTRERS04'],
            [
                'code_id' => 55,
                'name' => 'Duplicate of another registered Claim',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'FLUTYP01'],
            [
                'code_id' => 56,
                'name' => 'Manual Notification Register',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGMINAF'],
            [
                'code_id' => 52,
                'name' => 'Ministry of Home Affairs',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGMINHEA'],
            [
                'code_id' => 52,
                'name' => 'Ministry of Health',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PSCVERIF'],
            [
                'code_id' => 21,
                'name' =>'Verification',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        //Cause Cause Of Accident According to Agency

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAASE'],
            [
                'code_id' => 57,
                'name' => 'Steam engines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAICE'],
            [
                'code_id' => 57,
                'name' => 'Internal combustion engines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAATS'],
            [
                'code_id' => 57,
                'name' => 'Transmission shafts',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAPP'],
            [
                'code_id' => 57,
                'name' => 'Power presses',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAAW'],
            [
                'code_id' => 57,
                'name' => 'Abrasive wheels',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAFM'],
            [
                'code_id' => 57,
                'name' => 'Forging machines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAACS'],
            [
                'code_id' => 57,
                'name' => 'Circular saws',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAOS'],
            [
                'code_id' => 57,
                'name' => 'Other saws',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAOP'],
            [
                'code_id' => 57,
                'name' => 'Overhand planes',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAUC'],
            [
                'code_id' => 57,
                'name' => 'Under-cutters',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAA191'],
            [
                'code_id' => 57,
                'name' => '191 Earth-moving machines, excavating and scraping machines, except means of transport',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAA192'],
            [
                'code_id' => 57,
                'name' => '192 Spinning, weaving and other textile machines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAA193'],
            [
                'code_id' => 57,
                'name' => '193 Machines for the manufacture of foodstuffs and beverages',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAA194'],
            [
                'code_id' => 57,
                'name' => '194 Machines for the manufacture of paper',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAA195'],
            [
                'code_id' => 57,
                'name' => '195 Printing machines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAALE'],
            [
                'code_id' => 57,
                'name' => 'Lifts and elevators',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAPB'],
            [
                'code_id' => 57,
                'name' => 'Pulley blocks',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAIUR'],
            [
                'code_id' => 57,
                'name' => 'Inter-urban railways',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAART'],
            [
                'code_id' => 57,
                'name' => 'Rail transport in mines, tunnels, quarries, industrial establishments, docks, etc.',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMV'],
            [
                'code_id' => 57,
                'name' => 'Motor vehicles, not elsewhere classified',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAADV'],
            [
                'code_id' => 57,
                'name' => 'Animal-drawn vehicles',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAHDV'],
            [
                'code_id' => 57,
                'name' => 'Hand-drawn vehicles',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMAT'],
            [
                'code_id' => 57,
                'name' => 'Means of air transport',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMMWT'],
            [
                'code_id' => 57,
                'name' => 'Motorized means of water transport',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAANMMWT'],
            [
                'code_id' => 57,
                'name' => 'Non-motorized means of water transport',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAACC'],
            [
                'code_id' => 57,
                'name' => 'Cable-cars',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMC'],
            [
                'code_id' => 57,
                'name' => 'Mechanical conveyors, except cable-cars',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAB'],
            [
                'code_id' => 57,
                'name' => 'Boilers',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAPC'],
            [
                'code_id' => 57,
                'name' => 'Pressurized container',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAPPA'],
            [
                'code_id' => 57,
                'name' => 'Pressurized piping and accessories',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAGC'],
            [
                'code_id' => 57,
                'name' => 'Gas cylinders',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAABF'],
            [
                'code_id' => 57,
                'name' => 'Blast furnaces',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAARF'],
            [
                'code_id' => 57,
                'name' => 'Refining furnaces',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 0,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAOF'],
            [
                'code_id' => 57,
                'name' => 'Other furnaces',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 0,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAK'],
            [
                'code_id' => 57,
                'name' =>'Kilns',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 0,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAO'],
            [
                'code_id' => 57,
                'name' => 'Ovens',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAACA'],
            [
                'code_id' => 57,
                'name' => 'Control apparatus',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAEHT'],
            [
                'code_id' => 57,
                'name' => 'Electric hand tools',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAPDT'],
            [
                'code_id' => 57,
                'name' => 'Power-driven hand tools, except electric hand tools',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAHT'],
            [
                'code_id' => 57,
                'name' => 'Hand tools, not power-driven',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAALMR'],
            [
                'code_id' => 57,
                'name' => 'Ladders, mobile ramps',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMM'],
            [
                'code_id' => 57,
                'name' => 'Moulding machines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAOE'],
            [
                'code_id' => 57,
                'name' => 'Other equipment, not elsewhere classified',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAE'],
            [
                'code_id' => 57,
                'name' => 'Explosives',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAD'],
            [
                'code_id' => 57,
                'name' => 'Dusts',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAGVF'],
            [
                'code_id' => 57,
                'name' => 'Gases, vapours, fumes',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAALEC'],
            [
                'code_id' => 57,
                'name' => 'Liquids not elsewhere classified',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAACEC'],
            [
                'code_id' => 57,
                'name' => 'Chemicals not elsewhere classified',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 0,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAFF'],
            [
                'code_id' => 57,
                'name' => 'Flying fragments',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 0,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAATR'],
            [
                'code_id' => 57,
                'name' =>'Ionizing radiations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 0,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAOMS'],
            [
                'code_id' => 57,
                'name' => 'Other materials and substances not elsewhere classified',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAATWS'],
            [
                'code_id' => 57,
                'name' => 'Traffic and working surfaces',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAACQ'],
            [
                'code_id' => 57,
                'name' => 'Confined quarters',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAOTWS'],
            [
                'code_id' => 57,
                'name' => 'Other traffic and working surfaces',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAFOWO'],
            [
                'code_id' => 57,
                'name' => 'Floor openings and wall openings',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAEF'],
            [
                'code_id' => 57,
                'name' => 'Environmental factors (lighting, ventilation, temperature, noise, etc.)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAARFMRT'],
            [
                'code_id' => 57,
                'name' => 'Roofs and faces of mine roads and tunnels, etc.',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAFMRT'],
            [
                'code_id' => 57,
                'name' => 'Floors of mine roads and tunnels, etc.',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAWFM'],
            [
                'code_id' => 57,
                'name' => 'Working faces of mines, tunnels, etc.',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAS'],
            [
                'code_id' => 57,
                'name' => 'Stairs',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMS'],
            [
                'code_id' => 57,
                'name' => 'Mine shafts',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAOA'],
            [
                'code_id' => 57,
                'name' => 'Other agencies, not elsewhere classified',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAALA'],
            [
                'code_id' => 57,
                'name' => 'Live animals',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 0,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAAP'],
            [
                'code_id' => 57,
                'name' => 'Animal products',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 0,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAAC'],
            [
                'code_id' => 57,
                'name' =>'Agencies not classified for lack of sufficient data',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 0,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAF'],
            [
                'code_id' => 57,
                'name' =>'Fire',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 0,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAW'],
            [
                'code_id' => 57,
                'name' => 'Underground Water',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAL'],
            [
                'code_id' => 57,
                'name' => 'Lorries',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAR'],
            [
                'code_id' => 57,
                'name' => 'Refrigerating plants',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAARM'],
            [
                'code_id' => 57,
                'name' => 'Rotating machines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAC'],
            [
                'code_id' => 57,
                'name' => 'Conductors',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAT'],
            [
                'code_id' => 57,
                'name' => 'Transformers',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 0,
            ]
        );

        //Causes of accident according to type of accident

        //OSH or Prevention checklist
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACOHSA'],
            [
                'code_id' => 59,
                'name' => 'Occupational Health and Safety Administration',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACADORR'],
            [
                'code_id' => 59,
                'name' => 'Accident and Dangerous Occurence Reporting and Recording',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACT'],
            [
                'code_id' => 59,
                'name' => 'Training',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACERG'],
            [
                'code_id' => 59,
                'name' => 'Ergonomics',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACHW'],
            [
                'code_id' => 59,
                'name' => 'Health and Welfare',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACPPE'],
            [
                'code_id' => 59,
                'name' => 'Personal Protective Equipment (PPE)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACMUM'],
            [
                'code_id' => 59,
                'name' => 'Machine Use And Maintainance',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACCHEM'],
            [
                'code_id' => 59,
                'name' => 'Chemicals',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'OACEPRE'],
            [
                'code_id' => 59,
                'name' => 'Emergency Preparedness',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 7,
                'is_active' => 1,

            ]
        );

        //Investment types
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTFDR'],
            [
                'code_id' => 60,
                'name' => 'Fixed Deposit',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTTBILLS'],
            [
                'code_id' => 60,
                'name' => 'Treasury Bills',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTTBONDS'],
            [
                'code_id' => 60,
                'name' => 'Treasury Bonds',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTCBONDS'],
            [
                'code_id' => 60,
                'name' => 'Corporate Bonds',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTEQUITIES'],
            [
                'code_id' => 60,
                'name' => 'Equities',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTCIS'],
            [
                'code_id' => 60,
                'name' => 'Licensed Collective Scheme',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTRESTATE'],
            [
                'code_id' => 60,
                'name' => 'Real Estate',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 7,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTLOAN'],
            [
                'code_id' => 60,
                'name' => 'Loans',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 8,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTCA'],
            [
                'code_id' => 60,
                'name' => 'Call Account',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 9,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGTRA'],
            [
                'code_id' => 52,
                'name' => 'TRA',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGBREL'],
            [
                'code_id' => 52,
                'name' => 'BRELA',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 7,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'AUSDREGOTHE'],
            [
                'code_id' => 52,
                'name' => 'Other Source',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 8,
                'is_active' => 1,
            ]
        );



        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYPFAPER'],
            [
                'code_id' => 58,
                'name' => 'Falls of persons',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYPSTRFO'],
            [
                'code_id' => 58,
                'name' => 'Struck by falling objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYPSTSTK'],
            [
                'code_id' => 58,
                'name' => 'Stepping on, striking against or struck by objects excluding falling objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYPCBTO'],
            [
                'code_id' => 58,
                'name' => 'Caught in or between objects',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 4,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYOVMOV'],
            [
                'code_id' => 58,
                'name' => 'Overexertion or strenuous movements',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 5,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYEXPTEM'],
            [
                'code_id' => 58,
                'name' => 'Exposure to or contact with extreme temperatures',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 6,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYEXPELC'],
            [
                'code_id' => 58,
                'name' => 'Exposure to or contact with electric current',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 7,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYEXPHRM'],
            [
                'code_id' => 58,
                'name' => 'Exposure to or contact with harmful substances or radiations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 8,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYMTA'],
            [
                'code_id' => 58,
                'name' => 'MTA',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 9,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYTHROB'],
            [
                'code_id' => 58,
                'name' => 'Thugs, Robbery',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 10,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CATYOTHTA'],
            [
                'code_id' => 58,
                'name' => 'Other types of accident',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 11,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMACN'],
            [
                'code_id' => 57,
                'name' => 'Machines',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMSAR'],
            [
                'code_id' => 57,
                'name' => 'Materials, substances and radiations',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAMTLE'],
            [
                'code_id' => 57,
                'name' => 'Means of transport and lifting equipment',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CAAAWORKEN'],
            [
                'code_id' => 57,
                'name' => 'Working environment',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAARIFNASA'],
            [
                'code_id' => 62,
                'name' => 'Initiate Rejected Incident From Notification Approval Delays',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAAFRWVLDSA'],
            [
                'code_id' => 62,
                'name' => 'Forward for Notification Validation Delays',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAAPTCDFNVDTN'],
            [
                'code_id' => 62,
                'name' => 'PCADO to CADM Delays for Notification Validation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAACDMDANVDTN'],
            [
                'code_id' => 62,
                'name' => 'CADM Delays to Approve Notification Validation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAAHLSUDANV'],
            [
                'code_id' => 62,
                'name' => 'HLSU Delays to Approve Notification Validation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADASDANVDTN'],
            [
                'code_id' => 62,
                'name' => 'DAS Delays to Approve Notification Validation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAAINTBNFWKDLY'],
            [
                'code_id' => 62,
                'name' => 'Initiate Benefit Workflow Delays',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAAPTCDFBNFWRKLW'],
            [
                'code_id' => 62,
                'name' => 'PCADO to CADM Delays for Benefit Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLACDODLTCBNAWRDLTR'],
            [
                'code_id' => 62,
                'name' => 'CADO Delays to Create Benefit Award Letters',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAACDMDABNFWRKLW'],
            [
                'code_id' => 62,
                'name' => 'CADM Delays to Approve Benefit Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADASDABNFWRWPD'],
            [
                'code_id' => 62,
                'name' => 'DAS Delays to Approve Benefit Workflow (PD)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADASDABNFWRTDMSR'],
            [
                'code_id' => 62,
                'name' => 'DAS Delays to Approve Benefit Workflow (TD, MAE, Survivors)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADFPIDABNFWR'],
            [
                'code_id' => 62,
                'name' => 'DFPI Delays to Approve Benefit Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAAHLSUDABNFWR'],
            [
                'code_id' => 62,
                'name' => 'HLSU Delays to Approve Benefit Workflow',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADLYTCACLTR'],
            [
                'code_id' => 62,
                'name' => 'Delays to Create Acknowledgement Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAAINRJOTINDLY'],
            [
                'code_id' => 62,
                'name' => 'Initiate Rejected Out of Time Incident Delays',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADLYRONNTAPLCTN'],
            [
                'code_id' => 62,
                'name' => 'Delays to Review New Online Notification Application',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADLYAPVNTAPLCTN'],
            [
                'code_id' => 62,
                'name' => 'Delays to Approve Online Notification Application',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAARODLYDNTNLTR'],
            [
                'code_id' => 62,
                'name' => 'RO Delays to Dispatch Notification Letters',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'SLAADLYTCRMNDRLTR'],
            [
                'code_id' => 62,
                'name' => 'Delays to Create Reminder Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLIEMPCLREXT'],
            [
                'code_id' => 27,
                'name' => 'Employer Closure Extension Response Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTADRPAYDT'],
            [
                'code_id' => 61,
                'name' => 'Wrong receipt payment date',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTADRTRER'],
            [
                'code_id' => 61,
                'name' => 'Transfer Error',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INTADRINTER'],
            [
                'code_id' => 61,
                'name' => 'Internal Error',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLIEMPINTEADJ'],
            [
                'code_id' => 27,
                'name' => 'Employer Booking Interest Adjustment Response Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHACCRCLM'],
            [
                'code_id' => 14,
                'name' => 'Accrual Claim',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INVSTLP'],
            [
                'code_id' => 67,
                'name' => 'Telephone Investigation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INVPHY'],
            [
                'code_id' => 67,
                'name' => 'Physical Visit Investigation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INVSPL'],
            [
                'code_id' => 67,
                'name' => 'Special Investigation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INVINDVL'],
            [
                'code_id' => 68,
                'name' => 'Individual Investigation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INVGRP'],
            [
                'code_id' => 68,
                'name' => 'Group (General) Investigation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHINVPLAN'],
            [
                'code_id' => 14,
                'name' => 'Investigation Plan',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        /*New*/
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHCEMPDEREG'],
            [
                'code_id' => 14,
                'name' => 'Employer De-Registration Tasks',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMPDERREOP'],
            [
                'code_id' => 63,
                'name' => 'Employer De-Registered Re-Open Alert',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CDAEMPREOPUS'],
            [
                'code_id' => 36,
                'name' => 'Officers for Employer Temporary De-registrations Re Open Alert',
                'is_mandatory' => 1,
                'description' => "Officer to track Employer Temporary De-registration Re open alert",
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CHCPAYALERT'],
            [
                'code_id' => 14,
                'name' => 'Payroll Alert Tasks',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );



        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PAYDALEUS'],
            [
                'code_id' => 65,
                'name' => 'Officers for Payroll Alert Tasks',
                'is_mandatory' => 1,
                'description' => "Officers for Payroll Alert Tasks",
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        /*Payroll alert staging*/
        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALEPENAC'],
            [
                'code_id' => 64,
                'name' => 'Pensioners for Activation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALEDEPAC'],
            [
                'code_id' => 64,
                'name' => 'Dependents for Activation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALECHSALR'],
            [
                'code_id' => 64,
                'name' => 'Child Suspension Alerts',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALECHA18'],
            [
                'code_id' => 64,
                'name' => 'Child Approaching 18yrs',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALERETSU'],
            [
                'code_id' => 64,
                'name' => 'Retiree Suspensions',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALEPEDVER'],
            [
                'code_id' => 64,
                'name' => 'Pending Documents For Verification',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALEPEDBAN'],
            [
                'code_id' => 64,
                'name' => 'Pending Documents For Bank Modify',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PALEPEDDET'],
            [
                'code_id' => 64,
                'name' => 'Pending Documents For Details Modify',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PAYCHCOSCH'],
            [
                'code_id' => 66,
                'name' => 'School Continuation',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PAYCHCDIS'],
            [
                'code_id' => 66,
                'name' => 'Disability',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PSCCHDREI'],
            [
                'code_id' => 21,
                'name' =>'Child Reinstate (Extension)',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDULHIGH'],
            [
                'code_id' => 69,
                'name' =>'Higher Learning',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );



        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDULDEGR'],
            [
                'code_id' => 69,
                'name' =>'Degree',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDULDIPL'],
            [
                'code_id' => 69,
                'name' =>'Diploma',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PRFFSTLL'],
            [
                'code_id' => 70,
                'name' =>'Still Working',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PRFFRTPN'],
            [
                'code_id' => 70,
                'name' =>'Retired: Receiving Social Security Pension',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'PRFFRTNP'],
            [
                'code_id' => 70,
                'name' =>'Retired: Not Receiving Social Security Pension',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'ARREARCMT'],
            [
                'code_id' => 14,
                'name' =>'Arrears Commitments',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'CLIEMPPAYROLLMRG'],
            [
                'code_id' => 27,
                'name' => 'Employer Payroll Merge Response Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMCMMONTH'],
            [
                'code_id' => 72,
                'name' => 'Change Contribution Month',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 1,
                'is_active' => 1,

            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMCMRED'],
            [
                'code_id' => 72,
                'name' => 'Redistribute Contribution Amount',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 2,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMCMUNDER'],
            [
                'code_id' => 72,
                'name' => 'Underpayed Contribution',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'INSTMOUUPLOAD'],
            [
                'code_id' => 14,
                'name' => 'Instalment MoU Upload',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => 3,
                'is_active' => 1,

            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EMPCONTRIBMOD'],
            [
                'code_id' => 27,
                'name' => 'Employer Contribution Modification Response Letter',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDULPRIM'],
            [
                'code_id' => 69,
                'name' =>'Primary Education',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );


        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDULSEC'],
            [
                'code_id' => 69,
                'name' =>'Secondary Education',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );

        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDULTECH'],
            [
                'code_id' => 69,
                'name' =>'Technical Education',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );



        $code = CodeValue::query()->updateOrCreate(
            ['reference' => 'EDULCERT'],
            [
                'code_id' => 69,
                'name' =>'Certificate',
                'is_mandatory' => 1,
                'description' => NULL,
                'sort' => NULL,
                'is_active' => 1,
            ]
        );
    }

}