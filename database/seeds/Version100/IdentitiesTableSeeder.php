<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class IdentitiesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('identities');
        $this->delete('identities');
        
        \DB::table('identities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Voting card',
                'created_at' => '2017-04-18 17:52:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'National ID',
                'created_at' => '2017-04-18 17:52:00',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Driving Licence',
                'created_at' => '2017-04-18 17:52:12',
                'updated_at' => NULL,
            ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Passport',
                    'created_at' => '2017-10-08 17:52:12',
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'None',
                    'created_at' => '2017-10-08 17:52:12',
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('identities');
    }
}