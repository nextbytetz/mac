<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class OshAuditPeriodSeeder extends Seeder
{

	use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->disableForeignKeys('osh_audit_period');
    	$this->truncate('osh_audit_period');
    	\DB::table('osh_audit_period')->insert(array (
    		0 => 
    		array (
    			'fin_year_id' => 5,
    			'period'=> 1,
    			'fin_year' => '2019/2020',
    			'is_active' => 1,
    			'created_at' => '2020-03-30 12:03:21',
    			'updated_at' => NULL,

    		),
    		1 => 
    		array (
    			'fin_year_id' => 5,
    			'period'=> 2,
    			'fin_year' => '2019/2020',
    			'is_active' => 0,
    			'created_at' => '2020-03-30 12:03:21',
    			'updated_at' => NULL,
    		),
    		2 => 
    		array (
    			'fin_year_id' => 6,
    			'period'=> 1,
    			'fin_year' => '2020/2021',
    			'is_active' => 0,
    			'created_at' => '2020-03-30 12:03:21',
    			'updated_at' => NULL,
    		),
    		3 => 
    		array (
    			'fin_year_id' => 6,
    			'period'=> 2,
    			'fin_year' => '2020/2021',
    			'is_active' => 0,
    			'created_at' => '2020-03-30 12:03:21',
    			'updated_at' => NULL,
    		),
    		4 => 
    		array (
    			'fin_year_id' => 7,
    			'period'=> 1,
    			'fin_year' => '2021/2022',
    			'is_active' => 0,
    			'created_at' => '2020-03-30 12:03:21',
    			'updated_at' => NULL,
    		),
    		5 => 
    		array (
    			'fin_year_id' => 7,
    			'period'=> 2,
    			'fin_year' => '2021/2022',
    			'is_active' => 0,
    			'created_at' => '2020-03-30 12:03:21',
    			'updated_at' => NULL,
    		),
            6 => 
            array (
                'fin_year_id' => 4,
                'period'=> 1,
                'fin_year' => '2018/2019',
                'is_active' => 0,
                'created_at' => '2020-03-30 12:03:21',
                'updated_at' => NULL,
            ),

    	));

    	$this->enableForeignKeys('osh_audit_period');


    }
}
