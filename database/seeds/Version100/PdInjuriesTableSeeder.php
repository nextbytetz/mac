<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PdInjuriesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $count = \DB::table('pd_injuries')->limit(1)->count();

        if (!$count) {
            $this->disableForeignKeys('pd_injuries');
            $this->delete('pd_injuries');

            \DB::table('pd_injuries')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'name' => 'Hand/fingers',
                        'rank' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'name' => 'Vision',
                        'rank' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'name' => 'Knee',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'name' => 'Ankle',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'name' => 'Elbow',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'name' => 'Loss of grasping power',
                        'rank' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'name' => 'Wrist',
                        'rank' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'name' => 'Toe(s)',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'name' => 'Spine Thoracic',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'name' => 'General lower extremity',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'name' => 'Spine Lumbar',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'name' => 'Spine Cervical',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'name' => 'Hip',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'name' => 'General upper extremity',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'name' => 'Heart disease',
                        'rank' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'name' => 'General Abdominal',
                        'rank' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'name' => 'PT head syndrome',
                        'rank' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'name' => 'Lung disease',
                        'rank' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'name' => 'Shoulder',
                        'rank' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'name' => 'Hearing',
                        'rank' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'name' => 'Psychiatric',
                        'rank' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'name' => 'Impaired rib cage',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'name' => 'Cosmetic disfigurement',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'name' => 'General chest impairment',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'name' => 'Impaired mouth or jaw',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'name' => 'Speech impairment',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'name' => 'Impaired nose',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'name' => 'Impaired nervous system',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'name' => 'Vertigo',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'name' => 'Impaired smell',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'name' => 'Paralysis',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'name' => 'Mental deterioration',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'name' => 'Epilepsy',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'name' => 'Skull aperture',
                        'rank' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));

            $this->enableForeignKeys('pd_injuries');

$sql = <<<SQL
DROP TRIGGER IF EXISTS pd_injuries_ro ON pd_injuries;
create trigger pd_injuries_ro before insert or update or delete or truncate on pd_injuries for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);
        }
        
    }
}