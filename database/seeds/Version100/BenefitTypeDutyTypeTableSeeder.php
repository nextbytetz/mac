<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class BenefitTypeDutyTypeTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('benefit_type_duty_type');
        
        \DB::table('benefit_type_duty_type')->insert(array (
            0 => 
            array (
                'id' => '1',
                'benefit_type_id' => '8',
                'duty_type_id' => '1',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '2',
                'benefit_type_id' => '8',
                'duty_type_id' => '2',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '3',
                'benefit_type_id' => '7',
                'duty_type_id' => '3',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
        
    }
}