<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PaymentTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('payment_types');
        $this->delete('payment_types');
        
        \DB::table('payment_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Cash',
                'created_at' => '2017-04-18 07:46:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Cheque Payment',
                'created_at' => '2017-04-18 07:47:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Bank Slip',
                'created_at' => '2017-04-18 07:47:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Telegraphic Transfer (TT)',
                'created_at' => '2017-04-18 07:48:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Cheque Deposit',
                    'created_at' => '2017-04-18 07:48:23',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Bank Transfer',
                    'created_at' => '2017-04-18 07:48:23',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'name' => 'Mobile Payment',
                    'created_at' => '2017-04-18 07:48:23',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('payment_types');
    }
}