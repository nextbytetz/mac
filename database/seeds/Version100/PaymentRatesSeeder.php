<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;


class PaymentRatesSeeder extends Seeder
{

  use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     $this->disableForeignKeys("contribution_interest_rates");
     $this->delete('contribution_interest_rates');
     \DB::table('contribution_interest_rates')->insert([
        0 =>
        array (
            'id' => 1,
            'fin_code_id' => 2,
            'employer_category_cv_id' => 36,
            'rate' => 0.5,
            'start_date' => '2015-07-01',
            'end_date' => null,
            'user_id' => 62,
            'wf_status' => 2,
            'wf_done' => true,
            'wf_done_date' => '2015-07-01',
            'description' => 'Workers Compensation Act',
            'created_at' => '2015-07-01 00:00:00',
            'updated_at' => '2015-07-01 00:00:00',
            'deleted_at' => null,
            'parent_id' => null,
        ),

        1 =>
        array (
            'id' => 2,
            'fin_code_id' => 2,
            'employer_category_cv_id' => 37,
            'rate' => 1,
            'start_date' => '2015-07-01',
            'end_date' => '2021-06-30',
            'user_id' => 62,
            'wf_status' => 2,
            'wf_done' => true,
            'wf_done_date' => '2015-07-01',
            'description' => 'Workers Compensation Act',
            'created_at' => '2015-07-01 00:00:00',
            'updated_at' => '2015-07-01 00:00:00',
            'deleted_at' => null,
            'parent_id' => null,
        ),

        2 =>
        array (
            'id' => 3,
            'fin_code_id' => 1,
            'employer_category_cv_id' => 36,
            'rate' => 10,
            'start_date' => '2015-07-01',
            'end_date' => null,
            'user_id' => 62,
            'wf_status' => 2,
            'wf_done' => true,
            'wf_done_date' => '2015-07-01',
            'description' => 'Workers Compensation Act',
            'created_at' => '2015-07-01 00:00:00',
            'updated_at' => '2015-07-01 00:00:00',
            'deleted_at' => null,
            'parent_id' => null,
        ),

        3 =>
        array (
            'id' => 4,
            'fin_code_id' => 1,
            'employer_category_cv_id' => 37,
            'rate' => 10,
            'start_date' => '2015-07-01',
            'end_date' => null,
            'user_id' => 62,
            'wf_status' => 2,
            'wf_done' => true,
            'wf_done_date' => '2015-07-01',
            'description' => 'Workers Compensation Act',
            'created_at' => '2015-07-01 00:00:00',
            'updated_at' => '2015-07-01 00:00:00',
            'deleted_at' => null,
            'parent_id' => null,
        ),

        4 =>
        array (
            'id' => 5,
            'fin_code_id' => 2,
            'employer_category_cv_id' => 37,
            'rate' => 0.6,
            'start_date' => '2021-07-01',
            'end_date' => null,
            'user_id' => 62,
            'wf_status' => 2,
            'wf_done' => true,
            'wf_done_date' => '2021-07-01',
            'description' => 'GN 496G',
            'created_at' => '2015-07-01 00:00:00',
            'updated_at' => '2015-07-01 00:00:00',
            'deleted_at' => null,
            'parent_id' => 2,

        ),
    ]);
     $this->enableForeignKeys("contribution_interest_rates");

 }
}
