<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class AuditActionsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('audit_actions');
        
        \DB::table('audit_actions')->insert(array (
            0 => 
            array (
                'name' => 'Insert',
                'created_at' => '2017-04-18 12:08:34',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'name' => 'Delete',
                'created_at' => '2017-04-18 12:08:34',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'name' => 'Update',
                'created_at' => '2017-04-18 12:08:41',
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
    }
}