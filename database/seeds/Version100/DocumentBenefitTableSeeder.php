<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DocumentBenefitTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('document_benefits');
        $this->delete('document_benefits');

        \DB::table('document_benefits')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'document_id' => 27,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'document_id' => 4,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'document_id' => 7,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'document_id' => 26,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'document_id' => 6,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'document_id' => 10,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'document_id' => 55,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'document_id' => 56,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'document_id' => 54,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            9 =>
                array (
                    'id' => 10,
                    'document_id' => 4,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            10 =>
                array (
                    'id' => 11,
                    'document_id' => 6,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            11 =>
                array (
                    'id' => 12,
                    'document_id' => 27,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'document_id' => 26,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'document_id' => 8,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 10,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            14 =>
                array (
                    'id' => 15,
                    'document_id' => 30,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 16,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            15 =>
                array (
                    'id' => 16,
                    'document_id' => 30,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 16,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            16 =>
                array (
                    'id' => 17,
                    'document_id' => 58,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            17 =>
                array (
                    'id' => 18,
                    'document_id' => 59,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            18 =>
                array (
                    'id' => 19,
                    'document_id' => 60,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            19 =>
                array (
                    'id' => 20,
                    'document_id' => 30,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            20 =>
                array (
                    'id' => 21,
                    'document_id' => 58,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            21 =>
                array (
                    'id' => 22,
                    'document_id' => 59,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            22 =>
                array (
                    'id' => 23,
                    'document_id' => 60,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            23 =>
                array (
                    'id' => 24,
                    'document_id' => 30,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            24 =>
                array (
                    'id' => 25,
                    'document_id' => 58,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            25 =>
                array (
                    'id' => 26,
                    'document_id' => 59,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            26 =>
                array (
                    'id' => 27,
                    'document_id' => 60,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            27 =>
                array (
                    'id' => 28,
                    'document_id' => 30,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            28 =>
                array (
                    'id' => 29,
                    'document_id' => 58,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            29 =>
                array (
                    'id' => 30,
                    'document_id' => 59,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            30 =>
                array (
                    'id' => 31,
                    'document_id' => 60,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            31 =>
                array (
                    'id' => 32,
                    'document_id' => 30,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 17,
                    'member_type_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            32 =>
                array (
                    'id' => 33,
                    'document_id' => 17,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 7,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            33 =>
                array (
                    'id' => 34,
                    'document_id' => 19,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 7,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            34 =>
                array (
                    'id' => 35,
                    'document_id' => 21,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 7,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            35 =>
                array (
                    'id' => 36,
                    'document_id' => 17,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 7,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            36 =>
                array (
                    'id' => 37,
                    'document_id' => 19,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 7,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            37 =>
                array (
                    'id' => 38,
                    'document_id' => 21,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 7,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            38 =>
                array (
                    'id' => 39,
                    'document_id' => 17,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 8,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            39 =>
                array (
                    'id' => 40,
                    'document_id' => 19,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 8,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            40 =>
                array (
                    'id' => 41,
                    'document_id' => 21,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 8,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            41 =>
                array (
                    'id' => 42,
                    'document_id' => 17,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 8,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            42 =>
                array (
                    'id' => 43,
                    'document_id' => 19,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 8,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            43 =>
                array (
                    'id' => 44,
                    'document_id' => 21,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 8,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            44 =>
                array (
                    'id' => 45,
                    'document_id' => 17,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            45 =>
                array (
                    'id' => 46,
                    'document_id' => 5,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            46 =>
                array (
                    'id' => 47,
                    'document_id' => 19,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            47 =>
                array (
                    'id' => 48,
                    'document_id' => 20,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            48 =>
                array (
                    'id' => 49,
                    'document_id' => 21,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            49 =>
                array (
                    'id' => 50,
                    'document_id' => 18,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            50 =>
                array (
                    'id' => 51,
                    'document_id' => 5,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            51 =>
                array (
                    'id' => 52,
                    'document_id' => 17,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            52 =>
                array (
                    'id' => 53,
                    'document_id' => 19,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            53 =>
                array (
                    'id' => 54,
                    'document_id' => 20,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            54 =>
                array (
                    'id' => 55,
                    'document_id' => 21,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            55 =>
                array (
                    'id' => 56,
                    'document_id' => 18,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 14,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            56 =>
                array (
                    'id' => 57,
                    'document_id' => 5,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            57 =>
                array (
                    'id' => 58,
                    'document_id' => 17,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            58 =>
                array (
                    'id' => 59,
                    'document_id' => 19,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            59 =>
                array (
                    'id' => 60,
                    'document_id' => 20,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            60 =>
                array (
                    'id' => 61,
                    'document_id' => 21,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            61 =>
                array (
                    'id' => 62,
                    'document_id' => 18,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            62 =>
                array (
                    'id' => 63,
                    'document_id' => 5,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            63 =>
                array (
                    'id' => 64,
                    'document_id' => 17,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            64 =>
                array (
                    'id' => 65,
                    'document_id' => 19,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            65 =>
                array (
                    'id' => 66,
                    'document_id' => 20,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            66 =>
                array (
                    'id' => 67,
                    'document_id' => 21,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            67 =>
                array (
                    'id' => 68,
                    'document_id' => 18,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 15,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            68 =>
                array (
                    'id' => 69,
                    'document_id' => 5,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            69 =>
                array (
                    'id' => 70,
                    'document_id' => 17,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            70 =>
                array (
                    'id' => 71,
                    'document_id' => 19,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            71 =>
                array (
                    'id' => 72,
                    'document_id' => 20,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            72 =>
                array (
                    'id' => 73,
                    'document_id' => 21,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            73 =>
                array (
                    'id' => 74,
                    'document_id' => 18,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            74 =>
                array (
                    'id' => 75,
                    'document_id' => 5,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            75 =>
                array (
                    'id' => 76,
                    'document_id' => 17,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            76 =>
                array (
                    'id' => 77,
                    'document_id' => 19,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            77 =>
                array (
                    'id' => 78,
                    'document_id' => 20,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            78 =>
                array (
                    'id' => 79,
                    'document_id' => 21,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            79 =>
                array (
                    'id' => 80,
                    'document_id' => 18,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 3,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            80 =>
                array (
                    'id' => 81,
                    'document_id' => 5,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            81 =>
                array (
                    'id' => 82,
                    'document_id' => 17,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            82 =>
                array (
                    'id' => 83,
                    'document_id' => 19,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            83 =>
                array (
                    'id' => 84,
                    'document_id' => 20,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            84 =>
                array (
                    'id' => 85,
                    'document_id' => 21,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            85 =>
                array (
                    'id' => 86,
                    'document_id' => 18,
                    'incident_type_id' => 1,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            86 =>
                array (
                    'id' => 87,
                    'document_id' => 5,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            87 =>
                array (
                    'id' => 88,
                    'document_id' => 17,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            88 =>
                array (
                    'id' => 89,
                    'document_id' => 19,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            89 =>
                array (
                    'id' => 90,
                    'document_id' => 20,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            90 =>
                array (
                    'id' => 91,
                    'document_id' => 21,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            91 =>
                array (
                    'id' => 92,
                    'document_id' => 18,
                    'incident_type_id' => 2,
                    'benefit_type_id' => 4,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            92 =>
                array (
                    'id' => 93,
                    'document_id' => 5,
                    'incident_type_id' => 3,
                    'benefit_type_id' => 11,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            93 =>
                array (
                    'id' => 94,
                    'document_id' => 12,
                    'incident_type_id' => 3,
                    'benefit_type_id' => 11,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            94 =>
                array (
                    'id' => 95,
                    'document_id' => 13,
                    'incident_type_id' => 3,
                    'benefit_type_id' => 11,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            95 =>
                array (
                    'id' => 96,
                    'document_id' => 14,
                    'incident_type_id' => 3,
                    'benefit_type_id' => 11,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            96 =>
                array (
                    'id' => 97,
                    'document_id' => 46,
                    'incident_type_id' => 3,
                    'benefit_type_id' => 11,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            97 =>
                array (
                    'id' => 98,
                    'document_id' => 15,
                    'incident_type_id' => 3,
                    'benefit_type_id' => 11,
                    'member_type_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('document_benefits');

        (new \App\Repositories\Backend\Operation\Claim\DocumentBenefitRepository())->replicateForTransferToDeath();

    }

}