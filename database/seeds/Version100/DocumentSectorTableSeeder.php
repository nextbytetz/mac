<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DocumentSectorTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('document_sector');
        $this->delete('document_sector');

        \DB::table('document_sector')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'reference' => 'BSAFF',
                    'document_id' => 36,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'reference' => 'BSPSTA',
                    'document_id' => 34,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('document_sector');

    }
}
