<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class BenefitTypeGroupsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('benefit_type_groups');
        $this->delete('benefit_type_groups');
        
        \DB::table('benefit_type_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Short term benefits',
                'created_at' => '2017-04-19 11:14:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Long term benefits',
                'created_at' => '2017-04-19 11:14:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Medical Aid',
                'created_at' => '2017-04-19 11:14:35',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Employer Contribution',
                    'created_at' => '2017-04-19 11:14:35',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('benefit_type_groups');
    }
}