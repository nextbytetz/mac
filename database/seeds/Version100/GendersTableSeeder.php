<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class GendersTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('genders');
        $this->delete('genders');
        
        \DB::table('genders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Male',
                'created_at' => '2017-04-18 17:30:35',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Female',
                'created_at' => '2017-04-18 17:30:35',
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('genders');
    }
}