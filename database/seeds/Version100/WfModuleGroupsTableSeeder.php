<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class WfModuleGroupsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('wf_module_groups');
        $this->delete('wf_module_groups');

        \DB::table('wf_module_groups')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Interest Waiving',
                'table_name' => 'booking_interest_board_adjustments',
                'created_at' => '2017-04-18 15:29:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 0,
                'can_archive' => 0,
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Interest Adjustment (Single)',
                'table_name' => 'booking_interests',
                'created_at' => '2017-04-18 15:29:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),


            2 =>
            array (
                'id' => 3,
                'name' => 'Notification & Claim Processing',
                'table_name' => 'claims',
                'created_at' => '2017-04-18 15:30:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 1,
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Notification Rejection',
                'table_name' => 'notification_reports',
                'created_at' => '2017-04-18 15:30:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 1,
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Contribution Receipt',
                'table_name' => 'receipts',
                'created_at' => '2017-04-18 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 0,
                'can_archive' => 0,
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Employer Registration',
                'table_name' => 'registered_employers',
                'created_at' => '2017-09-12 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'Contribution Receipt Before Electronic',
                'table_name' => 'legacy_receipts',
                'created_at' => '2017-10-10 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 0,
                'can_archive' => 0,
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'Online Employer Verification',
                'table_name' => 'online_employer_verifications',
                'created_at' => '2017-10-10 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            8 =>
            array (
                'id' => 9,
                'name' => 'Interest Refund',
                'table_name' => 'booking_interest_refunds',
                'created_at' => '2018-01-10 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 0,
                'can_archive' => 0,
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'Payroll Run Processing',
                'table_name' => 'payroll_run_approvals',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'Payroll Bank Details Modification',
                'table_name' => 'payroll_bank_info_updates',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            11 =>
            array (
                'id' => 12,
                'name' => 'Payroll Status Change Approval',
                'table_name' => 'payroll_status_changes',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            12 =>
            array (
                'id' => 13,
                'name' => 'Payroll Recovery Processing',
                'table_name' => 'payroll_recoveries',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            13 =>
            array (
                'id' => 14,
                'name' => 'Payroll Reconciliation Processing',
                'table_name' => 'payroll_reconciliations',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            14 =>
            array (
                'id' => 15,
                'name' => 'Payroll Beneficiary Details modification',
                'table_name' => 'payroll_beneficiary_updates',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            15 =>
            array (
                'id' => 16,
                'name' => 'Employer Advance Payment',
                'table_name' => 'employer_advance_payments',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 0,
                'can_archive' => 0,
            ),
            16 =>
            array (
                'id' => 17,
                'name' => 'Online Notification Application',
                'table_name' => 'portal.incidents',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 0,
                'can_archive' => 0,
            ),

            17 =>
            array (
                'id' => 18,
                'name' => 'Employer Business De-Registration',
                'table_name' => 'employer_closures',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'Letter Issuance',
                'table_name' => 'letters',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'Employer Inspection Plan',
                'table_name' => 'inspections',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'Employer Inspection Task',
                'table_name' => 'inspections',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            21 =>
            array (
                'id' => 22,
                'name' => 'Employer De-Registration Re-Open',
                'table_name' => 'employer_closure_reopens',
                'created_at' => '2019-01-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            22 =>
            array (
                'id' => 23,
                'name' => 'Employer Particular Changes',
                'table_name' => 'employer_particular_changes',
                'created_at' => '2019-11-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            23 =>
            array (
                'id' => 24,
                'name' => 'Payroll Monthly Pension Updates',
                'table_name' => 'payroll_mp_updates',
                'created_at' => '2019-11-23 15:30:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            24 =>
            array (
                'id' => 25,
                'name' => 'Employer Return of Earnings',
                'table_name' => 'employer_return_earnings',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            25 =>
            array (
                'id' => 26,
                'name' => 'Contribution Receivables',
                'table_name' => 'receipt_receivables',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            26 =>
            array (
                'id' => 27,
                'name' => 'Contribution Liability',
                'table_name' => 'receipt_refunds',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            27 =>
            array (
                'id' => 28,
                'name' => 'OSH Audit',
                'table_name' => 'osh_audit_period',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            28 =>
            array (
                'id' => 29,
                'name' => 'Investment Budget',
                'table_name' => 'investment_budgets',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),


            array (
                'id' => 30,
                'name' => 'Employer De-Registration Extensions',
                'table_name' => 'employer_deregistration_extensions',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            30 =>
            array (
                'id' => 31,
                'name' => 'Claim Accrual',
                'table_name' => 'accrual_notification_reports',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),
            31 =>
            array (
                'id' => 32,
                'name' => 'Investigation Plan',
                'table_name' => 'investigation_plans',
                'created_at' => '2020-02-02 17:06:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            32 =>
            array (
                'id' => 33,
                'name' => 'Interest Adjustment (Receipt)',
                'table_name' => 'interest_adjustments',
                'created_at' => '2017-04-18 15:29:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            33 =>
            array (
                'id' => 34,
                'name' => 'Payroll Retiree Monthly Pension Update',
                'table_name' => 'payroll_retiree_mp_updates',
                'created_at' => '2017-04-18 15:29:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            34 =>
            array (
                'id' => 35,
                'name' => 'Multiple Payrolls Merging',
                'table_name' => 'employer_payroll_merges',
                'created_at' => '2018-01-10 16:43:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            35 =>
            array (
                'id' => 36,
                'name' => 'HCP / HSP Bills Vetting',
                'table_name' => 'hsp_bill_summaries',
                'created_at' => '2021-05-01 08:00:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            36 =>
            array (
                'id' => 37,
                'name' => 'Employer\'s Contribution Modification',
                'table_name' => 'contrib_modifications',
                'created_at' => '2021-07-20 08:00:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

            37 =>
            array (
                'id' => 38,
                'name' => 'Contribution / Interest Rate Adjustment',
                'table_name' => 'contribution_interest_rates',
                'created_at' => '2021-07-20 08:00:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'autolocate' => 1,
                'can_archive' => 0,
            ),

        ));

$this->enableForeignKeys('wf_module_groups');
}
}