<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class UnitGroupsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('unit_groups');
        $this->delete('unit_groups');
        
        \DB::table('unit_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Internal Unit',
                'created_at' => '2017-04-18 08:27:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Department',
                'created_at' => '2017-04-18 08:27:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Directorate',
                'created_at' => '2017-04-18 08:28:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'External Unit',
                    'created_at' => '2017-04-18 08:28:00',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('unit_groups');
    }
}