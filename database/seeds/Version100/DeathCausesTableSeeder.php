<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DeathCausesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('death_causes');
        $this->delete('death_causes');
        
        \DB::table('death_causes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Occupational accident',
                'created_at' => '2017-04-18 17:39:59',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Occupational disease',
                'created_at' => '2017-04-18 17:39:59',
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('death_causes');
    }
}