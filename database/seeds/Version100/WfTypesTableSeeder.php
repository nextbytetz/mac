<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class WfTypesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('wf_types');
        
        \DB::table('wf_types')->insert(array (
            0 => 
            array (
                'name' => 'Create Receipt',
                'created_at' => '2017-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'name' => 'Process Claim',
                'created_at' => '2017-04-18 16:07:00',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'name' => 'Reject Notification',
                'created_at' => '2017-04-18 16:07:20',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'name' => 'Approve Investigation Report',
                'created_at' => '2017-04-18 16:07:20',
                'updated_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
    }
}