<?php

use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;
use Database\TruncateTable;

class ReportCategoryReportTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        $this->disableForeignKeys('report_category_report');
        $this->delete('report_category_report');

        \DB::table('report_category_report')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'report_id' => 1,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'report_id' => 2,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'report_id' => 3,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'report_id' => 4,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'report_id' => 5,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'report_id' => 6,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'report_id' => 8,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'report_id' => 9,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'report_id' => 10,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            9 =>
                array (
                    'id' => 10,
                    'report_id' => 11,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            10 =>
                array (
                    'id' => 11,
                    'report_id' => 12,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            11 =>
                array (
                    'id' => 12,
                    'report_id' => 13,
                    'report_category_id' => 4,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            12 =>
                array (
                    'id' => 13,
                    'report_id' => 14,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            13 =>
                array (
                    'id' => 14,
                    'report_id' => 15,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            14 =>
                array (
                    'id' => 15,
                    'report_id' => 16,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            15 =>
                array (
                    'id' => 16,
                    'report_id' => 17,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            16 =>
                array (
                    'id' => 17,
                    'report_id' => 18,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            17 =>
                array (
                    'id' => 18,
                    'report_id' => 19,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            18 =>
                array (
                    'id' => 19,
                    'report_id' => 20,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            19 =>
                array (
                    'id' => 20,
                    'report_id' => 21,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            20 =>
                array (
                    'id' => 21,
                    'report_id' => 22,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            21 =>
                array (
                    'id' => 22,
                    'report_id' => 20,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            22 =>
                array (
                    'id' => 23,
                    'report_id' => 9,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            23 =>
                array (
                    'id' => 24,
                    'report_id' => 6,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            24 =>
                array (
                    'id' => 25,
                    'report_id' => 1,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            25 =>
                array (
                    'id' => 26,
                    'report_id' => 3,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            26 =>
                array (
                    'id' => 27,
                    'report_id' => 23,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            27 =>
                array (
                    'id' => 28,
                    'report_id' => 23,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            28 =>
                array (
                    'id' => 29,
                    'report_id' => 24,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            29 =>
                array (
                    'id' => 30,
                    'report_id' => 25,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            30 =>
                array (
                    'id' => 31,
                    'report_id' => 25,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            31 =>
                array (
                    'id' => 32,
                    'report_id' => 26,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            32 =>
                array (
                    'id' => 33,
                    'report_id' => 27,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            33 =>
                array (
                    'id' => 34,
                    'report_id' => 27,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            34 =>
                array (
                    'id' => 35,
                    'report_id' => 6,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            35 =>
                array (
                    'id' => 36,
                    'report_id' => 28,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            36 =>
                array (
                    'id' => 37,
                    'report_id' => 29,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            37=>
                array (
                    'id' => 38,
                    'report_id' => 30,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            38=>
                array (
                    'id' => 39,
                    'report_id' => 31,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            39=>
                array (
                    'id' => 40,
                    'report_id' => 32,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            40=>
                array (
                    'id' => 41,
                    'report_id' => 33,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            41=>
                array (
                    'id' => 42,
                    'report_id' => 34,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            42=>
                array (
                    'id' => 43,
                    'report_id' => 35,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            43=>
                array (
                    'id' => 44,
                    'report_id' => 36,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            44=>
                array (
                    'id' => 45,
                    'report_id' => 37,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            45=>
                array (
                    'id' => 46,
                    'report_id' => 38,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            46=>
                array (
                    'id' => 47,
                    'report_id' => 39,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            47=>
                array (
                    'id' => 48,
                    'report_id' => 40,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            48=>
                array (
                    'id' => 49,
                    'report_id' => 41,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            49=>
                array (
                    'id' => 50,
                    'report_id' => 42,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            50=>
                array (
                    'id' => 51,
                    'report_id' => 43,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            51=>
                array (
                    'id' => 52,
                    'report_id' => 44,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            52=>
                array (
                    'id' => 53,
                    'report_id' => 45,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            53=>
                array (
                    'id' => 54,
                    'report_id' => 46,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            54=>
                array (
                    'id' => 55,
                    'report_id' => 47,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            55=>
                array (
                    'id' => 56,
                    'report_id' => 1,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            56=>
                array (
                    'id' => 57,
                    'report_id' => 3,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            57=>
                array (
                    'id' => 58,
                    'report_id' => 48,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            58=>
                array (
                    'id' => 59,
                    'report_id' => 49,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            59=>
                array (
                    'id' => 60,
                    'report_id' => 50,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            60=>
                array (
                    'id' => 61,
                    'report_id' => 51,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            61=>
                array (
                    'id' => 62,
                    'report_id' => 52,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            62=>
                array (
                    'id' => 63,
                    'report_id' => 53,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            63=>
                array (
                    'id' => 64,
                    'report_id' => 54,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            64=>
                array (
                    'id' => 65,
                    'report_id' => 55,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            65=>
                array (
                    'id' => 66,
                    'report_id' => 56,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            66=>
                array (
                    'id' => 67,
                    'report_id' => 33,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            67=>
                array (
                    'id' => 68,
                    'report_id' => 34,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),



            68=>
                array (
                    'id' => 69,
                    'report_id' => 36,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),



            69=>
                array (
                    'id' => 70,
                    'report_id' => 46,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            70=>
                array (
                    'id' => 71,
                    'report_id' => 47,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            71=>
                array (
                    'id' => 72,
                    'report_id' => 48,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            72=>
                array (
                    'id' => 73,
                    'report_id' => 49,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            73=>
                array (
                    'id' => 74,
                    'report_id' => 50,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            74=>
                array (
                    'id' => 75,
                    'report_id' => 51,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            75=>
                array (
                    'id' => 76,
                    'report_id' => 53,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            76=>
                array (
                    'id' => 77,
                    'report_id' => 55,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            77=>
                array (
                    'id' => 78,
                    'report_id' => 56,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            78=>
                array (
                    'id' => 79,
                    'report_id' => 57,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            79=>
                array (
                    'id' => 80,
                    'report_id' => 58,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            80=>
                array (
                    'id' => 81,
                    'report_id' => 59,
                    'report_category_id' => 5,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            81=>
                array (
                    'id' => 82,
                    'report_id' => 60,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            82=>
                array (
                    'id' => 83,
                    'report_id' => 61,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            83=>
                array (
                    'id' => 84,
                    'report_id' => 62,
                    'report_category_id' => 4,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            84=>
                array (
                    'id' => 85,
                    'report_id' => 63,
                    'report_category_id' => 4,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            85=>
                array (
                    'id' => 86,
                    'report_id' => 64,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            86=>
                array (
                    'id' => 87,
                    'report_id' => 65,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            87=>
                array (
                    'id' => 88,
                    'report_id' => 66,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            88=>
                array (
                    'id' => 89,
                    'report_id' => 67,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            89=>
                array (
                    'id' => 90,
                    'report_id' => 68,
                    'report_category_id' => 6,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            90=>
                array (
                    'id' => 91,
                    'report_id' => 69,
                    'report_category_id' => 6,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            91=>
                array (
                    'id' => 92,
                    'report_id' => 70,
                    'report_category_id' => 6,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            92=>
                array (
                    'id' => 93,
                    'report_id' => 71,
                    'report_category_id' => 6,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            93=>
                array (
                    'id' => 94,
                    'report_id' => 72,
                    'report_category_id' => 6,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            94=>
                array (
                    'id' => 95,
                    'report_id' => 73,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            95=>
                array (
                    'id' => 96,
                    'report_id' => 74,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            96=>
                array (
                    'id' => 97,
                    'report_id' => 75,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            97=>
                array (
                    'id' => 98,
                    'report_id' => 76,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            98=>
                array (
                    'id' => 99,
                    'report_id' => 77,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            99=>
                array (
                    'id' => 100,
                    'report_id' => 78,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            100=>
                array (
                    'id' => 101,
                    'report_id' => 79,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            101=>
                array (
                    'id' => 102,
                    'report_id' => 80,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            102=>
                array (
                    'id' => 103,
                    'report_id' => 81,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            103=>
                array (
                    'id' => 104,
                    'report_id' => 82,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            104=>
                array (
                    'id' => 105,
                    'report_id' => 83,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            105=>
                array (
                    'id' => 106,
                    'report_id' => 84,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            106=>
                array (
                    'id' => 107,
                    'report_id' => 85,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            107=>
                array (
                    'id' => 108,
                    'report_id' => 86,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            108=>
                array (
                    'id' => 109,
                    'report_id' => 87,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            109=>
                array (
                    'id' => 110,
                    'report_id' => 88,
                    'report_category_id' => 7,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            110=>
                array (
                    'id' => 111,
                    'report_id' => 89,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            111=>
                array (
                    'id' => 112,
                    'report_id' => 90,
                    'report_category_id' => 8,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            /*pension statement*/
            112 =>
                array (
                    'id' => 113,
                    'report_id' => 91,
                    'report_category_id' => 4,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            113 =>
                array (
                    'id' => 114,
                    'report_id' => 92,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            114 =>
                array (
                    'id' => 115,
                    'report_id' => 93,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            115 =>
                array (
                    'id' => 116,
                    'report_id' => 94,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            116 =>
                array (
                    'id' => 117,
                    'report_id' => 95,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            117 =>
                array (
                    'id' => 118,
                    'report_id' => 96,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            118 =>
                array (
                    'id' => 119,
                    'report_id' => 97,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            119 =>
                array (
                    'id' => 120,
                    'report_id' => 98,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            120 =>
                array (
                    'id' => 121,
                    'report_id' => 99,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            121 =>
                array (
                    'id' => 122,
                    'report_id' => 100,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            122 =>
                array (
                    'id' => 123,
                    'report_id' => 101,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            123 =>
                array (
                    'id' => 124,
                    'report_id' => 102,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            124 =>
                array (
                    'id' => 125,
                    'report_id' => 103,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            125 =>
                array (
                    'id' => 126,
                    'report_id' => 104,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            /*Payroll recovery*/
//                 126 =>
//                array (
//                    'id' => 127,
//                    'report_id' => 105,
//                    'report_category_id' => 4,
//                    'created_at' => NULL,
//                    'updated_at' => NULL,
//                ),
            127 =>
                array (
                    'id' => 128,
                    'report_id' => 106,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            128 =>
                array (
                    'id' => 129,
                    'report_id' => 107,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            129 =>
                array (
                    'id' => 130,
                    'report_id' => 108,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            130 =>
                array (
                    'id' => 131,
                    'report_id' => 109,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            131 =>
                array (
                    'id' => 132,
                    'report_id' => 110,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            132 =>
                array (
                    'id' => 133,
                    'report_id' => 111,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            133 =>
                array (
                    'id' => 134,
                    'report_id' => 112,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),


            134 =>
                array (
                    'id' => 135,
                    'report_id' => 113,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

//            <new payroll reports>
            135 =>
                array (
                    'id' => 136,
                    'report_id' => 114,
                    'report_category_id' => 4,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            136 =>
                array (
                    'id' => 137,
                    'report_id' => 115,
                    'report_category_id' => 4,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            137 =>
                array (
                    'id' => 138,
                    'report_id' => 116,
                    'report_category_id' => 4,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            /*<end payroll report new>*/
            138 =>
                array (
                    'id' => 139,
                    'report_id' => 117,
                    'report_category_id' => 3,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            139 =>
                array (
                    'id' => 140,
                    'report_id' => 118,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            140 =>
                array (
                    'id' => 141,
                    'report_id' => 119,
                    'report_category_id' => 2,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            141 =>
                array (
                    'id' => 142,
                    'report_id' => 120,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            142 =>
                array (
                    'id' => 143,
                    'report_id' => 121,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            143 =>
                array (
                    'id' => 144,
                    'report_id' => 122,
                    'report_category_id' => 1,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('report_category_report');

    }
}