<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class FinCodeGroupsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys('fin_code_groups');
        $this->delete('fin_code_groups');
        
        \DB::table('fin_code_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Receivable',
                'created_at' => '2017-04-18 09:00:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Expenses',
                'created_at' => '2017-04-18 09:00:14',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Income',
                'created_at' => '2017-04-18 09:00:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Payable',
                'created_at' => '2017-04-18 09:00:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys('fin_code_groups');
    }
}