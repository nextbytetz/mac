<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class AuditEntitiesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
        $this->truncate('audit_entities');
        
        \DB::table('audit_entities')->insert(array (
            0 => 
            array (
                'name' => 'Receipt Register',
                'created_at' => '2017-04-18 12:03:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'name' => 'Interest',
                'created_at' => '2017-04-18 12:03:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'name' => 'Notification',
                'created_at' => '2017-04-18 12:04:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'name' => 'Claim',
                'created_at' => '2017-04-18 12:04:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'name' => 'Inspection',
                'created_at' => '2017-04-18 12:05:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'name' => 'Case',
                'created_at' => '2017-04-18 12:05:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'name' => 'Contribution',
                'created_at' => '2017-04-18 12:05:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'name' => 'Dishonored Cheques',
                'created_at' => '2017-04-18 12:05:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'name' => 'Cancel Receipt',
                'created_at' => '2017-04-18 12:05:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));

        $this->enableForeignKeys();
    }
}