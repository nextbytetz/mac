<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class PdOccupationAdjustmentsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $count = \DB::table('pd_occupation_adjustments')->limit(1)->count();

        if (!$count) {
            $this->disableForeignKeys('pd_occupation_adjustments');
            $this->delete('pd_occupation_adjustments');

            \DB::table('pd_occupation_adjustments')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'characteristic' => 'C',
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'characteristic' => 'C',
                        'prev_rating' => 2,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'characteristic' => 'C',
                        'prev_rating' => 3,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'characteristic' => 'C',
                        'prev_rating' => 4,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'characteristic' => 'C',
                        'prev_rating' => 5,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'characteristic' => 'C',
                        'prev_rating' => 6,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'characteristic' => 'C',
                        'prev_rating' => 7,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'characteristic' => 'C',
                        'prev_rating' => 8,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'characteristic' => 'C',
                        'prev_rating' => 9,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'characteristic' => 'C',
                        'prev_rating' => 10,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'characteristic' => 'C',
                        'prev_rating' => 11,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'characteristic' => 'C',
                        'prev_rating' => 12,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'characteristic' => 'C',
                        'prev_rating' => 13,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'characteristic' => 'C',
                        'prev_rating' => 14,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'characteristic' => 'C',
                        'prev_rating' => 15,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'characteristic' => 'C',
                        'prev_rating' => 16,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'characteristic' => 'C',
                        'prev_rating' => 17,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'characteristic' => 'C',
                        'prev_rating' => 18,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'characteristic' => 'C',
                        'prev_rating' => 19,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'characteristic' => 'C',
                        'prev_rating' => 20,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'characteristic' => 'C',
                        'prev_rating' => 21,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'characteristic' => 'C',
                        'prev_rating' => 22,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'characteristic' => 'C',
                        'prev_rating' => 23,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'characteristic' => 'C',
                        'prev_rating' => 24,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'characteristic' => 'C',
                        'prev_rating' => 25,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'characteristic' => 'C',
                        'prev_rating' => 51,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'characteristic' => 'C',
                        'prev_rating' => 52,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'characteristic' => 'C',
                        'prev_rating' => 53,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'characteristic' => 'C',
                        'prev_rating' => 54,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'characteristic' => 'C',
                        'prev_rating' => 55,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'characteristic' => 'C',
                        'prev_rating' => 56,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'characteristic' => 'C',
                        'prev_rating' => 57,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'characteristic' => 'C',
                        'prev_rating' => 58,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'characteristic' => 'C',
                        'prev_rating' => 59,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                34 =>
                    array (
                        'id' => 35,
                        'characteristic' => 'C',
                        'prev_rating' => 60,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                35 =>
                    array (
                        'id' => 36,
                        'characteristic' => 'C',
                        'prev_rating' => 61,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                36 =>
                    array (
                        'id' => 37,
                        'characteristic' => 'C',
                        'prev_rating' => 62,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                37 =>
                    array (
                        'id' => 38,
                        'characteristic' => 'C',
                        'prev_rating' => 63,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                38 =>
                    array (
                        'id' => 39,
                        'characteristic' => 'C',
                        'prev_rating' => 64,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                39 =>
                    array (
                        'id' => 40,
                        'characteristic' => 'C',
                        'prev_rating' => 65,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                40 =>
                    array (
                        'id' => 41,
                        'characteristic' => 'C',
                        'prev_rating' => 66,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                41 =>
                    array (
                        'id' => 42,
                        'characteristic' => 'C',
                        'prev_rating' => 67,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                42 =>
                    array (
                        'id' => 43,
                        'characteristic' => 'C',
                        'prev_rating' => 68,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                43 =>
                    array (
                        'id' => 44,
                        'characteristic' => 'C',
                        'prev_rating' => 69,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                44 =>
                    array (
                        'id' => 45,
                        'characteristic' => 'C',
                        'prev_rating' => 70,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                45 =>
                    array (
                        'id' => 46,
                        'characteristic' => 'C',
                        'prev_rating' => 71,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                46 =>
                    array (
                        'id' => 47,
                        'characteristic' => 'C',
                        'prev_rating' => 72,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                47 =>
                    array (
                        'id' => 48,
                        'characteristic' => 'C',
                        'prev_rating' => 73,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                48 =>
                    array (
                        'id' => 49,
                        'characteristic' => 'C',
                        'prev_rating' => 74,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                49 =>
                    array (
                        'id' => 50,
                        'characteristic' => 'C',
                        'prev_rating' => 75,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                50 =>
                    array (
                        'id' => 51,
                        'characteristic' => 'D',
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                51 =>
                    array (
                        'id' => 52,
                        'characteristic' => 'D',
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                52 =>
                    array (
                        'id' => 53,
                        'characteristic' => 'D',
                        'prev_rating' => 3,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                53 =>
                    array (
                        'id' => 54,
                        'characteristic' => 'D',
                        'prev_rating' => 4,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                54 =>
                    array (
                        'id' => 55,
                        'characteristic' => 'D',
                        'prev_rating' => 5,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                55 =>
                    array (
                        'id' => 56,
                        'characteristic' => 'D',
                        'prev_rating' => 6,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                56 =>
                    array (
                        'id' => 57,
                        'characteristic' => 'D',
                        'prev_rating' => 7,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                57 =>
                    array (
                        'id' => 58,
                        'characteristic' => 'D',
                        'prev_rating' => 8,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                58 =>
                    array (
                        'id' => 59,
                        'characteristic' => 'D',
                        'prev_rating' => 9,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                59 =>
                    array (
                        'id' => 60,
                        'characteristic' => 'D',
                        'prev_rating' => 10,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                60 =>
                    array (
                        'id' => 61,
                        'characteristic' => 'D',
                        'prev_rating' => 11,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                61 =>
                    array (
                        'id' => 62,
                        'characteristic' => 'D',
                        'prev_rating' => 12,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                62 =>
                    array (
                        'id' => 63,
                        'characteristic' => 'D',
                        'prev_rating' => 13,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                63 =>
                    array (
                        'id' => 64,
                        'characteristic' => 'D',
                        'prev_rating' => 14,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                64 =>
                    array (
                        'id' => 65,
                        'characteristic' => 'D',
                        'prev_rating' => 15,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                65 =>
                    array (
                        'id' => 66,
                        'characteristic' => 'D',
                        'prev_rating' => 16,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                66 =>
                    array (
                        'id' => 67,
                        'characteristic' => 'D',
                        'prev_rating' => 17,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                67 =>
                    array (
                        'id' => 68,
                        'characteristic' => 'D',
                        'prev_rating' => 18,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                68 =>
                    array (
                        'id' => 69,
                        'characteristic' => 'D',
                        'prev_rating' => 19,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                69 =>
                    array (
                        'id' => 70,
                        'characteristic' => 'D',
                        'prev_rating' => 20,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                70 =>
                    array (
                        'id' => 71,
                        'characteristic' => 'D',
                        'prev_rating' => 21,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                71 =>
                    array (
                        'id' => 72,
                        'characteristic' => 'D',
                        'prev_rating' => 22,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                72 =>
                    array (
                        'id' => 73,
                        'characteristic' => 'D',
                        'prev_rating' => 23,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                73 =>
                    array (
                        'id' => 74,
                        'characteristic' => 'D',
                        'prev_rating' => 24,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                74 =>
                    array (
                        'id' => 75,
                        'characteristic' => 'D',
                        'prev_rating' => 25,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                75 =>
                    array (
                        'id' => 76,
                        'characteristic' => 'D',
                        'prev_rating' => 51,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                76 =>
                    array (
                        'id' => 77,
                        'characteristic' => 'D',
                        'prev_rating' => 52,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                77 =>
                    array (
                        'id' => 78,
                        'characteristic' => 'D',
                        'prev_rating' => 53,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                78 =>
                    array (
                        'id' => 79,
                        'characteristic' => 'D',
                        'prev_rating' => 54,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                79 =>
                    array (
                        'id' => 80,
                        'characteristic' => 'D',
                        'prev_rating' => 55,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                80 =>
                    array (
                        'id' => 81,
                        'characteristic' => 'D',
                        'prev_rating' => 56,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                81 =>
                    array (
                        'id' => 82,
                        'characteristic' => 'D',
                        'prev_rating' => 57,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                82 =>
                    array (
                        'id' => 83,
                        'characteristic' => 'D',
                        'prev_rating' => 58,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                83 =>
                    array (
                        'id' => 84,
                        'characteristic' => 'D',
                        'prev_rating' => 59,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                84 =>
                    array (
                        'id' => 85,
                        'characteristic' => 'D',
                        'prev_rating' => 60,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                85 =>
                    array (
                        'id' => 86,
                        'characteristic' => 'D',
                        'prev_rating' => 61,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                86 =>
                    array (
                        'id' => 87,
                        'characteristic' => 'D',
                        'prev_rating' => 62,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                87 =>
                    array (
                        'id' => 88,
                        'characteristic' => 'D',
                        'prev_rating' => 63,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                88 =>
                    array (
                        'id' => 89,
                        'characteristic' => 'D',
                        'prev_rating' => 64,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                89 =>
                    array (
                        'id' => 90,
                        'characteristic' => 'D',
                        'prev_rating' => 65,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                90 =>
                    array (
                        'id' => 91,
                        'characteristic' => 'D',
                        'prev_rating' => 66,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                91 =>
                    array (
                        'id' => 92,
                        'characteristic' => 'D',
                        'prev_rating' => 67,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                92 =>
                    array (
                        'id' => 93,
                        'characteristic' => 'D',
                        'prev_rating' => 68,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                93 =>
                    array (
                        'id' => 94,
                        'characteristic' => 'D',
                        'prev_rating' => 69,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                94 =>
                    array (
                        'id' => 95,
                        'characteristic' => 'D',
                        'prev_rating' => 70,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                95 =>
                    array (
                        'id' => 96,
                        'characteristic' => 'D',
                        'prev_rating' => 71,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                96 =>
                    array (
                        'id' => 97,
                        'characteristic' => 'D',
                        'prev_rating' => 72,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                97 =>
                    array (
                        'id' => 98,
                        'characteristic' => 'D',
                        'prev_rating' => 73,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                98 =>
                    array (
                        'id' => 99,
                        'characteristic' => 'D',
                        'prev_rating' => 74,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                99 =>
                    array (
                        'id' => 100,
                        'characteristic' => 'D',
                        'prev_rating' => 75,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                100 =>
                    array (
                        'id' => 101,
                        'characteristic' => 'E',
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                101 =>
                    array (
                        'id' => 102,
                        'characteristic' => 'E',
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                102 =>
                    array (
                        'id' => 103,
                        'characteristic' => 'E',
                        'prev_rating' => 3,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                103 =>
                    array (
                        'id' => 104,
                        'characteristic' => 'E',
                        'prev_rating' => 4,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                104 =>
                    array (
                        'id' => 105,
                        'characteristic' => 'E',
                        'prev_rating' => 5,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                105 =>
                    array (
                        'id' => 106,
                        'characteristic' => 'E',
                        'prev_rating' => 6,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                106 =>
                    array (
                        'id' => 107,
                        'characteristic' => 'E',
                        'prev_rating' => 7,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                107 =>
                    array (
                        'id' => 108,
                        'characteristic' => 'E',
                        'prev_rating' => 8,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                108 =>
                    array (
                        'id' => 109,
                        'characteristic' => 'E',
                        'prev_rating' => 9,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                109 =>
                    array (
                        'id' => 110,
                        'characteristic' => 'E',
                        'prev_rating' => 10,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                110 =>
                    array (
                        'id' => 111,
                        'characteristic' => 'E',
                        'prev_rating' => 11,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                111 =>
                    array (
                        'id' => 112,
                        'characteristic' => 'E',
                        'prev_rating' => 12,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                112 =>
                    array (
                        'id' => 113,
                        'characteristic' => 'E',
                        'prev_rating' => 13,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                113 =>
                    array (
                        'id' => 114,
                        'characteristic' => 'E',
                        'prev_rating' => 14,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                114 =>
                    array (
                        'id' => 115,
                        'characteristic' => 'E',
                        'prev_rating' => 15,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                115 =>
                    array (
                        'id' => 116,
                        'characteristic' => 'E',
                        'prev_rating' => 16,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                116 =>
                    array (
                        'id' => 117,
                        'characteristic' => 'E',
                        'prev_rating' => 17,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                117 =>
                    array (
                        'id' => 118,
                        'characteristic' => 'E',
                        'prev_rating' => 18,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                118 =>
                    array (
                        'id' => 119,
                        'characteristic' => 'E',
                        'prev_rating' => 19,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                119 =>
                    array (
                        'id' => 120,
                        'characteristic' => 'E',
                        'prev_rating' => 20,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                120 =>
                    array (
                        'id' => 121,
                        'characteristic' => 'E',
                        'prev_rating' => 21,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                121 =>
                    array (
                        'id' => 122,
                        'characteristic' => 'E',
                        'prev_rating' => 22,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                122 =>
                    array (
                        'id' => 123,
                        'characteristic' => 'E',
                        'prev_rating' => 23,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                123 =>
                    array (
                        'id' => 124,
                        'characteristic' => 'E',
                        'prev_rating' => 24,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                124 =>
                    array (
                        'id' => 125,
                        'characteristic' => 'E',
                        'prev_rating' => 25,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                125 =>
                    array (
                        'id' => 126,
                        'characteristic' => 'E',
                        'prev_rating' => 51,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                126 =>
                    array (
                        'id' => 127,
                        'characteristic' => 'E',
                        'prev_rating' => 52,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                127 =>
                    array (
                        'id' => 128,
                        'characteristic' => 'E',
                        'prev_rating' => 53,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                128 =>
                    array (
                        'id' => 129,
                        'characteristic' => 'E',
                        'prev_rating' => 54,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                129 =>
                    array (
                        'id' => 130,
                        'characteristic' => 'E',
                        'prev_rating' => 55,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                130 =>
                    array (
                        'id' => 131,
                        'characteristic' => 'E',
                        'prev_rating' => 56,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                131 =>
                    array (
                        'id' => 132,
                        'characteristic' => 'E',
                        'prev_rating' => 57,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                132 =>
                    array (
                        'id' => 133,
                        'characteristic' => 'E',
                        'prev_rating' => 58,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                133 =>
                    array (
                        'id' => 134,
                        'characteristic' => 'E',
                        'prev_rating' => 59,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                134 =>
                    array (
                        'id' => 135,
                        'characteristic' => 'E',
                        'prev_rating' => 60,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                135 =>
                    array (
                        'id' => 136,
                        'characteristic' => 'E',
                        'prev_rating' => 61,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                136 =>
                    array (
                        'id' => 137,
                        'characteristic' => 'E',
                        'prev_rating' => 62,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                137 =>
                    array (
                        'id' => 138,
                        'characteristic' => 'E',
                        'prev_rating' => 63,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                138 =>
                    array (
                        'id' => 139,
                        'characteristic' => 'E',
                        'prev_rating' => 64,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                139 =>
                    array (
                        'id' => 140,
                        'characteristic' => 'E',
                        'prev_rating' => 65,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                140 =>
                    array (
                        'id' => 141,
                        'characteristic' => 'E',
                        'prev_rating' => 66,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                141 =>
                    array (
                        'id' => 142,
                        'characteristic' => 'E',
                        'prev_rating' => 67,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                142 =>
                    array (
                        'id' => 143,
                        'characteristic' => 'E',
                        'prev_rating' => 68,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                143 =>
                    array (
                        'id' => 144,
                        'characteristic' => 'E',
                        'prev_rating' => 69,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                144 =>
                    array (
                        'id' => 145,
                        'characteristic' => 'E',
                        'prev_rating' => 70,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                145 =>
                    array (
                        'id' => 146,
                        'characteristic' => 'E',
                        'prev_rating' => 71,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                146 =>
                    array (
                        'id' => 147,
                        'characteristic' => 'E',
                        'prev_rating' => 72,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                147 =>
                    array (
                        'id' => 148,
                        'characteristic' => 'E',
                        'prev_rating' => 73,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                148 =>
                    array (
                        'id' => 149,
                        'characteristic' => 'E',
                        'prev_rating' => 74,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                149 =>
                    array (
                        'id' => 150,
                        'characteristic' => 'E',
                        'prev_rating' => 75,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                150 =>
                    array (
                        'id' => 151,
                        'characteristic' => 'F',
                        'prev_rating' => 1,
                        'rating' => 1,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                151 =>
                    array (
                        'id' => 152,
                        'characteristic' => 'F',
                        'prev_rating' => 2,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                152 =>
                    array (
                        'id' => 153,
                        'characteristic' => 'F',
                        'prev_rating' => 3,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                153 =>
                    array (
                        'id' => 154,
                        'characteristic' => 'F',
                        'prev_rating' => 4,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                154 =>
                    array (
                        'id' => 155,
                        'characteristic' => 'F',
                        'prev_rating' => 5,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                155 =>
                    array (
                        'id' => 156,
                        'characteristic' => 'F',
                        'prev_rating' => 6,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                156 =>
                    array (
                        'id' => 157,
                        'characteristic' => 'F',
                        'prev_rating' => 7,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                157 =>
                    array (
                        'id' => 158,
                        'characteristic' => 'F',
                        'prev_rating' => 8,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                158 =>
                    array (
                        'id' => 159,
                        'characteristic' => 'F',
                        'prev_rating' => 9,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                159 =>
                    array (
                        'id' => 160,
                        'characteristic' => 'F',
                        'prev_rating' => 10,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                160 =>
                    array (
                        'id' => 161,
                        'characteristic' => 'F',
                        'prev_rating' => 11,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                161 =>
                    array (
                        'id' => 162,
                        'characteristic' => 'F',
                        'prev_rating' => 12,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                162 =>
                    array (
                        'id' => 163,
                        'characteristic' => 'F',
                        'prev_rating' => 13,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                163 =>
                    array (
                        'id' => 164,
                        'characteristic' => 'F',
                        'prev_rating' => 14,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                164 =>
                    array (
                        'id' => 165,
                        'characteristic' => 'F',
                        'prev_rating' => 15,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                165 =>
                    array (
                        'id' => 166,
                        'characteristic' => 'F',
                        'prev_rating' => 16,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                166 =>
                    array (
                        'id' => 167,
                        'characteristic' => 'F',
                        'prev_rating' => 17,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                167 =>
                    array (
                        'id' => 168,
                        'characteristic' => 'F',
                        'prev_rating' => 18,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                168 =>
                    array (
                        'id' => 169,
                        'characteristic' => 'F',
                        'prev_rating' => 19,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                169 =>
                    array (
                        'id' => 170,
                        'characteristic' => 'F',
                        'prev_rating' => 20,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                170 =>
                    array (
                        'id' => 171,
                        'characteristic' => 'F',
                        'prev_rating' => 21,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                171 =>
                    array (
                        'id' => 172,
                        'characteristic' => 'F',
                        'prev_rating' => 22,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                172 =>
                    array (
                        'id' => 173,
                        'characteristic' => 'F',
                        'prev_rating' => 23,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                173 =>
                    array (
                        'id' => 174,
                        'characteristic' => 'F',
                        'prev_rating' => 24,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                174 =>
                    array (
                        'id' => 175,
                        'characteristic' => 'F',
                        'prev_rating' => 25,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                175 =>
                    array (
                        'id' => 176,
                        'characteristic' => 'F',
                        'prev_rating' => 51,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                176 =>
                    array (
                        'id' => 177,
                        'characteristic' => 'F',
                        'prev_rating' => 52,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                177 =>
                    array (
                        'id' => 178,
                        'characteristic' => 'F',
                        'prev_rating' => 53,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                178 =>
                    array (
                        'id' => 179,
                        'characteristic' => 'F',
                        'prev_rating' => 54,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                179 =>
                    array (
                        'id' => 180,
                        'characteristic' => 'F',
                        'prev_rating' => 55,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                180 =>
                    array (
                        'id' => 181,
                        'characteristic' => 'F',
                        'prev_rating' => 56,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                181 =>
                    array (
                        'id' => 182,
                        'characteristic' => 'F',
                        'prev_rating' => 57,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                182 =>
                    array (
                        'id' => 183,
                        'characteristic' => 'F',
                        'prev_rating' => 58,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                183 =>
                    array (
                        'id' => 184,
                        'characteristic' => 'F',
                        'prev_rating' => 59,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                184 =>
                    array (
                        'id' => 185,
                        'characteristic' => 'F',
                        'prev_rating' => 60,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                185 =>
                    array (
                        'id' => 186,
                        'characteristic' => 'F',
                        'prev_rating' => 61,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                186 =>
                    array (
                        'id' => 187,
                        'characteristic' => 'F',
                        'prev_rating' => 62,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                187 =>
                    array (
                        'id' => 188,
                        'characteristic' => 'F',
                        'prev_rating' => 63,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                188 =>
                    array (
                        'id' => 189,
                        'characteristic' => 'F',
                        'prev_rating' => 64,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                189 =>
                    array (
                        'id' => 190,
                        'characteristic' => 'F',
                        'prev_rating' => 65,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                190 =>
                    array (
                        'id' => 191,
                        'characteristic' => 'F',
                        'prev_rating' => 66,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                191 =>
                    array (
                        'id' => 192,
                        'characteristic' => 'F',
                        'prev_rating' => 67,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                192 =>
                    array (
                        'id' => 193,
                        'characteristic' => 'F',
                        'prev_rating' => 68,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                193 =>
                    array (
                        'id' => 194,
                        'characteristic' => 'F',
                        'prev_rating' => 69,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                194 =>
                    array (
                        'id' => 195,
                        'characteristic' => 'F',
                        'prev_rating' => 70,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                195 =>
                    array (
                        'id' => 196,
                        'characteristic' => 'F',
                        'prev_rating' => 71,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                196 =>
                    array (
                        'id' => 197,
                        'characteristic' => 'F',
                        'prev_rating' => 72,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                197 =>
                    array (
                        'id' => 198,
                        'characteristic' => 'F',
                        'prev_rating' => 73,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                198 =>
                    array (
                        'id' => 199,
                        'characteristic' => 'F',
                        'prev_rating' => 74,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                199 =>
                    array (
                        'id' => 200,
                        'characteristic' => 'F',
                        'prev_rating' => 75,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                200 =>
                    array (
                        'id' => 201,
                        'characteristic' => 'G',
                        'prev_rating' => 1,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                201 =>
                    array (
                        'id' => 202,
                        'characteristic' => 'G',
                        'prev_rating' => 2,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                202 =>
                    array (
                        'id' => 203,
                        'characteristic' => 'G',
                        'prev_rating' => 3,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                203 =>
                    array (
                        'id' => 204,
                        'characteristic' => 'G',
                        'prev_rating' => 4,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                204 =>
                    array (
                        'id' => 205,
                        'characteristic' => 'G',
                        'prev_rating' => 5,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                205 =>
                    array (
                        'id' => 206,
                        'characteristic' => 'G',
                        'prev_rating' => 6,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                206 =>
                    array (
                        'id' => 207,
                        'characteristic' => 'G',
                        'prev_rating' => 7,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                207 =>
                    array (
                        'id' => 208,
                        'characteristic' => 'G',
                        'prev_rating' => 8,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                208 =>
                    array (
                        'id' => 209,
                        'characteristic' => 'G',
                        'prev_rating' => 9,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                209 =>
                    array (
                        'id' => 210,
                        'characteristic' => 'G',
                        'prev_rating' => 10,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                210 =>
                    array (
                        'id' => 211,
                        'characteristic' => 'G',
                        'prev_rating' => 11,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                211 =>
                    array (
                        'id' => 212,
                        'characteristic' => 'G',
                        'prev_rating' => 12,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                212 =>
                    array (
                        'id' => 213,
                        'characteristic' => 'G',
                        'prev_rating' => 13,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                213 =>
                    array (
                        'id' => 214,
                        'characteristic' => 'G',
                        'prev_rating' => 14,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                214 =>
                    array (
                        'id' => 215,
                        'characteristic' => 'G',
                        'prev_rating' => 15,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                215 =>
                    array (
                        'id' => 216,
                        'characteristic' => 'G',
                        'prev_rating' => 16,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                216 =>
                    array (
                        'id' => 217,
                        'characteristic' => 'G',
                        'prev_rating' => 17,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                217 =>
                    array (
                        'id' => 218,
                        'characteristic' => 'G',
                        'prev_rating' => 18,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                218 =>
                    array (
                        'id' => 219,
                        'characteristic' => 'G',
                        'prev_rating' => 19,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                219 =>
                    array (
                        'id' => 220,
                        'characteristic' => 'G',
                        'prev_rating' => 20,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                220 =>
                    array (
                        'id' => 221,
                        'characteristic' => 'G',
                        'prev_rating' => 21,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                221 =>
                    array (
                        'id' => 222,
                        'characteristic' => 'G',
                        'prev_rating' => 22,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                222 =>
                    array (
                        'id' => 223,
                        'characteristic' => 'G',
                        'prev_rating' => 23,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                223 =>
                    array (
                        'id' => 224,
                        'characteristic' => 'G',
                        'prev_rating' => 24,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                224 =>
                    array (
                        'id' => 225,
                        'characteristic' => 'G',
                        'prev_rating' => 25,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                225 =>
                    array (
                        'id' => 226,
                        'characteristic' => 'G',
                        'prev_rating' => 51,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                226 =>
                    array (
                        'id' => 227,
                        'characteristic' => 'G',
                        'prev_rating' => 52,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                227 =>
                    array (
                        'id' => 228,
                        'characteristic' => 'G',
                        'prev_rating' => 53,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                228 =>
                    array (
                        'id' => 229,
                        'characteristic' => 'G',
                        'prev_rating' => 54,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                229 =>
                    array (
                        'id' => 230,
                        'characteristic' => 'G',
                        'prev_rating' => 55,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                230 =>
                    array (
                        'id' => 231,
                        'characteristic' => 'G',
                        'prev_rating' => 56,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                231 =>
                    array (
                        'id' => 232,
                        'characteristic' => 'G',
                        'prev_rating' => 57,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                232 =>
                    array (
                        'id' => 233,
                        'characteristic' => 'G',
                        'prev_rating' => 58,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                233 =>
                    array (
                        'id' => 234,
                        'characteristic' => 'G',
                        'prev_rating' => 59,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                234 =>
                    array (
                        'id' => 235,
                        'characteristic' => 'G',
                        'prev_rating' => 60,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                235 =>
                    array (
                        'id' => 236,
                        'characteristic' => 'G',
                        'prev_rating' => 61,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                236 =>
                    array (
                        'id' => 237,
                        'characteristic' => 'G',
                        'prev_rating' => 62,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                237 =>
                    array (
                        'id' => 238,
                        'characteristic' => 'G',
                        'prev_rating' => 63,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                238 =>
                    array (
                        'id' => 239,
                        'characteristic' => 'G',
                        'prev_rating' => 64,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                239 =>
                    array (
                        'id' => 240,
                        'characteristic' => 'G',
                        'prev_rating' => 65,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                240 =>
                    array (
                        'id' => 241,
                        'characteristic' => 'G',
                        'prev_rating' => 66,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                241 =>
                    array (
                        'id' => 242,
                        'characteristic' => 'G',
                        'prev_rating' => 67,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                242 =>
                    array (
                        'id' => 243,
                        'characteristic' => 'G',
                        'prev_rating' => 68,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                243 =>
                    array (
                        'id' => 244,
                        'characteristic' => 'G',
                        'prev_rating' => 69,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                244 =>
                    array (
                        'id' => 245,
                        'characteristic' => 'G',
                        'prev_rating' => 70,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                245 =>
                    array (
                        'id' => 246,
                        'characteristic' => 'G',
                        'prev_rating' => 71,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                246 =>
                    array (
                        'id' => 247,
                        'characteristic' => 'G',
                        'prev_rating' => 72,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                247 =>
                    array (
                        'id' => 248,
                        'characteristic' => 'G',
                        'prev_rating' => 73,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                248 =>
                    array (
                        'id' => 249,
                        'characteristic' => 'G',
                        'prev_rating' => 74,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                249 =>
                    array (
                        'id' => 250,
                        'characteristic' => 'G',
                        'prev_rating' => 75,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                250 =>
                    array (
                        'id' => 251,
                        'characteristic' => 'H',
                        'prev_rating' => 1,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                251 =>
                    array (
                        'id' => 252,
                        'characteristic' => 'H',
                        'prev_rating' => 2,
                        'rating' => 3,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                252 =>
                    array (
                        'id' => 253,
                        'characteristic' => 'H',
                        'prev_rating' => 3,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                253 =>
                    array (
                        'id' => 254,
                        'characteristic' => 'H',
                        'prev_rating' => 4,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                254 =>
                    array (
                        'id' => 255,
                        'characteristic' => 'H',
                        'prev_rating' => 5,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                255 =>
                    array (
                        'id' => 256,
                        'characteristic' => 'H',
                        'prev_rating' => 6,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                256 =>
                    array (
                        'id' => 257,
                        'characteristic' => 'H',
                        'prev_rating' => 7,
                        'rating' => 10,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                257 =>
                    array (
                        'id' => 258,
                        'characteristic' => 'H',
                        'prev_rating' => 8,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                258 =>
                    array (
                        'id' => 259,
                        'characteristic' => 'H',
                        'prev_rating' => 9,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                259 =>
                    array (
                        'id' => 260,
                        'characteristic' => 'H',
                        'prev_rating' => 10,
                        'rating' => 13,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                260 =>
                    array (
                        'id' => 261,
                        'characteristic' => 'H',
                        'prev_rating' => 11,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                261 =>
                    array (
                        'id' => 262,
                        'characteristic' => 'H',
                        'prev_rating' => 12,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                262 =>
                    array (
                        'id' => 263,
                        'characteristic' => 'H',
                        'prev_rating' => 13,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                263 =>
                    array (
                        'id' => 264,
                        'characteristic' => 'H',
                        'prev_rating' => 14,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                264 =>
                    array (
                        'id' => 265,
                        'characteristic' => 'H',
                        'prev_rating' => 15,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                265 =>
                    array (
                        'id' => 266,
                        'characteristic' => 'H',
                        'prev_rating' => 16,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                266 =>
                    array (
                        'id' => 267,
                        'characteristic' => 'H',
                        'prev_rating' => 17,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                267 =>
                    array (
                        'id' => 268,
                        'characteristic' => 'H',
                        'prev_rating' => 18,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                268 =>
                    array (
                        'id' => 269,
                        'characteristic' => 'H',
                        'prev_rating' => 19,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                269 =>
                    array (
                        'id' => 270,
                        'characteristic' => 'H',
                        'prev_rating' => 20,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                270 =>
                    array (
                        'id' => 271,
                        'characteristic' => 'H',
                        'prev_rating' => 21,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                271 =>
                    array (
                        'id' => 272,
                        'characteristic' => 'H',
                        'prev_rating' => 22,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                272 =>
                    array (
                        'id' => 273,
                        'characteristic' => 'H',
                        'prev_rating' => 23,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                273 =>
                    array (
                        'id' => 274,
                        'characteristic' => 'H',
                        'prev_rating' => 24,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                274 =>
                    array (
                        'id' => 275,
                        'characteristic' => 'H',
                        'prev_rating' => 25,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                275 =>
                    array (
                        'id' => 276,
                        'characteristic' => 'H',
                        'prev_rating' => 51,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                276 =>
                    array (
                        'id' => 277,
                        'characteristic' => 'H',
                        'prev_rating' => 52,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                277 =>
                    array (
                        'id' => 278,
                        'characteristic' => 'H',
                        'prev_rating' => 53,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                278 =>
                    array (
                        'id' => 279,
                        'characteristic' => 'H',
                        'prev_rating' => 54,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                279 =>
                    array (
                        'id' => 280,
                        'characteristic' => 'H',
                        'prev_rating' => 55,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                280 =>
                    array (
                        'id' => 281,
                        'characteristic' => 'H',
                        'prev_rating' => 56,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                281 =>
                    array (
                        'id' => 282,
                        'characteristic' => 'H',
                        'prev_rating' => 57,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                282 =>
                    array (
                        'id' => 283,
                        'characteristic' => 'H',
                        'prev_rating' => 58,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                283 =>
                    array (
                        'id' => 284,
                        'characteristic' => 'H',
                        'prev_rating' => 59,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                284 =>
                    array (
                        'id' => 285,
                        'characteristic' => 'H',
                        'prev_rating' => 60,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                285 =>
                    array (
                        'id' => 286,
                        'characteristic' => 'H',
                        'prev_rating' => 61,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                286 =>
                    array (
                        'id' => 287,
                        'characteristic' => 'H',
                        'prev_rating' => 62,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                287 =>
                    array (
                        'id' => 288,
                        'characteristic' => 'H',
                        'prev_rating' => 63,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                288 =>
                    array (
                        'id' => 289,
                        'characteristic' => 'H',
                        'prev_rating' => 64,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                289 =>
                    array (
                        'id' => 290,
                        'characteristic' => 'H',
                        'prev_rating' => 65,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                290 =>
                    array (
                        'id' => 291,
                        'characteristic' => 'H',
                        'prev_rating' => 66,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                291 =>
                    array (
                        'id' => 292,
                        'characteristic' => 'H',
                        'prev_rating' => 67,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                292 =>
                    array (
                        'id' => 293,
                        'characteristic' => 'H',
                        'prev_rating' => 68,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                293 =>
                    array (
                        'id' => 294,
                        'characteristic' => 'H',
                        'prev_rating' => 69,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                294 =>
                    array (
                        'id' => 295,
                        'characteristic' => 'H',
                        'prev_rating' => 70,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                295 =>
                    array (
                        'id' => 296,
                        'characteristic' => 'H',
                        'prev_rating' => 71,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                296 =>
                    array (
                        'id' => 297,
                        'characteristic' => 'H',
                        'prev_rating' => 72,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                297 =>
                    array (
                        'id' => 298,
                        'characteristic' => 'H',
                        'prev_rating' => 73,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                298 =>
                    array (
                        'id' => 299,
                        'characteristic' => 'H',
                        'prev_rating' => 74,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                299 =>
                    array (
                        'id' => 300,
                        'characteristic' => 'H',
                        'prev_rating' => 75,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                300 =>
                    array (
                        'id' => 301,
                        'characteristic' => 'I',
                        'prev_rating' => 1,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                301 =>
                    array (
                        'id' => 302,
                        'characteristic' => 'I',
                        'prev_rating' => 2,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                302 =>
                    array (
                        'id' => 303,
                        'characteristic' => 'I',
                        'prev_rating' => 3,
                        'rating' => 5,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                303 =>
                    array (
                        'id' => 304,
                        'characteristic' => 'I',
                        'prev_rating' => 4,
                        'rating' => 7,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                304 =>
                    array (
                        'id' => 305,
                        'characteristic' => 'I',
                        'prev_rating' => 5,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                305 =>
                    array (
                        'id' => 306,
                        'characteristic' => 'I',
                        'prev_rating' => 6,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                306 =>
                    array (
                        'id' => 307,
                        'characteristic' => 'I',
                        'prev_rating' => 7,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                307 =>
                    array (
                        'id' => 308,
                        'characteristic' => 'I',
                        'prev_rating' => 8,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                308 =>
                    array (
                        'id' => 309,
                        'characteristic' => 'I',
                        'prev_rating' => 9,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                309 =>
                    array (
                        'id' => 310,
                        'characteristic' => 'I',
                        'prev_rating' => 10,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                310 =>
                    array (
                        'id' => 311,
                        'characteristic' => 'I',
                        'prev_rating' => 11,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                311 =>
                    array (
                        'id' => 312,
                        'characteristic' => 'I',
                        'prev_rating' => 12,
                        'rating' => 17,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                312 =>
                    array (
                        'id' => 313,
                        'characteristic' => 'I',
                        'prev_rating' => 13,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                313 =>
                    array (
                        'id' => 314,
                        'characteristic' => 'I',
                        'prev_rating' => 14,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                314 =>
                    array (
                        'id' => 315,
                        'characteristic' => 'I',
                        'prev_rating' => 15,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                315 =>
                    array (
                        'id' => 316,
                        'characteristic' => 'I',
                        'prev_rating' => 16,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                316 =>
                    array (
                        'id' => 317,
                        'characteristic' => 'I',
                        'prev_rating' => 17,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                317 =>
                    array (
                        'id' => 318,
                        'characteristic' => 'I',
                        'prev_rating' => 18,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                318 =>
                    array (
                        'id' => 319,
                        'characteristic' => 'I',
                        'prev_rating' => 19,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                319 =>
                    array (
                        'id' => 320,
                        'characteristic' => 'I',
                        'prev_rating' => 20,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                320 =>
                    array (
                        'id' => 321,
                        'characteristic' => 'I',
                        'prev_rating' => 21,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                321 =>
                    array (
                        'id' => 322,
                        'characteristic' => 'I',
                        'prev_rating' => 22,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                322 =>
                    array (
                        'id' => 323,
                        'characteristic' => 'I',
                        'prev_rating' => 23,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                323 =>
                    array (
                        'id' => 324,
                        'characteristic' => 'I',
                        'prev_rating' => 24,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                324 =>
                    array (
                        'id' => 325,
                        'characteristic' => 'I',
                        'prev_rating' => 25,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                325 =>
                    array (
                        'id' => 326,
                        'characteristic' => 'I',
                        'prev_rating' => 51,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                326 =>
                    array (
                        'id' => 327,
                        'characteristic' => 'I',
                        'prev_rating' => 52,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                327 =>
                    array (
                        'id' => 328,
                        'characteristic' => 'I',
                        'prev_rating' => 53,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                328 =>
                    array (
                        'id' => 329,
                        'characteristic' => 'I',
                        'prev_rating' => 54,
                        'rating' => 63,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                329 =>
                    array (
                        'id' => 330,
                        'characteristic' => 'I',
                        'prev_rating' => 55,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                330 =>
                    array (
                        'id' => 331,
                        'characteristic' => 'I',
                        'prev_rating' => 56,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                331 =>
                    array (
                        'id' => 332,
                        'characteristic' => 'I',
                        'prev_rating' => 57,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                332 =>
                    array (
                        'id' => 333,
                        'characteristic' => 'I',
                        'prev_rating' => 58,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                333 =>
                    array (
                        'id' => 334,
                        'characteristic' => 'I',
                        'prev_rating' => 59,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                334 =>
                    array (
                        'id' => 335,
                        'characteristic' => 'I',
                        'prev_rating' => 60,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                335 =>
                    array (
                        'id' => 336,
                        'characteristic' => 'I',
                        'prev_rating' => 61,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                336 =>
                    array (
                        'id' => 337,
                        'characteristic' => 'I',
                        'prev_rating' => 62,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                337 =>
                    array (
                        'id' => 338,
                        'characteristic' => 'I',
                        'prev_rating' => 63,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                338 =>
                    array (
                        'id' => 339,
                        'characteristic' => 'I',
                        'prev_rating' => 64,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                339 =>
                    array (
                        'id' => 340,
                        'characteristic' => 'I',
                        'prev_rating' => 65,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                340 =>
                    array (
                        'id' => 341,
                        'characteristic' => 'I',
                        'prev_rating' => 66,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                341 =>
                    array (
                        'id' => 342,
                        'characteristic' => 'I',
                        'prev_rating' => 67,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                342 =>
                    array (
                        'id' => 343,
                        'characteristic' => 'I',
                        'prev_rating' => 68,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                343 =>
                    array (
                        'id' => 344,
                        'characteristic' => 'I',
                        'prev_rating' => 69,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                344 =>
                    array (
                        'id' => 345,
                        'characteristic' => 'I',
                        'prev_rating' => 70,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                345 =>
                    array (
                        'id' => 346,
                        'characteristic' => 'I',
                        'prev_rating' => 71,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                346 =>
                    array (
                        'id' => 347,
                        'characteristic' => 'I',
                        'prev_rating' => 72,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                347 =>
                    array (
                        'id' => 348,
                        'characteristic' => 'I',
                        'prev_rating' => 73,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                348 =>
                    array (
                        'id' => 349,
                        'characteristic' => 'I',
                        'prev_rating' => 74,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                349 =>
                    array (
                        'id' => 350,
                        'characteristic' => 'I',
                        'prev_rating' => 75,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                350 =>
                    array (
                        'id' => 351,
                        'characteristic' => 'J',
                        'prev_rating' => 1,
                        'rating' => 2,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                351 =>
                    array (
                        'id' => 352,
                        'characteristic' => 'J',
                        'prev_rating' => 2,
                        'rating' => 4,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                352 =>
                    array (
                        'id' => 353,
                        'characteristic' => 'J',
                        'prev_rating' => 3,
                        'rating' => 6,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                353 =>
                    array (
                        'id' => 354,
                        'characteristic' => 'J',
                        'prev_rating' => 4,
                        'rating' => 8,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                354 =>
                    array (
                        'id' => 355,
                        'characteristic' => 'J',
                        'prev_rating' => 5,
                        'rating' => 9,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                355 =>
                    array (
                        'id' => 356,
                        'characteristic' => 'J',
                        'prev_rating' => 6,
                        'rating' => 11,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                356 =>
                    array (
                        'id' => 357,
                        'characteristic' => 'J',
                        'prev_rating' => 7,
                        'rating' => 12,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                357 =>
                    array (
                        'id' => 358,
                        'characteristic' => 'J',
                        'prev_rating' => 8,
                        'rating' => 14,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                358 =>
                    array (
                        'id' => 359,
                        'characteristic' => 'J',
                        'prev_rating' => 9,
                        'rating' => 15,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                359 =>
                    array (
                        'id' => 360,
                        'characteristic' => 'J',
                        'prev_rating' => 10,
                        'rating' => 16,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                360 =>
                    array (
                        'id' => 361,
                        'characteristic' => 'J',
                        'prev_rating' => 11,
                        'rating' => 18,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                361 =>
                    array (
                        'id' => 362,
                        'characteristic' => 'J',
                        'prev_rating' => 12,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                362 =>
                    array (
                        'id' => 363,
                        'characteristic' => 'J',
                        'prev_rating' => 13,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                363 =>
                    array (
                        'id' => 364,
                        'characteristic' => 'J',
                        'prev_rating' => 14,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                364 =>
                    array (
                        'id' => 365,
                        'characteristic' => 'J',
                        'prev_rating' => 15,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                365 =>
                    array (
                        'id' => 366,
                        'characteristic' => 'J',
                        'prev_rating' => 16,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                366 =>
                    array (
                        'id' => 367,
                        'characteristic' => 'J',
                        'prev_rating' => 17,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                367 =>
                    array (
                        'id' => 368,
                        'characteristic' => 'J',
                        'prev_rating' => 18,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                368 =>
                    array (
                        'id' => 369,
                        'characteristic' => 'J',
                        'prev_rating' => 19,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                369 =>
                    array (
                        'id' => 370,
                        'characteristic' => 'J',
                        'prev_rating' => 20,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                370 =>
                    array (
                        'id' => 371,
                        'characteristic' => 'J',
                        'prev_rating' => 21,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                371 =>
                    array (
                        'id' => 372,
                        'characteristic' => 'J',
                        'prev_rating' => 22,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                372 =>
                    array (
                        'id' => 373,
                        'characteristic' => 'J',
                        'prev_rating' => 23,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                373 =>
                    array (
                        'id' => 374,
                        'characteristic' => 'J',
                        'prev_rating' => 24,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                374 =>
                    array (
                        'id' => 375,
                        'characteristic' => 'J',
                        'prev_rating' => 25,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                375 =>
                    array (
                        'id' => 376,
                        'characteristic' => 'J',
                        'prev_rating' => 51,
                        'rating' => 64,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                376 =>
                    array (
                        'id' => 377,
                        'characteristic' => 'J',
                        'prev_rating' => 52,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                377 =>
                    array (
                        'id' => 378,
                        'characteristic' => 'J',
                        'prev_rating' => 53,
                        'rating' => 65,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                378 =>
                    array (
                        'id' => 379,
                        'characteristic' => 'J',
                        'prev_rating' => 54,
                        'rating' => 66,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                379 =>
                    array (
                        'id' => 380,
                        'characteristic' => 'J',
                        'prev_rating' => 55,
                        'rating' => 67,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                380 =>
                    array (
                        'id' => 381,
                        'characteristic' => 'J',
                        'prev_rating' => 56,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                381 =>
                    array (
                        'id' => 382,
                        'characteristic' => 'J',
                        'prev_rating' => 57,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                382 =>
                    array (
                        'id' => 383,
                        'characteristic' => 'J',
                        'prev_rating' => 58,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                383 =>
                    array (
                        'id' => 384,
                        'characteristic' => 'J',
                        'prev_rating' => 59,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                384 =>
                    array (
                        'id' => 385,
                        'characteristic' => 'J',
                        'prev_rating' => 60,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                385 =>
                    array (
                        'id' => 386,
                        'characteristic' => 'J',
                        'prev_rating' => 61,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                386 =>
                    array (
                        'id' => 387,
                        'characteristic' => 'J',
                        'prev_rating' => 62,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                387 =>
                    array (
                        'id' => 388,
                        'characteristic' => 'J',
                        'prev_rating' => 63,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                388 =>
                    array (
                        'id' => 389,
                        'characteristic' => 'J',
                        'prev_rating' => 64,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                389 =>
                    array (
                        'id' => 390,
                        'characteristic' => 'J',
                        'prev_rating' => 65,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                390 =>
                    array (
                        'id' => 391,
                        'characteristic' => 'J',
                        'prev_rating' => 66,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                391 =>
                    array (
                        'id' => 392,
                        'characteristic' => 'J',
                        'prev_rating' => 67,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                392 =>
                    array (
                        'id' => 393,
                        'characteristic' => 'J',
                        'prev_rating' => 68,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                393 =>
                    array (
                        'id' => 394,
                        'characteristic' => 'J',
                        'prev_rating' => 69,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                394 =>
                    array (
                        'id' => 395,
                        'characteristic' => 'J',
                        'prev_rating' => 70,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                395 =>
                    array (
                        'id' => 396,
                        'characteristic' => 'J',
                        'prev_rating' => 71,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                396 =>
                    array (
                        'id' => 397,
                        'characteristic' => 'J',
                        'prev_rating' => 72,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                397 =>
                    array (
                        'id' => 398,
                        'characteristic' => 'J',
                        'prev_rating' => 73,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                398 =>
                    array (
                        'id' => 399,
                        'characteristic' => 'J',
                        'prev_rating' => 74,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                399 =>
                    array (
                        'id' => 400,
                        'characteristic' => 'J',
                        'prev_rating' => 75,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                400 =>
                    array (
                        'id' => 401,
                        'characteristic' => 'C',
                        'prev_rating' => 26,
                        'rating' => 19,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                401 =>
                    array (
                        'id' => 402,
                        'characteristic' => 'C',
                        'prev_rating' => 27,
                        'rating' => 20,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                402 =>
                    array (
                        'id' => 403,
                        'characteristic' => 'C',
                        'prev_rating' => 28,
                        'rating' => 21,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                403 =>
                    array (
                        'id' => 404,
                        'characteristic' => 'C',
                        'prev_rating' => 29,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                404 =>
                    array (
                        'id' => 405,
                        'characteristic' => 'C',
                        'prev_rating' => 30,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                405 =>
                    array (
                        'id' => 406,
                        'characteristic' => 'C',
                        'prev_rating' => 31,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                406 =>
                    array (
                        'id' => 407,
                        'characteristic' => 'C',
                        'prev_rating' => 32,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                407 =>
                    array (
                        'id' => 408,
                        'characteristic' => 'C',
                        'prev_rating' => 33,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                408 =>
                    array (
                        'id' => 409,
                        'characteristic' => 'C',
                        'prev_rating' => 34,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                409 =>
                    array (
                        'id' => 410,
                        'characteristic' => 'C',
                        'prev_rating' => 35,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                410 =>
                    array (
                        'id' => 411,
                        'characteristic' => 'C',
                        'prev_rating' => 36,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                411 =>
                    array (
                        'id' => 412,
                        'characteristic' => 'C',
                        'prev_rating' => 37,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                412 =>
                    array (
                        'id' => 413,
                        'characteristic' => 'C',
                        'prev_rating' => 38,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                413 =>
                    array (
                        'id' => 414,
                        'characteristic' => 'C',
                        'prev_rating' => 39,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                414 =>
                    array (
                        'id' => 415,
                        'characteristic' => 'C',
                        'prev_rating' => 40,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                415 =>
                    array (
                        'id' => 416,
                        'characteristic' => 'C',
                        'prev_rating' => 41,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                416 =>
                    array (
                        'id' => 417,
                        'characteristic' => 'C',
                        'prev_rating' => 42,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                417 =>
                    array (
                        'id' => 418,
                        'characteristic' => 'C',
                        'prev_rating' => 43,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                418 =>
                    array (
                        'id' => 419,
                        'characteristic' => 'C',
                        'prev_rating' => 44,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                419 =>
                    array (
                        'id' => 420,
                        'characteristic' => 'C',
                        'prev_rating' => 45,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                420 =>
                    array (
                        'id' => 421,
                        'characteristic' => 'C',
                        'prev_rating' => 46,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                421 =>
                    array (
                        'id' => 422,
                        'characteristic' => 'C',
                        'prev_rating' => 47,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                422 =>
                    array (
                        'id' => 423,
                        'characteristic' => 'C',
                        'prev_rating' => 48,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                423 =>
                    array (
                        'id' => 424,
                        'characteristic' => 'C',
                        'prev_rating' => 49,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                424 =>
                    array (
                        'id' => 425,
                        'characteristic' => 'C',
                        'prev_rating' => 50,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                425 =>
                    array (
                        'id' => 426,
                        'characteristic' => 'C',
                        'prev_rating' => 76,
                        'rating' => 68,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                426 =>
                    array (
                        'id' => 427,
                        'characteristic' => 'C',
                        'prev_rating' => 77,
                        'rating' => 69,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                427 =>
                    array (
                        'id' => 428,
                        'characteristic' => 'C',
                        'prev_rating' => 78,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                428 =>
                    array (
                        'id' => 429,
                        'characteristic' => 'C',
                        'prev_rating' => 79,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                429 =>
                    array (
                        'id' => 430,
                        'characteristic' => 'C',
                        'prev_rating' => 80,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                430 =>
                    array (
                        'id' => 431,
                        'characteristic' => 'C',
                        'prev_rating' => 81,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                431 =>
                    array (
                        'id' => 432,
                        'characteristic' => 'C',
                        'prev_rating' => 82,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                432 =>
                    array (
                        'id' => 433,
                        'characteristic' => 'C',
                        'prev_rating' => 83,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                433 =>
                    array (
                        'id' => 434,
                        'characteristic' => 'C',
                        'prev_rating' => 84,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                434 =>
                    array (
                        'id' => 435,
                        'characteristic' => 'C',
                        'prev_rating' => 85,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                435 =>
                    array (
                        'id' => 436,
                        'characteristic' => 'C',
                        'prev_rating' => 86,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                436 =>
                    array (
                        'id' => 437,
                        'characteristic' => 'C',
                        'prev_rating' => 87,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                437 =>
                    array (
                        'id' => 438,
                        'characteristic' => 'C',
                        'prev_rating' => 88,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                438 =>
                    array (
                        'id' => 439,
                        'characteristic' => 'C',
                        'prev_rating' => 89,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                439 =>
                    array (
                        'id' => 440,
                        'characteristic' => 'C',
                        'prev_rating' => 90,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                440 =>
                    array (
                        'id' => 441,
                        'characteristic' => 'C',
                        'prev_rating' => 91,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                441 =>
                    array (
                        'id' => 442,
                        'characteristic' => 'C',
                        'prev_rating' => 92,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                442 =>
                    array (
                        'id' => 443,
                        'characteristic' => 'C',
                        'prev_rating' => 93,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                443 =>
                    array (
                        'id' => 444,
                        'characteristic' => 'C',
                        'prev_rating' => 94,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                444 =>
                    array (
                        'id' => 445,
                        'characteristic' => 'C',
                        'prev_rating' => 95,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                445 =>
                    array (
                        'id' => 446,
                        'characteristic' => 'C',
                        'prev_rating' => 96,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                446 =>
                    array (
                        'id' => 447,
                        'characteristic' => 'C',
                        'prev_rating' => 97,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                447 =>
                    array (
                        'id' => 448,
                        'characteristic' => 'C',
                        'prev_rating' => 98,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                448 =>
                    array (
                        'id' => 449,
                        'characteristic' => 'C',
                        'prev_rating' => 99,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                449 =>
                    array (
                        'id' => 450,
                        'characteristic' => 'C',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                450 =>
                    array (
                        'id' => 451,
                        'characteristic' => 'D',
                        'prev_rating' => 26,
                        'rating' => 22,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                451 =>
                    array (
                        'id' => 452,
                        'characteristic' => 'D',
                        'prev_rating' => 27,
                        'rating' => 23,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                452 =>
                    array (
                        'id' => 453,
                        'characteristic' => 'D',
                        'prev_rating' => 28,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                453 =>
                    array (
                        'id' => 454,
                        'characteristic' => 'D',
                        'prev_rating' => 29,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                454 =>
                    array (
                        'id' => 455,
                        'characteristic' => 'D',
                        'prev_rating' => 30,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                455 =>
                    array (
                        'id' => 456,
                        'characteristic' => 'D',
                        'prev_rating' => 31,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                456 =>
                    array (
                        'id' => 457,
                        'characteristic' => 'D',
                        'prev_rating' => 32,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                457 =>
                    array (
                        'id' => 458,
                        'characteristic' => 'D',
                        'prev_rating' => 33,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                458 =>
                    array (
                        'id' => 459,
                        'characteristic' => 'D',
                        'prev_rating' => 34,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                459 =>
                    array (
                        'id' => 460,
                        'characteristic' => 'D',
                        'prev_rating' => 35,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                460 =>
                    array (
                        'id' => 461,
                        'characteristic' => 'D',
                        'prev_rating' => 36,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                461 =>
                    array (
                        'id' => 462,
                        'characteristic' => 'D',
                        'prev_rating' => 37,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                462 =>
                    array (
                        'id' => 463,
                        'characteristic' => 'D',
                        'prev_rating' => 38,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                463 =>
                    array (
                        'id' => 464,
                        'characteristic' => 'D',
                        'prev_rating' => 39,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                464 =>
                    array (
                        'id' => 465,
                        'characteristic' => 'D',
                        'prev_rating' => 40,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                465 =>
                    array (
                        'id' => 466,
                        'characteristic' => 'D',
                        'prev_rating' => 41,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                466 =>
                    array (
                        'id' => 467,
                        'characteristic' => 'D',
                        'prev_rating' => 42,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                467 =>
                    array (
                        'id' => 468,
                        'characteristic' => 'D',
                        'prev_rating' => 43,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                468 =>
                    array (
                        'id' => 469,
                        'characteristic' => 'D',
                        'prev_rating' => 44,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                469 =>
                    array (
                        'id' => 470,
                        'characteristic' => 'D',
                        'prev_rating' => 45,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                470 =>
                    array (
                        'id' => 471,
                        'characteristic' => 'D',
                        'prev_rating' => 46,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                471 =>
                    array (
                        'id' => 472,
                        'characteristic' => 'D',
                        'prev_rating' => 47,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                472 =>
                    array (
                        'id' => 473,
                        'characteristic' => 'D',
                        'prev_rating' => 48,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                473 =>
                    array (
                        'id' => 474,
                        'characteristic' => 'D',
                        'prev_rating' => 49,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                474 =>
                    array (
                        'id' => 475,
                        'characteristic' => 'D',
                        'prev_rating' => 50,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                475 =>
                    array (
                        'id' => 476,
                        'characteristic' => 'D',
                        'prev_rating' => 76,
                        'rating' => 70,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                476 =>
                    array (
                        'id' => 477,
                        'characteristic' => 'D',
                        'prev_rating' => 77,
                        'rating' => 71,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                477 =>
                    array (
                        'id' => 478,
                        'characteristic' => 'D',
                        'prev_rating' => 78,
                        'rating' => 72,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                478 =>
                    array (
                        'id' => 479,
                        'characteristic' => 'D',
                        'prev_rating' => 79,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                479 =>
                    array (
                        'id' => 480,
                        'characteristic' => 'D',
                        'prev_rating' => 80,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                480 =>
                    array (
                        'id' => 481,
                        'characteristic' => 'D',
                        'prev_rating' => 81,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                481 =>
                    array (
                        'id' => 482,
                        'characteristic' => 'D',
                        'prev_rating' => 82,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                482 =>
                    array (
                        'id' => 483,
                        'characteristic' => 'D',
                        'prev_rating' => 83,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                483 =>
                    array (
                        'id' => 484,
                        'characteristic' => 'D',
                        'prev_rating' => 84,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                484 =>
                    array (
                        'id' => 485,
                        'characteristic' => 'D',
                        'prev_rating' => 85,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                485 =>
                    array (
                        'id' => 486,
                        'characteristic' => 'D',
                        'prev_rating' => 86,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                486 =>
                    array (
                        'id' => 487,
                        'characteristic' => 'D',
                        'prev_rating' => 87,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                487 =>
                    array (
                        'id' => 488,
                        'characteristic' => 'D',
                        'prev_rating' => 88,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                488 =>
                    array (
                        'id' => 489,
                        'characteristic' => 'D',
                        'prev_rating' => 89,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                489 =>
                    array (
                        'id' => 490,
                        'characteristic' => 'D',
                        'prev_rating' => 90,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                490 =>
                    array (
                        'id' => 491,
                        'characteristic' => 'D',
                        'prev_rating' => 91,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                491 =>
                    array (
                        'id' => 492,
                        'characteristic' => 'D',
                        'prev_rating' => 92,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                492 =>
                    array (
                        'id' => 493,
                        'characteristic' => 'D',
                        'prev_rating' => 93,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                493 =>
                    array (
                        'id' => 494,
                        'characteristic' => 'D',
                        'prev_rating' => 94,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                494 =>
                    array (
                        'id' => 495,
                        'characteristic' => 'D',
                        'prev_rating' => 95,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                495 =>
                    array (
                        'id' => 496,
                        'characteristic' => 'D',
                        'prev_rating' => 96,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                496 =>
                    array (
                        'id' => 497,
                        'characteristic' => 'D',
                        'prev_rating' => 97,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                497 =>
                    array (
                        'id' => 498,
                        'characteristic' => 'D',
                        'prev_rating' => 98,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                498 =>
                    array (
                        'id' => 499,
                        'characteristic' => 'D',
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                499 =>
                    array (
                        'id' => 500,
                        'characteristic' => 'D',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));
            \DB::table('pd_occupation_adjustments')->insert(array (
                0 =>
                    array (
                        'id' => 501,
                        'characteristic' => 'E',
                        'prev_rating' => 26,
                        'rating' => 24,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                1 =>
                    array (
                        'id' => 502,
                        'characteristic' => 'E',
                        'prev_rating' => 27,
                        'rating' => 25,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                2 =>
                    array (
                        'id' => 503,
                        'characteristic' => 'E',
                        'prev_rating' => 28,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                3 =>
                    array (
                        'id' => 504,
                        'characteristic' => 'E',
                        'prev_rating' => 29,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                4 =>
                    array (
                        'id' => 505,
                        'characteristic' => 'E',
                        'prev_rating' => 30,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                5 =>
                    array (
                        'id' => 506,
                        'characteristic' => 'E',
                        'prev_rating' => 31,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                6 =>
                    array (
                        'id' => 507,
                        'characteristic' => 'E',
                        'prev_rating' => 32,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                7 =>
                    array (
                        'id' => 508,
                        'characteristic' => 'E',
                        'prev_rating' => 33,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                8 =>
                    array (
                        'id' => 509,
                        'characteristic' => 'E',
                        'prev_rating' => 34,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                9 =>
                    array (
                        'id' => 510,
                        'characteristic' => 'E',
                        'prev_rating' => 35,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                10 =>
                    array (
                        'id' => 511,
                        'characteristic' => 'E',
                        'prev_rating' => 36,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                11 =>
                    array (
                        'id' => 512,
                        'characteristic' => 'E',
                        'prev_rating' => 37,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                12 =>
                    array (
                        'id' => 513,
                        'characteristic' => 'E',
                        'prev_rating' => 38,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                13 =>
                    array (
                        'id' => 514,
                        'characteristic' => 'E',
                        'prev_rating' => 39,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                14 =>
                    array (
                        'id' => 515,
                        'characteristic' => 'E',
                        'prev_rating' => 40,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                15 =>
                    array (
                        'id' => 516,
                        'characteristic' => 'E',
                        'prev_rating' => 41,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                16 =>
                    array (
                        'id' => 517,
                        'characteristic' => 'E',
                        'prev_rating' => 42,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                17 =>
                    array (
                        'id' => 518,
                        'characteristic' => 'E',
                        'prev_rating' => 43,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                18 =>
                    array (
                        'id' => 519,
                        'characteristic' => 'E',
                        'prev_rating' => 44,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                19 =>
                    array (
                        'id' => 520,
                        'characteristic' => 'E',
                        'prev_rating' => 45,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                20 =>
                    array (
                        'id' => 521,
                        'characteristic' => 'E',
                        'prev_rating' => 46,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                21 =>
                    array (
                        'id' => 522,
                        'characteristic' => 'E',
                        'prev_rating' => 47,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                22 =>
                    array (
                        'id' => 523,
                        'characteristic' => 'E',
                        'prev_rating' => 48,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                23 =>
                    array (
                        'id' => 524,
                        'characteristic' => 'E',
                        'prev_rating' => 49,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                24 =>
                    array (
                        'id' => 525,
                        'characteristic' => 'E',
                        'prev_rating' => 50,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                25 =>
                    array (
                        'id' => 526,
                        'characteristic' => 'E',
                        'prev_rating' => 76,
                        'rating' => 73,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                26 =>
                    array (
                        'id' => 527,
                        'characteristic' => 'E',
                        'prev_rating' => 77,
                        'rating' => 74,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                27 =>
                    array (
                        'id' => 528,
                        'characteristic' => 'E',
                        'prev_rating' => 78,
                        'rating' => 75,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                28 =>
                    array (
                        'id' => 529,
                        'characteristic' => 'E',
                        'prev_rating' => 79,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                29 =>
                    array (
                        'id' => 530,
                        'characteristic' => 'E',
                        'prev_rating' => 80,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                30 =>
                    array (
                        'id' => 531,
                        'characteristic' => 'E',
                        'prev_rating' => 81,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                31 =>
                    array (
                        'id' => 532,
                        'characteristic' => 'E',
                        'prev_rating' => 82,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                32 =>
                    array (
                        'id' => 533,
                        'characteristic' => 'E',
                        'prev_rating' => 83,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                33 =>
                    array (
                        'id' => 534,
                        'characteristic' => 'E',
                        'prev_rating' => 84,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                34 =>
                    array (
                        'id' => 535,
                        'characteristic' => 'E',
                        'prev_rating' => 85,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                35 =>
                    array (
                        'id' => 536,
                        'characteristic' => 'E',
                        'prev_rating' => 86,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                36 =>
                    array (
                        'id' => 537,
                        'characteristic' => 'E',
                        'prev_rating' => 87,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                37 =>
                    array (
                        'id' => 538,
                        'characteristic' => 'E',
                        'prev_rating' => 88,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                38 =>
                    array (
                        'id' => 539,
                        'characteristic' => 'E',
                        'prev_rating' => 89,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                39 =>
                    array (
                        'id' => 540,
                        'characteristic' => 'E',
                        'prev_rating' => 90,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                40 =>
                    array (
                        'id' => 541,
                        'characteristic' => 'E',
                        'prev_rating' => 91,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                41 =>
                    array (
                        'id' => 542,
                        'characteristic' => 'E',
                        'prev_rating' => 92,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                42 =>
                    array (
                        'id' => 543,
                        'characteristic' => 'E',
                        'prev_rating' => 93,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                43 =>
                    array (
                        'id' => 544,
                        'characteristic' => 'E',
                        'prev_rating' => 94,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                44 =>
                    array (
                        'id' => 545,
                        'characteristic' => 'E',
                        'prev_rating' => 95,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                45 =>
                    array (
                        'id' => 546,
                        'characteristic' => 'E',
                        'prev_rating' => 96,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                46 =>
                    array (
                        'id' => 547,
                        'characteristic' => 'E',
                        'prev_rating' => 97,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                47 =>
                    array (
                        'id' => 548,
                        'characteristic' => 'E',
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                48 =>
                    array (
                        'id' => 549,
                        'characteristic' => 'E',
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                49 =>
                    array (
                        'id' => 550,
                        'characteristic' => 'E',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                50 =>
                    array (
                        'id' => 551,
                        'characteristic' => 'F',
                        'prev_rating' => 26,
                        'rating' => 26,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                51 =>
                    array (
                        'id' => 552,
                        'characteristic' => 'F',
                        'prev_rating' => 27,
                        'rating' => 27,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                52 =>
                    array (
                        'id' => 553,
                        'characteristic' => 'F',
                        'prev_rating' => 28,
                        'rating' => 28,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                53 =>
                    array (
                        'id' => 554,
                        'characteristic' => 'F',
                        'prev_rating' => 29,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                54 =>
                    array (
                        'id' => 555,
                        'characteristic' => 'F',
                        'prev_rating' => 30,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                55 =>
                    array (
                        'id' => 556,
                        'characteristic' => 'F',
                        'prev_rating' => 31,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                56 =>
                    array (
                        'id' => 557,
                        'characteristic' => 'F',
                        'prev_rating' => 32,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                57 =>
                    array (
                        'id' => 558,
                        'characteristic' => 'F',
                        'prev_rating' => 33,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                58 =>
                    array (
                        'id' => 559,
                        'characteristic' => 'F',
                        'prev_rating' => 34,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                59 =>
                    array (
                        'id' => 560,
                        'characteristic' => 'F',
                        'prev_rating' => 35,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                60 =>
                    array (
                        'id' => 561,
                        'characteristic' => 'F',
                        'prev_rating' => 36,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                61 =>
                    array (
                        'id' => 562,
                        'characteristic' => 'F',
                        'prev_rating' => 37,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                62 =>
                    array (
                        'id' => 563,
                        'characteristic' => 'F',
                        'prev_rating' => 38,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                63 =>
                    array (
                        'id' => 564,
                        'characteristic' => 'F',
                        'prev_rating' => 39,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                64 =>
                    array (
                        'id' => 565,
                        'characteristic' => 'F',
                        'prev_rating' => 40,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                65 =>
                    array (
                        'id' => 566,
                        'characteristic' => 'F',
                        'prev_rating' => 41,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                66 =>
                    array (
                        'id' => 567,
                        'characteristic' => 'F',
                        'prev_rating' => 42,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                67 =>
                    array (
                        'id' => 568,
                        'characteristic' => 'F',
                        'prev_rating' => 43,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                68 =>
                    array (
                        'id' => 569,
                        'characteristic' => 'F',
                        'prev_rating' => 44,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                69 =>
                    array (
                        'id' => 570,
                        'characteristic' => 'F',
                        'prev_rating' => 45,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                70 =>
                    array (
                        'id' => 571,
                        'characteristic' => 'F',
                        'prev_rating' => 46,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                71 =>
                    array (
                        'id' => 572,
                        'characteristic' => 'F',
                        'prev_rating' => 47,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                72 =>
                    array (
                        'id' => 573,
                        'characteristic' => 'F',
                        'prev_rating' => 48,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                73 =>
                    array (
                        'id' => 574,
                        'characteristic' => 'F',
                        'prev_rating' => 49,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                74 =>
                    array (
                        'id' => 575,
                        'characteristic' => 'F',
                        'prev_rating' => 50,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                75 =>
                    array (
                        'id' => 576,
                        'characteristic' => 'F',
                        'prev_rating' => 76,
                        'rating' => 76,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                76 =>
                    array (
                        'id' => 577,
                        'characteristic' => 'F',
                        'prev_rating' => 77,
                        'rating' => 77,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                77 =>
                    array (
                        'id' => 578,
                        'characteristic' => 'F',
                        'prev_rating' => 78,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                78 =>
                    array (
                        'id' => 579,
                        'characteristic' => 'F',
                        'prev_rating' => 79,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                79 =>
                    array (
                        'id' => 580,
                        'characteristic' => 'F',
                        'prev_rating' => 80,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                80 =>
                    array (
                        'id' => 581,
                        'characteristic' => 'F',
                        'prev_rating' => 81,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                81 =>
                    array (
                        'id' => 582,
                        'characteristic' => 'F',
                        'prev_rating' => 82,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                82 =>
                    array (
                        'id' => 583,
                        'characteristic' => 'F',
                        'prev_rating' => 83,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                83 =>
                    array (
                        'id' => 584,
                        'characteristic' => 'F',
                        'prev_rating' => 84,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                84 =>
                    array (
                        'id' => 585,
                        'characteristic' => 'F',
                        'prev_rating' => 85,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                85 =>
                    array (
                        'id' => 586,
                        'characteristic' => 'F',
                        'prev_rating' => 86,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                86 =>
                    array (
                        'id' => 587,
                        'characteristic' => 'F',
                        'prev_rating' => 87,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                87 =>
                    array (
                        'id' => 588,
                        'characteristic' => 'F',
                        'prev_rating' => 88,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                88 =>
                    array (
                        'id' => 589,
                        'characteristic' => 'F',
                        'prev_rating' => 89,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                89 =>
                    array (
                        'id' => 590,
                        'characteristic' => 'F',
                        'prev_rating' => 90,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                90 =>
                    array (
                        'id' => 591,
                        'characteristic' => 'F',
                        'prev_rating' => 91,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                91 =>
                    array (
                        'id' => 592,
                        'characteristic' => 'F',
                        'prev_rating' => 92,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                92 =>
                    array (
                        'id' => 593,
                        'characteristic' => 'F',
                        'prev_rating' => 93,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                93 =>
                    array (
                        'id' => 594,
                        'characteristic' => 'F',
                        'prev_rating' => 94,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                94 =>
                    array (
                        'id' => 595,
                        'characteristic' => 'F',
                        'prev_rating' => 95,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                95 =>
                    array (
                        'id' => 596,
                        'characteristic' => 'F',
                        'prev_rating' => 96,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                96 =>
                    array (
                        'id' => 597,
                        'characteristic' => 'F',
                        'prev_rating' => 97,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                97 =>
                    array (
                        'id' => 598,
                        'characteristic' => 'F',
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                98 =>
                    array (
                        'id' => 599,
                        'characteristic' => 'F',
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                99 =>
                    array (
                        'id' => 600,
                        'characteristic' => 'F',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                100 =>
                    array (
                        'id' => 601,
                        'characteristic' => 'G',
                        'prev_rating' => 26,
                        'rating' => 29,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                101 =>
                    array (
                        'id' => 602,
                        'characteristic' => 'G',
                        'prev_rating' => 27,
                        'rating' => 30,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                102 =>
                    array (
                        'id' => 603,
                        'characteristic' => 'G',
                        'prev_rating' => 28,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                103 =>
                    array (
                        'id' => 604,
                        'characteristic' => 'G',
                        'prev_rating' => 29,
                        'rating' => 32,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                104 =>
                    array (
                        'id' => 605,
                        'characteristic' => 'G',
                        'prev_rating' => 30,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                105 =>
                    array (
                        'id' => 606,
                        'characteristic' => 'G',
                        'prev_rating' => 31,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                106 =>
                    array (
                        'id' => 607,
                        'characteristic' => 'G',
                        'prev_rating' => 32,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                107 =>
                    array (
                        'id' => 608,
                        'characteristic' => 'G',
                        'prev_rating' => 33,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                108 =>
                    array (
                        'id' => 609,
                        'characteristic' => 'G',
                        'prev_rating' => 34,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                109 =>
                    array (
                        'id' => 610,
                        'characteristic' => 'G',
                        'prev_rating' => 35,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                110 =>
                    array (
                        'id' => 611,
                        'characteristic' => 'G',
                        'prev_rating' => 36,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                111 =>
                    array (
                        'id' => 612,
                        'characteristic' => 'G',
                        'prev_rating' => 37,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                112 =>
                    array (
                        'id' => 613,
                        'characteristic' => 'G',
                        'prev_rating' => 38,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                113 =>
                    array (
                        'id' => 614,
                        'characteristic' => 'G',
                        'prev_rating' => 39,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                114 =>
                    array (
                        'id' => 615,
                        'characteristic' => 'G',
                        'prev_rating' => 40,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                115 =>
                    array (
                        'id' => 616,
                        'characteristic' => 'G',
                        'prev_rating' => 41,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                116 =>
                    array (
                        'id' => 617,
                        'characteristic' => 'G',
                        'prev_rating' => 42,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                117 =>
                    array (
                        'id' => 618,
                        'characteristic' => 'G',
                        'prev_rating' => 43,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                118 =>
                    array (
                        'id' => 619,
                        'characteristic' => 'G',
                        'prev_rating' => 44,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                119 =>
                    array (
                        'id' => 620,
                        'characteristic' => 'G',
                        'prev_rating' => 45,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                120 =>
                    array (
                        'id' => 621,
                        'characteristic' => 'G',
                        'prev_rating' => 46,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                121 =>
                    array (
                        'id' => 622,
                        'characteristic' => 'G',
                        'prev_rating' => 47,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                122 =>
                    array (
                        'id' => 623,
                        'characteristic' => 'G',
                        'prev_rating' => 48,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                123 =>
                    array (
                        'id' => 624,
                        'characteristic' => 'G',
                        'prev_rating' => 49,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                124 =>
                    array (
                        'id' => 625,
                        'characteristic' => 'G',
                        'prev_rating' => 50,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                125 =>
                    array (
                        'id' => 626,
                        'characteristic' => 'G',
                        'prev_rating' => 76,
                        'rating' => 78,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                126 =>
                    array (
                        'id' => 627,
                        'characteristic' => 'G',
                        'prev_rating' => 77,
                        'rating' => 79,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                127 =>
                    array (
                        'id' => 628,
                        'characteristic' => 'G',
                        'prev_rating' => 78,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                128 =>
                    array (
                        'id' => 629,
                        'characteristic' => 'G',
                        'prev_rating' => 79,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                129 =>
                    array (
                        'id' => 630,
                        'characteristic' => 'G',
                        'prev_rating' => 80,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                130 =>
                    array (
                        'id' => 631,
                        'characteristic' => 'G',
                        'prev_rating' => 81,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                131 =>
                    array (
                        'id' => 632,
                        'characteristic' => 'G',
                        'prev_rating' => 82,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                132 =>
                    array (
                        'id' => 633,
                        'characteristic' => 'G',
                        'prev_rating' => 83,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                133 =>
                    array (
                        'id' => 634,
                        'characteristic' => 'G',
                        'prev_rating' => 84,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                134 =>
                    array (
                        'id' => 635,
                        'characteristic' => 'G',
                        'prev_rating' => 85,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                135 =>
                    array (
                        'id' => 636,
                        'characteristic' => 'G',
                        'prev_rating' => 86,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                136 =>
                    array (
                        'id' => 637,
                        'characteristic' => 'G',
                        'prev_rating' => 87,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                137 =>
                    array (
                        'id' => 638,
                        'characteristic' => 'G',
                        'prev_rating' => 88,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                138 =>
                    array (
                        'id' => 639,
                        'characteristic' => 'G',
                        'prev_rating' => 89,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                139 =>
                    array (
                        'id' => 640,
                        'characteristic' => 'G',
                        'prev_rating' => 90,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                140 =>
                    array (
                        'id' => 641,
                        'characteristic' => 'G',
                        'prev_rating' => 91,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                141 =>
                    array (
                        'id' => 642,
                        'characteristic' => 'G',
                        'prev_rating' => 92,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                142 =>
                    array (
                        'id' => 643,
                        'characteristic' => 'G',
                        'prev_rating' => 93,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                143 =>
                    array (
                        'id' => 644,
                        'characteristic' => 'G',
                        'prev_rating' => 94,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                144 =>
                    array (
                        'id' => 645,
                        'characteristic' => 'G',
                        'prev_rating' => 95,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                145 =>
                    array (
                        'id' => 646,
                        'characteristic' => 'G',
                        'prev_rating' => 96,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                146 =>
                    array (
                        'id' => 647,
                        'characteristic' => 'G',
                        'prev_rating' => 97,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                147 =>
                    array (
                        'id' => 648,
                        'characteristic' => 'G',
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                148 =>
                    array (
                        'id' => 649,
                        'characteristic' => 'G',
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                149 =>
                    array (
                        'id' => 650,
                        'characteristic' => 'G',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                150 =>
                    array (
                        'id' => 651,
                        'characteristic' => 'H',
                        'prev_rating' => 26,
                        'rating' => 31,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                151 =>
                    array (
                        'id' => 652,
                        'characteristic' => 'H',
                        'prev_rating' => 27,
                        'rating' => 33,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                152 =>
                    array (
                        'id' => 653,
                        'characteristic' => 'H',
                        'prev_rating' => 28,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                153 =>
                    array (
                        'id' => 654,
                        'characteristic' => 'H',
                        'prev_rating' => 29,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                154 =>
                    array (
                        'id' => 655,
                        'characteristic' => 'H',
                        'prev_rating' => 30,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                155 =>
                    array (
                        'id' => 656,
                        'characteristic' => 'H',
                        'prev_rating' => 31,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                156 =>
                    array (
                        'id' => 657,
                        'characteristic' => 'H',
                        'prev_rating' => 32,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                157 =>
                    array (
                        'id' => 658,
                        'characteristic' => 'H',
                        'prev_rating' => 33,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                158 =>
                    array (
                        'id' => 659,
                        'characteristic' => 'H',
                        'prev_rating' => 34,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                159 =>
                    array (
                        'id' => 660,
                        'characteristic' => 'H',
                        'prev_rating' => 35,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                160 =>
                    array (
                        'id' => 661,
                        'characteristic' => 'H',
                        'prev_rating' => 36,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                161 =>
                    array (
                        'id' => 662,
                        'characteristic' => 'H',
                        'prev_rating' => 37,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                162 =>
                    array (
                        'id' => 663,
                        'characteristic' => 'H',
                        'prev_rating' => 38,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                163 =>
                    array (
                        'id' => 664,
                        'characteristic' => 'H',
                        'prev_rating' => 39,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                164 =>
                    array (
                        'id' => 665,
                        'characteristic' => 'H',
                        'prev_rating' => 40,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                165 =>
                    array (
                        'id' => 666,
                        'characteristic' => 'H',
                        'prev_rating' => 41,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                166 =>
                    array (
                        'id' => 667,
                        'characteristic' => 'H',
                        'prev_rating' => 42,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                167 =>
                    array (
                        'id' => 668,
                        'characteristic' => 'H',
                        'prev_rating' => 43,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                168 =>
                    array (
                        'id' => 669,
                        'characteristic' => 'H',
                        'prev_rating' => 44,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                169 =>
                    array (
                        'id' => 670,
                        'characteristic' => 'H',
                        'prev_rating' => 45,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                170 =>
                    array (
                        'id' => 671,
                        'characteristic' => 'H',
                        'prev_rating' => 46,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                171 =>
                    array (
                        'id' => 672,
                        'characteristic' => 'H',
                        'prev_rating' => 47,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                172 =>
                    array (
                        'id' => 673,
                        'characteristic' => 'H',
                        'prev_rating' => 48,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                173 =>
                    array (
                        'id' => 674,
                        'characteristic' => 'H',
                        'prev_rating' => 49,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                174 =>
                    array (
                        'id' => 675,
                        'characteristic' => 'H',
                        'prev_rating' => 50,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                175 =>
                    array (
                        'id' => 676,
                        'characteristic' => 'H',
                        'prev_rating' => 76,
                        'rating' => 80,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                176 =>
                    array (
                        'id' => 677,
                        'characteristic' => 'H',
                        'prev_rating' => 77,
                        'rating' => 81,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                177 =>
                    array (
                        'id' => 678,
                        'characteristic' => 'H',
                        'prev_rating' => 78,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                178 =>
                    array (
                        'id' => 679,
                        'characteristic' => 'H',
                        'prev_rating' => 79,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                179 =>
                    array (
                        'id' => 680,
                        'characteristic' => 'H',
                        'prev_rating' => 80,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                180 =>
                    array (
                        'id' => 681,
                        'characteristic' => 'H',
                        'prev_rating' => 81,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                181 =>
                    array (
                        'id' => 682,
                        'characteristic' => 'H',
                        'prev_rating' => 82,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                182 =>
                    array (
                        'id' => 683,
                        'characteristic' => 'H',
                        'prev_rating' => 83,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                183 =>
                    array (
                        'id' => 684,
                        'characteristic' => 'H',
                        'prev_rating' => 84,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                184 =>
                    array (
                        'id' => 685,
                        'characteristic' => 'H',
                        'prev_rating' => 85,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                185 =>
                    array (
                        'id' => 686,
                        'characteristic' => 'H',
                        'prev_rating' => 86,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                186 =>
                    array (
                        'id' => 687,
                        'characteristic' => 'H',
                        'prev_rating' => 87,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                187 =>
                    array (
                        'id' => 688,
                        'characteristic' => 'H',
                        'prev_rating' => 88,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                188 =>
                    array (
                        'id' => 689,
                        'characteristic' => 'H',
                        'prev_rating' => 89,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                189 =>
                    array (
                        'id' => 690,
                        'characteristic' => 'H',
                        'prev_rating' => 90,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                190 =>
                    array (
                        'id' => 691,
                        'characteristic' => 'H',
                        'prev_rating' => 91,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                191 =>
                    array (
                        'id' => 692,
                        'characteristic' => 'H',
                        'prev_rating' => 92,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                192 =>
                    array (
                        'id' => 693,
                        'characteristic' => 'H',
                        'prev_rating' => 93,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                193 =>
                    array (
                        'id' => 694,
                        'characteristic' => 'H',
                        'prev_rating' => 94,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                194 =>
                    array (
                        'id' => 695,
                        'characteristic' => 'H',
                        'prev_rating' => 95,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                195 =>
                    array (
                        'id' => 696,
                        'characteristic' => 'H',
                        'prev_rating' => 96,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                196 =>
                    array (
                        'id' => 697,
                        'characteristic' => 'H',
                        'prev_rating' => 97,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                197 =>
                    array (
                        'id' => 698,
                        'characteristic' => 'H',
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                198 =>
                    array (
                        'id' => 699,
                        'characteristic' => 'H',
                        'prev_rating' => 99,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                199 =>
                    array (
                        'id' => 700,
                        'characteristic' => 'H',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                200 =>
                    array (
                        'id' => 701,
                        'characteristic' => 'I',
                        'prev_rating' => 26,
                        'rating' => 34,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                201 =>
                    array (
                        'id' => 702,
                        'characteristic' => 'I',
                        'prev_rating' => 27,
                        'rating' => 35,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                202 =>
                    array (
                        'id' => 703,
                        'characteristic' => 'I',
                        'prev_rating' => 28,
                        'rating' => 36,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                203 =>
                    array (
                        'id' => 704,
                        'characteristic' => 'I',
                        'prev_rating' => 29,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                204 =>
                    array (
                        'id' => 705,
                        'characteristic' => 'I',
                        'prev_rating' => 30,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                205 =>
                    array (
                        'id' => 706,
                        'characteristic' => 'I',
                        'prev_rating' => 31,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                206 =>
                    array (
                        'id' => 707,
                        'characteristic' => 'I',
                        'prev_rating' => 32,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                207 =>
                    array (
                        'id' => 708,
                        'characteristic' => 'I',
                        'prev_rating' => 33,
                        'rating' => 42,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                208 =>
                    array (
                        'id' => 709,
                        'characteristic' => 'I',
                        'prev_rating' => 34,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                209 =>
                    array (
                        'id' => 710,
                        'characteristic' => 'I',
                        'prev_rating' => 35,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                210 =>
                    array (
                        'id' => 711,
                        'characteristic' => 'I',
                        'prev_rating' => 36,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                211 =>
                    array (
                        'id' => 712,
                        'characteristic' => 'I',
                        'prev_rating' => 37,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                212 =>
                    array (
                        'id' => 713,
                        'characteristic' => 'I',
                        'prev_rating' => 38,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                213 =>
                    array (
                        'id' => 714,
                        'characteristic' => 'I',
                        'prev_rating' => 39,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                214 =>
                    array (
                        'id' => 715,
                        'characteristic' => 'I',
                        'prev_rating' => 40,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                215 =>
                    array (
                        'id' => 716,
                        'characteristic' => 'I',
                        'prev_rating' => 41,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                216 =>
                    array (
                        'id' => 717,
                        'characteristic' => 'I',
                        'prev_rating' => 42,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                217 =>
                    array (
                        'id' => 718,
                        'characteristic' => 'I',
                        'prev_rating' => 43,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                218 =>
                    array (
                        'id' => 719,
                        'characteristic' => 'I',
                        'prev_rating' => 44,
                        'rating' => 53,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                219 =>
                    array (
                        'id' => 720,
                        'characteristic' => 'I',
                        'prev_rating' => 45,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                220 =>
                    array (
                        'id' => 721,
                        'characteristic' => 'I',
                        'prev_rating' => 46,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                221 =>
                    array (
                        'id' => 722,
                        'characteristic' => 'I',
                        'prev_rating' => 47,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                222 =>
                    array (
                        'id' => 723,
                        'characteristic' => 'I',
                        'prev_rating' => 48,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                223 =>
                    array (
                        'id' => 724,
                        'characteristic' => 'I',
                        'prev_rating' => 49,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                224 =>
                    array (
                        'id' => 725,
                        'characteristic' => 'I',
                        'prev_rating' => 50,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                225 =>
                    array (
                        'id' => 726,
                        'characteristic' => 'I',
                        'prev_rating' => 76,
                        'rating' => 82,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                226 =>
                    array (
                        'id' => 727,
                        'characteristic' => 'I',
                        'prev_rating' => 77,
                        'rating' => 83,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                227 =>
                    array (
                        'id' => 728,
                        'characteristic' => 'I',
                        'prev_rating' => 78,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                228 =>
                    array (
                        'id' => 729,
                        'characteristic' => 'I',
                        'prev_rating' => 79,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                229 =>
                    array (
                        'id' => 730,
                        'characteristic' => 'I',
                        'prev_rating' => 80,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                230 =>
                    array (
                        'id' => 731,
                        'characteristic' => 'I',
                        'prev_rating' => 81,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                231 =>
                    array (
                        'id' => 732,
                        'characteristic' => 'I',
                        'prev_rating' => 82,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                232 =>
                    array (
                        'id' => 733,
                        'characteristic' => 'I',
                        'prev_rating' => 83,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                233 =>
                    array (
                        'id' => 734,
                        'characteristic' => 'I',
                        'prev_rating' => 84,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                234 =>
                    array (
                        'id' => 735,
                        'characteristic' => 'I',
                        'prev_rating' => 85,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                235 =>
                    array (
                        'id' => 736,
                        'characteristic' => 'I',
                        'prev_rating' => 86,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                236 =>
                    array (
                        'id' => 737,
                        'characteristic' => 'I',
                        'prev_rating' => 87,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                237 =>
                    array (
                        'id' => 738,
                        'characteristic' => 'I',
                        'prev_rating' => 88,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                238 =>
                    array (
                        'id' => 739,
                        'characteristic' => 'I',
                        'prev_rating' => 89,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                239 =>
                    array (
                        'id' => 740,
                        'characteristic' => 'I',
                        'prev_rating' => 90,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                240 =>
                    array (
                        'id' => 741,
                        'characteristic' => 'I',
                        'prev_rating' => 91,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                241 =>
                    array (
                        'id' => 742,
                        'characteristic' => 'I',
                        'prev_rating' => 92,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                242 =>
                    array (
                        'id' => 743,
                        'characteristic' => 'I',
                        'prev_rating' => 93,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                243 =>
                    array (
                        'id' => 744,
                        'characteristic' => 'I',
                        'prev_rating' => 94,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                244 =>
                    array (
                        'id' => 745,
                        'characteristic' => 'I',
                        'prev_rating' => 95,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                245 =>
                    array (
                        'id' => 746,
                        'characteristic' => 'I',
                        'prev_rating' => 96,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                246 =>
                    array (
                        'id' => 747,
                        'characteristic' => 'I',
                        'prev_rating' => 97,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                247 =>
                    array (
                        'id' => 748,
                        'characteristic' => 'I',
                        'prev_rating' => 98,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                248 =>
                    array (
                        'id' => 749,
                        'characteristic' => 'I',
                        'prev_rating' => 99,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                249 =>
                    array (
                        'id' => 750,
                        'characteristic' => 'I',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                250 =>
                    array (
                        'id' => 751,
                        'characteristic' => 'J',
                        'prev_rating' => 26,
                        'rating' => 37,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                251 =>
                    array (
                        'id' => 752,
                        'characteristic' => 'J',
                        'prev_rating' => 27,
                        'rating' => 38,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                252 =>
                    array (
                        'id' => 753,
                        'characteristic' => 'J',
                        'prev_rating' => 28,
                        'rating' => 39,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                253 =>
                    array (
                        'id' => 754,
                        'characteristic' => 'J',
                        'prev_rating' => 29,
                        'rating' => 40,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                254 =>
                    array (
                        'id' => 755,
                        'characteristic' => 'J',
                        'prev_rating' => 30,
                        'rating' => 41,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                255 =>
                    array (
                        'id' => 756,
                        'characteristic' => 'J',
                        'prev_rating' => 31,
                        'rating' => 43,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                256 =>
                    array (
                        'id' => 757,
                        'characteristic' => 'J',
                        'prev_rating' => 32,
                        'rating' => 44,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                257 =>
                    array (
                        'id' => 758,
                        'characteristic' => 'J',
                        'prev_rating' => 33,
                        'rating' => 45,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                258 =>
                    array (
                        'id' => 759,
                        'characteristic' => 'J',
                        'prev_rating' => 34,
                        'rating' => 46,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                259 =>
                    array (
                        'id' => 760,
                        'characteristic' => 'J',
                        'prev_rating' => 35,
                        'rating' => 47,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                260 =>
                    array (
                        'id' => 761,
                        'characteristic' => 'J',
                        'prev_rating' => 36,
                        'rating' => 48,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                261 =>
                    array (
                        'id' => 762,
                        'characteristic' => 'J',
                        'prev_rating' => 37,
                        'rating' => 49,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                262 =>
                    array (
                        'id' => 763,
                        'characteristic' => 'J',
                        'prev_rating' => 38,
                        'rating' => 50,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                263 =>
                    array (
                        'id' => 764,
                        'characteristic' => 'J',
                        'prev_rating' => 39,
                        'rating' => 51,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                264 =>
                    array (
                        'id' => 765,
                        'characteristic' => 'J',
                        'prev_rating' => 40,
                        'rating' => 52,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                265 =>
                    array (
                        'id' => 766,
                        'characteristic' => 'J',
                        'prev_rating' => 41,
                        'rating' => 54,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                266 =>
                    array (
                        'id' => 767,
                        'characteristic' => 'J',
                        'prev_rating' => 42,
                        'rating' => 55,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                267 =>
                    array (
                        'id' => 768,
                        'characteristic' => 'J',
                        'prev_rating' => 43,
                        'rating' => 56,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                268 =>
                    array (
                        'id' => 769,
                        'characteristic' => 'J',
                        'prev_rating' => 44,
                        'rating' => 57,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                269 =>
                    array (
                        'id' => 770,
                        'characteristic' => 'J',
                        'prev_rating' => 45,
                        'rating' => 58,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                270 =>
                    array (
                        'id' => 771,
                        'characteristic' => 'J',
                        'prev_rating' => 46,
                        'rating' => 59,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                271 =>
                    array (
                        'id' => 772,
                        'characteristic' => 'J',
                        'prev_rating' => 47,
                        'rating' => 60,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                272 =>
                    array (
                        'id' => 773,
                        'characteristic' => 'J',
                        'prev_rating' => 48,
                        'rating' => 61,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                273 =>
                    array (
                        'id' => 774,
                        'characteristic' => 'J',
                        'prev_rating' => 49,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                274 =>
                    array (
                        'id' => 775,
                        'characteristic' => 'J',
                        'prev_rating' => 50,
                        'rating' => 62,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                275 =>
                    array (
                        'id' => 776,
                        'characteristic' => 'J',
                        'prev_rating' => 76,
                        'rating' => 84,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                276 =>
                    array (
                        'id' => 777,
                        'characteristic' => 'J',
                        'prev_rating' => 77,
                        'rating' => 85,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                277 =>
                    array (
                        'id' => 778,
                        'characteristic' => 'J',
                        'prev_rating' => 78,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                278 =>
                    array (
                        'id' => 779,
                        'characteristic' => 'J',
                        'prev_rating' => 79,
                        'rating' => 86,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                279 =>
                    array (
                        'id' => 780,
                        'characteristic' => 'J',
                        'prev_rating' => 80,
                        'rating' => 87,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                280 =>
                    array (
                        'id' => 781,
                        'characteristic' => 'J',
                        'prev_rating' => 81,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                281 =>
                    array (
                        'id' => 782,
                        'characteristic' => 'J',
                        'prev_rating' => 82,
                        'rating' => 88,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                282 =>
                    array (
                        'id' => 783,
                        'characteristic' => 'J',
                        'prev_rating' => 83,
                        'rating' => 89,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                283 =>
                    array (
                        'id' => 784,
                        'characteristic' => 'J',
                        'prev_rating' => 84,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                284 =>
                    array (
                        'id' => 785,
                        'characteristic' => 'J',
                        'prev_rating' => 85,
                        'rating' => 90,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                285 =>
                    array (
                        'id' => 786,
                        'characteristic' => 'J',
                        'prev_rating' => 86,
                        'rating' => 91,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                286 =>
                    array (
                        'id' => 787,
                        'characteristic' => 'J',
                        'prev_rating' => 87,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                287 =>
                    array (
                        'id' => 788,
                        'characteristic' => 'J',
                        'prev_rating' => 88,
                        'rating' => 92,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                288 =>
                    array (
                        'id' => 789,
                        'characteristic' => 'J',
                        'prev_rating' => 89,
                        'rating' => 93,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                289 =>
                    array (
                        'id' => 790,
                        'characteristic' => 'J',
                        'prev_rating' => 90,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                290 =>
                    array (
                        'id' => 791,
                        'characteristic' => 'J',
                        'prev_rating' => 91,
                        'rating' => 94,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                291 =>
                    array (
                        'id' => 792,
                        'characteristic' => 'J',
                        'prev_rating' => 92,
                        'rating' => 95,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                292 =>
                    array (
                        'id' => 793,
                        'characteristic' => 'J',
                        'prev_rating' => 93,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                293 =>
                    array (
                        'id' => 794,
                        'characteristic' => 'J',
                        'prev_rating' => 94,
                        'rating' => 96,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                294 =>
                    array (
                        'id' => 795,
                        'characteristic' => 'J',
                        'prev_rating' => 95,
                        'rating' => 97,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                295 =>
                    array (
                        'id' => 796,
                        'characteristic' => 'J',
                        'prev_rating' => 96,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                296 =>
                    array (
                        'id' => 797,
                        'characteristic' => 'J',
                        'prev_rating' => 97,
                        'rating' => 98,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                297 =>
                    array (
                        'id' => 798,
                        'characteristic' => 'J',
                        'prev_rating' => 98,
                        'rating' => 99,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                298 =>
                    array (
                        'id' => 799,
                        'characteristic' => 'J',
                        'prev_rating' => 99,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
                299 =>
                    array (
                        'id' => 800,
                        'characteristic' => 'J',
                        'prev_rating' => 100,
                        'rating' => 100,
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ),
            ));

            $this->enableForeignKeys('pd_occupation_adjustments');

$sql = <<<SQL
DROP TRIGGER IF EXISTS pd_occupation_adjustments_ro ON pd_occupation_adjustments;
create trigger pd_occupation_adjustments_ro before insert or update or delete or truncate on pd_occupation_adjustments for each statement execute procedure readonly_trigger_function();
SQL;
            DB::unprepared($sql);
        }

        
    }
}