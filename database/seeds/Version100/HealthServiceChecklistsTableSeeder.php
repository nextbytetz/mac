<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class HealthServiceChecklistsTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('health_service_checklists');
        $this->delete('health_service_checklists');
        
        \DB::table('health_service_checklists')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Hospitalization/Admission',
                'created_at' => '2017-04-18 19:00:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Treated as outPatient',
                'created_at' => '2017-04-18 19:00:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Laboratory Investigation',
                'created_at' => '2017-04-18 19:00:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Consultation',
                'created_at' => '2017-04-18 19:00:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Surgery',
                'created_at' => '2017-04-18 19:00:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Imaging Services',
                    'created_at' => '2017-04-18 19:00:58',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            6 =>
                array (
                    'id' => 7,
                    'name' => 'Pharmacy and Medical Consumables',
                    'created_at' => '2017-04-18 19:00:58',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            7 =>
                array (
                    'id' => 8,
                    'name' => 'Procedures',
                    'created_at' => '2017-04-18 19:00:58',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            8 =>
                array (
                    'id' => 9,
                    'name' => 'Medical Services',
                    'created_at' => '2017-04-18 19:00:58',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
        ));

        $this->enableForeignKeys('health_service_checklists');
    }
}