<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use Carbon\Carbon;

class InvestmentBudgetCategorySeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->disableForeignKeys('investment_budget_categories');
    	$this->delete('investment_budget_categories');

    	\DB::table('investment_budget_categories')->insert(array (
    		0 =>
    		array (
    			'id' => '1',
    			'name' => 'Planned Investments',
    			'description' => 'Planned investments',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
    		),
    		1 =>
    		array (
                'id' => '2',
                'name' => 'Income from Investments',
                'description' => 'Expected income from investment\'s interest',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
    		2 =>
    		array (
    			'id' => '3',
    			'name' => 'Maturing Investments',
    			'description' => 'Expected income (principal) after investments has reached maturity',
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
    		),
    	));

    	$this->enableForeignKeys('investment_budget_categories');

    }

}
