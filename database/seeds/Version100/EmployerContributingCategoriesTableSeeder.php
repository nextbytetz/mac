<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Operation\Compliance\Member\EmployerSizeType;
use \App\Models\Operation\Compliance\Member\EmployerContributingCategory;

class EmployerContributingCategoriesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    public function run()
    {

//        1
        $code= EmployerContributingCategory::updateOrCreate(
            ['id' => 1],
            ['name' => 'Large',
                'min_contribution' => 1500000,
                'max_contribution' => null,
                'created_at' => '2019-06-04 17:30:35',
            ]
        );

//        2
        $code= EmployerContributingCategory::updateOrCreate(
            ['id' => 2],
            ['name' => 'Medium Plus',
                'min_contribution' => 250000,
                'max_contribution' => 1499999.99,
                'created_at' => '2019-06-04 17:30:35',
            ]
        );


        //        3
        $code= EmployerContributingCategory::updateOrCreate(
            ['id' => 3],
            ['name' => 'Medium',
                'min_contribution' => 100000,
                'max_contribution' => 249999.99,
                'created_at' => '2019-06-04 17:30:35',
            ]
        );


//        4
        $code= EmployerContributingCategory::updateOrCreate(
            ['id' => 4],
            ['name' => 'Small',
                'min_contribution' => 0,
                'max_contribution' => 99999.99,
                'created_at' => '2019-06-04 17:30:35',
            ]
        );


    }

}