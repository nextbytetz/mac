<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class FinAccountCodesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('fin_account_codes');
        $this->delete('fin_account_codes');

        \DB::table('fin_account_codes')->insert(array (

            0 =>
                array (
                    'id' => 1,
                    'name' => 'Temporary Disablement',
                    'account_code' => '5310/000',
                    'created_at' => '2017-07-31 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Permanent Disablement',
                    'account_code' => '5315/000',
                    'created_at' => '2017-07-31 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Medical Aid',
                    'account_code' => '5300/010',
                    'created_at' => '2017-07-31 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

            3 =>
                array (
                    'id' => 4,
                    'name' => 'Funeral Grant',
                    'account_code' => '5350/000',
                    'created_at' => '2017-07-31 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),
            4 =>
                array (
                    'id' => 5,
                    'name' => 'Survivor Benefits',
                    'account_code' => '5360/000',
                    'created_at' => '2017-07-31 07:46:20',
                    'updated_at' => NULL,
                    'deleted_at' => NULL,
                ),

        ));

        $this->enableForeignKeys('fin_account_codes');
    }
}