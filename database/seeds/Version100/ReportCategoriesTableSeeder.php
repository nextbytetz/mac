<?php

use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;
use Database\TruncateTable;

class ReportCategoriesTableSeeder extends Seeder
{

    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->disableForeignKeys('report_categories');
        $this->delete('report_categories');

        \DB::table('report_categories')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Claim',
                    'created_at' => '2017-06-01 17:46:12',
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Finance',
                    'created_at' => '2017-06-01 17:46:12',
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Compliance',
                    'created_at' => '2017-06-01 17:46:20',
                    'updated_at' => NULL,
                ),

            3 =>
                array (
                    'id' => 4,
                    'name' => 'Payroll',
                    'created_at' => '2017-07-17 17:46:20',
                    'updated_at' => NULL,
                ),

            4 =>
                array (
                    'id' => 5,
                    'name' => 'Audit',
                    'created_at' => '2018-01-02 17:46:20',
                    'updated_at' => NULL,
                ),
            5 =>
                array (
                    'id' => 6,
                    'name' => 'Workplace Risk Assessment',
                    'created_at' => '2020-04-05 15:06:20',
                    'updated_at' => NULL,
                ),
           6 =>
                array (
                    'id' => 7,
                    'name' => 'Investment',
                    'created_at' => '2020-04-05 15:06:20',
                    'updated_at' => NULL,
                ),

            7 =>
                array (
                    'id' => 8,
                    'name' => 'External Audit',
                    'created_at' => '2020-04-05 15:06:20',
                    'updated_at' => NULL,
                ),

        ));

        $this->enableForeignKeys('report_categories');
    }

}
