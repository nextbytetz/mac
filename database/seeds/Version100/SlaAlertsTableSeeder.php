<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;
use App\Models\Reporting\SlaAlert;

class SlaAlertsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    public function run()
    {

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAARIFNASA',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 2,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 25,
                'parent_id' => NULL,
                'days_accumulative' => 2,
                'description' => 'Initiate Rejected Incident From Notification Approval Delays',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAAFRWVLDSA',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 2,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 29,
                'parent_id' => NULL,
                'days_accumulative' => 2,
                'description' => 'Forward for Notification Validation Delays',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAAPTCDFNVDTN',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 39,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'PCADO to CADM Delays for Notification Validation',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAACDMDANVDTN',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 39,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'CADM Delays to Approve Notification Validation',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAACDMDANVDTN',
                'sort' => 2,
            ],
            [
                'unit_id' => 11,
                'designation_id' => 1,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify DG',
                'configurable_report_id' => 39,
                'parent_id' => $slaalert->id,
                'days_accumulative' => 2,
                'description' => 'CADM Delays to Approve Notification Validation',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAAHLSUDANV',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 39,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'HLSU Delays to Approve Notification Validation',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADASDANVDTN',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 39,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'DAS Delays to Approve Notification Validation',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAAINTBNFWKDLY',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 2,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 9,
                'parent_id' => NULL,
                'days_accumulative' => 2,
                'description' => 'Officers Delays to Initiate Benefit Workflow',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAAPTCDFBNFWRKLW',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 38,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'PCADO to CADM Delays for Benefit Workflow',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLACDODLTCBNAWRDLTR',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 2,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 38,
                'parent_id' => NULL,
                'days_accumulative' => 2,
                'description' => 'CADO Delays to Create Benefit Award Letters',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAACDMDABNFWRKLW',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 38,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'CADM Delays to Approve Benefit Workflow',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADASDABNFWRWPD',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 10,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 38,
                'parent_id' => NULL,
                'days_accumulative' => 10,
                'description' => 'DAS Delays to Approve Benefit Workflow (PD)',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADASDABNFWRTDMSR',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 5,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 38,
                'parent_id' => NULL,
                'days_accumulative' => 5,
                'description' => 'DAS Delays to Approve Benefit Workflow (TD, MAE, Survivors)',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADFPIDABNFWR',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 3,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 38,
                'parent_id' => NULL,
                'days_accumulative' => 3,
                'description' => 'DFPI Delays to Approve Benefit Workflow',
            ]
        );


        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAAHLSUDABNFWR',
                'sort' => 1,
            ],
            [
                'unit_id' => 9,
                'designation_id' => 3,
                'days_after' => 3,
                'isactive' => 1,
                'action' => 'Notify DO',
                'configurable_report_id' => 38,
                'parent_id' => NULL,
                'days_accumulative' => 3,
                'description' => 'HLSU Delays to Approve Benefit Workflow',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADLYTCACLTR',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 2,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 69,
                'parent_id' => NULL,
                'days_accumulative' => 2,
                'description' => 'Delays to Create Acknowledgement Letter',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAAINRJOTINDLY',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 3,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 25,
                'parent_id' => NULL,
                'days_accumulative' => 3,
                'description' => 'Initiate Rejected Out of Time Incident Delays',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADLYRONNTAPLCTN',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 59,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'Delays to Review New Online Notification Application',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADLYAPVNTAPLCTN',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 1,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 41,
                'parent_id' => NULL,
                'days_accumulative' => 1,
                'description' => 'Delays to Approve Online Notification Application',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAARODLYDNTNLTR',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 3,
                'isactive' => 1,
                'action' => 'Notify CADM',
                'configurable_report_id' => 44,
                'parent_id' => NULL,
                'days_accumulative' => 3,
                'description' => 'RO Delays to Dispatch Benefit Letters',
            ]
        );

        $slaalert = SlaAlert::query()->updateOrCreate(
            [
                'reference' => 'SLAADLYTCRMNDRLTR',
                'sort' => 1,
            ],
            [
                'unit_id' => 14,
                'designation_id' => 5,
                'days_after' => 2,
                'isactive' => 0,
                'action' => 'Notify CADM',
                'configurable_report_id' => NULL,
                'parent_id' => NULL,
                'days_accumulative' => 2,
                'description' => 'Delays to Create Reminder Letter',
            ]
        );


    }

}