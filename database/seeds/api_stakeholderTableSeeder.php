<?php

use Illuminate\Database\Seeder;

class api_stakeholderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('api_stakeholders')->truncate();

       \DB::table('api_stakeholders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'shid' => 'SHID100',
                'shdescription'=>'Internal Systems',
                'ip_address' => '41.59.81.179',
                'created_at' =>'2019-12-27 15:13:00',
                'updated_at' =>'2019-12-27 15:13:00',
            ),
            1 => 
            array (
                'id' => 2,
                'shid' => 'SHID101',
                'shdescription'=>'Tanzania Revenue Authority',
                'ip_address' => '41.59.81.179',
                'created_at' =>'2019-12-27 15:13:00',
                'updated_at' =>'2019-12-27 15:13:00',
            ),
            2 => 
            array (
               'id' => 3,
                'shid' => 'SHID102',
                'shdescription'=>'PMO-LYED',
                'ip_address' => '41.59.81.179',
                'created_at' =>'2019-12-27 15:13:00',
                'updated_at' =>'2019-12-27 15:13:00',
            ),
            3 => 
            array (
               'id' => 4,
                'shid' => 'SHID103',
                'shdescription'=>'National Health Insurance Fund-NHIF',
                'ip_address' => '41.59.81.179',
                'created_at' =>'2019-12-27 15:13:00',
                'updated_at' =>'2019-12-27 15:13:00',
            ),
            4 => 
            array (
               'id' => 5,
                'shid' => 'SHID104',
                'shdescription'=>'National Social Security Fund-NSSF',
                'ip_address' => '41.59.81.179',
                'created_at' =>'2019-12-27 15:13:00',
                'updated_at' =>'2019-12-27 15:13:00',
            ),
     )); 




    }
}
