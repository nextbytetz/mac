<?php

$factory->define(\App\Models\Operation\Compliance\Member\ContributionTemp::class, function (Faker\Generator $faker) {
    $grosspay = $faker->randomFloat(2, 0, 15000000);
    return [
        'employeeno' => $faker->numberBetween(1, 100),
        'firstname' => $faker->firstName(),
        'middlename' => $faker->firstNameMale,
        'lastname' => $faker->lastName,
        'basicpay' => $grosspay * 0.754,
        'grosspay' => $grosspay,
        'dob' => $faker->date('Y-m-d', '1991-12-31'),
        'receipt_code_id' => $faker->numberBetween(1, 1000),
    ];

});