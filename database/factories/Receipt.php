<?php

$factory->define(\App\Models\Finance\Receipt\Receipt::class, function (Faker\Generator $faker) {

    return [
        'rctno' => $faker->numberBetween(1, 100000),
        'employer_id' => $faker->numberBetween(20, 30),
        'bank_reference' => $faker->swiftBicNumber,
        'rct_date' => $faker->date(),
        'amount' => $faker->randomFloat(2, 0, 1440000000),
        'currency_id' => 1,
        'payment_type_id' => $faker->numberBetween(1, 4),
        'user_id' => $faker->numberBetween(20, 34),
        'office_id' => 1,
        'bank_id' => $faker->numberBetween(1, 8),
    ];

});