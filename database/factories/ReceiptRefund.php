<?php

$factory->define(\App\Models\Finance\Receipt\ReceiptRefund::class, function (Faker\Generator $faker) {
    $receipt = \App\Models\Finance\Receipt\Receipt::query()->where(['isdishonoured' => 0, 'iscancelled' => 0, 'isverified' => 1])->get()->random();
    return [
        'receipt_id' => $receipt->id,
        'shouldpay' => $faker->numberBetween(0, 1),
        'amount' =>  $faker->randomElement([$receipt->amount, (int) $receipt->amount/2, (int) $receipt->amount/3, (int) $receipt->amount/4]),
    ];

});