<?php

$factory->define(\App\Models\Finance\Receipt\ReceiptCode::class, function (Faker\Generator $faker) {
/*
    return [
        'receipt_id' => function () {
            return factory(\App\Models\Finance\Receipt\Receipt::class)->create()->id;
        },
        'fin_code_id' => $faker->numberBetween(1, 2),
        'amount' => $faker->randomFloat(2, 0, 120000000),
        'member_count' => $faker->numberBetween(1, 900),
        'contrib_month' => $faker->date(),
        'booking_id' => function () {
            return factory(\App\Models\Finance\Receivable\Booking::class)->create()->id;
        },
    ];
*/

    return [
        'receipt_id' => $faker->numberBetween(1, 1000),
        'fin_code_id' => $faker->numberBetween(1, 2),
        'amount' => $faker->randomFloat(2, 0, 120000000),
        'member_count' => $faker->numberBetween(1, 900),
        'contrib_month' => $faker->date(),
        'booking_id' => $faker->numberBetween(1, 1000),
    ];

});