<?php

$factory->define(\App\Models\Finance\Receivable\Booking::class, function (Faker\Generator $faker) {

    return [
        'rcv_date' => $faker->date(),
        'employer_id' => $faker->numberBetween(20, 30),
        'fin_code_id' => $faker->numberBetween(1, 2),
        'amount' => $faker->randomFloat(2, 0, 120000000),
        'member_count' => $faker->numberBetween(1, 900),
    ];

});