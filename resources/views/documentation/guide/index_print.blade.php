@extends('layouts/backend/user_guide', ['title' => trans('labels.backend.documentation.guide.title')])

@section('content')

    <div id="content">
        <div class="bg-content-top">
            <div class="container">
                <div class="row">

                    <div class="span12">
                        <div class="box-content">
                            @include("documentation/guide/includes/content")
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection