@extends('layouts/backend/user_guide', ['title' => trans('labels.backend.documentation.guide.title')])

@section('content')
    @include("documentation/guide/includes/header")

    <div id="content">
        <div class="bg-content-top">
            <div class="container">
                <div class="row">

                    <div class="span3">
                        @include("documentation/guide/includes/menu")
                    </div>

                    <div class="span9">
                        <div class="box-content">
                            @include("documentation/guide/includes/content")
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include("documentation/guide/includes/footer")
@endsection