<section class="block-templates" id="inspection">
    <h2 class="item5"><i class="icon-location-arrow"></i> Inspection </h2>


    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB"> This section will demonstrate how to implement all actions on Inspection Module.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/public/template/assets/nextbyte/guide/media/inspection/inspection_menu_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/inspection_menu_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--i.	PROFILING--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">I.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB"> PROFILING</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This sub section instruct on how to manage employers profiling. </span></p>

{{--profile history--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Profiles History</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To open profile history, click on the link button  &lsquo;<strong><span style="color: #0070c0;">PROFILES&rsquo;</span></strong>  as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--b.	Create New Profile--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Create New Profile</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To create new profile, click on button  &lsquo;<strong><span style="color: #0070c0;">CREATE NEW&rsquo;</span></strong>  as shown on image below.</span></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">CREATE INSPECTION PROFILE</span></strong></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/profile/profile_create_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_create_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Insert all profile information then click button   &lsquo;<strong><span style="color: #0070c0;">CREATE&rsquo;</span></strong>  as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/profile/profile_insert.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_insert.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--c. open Profile dashboard--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Open Profile Dashboard </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To open profile dashboard, click at any position of table row of profile you wish to open as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/profile/profile_select.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_select.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--d.	Modify Profile--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Modify Profile </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To modify existing profile, click on button   &lsquo;<strong><span style="color: #0070c0;">EDIT&rsquo;</span></strong>  as shown on image below.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">EDIT INSPECTION PROFILE</span></strong></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/profile/profile_edit_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_edit_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify profile information and then click button    &lsquo;<strong><span style="color: #0070c0;">UPDATE&rsquo;</span></strong> to update profile information as shown on   image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/profile/profile_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--e.	delete Profile--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">e.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Delete Profile </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To delete/remove existing profile, click on button    &lsquo;<strong><span style="color: #ff0a07;">DELETE&rsquo;</span></strong>  as shown on image below.</span></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">DELETE INSPECTION PROFILE</span></strong></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/profile/profile_delete.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/profile/profile_delete.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>







    {{--II.	INSPECTION--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">II.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB"> INSPECTION</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This sub section instruct on how to manage inspection.  </span></p>

    {{--a.	Create New--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Create New</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To create new inspection, click on link button  &lsquo;<strong><span style="color: #0070c0;">CREATE NEW&rsquo;</span></strong>  as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/create_inspection_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/create_inspection_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Insert all inspection information then click   &lsquo;<strong><span style="color: #0070c0;">CREATE&rsquo;</span></strong>  as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/create_new_inspection.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/create_new_inspection.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--b.	b.	Open Inspection Dashboard--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Open Inspection Dashboard</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To open inspection dashboard, click at any position of table row of inspection you wish to open as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/inspection_select.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/inspection_select.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--c.	c.	Modify Inspection--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Modify Inspection</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To modify existing inspection, click on button    &lsquo;<strong><span style="color: #0070c0;">EDIT&rsquo;</span></strong>  as shown on image below.</span></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">EDIT INSPECTION ENTITY</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/inspection_edit_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/inspection_edit_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify inspection information and then click button   &lsquo;<strong><span style="color: #0070c0;">UPDATE&rsquo;</span></strong>  to update inspection information as shown on   image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/update_inspection.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/update_inspection.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--d.	Complete Inspection--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Complete Inspection</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To complete inspection after all tasks have been successful completed, click on button   &lsquo;<strong><span style="color: #0070c0;">COMPLETE INSPECTION&rsquo;</span></strong>  as shown on image below.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">COMPLETE INSPECTION ENTITY</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/complete_inspection_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/complete_inspection_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--e.	Cancel Inspection--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Cancel Inspection</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To cancel inspection, click on button    &lsquo;<strong><span style="color: #ff0a07;">CANCEL INSPECTION&rsquo;</span></strong>  as shown on image below.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">CANCEL INSPECTION ENTITY</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/cancel_inspection_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/cancel_inspection_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--III.	TASK--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">III.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB"> TASK</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This sub section instruct on how to manage inspection task(s).  </span></p>


    {{--a.	Tasks Hisory--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Tasks History</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Tasks history can be viewed    &lsquo;<strong><span style="color: #0070c0;">INSPECTION PROFILE DASHBOARD&rsquo;</span></strong>  as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_history.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_history.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--b.	Create New Task--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Create New Task</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To create new task, click on button    &lsquo;<strong><span style="color: #0070c0;">NEW TASK&rsquo;</span></strong>  as shown on image below.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">ASSIGN TASK FOR INSPECTION</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/new_task_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/new_task_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Insert all task information then click button    &lsquo;<strong><span style="color: #0070c0;">CREATE&rsquo;</span></strong>  as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_inspection_create.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_inspection_create.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--c. 	Open Task Dashboard--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Open Task Dashboard</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To task profile dashboard, click at any position of table row of task you wish to open as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_select.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_select.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--d. modfiy--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Modify Task</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To modify existing task, click on button     &lsquo;<strong><span style="color: #0070c0;">EDIT&rsquo;</span></strong>  as shown on image below.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">EDIT TASK FOR INSPECTION ENTITY</span></strong></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_edit_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_edit_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">
Modify task information and then click button      &lsquo;<strong><span style="color: #0070c0;">UPDATE&rsquo;</span></strong>  as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--e. Assign Staff--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">e.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Assign Staff</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To assign all staffs selected to this task, click on button     &lsquo;<strong><span style="color: #0070c0;">ASSIGN TASK &rsquo;</span></strong>  as shown on image below. This action will auto assign employers equally to each selected staff in this task.</span></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">ASSIGN STAFF FOR INSPECTION TASK</span></strong></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_assign_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_assign_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--f. f.	Download Employers List--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">f.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Download Employers List</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To download employers list on this task, click on button      &lsquo;<strong><span style="color: #0070c0;">DOWNLOAD EMPLOYERS LIST &rsquo;</span></strong> as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_download_employer_list.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_download_employer_list.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--g.	Update Task  Feedback--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">g.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Update Task Feedback</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To update task for each employer assigned on this task, click on link button as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/updating_task_feedback_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/updating_task_feedback_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Fill information and then click on button      &lsquo;<strong><span style="color: #2e74ff;">UPDATE&rsquo;</span></strong> as shown on images below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/updating_task_feedback.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/updating_task_feedback.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/updating_task_feedback2.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/updating_task_feedback2.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--h.	Delete Tasks--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">h.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Delete Task</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To delete/remove existing task, click on button     &lsquo;<strong><span style="color: #ff0a07;">DELETE&rsquo;</span></strong> as shown on image below.</span></p>



    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">DELETE TASK FOR INSPECTION ENTITY</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/inspection/task/task_delete.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/inspection/task/task_delete.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


</section>