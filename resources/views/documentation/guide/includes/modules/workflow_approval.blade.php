{{--WORKFLOW APPROVE--}}

<section class="block-templates ">
    <h2  id="workflow_approval" class="item7"><i class="icon-list"></i> Workflow Approval</h2>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB"> This section will demonstrate workflow approval actions.</span></p>



    {{--ASSIGN WORKFLOW--}}


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Assigning Workflow </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To assign pending workflow to yourself, click any position on table row as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/assign_workflow.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/assign_workflow.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Click button &lsquo;<strong><span style="color: #0070c0;">YES&rsquo;</span></strong> to assign workflow to yourself as shown on the image below.</span></p>

    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/workflow_assign_action.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/workflow_assign_action.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



{{--APPROVE---------------}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Approve </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To approve pending assigned workflow action, click any position on table row as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/workflow_assigned_pending.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/workflow_assigned_pending.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To approve, select action  &lsquo;<strong><span style="color: #0070c0;">APPROVED&rsquo;</span></strong> , then enter comment if there is any, then click button <strong><span style="color: #0070c0;">UPDATE&rsquo;</span></strong>. </span></p>

    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/approved_worflow.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/approved_worflow.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--REJECT--------}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Reject </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To reject pending assigned workflow action, click any position on table row as shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/workflow_assigned_pending.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/workflow_assigned_pending.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To reject, select action  &lsquo;<strong><span style="color: #0070c0;">REJECTED&rsquo;</span></strong> , then enter comment / reason for rejection, then click button  <strong><span style="color: #0070c0;">UPDATE&rsquo;</span></strong>. </span></p>

    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/rejected_workflow.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/rejected_workflow.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

</section>