
    {{--Register Notification--}}
<section class="block-templates"  >
    <h2  id="notification" class="item7"><i class="icon-plus-sign"></i> Notification </h2>

    <p class="MsoListParagraphCxSpMiddle"><span lang="EN-GB">This section will demonstrate on how to manage notification report and claim administration.</span></p>

    <p class="MsoListParagraphCxSpMiddle"><span lang="EN-GB">&nbsp;</span></p>
    <p id="register_notification"   class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; text-indent: -0.5in;"><!-- [if !supportLists]--><strong><span lang="EN-GB"><span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Register New Notification report</span></strong></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><strong><span lang="EN-GB">&nbsp;</span></strong><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Add new</span></strong></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To add new notification report click on link button &lsquo;<strong><span style="color: #0070c0;">NOTIFICATION&rsquo;</span></strong> as it seen on image below.<br /><br /></span></p>

    {{--Image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/new_notification.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/new_notification.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Select Employee</span></strong></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Search and select employee you wish to register notification report.</span></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB"> <br /></span></p>


    {{--Image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/select_employee.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/select_employee.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    <p class="MsoListParagraph" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">New Notification</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To register new notification click on link button &lsquo;<strong><span style="color: #0070c0;">NEW NOTIFICATION&rsquo;.</span></strong></span></p>

    {{--Image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/employee_profile_new_notification.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/employee_profile_new_notification.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--choose incident type--}}
    <p class="MsoListParagraph" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Choose incident type</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in;"></p>
    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">Choose incident type of notification report; Occupational Accident, Disease, Death.</span></p>


    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_choose_incident.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_choose_incident.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Register--}}
    <p class="MsoListParagraph" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">e.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Register</span></strong></p>
    <p class="MsoListParagraph" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">Register notification report information report and then click button &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> to insert notification report.</span></p>


{{--register accident--}}
    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Register Accident</span></strong></p>

    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_register_accident1.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_register_accident1.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    {{--register dieseas--}}
    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Register Disease</span></strong></p>

    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_register_disease.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_register_disease.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--register death--}}
    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Register Death</span></strong></p>

    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_register_death.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_register_death.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



</section>




{{--Modify Notification--}}
<section class="block-templates "  >
    <h2  id="modify_notification" class="item7"><i class="icon-edit"></i> Modify</h2>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB"><span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Modify notification</span></strong></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To edit / modify notification report, click on link button <strong><span style="color: #0070c0;">MODIFY NOTIFICATION </span></strong>as it shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_modify.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_modify.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify notification information and then click button <strong><span style="color: #0070c0;">SUBMIT</span></strong> to update notification information as it shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_report_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_report_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


</section>


{{--VERIFY--}}

<section class="block-templates ">
    <h2  id="verify_notification" class="item7"><i class="icon-check"></i> Verify</h2>

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB"><span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Verify notification</span></strong></p>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To confirm notification report verification, click <strong><span style="color: #0070c0;">VERIFY</span></strong> link button as it shown on the image below. This is verification of occurrence of incident reported from employer.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_verify.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_verify.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

</section>


{{--ACCIDENT WITNESS--}}

<section class="block-templates ">
    <h2  id="accident_witness" class="item7"><i class="icon-user"></i> Accident Witness</h2>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate on how to manage witness for occupational accident only.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/add_witness_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/add_witness_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


{{--Add new--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Add New</span></strong></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To add new witness click on link button &lsquo;<strong><span style="color: #0070c0;">ADD NEW WITNESS&rsquo; </span></strong>as shown on image above.</span></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB"></span></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Fill accident witness information and then click &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to insert new accident witness.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/add_witness.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/add_witness.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--edit--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Edit </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To edit existing accident witness click on witness you wish to modify his or her information as shown on the image below.</span></p>

{{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/edit_witness.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/edit_witness.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

{{--modify--}}
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify accident witness information and then click &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to update accident witness.</span></p>

{{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/update_witness.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/update_witness.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--delete--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Delete</span></strong></p>
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To remove / delete accident witbess, click button <strong><span style="color: red;">DELETE</span></strong> on edit accident witness page as it shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/delete_witness.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/delete_witness.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


</section>


{{--DEPENDENTS--}}

<section class="block-templates ">
    <h2  id="dependents" class="item7"><i class="icon-user"></i> Dependents</h2>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">This section demonstrate on how to manage dependents relate to employee. From employee profile, dependents can be managed on the tab 'DEPENDENT' as shown on image below. Dependents are added when notification reported (incident) is occupational death or incident with Permanent Total Disablement (PTD) which may require to add Assistant Care </span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/dependent_select.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/dependent_select.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--Add new--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Add New</span></strong></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To add new dependent click on button   ‘ &lsquo;<strong><span style="color: #0070c0;">ADD NEW&rsquo; </span></strong>as shown on image above.</span></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB"></span></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Fill dependent information and then click  &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong>button to insert new dependent.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/dependent_add.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/dependent_add.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/dependent_add_2.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/dependent_add_2.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--edit--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Edit </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To edit existing dependent click on any position of table row of an item you wish to update.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/dependent_edit.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/dependent_edit.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--modify--}}
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify dependent information and then click  &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to update dependent.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/dependent_edit_2.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/dependent_edit_2.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

</section>

{{--BANK DETAILS--}}

<section class="block-templates ">
    <h2  id="bank_details" class="item7"><i class="icon-building"></i> Bank Details</h2>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate on how to manage bank details of all members entitled to be compensated for a particular notification report.</span></p>




    {{--update bank details--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Update</span></strong></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Update bank details on each input field for all members and then click on button   ‘ &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo; </span></strong>as shown on image below.</span></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB"></span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_report_bank_details_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_report_bank_details_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




</section>


{{--INVESTIGATION--}}

<section class="block-templates ">
    <h2  id="investigation_notification" class="item7"><i class="icon-search"></i> Investigation</h2>

    <p class="MsoListParagraph" style=" mso-add-space: auto;"><span lang="EN-GB">After notification report has been registered, Manager will be auto notified to authorize if incident reported must be investigated or otherwise. Following are the actions to be done on this section.</span></p>

{{--investigate--}}
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Investigate</span></strong></p>
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">If incident reported must be investigated, click link button &lsquo;<strong><span style="color: #0070c0;">INVESTIGATE&rsquo; </span></strong>as it shown on the image below.</span></p>
    <p class="MsoListParagraph" style="margin-left: 1.75in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Activate Investigation for Notification report</span></strong></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_investigate.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_investigate.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Asssign Investigator--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Assign Investigators</span></strong></p>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">For notification report which is must be investigated, it will need to assign investigators for that particular investigation. After investigators have been assigned, system notification will be sent to each assigned investigators.<br /><br /></span><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Assign Investigation Officer.<br /><br /></span></strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Click on button </span><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">ASSIGN INVESTIGATORS </span></strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">to assign investigators.</span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_assign_investigators.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_assign_investigators.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoNormal" style="margin-left: 0.75in; text-indent: .5in;"><span lang="EN-GB">Add investigators and then click <strong><span style="color: #0070c0;">SUBMIT</span></strong> to insert new investigators.</span></p>

{{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_insert_investigators.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_insert_investigators.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Change Investigator--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Change Investigator</span></strong></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To change investigator and assign new investigator, click on table row of investigator you wish to change as it is shown on the image below.</span></p>


{{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_change_investigators.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_change_investigators.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">Choose new investigators to change existing investigator and then click button  &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo; </span></strong>as it shown on the image below.</span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_update_investigators.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_update_investigators.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Delete Investigator--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Delete Investigator</span></strong></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To remove / delete assigned investigator for a particular investigation, click button  &lsquo;<strong><span style="color: #ff0a07;">DELETE&rsquo; </span></strong>on change investigator page as it shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_delete_investigators.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_delete_investigators.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

{{--CANCEL INVESTIGATION--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">e.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Cancel Investigation</span></strong></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">For case where investigation was wrongly activated for a particular notification which did not require investigation or other reason(s), Investigation can be cancelled by clicking link button  <strong><span style="color: #ff0a07;">CANCEL INVESTIGATION </span></strong> as it shown on image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_cancel_investigation.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_cancel_investigation.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--Update INVESTIGATION REPORT--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">f.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Update Investigation Report</span></strong></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To update investigation feedbacks click on any row of table as it shown on the image below.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_investigation_select_edit.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_investigation_select_edit.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">Modify investigation feedbacks and then click on button  &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo; </span></strong>as it shown on the image below.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/investigation/notification_investigation_feedback_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/investigation/notification_investigation_feedback_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>
</section>


{{--DOCUMENTATION--}}

<section class="block-templates ">
    <h2  id="documentation_notification" class="item7"><i class="icon-list-alt"></i> Documentation</h2>

    <p class="MsoListParagraphCxSpMiddle"><span lang="EN-GB">This section will demonstrate how to process documents received concerning notification reported.</span></p>


    {{--register--}}
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Register Documents</span></strong></p>
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"></p>

    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To register all documents received, click link button  &lsquo;<strong><span style="color: #0070c0;">DOCUMENT REGISTER &rsquo; </span></strong>as it shown on the image below.</span></p>


        {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/documentation/notification_report_document_register.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/documentation/notification_report_document_register.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">Click on all documents which have already been received and then click   &lsquo;<strong><span style="color: #0070c0;">UPDATE &rsquo; </span></strong>as it shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/documentation/notification_report_document_register_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/documentation/notification_report_document_register_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--Upload document--}}

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Upload Documents</span></strong></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To upload scanned documents which have been received, open tab  &lsquo;<strong><span style="color: #0070c0;">DOCUMENT CENTER &rsquo; </span></strong>, from the document library &lsquo;<strong><span style="color: #0070c0;">1&rsquo; </span></strong> select document folder you wish to upload, then choose scanned document from the source &lsquo;<strong><span style="color: #0070c0;">2 &rsquo; </span></strong> then click button&lsquo;<strong><span style="color: #0070c0;">UPLOAD &rsquo; </span></strong> labelled as &lsquo;<strong><span style="color: #0070c0;">3 &rsquo; </span></strong>’ as it shown on the image below. </span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/documentation/notification_document_upload.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/documentation/notification_document_upload.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Open document--}}

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Open Document</span></strong></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To open document which has been uploaded, drop down document folder then right click on the file you wish to open then click on link button &lsquo;<strong><span style="color: #0070c0;">OPEN &rsquo; </span></strong> as it shown on the image below. </span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/documentation/notification_document_open.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/documentation/notification_document_open.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Download document--}}

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Download Document</span></strong></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To open document which has been uploaded, drop down document folder then right click on the file you wish to download then click on link button &lsquo;<strong><span style="color: #0070c0;">DOWNLOAD &rsquo; </span></strong> as it shown on the image below. </span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/documentation/notification_document_download.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/documentation/notification_document_download.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--Delete document--}}

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">e.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Delete Document</span></strong></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><span lang="EN-GB">To delete document which has been uploaded, drop down document folder then right click on the file you wish to delete then click on link button &lsquo;<strong><span style="color: #ff0a07;">DELETE &rsquo; </span></strong> as it shown on the image below. </span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/documentation/notification_document_delete.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/documentation/notification_document_delete.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>





</section>






{{--MEDICAL EXPENSE--}}

<section class="block-templates ">
    <h2  id="medical_expense" class="item7"><i class="icon-money"></i> Medical Expense</h2>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate on how to add medical expense incurred by all members on the particular notification report.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_expense/notification_report_add_medical_expense.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_expense/notification_report_add_medical_expense.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--Add new--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Add New</span></strong></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To add new medical expense click on button ‘ &lsquo;<strong><span style="color: #0070c0;">ADD MEDICAL EXPENSE&rsquo; </span></strong>as shown on image above.</span></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB"></span></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Fill medical expense information and then click &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to insert new medical expense.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_expense/notification_insert_medical_expense.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_expense/notification_insert_medical_expense.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--edit--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Edit </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To edit existing medical expense click on any position of table row of an item you wish to update.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_expense/notification_edit_medical_expense_arrow.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_expense/notification_edit_medical_expense_arrow.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--modify--}}
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify medical expense information and then click  &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to update medical expense.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_expense/notification_update_medical_expense.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_expense/notification_update_medical_expense.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




</section>



{{--MEDICAL CARE--}}

<section class="block-templates ">
    <h2  id="medical_care" class="item7"><i class="icon-medkit"></i> Medical Care / Referrals</h2>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate on how to manage medical care (health services / referrals) attended by employee for a particular notification report (incident).</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_care/notification_report_medical_care_add.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_care/notification_report_medical_care_add.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--Add new--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Add New</span></strong></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To add new medical care click on button  ‘ &lsquo;<strong><span style="color: #0070c0;">ADD NEW&rsquo; </span></strong>as shown on image above.</span></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB"></span></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Fill health services information and then click  &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong>button to insert new medical care (Health service).</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_care/notification_report_medical_care_insert.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_care/notification_report_medical_care_insert.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--edit--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Edit </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To edit existing medical care click on any position of table row of an item you wish to update.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_care/notification_medical_care_select_edit.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_care/notification_medical_care_select_edit.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--modify--}}
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify medical care information and then click   &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to update medical care.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/medical_care/notification_report_medical_care_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/medical_care/notification_report_medical_care_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

</section>



{{--CURRENT EMPLOYEE STATE--}}

<section class="block-templates ">
    <h2  id="current_employee_state" class="item7"><i class="icon-hospital"></i> Current Employee State</h2>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate on how to manage current employee state (health state / disability state).</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_states.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_states.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--Add new--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Add New</span></strong></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To add current employee state click on button &lsquo;<strong><span style="color: #0070c0;">ADD NEW&rsquo; </span></strong>as shown on image above.</span></p>
    <p class="MsoListParagraphCxSpMiddle" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB"></span></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Fill health services information and then   &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to insert current employee states.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_employee_state_insert1.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_employee_state_insert1.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_employee_state_insert2.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_employee_state_insert2.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    {{--edit--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Edit </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To edit existing current employee state click on any position of table row of an item you wish to update.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/current_employee_state/notification_current_state_select_edit.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/current_employee_state/notification_current_state_select_edit.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    {{--modify--}}
    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify current employee state information and then click   &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> button to update current employee state.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_state_edit.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/current_employee_state/notification_report_current_state_edit.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

</section>




{{--COMPENSATION SUMMARY--}}

<section class="block-templates ">
    <h2  id="compensation_summary" class="item7"><i class="icon-money"></i> Compensation Summary</h2>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">This tab section will display compensation summary; payable amount for each benefit to be compensated for that particular claim, Monthly pension distribution for pensioner and dependents, survivor gratuity distribution, funeral grants distribution and Payment Overview summary after the claim has been approved for payments.</span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/compensation_summary.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/compensation_summary.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




</section>




{{--APPROVE--}}

<section class="block-templates ">
    <h2  id="approve_notification" class="item7"><i class="icon-check"></i> Approve</h2>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">When notification administration is successfully completed, notification will be approved to start claim compensation workflow approval process.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_report_approve.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_report_approve.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


</section>


{{--UNDO--}}

<section class="block-templates ">
    <h2  id="undo_notification" class="item7"><i class="icon-reply"></i> Undo</h2>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">To undo notification report registration, click on link <strong><span style="color: #0070c0;">UNDO&rsquo; </span></strong> as it shown on the image below.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_undo.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_undo.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


</section>



{{--REJECT--}}

<section class="block-templates ">
    <h2  id="reject_notification" class="item7"><i class="icon-remove"></i> Reject</h2>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB">To reject notification report if does not qualify to be compensated, click on link button <strong><span style="color: #ff0a07;">REJECT&rsquo; </span></strong> as it shown on the image below. This action will start workflow rejection approval process to reject notification report.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_administration/notification_reject.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_administration/notification_reject.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


</section>