{{--WORKFLOW APPROVE--}}

<section class="block-templates ">
    <h2  id="claim_assessment" class="item7"><i class="icon-search"></i> Claim Assessment</h2>

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.5in; mso-list: l0 level1 lfo1; tab-stops: 63.0pt;"></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB"> This section will demonstrate actions to be performed at assessment level.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/assessment_level.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/assessment_level.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a></p>


    {{--DOCTORS MODIFICATION--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">i.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Doctor Modification </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">At assessment level, must modify notification report information as shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/notification_doctor_modification.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/notification_doctor_modification.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Modify notification report information and then click &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> as shown on image below.</span></p>

    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/doctor_update.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/doctor_update.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Assessment--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">ii.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Assessment </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate on how to assess current disability employee state (Temporary Disablement) and computation of permanent disablement.</span></p>


{{--assessment link--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Assessment Link </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To navigate to assessment page, click at action button as shown on image below.</span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/assess_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/assess_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    {{--Type of duty--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Type of Duty – Assessment (Temporary Disablement)</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This section is used to fill assessed type of duty (disability states i.e Hospitalization, Excuse from Duty and Light Duties) which are assessed against type of duty (disability states) claimed from WCC-1.</span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/type_of_duty_assessment.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/type_of_duty_assessment.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--c.	Claim – Assessment Expense Overview--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">c.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Claim – Assessment Expense Overview</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This section is display claimed expense summary against assessed expense summary for Medical Expense Aid and Temporary Disablement.</span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/claimed_assement_overview.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/claimed_assement_overview.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--d.	Computation of Permanent Total / Partial Disablement--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">d.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Computation of Permanent Total / Partial Disablement</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate computation of permanent disablement.  To compute permanent disablement compensation, insert percentage of disability (PD%).</span></p>



    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/compute_permanent_disablement.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/compute_permanent_disablement.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    {{--e	Submit--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">e.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Submit</span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To complete assessment, click on button &lsquo;<strong><span style="color: #0070c0;">SUBMIT&rsquo;</span></strong> as shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/claim_assessment/submit_assessment.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/claim_assessment/submit_assessment.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

</section>