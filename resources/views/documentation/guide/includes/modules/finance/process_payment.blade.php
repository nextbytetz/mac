{{--<section class="block-templates ">--}}


{{--</section>--}}

{{--PROCESS PAYMENT--}}

<section  id="process_payment"  class="block-templates ">

      {{--PROCESS PAYMENT--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB"><span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">PROCESS PAYMENT </span></strong></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This section will demonstrate on how to process payments which have finished workflow approval.</span></p>


    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">i.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Process Payment </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This action will process all payments which are not yet processed and already finished workflow approval process. Payments to be processed include; Medical Aid Expense, Temporary Disablement, Permanent Disablement, Funeral Grants and Survivor gratuity benefits.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/process_payment_finance_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/process_payment_finance_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To process payments, click on link button  &lsquo;<strong><span style="color: #0070c0;">PROCESS PAYMENT&rsquo;</span></strong> as shown on the image below.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">PROCESS PAYMENT</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/process_payment_pending.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/process_payment_pending.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>





    {{--ii.	Process Voucher Transactions--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">ii.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Process Voucher Transactions </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This action is used to process payment voucher transactions to create payment voucher for a particular member i.e. Employee, Employer, Dependent and Insurance. Voucher transaction processing is categorized into two as explained below;</span></p>


    {{--a.	Process Voucher Transaction – INDIVIDUAL--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Process Voucher Transaction – INDIVIDUAL </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This action is used to process payment voucher transactions for all payments of employees, employer and dependents. These payments can be processed at any time.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">PROCESS VOUCHER TRANSACTIONS</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/process_voucher_tran_individual.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/process_voucher_tran_individual.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--b.	Process Voucher Transaction – BATCH--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">b.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Process Voucher Transaction – BATCH </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This action is used to process payment voucher transactions for all payments of insurance in batch. These payments will be processed on monthly based (Once per month).</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">PROCESS VOUCHER BATCH</span></strong></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/process_voucher_tran_batch.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/process_voucher_tran_batch.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>





    {{--MAKE PAYMENT--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">iii.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Make Payment </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This action is used to make payment for all pending payment vouchers which are ready to be dispatched.</span></p>

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To make payment, click on action button as shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/make_payment.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/make_payment.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>






    {{--PRINT PAYMENT VOUCHER--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">iv.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Print Payment Voucher </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To print payment voucher, follow the navigation shown on the images below.</span></p>

    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">Navigate to MIS and then on REPORTS section click on link button  &lsquo;<strong><span style="color: #0070c0;">FINANCE&rsquo;</span></strong> </span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/finance_report.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/finance_report.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>


    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To open payment voucher report page, click at any position of table row as shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/payment_voucher_report_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/payment_voucher_report_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">On Payment voucher report page, user can search for specific payment voucher based on status and specific members voucher you wish to print.</span></p>


    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To print payment voucher click on the action button as shown on the image below.</span></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/payment_voucher_report.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/payment_voucher_report.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>




    {{--PAYMENT VOUCHER PRINTOUT--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">v.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Payment Voucher Printout </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/finance/payment_voucher_printout.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/finance/payment_voucher_printout.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>

</section>