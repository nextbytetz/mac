
<section class="block-templates ">


    <h2  id="run_monthly_pension_payroll" class="item7"><i class="icon-suitcase"></i> Payroll</h2>

    <p class="MsoListParagraphCxSpLast" style="margin-left: 0in; mso-add-space: auto;"><span lang="EN-GB"> This section will demonstrate actions to be performed on payroll module.</span></p>



    {{--RUN PAYROLL MONTHLY--}}

    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">i.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Run Monthly Pension Payroll </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">This section demonstrates on how to process monthly pension payroll. This process is used to process monthly payment to all eligible pensioners and dependents.</span></p>


    <p class="MsoListParagraph" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">To open payroll page, click on link button  &lsquo;<strong><span style="color: #0070c0;">RUN MONTHLY PENSION PAYROLL’ &rsquo;</span></strong> from CLAIM MENU as shown on image below.</span></p>

    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/payroll/process_payroll_link.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/payroll/process_payroll_link.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



    {{--Process Payroll--}}
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.5in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"><!-- [if !supportLists]--><strong><span lang="EN-GB">a.<span style="font-variant-numeric: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></strong><!--[endif]--><strong><span lang="EN-GB">Process Payroll </span></strong></p>
    <p class="MsoListParagraphCxSpFirst" style="margin-left: 1.25in; mso-add-space: auto; text-indent: -.25in; mso-list: l0 level1 lfo1;"></p>
    <p class="MsoListParagraphCxSpLast" style="margin-left: 1.25in; mso-add-space: auto;"><span lang="EN-GB">TTo process payroll, insert batch quarter i.e. month(s) which payroll is run, then click button &lsquo;<strong><span style="color: #0070c0;">SUBMIT &rsquo;</span></strong>as shown on image below.</span></p>


    <p class="MsoNormal" style="margin-left: 1.25in;"><strong><span lang="EN-GB">Action Need Permission: </span></strong><strong><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: #0070c0; letter-spacing: 0.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">RUN PAYROLL</span></strong></p>


    {{--image--}}
    <p class="cols-1"><a class="last" href="/mac/public/template/assets/nextbyte/guide/media/payroll/process_payroll.png" data-gal="prettyPhoto"><img src="{{ asset_url() }}/nextbyte/guide/media/payroll/process_payroll.png" width="460" alt="" style="opacity: 1;margin-left: 1.25in;"><span></span></a><br /><br /></p>



</section>