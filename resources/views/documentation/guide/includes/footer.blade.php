<footer>
    <div class="container">
        MAC User Guide<br>
        &copy; <span id="copyright-year"></span> <span id="copyright"> <a href="{!! env('VENDOR_WEB') !!}" target="_blank">{!! env('VENDOR') !!}</a>  </span>
    </div>
</footer>
<div id="back-top">
    <a href="#"><i class="icon-double-angle-up"></i></a>
</div>

<div class="language-modal">
    <div class="modal_bg"></div>
    <div class="modal">
        <div class="modal-header">
            <span class="modal_remove pull-right"><i class="icon-remove"></i></span>
            <h3>Choose language</h3>
        </div>
        <div class="modal-body">
            <ul id="modal_languages" class="nav nav-list bs-docs-sidenav"></ul>
        </div>
    </div>
</div>



@push('after-script-end')

<script  type="text/javascript">


    $(function () {

        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
                //var id = this.id;
                //alert(id);
            } else {
                location.hash = '#' + tab;
            }
        });



    });
</script>;

@endpush