
<div id="nav_container">
    <a href="javascript:;" id="affect_all">
    <span class="expand">
<i class="icon-angle-down"></i>
</span>
        <span class="close">
<i class="icon-angle-up"></i>
</span>
    </a>
    <div class="clearfix"></div>
    <ul class="nav nav-list bs-docs-sidenav" id="nav">
        <li class="nav-item item1">
            <dl class="slide-down">
                <dt><a href="#introduction" class="icon-info-sign">Introduction</a></dt>
                <dd></dd>
            </dl>
        </li>
        {{--ADMINISTRATION--}}
        {{--<li class="nav-item item2">--}}
            {{--<dl class="slide-down">--}}
                {{--<dt><a href="#administration" class="icon-terminal">Administration</a><i class="icon-sample"></i></dt>--}}
                {{--<dd>--}}
                    {{--<ul class="list">--}}
                        {{--<li><a href="#administration_users">Users</a></li>--}}
                        {{--<li><a href="#administration_roles_permissions">Roles and Permissions</a></li>--}}
                        {{--<li><a href="#administration_audits">Audit Trail</a></li>--}}
                    {{--</ul>--}}
                {{--</dd>--}}
            {{--</dl>--}}
        {{--</li>--}}

        <li class="nav-item item7">
            <dl class="slide-down">
                <dt><a href="#notification" class="icon-plus-sign">Notification</a><i class="icon-sample"></i></dt>
                <dd>
                    <ul class="list">
                        <li><a href="#register_notification" >Register New</a></li>
                        <li><a href="#modify_notification" >Modify</a></li>
                        <li><a href="#verify_notification">Verify</a></li>
                        <li><a href="#accident_witness">Accident Witness</a></li>
                        <li><a href="#dependents">Dependents</a></li>
                        <li><a href="#bank_details">Bank Details Update</a></li>
                        <li><a href="#investigation_notification">Investigation</a></li>
                        <li><a href="#documentation_notification">Documentation</a></li>
                        <li><a href="#medical_expense">Medical Expense</a></li>
                        <li><a href="#medical_care">Medical Care / Referrals</a></li>
                        <li><a href="#current_employee_state">Current Employee State</a></li>
                        <li><a href="#compensation_summary">Compensation Summary</a></li>
                        <li><a href="#approve_notification">Approve</a></li>
                        <li><a href="#undo_notification">Undo</a></li>
                        <li><a href="#reject_notification">Reject</a></li>
                    </ul>

                </dd>
            </dl>
        </li>
{{--Workflow approval--}}

        <li class="nav-item">
            <dl class="slide-down">
                <dt><a href="#workflow_approval" class="icon-list">Workflow Approval</a></dt>

            </dl>
        </li>


        {{--Claim Assessment--}}

        <li class="nav-item">
            <dl class="slide-down">
                <dt><a href="#claim_assessment" class="icon-search">Claim Assessment</a></dt>

            </dl>
        </li>

        {{--Finance--}}

        <li class="nav-item">
            <dl class="slide-down">
                <dt><a href="#finance" class="icon-money">Finance</a> <i class="icon-sample"></i></dt>
                <dd>

                    <ul class="list">
                        <li><a href="#receipt">Receipt</a> </li>
                        <li><a href="#process_payment">Process Payment</a> </li>
                    </ul>

                </dd>
            </dl>
        </li>



        {{--PAYROLL--}}

        <li class="nav-item">
            <dl class="slide-down">
                <dt><a href="#payroll" class="icon-suitcase">Payroll</a> <i class="icon-sample"></i></dt>
                <dd>

                    <ul class="list">
                        <li><a href="#run_monthly_pension_payroll">Run Monthly Pension Payroll</a> </li>

                    </ul>

                </dd>
            </dl>
        </li>


        <li class="nav-item">
            <dl class="slide-down">
                <dt><a href="#Compliance" class="icon-check"> Compliance </a> <i class="icon-sample"></i></dt>
                <dd>
                    <ul class="list">
                        <li><a href="#inspection">Inspection</a></li>

                    </ul>
                </dd>
            </dl>
        </li>

        <li class="nav-item item6">
            <dl class="slide-down">
                <dt><a href="#legal" class="icon-legal"> Legal </a> <i class="icon-sample"></i></dt>
                <dd>
                    <ul class="list">
                        <li><a href="#legal_lawyers">Lawyers</a></li>
                        <li><a href="#legal_case_management">Case Management</a></li>
                    </ul>
                </dd>
            </dl>
        </li>


        <li class="nav-item item9">
            <dl class="slide-down">
                <dt><a href="#report" class="icon-list-alt">Report</a></dt>
                <dd></dd>
            </dl>
        </li>
    </ul>
</div>