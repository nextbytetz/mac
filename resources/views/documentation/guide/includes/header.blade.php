<header class="header">
    <div class="container">
        <div class="row">
            <article class="span4"><a class="brand" href="{!! route("backend.home") !!}" target="_blank"></a></article>
            <article class="span6">
                <h1>
                    {{--<img alt="" src="html-icon.png">--}}
                    MAC User Guide
                </h1>
            </article>
            <article class="span2">
                {{--<div id="languages" class="select-menu pull-right">
                    <div class="select-menu_icon"><b>En</b><i class="icon-angle-down"></i></div>
                    <ul class="select-menu_list">
                    </ul>
                </div>--}}
                <div id="versions" class="select-menu pull-right">
                    <div class="select-menu_icon"><b>v1-0</b><i class="icon-angle-down"></i></div>
                    <ul class="select-menu_list">
                        {{--<li><a href="{!! route("guide") !!}"><span>v1-0</span></a></li>--}}
                        <li class="active"><a href="{!! route("guide") !!}"><span>v1-0</span></a></li>
                        {{--<li class="active"><a href="https://www.templatemonster.com/help/quick-start-guide/website-templates/responsive-website-templates-v1-2/"><span>v1-2</span></a></li>--}}
                    </ul>
                </div>
            </article>
        </div>
    </div>
</header>