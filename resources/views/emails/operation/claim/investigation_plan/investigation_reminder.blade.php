<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<style>

	body {
		padding: 0;
		margin: 0;
	}

	.btn {
		display: inline-block;
		padding: 9px 12px;
		padding-top: 7px;
		margin-bottom: 0;
		font-size: 14px;
		line-height: 20px;
		color: #5e5e5e;
		text-align: center;
		vertical-align: middle;
		cursor: pointer;
		background-color: #d1dade;
		-webkit-border-radius: 3px;
		-webkit-border-radius: 3px;
		-webkit-border-radius: 3px;
		background-image: none !important;
		border: none;
		text-shadow: none;
		box-shadow: none;
		transition: all 0.12s linear 0s !important;
		font: 14px/20px "Helvetica Neue",Helvetica,Arial,sans-serif;
	}
	.btn-cons {
		margin-right: 5px;
		min-width: 120px;
		margin-bottom: 8px;
	}
	.btn-default {
		color: #333;
		background-color: #fff;
		border-color: #ccc;
	}
	.btn-primary {
		color: #fff;
		background-color: #428bca;
		border-color: #357ebd;
	}
	.btn-success {
		color: #fff;
		background-color: #5cb85c;
		border-color: #4cae4c;
	}
	.btn-info {
		color: #fff;
		background-color: #5bc0de;
		border-color: #46b8da;
	}
	.btn-warning {
		color: #fff;
		background-color: #f0ad4e;
		border-color: #eea236;
	}
	.btn-danger {
		color: #fff;
		background-color: #d9534f;
		border-color: #d43f3a;
	}
	.btn-white {
		color: #5e5e5e;
		background-color: #fff;
		border: 1px solid #e5e9ec;
	}
	.btn-link, .btn-link:active, .btn-link[disabled], fieldset[disabled] .btn-link {
		background-color: transparent;
		-webkit-box-shadow: none;
		box-shadow: none;
	}
	.btn-link, .btn-link:hover, .btn-link:focus, .btn-link:active {
		border-color: transparent;
	}
	.btn-link {
		color: #5e5e5e;
		background-color: transparent;
		border: none;
	}
	.btn-link, .btn-link:hover, .btn-link:focus, .btn-link:active {
		border-color: transparent;
	}
	.btn-default, .btn-primary, .btn-success, .btn-info, .btn-warning, .btn-danger {
		text-shadow: 0 -1px 0 rgba(0,0,0,0.2);
		-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.15),0 1px 1px rgba(0,0,0,0.075);
		box-shadow: inset 0 1px 0 rgba(255,255,255,0.15),0 1px 1px rgba(0,0,0,0.075);
	}


	html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
	@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
		*[class="table_width_100"] {
			width: 96% !important;
		}
		*[class="border-right_mob"] {
			border-right: 1px solid #dddddd;
		}
		*[class="mob_100"] {
			width: 100% !important;
		}
		*[class="mob_center"] {
			text-align: center !important;
		}
		*[class="mob_center_bl"] {
			float: none !important;
			display: block !important;
			margin: 0px auto;
		}	
		.iage_footer a {
			text-decoration: none;
			color: #929ca8;
		}
		img.mob_display_none {
			width: 0px !important;
			height: 0px !important;
			display: none !important;
		}
		img.mob_width_50 {
			width: 40% !important;
			height: auto !important;
		}
	}
	.table_width_100 {
		width: 680px;
	}
</style>


<div id="mailsub" class="notification" align="center">

	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">
		<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
			<tr><td>
			</td></tr>
			<tr><td align="center" bgcolor="#ffffff">
				<!-- padding -->
				<table width="90%" border="0" cellspacing="0" cellpadding="0">
					<tr><td align="center">
						<a href="{{url('/')}}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; float:left; width:100%; padding:20px;text-align:center; font-size: 13px;">
							<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
								{{-- <img src="#" width="250" alt="WORKERS COMPENSATION FUND" border="0"  /> --}}
								<br>
								<p>
									<strong>MAC SYSTEM</strong>
								</p>
							</font>	
						</a>					
						<br>
					</td>
					<td align="right">

					</td></tr>

					<!--content 1 -->
					<tr><td align="center" bgcolor="#fbfcfd">
						<font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px; padding: 12px;">
							<table width="90%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										Dear <strong>{{$data['name']}}</strong>,<br/><br/>
										{!!$data['message']!!}
										<br><br>
										With Best Regards,
										<br>
										MAC - WCF.
										<br>
									</td>
								</tr>
								<tr>
									<td align="center">
										<div style="line-height: 24px;">
											<a href="{{ route('backend.claim.investigations.profile',$data['plan_id']) }}" target="_blank" class="btn btn-primary block-center">
												Click Here To Review
											</a>
										</div>
										<!-- padding --><div style="height: 60px; line-height: 60px; font-size: 10px;"></div>
									</td>
								</tr>

							</table>
						</font>
					</td></tr>
					<!--content 1 END-->


					<!--footer -->
					<tr><td class="iage_footer" align="center" bgcolor="#ffffff">


						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="center" style="padding:20px;flaot:left;width:100%; text-align:center;">
								<font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
									<span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
										{{date('Y')}} © Workers Compensation Fund.
									</span></font>				
								</td></tr>			
							</table>
						</td></tr>
						<!--footer END-->
						<tr><td>

						</td></tr>
					</table>
				</td></tr>
			</table>
