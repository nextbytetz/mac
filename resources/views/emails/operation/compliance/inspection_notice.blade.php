<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
    <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody>
                <tr>
                    <td style="vertical-align: top; padding-bottom:30px;" align="center">
                        <a href="{{ env("COMPANY_WEB") }}" target="_blank">
                            <img src="http://notification.nextbyte.co.tz/public/images/wcf_big_logo_no_background.png" alt="WCF Logo" style="border:none">
                            <br>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="background:dodgerblue; padding:20px; color:#fff; text-align:center; font-weight: bold;"> Dear {{ucwords($name)}} </td>
                </tr>


            </tbody>
        </table>
        <div style="padding: 40px; background: #fff;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody>
                    <tr>
                        <td>
                            <p>Greeting from <b>WCF</b></p>
                            <p>{{$description}}</p>

                            <p> For more details;
                                <a href="{{$url}}"> Click here </a>
                            </p>

                            <p>If you’re having trouble login to the portal, kindly contact the ICT Unit (Call:111 or email:ictsupport@wcf.go.tz) for support/help.
                            </p>
                            <p> Regards, <br> <strong>Workers Compensation Fund</strong></p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
