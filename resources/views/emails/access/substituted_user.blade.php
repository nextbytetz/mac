<div width="100%" style="background: #4c66a4; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
    <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody>
            <tr>
                <td style="vertical-align: top; padding-bottom:30px;" align="center">
                    <a href="{{ env("APP_URL") }}" target="_blank">
                        <img src="http://notification.nextbyte.co.tz/public/images/mac_icon.png" alt="MAC Logo" style="border:none">
                        <br>
                        {{--<img src="./basic_files/eliteadmin-text-dark.png" alt="Eliteadmin Responsive web app kit" style="border:none">--}}
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
            <tr>
                <td style="background:limegreen; padding:20px; color:#fff; text-align:center; font-weight: bold;"> Assigned to Substitute {{ $substituted->name }} </td>
            </tr>
            </tbody>
        </table>
        <div style="padding: 40px; background: #fff;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td>
                        <p style="font-weight: 700;">{{ $name }}</p>
                        <p>You have been assigned to substitute <span style="font-weight: 600;">{{ $substituted->name }}</span> in MAC system by {{ $substitution->userSet->name }} on <span style="font-weight: 600;">{{ $substitution->from_date_label }}</span>, besides your assigned tasks and workflow(s), you will also be able to perform all tasks assigned and attend workflow(s) pending to the user.</p>
                        <p>To revoke this substitution please contact ICT Unit or user who has substituted you.</p>
                        <center>
                            {!! link_to( env("APP_URL") , "Go to MAC", ['style' => 'display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #4c66a4; border-radius: 60px; text-decoration:none;']) !!}
                        </center>
                        <b>- @lang("emails.thanks") ( @lang("emails.wcf") )</b>  </td>
                </tr>
                </tbody>
            </table>
        </div>
        {{--        <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
                    <p> Powered by Themedesigner.in <br>
                        <a href="javascript: void(0);" style="color: #b2b2b5; text-decoration: underline;">Unsubscribe</a> </p>
                </div>--}}
    </div>
</div>