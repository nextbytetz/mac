<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
    <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody>
            <tr>
                <td style="vertical-align: top; padding-bottom:30px;" align="center">
                    <a href="{{ env("COMPANY_WEB") }}" target="_blank">
                        <img src="http://notification.nextbyte.co.tz/public/images/wcf_big_logo_no_background.png" alt="WCF Logo" style="border:none">
                        <br>
                        {{--<img src="./basic_files/eliteadmin-text-dark.png" alt="Eliteadmin Responsive web app kit" style="border:none">--}}
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
            <tr>
                <td style="background:dodgerblue; padding:20px; color:#fff; text-align:center; font-weight: bold;"> Dear {!! $staff_name  !!} </td>
            </tr>


            </tbody>
        </table>
        <div style="padding: 40px; background: #fff;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td>
                        {{--<p>Please find reminder for follow up done to {!! $employer_name !!} on {!! $date_of_follow_up !!}.</p>--}}
                        @if($elevation_level == 1)
                            <p>This is to inform you that you have missed your daily follow up target for the week from   {!! $start_date !!} to {!! $end_date !!}. Please ensure that you compensate the missing target in your forthcoming follow ups.</p>
                        @elseif($elevation_level >= 2)
                            <p>This is to inform you that you have missed your weekly follow up target for {{ convert_number_to_words($elevation_level)  }} consecutive weeks from {!! $start_date !!} to {!! $end_date !!}. Please ensure that you compensate the missing target in your forthcoming follow ups.</p>
                        @else

                        @endif

                        <p>Regards,</p>
                        <p>MAC - Debt Management Module</p>

                        {{--<p>Assigned Staff: {!! $assigned_staff !!}</p>--}}
                        <b>- @lang("emails.thanks") ( @lang("emails.wcf") )</b>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        {{--        <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
                    <p> Powered by Themedesigner.in <br>
                        <a href="javascript: void(0);" style="color: #b2b2b5; text-decoration: underline;">Unsubscribe</a> </p>
                </div>--}}
    </div>
</div>