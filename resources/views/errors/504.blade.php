@extends('layouts.error', ['title' => trans('http.504.title')])

@section('content')

    <div class="main-contain-error">
        <div class="main-contain-inner-error">
            <div class="error-des text-xs-center">
                <div class="error-image">
                    {{--<img src="{!! asset_url() !!}/pages/error/image/404-image.png" alt="error page"/>--}}

                </div>
                <h1 style="color: red">@lang('http.504.code')</h1>
                <h1>@lang('http.504.title')</h1>
                <p>@lang('http.504.description')</p>
                {{--<form action="#">
                    <div class="search-error">
                        <input type="text" placeholder="Search..." class="input-notify float-xs-left">
                        <button class="btn btn-primary float-md-left" type="submit">Search</button>
                    </div>
                </form>--}}
            </div>
        </div>
    </div>

@stop
