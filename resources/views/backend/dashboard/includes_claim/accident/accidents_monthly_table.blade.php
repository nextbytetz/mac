

{{--<div class="col-md-4 pull-right">--}}
{{--<div class="col-md-12">--}}
<h6>@lang('labels.backend.report.dashboard.claim.accidents_received_monthly')&nbsp;{!! financial_year() !!}</h6>

<table class="table table-striped table-bordered table-fixed" >
    <tbody class="tbody_per_region table_display_block">
    <tr style="background-color: lightskyblue; color: white"   >
        <th style="width:60px">@lang('labels.general.sn')</th>
        <th style="width:150px">@lang('labels.general.month')</th>
        <th style="width:60%">@lang('labels.general.no')</th>

    </tr>

    <?php
    $i = 1;
    ?>

    @foreach($claim_summary['accident_table'] as $value)
        <tr>
            <td>{!! $i   !!}</td>
            <td>{!! $value[0] !!}</td>
            <td>{!! number_0_format( $value[1])!!}</td>
         </tr>

        <?php
        $i++;
        ?>

    @endforeach

    </tbody>
</table>
{{--</div>--}}
{{--</div>--}}
