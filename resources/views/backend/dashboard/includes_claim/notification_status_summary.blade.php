<div class="row">
    {{--left Category summary--}}
    <h4 class="page-content-title float-xs-left "> @lang('labels.backend.report.dashboard.claim.notification_report_status_summary')&nbsp;{!! financial_year() !!}
    </h4>
    <div class="col-md-12">
        <div class="col-md-4">
            <div class="widget-box widget-margin last-widget-v3">
                <div class="static-header bg-success text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.complete') !!}   </h5>
                </div>
                <div class="widget-content text-warning">
                    <div class="static-detail-icon"></div>
                    <div class="text-xs-center"><h4 class="text-success">{!! number_0_format($claim_summary['paid_claim'])   !!}</h4>
                    </div>
                </div>
            </div>
        </div>
        {{--unregistered--}}
        <div class="col-md-4">
            <div class="widget-box widget-margin last-widget-v3">
                <div class="static-header bg-warning text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.pending') !!}   </h5>
                </div>
                <div class="widget-content text-warning">
                    <div class="static-detail-icon"></div>
                    <div class="text-xs-center"><h4 class="text-warning">{!! number_0_format($claim_summary['pending_claim'])   !!}</h4>
                    </div>
                </div>
            </div>
        </div>
        {{--total--}}
        <div class="col-md-4">
            <div class="widget-box widget-margin last-widget-v3">
                <div class="static-header bg-danger text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.rejected') !!}   </h5>
                </div>
                <div class="widget-content text-primary">
                    <div class="static-detail-icon"></div>
                    <div class="text-xs-center"><h4 class="text-danger">{!! number_0_format($claim_summary['rejected_claim'])  !!}</h4>
                    </div>
                </div>
            </div>

        </div>



    </div>
</div>