<div class="row">

    <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard_v4_block2">
        <div class="dashboard_v4_revenue_block" style="background: #f9f9f2 !important;">
            <h6>@lang('labels.backend.report.dashboard.claim.chart') &nbsp;{!! financial_year()
        !!}</h6>
            <div id="notification_received_graph" class="graph_height"></div>
        </div>
    </div>


    @include('backend.dashboard.includes_claim.notification_received.notification_received_table')

</div>
{{--</div>--}}



@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        var $graph_notification = {!! json_encode($claim_summary['graph_notification_received']) !!};
        var $splineData_notification = [{
            data: $graph_notification,
            splines: {
                show: true,
                tension: 0.45,
                lineWidth: 4,
                fill: 0.1
            }
        }, {
            data: $graph_notification,
            bars: {
                show: true,
                barWidth: 0.05,
                align: 'center',
                lineWidth: 0,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            }
        }];

        var $splineOptions_notification = {
            series: {
                points: {
                    show: false
                }
            },
            colors: ['#087380', '#087380'],
            grid: {
                borderColor: '#eee',
                borderWidth: 0,
                hoverable: true,
                clickable: true,
                backgroundColor: 'transparent'
            },
            tooltip: true,
            tooltipOpts: {
                content: function(label, x, y) {
                    return 'Notification Reports : ' + '' + y;
                }
            },
            xaxis: {
                mode: 'categories',
                show: true
            },
            yaxis: {
                /*max: 80,*/
                show: true
            },
            legend: {
                backgroundColor: 'transparent',
                show: true
            },
            shadowSize: 0
        };

        var plot_notification = $('#notification_received_graph').each(function(){
            runFlot(this, $splineData_notification, $splineOptions_notification);
        });
// Common function to run charts
// by reading custom height from data attribute
        function runFlot(el, data, opts) {
            /*console.log(el);*/
            var $el = $(el),
                $height = $el.data('height');
            if ($height) $el.height($height);
            $el.plot(data, opts);
        }
    });
</script>

@endpush