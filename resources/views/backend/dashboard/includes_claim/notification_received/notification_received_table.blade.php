<div class="col-md-4 pull-right">
    <div class="col-md-12">

        <h6>@lang('labels.backend.report.dashboard.claim.chart')&nbsp;{!! financial_year()
        !!}</h6>

        <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
            <tbody class="tbody_per_region table_display_block" >
            <tr style="background-color: lightskyblue; color: white;"   >
                <th  style="width:60px">@lang('labels.general.sn')</th>
                <th  style="width: 250px">@lang('labels.general.month')</th>
                <th  style="width:60%">@lang('labels.general.no')</th>
            </tr>

            <?php
            $i = 1;
            ?>

            @foreach($claim_summary['notification_received_table'] as $key => $value)

                <tr>
                    <td>{!! $i   !!}</td>
                    <td>{!! $value[0] !!}</td>
                    <td>{!!  $value[1]  !!}</td>
                </tr>

                <?php
                $i++;
                ?>

            @endforeach

            </tbody>
        </table>

        <div class="row">
            <div class="content">
                <div class="dashboard_v4_box_icon float-xs-left success_box">
                    <i class="fa fa-money"></i>
                </div>
                <div class="dashboard_v4_box_title float-xs-right">
                    <h4>{!! $claim_summary['overall_total_notification_received'] !!}</h4>
                    <p>@lang('labels.general.total')</p>
                </div>
            </div>
        </div>



    </div>
</div>



