<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h4 class="page-content-title">Status of Claims Processed</h4>
        {{--<p>Create reflow table using <code>.table-reflow</code> class.</p>--}}

        <div class="basic_table table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>&nbsp;<a href="{!! route('backend.report.configurable.refresh') !!}" class="btn btn-sm btn-secondary"><i class="icon fa fa-refresh"></i>&nbsp;Refresh</a></th>
                    @foreach ($claim_summary['fin_years'] as $value)
                        <th>{{ $value['name'] }}</th>
                    @endforeach
                    <th>Grand Total</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($claim_summary['processed_claims'] as $value)
                        <tr @if ($value['isbalance']) style="background: #f5f5f5;" @endif>
                            <td>{{ $value['description'] }}</td>
                            @foreach ($claim_summary['fin_years'] as $value_b)
                                <td>{{ $value['fin_year' . $value_b['id']] }} &nbsp;
                                    @if($value['fin_year' . $value_b['id']])
                                        <span class="basic_table_icon"><a href="{{ route("backend.claim.notification_report.download_configurable_report", ["report_id" => $value['id'], "fin_year_id" => $value_b["id"]]) }}"><i class="icon fa fa-download"></i></a></span>
                                    @endif
                                </td>
                            @endforeach
                            <td>{{ $value['grand_total'] }}
                                @if($value['grand_total'])
                                    <span class="basic_table_icon"><a href="{{ route("backend.claim.notification_report.download_configurable_report", ["report_id" => $value['id'], "fin_year_id" => 0]) }}"><i class="icon fa fa-download"></i></a></span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>