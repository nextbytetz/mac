@include('backend.includes.datatable_assets')

<div class="row">
    <div class="col-md-12">
        <h6>@lang('labels.backend.report.dashboard.claim.pending_claims_summary')</h6>

        <table class="display" id = "pending-claim-table" cellspacing="0" width="100%">
            <thead>
            <tr >
                <th>Case #</th>
                <th>@lang('labels.general.name')</th>
                <th>@lang('labels.backend.claim.incident_type')</th>
                <th>@lang('labels.backend.report.dashboard.claim.pending_level')</th>
                <th>@lang('labels.general.descriptions')</th>
                <th>@lang('labels.general.staff')</th>
                <th>@lang('labels.backend.table.workflow.wf_date')</th>
                <th>@lang('labels.general.aging') (@lang('labels.general.days'))</th>
            </tr>
            </thead>
        </table>

    </div>
</div>
@push('after-script-end')
<script>

    $(function () {

        var oTable = $('#pending-claim-table').DataTable({
            //dom : 'Bfrtip',
            //"dom": 'lBrtip',
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                oTable.buttons().container()
                    .insertBefore('#pending-claim-table');
            },
            processing: true,
            serverSide: true,
            info : true,
            ajax: {
                url: "{!! route("backend.claim.pending_claim_summary.get") !!}",
                type: "post",
                data: function (d) {
                    d.fin_year_id = getUrlVars()["fin_year_id"];
                }
            },
            columns: [
                {data: 'filename', name: 'notification_reports.filename'},
                {data: 'employee', name: 'employee', searchable: false, orderable: false},
                {data: 'incident', name: 'incident_types.name'},
                {data: 'level', name: 'wf_definitions.level'},
                {data: 'description', name: 'wf_definitions.description', searchable: false},
                {data: 'staff', name: 'staff', searchable: false},
                {data: 'receive_date', name: 'wf_tracks.receive_date'},
                {data: 'aging', name: 'resource_id', searchable: false, orderable: false},
                {data: 'resource_id', name: 'resource_id', visible: false, searchable: false, orderable: false},
                {data: 'firstname', name: 'employees.firstname', visible: false},
                {data: 'middlename', name: 'employees.middlename', visible: false},
                {data: 'lastname', name: 'employees.lastname', visible: false},
                {data: 'username', name: 'users.username', visible: false}
            ],
            'rowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    window.open(base_url + "/claim/notification_report/profile/" + aData['resource_id'], "_blank");
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
    });
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            vars[key] = value;
        });
        return vars;
    }
</script>
@endpush