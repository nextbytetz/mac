<div class="row">

    <div class="col-md-12">
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            @include('backend.dashboard.includes_claim.accident.accidents_monthly_table')

            {{--accident--}}
            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left primary_box">
                        <i class="fa fa-wheelchair"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_0_format($claim_summary['accident_overall_total']) !!}</h4>
                        <p>@lang('labels.general.overall_total')</p>
                    </div>
                </div>
            </div>

        </div>

        {{--disease--}}
        <div class="col-md-4">
            @include('backend.dashboard.includes_claim.disease.disease_monthly_table')

            {{--total--}}
            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left primary_box">
                        <i class="fa fa-wheelchair"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_0_format($claim_summary['disease_overall_total']) !!}</h4>
                        <p>@lang('labels.general.overall_total')</p>
                    </div>
                </div>
            </div>

        </div>

        {{--death--}}
        <div class="col-md-4">
            @include('backend.dashboard.includes_claim.death.deaths_monthly_table')

            {{--total--}}
            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left primary_box">
                        <i class="fa fa-wheelchair"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_0_format($claim_summary['death_overall_total']) !!}</h4>
                        <p>@lang('labels.general.overall_total')</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
