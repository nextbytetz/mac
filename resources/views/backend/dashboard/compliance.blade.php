
{{--COMPLIANCE--}}
@if($general_stat == 1)
    @include('backend.dashboard.includes_compliance.summary')
@else
    @include('backend.dashboard.includes_executive_dashboard.default_summary')
@endif