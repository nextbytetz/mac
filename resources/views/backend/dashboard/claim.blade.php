<div class="row">
    <div class="col-md-12 lg-dashboard-v2 " style="padding:0px !important;">
        <div class="content" >
            <div class="dashboard-content">
                <div class="dashboard-header ">
                    <h4 class="page-content-title float-xs-left "> @lang('labels.backend.report.dashboard.claim.title')
                    </h4>
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06" aria-hidden="true"></span></a></li>
                            <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-box claim_summary">
                    {{--CONTENTS--}}

                    {{--Graph for notification received monthly--}}

                    <br>

                    @if(isset($claim_summary['processed_claims']))
                        @include('backend.dashboard.includes_claim.notification_received.general_summary')
                        <hr><br/>
                    @endif

                    @include('backend.dashboard.includes_claim.notification_received.index')

                    <hr><br/>

                    @include('backend.dashboard.includes_claim.incidents_general_summary')

                    {{--Claim Summary--}}

                    {{--@include('backend.dashboard.includes_claim.claim_summary_graph')--}}

                    {{--Notification status summary--}}

                    <br>

                    {{--@include('backend.dashboard.includes_claim.notification_status_summary')--}}

                    <br>

                    {{--@include('backend.dashboard.includes_claim.pending_claims_summary')--}}


                    {{--eND CONTENTS--}}

                </div>
            </div>
        </div>
    </div>
</div>
