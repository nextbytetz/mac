<div class="row">
    <div class="col-md-12 lg-dashboard-v2 " style="padding:0px !important;">
        <div class="content" >
            <div class="dashboard-content">
                <div class="dashboard-header ">
                    <h4 class="page-content-title float-xs-left "> @lang('labels.backend.report.dashboard.finance.title')
                    </h4>
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06" aria-hidden="true"></span></a></li>
                            <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-box finance_summary">


                    {{--CONTENTS--}}

                    {{--finance Summary--}}

                    @include('backend.dashboard.includes_finance.finance_summary_graph')

                    {{--RECEIVABLE SECTION (CONTRIBUTION AND INTEREST)--}}

                    <br>
                    @include('backend.dashboard.includes_finance.receivable.index')

                    {{--eND CONTENTS--}}

                </div>
            </div>
        </div>
    </div>
</div>





