
{{--COMPLIANCE--}}
@if($general_stat == 1)
    @include('backend.dashboard.compliance')

    {{--FINANCE--}}

    @include('backend.dashboard.finance')

    {{--CLAIM--}}

    @include('backend.dashboard.claim')

    {{--LEGAL--}}

    @include('backend.dashboard.legal')

    {{-- OSH --}}
  {{--   @include('backend.dashboard.osh') --}}
    
@else

    @include('backend.dashboard.includes_executive_dashboard.default_summary')

@endif