{{--EMPLOYER SUMMARY ------------------------------}}

<div class="row">
    <div class="col-md-12 lg-dashboard-v2" style="padding:0px !important;">
        <div class="content">
            <div class="dashboard-content">
                <div class="dashboard-header ">
                    <h4 class="page-content-title float-xs-left "> @lang('labels.backend.report.dashboard.compliance.employer_summary') </h4>
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06" aria-hidden="true"></span></a></li>
                            <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-box compliance_employer_summary">
                    {{--CONTENTS--}}

                    {{--Summaries --}}
                    @include('backend.dashboard.includes_compliance.employer_category_size_summary')
                    <br>

                    @include('backend.dashboard.includes_compliance.employer_registration_summary')

                    <br>

                    @include('backend.dashboard.includes_compliance.employers.employers_registered_with_variance')


                    {{--END CONTENTS--}}

                </div>
            </div>
        </div>
    </div>
</div>