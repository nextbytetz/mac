{{--<div class="compliance_employer_summary">--}}
    <div class="row">

        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard_v4_block2">
            <div class="dashboard_v4_revenue_block" style="background: #f9f9f2 !important;">
                <h6>@lang('labels.backend.report.dashboard.compliance.number_of_employers_registered')&nbsp;{!! financial_year() !!}</h6>
                <div id="employer_registration_chart" class="graph_height"></div>
            </div>
        </div>


@include('backend.dashboard.includes_compliance.employer_registered_monthly_table')

</div>
{{--</div>--}}



@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
$(function () {
var $graph1 = {!! json_encode($compliance_summary['graph_employer_registered']) !!};
var $splineData12 = [{
    data: $graph1,
    splines: {
        show: true,
        tension: 0.45,
        lineWidth: 4,
        fill: 0.1
    }
}, {
    data: $graph1,
    bars: {
        show: true,
        barWidth: 0.05,
        align: 'center',
        lineWidth: 0,
        fillColor: {
            colors: [{
                opacity: 0.2
            }, {
                opacity: 0.2
            }]
        }
    }
}];

var $splineOptions12 = {
    series: {
        points: {
            show: false
        }
    },
    colors: ['#087380', '#087380'],
    grid: {
        borderColor: '#eee',
        borderWidth: 0,
        hoverable: true,
        clickable: true,
        backgroundColor: 'transparent'
    },
    tooltip: true,
    tooltipOpts: {
        content: function(label, x, y) {
            return 'Employers : ' + '' + y;
        }
    },
    xaxis: {
        mode: 'categories',
        show: true
    },
    yaxis: {
        /*max: 80,*/
        show: true
    },
    legend: {
        backgroundColor: 'transparent',
        show: true
    },
    shadowSize: 0
};

var plot2 = $('#employer_registration_chart').each(function(){
    runFlot(this, $splineData12, $splineOptions12);
});
// Common function to run charts
// by reading custom height from data attribute
function runFlot(el, data, opts) {
    /*console.log(el);*/
    var $el = $(el),
        $height = $el.data('height');
    if ($height) $el.height($height);
    $el.plot(data, opts);
}
});
</script>

@endpush