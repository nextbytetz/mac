{{--Employer registered - TIN Summary--}}
{{--<div class="col-md-12">--}}
    {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
    <div class="widget-box widget-margin">
        <div class="static-header bg-primary text-xs-center">
            <h5 class="text-secondary">{!! trans('labels.backend.member.registered_tin_summary') !!}   </h5>
        </div>
        <div class="widget-content text-info">

            <div class="row">
                <div class="col-sm-12 ">
                    {{--<div class="static-detail-icon text-xs-center">--}}
                    <h4 class="text-primary text-xs-center ">{!! number_0_format( $compliance_summary['total_employer_registered'])
                                !!}</h4>

                    {{--</div>--}}
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12 ">
                    {{--<div class="text-xs-right text-success">--}}
                    <table class="table table-striped table-bordered">
                        <tbody class="text-primary">
                        <tr >
                            <th  width="100px">@lang('labels.backend.member.with_tin')</th>
                            <td align="right"><b>{!! number_0_format($compliance_summary['employer_with_tin'])
                            !!}</b></td>
                        </tr>

                        <tr>
                            <th width="100px">@lang('labels.backend.member.without_tin')</th>
                            <td align="right"><b>{!! number_0_format($compliance_summary['employer_without_tin'] )
                            !!}</b></td>

                        </tr>


                        </tbody>
                    </table>
                    {{--</div>--}}
                </div>
            </div>


        </div>
    </div>
{{--</div>--}}