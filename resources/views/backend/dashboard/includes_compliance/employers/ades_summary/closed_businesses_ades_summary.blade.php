

{{--<div class="col-md-4 pull-right">--}}
{{--<div class="col-md-12">--}}
<h6>@lang('labels.general.business_deregistration') &nbsp;{!! financial_year() !!}</h6>

<table class="table table-striped table-bordered table-fixed"  style="width:100%" >
    <tbody class="tbody_per_region table_display_block" >
    <tr style="background-color: darkred; color: white;"   >
        <th  style="width:60px">@lang('labels.general.sn')</th>
        <th  style="width: 350px">@lang('labels.general.month')</th>
        <th  style="width:350px">@lang('labels.backend.member.no_of_employers')</th>
        <th  style="width:350px">@lang('labels.general.attended')</th>
    </tr>

    <?php
    $i = 1;
    ?>

    @foreach($compliance_summary['closed_businesses_table'] as $key => $value)

        <tr>
            <td>{!! $i   !!}</td>
            <td>{!! $value[0] !!}</td>
                       <td>{!! number_0_format( $value[1]) !!}</td>
            <td>{!! number_0_format( $value[2]) !!}</td>

        </tr>

        <?php
        $i++;
        ?>

    @endforeach

    </tbody>
</table>
{{--</div>--}}
{{--</div>--}}
