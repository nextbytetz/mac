{{-- ADES SUMMARY--}}
<div class="row">

    <div class="col-md-12">
        {{--new tax payers--}}
        <div class="col-md-6">
            <div class="row">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            @include('backend.dashboard.includes_compliance.employers.ades_summary.new_taxpayers_ades_summary')


            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left success_box">
                        <i class="fa fa-institution"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_0_format($compliance_summary['new_taxpayer_total']) !!}</h4>
                        <p>@lang('labels.general.total')</p>
                    </div>
                </div>
            </div>

            </div>
        </div>


        {{--closed business--}}
        <div class="col-md-6">
            <div class="row">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            @include('backend.dashboard.includes_compliance.employers.ades_summary.closed_businesses_ades_summary')


            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left primary_box">
                        <i class="fa fa-institution"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_0_format($compliance_summary['closed_businesses_total']) !!}</h4>
                        <p>@lang('labels.general.total')</p>
                    </div>
                </div>
            </div>


        </div>

        </div>

    </div>
</div>

