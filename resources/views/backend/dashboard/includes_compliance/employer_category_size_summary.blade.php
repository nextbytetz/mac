
{{--<div class="compliance_employer_category_summary">--}}

<div class="row">
    {{--left Category summary--}}
    <div class="col-md-12">
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            <div class="widget-box widget-margin">
                <div class="static-header bg-success text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.registered') !!}   </h5>                 </div>
                <div class="widget-content text-info">


                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="static-detail-icon text-xs-center">--}}
                            <h4 class="text-primary text-xs-center ">{!! number_0_format($compliance_summary['total_employer_registered'])
                                !!}</h4>
                            <h6 class="text-primary text-xs-center">{!! '('. number_2_format( $compliance_summary['employer_registered_percent']) . '%)' !!}</h6>
                            {{--</div>--}}
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="text-xs-right text-success">--}}
                            <table class="table table-striped table-bordered">
                                <tbody class="text-success">
                                <tr >
                                    <th  width="10px">@lang('labels.general.public')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['public_employers']) !!}</b></td>
                                </tr>

                                <tr>
                                    <th width="10px">@lang('labels.general.private')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['private_employers'])  !!}</b></td>

                                </tr>


                                </tbody>
                            </table>
                            {{--</div>--}}
                        </div>
                    </div>


                </div>
            </div>
        </div>

        {{--unregistered--}}
        <div class="col-md-4">
            <div class="widget-box widget-margin last-widget-v3">
                <div class="static-header bg-warning text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.unregistered') !!}   </h5>
                </div>
                <div class="widget-content text-warning">
                    <div class="static-detail-icon"></div>
                    <div class="text-xs-center"><h4 class="text-warning">{!! number_0_format($compliance_summary['unregistered_employers'])  !!}</h4>
                        <h6 class="text-warning">{!! '('.number_2_format($compliance_summary['employer_unregistered_percent'] ). '%)' !!}</h6>
                    </div>
                </div>
            </div>
        </div>
        {{--total--}}
        <div class="col-md-4">
            <div class="widget-box widget-margin last-widget-v3">
                <div class="static-header bg-info text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.total') !!}   </h5>
                </div>
                <div class="widget-content text-primary">
                    <div class="static-detail-icon"></div>
                    <div class="text-xs-center"><h4 class="text-primary">{!! number_0_format($compliance_summary['overall_total_employer']) !!}</h4>
                    </div>
                </div>
            </div>

        </div>



    </div>
</div>




{{--Employer Business status Summary-==================--}}


<div class="row">

    <div class="col-md-12">
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            <div class="widget-box widget-margin">
                <div class="static-header bg-success text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.active') !!}   </h5>                 </div>
                <div class="widget-content text-info">


                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="static-detail-icon text-xs-center">--}}
                                <h4 class="text-primary text-xs-center ">{!! number_0_format($compliance_summary['total_active_employer'])
                                !!}</h4>
                                <h6 class="text-primary text-xs-center">{!! '('. number_2_format($compliance_summary['active_employer_percent']). '%)' !!}</h6>
                            {{--</div>--}}
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="text-xs-right text-success">--}}
                                <table class="table table-striped table-bordered">
                                    <tbody class="text-success">
                                    <tr >
                                        <th  width="30px">@lang('labels.general.public')</th>
                                        <td align="right"><b>{!! number_0_format($compliance_summary['active_public_employer']) !!}</b></td>
                                    </tr>

                                    <tr>
                                        <th width="30px">@lang('labels.general.private')</th>
                                        <td align="right"><b>{!! number_0_format($compliance_summary['active_private_employer'] ) !!}</b></td>

                                    </tr>
                                    <tr>
                                        <th width="30px">Online Users</th>
                                        <td align="right"><b>{{ number_format($compliance_summary['online_registered']). " (" . number_2_format($compliance_summary['online_registered_percent']) . "%)" }}</b></td>
                                    </tr>
                                    <tr>
                                        <th width="30px">Large Contributors</th>
                                        <td align="right"><b>{{ $compliance_summary['large_contributor'] . " (" . number_2_format($compliance_summary['large_contributor_percent']) . "%)" }}</b></td>
                                    </tr>


                                    </tbody>
                                </table>
                            {{--</div>--}}
                        </div>
                    </div>





                </div>
            </div>
        </div>


        {{--dormant--}}
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            <div class="widget-box widget-margin">
                <div class="static-header bg-warning text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.dormant') !!}   </h5>                 </div>
                <div class="widget-content text-info">




                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="static-detail-icon text-xs-center">--}}
                            <h4 class="text-warning text-xs-center ">{!! number_0_format($compliance_summary['total_dormant_employer'])
                                !!}</h4>
                            <h6 class="text-warning text-xs-center">{!! '('. number_2_format($compliance_summary['dormant_employer_percent'] ). '%)' !!}</h6>
                            {{--</div>--}}
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="text-xs-right text-success">--}}
                            <table class="table table-striped table-bordered">
                                <tbody class="text-warning">
                                <tr >
                                    <th  width="10px">@lang('labels.general.public')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['dormant_public_employer']) !!}</b></td>
                                </tr>

                                <tr>
                                    <th width="10px">@lang('labels.general.private')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['dormant_private_employer'] ) !!}</b></td>

                                </tr>


                                </tbody>
                            </table>
                            {{--</div>--}}
                        </div>
                        <br><br><br><br><br><br><br><br><br><br>
                    </div>


                </div>
            </div>
        </div>


        {{--closed business--}}
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            <div class="widget-box widget-margin">
                <div class="static-header bg-danger text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.business_deregistration') !!}   </h5>                 </div>
                <div class="widget-content text-info">


                    <div class="row">
                        <div class="col-sm-12 ">
                           
                            <h4 class="text-danger text-xs-center ">{!! number_0_format( $compliance_summary['total_closed_employer'])
                                !!}</h4>
                            <h6 class="text-danger text-xs-center">{!! '('. number_2_format($compliance_summary['closed_employer_percent'] ). '%)' !!}</h6>
                           
                        </div>

                          </div>


                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="text-xs-right text-success">--}}
                            <table class="table table-striped table-bordered">
                                <tbody class="text-danger">
                                <tr >
                                    <th  width="10px">@lang('labels.general.public')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['closed_public_employer']) !!}</b></td>
                                </tr>

                                <tr>
                                    <th width="10px">@lang('labels.general.private')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['closed_private_employer'] ) !!}</b></td>

                                </tr>


                                </tbody>
                            </table>
                            {{--</div>--}}
                        </div>
                    </div>
           `         <div class="row">
                <br><br><br><br>
           </div>

                </div>
            </div>

        </div>


    </div>
</div>



























{{--right employer size--}}
<div class="row">
    <div class="col-md-12">
        <div class="col-md-8">
            <table class="table table-striped table-bordered" >
                <tbody>
                <tr style="background-color: lightskyblue; color: white">
                    <th>@lang('labels.general.sn')</th>
                    <th>@lang('labels.backend.member.type_of_employer')</th>
                    <th>@lang('labels.backend.member.no_of_employees_range')</th>
                    <th>@lang('labels.backend.member.no_of_employers')</th>
                </tr>



                @foreach($compliance_summary['employer_size_types'] as $employer_size_type)

                    <tr>
                        <td>{!! $employer_size_type->id !!}</td>
                        <td>{!! $employer_size_type->name !!}</td>
                        <td>{!! $employer_size_type->description  !!}</td>
                        <td>{!! number_0_format($employer_size_type->employer_count)  !!}</td>


                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>





        {{--side -Registered - TIN SUMMARY--}}
        <div class="col-md-4">
            @include('backend.dashboard.includes_compliance.employers.employer_registered_tin_summary')
        </div>










    </div>






</div>







{{--</div>--}}
{{--</div>--}}