
<br>
{{--EMPLOYER SUMMARY ------------------------------}}

<div class="row">
    <div class="col-md-12 lg-dashboard-v2" style="padding:0px !important;">
        <div class="content">
            <div class="dashboard-content">
                <div class="dashboard-header ">
                    <h4 class="page-content-title float-xs-left "> @lang('labels.backend.report.dashboard.compliance.employer_summary') </h4>
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06" aria-hidden="true"></span></a></li>
                            <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-box compliance_employer_summary">
                    {{--CONTENTS--}}

                    {{--Summaries --}}
                    @include('backend.dashboard.includes_compliance.employer_category_size_summary')
                    <br>

                    @include('backend.dashboard.includes_compliance.employer_registration_summary')

                    <br>
                    @include('backend.dashboard.includes_compliance.employers.employers_registered_with_variance')

                    <br>

                    {{--Employer ADES Summary--}}
                    @include('backend.dashboard.includes_compliance.employers.ades_summary.index')
                    {{--eND CONTENTS--}}

                </div>
            </div>
        </div>
    </div>
</div>


{{--END OF Employer Summary--}}




{{--COMPLIANCE SUMMARY---------------------}}
<div class="row">
    <div class="col-md-12 lg-dashboard-v2 " style="padding:0px !important;">
        <div class="content" >
            <div class="dashboard-content">
                <div class="dashboard-header ">
                    <h4 class="page-content-title float-xs-left "> @lang('labels.backend.report.dashboard.compliance.title') </h4>
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06" aria-hidden="true"></span></a></li>
                            <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-box compliance_summary">
                    {{--CONTENTS--}}

                    {{--Graph --}}
                    @include('backend.dashboard.includes_compliance.compliance_stat_summary')
                    <br>
                    {{--receivable section--}}
                    @include('backend.dashboard.includes_compliance.receivable.index')
                    <br>
                    <br>
                    {{--General summary--}}
                    @include('backend.dashboard.includes_compliance.general.general_summary')

                    <br>
                    {{--Staff performance summary--}}
                    {{--Not DG / Executive--}}
                    @if($selector <> 11 || $compliance_summary['compliance_manager'] == 1)
                        @include('backend.dashboard.includes_compliance.general.staff_performance')
                    @endif
                    {{--eND CONTENTS--}}

                </div>
            </div>
        </div>
    </div>
</div>

{{--End of Compliance Summary------------------------}}









<br>
{{--EMPLOYEE SUMMARY ------------------------------}}

<div class="row">
    <div class="col-md-12 lg-dashboard-v2" style="padding:0px !important;">
        <div class="content">
            <div class="dashboard-content">
                <div class="dashboard-header ">
                    <h4 class="page-content-title float-xs-left "> @lang('labels.backend.report.dashboard.compliance.employee_summary') </h4>
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06" aria-hidden="true"></span></a></li>
                            <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-box compliance_employee_summary">
                    {{--CONTENTS--}}


                    @include('backend.dashboard.includes_compliance.employees.employee_summary')
<br>
                    @include('backend.dashboard.includes_compliance.employees.employees_registered_with_variance')

                    {{--eND CONTENTS--}}

                </div>
            </div>
        </div>
    </div>
</div>


{{--END OF Employer Summary--}}



@push('after-script-end')
<!-- Custom javascript files for this page -->


@endpush