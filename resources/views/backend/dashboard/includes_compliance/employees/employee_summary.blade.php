{{--<div class="compliance_employee_summary">--}}


    <div class="row">

        @include('backend.dashboard.includes_compliance.employees.employees_registered_status')

    </div>



    <div class="row">

        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard_v4_block2">
            <div class="dashboard_v4_revenue_block" style="background: #f9f9f2 !important;">
                <h6>@lang('labels.backend.report.dashboard.compliance.number_of_employees_registered')&nbsp;{!! financial_year() !!}</h6>
                <div id="employee_registration_chart" class="graph_height"></div>
            </div>
        </div>


        @include('backend.dashboard.includes_compliance.employees.employee_registration_monthly_table')

    </div>
{{--</div>--}}



@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        var $graph1_employee = {!! json_encode($compliance_summary['graph_employee_registered']) !!};
        var $splineData12_employee = [{
            data: $graph1_employee,
            splines: {
                show: true,
                tension: 0.45,
                lineWidth: 4,
                fill: 0.1
            }
        }, {
            data: $graph1_employee,
            bars: {
                show: true,
                barWidth: 0.05,
                align: 'center',
                lineWidth: 0,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            }
        }];

        var $splineOptions12_employee = {
            series: {
                points: {
                    show: false
                }
            },
            colors: ['#087380', '#087380'],
            grid: {
                borderColor: '#eee',
                borderWidth: 0,
                hoverable: true,
                clickable: true,
                backgroundColor: 'transparent'
            },
            tooltip: true,
            tooltipOpts: {
                content: function(label, x, y) {
                    return 'Employees : ' + '' + y;
                }
            },
            xaxis: {
                mode: 'categories',
                show: true
            },
            yaxis: {
                /*max: 80,*/
                show: true
            },
            legend: {
                backgroundColor: 'transparent',
                show: true
            },
            shadowSize: 0
        };

        var plot_employee = $('#employee_registration_chart').each(function(){
            runFlot(this, $splineData12_employee, $splineOptions12_employee);
        });
// Common function to run charts
// by reading custom height from data attribute
        function runFlot(el, data, opts) {
            /*console.log(el);*/
            var $el = $(el),
                $height = $el.data('height');
            if ($height) $el.height($height);
            $el.plot(data, opts);
        }
    });
</script>

@endpush