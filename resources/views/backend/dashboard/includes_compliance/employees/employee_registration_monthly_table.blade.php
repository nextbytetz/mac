

<div class="col-md-4 pull-right">
    <div class="col-md-12">
        <h6>@lang('labels.backend.report.dashboard.compliance.number_of_employees_registered') &nbsp;{!! financial_year() !!}</h6>

        <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
            <tbody class="tbody_per_region table_display_block" >
            <tr style="background-color: lightskyblue; color: white;"   >
                <th  style="width:60px">@lang('labels.general.sn')</th>
                <th  style="width: 150px">@lang('labels.general.month')</th>
                <th  style="width:60%">@lang('labels.backend.member.no_of_employees')</th>
            </tr>

            <?php
            $i = 1;
            ?>

            @foreach($compliance_summary['table_employee_registered'] as $key => $value)

                <tr>
                    <td>{!! $i   !!}</td>
                    <td>{!! $value[0] !!}</td>
                    <td>{!!  number_0_format( $value[1])  !!}</td>
                </tr>

                <?php
                $i++;
                ?>
            @endforeach

            </tbody>
        </table>

        {{--total--}}
        <div class="row">
            <div class="content">
                <div class="dashboard_v4_box_icon float-xs-left success_box">
                    <i class="fa fa-users"></i>
                </div>
                <div class="dashboard_v4_box_title float-xs-right">
                    <h4>{!! number_0_format($compliance_summary['total_employees_registered']) !!}</h4>
                    <p>@lang('labels.general.overall_total')</p>
                </div>
            </div>
        </div>



    </div>
</div>
