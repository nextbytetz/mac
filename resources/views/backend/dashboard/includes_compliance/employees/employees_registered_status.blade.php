{{--Employee Summary based on status ; active, dormant, inactive--}}


<div class="row">

    <div class="col-md-12">
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            <div class="widget-box widget-margin">
                <div class="static-header bg-success text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.active') !!}   </h5>                 </div>
                <div class="widget-content text-info">




                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="static-detail-icon text-xs-center">--}}
                            <h4 class="text-primary text-xs-center ">{!! number_0_format($compliance_summary['total_active_employee'])
                                !!}</h4>
                            <h6 class="text-primary text-xs-center">{!! '('. number_2_format( $compliance_summary['active_employee_percent'] ). '%)' !!}</h6>
                            {{--</div>--}}
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="text-xs-right text-success">--}}
                            <table class="table table-striped table-bordered">
                                <tbody class="text-success">
                                <tr >
                                    <th  width="10px">@lang('labels.general.public')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['active_public_employee']) !!}</b></td>
                                </tr>

                                <tr>
                                    <th width="10px">@lang('labels.general.private')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['active_private_employee'] ) !!}</b></td>

                                </tr>


                                </tbody>
                            </table>
                            {{--</div>--}}
                        </div>
                    </div>




                </div>
            </div>
        </div>


        {{--dormant--}}
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            <div class="widget-box widget-margin">
                <div class="static-header bg-warning text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.dormant') !!}   </h5>                 </div>
                <div class="widget-content text-info">



                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="static-detail-icon text-xs-center">--}}
                            <h4 class="text-warning text-xs-center ">{!! number_0_format($compliance_summary['total_dormant_employee'])
                                !!}</h4>
                            <h6 class="text-warning text-xs-center">{!! '('. number_2_format( $compliance_summary['dormant_employee_percent'] ). '%)' !!}</h6>
                            {{--</div>--}}
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="text-xs-right text-success">--}}
                            <table class="table table-striped table-bordered">
                                <tbody class="text-warning">
                                <tr >
                                    <th  width="10px">@lang('labels.general.public')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['dormant_public_employee']) !!}</b></td>
                                </tr>

                                <tr>
                                    <th width="10px">@lang('labels.general.private')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['dormant_private_employee'])  !!}</b></td>

                                </tr>


                                </tbody>
                            </table>
                            {{--</div>--}}
                        </div>
                    </div>








                </div>
            </div>
        </div>


        {{--closed business--}}
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            <div class="widget-box widget-margin">
                <div class="static-header bg-danger text-xs-center">
                    <h5 class="text-secondary">{!! trans('labels.general.inactive') !!}   </h5>                 </div>
                <div class="widget-content text-info">




                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="static-detail-icon text-xs-center">--}}
                            <h4 class="text-danger text-xs-center ">{!! number_0_format($compliance_summary['total_closed_employee'])
                                !!}</h4>
                            <h6 class="text-danger text-xs-center">{!! '('. number_2_format( $compliance_summary['closed_employee_percent'] ). '%)' !!}</h6>
                            {{--</div>--}}
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 ">
                            {{--<div class="text-xs-right text-success">--}}
                            <table class="table table-striped table-bordered">
                                <tbody class="text-danger">
                                <tr >
                                    <th  width="10px">@lang('labels.general.public')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['closed_public_employee']) !!}</b></td>
                                </tr>

                                <tr>
                                    <th width="10px">@lang('labels.general.private')</th>
                                    <td align="right"><b>{!! number_0_format($compliance_summary['closed_private_employee'])  !!}</b></td>

                                </tr>


                                </tbody>
                            </table>
                            {{--</div>--}}
                        </div>
                    </div>






                </div>
            </div>
        </div>


    </div>
</div>

