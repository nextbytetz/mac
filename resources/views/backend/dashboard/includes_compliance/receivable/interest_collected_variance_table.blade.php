


<h6>@lang('labels.backend.report.dashboard.finance.interest') &nbsp;{!! financial_year()
        !!}</h6>

<table class="table table-striped table-bordered table-fixed"  style="width:100%" >
    <tbody class="tbody_per_region table_display_block" >
    <tr style="background-color: lightskyblue; color: white;"   >
        <th  style="width:60px">@lang('labels.general.sn')</th>
        <th  style="width: 250px">@lang('labels.general.month')</th>
        <th  style="width:60%">@lang('labels.general.amount')</th>
    </tr>

    <?php
    $i = 1;
    ?>

    @foreach($compliance_summary['interest_table'] as $key => $value)

        <tr>
            <td>{!! $i   !!}</td>
            <td>{!! $value[0] !!}</td>
            <td>{!!  number_2_format($value[1])  !!}</td>
        </tr>

        <?php
        $i++;
        ?>

    @endforeach

    </tbody>
</table>
