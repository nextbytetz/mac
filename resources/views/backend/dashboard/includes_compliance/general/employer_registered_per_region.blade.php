

    {{--<div class="col-md-4 pull-right">--}}
        {{--<div class="col-md-12">--}}
            <h6>@lang('labels.backend.report.dashboard.compliance.number_of_employers_per_region')</h6>

            <table class="table table-striped table-bordered table-fixed" >
                <tbody class="tbody_per_region table_display_block">
                <tr style="background-color: lightskyblue; color: white"   >
                    <th style="width:60px">@lang('labels.general.sn')</th>
                    <th style="width:250px">@lang('labels.general.name')</th>
                    <th style="width:250px">@lang('labels.backend.member.no_of_employers')</th>
                    <th style="width:250px">@lang('labels.general.unregistered')</th>
                    <th style="width:250px">@lang('labels.general.registered')%</th>
                    <th style="width:250px">@lang('labels.backend.member.certificates_issued')</th>
                    <th style="width:250px">@lang('labels.general.certificate')%</th>
                </tr>

                <?php
                $i = 1;
                ?>

                @foreach($compliance_summary['regions'] as $region)

                    <tr>
                        <td>{!! $i   !!}</td>
                        <td>{!! $region->name !!}</td>
                        <td>{!! number_0_format( $region->employerCount()) !!}</td>
                        <td>{!! number_0_format( $region->unregisteredEmployerCount()) !!}</td>
                        <td>{!! number_0_format( $region->employerRegisteredUnregisteredPercentage()) !!}%</td>
                        <td>{!! number_0_format(($region->employerCertificateIssued())) !!}</td>
                            <td>{!!
                   number_0_format($region->employerCertificateIssuedPercentage() )
                           !!}%</td>



                    </tr>



                    {{--<tr>--}}
                        {{--<td>{!! $i   !!}</td>--}}
                        {{--<td>{!! $region->name !!}</td>--}}
                        {{--<td>{!! number_0_format( $region->employers->count()) !!}</td>--}}
                        {{--<td>{!! number_0_format(($region->employers()->whereHas('employerCertificateIssues', function--}}
                        {{--($query){--}}
                        {{--$query->where('is_reissue', 0);--}}
                        {{--})->count()) ) !!}</td>--}}


                        {{--@if($region->employers->count() > 0)--}}
                            {{--<td>{!!--}}
                   {{--number_0_format( (($region->employers()->whereHas('employerCertificateIssues', function ($query){--}}
                        {{--$query->where('is_reissue', 0);--}}
                        {{--})->count()) / ($region->employers->count()  * 0.01)))--}}
                           {{--!!}</td>--}}
                        {{--@else--}}
                            {{--<td>{!! 0   !!}</td>--}}
                        {{--@endif--}}


                    {{--</tr>--}}








                    <?php
                    $i++;
                    ?>

                @endforeach

                </tbody>
            </table>
        {{--</div>--}}
    {{--</div>--}}
