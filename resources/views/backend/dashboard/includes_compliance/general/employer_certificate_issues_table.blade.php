

{{--<div class="col-md-4 pull-right">--}}
    {{--<div class="col-md-12">--}}
        <h6>@lang('labels.backend.report.dashboard.compliance.number_of_employer_certificate') &nbsp;{!! financial_year() !!}</h6>

        <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
            <tbody class="tbody_per_region table_display_block" >
            <tr style="background-color: lightsalmon; color: white;"   >
                <th  style="width:60px">@lang('labels.general.sn')</th>
                <th  style="width: 350px">@lang('labels.general.month')</th>
                <th  style="width:350px">@lang('labels.backend.member.no_of_employers')</th>
                <th  style="width:350px">@lang('labels.general.target')</th>
                <th  style="width:350px">@lang('labels.general.variance') %</th>
            </tr>

            <?php
            $i = 1;
            ?>

            @foreach($compliance_summary['employer_certificate_issues_table'] as $key => $value)

                <tr>
                    <td>{!! $i   !!}</td>
                    <td>{!! $value[0] !!}</td>
                    <td>{!! number_0_format( $value[1]) !!}</td>
                    <td>{!! number_0_format( $value[2] ) !!}</td>
                    <td>
                        @if ($value[3] > 0)
                            <span class="float-xs-left text-light-danger"> {!! number_0_format($value[3]) !!}%&nbsp;<a>-ve</a></span>
                        @elseif($value[3] == 0)
                            <span class="float-xs-left text-light-primary"> {!! number_0_format($value[3]) !!}%
                    </span>
                        @else
                            <span class="float-xs-left text-light-success"> {!! number_0_format($value[3] * -1) !!}%&nbsp;<a>+ve</a></span>
                        @endif
                    </td>

                </tr>

                <?php
                $i++;
                ?>

            @endforeach

            </tbody>
        </table>
    {{--</div>--}}
{{--</div>--}}
