{{--Employee Summary based on status ; active, dormant, inactive--}}


<div class="row">

    <div class="col-md-12">



        {{--dormant--}}
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            @include('backend.dashboard.includes_compliance.general.employer_certificate_issues_table')


            {{--total--}}
            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left primary_box">
                        <i class="fa fa-certificate"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_0_format($compliance_summary['total_certificates_issued']) !!}</h4>
                        <p>@lang('labels.general.overall_total')</p>
                    </div>
                </div>
            </div>



    </div>
</div>







<div class="row">

    <div class="col-md-12">

            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            @include('backend.dashboard.includes_compliance.general.employer_registered_per_region')


            {{--total--}}
            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left success_box">
                        <i class="fa fa-institution"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_0_format($compliance_summary['total_employers_registered']) !!}</h4>
                        <p>@lang('labels.general.overall_total')</p>
                    </div>
                </div>
            </div>

        </div>


</div>

