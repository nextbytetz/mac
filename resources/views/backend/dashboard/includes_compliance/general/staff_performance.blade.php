


{{--right employer size--}}
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <h6>@lang('labels.backend.report.dashboard.compliance.compliance_staff_performance')&nbsp;{!! financial_year() !!}</h6>
            <table class="table table-striped table-bordered" >
                <tbody>
                <tr style="background-color: lightskyblue; color: white"   >
                    <th>@lang('labels.general.sn')</th>
                    <th>@lang('labels.general.name')</th>
                                       <th>@lang('labels.backend.member.contributions_verified')</th>
                    <th>@lang('labels.backend.member.certificates_issued')</th>
                </tr>


                <?php
                $i = 1;
                ?>

                @foreach($compliance_summary['compliance_staff_performance'] as $key => $value)

                    <tr>
                        <td>{!! $i   !!}</td>
                        <td>{!! $value[0] !!}</td>
                        <td>{!!  number_0_format($value[1])  !!}</td>
                        <td>{!!  number_0_format($value[2])  !!}</td>
                    </tr>

                    <?php
                    $i++;
                    ?>

                @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>