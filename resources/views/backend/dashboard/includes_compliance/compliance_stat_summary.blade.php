{{--<div class="compliance_summary">--}}


{{--</div>--}}

                    {{--CONTENTS--}}

                    <div class="row">
                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard_v4_block">
                            <div class="dashboard_v4_project_main">
                                <h6 class="page-content-title">@lang('labels.backend.report.dashboard.statistics')&nbsp;{!! financial_year() !!}</h6>
                                <div class="divider15"></div>
                                <div class="dashboard_v4_project_block">
                                    <table class="table  table-striped">
                                        <tbody>
                                        <tr>
                                            <td><div class="stackline-bar">{!! $compliance_summary['contribution'] !!}</div></td>
                                            <td>
                                                <h6>@lang('labels.backend.report.dashboard.finance.contribution')</h6>
                                                <p style="font-weight: 700;">{!! number_2_format($compliance_summary['contribution_total']) !!}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><div class="stackline-bar">{!! $compliance_summary['interest'] !!}</div></td>
                                            <td>
                                                <h6>@lang('labels.backend.report.dashboard.finance.interest')</h6>
                                                <p style="font-weight: 700;">{!! number_2_format($compliance_summary['interest_total']) !!}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><div class="stackline-bar">{!! $compliance_summary['emergence_inspections'] !!}</div></td>
                                            <td>
                                                <h6>@lang('labels.backend.report.dashboard.compliance.emergence_inspection')</h6>
                                                <p style="font-weight: 700;">{!! number_0_format($compliance_summary['emergence_inspections_total']) !!}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><div class="stackline-bar">{!! $compliance_summary['inspected_employers'] !!}</div></td>
                                            <td>
                                                <h6>@lang('labels.backend.report.dashboard.compliance.inspected_employers')</h6>
                                                <p style="font-weight: 700;">{!! number_0_format($compliance_summary['inspected_employers_total']) !!}</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard_v4_block2">
                            <div class="dashboard_v4_revenue_block" style="background: #f9f9f2 !important;">
                                <h6>@lang('labels.backend.report.dashboard.compliance.chart')</h6>
                                <div id="employer_contributing_chart" class="graph_height"></div>
                            </div>
                        </div>
                    </div>

                    {{--eND CONTENTS--}}

{{--</div>--}}


@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        var $graph = {!! json_encode($compliance_summary['graph_contributing_employers']) !!};
        var $splineData2 = [{
            data: $graph,
            splines: {
                show: true,
                tension: 0.45,
                lineWidth: 4,
                fill: 0.1
            }
        }, {
            data: $graph,
            bars: {
                show: true,
                barWidth: 0.05,
                align: 'center',
                lineWidth: 0,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            }
        }];

        var $splineOptions2 = {
            series: {
                points: {
                    show: false
                }
            },
            colors: ['#087380', '#087380'],
            grid: {
                borderColor: '#eee',
                borderWidth: 0,
                hoverable: true,
                clickable: true,
                backgroundColor: 'transparent'
            },
            tooltip: true,
            tooltipOpts: {
                content: function(label, x, y) {
                    return 'Employers : ' + '' + y;
                }
            },
            xaxis: {
                mode: 'categories',
                show: true
            },
            yaxis: {
                /*max: 80,*/
                show: true
            },
            legend: {
                backgroundColor: 'transparent',
                show: true
            },
            shadowSize: 0
        };

        var plot = $('#employer_contributing_chart').each(function(){
            runFlot(this, $splineData2, $splineOptions2);
        });
        // Common function to run charts
        // by reading custom height from data attribute
        function runFlot(el, data, opts) {
            /*console.log(el);*/
            var $el = $(el),
                $height = $el.data('height');
            if ($height) $el.height($height);
            $el.plot(data, opts);
        }



    });

</script>
@endpush