


<h6>@lang('labels.backend.report.dashboard.compliance.no_of_employer_inspection') &nbsp;{!! financial_year()
        !!}</h6>

<table class="table table-striped table-bordered table-fixed"  style="width:100%" >
    <tbody class="tbody_per_region table_display_block" >
    <tr style="background-color: lightskyblue; color: white;"   >
        <th  style="width:60px">@lang('labels.general.sn')</th>
        <th  style="width: 350px">@lang('labels.general.month')</th>
        <th  style="width:350px">@lang('labels.general.no')</th>
        <th  style="width:350px">@lang('labels.general.target')</th>
        <th  style="width:350px">@lang('labels.general.variance') %</th>
    </tr>

    <?php
    $i = 1;
    ?>

    @foreach($compliance_summary['inspected_employer_table'] as $key => $value)

        <tr>
            <td>{!! $i   !!}</td>
            <td>{!! $value[0] !!}</td>
            <td>{!!  $value[1]  !!}</td>
            <td>{!!  number_0_format($value[2]) !!}</td>
            <td>
                @if ($value[3] > 0)

                    <span class="float-xs-left text-light-danger"> {!! number_0_format($value[3])!!}%&nbsp;<i class="fa fa-level-down"></i></span>
                @elseif($value[3] == 0)
                    <span class="float-xs-left text-light-primary"> {!! number_0_format($value[3])!!}%
                    </span>
                @else
                    <span class="float-xs-left text-light-success"> {!! number_0_format($value[3]) !!}%&nbsp;<i class="fa fa-level-up"></i></span>
                @endif
            </td>

        </tr>

        <?php
        $i++;
        ?>

    @endforeach

    </tbody>
</table>



<div class="row">
    <div class="content">
        <div class="dashboard_v4_box_icon float-xs-left success_box">
            <i class="fa fa-check"></i>
        </div>
        <div class="dashboard_v4_box_title float-xs-right">
            <h4>{!! number_0_format($compliance_summary['inspected_employers_overall_total']) !!}</h4>
            <p>@lang('labels.general.total')</p>
        </div>
    </div>
</div>


