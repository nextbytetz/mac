{{--Employee Summary based on status ; active, dormant, inactive--}}


<div class="row">

    <div class="col-md-12">
        {{--contribution--}}
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            @include('backend.dashboard.includes_finance.receivable.contributions_collected_table')


<div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left success_box">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_2_format($compliance_summary['contribution_total']) !!}</h4>
                        <p>@lang('labels.general.total')</p>
                    </div>
                </div>
</div>


        </div>


        {{--Interest--}}
        <div class="col-md-4">
            {{--<div class="col-xl-3 col-lg-5 col-md-12 col-sm-12 col-xs-12 widget-specing">--}}
            @include('backend.dashboard.includes_finance.receivable.interest_collected_table')



            <div class="row">
                <div class="content">
                    <div class="dashboard_v4_box_icon float-xs-left primary_box">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="dashboard_v4_box_title float-xs-right">
                        <h4>{!! number_2_format($compliance_summary['interest_total']) !!}</h4>
                        <p>@lang('labels.general.total')</p>
                    </div>
                </div>
            </div>


        </div>



    </div>
</div>

