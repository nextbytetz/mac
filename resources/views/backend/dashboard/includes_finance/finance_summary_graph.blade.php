
    {{--<h6>@lang('labels.backend.report.dashboard.finance.statistics')&nbsp;{!! financial_year() !!}</h6>--}}
    <div class="row">
        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard_v4_block">
            <div class="dashboard_v4_project_main">
                <h6 class="page-content-title">@lang('labels.backend.report.dashboard.statistics')&nbsp;{!! financial_year() !!}</h6>
                <div class="divider15"></div>
                <div class="dashboard_v4_project_block">
                    <table class="table  table-striped">
                        <tbody>
                        <tr>
                            <td><div class="stackline-bar">{!! $finance_summary['contribution'] !!}</div></td>
                            <td>
                                <h6>@lang('labels.backend.report.dashboard.finance.contribution')</h6>
                                <p style="font-weight: 700;">{!! number_2_format($finance_summary['contribution_total']) !!}/=</p>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="stackline-bar">{!! $finance_summary['interest'] !!}</div></td>
                            <td>
                                <h6>@lang('labels.backend.report.dashboard.finance.interest')</h6>
                                <p style="font-weight: 700;">{!! number_2_format($finance_summary['interest_total']) !!}/=</p>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="stackline-bar">{!! $finance_summary['receipts'] !!}</div></td>
                            <td>
                                <h6>@lang('labels.backend.table.receipts')</h6>
                                <p style="font-weight: 700;">{!! number_0_format($finance_summary['receipts_total']) !!}</p>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="stackline-bar">{!! $finance_summary['cancelled'] !!}</div></td>
                            <td>
                                <h6>@lang('labels.backend.finance.report.cancelled_receipts')</h6>
                                <p style="font-weight: 700;">{!! number_0_format($finance_summary['cancelled_total']) !!}</p>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="stackline-bar">{!! $finance_summary['dishonoured'] !!}</div></td>
                            <td>
                                <h6>@lang('labels.backend.table.dishonoured_cheques')</h6>
                                <p style="font-weight: 700;">{!! number_0_format($finance_summary['dishonoured_total']) !!}</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard_v4_block2">
            <div class="dashboard_v4_revenue_block" style="background: #eff2e8 !important;">
                <h6>@lang('labels.backend.report.dashboard.finance.chart')</h6>
                <div id="receipt_collection_chart" class="receipt_collection_chart"></div>
            </div>
        </div>
    </div>


@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        var $graph = {!! json_encode($finance_summary['graph_monthly_collection']) !!};
        /*console.log($graph);*/
        var $splineData2 = [{
            data: $graph,
            splines: {
                show: true,
                tension: 0.45,
                lineWidth: 4,
                fill: 0.1
            }
        }, {
            data: $graph,
            bars: {
                show: true,
                barWidth: 0.05,
                align: 'center',
                lineWidth: 0,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            }
        }];

        var $splineOptions2 = {
            series: {
                points: {
                    show: false
                }
            },
            colors: ['#087380', '#087380'],
            grid: {
                borderColor: '#eee',
                borderWidth: 0,
                hoverable: true,
                clickable: true,
                backgroundColor: 'transparent'
            },
            tooltip: true,
            tooltipOpts: {
                content: function(label, x, y) {
                    return 'Amount : ' + 'Tshs' + y;
                }
            },
            xaxis: {
                mode: 'categories',
                show: true
            },
            yaxis: {
                /*max: 200000,*/
                show: true
            },
            legend: {
                backgroundColor: 'transparent',
                show: true
            },
            shadowSize: 0
        };

        var plot = $('#receipt_collection_chart').each(function(){
            runFlot(this, $splineData2, $splineOptions2);
        });
        // Common function to run charts
        // by reading custom height from data attribute
        function runFlot(el, data, opts) {
            /*console.log(el);*/
            var $el = $(el),
                $height = $el.data('height');
            if ($height) $el.height($height);
            $el.plot(data, opts);
        }
    });
</script>


@endpush