
{{--Pending payment vouchers table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}

        <legend class="grey_modal" >@lang('labels.backend.finance.pending_payment_vouchers')</legend>
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="pending-payment-vouchers-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.claim.benefit_resource')</th>
                <th>@lang('labels.general.payee')</th>
                <th>@lang('labels.general.amount')</th>

                <th>@lang('labels.general.actions')</th>
            </tr>
            </thead>
            <tfoot>

            </tfoot>
        </table>

    </div>
</div>


@push('pending-payment-vouchers-script-end')

<script  type="text/javascript">
    $(function() {
        $("#make_payment_header").one("click", function(){
            var url = "{!! url("/") !!}";
            $('#pending-payment-vouchers-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.finance.payment.get_pending_payment_vouchers') !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'benefit_resource' , name: 'benefit_resource' },
                    { data: 'payee', name: 'payee', 'orderable' : true, 'searchable': true },
                    { data: 'amount', name: 'amount' },
                    { data: 'action', name: 'action' }
                ],

            });

        });
    });
</script>;

@endpush