
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

{!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%', 'id' => 'pending_payment_voucher_trans_batch'], true) !!}


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{!! $dataTable->scripts() !!}
<script>
    $(function() {

    });

</script>
@endpush