<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <meta charset="utf-8" />
    {{-- <title>@lang('labels.backend.registration.organization.print_certificate.title')</title> --}}
    <title>Direct Deposit Form</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="WCF PORTAL"/>
    {{-- <meta name="subject" content="{!! env('COMPANY') !!}, @lang('labels.backend.registration.organization.print_certificate.subject')"/> --}}
    <meta name="keywords" content="certificate"/>
    <meta name="date" content="2017-08-04"/>
    {{ Html::style(asset_url() . "/nextbyte/css/bill.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}

    <style type="text/css">
        {{-- @page { margin: 20px 25px; } --}}
    footer { position: fixed; bottom: 100px; left: 0px; right: 0px; font-family: Arial, sans-serif; /*background-color: lightblue; height: 50px; */}
    /*p { page-break-after: always; }*/
    /*p:last-child { page-break-after: never; }*/

</style>

</head>
<body>



    <footer>

        <p class="center copyright">Workers Compensation Fund &copy; {!! Carbon\Carbon::parse('now')->format('Y')!!}, All rights reserved.</p>
        <p class="center copyright">Powered By Government Payment Gateway (GePG).</p>
    </div>
</footer>

<div id="certificate">
    <div class="center">
        <img src="{!! asset_url() . "/nextbyte/img/wcf_big_logo_no_background.png"!!}" style="height: 70px;">
        <div class="h4">WORKERS COMPENSATION FUND</div>
        @if(is_null($bill->employer_id))
        <p>Non Contribution Bill</p>
        @else
        <p>Employer's Bill</p>
        @endif
        
        <hr class="style9">
    </div>
    <div class="row">
        <p>Control Number : {{$bill->payments[0]->control_no}}</p>
        @if(is_null($bill->employer_id))
        <p>Payer Name : {{strtoupper($payer_name )}}</p>
        @else
        <p>Name Of Employer : {{strtoupper($bill->employer->name)}}</p>
        <p>Registration Number : {{substr($bill->employer->reg_no,-6)}}</p>
        @endif
        <p>Description : {{$bill->bill_description}}</p>
        <p>Mobile Phone : {{$bill->mobile_number}}</p>
        <hr class="style9">
        <p>Item(s) billed: {{$bill->billed_item}} </p>
        <hr class="style9">
        <p>Total Amount : TZS. {{number_format($bill->bill_amount,2)}}</p>
        <p>Amount in Words: {{ucfirst(convert_number_to_words($bill->bill_amount))}} </p>
        <p>Expiry Date : {!! Carbon\Carbon::parse($bill->expire_date)->format('d-M-Y')!!}</p>
        <p>Printed On : {!! Carbon\Carbon::parse('now')->format('d-M-Y')!!}</p>
        <p>Printed By : {{ucwords(access()->user()->name)}}</p>
        <br><br><br>
        <div class="text-justify">
            <h4>How to Pay / Jinsi Ya Kulipa;</h4>
            <p>
                Payment can be made through Bank (CRDB or NMB) or Mobile (AirtelMoney/ Halopesa/ Mpesa/ Tigopesa with business number 888999). Use the control number provided as your payment reference. For more information please contact 0800 00 11 28/ 0800 00 11 29.
            </p>
            <p>

                Malipo yafanyike kupitia Benki (CRDB or NMB) au mitandao ya simu (AirtelMoney/ Halopesa/ Mpesa/ Tigopesa kwa kutumia namba ya kampuni 888999). Tumia namba (control number) uliyopewa kama kumbukumbu ya malipo. Kwa maelezo zaidi wasiliana nasi kupitia namba 0800 00 11 28/ 0800 00 11 29.

            </p>
        </div>

    </div>

    <script type="text/javascript">
        window.onload = function() {
            window.print();
            window.close();
        };
    //     window.onfocus=function(){ window.close();}

</script>
</body>
</html>