<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <meta charset="utf-8" />
    <title>@lang('labels.backend.finance.receipt.print.title')</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Martin Luhanjo <m.luhanjo@nextbyte.co.tz>"/>
    <meta name="subject" content="{!! env('COMPANY') !!}, @lang('labels.backend.finance.print_voucher.subject')"/>
    <meta name="keywords" content="payment_vouchers"/>
    <meta name="date" content="2017-08-04"/>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/print_payment_voucher.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
</head>
<body>


{{--end: Changes subject to be modified or removed in future--}}

<div class="page">
    {{--<div class="{!! $class !!}">--}}
    {{--</div>--}}


    <div class="heading">
        {{--wcf header--}}
        <table  class="border-less "  style="width:100%;text-align:center;">
            <tr>
                <th class="border-less color-blue" > {!!  strtoupper(trans('labels.general.wcf'))  !!}</th>
            </tr>
        </table>
        <div class="logo">

            <img  class="logo_img" src="{!! asset_url() . "/nextbyte/img/wcf_big_logo_no_background.png" !!}">
        </div>

    </div>

    <div >
        {{--Header--}}
        <table   style="width:100%;text-align:center;">
            <tr>
                <th class="single_row"> {!!  strtoupper(trans('labels.backend.finance.payment_voucher'))  !!}</th>
            </tr>
        </table>
        {{--end header -----------------}}

        {{--Reference-================--}}
        <table  style="width:100%;text-align:center;">
            <tr>
                <th  class="single_row" style="width: 66mm;"> {!!  (trans('labels.general.preparing_office'))  !!}</th>
                <th style="width: 66mm"> {!!  (trans('labels.general.reference'))  !!}</th>
                <th style="width: 66mm"> {!!  (trans('labels.backend.finance.payment_voucher'))  !!}</th>
            </tr>
            <tr>
                <th  class="single_row" style="width: 66mm"> {!!  strtoupper(trans('labels.general.accounts'))  !!}</th>
                <th style="width: 66mm"> {!! $notification_report->filename  !!}</th>
                <th style="width: 66mm"> {!!  $payment_voucher->id_formatted !!}</th>
            </tr>
        </table>

        {{--end reference -----------------}}

        {{--beneficiary info-=================--}}
        <table  style="width:100%;text-align:center;">
            <tr>
                <td style="width: 105mm;text-align:left;padding: 2mm">
                    <div class="pull-left">
                        @lang('labels.backend.finance.please_pay'):

                        <table class="border-less" style="width:100%;text-align:left; ">
                            <tr>
                                <td class="border-less" style="width: 20mm"> {!!  (trans('labels.general.name'))  !!}:</td>
                                <td  class="border-less" style="width: 80mm"> {!!  strtoupper($member->name) !!}</td>
                            </tr>

                            <tr>
                                <td  class="border-less " style="width: 20mm"> {!!  (trans('labels.general.address'))  !!}:</td>
                                <td  class="border-less" style="width: 80mm">{!!  strtoupper($member->address()) !!} </td>

                            </tr>
                        </table>


                    </div>
                </td>

                {{--Account codes and amount--}}

                <td style="width: 93mm;padding: 2mm">

                    <table class="border-less" style="width:100%;text-align:left; ">
                        <tr>
                            <th class="border-less" style="width: 40mm"> {!!  (trans('labels.backend.finance.account_code'))  !!}</th>
                            <th  class="border-less" style="width: 53mm"> {!!   (trans('labels.general.amount')) !!}</th>
                        </tr>
                        {{--PV Transactions--}}
                        @foreach($payment_voucher->paymentVoucherTransactions as $paymentVoucherTransaction)
                            <tr >
                                <td  class="border-less " style="width: 40mm;"> {!!  $paymentVoucherTransaction->benefitType->finAccountCode->account_code  !!}</td>
                                <td  class="border-less underline_v2" style="width: 53mm;"> {!! $paymentVoucherTransaction->amount_formatted !!}</td>

                            </tr>
                        @endforeach
                    </table>

                </td>

            </tr>

        </table>
        {{--benefiriciary end--------------------}}


        {{--Payment Description =================--}}
        <table  style="width:100%;text-align:center;">
            <tr>
                <th  class="single_row" style="width: 50mm"> {!!  strtoupper(trans('labels.general.date'))  !!}</th>
                <th style="width: 98mm"> {!!  strtoupper(trans('labels.general.particulars_of'))  !!}</th>
                <th style="width: 50mm"> {!!  strtoupper(trans('labels.general.amount'))  !!}</th>
            </tr>
            <tr>
                <th  class="single_row" style="width: 50mm"> {!!  \Carbon\Carbon::parse('now')->format('d-M-Y')  !!}</th>
                <th style="width: 98mm"></th>
                <th style="width: 50mm"> {!! (trans('labels.general.tzs')) !!}</th>
            </tr>

            <tr>
                {{--col 1--}}
                <td style="width: 50mm">


                    <table class="border-less" style="width:100%;text-align:center; ">
                        <tr>
                            <td class="border-less" style="width: 100%;"> {!!  (trans('labels.general.prepared_by'))  !!}</td>

                        </tr>


                        <tr>
                            <td class="border-less">  </br>  </td>

                        </tr>

                        {{--Username prepared by--}}
                        <tr >
                            <th  class="border-less " style="width: 100%;"> {!!  access()->user()->username  !!}</th>
                        </tr>

                    </table>

                </td>

                {{--col 2 particulars--}}
                <td style="width: 98mm;text-align:left;padding:2mm">
                    {!! $payment_description !!}
                </td>
                {{--col 3 amount--}}
                <th style="width: 50mm">
                    {!! $payment_voucher->amount_formatted !!}
                </th>
            </tr>
        </table>

        {{--Amount in words and total amount--}}
        <table  style="width:100%;">
            <tr>
                <td style="width: 154mm;text-align:right; ">
                    <div class="amount_words_v2 ">
                        <b>@lang('labels.backend.finance.receipt.shillings_in_words'):</b>&nbsp;&nbsp;
                        <div class="underline_v2">
                            <span > {!! ucfirst(convert_number_to_words($payment_voucher->amount)) !!}</span>
                        </div>
                    </div>

                </td>

                <th >

                </th>
            </tr>


            <tr>
                <th  class="single_row" style="width: 148mm;text-align:right; ">
                    {!! strtoupper(trans('labels.general.total')) !!}:
                </th>

                <th style="width: 50mm" > {!!  $payment_voucher->amount_formatted  !!}</th>

            </tr>



        </table>

        {{--end payment description---------------}}



        {{--i certify--}}
        <div>
            </br>
            @lang('labels.general.i_certify')
        </div>

        </br>
        {{-----Approved By-================--}}
        <table  style="width:100%;text-align:center;">
            <tr>
                <td style="width: 50mm"> {!!  (trans('labels.general.name_of_officer'))  !!}</td>
                <td style="width: 50mm"> {!!  (trans('labels.backend.finance.receipt.signature'))  !!}</td>
                <td style="width: 50mm"> {!!  (trans('labels.general.date'))  !!}</td>
                <td style="width: 50mm"> {!!  (trans('labels.backend.finance.receipt.designation'))  !!}</td>
            </tr>
            <tr>
                {{--<td style="width: 50mm"> {!!  count($finance_wf_track->user) ? $finance_wf_track->user->name : ' '!!}</td>--}}
                <td style="width: 50mm"> </td>
                <td style="width: 50mm"> </td>
                {{--<td style="width: 50mm"> {!! count($finance_wf_track->forward_date_formatted) ? $finance_wf_track->forward_date_formatted : ' '  !!}</td>--}}
                <td style="width: 50mm"> {!! ' '  !!}</td>
                <td style="width: 50mm"> {!! access()->user()->designationTable->name !!}</td>
            </tr>
        </table>
        {{--received amount stated--}}
        <div>
            </br>
            {!! strtoupper(trans('labels.general.received_amount_stated')) !!}

        </div>
        </br>
        {{--end approved -----------------}}


        {{--Bottom signature-====================--}}
        <table class="border-less"  style="width:100%;text-align:center;">

            <tr>
                <td  class="border-less " style="width: 66mm">....................................................</td>
                <td class="border-less " style="width: 66mm">....................................................</td>
                <td  class="border-less " style="width: 66mm">....................................................</td>
            </tr>

            <tr>
                <td class="border-less" style="width: 66mm"> {!!  (trans('labels.backend.finance.receipt.signature'))  !!}</td>
                <td class="border-less" style="width: 66mm"> {!!  (trans('labels.general.witness_identification'))  !!}</td>
                <td class="border-less" style="width: 66mm"> {!!  (trans('labels.general.date'))  !!}</td>
            </tr>

        </table>


        {{--end Bottom signature--}}


    </div>

</div>



<script type="text/javascript">
    /*window.onload = function() { window.print(); window.close();};*/
    /* window.onfocus=function(){ window.close();} */
    window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
</script>
</body>
</html>