
{{--Pending Interest Refund table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}

        <legend class="grey_modal" >@lang('labels.backend.finance.interest_refunds_be_processed')</legend>
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="pending-interest-refunds-table">
            <thead>
            <tr>
                <th>Refund No.</th>
                <th>@lang('labels.general.payee')</th>
                <th>@lang('labels.general.amount')</th>
            </tr>
            </thead>

        </table>

    </div>
</div>


@push('after-script-end')

<script  type="text/javascript">
    $(function() {

            var url = "{!! url("/") !!}";
            $('#pending-interest-refunds-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.finance.payment.get_pending_interest_refunds') !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'refund_id' , name: 'refund_id' },
                    { data: 'payee', name: 'payee', 'orderable' : true, 'searchable': true },
                    { data: 'amount', name: 'amount' },

                ],

            });

        });

</script>;

@endpush