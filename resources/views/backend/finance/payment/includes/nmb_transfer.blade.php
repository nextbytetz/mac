<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <meta charset="utf-8" />
    {{-- <title>@lang('labels.backend.registration.organization.print_certificate.title')</title> --}}
    <title>NMB transfer form</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="WCF PORTAL"/>
    {{-- <meta name="subject" content="{!! env('COMPANY') !!}, @lang('labels.backend.registration.organization.print_certificate.subject')"/> --}}
    <meta name="keywords" content="certificate"/>
    <meta name="date" content="2017-08-04"/>
    {{ Html::style(asset_url() . "/nextbyte/css/bill.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
    <style type="text/css">
    @page { margin: 20px 25px; }
footer { position: fixed; bottom: 190px; left: 0px; right: 0px; font-family: Arial, sans-serif; /*background-color: lightblue; height: 50px; */}
/*p { page-break-after: always; }*/
/*p:last-child { page-break-after: never; }*/

</style>

</head>
<body>


 <footer>

    <strong><span class="sml">Note to Commercial Bank:</span></strong>
    <ol class="text-justify sml">
     <li> Please capture the above information correctly. Do not change or add any text, symbols or digits on the information
     provided.</li>
     <li> Field 59 is an "Account Number" with value: <strong> /20110031399.</strong> Must be captured correctly.</li>
     <li> Field 70 is a "Control Number" with value: <strong>/ROC/{{$bill->payments[0]->control_no}}.</strong> Must be captured correctly.</li>
 </ol>
 <br>
 <p class="center copyright">Workers Compensation Fund &copy; {!! Carbon\Carbon::parse('now')->format('Y')!!}, All rights reserved.</p>
 <p class="center copyright">Powered By Government Payment Gateway (GePG).</p>
</footer>

<div id="certificate">
    <div class="center">
        <img src="{!! asset_url() . "/nextbyte/img/wcf_big_logo_no_background.png"!!}" style="height: 70px;">
        <div class="h4">WORKERS COMPENSATION FUND</div>
        Order Form for Electronic Funds Transfer to NMB Bank PLC
        <hr class="style9">
    </div>
    <table cellpadding=0 cellspacing=0 class="t0">
        <tr>
            <td colspan=2 class="tr0 td0"><p class="p3 ft2">(a). Remitter / Employer Details :-</p></td>
        </tr>
        <tr>
            <td class="tr1 td1"><p class="p4 ft2">Account Number</p></td>
            <td class="tr1 td2"><p class="p5 ft2">: ........................................................................................................................</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="tr2 td1"><p class="p4 ft3">Account Name</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: ........................................................................................................................</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td class="tr2 td1"><p class="p4 ft3">Bank Name</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: ........................................................................................................................</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td class="tr2 td1"><p class="p3 ft2">(b). Beneficiary Details :-</p></td>
            <td class="tr2 td2"><p class="p5 ft2">&nbsp;</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Account Number</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: /20110031399</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Account Name </p></td>
            <td class="tr2 td2"><p class="p5 ft2">: WORKERS COMPENSATION FUND</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Bank Name </p></td>
            <td class="tr2 td2"><p class="p5 ft2">: NMB Bank PLC </p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">SWIFT Code</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: NMIBTZTZ</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Control Number</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: /ROC/{{$bill->payments[0]->control_no}}</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Bill Amount</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: TZS. {{number_format($bill->bill_amount,2)}}</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
<!-- 
            <tr>
                <td class="tr1 td1"><p class="p4 ft2">Amount in Words</p></td>
                <td class="tr1 td2"><p class="p5 ft2">: Twelve million Four Hundred <NOBR>Thirty-Four</NOBR> thousand Four Hundred</p></td>
            </tr>

            <tr>
                <td class="tr4 td1"><p class="p3 ft4">&nbsp;</p></td>
                <td class="tr1 td2"><p class="p5 ft2"> <NOBR> Forty-Three</NOBR> Tanzanian Shilling and <NOBR>Forty-Three</NOBR> cent(s).</p></td>
            </tr>
        -->

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Being payment for</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: {{$bill->bill_description}}</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>


        <tr>
            <td class="tr2 td1"><p class="p4 ft2"> Billed Item</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: {{$bill->billed_item}} </p></td>
        </tr>


        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Expiry Date</p></td>
            <td class="tr2 td2"><p class="p5 ft2">:  {!! Carbon\Carbon::parse($bill->expire_date)->format('d-M-Y')!!}</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Printed on</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: {!! Carbon\Carbon::parse('now')->format('d-M-Y')!!}</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>


        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Printed By</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: {{ucwords(access()->user()->name)}}</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Signature</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: .................................................................. &nbsp; Date: .......................................</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td class="tr2 td1"><p class="p4 ft2">Signature</p></td>
            <td class="tr2 td2"><p class="p5 ft2">: ................................................................. &nbsp; Date: ........................................</p></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
</div>

<script type="text/javascript">
    window.onload = function() {
        window.print();
        window.close();
    };
    //     window.onfocus=function(){ window.close();}

</script>
</body>
</html>