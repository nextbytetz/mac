
{{--Pending payment vouchers for export to ERP table--}}

<div class = "row">
    <div class="col-md-12" >

        {{--<div>&nbsp; </div>--}}

        <legend class="grey_modal" >@lang('labels.backend.finance.pending_pv_trans_for_export')</legend>
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="pending-pv-trans-for-export-table">
            <thead>
            <tr>
                <th>Filename</th>
                <th>Payee</th>
                <th>Memberno</th>
                <th>Member Type</th>
                <th>Benefit Type</th>
                <th>Amount</th>
                <th>Debit Account</th>
                <th>Credit Account</th>
                <th>Accountno</th>
                <th>Bank Name</th>
                <th>Swift Code</th>
                <th>Bank Branch id</th>
                <th>Bank Address</th>
                <th>Country</th>
                <th>City of Bank</th>
                <th>Region</th>

            </tr>
            </thead>
            <tfoot>

            </tfoot>
        </table>

    </div>
</div>


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            $("#pv_tran_export_header").one("click", function(){
                var url = "{!! url("/") !!}";
                $('#pending-pv-trans-for-export-table').DataTable({
                    processing: true,
                    serverSide: false,
                    stateSave: true,
                    buttons: ['reset', 'reload', 'colvis'],
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.finance.payment.get_pv_trans_for_export') !!}',
                        type : 'get'
                    },
                    columns: [
                        { data: 'filename' , name: 'filename', 'orderable' : false, 'searchable': false },
                        { data: 'employee_name', name: 'employee_name', 'orderable' : false, 'searchable': false },
                        { data: 'memberno' , name: 'memberno', 'orderable' : false, 'searchable': false },
                        { data: 'member_type', name: 'member_type', 'orderable' : false, 'searchable': false },
                        { data: 'benefit_type' , name: 'benefit_type', 'orderable' : false, 'searchable': false },
                        { data: 'amount', name: 'amount', 'orderable' : false, 'searchable': false },
                        { data: 'debit_account_expense' , name: 'debit_account_expense', 'orderable' : false, 'searchable': false },
                        { data: 'credit_account_receivable', name: 'credit_account_expense', 'orderable' : false, 'searchable': false },
                        { data: 'accountno' , name: 'accountno', 'orderable' : false, 'searchable': false },
                        { data: 'bank_name', name: 'bank_name', 'orderable' : false, 'searchable': false },
                        { data: 'swift_code' , name: 'swift_code', 'orderable' : false, 'searchable': false },
                        { data: 'bank_branch_id', name: 'bank_branch_id', 'orderable' : false, 'searchable': false },
                        { data: 'bank_address' , name: 'bank_address', 'orderable' : false, 'searchable': false },
                        { data: 'country_name_bank', name: 'country_name_bank', 'orderable' : false, 'searchable': false },
                        { data: 'city_of_bank' , name: 'city_of_bank', 'orderable' : false, 'searchable': false },
                        { data: 'state_or_region', name: 'state_or_region', 'orderable' : false, 'searchable': false, },
                    ],

                });

            });
        });
    </script>;

@endpush