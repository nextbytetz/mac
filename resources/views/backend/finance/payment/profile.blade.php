@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.process_payment'), 'header_title' => trans('labels.backend.finance.process_payment')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')



    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#general" data-toggle="tab">@lang('labels.backend.finance.receipt.general')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id ="payment_vouchers_individual_header" href="#payment_vouchers_individual" data-toggle="tab">@lang('labels.backend.finance.payment_vouchers_individual')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id ="payment_vouchers_batch_header" href="#payment_vouchers_batch" data-toggle="tab">@lang('labels.backend.finance.payment_vouchers_batch')</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id ="make_payment_header" href="#make_payment" data-toggle="tab">@lang('labels.backend.finance.make_payment')</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id ="pv_tran_export_header" href="#pv_tran_export" data-toggle="tab">@lang('labels.backend.finance.export_to_erp')</a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">
                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            {{--Header Bar--}}
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        {{--process payment--}}
                                        <span>
	                                             {!! HTML::decode( link_to_route('backend.finance.payment.process',"<i class='icon fa fa-money ' aria-hidden='true'></i>&nbsp;". trans('buttons.backend.finance.process_payment') ,[], ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.finance.process_payment_confirm'), 'class' => 'btn btn-primary site-btn nav_button ']))  !!}

    </span>


                                        {{--close--}}
                                        <span>
						        <a href="{!! route('backend.finance.menu') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
						</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--MAIN tab contents--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-12">
                                    {{--Payment Compnesations to be Processed--}}
                                    <legend class="grey_modal" >@lang('labels.backend.finance.compensation_to_be_processed')</legend>
                                    </br>
                                    {!! $pending_process_compensations_datatable->render('backend.finance.payment.includes.get_pending_process_compensations') !!}
                                    <div>&nbsp;</div>

                                    {{--Funeral Grants--}}
                                    <legend class="grey_modal" >@lang('labels.backend.finance.funeral_grants_to_be_processed')</legend>
                                    </br>
                                    {!! $pending_funeral_grants_datatable->render('backend.finance.payment.includes.get_pending_funeral_grants') !!}



                                    {{--Survivor Gratuity--}}
                                    <legend class="grey_modal" >@lang('labels.backend.finance.survivor_gratuity_be_processed')</legend>
                                    </br>
                                    {!! $pending_survivor_gratuity_datatable->render('backend.finance.payment.includes.get_pending_survivor_gratuity') !!}




                                    {{--Interest Refunds--}}


                                    @include('backend.finance.payment.includes.get_pending_interest_refunds')

                                </div>

                            </div>
                        </div>


                    </div>
                    <div id="payment_vouchers_individual" class="nav_tab_pane tab-pane">
                        <div class="nav_tab_pane_header">
                            {{--Header Bar--}}
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        {{--process voucher individual--}}
                                        <span>
                                        {!! HTML::decode( link_to_route('backend.finance.payment.process_voucher_transactions',"<i class='icon fa fa-money ' aria-hidden='true'></i>&nbsp;". trans('buttons.backend.finance.process_voucher') ,1, ['data-method' => 'confirm','data-type'=>'success', 'confirm-button-color'=> '#2e74ff', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.finance.process_voucher_confirm'), 'class' => 'btn btn-primary site-btn nav_button  ']))  !!}

</span>

                            </div>
                        </div>
                    </div>
                </div>
                {{--Payment Vouchers Trans Individual--}}

                <div class="row">
                    <div class="col-md-12">

                        <div class="col-md-12">
                            {{--Pending Payment Vouchers Trans Individual--}}
                            <legend class="grey_modal" >@lang('labels.backend.finance.pending_payment_vouchers_transactions_individual')</legend>

                            </br>
                            {!! $pending_payment_voucher_trans_individual_datatable->render('backend.finance.payment.includes.get_pending_payment_voucher_trans_individual') !!}

                                </div>



                            </div>
                        </div>

                    </div>
                    {{--TAB - payment_vouchers_batch--}}
                    <div id="payment_vouchers_batch" class="nav_tab_pane tab-pane">

                        <div class="nav_tab_pane_header">
                            {{--Header Bar--}}
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        {{--process voucher batch--}}
                                        <span>
                                        {!! HTML::decode( link_to_route('backend.finance.payment.process_voucher_transactions',"<i class='icon fa fa-money ' aria-hidden='true'></i>&nbsp;". trans('buttons.backend.finance.process_voucher') ,2, ['data-method' => 'confirm','data-type'=>'success', 'confirm-button-color'=> '#2e74ff', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.finance.process_voucher_confirm'), 'class' => 'btn btn-primary site-btn nav_button  ']) )  !!}

</span>

                             </div>
                         </div>
                     </div>
                 </div>
                 {{--Payment Vouchers Trans Individual--}}

                 <div class="row">
                     <div class="col-md-12">

                         <div class="col-md-12">
                             {{--Pending Payment Vouchers Trans Individual--}}
                             <legend class="grey_modal" >@lang('labels.backend.finance.pending_payment_vouchers_transactions_batch')</legend>

                             </br>
                             {!! $pending_payment_voucher_trans_batch_datatable->render('backend.finance.payment.includes.get_pending_payment_voucher_trans_batch') !!}

                                </div>

                            </div>
                        </div>




                    </div>
                    <div id="make_payment" class="nav_tab_pane tab-pane">
                        @include('backend.finance.payment.includes.get_pending_payment_vouchers')
                    </div>

                    <div id="pv_tran_export" class="nav_tab_pane tab-pane">
                        <div class="nav_tab_pane_header">
                            {{--Header Bar--}}
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        {{--process voucher batch--}}
                                        <span>
                                        {!! HTML::decode( link_to_route('backend.finance.payment.export_pv_tran_to_erp',"<i class='icon fa fa-money ' aria-hidden='true'></i>&nbsp;". 'Export To ERP' ,2, ['data-method' => 'confirm','data-type'=>'success', 'confirm-button-color'=> '#2e74ff', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you want to export to ERP?', 'class' => 'btn btn-primary site-btn nav_button  ']) )  !!}

</span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('backend.finance.payment.includes.get_pending_pv_trans_for_export')
                    </div>
                </div>

            </div>
        </div>
    </div>



    {{--{!! Form::close() !!}--}}


@stop

@push('after-script-end')
@stack('pending-payment-vouchers-script-end')
{{ Html::script(asset_url(). "/nextbyte/js/backend/contribution-progress.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script>
    $(function () {

        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
                //var id = this.id;
                //alert(id);
            } else {
                location.hash = '#' + tab;
            }
        });
    });
</script>

{{--WORKFLOW DATATABLE SCRIPT--}}
<script  type="text/javascript">

    $(function() {

    });

</script>

@endpush
