@extends('layouts.backend.main', ['title' => 'Reconciliation', 'header_title' => 'Reconciliation'])


@section('content')
{{--<section id="content-wrapper">--}}
    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.view.successfull.trx') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Successfull WCFePG Transactions <label class="label label-success text-white">&nbsp;</label></large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                View paid Control Numbers
                            </p>
                        </li>
                    </a>

                </ul>
            </div>
           <br>
           <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.view.successfull.GePG.trx') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;GePG Daily Reconciliation Module <label class="label label-info text-white">&nbsp;</label></large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                View paid Control Numbers but no receipts issued in MAC
                            </p>
                        </li>
                    </a>

                </ul>
            </div>
            <br>
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.view.successfull.GePG.trx') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Search Control Numbers <label class="label label-info text-white">&nbsp;</label></large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                Search and View status of each control number
                            </p>
                        </li>
                    </a>

                </ul>
            </div>
        </div>



        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.view.failed.trx') !!}">
                        <li  class="border-less" class="list-group-item" >  
                            <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i>
                                <large>
                                &nbsp;Pending WCFePG Transactions <label class="label label-danger text-white">&nbsp; </label>
                                </large>
                            </h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                View Cancelled Or Pending Control Numbers
                            </p> 
                        </li>
                    </a>




                </ul>


            </div>
            <br>
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.view.failed.trx') !!}">
                        <li  class="border-less" class="list-group-item" >  
                            <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i>
                                <large>
                                &nbsp;Paid Control numbers with no receipts<label class="label label-danger text-white">&nbsp; </label>
                                </large>
                            </h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                View all paid controls but receipts not automatically generated
                            </p> 
                        </li>
                    </a>




                </ul>


            </div>

        </div>


    </div>

{{--</section>--}}

@stop


@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */
         $("#preview").click(function() {
         });
     });
 </script>;

 @endpush