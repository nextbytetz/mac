@extends('layouts.backend.main', ['title' => 'ERP Menu', 'header_title' => 'ERP Menu'])


@section('content')
{{--<section id="content-wrapper">--}}
    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.successfull.gl.erp') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Successfull Posted General Ledger to ERP <label class="label label-success text-white">&nbsp;({{$successfully_gl_count}}) &nbsp; </label></large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                @lang('labels.backend.finance_menu.gl_to_erp')
                            </p>
                        </li>
                    </a>

                </ul>
            </div>
            <br>
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.view.successfull.trx') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Successfull Posted Account Payable to ERP <label class="label label-success text-white">&nbsp;({{$successfully_ap_count}}) &nbsp; </label></large></h6> 

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                               @lang('labels.backend.finance_menu.ap_to_erp')
                           </p>
                       </li>
                   </a>

               </ul>
           </div>
           <br>
           <div class="list-group">
            {{--item 1--}}
            <ul class="list-unstyled">
                <a href="{!! route('backend.finance.view.successfull.GePG.trx') !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Successfull  Claims Payments from ERP <label class="label label-info text-white">&nbsp;({{$successfully_ap_paid_count}}) &nbsp; </label></large></h6>

                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                           @lang('labels.backend.finance_menu.ap_from_erp')
                       </p>
                   </li>
               </a>

           </ul>
       </div>

       <br>
       <div class="list-group">
        {{--item 1--}}
        <ul class="list-unstyled">
            <a href="{!! route('backend.finance.view.contrib.refund') !!}">
                <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Contribution Refund To ERP <label class="label label-info text-white">&nbsp;({{$contrib_refund_count}}) &nbsp; </label></large></h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                       @lang('labels.backend.finance_menu.ap_from_erp')
                   </p>
               </li>
           </a>

       </ul>
   </div>


</div>



{{--right div--}}

<div class="col-sm-6 col-md-6">
    <div class="list-group">
        {{--item 1--}}
        <ul class="list-unstyled">
            <a href="{!! route('backend.finance.view.failed.trx') !!}">
                <li  class="border-less" class="list-group-item" >  
                    <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i>
                        <large>
                            &nbsp;Pending General Ledger Transactions to ERP <label class="label label-danger text-white">&nbsp;({{$pending_gl_count}})&nbsp; </label>
                        </large>
                    </h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                     @lang('labels.backend.finance_menu.gl_to_erp_pending')
                 </p> 
             </li>
         </a>




     </ul>


 </div>
 <br>
 <div class="list-group">
    {{--item 1--}}
    <ul class="list-unstyled">
        <a href="{!! route('backend.finance.view.failed.trx') !!}">
            <li  class="border-less" class="list-group-item" >  
                <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i>
                    <large>
                        &nbsp;Pending Account Payable Transactions to ERP <label class="label label-danger text-white">&nbsp;({{$pending_ap_count}}) &nbsp; </label>
                    </large>
                </h6>

                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                    @lang('labels.backend.finance_menu.ap_to_erp_pending')
                </p> 
            </li>
        </a>




    </ul>


</div>
<br>
<div class="list-group">
    <ul class="list-unstyled">
        <a href="{!! route('backend.finance.search.view') !!}">
            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-truck"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.erp_supplier_employer')</large></h6>


                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.erp_supplier_summary')</p> </li>

            </a>
        </ul>


    </div>
<br>
<div class="list-group">
    <ul class="list-unstyled">
        <a href="{!! route('backend.finance.search.view.employee') !!}">
            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-user"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.erp_supplier_employee')</large></h6>


                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.erp_supplier_summary_employee')</p> </li>

            </a>
        </ul>


    </div>


    <br>
    <div class="list-group">
        <ul class="list-unstyled">
            <a href="{!! route('backend.compliance.dependent.ready_for_export_to_erp') !!}">
                <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-user"> </i><large>&nbsp;&nbsp;Create Supplier to ERP (Dependents)</large></h6>


                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Retrieve supplier from list of dependents i.e. survivors and constant care assistants then post to ERP for payment processing</p> </li>

            </a>
        </ul>


    </div>
</div>


</div>


&nbsp;

{{--</section>--}}

@stop


@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */
         $("#preview").click(function() {
         });
     });
 </script>;

 @endpush