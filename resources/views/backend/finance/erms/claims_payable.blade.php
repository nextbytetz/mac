{{-- {{dd($id)}} --}}
@extends('layouts.backend.main', ['title' => 'Claims Payable', 'header_title' =>' Claims Payable'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
	.btn-blue {
		background: #666699;
		color: white;
	}
	.drop {
		margin-top: 20pt;
	}
	/*.rotation-animation {
		animation: div-rotate 0.7s infinite steps(8);
		transform: translateZ(0);
		transform-origin: 50% 50%;
	}

	@keyframes div-rotate {
		0% { transform: rotate(  0deg) scale(0.1);}
		100% { transform: rotate(360deg) scale(0.1);}
	}
	.first {
		font-size: 200px;
		margin: -30px;
	}*/
</style>
@endpush

@section('content')
     <div class="custom_filter">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                {!! Form::open(['role' => 'form', 'id' => 'status-search']) !!}
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="workplaces">Status:</label>
                        {!! Form::select('status', ['200' => 'Pending', '202' => 'Accepted', '201' => 'Paid', '400' => 'Failed', '-1' => 'Not sent'], null, ['class' => 'form-control search-status', 'id' => 'status', 'placeholder' => '']) !!}
                    </div>
                    <div class="form-group col-md-1">
                        <input type="submit" class="btn btn-success btn-sm btn-submit drop" value="@lang('buttons.general.search')" />
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="pull-right mb-1">
			</div>
			<table class="display table table-bordered" cellspacing="0" width="100%" id ="claims_payable_datatable">
				<thead>
					<tr>
						<th>Member Type</th>
						<th>Case #</th>
						<th>Transaction Id</th>
						<th>Payee name</th>
						<th>Benefit Type</th>
						<th>Account #</th>
						<th>Bank name</th>
						<th>amount</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

@stop
@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/moment_precise/js/moment_precise.js") }}

<script  type="text/javascript">

	$(function () {

		$(".search-status").select2({
            allowClear: true,
        });

		$('.money').maskMoney({
			precision : 4,
			affixesStay : true
		});

		let datatable = $('#claims_payable_datatable').DataTable({
			dom : 'Bfrtip',
            buttons : ['colvis','excel','csv'],
            initComplete : function () {
                datatable.buttons().container().insertBefore('#claims_payable_datatable');
                $("#claims_payable_datatable").css("width","100%");
            },
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			order: [[ 0, "desc" ]],
			columnDefs: [
			// { className: "dt-right", "targets": [2,3] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{{ route('backend.finance.erms.get_claims_payable') }}',
				type : 'get',
				data : function ($d) {
                    $d.status = $('select[name=status]').val();
                }
			},
			columns: [
			{ data: 'member_type' , name: 'member_type'},
			{ data: 'notification_id' , name: 'notification_id'},
			{ data: 'trx_id' , name: 'trx_id'},
			{ data: 'payee_name' , name: 'payee_name'},
			{ data: 'benefit_type' , name: 'benefit_type'},
			{ data: 'accountno' , name: 'accountno'},
			{ data: 'bank_name' , name: 'bank_name'},
			{ data: 'amount' , name: 'amount'},
			{ data: 'erms_status_code' , name: 'erms_status_code'},
			{ data: 'action' , name: 'action', searchable: false, orderable: false},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});

		$('#claims_payable_datatable').on('click' ,'.repost', function($e){
			console.log($(this).attr('erms_status_code'))
			if($(this).attr('erms_status_code') == '' || $(this).attr('erms_status_code') == 400){
				$('#claims_payable_datatable .repost').attr('disabled', 'disabled')
				postClaimsPayable($(this).attr('Id'))
			}
		})

		function postClaimsPayable(Id){
			let url = '{{ url('finance/erms/claims_payable/post') }}/' + Id;
			$.ajax({
				type: 'post',
				url: url,
				data: {
					Id:Id,
					_token: '{{ csrf_token() }}'
				},
				success:function(data){
					console.log(data)
					if(data.success){
						swal({
							title: 'Dear {{ ucwords(strtolower(auth()->user()->name)) }}',
							text: data.message,
							type: 'success',
							button: true,
							html: true,
						},function(){ 
							location.reload();
						})
					}else{
						swal({
							title: 'Dear {{ ucwords(strtolower(auth()->user()->name)) }}',
							text: data.message,
							type: 'error',
							button: true,
							html: true,
						},function(){ 
							location.reload();
						})
					}
				}
			});
		}

		$('#status-search').on('submit', function($e) {
            datatable.draw();
            $e.preventDefault();
        });
	});

</script>;

@endpush