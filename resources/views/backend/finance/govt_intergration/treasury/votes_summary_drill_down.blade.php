@extends('layouts.backend.main', ['title' => 'Treasury', 'header_title' => $employer_name.' : '.strtoupper($funding_source). ' - '.$month.' Contribution details'])
@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush
@section('content')
<div class="row">
  <div class="col-sm-12">
    <table class="table table-bordered" id="summary_drill_down_table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Employer</th>
          <th>Grosspay</th>
          <th>Contribution </th>
          <th></th>
        </tr>
      </thead>
    </table>
    <br>
  </div>
</div>


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

<script type="text/javascript">
  $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */

         var vote_code = '{{$vote_code}}';
         var check_date = '{{$check_date}}';
         var funding_source = '{{$funding_source}}';

         console.log(check_date);
         console.log(funding_source);

         $('#summary_drill_down_table').DataTable({ 
           processing: true,
           serverSide: true,
           stateSave: true,
           searching: true,
           paging: true,
           info: true,
           ajax:{
            url : '{{url('finance/treasury/votes_datatable/')}}/'+vote_code+'/'+check_date+'/'+funding_source,
            type : 'GET'
          },
          columns: [
          { data: 'name' , name: 'name',searchable : false, orderable : false },
          { data: 'grosspay' , name: 'grosspay',searchable : true, orderable : true },
          { data: 'employer_contribution' , name: 'employer_contribution',searchable : true, orderable : true },
          { data: 'firstname' , name: 'firstname',searchable : true, visible : false },
          { data: 'middlename' , name: 'middlename',searchable : true, visible : false },
          { data: 'lastname' , name: 'lastname',searchable : true, visible : false },
          // { data: 'action' , name: 'action', searchable : false, orderable : false },
          ]
        });

         

         $("#preview").click(function() {
         });
       });
     </script>;

     @endpush