@extends('layouts.backend.main', ['title' => 'Treasury', 'header_title' => 'Treasury Payroll'])


@section('content')
{{--<section id="content-wrapper">--}}
    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a>
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-calendar"> </i><large>&nbsp;Check Payroll Changes<label class="label label-default text-white" style="background-color:grey;">&nbsp; (12)&nbsp; </label></large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                View new,left employees and votes from treasury payroll
                            </p>
                        </li>
                    </a>

                </ul>
            </div>
            <br>
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a> 
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Payroll Monthly(Government Funding)<label class="label label-info text-white">&nbsp; ({{-- {{$success_trx_no_rct}} --}}) &nbsp; </label></large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                View payroll summary and actions you can do
                            </p>
                        </li>
                    </a>

                </ul>
            </div>

        </div>



        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    {{-- <a href="{!! route('backend.finance.view.failed.trx') !!}"> --}}
                        <a href="{!! route('backend.finance.treasury.payment.summary') !!}">
                            <li  class="border-less" class="list-group-item" >  
                                <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i>
                                    <large>
                                        &nbsp;Control Numbers and Payment status
                                        <label class="label label-info text-white">&nbsp; 
                                            ({{-- {{$pending_trx}} --}}) &nbsp;
                                        </label>
                                    </large>
                                </h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    View control numbers and payment status for treasury
                                </p> 
                            </li>
                        </a>




                    </ul>
                    <br>
                    <ul class="list-unstyled">
                        <a>
                            <li  class="border-less" class="list-group-item" >  
                                <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i>
                                    <large>
                                        &nbsp;Payroll Monthly(Other Funding Source) <label class="label label-danger text-white">&nbsp; ({{-- {{$pending_trx}} --}}) &nbsp; </label>
                                    </large>
                                </h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    View payroll summary and actions you can do
                                </p> 
                            </li>
                        </a>




                    </ul>

                </div>
            </div>


        </div>

    {{--</section>--}}

    @stop


    @push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */
         $("#preview").click(function() {
         });
     });
 </script>;

 @endpush