@extends('layouts.backend.main', ['title' => 'Treasury', 'header_title' => $fund.' : '.$month.'  Contribution details'])
@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush
@section('content')
<div class="row">
  <div class="col-sm-12">
    <table class="table table-bordered" id="summary_drill_down_table">
      <thead>
        <tr>
          <th>Vote Name</th>
          <th>Vote Code</th>
          <th>Total Employees</th>
          <th>Total Grosspay</th>
          <th>Contribution Amount</th>
        </tr>
      </thead>
    </table>
    <br>
  </div>
</div>


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

<script type="text/javascript">
  $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */

         var check_date = '{{$check_date}}';
         var funding_source = '{{$funding_source}}';

         console.log(check_date);
         console.log(funding_source);

         $('#summary_drill_down_table').DataTable({ 
           processing: true,
           serverSide: true,
           stateSave: true,
           searching: true,
           paging: true,
           info: true,

           ajax:{
            url : '{{url('finance/treasury/summary_datatable/')}}/'+check_date+'/'+funding_source,
            type : 'GET'
          },
          columns: [
          { data: 'vote_name' , name: 'treasury_employees_contribution.vote_name',searchable : true, orderable : false },
          { data: 'vote_code' , name: 'vote_code',searchable : true, orderable : true },
          { data: 'total_employees' , name: 'total_employees',searchable : true, orderable : true },
          { data: 'total_grosspay' , name: 'total_grosspay',searchable : true, orderable : true },
          { data: 'total_contribution_amount' , name: 'total_contribution_amount',searchable : true, orderable : true },

          ],
          "rowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).click(function() {
                    // document.location.href = $return_url + "?id=" + aData['id']; 
                    var vote = $(aData['vote_code']).data('vote_code');
                    document.location.href = '{{url('finance/treasury/votes/')}}/'+vote+'/'+check_date+'/'+funding_source;
                  }).hover(function() {
                    $(this).css('cursor','pointer');
                  }, function() {
                    $(this).css('cursor','auto');
                  });
                },
              });

         

         $("#preview").click(function() {
         });
       });
     </script>;

     @endpush