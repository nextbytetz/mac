<!-- =========================== Traesury Fail modal starts here ================================= -->

<div class="modal fade in" tabindex="-1" role="dialog" id="mdlTreasuryFailData">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="treasuryFailTitle"> </h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h6 class="text-justify">
                            <span id="fail_reason"></span> 
                        </h6>
                        <p>
                            Started : <span id="start_date"></span> &nbsp;
                            Finished: <span id="finish_date"></span>
                        </p>
                    </div>
                    <div class="col-md-12">
                     <table class="table ">
                        <thead>
                          <tr>
                            <th class="bg-primary text-white"></th>
                            <th class="bg-primary text-white">Total </th>
                            <th class="bg-primary text-white">Loaded </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Employees</td>
                            <td id='total_employees'></td>
                            <td id='loaded_employees'></td>
                        </tr>
                        <tr>
                            <td>Pages</td>
                            <td id='total_pages'></td>
                            <td id='loaded_pages'></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12"> 
                <p>
                    <strong>Kindly contact ICT for further info</strong>
                </p>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-lg btn-info" data-dismiss="modal">OK</button>
        </div>
    </div>
</div>
</div>
</div>

<!-- =========================== Traesury Fail modal ends here ================================= -->