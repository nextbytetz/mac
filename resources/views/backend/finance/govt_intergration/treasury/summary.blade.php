@extends('layouts.backend.main', ['title' => 'Treasury', 'header_title' => 'Payment Summary'])
@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
<style type="text/css">
  .table_fund tr {
    cursor: pointer;
  }

</style>
@endpush
@section('content')
<div class="row">
  @if(count($pending_bill)>0)
  <div class="col-sm-12"> 
    <div class="row">
      <div class="col-sm-12">
        <table class="table table-bordered" id="gf_datatable">
          <thead>
            <tr>
              <th colspan="5">GOVERNMENT FUNDED</th>
            </tr>
            <tr>
              <th>Name</th>
              <th>Control No#</th>
              <th>Amount</th>
              <th>Description</th>
              <th></th>
            </tr>
          </thead>
        </table>
        <br>
      </div>

      <div class="col-sm-12">
        <table class="table table-bordered" id="cf_datatable">
          <thead>
           <tr>
            <th colspan="5">COUNCIL FUNDED</th>
          </tr>
          <tr>
            <th>Name</th>
            <th>Control No#</th>
            <th>Amount</th>
            <th>Description</th>
            <th></th>
          </tr>
        </thead>
      </table>


    </div>
  </div>

</div>
@else
<div class="col-sm-12"> 
  <table class="table table-bordered table_fund table-hover">
    <tbody>
      <tr>
        <td colspan="5" class="bg-success">
          <h6>{{$previous['gf']['contrib_month']}}  Details </h6>
        </td>
      </tr>
      <tr class="active">
        <th colspan="2"></th> 

        <th>Number of Votes</th>
        <th>Number Of Employees</th>
        <th>Amount</th>
      </tr>
      <tr class='clickable-row' data-href='{{url('finance/treasury/summary/'.\Carbon\Carbon::parse($previous['gf']['contrib_month'])->endOfMonth()->format('Y-m-d'))}}/gf'>
        <th></th>
        <th>GOVERNMENT FUNDED</th>
        <td>{{$previous['gf']['votes_count']}}</td>
        <td>{{number_format($previous['gf']['employees_count'])}}</td>
        <td>{{number_format($previous['gf']['amount'],2)}}</td>
      </tr>
      <tr class='clickable-row' data-href='{{url('finance/treasury/summary/'.\Carbon\Carbon::parse($previous['cf']['contrib_month'])->endOfMonth()->format('Y-m-d'))}}/cf'>
        <th></th>
        <th>COUNCIL FUNDED</th>
        <td>{{$previous['cf']['votes_count']}}</td>
        <td>{{number_format($previous['cf']['employees_count'])}}</td>
        <td>{{number_format($previous['cf']['amount'],2)}}</td>
        <br>
      </tr>
      <tr >
        <td colspan="5" class="bg-success">
          <h6> <span id="current_month">{{$contribution['gf']['contrib_month']}}</span>  Details </h6>
        </td>
      </tr>
      <form   id="frmTreasuryPay">
        {{ csrf_field() }}
        <input type="text" id="contrib_month" name="contrib_month" value="{{$contribution['month']}}" hidden="hidden" class="hidden">
        <input type="text" id="type" value="2" hidden="hidden" class="hidden">
        <tr class='clickable-row' data-href='{{url('finance/treasury/summary/'.\Carbon\Carbon::parse($contribution['gf']['contrib_month'])->endOfMonth()->format('Y-m-d'))}}/gf'>
          <th>{{-- <input type="checkbox" name="fund" class="choose_fund" value="1" checked="checked" /> --}}</th>
          <th>GOVERNMENT FUNDED</th>
          <td>{{$contribution['gf']['votes_count']}}</td>
          <td>{{number_format($contribution['gf']['employees_count'])}}</td>
          <td>{{number_format($contribution['gf']['amount'],2)}}</td>
        </tr>
        <tr class='clickable-row' data-href='{{url('finance/treasury/summary/'.\Carbon\Carbon::parse($contribution['cf']['contrib_month'])->endOfMonth()->format('Y-m-d'))}}/cf'>
          <th>{{-- <input type="checkbox" name="fund" class="choose_fund" value="2" checked="checked" /> --}}</th>
          <th>COUNCIL FUNDED</th>
          <td>{{$contribution['cf']['votes_count']}}</td>
          <td>{{number_format($contribution['cf']['employees_count'])}}</td>
          <td>{{number_format($contribution['cf']['amount'],2)}}</td>
        </tr>
        <tr>
          <th></th>
          <th colspan="3"> CURRENT TOTAL</th>

          <td>
            <span id="total">{{number_format(($contribution['cf']['amount']+$contribution['gf']['amount']),2)}} </span>
          </td>
        </tr>
        <tr>

          <td colspan="5">
            <div class="pull-right">
             <span class="btn btn-warning btn-md" id="btnRefreshData"><i class="fa fa-refresh"></i> Request Data Again </span>
             @if(isset($overall_summary->status) && ($overall_summary->status == 'success'))
             <span class="btn btn-info btn-md" id="btnTrsGenerateCn"><i class="fa fa-cogs"></i> Generate Control Number</span>
             @else
             <span class="btn btn-md btn-danger" id="BtnTreasuryfailed">Data not loaded completely</span>
             @endif


           </div>
         </td>
       </tr>
     </form>

   </tbody>

 </table>



 @endif
 <br/>
</div>


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

<script type="text/javascript">
  $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */

         @if(count($pending_bill)>0)
         var bill_id = '{{$pending_bill->bill_no}}';
         @else
         var bill_id = null;
         @endif

         $('#gf_datatable').DataTable({ 
           processing: true,
           serverSide: true,
           stateSave: true,
           searching: true,
           paging: true,
           info: true,
           ajax:{
            url : '{{url('finance/treasury/bill/')}}/'+bill_id+'/'+'GF',
            type : 'GET'
          },
          columns: [
          { data: 'name' , name: 'employers.name'},
          { data: 'control_no' , name: 'payments.control_no'},
          { data: 'bill_amount', name: 'bills.bill_amount'},
          { data: 'billed_item' , name: 'billed_item'},
          { data: 'action' , name: 'action', searchable : false, orderable : false },
          ]
        });

         $('#cf_datatable').DataTable({ 
          processing: true,
          serverSide: true,
          stateSave: true,
          searching: true,
          paging: true,
          info: true,
          ajax:{
            url : '{{url('finance/treasury/bill/')}}/'+bill_id+'/'+'CF',
            type : 'GET'
          },
          columns: [
          { data: 'name' , name: 'employers.name'},
          { data: 'control_no' , name: 'payments.control_no'},
          { data: 'bill_amount', name: 'bills.bill_amount'},
          { data: 'billed_item' , name: 'billed_item'},
          { data: 'action' , name: 'action', searchable : false, orderable : false },
          ]
        });

         $('#cf_datatable').on('click','.btnRefreshCn',function(e){
          location.reload();
        });
         $('#gf_datatable').on('click','.btnRefreshCn',function(e){
          location.reload();
        });
         // $('#cf_datatable','#gf_datatable').onclick(function(){ 
         //  var total = 0;
         //        $('input:checkbox:checked').each(function(){ // iterate through each checked element.
         //          total += isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
         //        });     
         //        $("#total").text(total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
         //        $("#totalnmb").text(total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

         //        alert(total);
         //      });

         $("#btnTrsGenerateCn").click(function(e){
          e.preventDefault();

          var total_amount = $('#total').text();
          var current_month = $('#current_month').text();


          if (total_amount < 1) {
            swal({
              title: "Sorry!", 
              text: "<h5>Request Failed <br/> Can't generate control number with <br>Tzs. "+total_amount+" </h5>",
              type: "warning",
              html:true,
              showCancelButton: false,
              buttonsStyling: true,
              confirmButtonColor: "#67CD8B",
              cancelButtonColor: "#E24848",
              confirmButtonText: "OK",
              closeOnConfirm: true,
              closeOnCancel: false,
            },
            function(isConfirm) {
              if (isConfirm) {
                location.reload(); 
              }
            });

          } else {

            var frm = $('#frmTreasuryPay');
            swal({
              title: "Hi! {{access()->user()->firstname}}", 
              text: "<h5> This will generate control number for <br> Tzs. "+total_amount+" of "+current_month+"</h5>",
              type: "info",
              html:true,
              showCancelButton: true,
              buttonsStyling: false,
              confirmButtonColor: "#67CD8B",
              cancelButtonColor: "#E24848",
              confirmButtonText: "CONFIRM",
              cancelButtonText: "CANCEL",
              closeOnConfirm: false,
              closeOnCancel: true,
            },
            function(isConfirm) {
              if (isConfirm) { 
                $.ajax({ 
                  url : '{!! route("backend.finance.treasury.generate_bill") !!}',
                  type : 'POST',
                  data : frm.serialize(),
                  datatype : 'json',
                  success:function(data){
                    if (data.success) {
                     swal({
                      title: "Success!",
                      text: 'Sweet',
                      type: "success",
                      confirmButtonClass: 'btn btn-primary',
                      confirmButtonText: 'OK!'
                    },
                    function(){ location.reload(); });
                   }
                   else{
                    if (data.errors.message) {
                     swal({
                      title: "Sorry!", 
                      text: "<h5> Request Failed! <br>"+data.errors.message+"</h5>",
                      type: "error",
                      html:true,
                      showCancelButton: false,
                      buttonsStyling: false,
                      confirmButtonColor: "#67CD8B",
                      confirmButtonText: "OK",
                      closeOnConfirm: true,
                      closeOnCancel: true,
                    },
                    function(){ location.reload(); });
                   } else {
                    swal({
                      title: "Sorry!", 
                      text: "<h5> Request Failed! <br> Something went wrong <br> Please try again</h5>",
                      type: "error",
                      html:true,
                      showCancelButton: false,
                      buttonsStyling: false,
                      confirmButtonColor: "#67CD8B",
                      confirmButtonText: "OK",
                      closeOnConfirm: true,
                      closeOnCancel: true,
                    },
                    function(){ location.reload(); });
                  }
                }
              }

            });

              }

            });
          }


        });



         $("#btnRefreshData").click(function(e){
          e.preventDefault();

          swal({
            title: "Hi! {{access()->user()->firstname}}", 
            text: "<h5> This will Request "+$('.current_month').text()+" data once again from MoF. <br> The processing will start at midnight today {{date('d F, Y')}}</h5> ",
            type: "warning",
            html:true,
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonColor: "#DC9A45",
            cancelButtonColor: "#E24848",
            confirmButtonText: "OK - RESCHEDULE",
            cancelButtonText: "CANCEL",
            closeOnConfirm: false,
            closeOnCancel: true,
          },
          function(isConfirm) {
            if (isConfirm) { 
                   // swal('Success!','Sweet execution','success');
                   swal({
                    title: "Success!",
                    text: 'Sweet',
                    type: "success",
                    confirmButtonClass: 'btn btn-primary',
                    confirmButtonText: 'OK!'
                  });
                 }

               });
        });


         $("#BtnTreasuryfailed").click(function(e){
          e.preventDefault();
          var fail_month = $('#current_month').text();
          $.ajax({
            url : '{{ url("finance/treasury/failed_summary") }}'+'/'+fail_month,
            type : 'GET',
            datatype : 'json',
            success:function(data){
              $('#treasuryFailTitle').text(fail_month+' contribution');
                // console.log(data.total_employees);

                $('#mdlTreasuryFailData').modal('show');
                $('#total_employees').text(data.total_employees);
                $('#loaded_employees').text(data.loaded_employees); 
                $('#total_pages').text(data.total_pages);
                $('#loaded_pages').text(data.loaded_pages);
                $('#fail_reason').text(data.reason);
                $('#start_date').text(data.start_date);
                $('#finish_date').text(data.finish_date);
              }

            });
        });

         $(".clickable-row").click(function() {
          window.location = $(this).data("href");
        });

         $("#preview").click(function() {
         });
       });
     </script>;

     @endpush