@extends('layouts.backend.main', ['title' => 'ERMS Menu', 'header_title' => 'ERMS Menu'])


@section('content')
{{--<section id="content-wrapper">--}}
    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.erms.claims_payable') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;Claims Payable <label class="label label-success text-white">&nbsp;({{$claims_payable_count}}) &nbsp; </label></large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                               Post claims payable to ERMS
                            </p>
                        </li>
                    </a>

                </ul>
            </div>
            <br>

        </div>



        {{--right div--}}

        <div class="col-sm-6 col-md-6">

        </div>


    </div>


    &nbsp;

{{--</section>--}}

@stop


@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */
         $("#preview").click(function() {
         });
     });
 </script>;

 @endpush