
    {{--<section id="content-wrapper">--}}
        {{--<div style="color:#fff">--}}
            <div class="col-md-12" >
                @include('includes.partials.messages')
                <div class="pull-right">
                    <a href="{!! route('backend.finance.currency.create') !!}"   class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;Add Currency</a>
                </div> <br>
                <input type='hidden' name='method' value='delete'>
                <div>&nbsp; </div>
                <table class="display" cellspacing="0" width="100%" id ="currencies-table"  >
                    <thead>
                    <tr >
                        <th>Name</th>
                        <th>Code</th>
                        <th>Exchange rate</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                </table>

                </div>
            {{--</div>--}}


    {{--</section>--}}

@stop


@push('after-script-end')

{{--{{ Html::script(asset_url() . "/global/js/backend/backend.js") }}--}}
{{ Html::script(asset_url(). "/global/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/global/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url() . "/global/js/plugins/sweetalert/lib/sweet-alert.min.js") }}




<script  type="text/javascript">

    $(function() {
        $('#currencies-table').DataTable({

            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.currency.get') !!}',
                type : 'get'


            },
            columns: [
                { data: 'name' , name: 'name' },
                { data: 'code', name: 'code' },
                { data: 'exchange_rate', name: 'exchange_rate' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]


        });

    });



</script>;

@endpush
