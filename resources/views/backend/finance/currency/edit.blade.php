@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.currency.index'), 'header_title' => trans('labels.backend.finance.header.currency.index')])

@section('after-styles-end')

@endsection

@section('content')
    <section id="content-wrapper">
        {{--<div style="color-:#fff">--}}
        <div class="col-md-12" >
        &nbsp;<legend>Edit Currency</legend>
        </div>
        <div class="col-md-7" >
            @include('includes.partials.messages')

                     {!! Form::model($currency, ['route' => ['backend.finance.currency.update', $currency->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}

       &nbsp;
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right"><label>Name</label></div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">

                        {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right"><label>Code</label></div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'code', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('code', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        <div class="element-form">
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right"><label>Exchange Rate</label></div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
                <div class="form-group">
                    {!! Form::input('text', 'exchange_rate', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('exchange_rate', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
            {{--currency-ID hidden--}}
            {!! Form::input('hidden', 'id', null, ['class' => 'form-control']) !!}


{{--Buttons--}}
        <div class="element-form">
            <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
            <div class="col-xl-9 col-lg-9 col-sm-12 col-md-9 col-xs-12">
                <div class="pull-right">

                    <a  href="{!! route('backend.finance.currency.index') !!}"  class="btn btn-primary site-btn cancel_button">Cancel</a>
                    {!! Form::button('Save', ['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
        </div>
            {!! Form::close() !!}


        </div>


    </section>



@stop


@push('after-script-end')


<script  type="text/javascript">




</script>;

@endpush