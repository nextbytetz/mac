@extends('layouts.backend.main', ['title' => "Third Party Payer Profile", 'header_title' => "Third Party Payer Profile"])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush
@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">

        {{--Tabs navigation--}}
        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#tab-1" data-toggle="tab">General</a>
                        </li>
                    </ul>
                    <div class="nav_tab_contain tab-content">
                        <div id="tab-1" class="nav_tab_pane tab-pane active in">
                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >
                                            {{--edit--}}
                                            <span>
						                     <a href="{!! route('backend.finance.thirdparty.edit', $thirdparty->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>
						                    </span>
                                            @if ($thirdparty->isactive)
                                                <a href="{!! route('backend.finance.thirdparty.status_change', [$thirdparty->id, 0]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-level-down" aria-hidden="true"></i>&nbsp;Deactivate </a>
                                            @else
                                                <a href="{!! route('backend.finance.thirdparty.status_change', [$thirdparty->id, 1]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-level-up" aria-hidden="true"></i>&nbsp;Activate </a>
                                            @endif
                                            {{--<span>
                                            <a href="{!! route('backend.legal.lawyer.destroy', $lawyer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-trash"></i>&nbsp;@lang('buttons.general.crud.delete')</a>
                                            </span>--}}
                                            <span>
                                           {!! HTML::decode(link_to_route('backend.finance.thirdparty.destroy', "&nbsp;" . trans('buttons.general.crud.delete'), $thirdparty->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure you want to delete this third party payer", 'class' => 'icon fa fa-trash btn btn-primary site-btn nav_button'])) !!}
                                         </span>

                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{--main tab content--}}
                            <div class = "row">
                                {{--<div class="class container">--}}
                                <div class="col-md-8 col-sm-8">
                                    <legend class="grey_modal" >Receipts</legend>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <table class="display" cellspacing="0" width="100%" id ="thirdparty-receipt-table">
                                                <thead>
                                                <tr >
                                                    <th>Receipt No</th>
                                                    <th>Pay For</th>
                                                    <th>Amount</th>
                                                    <th>Payment Type</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <br/>
                                </div>
                                <div class="col-md-4 col-sm-4 ">
                                    {{--sidebar--}}
                                    @include('backend/finance/receipt/thirdparty/includes/summary')
                                </div>
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@stop



@push('after-script-end')

<script  type="text/javascript">

    $(function() {
        $('#thirdparty-receipt-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.thirdparty.get.receipts', $thirdparty->id) !!}',
                type : 'post'

            },
            columns: [
                { data: 'rctno', name: 'rctno'},
                { data: 'description', name: 'description'},
                { data: 'amount', name: 'amount'},
                { data: 'payment_type', name: 'payment_type', searchable: false, orderable:false}
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/finance/receipt/" + aData['id'] + "/edit";
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
    });
</script>
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush
