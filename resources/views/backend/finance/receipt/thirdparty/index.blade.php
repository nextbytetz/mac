@extends('layouts.backend.main', ['title' => "Third Party Payers", 'header_title' => "Third Party Payers"])

@include('backend.includes.datatable_assets')

@section('content')

    {{--<section id="content-wrapper">--}}
    {{--<div style="color:#fff">--}}
    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12 " >
                <div class="class pull-right">
                    <a href="{!! route('backend.finance.thirdparty.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;Add New</a>
                </div>
            </div>
            <div>&nbsp;</div>
            <table class="display" cellspacing="0" width="100%" id ="thirdparty-table"  >
                <thead>
                <tr >
                    <th>@lang('labels.general.name')</th>
                    <th>Receipt Counts</th>
                    {{--<th>@lang('labels.backend.table.case_mentions')</th>--}}
                    <th>@lang('labels.backend.table.external_id')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>



@stop


@push('after-script-end')

<script  type="text/javascript">

    $(function() {
        $('#thirdparty-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax : {
                url : '{!! route('backend.finance.thirdparty.get') !!}',
                type : 'post'
            },
            columns: [
                { data: 'name', name: 'name', searchable:false, orderable:false},
                { data: 'receipts', name: 'receipts', searchable:false, orderable:false},
                { data: 'external_id', name: 'external_id' }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/finance/thirdparty/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
    });







</script>;

@endpush