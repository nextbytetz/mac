<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="light_grey_bg">&nbsp;</div>
        <div class="light_grey_bg" style="padding: 5px;">
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.general.name') :</h6>
            <p style="font-weight: bold;">{!! $thirdparty->name_formatted !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.external_id') :</h6>
            <p style="font-weight: bold;">{!! $thirdparty->external_id_label !!}</p>
        </div>
    </div>
</div>