@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.receipt.retrieve.index'), 'header_title' => trans('labels.backend.finance.header.receipt.retrieve.index')])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >

            <input type='hidden' name='method' value='delete'>
            <div>&nbsp; </div>
            <table class="display" cellspacing="0" width="100%" id ="receipts-table">
                <thead>
                <tr >
                    <th>@lang('labels.backend.table.receipt.rctno')</th>
                    <th>@lang('labels.backend.table.receipt.payer')</th>
                    <th>@lang('labels.backend.table.receipt.payfor')</th>
                    <th>@lang('labels.backend.table.amount')</th>
                    <th>@lang('labels.backend.table.receipt.chequeno')</th>
                    <th>@lang('labels.backend.table.receipt.rct_date')</th>
                    <th>@lang('labels.backend.table.status')</th>
                </tr>
                </thead>

            </table>

        </div>
    </div>

@stop

@push('after-script-end')

<script  type="text/javascript">

    var url = "{!! url("/") !!}";
    $(function() {
        $('#receipts-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.receipt.get') !!}',
                type : 'get'


            },
            columns: [
                { data: 'rctno' , name: 'rctno' },
                { data: 'payer', name: 'payer' },
                { data: 'description', name: 'description' },
                { data: 'amount', name: 'amount' },
                { data: 'chequeno', name: 'chequeno' },
                { data: 'rct_date', name: 'rct_date' },
                { data: 'iscancelled', name: 'iscancelled'}
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/finance/receipt/" + aData['id'] + "/edit";
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });

</script>;

@endpush
