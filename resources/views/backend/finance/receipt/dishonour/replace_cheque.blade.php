@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.receipt.dishonour.replace'), 'header_title' => trans('labels.backend.finance.header.receipt.dishonour.replace')])

@@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@@endpush

@section('content')

    <div class="row">

        <div class="col-md-12" >


            {!! Form::model($receipt, ['route' => ['backend.finance.receipt.replace_cheque', $receipt->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}

           {{--Rctno--}}
            <div class="row">
                <div class="col-md-10" >
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.receipt_no')</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','rctno', null, ['class' => 'form-control', 'disabled' => true]) !!}

                            </div>
                        </div>
                    </div>
{{--Cheque no--}}
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cheque_no')</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','chequeno', null, ['class' => 'form-control', 'disabled' =>
                                true]) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-10" class="form-inline" >


                    {{--bank name--}}
                    <div class="element-form"  >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.bank'):</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::select('bank', $banks, [], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'bank_select']) !!}
                                <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
                                {!! $errors->first('bank', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>


                    {{--New Cheque No--}}
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.new_cheque_no')</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','new_chequeno', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('new_chequeno', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div>&nbsp;</div>

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-7" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-9 col-lg-9 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">
                                <a  href="{!! route('backend.finance.receipt.edit', $receipt->id) !!}"  class="btn btn-primary site-btn cancel_button">@lang('buttons.general.cancel')</a>

                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}


        </div>
    </div>


    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function () {
        $(".search-select").select2();
    });
</script>

@endpush
