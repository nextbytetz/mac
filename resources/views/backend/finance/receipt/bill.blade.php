
@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.receipt.for_employer'), 'header_title' => 'Control Number '])


@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12">
		<h5 class="client-title">
			<i class="icon-circle"></i>
			<!-- Employer header detail -->
			<span><span class="tag tag-success success_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="Approved">.</span> </span>

			<strong> <label for="name" id="name">{{ucwords($employer->name)}}</label> </strong>

			<small>
				Reg #: <label for="reg_no" id="reg_no">{{substr($employer->reg_no, -6)}}</label>
			</small>
		</h5>
		<legend></legend>
	</div>
</div>

<div class="row"> 
	<div class="col-md-12"> 
		<div class="row">

			@if(count($pending_bill)>0)
			<div class="col-sm-6 col-md-12"> 
				{{-- @if(!empty($pending_bill->payments[0])) --}}

				<h6>{{$employer->name}} has a pending Bill of Tzs. {{number_format($pending_bill->bill_amount, 2)}}</h6> 

				<br>
				@if(!empty($pending_bill->payments[0]))
				<p>
					Direct Deposit Form : <a href="{{url('/finance/normal_bill/'.$pending_bill->id)}}" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download Direct Deposit</a>
				</p>

				<p>
					NMB Transfer Form : <a href="{{url('/finance/nmb_transfer/'.$pending_bill->id)}}" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download NMB Transfer Form</a>
				</p>


				<p>
					CRDB transfer Form : <a href="{{url('/finance/crdb_transfer/'.$pending_bill->id)}}" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download CRDB transfer Form</a>
				</p>
				@endif
				@else
				<br>
				<div class="col-sm-6"> 
					<table class="table table-striped table-bordered">
						<tbody class="text-primary">
							<tr>
								<th>Last Contribution</th>
								<td><b>{{$contribution['last_contrib_mnth']}}</b></td>
							</tr>

							<tr>
								<th> Number of Employees</th>
								<td><b>{{$contribution['employees_count']}}</b></td>

							</tr>


						</tbody>
					</table>


					<br><br><br>
					{{-- <form  accept-charset="UTF-8"  action="{{url('/finance/generate_bill')}}" method="POST" id="frmPay"> --}}
					</div>
				</div>	
				<div class="row">
					<div class="col-md-12">

						<div class="element-form">
							<div class="col-md-3 col-sm-12"><label>Contribution Amount:</label></div>
							<div class="col-md-3 col-sm-3">
								<form  accept-charset="UTF-8"  id="frmPay">
									{{ csrf_field() }}
									<div class="form-group">
										<input class="form-control" name="employer_id" type="text" value="{{$employer->id}}" hidden="hidden">

										<input class="form-control" name="contribution" type="text" id="contribution" value="{{$contribution['amount']}}" disabled="disabled">
										{{-- <p class="error_firstname text-danger hidden"></p> --}}
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="element-form">
								<div class="col-md-3 col-sm-12"><p>&nbsp;</p></div>
								<div class="col-md-3 col-sm-3">
									<div class="pull-right">
										<button class="btn btn-primary site-btn save_button" id="btnSbmt" type="submit">Generate Control Number</button>
									</div>
								</div>
							</div>
						</div>
					</div>


				</form>
				@endif
			</div>
		</div>

		@endsection

		@push('after-script-end')
		<!-- Custom javascript files for this page -->
		{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
		{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


		<script type="text/javascript">

			$('document').ready(function(){


				$("#btnSbmt").click(function(e){
					e.preventDefault();
					var frm = $('#frmPay');
					$.fn.serializeIncludeDisabled = function () {
						let disabled = this.find(":input:disabled").removeAttr("disabled");
						let serialized = this.serialize();
						disabled.attr("disabled", "disabled");
						return serialized;
					};

					swal({
						html: true,
						title: "Are you sure want to Generate Control number for <br> TZS. "+$('#contribution').val(),
						type: "info",
						showCancelButton: true,
						confirmButtonClass: "btn-info",
						confirmButtonText: "Yes",
						cancelButtonText: "No",
						closeOnConfirm: true,
						closeOnCancel: true,
					}, 
					function(isConfirm) {
						if (isConfirm) {
							$.ajax({
								url : '{{route('backend.finance.generatebill')}}',
								type : 'POST',
								datatype : 'json',
								data : frm.serializeIncludeDisabled(),
								success:function(data){
									if (!data.error) {
										swal({
											title: "Success!",
											text: 'Your request has been sent! You will receive sms shortly',
											type: "success",
											confirmButtonClass: 'btn btn-primary',
											confirmButtonText: 'OK!'
										},
										function(){ location.reload(); });
									}
									else{
										swal({
											title: "Error Perfoming Request",
											text: data.message,
											type: "error",
											showCancelButton: false,
											buttonsStyling: false,
											confirmButtonClass: "btn btn-danger",
											confirmButtonText: 'OK!'
										},
										function(){ location.reload(); });
									}
								}

							});

						}
					});
				});

			});

		</script>





		<script>
			(function (window, document, $) {

			})(window, document, jQuery);
		</script>

		@endpush