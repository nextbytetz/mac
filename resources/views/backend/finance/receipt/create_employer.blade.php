@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.receipt.for_employer'), 'header_title' => trans('labels.backend.finance.receipt.create_employer')])

{{--@include('backend.includes.toastr_assets')--}}

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/employer.css") }}
<style>
    .payment_mode:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.finance.receipt.mode')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .payment_mode {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

     .online_user:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Online User/s of this employer";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .online_user {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .receipt_details:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.finance.receipt.details')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .receipt_details {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .receivables:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.finance.receipt.receivable')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .receivables {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .contribution_entry:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: rgba(1, 196, 4, 0.99);
        content: "@lang('labels.backend.finance.receipt.month_contribution')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .contribution_entry {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .interest_entry:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: rgba(1, 196, 4, 0.99);
        content: "@lang('labels.backend.finance.receipt.month_interest')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .interest_entry {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                @lang('labels.backend.table.employer.reg_no') &nbsp;:&nbsp;<strong>  {!! $employer->reg_no !!}</strong> &nbsp;&nbsp;&nbsp;
                <small >
                    @lang('labels.backend.finance.receipt.employer'): <label class="underline">{!! $employer->name !!}</label>
                    ,
                </small>
            </h5>
            <legend>
            </legend>
        </div>
    </div>
    <br/>

    {!! Form::open(['route' => 'backend.finance.receipt.store', 'name' => 'employer_receipt']) !!}
    {!! Form::hidden('employer_id', $employer->id) !!}

    <div class="row">
        <div class="col-md-12">
            <div class="receipt_details">
                <div class="row">
                    <div class="col-md-6">
                        <div class="field-layout">
                            <div class="form-group">
                                <label class="required" for="receipt_amount">@lang('labels.backend.table.amount')</label>
                                <div class="input-group">
                                    {!! Form::text('amount', null, ['class' => 'form-control money', 'style' => "border-radius:3px;", 'id' => 'receipt_amount']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span>{{ trans('labels.backend.finance.receipt.bank_amount') }}</span>
                                </span>
                            </div>
                        </div>
                        <div class="filed-layout">
                            <label class="required">@lang('labels.backend.finance.receipt.receipt_date')</label>
                            <div class="form-group">
                                <div class="form-inline">
                                    <span>
                                        {!!  Form::selectRange('receipt_date',1 , 31, null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                                    <span>
                                        {!!  Form::selectMonth('receipt_month',null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                                    </span>
                                    <span>
                                        {!!  Form::selectRange('receipt_year', Carbon\Carbon::now()->format('Y'), 2015 , null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                                    </span>
                                    <span>
                                        {!! Form::hidden('rct_date') !!}
                                        {!! Form::hidden('today_date', $this_date) !!}
                                        {!! Form::hidden('start_date', "2015-6-1") !!}
                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.table.bank')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('bank_id', $banks, null, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '', 'id' => 'bank_select']) !!}
                                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">Bank Account</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('fin_code_id', [], null, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '', 'id' => 'fin_code_id']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.finance.currency.index')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('currency_id', $currencies, null, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>@lang('labels.backend.finance.receipt.reference')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('bank_reference', null, ['class' => 'form-control', 'style' => "border-radius:3px;"]) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-exchange" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span>{{ trans('labels.backend.finance.receipt.reference_helper') }}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="payment_mode">
                               
                                    @foreach ($payment_types as $payment_type)
                                        <div class="radio-button">
                                            {!! Form::radio('payment_type_id', $payment_type->id, false, ['id' => "radio-button" . $payment_type->id]) !!}
                                            <label for="radio-button{!! $payment_type->id !!}"></label>
                                            <span>{!! $payment_type->name !!}</span>
                                        </div>
                                    @endforeach
                            
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="fileld-layout" id="chequeno_entry" style="display:none;">
                            <label class="required">@lang('labels.backend.finance.receipt.cheque')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('chequeno', null, ['class' => 'form-control', 'style' => "border-radius:3px;"]) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout" style="display:none;">
                            <label>@lang('labels.backend.finance.receipt.payment_description')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {{--{!! Form::textarea('description', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}--}}
                                    {{ Form::hidden("description")  }}
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($isonline)
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="online_user">
                                <div class="radio-vertical">
                                  
                              {!! Form::select('payroll_id', $onlineusers, null, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                      
                    </div>
                       @endif
                </div>
            </div>
        </div>


    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="receivables">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fileld-layout">
                            <div class="form-group">
                                <div class="form-inline">
                                        <span>
                                            <label class="required">@lang('labels.backend.finance.receipt.pay_months')</label>&nbsp;&nbsp;
                                        </span>
                                    <span>
                                        {!! Form::text('pay_months', null, ['class' => 'form-control number', 'style' => "border-radius:3px;width:5%;", 'id' => 'pay_months']) !!}&nbsp;&nbsp;
                                    </span>
                                    <span>
                                            <a href="#" id="add_contribution_entry"><i class="icon fa fa-2x fa-plus" aria-hidden="true" style="color:darkblue"></i></a>
                                        </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div id="contribution_entries">
                            {{--@include('backend/finance/receipt/includes/monthly_contribution_entry')--}}
                        </div>
                        <br/>
                        {{--<div id="contribution_summary">--}}
                        <div class="fileld-layout">
                            <div class="form-group">
                                <div class="form-inline">
                                        <span>
                                            @lang('labels.backend.finance.receipt.total_entry')&nbsp;:&nbsp;&nbsp;
                                        </span>
                                    <span id = "total_entry" class="underline" style="font-weight: bold;">
                                            0
                                        </span>
                                    <span>
                                            ,&nbsp;@lang('labels.backend.finance.receipt.bank_amount') &nbsp;:&nbsp;&nbsp;
                                        </span>
                                    <span id="bank_amount" class="underline" style="font-weight: bold;">
                                            0
                                        </span>
                                    <span id="contribution_summary_icon">

                                        </span>
                                </div>
                            </div>
                        </div>
                        {{--</div>--}}
                        <hr/>
                        <div class="pull-right">
                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.create')" />
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@push('after-script-end')

<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}

<script>

    var selected_entries;
    var total_entries = 0;
    var max_entries = parseInt("{!! $max_entries !!}");
    var employer_id = parseInt("{!! $employer->id !!}");
    var month_threshold = parseInt("{!! $month_threshold !!}");
    var year_threshold = parseInt("{!! $year_threshold !!}");
    var $minimum_contribution = parseInt("{!! $minimum_contribution !!}");
    var receipt_amount;
    var entry_amount = 0;
    $(function() {
        $(".search-select").select2({});
        autosize($("textarea.autosize"));
        var pay_monthes_textbox = $("#pay_months");
        $('#bank_select').on('change', function (e) {
            $("#spin2").show();
            var $bank_id = e.target.value;
            $.get("{{ url('/') }}/getFinCodes?id=" + $bank_id, function (data) {
                $('#fin_code_id').empty();
                $("#fin_code_id").select2("val", "");
                $('#fin_code_id').html(data);
                $("#spin2").hide();
            });
        });
        /* start : on click contribution entry add link */
        $('body').on('click', 'a#add_contribution_entry', function (e) {
            e.preventDefault();
            selected_entries = pay_monthes_textbox.val();
            if (validate_entry(selected_entries, pay_monthes_textbox)) {
                if (!add_contribution_entry_control(parseInt(selected_entries))) {
                    amaran_notify("{!! trans("exceptions.backend.finance.receipts.max_entries", ['total' => $max_entries]) !!}", "{!! trans("labels.general.warning") !!}", "warning");
                    return false;
                }
                /* start : Retrieve Contribution entries from the server  */
                $.post("{!! route('backend.finance.receipt.contribution_entries') !!}", {'selected_entries' : selected_entries}, function( data ) {
                    $( data ).prependTo( "#contribution_entries" );
                }, "html").done(function () {
                    $(".search-select").select2({});
                    /* start : check selection change event of the drop down months and year  */
                    $('body').off('change', '.contribution_month').on('change', '.contribution_month', function () {
                        var name = $(this).attr('name');
                        var month = $(this).val();
                        var special = name.split("contribution_month").join("");
                        var year = $("#contribution_year" + special).val();
                        if (year == "" || month == "") {
                            return false;
                        }
                        let contrib_month = year + "-" + month + "-28";
                        $.post("{!! route('backend.finance.receipt_code.check_existing') !!}", {'employer_id' : employer_id, 'contrib_month' : contrib_month}, function( data ) {
                            if (data.return) {
                                $(".duplicate_month_check").show();
                            } else {
                                $(".duplicate_month_check").hide();
                            }
                        }, "json");
                        $(".contribution_month").not(this).each(function() {
                            var other_name = $(this).attr('name');
                            var other_month = $(this).val();
                            var other_special = other_name.split("contribution_month").join("");
                            var other_year = $("#contribution_year" + other_special).val();
                            if ((year == other_year) && (month == other_month)) {
                                /*$("#contribution_month" + special).select2("trigger", "select", {
                                    data: { id: "" }
                                });
                                $("#contribution_year" + special).select2("trigger", "select", {
                                    data: { id: "" }
                                });*/
                                $("#contribution_month" + special).val(null).trigger('change.select2');
                                $("#contribution_year" + special).val(null).trigger('change.select2');
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.duplicate_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                $('#contribution_select' + special).attr('checked', false);
                                $('#contribution_select' + special).trigger('change');
                                return false;
                            }
                        });
                        if ((month > month_threshold) && (year >= year_threshold)) {
                            /*$("#contribution_month" + special).select2("trigger", "select", {
                                data: { id: null }
                            });
                            $("#contribution_year" + special).select2("trigger", "select", {
                                data: { id: null }
                            });*/
                            $("#contribution_month" + special).val(null).trigger('change.select2');
                            $("#contribution_year" + special).val(null).trigger('change.select2');
                            amaran_notify("{!! trans("exceptions.backend.finance.receipts.date_threshold", ['max' => sysdefs()->data()->total_allowed_contrib_months ]) !!}", "{!! trans("labels.general.warning") !!}", "warning");
                            $('#contribution_select' + special).attr('checked', false);
                            $('#contribution_select' + special).trigger('change');
                            return false;
                        }
                        booked_amount(month, year, $("#booked_amount" + special));
                        interest_checkbox_state(special);
                    });
                    $('body').off('change', '.contribution_year').on('change', '.contribution_year', function () {
                        var name = $(this).attr('name');
                        var year = $(this).val();
                        var special = name.split("contribution_year").join("");
                        var month = $("#contribution_month" + special).val();
                        if (year == "" || month == "") {
                            return false;
                        }
                        let contrib_month = year + "-" + month + "-28";
                        $.post("{!! route('backend.finance.receipt_code.check_existing') !!}", {'employer_id' : employer_id, 'contrib_month' : contrib_month}, function( data ) {
                            if (data.return) {
                                $(".duplicate_month_check").show();
                            } else {
                                $(".duplicate_month_check").hide();
                            }
                        }, "json");
                        $(".contribution_year").not(this).each(function() {
                            var other_name = $(this).attr('name');
                            var other_year = $(this).val();
                            var other_special = other_name.split("contribution_year").join("");
                            var other_month = $("#contribution_month" + other_special).val();
                            if ((year == other_year) && (month == other_month)) {
                                /*$("#contribution_month" + special).select2("trigger", "select", {
                                    data: { id: null }
                                });
                                $("#contribution_year" + special).select2("trigger", "select", {
                                    data: { id: null }
                                });*/
                                $("#contribution_month" + special).val(null).trigger('change.select2');
                                $("#contribution_year" + special).val(null).trigger('change.select2');
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.duplicate_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                $('#contribution_select' + special).attr('checked', false);
                                $('#contribution_select' + special).trigger('change');
                                return false;
                            }
                        });
                        if ((month > month_threshold) && (year >= year_threshold)) {
                            /*$("#contribution_month" + special).select2("trigger", "select", {
                                data: { id: null }
                            });
                            $("#contribution_year" + special).select2("trigger", "select", {
                                data: { id: null }
                            });*/
                            $("#contribution_month" + special).val(null).trigger('change.select2');
                            $("#contribution_year" + special).val(null).trigger('change.select2');
                            amaran_notify("{!! trans("exceptions.backend.finance.receipts.date_threshold", ['max' => sysdefs()->data()->total_allowed_contrib_months ]) !!}", "{!! trans("labels.general.warning") !!}", "warning");
                            $('#contribution_select' + special).attr('checked', false);
                            $('#contribution_select' + special).trigger('change');
                            return false;
                        }
                        booked_amount(month, year, $("#booked_amount" + special));
                        interest_checkbox_state(special);
                    });
                    /* end : check selection change event of the drop down months and year  */
                    /* start: contribution and interest checkbox control */
                    $('body').off('change', '.contribution_select').on('change', '.contribution_select', function(e) {
                        var name = $(this).attr('name');
                        var special = name.split("contribution_select").join("");
                        if(this.checked) {
                            var $year = $("#contribution_year" + special).val();
                            var $month = $("#contribution_month" + special).val();
                            var $checkbox = $(this);
                            if (!$year || !$month) {
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.select_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                prevent_check(e, $checkbox);
                            } else {
                                $("#contribution_entry" + special).show();
                            }
                        } else {
                            /* Checkbox is unchecked */
                            $("#contribution_entry" + special).hide();
                            var contribution_amount = $("#contribution_amount" + special).val();
                            if (contribution_amount != '') {
                                entry_amount -= parseFloat(contribution_amount.split(",").join(""));
                                $("#contribution_amount" + special).val("");
                                $('#total_entry').text(addCommas(entry_amount));
                                check_amount_match(receipt_amount, entry_amount);
                            }
                        }
                    });
                    $('body').off('change', '.interest_select').on('change', '.interest_select', function(e) {
                        var name = $(this).attr('name');
                        var special = name.split("interest_select").join("");
                        if(this.checked) {
                            var $year = $("#contribution_year" + special).val();
                            var $month = $("#contribution_month" + special).val();
                            var $checkbox = $(this);
                            var $amount = 0;
                            if (!$year || !$month) {
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.select_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                prevent_check(e, $checkbox);
                            } else {
                                interest_due_amount($month, $year).done(function (data) {
                                    $amount = data.amount;
                                    /* console.log(data.amount); */
                                    /*if ($amount == '0.00') {
                                        prevent_check(e, $checkbox);
                                        $("#no_interest" + special).show();
                                        $("#interest_entry" + special).hide();
                                    } else {
                                        $("#interest_due" + special).val($amount);
                                        $("#interest_entry" + special).show();
                                        $("#no_interest" + special).hide();
                                    }*/
                                    $("#interest_due" + special).val($amount);
                                    $("#interest_entry" + special).show();
                                    $("#no_interest" + special).hide();
                                });
                            }
                        } else {
                            /* Checkbox is unchecked */
                            $("#interest_entry" + special).hide();
                            var interest_amount = $("#interest_amount" + special).val();
                            if (interest_amount != '') {
                                entry_amount -= parseFloat(interest_amount.split(",").join(""));
                                $("#interest_amount" + special).val("");
                                $('#total_entry').text(addCommas(entry_amount));
                                check_amount_match(receipt_amount, entry_amount);
                            }
                        }
                    });
                    /* end: contribution and interest checkbox control */
                    /* start : mask all money input */
                    $('.money').maskMoney({
                        precision : 2,
                        affixesStay : false
                    });
                    /* end : mask all money input */
                    /* start : ensure only numbers are input on monetary boxes */
                    $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                        number_only(e)
                    });
                    /* end : ensure only numbers are input on monetary boxes */
                    /* start : handle amount textboxes change event and sum all the entered amounts */
                    $('body').off('keyup', '.contribution_amount').on('keyup', '.contribution_amount', function(e) {
                        var value;
                        var sum = 0;
                        $('.contribution_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        $('.interest_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        entry_amount = sum;
                        $('#total_entry').text(addCommas(entry_amount));
                        check_amount_match(receipt_amount, entry_amount);
                    });
                    $('body').off('keyup', '.interest_amount').on('keyup', '.interest_amount', function(e) {
                        var value;
                        var sum = 0;
                        $('.interest_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        $('.contribution_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        entry_amount = sum;
                        $('#total_entry').text(addCommas(entry_amount));
                        check_amount_match(receipt_amount, entry_amount);
                    });
                    /* end : handle amount textboxes leave event and sum all the entered amounts */
                }).fail(function () {

                });
                /* end : Retrieve Contribution entries from the server  */
            }
        });
        /* end : on click contribution entry add link */
        /* start: on click contribution entry remove link */
        $('body').on('click', 'a.remove_contribution_entry', function (e) {
            e.preventDefault();
            var id = this.id;
            var amount = 0;
            total_entries -= 1;
            if ($('#contribution_select' + id).is(':checked')) {
                var contribution_amount = $("#contribution_amount" + id).val();
                if (contribution_amount != '') {
                    amount += parseFloat(contribution_amount.split(",").join(""));
                }
            }
            if ($('#interest_select' + id).is(':checked')) {
                var interest_amount = $("#interest_amount" + id).val();
                if (interest_amount != '') {
                    amount += parseFloat(interest_amount.split(",").join(""));
                }
            }

            entry_amount -= amount;
            $('#total_entry').text(addCommas(entry_amount));
            check_amount_match(receipt_amount, entry_amount);
            $("#contribution_entry_group" + id).remove();
        });
        /* end: on click contribution entry remove link */
        /* start : ensure only numbers are input on monetary boxes */
        $(".number").keydown(function (e) {
            number_only(e)
        });
        /* end : ensure only numbers are input on monetary boxes */
        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });
        /* end : mask all money input */
        /* start : handle bank amount textboxes change event and sum all the entered amounts */
        $('body').off('keyup', '#receipt_amount').on('keyup', '#receipt_amount', function(e) {
            var value = parseFloat($(this).val().split(",").join(""))
            $('#bank_amount').text(addCommas(value));
            receipt_amount = value;
            check_amount_match(receipt_amount, entry_amount);
        });
        /* end : handle bank amount textboxes leave event and sum all the entered amounts */
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form[name=employer_receipt]', function (e) {
            e.preventDefault();
            var form = this;
            var $receipt_date = $("select[name=receipt_date]").val();
            var $receipt_month = $("select[name=receipt_month]").val();
            var $receipt_year = $("select[name=receipt_year]").val();
            if ($receipt_date && $receipt_month && $receipt_year) {
                $("input[name=rct_date]").val($receipt_year + '-' + $receipt_month + '-' + $receipt_date);
            } else {
                $("input[name=rct_date]").val("");
            }
            var $data = $(form).serialize();
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            $.ajax({
                data : $data,
                dataType : "json",
                method : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    if (receipt_amount < $minimum_contribution) {
                        amaran_notify("{!! trans("exceptions.backend.finance.receipts.small_amount", ['amount' => $minimum_contribution]) !!}", "{!! trans("labels.general.error") !!}", "error");
                        return false;
                    }
                    if (!allow_match(receipt_amount, entry_amount)) {
                        amaran_notify("{!! trans("exceptions.backend.finance.receipts.not_tallied") !!}", "{!! trans("labels.general.error") !!}", "error");
                        return false;
                    }
                    $(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    if (data.success) {
                        /* window.location = base_url + "/finance/receipt/" + data.id + "/edit"; */
                        document.location.replace(base_url + "/finance/receipt/" + data.id + "/edit");
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    /*console.log(errors);*/
                    $.each(errors, function(index, value) {
                        $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });
                },
            }).done(function() {

            }).fail(function() {
                $(".btn-submit").prop('disabled', false);
            }).always(function() {
                $(".btn-submit").prop('disabled', false);
            });
        });
        /* end: Submitting Form and perfom validation on the server side */
        /* start : Payment type change event */
        $('input[type=radio][name=payment_type_id]').change(function() {
            if (this.value == 2 || this.value == 5) {
                $("#chequeno_entry").show();
            } else {
                $("#chequeno_entry").hide();
            }
        });
        /* end : Payment type change event */
    });
    /* start : check if the receipt amount and the total contribution entries matches */
    function check_amount_match($receipt_amount, $entry_amount) {
        var $diff = $receipt_amount - $entry_amount;
        switch(true) {
            case ($diff < -0.1):
                $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:red;'><i class='icon fa fa-2x fa-close' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.less")</span>");
                break;
            case ($diff > 0.1):
                $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:#ffd303;'><i class='icon fa fa-2x fa-warning' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.greater")</span>");
                break;
            case ($diff >= -0.1 && $diff <= 0.1):
                $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:green;'><i class='icon fa fa-2x fa-check-square-o' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.matched")</span>");
                break;
        }
    }
    /* end : check if the receipt amount and the total contribution entries matches */
    /* start : start allow amount match */
    function allow_match($receipt_amount, $entry_amount) {
        var $diff = $receipt_amount - $entry_amount;
        var $return = false;
        switch(true) {
            case ($diff < -0.1):
                $return = false;
                break;
            case ($diff > 0.1):
                $return = false;
                break;
            case ($diff >= -0.1 && $diff <= 0.1):
                $return = true;
                break;
        }
        return $return;
    }
    /* end : start allow amount match */
    /* start : ensure only numbers are input on monetary boxes */
    function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    /* end : ensure only numbers are input on monetary boxes */
    /* start : check the interest select checkbox checked state and alter the values respectively */
    function interest_checkbox_state(special) {
        if ($('#interest_select' + special).is(':checked')) {
            $('#interest_select' + special).prop('checked', true).trigger('change');
        }
    }
    /* end : check the interest select checkbox checked state and alter the values respectively */
    /* start: Get booked amount for a particular month and year */
    function booked_amount(month, year, target) {
        $.post( "{!! route('backend.finance.receipt.booked_amount') !!}", {'month' : month, 'year' : year, 'employer_id' : employer_id}, function( data ) {
            $(target).val(data.amount);
            /* console.log(data); */
        }, "json").done(function () {

        }).fail(function () {

        });
    }
    /* end: Get booked amount for a particular month and year */
    /* start: Get the interest charged for a particular month and a year */
    function interest_due_amount(month, year) {
        return $.post( "{!! route('backend.finance.receipt.interest_due_amount') !!}", {'month' : month, 'year' : year, 'employer_id' : employer_id}, function( data ) {
            /* callback(data.amount); */
            /* console.log(data); */
        }, "json");
    }
    /* end: Get the interest charged for a particular month and a year */
    /* start: Prevent the checkbox from being checked when the conditions don't allow */
    function prevent_check($e, $target) {
        setTimeout(function() {
            $target.removeAttr('checked');
        }, 0);
        $e.preventDefault();
        $e.stopPropagation();
    }
    /* end: Prevent the checkbox from being checked when the conditions don't allow */
    function validate_entry(selected_entries, control) {
        if (selected_entries === '') {
            control.addClass('form-error');
            setTimeout(
                function() { control.removeClass('form-error'); },
                2000
            );
            return false;
        } else {
            return true;
        }
    }
    function add_contribution_entry_control(selected_entries) {
        return true;
        /*
        if (max_entries >= (total_entries + selected_entries)) {
            total_entries = total_entries + selected_entries;
            return true;
        } else {
            return false;
        }
        */
    }
    function amaran_notify(message, title, type) {
        var icon;
        if(typeof type === "undefined") {
            type = 'success';
        }
        switch(type) {
            case "success":
                icon = "fa fa-check-square-o";
                break;
            case "warning":
                icon = "icon fa fa-warning";
                break;
            case "error":
                icon = "icon fa fa-ban";
                break;
            default:
                icon = "fa fa-check-square-o";
        }
        $.amaran({
            'theme'     :'awesome ' + type,
            'content'   :{
                title:title,
                message:message,
                info:'',
                icon:icon,
            },
            'position'  :'bottom left',
            'outEffect' :'slideBottom',
            'inEffect'  :'slideLeft'
        });
    }
    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>
@endpush