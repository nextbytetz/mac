@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.receipt.retrieve.index'), 'header_title' => trans('labels.backend.finance.header.receipt.retrieve.index')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')


    <div class = "row">
        {!! Form::model($receipt, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
        <div class="col-md-9 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Receipt Header detail -->
                <span>{!! $receipt->complete_status_label !!} </span>
                <span>{!! $receipt->isverified_label !!}</span>
                <strong> {!! Form::label( 'name', $receipt->rctno, [ 'id'=> 'rctno']) !!}</strong>
                <small >
                    Payer: {!! Form::label( 'payer', $receipt->payer, [ 'id'=> 'payer']) !!}
                </small>

            </h5>
        </div>
    </div>

    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#general" data-toggle="tab">@lang('labels.backend.finance.receipt.general')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id ="tab_2_header" href="#interests" data-toggle="tab">@lang('labels.backend.finance.receipt.interest')</a>
                    </li>
                </ul>
                <div class="nav_tab_contain tab-content">
                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            {{--Header Bar--}}
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        {{--reconcile receipt--}}
                                        {{--@if (!$receipt->hasreconciled)--}}
                                        @if (true)
                                            <span>
                                                <a href="{!! route('backend.finance.receipt.reconcile', $receipt->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-recycle"></i>&nbsp;Reconcile</a>
                                            </span>
                                        @endif
                                        {{--recall receipt--}}
                                        <span>
                                            <a href="{!! route('backend.finance.receipt.edit', $receipt->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-reply-all"></i>&nbsp;@lang('labels.backend.finance_menu.recall_receipt')</a>
                                        </span>
                                        {{--Edit Contribution--}}
                                    {{--     @permission("edit_contribution_months")
                                            <span>
                                                <a href="{!! route('backend.finance.receipt.edit_months_permission', $receipt->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit" aria-hidden="true"></i>&nbsp;@lang('buttons.general.crud.edit')
                                                </a>
                                            </span>
                                        @else
                                            <span>
                                                <a href="{!! route('backend.finance.receipt.edit_months', $receipt->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit" aria-hidden="true"></i>&nbsp;@lang('buttons.general.crud.edit')
                                                </a>
                                            </span>
                                        @endauth --}}

                                        {{--close--}}
                                        <span>
						        <a href="{!! route('backend.compliance.receipt.search') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close" ></i>&nbsp;@lang('buttons.general.close')</a>
						</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--MAIN tab contents--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--contribution--}}

                                    @include('backend.finance.receipt.includes.contribution')

                                    <div>&nbsp;</div>

                                    {{--Bottom tab navigation--}}

                                    {{--<div class = "row">
                                        <div class="col-md-12">
                                            <div class="basic_nav_pills nav_basic_tab">
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.legend.workflow')</a>
                                                    </li>
                                                </ul>
                                                <div class="nav_tab_contain tab-content">
                                                    <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                                                        --}}{{--main workflow tracks--}}{{--
                                                        @include('backend.includes.workflow_table_header')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}

                                </div>
                                <div class="col-md-3">
                                    {{--sidebar receipt Summary--}}
                                    @include('backend.finance.receipt.includes.sidebar_summary')
                                </div>
                            </div>
                        </div>


                    </div>
                    <div id="interests" class="nav_tab_pane tab-pane">
                        <div class="nav_tab_pane_header">
                            {{--Header Bar--}}
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        {{--Interest Adjustment--}}
                                        @if($receipt->interestAdjustment()->count() > 0)
                                        <span>
						        <a href="{!! route('backend.compliance.interest_adjustment.profile', $receipt->interestAdjustment->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-adjust"></i>&nbsp;View Interest Adjustment</a>
						</span>
                                            @else

                                            <span>
						        <a href="{!! route('backend.compliance.interest_adjustment.create', $receipt->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-adjust"></i>&nbsp;Adjust Interest</a>
						</span>
                                        @endif
                                        {{--close--}}
                                        <span>
						        <a href="{!! route('backend.compliance.receipt.search') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
						</span>


                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--Interests Paid on this receipt--}}

                        <div class="row">
                            <div class="col-md-12">

                                <div class="col-md-9">

                                    @include('backend.finance.receipt.includes.interest')
                                    <div>&nbsp;</div>

                                    {{--Bottom tab navigation--}}

                                    {{--<div class = "row">
                                        <div class="col-md-12">
                                            <div class="basic_nav_pills nav_basic_tab">
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.legend.workflow')</a>
                                                    </li>
                                                </ul>
                                                <div class="nav_tab_contain tab-content">
                                                    <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                                                        --}}{{--main workflow tracks--}}{{--
                                                        --}}{{--@include('backend.includes.workflow_table_header')--}}{{--
                                                        {!! $workflowtrack->with('resource_id', $receipt->id)->render('backend.includes.workflow_track') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>--}}

                                </div>
                                <div class="col-md-3">
                                    {{--sidebar receipt Summary--}}
                                    @include('backend.finance.receipt.includes.sidebar_summary')
                                </div>

                            </div>
                        </div>

                    </div>
                    <div id="tab-3" class="nav_tab_pane tab-pane">

                    </div>
                    <div id="tab-4" class="nav_tab_pane tab-pane">

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class = "row">
        <div class="col-md-12">
            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.legend.workflow')</a>
                    </li>
                </ul>
                <div class="nav_tab_contain tab-content">
                    <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                        {{--main workflow tracks--}}
                        <br/>
                        @php
                            $wfModule = $receipt->wfModule;
                            $workflowinput = ['resource_id' => $receipt->id, 'wf_module_group_id'=> 5, 'type' => $wfModule->type];
                        @endphp
                        {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}


@stop

@push('after-script-end')

{{ Html::script(asset_url(). "/nextbyte/js/backend/contribution-progress.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}


@stack('interest-script-end')
@stack('contribution-script-end')

<script>
    $(function () {

        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
                //var id = this.id;
                //alert(id);
            } else {
                location.hash = '#' + tab;
            }
        });
    });
</script>

{{--WORKFLOW DATATABLE SCRIPT--}}
<script  type="text/javascript">

    $(function() {

    });

</script>

@endpush
