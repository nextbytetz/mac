@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.receipt.retrieve.index'), 'header_title' => trans('labels.backend.finance.receipt.retrieve_receipt')])

@section('after-styles-end')

@endsection

@section('content')

    <div class="row">

        <div class="col-md-12" >


            {!! Form::open(['route' => ['backend.finance.receipt.profile_by_request'], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'get']) !!}

            {{--Receipt No--}}
            <div class="row">
                <div class="col-md-9" >

                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.enter_receipt_no')</label></div>
                        <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','rctno', null, ['class' => 'form-control' , 'autofocus'=>true]) !!}
                                {!! $errors->first('rctno', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>&nbsp;</div>

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-6" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">
                                <a  href="{!! route('backend.compliance.menu') !!}"  class="btn btn-primary site-btn cancel_button">@lang('buttons.general.cancel')</a>

                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}


        </div>
    </div>

    {{--</section>--}}

@stop


@push('after-script-end')

<script  type="text/javascript">

</script>

@endpush
