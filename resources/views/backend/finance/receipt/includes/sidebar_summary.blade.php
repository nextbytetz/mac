

{{--sidebar receipt summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            {{--Rctno--}}
            <tr>
                <td align="center"><h5><b><span class="light_dark_color" >@lang('labels.backend.finance.header.receipt.action.index'): {!! Form::label( 'rctno', $receipt->rctno, [ 'id'=> 'rctno'])
                        !!}</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table>
            {{--Duplicate Status--}}
            <tr>
                <td style="padding-left: 5px" width="140px"  colspan="2">
                    @if ($receipt->duplicate_status)
                        <b style="font-size: 15px;"><span class='tag tag-danger white_color'>@lang('labels.backend.table.receipt.duplicate')</span></b>
                    @endif
                </td>
            </tr>
            {{--Status--}}
            <tr>
                <td style="padding-left: 5px" width="140px"><b>@lang('labels.backend.table.receipt.status')</b></td>
                <td  height="20px"><b style="font-size: 20px;">{!! $receipt->status_label !!}</b></td>
            </tr>
            {{--Payer--}}
            <tr>
                <td style="padding-left: 5px" width="140px">@lang('labels.backend.finance.receipt.received_from')</td>
                <td  height="20px"><b>{!! Form::label( 'payer', $receipt->payer, [ 'id'=> 'payer']) !!}</b></td>
            </tr>
            {{--Amount--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.amount')</td>
                <td><b class="underline">{!! Form::label( 'amount', $receipt->amount_comma, [ 'id'=> 'amount']) !!}</b></td>
            </tr>
            {{--Total Contribution--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.total_contribution')</td>
                <td><b class="underline">{!! Form::label( 'total_contribution', number_format($receipt->receiptCodes()->where(['fin_code_id' => 2])->sum('amount'), 2 , '.' , ',' ), [ 'id'=> 'total_contribution']) !!}</b></td>
            </tr>
            {{--Total Interest--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.total_interest')</td>
                <td><b class="underline">{!! Form::label( 'total_interest', number_format($receipt->receiptCodes()->where(['fin_code_id' => 1])->sum('amount'), 2 , '.' , ',' ), [ 'id'=> 'total_interest']) !!}</b></td>
            </tr>
            {{--Rct Date--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.receipt_date')</td>
                <td><b>{!! Form::label( 'rct_date', $receipt->rct_date_formatted, [ 'id'=> 'rct_date']) !!}</b></td>

            </tr>
            {{--Payment Type--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.payment_type')</td>
                <td><b>{!! Form::label( 'payment_type', ($receipt->payment_types()->count()) ? $receipt->payment_types->name : ' ', [ 'id'=> 'payment_type'])
                        !!}</b></td>

            </tr>
            {{--if payment type is cheque--}}
            @if ( $receipt->payment_type_id == 2)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.cheque_no')</td>
                    <td><b>{!! Form::label( 'chequeno', $receipt->chequeno, [ 'id'=> 'chequeno'])
                        !!}</b></td>
                </tr>
            @endif
            {{--Currency--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.currency')</td>
                <td><b>{!! Form::label( 'currency', $receipt->currencies->code, [ 'id'=> 'currency'])
                        !!}</b></td>
            </tr>
            {{--Banks--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.bank')</td>
                <td><b>{!! Form::label( 'bank', ($receipt->banks()->count()) ? $receipt->banks->name : ' ', [ 'id'=> 'bank'])
                        !!}</b></td>
            </tr>
            {{--Description--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.payment_description')</td>
                <td><b>{!! Form::label( 'descr', $receipt->description, [ 'id'=> 'descr']) !!}</b></td>
            </tr>
            {{--User Name--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.captured_by')</td>
                <td><b>{!! Form::label( 'user_name', ($receipt->user()->count()) ? $receipt->user->username : ' ', [ 'id'=> 'user_name'])
                        !!}</b></td>
            </tr>
            {{--Created At--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.captured_date')</td>
                <td><b>{!! Form::label( 'created_at', $receipt->created_at_formatted, [ 'id'=> 'created_at'])
                        !!}</b></td>
            </tr>
            {{--Source--}}
            <tr>
                <td style="padding-left: 5px">Source</td>
                <td>{!! $receipt->source_formatted !!}</td>
            </tr>
            {{--Arrear Status--}}
            @if ($receipt->arrear_status)
                <tr>
                    <td colspan="2">"<span class='tag tag-danger white_color'>Has Arrears</span>"</td>
                </tr>
            @endif

        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush