
{{--contribution table--}}

<legend class="grey_modal" >@lang('labels.backend.legend.contribution_overview')</legend>
<br/>
<table class="display" cellspacing="0" width="100%" id ="contribution-table">
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.backend.table.receipt.contrib_month')</th>
        <th>@lang('labels.backend.table.member_count')</th>
        <th>@lang('labels.backend.table.amount')</th>
        <th>@lang('labels.backend.table.linked_file')</th>
        <th>@lang('labels.backend.table.action')</th>
        <th>Pay Status</th>
        <th>Member Count</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th colspan="3" style="text-align:right">Total:</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
</table>



@push('contribution-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#contribution-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.receipt.contributions', $receipt->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'workflow_status' , name: 'workflow_status' },
                { data: 'contrib_month_formatted' , name: 'contrib_month' },
                { data: 'member_count', name: 'member_count' },
                { data: 'amount', name: 'amount' },
                { data: 'status', name: 'status' }, //added col
                { data: 'action', name: 'action' }, //added col
                { data: 'pay_status_label', name: 'pay_status_label', searchable: false, orderable: false },
                { data: 'member_diff_label', name: 'hasmemberdiff', searchable: false, orderable: false },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/finance/receipt_code/" + aData['id'];
                    {{--document.location.href ='{!! route('backend.finance.receipt_code.edit', $id) !!}';--}}

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            },
//            Get Total amount
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                function commaSeparateNumber(val){
                    while (/(\d+)(\d{3})/.test(val.toString())){
                        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                    }
                    return val ;
                }

                $( api.column( 3 ).footer() ).html(
                    ''+ commaSeparateNumber(total.toFixed(2))
                );
            }
        } );
    });
</script>;

@endpush