<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <meta charset="utf-8" />
    <title>@lang('labels.backend.finance.receipt.print.title')</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Erick Chrysostom <e.chrysostom@nextbyte.co.tz>"/>
    <meta name="subject" content="{!! env('COMPANY') !!}, @lang('labels.backend.finance.receipt.print.subject')"/>
    <meta name="keywords" content="receipts"/>
    <meta name="date" content="2017-05-25"/>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/print_receipt.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
</head>
<body>

@for ($i = 0; $i < 3; $i++)
    <?php $bottom_separator = "jamana_bottom_separator"; $top_space = "top_space"; $class="$bottom_separator top_space"; $jamana_adjust = ""; ?>
    @if ($i == 2)
        <?php
        $class= $top_space;
        ?>
    @endif
    @if ($i == 0)
        <?php
        $class= $bottom_separator;
        ?>
    @endif
    {{--start: Changes subject to be modified or removed in future--}}
    @if ($i == 1)
        <?php
        $jamana_adjust = "jamana_adjust_second_receipt";
        ?>
    @endif
    @if ($i == 2)
        <?php
        $jamana_adjust = "jamana_adjust_third_receipt";
        ?>
    @endif
    {{--end: Changes subject to be modified or removed in future--}}

    <div class="page">
        <div class="{!! $class !!}">
            {{--start: To be edited in a near future--}}
            <div class="{!! $jamana_adjust !!}"></div>
            {{--end: To be edited in a near future--}}
            {{--<div class="background">
            <p class="bg-text">WCF</p>
        </div>--}}
            <div class="heading">
                <div class="logo">
                    {{--<img  class="logo_img" src="{!! asset_url() . "/nextbyte/img/wcf_big_logo_no_background.png" !!}">--}}
                </div>
                <div class="rctno">@lang('labels.backend.finance.receipt.receipt_no'):&nbsp;&nbsp;<b>{!! $receipt->rctno !!}</b></div>
            </div>
            <div class="received_from">
                <div style="float: left;">@lang('labels.backend.finance.receipt.received_from'):</div>
                <div style="float: right;"><b>@lang('labels.backend.finance.receipt.amount'):&nbsp;&nbsp;{!! $receipt->currencies->code !!}&nbsp;&nbsp;<span class="amount">{!! $receipt->amount_comma !!}</span></b></div>
            </div>
            <div class="payer">
                <div style="float: left;font-weight: bold;"> @if (!is_null($receipt->employer_id)) {!! $receipt->employer->reg_no !!}&nbsp;,&nbsp; @endif {!! $receipt->payer !!}</div>
            </div>
            {{--<div class="amount_words">
                <div style="float: left;">
                    <b>@lang('labels.backend.finance.receipt.shillings_in_words'):</b>&nbsp;&nbsp;
                    <span class="underline">{!! ucfirst(convert_number_to_words($receipt->amount)) !!}</span>
                </div>
            </div>--}}
            <div class="amount_words_v2">
                <b>@lang('labels.backend.finance.receipt.shillings_in_words'):</b>&nbsp;&nbsp;
                <div class="underline_v2">
                    {!! ucfirst(convert_number_to_words($receipt->amount)) !!}
                </div>
            </div>
            <div class="in_respect">
                <div style="float: left;"><b>@lang('labels.backend.finance.receipt.in_respect'):</b>&nbsp;&nbsp;{!! $receipt->isPayFor() !!}
                </div>
            </div>
            <div class="bank">
                <div style="float: left;">
                    @if ($receipt->payment_types->id == 2)
                        {!! $receipt->payment_types->name !!}&nbsp;&nbsp;-&nbsp;&nbsp;<b>{!! $receipt->chequeno !!}</b>&nbsp;of&nbsp;{!! $receipt->rct_date_formatted !!}
                    @else
                        {!! $receipt->payment_types->name !!}&nbsp;&nbsp;of&nbsp;&nbsp;{!! $receipt->rct_date_formatted !!}
                    @endif
                </div>
                <div style="float: right;">
                    @lang('labels.backend.finance.receipt.bank')&nbsp;&nbsp;:&nbsp;&nbsp;<b>{!! $receipt->banks->name !!}</b>
                </div>
            </div>
            <div class="content">
                <table>
                    <tr>

                        <td> @lang('labels.general.name'):&nbsp;<b>{!! $receipt->user->username !!}</b>&nbsp;
                            @lang('labels.backend.finance.receipt.signature'):&nbsp;&nbsp;<span style="width: 8mm;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        <td>@lang('labels.backend.finance.receipt.captured_date'):&nbsp;<b>{!! $receipt->created_at_formatted !!}</b></td>
                        <td>@lang('labels.backend.finance.receipt.station'):&nbsp;<b>{!! (!is_null($receipt->office)) ? $receipt->office->name : '' !!}</b></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

@endfor

<script type="text/javascript">
    /*window.onload = function() { window.print(); window.close();}*/
    window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    /* window.onfocus=function(){ window.close();} */
</script>
</body>
</html>