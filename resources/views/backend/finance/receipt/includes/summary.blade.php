{{--RECEIPT SUMMARY--}}
{{--Receipt no--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.receipt_no'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','rctno', null, ['class' => 'form-control', 'disabled' => true]) !!}
                    {!! Form::input('hidden', 'rctno', null, ['class' => 'form-control']) !!}

                </div>
            </div>
        </div>

        {{--currency--}}
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.currency'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'currency_name', ($receipt->currencies()->count()) ? $receipt->currencies->code : ' ', ['class' => 'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--AMOUNT--}}
<div class="row">
    <div class="col-md-9" class="form-inline">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.amount'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'amount', $receipt->amount_comma,
                    ['class' =>
                    'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
        {{--BANK--}}
        <div class="element-form">
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.bank'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'bank_name', ($receipt->banks()->count()) ? $receipt->banks->name : ' ', ['class' => 'form-control', 'disabled' => true]) !!}


                </div>
            </div>
        </div>
    </div>
</div>


{{--PAYMENT TYPE--}}
<div class="row">
    <div class="col-md-9" class="form-inline">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.payment_type'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'payment_type', ($receipt->payment_types()->count()) ?  $receipt->payment_types->name : ' ', ['class' => 'form-control', 'disabled' => true]) !!}
                </div>
            </div>
        </div>
        {{--Checque no--}}
        {{--if payment type is cheque--}}
        @if ($receipt->payment_types->id == 2)
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cheque_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'chequeno', null, ['class' => 'form-control','disabled' => true]) !!}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

{{--Receipt date No--}}
<div class="row">
    <div class="col-md-9" class="form-inline" >
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.receipt_date'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'rct_date', $receipt->rct_date_formatted, ['class' => 'form-control','disabled' => true]) !!}
                </div>
            </div>
        </div>
        {{--Captured Date--}}
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.captured_date'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'created_at', $receipt->created_at_formatted, ['class' => 'form-control','disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--RECEIVED FROM--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.received_from'):</label></div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-xs-10">
                <div class="form-group">
                    {!! Form::input('text', 'payer', null, ['class' => 'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--Payment Description--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.payment_description'):</label></div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-xs-10">
                <div class="form-group">
                    {!! Form::textarea('description', $receipt->isPayFor(), ['class' => 'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--captured by--}}
<div class="row">
    <div class="col-md-9" class="form-inline" >
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.captured_by'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'capture_by',($receipt->user()->count()) ? $receipt->user->username : ' ', ['class' => 'form-control','disabled' =>
                    true]) !!}
                </div>
            </div>
        </div>
        {{--CANCELLED BY--}}
        {{--Status if iscancelled--}}
        @if ($receipt->iscancelled == 1)
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cancelled_by'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'cancelled_by',($receipt->cancelUser()->count()) ? $receipt->cancelUser->username : ' ', ['class' => 'form-control','disabled' =>
                        true]) !!}
                    </div>
                </div>
            </div>
        @endif


        {{--DSIHONOURED BY--}}
        {{--Status if isdishonoured--}}
        @if ($receipt->isdishonoured == 1)
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.dishonoured_by'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'dishonoured_by',$receipt->dishonouredCheques->last()->dishonourUser->username , ['class' => 'form-control','disabled' =>
                        true]) !!}
                    </div>
                </div>
            </div>
        @endif

    </div>
</div>

{{--Status if iscancelled--}}
    @if ($receipt->iscancelled == 1)
        {{--cancel date --}}
        <div class="row">
            <div class="col-md-9" class="form-inline" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cancelled_date'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input('text', 'cancel_date', $receipt->cancel_date_formatted, ['class' => 'form-control','disabled' => true]) !!}
                        </div>
                    </div>
                </div>
                {{--cancelled image--}}
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label></div>
                    <div  class="col-md-3"  >
                        <img class="right"height="40px"  width = "120px" src="{!! asset_url()
                        !!}/global/image/cancelled.png"
                             alt="images">
                    </div>
                </div>
            </div>
        </div>
        {{--cancel reason --}}
        <div class="row">
            <div class="col-md-9" class="form-inline" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cancel_reason'):</label></div>
                    <div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'cancel_reason', null, [ 'id'=> 'cancel_reason',
                            'class' =>'form-control', 'disabled' => true ]) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endif


{{--WHEN RECEIPT IS DISHONOURED--}}
@if ($receipt->isdishonoured == 1)
    {{--cancel date --}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.dishonoured_date'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'dishonour_date',
                        $receipt->dishonour_date_formatted,
                        ['class' => 'form-control','disabled' => true]) !!}
                    </div>
                </div>
            </div>
            {{--cancelled image--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label></div>
                <div  class="col-md-3"  >
                    <img class="right"height="65px"  width = "120px" src="{!! asset_url() !!}/global/image/dishonouredcheque.jpg"
                         alt="images">
                </div>
            </div>
        </div>
    </div>
    {{--dishonour reason --}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.dishonour_reason'):</label></div>
                <div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">
                    <div class="form-group" id = "text_content_dishonour">
                        {!! Form::textarea( 'dishonour_reason', $receipt->dishonouredCheques->last()->dishonour_reason, [ 'id'=>
                        'dishonour_reason',
                        'class' =>'form-control', 'disabled' => true ]) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
