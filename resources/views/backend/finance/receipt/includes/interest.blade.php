
{{--Interest table--}}


{{--<div class="col-md-9" >--}}
    {{--<div>&nbsp; </div>--}}
    <legend class="grey_modal" >@lang('labels.backend.legend.interest_overview')</legend>

    <table class="display" cellspacing="0" width="100%" id ="interest-table">
        <thead>
        <tr>
            <th>@lang('labels.backend.table.receipt.contrib_month')</th>
            <th>@lang('labels.backend.table.booking.late_months')</th>
            <th>@lang('labels.backend.table.interest')</th>
            <th>@lang('labels.backend.table.amount_paid')</th>
        </thead>
        <tfoot>
        <tr>
            <th colspan="2" style="text-align:right"></th>
            <th></th>
            {{--<th colspan="3" style="text-align:right"></th>--}}
            <th></th>
        </tr>
        </tfoot>
    </table>
{{--</div>--}}



@push('interest-script-end')

<script  type="text/javascript">
    $("#tab_2_header").one("click", function(){
    $(function() {
        $('#interest-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info: false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.receipt.interests', $receipt->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'contrib_month_formatted' , name: 'contrib_month' },
                { data: 'late_months', name: 'late_months' }, //added col
                { data: 'interest', name: 'interest' }, //added col
                { data: 'amount', name: 'amount'}
            ],
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

//                get Total for amount booked
                // Update footer
                function commaSeparateNumber(val){
                    while (/(\d+)(\d{3})/.test(val.toString())){
                        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                    }
                    return val;
                }

                $( api.column( 2 ).footer() ).html(
                    ''+ commaSeparateNumber(total.toFixed(2))
                );

//                get total for amount paid

                // Total over all pages
                total = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                $( api.column( 3 ).footer() ).html(
                    ''+ commaSeparateNumber(total.toFixed(2))
                );

            }
        } );
    });
    });
</script>;

@endpush