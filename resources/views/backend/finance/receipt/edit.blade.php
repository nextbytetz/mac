@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_contribution_months'), 'header_title' => trans('labels.backend.member.edit_contribution_months')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <div class = "row">
        <div class="col-md-9 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Receipt Header detail -->
                <span>{!! $receipt->complete_status_label !!} </span>
                <span>{!! $receipt->isverified_label !!}</span>
                <strong> {!! Form::label( 'name', $receipt->rctno, [ 'id'=> 'rctno']) !!}</strong>
                <small >
                    Payer: {!! Form::label( 'payer', $receipt->payer, [ 'id'=> 'payer']) !!}
                </small>

            </h5>
        </div>
    </div>
    <br/>
    {!! Form::open(['route' => ['backend.finance.receipt.edit_months.post', $receipt->id], 'name' => 'edit_contribution_months']) !!}
    {!! Form::hidden("employer_id", $receipt->employer_id) !!}
    <div class="row">
        <div class="col-md-12">
            <?php
            $color = [
                0 => 'blue',
                1 => 'grey',
            ];
            $x = 1;
            $entry_amount = 0;
            ?>
            @foreach ($receipt->receiptCodes as $code)
                <div class="fileld-layout" id="contribution_entry_group{!! $code->id !!}" style="padding-left:20px; border-left: 2px solid {!! $color[$x % 2] !!};">
                    <div class="form-group">
                        <legend></legend><br/>
                        <div class="form-inline">
                            {{--Contribution Month--}}
                            <span>
                Month:
            </span>
                            <span>
                                        {!!  Form::selectMonth('contribution_month' . $code->id, \Carbon\Carbon::parse($code->contrib_month)->format("n"), ['class' => 'form-control contribution_month','style'=>'width:100px;border-radius:6px;', 'placeholder' => '', 'id' => 'contribution_month' . $code->id]) !!}
                                    </span>
                            <span>
                Year:
            </span>
                            <span>
                                        {!!  Form::selectRange('contribution_year' . $code->id, Carbon\Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1)->format('Y'), 2015 , \Carbon\Carbon::parse($code->contrib_month)->format("Y"), ['class' => 'form-control contribution_year','style'=>'width:100px;border-radius:6px;', 'placeholder' => '', 'id' => 'contribution_year' . $code->id]) !!}&nbsp;&nbsp;
                                    </span>
                            @if ($code->fin_code_id == 2)
                        <!-- start : Entry for contribution paid amount -->
                            <span>
                                        <label class="required">  @lang('labels.backend.finance.receipt.paid_amount')&nbsp;{!! $code->pay_status_label !!}&nbsp;:</label>&nbsp;&nbsp;
                                    </span>
                            <span>
                                        {!! Form::text('contribution_amount' . $code->id, $code->amount, ['class' => 'form-control number money contribution_amount', 'style' => "border-radius:3px;width:20%;", 'id' => 'contribution_amount' . $code->id]) !!}&nbsp;&nbsp;
                                    </span>
                            <!-- end : Entry for contribution paid amount -->
                            <!-- start : Entry for contribution member count -->
                            <span>
                                        <label> @lang('labels.backend.finance.receipt.member_count')&nbsp;{!! $code->member_diff_label !!}&nbsp;:</label>&nbsp;&nbsp;
                                    </span>
                            <span>
                                        {!! Form::text('member_count' . $code->id, $code->member_count, ['class' => 'form-control number', 'style' => "border-radius:3px;width:10%;", 'id' => 'member_count' . $code->id]) !!}&nbsp;&nbsp;
                                    </span>
                            <!-- end : Entry for contribution member count -->
                            @endif
                            @if ($code->fin_code_id == 1)
                            <!-- start : Interest Paid -->
                            <span>
                                            <label class="required"> @lang('labels.backend.finance.receipt.paid_interest') :</label>&nbsp;&nbsp;
                                        </span>
                            <span>
                                            {!! Form::text('interest_amount' . $code->id, $code->amount, ['class' => 'form-control number money interest_amount', 'style' => "border-radius:3px;width:20%;", 'id' => 'interest_amount' . $code->id]) !!}&nbsp;&nbsp;
                                        </span>
                            <!-- end : Interest Paid -->
                            @endif
                        </div>
                    </div>
                </div>
                <?php
                $x++;
                $entry_amount += $code->amount;
                ?>
            @endforeach
            <hr/>
                {{--<div id="contribution_summary">--}}
                <div class="fileld-layout">
                    <div class="form-group">
                        <div class="form-inline">
                                        <span>
                                            @lang('labels.backend.finance.receipt.total_entry')&nbsp;:&nbsp;&nbsp;
                                        </span>
                            <span id = "total_entry" class="underline" style="font-weight: bold;">
                                            0
                                        </span>
                            <span>
                                            ,&nbsp;@lang('labels.backend.finance.receipt.bank_amount') &nbsp;:&nbsp;&nbsp;
                                        </span>
                            <span id="bank_amount" class="underline" style="font-weight: bold;">
                                            0
                                        </span>
                            <span id="contribution_summary_icon">

                                        </span>
                        </div>
                    </div>
                </div>
                {{--</div>--}}
                <hr/>
            <div class="pull-right">
                {!! link_to_route('backend.finance.receipt.profile_by_request', trans('buttons.general.cancel'), [$receipt->id], ['class' => 'btn btn-danger btn-sm cancel_button']) !!}
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.update')" />
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
<script>
    var month_threshold = parseInt("{!! $month_threshold !!}");
    var year_threshold = parseInt("{!! $year_threshold !!}");
    var $minimum_contribution = parseInt("{!! $minimum_contribution !!}");
    var receipt_amount = {!! $receipt->amount !!};
    var entry_amount = {!! $entry_amount !!};
    $(function(){
        /*$(".contribution_year").prop('disabled', true);
        $(".contribution_month").prop('disabled', true);*/
        /*$(".search-select").select2({});*/
        //$(".contribution_year").select2({});
        //$(".contribution_month").select2({});
        $('#bank_amount').text(addCommas(receipt_amount));
        $('#total_entry').text(addCommas(receipt_amount));
        check_amount_match(receipt_amount, entry_amount);
        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });
        /* end : mask all money input */
        /* start : handle amount textboxes change event and sum all the entered amounts */
        $('body').off('keyup', '.contribution_amount').on('keyup', '.contribution_amount', function(e) {
            var value;
            var sum = 0;
            $('.contribution_amount').each(function() {
                value = $(this).val();
                if (value != '') {
                    sum += parseFloat(value.split(",").join(""));
                }
            });
            $('.interest_amount').each(function() {
                value = $(this).val();
                if (value != '') {
                    sum += parseFloat(value.split(",").join(""));
                }
            });
            entry_amount = sum;
            $('#total_entry').text(addCommas(entry_amount));
            check_amount_match(receipt_amount, entry_amount);
        });
        $('body').off('keyup', '.interest_amount').on('keyup', '.interest_amount', function(e) {
            var value;
            var sum = 0;
            $('.interest_amount').each(function() {
                value = $(this).val();
                if (value != '') {
                    sum += parseFloat(value.split(",").join(""));
                }
            });
            $('.contribution_amount').each(function() {
                value = $(this).val();
                if (value != '') {
                    sum += parseFloat(value.split(",").join(""));
                }
            });
            entry_amount = sum;
            $('#total_entry').text(addCommas(entry_amount));
            check_amount_match(receipt_amount, entry_amount);
        });
        /* end : handle amount textboxes leave event and sum all the entered amounts */
        /* start : ensure only numbers are input on monetary boxes */
        $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
            number_only(e)
        });
        /* end : ensure only numbers are input on monetary boxes */
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form[name=edit_contribution_months]', function (e) {
            e.preventDefault();
            var form = this;
            //alert($form);
            //$form.submit();
            var $data = $(form).serialize();
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            $.ajax({
                data : $data,
                dataType : "json",
                method : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    if (receipt_amount < $minimum_contribution) {
                        amaran_notify("{!! trans("exceptions.backend.finance.receipts.small_amount", ['amount' => $minimum_contribution]) !!}", "{!! trans("labels.general.error") !!}", "error");
                        return false;
                    }
                    if (!allow_match(receipt_amount, entry_amount)) {
                        amaran_notify("{!! trans("exceptions.backend.finance.receipts.not_tallied") !!}", "{!! trans("labels.general.error") !!}", "error");
                        return false;
                    }
                    $(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    if (data.success) {
                        /* window.location = base_url + "/finance/receipt/" + data.id + "/edit"; */
                        document.location.replace(base_url + "/finance/receipt/profile/" + data.id);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    /* console.log(errors); */
                    $.each(errors, function(index, value) {
                        $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });
                },
            }).done(function() {

            }).fail(function() {
                $(".btn-submit").prop('disabled', false);
            }).always(function() {
                $(".btn-submit").prop('disabled', false);
            });
        });
        /* end: Submitting Form and perfom validation on the server side */
    });
    /* start : ensure only numbers are input on monetary boxes */
    function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    /* end : ensure only numbers are input on monetary boxes */
    /* start : check if the receipt amount and the total contribution entries matches */
    function check_amount_match($receipt_amount, $entry_amount) {
        var $diff = $receipt_amount - $entry_amount;
        switch(true) {
            case ($diff < 0):
                $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:red;'><i class='icon fa fa-2x fa-close' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.less")</span>");
                break;
            case ($diff > 0.1):
                $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:#ffd303;'><i class='icon fa fa-2x fa-warning' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.greater")</span>");
                break;
            case ($diff >= 0 && $diff <= 0.1):
                $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:green;'><i class='icon fa fa-2x fa-check-square-o' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.matched")</span>");
                break;
        }
    }
    /* end : check if the receipt amount and the total contribution entries matches */
    function amaran_notify(message, title, type) {
        var icon;
        if(typeof type === "undefined") {
            type = 'success';
        }
        switch(type) {
            case "success":
                icon = "fa fa-check-square-o";
                break;
            case "warning":
                icon = "icon fa fa-warning";
                break;
            case "error":
                icon = "icon fa fa-ban";
                break;
            default:
                icon = "fa fa-check-square-o";
        }
        $.amaran({
            'theme'     :'awesome ' + type,
            'content'   :{
                title:title,
                message:message,
                info:'',
                icon:icon,
            },
            'position'  :'bottom left',
            'outEffect' :'slideBottom',
            'inEffect'  :'slideLeft'
        });
    }
    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    /* start : start allow amount match */
    function allow_match($receipt_amount, $entry_amount) {
        var $diff = $receipt_amount - $entry_amount;
        var $return = false;
        switch(true) {
            case ($diff < 0):
                $return = false
                break;
            case ($diff > 0.1):
                $return = false;
                break;
            case ($diff >= 0 && $diff <= 0.1):
                $return = true;
                break;
        }
        return $return;
    }
    /* end : start allow amount match */
</script>
@endpush