@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.receipt.administrative'), 'header_title' => trans('labels.backend.finance.receipt.create_administrative')])

{{--@include('backend.includes.toastr_assets')--}}

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/employer.css") }}
<style>
    .payment_mode:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.finance.receipt.mode')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .payment_mode {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .receipt_details:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.finance.receipt.details')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .receipt_details {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    {!! Form::open(['route' => 'backend.finance.receipt.administrative.post', 'name' => 'administrative_receipt']) !!}
    {!! Form::hidden("source", 1) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="receipt_details">
                <div class="row">
                    <div class="col-md-6">
                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.finance.receipt.pay_for')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('fin_code_id', $fin_codes, null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">Payer Category</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select( 'category', ['0' => 'Staff', '1' => 'Third Party'], null, ['class' => 'form-control search-select category_select', 'placeholder' => '', 'style' => 'width:50%;']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout" style="display: none;" id = "staff">
                            <label class="required">@lang('labels.backend.finance.receipt.payer')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {{--{!! Form::text('payer', null, ['class' => 'form-control', 'style' => "border-radius:3px;"]) !!}--}}
                                    {!! Form::select('staff_id', $staffs, null, ['class' => 'form-control search-select', 'id' => 'staff_select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout" style="display: none;" id = "firm">
                            <label class="required">Third Party Payer</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select( 'thirdparty_id', $thirdparty_payers, null, ['class' => 'form-control search-select', 'id' => 'firm_select', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <div class="form-group">
                                <label class="required" for="receipt_amount">@lang('labels.backend.table.amount')</label>
                                <div class="input-group">
                                    {!! Form::text('amount', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'receipt_amount']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span>{{ trans('labels.backend.finance.receipt.bank_amount') }}</span>
                                </span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.finance.receipt.receipt_date')</label>
                            <div class="form-group">
                                <div class="form-inline">
                                    <span>
                                        {!!  Form::selectRange('receipt_date',1 , 31, null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                                    <span>
                                        {!!  Form::selectMonth('receipt_month',null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                                    </span>
                                    <span>
                                        {!!  Form::selectRange('receipt_year', Carbon\Carbon::now()->format('Y'), 2015 , null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                                    </span>
                                    <span>
                                        {!! Form::hidden('rct_date') !!}
                                        {!! Form::hidden('today_date', $this_date) !!}
                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.table.bank')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('bank_id', $banks, null, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '', 'id' => 'bank_select']) !!}
                                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">Bank Account</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('bank_code', [], null, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '', 'id' => 'fin_code_id']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.finance.currency.index')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('currency_id', $currencies, null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>@lang('labels.backend.finance.receipt.reference')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('bank_reference', null, ['class' => 'form-control', 'style' => "border-radius:3px;"]) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-exchange" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span>{{ trans('labels.backend.finance.receipt.reference_helper') }}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="payment_mode">
                                <div class="radio-vertical">
                                    @foreach ($payment_types as $payment_type)
                                        <div class="radio-button">
                                            {!! Form::radio('payment_type_id', $payment_type->id, false, ['id' => "radio-button" . $payment_type->id]) !!}
                                            <label for="radio-button{!! $payment_type->id !!}"></label>
                                            <span>{!! $payment_type->name !!}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="fileld-layout" id="chequeno_entry" style="display:none;">
                            <label class="required">@lang('labels.backend.finance.receipt.cheque')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('chequeno', null, ['class' => 'form-control', 'style' => "border-radius:3px;"]) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>@lang('labels.backend.finance.receipt.payment_description')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::textarea('description', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="receivables">
                <div class="row">
                    <div class="col-md-12">
                        {{--</div>--}}
                        <hr/>
                        <div class="pull-right">
                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.create')" />
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@push('after-script-end')

<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}

<script>
    $(function(){
        $(".search-select").select2({});
        autosize($("textarea.autosize"));
        $('#bank_select').on('change', function (e) {
            $("#spin2").show();
            var $bank_id = e.target.value;
            $.get("{{ url('/') }}/getFinCodes?id=" + $bank_id, function (data) {
                $('#fin_code_id').empty();
                $("#fin_code_id").select2("val", "");
                $('#fin_code_id').html(data);
                $("#spin2").hide();
            });
        });
        /* start : ensure only numbers are input on monetary boxes */
        $(".number").keydown(function (e) {
            number_only(e)
        });
        /* end : ensure only numbers are input on monetary boxes */
        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });
        /* end : mask all money input */
        $(".category_select").on("change", function () {
            var $category = $(this).val();
            switch($category) {
                case '0':
                    /* staff Category */
                    $("#staff").show();
                    $(".staff_select").prop('disabled', false);
                    $("#firm").hide();
                    $("#firm_select").prop('disabled', true);
                    break;
                case '1':
                    /* Third Party Payer Category */
                    $("#staff").hide();
                    $(".staff_select").prop('disabled', true);
                    $("#firm").show();
                    $("#firm_select").prop('disabled', false);
                    break;
                default:
                    $("#staff").hide();
                    $(".staff_select").prop('disabled', true);
                    $("#firm").hide();
                    $("#firm_select").prop('disabled', true);
            }
        });
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form[name=administrative_receipt]', function (e) {
            e.preventDefault();
            var form = this;
            var $receipt_date = $("select[name=receipt_date]").val();
            var $receipt_month = $("select[name=receipt_month]").val();
            var $receipt_year = $("select[name=receipt_year]").val();
            if ($receipt_date != '' && $receipt_month != '' && $receipt_year != '') {
                $("input[name=rct_date]").val($receipt_year + '-' + $receipt_month + '-' + $receipt_date);
            } else {
                $("input[name=rct_date]").val("");
            }
            var $data = $(form).serialize();
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            $.ajax({
                data : $data,
                dataType : "json",
                method : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    $(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    if (data.success) {
                        /* window.location = base_url + "/finance/receipt/" + data.id + "/edit"; */
                        document.location.replace(base_url + "/finance/receipt/" + data.id + "/edit");
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    /* console.log(errors); */
                    $.each(errors, function(index, value) {
                        $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });
                },
            }).done(function() {

            }).fail(function() {

            }).always(function() {
                $(".btn-submit").prop('disabled', false);
            });
        });
        /* end: Submitting Form and perfom validation on the server side */
        /* start : Payment type change event */
        $('input[type=radio][name=payment_type_id]').change(function() {
            if (this.value == 2 || this.value == 5) {
                $("#chequeno_entry").show();
            } else {
                $("#chequeno_entry").hide();
            }
        });
        /* end : Payment type change event */
    });

    /* start : ensure only numbers are input on monetary boxes */
    function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    /* end : ensure only numbers are input on monetary boxes */

</script>
@endpush