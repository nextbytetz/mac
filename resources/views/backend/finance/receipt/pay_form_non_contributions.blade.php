{{-- {{dd($bill)}} --}}
@extends('layouts.backend.main', ['title' => "Payment Forms", 'header_title' => ' Control Number Order Forms '])


@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12">
		<h4 class="client-title">
			<i class="icon-circle"></i>

			@if(!is_null($bill->control_no))
			<span><span class="tag tag-success success_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="Has Control No">.</span> </span>
			
			@else
			<span><span class="tag tag-warning warning_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="No Control No">.</span> </span>
			@endif
			&nbsp; &nbsp; {{ucwords($payer_name)}} - {{$bill->bill_description}}
		</h4>
		<legend></legend>
	</div>
</div>

<div class="row"> 
	<br><br>
	<div class="col-md-12"> 
		@if(isset($bill))
		{{-- @if(empty($bill->control_no))
		<div class="col-sm-12">
			<div class="element-form">
				<div class="col-md-6 col-sm-12"><label>Amount:</label></div>
				<div class="col-md-6 col-sm-12"><label>{{$bill->bill_amount}}</label></div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="element-form">
				<div class="col-md-6 col-sm-12"><label>Description:</label></div>
				<div class="col-md-6 col-sm-12"><label>{{$bill->bill_description}}</label></div>
			</div>
		</div>

		@else --}}
		<div class="col-sm-6 col-md-12"> 
			<h5>
				@if(!empty($bill->control_no))
				Control Number For Tzs. {{number_format($bill->bill_amount, 2)}} - {{$bill->control_no}}
				@else
				Control Number Of Tzs. {{number_format($bill->bill_amount, 2)}} 
				Is still being processed. {{-- Please <a href="" class="btn btn-md btn-warning"><i class="fa fa-refresh"></i> Click To Refresh</a> --}}
				@endif
			</h5> 

			<br><br>

			<h6>
				<div class="row">
					<div class="col-sm-4">Direct Deposit Form :</div>
					<div class="col-sm-8">
						@if(!empty($bill->control_no))
						<a href="{{url('/finance/normal_bill/'.$bill->b_id)}}" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download Direct Deposit Form</a>
						@else
						<a href="" class="btn btn-md btn-warning"><i class="fa fa-refresh"></i> Waiting For Control No#</a>
						@endif
					</div>
				</div>				
			</h6>
			<br><br>

			<h6>
				<div class="row">
					<div class=" col-sm-4">NMB Transfer Form :</div>
					<div class=" col-sm-8">
						@if(!empty($bill->control_no))
						<a href="{{url('/finance/nmb_transfer/'.$bill->b_id)}}" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download NMB Transfer Form</a>
						@else
						<a href="" class="btn btn-md btn-warning"><i class="fa fa-refresh"></i> Waiting For Control No#</a>
						@endif
					</div>
				</div>				
			</h6>

			<br><br>
			<h6>

				<div class="row">
					<div class=" col-sm-4">CRDB transfer Form :</div>
					<div class=" col-sm-8">
						@if(!empty($bill->control_no))
						<a href="{{url('/finance/crdb_transfer/'.$bill->b_id)}}" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download CRDB transfer Form</a>
						@else
						<a href="" class="btn btn-md btn-warning"><i class="fa fa-refresh"></i> Waiting For Control No#</a>
						@endif
					</div>
				</div>	
			</h6>

		</div>

		{{-- @endif --}}
		@endif
	</div>

	@endsection

	@push('after-script-end')
	<!-- Custom javascript files for this page -->
	{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
	{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


	<script type="text/javascript">

		$('document').ready(function(){



		});

	</script>





	<script>
		(function (window, document, $) {

		})(window, document, jQuery);
	</script>

	@endpush