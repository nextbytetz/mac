@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.receipt.action.index'), 'header_title' => trans('labels.backend.finance.header.receipt.action.index')])

@push('after-styles-end')

@endpush

@section('content')
    {!! Form::model($receipt, ['route' => ['backend.finance.receipt.dishonour', $receipt->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12" >

            <div class = "pull-right">
                {{--REPLACE CHEQUE--}}
                @if ($receipt->isdishonoured == 1)
                    {!! link_to_route('backend.finance.receipt.new_cheque',trans('buttons.general.replace_cheque'), [$receipt->id],['id'=> 'replace_submit',  'class' => 'btn btn-primary site-btn dishonour_button', ]) !!}
                    {{--CANCEL--}}
                    {!! link_to_route('backend.finance.receipt.cancel_reason',trans('buttons.general.cancel'), [$receipt->id],['id'=> 'cancel_submit',  'class' => 'btn btn-primary site-btn delete_button', ]) !!}
                @endif
                @if ($receipt->iscancelled== 0 && $receipt->isdishonoured == 0 )
                    {{--PRINT--}}
                    {{ link_to_route('backend.finance.receipt.print', trans('buttons.general.print'), [$receipt->id], ['class' => 'btn btn-primary site-btn invoice-print save_button', 'data-icon' => ' ', 'target' => '_blank']) }}

                    @if(!is_null($receipt->chequeno))
                        {{--DISHONOUR--}}
                        {!! link_to_route('backend.finance.receipt.dishonour_reason',trans('buttons.general.dishonour'), [$receipt->id],['id'=> 'dishonour_submit',  'class' => 'btn btn-primary site-btn dishonour_button', ]) !!}
                    @endif
                    {{--CANCEL--}}
                    {!! link_to_route('backend.finance.receipt.cancel_reason',trans('buttons.general.cancel'), [$receipt->id],['id'=> 'cancel_submit',  'class' => 'btn btn-primary site-btn delete_button', ]) !!}
                @endif

            </div>

        </div>
    </div>
    <legend>
    </legend>
    <div>&nbsp;</div>

    {{--Summary of receipt--}}
    <div class="row">
        <div class="col-md-12" >
            @include('backend.finance.receipt.includes.summary')
        </div>
    </div>

    {{--modal contain receipt printout--}}
    {{--@include('backend.finance.receipt.includes.print_receipt')--}}


    {{--    <div class="modal fade danger-modal danger-background-modal " id="printout-modal" tabindex="-1" role="dialog" aria-labelledby="danger-background-modal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content grey_modal">

                    <div class="modal-body grey_modal">
                        @include('backend.finance.receipt.includes.print_receipt')
                    </div>

                </div>
            </div>
        </div>--}}


    {!! Form::close() !!}

@stop


@push('after-script-end')
<script  type="text/javascript">
    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();

    $('#text_content_dishonour').on( 'change keyup keydown paste cut',
        'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();
</script>;

{{--{{ Html::script(asset_url() . "/global/plugins/jQuery.print/jQuery.print.js") }}
{{ Html::script(asset_url() . "/global/js/global/invoice.js") }}--}}

@endpush
