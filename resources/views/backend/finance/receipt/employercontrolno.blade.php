@extends('layouts.backend.main', ['title' => trans('labels.backend.member.employer.title.index'), 'header_title' => trans('labels.backend.member.employer.header.index')])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            {{----------Button Add New Employer -----------}}
            {{--<div class="col-md-12" >--}}
            {{--<div class="pull-right">--}}
            {{--<a href="#"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.backend.table.employer.add_employer')</a>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div>&nbsp;</div>

            <table class="display" cellspacing="0" width="100%" id ="employers-table">
                <thead>
                <tr >
                    <th>@lang('labels.backend.table.name')</th>
                    <th>@lang('labels.backend.table.employer.reg_no')</th>
                    <th>@lang('labels.backend.table.employer.date_commenced')</th>
                    <th>@lang('labels.backend.table.country')</th>
                    <th>@lang('labels.backend.table.region')</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        $('#employers-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.get.employers') !!}',
                type : 'get'

            },
            columns: [
                { data: 'name_formatted' , name: 'name'},
                { data: 'reg_no', name: 'reg_no'},
                { data: 'doc_formatted', name: 'doc'},
                { data: 'country', name: 'country' },
                { data: 'region', name: 'region' }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = "generate_bill_form/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });







</script>;

@endpush
