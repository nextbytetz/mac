@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance.employees'), 'header_title' => trans('labels.backend.compliance.employees')])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            {{----------Button Add New Employee -----------}}
            {{-- <div class="col-md-12" > --}}
            {{-- <div class="pull-right"> --}}
           {{--  <a href="{!! route('backend.compliance.employee.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a> --}}
            {{-- </div> --}}
            {{-- </div> --}}


            <div>&nbsp;</div>

            <table class="display" cellspacing="0" width="100%" id ="employees-table">
                <thead>
                <tr >
                    <th>@lang('labels.backend.table.memberno')</th>
                    <th>@lang('labels.backend.table.name')</th>
                    <th>@lang('labels.backend.table.dob')</th>
                    <th>@lang('labels.backend.table.receipt.employer')</th>
                    <th>@lang('labels.backend.table.category')</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#employees-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employee.get') !!}',
                type : 'get'
            },
            columns: [
                { data: 'memberno', name: 'memberno'},
                { data: 'fullname' , name: 'fullname', orderable : true, searchable : false},
                { data: 'dob', name: 'dob',  orderable : false, searchable : false},
                { data: 'employer_name', name: 'employers.name',  orderable : true, searchable : true},
                { data: 'category', name: 'category' ,orderable : false, searchable : false},
                { data: 'firstname', name: 'firstname' ,  orderable : false, searchable : true, visible: false},
                { data: 'middlename', name: 'middlename' ,  orderable : false, searchable : true, visible: false},
                { data: 'lastname', name: 'lastname' ,  orderable : false, searchable : true,visible: false }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = "create_supplier_employee_form/" + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });







</script>;

@endpush
