@extends('layouts.backend.main', ['title' => 'Pending or Cancelled Transactions', 'header_title' => 'Pending or Cancelled Transactions'])

@include('backend.includes.datatable_assets')

@section('content') 

    <div class = "row">
        <div class="col-md-12" >
           
            <div>&nbsp;</div>

            <table class="display" cellspacing="0" width="100%" id ="control-table">
                <thead>
                <tr >
                    <th>Employer Name</th>
                    <th>Control Number</th>
                    <th>Billed Amount</th>
                    <th>Bill Status</th>
                    <th>Number of employees</th>
                    <th>Bill Description</th>
                    <th>Date Created</th>
                    <th>Expire Date</th>
                    <th>Genaratedby</th>
                    <th>Contact</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        $('#control-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                 url : '{!! route('backend.finance.pending') !!}',
                type : 'get'

            },
            columns: [
                { data: 'payer_name' , name: 'payer_name'},
                { data: 'control_no', name: 'control_no'},
                { data: 'bill_amount', name: 'bills.bill_amount'},
                 { data: 'bill_status', name: 'bills.bill_status'},
                { data: 'member_count', name: 'bills.member_count' },
                { data: 'bill_description', name:'bills.bill_description' },
                { data: 'created_at', name: 'bills.created_at' },
                { data: 'expire_date', name:'bills.expire_date'},
                { data: 'name', name:'users.name'},
                { data: 'mobile_number', name:'bills.mobile_number'}
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = "employer/"+ "profile/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });







</script>;

@endpush
