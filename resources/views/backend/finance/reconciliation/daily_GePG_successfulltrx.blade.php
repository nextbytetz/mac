@extends('layouts.backend.main', ['title' => 'Successfull Transactions', 'header_title' => 'Successfull Transactions from GePG Not Updated in MAC'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content') 



    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.finance.view.successfull.GePG.trx'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')


    {!! Form::close() !!}
     <br>

{!! $dataTable->table(['class' => 'display', 'width' => '100%', 'íd' => 'dataTable'], true) !!}

@stop


@push('after-script-end')
@stack('date-range-script-end')
{!! $dataTable->scripts() !!}

<script type="text/javascript">
    $(function () {
        $(".search-select").select2();
    });
</script>
@endpush
