@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.finance_menu'), 'header_title' => trans('labels.backend.finance.title.finance_menu')])


@section('content')
    {{--<section id="content-wrapper">--}}
    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.receipt.administrative') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-newspaper-o"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.create_new_receipt')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.create_new_receipt_summary')</p> </li>
                    </a>

                </ul>

                {{--item2--}}
                &nbsp;<ul class="list-unstyled">
                    <a href="{!! route('backend.finance.receipt.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-reply-all"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.recall_receipt')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.recall_receipt_summary')</p> </li>
                    </a>

                </ul>
                  {{--Reconcilliation item--}}
                     
             &nbsp;<ul class="list-unstyled">
                    <a href="{!! route('backend.finance.reconcilliation') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-balance-scale"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.gepg_reconcile')</large></h6>


                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.gepg_reconcile_summary')</p> </li>

                {{--item3--}}
                &nbsp<ul class="list-unstyled">
                    <a href="{!! route('backend.finance.thirdparty.index') !!}">
                        <li  class="border-less" class="list-group-item" >
                            <h6 class="list-group-item-heading ng-binding">
                                <i class="icon fa fa-suitcase" aria-hidden="true"></i>
                                <large>&nbsp;&nbsp;Third Party Payers</large>
                            </h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding"> Manage a list of third party payers  </p> </li>

                    </a>

                </ul>

                &nbsp;
                

                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.erp.menu.view') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-truck"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.erp_menu')</large></h6>


                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.erp_menu_summary')</p> </li>

                </a>
            </ul>

                &nbsp;
                

                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.erms.menu.view') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-link  "> </i><large>&nbsp;&nbsp;ERMS intergration menu list</large></h6>


                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding"> All MAC and ERMS intergration functionality </p> </li>

                </a>
            </ul>

            </div>
        </div>



        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.receipt.employer') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.receive_contribution_interest')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.receive_contribution_interest_summary')</p> </li>
                    </a>




                </ul>





                {{--item2--PROCESS PAYMENT---}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.payment.profile') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-money"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.process_payment')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.process_payment_summary')</p> </li>
                    </a>

                </ul>





                {{--item 3-EXPORT TO ERP ---}}
                {{--&nbsp;<ul class="list-unstyled">--}}
                {{--<a href="#/offices">--}}
                {{--<li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-random"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.export_erp')</large></h6>--}}

                {{--<p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.export_erp_summary')</p> </li>--}}
                {{--</a>--}}

                {{--</ul>--}}

                {{--item 1--}}


                 &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.non.contributions') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-qrcode"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.non_contribution_controlno')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.non_contribution_controlno_summary')</p> </li>
                    </a>

                </ul>

                {{--Upload Contribution Before Electronic--}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.legacy_receipt.upload_manual_contribution') !!}">
                        <li  class="border-less" class="list-group-item" >
                            <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-file-excel-o" aria-hidden="true"></i>
                                <large>&nbsp;&nbsp;Upload Receipts Before Electronic</large>
                            </h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Upload contribution receipts created manually in the financial year 2015/16 and 2016/17</p> </li>
                    </a>
                </ul>
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.treasury.menu.view') !!}">
                        <li  class="border-less" class="list-group-item" >
                            <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-university" aria-hidden="true"></i>
                                <large>&nbsp;&nbsp;Treasury Integration Menu</large>
                            </h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">View employers(votes),employees,contributions and changes of payroll from Treasury</p> </li>
                    </a>
                </ul>




                {{--Bulk update paid invoice from ERP--}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.bulk_update_erp_paid_invoice') !!}">
                        <li  class="border-less" class="list-group-item" >
                            <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-file-excel-o" aria-hidden="true"></i>
                                <large>&nbsp;&nbsp;Bulk update paid invoice from ERP - Current Month</large>
                            </h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Bulk update paid invoices from ERP for the current month.</p> </li>
                    </a>
                </ul>

            </div>
        </div>


    </div>

    {{--</section>--}}

@stop


@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */
        $("#preview").click(function() {
        });
    });
</script>;

@endpush