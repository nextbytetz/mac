@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.receipt.dishonour.reason'), 'header_title' => trans('labels.backend.finance.header.receipt.dishonour.reason')])

@section('after-styles-end')

@endsection

@section('content')

    <div class="row">

        <div class="col-md-12" >


            {!! Form::model($legacy_receipt, ['route' => ['backend.finance.legacy_receipt.dishonour', $legacy_receipt->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}

            {{--rct no --}}
            <div class="row">
                <div class="col-md-9" >
                    <div class="element-form" >
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.receipt_no'):</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','rctno', null, ['class' => 'form-control', 'disabled' => true]) !!}

                            </div>
                        </div>
                    </div>
{{--chequeno--}}
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cheque_no'):</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','chequeno', null, ['class' => 'form-control', 'disabled' =>
                                true]) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>



            {{--dishonour reason--}}
            <div class="row">
                <div class="col-md-9" class="form-inline" >
                    <div class="element-form"  >
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12
                        col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.dishonour_reason'):</label><span class="required_asterik">*</span></div>
                        <div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">
                            <div class="form-group" id = "text_content">
                                {!! Form::textarea( 'dishonour_reason', null, [ 'id'=> 'dishonour_reason',
                                'class' =>'form-control',]) !!}
                                {!! $errors->first('dishonour_reason', '<span class="help-block label
                                label-danger">:message</span>') !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>&nbsp;</div>

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-6" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-3 col-lg-3 col-sm-12 col-md-3
                        text-xs-right"></div>
                        <div class="col-xl-9 col-lg-9 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">
                                <a  href="{!! route('backend.finance.legacy_receipt.edit', $legacy_receipt->id) !!}"  class="btn btn-primary site-btn cancel_button">@lang('buttons.general.cancel')</a>

                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}


        </div>
    </div>


    {{--</section>--}}

@stop


@push('after-script-end')

<script  type="text/javascript">
    $('#dishonour_reason').focus();

    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();

</script>

@endpush
