

<div class = "row" id="contribution-div">
    <div class="col-md-12"  >

        {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%', 'id' => 'contribution_id'], true) !!}

    </div>
</div>



@push('contribution-script-end')
{!! $dataTable->scripts() !!}
<script>
    $(function() {

    });

</script>
@endpush