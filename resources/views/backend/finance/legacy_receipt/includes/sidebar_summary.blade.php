

{{--sidebar legacy receipt summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            {{--Rctno--}}
            <tr>
                <td align="center"><h5><b><span class="light_dark_color" >@lang('labels.backend.finance.header.receipt.action.index'): {!! Form::label( 'rctno', $legacy_receipt->rctno_formatted, [ 'id'=> 'rctno'])
                        !!}</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table>
            {{--Payer--}}
            <tr>
                <td style="padding-left: 5px" width="140px">@lang('labels.backend.finance.receipt.received_from')</td>
                <td  height="20px"><b>{!! Form::label( 'payer', isset($legacy_receipt->payer) ? $legacy_receipt->payer : (($legacy_receipt->employer()->count()) ? $legacy_receipt->employer->name : ' '), [ 'id'=> 'payer']) !!}</b></td>
            </tr>
            {{--Amount--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.amount')</td>
                <td><b class="underline">{!! Form::label( 'amount', $legacy_receipt->amount_formatted, [ 'id'=> 'amount']) !!}</b></td>
            </tr>
            {{--Total Contribution--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.total_contribution')</td>
                <td><b class="underline">{!! Form::label( 'total_contribution', number_format($legacy_receipt->legacyReceiptCodes()->where(['fin_code_id' => 2])->sum('amount'), 2 , '.' , ',' ), [ 'id'=> 'total_contribution']) !!}</b></td>
            </tr>
            {{--Total Interest--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.total_interest')</td>
                <td><b class="underline">{!! Form::label( 'total_interest', number_format($legacy_receipt->legacyReceiptCodes()->where(['fin_code_id' => 1])->sum('amount'), 2 , '.' , ',' ), [ 'id'=> 'total_interest']) !!}</b></td>
            </tr>
            {{--Rct Date--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.receipt_date')</td>
                <td><b>{!! Form::label( 'rct_date', $legacy_receipt->rct_date_formatted, [ 'id'=> 'rct_date']) !!}</b></td>

            </tr>
            {{--Payment Type--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.payment_type')</td>
                <td><b>{!! Form::label( 'payment_type', ($legacy_receipt->paymentType()->count()) ? $legacy_receipt->paymentType->name : ' ', [ 'id'=> 'payment_type'])
                        !!}</b></td>

            </tr>
            {{--if payment type is cheque--}}
            @if ( $legacy_receipt->payment_type_id == 2)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.cheque_no')</td>
                    <td><b>{!! Form::label( 'chequeno', $legacy_receipt->chequeno, [ 'id'=> 'chequeno'])
                        !!}</b></td>
                </tr>
            @endif
            {{--cheque--}}
            @if ($legacy_receipt->payment_type_id == 2 || $legacy_receipt->payment_type_id == 5)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.cheque')</td>
                    <td><b>{!! Form::label( 'cheque', $legacy_receipt->chequeno, [ 'id'=> 'bank'])
                        !!}</b></td>
                </tr>
            @endif
            {{--Currency--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.currency')</td>
                <td><b>{!! Form::label( 'currency', ($legacy_receipt->currency_id) ? $legacy_receipt->currency->code : ' ', [ 'id'=> 'currency'])
                        !!}</b></td>
            </tr>

            {{--Description--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.payment_description')</td>
                <td><b>{!! Form::label( 'descr', $legacy_receipt->description, [ 'id'=> 'descr']) !!}</b></td>
            </tr>
            {{--User Name--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.captured_by')</td>
                <td><b>{!! Form::label( 'user_name', ($legacy_receipt->user()->count()) ? $legacy_receipt->user->username : ' ', [ 'id'=> 'user_name'])
                        !!}</b></td>
            </tr>
            {{--Created At--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.captured_date')</td>
                <td><b>{!! Form::label( 'created_at', $legacy_receipt->capture_date_formatted, [ 'id'=> 'created_at'])
                        !!}</b></td>
            </tr>

        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush