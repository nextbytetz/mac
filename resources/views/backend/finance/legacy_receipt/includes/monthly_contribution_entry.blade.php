<?php
$color = [
    0 => 'blue',
    1 => 'grey',
];
?>
@for ($x = 1; $x <= $selected_entries; $x++)
<?php
    $i = str_random(5);
?>
<!-- start : Single contribution line entry -->
<div class="fileld-layout" id="contribution_entry_group{!! $i !!}" style="padding-left:20px; border-left: 2px solid {!! $color[$x % 2] !!};">
    <div class="form-group">
        <legend></legend><br/>
        <div class="form-inline">
            <span>
                Month:
            </span>
                                    <span>
                                        {!!  Form::selectMonth('contribution_month' . $i, null, ['class' => 'form-control search-select contribution_month','style'=>'width:100px', 'placeholder' => '', 'id' => 'contribution_month' . $i]) !!}
                                    </span>
            <span>
                Year:
            </span>
            <span>
                                        {!!  Form::selectRange('contribution_year' . $i,  2015, Carbon\Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1)->format('Y') , null, ['class' => 'form-control search-select contribution_year','style'=>'width:100px', 'placeholder' => '', 'id' => 'contribution_year' . $i]) !!}&nbsp;&nbsp;
                                    </span>
            <!-- start : Choice for contribution -->
            <span class="checkbox-squared">
                                        {!! Form::checkbox('contribution_select' . $i, '1', false, ['id' => 'contribution_select' . $i, 'class' => 'contribution_select']) !!}
                <label for="contribution_select{!! $i !!}"></label>
                                        <span><b>@lang('labels.general.contribution')</b>&nbsp;&nbsp;</span>
                                    </span>
            <!-- end : Choice for contribution -->
            <!--------- start : Choice for interest -------------->

            {{--<span class="checkbox-squared">--}}
                                        {{--{!! Form::checkbox('interest_select' . $i, '1', false, ['id' => 'interest_select' . $i, 'class' => 'interest_select']) !!}--}}
                {{--<label for="interest_select{!! $i !!}"></label>--}}
                                        {{--<span><b>@lang('labels.backend.finance.receipt.interest')</b>&nbsp;&nbsp;&nbsp;&nbsp;</span>--}}
                                    {{--</span>--}}

            <span id="no_interest{!! $i !!}" class="underline" style="display : none; color : #8b2306;">
                @lang('labels.backend.finance.receipt.no_interest')
            </span>
            <span class="help-block"></span>
            <!-- end : Choice for interest -->
            <!-- start : Close button for this contribution month -->
            <span class="pull-right">
                                        <a href="#" id="{!! $i !!}" class="remove_contribution_entry"><i class="icon fa fa-2x fa-remove" aria-hidden="true" style="color:darkred;"></i></a>
                                    </span>
            <!-- end : Close button for this contribution month -->
        </div>
        {!! $errors->first('choose_receivable', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
    <!-- start : Group Box for Contribution  -->
    <div id="contribution_entry{!! $i !!}" class="contribution_entry" style="display:none;">
        <div class="form-group">
            <div class="form-inline">
                <!-- start : Entry for contribution paid amount -->
                <span>
                                        <label class="required">  @lang('labels.backend.finance.receipt.paid_amount') :</label>&nbsp;&nbsp;
                                    </span>
                <span>
                                        {!! Form::text('contribution_amount' . $i, null, ['class' => 'form-control number money contribution_amount', 'style' => "border-radius:3px;width:20%;", 'id' => 'contribution_amount' . $i]) !!}&nbsp;&nbsp;
                                    </span>
                <!-- end : Entry for contribution paid amount -->
                <!-- start : Entry for contribution member count -->
                <span>
                                        <label> @lang('labels.backend.finance.receipt.member_count') :</label>&nbsp;&nbsp;
                                    </span>
                <span>
                                        {!! Form::text('member_count' . $i, null, ['class' => 'form-control number', 'style' => "border-radius:3px;width:10%;", 'id' => 'member_count' . $i]) !!}&nbsp;&nbsp;
                                    </span>
                <!-- end : Entry for contribution member count -->
                <!-- start : Booked Amount -->
                <span>
                                        <label>  @lang('labels.backend.finance.receipt.booked_amount') :</label>&nbsp;&nbsp;
                                    </span>
                <span>
                                        {!! Form::text('booked_amount' . $i, null, ['class' => 'form-control', 'style' => "border-radius:3px;width:20%;", 'readonly' => 'readonly', 'id' => 'booked_amount' . $i]) !!}&nbsp;&nbsp;
                                    </span>
                <span class="help-block"></span>
                <!-- end : Booked Amount -->
            </div>
        </div>
    </div>
    <!-- end : Group Box for Contribution  -->
    <!-- start : Group Box for Interest  -->
    <div id="interest_entry{!! $i !!}" class="interest_entry" style="display : none;">
        <div class="form-group">
            <div class="form-inline">
                <!-- start : Interest Paid -->
                <span>
                                            <label class="required"> @lang('labels.backend.finance.receipt.paid_interest') :</label>&nbsp;&nbsp;
                                        </span>
                <span>
                                            {!! Form::text('interest_amount' . $i, null, ['class' => 'form-control number money interest_amount', 'style' => "border-radius:3px;width:20%;", 'id' => 'interest_amount' . $i]) !!}&nbsp;&nbsp;
                                        </span>
                <!-- end : Interest Paid -->
                <!-- start : Interest Due -->
                <span>
                                            <label>  @lang('labels.backend.finance.receipt.due_interest') :</label>&nbsp;&nbsp;
                                        </span>
                <span>
                                            {!! Form::text('interest_due' . $i, null, ['class' => 'form-control money', 'style' => "border-radius:3px;width:20%;", 'readonly' => 'readonly', 'id' => 'interest_due' . $i]) !!}&nbsp;&nbsp;
                                        </span>
                <span class="help-block"></span>
                <!-- end : Interest Due -->
            </div>
        </div>
    </div>
    <!-- end : Group Box for Interest  -->
</div>
<!-- end : Single contribution line entry -->

@endfor