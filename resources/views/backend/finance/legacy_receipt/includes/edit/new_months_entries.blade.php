{{--if there is no receipt codes--}}

@if($legacy_receipt->legacyReceiptCodes()->count() == 0)
    <div class="row">
        <div class="col-md-12">
            <div class="receivables">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fileld-layout">
                            <div class="form-group">
                                <div class="form-inline">
                                        <span>
                                            <label class="required">@lang('labels.backend.finance.receipt.pay_months')</label>&nbsp;&nbsp;
                                        </span>
                                    <span>
                                        {!! Form::text('pay_months', null, ['class' => 'form-control number', 'style' => "border-radius:3px;width:5%;", 'id' => 'pay_months']) !!}&nbsp;&nbsp;
                                    </span>
                                    <span>
                                            <a href="#" id="add_contribution_entry"><i class="icon fa fa-2x fa-plus" aria-hidden="true" style="color:darkblue"></i></a>
                                        </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div id="contribution_entries">
                            {{--@include('backend/finance/receipt/includes/monthly_contribution_entry')--}}
                        </div>
                        <br/>
                        {{--<div id="contribution_summary">--}}
                        {{--<div class="fileld-layout">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="form-inline">--}}
                                        {{--<span>--}}
                                            {{--@lang('labels.backend.finance.receipt.total_entry')&nbsp;:&nbsp;&nbsp;--}}
                                        {{--</span>--}}
                                    {{--<span id = "total_entry" class="underline" style="font-weight: bold;">--}}
                                            {{--0--}}
                                        {{--</span>--}}
                                    {{--<span>--}}
                                            {{--,&nbsp;@lang('labels.backend.finance.receipt.bank_amount') &nbsp;:&nbsp;&nbsp;--}}
                                        {{--</span>--}}
                                    {{--<span id="bank_amount" class="underline" style="font-weight: bold;">--}}
                                            {{--0--}}
                                        {{--</span>--}}
                                    {{--<span id="contribution_summary_icon">--}}

                                        {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<hr/>--}}
                        {{--<div class="pull-right">--}}
                            {{--<input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.create')" />--}}
                        {{--</div>--}}

                    </div>
                </div>

            </div>
        </div>
    </div>
@endif




