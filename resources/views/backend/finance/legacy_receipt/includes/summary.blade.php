{{--RECEIPT SUMMARY--}}
{{--Receipt no--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.receipt_no'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','rctno', $legacy_receipt->rctno, ['class' => 'form-control', 'disabled' => true]) !!}
                    {!! Form::input('hidden', 'rctno', $legacy_receipt->rctno, ['class' => 'form-control']) !!}

                </div>
            </div>
        </div>

        {{--currency--}}
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.currency'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'currency_name', ($legacy_receipt->currency()->count()) ? $legacy_receipt->currency->code : ' ', ['class' => 'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--AMOUNT--}}
<div class="row">
    <div class="col-md-9" class="form-inline">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.amount'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'amount', $legacy_receipt->amount_formatted,
                    ['class' =>
                    'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
        {{--BANK--}}
        <div class="element-form">
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.bank'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'bank_name', ($legacy_receipt->bank()->count()) ? $legacy_receipt->bank->name : ' ', ['class' => 'form-control', 'disabled' => true]) !!}


                </div>
            </div>
        </div>
    </div>
</div>


{{--PAYMENT TYPE--}}
<div class="row">
    <div class="col-md-9" class="form-inline">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.payment_type'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'payment_type', ($legacy_receipt->paymentType()->count()) ?  $legacy_receipt->paymentType->name : ' ', ['class' => 'form-control', 'disabled' => true]) !!}
                </div>
            </div>
        </div>
        {{--Checque no--}}
        {{--if payment type is cheque--}}
        @if ($legacy_receipt->payment_type_id == 2)
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cheque_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'chequeno', $legacy_receipt->chequeno, ['class' => 'form-control','disabled' => true]) !!}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

{{--Receipt date No--}}
<div class="row">
    <div class="col-md-9" class="form-inline" >
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.receipt_date'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'rct_date', $legacy_receipt->rct_date_formatted, ['class' => 'form-control','disabled' => true]) !!}
                </div>
            </div>
        </div>
        {{--Captured Date--}}
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.captured_date'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'created_at', $legacy_receipt->capture_date_formatted, ['class' => 'form-control','disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--RECEIVED FROM--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.received_from'):</label></div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-xs-10">
                <div class="form-group">
                    {!! Form::input('text', 'payer', $legacy_receipt->payer, ['class' => 'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--Payment Description--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.payment_description'):</label></div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-xs-10">
                <div class="form-group">
                    {!! Form::textarea('description', $legacy_receipt->isPayFor(), ['class' => 'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
    </div>
</div>
{{--captured by--}}
<div class="row">
    <div class="col-md-9" class="form-inline" >
        <div class="element-form"  >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.captured_by'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input('text', 'capture_by',($legacy_receipt->user()->count()) ? $legacy_receipt->user->username : ' ', ['class' => 'form-control','disabled' =>
                    true]) !!}
                </div>
            </div>
        </div>
        {{--CANCELLED BY--}}
        {{--Status if iscancelled--}}
        @if ($legacy_receipt->iscancelled == 1)
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cancelled_by'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'cancelled_by',($legacy_receipt->cancelUser()->count()) ? $legacy_receipt->cancelUser->username : ' ', ['class' => 'form-control','disabled' =>
                        true]) !!}
                    </div>
                </div>
            </div>
        @endif


        {{--DSIHONOURED BY--}}
        {{--Status if isdishonoured--}}
        {{--@if ($legacy_receipt->isdishonoured == 1)--}}
            {{--<div class="element-form"  >--}}
                {{--<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.dishonoured_by'):</label></div>--}}
                {{--<div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">--}}
                    {{--<div class="form-group">--}}
                        {{--{!! Form::input('text', 'dishonoured_by',$legacy_receipt->dishonouredCheques->last()->dishonourUser->username , ['class' => 'form-control','disabled' =>--}}
                        {{--true]) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}

    </div>
</div>

{{--Status if iscancelled--}}
    @if ($legacy_receipt->iscancelled == 1)
        {{--cancel date --}}
        <div class="row">
            <div class="col-md-9" class="form-inline" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cancelled_date'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input('text', 'cancel_date', $legacy_receipt->cancel_date_formatted, ['class' => 'form-control','disabled' => true]) !!}
                        </div>
                    </div>
                </div>
                {{--cancelled image--}}
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label></div>
                    <div  class="col-md-3"  >
                        <img class="right"height="40px"  width = "120px" src="{!! asset_url()
                        !!}/global/image/cancelled.png"
                             alt="images">
                    </div>
                </div>
            </div>
        </div>
        {{--cancel reason --}}
        <div class="row">
            <div class="col-md-9" class="form-inline" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.cancel_reason'):</label></div>
                    <div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'cancel_reason', null, [ 'id'=> 'cancel_reason',
                            'class' =>'form-control', 'disabled' => true ]) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endif


{{--WHEN RECEIPT IS DISHONOURED--}}
@if ($legacy_receipt->isdishonoured == 1)
    {{--cancel date --}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.dishonoured_date'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'dishonour_date',
                        $legacy_receipt->dishonour_date_formatted,
                        ['class' => 'form-control','disabled' => true]) !!}
                    </div>
                </div>
            </div>
            {{--cancelled image--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label></div>
                <div  class="col-md-3"  >
                    <img class="right"height="65px"  width = "120px" src="{!! asset_url() !!}/global/image/dishonouredcheque.jpg"
                         alt="images">
                </div>
            </div>
        </div>
    </div>
    {{--dishonour reason --}}
    {{--<div class="row">--}}
        {{--<div class="col-md-9" class="form-inline" >--}}
            {{--<div class="element-form"  >--}}
                {{--<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.dishonour_reason'):</label></div>--}}
                {{--<div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">--}}
                    {{--<div class="form-group" id = "text_content_dishonour">--}}
                        {{--{!! Form::textarea( 'dishonour_reason', $legacy_receipt->dishonouredCheques->last()->dishonour_reason, [ 'id'=>--}}
                        {{--'dishonour_reason',--}}
                        {{--'class' =>'form-control', 'disabled' => true ]) !!}--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endif
