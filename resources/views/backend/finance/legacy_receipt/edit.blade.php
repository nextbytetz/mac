@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_contribution_months'), 'header_title' => trans('labels.backend.member.edit_contribution_months')])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

    <div class = "row">
        {{--{!! Form::model($receipt, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        <div class="col-md-12 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Receipt Header detail -->
                <span>{!! $legacy_receipt->complete_status_label !!} </span>
                <span>{!! $legacy_receipt->isverified_label !!}</span>
                <strong> {!! Form::label( 'name', $legacy_receipt->rctno, [ 'id'=> 'rctno']) !!}</strong>
                <small >
                    Payer: {!! Form::label( 'payer', ($legacy_receipt->payer) ? $legacy_receipt->payer : (($legacy_receipt->employer()->count()) ? $legacy_receipt->employer->name : ' '), [ 'id'=> 'payer']) !!}
                </small>

            </h5>
        </div>
    </div>

    <br/>
    {!! Form::open(['route' => ['backend.finance.legacy_receipt.update_months', $legacy_receipt->id], 'name' => 'edit_contribution_months']) !!}
    {!! Form::hidden('employer_id', $legacy_receipt->employer->id) !!}
    {!! Form::hidden('legacy_receipt_id', $legacy_receipt->id) !!}

    <div class="row">
        <div class="col-md-12">
            <div class="receipt_details">
                <div class="row">
                    <div class="col-md-6">
                        <div class="field-layout">
                            <div class="form-group">
                                <label class="required" for="receipt_no">@lang('labels.backend.finance.receipt.receipt_no')</label>
                                <div class="input-group">
                                    {!! Form::text('rctno', $legacy_receipt->rctno, ['class' => 'form-control', 'style' => "border-radius:3px;", 'id' => 'receipt_no']) !!}

                                    <span class="input-group-addon">
                                        <i class="icon fa fa-list-alt" aria-hidden="true"></i>
                                    </span>

                                </div>

                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="field-layout">
                            <div class="form-group">
                                <label class="required" for="receipt_amount">@lang('labels.backend.table.amount')</label>
                                <div class="input-group">
                                    {!! Form::text('amount', $legacy_receipt->amount, ['class' => 'form-control money', 'style' => "border-radius:3px;", 'id' => 'receipt_amount']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span>{{ trans('labels.backend.finance.receipt.bank_amount') }}</span>
                                </span>
                            </div>
                        </div>
                        <div class="filed-layout">
                            <label class="required">@lang('labels.backend.finance.receipt.receipt_date')</label>
                            <div class="form-group">
                                <div class="form-inline">
                                    <span>
                                        {!!  Form::selectRange('receipt_date',1 , 31, $payment_date->format('j'), ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                                    <span>
                                        {!!  Form::selectMonth('receipt_month',$payment_date->format('n'), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                                    </span>
                                    <span>
                                        {!!  Form::selectRange('receipt_year', Carbon\Carbon::now()->format('Y'), 2015 , $payment_date->format('Y'), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                                    </span>
                                    <span>
                                        {!! Form::hidden('rct_date') !!}
                                        {!! Form::hidden('today_date', \Carbon\Carbon::now()->format('Y-n-j')) !!}
                                        {!! Form::hidden('start_date', "2017-7-1") !!}
                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        {{--date created manually--}}

                        <div class="filed-layout">
                            <label class="required">@lang('labels.backend.finance.receipt.captured_date')</label>
                            <div class="form-group">
                                <div class="form-inline">
                                    <span>
                                        {!!  Form::selectRange('capture_date',1 , 31,  $receipt_date->format('j'), ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                                    <span>
                                        {!!  Form::selectMonth('capture_month',$receipt_date->format('n'), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                                    </span>
                                    <span>
                                        {!!  Form::selectRange('capture_year', Carbon\Carbon::now()->format('Y'), 2015 , $receipt_date->format('Y'), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                                    </span>
                                    <span>
                                        {!! Form::hidden('capture_date') !!}

                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.finance.currency.index')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('currency_id', $currencies, $legacy_receipt->currency_id, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">@lang('labels.backend.finance.receipt.payment_type')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select('payment_type_id', $payment_types, $legacy_receipt->payment_type_id, ['class' => 'search-select', 'style' => 'width:60%', 'placeholder' => '', 'id'
                                    => 'payment_type_id']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="fileld-layout" id="chequeno_entry" >
                            <label class="required">@lang('labels.backend.finance.receipt.cheque')</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('chequeno', $legacy_receipt->chequeno, ['class' => 'form-control', 'style' => "border-radius:3px;" ]) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>



    <br>

    <div class="row">
        <div class="col-md-12">
            <?php
            $color = [
                0 => 'blue',
                1 => 'grey',
            ];
            $x = 1;
            $entry_amount = 0;

            $legacy_receipt_codes = $legacy_receipt->legacyReceiptCodes;
            ?>
            @if($legacy_receipt->legacyReceiptCodes->count() > 0)
                    {!! Form::hidden('isnew', 0) !!}
                @foreach ($legacy_receipt_codes as $code)
                    <div class="fileld-layout" id="contribution_entry_group{!! $code->id !!}" style="padding-left:20px; border-left: 2px solid {!! $color[$x % 2] !!};">
                        <div class="form-group">
                            <legend></legend><br/>
                            <div class="form-inline">
                                {{--Contribution Month--}}
                                <span>
                Month:
            </span>
                                <span>
                                        {!!  Form::selectMonth('contribution_month' . $code->id, \Carbon\Carbon::parse($code->contrib_month)->format("n"), ['class' => 'form-control contribution_month','style'=>'width:100px;border-radius:6px;', 'placeholder' => '', 'id' => 'contribution_month' . $code->id]) !!}
                                    </span>
                                <span>
                Year:
            </span>
                                <span>
                                        {!!  Form::selectRange('contribution_year' . $code->id, 2015, Carbon\Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1)->format('Y'), \Carbon\Carbon::parse($code->contrib_month)->format("Y"), ['class' => 'form-control contribution_year','style'=>'width:100px;border-radius:6px;', 'placeholder' => '', 'id' => 'contribution_year' . $code->id]) !!}&nbsp;&nbsp;
                                    </span>
                                @if ($code->fin_code_id == 2)
                                <!-- start : Entry for contribution paid amount -->
                                    <span>
                                        <label class="required">  @lang('labels.backend.finance.receipt.paid_amount') :</label>&nbsp;&nbsp;
                                    </span>
                                    <span>
                                        {!! Form::text('contribution_amount' . $code->id, $code->amount, ['class' => 'form-control number money contribution_amount', 'style' => "border-radius:3px;width:20%;", 'id' => 'contribution_amount' . $code->id]) !!}&nbsp;&nbsp;
                                    </span>
                                    <!-- end : Entry for contribution paid amount -->
                                    <!-- start : Entry for contribution member count -->
                                    <span>
                                        <label> @lang('labels.backend.finance.receipt.member_count') :</label>&nbsp;&nbsp;
                                    </span>
                                    <span>
                                        {!! Form::text('member_count' . $code->id, $code->member_count, ['class' => 'form-control number', 'style' => "border-radius:3px;width:10%;", 'id' => 'member_count' . $code->id]) !!}&nbsp;&nbsp;
                                    </span>
                                    <!-- end : Entry for contribution member count -->
                                @endif
                                @if ($code->fin_code_id == 1)
                                <!-- start : Interest Paid -->
                                    <span>
                                            <label class="required"> @lang('labels.backend.finance.receipt.paid_interest') :</label>&nbsp;&nbsp;
                                        </span>
                                    <span>
                                            {!! Form::text('interest_amount' . $code->id, $code->amount, ['class' => 'form-control number money interest_amount', 'style' => "border-radius:3px;width:20%;", 'id' => 'interest_amount' . $code->id]) !!}&nbsp;&nbsp;
                                        </span>
                                    <!-- end : Interest Paid -->
                                @endif
                            </div>
                        </div>
                    </div>
                    <?php
                    $x++;
                    $entry_amount += $code->amount;
                    ?>
                @endforeach
            @else
                    {!! Form::hidden('isnew', 1) !!}
                @include('backend/finance/legacy_receipt/includes/edit/new_months_entries')
            @endif
            <hr/>
            {{--<div id="contribution_summary">--}}
            <div class="fileld-layout">
                <div class="form-group">
                    <div class="form-inline">
                                        <span>
                                            @lang('labels.backend.finance.receipt.total_entry')&nbsp;:&nbsp;&nbsp;
                                        </span>
                        <span id = "total_entry" class="underline" style="font-weight: bold;">
                                            0
                                        </span>
                        <span>
                                            ,&nbsp;@lang('labels.backend.finance.receipt.bank_amount') &nbsp;:&nbsp;&nbsp;
                                        </span>
                        <span id="bank_amount" class="underline" style="font-weight: bold;">
                                            0
                                        </span>
                        <span id="contribution_summary_icon">

                                        </span>
                    </div>
                </div>
            </div>
            {{--</div>--}}
            <hr/>
            <div class="pull-right">
                {!! link_to_route('backend.finance.legacy_receipt.profile', trans('buttons.general.cancel'), [$legacy_receipt->id], ['class' => 'btn btn-danger btn-sm cancel_button']) !!}
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.update')" />
            </div>
        </div>
    </div>







    {!! Form::close() !!}
@endsection

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}

    <script>
        var employer_id = parseInt("{!! $legacy_receipt->employer_id !!}");
        var month_threshold = parseInt("{!! $month_threshold !!}");
        var year_threshold = parseInt("{!! $year_threshold !!}");
        var $minimum_contribution = parseInt("{!! $minimum_contribution !!}");
        var receipt_amount = {!! $legacy_receipt->amount !!};
        var entry_amount = {!! $entry_amount !!};
        var total_entries = 0;
        $(function(){
            $(".search-select").select2({});
            payment_type_option('payment_type_id', 'chequeno_entry');

            //Todo - reset to unable edit months
            $(".contribution_year").prop('disabled', false);
            $(".contribution_month").prop('disabled', false);
            /*$(".search-select").select2({});*/
            //$(".contribution_year").select2({});
            //$(".contribution_month").select2({});
            $('#bank_amount').text(addCommas(receipt_amount));
            $('#total_entry').text(addCommas(receipt_amount));
            check_amount_match(receipt_amount, entry_amount);
            /* start : mask all money input */
            $('.money').maskMoney({
                precision : 2,
                affixesStay : false
            });
            /* end : mask all money input */


            /* start : Payment type change event */
            $("#payment_type_id").on("change", function (e) {
                payment_type_option('payment_type_id', 'chequeno_entry');
            });
            /* end : Payment type change event */


            /* start : handle bank amount textboxes change event and sum all the entered amounts */
            $('body').off('keyup', '#receipt_amount').on('keyup', '#receipt_amount', function(e) {
                var value = parseFloat($(this).val().split(",").join(""))
                $('#bank_amount').text(addCommas(value));
                receipt_amount = value;
                check_amount_match(receipt_amount, entry_amount);
            });

            /* start : handle amount textboxes change event and sum all the entered amounts */
            $('body').off('keyup', '.contribution_amount').on('keyup', '.contribution_amount', function(e) {
                var value;
                var sum = 0;
                $('.contribution_amount').each(function() {
                    value = $(this).val();
                    if (value != '') {
                        sum += parseFloat(value.split(",").join(""));
                    }
                });
                $('.interest_amount').each(function() {
                    value = $(this).val();
                    if (value != '') {
                        sum += parseFloat(value.split(",").join(""));
                    }
                });
                entry_amount = sum;
                $('#total_entry').text(addCommas(entry_amount));
                check_amount_match(receipt_amount, entry_amount);
            });
            $('body').off('keyup', '.interest_amount').on('keyup', '.interest_amount', function(e) {
                var value;
                var sum = 0;
                $('.interest_amount').each(function() {
                    value = $(this).val();
                    if (value != '') {
                        sum += parseFloat(value.split(",").join(""));
                    }
                });
                $('.contribution_amount').each(function() {
                    value = $(this).val();
                    if (value != '') {
                        sum += parseFloat(value.split(",").join(""));
                    }
                });
                entry_amount = sum;
                $('#total_entry').text(addCommas(entry_amount));
                check_amount_match(receipt_amount, entry_amount);
            });
            /* end : handle amount textboxes leave event and sum all the entered amounts */
            /* start : ensure only numbers are input on monetary boxes */
            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e)
            });
            /* end : ensure only numbers are input on monetary boxes */



            /* start: on click contribution entry remove link */
            $('body').on('click', 'a.remove_contribution_entry', function (e) {
                e.preventDefault();
                var id = this.id;
                var amount = 0;
                total_entries -= 1;
                if ($('#contribution_select' + id).is(':checked')) {
                    var contribution_amount = $("#contribution_amount" + id).val();
                    if (contribution_amount != '') {
                        amount += parseFloat(contribution_amount.split(",").join(""));
                    }
                }
                if ($('#interest_select' + id).is(':checked')) {
                    var interest_amount = $("#interest_amount" + id).val();
                    if (interest_amount != '') {
                        amount += parseFloat(interest_amount.split(",").join(""));
                    }
                }

                entry_amount -= amount;
                $('#total_entry').text(addCommas(entry_amount));
                check_amount_match(receipt_amount, entry_amount);
                $("#contribution_entry_group" + id).remove();
            });
            /* end: on click contribution entry remove link */
            /* start : ensure only numbers are input on monetary boxes */
            $(".number").keydown(function (e) {
                number_only(e)
            });
            /* end : ensure only numbers are input on monetary boxes */
            /* start : mask all money input */
            $('.money').maskMoney({
                precision : 2,
                affixesStay : false
            });
            /* end : mask all money input */
            /* start : handle bank amount textboxes change event and sum all the entered amounts */
            $('body').off('keyup', '#receipt_amount').on('keyup', '#receipt_amount', function(e) {
                var value = parseFloat($(this).val().split(",").join(""))
                $('#bank_amount').text(addCommas(value));
                receipt_amount = value;
                check_amount_match(receipt_amount, entry_amount);
            });
            /* end : handle bank amount textboxes leave event and sum all the entered amounts */

            /* start: Submitting Form and perform validation on the server side */
            $('body').on('submit', 'form[name=edit_contribution_months]', function (e) {
                e.preventDefault();
                var form = this;

                var $receipt_date = $("select[name=receipt_date]").val();
                var $receipt_month = $("select[name=receipt_month]").val();
                var $receipt_year = $("select[name=receipt_year]").val();
                if ($receipt_date && $receipt_month && $receipt_year) {
                    $("input[name=rct_date]").val($receipt_year + '-' + $receipt_month + '-' + $receipt_date);
                } else {
                    $("input[name=rct_date]").val("");
                }

//            capture date
                var $capture_date = $("select[name=capture_date]").val();
                var $capture_month = $("select[name=capture_month]").val();
                var $capture_year = $("select[name=capture_year]").val();
                if ($capture_date && $capture_month && $capture_year) {
                    $("input[name=capture_date]").val($capture_year + '-' + $capture_month + '-' + $capture_date);
                } else {
                    $("input[name=capture_date]").val("");
                }



                //alert($form);
                //$form.submit();
                var $data = $(form).serialize();
                /* start: remove any printed error message in the input controls */
                $(form).find(':input').each(function () {
                    var $name = $(this).attr('name');
                    $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                $.ajax({
                    data : $data,
                    dataType : "json",
                    method : "POST",
                    url : $(form).attr("action"),
                    beforeSend : function (e) {
                        if (receipt_amount < $minimum_contribution) {
                            amaran_notify("{!! trans("exceptions.backend.finance.receipts.small_amount", ['amount' => $minimum_contribution]) !!}", "{!! trans("labels.general.error") !!}", "error");
                            return false;
                        }
                        if (!allow_match(receipt_amount, entry_amount)) {
                            amaran_notify("{!! trans("exceptions.backend.finance.receipts.not_tallied") !!}", "{!! trans("labels.general.error") !!}", "error");
                            return false;
                        }
                        $(".btn-submit").prop('disabled', true);
                    },
                    success : function (data) {
                        if (data.success) {
                            /* window.location = base_url + "/finance/receipt/" + data.id + "/edit"; */
                            document.location.replace(base_url + "/finance/legacy_receipt/profile/" + data.id);
                            // document.location.replace(base_url + "/finance/legacy_receipt/" + data.id + "/edit");
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function(index, value) {
                            $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        });
                    },
                }).done(function() {

                }).fail(function() {
                    $(".btn-submit").prop('disabled', false);
                }).always(function() {
                    $(".btn-submit").prop('disabled', false);
                });
            });
            /* end: Submitting Form and perfom validation on the server side */

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
        /* end : ensure only numbers are input on monetary boxes */
        /* start : check if the receipt amount and the total contribution entries matches */
        function check_amount_match($receipt_amount, $entry_amount) {
            var $diff = $receipt_amount - $entry_amount;
            switch(true) {
                case ($diff < 0):
                    $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:red;'><i class='icon fa fa-2x fa-close' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.less")</span>");
                    break;
                case ($diff > 0.1):
                    $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:#ffd303;'><i class='icon fa fa-2x fa-warning' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.greater")</span>");
                    break;
                case ($diff >= 0 && $diff <= 0.1):
                    $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:green;'><i class='icon fa fa-2x fa-check-square-o' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.matched")</span>");
                    break;
            }
        }
        /* end : check if the receipt amount and the total contribution entries matches */
        function amaran_notify(message, title, type) {
            var icon;
            if(typeof type === "undefined") {
                type = 'success';
            }
            switch(type) {
                case "success":
                    icon = "fa fa-check-square-o";
                    break;
                case "warning":
                    icon = "icon fa fa-warning";
                    break;
                case "error":
                    icon = "icon fa fa-ban";
                    break;
                default:
                    icon = "fa fa-check-square-o";
            }
            $.amaran({
                'theme'     :'awesome ' + type,
                'content'   :{
                    title:title,
                    message:message,
                    info:'',
                    icon:icon,
                },
                'position'  :'bottom left',
                'outEffect' :'slideBottom',
                'inEffect'  :'slideLeft'
            });
        }

        /* start : start allow amount match */
        function allow_match($receipt_amount, $entry_amount) {
            var $diff = $receipt_amount - $entry_amount;
            var $return = false;
            switch(true) {
                case ($diff < 0):
                    $return = false
                    break;
                case ($diff > 0.1):
                    $return = false;
                    break;
                case ($diff >= 0 && $diff <= 0.1):
                    $return = true;
                    break;
            }
            return $return;
        }
        /* end : start allow amount match */


        function payment_type_option($payment_type, chequeno_entry) {

            var choice = $("#"+ $payment_type).val();
            switch (choice) {
                case '2':
                    $("#" + chequeno_entry).show();
                    break;
                case '5':
                    $("#"+ chequeno_entry).show();
                    break;
                default:
                    $("#" + chequeno_entry).hide();

            }
        }






        /*New - Added*/

        autosize($("textarea.autosize"));
        var pay_monthes_textbox = $("#pay_months");
        /* start : on click contribution entry add link */
        $('body').on('click', 'a#add_contribution_entry', function (e) {
            e.preventDefault();
            selected_entries = pay_monthes_textbox.val();
            if (validate_entry(selected_entries, pay_monthes_textbox)) {
                if (!add_contribution_entry_control(parseInt(selected_entries))) {
                    amaran_notify("{!! trans("exceptions.backend.finance.receipts.max_entries", ['total' => $max_entries]) !!}", "{!! trans("labels.general.warning") !!}", "warning");
                    return false;
                }
                /* start : Retrieve Contribution entries from the server  */
                $.post("{!! route('backend.finance.legacy_receipt.contribution_entries') !!}", {'selected_entries' : selected_entries}, function( data ) {
                    $( data ).prependTo( "#contribution_entries" );
                }, "html").done(function () {
                    $(".search-select").select2({});
                    /* start : check selection change event of the drop down months and year  */
                    $('body').off('change', '.contribution_month').on('change', '.contribution_month', function () {
                        var name = $(this).attr('name');
                        var month = $(this).val();
                        var special = name.split("contribution_month").join("");
                        var year = $("#contribution_year" + special).val();
                        if (year == "" || month == "") {
                            return false;
                        }

                        $(".contribution_month").not(this).each(function() {

                            var other_name = $(this).attr('name');
                            var other_month = $(this).val();
                            var other_special = other_name.split("contribution_month").join("");
                            var other_year = $("#contribution_year" + other_special).val();
                            if ((year == other_year) && (month == other_month)) {
                                /*$("#contribution_month" + special).select2("trigger", "select", {
                                 data: { id: "" }
                                 });
                                 $("#contribution_year" + special).select2("trigger", "select", {
                                 data: { id: "" }
                                 });*/
                                $("#contribution_month" + special).val(null).trigger('change.select2');
                                $("#contribution_year" + special).val(null).trigger('change.select2');
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.duplicate_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                $('#contribution_select' + special).attr('checked', false);
                                $('#contribution_select' + special).trigger('change');
                                return false;
                            }
                        });
                        if ((month > month_threshold) && (year >= year_threshold)) {
                            /*$("#contribution_month" + special).select2("trigger", "select", {
                             data: { id: null }
                             });
                             $("#contribution_year" + special).select2("trigger", "select", {
                             data: { id: null }
                             });*/
                            $("#contribution_month" + special).val(null).trigger('change.select2');
                            $("#contribution_year" + special).val(null).trigger('change.select2');
                            amaran_notify("{!! trans("exceptions.backend.finance.receipts.date_threshold", ['max' => sysdefs()->data()->total_allowed_contrib_months ]) !!}", "{!! trans("labels.general.warning") !!}", "warning");
                            $('#contribution_select' + special).attr('checked', false);
                            $('#contribution_select' + special).trigger('change');
                            return false;
                        }
                        booked_amount(month, year, $("#booked_amount" + special));
                        interest_checkbox_state(special);
                    });
                    $('body').off('change', '.contribution_year').on('change', '.contribution_year', function () {
                        var name = $(this).attr('name');
                        var year = $(this).val();
                        var special = name.split("contribution_year").join("");
                        var month = $("#contribution_month" + special).val();

                        if (year == "" || month == "") {
                            return false;
                        }
                        $(".contribution_year").not(this).each(function() {
                            var other_name = $(this).attr('name');
                            var other_year = $(this).val();
                            var other_special = other_name.split("contribution_year").join("");
                            var other_month = $("#contribution_month" + other_special).val();
                            if ((year == other_year) && (month == other_month)) {
                                /*$("#contribution_month" + special).select2("trigger", "select", {
                                 data: { id: null }
                                 });
                                 $("#contribution_year" + special).select2("trigger", "select", {
                                 data: { id: null }
                                 });*/
                                $("#contribution_month" + special).val(null).trigger('change.select2');
                                $("#contribution_year" + special).val(null).trigger('change.select2');
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.duplicate_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                $('#contribution_select' + special).attr('checked', false);
                                $('#contribution_select' + special).trigger('change');
                                return false;
                            }
                        });
                        if ((month > month_threshold) && (year >= year_threshold)) {
                            /*$("#contribution_month" + special).select2("trigger", "select", {
                             data: { id: null }
                             });
                             $("#contribution_year" + special).select2("trigger", "select", {
                             data: { id: null }
                             });*/
                            $("#contribution_month" + special).val(null).trigger('change.select2');
                            $("#contribution_year" + special).val(null).trigger('change.select2');
                            amaran_notify("{!! trans("exceptions.backend.finance.receipts.date_threshold", ['max' => sysdefs()->data()->total_allowed_contrib_months ]) !!}", "{!! trans("labels.general.warning") !!}", "warning");
                            $('#contribution_select' + special).attr('checked', false);
                            $('#contribution_select' + special).trigger('change');
                            return false;
                        }
                        booked_amount(month, year, $("#booked_amount" + special));
                        interest_checkbox_state(special);

                    });
                    /* end : check selection change event of the drop down months and year  */
                    /* start: contribution and interest checkbox control */
                    $('body').off('change', '.contribution_select').on('change', '.contribution_select', function(e) {
                        var name = $(this).attr('name');
                        var special = name.split("contribution_select").join("");
                        if(this.checked) {
                            var $year = $("#contribution_year" + special).val();
                            var $month = $("#contribution_month" + special).val();
                            var $checkbox = $(this);
                            if (!$year || !$month) {
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.select_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                prevent_check(e, $checkbox);
                            } else {
                                $("#contribution_entry" + special).show();
                            }
                        } else {
                            /* Checkbox is unchecked */
                            $("#contribution_entry" + special).hide();
                            var contribution_amount = $("#contribution_amount" + special).val();
                            if (contribution_amount != '') {
                                entry_amount -= parseFloat(contribution_amount.split(",").join(""));
                                $("#contribution_amount" + special).val("");
                                $('#total_entry').text(addCommas(entry_amount));
                                check_amount_match(receipt_amount, entry_amount);
                            }
                        }
                    });
                    $('body').off('change', '.interest_select').on('change', '.interest_select', function(e) {
                        var name = $(this).attr('name');
                        var special = name.split("interest_select").join("");
                        if(this.checked) {
                            var $year = $("#contribution_year" + special).val();
                            var $month = $("#contribution_month" + special).val();
                            var $checkbox = $(this);
                            var $amount = 0;
                            if (!$year || !$month) {
                                amaran_notify("{!! trans("exceptions.backend.finance.receipts.select_month") !!}", "{!! trans("labels.general.warning") !!}", "warning");
                                prevent_check(e, $checkbox);
                            } else {
                                interest_due_amount($month, $year).done(function (data) {
                                    $amount = data.amount;
                                    /* console.log(data.amount); */
                                    if ($amount == '0.00') {
                                        prevent_check(e, $checkbox);
                                        $("#no_interest" + special).show();
                                        $("#interest_entry" + special).hide();
                                    } else {
                                        $("#interest_due" + special).val($amount);
                                        $("#interest_entry" + special).show();
                                        $("#no_interest" + special).hide();
                                    }
                                });
                            }
                        } else {
                            /* Checkbox is unchecked */
                            $("#interest_entry" + special).hide();
                            var interest_amount = $("#interest_amount" + special).val();
                            if (interest_amount != '') {
                                entry_amount -= parseFloat(interest_amount.split(",").join(""));
                                $("#interest_amount" + special).val("");
                                $('#total_entry').text(addCommas(entry_amount));
                                check_amount_match(receipt_amount, entry_amount);
                            }
                        }
                    });
                    /* end: contribution and interest checkbox control */
                    /* start : mask all money input */
                    $('.money').maskMoney({
                        precision : 2,
                        affixesStay : false
                    });
                    /* end : mask all money input */
                    /* start : ensure only numbers are input on monetary boxes */
                    $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                        number_only(e)
                    });
                    /* end : ensure only numbers are input on monetary boxes */
                    /* start : handle amount textboxes change event and sum all the entered amounts */

                    $('body').off('keyup', '.contribution_amount').on('keyup', '.contribution_amount', function(e) {
                        var value;
                        var sum = 0;
                        $('.contribution_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        $('.interest_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        entry_amount = sum;
                        $('#total_entry').text(addCommas(entry_amount));
                        check_amount_match(receipt_amount, entry_amount);
                    });
                    $('body').off('keyup', '.interest_amount').on('keyup', '.interest_amount', function(e) {
                        var value;
                        var sum = 0;
                        $('.interest_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        $('.contribution_amount').each(function() {
                            value = $(this).val();
                            if (value != '') {
                                sum += parseFloat(value.split(",").join(""));
                            }
                        });
                        entry_amount = sum;
                        $('#total_entry').text(addCommas(entry_amount));
                        check_amount_match(receipt_amount, entry_amount);
                    });
                    /* end : handle amount textboxes leave event and sum all the entered amounts */
                }).fail(function () {

                });





                /* end : Retrieve Contribution entries from the server  */
            }
        });




        function validate_entry(selected_entries, control) {
            if (selected_entries === '') {
                control.addClass('form-error');
                setTimeout(
                    function() { control.removeClass('form-error'); },
                    2000
                );
                return false;
            } else {
                return true;
            }
        }


        /* start : check the interest select checkbox checked state and alter the values respectively */
        function interest_checkbox_state(special) {
            if ($('#interest_select' + special).is(':checked')) {
                $('#interest_select' + special).prop('checked', true).trigger('change');
            }
        }
        /* end : check the interest select checkbox checked state and alter the values respectively */


        /* start: Get booked amount for a particular month and year */
        function booked_amount(month, year, target) {
            $.post( "{!! route('backend.finance.receipt.booked_amount') !!}", {'month' : month, 'year' : year, 'employer_id' : employer_id}, function( data ) {
                $(target).val(data.amount);
                /* console.log(data); */
            }, "json").done(function () {

            }).fail(function () {

            });
        }
        /* start: Get the interest charged for a particular month and a year */
        function interest_due_amount(month, year) {
            return $.post( "{!! route('backend.finance.receipt.interest_due_amount') !!}", {'month' : month, 'year' : year, 'employer_id' : employer_id}, function( data ) {
                /* callback(data.amount); */
                /* console.log(data); */
            }, "json");
        }
        /* end: Get the interest charged for a particular month and a year */

        /* end: Get booked amount for a particular month and year */
        /* start: Prevent the checkbox from being checked when the conditions don't allow */
        function prevent_check($e, $target) {
            setTimeout(function() {
                $target.removeAttr('checked');
            }, 0);
            $e.preventDefault();
            $e.stopPropagation();
        }
        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        function add_contribution_entry_control(selected_entries) {
            return true;
            /*
             if (max_entries >= (total_entries + selected_entries)) {
             total_entries = total_entries + selected_entries;
             return true;
             } else {
             return false;
             }
             */
        }

        });


    </script>
@endpush