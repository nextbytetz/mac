@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.title.receipt.action.index'), 'header_title' => trans('labels.backend.finance.header.receipt.action.index')])

@push('after-styles-end')

@endpush

@section('content')

    <div class="row">
        <div class="col-md-12" >

            <div class = "pull-right">
                {{--REPLACE CHEQUE--}}
                @if ($legacy_receipt->isdishonoured == 1)
                    {!! link_to_route('backend.finance.legacy_receipt.new_cheque',trans('buttons.general.replace_cheque'), [$legacy_receipt->id],['id'=> 'replace_submit',  'class' => 'btn btn-primary site-btn dishonour_button', ]) !!}
                    {{--CANCEL--}}
                    {!! link_to_route('backend.finance.legacy_receipt.cancel_reason',trans('buttons.general.cancel'), [$legacy_receipt->id],['id'=> 'cancel_submit',  'class' => 'btn btn-primary site-btn delete_button', ]) !!}
                @endif
                @if ($legacy_receipt->iscancelled== 0 && $legacy_receipt->isdishonoured == 0 )
                    {{--PRINT--}}
                    {{--{{ link_to_route('backend.finance.legacy_receipt.print', trans('buttons.general.print'), [$legacy_receipt->id], ['class' => 'btn btn-primary site-btn invoice-print save_button', 'data-icon' => ' ', 'target' => '_blank']) }}--}}

                    @if(!is_null($legacy_receipt->chequeno))
                        {{--DISHONOUR--}}
                        {!! link_to_route('backend.finance.legacy_receipt.dishonour_reason',trans('buttons.general.dishonour'), [$legacy_receipt->id],['id'=> 'dishonour_submit',  'class' => 'btn btn-primary site-btn dishonour_button', ]) !!}
                    @endif
                    {{--CANCEL--}}
                    {!! link_to_route('backend.finance.legacy_receipt.cancel_reason',trans('buttons.general.cancel'), [$legacy_receipt->id],['id'=> 'cancel_submit',  'class' => 'btn btn-primary site-btn delete_button', ]) !!}
                @endif

            </div>

        </div>
    </div>
    <legend>
    </legend>
    <div>&nbsp;</div>

    {{--Summary of receipt--}}
    <div class="row">
        <div class="col-md-12" >
            @include('backend.finance.legacy_receipt.includes.summary')
        </div>
    </div>



@stop


@push('after-script-end')
<script  type="text/javascript">
    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();

    $('#text_content_dishonour').on( 'change keyup keydown paste cut',
        'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();
</script>;

{{--{{ Html::script(asset_url() . "/global/plugins/jQuery.print/jQuery.print.js") }}
{{ Html::script(asset_url() . "/global/js/global/invoice.js") }}--}}

@endpush
