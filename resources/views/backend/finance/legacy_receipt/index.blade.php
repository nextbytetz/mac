@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.receipt.receipts_before_jul_2017'), 'header_title' => trans('labels.backend.finance.receipt.receipts_before_jul_2017')])

@include('backend.includes.datatable_assets')

@section('content')

    {{--<div>&nbsp;</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

        </div>
    </div>

@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
@endpush
