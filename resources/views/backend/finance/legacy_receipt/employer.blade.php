@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.receipt.for_employer'), 'header_title' => trans('labels.backend.finance.receipt.choose_employer_before_electronic')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {!! Form::open(['route' => 'backend.finance.legacy_receipt.employer.post']) !!}
    <p>
        {!! getLanguageBlock('backend.lang.search.employer') !!}
    </p>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="grid-column">
                {!! Form::select('employer', [], null, ['class' => 'employer-select', 'style' => 'width:100%']) !!}
                {!! $errors->first('employer', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::submit(trans('labels.general.continue'), ['class' => 'btn btn-success btn-save btn-block']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script>

    $(function () {
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
    });

</script>
@endpush