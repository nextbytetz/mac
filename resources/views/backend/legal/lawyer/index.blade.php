@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.lawyer.index'), 'header_title' => trans('labels.backend.legal.lawyer.index')])

@include('backend.includes.datatable_assets')

@section('content')

    {{--<section id="content-wrapper">--}}
    {{--<div style="color:#fff">--}}
    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12 " >
                <div class="class pull-right">
                    <a href="{!! route('backend.legal.case.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;Add Case</a>
                    <a href="{!! route('backend.legal.lawyer.create') !!}"  class="btn btn-success" ><i class="icon fa fa-plus-circle"></i>&nbsp;Add Lawyer</a>
                </div>
            </div>
            <div>&nbsp;</div>
            <table class="display" cellspacing="0" width="100%" id ="lawyers-table"  >
                <thead>
                <tr >
                    <th>@lang('labels.general.name')</th>
                    <th>@lang('labels.backend.table.case_hearings')</th>
                    {{--<th>@lang('labels.backend.table.case_mentions')</th>--}}
                    <th>@lang('labels.backend.table.external_id')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>



@stop


@push('after-script-end')

<script  type="text/javascript">

    $(function() {
        $('#lawyers-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.legal.lawyer.get') !!}',
                type : 'post'

            },
            columns: [
                { data: 'name', name: 'name', searchable:false, orderable:false},
                { data: 'case_hearings', name: 'case_hearings', searchable:false, orderable:false},
                /*{ data: 'case_mentionings', name: 'case_mentionings', searchable:false, orderable:false},*/
                { data: 'external_id', name: 'external_id' }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/legal/lawyer/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
    });







</script>;

@endpush
