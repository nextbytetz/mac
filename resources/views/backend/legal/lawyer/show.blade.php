@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.lawyer.profile'), 'header_title' => trans('labels.backend.legal.lawyer.profile')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush
@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">

        {{--Tabs navigation--}}
        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#tab-1" data-toggle="tab">General</a>
                        </li>
                    </ul>
                    <div class="nav_tab_contain tab-content">
                        <div id="tab-1" class="nav_tab_pane tab-pane active in">
                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >
                                            {{--edit--}}
                                            <span>
						                     <a href="{!! route('backend.legal.lawyer.edit', $lawyer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>
						                    </span>
                                            {{--<span>
                                            <a href="{!! route('backend.legal.lawyer.destroy', $lawyer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-trash"></i>&nbsp;@lang('buttons.general.crud.delete')</a>
                                            </span>--}}

                                            <span>
                                           {!! HTML::decode(link_to_route('backend.legal.lawyer.destroy', "&nbsp;" . trans('buttons.general.crud.delete'), $lawyer->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.legal.confirm_lawyer_delete'), 'class' => 'icon fa fa-trash btn btn-primary site-btn nav_button'])) !!}
                                         </span>


                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{--main tab content--}}
                            <div class = "row">
                                {{--<div class="class container">--}}
                                <div class="col-md-8 col-sm-8">
                                    <legend class="grey_modal" >@lang('labels.backend.legal.title.hearings')</legend>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <table class="display" cellspacing="0" width="100%" id ="lawyer-hearings-table">
                                                <thead>
                                                <tr >
                                                    <th>@lang('labels.backend.table.case_title')</th>
                                                    <th>@lang('labels.backend.table.hearingdate')</th>
                                                    <th>@lang('labels.backend.table.case_fee')</th>
                                                    <th>@lang('labels.backend.table.status')</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <br/>
                                    {{--<legend class="grey_modal" >@lang('labels.backend.legal.title.mentions')</legend>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <table class="table table-bordered" id ="lawyer-mentionings-table" style="width:100%" >
                                                <thead>
                                                <tr >
                                                    <th>@lang('labels.backend.table.case_title')</th>
                                                    <th>@lang('labels.backend.table.mentioningdate')</th>
                                                    <th>@lang('labels.backend.table.mentioningtime')</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>--}}
                                    <br/>
                                </div>
                                <div class="col-md-4 col-sm-4 ">
                                    {{--sidebar--}}
                                    @include('backend.legal.lawyer.includes.sidebar_summary')
                                </div>
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@stop



@push('after-script-end')

<script  type="text/javascript">

    $(function() {
        $('#lawyer-hearings-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.legal.case.hearing.get.lawyer', $lawyer->id) !!}',
                type : 'post'

            },
            columns: [
                { data: 'title', name: 'title', searchable: false, orderable:false},
                { data: 'hearing_date_label', name: 'hearing_date_label', searchable: false, orderable:false},
                { data: 'fee_label', name: 'fee_label', searchable: false, orderable:false},
                { data: 'status', name: 'status', searchable: false, orderable:false},
                { data: 'fee', name: 'fee', visible : false},
                { data: 'hearing_date', name: 'hearing_date', visible : false},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/legal/case/hearing/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
        /*$('#lawyer-mentionings-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.legal.case.mention.get.lawyer', $lawyer->id) !!}',
                type : 'post'

            },
            columns: [
                { data: 'title', name: 'title', searchable: false, orderable:false},
                { data: 'mention_date_label', name: 'mention_date_label', searchable: false, orderable:false},
                { data: 'mention_time_label', name: 'mention_time_label', searchable: false, orderable:false},
                { data: 'mention_date', name: 'mention_date', visible : false},
                { data: 'mention_time', name: 'mention_time', visible : false},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/legal/case/mention/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });*/
    });
</script>
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush
