@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.edit_lawyer'), 'header_title' => trans('labels.backend.legal.edit_lawyer')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

    {!! Form::model($lawyer, ['route' => ['backend.legal.lawyer.update', $lawyer->id], 'class' => 'update_lawyer', 'method' => 'put']) !!}
    {!! Form::hidden("lawyer_id", $lawyer->id) !!}
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.lawyer.category')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select( 'category', ['0' => 'Individual', '1' => 'Firm'], $category, ['class' => 'form-control search-select category_select', 'placeholder' => '', 'style' => 'width:50%;']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div  style="display: none;" id = "individual">
                <div class="fileld-layout">
                    <label class="required">{!! trans('labels.general.firstname') !!}</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::input( 'text','firstname', null, ['class' => 'form-control input-radius lawyers_individual', 'style' => 'width:50%']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>{!! trans('labels.general.middlename') !!}</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::input( 'text','middlename', null, ['class' => 'form-control input-radius lawyers_individual', 'style' => 'width:50%']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label class="required">{!! trans('labels.general.lastname') !!}</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::input( 'text','lastname', null, ['class' => 'form-control input-radius lawyers_individual', 'style' => 'width:50%']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="fileld-layout" style="display: none;" id = "firm">
                <label class="required">@lang('labels.backend.legal.firm_responsible')</label>
                <div class="form-group">
                    <div class="input-group">
                        {{--{!! Form::select( 'lawyer_id', $lawyers_firm, null, ['class' => 'form-control search-select', 'placeholder' => '', 'id' => 'lawyers_firm', 'style' => 'width:100%;']) !!}--}}
                        {!! Form::select('employer_id', [], null, ['class' => 'employer-select', 'id' => 'lawyers_firm', 'style' => 'width:100%']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout">
                <label>@lang('labels.backend.legal.external_id')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','external_id', null, ['class' => 'form-control input-radius', 'style' => 'width:100%']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <div class="pull-right">
                <a  href="{!! route('backend.legal.lawyer.index') !!}"  class="btn btn-primary site-btn cancel_button">@lang('buttons.general.cancel')</a>
                {!! Form::button(trans('buttons.general.crud.update'),['class' => 'btn btn-primary site-btn save_button btn-submit', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function () {
        $(".search-select").select2();
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $(".category_select").on("change", function () {
            var $category = $(this).val();
            switch($category) {
                case '0':
                    /* Individual Category */
                    $("#individual").show();
                    $(".lawyers_individual").prop('disabled', false);
                    $("#firm").hide();
                    $("#lawyers_firm").prop('disabled', true);
                    break;
                case '1':
                    /* Firm Category */
                    $("#individual").hide();
                    $(".lawyers_individual").prop('disabled', true);
                    $("#firm").show();
                    $("#lawyers_firm").prop('disabled', false);
                    break;
                default:
                    $("#individual").hide();
                    $(".lawyers_individual").prop('disabled', true);
                    $("#firm").hide();
                    $("#lawyers_firm").prop('disabled', true);
            }
        });
        $(".category_select").trigger("change");
        /* end : mask all money input */
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form.update_lawyer', function (e) {
            e.preventDefault();
            var form = this;
            var $data = $(form).serialize();
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $(form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $(form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $(form).find("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            $.ajax({
                data : $data,
                dataType : "json",
                method : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    $(form).find(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    if (data.success) {
                        document.location.replace(base_url + "/legal/lawyer/" + data.id);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    /* console.log(errors); */
                    $.each(errors, function(index, value) {
                        $(form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $(form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $(form).find("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });
                },
            }).done(function() {
                $(form).find(".btn-submit").prop('disabled', false);
            }).fail(function() {

            }).always(function() {
                $(form).find(".btn-submit").prop('disabled', false);
            });
        });
        /* end: Submitting Form and perform validation on the server side */
    });
</script>

@endpush