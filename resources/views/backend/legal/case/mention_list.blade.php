<legend class="grey_modal" >@lang('labels.backend.legal.title.mentions')</legend>

<br/>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" cellspacing="0" width="100%" id ="mentions-table">
            <thead>
            <tr >
                <th>@lang('labels.backend.table.mention_date')</th>
                <th>@lang('labels.backend.table.mention_time')</th>
                <th>@lang('labels.backend.table.lawyer')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#mentions-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.legal.case.mention.get', $cases->id) !!}',
                type : 'post'

            },
            columns: [
                { data: 'mention_date'},
                { data: 'mention_time' , name: 'lawyer_id'},
                { data: 'lawyer'},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/legal/case/mention/" + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
    });

</script>;

@endpush
