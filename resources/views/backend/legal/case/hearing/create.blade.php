@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.addhearing'), 'header_title' => trans('labels.backend.legal.addhearing')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/timepicki/css/timepicki.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .case_personnel:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.legal.case_personnel')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .case_personnel {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')
    {!! Form::open(['route' => ['backend.legal.case.hearing.store'], 'method'=>'post', 'class' => 'add_hearing']) !!}
    {!! Form::hidden("case_id", $cases->id) !!}
    {!! Form::hidden("user_id", access()->id()) !!}
    <div class="row">
        <div class="col-md-12" >
            <div class="col-md-8 col-sm-8">
                <h5 class="client-title">
                    <i class="icon-circle"></i>
                    <!-- ngIf: client.subStatus.description -->
                    <small >
                        Case #: <strong>{!! Form::label( 'reg_no', $cases->number, [ 'class'=> 'underline']) !!}</strong>
                    </small>
                    <small>
                        Case Title: <strong>{!! Form::label( 'name', $cases->title, [ 'class'=> 'underline']) !!}</strong>
                    </small>
                </h5>
            </div>
        </div>
    </div>
    <legend></legend>
<br/>
    <div class="row">
        <div class="col-md-6 col-sm-6" style="padding-right:20px; border-right: 1px solid #ddd;">
            <div class="filed-layout">
                <label class="required">@lang('labels.backend.legal.add_hearing_date')</label>
                <div class="form-group">
                    <div class="form-inline">
                        <span>
                            {!!  Form::selectRange('hearing_day',1 , 31, null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                        <span>
                            {!!  Form::selectMonth('hearing_month',null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                        </span>
                        <span>
                            {!!  Form::selectRange('hearing_year', 2015, $max_year , null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                        </span>
                        <span>
                            {!! Form::hidden('hearing_date') !!}
                        </span>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout">
                <label>{!! trans('labels.backend.legal.hearing_time') !!}</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','hearing_time', null, ['class' => 'form-control input-radius timepicker', 'style' => 'width:30%']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.lawyer.category')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select( 'category', ['0' => 'Individual', '1' => 'Firm'], null, ['class' => 'form-control search-select category_select', 'placeholder' => '', 'style' => 'width:50%;']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout" style="display: none;" id = "individual">
                <label class="required">@lang('labels.backend.legal.lawyer_responsible')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select( 'lawyer_id', $lawyers_individual, null, ['class' => 'form-control search-select', 'placeholder' => '', 'id' => 'lawyers_individual', 'style' => 'width:100%;']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout" style="display: none;" id = "firm">
                <label class="required">@lang('labels.backend.legal.firm_responsible')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select( 'lawyer_id', $lawyers_firm, null, ['class' => 'form-control search-select', 'placeholder' => '', 'id' => 'lawyers_firm', 'style' => 'width:100%;']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout">
                <label>{!! trans('labels.backend.legal.filling_fee') !!}</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','fee', null, ['class' => 'form-control input-radius money', 'style' => 'width:50%']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="filed-layout">
                <label>@lang('labels.backend.legal.filling_date')</label>
                <div class="form-group">
                    <div class="form-inline">
                        <span>
                            {!!  Form::selectRange('filling_day',1 , 31, null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                        <span>
                            {!!  Form::selectMonth('filling_month',null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                        </span>
                        <span>
                            {!!  Form::selectRange('filling_year', 2015, $max_year , null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                        </span>
                        <span>
                            {!! Form::hidden('filling_date') !!}
                        </span>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.status')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select( 'status', ['0' => 'Not Heard', '1' => 'Heard', '2' => 'Cancelled'], null, ['class' => 'form-control search-select', 'placeholder' => '', 'style' => 'width:50%;']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="fileld-layout">
                <label>@lang('labels.backend.legal.description')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'status_description' ,null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>

        </div>
        <div class="col-md-6 col-sm-6">
            <div class="case_personnel">
                <div class="fileld-layout">
                    <label class="required">@lang('labels.general.type')</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select( 'case_personnel_id', $personnels, null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label class="required">{!! trans('labels.general.firstname') !!}</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::input( 'text','firstname', null, ['class' => 'form-control input-radius', 'style' => 'width:50%']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>{!! trans('labels.general.middlename') !!}</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::input( 'text','middlename', null, ['class' => 'form-control input-radius', 'style' => 'width:50%']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label class="required">{!! trans('labels.general.lastname') !!}</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::input( 'text','lastname', null, ['class' => 'form-control input-radius', 'style' => 'width:50%']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="pull-right">
                <a  href="{!! route('backend.legal.case.show', $cases->id) !!}"  class="btn btn-primary site-btn cancel_button">@lang('buttons.general.cancel')</a>
                {!! Form::button(trans('buttons.general.crud.create'),['class' => 'btn btn-primary site-btn save_button btn-submit', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/timepicki/js/timepicki.js") }}
<script>
    $(function () {
        $(".search-select").select2();
        autosize($("textarea.autosize"));
        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });
        $('.timepicker').timepicki({
            step_size_minutes:1,
            overflow_minutes:false,
            increase_direction:'up',
        });
        $(".category_select").on("change", function () {
            var $category = $(this).val();
            switch($category) {
                case '0':
                    /* Individual Category */
                    $("#individual").show();
                    $("#lawyers_individual").prop('disabled', false);
                    $("#firm").hide();
                    $("#lawyers_firm").prop('disabled', true);
                    break;
                case '1':
                    /* Firm Category */
                    $("#individual").hide();
                    $("#lawyers_individual").prop('disabled', true);
                    $("#firm").show();
                    $("#lawyers_firm").prop('disabled', false);
                    break;
                default:
                    $("#individual").hide();
                    $("#lawyers_individual").prop('disabled', true);
                    $("#firm").hide();
                    $("#lawyers_firm").prop('disabled', true);
            }
        });
        /* end : mask all money input */
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form.add_hearing', function (e) {
            e.preventDefault();
            var form = this;
            var $hearing_day = $(form).find("select[name=hearing_day]").val();
            var $hearing_month = $(form).find("select[name=hearing_month]").val();
            var $hearing_year = $(form).find("select[name=hearing_year]").val();
            if ($hearing_day && $hearing_month && $hearing_year) {
                $(form).find("input[name=hearing_date]").val($hearing_year + '-' + $hearing_month + '-' + $hearing_day);
            } else {
                $(form).find("input[name=hearing_date]").val("");
            }
            var $filling_day = $(form).find("select[name=filling_day]").val();
            var $filling_month = $(form).find("select[name=filling_month]").val();
            var $filling_year = $(form).find("select[name=filling_year]").val();
            if ($filling_day && $filling_month && $filling_year) {
                $(form).find("input[name=filling_date]").val($filling_year + '-' + $filling_month + '-' + $filling_day);
            } else {
                $(form).find("input[name=filling_date]").val("");
            }
            var $data = $(form).serialize();
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $(form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $(form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $(form).find("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            $.ajax({
                data : $data,
                dataType : "json",
                method : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    $(form).find(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    if (data.success) {
                        document.location.replace(base_url + "/legal/case/hearing/" + data.id);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    /* console.log(errors); */
                    $.each(errors, function(index, value) {
                        $(form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $(form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $(form).find("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });
                },
            }).done(function() {
                $(form).find(".btn-submit").prop('disabled', false);
            }).fail(function() {

            }).always(function() {
                $(form).find(".btn-submit").prop('disabled', false);
            });
        });
        /* end: Submitting Form and perform validation on the server side */
    });
</script>

@endpush