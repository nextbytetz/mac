@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.hearing.dashboard'), 'header_title' => trans('labels.backend.legal.hearing.dashboard')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Case hearing Profile header detail -->
                <small >
                    Case #: <strong>{!! Form::label( 'reg_no', $hearing->cases->number, [ 'class'=> 'underline']) !!}</strong>
                </small>
                <small>
                    Case Title: <strong>{!! Form::label( 'name', $hearing->cases->title, [ 'class'=> 'underline']) !!}</strong>
                </small>
                <span>{!! $hearing->status_label !!}</span>
            </h5>
        </div>
    </div>
    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
        <div class="row">
            <div class="col-md-12" >
                <div class="pull-right" >
                    {{--Delete--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.legal.case.hearing.destroy', "<i class='icon fa fa-trash' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.delete'), [$hearing->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.legal.hearing.delete_confirm'), 'class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;']))  !!}
                    </span>
                    {{--Edit--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.legal.case.hearing.edit', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.update'), [$hearing->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                    {{--Exit--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.legal.case.show', "<i class='icon fa fa-close' aria-hidden='true'></i>&nbsp;" . trans('labels.general.exit'), [$hearing->cases->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class = "row">
        {{--<div class="col-md-12">--}}
        <div class="col-md-3 col-sm-3">
            {{-- employer task list overview --}}
        </div>
        <div class="col-md-6 col-sm-6">
            {{--sidebar summary--}}
            @include('backend/legal/case/hearing/summary')
        </div>
        {{--</div>--}}
    </div>
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script>
    $(function(){

    });
</script>
@endpush