<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="light_grey_bg">&nbsp;</div>
        <div class="light_grey_bg" style="padding: 5px;">
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.add_hearing_date') :</h6>
            <p style="font-weight: bold;">{!! $hearing->hearing_date_label !!}</p>
            <h6 class="underline" style="font-weight: lighter;">{!! trans('labels.backend.legal.hearing_time') !!} :</h6>
            <p style="font-weight: bold;">{!! $hearing->hearing_time_label !!}</p>
            @if (is_null($hearing->lawyer->employer_id))
                <h6 class="underline" style="font-weight: lighter;"> @lang('labels.backend.legal.lawyer_responsible') :</h6>
            @else
                <h6 class="underline" style="font-weight: lighter;"> @lang('labels.backend.legal.firm_responsible') :</h6>
            @endif
            <p style="font-weight: bold;">{!! $hearing->lawyer->name !!}</p>
            <h6 class="underline" style="font-weight: lighter;">{!! trans('labels.backend.legal.filling_fee') !!} :</h6>
            <p style="font-weight: bold;">{!! $hearing->fee_label !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.filling_date') :</h6>
            <p style="font-weight: bold;">{!! $hearing->filling_date_label !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.status') :</h6>
            <p style="font-weight: bold;">{!! $hearing->status_label !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.description') :</h6>
            <p style="font-weight: bold;">{!! $hearing->status_description_label !!}</p>
            <h6 class="underline" style="font-weight: lighter;">(&nbsp;@lang('labels.backend.legal.case_personnel')&nbsp;)&nbsp; <i>{!! $hearing->casePersonnel->name !!}</i>&nbsp;:&nbsp;</h6>
            <p style="font-weight: bold;">{!! $hearing->full_name !!}</p>
        </div>
    </div>
</div>