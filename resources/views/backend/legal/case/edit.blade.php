@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.edit'), 'header_title' => trans('labels.backend.legal.edit')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

    {!! Form::model($cases, ['route' => ['backend.legal.case.update', $cases->id], 'class' => 'update_legal_case', 'role' => 'form', 'method' => 'put']) !!}

    {!! Form::hidden("id", $cases->id) !!}
    <div class="row">
        <div class="col-md-6 col-sm-6" style="padding-right:20px; border-right: 1px solid #ddd;">
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.case_no')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','number', null, ['class' => 'form-control input-radius' ])!!}
                    </div>
                    {!! $errors->first('number', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout" style="display: none;">
                <label>@lang('labels.backend.legal.parent')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('parent_id', $other_cases, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    {!! $errors->first('parent_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.case_title')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','title', null, ['class' => 'form-control input-radius']) !!}
                    </div>
                    {!! $errors->first('title', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.complainant')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','complainant', null, ['class' => 'form-control input-radius']) !!}
                    </div>
                    {!! $errors->first('complainant', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label>@lang('labels.backend.legal.complainant_address')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'complainant_address' ,null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('complainant_address', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.court_category')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('court_category_id', $court_categories, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    {!! $errors->first('court_category_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.court_name')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','court', null, ['class' => 'form-control input-radius']) !!}
                    </div>
                    {!! $errors->first('court', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label>@lang('labels.backend.legal.location')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','location', null, ['class' => 'form-control input-radius']) !!}
                    </div>
                    {!! $errors->first('location', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.district')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('district_id', $districts, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="fileld-layout">
                <label>{!! trans('labels.backend.legal.filling_fee') !!}</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','fee', null, ['class' => 'form-control input-radius money']) !!}
                    </div>
                    {!! $errors->first('fee', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="filed-layout">
                <label>@lang('labels.backend.legal.filling_date')</label>
                <div class="form-group">
                    <div class="form-inline">
                        <span>
                            {!!  Form::selectRange('filling_day',1 , 31, (($filling_date) ? $filling_date->format('j') : null), ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                        <span>
                            {!!  Form::selectMonth('filling_month', (($filling_date) ? $filling_date->format('n') : null), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                        </span>
                        <span>
                            {!!  Form::selectRange('filling_year', 2015, $max_year , (($filling_date) ? $filling_date->format('Y') : null), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                        </span>
                        <span>
                            {!! Form::hidden('filling_date') !!}
                        </span>
                    </div>
                    {!! $errors->first('filling_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>


            {{--<div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.contigent_liability')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'liability' ,null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('liability', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>--}}

            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.contigent_liability')</label>
                <div class="input-group">
                    {!! Form::select('liability_type', ['1' => 'Contentious', '2' => 'Non Contentious'], null, ['class' => 'form-control search-select contigent_liability_select','style'=>'width:100%', 'placeholder' => '']) !!}
                </div>
                {!! $errors->first('liability_type', '<span class="help-block label label-danger">:message</span>') !!}
                <br/>
            </div>
            <div id="contentious_type" class="fileld-layout" style="display: none;">
                <label class="required">@lang('labels.backend.legal.contentious_type')</label>
                <div class="input-group">
                    {!! Form::select('contentious_type', ['1' => 'Obliged to Pay', '2' => 'To be Paid'], null, ['class' => 'form-control search-select contentious_type_select','style'=>'width:100%', 'placeholder' => '']) !!}
                </div>
                {!! $errors->first('contentious_type', '<span class="help-block label label-danger">:message</span>') !!}
                <br/>
            </div>
            <div id="liability" class="fileld-layout" style="display: none;">
                <label class="required">@lang('labels.backend.legal.contigent_liability_description')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'liability' ,null, ['class' => 'form-control autosize liability',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('liability', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>


            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.legal.respondent')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','respondent', null, ['class' => 'form-control input-radius']) !!}
                    </div>
                    {!! $errors->first('respondent', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label>@lang('labels.backend.legal.respondent_address')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'respondent_address' ,null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('respondent_address', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label>@lang('labels.backend.legal.brief_facts')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'fact' ,null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('fact', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="fileld-layout">
                <label>@lang('labels.backend.legal.status')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select( 'status', ['0' => 'Open', '1' => 'Closed'], null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
                    </div>
                    {!! $errors->first('status', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <a  href="{!! route('backend.legal.case.show', $cases->id) !!}"  class="btn btn-primary site-btn cancel_button">@lang('buttons.general.cancel')</a>
                {!! Form::button(trans('buttons.general.crud.update'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
<script>
    $(function(){
        $(".search-select").select2({});

        $(".contigent_liability_select").on("change", function () {
            var $category = $(this).val();
            switch($category) {
                case '1':
                    /* Contentious */
                    $("#contentious_type").show();
                    $(".contentious_type_select").prop('disabled', false);
                    $("#liability").show();
                    $(".liability").prop('disabled', false);
                    break;
                case '2':
                    /*Non Contentious*/
                    $("#contentious_type").hide();
                    $(".contentious_type_select").prop('disabled', true);
                    $("#liability").hide();
                    $(".liability").prop('disabled', true);
                    break;
                default:
                    $("#contentious_type").hide();
                    $(".contentious_type_select").prop('disabled', true);
                    $("#liability").hide();
                    $(".liability").prop('disabled', true);
            }
        });
        $('.contigent_liability_select').trigger('change');

        autosize($("textarea.autosize"));
        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });
        /* end : mask all money input */
        /* start : Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form.update_legal_case', function (e) {
            e.preventDefault();
            var form = this;
            var $filling_day = $(form).find("select[name=filling_day]").val();
            var $filling_month = $(form).find("select[name=filling_month]").val();
            var $filling_year = $(form).find("select[name=filling_year]").val();
            if ($filling_day && $filling_month && $filling_year) {
                $(form).find("input[name=filling_date]").val($filling_year + '-' + $filling_month + '-' + $filling_day);
            } else {
                $(form).find("input[name=filling_date]").val("");
            }
            form.submit();
        });
        /* end : Submitting Form and perform validation on the server side */
    });
</script>
@endpush