@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.title.case_profile'), 'header_title' => trans('labels.backend.legal.title.case_profile')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        {!! Form::model($cases, ['route' => ['backend.finance.receipt.dishonour', $cases->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <small >
                    Case #: <strong>{!! Form::label( 'reg_no', $cases->number, [ 'class'=> 'underline']) !!}</strong>
                </small>
                <small>
                    Case Title: <strong>{!! Form::label( 'name', $cases->title, [ 'class'=> 'underline']) !!}</strong>
                </small>
                <span>{!! $cases->status_label !!}</span>
            </h5>
        </div>
    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12 col-sm-12">
            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#general" data-toggle="tab"> General </a>
                    </li>
                </ul>
                <div class="nav_tab_contain tab-content">
                    <div id="general" class="nav_tab_pane tab-pane active in">
                        @include("backend/legal/case/includes/header")
                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-7 col-sm-7">
                                @include("backend/legal/case/hearing_list")
                                <br/>
                                {{--@include("backend/legal/case/mention_list")--}}
                            </div>
                            {{--<div class="class container">--}}
                            <div class="col-md-5 col-sm-5">
                                {{--sidebar--}}
                                @include('backend.legal.case.includes.sidebar_summary')
                            </div>
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });
</script>
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush
