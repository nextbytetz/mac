@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.mention.dashboard'), 'header_title' => trans('labels.backend.legal.mention.dashboard')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Inspection Profile header detail -->
                <small >
                    Case #: <strong>{!! Form::label( 'reg_no', $mention->cases->number, [ 'class'=> 'underline']) !!}</strong>
                </small>
                <small>
                    Case Title: <strong>{!! Form::label( 'name', $mention->cases->title, [ 'class'=> 'underline']) !!}</strong>
                </small>
            </h5>
        </div>
    </div>
    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
        <div class="row">
            <div class="col-md-12" >
                <div class="pull-right" >
                    {{--Delete--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.legal.case.mention.destroy', "<i class='icon fa fa-trash' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.delete'), [$mention->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.legal.mention.delete_confirm'), 'class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;']))  !!}
                    </span>
                    {{--Edit--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.legal.case.mention.edit', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.edit'), [$mention->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                    {{--Exit--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.legal.case.show', "<i class='icon fa fa-close' aria-hidden='true'></i>&nbsp;" . trans('labels.general.close'), [$mention->cases->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class = "row">
        <div class="col-md-12">
            <div class="col-md-3 col-sm-3">
                {{-- employer task list overview --}}
            </div>
            <div class="col-md-6 col-sm-6">
                {{--sidebar summary--}}
                @include('backend/legal/case/mention/summary')
            </div>
        </div>
    </div>
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script>
    $(function(){

    });
</script>
@endpush