<legend class="grey_modal" >@lang('labels.backend.legal.title.hearings')</legend>

<br/>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" cellspacing="0" width="100%" id ="cases-table">
            <thead>
            <tr >
                <th>@lang('labels.backend.table.hearingdate')</th>
                <th>@lang('labels.backend.table.lawyer')</th>
                <th>@lang('labels.backend.table.case_fee')</th>
                <th>@lang('labels.backend.table.status')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#cases-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.legal.case.hearing.get', $cases->id) !!}',
                type : 'post'

            },
            columns: [
                { data: 'hearing_date', name: 'hearing_date'},
                { data: 'lawyer_id' , name: 'lawyer_id'},
                { data: 'fee', name: 'fee'},
                { data: 'status', name: 'status' }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/legal/case/hearing/" + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
    });

</script>;

@endpush