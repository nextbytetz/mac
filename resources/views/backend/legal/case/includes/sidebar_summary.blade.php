<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="light_grey_bg">&nbsp;</div>
        <div class="light_grey_bg" style="padding: 5px;">
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.complainant') :</h6>
            <p style="font-weight: bold;">{!! $cases->complainant !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.respondent') :</h6>
            <p style="font-weight: bold;">{!! $cases->respondent !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.court_name') :</h6>
            <p style="font-weight: bold;">{!! $cases->court !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.contigent_liability') :</h6>
            <p style="font-weight: bold;">{!! $cases->liability_type_label !!}</p>

            @if ($cases->liability_type == 1)
                <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.contentious_type') :</h6>
                <p style="font-weight: bold;">{!! $cases->contentious_type_label !!}</p>
                <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.contigent_liability_description') :</h6>
                <p style="font-weight: bold;">{!! $cases->liability !!}</p>
            @endif

            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.filling_fee') :</h6>
            <p style="font-weight: bold;">{!! $cases->fee_formatted !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.filling_date') :</h6>
            <p style="font-weight: bold;">{!! $cases->filling_date_formatted !!}</p>
            <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.legal.brief_facts') :</h6>
            <p style="font-weight: bold;">{!! $cases->fact !!}</p>
        </div>
    </div>
</div>