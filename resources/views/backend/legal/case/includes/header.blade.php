<div class="nav_tab_pane_header">
    <div class="row">
        <div class="col-md-12" >

            <div class="pull-right" >
                {{--Add Case Mentioning--}}
                                            {{--<span>
                                            <a href="{!! route('backend.legal.case.mention.create', $cases->id)!!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-plus-square-o" aria-hidden="true"></i>&nbsp;@lang('buttons.backend.legal.case.add_mentioning')
                                            </a>
                                            </span>--}}
                {{--Add Case Hearing--}}
                <span>
                                            <a href="{!! route('backend.legal.case.hearing.create', $cases->id)!!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-plus"></i>&nbsp;@lang('buttons.backend.legal.case.add_hearing')
                                            </a>
                                            </span>
                {{--edit Case--}}
                <span>
						                     <a href="{!! route('backend.legal.case.edit', $cases->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>
						                    </span>
                {{--Close Case--}}

                <span>
                                                {!! HTML::decode(link_to_route('backend.legal.case.close', "<i class='icon fa fa-folder-o' aria-hidden='true'></i>&nbsp;" . trans('buttons.backend.legal.case.close'), [$cases->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.legal.closecase_confirm'), 'class' => 'btn btn-primary site-btn nav_button']))  !!}
						                    </span>
                {{--Delete Case--}}
                <span>
                                           {!! HTML::decode(link_to_route('backend.legal.case.destroy', "<i class='icon fa fa-trash'></i>&nbsp;" . trans('buttons.general.crud.delete'), $cases->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.legal.confirm_case_delete'), 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                         </span>
            </div>
        </div>
    </div>
</div>