@extends('layouts.backend.main', ['title' => trans('labels.backend.legal.index'), 'header_title' => trans('labels.backend.legal.index')])

@push('after-styles-end')
<style>
    .next_proceeding:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.legal.next_proceeding')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .next_proceeding {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@include('backend.includes.datatable_assets')

@section('content')

    {{--<section id="content-wrapper">--}}
    {{--<div style="color:#fff">--}}
    <div class="row">
        <div class="col-md-12 " >
            <div class="class pull-right">
                <a href="{!! route('backend.legal.case.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;Add Case</a>
            </div>
        </div>
    </div>

    <div class="next_proceeding">
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <table class="display" cellspacing="0" width="100%" id ="next-proceeding-table">
                    <thead>
                    <tr >
                        <th>@lang('labels.backend.table.case_title')</th>
                        <th>@lang('labels.backend.table.hearingdate')</th>
                        <th>@lang('labels.backend.table.lawyer')</th>
                        <th>@lang('labels.backend.table.case_fee')</th>
                        <th>@lang('labels.backend.table.status')</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <legend></legend>
    <br/>

    <div class = "row">
        <div class="col-md-12" >
            <div>&nbsp;</div>

            <table class="display" cellspacing="0" width="100%" id ="cases-table"  >
                <thead>
                <tr >
                    <th>@lang('labels.backend.table.case_no')</th>
                    <th>@lang('labels.backend.table.case_title')</th>
                    <th>@lang('labels.backend.table.complainant')</th>
                    <th>@lang('labels.backend.table.respondent')</th>
                    <th>@lang('labels.backend.table.court_name')</th>
                    <th>@lang('labels.backend.table.case_fee')</th>
                    <th>@lang('labels.backend.table.location')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@stop

@push('after-script-end')

<script  type="text/javascript">

    $(function() {
        $('#cases-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.legal.case.get') !!}',
                type : 'post'

            },
            columns: [
                { data: 'number', name: 'number'},
                { data: 'title' , name: 'title'},
                { data: 'complainant', name: 'complainant'},
                { data: 'respondent', name: 'respondent' },
                { data: 'court', name: 'court' },
                { data: 'fee', name: 'fee' },
                { data: 'location', name: 'location' }

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/legal/case/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });

        $('#next-proceeding-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.legal.case.hearing.get_next') !!}',
                type : 'post'

            },
            columns: [
                { data: 'case_title', name: 'case_title'},
                { data: 'hearing_date', name: 'hearing_date'},
                { data: 'lawyer_id' , name: 'lawyer_id'},
                { data: 'fee', name: 'fee'},
                { data: 'status', name: 'status' }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/legal/case/hearing/" + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });

    });

</script>;

@endpush
