@extends('layouts.backend.main', ['title' => trans('labels.general.legal_header'), 'header_title' => trans('labels.general.legal_header')])


@section('content')

    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.legal.case.index') !!}">
                        <li  class="border-less" class="list-group-item" >
                        <h6 class="list-group-item-heading"><i class="icon fa fa-archive"> </i>
                            <large>&nbsp;&nbsp;@lang('labels.backend.legal.management')
                            </large>
                        </h6>
                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text">@lang('labels.backend.legal.management_helper')</p> </li>
                    </a>
                </ul>

                {{--item2--}}
            </div>
        </div>


        {{--right div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.legal.lawyer.index") !!}">
                        <li  class="border-less" class="list-group-item" >
                            <h6 class="list-group-item-heading"><i class="icon fa fa-users"> </i>
                                <large>&nbsp;&nbsp;@lang('labels.backend.legal.title.lawyers')
                                </large>
                            </h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text">@lang('labels.backend.legal.title.lawyers_helper')</p> </li>
                    </a>
                </ul>

            </div>
        </div>




    </div>
@endsection