We conducted inspection on the account of {{ $employername }} on {!! $inspection_date !!}. Our examination has revealed that the employer is compliant for the period from {{ $assessment_start }} to {{ $assessment_end }}.
<br/>
In view of above, may you please approve issuance of Letter of Compliance to {{ $employername }} as per attached Inspection Report.