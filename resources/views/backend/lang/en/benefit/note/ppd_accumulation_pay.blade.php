Claim assessment in respect of {{ $employee }} recommends an award of TZS {{ $ppd }} for Permanent Partial Disablement (PPD) which is paid on monthly basis where the first payment is TZS {{ $accumulation }} being the accumulation of {{ $accumulation_length }} monthly pension from {{ $mmi_date }} to {{ $end_month }} {{ $mae[0] Or "" }}.
<br/>
Your approval is sought to pay TZS {{ $accumulation }} being an accumulation  to {{ $employee }} being PPD.
