Claim assessment in respect of {{ $employee }} recommends an award of TZS {{ $tpd }} for Temporary Partial Disablement (TPD) {{ $mae[0] or "" }}.
<br/>
Your approval is sought to pay TZS {{ $td_lumpsum }} to the employer {{ $employer }} being TPD {{ $mae[1] or "" }} who will pay TZS {{ $td_total }} to {{ $employee }} if he was not paid salary during the healing period {{ $mae[2] or "" }}.
