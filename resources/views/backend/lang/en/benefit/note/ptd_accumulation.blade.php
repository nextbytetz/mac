Claim assessment in respect of {{ $employee }} recommends an award of TZS {{ $ptd }} for Permanent Total Disablement (PTD) which is paid on monthly basis where the first payment will be the accumulation of pension payment from {{ $mmi_date }} to the date when the next pensioner payroll will run {{ $mae[0] Or "" }}.
<br/>
Your approval is sought to pay TZS {{ $ptd }} to {{ $employee }} being PTD {{ $mae[2] Or "" }}.
