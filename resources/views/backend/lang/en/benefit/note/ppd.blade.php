Claim assessment in respect of {{ $employee }} recommends an award of TZS {{ $ppd }} for Permanent Partial Disablement (PPD) {{ $mae[0] Or "" }}.
<br/>
Your approval is sought to pay TZS {{ $pd_lumpsum }} to {{ $employee }} being PPD {{ $mae[2] Or "" }}.
