Claim assessment in respect of {{ $employee }} recommends an award of TZS {{ $ttd }} for Temporary Total Disablement (TTD) and TZS {{ $ppd }} for Permanent Partial Disablement (PPD) {{ $mae[0] or "" }}.
<br/>
Your approval is sought to pay TZS {{ $td_lumpsum }} to the employer {{ $employer }} being TTD {{ $mae[1] or "" }} who will pay TZS {{ $td_total }} to {{ $employee }} if he was not paid salary during the healing period and TZS {{ $pd_lumpsum }} to {{ $employee }} being PPD {{ $mae[2] or "" }}.
