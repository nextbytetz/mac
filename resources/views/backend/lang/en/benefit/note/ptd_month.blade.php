Claim assessment in respect of {{ $employee }} recommends an award of TZS {{ $ptd }} for Permanent Total Disablement (PTD) which is paid on monthly basis {{ $mae[0] Or "" }}.
<br/>
Your approval is sought to pay TZS {{ $ptd }} to {{ $employee }} being PTD {{ $mae[2] Or "" }}.
