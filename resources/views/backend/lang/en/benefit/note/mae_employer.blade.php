Claim assessment in respect of {{ $employee }} recommends an award of TZS {{ $mae }} for Medical Aid Expenses (MAE).
<br>
Your approval is sought to pay TZS {{ $mae }} to the employer {{ $employer }} being MAE.
