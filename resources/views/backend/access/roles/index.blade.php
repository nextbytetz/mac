@extends('layouts.backend.main', ['title' => trans('labels.backend.role.title'), 'header_title' => trans('labels.backend.role.title')])

@include('backend.includes.datatable_assets')

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="{!! route('backend.role.create') !!}"   class="btn btn-primary save_button" >&nbsp;@lang('labels.backend.role.create')  &nbsp; <i class="icon fa fa-plus-circle"></i></a>
            </div>
        </div>
    </div>
    <br/>
    <table class="display" cellspacing="0" width="100%" id ="roles_table"  >
        <thead>
        <tr >
            <th>@lang('labels.general.name')</th>
            <th>{!! trans_choice('labels.general.description', 2) !!}</th>
            <th>@lang('labels.backend.role.permissions')</th>
        </tr>
        </thead>
    </table>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        var table = $('#roles_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:  '{{ route("backend.role.get") }}',
            columns: [
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'permissions', name: 'permissions', "searchable": false},
                {data: 'id', "visible": false},
            ],
            order: [[0, "asc"]],
            searchDelay: 500,
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = "role/" + aData['id'] + "/edit";
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            },
        });

    });
</script>

@endpush