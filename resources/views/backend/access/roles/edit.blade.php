@extends('layouts.backend.main', ['title' => trans('labels.backend.role.title'), 'header_title' => trans('labels.backend.role.view')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="switch-box">
                <label class="switch-button switch-light-button">
                    <input class="switch-input-button primary-switch" type="checkbox" />
                    <span class="switch-label-button primary-switch" data-on="@lang('labels.general.edit_on')" data-off="@lang('labels.general.edit_off')"></span> <span class="switch-handle-button "></span>
                </label>
            </div>

            {{--<button type="button" class="btn btn-secondary btn-round-left">@lang('buttons.general.crud.edit')</button>--}}
            {{--<button type="button" class="btn btn-danger btn-round-right">@lang('buttons.general.crud.delete')</button>--}}
            {{ link_to_route('backend.role.destroy', trans('buttons.general.crud.delete'), [$role->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.role.delete'), 'class' => 'btn btn-danger btn-round-right']) }}
        </div>
    </div>

    <!-- Put the page specifically for this page here -->
    {!! Form::model($role, ['route' => ['backend.role.update', $role->id], 'id' => 'edit_role', 'method' => 'PATCH']) !!}
    <div class="all-form-section">
        <div class="row">

            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                    {!! Form::label('name', trans('labels.general.name')) !!}
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('labels.general.name')]) !!}
                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                    {!! Form::label('description', trans_choice('labels.general.description', 1)) !!}
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {!! Form::textarea('description', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                        {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="alert alert-success">
                        <i class="fa fa-info-circle"></i>
                        {!! getLanguageBlock('backend.lang.access.associated-permissions-explanation') !!}
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p><strong>@lang('labels.backend.role.grouped_permissions')</strong></p>
                            @if ($groups->count())
                                <div id="permission-tree">
                                    <ul>
                                        @foreach ($groups as $group)
                                            <li>{!! $group->name !!}
                                                @if ($group->permissions()->count())
                                                    <ul>
                                                        @foreach ($group->permissions as $permission)
                                                            <li id="{!! $permission->id !!}" data-dependencies='{!! json_encode($permission->dependencies->pluck('dependency_id')->all()) !!}' data-backdependancies='{!! json_encode($permission->backDependencies->pluck('permission_id')->all()) !!}'>

                                                                @if ($permission->dependencies()->count())
                                                                    <?php
                                                                    //Get the dependency list for the tooltip
                                                                    $dependency_list = [];
                                                                    foreach ($permission->dependencies as $dependency)
                                                                        array_push($dependency_list, $dependency->permission->display_name);
                                                                    $dependency_list = implode(", ", $dependency_list);
                                                                    ?>
                                                                    <a data-toggle="tooltip" data-html="true" title="@lang('labels.backend.role.dependencies') : {!! $dependency_list !!}">{!! $permission->display_name !!} <small><b>(D)</b></small></a>
                                                                @else
                                                                    {!! $permission->display_name !!}
                                                                @endif

                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @else
                                <p>@lang('labels.backend.role.no_groups')</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {!! Form::hidden('permissions') !!}
    {!! Form::hidden('role_id', $role->id) !!}

    <div id="edit_buttons">
        <hr/>
        {{--
                <div class="pull-left">
                    <a href="{{route('backend.role.index')}}" class="btn btn-danger btn-sm cancel_button">@lang('buttons.general.cancel')</a>
                </div>
        --}}
        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-sm" value="@lang('buttons.general.crud.update')" />
        </div>
        <div class="clearfix"></div>
    </div>

    {!! Form::close() !!}
@endsection

@push('after-script-end')

{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/access/role.js") }}
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        autosize($("textarea.autosize"));
        $("#edit_buttons").hide();
        $("#edit_role :input").prop("disabled", true);

        tree_enable_state('disable_node');

        $(".switch-input-button").change(function() {
            if(this.checked) {
                /* Checkbox is checked */
                $("#edit_buttons").show();
                $("#edit_role :input").prop("disabled", false);
                tree_enable_state('enable_node');
            } else {
                /* Checkbox is unchecked */
                $("#edit_buttons").hide();
                $("#edit_role :input").prop("disabled", true);
                tree_enable_state('disable_node');
            }
        });
        @foreach ($role_permissions as $permission)
            $('#permission-tree').jstree('check_node', '#{!! $permission !!}');
        @endforeach

        @if (isset($errors) && $errors->any())
            $(".switch-input-button").prop('checked', true);
            $(".switch-input-button").trigger('change');
        @endif

    });

    function tree_enable_state(action) {
        var perm_tree = $('#permission-tree').jstree(true).get_json('#', { 'flat': true });
        $.each(perm_tree, function( key, value ) {
            $('#permission-tree').jstree(action, value.id);
        });
    }
</script>

{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}

@endpush