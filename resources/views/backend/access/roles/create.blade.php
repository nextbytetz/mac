@extends('layouts.backend.main', ['title' => trans('labels.backend.role.title'), 'header_title' => trans('labels.backend.role.create')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {!! Form::open(['route' => 'backend.role.store', 'id' => 'create_role']) !!}
    <div class="all-form-section">
        <div class="row">

            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                    {!! Form::label('name', trans('labels.general.name')) !!}
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('labels.general.name')]) !!}
                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                    {!! Form::label('description', trans_choice('labels.general.description', 1)) !!}
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {!! Form::textarea('description', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                        {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="alert alert-success">
                        <i class="fa fa-info-circle"></i>
                        {!! getLanguageBlock('backend.lang.access.associated-permissions-explanation') !!}
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p><strong>@lang('labels.backend.role.grouped_permissions')</strong></p>
                            @if ($groups->count())
                                <div id="permission-tree">
                                    <ul>
                                        @foreach ($groups as $group)
                                            <li>{!! $group->name !!}
                                                @if ($group->permissions()->count())
                                                    <ul>
                                                        @foreach ($group->permissions as $permission)
                                                            <li id="{!! $permission->id !!}" data-dependencies='{!! json_encode($permission->dependencies->pluck('dependency_id')->all()) !!}' data-backdependancies='{!! json_encode($permission->backDependencies->pluck('permission_id')->all()) !!}'>

                                                                @if ($permission->dependencies()->count())
                                                                    <?php
                                                                    //Get the dependency list for the tooltip
                                                                    $dependency_list = [];
                                                                    foreach ($permission->dependencies as $dependency)
                                                                        array_push($dependency_list, $dependency->permission->display_name);
                                                                    $dependency_list = implode(", ", $dependency_list);
                                                                    ?>
                                                                    <a data-toggle="tooltip" data-html="true" title="@lang('labels.backend.role.dependencies') : {!! $dependency_list !!}">{!! $permission->display_name !!} <small><b>(D)</b></small></a>
                                                                @else
                                                                    {!! $permission->display_name !!}
                                                                @endif

                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @else
                                <p>@lang('labels.backend.role.no_groups')</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {!! Form::hidden('permissions') !!}
    <hr/>
    <div class="pull-left">
        <a href="{{route('backend.role.index')}}" class="btn btn-danger btn-sm cancel_button">@lang('buttons.general.cancel')</a>
    </div>

    <div class="pull-right">
        <input type="submit" class="btn btn-success btn-sm" value="@lang('buttons.general.crud.create')" />
    </div>
    <div class="clearfix"></div>
    {!! Form::close() !!}
@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/access/role.js") }}
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        autosize($("textarea.autosize"));
        @if ((old('permissions')))
        <?php $permissions = old('permissions'); $perms = explode( ",", $permissions); ?>
        @foreach ($perms as $permission)
        @if (is_numeric($permission))
	    $('#permission-tree').jstree('check_node', '#{!! $permission !!}');
        @endif
        @endforeach
        @endif
    });
</script>

@endpush