@extends('layouts.backend.main', ['title' => trans('labels.backend.user.title'), 'header_title' => trans('labels.backend.user.view')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

@include("backend/access/includes/menu", ['user'=> $user])

<div class="row">
    {{--HEADER--}}
    @include("backend/access/includes/header_info", ['user'=> $user])
    <div>&nbsp;<br/></div>
</div>
<h4 class="page-content-title">@lang('labels.backend.user.info')</h4>&nbsp;&nbsp;<small>{!! getLanguageBlock('backend.lang.edit-fields') !!}</small>
{!! Form::model($user, ['route' => ['backend.user.update', $user->id], 'method' => 'PATCH']) !!}
<div class="row">
    <div class="col-md-4">
        {{--<div class="divider15"></div>--}}
        <div class="personal-info-box">
            <div class="row">
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">@lang('labels.backend.user.username') :</div>
                    <div class="detail-profile">{!! $user->username !!}</div>
                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">@lang('labels.backend.user.firstname') :</div>
                    <div class="detail-profile">{!! $user->firstname !!}</div>
                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">@lang('labels.backend.user.middlename') :</div>
                    <div class="detail-profile">{!! $user->middlename !!} &nbsp;&nbsp;</div>
                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">@lang('labels.backend.user.lastname') :</div>
                    <div class="detail-profile">{!! $user->lastname !!}</div>
                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">Phone:</div>
                    <div class="detail-profile">   {!! Form::text('phone', null, ['class' => 'form-control']) !!}</div>
                    {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        {{--<div class="divider15"></div>--}}
        <div class="personal-info-box">
            <div class="row">
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">@lang('labels.backend.user.email') <i style="color: red">*</i> :</div>
                    <div class="detail-profile">
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">Office <i style="color: red">*</i> :</div>
                    <div class="detail-profile">
                        {!! Form::select('office_id', $offices, null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                        {!! $errors->first('office_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">@lang('labels.backend.user.unit') <i style="color: red">*</i> :</div>
                    <div class="detail-profile">
                        {!! Form::select('unit_id', $units, null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                        {!! $errors->first('unit_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">@lang('labels.backend.user.designation') <i style="color: red">*</i> :</div>
                    <div class="detail-profile">
                        {!! Form::select('designation_id', $designations, null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                        {!! $errors->first('designation_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>

                </div>
                <div class="user-name-profile col-md-12 col-xs-12">
                    <div class="name-profile">External ID  :</div>
                    <div class="detail-profile">
                        {!! Form::text('external_id', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('external_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">

     <div class="user-name-profile col-md-12 col-xs-12">
        <div class="name-profile">Report To <i style="color: red">*</i> :</div>
        <div class="detail-profile">
            {!! Form::select('report_to', $alternative_approvers, $user->report_to, ['class' => 'supervisor-select', 'style' => 'width:100%', 'placeholder' => '','id'=>'report_to']) !!}
            {!! $errors->first('report_to', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>

    <div class="user-name-profile  col-md-12 col-xs-12">
        <div class="fileld-layout">
            <label>Alternative Approvers</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('alternative_approvers[]', $alternative_approvers, $alternative_approvers_values, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'alternative_approvers']) !!}
                </div>
                <span class="help-block">
                    <small class="form-text text-muted" style="width:100% !important;">Can select multiple users</small>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="element-form">
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                {!! Form::label('role', trans('labels.backend.user.role')) !!} &nbsp;:
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                @if (count($roles) > 0)
                @foreach($roles as $role)
                <input type="checkbox" value="{{$role->id}}" name="assignees_roles[]" {{in_array($role->id, $user_roles) ? 'checked' : ''}} id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{!! $role->name !!}</label>
                <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                    (
                    <span class="show-text">{{ trans('labels.general.show') }}</span>
                    <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                    {{ trans('labels.backend.user.permissions') }}
                    )
                </a>
                <br/>
                <div class="permission-list hidden" data-role="role_{{$role->id}}" style="margin-bottom: 3px;margin-top: 2px">
                    @if ($role->permissions()->count() > 0)
                    <div class="wrapper">
                        <div class="content-blockquote">
                            <div class="simple-blockquote-section">
                                <blockquote class="small">
                                    <p>
                                        @foreach ($role->permissions as $perm)
                                        {{$perm->display_name}} <br/>
                                        @endforeach
                                    </p>

                                </blockquote>
                            </div>
                        </div>
                    </div>
                    @else
                    {{ trans('labels.backend.user.no_permissions') }}<br/><br/>
                    @endif
                </div><!--permission list-->
                @endforeach
                @else
                {{ trans('labels.backend.user.no_roles') }}
                @endif
            </div>
        </div>
    </div>
    <br/>

</div>
</div>
<div class="row">
    <div class="col-md-1 text-xs-right">
        {!! Form::label('other_permission', trans('labels.backend.user.other_permission')) !!}
    </div>
    <div class="col-md-11">
        <div class="alert alert-success">
            <i class="fa fa-info-circle"></i>
            {!! Form::label('permission_check', trans('labels.backend.user.permission_check')) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @if (count($permissions))
        @foreach (array_chunk($permissions->toArray(), count($permissions)/4) as $perm)
        <div class="col-lg-3">
            <ul style="margin:0;padding:0;list-style:none;">
                @foreach ($perm as $p)
                <?php
                                //Since we are using array format to nicely display the permissions in rows
                                //we will just manually create an array of dependencies since we do not have
                                //access to the relationship to use the pluck() function of eloquent
                                //but the relationships are eager loaded in array format now
                $dependencies = [];
                $backdependencies = [];
                $dependency_list = [];
                if (count($p['dependencies'])) {
                    foreach ($p['dependencies'] as $dependency) {
                        array_push($dependencies, $dependency['dependency_id']);
                        array_push($dependency_list, $dependency['permission']['display_name']);
                    }
                }
                if (count($p['back_dependencies'])) {
                    foreach ($p['back_dependencies'] as $backdependency) {
                        array_push($backdependencies, $backdependency['back_permission']['id']);
                    }
                }
                $dependencies = json_encode($dependencies);
                $backdependencies = json_encode($backdependencies);
                $dependency_list = implode(", ", $dependency_list);
                ?>
                <li><input type="checkbox" value="{{$p['id']}}" name="permission_user[]" data-dependencies='{!! $dependencies !!}' data-backdependencies='{!! $backdependencies !!}' {{in_array($p['id'], $user_permissions) ? 'checked' : ""}} id="permission-{{$p['id']}}" /> <label for="permission-{{$p['id']}}">

                    @if ($p['dependencies'])
                    <a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="{{ trans('labels.backend.role.dependencies') }}: {!! $dependency_list !!}">{!! $p['display_name'] !!} <small><strong>(D)</strong></small></a>
                    @else
                    {!! $p['display_name'] !!}
                    @endif

                </label></li>
                @endforeach
            </ul>
        </div>
        @endforeach
        @else
        {{ trans('labels.backend.user.no_other_permissions') }}
        @endif
    </div>
</div>
<hr/>
{!! Form::hidden('user_id', $user->id) !!}
<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="@lang('buttons.general.crud.update')" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}

@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/js/backend/access/permission.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/access/user.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<!-- Custom javascript files for this page -->
<script>
    $(function () {
       $(".supervisor-select").select2({
        placeholder: "",
        allowClear: true,
        });

       @if ((old('permission_user')))
       <?php $permission_user = old('permission_user'); ?>
       var $permission_user = <?= json_encode($permission_user); ?>;
       $.each($permission_user, function (index, value) {
        $("input[name='permission_user[]'][value=" + value + "]").prop("checked", true);
    });
       @endif

       @if ((old('assignees_roles')))
       <?php $assignees_roles = old('assignees_roles'); ?>
       var $assignees_roles = <?= json_encode($assignees_roles); ?>;
       $.each($assignees_roles, function (index, value) {
        $("input[name='assignees_roles[]'][value=" + value + "]").prop("checked", true);
    });
       @endif
   });
</script>

@endpush