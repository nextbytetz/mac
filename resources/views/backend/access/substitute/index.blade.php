@extends('layouts.backend.main', ['title' => "My Substitute", 'header_title' => "My Substitute"])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>

</style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}
        @include("backend/access/includes/header_info",['user'=> $user])
        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.users.substitute.post'], 'name' => 'substitute_user', 'class' => 'substitute_user']) !!}
        {!! Form::hidden('user_id', $user->id) !!}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="fileld-layout">
                        <label class="required">Reason for Substituting User</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::textarea('reason', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                            </div>
                            <span class="help-block" style="width:100% !important;">
                                <p>Specify reason(s) for substituting user</p>
                                {!! $errors->first('reason', '<p class="help-block label label-danger">:message</p>') !!}
                            </span>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="filed-layout">
                        <label class="required">User</label>
                        <div class="form-group">
                            {!! Form::select('sub', $users, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                            <span class="help-block" style="width:100% !important;">
                                <p>User to substitute</p>
                                {!! $errors->first('sub', '<p class="help-block label label-danger">:message</p>') !!}
                            </span>
                        </div>
                    </div>
                </div>

            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <input type="submit" class="btn btn-success btn-block btn-submit" value="Update" />
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
<script>
    $(function() {
        $(".search-select").select2({});
        autosize($("textarea.autosize"));
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form.substitute_user', function (e) {
            e.preventDefault();
            var $form = this;
            swal({
                title: "Warning",
                text: "Are you sure to place selected user as substitute?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function (confirmed) {
                if (confirmed) {
                    $form.submit();
                }
            });
        });
    });
</script>
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
@endpush