@extends('layouts.backend.main', ['title' => trans('labels.backend.user.title'), 'header_title' => trans('labels.backend.user.title')])

@include('backend.includes.datatable_assets')

@section('content')
    <!-- Put the page specifically for this page here -->
    <br/>
    <table class="display" cellspacing="0" width="100%" id ="roles_table"  >
        <thead>
        <tr >
            <th>@lang('labels.backend.user.username')</th>
            <th>@lang('labels.backend.user.firstname')</th>
            <th>@lang('labels.backend.user.lastname')</th>
            <th>@lang('labels.backend.user.role')</th>
        </tr>
        </thead>
    </table>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        var table = $('#roles_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:  '{{ route("backend.users.get") }}',
            columns: [
                {data: 'username', name: 'username'},
                {data: 'firstname', name: 'firstname'},
                {data: 'lastname', name: 'lastname'},
                {data: 'roles', name: 'roles', "searchable": false},
                {data: 'id', "visible": false},
            ],
            order: [[0, "asc"]],
            searchDelay: 500,
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = "user/" + aData['id'] + "/edit";
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            },
        });

    });
</script>

@endpush