<div class="nav_tab_pane_header">
    {{--Header Bar--}}
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
                @if ($user->available)
                {{-- Substitute User--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.users.substitute', "<i class='icon fa fa-recycle' aria-hidden='true'></i>&nbsp;" . "Substitute User", [$user->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                @else
                    {{-- Undo Substitute User--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.users.revoke_substitute', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . "Revoke Substitution", [$user->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to revoke substitution for this user?", 'class' => 'btn btn-primary site-btn nav_button']))  !!}
                    </span>
                @endif
                {{-- Close User--}}
                {!! HTML::decode(link_to_route('backend.user.index', "<i class='icon fa fa-close' aria-hidden='true'></i>&nbsp;" . "Close", [],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
            </div>
        </div>
    </div>
</div>
<br/>