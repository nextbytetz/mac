@extends('layouts.backend.main', ['title' => trans('labels.general.codes'), 'header_title' => trans('labels.general.codes')])


@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush


@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            {{----------Button Add New branch -----------}}

            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="pull-right">--}}
                        {{--<a href="{!! route('backend.system.code.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('buttons.general.add_new')</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div>&nbsp;</div>

            <div class="row">


                <div class="col-md-12">

                    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

                </div>
            </div>



        </div><!-- /.box-header -->

    </div><!--box box-success-->


@endsection

@push('after-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function () {
        $(".search-select").select2();

    });
</script>


@endpush