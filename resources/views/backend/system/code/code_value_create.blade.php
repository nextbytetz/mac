@extends('layouts.backend.main', ['title' => trans('labels.backend.system.create_code_value'), 'header_title' => trans('labels.backend.system.create_code_value')])


@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush


@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            {!! Form::open(['route' => ['backend.system.code.store_value'],'method'=>'post',  'id' => 'create']) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Form::hidden('code_id', $code->id, []) !!}

                    {{-- code--}}
                    <div class="row">
                        <div class="element-form" >
                            <div class="col-md-2 text-xs-right"><label>@lang('labels.general.code'):</label></div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::input( 'text','code', $code->name, ['class' => 'form-control','disabled'=>true ]) !!}

                                </div>
                            </div>

                        </div>
                    </div>
                    {{-- name--}}
                    <div class="row">
                        <div class="element-form" >
                            <div class="col-md-2 text-xs-right"><label>@lang('labels.general.name'):</label><span class="required_asterik">*</span></div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::input( 'text','name', null, ['class' => 'form-control', ]) !!}
                                    {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    {{--Description--}}
                    <div class="row">
                        <div class="element-form" >
                            <div class="col-md-2 text-xs-right"><label>@lang('labels.general.description'):</label></div>


                            <div class="col-md-3">
                                <div class="form-group text_content">
                                    {!! Form::textarea( 'description', null, [ 'id'=> 'description',  'class' =>'form-control']) !!}
                                    {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>
                    {{--Sort--}}
                    <div class="row">
                        <div class="element-form" >
                            <div class="col-md-2 text-xs-right"><label>@lang('labels.general.sort'):</label></div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::input( 'text','sort', null, ['class' => 'form-control', ]) !!}
                                    {!! $errors->first('sort', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    {{--is active--}}
                    <div class="row">
                        <div class="element-form" >
                            <div class="col-md-2 text-xs-right"><label>@lang('labels.general.is_active'):</label><span class="required_asterik">*</span></div>

                            <div class="col-md-3">
                                <div class="form-group">

                                    {!!  Form::select('is_active' , ['0' => 'No', '1' => 'Yes'],'1', ['class' => 'form-control search-select' ,'placeholder'=> '']) !!}
                                    {!! $errors->first('is_active', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    {{--is mandatory--}}
                    {{--<div class="row">--}}
                        {{--<div class="element-form" >--}}
                            {{--<div class="col-md-2 text-xs-right"><label>@lang('labels.general.is_mandatory'):</label><span class="required_asterik">*</span></div>--}}

                            {{--<div class="col-md-3">--}}
                                {{--<div class="form-group">--}}

                                    {{--{!!  Form::select('is_mandatory' , ['0' => 'No', '1' => 'Yes'],'0', ['class' => 'form-control search-select' ,'placeholder'=> '']) !!}--}}

                                    {{--{!! $errors->first('is_mandatory', '<span class="help-block label label-danger">:message</span>') !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}


                </div>
            </div>

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-6" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-4 col-lg-4 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-10 col-lg-10 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">
                                {!! link_to_route('backend.system.code.index',trans('buttons.general.cancel'), [],['id'=> 'replace_submit',  'class' => 'btn btn-primary cancel_button', ]) !!}&nbsp;
                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        </div><!-- /.box-header -->

    </div><!--box box-success-->


@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function () {

        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();


        $(".search-select").select2();

    });


</script>


@endpush