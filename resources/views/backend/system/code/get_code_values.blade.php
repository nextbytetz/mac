@extends('layouts.backend.main', ['title' => trans('labels.general.code_values'), 'header_title' => trans('labels.general.code_values')])


@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush



@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="client-title">
                        <i class="icon-circle"></i>
                        <!-- code header detail -->
                        <strong><label>@lang('labels.general.code'):</label> {!! Form::label( 'name', $code->name  , [ 'id'=> 'name']) !!}</strong>
                        <legend></legend>
                    </h5>
                </div>


                <div class="col-md-12">
                    <div class="pull-right">
                        <a href="{!! route('backend.system.code.create_value', $code->id) !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('buttons.general.add_new')</a>
                    </div>
                </div>
                <div>&nbsp;</div>


            </div>

            <div class="row">



                <div class="col-md-12">



                    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}





                </div>
            </div>



        </div><!-- /.box-header -->

    </div><!--box box-success-->


@endsection

@push('after-script-end')
{!! $dataTable->scripts() !!}

{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}


<script  type="text/javascript">
    $(function () {
        $(".search-select").select2();

    });
</script>

@endpush