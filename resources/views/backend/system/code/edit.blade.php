@extends('layouts.backend.main', ['title' => trans('labels.backend.system.edit_code'), 'header_title' => trans('labels.backend.system.edit_code')])


@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush



@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            {!! Form::model($code,['route' => ['backend.system.code.update',$code->id],'method'=>'put',  'id' => 'update']) !!}
            <div class="row">
                <div class="col-md-12">

                    {{--Name--}}
                    <div class="row">
                        <div class="element-form" >
                            <div class="col-md-2 text-xs-right"><label>@lang('labels.general.name'):</label><span class="required_asterik">*</span></div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::input( 'text','name', null, ['class' => 'form-control', ]) !!}
                                    {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>




                    {{--Buttons--}}
                    <div class="row">
                        <div class="col-md-6" class="form-inline" >
                            <div class="element-form">
                                <div class="col-xl-4 col-lg-4 col-sm-12 col-md-3 text-xs-right"></div>
                                <div class="col-xl-10 col-lg-10 col-sm-12 col-md-9 col-xs-12">
                                    <div class="pull-right">
                                        {!! link_to_route('backend.system.code.index',trans('buttons.general.cancel'), [],[  'class' => 'btn btn-primary cancel_button', ]) !!}&nbsp;
                                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary save_button', 'type'=>'submit']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div><!-- /.box-header -->

            </div><!--box box-success-->


            @endsection

            @push('after-script-end')
            {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
            <script  type="text/javascript">
                $(function () {
                    $(".search-select").select2();

                });
            </script>


    @endpush