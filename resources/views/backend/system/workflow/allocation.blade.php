@extends('layouts.backend.main', ['title' => "Workflow Allocation", 'header_title' => "Workflow Allocation"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    <style>
        .custom_filter:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "@lang('labels.backend.system.workflow.custom_filter')";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }

        .custom_filter {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }

    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    @include("backend/system/workflow/pending_filter")

    <legend></legend>
    <br/>
    @permission("workflow_allocation")
    {{--Assign to--}}
    <div class="row">
        <div class="col-md-12">
            <div class="form-row">
                {!! Form::open(['role' => 'form', 'id' => 'assign-user', 'route' => 'backend.workflow.assign_allocation']) !!}
                <div class="form-group offset-md-5 col-md-5 assign_user_select">
                    <label for="assigned_user">Assign To:</label>
                    {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
                </div>
                <div class="form-group col-md-2">
                    <label for="allocate_submit">&nbsp;</label>
                    <input type="submit" class="form-control btn btn-success btn-sm" value="Submit" id="allocate_submit">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endauth

    @include("backend/system/workflow/allocation_table")

@endsection

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    <!-- Custom javascript files for this page -->
    <!-- Custom javascript files for this page -->
    @include("backend/system/workflow/pending_script")
    @include("backend.system.workflow.includes.allocation_datatable")
@endpush