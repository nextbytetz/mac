<div class="custom_filter">
    {!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="module">@lang('labels.backend.system.workflow.module')&nbsp;:</label>
                    {!! Form::select('wf_module_id', $wf_modules, null, ['class' => 'form-control wf-module-select', 'placeholder' => '', 'data-group' => '', 'id' => 'module']) !!}
                </div>
                {{-- start: Choose filter for workflow module group: Claim & Notification Processing, Notification Rejection --}}
                <div class="form-group col-md-3 notification_select">
                    <label for="incident">Incident Type</label>
                    {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident']) !!}
                </div>
                <div class="form-group col-md-4 notification_select">
                    <label for="employee">Employee Name</label>
                    {!! Form::select('employee', [], null, ['class' => 'form-control employee-select', 'id' => 'employee']) !!}
                </div>
                {{-- end: Choose filter for workflow module group: Claim & Notification Processing, Notification Rejection --}}

                {{-- start: Choose filter for workflow module group: Interest Waiving, Interest Adjustment, Employer Registration, Online Employer Verification --}}
                <div class="form-group col-md-4 employer_select">
                    <label for="employer">Employer Name</label>
                    {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                </div>
                {{-- end: Choose filter for workflow module group: Interest Waiving, Interest Adjustment, Employer Registration, Online Employer Verification --}}

                {{-- start: Choose filter for workflow module group: Contribution Receipt, Contribution Receipt Legacy --}}
                <div class="form-group col-md-2 receipt_select">
                    <label for="operator">Receipt # Comparator:</label>
                    <span>{!! Form::select('operator', ['=' => '=', 'like' => 'like'], ['='], ['class' => 'form-control search-select', 'placeholder' => '', 'id' => 'operator']) !!}</span>
                </div>
                <div class="form-group col-md-2 receipt_select">
                    <label for="search"># of Receipt :</label>
                    <span>{!! Form::text('search', null, ['class' => 'form-control', 'style' => "border-radius:3px;width:80%;", 'id' => 'search']) !!}</span>
                </div>
                {{-- end: Choose filter for workflow module group: Contribution Receipt, Contribution Receipt Legacy --}}

                {{--Start: choose filter for workflow module group payroll processing i.e. payroll status change, bank modification and payroll recovery processing--}}
                <div class="form-group col-md-4 payroll_select">
                    <label for="member_type">Member Type</label>
                    {!! Form::select('member_type', [ '0' => '', '4' => 'Dependent', '5' => 'Pensioner'],null, ['class' => 'form-control search-select', 'id' => 'member_type_id']) !!}
                </div>
                <div class="form-group col-md-4 payroll_select " id="dependent_div">
                    <label for="dependent">Dependent</label>
                    {!! Form::select('dependent', [], null, ['class' => 'form-control dependent-select', 'id' => 'dependent_id']) !!}

                </div>
                <div class="form-group col-md-4 payroll_select " id =pensioner_div>
                    <label for="pensioner">Pensioner</label>
                    {!! Form::select('pensioner', [], null, ['class' => 'form-control pensioner-select', 'id' => 'pensioner_id']) !!}
                </div>
                {{--end: choose filter for workflow module group payroll processing i.e. payroll status change, bank modification and payroll recovery processing--}}

                {{-- start: Choose filter for statuses & users --}}
                @if ($state == "all" || $state == "attended" || $state == "full" || $state == "archived")
                    <div class="form-group col-md-3 status_select">
                        <label for="status">Status:</label>
                        {!! Form::select('status', $statuses, null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                    </div>

                    <div class="form-group col-md-3 user_select">
                        <label for="user">User:</label>
                        {!! Form::select('user_id', $users, null, ['class' => 'form-control search-select', 'id' => 'user']) !!}
                    </div>
                @endif
                {{-- end: Choose filter for statuses & users --}}

            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
</div>