@if($archives)
    @if ($archives->count())
        <div class="row">
            <div class="col-md-12">
                <div class="grid-column  content-list">
                    <div class="list-group message-list-group">
                        @foreach($archives as $archive)
                            <div class="list-group-item">
                                <div class="list-message float-xs-left">
                                    <span class="float-xs-right">{{ $archive->period_days_label }}&nbsp;day(s)</span>
                                    <h6>Archive Description @if (!$archive->restore_date) &nbsp;&nbsp;{!! link_to_route('backend.workflow.edit_archive_workflow', 'Edit', [$archive->id], ['class' => 'btn btn-secondary btn-sm site-btn', ]) !!} @endif</h6>
                                    <p>{!! $archive->description !!}</p>
                                    <p><small>From Date&nbsp;:&nbsp;<span class="underline">{{ $archive->from_date }}</span> &nbsp;&nbsp;,&nbsp;&nbsp; To Date&nbsp;:&nbsp;<span class="underline">{{ $archive->to_date }}</span> </small></p>
                                    <p><small>User Archived&nbsp;:&nbsp;<span class="underline">{{ $archive->user->name }}</span></small></p>
                                    <p><small>Archive Reason&nbsp;:&nbsp;<span class="underline">{{  ($archive->reason()->count()) ? $archive->reason->name : '' }}</span></small></p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <br/>
    @endif
@endif