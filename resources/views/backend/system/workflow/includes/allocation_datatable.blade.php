<script>
    $(function(){


        let $oTable = $('#allocation-workflow-table').DataTable({
            //dom : 'Bfrtip',
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                $oTable.buttons().container()
                    .insertBefore('#allocation-workflow-table');
            },
            processing: true,
            serverSide: true,
            info : true,
            ajax: {
                url: "{!! route("backend.workflow.pending.get") !!}",
                data: function (d) {
                    d.wf_module_id = $('select[name=wf_module_id]').val();
                    d.wf_module_group_id = $wf_module_group_id;
                    d.operator = $('select[name=operator]').val();
                    d.incident = $('select[name=incident]').val();
                    d.search = $('input[name=search]').val();
                    d.state = "{{ $state }}";
                    @if ($state == "all" Or $state == "attended" Or $state == "full")
                        d.status = $('select[name=status]').val();
                    d.user_id = $('select[name=user_id]').val();
                    d.member_type_id = $('select[name=member_type]').val();
                    d.pensioner_id = $('select[name=pensioner]').val();
                    d.dependent_id = $('select[name=dependent]').val();
                    @endif
                }
            },
            columnDefs: [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ],
            select: {
                'style': 'multi'
            },
            columns: [
                {
                    orderable: false,
                    searchable: false,
                    data: 'id',
                    name : 'wf_tracks.id'
                },
                {data: 'resource_name', name: 'resource_name', searchable: false, orderable: false},
                {data: 'receive_date_formatted', name: 'receive_date'},
                {data: 'level', name: 'level'},
                {data: 'module', name: 'wf_modules.name'},
                {data: 'assign_status', name: 'assigned', searchable: false},
                {data: 'user', name: 'firstname', searchable: false},
                {data: 'resource_id', name: 'resource_id', visible: false, searchable: false, orderable: false},
                {data: 'module_group_id', name: 'wf_module_groups.id', visible: false, searchable: false, orderable: false},
                {data: 'module_id', name: 'wf_modules.id', visible: false, searchable: false, orderable: false},
            ],
            'rowCallback': function ($nRow, $aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:first-child)', $nRow).click(function() {
                    switch($aData['module_group_id']) {
                        case '1':
                            /* Interest Waiving */
                            /*document.location.href =  base_url + "/compliance/employer/profile/interest_write_off_approve/" + aData['resource_id'];*/
                            window.open(base_url + "/compliance/employer/profile/interest_write_off_approve/" + $aData['resource_id'], "_self");
                            break;
                        case '2':
                            /*Interest Adjustment*/
                            /*document.location.href =  base_url + "/compliance/employer/profile/interest/" + aData['resource_id'];*/
                            window.open(base_url + "/compliance/employer/profile/interest/" + $aData['resource_id'], "_self");
                            break;
                        case '3':
                            /*Claim & Notification Processing*/
                            /*document.location.href =  base_url + "/claim/notification_report/profile/" + aData['resource_id'];*/
                            window.open(base_url + "/claim/notification_report/profile/" + $aData['resource_id'], "_self");
                            break;
                        case '4':
                            /* Notification Rejection */
                            /*document.location.href =  base_url + "/claim/notification_report/profile/" + aData['resource_id'];*/
                            window.open(base_url + "/claim/notification_report/profile/" + $aData['resource_id'], "_self");
                            break;
                        case '5':
                            /* Contribution Receipt */
                            /* document.location.href =  base_url + "/finance/receipt/profile/" + aData['resource_id'];*/
                            window.open(base_url + "/finance/receipt/profile/" + $aData['resource_id'], "_self");
                            break;
                        case '6':
                            /* Employer Registration */
                            /*document.location.href =  base_url + "/compliance/employer_registration/profile/" + aData['resource_id'];*/
                            window.open(base_url + "/compliance/employer_registration/profile/" + $aData['resource_id'], "_self");
                            break;
                        case '7':
                            /* Contribution Receipt Legacy */
                            /*document.location.href =  base_url + "/finance/legacy_receipt/profile/" + aData['resource_id'];*/
                            window.open(base_url + "/finance/legacy_receipt/profile/" + $aData['resource_id'], "_self");
                            break;
                        case '8':
                            /* Online Employer Verification */
                            /* document.location.href =  base_url + "/compliance/employer/" + aData['resource_id'] + "/verification_profile";*/
                            window.open(base_url + "/compliance/employer/" + $aData['resource_id'] + "/verification_profile", "_self");
                            break;

                        case '10':
                            /*Payroll run approval*/
                            window.open(base_url + "/payroll/run_approval/profile/" + $aData['resource_id'] , "_self");
                            break;

                        case '11':
                            /* Payroll Bank details */
                            window.open(base_url + "/payroll/bank_update/profile/" + $aData['resource_id'] , "_self");
                            break;

                        case '12':
                            /* Payroll Status Change */
                            window.open(base_url + "/payroll/status_change/profile/" + $aData['resource_id'] , "_self");
                            break;

                        case '13':
                            /* Payroll Recovery */
                            window.open(base_url + "/payroll/recovery/profile/" + $aData['resource_id'] , "_self");
                            break;
                        case '14':
                            /* Payroll Reconciliation Processing */
                            window.open(base_url + "/payroll/reconciliation/profile/" + $aData['resource_id'] , "_self");
                            break;
                        case '15':
                            /* Payroll Beneficiary Details modification */
                            window.open(base_url + "/payroll/beneficiary_update/profile/" + $aData['resource_id'] , "_self");
                            break;
                        case '16':
                            /* Employer Advance Payments */
                            window.open(base_url + "/compliance/employer/" + $aData['resource_id'] + "/advance_payment_profile", "_self");
                            break;

                        case '18':
                            /* Employer business closure  */
                            window.open(base_url + "/compliance/employer/closure/profile/" + $aData['resource_id'], "_self");
                            break;

                        case '22':
                            /* Employer business closure - Reopen  */
                            window.open(base_url + "/compliance/employer_closure/open/profile/" + $aData['resource_id'], "_self");
                            break;

                        case '23':
                            /* Employer Particular Changes  */
                            window.open(base_url + "/compliance/employer/change_particular/profile/" + $aData['resource_id'], "_self");
                            break;

                        case '24':
                            /* Payroll Monthly Pension Updates  */
                            window.open(base_url + "/payroll/mp_update/profile/" + $aData['resource_id'],  "_self");
                            break;


                        default:
                            break;
                    }
                }).hover(function() {
                    $(this).css('cursor', 'alias');
                }, function() {
                    $(this).css('cursor', 'auto');
                });
            }
        });

        $('#search-form').on('submit', function(e) {
            /*alert($wf_module_group_id);*/
            $oTable.draw();
            e.preventDefault();
        });

    @permission("workflow_allocation")
        $('#assign-user').on('submit', function($e) {
            $e.preventDefault();
            let $form = this;
            let $rowsSelected = $oTable.column(0).checkboxes.selected();
            //Remove all previous selected
            $($form).find("input[name='id[]']").remove();
            // Iterate over all selected checkboxes
            $.each($rowsSelected, function($index, $rowId) {
                // Create a hidden element
                $($form).append($('<input>').attr('type', 'hidden').attr('name', 'id[]').val($rowId));
            });
            swal({
                title: "Warning",
                text: "Are you sure to allocate selected files to the selected assigned user",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function ($confirmed) {
                if ($confirmed) {
                    //$form.submit();
                    //Do the ajax submission ...
                    //route : backend.claim.notification_report.allocation.assign
                    let $options = {
                        dataType : "json",
                        type : "PUT",
                        url : $($form).attr("action"),
                        success : function (data) {
                            if (data.success) {
                                $oTable.draw();
                                $oTable.column(0).checkboxes.deselectAll();
                                $.amaran({
                                    'theme'     :'awesome success',
                                    'content'   :{
                                        title : "Success",
                                        message: data.message,
                                        info:'',
                                        icon: 'fa fa-check-square-o'
                                    },
                                    'position'  :'bottom left',
                                    'outEffect' :'slideBottom',
                                    'inEffect'  :'slideLeft'
                                });
                            } else {
                                alert(data.message);
                                /*swal({ title : "Error Assigning User to Resource(s)", text : data.message});*/
                            }
                        },
                        error: function (data) {

                        }
                    };
                    // pass options to ajaxForm
                    $($form).ajaxSubmit($options);
                }
            });
        });
        @endauth

    });
</script>