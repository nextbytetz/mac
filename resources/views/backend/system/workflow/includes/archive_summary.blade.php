<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Workflow Summary</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">Name :</h6>
        <div style="font-weight: bold;">{{ $wf_track->resource->resource_name }}</div>
        <h6 class="underline" style="font-weight: lighter;">Module :</h6>
        <div style="font-weight: bold;">{{ $wf_track->wfDefinition->wfModule->name }}</div>
        <h6 class="underline" style="font-weight: lighter;">Workflow Date :</h6>
        <div style="font-weight: bold;">{{ $wf_track->receive_date }}</div>
    </div>
</div>