{{--@if ($wf_track->wf_archive_id && $wf_track->status == 6)--}}
@if (true)
    @php
        $archives = $wf_track->archives;
    @endphp

@include('backend/system/workflow/includes/archive_log', ['archives' => $archives])

@endif
<div class="row">
    <div class="col-md-12">
        <span class="pull-right">
            <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonLinks" data-toggle="dropdown" >Workflow Options</a>
            <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonLinks" style="z-index: 99999999">
                @if ($wf_track->wf_archive_id && $wf_track->status == 6)
                    {{--Unarchive Workflow--}}
                    {!! Html::decode(link_to_route('backend.workflow.archive_workflow', "<i class='icon fa fa-archive' aria-hidden='true'></i>&nbsp;Un-archive Workflow" , ['resource_id' => $resource_id, 'wf_module_group_id' => $wf_module_group_id, 'type' => $type, 'action' => 0], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'Are you sure to un-archive workflow', 'class' => 'dropdown-item'])) !!}
                @elseif ($wf_track->status == 0)
                    {{--Archive Workflow--}}
                    {!! Html::decode(link_to_route('backend.workflow.archive_workflow', "<i class='icon fa fa-archive' aria-hidden='true'></i>&nbsp;Archive Workflow" , ['resource_id' => $resource_id, 'wf_module_group_id' => $wf_module_group_id, 'type' => $type, 'action' => 1], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'Are you sure to archive workflow', 'class' => 'dropdown-item'])) !!}
                @endif
            </span>
        </span>
    </div>
</div>

