<div class="modal hide fade" id="initiate_benefit_workflow_modal" role="dialog" aria-labelledby="initiate_benefit_workflow_modal" aria-hidden="true">
    <div class="modal-dialog white_modal" role="document">
        <div class="modal-content" id="modal-content">
            @include("backend/system/workflow/includes/initiate_benefit_form")
        </div>
    </div>
</div>

@push('after-script-end')
    <script  type="text/javascript">
        $(function() {
            var $body = $('body');
            $body.on('click', 'a.initiate_benefit_workflow', function ($e) {
                $e.preventDefault();
                /*alert($(this).attr("data-description"));*/
                var $form = $('form[name=initiate_benefit_workflow_form]');
                var $modal = $('#initiate_benefit_workflow_modal');
                $form.attr('action', $(this).attr("data-route"));
                $form.find("input[name^='incident']").val($(this).attr("data-incident"));
                $form.find("input[name^='benefit']").val($(this).attr("data-benefit"));
                $form.find("input[name^='eligible']").val($(this).attr("data-eligible"));
                $modal.find("#modal-title").html($(this).attr("data-description"));
                $modal.modal('show');
            });
            $body.on('submit', 'form[name=initiate_benefit_workflow_form]', function(e) {
                e.preventDefault();
                var $form = this;
                swal({
                    title: "Warning",
                    text: "Are you sure to continue?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        $form.submit();
                    }
                });
            });
        });
    </script>
@endpush