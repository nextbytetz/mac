<div class="modal hide fade" id="initiate_workflow_modal" role="dialog" aria-labelledby="initiate_workflow_modal" aria-hidden="true">
    <div class="modal-dialog white_modal" role="document">
        <div class="modal-content" id="modal-content">
            @include("backend/system/workflow/includes/initiate_form")
        </div>
    </div>
</div>

@push('after-script-end')
<script  type="text/javascript">
    $(function() {
        var $body = $('body');
        $body.on('click', 'a.initiate_workflow', function ($e) {
            $e.preventDefault();
            /*alert($(this).attr("data-description"));*/
            var $form = $('form[name=initiate_workflow_form]');
            var $modal = $('#initiate_workflow_modal');
            $form.attr('action', $(this).attr("data-route"));
            $form.find("input[name^='group']").val($(this).attr("data-group"));
            $form.find("input[name^='type']").val($(this).attr("data-type"));
            $form.find("input[name^='resource']").val($(this).attr("data-resource"));
            $modal.find("#modal-title").html($(this).attr("data-description"));
            $modal.modal('show');
        });
        $body.on('submit', 'form[name=initiate_workflow_form]', function(e) {
            e.preventDefault();
            var $form = this;
            swal({
                title: "Warning",
                text: "Are you sure to continue?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function (confirmed) {
                if (confirmed) {
                    $form.submit();
                }
            });
        });
    });
</script>
@endpush