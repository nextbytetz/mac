
<script>
    $(document).ready( function () {
        let oTable = $('#reversed-pending-workflow-table').DataTable({
            //dom : 'Bfrtip',
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                oTable.buttons().container()
                .insertBefore('#reversed-pending-workflow-table');
            },
            processing: true,
            serverSide: true,
            info : true,
            ajax: {
                url: "{!! route('backend.workflow.reversed_pending.get') !!}",
                data: function (d) {
                    d.wf_module_id = $('select[name=wf_module_id]').val();
                    d.wf_module_group_id = $wf_module_group_id;
                    d.operator = $('select[name=operator]').val();
                    d.incident = $('select[name=incident]').val();
                    d.search = $('input[name=search]').val();
                    d.state = "{{ $state }}";
                    @if ($state == "all" || $state == "attended" || $state == "archived")
                    d.status = $('select[name=status]').val();
                    d.user_id = $('select[name=user_id]').val();
                    d.member_type_id = $('select[name=member_type]').val();
                    d.pensioner_id = $('select[name=pensioner]').val();
                    d.dependent_id = $('select[name=dependent]').val();
                    @endif
                }
            },
            columns: [
            {data: 'resource_name', name: 'resource_name', searchable: false, orderable: false},
            {data: 'receive_date_formatted', name: 'receive_date'},
            {data: 'level', name: 'level'},
            {data: 'module', name: 'wf_modules.name'},
            {data: 'assign_status', name: 'assigned', searchable: false},
            {data: 'resource_id', name: 'resource_id', visible: false, searchable: false, orderable: false},
            {data: 'module_group_id', name: 'wf_module_groups.id', visible: false, searchable: false, orderable: false},
            {data: 'module_id', name: 'wf_modules.id', visible: false, searchable: false, orderable: false},
            ],
            'rowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr('data-module-group', aData['module_group_id']);
                $(nRow).attr('data-resource', aData['resource_id']);
                $(nRow).click(function() {
                    switch ($("#workflow_open_mode").val()) {
                        case "1":
                        open_resource(aData, "_blank");
                        break;
                        default:
                        open_resource(aData);
                        break;
                    }
                }).hover(function() {
                    $(this).css('cursor', 'alias');
                }, function() {
                    $(this).css('cursor', 'auto');
                });
            },
/*            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $(nRow).attr('data-module-group', aData['module_group_id']);
                $(nRow).attr('data-resource', aData['resource_id']);
            }*/
/*            'drawCallback' : function() {
                $.contextMenu({
                    //selector: 'tbody tr td',
                    selector: 'tbody tr',
                    callback: function($key, $options) {
                        console.log($options);
                    },
                    items: {
                        "target_blank": {name: "New Tab", icon: function(){
                            return 'icon fa fa-location-arrow';
                        }},
                        /!*"cut": {name: "Cut", icon: "cut"},*!/
                        "sep1": "---------",
                        "quit": {name: "Quit", icon: function(){
                                return 'icon fa fa-close';
                            }}
                    }
                });
            }*/
        });

        $(document).contextmenu({
            delegate: "#reversed-pending-workflow-table tr",
            menu: [
            {title: "Open in new tab", cmd: "blank", uiIcon: "ui-icon-extlink"},
            {title: "Exit", cmd: "exit", uiIcon: "ui-icon-close"}
            ],
            select: function(event, ui) {
                /*var celltext = ui.target.text();
                var colvindex = ui.target.parent().children().index(ui.target);
                var colindex = $('table thead tr th:eq('+colvindex+')').data('column-index');*/
                switch(ui.cmd){
                    case "blank":
                        /*table
                            .column( colindex )
                            .search( '^' + celltext + '$', true )
                            .draw();*/
                            /*console.log(ui);*/
                            let group = ui.target.parent().attr("data-module-group");
                            let resource = ui.target.parent().attr("data-resource");
                            /*console.log(colvindex);*/
                            let aData = {'module_group_id': group, 'resource_id': resource};
                            open_resource(aData, "_blank");
                            break;
                            case "exit":
                        /*table
                            .search('')
                            .columns().search('')
                            .draw();*/
                            break;
                        }
                    },
                    beforeOpen: function(event, ui) {
                        var $menu = ui.menu,
                        $target = ui.target,
                        extraData = ui.extraData;
                //ui.menu.zIndex(9999);
            }
        });

        $('#search-form').on('submit', function(e) {
            /*alert($wf_module_group_id);*/
            oTable.draw();
            e.preventDefault();
        });
    });

function open_resource($aData, $tab_option = "_self") {
        //tab_option : _self | _blank
        switch($aData['module_group_id']) {
            case '1':
            /* Interest Waiving */
            /*document.location.href =  base_url + "/compliance/employer/profile/interest_write_off_approve/" + aData['resource_id'];*/
            window.open(base_url + "/compliance/employer/profile/interest_write_off_approve/" + $aData['resource_id'], $tab_option);
            break;
            case '2':
            /*Interest Adjustment*/
            /*document.location.href =  base_url + "/compliance/employer/profile/interest/" + aData['resource_id'];*/
            window.open(base_url + "/compliance/employer/profile/interest/" + $aData['resource_id'], $tab_option);
            break;
            case '3':
            /*Claim & Notification Processing*/
            /*document.location.href =  base_url + "/claim/notification_report/profile/" + aData['resource_id'];*/
            window.open(base_url + "/claim/notification_report/profile/" + $aData['resource_id'], $tab_option);
            break;
            case '4':
            /* Notification Rejection */
            /*document.location.href =  base_url + "/claim/notification_report/profile/" + aData['resource_id'];*/
            window.open(base_url + "/claim/notification_report/profile/" + $aData['resource_id'], $tab_option);
            break;
            case '5':
            /* Contribution Receipt */
            /* document.location.href =  base_url + "/finance/receipt/profile/" + aData['resource_id'];*/
            window.open(base_url + "/finance/receipt/profile/" + $aData['resource_id'], $tab_option);
            break;
            case '6':
            /* Employer Registration */
            /*document.location.href =  base_url + "/compliance/employer_registration/profile/" + aData['resource_id'];*/
            window.open(base_url + "/compliance/employer_registration/profile/" + $aData['resource_id'], $tab_option);
            break;
            case '7':
            /* Contribution Receipt Legacy */
            /*document.location.href =  base_url + "/finance/legacy_receipt/profile/" + aData['resource_id'];*/
            window.open(base_url + "/finance/legacy_receipt/profile/" + $aData['resource_id'], $tab_option);
            break;
            case '8':
            /* Online Employer Verification */
            /* document.location.href =  base_url + "/compliance/employer/" + aData['resource_id'] + "/verification_profile";*/
            window.open(base_url + "/compliance/employer/" + $aData['resource_id'] + "/verification_profile", $tab_option);
            break;

            case '10':
            /*Payroll run approval*/
            window.open(base_url + "/payroll/run_approval/profile/" + $aData['resource_id'] , $tab_option);
            break;

            case '11':
            /* Payroll Bank details */
            window.open(base_url + "/payroll/bank_update/profile/" + $aData['resource_id'] , $tab_option);
            break;

            case '12':
            /* Payroll Status Change */
            window.open(base_url + "/payroll/status_change/profile/" + $aData['resource_id'] , $tab_option);
            break;

            case '13':
            /* Payroll Recovery */
            window.open(base_url + "/payroll/recovery/profile/" + $aData['resource_id'] , $tab_option);
            break;
            case '14':
            /* Payroll Reconciliation Processing */
            window.open(base_url + "/payroll/reconciliation/profile/" + $aData['resource_id'] , $tab_option);
            break;
            case '15':
            /* Payroll Beneficiary Details modification */
            window.open(base_url + "/payroll/beneficiary_update/profile/" + $aData['resource_id'] , $tab_option);
            break;
            case '16':
            /* Employer Advance Payments */
            window.open(base_url + "/compliance/employer/" + $aData['resource_id'] + "/advance_payment_profile", $tab_option);
            break;
            case '17':
            /* Online Notification Application */
            window.open(base_url + "/compliance/employer/" + $aData['resource_id'] + "/incident_profile", $tab_option);
            break;
            case '18':
            /* Employer business closure  */
            window.open(base_url + "/compliance/employer/closure/profile/" + $aData['resource_id'], $tab_option);
            break;
            case '19':
            /*Letter Insuance*/
            window.open(base_url + "/letter/show/" + $aData['resource_id'], $tab_option);
            break;
            case '20':
            /* Employer Inspection Plan */
            window.open(base_url + "/compliance/inspection/" + $aData['resource_id'], $tab_option);
            break;
            case '21':
            /* Employer Inspection Task */
            window.open(base_url + "/compliance/inspection/employer_task/" + $aData['resource_id'], $tab_option);
            break;

            case '22':
            /* Employer business closure - Reopen  */
            window.open(base_url + "/compliance/employer_closure/open/profile/" + $aData['resource_id'], $tab_option);
            break;

            case '23':
            /* Employer Particular Changes  */
            window.open(base_url + "/compliance/employer/change_particular/profile/" + $aData['resource_id'], $tab_option);
            break;

            case '24':
            /* Payroll Monthly Pension Updates  */
            window.open(base_url + "/payroll/mp_update/profile/" + $aData['resource_id'], $tab_option);
            break;
            case '28':
            /* Osh Audit List */
            window.open(base_url + "/workplace_risk_assesment/audit/" + $aData['resource_id']+"/profile#workflow_tab", $tab_option);
            break;
            case '29':
            /* Investment Budget */ 
            window.open(base_url + "/investment/budget/show/" + $aData['resource_id']+"#workflow");
            break;
            case '30':
            /*Employer De-registrations extension requests */
            window.open(base_url + "/compliance/employer_closure/extension/profile/" + $aData['resource_id']+"#workflow");
            break;
            case '31':
            /* Investment Budget */ 
            window.open(base_url + "/claim_accrual/test/"+$aData['resource_id']);
            break;
            case '32':
            /* Investigation Plan */ 
            window.open(base_url + "/claim/investigations/profile/"+$aData['resource_id']);
            break;
            case '33':
            /* Interest Adjustment (Receipt) */
            window.open(base_url + "/compliance/interest_adjustment/profile/"+$aData['resource_id']);
            break;
            case '34':
            /* Payroll retiree mp update */
            window.open(base_url + "/payroll/retiree/mp_update/profile/"+$aData['resource_id']);
            break;

            case '35':
            /* Payroll merging profile */
            window.open(base_url + "/compliance/employer/payroll_merge/profile/"+$aData['resource_id']);

            case '36':
            /* HSP Bill vetting */
            window.open(base_url + "/assessment/hcp_hsp_billing/billing_profile/"+$aData['resource_id']);
            break;
            case '37':
            /* Contribution Modification */
            window.open(base_url + "/compliance/employer/contrib_modification/profile/"+$aData['resource_id']);
            break;

            case '38':
            /* Contribution - Interest Rate */
            window.open(base_url + "/compliance/employer/rates/profile/"+$aData['resource_id']);
            break;
            default:
            break;
        }
    }
</script>