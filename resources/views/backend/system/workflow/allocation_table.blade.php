<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" id = "allocation-workflow-table" cellspacing="0" width="100%">
            <thead>
            <tr >
                <th></th>
                <th>@lang('labels.backend.system.workflow.resource')</th>
                {{--<th>@lang('labels.backend.system.workflow.wf_date')</th>--}}
                <th>@lang('labels.backend.table.receive_date')</th>
                <th>@lang('labels.backend.table.level')</th>
                {{--<th>@lang('labels.backend.system.workflow.description')</th>--}}
                <th>@lang('labels.backend.system.workflow.module')</th>
                {{--<th>@lang('labels.backend.system.workflow.module')</th>--}}
                <th>@lang('labels.backend.table.status')</th>
                <th>@lang('labels.backend.table.user')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>