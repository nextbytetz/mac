@extends('layouts.backend.main', ['title' => trans('labels.backend.workflow.title'), 'header_title' => trans('labels.backend.workflow.view')])

{{--@include('backend.includes.toastr_assets')--}}

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/duallistbox/css/prettify.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/duallistbox/css/bootstrap-duallistbox.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-6">
            @if ($groups->count())
                <div id="workflow-tree">
                    <ul>
                        @foreach ($groups as $group)
                            <li data-jstree='{"icon":"{!! asset_url() . "/nextbyte/img/workflow_module_group.png" !!}"}'>{!! $group->name !!}
                                @if ($group->modules->count())
                                    <ul>
                                        @foreach ($group->modules as $module)
                                            <li data-jstree='{"icon":"{!! asset_url() . "/nextbyte/img/workflow_module.png" !!}"}'>
                                                @if (!$module->isactive)
                                                    <span class="inactive"><span style='color:black'>{!! $module->name !!}</span></span>
                                                @else
                                                    {!! $module->name !!}
                                                @endif
                                                @if ($module->isactive)
                                                    &nbsp;&#x21AA;&nbsp;<span style="font-weight: 500;border:1px;border-style: dashed;border-color: green;padding: 2px;">(Active)</span>
                                                @endif
                                                @if ($module->definitions->count())
                                                    <ul>
                                                        @foreach ($module->definitions as $definition)
                                                            <li id="{!! $definition->id !!}"  data-jstree='{"icon":"{!! asset_url() . "/nextbyte/img/workflow_definition.png" !!}"}'>&nbsp;{!! $definition->level !!}&nbsp;.&nbsp;<span style="font-weight: 500" class="underline">{!! $definition->designation->name !!}&nbsp;-&nbsp;<i>{!! $definition->unit->name !!}</i></span>&nbsp;&nbsp;{!! $definition->description !!}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-6">
            <div  style="position: fixed;background: white;padding: 4px;border-radius: 3px;">
                {{--<div class="row">
                    <div class="col-md-12">--}}

                        {!! Form::open(['id' => 'users_form', 'method' => 'PATCH']) !!}

                        {!! Form::select('users_select[]', $users->pluck('name', 'id')->all(), [], ['multiple' => 'multiple', 'size' => 10, 'class' => 'users_select']) !!}

                        {!! Form::hidden('wf_definition_id', null, ['id' => 'wf_definition_id']) !!}

                        {{--            {!! Form::hidden('level') !!}
                                    {!! Form::hidden('module_id') !!}--}}
                        <br>
                        <button type="submit" class="btn btn-success btn-block">@lang('buttons.general.save')</button>
                        {!! Form::close() !!}
                        <br/>
                        <br/>
                        <p>
                            {!! getLanguageBlock('backend.lang.workflow.select-users-explanation') !!}
                        </p>
                    {{--</div>
                </div>--}}
            </div>

        </div>
        {{--        <div class="col-md-6">

                </div>--}}
    </div>
    <hr/>


@endsection
<script>
    var url = "{!! url("/") !!}";
</script>
@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/duallistbox/js/jquery.bootstrap-duallistbox.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/duallistbox/js/run_prettify.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/system/workflow.js") }}
<!-- Custom javascript files for this page -->
<script>
    $(function () {

    });
</script>

@endpush