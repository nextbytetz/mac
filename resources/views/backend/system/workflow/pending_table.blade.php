<div class="row">
    <div class="col-md-12">
        <div class="form-row">
            <div class="form-group col-md-12">
                <span class="assign_status_select">
                    <label for="assign_status"><b>Open Mode:</b></label>
                    {!! Form::select('workflow_open_mode', ['0' => 'Same Tab', '1' => 'New Tab'] , null, ['class' => 'form-control search-select', 'id' => 'workflow_open_mode']) !!}
                </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" id = "pending-workflow-table" cellspacing="0" width="100%">
            <thead>
            <tr >
                <th>@lang('labels.backend.system.workflow.resource')</th>
                {{--<th>@lang('labels.backend.system.workflow.wf_date')</th>--}}
                <th>@lang('labels.backend.table.receive_date')</th>
                <th>@lang('labels.backend.table.level')</th>
                {{--<th>@lang('labels.backend.system.workflow.description')</th>--}}
                <th>@lang('labels.backend.system.workflow.module')</th>
                {{--<th>@lang('labels.backend.system.workflow.module')</th>--}}
                <th>@lang('labels.backend.table.status')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>