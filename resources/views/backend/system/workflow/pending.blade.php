@extends('layouts.backend.main', ['title' => trans('labels.backend.system.workflow.pending_title', ['name' => $control['wfname']]), 'header_title' => trans('labels.backend.system.workflow.pending_title', ['name' => $control['wfname']])])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>
@endpush

@section('content')
    {{--pending workflows count summary --}}
    @include("backend/system/workflow/count_summary")

    @if ($control['show'])
        @if ($control['wf_mode'] == 2)
        {{--@if (false)--}}
            @php
                $cr = $control['cr'];
            @endphp
            <br/>
            <legend>Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $cr->description }}</small></span></legend>

            <!--Include Report-->
            @include('backend/report/configurable/includes/content', [
                        'cr' => $cr,
                        'cr_params' => [
                            'hasfilter' => 1,
                            'shouldrefresh' => 0,
                            'canbuild' => 1,
                            'buttons' => 1,
                            'query_filter' => [
                                    [
                                        'or' => ['status' => ['in', [0,6]], 'assigned' => 0],
                                        'and' => ['wf_module_id' => $control['wf_module_id'], 'wf_definition_id' => ['in', $control['wf_definitions']]],
                                    ],
                                ],
                            ],
                        ])
            {{--status queried for pending workflow and archived workflow--}}

        @elseif ($control['wf_mode'] == 1)
            <!-- Put the page specifically for this page here -->
            @include("backend/system/workflow/pending_filter")
            @include("backend/system/workflow/pending_table")
        @endif

    @else
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <br/>
                {{--user should click the module group to open--}}
                <div class="alert alert-success alert-icon-left" role="alert">
                    <i class="fa fa-hand-o-up"></i>
                    <div class="float-xs-left">
                        <strong>Hello</strong> Please click on any module above to open workflow.
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@push('after-script-end')
    @if ($control['show'])
        @if ($control['wf_mode'] == 1)
            <!-- Custom javascript files for this page -->
            @include("backend/system/workflow/pending_script")
            @include("backend.system.workflow.includes.pending_datatable")
        @endif
    @endif
@endpush