{{--pending workflows count summary --}}
<div class="row">
    <div class="col-md-12">
        @foreach(array_chunk($group_counts, 4) as $group_count)
        <br/>
            <div class="row"> 
            @foreach($group_count as $value)
            <div class="col-md-3">
                <a href="{{ url("/") . "/" . request()->route()->uri() }}?wf_module_id={{ $value['id'] }}"
                 @if(!empty($value['description']))
                 data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ $value['description'] }}"
                 @endif
                 >
                 <div class="grid-column">
                    {{ $value['name'] }}&nbsp;<span class="badge-summary @if($value['notify']) blink  @endif">{{ $value['count'] }} </span><br/>
                    <span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>
                    @if ($value['notify_archive'])
                    <br/>
                    <span class="blink" style="padding: 4px;">&rdsh;&nbsp;{{ $value['notify_archive'] }} workflow ready to un-archive</span>
                    @endif
                </div>
            </a>
        </div>
        @endforeach
    </div>
    @endforeach
</div>
</div>