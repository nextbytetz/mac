@extends('layouts.backend.main', ['title' => 'Edit Archive Workflow', 'header_title' => 'Edit Archive Workflow'])

@push('after-styles-end')
    {{--    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
        {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}--}}
    {{ Html::style(asset_url() . '/nextbyte/plugins/select2/css/select2.min.css') }}
    <style>
    </style>
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <!-- Put the page specifically for this page here -->
    {!! Form::model($wf_archive, ['route' => ['backend.workflow.update_archive_workflow', $wf_archive->id], 'name' => 'post_archive_workflow', 'class' => 'post_archive_workflow', 'enctype' => 'multipart/form-data']) !!}

    {!! Form::hidden('wf_track_id', $wf_track->id) !!}
    {!! Form::hidden('prevurl', $prevurl) !!}
    {!! Form::hidden('wf_date', $wf_track->receive_date) !!}

    {{--<br/>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to($prevurl, trans('buttons.general.cancel'), ['id'=> 'cancel', 'class' => 'btn btn-primary btn-sm cancel_button', ]) !!}
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="Archive" />
            </div>
        </div>
    </div>
    <hr/>

    <div class="row">

        <div class="col-md-4" style="padding-right:20px; border-right: 1px solid #ddd;">

            <div class="fileld-layout">
                <label>Description</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea('description', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;">
		                <small class="form-text text-muted" style="width:100% !important;">Description for archiving the workflow</small>
	                </span>

                </div>
            </div>

            <div class="fileld-layout">
                <label class="required">From Date</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('from_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Start date of archiving</small>
                    </span>
                </div>
            </div>

            <div class="fileld-layout">
                <label class="required">To Date</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('to_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Estimated end date of archiving</small>
                    </span>
                </div>
            </div>

        </div>

        <div class="col-md-4">

            <div class="fileld-layout">
                <label class="required">Archive Reason</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('archive_reason_cv_id', $archive_reasons, NULL, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Reason for archiving</small>
                    </span>
                </div>
            </div>

            <div class="fileld-layout" style="display: none;">
                <label class="required">Should Send to Record?</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('instore', [0 => 'No', 1 => 'Yes'], null, ['class' => 'search-select instore', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Specify whether the file for this workflow will be sent to record department</small>
                    </span>
                </div>
            </div>

            <div class="fileld-layout record_users" style="display: none;">
                <label class="required">Receiver</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('receiver_user', $record_users, null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">User from record department to receive the file</small>
                    </span>
                </div>
            </div>

        </div>

        <div class="col-md-4">
            {{--Summary input of this workflow--}}
            @include('backend.system.workflow.includes.archive_summary')
        </div>

    </div>

    @include('backend/system/workflow/includes/archive_log', ['archives' => $wf_track->archives])

    {!! Form::close() !!}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). '/nextbyte/plugins/select2/js/select2.min.js') }}
    {{ Html::script(asset_url(). '/nextbyte/plugins/autosize/js/autosize.min.js') }}
    {{ Html::script(asset_url(). '/nextbyte/plugins/forms/js/jquery.form.min.js') }}
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}--}}

    <script>
        $(function() {
            let $body = $('body');
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            let $instore = $(".instore");
            let $record_users = $('.record_users');
            $instore.on('change', function (e) {
                let $choice = $instore.val();
                switch ($choice) {
                    case '1':
                        /*Send to Record Department*/
                        $record_users.show();
                        break;
                    default:
                        $record_users.hide();
                        break;
                }
            });
            $instore.trigger("change");
            $body.on('submit', 'form[name=post_archive_workflow]', function ($e) {
                $e.preventDefault();
                let $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                let $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    beforeSend : function ($e) {
                        $($form).find(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        /*console.log($data);*/
                        if (!$data.success) {

                        } else {
                            document.location.href =  "{{ $prevurl }}";
                        }
                    },
                    error: function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        var errors = $.parseJSON($data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function($index, $value) {
                            $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                            if ($index === 'general_error') { $($form).prepend('<div class="alert alert-danger general_error" role="alert">' + $value + '</div>'); $('.general_error').fadeOut(11000); }
                        });
                    },
                    uploadProgress : function (event, position, total, percentComplete) {
                    }
                };
                $($form).ajaxSubmit($options);
            });
        });
    </script>
@endpush