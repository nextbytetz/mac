{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    let $wf_module_group_id = 0;
    $(function() {

        $(".wf-module-select").select2();
        $(".search-select").select2();

        @if ($state == "all" || $state == "attended" || $state == "full" || $state == 'archived')
        $("#status").on("change", function () {
            var $status = $(this).val();
            switch($status) {
                case '3':
                    /* Selected to choose User Assigned */
                    $(".user_select").show();
                    break;
                default:
                    $(".user_select").hide();
                    break;
            }
        });
        $('#status').trigger('change');
        @endif

        $(".wf-module-select").on("change", function () {
            get_module_group($(this).val()).done(function (data) {
                var $category = data.wf_module_group_id;
                $wf_module_group_id = $category;
                $('input[name=search]').val("");
                let $unregistered_modules = {{ json_encode($unregistered_modules) }};
                switch($category) {
                    case 1:
                    case 2:
                    case 6:
                    case 8:
                    case 16:
                        /* Interest Waiving */
                        /* Interest Adjustment */
                        /* Employer Registration */
                        /* Online Employer Verification */
                        /* Employer Advance Payment */
                        employer_filter();
                        break;
                    case 3:
                    case 4:
                        /*Claim & Notification Processing*/
                        /* Notification Rejection */
                        if (!$unregistered_modules.includes(data.wf_module_id)) {
                            notification_filter();
                        } else {
                            reset_filter();
                        }
                        break;
                    case 5:
                    case 7:
                        /* Contribution Receipt */
                        /* Contribution Receipt Legacy */
                        receipt_filter();
                        break;

                    case 11:
                        payroll_filter();
                        break;
                    case 12:
                        payroll_filter();
                        break;
                    case 13:
                        payroll_filter();
                        break;
                    case 14:
                        payroll_filter();
                        break;
                    default:
                        reset_filter();
                        break;
                }
            });
        });
        $('.wf-module-select').trigger('change');

        /*On change of member type*/
        $("#member_type_id").on("change", function () {
            var member_type_selected = $('#member_type_id').val();
            if(member_type_selected == 4)
            {
                /*Dependent*/
                $("#dependent_div").show();
                $("#pensioner_div").hide();
            }else if(member_type_selected == 5){
                /*Pensioner*/
                $("#dependent_div").hide();
                $("#pensioner_div").show();
            }
        });

        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.all_employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employees') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
        /* end : Searching Employee */


        /* start : Searching Beneficiary i.e. pensioners*/
        $(".pensioner-select").select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                url: "{!! route('backend.payroll.pensioner.get_for_select') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.pensioner,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
        /* end : Searching pensioners */

        /* start : Searching Beneficiary i.e. dependent*/
        $(".dependent-select").select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.dependent.get_for_select') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.dependent,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
        /* end : Searching dependent */

    });

    function get_module_group($module) {
        return $.post("{{ url('/') }}/getModuleGroup", {'module': $module}, function ($data) {}, "json");
    }

    function reset_filter() {
        $(".notification_select").hide();
        $(".employer_select").hide();
        $(".receipt_select").hide();
        $(".payroll_select").hide();
    }

    function employer_filter() {
        reset_filter();
        $(".notification_select").hide();
        $(".employer_select").show();

    }

    function notification_filter() {
        reset_filter();
        $(".notification_select").show();
        $(".employer_select").show();
    }

    function receipt_filter() {
        reset_filter();
        $(".receipt_select").show();
    }

    function payroll_filter() {
        reset_filter();
        $(".payroll_select").show();
        $("#dependent_div").hide();
        $("#pensioner_div").hide();
        $("#dependent_id").val(0).change();
        $("#pensioner_id").val(0).change();
        $("#member_type_id").val(0).change();
    }

</script>
