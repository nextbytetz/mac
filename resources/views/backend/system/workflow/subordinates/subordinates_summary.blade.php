@extends('layouts.backend.main', ['title' => trans('labels.backend.system.workflow.subordinates_title', ['name' => 'Workflow']), 'header_title' => trans('labels.backend.system.workflow.subordinates_title', ['name' => 'Workflow'])])

@include('backend.includes.datatable_assets')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" id = "subordinates-workflow_summary-table" cellspacing="0" width="100%">
            <thead>
                <tr >
                    {{-- <th>S No#</th> --}}
                    <th>Name</th>
                    <th>Pending Workflow</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@endsection

@push('after-script-end')
<script type="text/javascript">
    $(function() {
        $('#subordinates-workflow_summary-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:true,
            order: [[ 0, "desc" ]],
            stateSaveCallback: function (settings, data) {
              localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
          },
          stateLoadCallback: function (settings) {
              return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
          },
          ajax:{
              url : '{!! route('backend.workflow.subordinates.get') !!}',
              type : 'get'
          },
          columns: [
          // { data: 'rownum', name: 'rownum',searchable:false},
          { data: 'username' , name: 'users.firstname',searchable:false},
          { data: 'workflows' , name :'workflows', searchable:false},
          ],
          fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td', nRow).click(function() {
                document.location.href = '{!! url("workflow/subordinate_pending/") !!}/'+aData['user_id'];
            }).hover(function() {
                $(this).css('cursor','pointer');
            }, function() {
                $(this).css('cursor','auto');
            });
        }
    });
    });
</script>
@endpush