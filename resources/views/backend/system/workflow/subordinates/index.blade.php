@extends('layouts.backend.main', ['title' => trans('labels.backend.system.workflow.subordinate_title', ['user_name' => $user->name,'name' => $control['wfname']]), 'header_title' => trans('labels.backend.system.workflow.subordinate_title', ['user_name' => $user->name,'name' => $control['wfname']])])

@include('backend.includes.datatable_assets')
{{-- {{dd(str_replace('{user_id}', $user_id, request()->route()->uri()))}} --}}
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>
@endpush
{{-- {{dd($group_counts)}} --}}
@section('content')
{{--pending workflows count summary --}}
<div class="row">
    <div class="col-md-12">
        @foreach(array_chunk($group_counts, 4) as $group_count)
        <br/>
        <div class="row">
            @foreach($group_count as $value)
            <div class="col-md-3">
                <a href="{{ url("/") . "/" . str_replace('{user_id}', $user_id, request()->route()->uri()) }}?wf_module_id={{ $value['id'] }}"
                   @if(!empty($value['description']))
                   data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ $value['description'] }}"
                   @endif
                   >
                   <div class="grid-column">
                    {{ $value['name'] }}&nbsp;<span class="badge-summary @if($value['notify']) blink  @endif">{{ $value['count'] }} </span><br/>
                    <span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>
                    @if ($value['notify_archive'])
                    <br/>
                    <span class="blink" style="padding: 4px;">&rdsh;&nbsp;{{ $value['notify_archive'] }} workflow ready to un-archive</span>
                    @endif
                </div>
            </a>
        </div>
        @endforeach
    </div>
    @endforeach
</div>
</div>

@if ($control['show'])
@if ($control['wf_mode'] == 2)
{{--@if (false)--}}
@php
$cr = $control['cr'];
@endphp
<br/>
<legend>Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $cr->description }}</small></span></legend>

<!--Include Report-->
@include('backend/system/workflow/subordinates/content', ['cr' => $cr, 'cr_params' => ['hasfilter' => 1, 'shouldrefresh' => 0, 'canbuild' => 1, 'buttons' => 1, 'query_filter' => ['query_filter_or' => ['status' => [0, 6], 'assigned' => 0], 'query_filter_and' => ['wf_module_id' => $control['wf_module_id'], 'wf_definition_id' => $control['wf_definitions']]]]])
{{--status queried for pending workflow and archived workflow--}}

@elseif ($control['wf_mode'] == 1)
<!-- Put the page specifically for this page here -->
@include("backend/system/workflow/pending_filter")

<div class="row">
    <div class="col-md-12">
        <div class="form-row">
            <div class="form-group col-md-12">
                <span class="assign_status_select">
                    <label for="assign_status"><b>Open Mode:</b></label>
                    {!! Form::select('workflow_open_mode', ['0' => 'Same Tab', '1' => 'New Tab'] , null, ['class' => 'form-control search-select', 'id' => 'workflow_open_mode']) !!}
                </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" id = "subordinates-pending-workflow-table" cellspacing="0" width="100%">
            <thead>
                <tr >
                    <th>@lang('labels.backend.system.workflow.resource')</th>
                    {{--<th>@lang('labels.backend.system.workflow.wf_date')</th>--}}
                    <th>@lang('labels.backend.table.receive_date')</th>
                    <th>@lang('labels.backend.table.level')</th>
                    {{--<th>@lang('labels.backend.system.workflow.description')</th>--}}
                    <th>@lang('labels.backend.system.workflow.module')</th>
                    {{--<th>@lang('labels.backend.system.workflow.module')</th>--}}
                    <th>@lang('labels.backend.table.status')</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endif

@else
<div class="row">
    <div class="col-md-12 col-sm-12">
        <br/>
        {{--user should click the module group to open--}}
        <div class="alert alert-success alert-icon-left" role="alert">
            <i class="fa fa-hand-o-up"></i>
            <div class="float-xs-left">
                <strong>Hello</strong> Please click on any module above to open workflow.
            </div>
        </div>
    </div>
</div>
@endif

@endsection

@push('after-script-end')
@if ($control['show'])
@if ($control['wf_mode'] == 1)
<!-- Custom javascript files for this page -->
@include("backend/system/workflow/pending_script")
@include("backend.system.workflow.subordinates.subordinates_pending_datatable")
@endif
@endif
@endpush