@extends('layouts.backend.main', ['title' => 'Users Review', 'header_title' => 'Users review workflows roles and permissions'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        .custom_filter:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "@lang('labels.backend.system.workflow.custom_filter')";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }

        .custom_filter {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }

    </style>
@endpush

@section('content')
    {{--pending workflows count summary --}}
    {{-- @include('backend/system/workflow/count_summary') --}}

    <!-- Put the page specifically for this page here -->
    {{-- @include('backend/system/workflow/pending_filter') --}}

    {{-- @include('backend/system/workflow/pending_table') --}}
{!! $dataTable->table(['class' => 'display', 'width' => '100%', 'íd' => 'dataTable'], true) !!}
@endsection

@push('after-script-end')
    {!! $dataTable->scripts() !!}
 @endpush