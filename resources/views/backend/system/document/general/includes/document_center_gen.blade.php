<div class = "row">
    <div class="col-md-12">

        <div class="col-md-4">

            <legend>Attachment(s)
                @if($allow_modify)
                <a class="pull-right" style="color:blue; font-size: 12px"  href="{!! route('backend.document.attach_doc_gen',['resource_id'=> $resource_id, 'reference' => $reference])  !!}">Add Attachment</a>
                @endif
                {{--<a class="pull-right" style="color:blue; font-size: 12px"  href="{!! url(' document/'.$resource.'/'. $reference .'/attach/general?external_id=' . (isset($external_id) ? $external_id : null) ) !!}">Add Attachment</a>--}}
            </legend>

            <br/>

            {{--new Bank--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="element-form" >

                        @if ($docs_attached->count())
                            @foreach($docs_attached as $doc)
                                {{--@php--}}
                                    {{--$document = $doc->document;--}}
                                {{--@endphp--}}
                                {{--<li>--}}
                                <i class="fa fa-file-pdf-o" ></i>
                                <a  style="color:dodgerblue;" class="doc_attached"  href="#" id="{!! 'doc'. $doc->pivot->id !!}">{!! (!$doc->isother) ? $doc->name : $doc->pivot->description !!}</a>
                                @if($allow_modify)
                                |
                                <a class="" style="color:grey"  href="{!! route('backend.document.edit_doc_gen', ["doc_pivot_id" => $doc->pivot->id, 'resource_id' => $resource_id, "reference" => $reference]) !!}">{!! 'Edit' !!}</a>
                                @endif
                                {{--<span style="font-size: 12px; color:#414141"> {!!   '' . ' : ' . $pollOption->votes  !!}   </span>--}}
                                {{--<span style="font-size: 12px; color:#414141"> {!!   $pollOption->vote_percent_label  !!} </span>--}}
                                {{--</li>--}}
                                <br/>
                            @endforeach

                        @else
                            <span class="tag tag-success">None</span>
                        @endif
                        {{--<ul>--}}

                        {{--</ul>--}}
                    </div>

                </div>
            </div>

        </div>




        <div class="col-md-8">
            <div class = "row">
                <div class="col-md-12">
                    {{--Document Preview--}}
                    <legend>Document Preview</legend>
                    <br/>
                    <div id="document_frame" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script  type="text/javascript">

        $(function () {
            /*Documents which pending to be used list*/
            $(".doc_attached").click(function() {
                var $doc_id = this.id;
                var $pivot_id = $doc_id.substr(3);
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });
            function get_current_document($doc_resource_id) {
                var resource_id = '{{ $resource_id }}';
                var reference = '{{ $reference }}';
                return $.ajax({
                    url: base_url + "/document/" + $doc_resource_id  + "/" + resource_id + "/" + reference + "/preview/general",
                    dataType : 'json',
                    async : false,
                    method : "POST"
                });
            }
        });
    </script>

@endpush
