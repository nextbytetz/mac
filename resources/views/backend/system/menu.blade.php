@extends('layouts.backend.main', ['title' => trans('labels.backend.sysdef_title'), 'header_title' => trans('labels.general.sysdef_header')])


@section('content')

<div style="color:#fff">
    {{--left div--}}
    <div class="col-sm-6 col-md-6">
       <div class="list-group">


        {{--item1--}}
        <ul class="list-unstyled">
            <a href="{!! route('backend.role.index') !!}">
                <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-key"> </i><large>&nbsp;&nbsp;@lang('labels.backend.sysdef_menu.manage_roles_permission')</large></h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.sysdef_menu.manage_roles_permission_summary')</p> </li>
                </a>

            </ul>
            {{--item2--}}
            &nbsp;<ul class="list-unstyled">
                <a href="{!! route('backend.workflow.defaults') !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-sitemap"> </i><large>&nbsp;&nbsp;@lang('labels.backend.sysdef_menu.assign_workflow')</large></h6>

                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.sysdef_menu.assign_workflow_summary')</p> </li>
                    </a>

                </ul>

                {{--item3--}}
                &nbsp;<ul class="list-unstyled">
                   <a href="{!! route('backend.report.configurable.build') !!}">
                       <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-magic"> </i><large>&nbsp;&nbsp;Build Configurable Reports</large></h6>

                           <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Build Configurable Reports</p> </li>
                       </a>

                   </ul>

                   {{--Monthly user review--}}
                   &nbsp;<ul class="list-unstyled">
                    <a href="{!! route('backend.workflow.users_review') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i><large>&nbsp;&nbsp;@lang('Monthly Users Review')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('Monthly Users Review')</p> </li>
                        </a>
                    </ul>

                </div>
            </div>

            {{--right div--}}

            <div class="col-sm-6 col-md-6">
                <div class="list-group">

                    {{--item 1---Manage codes--}}
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.system.code.index') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list"> </i><large>&nbsp;&nbsp;@lang('labels.backend.sysdef_menu.manage_codes')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.sysdef_menu.manage_code_summary')</p> </li>
                            </a>
                        </ul>

                        {{--Workflow Allocations--}}
                        &nbsp;<ul class="list-unstyled">
                            <a href="{!! route('backend.workflow.allocation') !!}">
                                <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;Workflow Allocation</large></h6>

                                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Allocate and reallocate officers assigned in workflow levels </p> </li>
                                </a>
                            </ul>

                            {{--item3--}}
                            &nbsp;<ul class="list-unstyled">
                                <a href="{!! route('backend.report.configurable.refresh') !!}">
                                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-refresh"> </i><large>&nbsp;&nbsp;Refresh Configurable Reports</large></h6>

                                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Refresh Configurable Reports</p> </li>
                                    </a>

                                </ul>

                                &nbsp;<ul class="list-unstyled">
                                    <a href="{!! route('backend.finance.resend_bill') !!}">
                                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-repeat"> </i><large>&nbsp;&nbsp;Resend Bill</large></h6>

                                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Resend Pending Bills To GePG</p> </li>
                                        </a>

                                    </ul>

                                    {{--item 1  --Audit trail--}}
                   {{--  <ul class="list-unstyled">
                        <a href="#/offices">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i><large>&nbsp;&nbsp;@lang('labels.backend.sysdef_menu.audit_trails')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.sysdef_menu.audit_trail_summary')</p> </li>
                        </a>
                    </ul> --}}
                    &nbsp;{{--<ul class="list-unstyled">
                        <a href="#/offices">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-sitemap"> </i><large>&nbsp;&nbsp;@lang('labels.backend.sysdef_menu.config_maker_checker')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.sysdef_menu.config_maker_checker_summary')</p> </li>
                        </a>

                    </ul>To be added later--}}

                    {{--item 2--}}
                    &nbsp;{{--<ul class="list-unstyled">
                        <a href="#/offices">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-cogs"> </i><large>&nbsp;&nbsp;@lang('labels.backend.sysdef_menu.configurations')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.sysdef_menu.configuration_summary')</p> </li>
                        </a>

                    </ul>--}}

                    {{--item3--}}
                    &nbsp; {{--<ul class="list-unstyled">
                        <a href="#/offices">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-key"> </i><large>&nbsp;&nbsp;@lang('labels.backend.sysdef_menu.reference_number_preferences')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.sysdef_menu.reference_number_preference_summary')</p> </li>
                        </a>

                    </ul>--}}


                </div>
            </div>

        </div>


        @stop

        @push('after-script-end')
        <script type="text/javascript">
            $(document).ready(function() {
            /*
            $("#site-header-title").hide();
            */
            $("#preview").click(function() {

            });
        });
    </script>;

    @endpush