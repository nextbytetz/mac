@if ($notifications->count())
    @foreach ($notifications as $notification)
        <?php $data = $notification->data; ?>
        <div class="first-activity-box {!! is_null($notification->read_at) ? "unread" : "" !!} clearfix">
            {{--<div class="profile-accept float-xs-left">
                <img alt="Activity image" src="../../../assets/global/image/image1-profile.jpg">
            </div>--}}
            <a href="{!! $data['url'] !!}" class="notification_selector" id="{!! $notification->id !!}">
                <div class="right-side-activity">
                    <div class="top-section-activity">
                        <h6 class="float-xs-left">{!! $data['title'] !!}</h6>
                        <p class="float-xs-right">{!! \Carbon\Carbon::parse($notification->created_at)->diffForHumans() !!}</p>
                    </div>
                    <div class="image-activity">
                        <p class="des-activity">
                            {!! $data['message'] !!}
                        </p>
                        {{-- <a class="btn btn-primary float-md-left">More</a>--}}
                    </div>
                </div>
            </a>
        </div>
    @endforeach
    <div class="clearfix"></div>
    <div class="next jscroll-next-parent" style="display: none;"><a href="{!! route("backend.notification.fetch.next", $next_offset) !!}">next</a></div>
@else
    <br/>
    <br/>
    @lang("labels.backend.system.notification.none")
@endif

@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        $("body").off('click', 'a.notification_selector').on('click', 'a.notification_selector', function(e) {
            e.preventDefault();
            var $href = $(this).attr("href");
            $.post( base_url + "/notification/read/" + this.id, function( data ) {

            }).done(function() {
                document.location.href =  $href;
            });
        });
    });
</script>
@endpush