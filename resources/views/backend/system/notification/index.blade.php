@extends('layouts.backend.main', ['title' => trans('labels.backend.system.notification.title'), 'header_title' => trans('labels.backend.system.notification.title')])

@push('after-styles-end')

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="activity-box scroll">
                @include("backend/system/notification/next")
            </div>
        </div>
    </div>
@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/jscroll/js/jquery.jscroll.js") }}
<!-- Custom javascript files for this page -->
<script>
    $(function () {
        $('.scroll').jscroll();
    });
</script>
@endpush