@push('after-styles-end')

@endpush

<div class="row">
	<div class="col-md-9 col-sm-9">
		<legend>{{ $category_name }}</legend>
		<br/>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<table class="display" cellspacing="0" width="100%" id ="instalment-table">
					<thead>
						<tr >
							<th>Employer</th>
							<th># Of Months</th>
							<th>Payment Option</th>
							<th>User Submitted</th>
							<th>Date submitted</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>

@push('after-script-end')
<!-- Custom javascript files for this page -->

<script>
	$(function() {
		
		
		var $oTable = $('#instalment-table').DataTable({
			buttons : ['colvis'],
			initComplete : function () {
				$oTable.buttons().container().insertBefore('#instalment-table');
				$("#instalment-table").css("width","100%");
			},
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! route('backend.compliance.employer.online.checker_instalments') !!}',
				type : 'GET'
			},
			columns: [
			{ data: 'employer', name: 'employers.name'},
			{ data: 'number_of_month', name: 'installment_requests.number_of_month'},
			{ data: 'payment_option', name: 'installment_requests.payment_option'},
			{ data: 'user_name', name: 'user_name'},
			{ data: 'created_at', name:'created_at',searchable : false, orderable : false },
			],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$(nRow).click(function() {
					document.location.href = '{!! url('compliance/employer/online/view_installment/') !!}/'+aData['id'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});

	});

</script>
@endpush
