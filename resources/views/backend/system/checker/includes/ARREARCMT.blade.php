@push('after-styles-end')

@endpush

<div class="row">
	<div class="col-md-9 col-sm-9">
		<legend>Arrears Commitments Submitted Online</legend>
		<br/>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<table class="display" cellspacing="0" width="100%" id ="arrear-commitments-table">
					<thead>
						<tr >
							<th>Employer</th>
							<th>Commitment Type</th>
							<th>User Submitted</th>
							<th>Date submitted</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>

@push('after-script-end')
<!-- Custom javascript files for this page -->

<script>
	$(function() {
		
		
		var $oTable = $('#arrear-commitments-table').DataTable({
			buttons : ['colvis'],
			initComplete : function () {
				$oTable.buttons().container().insertBefore('#arrear-commitments-table');
				$("#arrear-commitments-table").css("width","100%");
			},
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! route('backend.compliance.employer.online.checker_commitments') !!}',
				type : 'GET'
			},
			columns: [
			{ data: 'employer', name: 'employers.name'},
			{ data: 'commitment_type', name: 'commitment_type'},
			{ data: 'user_name', name: 'user_name'},
			{ data: 'created_at', name:'created_at',searchable : false, orderable : false },
			],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$(nRow).click(function() {
					document.location.href = '{!! url('compliance/employer/online/view_commitment/') !!}/'+aData['id'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});

	});

</script>
@endpush
