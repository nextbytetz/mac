{{--Notification & Claim--}}
@push('after-styles-end')

@endpush

<div class="row">
    <div class="col-md-9 col-sm-9">
        <legend>{{ $category_name }}</legend>

        <div class="row">
            <div class="col-md-12">
                <div class="divider15"></div>
                <div class="card-accordions">
                    <div id="summary_link_notification" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#summary_link_notification" href="#overview_notification">
                                        Overview&nbsp;&nbsp;
                                        @if ($notify_count)
                                            <span class="tag tag-pill tag-danger blink">&nbsp;</span>
                                        @endif
                                    </a>
                                </h6>
                            </div>
                            <div id="overview_notification" class="collapse">
                                <div class="card-block">
                                    {{--pending group count summary --}}
                                    <div class="row">
                                        <div class="col-md-12">

                                            @include('backend/system/checker/includes/stage_summary', ['stage_counts' => $stage_counts, 'uri_param' => 'notification_stage_id'])

                                            <div class="underline">
                                                General Overview
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @foreach(array_chunk($overview_counts, 4) as $overview_count)
                                                        <br/>
                                                        <div class="row">
                                                            @foreach($overview_count as $value)
                                                                <div class="col-md-3">
                                                                    <label><i class="icon fa fa-level-down" aria-hidden="true"></i>&nbsp;&nbsp;{{ camelToWord($value['description']) }}</label>
                                                                    <a href="{{ url("/") . "/" . request()->route()->uri() }}?overview={{ $value['id'] }}&type={{ $value['type'] }}">
                                                                        <div class="grid-column" @if($value['class'] == 'blink') style="background: #e6f5f0;" @endif>
                                                                            {{ $value['name'] }}&nbsp;<span class="badge-summary" @if($value['class'] == 'blink') style="-webkit-animation: blink .3s step-end infinite alternate; border: 1px solid;border-color: #ff0000;" @endif>{{ $value['count'] }}</span><br/>
                                                                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#summary_link" href="#ready_to_initiate_link">Ready to Initiate</a>
                                </h6>
                            </div>
                            <div id="ready_to_initiate_link" class="collapse">
                                <div class="card-block">
                                    {{--pending benefit ready summary --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            @foreach(array_chunk($ready_to_initiate_counts, 4) as $ready_to_initiate_count)
                                                <br/>
                                                <div class="row">
                                                    @foreach($ready_to_initiate_count as $value)
                                                        <div class="col-md-3">
                                                            <a href="{{ url("/") . "/" . request()->route()->uri() }}?ready_to_initiate={{ $value['id'] }}">
                                                                <div class="grid-column" @if($value['class'] == 'blink') style="background: #e6f5f0;" @endif>
                                                                    {{ $value['name'] }}&nbsp;<span class="badge-summary" @if($value['class'] == 'blink') style="-webkit-animation: blink .3s step-end infinite alternate; border: 1px solid;border-color: #ff0000;" @endif>{{ $value['count'] }}</span><br/>
                                                                    {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                                                                </div>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <legend></legend>
        {{--Start : Custom filter--}}
        <div class="custom_filter">
            {!! Form::open(['role' => 'form', 'id' => 'search-checker-notification-form']) !!}
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="form-row">
                        {!! Form::hidden('type') !!}
                        {!! Form::hidden('overview') !!}
                        {{--Task Category--}}
                        <div class="form-group col-md-3">
                            <label for="category">Task Category</label>
                            {!! Form::select('category', ['0' => 'Notification Checklist', '1' => 'Notification Investigation',], null, ['class' => 'form-control search-select', 'id' => 'category', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                        {{--Incident Type--}}
                        <div class="form-group col-md-3">
                            <label for="incident">Incident Type</label>
                            {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident', 'style' => 'width:100%;']) !!}
                        </div>
                        {{--Notification Stages--}}
                        <div class="form-group col-md-6">
                            <label for="stage">Stage</label>
                            {!! Form::select('notification_stage_id', $stages, null, ['class' => 'form-control search-select', 'id' => 'incident_stage', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                        {{--Employee Name--}}
                        <div class="form-group col-md-4">
                            <label for="employee">Employee Name</label>
                            {!! Form::select('employee', [], null, ['class' => 'form-control employee-select', 'id' => 'employee', 'style' => 'width:100%;']) !!}
                        </div>
                        {{--Employer Name--}}
                        <div class="form-group col-md-4">
                            <label for="employer">Employer Name</label>
                            {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer', 'style' => 'width:100%;']) !!}
                        </div>
                        {{--Checker Priority--}}
                        <div class="form-group col-md-3">
                            <label for="priority">Priority:</label>
                            {!! Form::select('priority', ['1' => 'High', '0' => 'Low' ], null, ['class' => 'form-control search-select', 'id' => 'priority', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                        {{--Benefit Ready Selection--}}
                        {{--<div class="form-group col-md-3">
                            <label for="region">Benefit Ready:</label>
                            {!! Form::select('benefit_ready', $benefit_ready, null, ['class' => 'form-control search-select', 'id' => 'benefit_ready', 'placeholder' => '']) !!}
                        </div>--}}
                        {{--Ready To Initiate Selection--}}
                        <div class="form-group col-md-3">
                            <label for="region">Ready to Initiate:</label>
                            {!! Form::select('ready_to_initiate', $ready_to_initiate, null, ['class' => 'form-control search-select', 'id' => 'ready_to_initiate', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                        {{--Region Selection--}}
                        <div class="form-group col-md-3">
                            <label for="region">Incident Region:</label>
                            {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                            <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                        </div>
                        {{--District Selection--}}
                        <div class="form-group col-md-3">
                            <label for="region">Incident District:</label>
                            {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--End : Custom filter--}}
        <legend></legend>
        <br/>

        {{--Resource Datatable--}}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <table class="display table" id = "checker-notification-table" cellspacing="0" width="100%">
                    <thead>
                    <tr >
                        <th></th>
                        <th>Case#</th>
                        <th>Incident Type</th>
                        <th>Employee</th>
                        <th>Employer</th>
                        <th>Incident Date</th>
                        <th>Region</th>
                        <th>Stage</th>
                        <th>Task Category</th>
                        <th>Priority</th>
                        <th>Notification Date</th>
                        <th>Registration Date</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>

@push('after-script-end')
    <!-- Custom javascript files for this page -->

    <script id="details-template" type="text/x-handlebars-template">
        <div class="label label-info">Notification <b>@{{ filename }}</b> Status </div>
        <legend></legend>
        <div id="notification-@{{ resource_id }}"></div>
    </script>

    <script>
        $(function() {
            let $template = Handlebars.compile($("#details-template").html());
            $('#region').on('change', function (e) {
                var region_id = e.target.value;
                if (region_id) {
                    $("#spin2").show();
                    $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                        $('#district').empty();
                        $("#district").select2("val", "");
                        $('#district').html(data);
                        $("#spin2").hide();
                    });
                }
            });
            /* start : Searching Employer */
            $(".employer-select").select2({
                minimumInputLength: 1,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:selecting", function($e) {
                var $selected = $e.params.args.data.id;
            });
            /* end : Searching Employer */
            /* start : Searching Employee */
            $(".employee-select").select2({
                minimumInputLength: 3,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.compliance.employees') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.employee,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:selecting", function($e) {
                var $selected = $e.params.args.data.id;
            });
            /* end : Searching Employee */
            $( "#clear_filter" ).click(function() {
                /* Clear the Filter Form */
                $(".employer-select").val(null).trigger('change.select2');
                $(".employee-select").val(null).trigger('change.select2');
                $(".search-select").val(null).trigger('change.select2');
            });
            var $oTable = $('#checker-notification-table').DataTable({
                /*dom : 'Bfrtip',*/
                buttons : ['reload', 'colvis', 'print'],
                initComplete : function () {
                    $oTable.buttons().container().insertBefore('#checker-notification-table');
                    $("#checker-notification-table").css("width","100%");
                },
                processing: true,
                serverSide: true,
                info : true,
                bAutoWidth: false,
                width: false,
                ajax: {
                    url: "{!! route("backend.checker.get_datatable", $code_value_id) !!}",
                    method : "PUT",
                    data: function ($d) {
                        $d.category = $('select[name=category]').val();
                        $d.incident = $('select[name=incident]').val();
                        $d.employee = $('select[name=employee]').val();
                        $d.employer = $('select[name=employer]').val();
                        $d.region = $('select[name=region]').val();
                        $d.district = $('select[name=district]').val();
                        $d.priority = $('select[name=priority]').val();
                        $d.stage = $('select[name=notification_stage_id]').val();
                        /*$d.benefit_ready = $('select[name=benefit_ready]').val();*/
                        $d.ready_to_initiate = $('select[name=ready_to_initiate]').val();
                        $d.overview = $('input[name=overview]').val();
                        $d.type = $('input[name=type]').val();
                    }
                },
                columns: [
                    {
                        "className" : 'details-control',
                        "orderable" : false,
                        "searchable" : false,
                        "data" : null,
                        "defaultContent" : ''
                    },
                    {data: 'filename', name: 'notification_reports.filename', visible: false},
                    {data: 'incident', name: 'incident_types.name'},
                    {data: 'employee', name: 'employee', searchable: false, orderable: false},
                    {data: 'employer', name: 'employer_id', searchable: false, orderable: true},
                    {data: 'incident_date', name: 'notification_reports.incident_date', visible: false},
                    {data: 'region', name: 'regions.name', visible: false},
                    {data: 'stage', name: 'code_values.name', visible: false},
                    {data: 'category', name: 'notification_reports.notification_staging_cv_id', searchable: false, orderable: true, visible: true},
                    {data: 'priority_status', name: 'checkers.priority', searchable: false, orderable: true},
                    {data: 'receipt_date', name: 'notification_reports.receipt_date', searchable: false, orderable: true, visible: false},
                    {data: 'created_at', name: 'notification_reports.created_at', searchable: false, orderable: true, visible: false}
                ],
                'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                    $('td:not(:first-child)', $nRow).click(function() {
                        window.open(base_url + "/claim/notification_report/attend_profile/" + $aData['resource_id'], "_self");
                        /*targets : _blank , _self*/
                    }).hover(function() {
                        $(this).css('cursor', 'alias');
                    }, function() {
                        $(this).css('cursor', 'auto');
                    });
                }
            });
            // Add event listener for opening and closing details
            $('#checker-notification-table tbody').on('click', 'td.details-control', function () {
                var $tr = $(this).closest('tr');
                var $row = $oTable.row($tr);
                var $tableId = 'notification-' + $row.data().resource_id;

                if ($row.child.isShown()) {
                    // This row is already open - close it
                    $row.child.hide();
                    $tr.removeClass('shown');
                } else {
                    // Open this row
                    $row.child($template($row.data())).show();
                    initTableExpand($tableId, $row.data());
                    $tr.addClass('shown');
                    $tr.next().find('td').addClass('no-padding');
                }
            });
            $('#search-checker-notification-form').on('submit', function($e) {
                $oTable.draw();
                $e.preventDefault();
            });
        });

    </script>
@endpush