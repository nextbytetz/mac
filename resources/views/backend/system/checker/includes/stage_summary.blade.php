<div class="underline">Stage Summary</div>
<br/>
@php
    $count_stage = count($stage_counts);
    $array_total = ($count_stage > 1) ? $count_stage : 2;
@endphp
@foreach(array_chunk($stage_counts, $array_total / 2) as $values)
    <div style="display: inline-block;">
        @foreach($values as $stage_count )
            <div style="border: dotted 1px #333;border-radius: 3px;padding: 7px; @if($stage_count['class'] === 'blink') background: #e6f5f0; @endif">
                <a class="stage_summary" id="{{ 'stage_id' . $stage_count['id'] }}" href="{{ url('/') . '/' . request()->route()->uri() }}?{{ $uri_param }}={{ $stage_count['id'] }}">

                    {{ $stage_count['name'] }} &nbsp;&nbsp;
                    <span class="badge-summary @if($stage_count['class'] === 'blink') blink @endif" >{{ $stage_count['count'] }}</span>
                </a>
            </div>

        @endforeach
    </div>
@endforeach
<br/>
<br/>
{{--@foreach(array_chunk($stage_counts, 4) as $stage_count)
    <br/>
    <div class="row">
        @foreach($stage_count as $value)
            <div class="col-md-3">
                @php

                @endphp
                <a href="{{ url('/') . '/' . request()->route()->uri() }}?notification_stage_id={{ $value['id'] }}">
                    <div class="grid-column" @if($value['class'] === 'blink') style="background: #e6f5f0;" @endif>
                        {{ $value['name'] }}&nbsp;<span class="badge-summary @if($value['class'] === 'blink') blink @endif" >{{ $value['count'] }}</span><br/>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endforeach--}}
