{{--Inspection Plan--}}
@push('after-styles-end')

@endpush

<div class="row">
    <div class="col-md-9 col-sm-9">
        {{--todo: implement stage count for employer deregistration--}}
        <legend>{{ $category_name }}</legend>

        <div class="row">
            <div class="col-md-12">
                <div class="divider15"></div>
                <div class="card-accordions">
                    <div id="summary_link_payroll_alert" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#summary_link_payroll_alert" href="#stage_summary_payroll_alert">Stage Summary</a>
                                </h6>
                            </div>
                            <div id="stage_summary_payroll_alert" class="collapse">
                                <div class="card-block">
                                    {{--pending group count summary --}}
                                    <div class="pull-right">
                                        <a style="color:green" href="{{ route('backend.payroll.alert_monitor.refresh_alert_task_checker') }}" >Refresh Data</a>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('backend/system/checker/includes/stage_summary', ['stage_counts' => $stage_counts_payroll_alert, 'uri_param' => 'notification_stage'])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <legend></legend>

        {{--@if(!is_null(request()->input('notification_stage_id')))--}}
            {{--@php--}}
                {{--$stage_cv_ref = $cv_repo->find(request()->input('notification_stage_id'))->reference;--}}
            {{--@endphp--}}

            <div id="payroll-alert-tasks-content">
                @include('backend/system/checker/includes/include/CHCPAYALERT_includes/payroll_alert_monitor_content')
            </div>

        {{--@endif--}}


    </div>
</div>

@push('after-script-end')
    {{--<script id="payroll_alert-template" type="text/x-handlebars-template">--}}
        {{--<div class="label label-info">Payroll Alert Status </div>--}}
        {{--<legend></legend>--}}
        {{--<div id="payroll_alert-@{{ resource_id }}"></div>--}}
    {{--</script>--}}


    <script >
        $(function() {
            var url = "{!! url("/") !!}";

            $('.stage_summary').click(function(e){
                e.preventDefault();
                var element_id = this.id;
                var stage_id = element_id.substr(8);
                loadPayrollAlertTaskContent(stage_id);
            });


            // loadProductSalesContent(shift_id);
            function loadPayrollAlertTaskContent(stage_id) {

                hide_show('hide_class', 'staging_summary_payroll_alert_div');
                hide_show('show_id', 'stage_dt_div' + stage_id);

                $('.stage_summary').css({color: 'black'});
                $('#stage_id'+ stage_id).css({color: 'blue'});
                {{--$.get("{{ route('backend.payroll.alert_monitor.load_payroll_alert_content_inbox') }}", {'isinbox_task': 1, 'stage_id' :stage_id }, function (data) {--}}
                    {{--$("#payroll-alert-tasks-content").empty();--}}
                    {{--$(data).prependTo("#payroll-alert-tasks-content");--}}
                {{--}, "html").done()--}}



            }
        });


    </script>;

@endpush