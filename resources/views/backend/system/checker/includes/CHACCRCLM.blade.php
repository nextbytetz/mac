{{--Claim Accrual--}}

{{--Notification & Claim--}}
@push('after-styles-end')

@endpush

<div class="row">
	<div class="col-md-9 col-sm-9">
		<legend>Claims For Accrual</legend>
		<br/>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<table class="display" cellspacing="0" width="100%" id ="accrual-claim-table">
					<thead>
						<tr>
							{{-- <th>ID</th> --}}
							<th>@lang('labels.backend.legal.case_no')</th>
							<th>@lang('labels.backend.claim.incident_type')</th>
							<th>@lang('labels.backend.compliance.employee')</th>
							<th>@lang('labels.backend.compliance.employer')</th>
							<th>@lang('labels.backend.table.incident_date')</th>
							<th>@lang('labels.backend.claim.receipt_date')</th>
							<th>@lang('labels.general.region')</th>
							<th>Checklist User</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>

@push('after-script-end')
<!-- Custom javascript files for this page -->

<script>
	$(function() {
		
		
		var $oTable = $('#accrual-claim-table').DataTable({
			buttons : ['reload', 'colvis'],
			initComplete : function () {
				$oTable.buttons().container().insertBefore('#accrual-claim-table');
				$("#accrual-claim-table").css("width","100%");
			},
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! route('backend.claim.claim_accrual.checker_datatable') !!}',
				type : 'get'
			},
			columns: [
			// { data: 'case_no' , name: 'notification_reports.id' ,orderable : true, searchable : false},
			{ data: 'filename' , name: 'filename' ,orderable : true, searchable : true},
			{ data: 'incident_type' , name: 'incident_types.name'},
			{ data: 'fullname' , name: 'fullname', orderable : false, searchable : false},
			{ data: 'employer' , name: 'employers.name', orderable : true, searchable : true},
			{ data: 'incident_date' , name: 'notification_reports.incident_date', orderable : true, searchable : true},
			{ data: 'receipt_date' , name: 'notification_reports.receipt_date', orderable : true, searchable : true},
			{ data: 'region' , name: 'regions.name'},
			{ data: 'checklistuser' , name: 'users.firstname', orderable : true, searchable : true},
			/*{ data: 'status' , name: 'status', orderable : false, searchable :  false, visible : false},*/
			{ data: 'firstname' , name: 'employees.firstname', orderable : false, searchable : true, visible : false},
			{ data: 'middlename' , name: 'employees.middlename', orderable : false, searchable : true, visible : false},
			{ data: 'lastname' , name: 'employees.lastname', orderable : false, searchable : true, visible : false}
			],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$(nRow).click(function() {
					document.location.href ="{{url('/claim_accrual/profile/')}}/"  + aData['case_no'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});
		$('#search-recall-notification-form').on('submit', function($e) {
			$oTable.draw();
			$e.preventDefault();
		});

	});

</script>
@endpush
