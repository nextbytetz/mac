{{--Online Notification Application--}}
@push('after-styles-end')

@endpush

<div class="row">
    <div class="col-md-9 col-sm-9">
        {{--todo: implement stage count for Online Notification Application here--}}
        <legend>{{ $category_name }}</legend>

        <div class="row">
            <div class="col-md-12">
                <div class="divider15"></div>
                <div class="card-accordions">
                    <div id="summary_link_online_notification_application" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#summary_link_online_notification_application" href="#stage_summary_online_notification_application">Stage Summary</a>
                                </h6>
                            </div>
                            <div id="stage_summary_online_notification_application" class="collapse">
                                <div class="card-block">
                                    {{--pending group count summary --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('backend/system/checker/includes/stage_summary', ['stage_counts' => $stage_counts_online_notification_application, 'uri_param' => 'online_notification_application_stage'])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h6 class="mb-0">
                                                            <a class="card-title" data-toggle="collapse" data-parent="#summary_link" href="#ready_to_initiate_link">Ready to Initiate</a>
                                                        </h6>
                                                    </div>
                                                    <div id="ready_to_initiate_link" class="collapse">
                                                        <div class="card-block">
                                                            --}}{{--pending benefit ready summary --}}{{--
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    @foreach(array_chunk($ready_to_initiate_counts, 4) as $ready_to_initiate_count)
                                                                        <br/>
                                                                        <div class="row">
                                                                            @foreach($ready_to_initiate_count as $value)
                                                                                <div class="col-md-3">
                                                                                    <a href="{{ url("/") . "/" . request()->route()->uri() }}?ready_to_initiate={{ $value['id'] }}">
                                                                                        <div class="grid-column" @if($value['class'] == 'blink') style="background: #e6f5f0;" @endif>
                                                                                            {{ $value['name'] }}&nbsp;<span class="badge-summary" @if($value['class'] == 'blink') style="-webkit-animation: blink .3s step-end infinite alternate; border: 1px solid;border-color: #ff0000;" @endif>{{ $value['count'] }}</span><br/>
                                                                                            --}}{{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}{{--
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>--}}

                    </div>
                </div>
            </div>
        </div>

        <legend></legend>
        {{--Start : Custom filter--}}
        <div class="custom_filter">
            {!! Form::open(['role' => 'form', 'id' => 'search-checker-online_notification_application-form']) !!}
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <div class="form-row">
                        {{--online_notification_application Stages--}}
                        <div class="form-group col-md-6">
                            <label for="stage">Stage</label>
                            {!! Form::select('online_notification_application_stage', $stages_online_notification_application, null, ['class' => 'form-control search-select', 'id' => 'online_notification_application_stage', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="button" class="btn btn-secondary site-btn clear_filter" id="clear_filter" value="Clear" />
                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--End : Custom filter--}}
        <legend></legend>
        <br/>

        {{--Resource Datatable--}}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <table class="display table" id = "checker-online_notification_application-plan-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Incident Type</th>
                        <th>Employee</th>
                        <th>Employer</th>
                        <th>Region</th>
                        <th>District</th>
                        <th>Incident Date</th>
                        <th>Reporting Date</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>

@push('after-script-end')
    <script id="online_notification_applications-template" type="text/x-handlebars-template">
        <div class="label label-info">Online Notification Application <b>@{{ incident_type }}</b> Status </div>
        <legend></legend>
        <div id="online_notification_application-@{{ resource_id }}"></div>
    </script>
    <script>
        $(function() {
            var $template = Handlebars.compile($("#online_notification_applications-template").html());
            var $inspTable = $('#checker-online_notification_application-plan-table').DataTable({
                /*dom : 'Bfrtip',*/
                buttons : ['reload', 'colvis', 'print'],
                initComplete : function () {
                    $inspTable.buttons().container().insertBefore('#checker-online_notification_application-plan-table');
                    $("#checker-online_notification_application-plan-table").css("width","100%");
                },
                processing: true,
                serverSide: true,
                info : true,
                bAutoWidth: false,
                width: false,
                ajax: {
                    url: "{!! route("backend.checker.get_datatable", $code_value_id) !!}",
                    method : "PUT",
                    data: function ($d) {
                        $d.online_notification_application_stage = $('select[name=online_notification_application_stage]').val();
                    }
                },
                columns: [
                    {
                        "className" : 'online_notification_applications-control',
                        "orderable" : false,
                        "searchable" : false,
                        "data" : null,
                        "defaultContent" : ''
                    },
                    {data: 'incident_type', name: 'main.incident_types.name'},
                    {data: 'employee', name: 'employees.firstname'},
                    {data: 'employer', name: 'employers.name'},
                    {data: 'region', name: 'main.regions.name'},
                    {data: 'district', name: 'main.districts.name'},
                    {data: 'incident_date', name: 'main.incidents.incident_date'},
                    {data: 'reporting_date', name: 'main.incidents.reporting_date'},
                    {data: 'middlename', name: 'employees.middlename', orderable: false, visible: false},
                    {data: 'lastname', name: 'employees.lastname', orderable: false, visible: false},
                ],
                'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                    $('td:not(:first-child)', $nRow).click(function() {
                        window.open(base_url + "/compliance/employer/" + $aData['resource_id'] + "/incident_profile", "_self");
                        /*targets : _blank , _self*/
                    }).hover(function() {
                        $(this).css('cursor', 'alias');
                    }, function() {
                        $(this).css('cursor', 'auto');
                    });
                }
            });
            $('#search-checker-online_notification_application-form').on('submit', function($e) {
                $inspTable.draw();
                $e.preventDefault();
            });
        });
    </script>
@endpush