


{{--Pensioners pending for activation count--}}
{{--<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALEPENAC') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">--}}
    {{--@include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pensioners_ready_for_payroll_datatable',  ['isinbox_task' => 1])--}}

{{--</div>--}}
{{--Dependents pending for activation count--}}

{{--<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALEDEPAC') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">--}}
    {{--@include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_dependents_ready_for_payroll_datatable', ['isinbox_task' => 1])--}}
{{--</div>--}}
{{--@endif--}}

{{--<!-- Put the page specifically for this page here -->--}}
{{--<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALECHSALR') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">--}}
    {{--Child suspensions list--}}
    {{--@include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_suspension_for_datatable',  ['isinbox_task' => 1])--}}
{{--</div>--}}
{{--Child alert who approach 18yrs old--}}
{{--<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALECHA18') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">--}}
    {{--@include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_child_alert_for_datatable',  ['isinbox_task' => 1])--}}
{{--</div>--}}
{{--Retiree suspensions count--}}
{{--<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALERETSU') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">--}}
    {{--@include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_retiree_suspensions_datatable',  ['isinbox_task' => 1])--}}
{{--</div>--}}

{{--Documents : pending for verifications --}}
<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALEPEDVER') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">
    @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pending_documents_need_action',['document_type'=>63 , 'isinbox_task' => 1])

</div>
{{--Documents : pending for bank updates --}}
<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALEPEDBAN') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">
    @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pending_documents_need_action',['document_type'=>64, 'isinbox_task' => 1])

</div>
{{--Documents : pending for beneficiary details updates --}}
<div id="{{  'stage_dt_div' .$cv_repo->findIdByReference('PALEPEDDET') }}" class="staging_summary_payroll_alert_div" style="{{  'display:none;'}}">
    @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pending_documents_need_action',['document_type'=>65, 'isinbox_task' => 1])

</div>
