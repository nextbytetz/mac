{{--Inspection Plan--}}
@push('after-styles-end')

@endpush

<div class="row">
    <div class="col-md-9 col-sm-9">
        {{--todo: implement stage count for inspection plan here--}}
        <legend>{{ $category_name }}</legend>

        <div class="row">
            <div class="col-md-12">
                <div class="divider15"></div>
                <div class="card-accordions">
                    <div id="summary_link_letter" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#summary_link_letter" href="#stage_summary_letter">Stage Summary</a>
                                </h6>
                            </div>
                            <div id="stage_summary_letter" class="collapse">
                                <div class="card-block">
                                    {{--pending group count summary --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('backend/system/checker/includes/stage_summary', ['stage_counts' => $stage_counts_letter, 'uri_param' => 'letter_stage'])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h6 class="mb-0">
                                                            <a class="card-title" data-toggle="collapse" data-parent="#summary_link" href="#ready_to_initiate_link">Ready to Initiate</a>
                                                        </h6>
                                                    </div>
                                                    <div id="ready_to_initiate_link" class="collapse">
                                                        <div class="card-block">
                                                            --}}{{--pending benefit ready summary --}}{{--
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    @foreach(array_chunk($ready_to_initiate_counts, 4) as $ready_to_initiate_count)
                                                                        <br/>
                                                                        <div class="row">
                                                                            @foreach($ready_to_initiate_count as $value)
                                                                                <div class="col-md-3">
                                                                                    <a href="{{ url("/") . "/" . request()->route()->uri() }}?ready_to_initiate={{ $value['id'] }}">
                                                                                        <div class="grid-column" @if($value['class'] == 'blink') style="background: #e6f5f0;" @endif>
                                                                                            {{ $value['name'] }}&nbsp;<span class="badge-summary" @if($value['class'] == 'blink') style="-webkit-animation: blink .3s step-end infinite alternate; border: 1px solid;border-color: #ff0000;" @endif>{{ $value['count'] }}</span><br/>
                                                                                            --}}{{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}{{--
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>--}}

                    </div>
                </div>
            </div>
        </div>

        <legend></legend>
        {{--Start : Custom filter--}}
        <div class="custom_filter">
            {!! Form::open(['role' => 'form', 'id' => 'search-checker-letter-form']) !!}
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <div class="form-row">
                        {{--Inspection Stages--}}
                        <div class="form-group col-md-6">
                            <label for="stage">Stage</label>
                            {!! Form::select('letter_stage', $stages_letter, null, ['class' => 'form-control search-select', 'id' => 'letter_stage', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="button" class="btn btn-secondary site-btn clear_filter" id="clear_filter" value="Clear" />
                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--End : Custom filter--}}
        <legend></legend>
        <br/>

        {{--Resource Datatable--}}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <table class="display table" id = "checker-letter-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Reference</th>
                        <th>Type</th>
                        <th>Stage</th>
                        <th>Letter Date</th>
                        {{--<th>Status</th>--}}
                        <th>Priority</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>

@push('after-script-end')
    <script id="letter-template" type="text/x-handlebars-template">
        <div class="label label-info">Letter <b>@{{ reference }}</b> Status </div>
        <legend></legend>
        <div id="letter-@{{ resource_id }}"></div>
    </script>
    <script>
        $(function() {
            var $template = Handlebars.compile($("#letter-template").html());
            var $letterTable = $('#checker-letter-table').DataTable({
                /*dom : 'Bfrtip',*/
                buttons : ['reload', 'colvis', 'print'],
                initComplete : function () {
                    $letterTable.buttons().container().insertBefore('#checker-letter-table');
                    $("#checker-letter-table").css("width","100%");
                },
                processing: true,
                serverSide: true,
                info : true,
                bAutoWidth: false,
                width: false,
                ajax: {
                    url: "{!! route("backend.checker.get_datatable", $code_value_id) !!}",
                    method : "PUT",
                    data: function ($d) {
                        $d.letter_stage = $('select[name=letter_stage]').val();
                    }
                },
                columns: [
                    {
                        "className" : 'letters-control',
                        "orderable" : false,
                        "searchable" : false,
                        "data" : null,
                        "defaultContent" : ''
                    },
                    {data: 'reference', name: 'letters.reference'},
                    {data: 'letter_type', name: 'b.name'},
                    {data: 'stage', name: 'a.name'},
                    {data: 'letter_date', name: 'letters.letter_date'},
                    {data: 'priorityall', name: 'checkers.priority', searchable: false, orderable: true},
                ],
                'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                    $('td:not(:first-child)', $nRow).click(function() {
                        window.open(base_url + "/letter/show/" + $aData['resource_id'], "_self");
                        /*targets : _blank , _self*/
                    }).hover(function() {
                        $(this).css('cursor', 'alias');
                    }, function() {
                        $(this).css('cursor', 'auto');
                    });
                }
            });
            $('#search-checker-letter-form').on('submit', function($e) {
                $letterTable.draw();
                $e.preventDefault();
            });
        });
    </script>
@endpush