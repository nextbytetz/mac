@push('after-styles-end')

@endpush

<div class="row">
	<div class="col-md-9 col-sm-9">
		<legend>Notifications Planned For Investigation</legend>
		<br/>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<table class="display" cellspacing="0" width="100%" id ="investigation-plan-table">
					<thead>
						<tr>
							{{-- <th>ID</th> --}}
							<th>@lang('labels.backend.legal.case_no')</th>
							<th>@lang('labels.backend.claim.incident_type')</th>
							<th>@lang('labels.backend.compliance.employee')</th>
							<th>@lang('labels.backend.compliance.employer')</th>
							<th>@lang('labels.backend.table.incident_date')</th>
							<th>@lang('labels.backend.claim.receipt_date')</th>
							<th>@lang('labels.general.region')</th>
							{{-- <th>Checklist User</th> --}}
						</tr>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>

@push('after-script-end')
<!-- Custom javascript files for this page -->

<script>
	$(function() {
		
		
		var $oTable = $('#investigation-plan-table').DataTable({
			buttons : ['reload', 'colvis'],
			initComplete : function () {
				$oTable.buttons().container().insertBefore('#investigation-plan-table');
				$("#investigation-plan-table").css("width","100%");
			},
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! route("backend.claim.investigations.get_user_pending_allocated_files",access()->user()->id) !!}',
				type : 'PUT'
			},
			columns: [
			{data: 'filename', name: 'notification_reports.filename', visible: true},
			{data: 'incident', name: 'incident_types.name', visible: true,searchable: false,orderable: false},
			{data: 'employee', name: 'employees.firstname', searchable: true, orderable: false, visible:true},
			{data: 'employer', name: 'employers.name', searchable: false, orderable: false},
			{data: 'incident_date', name: 'incident_date.incident_date', searchable: true, orderable: true, visible: false},
			{data: 'receipt_date', name: 'notification_reports.receipt_date', searchable: false, orderable: false,visible:true},
			{data: 'region', name: 'regions.name', searchable: false, orderable: false, visible: true},
			],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$(nRow).click(function() {
					document.location.href ="{{url('/claim/notification_report/profile/')}}/"  + aData['id'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});
		$('#search-recall-notification-form').on('submit', function($e) {
			$oTable.draw();
			$e.preventDefault();
		});

	});

</script>
@endpush
