{{--Inspection Plan--}}
@push('after-styles-end')

@endpush

<div class="row">
    <div class="col-md-9 col-sm-9">
        {{--todo: implement stage count for employer inspection task plan here--}}
        <legend>{{ $category_name }}</legend>

        <div class="row">
            <div class="col-md-12">
                <div class="divider15"></div>
                <div class="card-accordions">
                    <div id="summary_link_employer_inspection" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#summary_link_employer_inspection" href="#stage_summary_employer_inspection">Stage Summary</a>
                                </h6>
                            </div>
                            <div id="stage_summary_employer_inspection" class="collapse">
                                <div class="card-block">
                                    {{--pending group count summary --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('backend/system/checker/includes/stage_summary', ['stage_counts' => $stage_counts_employer_task, 'uri_param' => 'employer_inspection_stage'])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--
                        <div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#summary_link" href="#ready_to_initiate_link">Ready to Initiate</a>
                                </h6>
                            </div>
                            <div id="ready_to_initiate_link" class="collapse">
                                <div class="card-block">
                                    --}}{{--pending benefit ready summary --}}{{--
                                    <div class="row">
                                        <div class="col-md-12">
                                            @foreach(array_chunk($ready_to_initiate_counts, 4) as $ready_to_initiate_count)
                                                <br/>
                                                <div class="row">
                                                    @foreach($ready_to_initiate_count as $value)
                                                        <div class="col-md-3">
                                                            <a href="{{ url("/") . "/" . request()->route()->uri() }}?ready_to_initiate={{ $value['id'] }}">
                                                                <div class="grid-column" @if($value['class'] == 'blink') style="background: #e6f5f0;" @endif>
                                                                    {{ $value['name'] }}&nbsp;<span class="badge-summary" @if($value['class'] == 'blink') style="-webkit-animation: blink .3s step-end infinite alternate; border: 1px solid;border-color: #ff0000;" @endif>{{ $value['count'] }}</span><br/>
                                                                    --}}{{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}{{--
                                                                </div>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}

                    </div>
                </div>
            </div>
        </div>


        <legend></legend>
        {{--Start : Custom filter--}}
        <div class="custom_filter">
            {!! Form::open(['role' => 'form', 'id' => 'search-checker-employer_inspection-form']) !!}
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <div class="form-row">
                        {{--Employer Inspection Task Stages--}}
                        <div class="form-group col-md-6">
                            <label for="stage">Stage</label>
                            {!! Form::select('employer_inspection_stage', $stages_employer_task, null, ['class' => 'form-control search-select', 'id' => 'employer_inspection_stage', 'placeholder' => '', 'style' => 'width:100%;']) !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="button" class="btn btn-secondary site-btn clear_filter" id="clear_filter" value="Clear" />
                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--End : Custom filter--}}
        <legend></legend>
        <br/>

        {{--Resource Datatable--}}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <table class="display table" id = "checker-employer_inspection_task-plan-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Task Type</th>
                            <th>Employer</th>
                            <th>Attended</th>
                            <th>Stage</th>
                            <th>Region</th>
                            <th>District</th>
                            <th>Visit Date</th>
                            <th>Priority</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>

@push('after-script-end')
    <script id="employer_inspection_task-template" type="text/x-handlebars-template">
        <div class="label label-info">Employer Inspection Task <b>@{{ inspection_type }}</b> Status </div>
        <legend></legend>
        <div id="employer_inspection_task-@{{ resource_id }}"></div>
    </script>
    <script>
        $(function() {
            var $template = Handlebars.compile($("#employer_inspection_task-template").html());
            var $employerInspTaskTable = $('#checker-employer_inspection_task-plan-table').DataTable({
                /*dom : 'Bfrtip',*/
                buttons : ['reload', 'colvis', 'print'],
                initComplete : function () {
                    $employerInspTaskTable.buttons().container().insertBefore('#checker-employer_inspection_task-plan-table');
                    $("#checker-employer_inspection_task-plan-table").css("width","100%");
                },
                processing: true,
                serverSide: true,
                info : true,
                bAutoWidth: false,
                width: false,
                ajax: {
                    url: "{!! route("backend.checker.get_datatable", $code_value_id) !!}",
                    method : "PUT",
                    data: function ($d) {
                        $d.employer_inspection_stage = $('select[name=employer_inspection_stage]').val();
                    },
                },
                columns: [
                    {
                        "className" : 'inspections-control',
                        "orderable" : false,
                        "searchable" : false,
                        "data" : null,
                        "defaultContent" : ''
                    },
                    {data: 'task_type', name: 'a.name'},
                    {data: 'employer', name: 'employers.name', orderable: true, searchable: true},
                    {data: 'attend_status', name: 'employer_inspection_task.attended', searchable: false, orderable: true},
                    {data: 'stage', name: 'b.name', searchable: true, orderable: false},
                    {data: 'region', name: 'regions.name'},
                    {data: 'district', name: 'districts.name', visible: false},
                    {data: 'visit_date', name: 'employer_inspection_task.visit_date', visible: false},
                    {data: 'priority_status', name: 'checkers.priority', orderable: true, searchable: false},
                ],
                'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                    $('td:not(:first-child)', $nRow).click(function() {
                        window.open(base_url + "/compliance/inspection/employer_task/" + $aData['resource_id'], "_self");
                        /*targets : _blank , _self*/
                    }).hover(function() {
                        $(this).css('cursor', 'alias');
                    }, function() {
                        $(this).css('cursor', 'auto');
                    });
                }
            });
            $('#search-checker-employer_inspection-form').on('submit', function($e) {
                $employerInspTaskTable.draw();
                $e.preventDefault();
            });
        });
    </script>
@endpush