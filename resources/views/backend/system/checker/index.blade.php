@extends('layouts.backend.main', ['title' => "Checker Inbox & Tasks", 'header_title' => "Pending Tasks"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        @-webkit-keyframes blink { 50% { border-color: #ff0000; }  }
        .custom_filter:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "@lang('labels.backend.system.workflow.custom_filter')";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .custom_filter {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    @php
        $checkerScriptAlreadyIncluded = false;
    @endphp

    <div class="row">
        <div class="col-md-12">
            @if ($categories->count())
                <div class="divider15"></div>
                <div class="nav-tab-vertical-left">
                    <ul class="nav nav-tabs float-xs-left" role="tablist">
                        @foreach ($categories as $category)
                            <li class="nav-item">
                                <a class="nav-link @if ($loop->first) active @endif" data-toggle="tab" href="#{{ $category->reference }}" role="tab">{{ $category->name }}&nbsp;
                                    @php
                                        $count = $category->checkers()->where('status', 0)->where('shouldcount', 1)->whereIn('user_id', access()->allUsers())->count();
                                        $notifyCount = $category->checkers()->where(['status' => 0, 'notify' => 1])->whereIn("user_id", access()->allUsers())->count();
                                    @endphp
                                    <span class="tag tag-pill tag-primary @if($notifyCount) blink  @endif "><b>{{ $count }}</b></span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div data-plugin="scrollbar" data-height="150">
                        <div class="tab-content">
                            @foreach ($categories as $category)
                                @php
                                    $notifyCount = $category->checkers()->where(['status' => 0, 'notify' => 1])->whereIn("user_id", access()->allUsers())->count();
                                @endphp
                                <div class="tab-pane @if ($loop->first) active @endif" id="{{ $category->reference }}" role="tabpanel">
                                    @include("backend/system/checker/includes/{$category->reference}", ["code_value_id" => $category->id, "category_name" => $category->name, 'notify_count' => $notifyCount])
                                </div>
                                @php
                                    $checkerScriptAlreadyIncluded = true;
                                @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            @else
                <div class="alert-left-border">
                    <div class="alert alert-primary alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>No Pending Tasks.</strong> No allocated tasks waiting for your action
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/handlebars.js") }}
<script>
    $(function() {
        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });
        $( ".clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".search-select").val(null).trigger('change.select2');
        });
        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show').trigger('click');
        }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var $tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + $tab);
            } else {
                location.hash = '#' + $tab;
            }
        });
    });
    function initTableExpand($tableId, $data) {
        var $div = '#' + $tableId;
        $.post( $data.details_url, {}, function( data ) {
            $( $div ).html( data );
        }, "html").done(function() {
            /* When Done */
        });
    }
</script>
@endpush