@extends('layouts.backend.main', ['title' => trans('labels.backend.investment_menu.bond_calculator'), 'header_title' => trans('labels.backend.investment_menu.bond_calculator')])

@section('content')


<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
.error{
  display: none;
  margin-left: 10px;
  } .error_show{
    color: red;
    margin-left: 10px;
  }</style></head>
  <body>
    <br>
    <div class="container">
      <form>
        <table class="table" style="border: 1px solid #dddddd;">
          <thead>
            <tr><th style="background-color:#EFEFEF;" colspan="2">Bond Pricing Calculator: Inputs</th>
            </tr></thead>
            <tbody>
              <tr>
                <td>Face Value/Par Value ($)<span style="color: red;">*</span></td>
                <td><input type="text" class="form-control bond" id="PAR_VALUE" value="" ><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Yield (%) <span style="color: red;">*</span></td>
                <td><input type="number" class="form-control bond" id="YIELD" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Coupon Rate (%) <span style="color: red;">*</span></td>
                <td><input type="number" class="form-control bond" id="RATE" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Auction Date <span style="color: red;">*</span></td>
                <td><input type="date" class="form-control bond" id="AUCTION" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Maturity Date <span style="color: red;">*</span></td>
                <td><input type="date" class="form-control bond" id="YEARS_TO_MATURITY" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Next Coupon <span style="color: red;">*</span></td>
                <td><input type="date" class="form-control bond" id="NEXT_COUPON" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Last Coupon <span style="color: red;">*</span></td>
                <td><input type="date" class="form-control bond" id="LAST_COUPON" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Value/Settlement Date <span style="color: red;">*</span></td>
                <td><input type="date" class="form-control bond" id="VALUE_DATE" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Clean Price</td>
                <td><input type="number" class="form-control bond" id="CLEAN_PRICE" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Difference btw "x & y" <span style="color: red;">*</span></td>
                <td><input type="date" class="form-control bond" id="DIFFERENCE" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Full coupon period (6 months)</td>
                <td><input type="number" class="form-control bond" id="FULL_COUPON_RATE" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Accrued Interest</td>
                <td><input type="number" class="form-control bond" id="ACCRUED_INTEREST" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Dirty price</td>
                <td><input type="number" class="form-control bond" id="DIRTY_PRICE" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr>
                <td>Actual Cost</td>
                <td><input type="text" class="form-control bond" id="ACTUAL" value=""><span class="error">This field is required</span></td>
              </tr>
              <tr> 
               <td colspan="2" class="text-center"><button  id="save" class="btn btn-info" ><i class="fa fa-calculator">&nbsp;Compute Bond Pricing</i></button></td>
             </tr>
           </tbody>
         </table>

         <table class="table" style="border: 1px solid #dddddd;">
          <thead>
            <tr><th style="background-color:#EFEFEF;" colspan="2">Bond Pricing Calculator: Outputs</th>
            </tr></thead>
            <tbody>
              <tr>
                <td style="width:40%">Cost:</td><td><input type="number" class="form-control prctotal" id="cost" placeholder="Hit Compute Bond Pricing to See!"></td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>

      @endsection
      @push('after-script-end')
      {{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
      {{-- {{ Html::script(asset_url() . "/nextbyte/global/plugins/jquery/dist/jquey.min.js")}} --}}

      <script type="text/javascript">
        $(document).ready(function(e){
          $('#save').click(function(e){
            e.preventDefault();
            // let clean_price = price($settlement, $maturity, $rate, $yield, $redemption, $frequency, $basis);
            console.log('imefika')
            let settlement =  $('#VALUE_DATE').val();
            let maturity =  $('#YEARS_TO_MATURITY').val();
            let rate =  $('#RATE').val();
            let Yield =  $('#YIELD').val();
            let redemption = 100;
            let frequency = 2;
            let basis = 3;
            let full_coupon = $('#FULL_COUPON_RATE').val();
            let next_coupon = $('#NEXT_COUPON').val();
            let auction = $('#AUCTION').val();
            let last_coupon = $('#LAST_COUPON').val();
            let face_value = $('#PAR_VALUE').val();
            let accrued_interest_price = $('#ACCRUED_INTEREST').val();
            let dirty_price  = $('#DIRTY_PRICE').val();
            let actual = $('#ACTUAL').val();
            // console.log(face_value);
           

            let urlz = "{{'/investment/bond_calculator'}}"
             $.ajax({
                data : {
                  'settlement' : settlement,
                  'maturity' : maturity,
                  'rate' : rate,
                  'yield' : Yield,
                  'redemption' : redemption,
                  'frequency' : frequency,
                  'basis' : basis,
                  'full_coupon' : full_coupon,
                  'next_coupon' : next_coupon,
                  'last_coupon' : last_coupon,
                  'auction' : auction,
                  'accrued_interest_price' : accrued_interest_price,
                  'face_value' : face_value,
                  'actual' : actual, 
                },
                dataType : "json",
                method : "get",
                url : urlz,
                success : function (data) {
                  if (data) {
                     $('#CLEAN_PRICE').val(data.price);
                     $('#DIRTY_PRICE').val(data.dirty_price);
                     $('#ACCRUED_INTEREST').val(data.accrued_interest_price);
                     $('#FULL_COUPON_RATE').val(data.full_coupon);
                     $('#ACTUAL').val(data.actual);
                     $('#cost').val(data.cost);

                  }
                }
              })
                    
          })
        })

  

  $('#PAR_VALUE').maskMoney({
    precision : 2,
    affixesStay : false
  });
  $('#ACTUAL').maskMoney({
    precision : 2,
    affixesStay : false
  });
  $('#COST').maskMoney({
    precision : 2,
    affixesStay : false
  });

</script>
<div style="clear: both; display: block;"></div></body></html>
@endpush