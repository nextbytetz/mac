@extends('layouts.backend.main', ['title' => trans('labels.general.investment_header'), 'header_title' => trans('labels.general.investment_header')])


@section('content')

<div style="color:#fff">
    <div class="col-sm-6 col-md-6">
        <div class="list-group">
            {{--Add investment--}}
            <ul class="list-unstyled">
                <a href="{!! route('backend.investment.choose_type') !!}">
                    <li  class="border-less" class="list-group-item" >  
                        <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-plus-square"></i>
                            <large>&nbsp;&nbsp;Add Investment</large>
                        </h6>
                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                            Add new investment-choose investment type
                        </p> 
                    </li>
                </a>
            </ul>
            <br/>
            {{--Information Dashboard--}}
            <ul class="list-unstyled">
                <a href="{!! route("backend.investment.dashboard") !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-area-chart"> </i><large>&nbsp;&nbsp;Investment Portfolio Dashboard</large></h6>
                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Investment portfolio highlights</p> </li>
                    </a>
                </ul>
                <br/>
                 {{--Bond Calculator--}}
            <ul class="list-unstyled">
                <a href="{!! route("backend.investment.budget.calculator") !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-calculator"> </i><large>&nbsp;&nbsp;Bond Calculator</large></h6>
                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Bond Calculator for Price</p> </li>
                    </a>
                </ul>
                <br/>
            </div>
        </div>

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--Recall Notification item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.investment.summary.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-search"> </i><large>&nbsp;&nbsp;@lang('labels.backend.investment_menu.recall_investment')</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                @lang('labels.backend.investment_menu.recall_investment_summary')
                            </p> 
                        </li>
                    </a>
                </ul>
                <br/>

                {{--Investment Defaults--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.investment.budget.index") !!}">
                        <li  class="border-less" class="list-group-item" >  
                            <h6 class="list-group-item-heading ng-binding">
                                <i class="icon fa fa-terminal" aria-hidden="true"></i>
                                <large>&nbsp;&nbsp;Defaults</large>
                            </h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                Set default values for investment
                            </p> 
                        </li>
                    </a>
                </ul>
            </div>
        </div>
    </div>
    @stop

    @push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {

            $("#preview").click(function() {

            });
        });
    </script>;

    @endpush