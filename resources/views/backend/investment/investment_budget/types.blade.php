@extends('layouts.backend.main', ['title' => $budget->fin_year->name.' Investment Budget', 'header_title' => $budget->fin_year->name.' Investment Budget'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	/* start: File Icons CSS */
	#folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
	#folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
	#folder-tree .file-pdf { background-position: -32px 0 }
	#folder-tree .file-as { background-position: -36px 0 }
	#folder-tree .file-c { background-position: -72px -0px }
	#folder-tree .file-iso { background-position: -108px -0px }
	#folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
	#folder-tree .file-cf { background-position: -162px -0px }
	#folder-tree .file-cpp { background-position: -216px -0px }
	#folder-tree .file-cs { background-position: -236px -0px }
	#folder-tree .file-sql { background-position: -272px -0px }
	#folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
	#folder-tree .file-h { background-position: -488px -0px }
	#folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
	#folder-tree .file-php { background-position: -108px -18px }
	#folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
	#folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
	#folder-tree .file-rb { background-position: -180px -18px }
	#folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
	#folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
	#folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
	#folder-tree .file-js { background-position: -434px -18px }
	#folder-tree .file-css { background-position: -144px -0px }
	#folder-tree .file-fla { background-position: -398px -0px }
	/* end: File Icon CSS */
	/* start: upload progress bar css */
	.progress-bar {
		background-color: #12CC1A;
		height:20px;
		color: #FFFFFF;
		width:0%;
		-webkit-transition: width .3s;
		-moz-transition: width .3s;
		transition: width .3s;
	}
	.progress-div {
		border:#0FA015 1px solid;
		padding: 5px 0px;
		margin:30px 0px;
		border-radius:4px;
		text-align:center;
	}
	/* end: upload progress bar css */
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
</style>
@endpush

@section('content')


<div class = "row">

	<div>&nbsp;</div>
	{{--Tabs navigation--}}
	<div class = "row">
		<div class="col-md-12">

			<div class="basic_nav_pills nav_basic_tab">
				<ul class="nav nav-tabs">
					{{--General--}}
					<li class="nav-item">
						<a class="nav-link active" href="#general"
						data-toggle="tab">@lang('labels.general.general')
					</a>
				</li>
				{{--Bills Generated--}}
				<li class="nav-item">
					<a class="nav-link"  id = "tab_2_header"  href="#workflow" data-toggle="tab">Workflow</a>
				</li>
			</ul>
			<br>
			<div class="nav_tab_contain tab-content">
				<div id="general" class="nav_tab_pane tab-pane active in">
					<div class = "row">
						<div class="col-md-12">
							<div class="col-md-9">
								{{-- datatable --}}
								<table class="display" cellspacing="0" width="100%" id ="budget_datatable">
									<thead>
										<tr>
											<th>Invetsment Type</th>
											<th>Amount</th>
											<th>Percent</th>
											{{-- <th>Action</th> --}}
										</tr>
									</thead>
								</table>
							</div>
							<div class="col-md-3">
								{{-- summary --}}
								<div class="row">
									<div class="grey_modal">
										<table style="width:100%">
											<tr>
												<td  align="center"><h5><strong><span class="light_dark_color">Investment Budget Summary</span></strong></h5></td>
											</tr>
										</table>
									</div>
								</div>

								<div class="row">
									<div class="light_grey_bg">&nbsp;</div>
									<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
										<p class="underline" style="padding-bottom: 2%;" >
											<span style="font-weight: lighter;">Period :</span> 
											<span style="font-weight: bold;" >{{$budget->fin_year->name}}</span>
										</p>
										<p class="underline" style="padding-top: 2%;">
											<span style="font-weight: lighter;">Total Budget :</span> 
											<span style="font-weight: bold;" > {{number_format($budget->amount,2)}}/=</span>
										</p>
										<p class="underline" style="padding-top: 2%;">
											<span style="font-weight: lighter;"> Total allocation :</span> 
											<span style="font-weight: bold;" >{{count($budget->allocations)}}</span>
										</p>
										{{-- <p class="underline" style="padding-top: 2%;">
											<span style="font-weight: lighter;"> :</span> 
											<span style="font-weight: bold;" ></span>
										</p> --}}
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div id="workflow" class="nav_tab_pane tab-pane">

				</div>
			</div>
		</div>
	</div>

</div>
</div>

@stop


@push('after-script-end')

<script  type="text/javascript">

	$(function () {

		let budget_datatable = $('#budget_datatable').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			order: [[ 2, "desc" ]],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url("investment/budget/budget_datatable/".$budget->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'name' , name: 'code_values.name'},
			{ data: 'allocated_amount' , name: 'investment_budget_allocation.amount'},
			{ data: 'percent' , name :'investment_budget_allocation.percent'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
					let col = $(this).index()+1;
					let colText = $('#budget_datatable thead th:nth-child('+col+')').text();
					if(colText !='Action'){
						// document.location.href = '{!! url("investment/budget/show") !!}/'+aData['id'];
					}
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});


		if (location.hash !== '') {
			$('a[href="' + location.hash + '"]').tab('show');
			$('a[href="' + location.hash + '"]').trigger('click');
		}


		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var tab = $(e.target).attr('href').substr(1);
			if (history.pushState) {
				history.pushState(null, null, '#' + tab);
			} else {
				location.hash = '#' + tab;
			}
		});
	});

</script>;

@endpush
