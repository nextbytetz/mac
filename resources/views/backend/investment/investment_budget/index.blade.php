@extends('layouts.backend.main', ['title' => 'Investment Budgets', 'header_title' => 'Investment Budgets'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

<style>
  .custom_error {
    position: relative;
    border: 1px solid red;
  }
</style>
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div>&nbsp; </div>
    <div class="col-md-12">
      <div class="pull-right">
        <button class="btn btn-info btn-md " id="add_budget_btn" role="button">Add Budget </button>
      </div>
    </div>
    <div>&nbsp; </div>
    <table class="display" cellspacing="0" width="100%" id ="investment_datatable">
      <thead>
        <tr>
          <th>Financial Year</th>
          <th>Amount</th>
          <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
</div>


<div id="add_budget_modal" class="modal fade" role="dialog" data-focus="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <form role="form" id="add_budget_form">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row pb-3 pt-1 financial">
          <div class="col-xs-4">
            <label for="financial_year"><h6>Financial Year</h6></label>
          </div>
          <div class="col-xs-6">
            <select class="form-control" id="fin_year_id" name="fin_year_id">
              <option value="" selected="selected">Select applicable financial year</option>
              @foreach($fin_years as $fin_year)
              <option value="{{$fin_year->id}}">{{$fin_year->name}}</option>
              @endforeach
            </select>
            <select class="form-control hidden" id="fin_year" name="fin_year">
            </select>
            <span class="fin_year_id_error error_field hidden mt-1 text-danger"></span>
          </div>
        </div>
        <div class="row pb-3 pt-1">
          <div class="col-xs-4">
            <label for="budget_file"><h6>Budget File</h6></label>
          </div>
          <div class="col-xs-8">
            <input name="budget_file" id="budget_file" type="file" class="form-control-file" required>
            <span class="budget_file_error error_field hidden mt-1 text-danger"></span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="footer_button">
          <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-lg btn-success" id="add_budget_submit">Save</button>
          <button type="button" class="btn btn-lg btn-success hidden" id="edit_budget_submit">Update</button>
        </span>
        <span class="wait hidden pull-right">
          <img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
          &nbsp; <span class="h6"> Please wait .... </span>
        </span>
      </div>
    </form>
  </div>
</div>
</div>



@endsection

@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


<script type="text/javascript">

  $(function(){

   let investment_datatable = $('#investment_datatable').DataTable({
    processing: true,
    serverSide: true,
    stateSave: true,
    searching: true,
    paging: true,
    info:false,
    order: [[ 0, "desc" ]],
    stateSaveCallback: function (settings, data) {
      localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
    },
    stateLoadCallback: function (settings) {
      return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
    },
    ajax:{
      url : '{!! route('backend.investment.budget.datatable') !!}',
      type : 'get'
    },
    columns: [
    { data: 'name' , name: 'fin_years.name'},
    { data: 'amount' , name: 'investment_budgets.amount'},
    { data: 'action' , name :'action', searchable:false },
    ],
    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
     $('td', nRow).click(function() {
      let col = $(this).index()+1;
      let colText = $('#investment_datatable thead th:nth-child('+col+')').text();
      if(colText !='Action'){
        document.location.href = '{!! url("investment/budget/show") !!}/'+aData['id'];
      }
    }).hover(function() {
      $(this).css('cursor','pointer');
    }, function() {
      $(this).css('cursor','auto');
    });
  }
});


   $('#investment_datatable').on('click','.edit_budget_btn',function(e){
    e.preventDefault();
    let f_year = $(this).data('fin_year');
    let investment_budget_id = $(this).data('id');
    let fin_year_id = $(this).data('fin_year_id');
    $('#fin_year').empty();
    $('#fin_year').append('<option value="'+$(this).data('fin_year_id')+'" >'+$(this).data('fin_year')+'</option>');

    swal({
      title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
      text: '<h6>Are you sure you want to edit investment budget of  '+f_year+'</h6>',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#75E0A2',
      confirmButtonText: 'Confirm',
      html: true,
      closeOnConfirm: true
    }, function (isConfirmed) {
      if (isConfirmed) {
        $('#fin_year_id').addClass('hidden');
        $('#fin_year').removeClass('hidden');
        $('#financial_id').val(fin_year_id);
        $('#financial').val(f_year);
        $('#add_budget_submit').addClass('hidden');
        $('#edit_budget_submit').removeClass('hidden');
        $('#add_budget_modal').modal('show');
      }
    });
  });



   $('#add_budget_btn').on('click', function(e) {
    e.preventDefault();

    $('.error_field').addClass('hidden');
    $('.footer_button').removeClass('hidden');
    $('.wait').addClass('hidden');
    $('#fin_year_id').removeClass('hidden');
    $('#fin_year').addClass('hidden');
    $('#add_budget_form').trigger('reset');
    $('#add_budget_submit').removeClass('hidden');
    $('#edit_budget_submit').addClass('hidden');
    $('#add_budget_modal').modal('show');

  });



   $('#add_budget_submit').on('click', function(e){
    e.preventDefault();

    $('.footer_button').addClass('hidden');
    $('.wait').removeClass('hidden');

    $('.error_field').addClass('hidden');
    let form = $('#add_budget_form');
    let fin_year =  $('#fin_year_id option:selected').text();
    let fin_year_id =  $('#fin_year_id option:selected').val();

    if (fin_year_id) {
      let options = {
        dataType : 'json',
        url: '{{route('backend.investment.budget.set')}}',
        data : {},
        type : 'POST',
        success : function (data) {
         if (data.success) {
           $('#add_budget_modal').modal('hide');
           $.amaran({
             'theme'     :'awesome success',
             'content'   :{
              title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
              message: fin_year+' Budget successfully set',
              info:'',
              icon: 'fa fa-check-square-o'
            },
            'position'  :'bottom left',
            'outEffect' :'slideBottom',
            'inEffect'  :'slideLeft'
          });
           setTimeout(function(){location.reload(); }, 2000);
         } 
         else {
          $('.footer_button').removeClass('hidden');
          $('.wait').addClass('hidden');
          if (data.errors) {
            let m = Object.keys(data.errors);
            $.each(m, function( index, value ) {
              $('.'+value+'_error').removeClass('hidden');
              $('.'+value+'_error').text(data.errors[value][0]);
              $('#'+value).addClass('custom_error');
            });
          }else{
           $('#add_budget_modal').modal('hide');
           swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
         }
       }
     },
     error: function (data) {
      $('.footer_button').removeClass('hidden');
      $('.wait').addClass('hidden');
    }
  };
  $(form).ajaxSubmit(options);
}else{
  $('.footer_button').removeClass('hidden');
  $('.wait').addClass('hidden');
  $('.fin_year_id_error').removeClass('hidden');
  $('.fin_year_id_error').text('kindly select applicable financial year');
}
});

   $('#edit_budget_submit').on('click', function(e){
    e.preventDefault();

    $('.footer_button').addClass('hidden');
    $('.wait').removeClass('hidden');

    $('.error_field').addClass('hidden');
    let form = $('#add_budget_form');
    let fin_year =  $('#fin_year').text();
    let fin_year_id =  $('#fin_year').val();
    if (fin_year_id) {
      let data_to_save = form.serializeArray();
      data_to_save.push({name: 'fin_year_id',value: fin_year_id});
      let options = {
        dataType : 'json',
        url: '{{route('backend.investment.budget.set')}}',
        data : data_to_save,
        type : 'POST',
        success : function (data) {
         if (data.success) {
           $('#add_budget_modal').modal('hide');
           $.amaran({
             'theme'     :'awesome success',
             'content'   :{
              title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
              message: fin_year+' Budget successfully updated',
              info:'',
              icon: 'fa fa-check-square-o'
            },
            'position'  :'bottom left',
            'outEffect' :'slideBottom',
            'inEffect'  :'slideLeft'
          });
           setTimeout(function(){location.reload(); }, 2000);
         } 
         else {
          $('.footer_button').removeClass('hidden');
          $('.wait').addClass('hidden');
          if (data.errors) {
            let m = Object.keys(data.errors);
            $.each(m, function( index, value ) {
              $('.'+value+'_error').removeClass('hidden');
              $('.'+value+'_error').text(data.errors[value][0]);
              $('#'+value).addClass('custom_error');
            });
          }else{
           $('#add_budget_modal').modal('hide');
           swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
         }
       }
     },
     error: function (data) {
      $('.footer_button').removeClass('hidden');
      $('.wait').addClass('hidden');
    }
  };
  $(form).ajaxSubmit(options);
}else{
  $('.footer_button').removeClass('hidden');
  $('.wait').addClass('hidden');
  $('.fin_year_id_error').removeClass('hidden');
  $('.fin_year_id_error').text('kindly select applicable financial year');
}
});



 });

</script>
@endpush