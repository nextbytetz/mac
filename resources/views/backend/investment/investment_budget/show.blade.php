@extends('layouts.backend.main', ['title' => $budget->fin_year->name.' Investment Budget', 'header_title' => $budget->fin_year->name.' Investment Budget'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
</style>
@endpush

@section('content')

<div class = "row">

	<div>&nbsp;</div>
	{{--Tabs navigation--}}
	<div class = "row">
		<div class="col-md-12">

			<div class="basic_nav_pills nav_basic_tab">
				<ul class="nav nav-tabs">
					{{--General--}}
					<li class="nav-item">
						<a class="nav-link active" href="#general"
						data-toggle="tab">@lang('labels.general.general')
					</a>
				</li>
				@if(!is_null($budget->status))
				<li class="nav-item">
					<a class="nav-link"  id = "tab_2_header"  href="#workflow" data-toggle="tab">Workflow</a>
				</li>
				@endif
			</ul>
			<br>
			<div class="nav_tab_contain tab-content">
				<div id="general" class="nav_tab_pane tab-pane active in">
					<div class = "row">
						@if(is_null($budget->status))
						<div class="col-xs-12 pb-1">
							<form id="recommend_investment_form">
								<input type="text" name="investment_budget_id" value="{{$budget->id}}" class="hidden">
								<button type="submit" id="recommend_investment_submit" class="btn btn-success btn-submit pull-right">Recommend Budget</button>
								<span class="wait hidden pull-right">
									<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 75%; width:20%;" />  
									&nbsp; <span class="h6"> Please wait .... </span>
								</span>
							</form>
						</div>
						@endif
						<div class="col-md-12">
							<div class="col-md-9">
								{{-- datatable --}}
								<table class="display" cellspacing="0" width="100%" id ="budget_datatable">
									<thead>
										<tr>
											<th>Invetsment Type</th>
											<th>Allocated Amount</th>
											<th>Allocated Percent</th>
											{{-- <th>Action</th> --}}
										</tr>
									</thead>
								</table>
							</div>
							<div class="col-md-3">
								{{-- summary --}}
								<div class="row">
									<div class="grey_modal">
										<table style="width:100%">
											<tr>
												<td  align="center"><h5><strong><span class="light_dark_color">Investment Budget Summary</span></strong></h5></td>
											</tr>
										</table>
									</div>
								</div>

								<div class="row">
									<div class="light_grey_bg">&nbsp;</div>
									<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
										<p class="underline" style="padding-bottom: 2%;" >
											<span style="font-weight: lighter;">Period :</span> 
											<span style="font-weight: bold;" >{{$budget->fin_year->name}}</span>
										</p>
										<p class="underline" style="padding-top: 2%;">
											<span style="font-weight: lighter;">Total Budget :</span> 
											<span style="font-weight: bold;" > {{number_format($budget->amount,2)}}/=</span>
										</p>
										<p class="underline" style="padding-top: 2%;">
											<span style="font-weight: lighter;"> Total allocation :</span> 
											<span style="font-weight: bold;" >{{count($budget->allocations)}}</span>
										</p>
										{{-- <p class="underline" style="padding-top: 2%;">
											<span style="font-weight: lighter;"> :</span> 
											<span style="font-weight: bold;" ></span>
										</p> --}}
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				@if(!is_null($budget->status))
				<div id="workflow" class="nav_tab_pane tab-pane">
					@php
					$workflowinput = ['resource_id' => $budget->id, 'wf_module_group_id'=> 29, 'type' => -1];
					@endphp
					{!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
				</div>
				@endif
			</div>
		</div>
	</div>

</div>
</div>

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script  type="text/javascript">

	$(function () {

		let budget_datatable = $('#budget_datatable').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			order: [[ 2, "desc" ]],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url("investment/budget/budget_datatable/".$budget->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'name' , name: 'code_values.name'},
			{ data: 'allocated_amount' , name: 'investment_budget_allocation.amount'},
			{ data: 'percent' , name :'investment_budget_allocation.percent'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
					let col = $(this).index()+1;
					let colText = $('#budget_datatable thead th:nth-child('+col+')').text();
					if(colText !='Action'){
						if (aData['code_value_reference'] === "INTTBILLS" || aData['code_value_reference'] === "INTTBONDS" || aData['code_value_reference'] === "INTCBONDS" || aData['code_value_reference'] === "INTFDR") {
							
							document.location.href = '{!! url("investment/summary/investment_type") !!}/'+aData['fin_year_id']+'/'+aData['allocation_id']+'/'+aData['code_value_reference'];
						} else {
							let urlz = '{!! url("investment/show") !!}/'+aData['code_value'].trim().toLowerCase().replace(/ /g, "_")+'/'+aData['allocation_id'];
						    document.location.href = urlz;
						}

						
					}

					
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});

		$('#recommend_investment_submit').on('click', function(e) {
			e.preventDefault();

			$('#recommend_investment_submit').addClass('hidden');
			$('.wait').removeClass('hidden');

			swal({
				title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
				text: '<h6>Are you sure you want to recommend investment budget of {{$budget->amount}} for {{$budget->fin_year->name}}</h6>',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Cancel',
				confirmButtonColor: '#75E0A2',
				confirmButtonText: 'Confirm',
				html: true,
				closeOnConfirm: true
			}, function (isConfirmed) {
				if (isConfirmed) {
					let submit_options = $.ajax({
						url : '{!! url("investment/budget/recommend_budget/".$budget->id) !!}',
						type: 'GET',
						datatype : 'json',
						success:function(data){
							if (data.success) {
								$('.wait').addClass('hidden');
								$.amaran({
									'theme'     :'awesome success',
									'content'   :{
										title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
										message: '{{$budget->fin_year->name}} Budget successfully Recommended',
										info:'',
										icon: 'fa fa-check-square-o'
									},
									'position'  :'bottom left',
									'outEffect' :'slideBottom',
									'inEffect'  :'slideLeft'
								});
								setTimeout(function(){location.reload(); }, 2000);
							}else{
								$('#recommend_investment_submit').removeClass('hidden');
								$('.wait').addClass('hidden');
								setTimeout(function(){location.reload(); }, 2000);
							}
						}
					});
					$('#recommend_investment_form').ajaxSubmit(submit_options);
				}
			});

		});


		if (location.hash !== '') {
			$('a[href="' + location.hash + '"]').tab('show');
			$('a[href="' + location.hash + '"]').trigger('click');
		}


		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var tab = $(e.target).attr('href').substr(1);
			if (history.pushState) {
				history.pushState(null, null, '#' + tab);
			} else {
				location.hash = '#' + tab;
			}
		});
	});

</script>;

@endpush
