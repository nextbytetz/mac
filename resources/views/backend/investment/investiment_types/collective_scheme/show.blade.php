@extends('layouts.backend.main', ['title' => 'Schemes', 'header_title' => $scheme->scheme_name])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}
<style>
	.xdsoft_datetimepicker {z-index:999999999 !important;}
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
</style>
@endpush

@section('content')

<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="basic_nav_pills nav_basic_tab">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link active" href="#general"data-toggle="tab">
							@lang('labels.general.general')
						</a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link"  id = "tab_2_header"  href="#scheme_lots" data-toggle="tab">Scheme Lots</a>
					</li>

					<li class="nav-item">
						<a class="nav-link"  id = "tab_3_header"  href="#unit_sales" data-toggle="tab">Unit Sales</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" id = "tab_4_header" href="#unit_dividend" data-toggle="tab">Dividends</a>
					</li>
				</ul>
				<br>
				<div class="nav_tab_contain tab-content">
					<div id="general" class="nav_tab_pane tab-pane active in">
						<div class = "row">
							<div class="col-md-9">
								@if($scheme->total_units > 0)
								<div class="pull-right mb-1">
									<button class="btn btn-primary btn-md" id="scheme_update_btn" role="button">Update Price</button>
								</div>
								@endif
								<table class="display table table-bordered" cellspacing="0" width="100%" id ="scheme_update_table">
									<thead>
										<tr>
											<th>Number of Units</th>
											<th>Unit Value</th>
											<th>Value Date</th>
											<th>Total Units Value</th>
										</tr>
									</thead>
								</table>
							</div>
							<div class="col-md-3">
								<div class="row">
									<div class="grey_modal">
										<table style="width:100%">
											<tr>
												<td  align="center"><h5><strong><span class="light_dark_color">	Summary</span></strong></h5></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="light_grey_bg">&nbsp;</div>
									<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
										<p class="underline ">
											<span style="font-weight: lighter;"> Scheme Name :</span> 
											<span style="font-weight: bold;" > 
												{{!empty($scheme->scheme_name) ? $scheme->scheme_name : ''}}
											</span>
										</p>
										<p class="underline ">
											<span style="font-weight: lighter;"> Scheme Manager :</span> 
											<span style="font-weight: bold;" > 
												{{!empty($scheme->manager) ? $scheme->manager : ''}}
											</span>
										</p>
										<p class="underline pt-1">
											<span style="font-weight: lighter;">Units Bought:</span> 
											<span style="font-weight: bold;" > 
												{{number_format($scheme->total_units,0)}}
											</span>
										</p>
										<p class="underline pt-1">
											<span style="font-weight: lighter;">Units Sold:</span> 
											<span style="font-weight: bold;" > 
												{{number_format($scheme->units_sold,0)}}
											</span>
										</p>
										<p class="underline pt-1">
											<span style="font-weight: lighter;">Remaining Units:</span> 
											<span style="font-weight: bold;" > 
												{{number_format($scheme->remain_units,0)}}
											</span>
										</p>
										<p class="underline  pt-1">
											<span style="font-weight: lighter;">Total Value:</span> 
											<span style="font-weight: bold;" >
												{{number_format($scheme->units_value,2)}}
											</span>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="scheme_lots" class="nav_tab_pane tab-pane">
						<div class = "row">
							<div class="col-md-12">
								<div class="pull-right mb-1">
									<a class="btn btn-primary btn-md" href="{!!route('backend.investment.collective_scheme.create')!!}" role="button">Add Units</a>
								</div>
								<table class="display  table table-bordered" cellspacing="0" width="100%" id ="scheme_units_table">
									<thead>
										<tr>
											<th>Lot No#</th>
											<th>Units Bought</th>
											<th>Units Sold</th>
											<th>Remaining Units</th>
											<th>Unit Value</th>
											<th>Purchase Date</th>
											<th>Total Purchase Amount</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div id="unit_sales" class="nav_tab_pane tab-pane">
						<div class = "row">
							<div class="col-md-12">
								@if($scheme->total_units > 0)
								<div class="pull-right mb-1">
									<button class="btn btn-primary btn-md" id="scheme_sell_btn" role="button">New Sale</button>
								</div>
								@endif
								<table class="display  table table-bordered" cellspacing="0" width="100%" id ="sales_table">
									<thead>
										<tr>
											<th>Lot No#</th>
											<th>Units Sold</th>
											<th>Unit Value</th>
											<th>Selling Date</th>
											<th>Total Selling Amount</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div id="unit_dividend" class="nav_tab_pane tab-pane">
						<div class = "row">
							<div class="col-md-12">
								@if($scheme->total_units > 0)
								<div class="pull-right mb-1">
									<button class="btn btn-primary btn-md" id="scheme_dividend_btn" role="button">New Dividend</button>
								</div>
								@endif
								<table class="display table table-bordered" cellspacing="0" width="100%" id ="dividend_table">
									<thead>
										<tr>
											<th>Declaration Date</th>
											<th>Number Of Units</th>
											<th>Dividend Per Unit</th>
											<th>Dividend Income</th>
											<th>Payment Date</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
</div>
@include('backend.investment.investiment_types.collective_scheme.includes.update_scheme_modal')
@include('backend.investment.investiment_types.collective_scheme.includes.scheme_sell_modal')
@include('backend.investment.investiment_types.collective_scheme.includes.dividend_modal')


@stop

@php
if (empty($budget_allocation_id)) {
	$url = url('investment/equity/datatable/');
} else {
	$url = url('investment/equity/datatable/'.$budget_allocation_id);
}
@endphp


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}

<script  type="text/javascript">

	$(function () {

		$('.date_picker').datetimepicker({
			timepicker:false,
			format:'d-m-Y',
		});

		$('.money').maskMoney({
			precision : 4,
			// affixesStay : true,
			// formatOnBlur: true
		});

		

		$('#scheme_update_table').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [0,1,3] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/collective_scheme/updates_datatable/'.$scheme->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'number_of_unit' , name: 'number_of_unit'},
			{ data: 'unit_value' , name: 'unit_value'},
			{ data: 'value_date' , name: 'value_date'},
			{ data: 'total_unit_amount' , name: 'total_unit_amount'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});

		$('#scheme_update_btn').on('click', function(e) {
			e.preventDefault();
			
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#update_scheme_form').trigger('reset');
			$('#update_scheme_modal').modal('show');
		});


		$('#update_scheme_submit').on('click', function(e) {
			e.preventDefault();
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#update_scheme_form');
			let unit_value = $('#unit_value').val().replace(/,/g, "");
			// let share_no =  $('#update_total_shares').val().replace(/,/g, "");

			let data_to_save = form.serializeArray();
			data_to_save.push({name: 'unit_value',value: unit_value });
			data_to_save.push({name: 'scheme_id',value: {{$scheme->id}} });
			data_to_save.push({name: 'number_of_unit',value: {{$scheme->total_units}} });

			let options = {
				dataType : 'json',
				url: '{!!route('backend.investment.collective_scheme.update_price')!!}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$('.wait').addClass('hidden');
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: 'Unit Price Updated successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						$('#update_scheme_modal').modal('hide');
						form.trigger("reset");
						setTimeout(function(){location.reload(); }, 1000);
					} 
					else {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
					$('.footer_button').removeClass('hidden');
					$('.wait').addClass('hidden');
				}
			};
			$(form).ajaxSubmit(options);
		});

		$('#unit_value').on('focusout', function (e) {
			totalUnitValue();
		});



		function totalUnitValue() {
			let unit_value = $('#unit_value').val().replace(/,/g, "");
			let total_units =  $('#total_units').val().replace(/,/g, "");
			if (total_units && unit_value) {
				let total_unit_value = total_units * unit_value;
				if (!isNaN(total_unit_value)) {
					$('#total_unit_value').val(total_unit_value.toLocaleString());
				} else {
					$('#total_unit_value').val('');	
				}
			} else {
				$('#total_unit_value').val('');
			}
		}



		$('#scheme_units_table').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [1,2,3,4,6] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/collective_scheme/lots_datatable/'.$scheme->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'lot_number' , name: 'lot_number'},
			{ data: 'purchased_unit' , name: 'purchased_unit'},
			{ data: 'sold_units' , name: 'sold_units'},
			{ data: 'remaining_unit' , name: 'remaining_unit'},
			{ data: 'purchase_nav' , name: 'purchase_nav'},
			{ data: 'purchase_date' , name: 'purchase_date'},
			{ data: 'purchase_amount' , name: 'purchase_amount'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});

		$('#sales_table').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [1,2,4] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/collective_scheme/sales_datatable/'.$scheme->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'lot_number' , name: 'lot_number'},
			{ data: 'number_of_unit' , name: 'number_of_unit'},
			{ data: 'selling_price' , name: 'selling_price'},
			{ data: 'selling_date' , name: 'selling_date'},
			{ data: 'total_unit_amount' , name: 'total_unit_amount'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});


		$('#scheme_sell_btn').on('click', function(e) {
			e.preventDefault();
			
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#scheme_sell_form').trigger('reset');
			$('#scheme_sell_modal').modal('show');
		});


		$('#sell_lot').on('change', function (e) {
			let remain_units = $('#sell_lot option:selected').data('total_units');
			if (remain_units) {
				remain_units =  remain_units;
			} else {
				remain_units = 0;
			}
			$('#total_remaining_unit').val(remain_units.toLocaleString());
			$('#sell_units').attr("max",remain_units);
		});



		$('#scheme_sell_submit').on('click', function(e) {
			e.preventDefault();
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');

			let max_units = $('#sell_lot option:selected').data('total_units');
			if (max_units) {
				max_units = max_units;
			} else {
				max_units = 0;
			}
			let units_to_sell = $('#sell_units').val();

			if (max_units > units_to_sell) {
				let form = $('#scheme_sell_form');

				let sell_price = $('#sell_price').val().replace(/,/g, "");

				let data_to_save = form.serializeArray();
				data_to_save.push({name: 'sell_price',value: sell_price});

				let options = {
					dataType : 'json',
					url: '{!!route('backend.investment.collective_scheme.sell')!!}',
					data : data_to_save,
					type : 'POST',
					success : function (data) {
						if (data.success) {
							$('.wait').addClass('hidden');
							$.amaran({
								'theme'     :'awesome success',
								'content'   :{
									title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
									message: 'Units Sold successfully',
									info:'',
									icon: 'fa fa-check-square-o'
								},
								'position'  :'bottom left',
								'outEffect' :'slideBottom',
								'inEffect'  :'slideLeft'
							});
							$('#scheme_sell_modal').modal('hide');
							form.trigger("reset");
							setTimeout(function(){location.reload(); }, 1000);
						} 
						else {
							$('.footer_button').removeClass('hidden');
							$('.wait').addClass('hidden');
							if (data.errors) {
								let m = Object.keys(data.errors);
								$.each(m, function( index, value ) {
									$('.'+value+'_error').removeClass('hidden');
									$('.'+value+'_error').text(data.errors[value][0]);
									$('#'+value).addClass('custom_error');
								});
							}else{
								swal({
									title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
									text: '<h6>Something went wrong! Kindly try again</h6>',
									type: 'error',
									showCancelButton: false,
									cancelButtonText: 'Cancel',
									confirmButtonColor: '#75E0A2',
									confirmButtonText: 'OK',
									html: true,
									closeOnConfirm: true
								}, function (isConfirmed) {
									location.reload();
								});
							}
						}
					},
					error: function (data) {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
					}
				};
				$(form).ajaxSubmit(options);
			} else {
				
				$('.footer_button').removeClass('hidden');
				$('.wait').addClass('hidden');
				$('.sell_units_error').removeClass('hidden');
				$('.sell_lot_error').removeClass('hidden');
				
				if (max_units >0) {
					$('.sell_units_error').text('Unit should not exceed '+max_units.toLocaleString());
				} else {
					$('.sell_units_error').text('Kindly select lot first');
					$('.sell_lot_error').text('This lot has no units to sell');
				}
				$('#sell_units').addClass('custom_error');
				$('#sell_lot').addClass('custom_error');
			}
		});

		$('#sell_units').on('focusout', function (e) {
			totalSellAmount();
		});

		$('#sell_price').on('focusout', function (e) {
			totalSellAmount();
		});



		function totalSellAmount() {
			let sell_price = $('#sell_price').val().replace(/,/g, "");
			let sell_units =  $('#sell_units').val().replace(/,/g, "");
			if (sell_units && sell_price) {
				let total_sell_amount = sell_price * sell_units;
				if (!isNaN(total_sell_amount)) {
					$('#total_sell_amount').val(total_sell_amount.toLocaleString());
				} else {
					$('#total_sell_amount').val('');	
				}
			} else {
				$('#total_sell_amount').val('');
			}
		}


		$('#dividend_table').DataTable({	
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [1,2,3] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/collective_scheme/dividend_datatable/'.$scheme->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'dividend_declaration_date' , name: 'dividend_declaration_date'},
			{ data: 'total_unit' , name: 'total_unit'},
			{ data: 'dividend_nav' , name: 'dividend_nav'},
			{ data: 'dividend_income' , name: 'dividend_income'},
			{ data: 'dividend_payment_date' , name: 'dividend_payment_date'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});


		$('#scheme_dividend_btn').on('click', function(e) {
			e.preventDefault();
			dividendValue();
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#scheme_dividend_form').trigger('reset');
			$('#scheme_dividend_modal').modal('show');
		});


		$('#scheme_dividend_submit').on('click', function(e) {
			e.preventDefault();
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#scheme_dividend_form');
			let dividend_per_unit = $('#dividend_per_unit').val().replace(/,/g, "");
			// let share_no =  $('#update_total_shares').val().replace(/,/g, "");

			let data_to_save = form.serializeArray();
			data_to_save.push({name: 'dividend_per_unit',value: dividend_per_unit });
			data_to_save.push({name: 'scheme_id',value: {{$scheme->id}} });

			let options = {
				dataType : 'json',
				url: '{!!route('backend.investment.collective_scheme.add_dividend')!!}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$('.wait').addClass('hidden');
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: 'Dividend Added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						$('#scheme_dividend_modal').modal('hide');
						form.trigger("reset");
						setTimeout(function(){location.reload(); }, 1000);
					} 
					else {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
					$('.footer_button').removeClass('hidden');
					$('.wait').addClass('hidden');
				}
			};
			$(form).ajaxSubmit(options);
		});


		$('#dividend_per_unit').on('focusout', function (e) {
			dividendValue();
		});

		function dividendValue() {
			let dividend_per_unit = $('#dividend_per_unit').val().replace(/,/g, "");
			let dividend_units =  $('#dividend_units').val().replace(/,/g, "");
			if (dividend_units && dividend_per_unit) {
				let dividend_value = dividend_units * dividend_per_unit;
				if (!isNaN(dividend_value)) {
					$('#dividend_income').val(dividend_value.toLocaleString());
					$('#dividend_receivable').val(dividend_value.toLocaleString());

				} else {
					$('#dividend_income').val('');	
					$('#dividend_receivable').val('');	
				}
			} else {
				$('#dividend_income').val('');
				$('#dividend_receivable').val('');
			}
		}

		if (location.hash !== '') {
			$('a[href="' + location.hash + '"]').tab('show');
			$('a[href="' + location.hash + '"]').trigger('click');
		}

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var tab = $(e.target).attr('href').substr(1);
			if (history.pushState) {
				history.pushState(null, null, '#' + tab);
			} else {
				location.hash = '#' + tab;
			}
		});


	});

</script>;

@endpush
