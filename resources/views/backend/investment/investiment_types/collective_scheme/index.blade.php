@extends('layouts.backend.main', ['title' => 'Collective Schemes', 'header_title' =>' Collective Schemes'])
@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
</style>
@endpush

@section('content')

<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="col-md-9">
				<div class="pull-right mb-1">
					<button class="btn btn-primary btn-md"  role="button" id="add_new_scheme">Add New Scheme</button>
				</div>
				<table class="display table table-hover" cellspacing="0" width="100%" id ="schemes_table">
					<thead>
						<tr>
							<th>Scheme</th>
							<th>Manager</th>
							<th>Scheme Reference</th>
							<th>Total Units</th>
							<th>Total Value</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="grey_modal">
						<table style="width:100%">
							<tr>
								<td  align="center"><h5><strong><span class="light_dark_color">	Summary</span></strong></h5></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="light_grey_bg">&nbsp;</div>
					<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
						<p class="underline pt-1">
							<span style="font-weight: lighter;">Total Managers :</span> 
							<span style="font-weight: bold;" > 
								{{$summary['manager_count']}}
							</span>
						</p>
						<p class="underline pt-1">
							<span style="font-weight: lighter;"> Total Schemes :</span> 
							<span style="font-weight: bold;" >
								{{$summary['scheme_count']}}
							</span>
						</p>
						<p class="underline pt-1">
							<span style="font-weight: lighter;"> Total Units :</span> 
							<span style="font-weight: bold;" >
								{{number_format($summary['units_count'],0)}}
							</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@include('backend.investment.investiment_types.collective_scheme.includes.add_scheme')

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">

	$(function () { 

		$('#schemes_table').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [3,4] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/collective_scheme/datatable/') !!}',
				type : 'get'
			},
			columns: [
			{ data: 'scheme_name' , name: 'scheme_name'},
			{ data: 'manager' , name: 'employers.name'},
			{ data: 'scheme_reference' , name: 'scheme_reference'},
			{ data: 'total_units' , name: 'total_units'},
			{ data: 'units_value' , name: 'units_value'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
					document.location.href = '{!! url('investment/collective_scheme/show/') !!}/'+aData['id'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});
		
		$('#add_new_scheme').on('click', function(e) {
			e.preventDefault();
			
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#add_scheme_form').trigger('reset');
			$('#add_scheme_modal').modal('show');
		});


		$('#add_scheme_submit').on('click', function(e) {
			e.preventDefault();
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#add_scheme_form');

			let options = {
				dataType : 'json',
				url: '{{route('backend.investment.collective_scheme.add_scheme')}}',
				data : {},
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$('.wait').addClass('hidden');
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: 'Scheme added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						$('#add_scheme_modal').modal('hide');
						form.trigger("reset");
						setTimeout(function(){location.reload(); }, 1000);
					} 
					else {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
					$('.footer_button').removeClass('hidden');
					$('.wait').addClass('hidden');
				}
			};
			$(form).ajaxSubmit(options);
		});




		$(".select_employer").select2({
			dropdownParent: $("#add_scheme_modal"),
			minimumInputLength: 3,
			width: '100%',
			multiple: false,
			allowClear: true,
			debug: true,
			placeholder: "",
			ajax: {
				url: '{!! route('backend.investment.employers') !!}',
				dataType: 'json',
				delay: 250,
				processResults: function (data) {
					data = data.map(function (item) {
						return {
							text: item.name,
							id: item.id,
							name: item.name,
						};
					});
					return {
						results:  data
					};
				},
				cache: true
			}
		});
	});

</script>;

@endpush
