<div class="modal fade" id="add_scheme_modal" tabindex="-1" role="dialog">
	<form role="form" id="add_scheme_form">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pb-1">Add Scheme</h5>
					<span class="help-block pt-3"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body">
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required pb-1">Scheme Manager</label>
							<div class="form-group">
								<div class="input-group">
									<select name="manager" class="form-control select_employer" id="manager"></select>
								</div>
								<span class="manager_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1">Scheme Name</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="scheme_name" class='form-control' id="scheme_name">
								</div>
								<span class="scheme_name_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Scheme Reference</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="scheme_reference" class='form-control' id="scheme_reference">
								</div>
								<span class="scheme_reference_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<span class="footer_button">
						<button type="button" class="btn btn-primary" id="add_scheme_submit">Save changes</button>
						<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
					</span>
					<span class="wait hidden pull-right">
						<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
						&nbsp; <span class="h6"> Please wait .... </span>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>
