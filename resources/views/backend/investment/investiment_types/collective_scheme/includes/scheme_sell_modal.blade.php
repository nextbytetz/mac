<div class="modal fade" id="scheme_sell_modal" tabindex="-1" role="dialog">
	<form role="form" id="scheme_sell_form">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pb-2">{{$scheme->scheme_name}}: Sell Units</h5>
					<span class="help-block pt-2"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body"> 

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required pb-1">Unit Lot</label>
							<div class="form-group">
								<div class="input-group">
									<select name="sell_lot" class="form-control select_employer" id="sell_lot">
										<option value="" selected="selected">Select Lot</option>
										@if(count($scheme->lots))
										@foreach($scheme->lots as $lot)
										@if($lot->remaining_unit>0)
										<option value="{{$lot->id}}" data-total_units='{{$lot->remaining_unit}}'>
											{{$lot->lot_number}}
										</option>
										@endif
										@endforeach
										@endif
									</select>
								</div>
								<span class="sell_lot_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="mb-1">Total Remaining Units</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="total_remaining_unit" class='form-control' id="total_remaining_unit" disabled="disabled">
								</div>
								<span class="total_remaining_unit_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Number Of Units</label>
							<div class="form-group">
								<div class="input-group">
									<input type="number" min='1' name="sell_units" class='form-control' id="sell_units">
								</div>
								<span class="sell_units_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1">Price Per Unit</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="sell_price" class='form-control money' id="sell_price">
								</div>
								<span class="sell_price_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="mb-1">Total Unit Amount</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="total_sell_amount" class='form-control' id="total_sell_amount" disabled="disabled">
								</div>
								<span class="total_sell_amount_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1"> Selling Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="selling_date" class='form-control date_picker' id="selling_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="selling_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<span class="footer_button">
						<button type="button" class="btn btn-primary" id="scheme_sell_submit">Save</button>
						<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
					</span>
					<span class="wait hidden pull-right">
						<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
						&nbsp; <span class="h6"> Please wait .... </span>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>
