<div id="update_scheme_modal" class="modal fade" role="dialog" data-focus="false">
	<form role="form" id="update_scheme_form">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pb-2">Update Value</h5>
					<span class="help-block"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body">
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Total Units</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" value="{{number_format($scheme->total_units,0)}}" name="total_units" class='form-control' id="total_units" disabled="disabled">
								</div>
								<span class="total_units_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1">Unit Value</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="unit_value" class='form-control money' id="unit_value">
								</div>
								<span class="unit_value_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Total Units Value</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="total_unit_value" class='form-control money' id="total_unit_value" disabled="disabled">
								</div>
								<span class="total_unit_value_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1"> Value Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="value_date" class='form-control date_picker' id="value_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="value_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>


				</div>
				<div class="modal-footer">
					<span class="footer_button">
						<button type="button" class="btn btn-primary" id="update_scheme_submit">Save changes</button>
						<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
					</span>
					<span class="wait hidden pull-right">
						<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
						&nbsp; <span class="h6"> Please wait .... </span>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>
