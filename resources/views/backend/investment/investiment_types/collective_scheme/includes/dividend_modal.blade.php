<div class="modal fade" id="scheme_dividend_modal" tabindex="-1" role="dialog">
	<form role="form" id="scheme_dividend_form">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pb-2">Add Dividend </h5>
					<span class="help-block pt-2"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body">
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="mb-1">Number Of Units</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" value="{{number_format($scheme->remain_units,0)}}" name="dividend_units" class='form-control' id="dividend_units" disabled="disabled">
								</div>
								<span class="dividend_units_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1">Dividend Per Unit</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="dividend_per_unit" class='form-control money' id="dividend_per_unit">
								</div>
								<span class="dividend_per_unit_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Dividend Income</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="dividend_income" class='form-control' id="dividend_income" disabled="disabled">
								</div>
								<span class="dividend_income_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<label class="required mb-1">Dividend Receivable</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="dividend_receivable" class='form-control' id="dividend_receivable" disabled="disabled">
								</div>
								<span class="dividend_receivable_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>	
					</div>
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1"> Declaration Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="declaration_date" class='form-control date_picker' id="declaration_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="declaration_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>	
						<div class="col-sm-6">
							<label class="required mb-1"> Payment Date</label>
							<div class="form-group">
								<div class="input-group">
									<div class="input-group">
										<input type="text" name="payment_date" class='form-control date_picker' id="payment_date">
										<span class="input-group-addon">
											<i class="icon fa fa-calendar" aria-hidden="true"></i>
										</span>
									</div>
								</div>
								<span class="payment_date_error error_field hidden mt-1 text-danger" ></span>
							</div>
						</div>
					</div>

				{{-- 	<div class="row pb-2">
						<div class="col-sm-6">
							<div class="form-group row">
								<div class="col-sm-6 pl-3 ">
									<div class="form-check">
										<input class="form-check-input p-4" type="checkbox" id="is_paid">
										<label class="form-check-label" for="is_paid">
											Is Paid
										</label>
									</div>
								</div>
							</div>
							<span class="is_paid_error error_field hidden mt-1 text-danger" ></span>
						</div>
					</div> --}}
					<div class="modal-footer">
						<span class="footer_button">
							<button type="button" class="btn btn-primary" id="scheme_dividend_submit">Save changes</button>
							<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
						</span>
						<span class="wait hidden pull-right">
							<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
							&nbsp; <span class="h6"> Please wait .... </span>
						</span>
					</div>
				</div>
			</div>
		</form>
	</div>
