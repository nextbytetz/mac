@extends('layouts.backend.main', ['title' => 'Collective Investment', 'header_title' =>'Collective Investment'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}

<style>
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
</style>
@endpush
@section('content')

<div class = "row">
	<div class="col-md-12">
		{!! Form::open(['route' => 'backend.investment.collective_scheme.save', 'name' => 'scheme_units_form', 'id'=>'scheme_units_form']) !!}
		<div class="row mb-2">
			<h5 class="pl-2 pb-1">Add New Units</h5>
			<span class="help-block pl-2"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span>
			<div class="col-md-12">
				<legend></legend>
			</div>
		</div>

		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1">Manager</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="manager" name="manager">
							<option value="" selected="selected">Select manager</option>
							@foreach($managers as $manager)
							<option value="{{$manager->id}}">
								{{$manager->name}}
							</option>
							@endforeach
						</select>
					</div>
					<span class="manager_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>

			<div class="col-sm-4 ">
				<label class="required pb-1">Scheme</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="scheme" name="scheme">

						</select>
					</div>
					<span class="scheme_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>


			<div class="col-sm-4 ">
				<label class="required pb-1">Market Category</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="category" name="category">
							<option value="" selected="selected">Select category</option>
							<option value="1">IPO</option>
							<option value="2">Secondary</option>
						</select>
					</div>
					<span class="category_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1">Lot Number</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('lot_number', null, ['class' => 'form-control lot_number','id' => 'lot_number', 'disabled'=>'disabled']) !!}
					</div>
					<span class="lot_number_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1" >Unit Value</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('unit_value', null, ['class' => 'form-control money','id' => 'unit_value']) !!}
					</div>
					<span class="unit_value_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>

			<div class="col-sm-4  listed">
				<label class="pb-1" >Total Units</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('total_units', null, ['class' => 'form-control number_fields','id' => 'total_units']) !!}
					</div>
					<span class="total_units_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			
		</div>
		<div class="row pb-2">
			
			<div class="col-sm-4  listed">
				<label class="pb-1" >Total Purchase Amount</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('total_value', null, ['class' => 'form-control money_two','id' => 'total_value', 'disabled'=>'disabled']) !!}
					</div>
					<span class="total_value_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>

			<div class="col-sm-4 ">
				<label class="required pb-1" >Purchase Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('purchase_date', null, ['class' => 'form-control date_picker','id' => 'purchase_date']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="purchase_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>

			<div class="col-sm-4 ">
				<label class="required pb-1" >Disbursment Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('disbursment_date', null, ['class' => 'form-control date_picker','id' => 'disbursment_date']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="disbursment_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>

			
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a type="button" href="{!! route('backend.investment.menu') !!}" class="btn btn-lg btn-secondary" id="" data-dismiss="modal">Cancel</a>
					<input type="submit" class="btn btn-success btn-lg btn-submit" id="scheme_units_submit" value="@lang('buttons.general.save')" />
				</div>
			</div>
		</div>

		{!! Form::close() !!}
	</div>
</div>

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">

	$(function () {

		getSchemes();
		getLotNumber();

		$('.money').maskMoney({
			precision : 4,
			affixesStay : true
		});

		$('.money_two').maskMoney({
			precision : 2,
			affixesStay : true
		});

		$('.number_fields').maskMoney({
			precision : 0,
			affixesStay : true
		});

		$('.date_picker').datetimepicker({
			timepicker:false,
			format:'d-m-Y',
		});
		
		$('#manager').on('change', function (e) {
			getSchemes();
		});

		$('#scheme').on('change', function (e) {
			getLotNumber();

			// $(this).val();
		});


		$('#unit_value').on('focusout', function (e) {
			totalUnits();
		});

		$('#total_units').on('focusout', function (e) {
			totalUnits();
		});



		function totalUnits() {
			let unit_value = $('#unit_value').val().replace(/,/g, "");
			let total_units =  $('#total_units').val().replace(/,/g, "");
			if (total_value && unit_value) {
				let total_value = total_units * unit_value;
				if (!isNaN(total_value)) {
					$('#total_value').val(total_value.toLocaleString());
				} else {
					$('#total_value').val('');	
				}
			} else {
				$('#total_value').val('');
			}
		}


		

		
		$('#scheme_units_submit').on('click', function(e) {
			e.preventDefault();
			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#scheme_units_form');
			
			let total_units =  $('#total_units').val().replace(/,/g, "");
			let unit_value = $('#unit_value').val().replace(/,/g, "");
			let scheme_id = $('#scheme option:selected').val();

			let data_to_save = form.serializeArray();
			
			data_to_save.push({name: 'total_units',value: total_units});
			data_to_save.push({name: 'unit_value',value: unit_value});

			let options = {
				dataType : 'json',
				url: '{{route('backend.investment.collective_scheme.save')}}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: ' Units Added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						form.trigger("reset");
						setTimeout(function(){
							window.location.href = "{!! url('investment/collective_scheme/show/') !!}/"+scheme_id+'#scheme_lots'; 
						}, 1500);

					} 
					else {
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
				}
			};
			$(form).ajaxSubmit(options);
		});

		$(".select_employer").select2({
			minimumInputLength: 3,
			multiple: false,
			width: '100%',
			allowClear: true,
			debug: true,
			placeholder: "",
			ajax: {
				url: '{!! route('backend.investment.employers') !!}',
				dataType: 'json',
				delay: 250,
				processResults: function (data) {
					data = data.map(function (item) {
						return {
							text: item.name,
							id: item.id,
							name: item.name,
						};
					});
					return {
						results:  data
					};
				},
				cache: true
			}
		});

		function getSchemes(){
			$('#scheme').empty();
			let manager_id = $('#manager option:selected').val();
			if (manager_id) {
				$.ajax({
					url : "{!! url('investment/collective_scheme/return_schemes/') !!}/"+manager_id,
					type: 'GET',
					datatype : 'json',
					success: function(data) {
						if (data.schemes) {
							$('#scheme').append('<option value="" selected>Select scheme</option>');
							$.each( data.schemes, function(index, value){ 
								$('#scheme').append('<option value="'+value.id+'">'+value.scheme_name+'</option>');
							});
						} else {
							$('#scheme').append('<option value="">Select manager first</option>');
						}

					},
					error: function (xhr, ajaxOptions, thrownError) {
						
					}});
			} else {
				$('#scheme').append('<option value="">Select manager first</option>');
			}

		}

		function getLotNumber(){
			$('#lot_number').val('');
			let scheme_id = $('#scheme option:selected').val();
			if (scheme_id) {
				$.ajax({
					url : "{!! url('investment/collective_scheme/return_lot_number/') !!}/"+scheme_id,
					type: 'GET',
					datatype : 'json',
					success: function(data) {
						if (data.lot) {
							$('#lot_number').val(data.lot);
						} else {
							$('#lot_number').val('');
						}

					},
					error: function (xhr, ajaxOptions, thrownError) {
						
					}});
			} else {
				$('#lot_number').val('');
			}

		}

	});

</script>;

@endpush
