@extends('layouts.backend.main', ['title' => 'New Equity', 'header_title' =>''])
@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}

<style>
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
</style>
@endpush
@section('content')

{!! Form::open(['route' => 'backend.investment.equity.save', 'name' => 'investment_equity_form', 'id'=>'equity_form']) !!}
<div class="row mb-2">
	<h5 class="pl-2 pb-1">Add New Equity</h5>
	<span class="help-block pl-2"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
	<div class="col-md-12">
		<legend></legend>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1">Category</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control" id="category" name="category">
							<option value="" id="m" selected="selected">Select applicable category</option>
							@foreach($code_value as $category)
							<option id="{{$category->name == 'Unlisted Equities' ? 'unlisted' : 'listed'}}" value="{{$category->id}}">
								{{$category->name}}
							</option>
							@endforeach
						</select>
					</div>
					<span class="category_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1" >Issuer</label>
				<div class="form-group">
					<div class="input-group">
						<select class="form-control issuer" id="issuer" name="issuer">
							<option value="" id="m" selected="selected">Select issuer</option>
							@foreach($issuers as $issuer)
							<option id="{{$issuer->id}}" value="{{$issuer->id}}">
								{{$issuer->issuer_name}}
							</option>
							@endforeach
						</select>
					</div>
					<span class="issuer_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>

			<div class="col-sm-4 ">
				<label class="pb-1">Lot Number</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('lot_number', null, ['class' => 'form-control lot_number','id' => 'lot_number', 'disabled'=>'disabled']) !!}
					</div>
					<span class="lot_number_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1">Total issued shares</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('total_issued_shares', null, ['class' => 'form-control number_fields','id' => 'total_issued_shares']) !!}
					</div>
					<span class="total_issued_shares_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1" >Number Of Purchased Shares</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('number_of_shares', null, ['class' => 'form-control number_fields','id' => 'number_of_shares']) !!}
					</div>
					<span class="number_of_shares_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>

			<div class="col-sm-4 ">
				<label class="required pb-1" >Share Price</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('price_per_share', null, ['class' => 'form-control money','id' => 'price_per_share']) !!}
					</div>
					<span class="price_per_share_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1" >Purchase Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('purchase_date', null, ['class' => 'form-control date_picker','id' => 'purchase_date']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="purchase_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4">
				<label class="pb-1">Total Share Value (share price * number of purchased shares)</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('total_share_value', null, ['class' => 'form-control','id' => 'total_share_value', 'disabled'=>'disabled']) !!}
					</div>
					<span class="total_share_value_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4  listed">
				<label class="required pb-1">Subscription Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('subscription_date', null, ['class' => 'form-control date_picker','id' => 'subscription_date']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="subscription_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row pb-2 listed">
			<div class="col-sm-4 ">
				<label class="required pb-1">Broker</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::select('broker', [], null, ['class' => 'form-control select_employer w-100', 'id' => 'broker']) !!}
					</div>
					<span class="broker_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1" >Commission</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('commission', null, ['class' => 'form-control money','id' => 'commission']) !!}
					</div>
					<span class="commission_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1" >Allotment Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('allotment_date', null, ['class' => 'form-control date_picker','id' => 'allotment_date']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="allotment_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a type="button" href="{!! route('backend.investment.menu') !!}" class="btn btn-lg btn-secondary" id="" data-dismiss="modal">Cancel</a>
					<input type="submit" class="btn btn-success btn-lg btn-submit" id="equity_submit_button" value="@lang('buttons.general.save')" />
				</div>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">

	$(function () {

		$('.money').maskMoney({
			precision : 4,
			affixesStay : true
		});

		$('.number_fields').maskMoney({
			precision : 0,
			affixesStay : true
		});

		$('.date_picker').datetimepicker({
			timepicker:false,
			format:'d-m-Y',
		});
		
		categoryFields();
		totalShareValues();
		lotNumber();

		$('#category').on('change', function(e) {
			categoryFields();
		});

		function categoryFields() {
			let cat = $('#category option:selected').attr('id');
			if (cat == 'listed') {
				$('.listed').removeClass('hidden');
			} else {
				$('.listed').addClass('hidden');
			}
		}

		$('#price_per_share').on('focusout', function (e) {
			totalShareValues();
		});

		$('#number_of_shares').on('focusout', function (e) {
			totalShareValues();
		});



		function totalShareValues() {
			let share_no =  $('#number_of_shares').val().replace(/,/g, "");
			let share_price = $('#price_per_share').val().replace(/,/g, "");

			if (share_price && share_no) {
				let total_share_value = share_price * share_no;
				if (!isNaN(total_share_value)) {
					$('#total_share_value').val(total_share_value.toLocaleString());
				} else {
					$('#total_share_value').val('');	
				}
			} else {
				$('#total_share_value').val('');
			}
		}

		$('#equity_submit_button').on('click', function(e) {
			e.preventDefault();
			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#equity_form');
			let issuer_id = $('#issuer option:selected').val();
			let share_no =  $('#number_of_shares').val().replace(/,/g, "");
			let total_issued_shares = $('#total_issued_shares').val().replace(/,/g, "");
			let commission = $('#commission').val().replace(/,/g, "");
			let data_to_save = form.serializeArray();
			
			if (commission) {
				data_to_save.push({name: 'commission',value: commission});
			}
			data_to_save.push({name: 'total_issued_shares',value: total_issued_shares});
			data_to_save.push({name: 'number_of_shares',value: share_no});
			let options = {
				dataType : 'json',
				url: '{{route('backend.investment.equity.save')}}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: 'Shares Added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						form.trigger("reset");
						setTimeout(function(){
							window.location.href = '{!! url('investment/equity/show/') !!}/'+issuer_id+'#lots'; 
						}, 1500);

					} 
					else {
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
				}
			};
			$(form).ajaxSubmit(options);
		});

		$('#issuer').on('change', function (e) {
			lotNumber();
		});




		$(".issuer").select2({
			// minimumInputLength: 3,
			multiple: false,
			width: '100%',
			allowClear: true,
			debug: true,
			placeholder: ""
		});

		$(".select_employer").select2({
			minimumInputLength: 3,
			multiple: false,
			width: '100%',
			allowClear: true,
			debug: true,
			placeholder: "",
			ajax: {
				url: '{!! route('backend.investment.employers') !!}',
				dataType: 'json',
				delay: 250,
				processResults: function (data) {
					data = data.map(function (item) {
						return {
							text: item.name,
							id: item.id,
							name: item.name,
						};
					});
					return {
						results:  data
					};
				},
				cache: true
			}
		});


		function lotNumber() {
			$('#lot_number').val('');
			let issuer_id = $('#issuer option:selected').val();
			if (issuer_id) {
				$.ajax({
					url : "{!! url('investment/equity/return_lot_number/') !!}/"+issuer_id,
					type: 'GET',
					datatype : 'json',
					success: function(data) {
						if (data.lot) {
							$('#lot_number').val(data.lot);
						} else {
							$('#lot_number').val('');
						}

					},
					error: function (xhr, ajaxOptions, thrownError) {

					}});
			} else {
				$('#lot_number').val('');
			}
		}

	});

</script>;

@endpush
