@extends('layouts.backend.main', ['title' => 'Equity', 'header_title' => $issuer->issuer_name])
@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}
<style>
	.xdsoft_datetimepicker {z-index:999999999 !important;}
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
</style>
@endpush

@section('content')

<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="basic_nav_pills nav_basic_tab">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link active" href="#general"data-toggle="tab">
							@lang('labels.general.general')
						</a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link"  id = "tab_2_header"  href="#lots" data-toggle="tab">Lots</a>
					</li>

					<li class="nav-item">
						<a class="nav-link"  id = "tab_3_header"  href="#share_sales" data-toggle="tab">Share Sales</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" id = "tab_4_header" href="#dividends" data-toggle="tab">Dividends</a>
					</li>
				</ul>
				<br>
				<div class="nav_tab_contain tab-content">
					<div id="general" class="nav_tab_pane tab-pane active in">
						<div class = "row">
							<div class="col-md-9">
								@if($issuer->total_shares > 0)
								<div class="pull-right mb-1">
									<button class="btn btn-primary btn-md" id="update_price_btn" role="button">Update Price</button>
								</div>
								@endif
								<table class="display table table-bordered" cellspacing="0" width="100%" id ="price_table">
									<thead>
										<tr>
											<th>Number of Units</th>
											<th>Unit Value</th>
											<th>Value Date</th>
											<th>Total Units Value</th>
										</tr>
									</thead>
								</table>
							</div>
							<div class="col-md-3">
								<div class="row">
									<div class="grey_modal">
										<table style="width:100%">
											<tr>
												<td  align="center"><h5><strong><span class="light_dark_color">	Summary</span></strong></h5></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="light_grey_bg">&nbsp;</div>
									<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
										<p class="underline ">
											<span style="font-weight: lighter;"> Issuer :</span> 
											<span style="font-weight: bold;" > 
												{{!empty($issuer->issuer_name) ? $issuer->issuer_name : ''}}
											</span>
										</p>
										{{-- <p class="underline pt-1">
											<span style="font-weight: lighter;">Units Bought:</span> 
											<span style="font-weight: bold;" > 
												{{number_format($issuer->total_shares,0)}}
											</span>
										</p>
										<p class="underline pt-1">
											<span style="font-weight: lighter;">Units Sold:</span> 
											<span style="font-weight: bold;" > 
												{{number_format($issuer->shares_sold,0)}}
											</span>
										</p> --}}
										<p class="underline pt-1">
											<span style="font-weight: lighter;">Total shares:</span> 
											<span style="font-weight: bold;" > 
												{{number_format($issuer->total_shares,0)}}
											</span>
										</p>
										<p class="underline  pt-1">
											<span style="font-weight: lighter;">Total Value:</span> 
											<span style="font-weight: bold;" >
												{{number_format($issuer->shares_value,2)}}
											</span>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="lots" class="nav_tab_pane tab-pane">
						<div class = "row">
							<div class="col-md-12">
								<div class="pull-right mb-1">
									<a class="btn btn-primary btn-md" href="{!!route('backend.investment.equity.create')!!}" role="button">Add Shares</a>
								</div>
								<table class="display  table table-bordered" cellspacing="0" width="100%" id ="shares_table">
									<thead>
										<tr>
											<th>Lot No#</th>
											<th>Category</th>
											<th>Number of Shares</th>
											<th>Remaining Shares</th>
											<th>Price Per Share</th>
											<th>Purchase Date</th>
											<th>Total Price</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div id="share_sales" class="nav_tab_pane tab-pane">
						<div class = "row">
							<div class="col-md-12">
								@if($issuer->total_shares > 0)
								<div class="pull-right mb-1">
									<button class="btn btn-primary btn-md" id="equity_sell_btn" role="button">New Sale</button>
								</div>
								@endif
								<table class="display  table table-bordered" cellspacing="0" width="100%" id ="sales_table">
									<thead>
										<tr>
											<th>Lot No#</th>
											<th>Units Sold</th>
											<th>Unit Value</th>
											<th>Selling Date</th>
											<th>Total Selling Amount</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div id="dividends" class="nav_tab_pane tab-pane">
						<div class = "row">
							<div class="col-md-12">
								@if($issuer->total_shares > 0)
								<div class="pull-right mb-1">
									<button class="btn btn-primary btn-md" id="dividend_btn" role="button">Add Dividend</button>
								</div>
								@endif
								<table class="display table table-bordered" cellspacing="0" width="100%" id ="dividend_table">
									<thead>
										<tr>
											<th>Declaration Date</th>
											<th>Number Of Shares</th>
											<th>Dividend Nav</th>
											<th>Dividend Income</th>
											<th>Payment Date</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
</div>

@include('backend.investment.investiment_types.equities.includes.update_price_modal')
@include('backend.investment.investiment_types.equities.includes.sell_modal')
@include('backend.investment.investiment_types.equities.includes.dividend_modal')


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">

	$(function () {

		$('.date_picker').datetimepicker({
			timepicker:false,
			format:'d-m-Y',
		});

		$('.money').maskMoney({
			precision : 4,
			// affixesStay : true,
			// formatOnBlur: true
		});

		

		$('#price_table').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [0,1,3] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/equity/updates_datatable/'.$issuer->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'number_of_share' , name: 'number_of_share'},
			{ data: 'share_value' , name: 'share_value'},
			{ data: 'value_date' , name: 'value_date'},
			{ data: 'total_share_value' , name: 'total_share_value'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});

		$('#update_price_btn').on('click', function(e) {
			e.preventDefault();
			
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#equity_update_form').trigger('reset');
			$('#equity_update_modal').modal('show');
		});

		$('#shares_table').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [1,2,3,5] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/equity/lots_datatable/'.$issuer->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'lot_number' , name: 'lot_number'},
			{ data: 'category_name' , name: 'category_name'},
			{ data: 'number_of_share' , name: 'number_of_share'},
			{ data: 'remaining_share' , name: 'remaining_share'},
			{ data: 'price_per_share' , name: 'price_per_share'},
			{ data: 'purchase_date' , name: 'purchase_date'},
			{ data: 'total_price' , name: 'total_price'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});



		$('#update_price_per_share').on('focusout', function (e) {
			let total_shares = {{$issuer->total_shares}};
			let update_price_per_share =  $('#update_price_per_share').val().replace(/,/g, "");
			if (update_price_per_share && total_shares) {
				let total_value = update_price_per_share * total_shares;
				if (!isNaN(total_value)) {
					$('#update_total_value').val(total_value.toLocaleString());
				} else {
					$('#update_total_value').val('');	
				}
			} else {
				$('#update_total_value').val('');
			}
		});



		$('#equity_update_submit').on('click', function(e) {
			e.preventDefault();
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#equity_update_form');
			let price_per_share = $('#update_price_per_share').val().replace(/,/g, "");

			let data_to_save = form.serializeArray();

			data_to_save.push({name: 'price_per_share',value: price_per_share });
			data_to_save.push({name: 'equity_issuer_id',value: {{$issuer->id}} });
			data_to_save.push({name: 'number_of_share',value: {{$issuer->total_shares}} });

			let options = {
				dataType : 'json',
				url: '{!!route('backend.investment.equity.update_price')!!}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$('.wait').addClass('hidden');
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: 'Unit Price Updated successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						$('#equity_update_modal').modal('hide');
						form.trigger("reset");
						setTimeout(function(){location.reload(); }, 1000);
					} 
					else {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
					$('.footer_button').removeClass('hidden');
					$('.wait').addClass('hidden');
				}
			};
			$(form).ajaxSubmit(options);
		});



		$('#sales_table').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [1,2,4] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/equity/sales_datatable/'.$issuer->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'lot_number' , name: 'lot_number'},
			{ data: 'number_of_share' , name: 'number_of_share'},
			{ data: 'share_value' , name: 'share_value'},
			{ data: 'selling_date' , name: 'selling_date'},
			{ data: 'total_share_value' , name: 'total_share_value'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});

		sellLots();

		$('#sell_price').on('focusout', function (e) {
			totalSellPrice();
		});

		$('#total_share_to_sell').on('focusout', function (e) {
			totalSellPrice();
		});



		function totalSellPrice() {
			let sell_price = $('#sell_price').val().replace(/,/g, "");
			let total_share_to_sell =  $('#total_share_to_sell').val().replace(/,/g, "");
			if (total_share_to_sell && sell_price) {
				let total_share_value = total_share_to_sell * sell_price;
				if (!isNaN(total_share_value)) {
					$('#total_sell_cost').val(total_share_value.toLocaleString());
				} else {
					$('#total_sell_cost').val('');	
				}
			} else {
				$('#total_sell_cost').val('');
			}
		}


		$('#equity_sell_btn').on('click', function(e) {
			e.preventDefault();
			
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#equity_sell_form').trigger('reset');
			$('#equity_sell_modal').modal('show');
		});


		$('#equity_sell_submit').on('click', function(e) {
			e.preventDefault();
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');

			let max_shares = $('#sell_lot option:selected').data('total_shares');
			if (max_shares) {
				max_shares = max_shares;
			} else {
				max_shares = 0;
			}
			let units_to_sell = $('#total_share_to_sell').val().replace(/,/g, "");

			if (units_to_sell <= max_shares ) {
				let form = $('#equity_sell_form');

				let sell_price = $('#sell_price').val().replace(/,/g, "");
				let commission = $('#commission').val().replace(/,/g, "");

				let data_to_save = form.serializeArray();
				data_to_save.push({name: 'price_per_share',value: sell_price});
				data_to_save.push({name: 'commission',value: commission});

				let options = {
					dataType : 'json',
					url: '{!!route('backend.investment.equity.sell')!!}',
					data : data_to_save,
					type : 'POST',
					success : function (data) {
						if (data.success) {
							$('.wait').addClass('hidden');
							$.amaran({
								'theme'     :'awesome success',
								'content'   :{
									title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
									message: 'Shares have been sold successfully',
									info:'',
									icon: 'fa fa-check-square-o'
								},
								'position'  :'bottom left',
								'outEffect' :'slideBottom',
								'inEffect'  :'slideLeft'
							});
							$('#equity_sell_modal').modal('hide');
							form.trigger("reset");
							setTimeout(function(){location.reload(); }, 1000);
						} 
						else {
							$('.footer_button').removeClass('hidden');
							$('.wait').addClass('hidden');
							if (data.errors) {
								let m = Object.keys(data.errors);
								$.each(m, function( index, value ) {
									$('.'+value+'_error').removeClass('hidden');
									$('.'+value+'_error').text(data.errors[value][0]);
									$('#'+value).addClass('custom_error');
								});
							}else{
								swal({
									title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
									text: '<h6>Something went wrong! Kindly try again</h6>',
									type: 'error',
									showCancelButton: false,
									cancelButtonText: 'Cancel',
									confirmButtonColor: '#75E0A2',
									confirmButtonText: 'OK',
									html: true,
									closeOnConfirm: true
								}, function (isConfirmed) {
									location.reload();
								});
							}
						}
					},
					error: function (data) {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
					}
				};
				$(form).ajaxSubmit(options);
			} else {
				
				$('.footer_button').removeClass('hidden');
				$('.wait').addClass('hidden');
				$('.number_of_shares_error').removeClass('hidden');
				$('.sell_lot_error').removeClass('hidden');
				
				if (units_to_sell > max_shares ) {
					$('.number_of_shares_error').text('Number of shares should not exceed '+max_shares.toLocaleString());
				} else {
					$('.number_of_shares_error').text('Kindly select lot first');
					$('.sell_lot_error').text('This lot has no shares to sell');
				}
				$('#total_share_to_sell').addClass('custom_error');
				$('#sell_lot').addClass('custom_error');
			}
		});


		$('#sell_lot').on('change', function (e) {
			e.preventDefault();
			sellLots();
		});

		function sellLots() {
			let remain_units = $('#sell_lot option:selected').data('total_shares');
			if (remain_units) {
				remain_units =  remain_units;
			} else {
				remain_units = 0;
			}
			$('#total_share_for_sell').val(remain_units.toLocaleString());
			$('#total_share_to_sell').attr("max",remain_units);
		}


		$(".select_broker").select2({
			dropdownParent: $("#equity_sell_modal"),
			minimumInputLength: 3,
			width: '100%',
			multiple: false,
			allowClear: true,
			debug: true,
			placeholder: "",
			ajax: {
				url: '{!! route('backend.investment.employers') !!}',
				dataType: 'json',
				delay: 250,
				processResults: function (data) {
					data = data.map(function (item) {
						return {
							text: item.name,
							id: item.id,
							name: item.name,
						};
					});
					return {
						results:  data
					};
				},
				cache: true
			}
		});


		$(".select_search").select2({
			dropdownParent: $("#equity_sell_modal"),
			multiple: false,
			width: '100%',
			allowClear: true,
			debug: true,
			placeholder: ""
		});


		$('#dividend_table').DataTable({	
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{ className: "dt-right", "targets": [1,2,3] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/equity/dividend_datatable/'.$issuer->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'dividend_declaration_date' , name: 'dividend_declaration_date'},
			{ data: 'number_of_share' , name: 'number_of_share'},
			{ data: 'share_value' , name: 'share_value'},
			{ data: 'total_share_value' , name: 'total_share_value'},
			{ data: 'dividend_payment_date' , name: 'dividend_payment_date'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {

				}).hover(function() {
					// $(this).css('cursor','pointer');
				}, function() {
					// $(this).css('cursor','auto');
				});
			}
		});


		$('#dividend_btn').on('click', function(e) {
			e.preventDefault();
			// dividendValue();
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#dividend_form').trigger('reset');
			$('#dividend_modal').modal('show');
		});

		$('#dividend_per_share').on('focusout', function (e) {
			dividendValue();
		});

		function dividendValue() {
			let dividend_per_share = $('#dividend_per_share').val().replace(/,/g, "");
			let dividend_shares =  $('#dividend_shares').val().replace(/,/g, "");
			if (dividend_shares && dividend_per_share) {
				let dividend_value = dividend_shares * dividend_per_share;
				if (!isNaN(dividend_value)) {
					$('#dividend_amount').val(dividend_value.toLocaleString());
					// $('#dividend_receivable').val(dividend_value.toLocaleString());

				} else {
					$('#dividend_amount').val('');	
					// $('#dividend_receivable').val('');	
				}
			} else {
				$('#dividend_amount').val('');
				// $('#dividend_receivable').val('');
			}
		}


		$('#dividend_submit').on('click', function(e) {
			e.preventDefault();
			
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#dividend_form');
			let dividend_per_share = $('#dividend_per_share').val().replace(/,/g, "");
			// let share_no =  $('#update_total_shares').val().replace(/,/g, "");

			let data_to_save = form.serializeArray();
			data_to_save.push({name: 'dividend_per_share',value: dividend_per_share });
			data_to_save.push({name: 'id',value: {{$issuer->id}} });

			let options = {
				dataType : 'json',
				url: '{!!route('backend.investment.equity.add_dividend')!!}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$('.wait').addClass('hidden');
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: 'Dividend Added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						$('#dividend_modal').modal('hide');
						form.trigger("reset");
						setTimeout(function(){location.reload(); }, 1000);
					} 
					else {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
					$('.footer_button').removeClass('hidden');
					$('.wait').addClass('hidden');
				}
			};
			$(form).ajaxSubmit(options);
		});



		if (location.hash !== '') {
			$('a[href="' + location.hash + '"]').tab('show');
			$('a[href="' + location.hash + '"]').trigger('click');
		}

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var tab = $(e.target).attr('href').substr(1);
			if (history.pushState) {
				history.pushState(null, null, '#' + tab);
			} else {
				location.hash = '#' + tab;
			}
		});


	});

</script>;

@endpush
