<div class="modal fade" id="equity_sell_modal" tabindex="-1" role="dialog">
	<form role="form" id="equity_sell_form">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Sell Shares</h5>
					<span class="help-block pt-2"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body">

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Lot Number</label>
							<div class="form-group">
								<div class="input-group">
									<select name="sell_lot" class="form-control select_search" id="sell_lot">
										<option value="" selected="selected">Select Lot</option>
										@if(count($issuer->lots))
										@foreach($issuer->lots as $lot)
										@if($lot->remaining_share>0)
										<option value="{{$lot->id}}" data-total_shares='{{$lot->remaining_share}}'>
											{{$lot->lot_number}}
										</option>
										@endif
										@endforeach
										@endif
									</select>
								</div>
								<span class="sell_lot_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class=" mb-1">Total Shares</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="total_share" class='form-control money' id="total_share_for_sell" disabled="disabled">
								</div>
								<span class="total_share_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Number Of Shares</label>
							<div class="form-group">
								<div class="input-group">
									<input type="number" min='1' name="number_of_shares" class='form-control' id="total_share_to_sell">
								</div>
								<span class="number_of_shares_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1">Price Per Share</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="price_per_share" class='form-control money' id="sell_price">
								</div>
								<span class="price_per_share_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Total Price</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="total_cost" class='form-control' id="total_sell_cost" disabled="disabled">
								</div>
								<span class="total_cost_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1"> Selling Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="selling_date" class='form-control date_picker' id="selling_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="selling_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required pb-1">Broker</label>
							<div class="form-group">
								<div class="input-group">
									<select name="broker" class="form-control select_broker" id="broker"></select>
								</div>
								<span class="broker_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required pb-1" >Commission</label>
							<div class="form-group">
								<div class="input-group">
									{!! Form::text('commission', null, ['class' => 'form-control money','id' => 'commission']) !!}
								</div>
								<span class="commission_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<span class="footer_button">
						<button type="button" class="btn btn-primary" id="equity_sell_submit">Save changes</button>
						<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
					</span>
					<span class="wait hidden pull-right">
						<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
						&nbsp; <span class="h6"> Please wait .... </span>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>
