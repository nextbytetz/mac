<div class="modal fade" id="dividend_modal" tabindex="-1" role="dialog">
	<form role="form" id="dividend_form">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Dividend </h5>
					<span class="help-block pl-2"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body">
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Number Of Shares</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" value="{{number_format($issuer->total_shares,0)}}" name="dividend_shares" class='form-control' id="dividend_shares" disabled="disabled">
								</div>
								<span class="dividend_shares_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1">Dividend Per Share</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="dividend_per_share" class='form-control money' id="dividend_per_share">
								</div>
								<span class="dividend_per_share_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Dividend Amount</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="dividend_amount" class='form-control' id="dividend_amount" disabled="disabled">
								</div>
								<span class="dividend_amount_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1"> Declaration Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="declaration_date" class='form-control date_picker' id="declaration_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="declaration_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>


					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1"> Payment Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="dividend_payment_date" class='form-control date_picker' id="dividend_payment_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="dividend_payment_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						{{-- <div class="col-sm-6">
							<label class="required mb-1">Tax Rate</label>
							<div class="form-group">
								<div class="input-group">
									<input type="number" min="1" max="100" name="tax_rate" class='form-control' id="tax_rate">
								</div>
								<span class="tax_rate_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div> --}}
						{{-- <div class="col-sm-6">
							<label class="required mb-1"> Dividend Income</label>
							<div class="form-group">
								<div class="input-group">
									<div class="input-group">
										<input type="text" name="dividend_income" class='form-control' id="dividend_income" disabled="disabled">
									</div>
								</div>
								<span class="dividend_income_error error_field hidden mt-1 text-danger" ></span>
							</div>
						</div> --}}
					</div>

				</div>
				<div class="modal-footer">
					<span class="footer_button">
						<button type="button" class="btn btn-primary" id="dividend_submit">Save changes</button>
						<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
					</span>
					<span class="wait hidden pull-right">
						<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
						&nbsp; <span class="h6"> Please wait .... </span>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>
