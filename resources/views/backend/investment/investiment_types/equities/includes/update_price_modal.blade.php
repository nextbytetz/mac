<div id="equity_update_modal" class="modal fade" role="dialog" data-focus="false">
	<form role="form" id="equity_update_form">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mb-1">Update Price</h5>
					<span class="help-block"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body">
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Total Number Of Shares</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" value="{{number_format($issuer->total_shares,0)}}" name="total_shares" class='form-control' id="update_total_shares" disabled="disabled">
								</div>
								<span class="total_shares_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1">Total Value</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="total_value" class='form-control' id="update_total_value" disabled="disabled">
								</div>
								<span class="total_value_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Price Per Share</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="price_per_share" class='form-control money' id="update_price_per_share">
								</div>
								<span class="price_per_share_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required mb-1"> Price Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="price_date" class='form-control date_picker' id="price_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="price_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>


				</div>
				<div class="modal-footer">
					<span class="footer_button">
						<button type="button" class="btn btn-primary" id="equity_update_submit">Save changes</button>
						<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
					</span>
					<span class="wait hidden pull-right">
						<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
						&nbsp; <span class="h6"> Please wait .... </span>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>
