@extends('layouts.backend.main', ['title' => 'Equities', 'header_title' =>' Equities'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
</style>
@endpush

@section('content')

<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="col-md-9">
				<div class="pull-right mb-1">
					<button class="btn btn-primary btn-md" id="add_new_issuer"  role="button">Add New Issuer</button>
				</div>
				<table class="display table table-bordered" cellspacing="0" width="100%" id="issuer_datatable">
					<thead>
						<tr>
							<th>Issuer</th>
							<th>Issuer Reference</th>
							<th>Total Shares</th>
							<th>Price Per Share</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="grey_modal">
						<table style="width:100%">
							<tr>
								<td  align="center"><h5><strong><span class="light_dark_color">	Summary</span></strong></h5></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="light_grey_bg">&nbsp;</div>
					<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
						<p class="underline pt-1">
							<span style="font-weight: lighter;">Total Issuers :</span> 
							<span style="font-weight: bold;" > 
								{{$summary['issuer_count']}}
							</span>
						</p>
						{{-- <p class="underline pt-1">
							<span style="font-weight: lighter;"> Total Schemes :</span> 
							<span style="font-weight: bold;" >
								{{$summary['scheme_count']}}
							</span>
						</p> --}}
						<p class="underline pt-1">
							<span style="font-weight: lighter;"> Total Shares :</span> 
							<span style="font-weight: bold;" >
								{{number_format($summary['shares_count'],0)}}
							</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

<div class="modal fade" id="add_issuer_modal" tabindex="-1" role="dialog">
	<form role="form" id="add_issuer_form">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pb-1">Add Issuer</h5>
					<span class="help-block pt-3"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
				</div>
				<div class="modal-body">
					<div class="row pb-2">
						<div class="col-sm-6 issuer_id">
							<label class="required pb-1">Select Issuer</label>
							<div class="form-group">
								<div class="input-group">
									<select name="issuer" class="form-control select_employer" id="issuer"></select>
								</div>
								<span class="issuer_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6 issuer_name hidden">
							<label class="required mb-1">Issuer Name</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="issuer_name" class='form-control hidden' id="issuer_name">
								</div>
								<span class="issuer_name_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<label class="required mb-1">Issuer Is in</label>
							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<input type="radio" value="MAC" name="issuer_source" class="issuer_source" checked='checked'> &nbsp;MAC System
									</div>
									<div class="col-xs-12 pt-1">
										<input type="radio" value="OTHER" name="issuer_source" class="issuer_source"> &nbsp; Not In MAC
									</div>
									
								</div>
								<span class="issuer_source_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required mb-1">Issuer Reference</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="issuer_reference" class='form-control' id="issuer_reference">
								</div>
								<span class="issuer_reference_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<span class="footer_button">
						<button type="button" class="btn btn-primary" id="add_issuer_submit">Save changes</button>
						<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
					</span>
					<span class="wait hidden pull-right">
						<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 90%; width:50%;" />  
						&nbsp; <span class="h6"> Please wait .... </span>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>


@stop

@php
if (empty($budget_allocation_id)) {
	$url = url('investment/equity/datatable/');
} else {
	$url = url('investment/equity/datatable/'.$budget_allocation_id);
}
@endphp


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">

	$(function () {

		$('#issuer_datatable').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			autoWidth: true,
			order: [[2, 'desc']],
			columnDefs: [
			{className: "dt-right", "targets": [3]},
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/equity/datatable/') !!}',
				type : 'GET'
			},
			columns: [
			{ data: 'issuer_name' , name: 'issuer_name'},
			{ data: 'issuer_reference' , name: 'issuer_reference'},
			{ data: 'total_shares' , name: 'total_shares'},
			{ data: 'shares_value' , name: 'shares_value'},
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
					document.location.href = '{!! url('investment/equity/show/') !!}/'+aData['id'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});

		$('#add_new_issuer').on('click', function(e) {
			e.preventDefault();
			
			$('.error_field').addClass('hidden');
			$('.footer_button').removeClass('hidden');
			$('.wait').addClass('hidden');
			$('#add_issuer_form').trigger('reset');
			$('#add_issuer_modal').modal('show');
			issuerSource();
		});


		$('.issuer_source').on('click', function (e) {
			issuerSource();
		});


		$('#add_issuer_submit').on('click', function(e) {
			e.preventDefault();
			$('.footer_button').addClass('hidden');
			$('.wait').removeClass('hidden');

			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#add_issuer_form');
			let options = {
				dataType : 'json',
				url: '{!!route('backend.investment.equity.save_issuer')!!}',
				data : {},
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$('.wait').addClass('hidden');
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: 'Issuer Added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						$('#add_issuer_modal').modal('hide');
						form.trigger("reset");
						setTimeout(function(){location.reload(); }, 1000);
					} 
					else {
						$('.footer_button').removeClass('hidden');
						$('.wait').addClass('hidden');
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
					$('.footer_button').removeClass('hidden');
					$('.wait').addClass('hidden');
				}
			};
			$(form).ajaxSubmit(options);
		});

		function issuerSource(){
			let source_of_issuer = $('.issuer_source:checked').val();
			if (source_of_issuer == 'MAC') {
				$('.issuer_id').removeClass('hidden');
				$('.issuer_name').addClass('hidden');	
				$('#issuer_name').addClass('hidden');	
			} else {
				$('.issuer_name').removeClass('hidden');
				$('#issuer_name').removeClass('hidden');
				$('.issuer_id').addClass('hidden');			
			}
		}

		$(".select_employer").select2({
			dropdownParent: $("#add_issuer_modal"),
			minimumInputLength: 3,
			width: '100%',
			multiple: false,
			allowClear: true,
			debug: true,
			placeholder: "",
			ajax: {
				url: '{!! route('backend.investment.employers') !!}',
				dataType: 'json',
				delay: 250,
				processResults: function (data) {
					data = data.map(function (item) {
						return {
							text: item.name,
							id: item.id,
							name: item.name,
						};
					});
					return {
						results:  data
					};
				},
				cache: true
			}
		});

	});

</script>;

@endpush
