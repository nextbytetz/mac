@extends('layouts.backend.main', ['title' => $investment_type->name.' '.$period->name, 'header_title' => $investment_type->name.' '.$period->name])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')
<div class = "row">

  <div>&nbsp;</div>
  {{--Tabs navigation--}}
  <div class = "row">
    <div class="col-md-12">

      <div class="basic_nav_pills nav_basic_tab">
        <ul class="nav nav-tabs">
          {{--General--}}
          <li class="nav-item">
            <a class="nav-link active" href="#general"
            data-toggle="tab">@lang('labels.general.general')
          </a>
        </li>
        <li class="nav-item">
            {{-- <a class="btn btn-success new" style="padding: 11px;" href="#">New {{$investment_type->name}}</a> --}}
            {!! Form::open(['route' => 'backend.investment.choose_type']) !!}
            <input class="reference form-control" name="reference" hidden="hidden" value="{{$investment_type->reference}}" />
            {!! Form::submit('Add '.$investment_type->name, ['style' => 'padding: 11px;','class' => 'btn btn-success btn-save btn-block']) !!}
             {!! Form::close() !!}
        </li>
      </ul>
      <br>
      <div class="nav_tab_contain tab-content">
        <div id="general" class="nav_tab_pane tab-pane active in">
          <div class = "row">
            <div class="col-md-12">
              <div class="col-md-9">
                {{-- datatable --}}
                <table class="display" cellspacing="0" width="100%" id ="investiment_type">
                  <thead>
                    <tr>
                      <th>Start Date</th>
                      <th>End date</th>
                      <th>Tenure</th>
                      <th>Amount Invested</th>
                      <th>Institution / Issuer</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <div class="col-md-3">
                {{-- summary --}}
                <div class="row">
                  <div class="grey_modal">
                    <table style="width:100%">
                      <tr>
                        <td  align="center"><h5><strong><span class="light_dark_color">Investment Budget Summary</span></strong></h5></td>
                      </tr>
                    </table>
                  </div>
                </div>

                <div class="row">
                  <div class="light_grey_bg">&nbsp;</div>
                  <div class="col-md-12 light_grey_bg" style="min-height: 360px;">
                    <p class="underline" style="padding-bottom: 2%;" >
                      <span style="font-weight: lighter;">Period : </span> 
                      <span style="font-weight: bold;" >{{$period->name}}</span>
                    </p>
                    <p class="underline" style="padding-top: 2%;">
                      <span style="font-weight: lighter;">Total Budget :</span> 
                      <span style="font-weight: bold;" > {{number_format($budget->total_amount_invested,2)}}/=</span>
                    </p>
                    <p class="underline" style="padding-top: 2%;">
                      <span style="font-weight: lighter;"> Total allocation :</span> 
                      <span style="font-weight: bold;" >{{number_format($budget->amount_alocated,2)}}</span>
                    </p>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
@endsection

@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


<script type="text/javascript">

  $(function(){
    // let fin_id = $('#fin_year option:selected').val();
    let fin_id = "{{$fin_id}}";
    let budget_alocation_id = "{{$budget_alocation_id}}";
    let reference = "{{$reference}}";
    investmentTypeDatatable(fin_id,budget_alocation_id,reference);

    function investmentTypeDatatable(fin_id,budget_alocation_id,reference){
      $('#investiment_type').DataTable().clear().destroy();

      let investiment_type = $('#investiment_type').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: true,
      paging: true,
      info:false,
      order: [[ 0, "desc" ]],
      "columnDefs": [
        {"className": "dt-right", "targets": [ 2, 3]}
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{!! url('investment/summary/investment_type_datatable') !!}/'+fin_id+'/'+budget_alocation_id+'/'+reference,
        type : 'get'
      },
      columns: [
      { data: 'start_date' , name: 'start_date',searchable:false},
      { data: 'maturity_date' , name: 'maturity_date'},
      { data: 'tenure' , name: 'tenure', searchable:false},
      { data: 'amount_invested' , name :'amount_invested', searchable:false },
      { data: 'issuer' , name :'issuer', searchable:true },
      ],
      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
       $('td', nRow).click(function() {
        let col = $(this).index()+1;
        let colText = $('#investiment_type thead th:nth-child('+col+')').text();
        if(colText !='Action'){
          document.location.href = '{!! url("investment/summary/show") !!}/'+aData['inv_id']+'/'+reference+'/'+fin_id;
        }
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
    }

    $('.new').click(function(e){
      console.log(e)
    })



  });



</script>
@endpush