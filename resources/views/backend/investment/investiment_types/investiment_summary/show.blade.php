@extends('layouts.backend.main', ['title' => $issuer.' '.$period->name, 'header_title' => $issuer.' '.$period->name])
@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')
<div class = "row">

  <div>&nbsp;</div>
  {{--Tabs navigation--}}
  <div class = "row">
    <div class="col-md-12">

      <div class="basic_nav_pills nav_basic_tab">
        <ul class="nav nav-tabs">
          {{--General--}}
          <li class="nav-item">
            <a class="nav-link active" href="#general"
            data-toggle="tab">@lang('labels.general.general')
          </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#calculated"
            data-toggle="tab">Calculated
          </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#edit"
            data-toggle="tab">edit
          </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#update"
            data-toggle="tab">Update
          </a>
        </li>
      </ul>
      <br>
      <div class="nav_tab_contain tab-content">
        <div id="general" class="nav_tab_pane tab-pane active in">
          <div class = "row">
            <div class="col-md-12">
              <div class="col-md-9">
                {{-- {{dd($reference)}} --}}
                {{-- datatable --}}
                @if($reference == 'INTFDR')
                @include('backend.investment.investiment_types.investiment_summary.includes.fixed_deposit_general')
                @endif
                @if($reference == 'INTTBILLS')
                @include('backend.investment.investiment_types.investiment_summary.includes.treasury_bills_general')
                @endif
                @if($reference == 'INTTBONDS')
                @include('backend.investment.investiment_types.investiment_summary.includes.treasury_bonds_general')
                @endif
                @if($reference == 'INTCBONDS')
                  @include('backend.investment.investiment_types.investiment_summary.includes.corporate_bonds_general')
                 @endif
              </div>
              <div class="col-md-3">
                {{-- summary --}}
                <div class="row">
                  <div class="grey_modal">
                    <table style="width:100%">
                      <tr>
                        <td  align="center"><h5><strong><span class="light_dark_color">Investment Budget Summary</span></strong></h5></td>
                      </tr>
                    </table>
                  </div>
                </div>

                <div class="row">
                  @include('backend.investment.investiment_types.investiment_summary.includes.right_side_details')
                </div>

              </div>
            </div>
          </div>
        </div>
        <div id="calculated" class="nav_tab_pane tab-pane">
          <div class = "row">
            <div class="col-md-12">
              <div class="col-md-9">
                {{-- datatable --}}
                 @if($reference == 'INTFDR')
                 @include('backend.investment.investiment_types.investiment_summary.includes.fixed_deposit_calculated')
                 @endif
                 @if($reference == 'INTTBILLS')
                  @include('backend.investment.investiment_types.investiment_summary.includes.treasury_bills_calculated')
                 @endif
                 @if($reference == 'INTTBONDS')
                  @include('backend.investment.investiment_types.investiment_summary.includes.treasury_bonds_calculated')
                 @endif
                 @if($reference == 'INTCBONDS')
                  @include('backend.investment.investiment_types.investiment_summary.includes.corporate_bonds_calculated')
                 @endif
              </div>
              <div class="col-md-3">
                {{-- summary --}}
                <div class="row">
                  <div class="grey_modal">
                    <table style="width:100%">
                      <tr>
                        <td  align="center"><h5><strong><span class="light_dark_color">Investment Budget Summary</span></strong></h5></td>
                      </tr>
                    </table>
                  </div>
                </div>

                <div class="row">
                  @include('backend.investment.investiment_types.investiment_summary.includes.right_side_details')
                </div>

              </div>
            </div>
          </div>
        </div>
        <div id="update" class="nav_tab_pane tab-pane">
          <div class = "row">
            <div class="col-md-12">
              <div class="col-md-9">
                {{-- datatable --}}
                 @if($reference == 'INTFDR' )
                    @include('backend.investment.investiment_types.investiment_summary.includes.fixed_deposit_update')
                 @endif
                 @if($reference == 'INTTBONDS')
                    @include('backend.investment.investiment_types.investiment_summary.includes.treasury_bonds_update')
                 @endif
                 @if($reference == 'INTTBILLS')
                    @include('backend.investment.investiment_types.investiment_summary.includes.treasury_bills_update')
                 @endif
                  @if($reference == 'INTCBONDS')
                    @include('backend.investment.investiment_types.investiment_summary.includes.corporate_bonds_update')
                 @endif
                
              </div>
              <div class="col-md-3">
                {{-- summary --}}
                <div class="row">
                  <div class="grey_modal">
                    <table style="width:100%">
                      <tr>
                        <td  align="center"><h5><strong><span class="light_dark_color">Investment Budget Summary</span></strong></h5></td>
                      </tr>
                    </table>
                  </div>
                </div>

                <div class="row">
                  @include('backend.investment.investiment_types.investiment_summary.includes.right_side_details')
                </div>

              </div>
            </div>
          </div>
        </div>

        <div id="edit" class="nav_tab_pane tab-pane">
          <div class = "row">
            <div class="col-md-12">
                 @if($reference == 'INTFDR' )
                    @include('backend.investment.investiment_types.updates.fixed_deposit')
                 @endif
                 @if($reference == 'INTTBONDS')
                    @include('backend.investment.investiment_types.updates.treasury_bonds')
                 @endif
                 @if($reference == 'INTTBILLS')
                    @include('backend.investment.investiment_types.updates.treasury_bills')
                 @endif
                  @if($reference == 'INTCBONDS')
                    @include('backend.investment.investiment_types.updates.corporate_bonds')
                 @endif
                
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


<script type="text/javascript">

  $(function(){ 

    if (location.hash !== '') {
      $('a[href="' + location.hash + '"]').tab('show');
      $('a[href="' + location.hash + '"]').trigger('click');
    }


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var tab = $(e.target).attr('href').substr(1);
      if (history.pushState) {
        history.pushState(null, null, '#' + tab);
      } else {
        location.hash = '#' + tab;
      }
    });



  });



</script>
@endpush