@extends('layouts.backend.main', ['title' => 'Investment summary', 'header_title' => 'Investment summary'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')
@include('backend.investment.investiment_types.includes.fin_years.fin_year_active')
<div class="row">
  <div class="col-md-12">
    <table class="display" cellspacing="0" width="100%" id ="investiment_summary">
      <thead>
        <tr>
          <th>Investment type</th>
          <th>total Investments</th>
          <th>total amount</th>
          <th>percentage</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
@endsection

@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


<script type="text/javascript">

  $(document).ready(function(){
    let fin_id = $('#fin_year option:selected').val();
    investmentSummaryDatatable(fin_id);

    $('#fin_year').change(function(e){
       let fin_id = $('#fin_year option:selected').val();
       investmentSummaryDatatable(fin_id);
    });

    function investmentSummaryDatatable(fin_id){
      $('#investiment_summary').DataTable().clear().destroy();

      let investiment_summary = $('#investiment_summary').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: true,
      paging: true,
      info:false,
      order: [[ 0, "desc" ]],
      "columnDefs": [
        {"className": "dt-right", "targets": [ 1, 2, 3]}
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{!! url('investment/summary/datatable') !!}/'+fin_id,
        type : 'get'
      },
      columns: [
      { data: 'name' , name: 'name'},
      { data: 'total_investment' , name: 'total_investment'},
      { data: 'amount' , name: 'amount', searchable:false},
      { data: 'percent' , name :'percent', searchable:false },
      ],
      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
       $('td', nRow).click(function() {

        let col = $(this).index()+1;
        let colText = $('#investiment_summary thead th:nth-child('+col+')').text();
        if(colText !='Action'){
          if (aData['cid'] == "INTLOAN") {
            document.location.href = '{!! url("investment/show/loans") !!}/'+aData['investment_budget_allocation_id'];
          } 
          else if(aData['cid'] == "INTEQUITIES"){
            document.location.href = '{!! url("investment/equity/index") !!}/'+aData['investment_budget_allocation_id'];

          }
          else if(aData['cid'] == "INTRESTATE"){
            // document.location.href = '{!! url("investment/equity/index") !!}/'+aData['investment_budget_allocation_id'];

          }
          else if(aData['cid'] == "INTCIS"){
            document.location.href = '{!! url("investment/collective_scheme/index") !!}/'+aData['investment_budget_allocation_id'];

          }else {
            document.location.href = '{!! url("investment/summary/investment_type") !!}/'+fin_id+'/'+aData['investment_budget_allocation_id']+'/'+aData['cid'];
          }
          
        }
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
    }



  });



</script>
@endpush