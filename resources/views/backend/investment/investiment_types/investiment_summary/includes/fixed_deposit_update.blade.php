@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/employer.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}
<style>

    .fixed_deposit_update:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.investiment.fixed_deposit_update')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_update {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .fixed_deposit_update_records:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Updating Records";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_update_records {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .fixed_deposit_update_payment:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Updating Payment After Maturity";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_update_payment {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>
@endpush
{!! Form::open(['route' => 'backend.investment.fixed_deposit_update', 'name' => 'fixed_deposit_update']) !!}
    <div class="fileld-layout">
        <input type="text" name="investment_fixed_deposit_id" value="{{$investment_id}}" hidden>
                    <div class="fixed_deposit_update_records">
                        <div class="fileld-layout">
                            <div class="row">
                            <div class="col-md-12">
                            <div class="col-md-6">
                            <label class="required">Certificate number</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('certificate_number', $investment->certificate_number, ['class' => 'form-control ', 'style' => "border-radius:3px;", 'id' => '']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            {{-- <label class="required">Scanned Certificate Copy</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('certificate_copy', null, ['class' => 'form-control ', 'style' => "border-radius:3px;", 'id' => '']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div> --}}
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
    <br>
        <div class="fixed_deposit_update_payment">
              <div class="fileld-layout">
                            <div class="row">
                            <div class="col-md-12">
                            <div class="col-md-1">
                                <label for="amount_invested"></label>
                                <select class="form-control currency" name="currency_id">
                                    @foreach($currencies as $c)
                                    <option value="{{$c->id}}" {{$c->id == 1 ? 'selected' : ''}}>{{$c->code}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-10">
                            <div class="exchange-rate">
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="fileld-layout">
                    <div class="row">
                            <div class="col-md-12">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label class="required" for="interest_received">Interest Received</label>
                                <div class="input-group">
                                    {!! Form::text('interest_received', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'interest_received']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                                <label class="required" for="principal_redeemed">Principal redeemed</label>
                                <div class="input-group">
                                    {!! Form::text('principal_redeemed', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'principal_redeemed']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="row">
                            <div class="col-md-12">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label class="required" for="penalty_received">Penalty Received</label>
                                <div class="input-group">
                                    {!! Form::text('penalty_received', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'penalty_received']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                           <label class="required">Date of payment received</label>
                           <div class="form-group">
                                     <div class="input-group">
                                    {!! Form::text('payment_date_received', null, ['class' => 'form-control to-tenure datepicker', 'style' => "border-radius:3px;", 'id' => 'payment_date_received','autocomplete'=>'off']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                    </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
        <div class="row">
        <div class="col-md-12">
            <div class="receivables">
                <div class="row">
                    <div class="col-md-12">
                        {{--</div>--}}
                        <hr/>
                        <div class="pull-right">
                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="Update" />
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    {!! Form::close() !!} 
@push('after-script-end')

<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}

<script>
    $(function(){
        $(function () {
            jQuery('.datepicker').datetimepicker({
                timepicker:false,
                format:'Y-m-d',
            });
        });
        let reference = "{{$reference}}";
        let fin_id = "{{$fin_id}}";
        let budget_id = "{{$budget->bgtId}}";

         /*retreive used and remained budget*/
        function calculateSummary(reference,fin_id,budget_id){
            $.ajax({
                data : {
                    'reference' : reference,
                    'fin_id' : fin_id,
                    'budget_id' : budget_id,
                },
                dataType : "json",
                method : "get",
                url : "{{url('investment/summary/calculate')}}",
                success : function (data) {
                    console.log(data.remained_budget)
                    if (data) {
                        $('.used-budget').text(data.used_budget);
                        $('.remained-budget').text(data.remained_budget);
                    }else{
                        $('.used-budget').text('');
                        $('.remained-budget').text('');
                    }

                 
                },
            })

        }

         $('.currency').change(function(e){
            let exchange_rate = ' <div class="form-group"><label class="required" for="exchange_rate">Exchange rate</label><div class="input-group">{!! Form::text('exchange_rate', null, ['class' => 'form-control numbers moneys', 'style' => "border-radius:3px;", 'id' => 'exchange_rate']) !!}<span class="input-group-addon"><i class="icon fa fa-money" aria-hidden="true"></i></span></div><span class="help-block"><span></span></span></div>';

            if ($('.currency option:selected').val() == 1) {
               $('.exchange-rate').fadeOut();
               $('.exchange-rate').empty();
            } else {
                $('.exchange-rate').append(exchange_rate);
                $('.exchange-rate').fadeIn();
            }
            
        })


        if(localStorage.getItem("Status"))
        {

        $.amaran({
        'theme'     :'awesome success',
        'content'   :{
        title : 'Dear {{ucwords(strtolower(access()->user()->firstname))}}',
        message: 'Fixed Deposit successfully Updated',
        info:'',
        icon: 'fa fa-check-square-o'
        },
        'position'  :'bottom left',
        'outEffect' :'slideBottom',
        'inEffect'  :'slideLeft'
        });

        localStorage.clear();
        }

          /* start : ensure only numbers are input on monetary boxes */
        $(".number").keydown(function (e) {
            number_only(e)
        });
        /* end : ensure only numbers are input on monetary boxes */
        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });

        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form[name=fixed_deposit_update]', function (e) {
            e.preventDefault();
            var form = this;

             $(".money").each(function() {
                var value = $(this).val().replace(/,/g, '')
                $(this).val(value)

            })

            var $data = $(form).serializeArray();
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            $("#fin_year").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            /* end: remove any printed error message in the input controls */
            $.ajax({
                data : $data,
                dataType : "json",
                method : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    $(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    calculateSummary(reference,fin_id,budget_id)
                    if (data.success) {

                        localStorage.setItem("Status",data.success)
                        window.location.reload();

                    }else if(data.errors){
                        $.each(data.errors, function(index, value) {
                        swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
                        $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });

                    }
                    else if(data.error){
                        console.log(data.error)
                        $.amaran({
                        'theme'     :'awesome warning',
                        'content'   :{
                        title : 'Dear {{ucwords(strtolower(access()->user()->firstname))}}',
                        message: data.error,
                        info:'',
                        icon: 'fa fa-times-circle-o'
                        },
                        'position'  :'bottom left',
                        'outEffect' :'slideBottom',
                        'inEffect'  :'slideLeft'
                        });

                    }
                },
            }).done(function() {

            }).fail(function() {

            }).always(function() {
                $(".btn-submit").prop('disabled', false);
            });
        });

        /* end: Submitting Form and perfom validation on the server side */

            /* start : ensure only numbers are input on monetary boxes */
    function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    /* end : ensure only numbers are input on monetary boxes */
    });
</script>
@endpush             
