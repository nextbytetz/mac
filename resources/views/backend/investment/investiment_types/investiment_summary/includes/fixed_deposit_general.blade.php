<div class="table table-responsive">
  <table class="display" cellspacing="0" width="100%" id ="general_data">
    <thead>
      <tr>
        <th>Type</th>
        <th>Date of Opening</th>
        <th>Date of Maturity</th>
        <th>Tenure</th>
        <th>Interest Type</th>
        <th>Interest Rate</th>
        <th>Penalty Rate</th>
        <th>Amount Invested</th>
        <th>Institution</th>
      </tr>
    </thead>
  </table>
</div>
@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


<script type="text/javascript">
$(function(){
    let fin_id = "{{$fin_id}}";
    let investment_id = "{{$investment_id}}";
    let budget_alocation_id = "{{$budget_alocation_id}}";
    let reference = "{{$reference}}";
    fixedDepositGeneralDatatable(fin_id,budget_alocation_id,reference);

      function fixedDepositGeneralDatatable(fin_id,budget_alocation_id,reference){
      $('#general_data').DataTable().clear().destroy();

      let general_data = $('#general_data').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: false,
      paging: false,
      info:false,
      order: [[ 0, "desc" ]],
      "columnDefs": [
        {"className": "dt-right", "targets": [7]}
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{!! url('investment/summary/show_datatable') !!}/'+investment_id+'/'+reference,
        type : 'get'
      },
      columns: [
      { data: 'type' , name: 'type',searchable:false},
      { data: 'opening_date' , name: 'opening_date',searchable:false},
      { data: 'maturity_date' , name: 'maturity_date'},
      { data: 'tenure' , name: 'tenure', searchable:false},
      { data: 'interest_type' , name :'interest_type', searchable:false },
      { data: 'interest_rate' , name :'interest_rate', searchable:false },
      { data: 'penalty_rate' , name: 'opening_date',searchable:false},
      { data: 'amount_invested' , name :'amount_invested', searchable:false },
      { data: 'institution_name' , name :'institution_name', searchable:false },
      ],
      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
       $('td', nRow).click(function() {
        let col = $(this).index()+1;
        let colText = $('#general_data thead th:nth-child('+col+')').text();
        if(colText !='Action'){
          {{-- document.location.href = '{!! url("investment/summary/show") !!}/'+aData['inv_id']+'/'+reference; --}}
        }
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
    }

  });
</script>
@endpush