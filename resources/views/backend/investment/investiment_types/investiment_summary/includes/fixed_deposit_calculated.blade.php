<div class="table table-responsive">
  <table class="display" cellspacing="0" width="100%" id ="calculated_data">
    <thead>
      <tr>
        <th>Payment Date</th>
        <th>Interest Receivable</th>
        <th>Interest Income</th>
        <th>Penalty Income</th>
        <th>Penalty Receivable</th>
        <th>Penalty Received</th>
        <th>Outstanding/Overpaid</th>
        <th>FDR weighted average return</th>
      </tr>
    </thead>
  </table>
</div>
@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


<script type="text/javascript">
$(function(){
    let fin_id = "{{$fin_id}}";
    let investment_id = "{{$investment_id}}";
    let budget_alocation_id = "{{$budget_alocation_id}}";
    let reference = "{{$reference}}";
    fixedDepositCalculatedDatatable(fin_id,budget_alocation_id,reference);

       function fixedDepositCalculatedDatatable(fin_id,budget_alocation_id,reference){
      $('#calculated_data').DataTable().clear().destroy();

      let calculated_data = $('#calculated_data').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: false,
      paging: false,
      info:false,
      order: [[ 0, "desc" ]],
      "columnDefs": [
        {"className": "dt-right", "targets": [ 1, 2, 3, 4, 5, 6]}
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{!! url('investment/summary/show_fixed_datatable') !!}/'+investment_id,
        type : 'get'
      },
      columns: [
      { data: 'payment_date_received' , name: 'payment_date_received',searchable:false},
      { data: 'interest_receivable' , name: 'interest_receivable',searchable:false},
      { data: 'interest_income' , name: 'interest_income',searchable:false},
      { data: 'penalty_income' , name: 'penalty_income'},
      { data: 'penalty_receivable' , name: 'penalty_receivable'},
      { data: 'penalty_received' , name: 'penalty_received'},
      { data: 'overpaid_interest' , name: 'overpaid_interest', searchable:false},
      { data: 'weighted_average_return' , name :'weighted_average_return', searchable:false },
      ],
      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
       $('td', nRow).click(function() {
        let col = $(this).index()+1;
        let colText = $('#general_data thead th:nth-child('+col+')').text();
        if(colText !='Action'){
          {{-- document.location.href = '{!! url("investment/summary/show") !!}/'+aData['inv_id']+'/'+reference; --}}
        }
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
    }

  });
</script>
@endpush