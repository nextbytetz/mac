<div class="table table-responsive">
<table class="display" cellspacing="0" width="100%" id ="general_data">
  <thead>
    <tr>
      <th>Holding Number</th>
      <th>Date of Settlement</th>
      <th>Date of Maturity</th>
      <th>Tenure</th>
      <th>Auction Date</th>
      <th>Auction Number</th>
      {{-- <th>Coupon Date</th> --}}
      <th>Coupon Rate</th>
      <th>Price</th>
      <th>Yield</th>
      <th>Face Value</th>
      <th>Tax rate</th>
    </tr>

  </thead>
</table>
</div>
@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}

{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


{{-- {{ Html::script(asset_url(). "/investmentDatatableScripts/jqueryDatatable2.js") }}
{{ Html::script(asset_url(). "/investmentDatatableScripts/jqueryDatatable.min.js") }} --}}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}


<script type="text/javascript">
$(function(){
    let fin_id = "{{$fin_id}}";
    let investment_id = "{{$investment_id}}";
    let budget_alocation_id = "{{$budget_alocation_id}}";
    let reference = "{{$reference}}";
    treasuryBondsGeneralDatatable(fin_id,budget_alocation_id,reference);
    
      function treasuryBondsGeneralDatatable(fin_id,budget_alocation_id,reference){
      $('#general_data').DataTable().clear().destroy();

      let general_data = $('#general_data').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: false,
      paging: false,
      info:false,
      order: [[ 0, "desc" ]],
      "columnDefs": [
        {"className": "dt-right", "targets": [ 6, 7, 8, 9, 10]}
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{!! url('investment/summary/show_datatable') !!}/'+investment_id+'/'+reference,
        type : 'get'
      },
      columns: [
      { data: 'holding_number' , name: 'holding_number',searchable:false},
      { data: 'settlement_date' , name: 'settlement_date',searchable:false},
      { data: 'maturity_date' , name: 'maturity_date'},
      { data: 'tenure' , name: 'tenure', searchable:false},
      { data: 'auction_date' , name :'auction_date', searchable:false },
      { data: 'auction_number' , name :'auction_number', searchable:false },
      // { data: 'coupon_date' , name: 'coupon_date',searchable:false},
      { data: 'coupon_rate' , name: 'coupon_rate',searchable:false},
      { data: 'price' , name: 'price',searchable:false},
      { data: 'yield' , name: 'yield',searchable:false},
      { data: 'amount_invested' , name :'amount_invested', searchable:false },
      { data: 'tax_rate' , name :'tax_rate', searchable:false },
      ],
      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
       $('td', nRow).click(function() {
        let col = $(this).index()+1;
        let colText = $('#general_data thead th:nth-child('+col+')').text();
        if(colText !='Action'){
          {{-- document.location.href = '{!! url("investment/summary/show") !!}/'+aData['inv_id']+'/'+reference; --}}
        }
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
    }

  });
</script>
@endpush