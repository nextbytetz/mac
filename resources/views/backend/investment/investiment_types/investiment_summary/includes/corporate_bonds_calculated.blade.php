<div class="table table-responsive">
<table class="display table-responsive" cellspacing="0" width="100%" id ="calculated_data">
  <thead>
    <tr>
      <th>Clean Price</th>
      <th>Dirty Price</th>
      <th>Cost</th>
      <th>Carrying Amount</th>
      <th>Balance Sheet Value</th>
      <th>Discount/Premium</th>
      <th>Fair value</th>
      <th>Amortization of premium/discount</th>
      <th>Interest income</th>
      <th>Interest receivable</th>
      <th>Interest Date</th>
      <th>Interest Amount</th>
      <th>Payment Date</th>
      <th>Interest Earned</th>
    </tr>
  </thead>
</table>
</div>
@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}

{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{-- 
{{ Html::script(asset_url(). "/investmentDatatableScripts/jqueryDatatable2.js") }}

{{ Html::script(asset_url(). "/investmentDatatableScripts/jqueryDatatable.min.js") }} --}}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}


<script type="text/javascript">
$(function(){
    let fin_id = "{{$fin_id}}";
    let investment_id = "{{$investment_id}}";
    let budget_alocation_id = "{{$budget_alocation_id}}";
    let reference = "{{$reference}}";
    corporateBondsCalculatedDatatable(fin_id,budget_alocation_id,reference,investment_id);

     function corporateBondsCalculatedDatatable(fin_id,budget_alocation_id,reference,investment_id){
      $('#calculated_data').DataTable().clear().destroy();

      let calculated_data = $('#calculated_data').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: false,
      paging: false,
      info:true,
      responsive:true,
      order: [[ 0, "desc" ]],
      "columnDefs": [
        {"className": "dt-right", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11]}
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{!! url('investment/summary/corporate_bonds_datatable') !!}/'+investment_id,
        type : 'get'
      },
      columns: [
      { data: 'price' , name: 'price',searchable:false},
      { data: 'dirty_price' , name: 'dirty_price',searchable:false},
      { data: 'cost' , name: 'cost'},
      { data: 'carrying_amount' , name: 'carrying_amount'},
      { data: 'balance_sheet_value' , name :'balance_sheet_value', searchable:false },
      { data: 'discount' , name: 'discount',searchable:false},
      { data: 'fair_value' , name: 'fair_value',searchable:false},
      { data: 'amortization' , name: 'amortization'},
      { data: 'interest_income' , name: 'interest_income'},
      { data: 'interest_receivable' , name: 'interest_receivable',searchable:false},
      { data: 'interest_date' , name: 'interest_date',searchable:false},
      { data: 'interest_amount' , name: 'interest_amount'},
      { data: 'payment_date' , name: 'payment_date'},
      { data: 'interest_earned' , name :'interest_earned', searchable:false },
      ],

      footerCallback :function ( row, data, start, end, display ) {
      // var api = this.api, data;
      var api = this.api(), data;
        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 11 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 11, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
            console.log(pageTotal)
             console.log($(api.column(0).footer()).html( 'TOTAL'  ))

        // Update footer
        $( api.column(0).footer() ).html( 'TOTAL'  );
        $( api.column( 11 ).footer() ).html(
            '$'+pageTotal +' ( $'+ total +' total)'
        );
      },
      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
       $('td', nRow).click(function() {
        let col = $(this).index()+1;
        let colText = $('#general_data thead th:nth-child('+col+')').text();
        if(colText !='Action'){
          {{-- document.location.href = '{!! url("investment/summary/show") !!}/'+aData['inv_id']+'/'+reference; --}}
        }
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
    }

  });
</script>
@endpush