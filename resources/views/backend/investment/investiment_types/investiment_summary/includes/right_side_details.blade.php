<div class="light_grey_bg">&nbsp;</div>
<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
  <p class="underline" style="padding-bottom: 2%;" >
    <span style="font-weight: lighter;">Period : </span> 
    <span style="font-weight: bold;" >{{$period->name}}</span>
  </p>
  <p class="underline" style="padding-top: 2%;">
    <span style="font-weight: lighter;">Total Budget :</span> 
    <span style="font-weight: bold;" > {{number_format($budget->total_amount_invested,2)}}/=</span>
  </p>
  <p class="underline" style="padding-top: 2%;">
    <span style="font-weight: lighter;"> Total allocation :</span> 
    <span style="font-weight: bold;" >{{number_format($budget->amount_alocated,2)}}</span>
  </p>
  <p class="underline " style="padding-top: 2%;">
    <span style="font-weight: lighter;"> Used Budget :</span> 
    <span style="font-weight: bold;" class="used-budget"></span>
  </p>
  <p class="underline " style="padding-top: 2%;">
    <span style="font-weight: lighter;"> Remained Budget :</span> 
    <span style="font-weight: bold;" class="remained-budget"></span>
  </p>
</div>

@push('after-script-end')
<script type="text/javascript">
  $(document).ready(function(e){
        let reference = "{{$reference}}";
        let fin_id = "{{$fin_id}}";
        let budget_id = "{{$budget->bgtId}}";

        calculateSummary(reference,fin_id,budget_id)
          /*retreive used and remained budget*/
        function calculateSummary(reference,fin_id,budget_id){
            $.ajax({
                data : {
                    'reference' : reference,
                    'fin_id' : fin_id,
                    'budget_id' : budget_id,
                },
                dataType : "json",
                method : "get",
                url : "{{url('investment/summary/calculate')}}",
                success : function (data) {
                    console.log(data.remained_budget)
                    if (data) {
                        $('.used-budget').text(data.used_budget);
                        $('.remained-budget').text(data.remained_budget);
                    }else{
                        $('.used-budget').text('');
                        $('.remained-budget').text('');
                    }

                 
                },
            })

        }
  })
</script>

@endpush