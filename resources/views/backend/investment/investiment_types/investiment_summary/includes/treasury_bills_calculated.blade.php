<div class="table table-responsive">
<table class="display" cellspacing="0" width="100%" id ="calculated_data">
  <thead>
    <tr>
      <th> Price</th>
      <th>Cost</th>
      <th>Interest income</th>
      <th>Interest receivable</th>
      <th>Interest Paid</th>
      <th>With holding tax</th>
      <th>Outstanding Amount</th>
      <th>Yield</th>
    </tr>
  </thead>
</table>
</div>
@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}

{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{-- 
{{ Html::script(asset_url(). "/investmentDatatableScripts/jqueryDatatable2.js") }}

{{ Html::script(asset_url(). "/investmentDatatableScripts/jqueryDatatable.min.js") }} --}}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}


<script type="text/javascript">
$(function(){
    let fin_id = "{{$fin_id}}";
    let investment_id = "{{$investment_id}}";
    let budget_alocation_id = "{{$budget_alocation_id}}";
    let reference = "{{$reference}}";
    treasuryBillsCalculatedDatatable(fin_id,budget_alocation_id,reference,investment_id);

     function treasuryBillsCalculatedDatatable(fin_id,budget_alocation_id,reference,investment_id){
      $('#calculated_data').DataTable().clear().destroy();

      let calculated_data = $('#calculated_data').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: false,
      paging: false,
      info:true,
      responsive:true,
      order: [[ 0, "desc" ]],
      "columnDefs": [
        {"className": "dt-right", "targets": [0, 1, 2, 3, 4]}
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{!! url('investment/summary/treasury_bills_datatable') !!}/'+investment_id,
        type : 'get'
      },
      columns: [
      { data: 'price' , name: 'price',searchable:false},
      { data: 'cost' , name: 'cost'},
      { data: 'interest_income' , name: 'interest_income'},
      { data: 'interest_receivable' , name: 'interest_receivable',searchable:false},
      { data: 'receivable' , name :'receivable', searchable:false },
      { data: 'with_holding_tax' , name :'with_holding_tax', searchable:false },
      { data: 'outstanding' , name :'outstanding', searchable:false },
      { data: 'yield' , name :'yield', searchable:false },
      ],
      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
       $('td', nRow).click(function() {
        let col = $(this).index()+1;
        let colText = $('#general_data thead th:nth-child('+col+')').text();
        if(colText !='Action'){
          {{-- document.location.href = '{!! url("investment/summary/show") !!}/'+aData['inv_id']+'/'+reference; --}}
        }
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
    }

  });
</script>
@endpush