@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/employer.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}
<style>

    .fixed_deposit_calculated:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.investiment.fixed_deposit_calculated')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_calculated {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .fixed_deposit_create:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.investiment.fixed_deposit_create')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_create {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .fixed_deposit_update:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #F5F5F5;
        content: "_______________";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_update {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .fixed_deposit_update_records:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Updating Records";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_update_records {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .fixed_deposit_update_payment:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Updating Payment After Maturity";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .fixed_deposit_update_payment {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>
@endpush
{{-- {{dd($investment)}} --}}
<!-- Put the page specifically for this page here -->

{!! Form::open(['route' => 'backend.investment.fixed_deposit_store_update', 'name' => 'fixed_deposit_store_update']) !!}
{{-- {!! Form::hidden("source", 1) !!} --}}
@include('backend.investment.investiment_types.includes.fin_years.fin_years')

<div class="row">
    <div class="col-md-12">
        {{-- <div class="fixed_deposit_create"> --}}
            <input type="hidden" name="id" value="{{$investment->id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="fixed_deposit_create">
                        <div class="fileld-layout">
                            <label class="required">Type</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select( 'type', [ 'new issue' => 'new issue'], $investment->type, ['class' => 'form-control search-select other-select type-select selected', 'placeholder' => '', 'style' => 'width:95%;']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required" for="opening_dates">Date of opening</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                         <div class="input-group">
                                            {!! Form::text('opening_date', $investment->opening_date, ['class' => 'form-control to-tenure datepicker', 'style' => "border-radius:3px;", 'id' => 'opening_date','autocomplete'=>'off']) !!}
                                            <span class="input-group-addon">
                                                <i class="icon fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="col-md-5" style="margin-top: -20px;">
                                    <label class="required" for="maturity_dates">Date of maturity</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            {!! Form::text('maturity_date', $investment->maturity_date, ['class' => 'form-control to-tenure datepicker', 'style' => "border-radius:3px;width:100%;", 'id' => 'maturity_date','autocomplete'=>'off']) !!}
                                            <span class="input-group-addon">
                                                <i class="icon fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-md-2" style="margin-top: -17px;">
                                    <label class="required">Tenure</label>
                                    <span>
                                        {!! Form::hidden('tenure') !!}
                                    </span>
                                    <div class="form-group">
                                        <span class="tenure" style="color: blue;"><strong></strong></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{-- </div> --}}
                </div>
                <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="interest_rate">Interest rate</label>
                        <div class="input-group">
                            {!! Form::number('interest_rate', $investment->interest_rate, ['class' => 'form-control number', 'style' => "border-radius:3px;", 'id' => 'interest_rate','max' => '100','min' => '0']) !!}
                            <span class="input-group-addon">
                                <i class="icon fa fa-percent" aria-hidden="true"></i>
                            </span>
                        </div>
                        <span class="help-block">
                            <span></span>
                        </span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="interest_type">Interest Type</label>
                        <div class="input-group">
                            {!! Form::select( 'interest_type', ['Fixed' => 'Fixed', 'Floating' => 'Floating'], $investment->interest_type, ['class' => 'form-control search-select other-select interest_type', 'placeholder' => '', 'style' => 'width:100%;', 'selected' => 'selected']) !!}
                        </div>
                        <span class="help-block">
                            <span></span>
                        </span>
                    </div>
                </div>
                <div class="fileld-layout narration">

                </div>
            </div>
        </div>

        {{-- ************************************************************************************** --}}

        <div class="col-md-6">
            <div class="fixed_deposit_update">
                <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="penalty_rate">Penalty rate</label>
                        <div class="input-group">
                            {!! Form::number('penalty_rate', $investment->penalty_rate, ['class' => 'form-control number', 'style' => "border-radius:3px;", 'id' => 'penalty_rate','max' => '100','min' => '0']) !!}
                            <span class="input-group-addon">
                                <i class="icon fa fa-percent" aria-hidden="true"></i>
                            </span>
                        </div>
                        <span class="help-block">
                            <span></span>
                        </span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label for="amount_invested"></label>
                                <select class="form-control currency" name="currency_id">
                                    @foreach($currencies as $c)
                                    <option value="{{$c->id}}" {{$c->id == $investment->currency_id ? 'selected' : ''}}>{{$c->code}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="required" for="amount_invested">Amount invested (face value)</label>
                                    <div class="input-group">
                                        {!! Form::text('amount_invested', $investment->amount_invested, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'amount_invested']) !!}
                                        <span class="input-group-addon">
                                            <i class="icon fa fa-money" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <span class="help-block">
                                        <span></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fileld-layout exchange-rate">

                </div>
                <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="institution_name">Name of the Institution</label>
                        <div class="input-group">
                            {!! Form::select('employer_id', [$investment->employer_id => $investment->institution_name], null, ['class' => 'form-control employer_id institution-select', 'style' => 'width:100%;', 'id' => 'employer_id']) !!}
                            <input type="text" name="institution_name" hidden>
                        </div>
                        <span class="help-block">
                            <span></span>
                        </span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="tax_rate">Tax rate</label>
                        <div class="input-group">
                            {!! Form::number('tax_rate', $investment->tax_rate, ['class' => 'form-control number', 'style' => "border-radius:3px;", 'id' => 'tax_rate','max' => '100','min' => '0']) !!}
                            <span class="input-group-addon">
                                <i class="icon fa fa-percent" aria-hidden="true"></i>
                            </span>
                        </div>
                        <span class="help-block">
                            <span></span>
                            {{-- <span class="tax_rate" style="margin-top: 1px;color: white;background: red;" role="alert" hidden></span> --}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="receivables">
            <div class="row">
                <div class="col-md-12">
                {{--</div>--}}
                <div class="pull-right">
                    <input type="submit" class="btn btn-success btn-sm btn-submit" value="Update" />
                </div>

            </div>
        </div>

    </div>
</div>
</div>
{!! Form::close() !!}
@include('backend.investment.investiment_types.includes.modals.calculated_fixed_deposit')

@push('after-script-end')

<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{-- {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }} --}}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{-- {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }} --}}
{{-- {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }} --}}
{{ Html::script(asset_url(). "/nextbyte/plugins/moment_precise/js/moment_precise.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}

<script>
    $(function(){
        $(".search-select").select2({});
        $(".search-select2").select2({});
        autosize($("textarea.autosize"));
        $(function () {
            jQuery('.datepicker').datetimepicker({
                timepicker:false,
                format:'Y-m-d',
            });
        });

        $('.narration').hide();

        $(".institution-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: '{!! route('backend.investment.employers') !!}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    data = data.map(function (item) {

                        return {
                            text: item.name,
                            id: item.id,
                            name: item.name,
                        };
                    });
                    return {
                        results:  data
                    };
                },
                cache: true
            }
        });
        
        $('.currency').change(function(e){
            let exchange_rate = ' <div class="form-group"><label class="required" for="exchange_rate">Exchange rate</label><div class="input-group">{!! Form::text('exchange_rate', null, ['class' => 'form-control numbers moneys', 'style' => "border-radius:3px;", 'id' => 'exchange_rate']) !!}<span class="input-group-addon"><i class="icon fa fa-money" aria-hidden="true"></i></span></div><span class="help-block"><span></span></span></div>';

            if ($('.currency option:selected').val() == 1) {
               $('.exchange-rate').fadeOut();
               $('.exchange-rate').empty();
           } else {
            $('.exchange-rate').append(exchange_rate);
            $('.exchange-rate').fadeIn();
        }

    })
        let narration = '<div class="form-group"><label class="required" for="interest_type_description">Narration</label><div class="input-group">{!! Form::text('interest_type_description', $investment->interest_type_description, ['class' => 'form-control', 'style' => "border-radius:3px;", 'id' => 'interest_type_description']) !!}<span class="input-group-addon"><i class="fa fa-align-left" aria-hidden="true"></i></span></div><span class="help-block"><span></span></span></div>';

        $('.interest_type').change(function(e){

            if ($('.interest_type option:selected').val() == 'Fixed') {
             $('.narration').fadeOut();
             $('.narration').empty();
         } else {
            $('.narration').append(narration);
            $('.narration').fadeIn();
        }

    })

        if ($('.interest_type').val() == 'Fixed') {
         $('.narration').fadeOut();
         $('.narration').empty();
     } else {
        $('.narration').append(narration);
        $('.narration').fadeIn();
    }

    if(localStorage.getItem("Status"))
    {

        $.amaran({
            'theme'     :'awesome success',
            'content'   :{
                title : 'Dear {{ucwords(strtolower(access()->user()->firstname))}}',
                message: 'Fixed Deposit successfully saved',
                info:'',
                icon: 'fa fa-check-square-o'
            },
            'position'  :'bottom left',
            'outEffect' :'slideBottom',
            'inEffect'  :'slideLeft'
        });

        localStorage.clear();
    }

    var fin_id = $('#fin_year option:selected').val();

    var $opening_date = $("input[name=opening_date]").val();

    var $maturity_date = $("input[name=maturity_date]").val();
    if (($opening_date != '') && 
        ($maturity_date != '')) {
        var a = moment($opening_date);
    var b = moment($maturity_date);
    var days = b.diff(a,'days') 
    var months = b.diff(a,'months')
    var tenure =  days+'-days('+months+'-months)';
    $(".tenure").text(tenure);
    $("input[name=tenure]").val(tenure);
} else {
    $(".tenure").text(0);
}

$('.to-tenure').on('change', function (e) {
    var $opening_date = $("input[name=opening_date]").val();

    var $maturity_date = $("input[name=maturity_date]").val();
    if (($opening_date != '') && 
        ($maturity_date != '')) {
        var a = moment($opening_date);
    var b = moment($maturity_date);
    var days = b.diff(a,'days') 
    var months = b.diff(a,'months')
    var tenure =  days+'-days('+months+'-months)';
    $(".tenure").text(tenure);
    $("input[name=tenure]").val(tenure);
} else {
    $(".tenure").text(0);
}

});



/* start : ensure only numbers are input on monetary boxes */
$(".number").keydown(function (e) {
    number_only(e)
});
/* end : ensure only numbers are input on monetary boxes */
/* start : mask all money input */
$('.money').maskMoney({
    precision : 4,
    affixesStay : false
});

$(".numbers").keydown(function (e) {
    number_only(e)
});
/* end : ensure only numbers are input on monetary boxes */
/* start : mask all money input */
$('.moneys').maskMoney({
    precision : 4,
    affixesStay : false
});

/* start: Submitting Form and perform validation on the server side */
$('body').on('submit', 'form[name=fixed_deposit_store_update]', function (e) {
    e.preventDefault();
    var form = this;
    var $opening_date = $("input[name=opening_date]").val();
    var $maturity_date = $("input[name=maturity_date]").val();

    $("input[name=institution_name]").val($("select[name=employer_id]").text());


    $(".money").each(function() {
        var value = $(this).val().replace(/,/g, '')
        $(this).val(value)

    })
    var fin_year_id = $(".search-select2 option:selected").val();

    var $data = $(form).serializeArray();
    $data.push({name: "fin_year_id", value: fin_year_id});
    console.log($data);
    /* start: remove any printed error message in the input controls */
    $(form).find(':input').each(function () {
        var $name = $(this).attr('name');
        $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
        $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
    });
    $("#fin_year").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
    /* end: remove any printed error message in the input controls */
    $.ajax({
        data : $data,
        dataType : "json",
        method : "POST",
        url : $(form).attr("action"),
        beforeSend : function (e) {
            $(".btn-submit").prop('disabled', true);
        },
        success : function (data) {
            console.log(data)
            if (data.success) {

                $.when(
                    $.amaran({
                        'theme'     :'awesome success',
                        'content'   :{
                            title : 'Dear {{ucwords(strtolower(access()->user()->firstname))}}',
                            message: 'Fixed Deposit successfully Updated',
                            info:'',
                            icon: 'fa fa-check-square-o'
                        },
                        'position'  :'bottom left',
                        'outEffect' :'slideBottom',
                        'inEffect'  :'slideLeft'
                    })
                    ).then(function(){
                        setTimeout(function(){
                         document.location.href = '{!! url("investment/summary/investment_type") !!}/'+data.success.fin_year_id+'/'+data.success.allocation_id+'/'+data.success.reference; 
                     }, 2000)
                    })

                }else if(data.errors){
                    $.each(data.errors, function(index, value) {
                        swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
                        $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });

                }
                else if(data.error){
                    console.log(data.error)
                    $.amaran({
                        'theme'     :'awesome warning',
                        'content'   :{
                            title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
                            message: data.error,
                            info:'',
                            icon: 'fa fa-times-circle-o'
                        },
                        'position'  :'bottom left',
                        'outEffect' :'slideBottom',
                        'inEffect'  :'slideLeft'
                    });

                }
            },
        }).done(function() {

        }).fail(function() {

        }).always(function() {
            $(".btn-submit").prop('disabled', false);
        });
    });
/* end: Submitting Form and perfom validation on the server side */
/* start : Payment type change event */
$('input[type=radio][name=payment_type_id]').change(function() {
    if (this.value == 2 || this.value == 5) {
        $("#chequeno_entry").show();
    } else {
        $("#chequeno_entry").hide();
    }
});
/* end : Payment type change event */
});

/* start : ensure only numbers are input on monetary boxes */
function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
        return;
    }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    /* end : ensure only numbers are input on monetary boxes */

</script>
@endpush