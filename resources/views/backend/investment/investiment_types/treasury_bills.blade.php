@extends('layouts.backend.main', ['title' => trans('labels.backend.investiment.treasury_bill_index'), 'header_title' => trans('labels.backend.investiment.treasury_bill_index')])

{{--@include('backend.includes.toastr_assets')--}}

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/employer.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}
<style>
    .payment_mode:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.finance.receipt.mode')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .payment_mode {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .treasury_bill_calculated:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.investiment.treasury_bill_calculated')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .treasury_bill_calculated {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .treasury_bill_create:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.investiment.treasury_bill_create')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .treasury_bill_create {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .treasury_bill_update:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #F5F5F5;
        content: "@lang('labels.backend.investiment.treasury_bill_update')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .treasury_bill_update {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>
@endpush

@section('content')
@include('backend.investment.investiment_types.includes.fin_years.fin_years')
<!-- Put the page specifically for this page here -->

{!! Form::open(['route' => 'backend.investment.treasury_bills', 'name' => 'treasury_bills']) !!}
{{-- {!! Form::hidden("source", 1) !!} --}}
<div class="row">
    <div class="col-md-12">
        {{-- <div class="treasury_bill_create"> --}}
            <div class="row">
                <div class="col-md-6">
                    <div class="treasury_bill_create">
                        <div class="fileld-layout">
                            <label class="required">Source</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::select( 'source', [ '1' => 'Primary Market'], null, ['class' => 'form-control search-select other-select source-select', 'placeholder' => '', 'style' => 'width:95%;']) !!}
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <div class="form-group">
                                <label class="required" for="amount_invested">Amount invested (face value)</label>
                                <div class="input-group">
                                    {!! Form::text('amount_invested', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'amount_invested']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label class="required">Settlement Date</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                           <div class="input-group">
                                            {!! Form::text('settlement_date', null, ['class' => 'form-control to-tenure datepicker', 'style' => "border-radius:3px;width:100%;", 'id' => 'settlement_date','autocomplete'=>'off']) !!}
                                            <span class="input-group-addon">
                                                <i class="icon fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="col-md-5" style="margin-top: -20px;">
                                    <label class="required">Maturity Date</label>
                                    <div class="form-group">
                                     <div class="input-group">
                                        {!! Form::text('maturity_date', null, ['class' => 'form-control to-tenure datepicker', 'style' => "border-radius:3px;width:100%;", 'id' => 'maturity_date','autocomplete'=>'off']) !!}
                                        <span class="input-group-addon">
                                            <i class="icon fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: -17px;">
                                <label class="required">Count</label>
                                <span>
                                    {!! Form::hidden('tenure') !!}
                                </span>
                                <div class="form-group">
                                    <span class="tenure" style="color: blue;"><strong></strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label class="required">Auction date</label>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('auction_date', null, ['class' => 'form-control to-tenure datepicker', 'style' => "border-radius:3px;width:100%;", 'id' => 'auction_date','autocomplete'=>'off']) !!}
                                        <span class="input-group-addon">
                                            <i class="icon fa fa-calendar" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="col-md-7" style="margin-top: -20px;">
                                <label class="required">Auction Number</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('auction_number', null, ['class' => 'form-control ', 'style' => "border-radius:3px;", 'id' => 'auction_number']) !!}
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- ************************************************************************************** --}}

        <div class="col-md-6">
            <div class="treasury_bill_update">
                <div class="fileld-layout bond-interchange">

                </div>
                <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="yield">Tenure</label>
                        <div class="input-group">
                            {!! Form::number('tenure', null, ['class' => 'form-control ', 'style' => "border-radius:3px;", 'id' => 'tenure','min' => '0']) !!}
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                            </span>
                        </div>
                        <span class="help-block">
                            <span></span>
                        </span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="tax_rate">Tax rate</label>
                        <div class="input-group">
                            {!! Form::number('tax_rate', null, ['class' => 'form-control number', 'style' => "border-radius:3px;", 'id' => 'tax_rate','max' => '100','min' => '0']) !!}
                            <span class="input-group-addon">
                                <i class="icon fa fa-percent" aria-hidden="true"></i>
                            </span>
                        </div>
                        <span class="help-block">
                            <span></span>
                        </span>
                    </div>
                </div>
                  <div class="fileld-layout">
                    <div class="form-group">
                        <label class="required" for="tax_rate">Holding number</label>
                        <div class="input-group">
                            {!! Form::text('holding_number', null, ['class' => 'form-control', 'style' => "border-radius:3px;", 'id' => 'holding_number']) !!}
                            <span class="input-group-addon">
                                <i class="fa fa-address-card" aria-hidden="true"></i>
                            </span>
                        </div>
                        <span class="help-block">
                            <span></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="receivables">
                <div class="row">
                    <div class="col-md-12">
                    {{--</div>--}}
                    <hr/>
                    <div class="pull-right">
                        <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.save')" />
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection

@push('after-script-end')

<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/moment_precise/js/moment_precise.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}


<script>
    $(function(){
        $('.treasury_bill_calculated').hide();

        $('#treasury_bill_button').click(function(e){
            $( ".treasury_bill_calculated" ).toggle('slow',function(){
                console.log('test')
            });
        })

        $('.source-select').change(function(e){
            console.log($('.source-select option:selected').val())

            let Price = '<div class="form-group"><label class="required" for="price">Price</label><div class="input-group">{!! Form::text('price', null, ['class' => 'form-control number money price', 'style' => "border-radius:3px;", 'id' => 'price']) !!}<span class="input-group-addon"><i class="icon fa fa-money" aria-hidden="true"></i></span></div><span class="help-block"><span></span></span></div>';

            let Yield = '<div class="form-group"><label class="required" for="yield">Yield</label><div class="input-group">{!! Form::number('yield', null, ['class' => 'form-control yield','step' => '0.0001','max' => '100','min' => '0','id' => 'yield']) !!}<span class="input-group-addon"><i class="icon fa fa-percent" aria-hidden="true"></i></span></div><span class="help-block"><span></span></span></div>';


            if ($('.source-select option:selected').val() == 1) {
                $('.bond-interchange').empty();
                $('.bond-interchange').append(Price);
                $('.bond-interchange').find(".number").keydown(function (e) { number_only(e)});
                $('.bond-interchange').find('.money').maskMoney({
                    precision : 4,
                    affixesStay : false
                });
                $('.bond-interchange').fadeIn();

            } else if($('.source-select option:selected').val() == 2) {
                $('.bond-interchange').empty();
                $('.bond-interchange').append(Yield);
                $('.bond-interchange').fadeIn();

            }else{
                $('.bond-interchange').fadeOut();
                $('.bond-interchange').empty();


            }

            
            
        })

        $(function () {
            jQuery('.datepicker').datetimepicker({
                timepicker:false,
                format:'Y-m-d',
            });
        });

        if(localStorage.getItem("Status"))
        {

            $.amaran({
                'theme'     :'awesome success',
                'content'   :{
                    title : 'Dear {{ucwords(strtolower(access()->user()->firstname))}}',
                    message: 'Tresury Bond successfully saved',
                    info:'',
                    icon: 'fa fa-check-square-o'
                },
                'position'  :'bottom left',
                'outEffect' :'slideBottom',
                'inEffect'  :'slideLeft'
            });

            localStorage.clear();
        }

        
        $(".search-select").select2({});
        $(".search-select2").select2({});
        autosize($("textarea.autosize"));
        var fin_id = $('#fin_year option:selected').val();
        // console.log(fin_id)
        $('.to-tenure').on('change', function (e) {
            var $settlement_date = $("input[name=settlement_date]").val();

            var $maturity_date = $("input[name=maturity_date]").val();
            if (($settlement_date != '') && 
                ($maturity_date != '')) {
                var a = moment($settlement_date);
            var b = moment($maturity_date);
            var days = b.diff(a,'days') 
            var months = b.diff(a,'months')
            var years = b.diff(a,'years')
            var tenure =  days+'-days('+years+'-years)';
            $(".tenure").text(tenure);
            $("input[name=tenure]").val(tenure);
        } else {
            $(".tenure").text(0);
        }

    });
        
        /* start : ensure only numbers are input on monetary boxes */
        $(".number").keydown(function (e) {
            number_only(e)
        });
        /* end : ensure only numbers are input on monetary boxes */
        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 4,
            affixesStay : false
        });
        /* end : mask all money input */
        /* start: Submitting Form and perform validation on the server side */
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form[name=treasury_bills]', function (e) {
            e.preventDefault();
            var form = this;
            var $settlement_date = $("select[name=settlement_date]").val();
            var $maturity_date = $("select[name=maturity_date]").val();
            var $auction_date = $("select[name=auction_date]").val();
            var $coupon_date = $("select[name=coupon_date]").val();

            $(".money").each(function() {
                var value = $(this).val().replace(/,/g, '')
                $(this).val(value)

            })
            var fin_year_id = $(".search-select2 option:selected").val();


            // console.log(fin_year_id)

            var $data = $(form).serializeArray();
            $data.push({name: "fin_year_id", value: fin_year_id});
            console.log($data);
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            $("#fin_year").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            /* end: remove any printed error message in the input controls */
            $.ajax({
                data : $data,
                dataType : "json",
                method : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    $(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    if (data.success) {

                        $.when(
                           $.amaran({
                            'theme'     :'awesome success',
                            'content'   :{
                                title : 'Dear {{ucwords(strtolower(access()->user()->firstname))}}',
                                message: 'Treasury Bills successfully saved',
                                info:'',
                                icon: 'fa fa-check-square-o'
                            },
                            'position'  :'bottom left',
                            'outEffect' :'slideBottom',
                            'inEffect'  :'slideLeft'
                        })
                           ).then(function(){
                            setTimeout(function(){
                               document.location.href = '{!! url("investment/summary/investment_type") !!}/'+data.success.fin_year_id+'/'+data.success.allocation_id+'/'+data.success.reference; 
                           }, 2000)
                        })
                       }else if(data.errors){
                        $.each(data.errors, function(index, value) {
                            swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
                            $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        });

                    }
                    else if(data.error){
                        console.log(data.error)
                        $.amaran({
                            'theme'     :'awesome warning',
                            'content'   :{
                                title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
                                message: data.error,
                                info:'',
                                icon: 'fa fa-times-circle-o'
                            },
                            'position'  :'bottom left',
                            'outEffect' :'slideBottom',
                            'inEffect'  :'slideLeft'
                        });

                    }
                },
            }).done(function() {

            }).fail(function() {

            }).always(function() {
                $(".btn-submit").prop('disabled', false);
            });
        });
/* end: Submitting Form and perfom validation on the server side */
/* start : Payment type change event */
$('input[type=radio][name=payment_type_id]').change(function() {
    if (this.value == 2 || this.value == 5) {
        $("#chequeno_entry").show();
    } else {
        $("#chequeno_entry").hide();
    }
});
/* end : Payment type change event */
});

/* start : ensure only numbers are input on monetary boxes */
function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
        return;
    }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    /* end : ensure only numbers are input on monetary boxes */

</script>
@endpush