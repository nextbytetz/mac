@push('after-styles-end')
<style>

    .bg-white {
        background: white;
    }

</style>
@endpush

<div class="modal fade "  tabindex="-1" id="calculated_fixed_deposit_modal" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-white" style=" background: white;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="calculated_fixed_deposit_modal">Calculated Fixed Deposit</h4>
            </div>
            <div class="modal-body">
                <div class="fixed_deposit_calculatesd">
                        <div class="fileld-layout">
                            <div class="row">
                            <div class="col-md-12">
                            <div class="col-md-6">
                             <div class="form-group">
                                <label class="required" for="interest_receivable">Interest Receivable</label>
                                <div class="input-group">
                                    {!! Form::text('interest_receivable', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'interest_receivable']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="required" for="interest_income">Interest income</label>
                                <div class="input-group">
                                    {!! Form::text('interest_income', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'interest_income']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fileld-layout">
                    <div class="row">
                            <div class="col-md-12">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label class="required" for="weighted_average_return">FDR weighted average return</label>
                                <div class="input-group">
                                    {!! Form::text('weighted_average_return', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'weighted_average_return']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                                <label class="required" for="penalty_income">Penalty income (if any)</label>
                                <div class="input-group">
                                    {!! Form::text('penalty_income', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'penalty_income']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="row">
                            <div class="col-md-12">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label class="required" for="penalty_receivable">Penalty receivable</label>
                                <div class="input-group">
                                    {!! Form::text('penalty_receivable', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'penalty_receivable']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                           <label class="required">Outstanding/Overpaid interest</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('overpaid_interest', null, ['class' => 'form-control number money', 'style' => "border-radius:3px;", 'id' => 'overpaid_interest']) !!}
                                    <span class="input-group-addon">
                                        <i class="icon fa fa-money" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <span></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
                          
        </div>
    </div>
</div>
</div>
</div>
</div>

@push('after-script-end')
    <script>
        $(document).ready(function(e){
            $('#fixed_deposit_button').click(function(e){
                $('#calculated_fixed_deposit_modal').modal('show')
            })
        })
        // $(function() {
        //     $('.search-select-hsp').select2({
        //         dropdownParent: $('#calculated_fixed_deposit_modal')
        //     });
        //     $('#region_id_hsp').on('change', function (e) {
        //         var region_id = e.target.value;
        //         if (region_id) {
        //             $(".spin2").show();
        //             $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
        //                 $('#district_id_hsp').empty();
        //                 $("#district_id_hsp").select2("val", "");
        //                 $('#district_id_hsp').html(data);
        //                 $(".spin2").hide();
        //             });
        //         }
        //     });
            // $('body').off('submit', 'form[name=create_hsp]').on('submit', 'form[name=create_hsp]', function(e) {
            //     e.preventDefault();
            //     var form = this;
            //     var $data = $(form).serialize();
            //     /* start: remove any printed error message in the input controls */
            //     $(form).find(':input').each(function () {
            //         var $name = $(this).attr('name');
            //         $("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            //         $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            //         $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            //     });
            //     /* end: remove any printed error message in the input controls */
            //     $.ajax({
            //         data : $data,
            //         dataType : "json",
            //         method : "POST",
            //         url : $(form).attr("action"),
            //         beforeSend : function (e) {
            //             $(".site-btn").prop('disabled', true);
            //         },
            //         success : function (data) {
            //             $("#calculated_fixed_deposit_modal").modal('hide');
            //             if (data.success) {
            //                 $.amaran({
            //                     'theme'     :'awesome success',
            //                     'content'   :{
            //                         title : "Success",
            //                         message: data.message,
            //                         info:'',
            //                         icon: 'fa fa-check-square-o',
            //                     },
            //                     'position'  :'bottom left',
            //                     'outEffect' :'slideBottom',
            //                     'inEffect'  :'slideLeft'
            //                 });
            //             } else {

            //             }
            //         },
            //         error: function (data) {
            //             var errors = $.parseJSON(data.responseText);
            //             /* console.log(data); */
            //             $.each(errors, function(index, value) {
            //                 $("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
            //                 $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
            //                 $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
            //             });
            //         },
            //     }).done(function() {

            //     }).fail(function() {

            //     }).always(function() {
            //         $(".site-btn").prop('disabled', false);
            //     });
            // });
        // });
    </script>
@endpush