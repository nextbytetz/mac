    
@if(!isset($fin_id))
<div class="d-inline-block pull-right">
    <div class="form-group">
        <select id="fin_year" type="select" name="fin_year_id" class="custom-select search-select2">
            <option value="" >Select Financial Year..</option>
            @foreach($fin_years as $fy)
            <option value="{{$fy->id}}">{{$fy->name}}</option>
            @endforeach
        </select>
        <span class="help-block"></span>
    </div>
</div>

@else
<div class="d-inline-block pull-right">
    <div class="form-group">
        <select id="fin_year" type="select" name="fin_year_id" class="custom-select search-select2">
            <option value="" >Select Financial Year..</option>
            @foreach($fin_years as $fy)
            <option value="{{$fy->id}}" {{$fy->id == $fin_id ? 'selected' : ''}}>{{$fy->name}}</option>
            @endforeach
        </select>
        <span class="help-block"></span>
    </div>
</div>
@endif