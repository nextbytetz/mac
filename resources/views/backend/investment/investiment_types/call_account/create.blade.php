@extends('layouts.backend.main', ['title' => 'New Call Account', 'head_title' =>''])
@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}

<style>
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
</style>
@endpush
@section('content')

{!! Form::open(['route' => 'backend.investment.call_account.save', 'name' => 'investment_call_account_form', 'id'=>'call_account_form']) !!}
<div class="row mb-2">
	<h5 class="pl-2 pb-1">Add New Call Account</h5>
	<span class="help-block pl-2"><strong>Note:</strong> All fields marked <span class="required"></span> must be filled</span></span>
	<div class="col-md-12">
		<legend></legend>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1" >Balance amount</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('balance_amount', null, ['class' => 'form-control money','id' => 'balance_amount']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-money" aria-hidden="true"></i>
						</span>
					</div>
					<span class="balance_amount_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1">Interest rate</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('interest_rate', null, ['class' => 'form-control','id' => 'interest_rate']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-percent" aria-hidden="true"></i>
						</span>
					</div>
					<span class="interest_rate_error error_field hidden mt-1 text-danger">
					</span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1" >Tax rate</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('tax_rate', null, ['class' => 'form-control','id' => 'tax_rate']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-percent" aria-hidden="true"></i>
						</span>
					</div>
					<span class="tax_rate_error error_field hidden mt-1 text-danger">
					</span>
				</div>
			</div>
		</div>
		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1" >Received interest</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('received_interest', null, ['class' => 'form-control money','id' => 'received_interest']) !!}
						<span class="input-group-addon" >	
							<i class="icon fa fa-money" aria-hidden="true"></i>
						</span>
					</div>
					<span class="received_interest_error error_field hidden mt-1 text-danger">
						
					</span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="pb-1" >Interest income</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('interest_income', null, ['class' => 'form-control money','id' => 'interest_income']) !!}
						<span class="input-group-addon" >	
							<i class="icon fa fa-money" aria-hidden="true"></i>
						</span>
					</div>
					<span class="interest_income_error error_field hidden mt-1 text-danger">
					</span>
				</div>
			</div>
			<div class="col-sm-4 ">
				<label class="required pb-1" >Interest receivable</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('interest_receivable', null, ['class' => 'form-control money','id' => 'interest_receivable']) !!}
						<span class="input-group-addon" >	
							<i class="icon fa fa-money" aria-hidden="true"></i>
						</span>
					</div>
					<span class="interest_receivable_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>
		<div class="row pb-2">
			<div class="col-sm-4 ">
				<label class="required pb-1" >Balance Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('balance_date', null, ['class' => 'form-control date_picker','id' => 'date_picker']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="balance_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a type="button" href="{!! route('backend.investment.menu') !!}" class="btn btn-lg btn-secondary" id="" data-dismiss="modal">Cancel</a>
					<input type="submit" class="btn btn-success btn-lg btn-submit" id="call_account_submit_button" value="@lang('buttons.general.save')" />
				</div>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}

@stop

@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/moment_precise/js/moment_precise.js") }}

<script  type="text/javascript">

	$(function () {

		$('.money').maskMoney({
			precision : 4,
			affixesStay : true
		});

	
		$('.date_picker').datetimepicker({
			timepicker:false,
			format:'d-m-Y',
		});
		
		categoryFields();
		// totalShareValues();

		$('#category').on('change', function(e) {
			categoryFields();
		});

		function categoryFields() {
			let cat = $('#category option:selected').attr('id');
			if (cat == 'listed') {
				$('.listed').removeClass('hidden');
			} else {
				$('.listed').addClass('hidden');
			}
		}
	});
	$('body').on('submit', 'form[name=investment_call_account_form]', function (e) {
		// e.preventDefault();
		// console.log($data);
		// var form = this;
		// var $balance_date = $("select[name=balance_date]").val();

		$(".money").each(function() {
			var value = $(this).val().replace(/,/g, '')
			$(this).val(value)

		});
	});
		$('#call_account_submit_buttons').on('click', function(e) {
			e.preventDefault();
			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');

			let data_to_save = form.serializeArray();


			let options = {
				dataType : 'json',
				url: '{{route('backend.investment.call_account.save')}}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: ' Call Account Added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						form.trigger("reset");
						setTimeout(function(){
							window.location.href = "{!! url('investment/call_account/show/') !!}/"+scheme_id; 
						}, 1500);

					} 
					else {
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							swal({
								title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								text: '<h6>Something went wrong! Kindly try again</h6>',
								type: 'error',
								showCancelButton: false,
								cancelButtonText: 'Cancel',
								confirmButtonColor: '#75E0A2',
								confirmButtonText: 'OK',
								html: true,
								closeOnConfirm: true
							}, function (isConfirmed) {
								location.reload();
							});
						}
					}
				},
				error: function (data) {
				}
			};
		});
	</script>
	@endpush