{{-- {{dd($id)}} --}}
@extends('layouts.backend.main', ['title' => 'Call Account', 'header_title' =>' Call Account'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
      }
</style>
@endpush

@section('content')

<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="col-md-9">
				<div class="pull-right mb-1">
					{{-- {{ route('backend.investment.call_account.create') }} --}}
					<a href="{{ route('backend.investment.call_account.create') }}" class="btn btn-primary btn-md"  role="button">Add New Call Account</a>
					{{-- <a href="{{ route('backend.investment.call_account.show') }}" class="btn btn-primary btn-md"  role="button">Update</a> --}}
				</div>
				<table class="display table table-bordered" cellspacing="0" width="100%" id ="call_account_datatable">
					<thead>
						<tr>
							<th>Balance amount</th>
							<th>Interest rate</th>
							<th>Tax rate</th>
							<th>Received interest</th>
							<th>Balance Date</th>
							<th>Interest income</th>
							<th>Interest receivable</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="grey_modal">
						<table style="width:100%">
							<tr>
								<td  align="center"><h5><strong><span class="light_dark_color">	Summary</span></strong></h5></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="light_grey_bg">&nbsp;</div>
					<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
						<p class="underline ">
							<span style="font-weight: lighter;">Period :</span> 
							<span style="font-weight: bold;" > 
								{{!empty($calls->investmentCallAccount->fin_year) ? $calls->investmentCallAccount->fin_year->name : ''}}
							</span>
						</p>
						<p class="underline pt-1">
							<span style="font-weight: lighter;">Balance Amount :</span> 
							<span style="font-weight: bold;" > 
								{{-- {{!empty($loan_budget->amount) ? number_format($loan_budget->amount,2) : ''}} --}}
							</span>
						</p>
						<p class="underline  pt-1">
							<span style="font-weight: lighter;">Received Interest :</span> 
							{{-- @php
							$total_loaned = 0;
							foreach($loan_budget->investmentLoan as $loaned){
								// dd($loaned);
								$total_loaned += $loaned->loan_amount;
							}
							@endphp --}}
							<span style="font-weight: bold;" >{{number_format($received_interest,2)}}</span>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="light_grey_bg">&nbsp;</div>
					<div class="col-md-12 light_grey_bg" style="min-height: 40px;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@php
// if (!empty($url)) {
	$url = url('investment/call_account/datatable/');
//} 
// else {
// 	$url = url('investment/call_account/datatable/'.$id);
// }
@endphp


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/moment_precise/js/moment_precise.js") }}

<script  type="text/javascript">

	$(function () {
		 // $('.money').each(function () {
   //      this.value = this.value.replace(/(?!^)-/g, '').replace(/^[.]/, '').replace(/^-[.]/, '-');
   //  });

		$('.money').maskMoney({
           precision : 4,
           affixesStay : true
		});

		let datatable = $('#call_account_datatable').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			order: [[ 0, "desc" ]],
			columnDefs: [
			{ className: "dt-right", "targets": [2,3] },
			],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{{$url}}',
				type : 'get'
			},
			columns: [
			{ data: 'balance_amount' , name: 'balance_amount'},
			{ data: 'interest_rate' , name: 'interest_rate'},
			{ data: 'tax_rate' , name: 'tax_rate'},
			{ data: 'received_interest' , name: 'received_interest'},
			{ data: 'balance_date' , name: 'balance_date'},
			{ data: 'interest_income' , name: 'interest_income'},
			{ data: 'interest_receivable' , name: 'interest_receivable'},
			
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
					document.location.href = '{!! url('investment/call_account/show/') !!}/'+aData['id'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});
	});

</script>;

@endpush
