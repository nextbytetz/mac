@extends('layouts.backend.main', ['title' => 'Loans', 'header_title' =>' Loans'])
@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}

<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
	.xdsoft_datetimepicker {z-index:999999999 !important;}
</style>
@endpush

@section('content')

<div class = "row">
	<div class="col-md-12 col-sm-12">
		<h5 class="client-title">
			<i class="icon-circle"></i>
			<strong> <label for="name" id="name">Loanee: {{ucwords(strtolower($loan->loanee_name))}}</label> </strong>
			{{-- <small>
				Reg #: <label for="reg_no" id="reg_no">2005-9-028484</label>
			</small> --}}
		</h5>
		<legend></legend>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-9">
				<div class="pull-left pb-1 pt-2">
					<a href="#" class="btn btn-success btn-md " id="add_repayment_btn" role="button">Add Repayment</a>
				</div>
				<table class="display" cellspacing="0" width="100%" id ="loans_datatable">
					<thead>
						<tr>
							<th>Received Repayment</th>
							<th>Received Repayment Date</th>
							<th>Interest Received</th>
							<th>Interest Received Date</th>
							{{-- <th></th> --}}
						</tr>
					</thead>
				</table>
			</div>
			<div class="col-md-3 pt-2">
				<div class="row">
					<div class="grey_modal">
						<table style="width:100%">
							<tr>
								<td  align="center"><h5><strong><span class="light_dark_color">	Summary</span></strong></h5></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="light_grey_bg">&nbsp;</div>
					<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
						<p class="underline ">
							<span style="font-weight: lighter;">Loan Type :</span> 
							<span style="font-weight: bold;" > 
								{{!empty($loan->loan_type_name) ? $loan->loan_type_name : ''}}
							</span>
						</p>
						<p class="underline pt-1">
							<span style="font-weight: lighter;">Loan Amount :</span> 
							<span style="font-weight: bold;" > 
								{{!empty($loan->loan_amount) ? number_format($loan->loan_amount,2) : ''}}
							</span>
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>


</div>

<div id="add_repayment_modal" class="modal fade" role="dialog" data-focus="true">
	<div class="modal-dialog modal-lg" role="document">
		<form role="form" id="add_repayment_form">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Repayment</h5>
				</div>
				<div class="modal-body">
					<div class="row pb-2">
						<div class="col-sm-6">
							<label class="required">Received Repayment</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="received_repayment" class='form-control money' id="received_repayment">
								</div>
								<span class="received_repayment_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label class="required">Received Repayment Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="received_repayment_date" class='form-control date_picker' id="received_repayment_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="received_repayment_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label>Interest Received</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="interest_received" class='form-control money' id="interest_received">
								</div>
								<span class="interest_received_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label >Interest Received Date</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="interest_received_date" class='form-control date_picker' id="interest_received_date">
									<span class="input-group-addon">
										<i class="icon fa fa-calendar" aria-hidden="true"></i>
									</span>
								</div>
								<span class="interest_received_date_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>

					<div class="row pb-2">
						<div class="col-sm-6">
							<label>Amount disbursed</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="amount_disbursed" class='form-control money' id="amount_disbursed">
								</div>
								<span class="amount_disbursed_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<label>Principle Redeemed</label>
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="principal_redeemed" class='form-control money' id="principal_redeemed">
								</div>
								<span class="principal_redeemed_error error_field hidden mt-1 text-danger"></span>
							</div>
						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="add_repayment_submit">Save changes</button>
					<button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>


@stop




@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}


<script  type="text/javascript">

	$(function () {

		let loans_datatable = $('#loans_datatable').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			order: [[ 0, "desc" ]],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/loan/repayment_datatable/'.$loan->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'received_repayment' , name: 'investment_loan_repayments.received_repayment'},
			{ data: 'received_repayment_date' , name: 'investment_loan_repayments.received_repayment_date'},
			{ data: 'interest_received' , name: 'investment_loan_repayments.interest_received'},
			{ data: 'interest_received_date' , name: 'investment_loan_repayments.interest_received_date'},
			// { data: 'action' , name :'action', searchable:false },
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
					// document.location.href = '{!! url('investment/loan/show/') !!}/'+aData['id'];
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});
	});

	$('.money').maskMoney({
		precision : 2,
		affixesStay : false
	});

	$('.date_picker').datetimepicker({
		timepicker:false,
		format:'d-m-Y',
	});

	$('#add_repayment_btn').on('click', function(e) {
		e.preventDefault();
		$('.error_field').addClass('hidden');
		$('#add_repayment_form').trigger('reset');
		$('#add_repayment_modal').modal('show');
	});


	$('#add_repayment_submit').on('click', function(e) {
		e.preventDefault();
		$('.error_field').addClass('hidden');
		$('.custom_error').removeClass('custom_error');
		let form = $('#add_repayment_form');
		
		let received_repayment =  $('#received_repayment').val().replace(/,/g, "");
		let interest_received =  $('#interest_received').val().replace(/,/g, "");
		let amount_disbursed =  $('#amount_disbursed').val().replace(/,/g, "");
		let principal_redeemed =  $('#principal_redeemed').val().replace(/,/g, "");
		
		let data_to_save = form.serializeArray();
		data_to_save.push({name: 'investment_loan_id',value: {{$loan->id}} });
		data_to_save.push({name: 'received_repayment',value: received_repayment });
		data_to_save.push({name: 'interest_received',value: interest_received });
		data_to_save.push({name: 'amount_disbursed',value: amount_disbursed });
		data_to_save.push({name: 'principal_redeemed',value: principal_redeemed });


		let options = {
			dataType : 'json',
			url: '{{route('backend.investment.loan.save_repayment')}}',
			data : data_to_save,
			type : 'POST',
			success : function (data) {
				if (data.success) {
					$('#add_repayment_modal').modal('hide');
					form.trigger("reset");
					$.amaran({
						'theme'     :'awesome success',
						'content'   :{
							title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
							message: 'Loan Added successfully',
							info:'',
							icon: 'fa fa-check-square-o'
						},
						'position'  :'bottom left',
						'outEffect' :'slideBottom',
						'inEffect'  :'slideLeft'
					});
					
					setTimeout(function(){location.reload(); }, 2000);
				} 
				else {
					if (data.errors) {
						let m = Object.keys(data.errors);
						$.each(m, function( index, value ) {
							$('.'+value+'_error').removeClass('hidden');
							$('.'+value+'_error').text(data.errors[value][0]);
							$('#'+value).addClass('custom_error');
						});
					}else{
						location.reload();
					}
				}
			},
			error: function (data) {
				// location.reload();
			}
		};
		$(form).ajaxSubmit(options);
	});

</script>;

@endpush
