@extends('layouts.backend.main', ['title' => 'New Loan', 'header_title' =>'New Loan'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}

<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
	.custom_error {
		position: relative;
		border: 1px solid red;
	}
</style>
@endpush
@section('content')

{!! Form::open(['route' => 'backend.investment.loan.save', 'name' => 'investment_loan', 'id'=>'loan_form']) !!}
<div class="row">
	<div class="col-md-12">
		<div class="row pb-2">
			<div class="col-sm-3">
				<label class="required">Financial Year</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::select( 'fin_year',$fin_years, null, ['class' => 'form-control', 'placeholder' => '', 'id'=>'fin_year']) !!}
					</div>
					<span class="fin_year_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-3">
				<label >Total Budget</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('total_budget', null, ['class' => 'form-control','id' => 'total_budget', 'disabled'=>'disabled','id'=>'total_budget']) !!}
					</div>
					<span class="total_budget_error error_field hidden mt-1 text-danger"></span>

				</div>
			</div>
			<div class="col-sm-4">
				<label >Total Available Budget</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('available_budget', null, ['class' => 'form-control','id' => 'available_budget', 'disabled'=>'disabled']) !!}
					</div>
					<span class="available_budget_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row pb-2">
			<div class="col-sm-3">
				<label class="required">Loan type</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::select( 'loan_type', [1=>'Government', 2 => 'Cooperate', 3 => 'Cooperative Union'], null, ['class' => 'form-control', 'id' => 'loan_type']) !!}
					</div>
					<span class="loan_type_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-3">
				<label class="required">Loan Amount</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('loan_amount', null, ['class' => 'form-control money','id' => 'loan_amount']) !!}
					</div>
					<span class="loan_amount_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4">
				<label class="required">Loanee</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('loanee', null, ['class' => 'form-control','id' => 'loanee']) !!}
					</div>
					<span class="loanee_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row pb-2">
			<div class="col-sm-3">
				<label class="required">Disbursment Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('disbursement_date', null, ['class' => 'form-control date_picker', 'id' => 'disbursement_date']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="disbursement_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-3">
				<label class="required">Maturity Date</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('maturity_date', null, ['class' => 'form-control mt_datepicker1', 'id' => 'maturity_date']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-calendar" aria-hidden="true"></i>
						</span>
					</div>
					<span class="maturity_date_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-4">
				<label class="required">Tenure</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::text('tenure', null, ['class' => 'form-control','id' => 'tenure', 'disabled'=>'disabled']) !!}
					</div>
					<span class="help-block">Difference in months between Disbursment and Maturity date</span>
				</div>
			</div>
		</div>


		<div class="row">

			<div class="col-sm-3">
				<label class="required" for="interest_rate">Interest rate</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::number('interest_rate', null, ['class' => 'form-control number', 'id' => 'interest_rate','max' => '100','min' => '0']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-percent" aria-hidden="true"></i>
						</span>
					</div>
					<span class="interest_rate_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-3">
				<label class="required" for="penalty_rate">Penalty rate</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::number('penalty_rate', null, ['class' => 'form-control number', 'id' => 'penalty_rate','max' => '100','min' => '0']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-percent" aria-hidden="true"></i>
						</span>
					</div>
					<span class="penalty_rate_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
			<div class="col-sm-3">
				<label class="required" for="tax_rate">Tax rate</label>
				<div class="form-group">
					<div class="input-group">
						{!! Form::number('tax_rate', null, ['class' => 'form-control number', 'id' => 'tax_rate','max' => '100','min' => '0']) !!}
						<span class="input-group-addon">
							<i class="icon fa fa-percent" aria-hidden="true"></i>
						</span>
					</div>
					<span class="tax_rate_error error_field hidden mt-1 text-danger"></span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<input type="submit" class="btn btn-success btn-md btn-submit" id="loan_submit_button" value="@lang('buttons.general.save')" />
				</div>
			</div>
		</div>
	</div>
</div>
</div>
{!! Form::close() !!}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/moment_js/moment_js.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}

<script  type="text/javascript">

	$(function () {


		$('.money').maskMoney({
			precision : 2,
			affixesStay : true
		});


		maturityDate2();

		function maturityDate2() {
			let dsb_date = $('#disbursement_date').val();
			if (dsb_date) {
				let mt_date = $('#maturity_date').val();
				if (mt_date) {
					let dsb = moment(dsb_date,"DD/MM/YYYY");
					let mt = moment(mt_date,"DD/MM/YYYY");
					if (moment(mt).isAfter(dsb)) {
						tenureVal();
					} else {
						$('#maturity_date').val('');
						setMinDate(dsb_date);
					}
				} else {
					setMinDate(dsb_date);
				}
			} else {
				$('.mt_datepicker1').datetimepicker({
					timepicker:false,
					format:'d-m-Y',		
				});
			}
		}

		function setMinDate(from_date) {
			let futureMonth = moment(from_date,'DD-MM-YYYY').add(1, 'M').format('DD-MM-YYYY');
			let min_date = moment(futureMonth,'DD-MM-YYYY').startOf('month').format('DD-MM-YYYY');

			$('.mt_datepicker1').datetimepicker({
				timepicker:false,
				defaultDate: min_date, 
				format:'d-m-Y',	
				minDate: min_date,
			});	
		}


		$('#maturity_date').on('focusin', function (e) {
			maturityDate2();
		});
		$('#maturity_date').on('focusout', function (e) {
			tenureVal();
		});

		$('#disbursement_date').on('focusout', function (e) {
			$('#maturity_date').val('');
			if($('#disbursement_date').val()){
				setMinDate($('#disbursement_date').val());
			}
		});

		function tenureVal() {
			let dsb_date = $('#disbursement_date').val();
			let mt_date = $('#maturity_date').val();
			if (dsb_date && mt_date) {
				let dsb = moment(dsb_date,"DD/MM/YYYY");
				let mt = moment(mt_date,"DD/MM/YYYY");
				let months = mt.diff(dsb,'months');
				$('#tenure').val(months);
			}else{
				$('#tenure').val(0);
			}
		}


		$('.date_picker').datetimepicker({
			timepicker:false,
			format:'d-m-Y',
		});

		$('#fin_year').on('change', function(e) {
			e.preventDefault();
			let fy_id = $('#fin_year option:selected').val();
			let code_value_id = {{$code_value->id}};
			if (fy_id && code_value_id) {
				$.ajax({
					url : '{!! url("investment/budget/return_allocated_budget") !!}/'+fy_id+'/'+code_value_id,
					type: 'GET',
					datatype : 'json',
					success:function(data){
						if (data.success) {
							$('#total_budget').val(data.allocated_budget.total);
							$('#available_budget').val(data.allocated_budget.remain);
						}else{
							/*reload or do nothing*/
						}
					}
				});
			}else{
				/*reload or do nothing*/
			}
		});

		// let data_to_save = $('#add_budget_form').serializeArray();
		// data_to_save.push({name: 'total_budget',value: total});


		$('#loan_submit_button').on('click', function(e) {
			e.preventDefault();
			$('.error_field').addClass('hidden');
			$('.custom_error').removeClass('custom_error');
			let form = $('#loan_form');
			
			let loan_amount =  $('#loan_amount').val().replace(/,/g, "");
			let data_to_save = form.serializeArray();
			data_to_save.push({name: 'loan_amount',value: loan_amount});
			data_to_save.push({name: 'code_value_id',value: {{$code_value->id}} });
			let options = {
				dataType : 'json',
				url: '{{route('backend.investment.loan.save')}}',
				data : data_to_save,
				type : 'POST',
				success : function (data) {
					if (data.success) {
						$.amaran({
							'theme'     :'awesome success',
							'content'   :{
								title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
								message: ' Loan Added successfully',
								info:'',
								icon: 'fa fa-check-square-o'
							},
							'position'  :'bottom left',
							'outEffect' :'slideBottom',
							'inEffect'  :'slideLeft'
						});
						if (data.allocation_id) {
							setTimeout(function(){window.location.replace(" {!! url('investment/show') !!}/loans/"+data.allocation_id); }, 2000);
						}else{
							form.trigger("reset");
						}
					} 
					else {
						if (data.errors) {
							let m = Object.keys(data.errors);
							$.each(m, function( index, value ) {
								$('.'+value+'_error').removeClass('hidden');
								$('.'+value+'_error').text(data.errors[value][0]);
								$('#'+value).addClass('custom_error');
							});
						}else{
							// form.trigger("reset");
						}
					}
				},
				error: function (data) {
					// location.reload();
				}
			};
			$(form).ajaxSubmit(options);
		});



	});

</script>;

@endpush
