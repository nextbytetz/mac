@extends('layouts.backend.main', ['title' => 'Loans', 'header_title' =>' Loans'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
</style>
@endpush

@section('content')

<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="col-md-9">
				<table class="display" cellspacing="0" width="100%" id ="budget_datatable">
					<thead>
						<tr>
							<th>Invetsment Type</th>
							<th>Allocated Amount</th>
							<th>Allocated Percent</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="grey_modal">
						<table style="width:100%">
							<tr>
								<td  align="center"><h5><strong><span class="light_dark_color">Investment Budget Summary</span></strong></h5></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="light_grey_bg">&nbsp;</div>
					<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
						<p class="underline" style="padding-bottom: 2%;" >
							<span style="font-weight: lighter;">Period :</span> 
							<span style="font-weight: bold;" ></span>
						</p>
						<p class="underline" style="padding-top: 2%;">
							<span style="font-weight: lighter;">Total Budget :</span> 
							<span style="font-weight: bold;" > </span>
						</p>
						<p class="underline" style="padding-top: 2%;">
							<span style="font-weight: lighter;"> Total allocation :</span> 
							<span style="font-weight: bold;" ></span>
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script  type="text/javascript">

	$(function () {

		let budget_datatable = $('#budget_datatable').DataTable({});

	});

</script>;

@endpush
