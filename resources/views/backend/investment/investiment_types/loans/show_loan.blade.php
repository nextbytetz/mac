{{-- {{dd($loan_budget)}} --}}
@extends('layouts.backend.main', ['title' => 'Loans', 'header_title' =>' Loans'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
</style>
@endpush

@section('content')

<div class = "row">
	<div class = "row">
		<div class="col-md-12">
			<div class="col-md-9">
				<div class="pull-left pb-1">
					<a href="{{route('backend.investment.loan.create')}}" class="btn btn-success btn-md " role="button">Add Loan</a>
				</div>
				<table class="display" cellspacing="0" width="100%" id ="loans_datatable">
					<thead>
						<tr>
							<th>Loan Type</th>
							<th>Loanee</th>
							<th>Loan Amount</th>
							<th>Maturity Date</th>
							{{-- <th></th> --}}
						</tr>
					</thead>
				</table>
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="grey_modal">
						<table style="width:100%">
							<tr>
								<td  align="center"><h5><strong><span class="light_dark_color">	Summary</span></strong></h5></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="light_grey_bg">&nbsp;</div>
					<div class="col-md-12 light_grey_bg" style="min-height: 360px;">
						<p class="underline ">
							<span style="font-weight: lighter;">Period :</span> 
							<span style="font-weight: bold;" > 
								{{!empty($loan_budget->investmentBudget->fin_year) ? $loan_budget->investmentBudget->fin_year->name : ''}}
							</span>
						</p>
						<p class="underline pt-1">
							<span style="font-weight: lighter;">Total Budget :</span> 
							<span style="font-weight: bold;" > 
								{{!empty($loan_budget->amount) ? number_format($loan_budget->amount,2) : ''}}
							</span>
						</p>
						<p class="underline pt-1">
							<span style="font-weight: lighter;"> Total Loans :</span> 
							<span style="font-weight: bold;" >{{count($loan_budget->investmentLoan)}}</span>
						</p>
						<p class="underline  pt-1">
							<span style="font-weight: lighter;"> Total Loaned Amount :</span> 
							@php
							$total_loaned = 0;
							foreach($loan_budget->investmentLoan as $loaned){
								// dd($loaned);
								$total_loaned += $loaned->loan_amount;
							}
							@endphp
							<span style="font-weight: bold;" >{{number_format($total_loaned,2)}}</span>
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script  type="text/javascript">

	$(function () {

		let loans_datatable = $('#loans_datatable').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			searching: true,
			paging: true,
			info:false,
			order: [[ 0, "desc" ]],
			stateSaveCallback: function (settings, data) {
				localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			},
			stateLoadCallback: function (settings) {
				return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			},
			ajax:{
				url : '{!! url('investment/loan/datatable/'.$loan_budget->id) !!}',
				type : 'get'
			},
			columns: [
			{ data: 'loan_type_name' , name: 'investment_loans.loan_type_name'},
			{ data: 'loanee_name' , name: 'investment_loans.loanee_name'},
			{ data: 'loan_amount' , name: 'investment_loans.loan_amount'},
			{ data: 'maturity_date' , name: 'investment_loans.maturity_date'},
			{ data: 'action' , name :'action', searchable:false },
			],
			fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).click(function() {
					// let col = $(this).index()+1;
					// let colText = $('#loans_datatable thead th:nth-child('+col+')').text();
					// if(colText !=''){
					// 	// document.location.href = '{!! url("investment/budget/show") !!}/'+aData['id'];
					// }
				}).hover(function() {
					$(this).css('cursor','pointer');
				}, function() {
					$(this).css('cursor','auto');
				});
			}
		});
	});

</script>;

@endpush
