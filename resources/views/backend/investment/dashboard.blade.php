@extends('layouts.backend.main', ['title' => trans('labels.backend.investment_menu.dashboard_investment'), 'header_title' => trans('labels.backend.investment_menu.dashboard_investment')])

@section('content')


@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
 .table_summary:after {
  background-color: #F5F5F5;
  border: 1px solid #DDDDDD;
  border-radius: 4px 0 4px 0;
  color: #3c5ba4;
  content: "@lang('labels.backend.investment_menu.portfolio_title')";
  font-size: 18px;
  font-weight: bold; 
  left: -1px;
  padding: 3px 5px;
  position: absolute;
  top: -1px;
}

.table_summary {
  background-color: #FFFFFF;
  border: 1px solid #DDDDDD;
  border-radius: 4px 4px 4px 4px;
  margin: 5px 0px;
  padding: 39px 19px 14px;
  position: relative;
}
.investment_portfolio_summary:after {
  background-color: #F5F5F5;
  border: 1px solid #DDDDDD;
  border-radius: 4px 0 4px 0;
  color: #3c5ba4;
  content: "@lang('labels.backend.investment_menu.dashboard_title')";
  font-size: 18px;
  font-weight: bold; 
  left: -1px;
  padding: 3px 7px;
  position: absolute;
  top: -1px;
}

.investment_portfolio_summary {
  background-color: #FFFFFF;
  border: 1px solid #DDDDDD;
  border-radius: 4px 4px 4px 4px;
  margin: 5px 0px;
  padding: 39px 19px 14px;
  position: relative;
}

.investment_portfolio_summary:after {
  background-color: #F5F5F5;
  border: 1px solid #DDDDDD;
  border-radius: 4px 0 4px 0;
  color: #3c5ba4;
  content: "@lang('labels.backend.investment_menu.dashboard_title')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
      }

      .investment_portfolio_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
      }                                 
      span {
        content: "\27F5";
      }
    /*  .card1{
        width: 14rem;
      }*/
      .card-text {
        text-align:center; 
        font-size: 48px; 
        color: #339999; 
      }
      .card-header{
        color: #fff;
        font-size: 20px;
        text-align: center;
        /*color: #3366FF;*/
      }
      #table{
        color: #5E86FE;
        font-size: 18px;
      }    
    </style>
    @endpush
    @section('content')
    <div class="row">
      {{--style="position: fixed;z-index: 99999"--}}
      <div class="col-md-6">
        {!! Form::open(['class' => 'select_financial_year', 'method' => 'GET', 'id' => 'select_financial_year']) !!}
        <label class="required">Financial Year</label>
        <div class="form-group">
          <div class="input-group">
            {!! Form::select('fin_year_id', fin_year()->getAll(), [], ['class' => 'form-control search-select','style'=>'width:100px', 'id' => 'select_finance_year']) !!}
          </div>
          <span class="help-block">
            Select for the investment based on financial year
          </span>
        </div>
        {!! Form::close() !!}
        
      </div>
      <div class="col-md-6">
        {{ link_to_route('backend.home', 'Refresh Statistics', ['refresh_statistics' => 1], ['class' => 'btn btn-sm btn-success pull-right']) }}
      </div>
    </div>
    <!-- Put the page specifically for this page here -->      
    
    <div class="row">
      <div class="col-sm-3 col-md-3">
        <div class="card bg-light mb-3" style="max-width: 18rem;">
          <div class="card-header" style="background: #00A5A8">Outstanding Amount</div>
          <div class="card-body"><br/>
            {{-- <h5 class="card-title"> 60</h5> --}}
            <p class="card-text">TZS 60 Bil</p>
          </div>
        </div>
      </div>
      <div class="col-sm-3 col-md-3">
        <div class="card bg-light mb-3" style="max-width: 18rem;">
          <div class="card-header"  style="background: #B6B5B5;">Weighted Average Return</div>
          <div class="card-body"><br/>
            <p class="card-text">24%</p>
          </div>
        </div> 
      </div>
     {{--  <div class="col-sm-3 col-md-3">
        <div class="card bg-light mb-3" style="max-width: 22rem;">
          <div class="card-header bg-success">Allocated Budget Across Portfolios</div>
          <div class="card-body"><br/>
            <p class="card-text"><a href='#'></a>26.27 M</p>
          </div>
        </div>
      </div> --}}
      <div class="col-sm-3 col-md-3">
        <div class="card bg-light mb-3" style="max-width: 18rem;">
          <div class="card-header bg-warning">Total Income(This quarter)</div>
          <div class="card-body">
            <br/>
            <p class="card-text"> TZS 907 M</p>
          </div>
        </div>
      </div>
      <div class="col-sm-3 col-md-3">
        <div class="card bg-light mb-3" style="max-width: 22rem;">
          <div class="card-header" style="background: #4863A0;">Total Income(End of year)</div>
          <div class="card-body"><br/>
            <p class="card-text"><a href='#'></a>&nbsp; TZS 3 Bil</p>
          </div>
        </div>
      </div>
    </div>

      <div class="content">
        <div class="dashboard-content">
          <div class="dashboard-header ">
           <div class="dashboard-action">
            <ul class="right-action float-xs-right">
              <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06 icon_plus" aria-hidden="true"></span></a></li>
              <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
              </li>
            </ul>
          </div>
        </div>
         <div class="dashboard-box table_summary">
         <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <div class="content"> 
                <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                  <h6 class="text-secondary">Maturity today</h6>  </div>


              </div>
            </div>
            {{-- statarting of the Graph --}}

            <div class="col-md-6">
              <div class="content">
                <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                  <h6 class="text-secondary">Maturity expected in 7 days</h6>  </div>
                  {{-- <div id="chart-div"></div>
                  @piechart('Investment', 'chart-div')
                </div> --}}
              </div>
            </div>
          </div>
        </div>      

        <br>
        <div class="dashboard-box table_summary">
         <div class="row">
          <div class="col-md-12">
            <div class="col-md-5">
              <div class="content"> 
                <div class="daynamic-header  text-xs-center"> </div>
                <!-- Light table -->
                <div class="table-responsive">
                  <table class="table  table-bordered">
                    <thead class="thead-light" >
                      <tr>
                        <th class="name" id="table">Investment Category</th>
                        <th class="budget" id="table">Budget</th>
                        <th class="used_budget" id="table">Invested Amount</th>
                        <th class="investment" id="table">Number Of Investment</th>
                        <th  class="return" id="table">ROI</th>
                      </tr>
                    </thead>
                    <tbody class="list">
                      <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Fixed Deposit</span>
                            </div>
                          </div>
                        </th>
                        <td class="budget"> TZS 25 M</td>
                        <td class="used_budget">TZS 16M</td>
                        <td class="investment">6 </td>
                        <td class="return">5%</td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Treasury Bills</span>
                            </div>
                          </div>
                        </th>
                        <td class="budget"> TZS 35B</td>
                        <td class="used_budget">TZS 23B</td>
                        <td class="investment">120 </td>
                        <td class="return">15%</td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Treasury Bond</span>
                            </div>
                          </div>
                        </th>
                        <td class="budget"> TZS 250B</td>
                        <td class="used_budget">TZS 160B</td>
                        <td class="investment">6 </td>
                        <td class="return">45%</td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Corporate Bonds</span>
                            </div>
                          </div>
                        </th>
                      <td class="budget"> TZS 15B</td>
                        <td class="used_budget">TZS 6B</td>
                        <td class="investment">34 </td>
                        <td class="return">34%</td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Equities</span>
                            </div>
                          </div>
                        </th>
                        <td class="budget"> TZS 35 M</td>
                        <td class="used_budget">TZS 29M</td>
                        <td class="investment">5 </td>
                        <td class="return">2%</td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Loans</span>
                            </div>
                          </div>
                        </th>
                       <td class="budget"> TZS 0</td>
                        <td class="used_budget">TZS 0</td>
                        <td class="investment">0 </td>
                        <td class="return">0%</td>
                      </tr>
                      <tr>
                        <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Collective Investment Schemes</span>
                            </div>
                          </div>
                        </th>
                       <td class="budget"> TZS 349B M</td>
                        <td class="used_budget">TZS 90B</td>
                        <td class="investment">7 </td>
                        <td class="return">87%</td>
                      </tr>
                      <tr>

                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm">Real Estate</span>
                            </div>
                          </div>
                        </th>
                       <td class="budget"> TZS 0</td>
                        <td class="used_budget">TZS 0</td>
                        <td class="investment">0 </td>
                        <td class="return">0%</td>                      
                      </tr>
                      <tr>
                        <th scope="row">
                          <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                            </a>
                            <div class="media-body">
                              <span class="name mb-0 text-sm"> Call Account</span>
                            </div>
                          </div>
                        </th>
                    <td class="budget"> TZS 0</td>
                        <td class="used_budget">TZS 0</td>
                        <td class="investment">0 </td>
                        <td class="return">0%</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            {{-- statarting of the Graph --}}

            <div class="col-md-7">
              <div class="content">
                <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                  <h5 class="text-secondary">Portfolio Share(%)</h5>  </div>
                  <div id="chart-div"></div>
                  @piechart('Investment', 'chart-div')
                </div>
              </div>
            </div>
          </div>
        </div>
        {{--End of Pie graph  --}}
      </div>
    </div>
    @endsection

    @push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.js") }}
    {{ Html::script(asset_url(). "/global/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js") }}
    {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.resize.js") }}
    {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.pie.js") }}
    {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.time.js") }}
    {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.categories.js") }}
    {{ Html::script(asset_url(). "/global/plugins/flot-spline/js/jquery.flot.spline.min.js") }}
    {{ Html::script(asset_url(). "/global/plugins/arcseldon-jquery.sparkline/dist/jquery.sparkline.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
      $(function () {
        $(".search-select").select2({});
        $("#select_finance_year").on("change", function () {
          $("#select_financial_year").submit();
        });
        $(".stackline-bar").sparkline("html", {
          type: "bar",
          height: "40px",
          barWidth: 5,
          barSpacing: 5,
          barColor: ['#2dbf7c'],
          negBarColor: ['#c9302c'],
          stackedBarColor: ['#087380', "#c9302c"]
        });
      });


    </script>

    @endpush


