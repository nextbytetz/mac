@extends('layouts.backend.main', ['title' => "Choose Investment Type", 'header_title' => "Choose Investment Type"])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

{!! Form::open(['route' => 'backend.investment.choose_type']) !!}
<p class="h5">
  Search Investment type using, {{-- <code><b>Inv number e.g <i class="underline">INV-01 </i> </b> </code> --}} or 
  <code><b>name e.g <i class="underline">Fixed  Deposit Bond</i> </b></code>
</p>
<hr/>
<div class="row">
  <div class="col-md-12">
    <div class="grid-column">
      <label class="h4">Investment Type</label><br>
      {!! Form::select('investment_type', [], null, ['class' => 'investment_type', 'style' => 'width:100%', 'id' => 'investment_type']) !!}
      <input class="reference form-control" name="reference" hidden="hidden" />
      <br><br>
      {!! $errors->first('investment_type', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
  </div>
</div>
<br/>
<div class="row">
  <div class="col-md-12">
    {!! Form::submit(trans('labels.general.continue'), ['class' => 'btn btn-success btn-save btn-block']) !!}
  </div>
</div>
{!! Form::close() !!}
@endsection


@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script>

  $(function () {

    let $body = $('body');


    $(".investment_type").select2({
     minimumInputLength: 3,
     multiple: false,
     allowClear: true,
     debug: true,
     placeholder: "",
     ajax: {
      url: '{!! route('backend.investment.select_investment_type') !!}',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        data = data.map(function (item) {
          return {
            text: item.name,
            id: item.id,
            name: item.name,
            reference: item.reference,
          };
        });
        return {
          results:  data
        };
      },
      cache: true
    }
  });

    $('.investment_type').on('change',function(e){ 
      let data=$('.investment_type').select2('data')[0];
      if (data) {
        $('.reference').val(data.reference);
      }


    }); 



  });


</script>
@endpush