@extends('layouts.backend.main', ['title' => "Letter Profile", 'header_title' => "Letter Profile"])

@include('backend.includes.datatable_assets')
@include('backend.includes.assets.datetimepicker')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        .bordered {
            border: 1px solid #CCCCCC;
            border-radius: 6px 6px 6px 6px;
            -moz-border-radius: 6px 6px 6px 6px;
            -webkit-border-radius: 6px 6px 6px 6px;
            box-shadow: 0 1px 1px #CCCCCC;
        }
        table {
            border-spacing: 0;
            /*width: 600px;*/
            width: 100%;
            margin: 30px;
        }

        .bordered td:first-child, .bordered th:first-child {
            border-left: medium none;
        }

        .bordered td {
            border-left: 1px solid #CCCCCC;
            border-top: 1px solid #CCCCCC;
            padding: 10px;
            text-align: left;
        }
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    @include("backend/letters/includes/header_info")

    <div class="nav_tab_pane_header">
        <div class="row">
            <div class="col-md-12" >

                <div class="pull-right" >

                    @if (!$letter->wf_done)

                    @endif
                    {{--Update Dispatch Info--}}
                    <span>
                        <a href="#"  class="btn btn-primary site-btn nav_button update_dispatch_info" >
                            <i class="icon fa fa-edit" aria-hidden="true"></i>
                        Dispatch Info</a>
                    </span>

                    {{--initiate for approval--}}
                    @if (!$letter->isinitiated)
                        <span>
                            {!! Html::decode(link_to('#', "<i class=\"icon fa fa-level-up\" aria-hidden=\"true\"></i>&nbsp;Initiate Approval", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Approval', 'data-group' => 19, 'data-type' => $wfmodule->type, 'data-resource' => $letter->id, 'data-route' => route("backend.workflow.initiate")])) !!}
                        </span>
                    @endif

{{--                    @if (!$letter->approved)--}}
                    @if (true)
                        {{--Edit Letter--}}
                        <span>
                        <a href="{!! route('backend.letter.edit', $letter->id) !!}"  class="btn btn-primary site-btn nav_button" >
                                <i class="icon fa fa-edit" aria-hidden="true"></i>
                            Modify</a>
                    </span>
                    @endif
                    @if ($letter->approved)
                        {{--Print Letter--}}
                        <span>
                            <a href="{!! route('backend.letter.live_print', $letter->id) !!}"  class="btn btn-primary site-btn nav_button print_letter" target="_blank">
                                    <i class="icon fa fa-print" aria-hidden="true"></i>
                                Print</a>
                        </span>
                        {{--Generate PDF--}}
                        <span>
                            <a href="{!! route('backend.letter.make_pdf', $letter->id) !!}"  class="btn btn-secondary site-btn">
                                    <i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>
                                Make PDF</a>
                        </span>
                    @endif

                    {{--Mark as printed--}}
                    @if ($letter->approved And !$letter->isprinted)
                   {{-- @if (true)--}}
                        <span>
                            {!! Html::decode(link_to_route('backend.letter.printed', "<i class='icon fa fa-print' aria-hidden='true'></i>&nbsp;Mark Printed" , $letter->id, ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to mark this letter as printed", 'class' => 'btn btn-primary site-btn nav_button'])) !!}

                        </span>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <legend></legend>
    <br/>

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs">
                    {{--Letter Profile--}}
                    <li class="nav-item">
                        <a class="nav-link active" id="inspection_process_tab" data-toggle="tab" href="#letter_profile" role="tab">
                            <i class="icon fa fa-briefcase" aria-hidden="true"></i>Profile
                        </a>
                    </li>
                    {{--Document Centre--}}
                    <li class="nav-item">
                        <a class="nav-link" id="document_centre_tab" data-toggle="tab" href="#document_centre" role="tab">
                            <i class="icon fa fa-paperclip" aria-hidden="true"></i>Attachment
                        </a>
                    </li>
                    {{--General Information--}}
                    <li class="nav-item">
                        <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info-circle" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                    {{--@if ($hasold)
                        <li class="nav-item">
                            <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#legacy_letter" role="tab">
                                <i class="icon fa fa-file-archive-o" aria-hidden="true"></i>Legacy Letter
                            </a>
                        </li>
                    @endif--}}
                </ul>
                <div class="nav_tab_contain tab-content">
                    <div class="tab-pane active" id="letter_profile" role="tabpanel">
                        {{--Letter Template--}}
                        <div class="row">
                            @if (!is_null($letter->resources))
                                <div class="col-md-4">
                                    @include("backend/letters/includes/resource_view", ['resources' => json_decode($letter->resources, true)])
                                    <div style="display: none;" id="dispatchform">
                                        @include("backend/letters/dispatchinfo")
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    @else
                                        <div class="col-md-8 offset-md-2">
                                            @endif
                                            {{-- @if ($letter->approved)--}}
                                            @if (true)
                                                {{--approved document--}}
                                                {{--preview for approval--}}
                                                @if (!$letter->pdfready)
                                                    {{--@if (true)--}}
                                                    <iframe allowfullscreen src="{{ route("backend.letter.preview", $letter->id) }}" width='100%' height='600px' frameborder="1"></iframe>
                                                @else
                                                    <iframe allowfullscreen src="{{ letters_path() . "/" . $letter->id . ".pdf" }}" width='100%' height='600px' frameborder="1"></iframe>
                                                @endif

                                            @endif
                                        </div>
                                </div>

                                {{--Workflow Track--}}
                                <div class="row">
                                    <div class="col-md-12">
                                        @if ($letter->isinitiated)
                                            @php
                                                //$wfModule = $letter->wfModule;
                                                //$type = $wfModule->type;
                                                //$moduleGroup = $wfModule->wfModuleGroup->id;
                                                $workflowinput = ['resource_id' => $letter->id, 'wf_module_group_id'=> 19, 'type' => $wfmodule->type];
                                            @endphp
                                            {{--                                    {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}--}}
                                            @include("backend/includes/workflow/wf_track_html", $workflowinput)
                                            <br/>
                                        @else
                                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <i class="fa fa-exclamation-circle"></i><strong>Info</strong> No active workflow recently initiated
                                            </div>
                                        @endif
                                    </div>
                                </div>
                        </div>
                    <div class="tab-pane" id="document_centre" role="tabpanel">
                        <br/>
                        @include("backend/system/document/includes/document_center", ['resource' => $letter, 'reference' => "DRLETTER"])
                    </div>
                    <div class="tab-pane" id="general_information" role="tabpanel">
                        <legend class="grey_modal" >Staging Logs</legend>
                        <div class="label label-info">Letter <b>{{ $letter->reference }}</b> Status </div>
                        <legend></legend>
                        @include("backend/letters/includes/stage_comments", ['stages' => $letter->stages()])
                    </div>
                    {{--@if ($hasold)
                        <div class="tab-pane" id="legacy_letter" role="tabpanel">
                            <div class="row">
                                <div class="col-md-9 offset-md-1">
                                    <iframe allowfullscreen src="{{ route("backend.letter.preview_legacy", $letter->id) }}" id="letter_preview" name="letter_preview" width='100%' height='900px' frameborder="1"></iframe>
                                </div>
                        </div>
                    @endif--}}
                </div>
            </div>
        </div>
    </div>

    @include("backend.system.workflow.includes.initiate_modal")
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script>
        $(function() {
            let $body = $('body');
            if (location.hash !== '') {
                let $linkhref = $('a[href="' + location.hash + '"]');
                $linkhref.tab('show');
                $linkhref.trigger('click');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                let tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });
            $body.on('click', 'a.print_letter', function ($e) {
                $e.preventDefault();
                let $letter_date = "{{ $letter->letter_date }}";
                if (!$letter_date) {
                    swal("Letter date not registered, update dispatch information");
                } else {
                    window.open($(this).attr('href'), "_blank");
                }
            });
            $body.on('click', 'a.update_dispatch_info', function (e) {
                $("#dispatchform").show();
            });
            $body.on('submit', 'form.update_dispatch_info', function (e) {
                e.preventDefault();
                var $form = this;
                $($form).find(':input').each(function () {
                    var $name = $(this).attr('name');
                    $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                var $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        location.reload();
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function(index, value) {
                            $($form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        });
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });
        });
    </script>
@endpush