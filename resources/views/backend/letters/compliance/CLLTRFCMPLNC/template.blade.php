<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/letter/standard.css") }}
</head>
<body>
<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">WORKERS COMPENSATION FUND</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 100%; vertical-align: top;" colspan="2">
                        <div style="text-align: left;">Telegraphic address "WCF"</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Tel:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Fax:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Email:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Web:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: middle; text-align: center;"><img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>P.O. Box 79655</div>
            <div>GEPF House</div>
            <div>Plot No. 37</div>
            <div>Regent Estate</div>
            <div>Bagamoyo Road</div>
            <div>Dar es Salaam</div>
        </td>
    </tr>
    </tbody>
</table>
@php
    $extras = json_decode($letter->extras, true);
    $employer_name = (isset($extras['employername'])) ? nl2br($extras['employername']) : ucwords(strtolower($employer->name));
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $letterDateCarbon = \Carbon\Carbon::parse($letter->letter_date_label);
    $visitDateCarbon = \Carbon\Carbon::parse($employer_task->visit_date);
    $lastInspectionDateCarbon = \Carbon\Carbon::parse($employer_task->last_inspection_date);
    $noticeDateCarbon = \Carbon\Carbon::parse($extras['noticedate']);
    $assessmentStartCarbon = \Carbon\Carbon::parse($employer_task->review_start_date);
    $assessmentEndCarbon = \Carbon\Carbon::parse($employer_task->review_end_date);
    $waiverDateCarbon = \Carbon\Carbon::parse("2017-10-01");
    $endContributionBeforeWaiveCarbon = \Carbon\Carbon::parse("2017-09-30");
    $visit_date = $visitDateCarbon->format("d<\s\u\p>S</\s\u\p> F, Y");
    $letter_date = $letterDateCarbon->format("d<\s\u\p>S</\s\u\p> F, Y");
    $last_inspection_date = $lastInspectionDateCarbon->format("d<\s\u\p>S</\s\u\p> F, Y");
    $notice_date = $noticeDateCarbon->format("d<\s\u\p>S</\s\u\p> F, Y");
    $waiver_date = $waiverDateCarbon->format("d<\s\u\p>S</\s\u\p> F, Y");
    $assessment_start = $assessmentStartCarbon->format("F Y");
    $assessment_end = $assessmentEndCarbon->format("F Y");
    $assessment_end_period = $assessmentEndCarbon->endOfMonth()->format("d<\s\u\p>S</\s\u\p> F, Y");
    $end_contribution_before_waive = $endContributionBeforeWaiveCarbon->format("d<\s\u\p>S</\s\u\p> F, Y");
@endphp
<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="0">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>In reply please quote:&nbsp;</strong></div>
                <div>&nbsp;</div>
                <div><strong>Re.No: {{ $letter->reference }}</strong></div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;"><strong>{!! $letter_date !!}</strong></td>
        </tr>
        </tbody>
    </table>
</div>
<div>&nbsp;</div>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{ $letter->salutation }},</div>
            <div>{{ $employer_name }},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            <div>&nbsp;</div>
            <div><strong>REF: LETTER OF COMPLIANCE </strong></div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">
                Kindly refer to the Inspection exercise that was conducted at your office premises by our authorized officers on {!! $last_inspection_date !!}.
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">
                The objective of the referred inspection was to ascertain how your institution complies with the legal obligation imposed to you under Part VIII of the Workers Compensation Act [Cap 263 R.E. 2015] read together with the Workers Compensation (Payment of Tariff) Regulations GN No. 185 of 2015, GN No. 212A of 2016 and GN No. 229A of 2017.
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">
                We are pleased to inform you that the conducted inspection revealed that {{ $employer_name }} has complied with the requirements of Workers Compensation Act [Cap 263, R.E 2015]. In addition, your institution has been recorded in the Register of compliant employers for the period ended {!! $assessment_end_period !!}.
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">
                We would like to take this opportunity to thank you for complying with the requirements of the Workers Compensation Act and the cooperation you extended to our officers during the inspection exercise.
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">Looking forward to your continued cooperation with us.</div>
            <div>&nbsp;</div>

            <div class="div_break">
                @include('backend/letters/compliance/includes/signature')

                {{--<div>&nbsp;&nbsp;&nbsp;<strong>Cc:&nbsp;</strong>{{ ucwords(strtolower($gender_title)) }}. {{ ucwords(strtolower($employee_name)) }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $employer_name }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $letter->box_no }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>{{ strtoupper($letter->location) }}.</strong></div>
                <div>&nbsp;</div>--}}

            </div>
        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . '/global/plugins/jquery/dist/jquery.min.js') }}
<script type="text/javascript">
    @if ($print)
        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function(){
        $(document).on("contextmenu",function(e){
            return false;
        });
    });
</script>
</body>
</html>