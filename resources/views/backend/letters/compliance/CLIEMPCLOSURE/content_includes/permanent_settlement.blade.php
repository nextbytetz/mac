


{{--permanent--}}
<div style="text-align: justify !important;" class="div_break">Kindly refer to the above subject matter and your letter dated {!! $closure_app_date . ' ' !!} which informed the Fund that you have temporary closed  {{ $employer_name }}.</div>
<div>&nbsp;</div>

<div style="text-align: justify !important;" class="div_break">Our records indicate you have outstanding arrears amounting to TZS {{  number_2_format($closure->arrears) }} in respect to contribution for the period of {{ number_0_format($closure->missing_months) }}  @if($closure->interest_amount == 'muted')
        months and interest thereon of TZS  {{ number_2_format($closure->interest_amount) }}.
                                                                                                                                                                                                                                                                              @else
        month(s).
    @endif You are therefore required to settle the outstanding arrears as required under the Workers Compensation Act, Cap 263 [R. E. 2015] to enable the Fund process your request.</div>
<div>&nbsp;</div>

<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">For more information, please visit WCF offices or contact our Compliance Manager, Mr. Victor R. Luvena through mobile number 0737344099.</div>
<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Thank you for your cooperation.</div>
<div>&nbsp;</div>