



<div style="text-align: justify !important;" class="div_break">Kindly refer to the above subject matter and your letter dated {!! $closure_app_date !!} which informed the Fund that you have temporarily closed {{ $employer_name }}.</div>
<div>&nbsp;</div>

<div style="text-align: justify !important;" class="div_break">We wish to inform you that based on the current information availed to the Fund, your request to temporarily deregister {{ $employer_name }} with registration number {{ $employer_regno }} is approved and the company has been temporarily removed from the register of active employers with effect from {!! $closure_close_date !!} to {!!  $reopen_date !!} as you had requested.</div>

<div>&nbsp;</div>

<div style="text-align: justify !important;" class="div_break">We also wish to inform you that in the circumstance your business is still closed beyond such time, you will be required to submit a request for extension of the period of temporary deregistration, otherwise the Fund will consider your business has resumed. Please be informed that, in the event it is established that the Fund was misinformed during the request for temporary deregistration, legal action will be taken against you in accordance to the Workers Compensation Act, Cap 263 [Revised Edition 2015].  </div>

<div>&nbsp;</div>

<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Thank you for your cooperation.</div>