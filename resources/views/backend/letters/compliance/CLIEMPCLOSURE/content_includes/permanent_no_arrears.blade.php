

<div style="text-align: justify !important;" class="div_break">Kindly refer to the above subject matter and your letter dated {!! $closure_app_date !!}.</div>
<div>&nbsp;</div>


<div style="text-align: justify !important;" class="div_break">We acknowledge receipt of your request for {{ $acknlg_closure_type  }} of your account.  We wish to inform you that your request has been approved. Subsequently, {{ $employer_name }} with registration number {{ $employer_regno }} has been removed from the register of active employers with effect from {!! $closure_close_date !!}.</div>
<div>&nbsp;</div>


<div style="text-align: justify !important;" class="div_break">We would like to thank you for being a compliant employer for the period you were a member of the Fund. Kindly be informed. </div>

<div>&nbsp;</div>

<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Thank you for your cooperation.</div>