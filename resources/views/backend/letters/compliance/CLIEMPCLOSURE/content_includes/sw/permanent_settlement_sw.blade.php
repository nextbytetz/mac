


{{--permanent--}}
<div style="text-align: justify !important;" class="div_break">Tafadhali rejea somo tajwa hapo juu pamoja na barua yako ya tarehe {!!  sw_date($closure_app_date_carbon) . ' ' !!} iliyoutaarifu Mfuko kuwa kampuni ya {{ $employer_name }} inafunga biashara.</div>
<div>&nbsp;</div>

<div style="text-align: justify !important;" class="div_break">Kumbukumbu zetu zinaonesha kwamba kampuni yako inadaiwa kiasi cha TZS {{  number_2_format($closure->arrears) }} ikiwa ni michango ya kipindi cha {{  ($closure->missing_months == 1) ? ('mwezi ' . number_0_format($closure->missing_months)) : ('miezi ' . number_0_format($closure->missing_months)) }}.
    @if($closure->interest_amount == 'muted')
        na tozo la kuchelewesha michango kiasi cha TZS  {{ number_2_format($closure->interest_amount) }}.
    @else

    @endif Hivyo, unatakiwa kulipa deni hilo kwa mujibu wa Sheria ya Fidia kwa Wafanyakazi Sura ya 263 [Marejeo ya Mwaka 2015] ili kuuwezesha Mfuko kushughulikia ombi lako.</div>
<div>&nbsp;</div>

<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Kwa taarifa zaidi, tafadhali tembelea ofisi za Mfuko au wasiliana na Meneja Matekelezo, Bw. Victor R. Luvena kupitia simu ya mkononi nambari 0737 344099.</div>
<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Asante kwa ushirikiano wako.</div>
<div>&nbsp;</div>