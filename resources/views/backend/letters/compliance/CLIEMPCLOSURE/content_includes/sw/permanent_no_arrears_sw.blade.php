

<div style="text-align: justify !important;" class="div_break">Tafadhali rejea somo tajwa hapo juu pamoja na barua yako ya tarehe {!! sw_date($closure_app_date_carbon) !!} iliyoutaarifu Mfuko kuwa kampuni ya {{ $employer_name }} inafunga biashara.</div>
<div>&nbsp;</div>


<div style="text-align: justify !important;" class="div_break">Tunapenda kukuarifu kuwa kulingana na taarifa iliyowasilishwa katika Mfuko, kwa sasa ombi lako la kufuta usajili wa {{ $employer_name }} yenye nambari ya usajili {{ $employer_regno }} limekubaliwa na hivyo kampuni imeondolewa kwenye orodha ya waajiri kuanzia tarehe {!! sw_date($closure_close_date_carbon) !!}. Aidha, unataarifiwa kuwa endapo itathibitika kuwepo udanganyifu kwenye taarifa za kufuta usajili ambazo zimewasilishwa katika Mfuko, hatua za kisheria zitachukuliwa dhidi yako kwa mujibu wa Sheria ya Fidia kwa Wafanyakazi, Sura 263 [Marejeo ya 2015].</div>
<div>&nbsp;</div>


{{--<div style="text-align: justify !important;" class="div_break">We would like to thank you for being a compliant employer for the period you were a member of the Fund. Kindly be informed. </div>--}}

<div>&nbsp;</div>

<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Asante kwa ushirikiano wako.</div>