



<div style="text-align: justify !important;" class="div_break">Tafadhali rejea somo tajwa hapo juu pamoja na barua yako ya tarehe {!!  sw_date($closure_app_date_carbon) !!} ikiutaarifu Mfuko kuhusu kufungwa kwa muda kampuni ya {{ $employer_name }}.</div>
<div>&nbsp;</div>

<div style="text-align: justify !important;" class="div_break">Tunapenda kukuarifu kuwa kulingana na taarifa iliyowasilishwa katika Mfuko, ombi lako la kuifunga kwa muda kampuni ya {{ $employer_name }} yenye nambari ya usajili {{ $employer_regno }} limekubaliwa na hivyo kampuni imeondolewa kwenye orodha ya waajiri kuanzia tarehe {!! sw_date($closure_close_date_carbon) !!} mpaka {!!  sw_date($reopen_date_carbon) !!} kama ulivyoomba.</div>

<div>&nbsp;</div>

<div style="text-align: justify !important;" class="div_break">Tunapenda kukuarifu kuwa endapo kampuni yako itaendelea kufungwa zaidi ya kipindi tajwa, utalazimika kuwasilisha maombi ya kuongezewa muda wa kufunga biashara, la sivyo, Mfuko utachukulia kwamba kampuni imerejea katika shughuli zake. Pia, ifahamike kwamba endapo itathibitika kuwepo udanganyifu wowote kwa Mfuko katika taarifa zilizowasilishwa za kufunga kwa muda kampuni, hatua za kisheria zitachukuliwa dhidi yako kulingana na Sheria ya Fidia kwa Wafanyakazi, Sura 263 [Marejeo ya Mwaka 2015].</div>

<div>&nbsp;</div>

<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Asante kwa ushirikiano wako.</div>