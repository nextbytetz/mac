<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/letter/standard.css") }}
</head>
<body>


@php

    $employer_name = proper_case_word($employer->name);
    $employer_regno = append_zero_before_num($employer->id,6);
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $acknlg_closure_type = $closure->letter_closure_type_acknlg;
    $nowCarbon = \Carbon\Carbon::now();
    $closure_app_date_carbon = \Carbon\Carbon::parse($closure->application_date);
    $closure_app_date = $closure_app_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
     $closure_close_date_carbon = \Carbon\Carbon::parse($closure->close_date);
    $closure_close_date = $closure_close_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $approved_date_carbon = \Carbon\Carbon::parse($closure->wf_done_date);
    $approved_date = $approved_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
        $letterDateCarbon = \Carbon\Carbon::parse($letter->letter_date_label);
    $letter_date = $letterDateCarbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $reopen_date_carbon = isset($closure->specified_reopen_date) ? \Carbon\Carbon::parse($closure->specified_reopen_date) : null;
    $reopen_date = isset($reopen_date_carbon) ? $reopen_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y") : null;
//$has_arrears = ($closure->arrears > 0 || $closure->interest_amount > 0) ? 1 : 0;
    $has_arrears = ($closure->arrears > 0) ? 1 : 0;
    $letter_ref_prefix =  ($letter->lang == 'en') ? 'REF:' : 'YAH:';
$lang = $letter->lang;
@endphp

{{--Top Part--}}
@if($letter->lang == 'en')
    @include('backend/letters/includes/top_part/top_en', ['letter_reference_no' => $letter->reference, 'letter_date' => $letter_date ])
@else
    @include('backend/letters/includes/top_part/top_sw',  ['letter_reference_no' => $letter->reference, 'letter_date' => $letter_date ])
@endif



<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{ $letter->salutation }},</div>
            <div>{{ $employer_name }},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            <div>&nbsp;</div>
            <div><strong> {{ $letter_ref_prefix . ' ' .    $letter_reference }} </strong></div>
            <div>&nbsp;</div>
            {{--Start content--}}

            @if($closure->closure_type == 1)
                {{--tempo--}}
                @if($has_arrears == 0)
                    {{--tempo with no arrears--}}
                    @if(isset($reopen_date))
                        {{--with period--}}
                        @if($lang == 'en')
                            @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/temporary_no_arrears_with_period')
                        @else
                            @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/sw/temporary_no_arrears_with_period_sw')
                        @endif
                    @else
                        {{--no period--}}
                        @if($lang == 'en')
                            @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/temporary_no_arrears')
                        @else
                            @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/sw/temporary_no_arrears_sw')
                        @endif
                    @endif
                @else
                    {{--tempo -with arrears--}}

                    @if($closure->sub_type_reference_cv == 'ETCLPERMSETTL')
                        {{--settlement for permanent--}}
                        @if($lang == 'en')
                            @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/permanent_settlement')
                        @else
                            @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/sw/permanent_settlement_sw')
                        @endif
                    @else
                        @if(isset($reopen_date))
                            {{--arrears with period--}}
                            @if($lang == 'en')
                                @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/temporary_arrears_with_period')
                            @else
                                @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/sw/temporary_arrears_with_period_sw')
                            @Endif
                        @else
                            {{--arrears with no period--}}
                            @if($lang == 'en')
                                @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/temporary_arrears')
                            @else
                                @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/sw/temporary_arrears_sw')
                            @endif
                        @endif

                    @endif
                @endif

            @else
                {{--Permanent--}}
                @if($lang == 'en')
                    @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/permanent_no_arrears')
                @else
                    @include('backend/letters/compliance/CLIEMPCLOSURE/content_includes/sw/permanent_no_arrears_sw')
                @endif
            @endif

            {{--end content--}}
            <div>&nbsp;</div>
            <div class="div_break">
                @include("backend/letters/compliance/includes/signature")

                {{--<div>&nbsp;&nbsp;&nbsp;<strong>Cc:&nbsp;</strong>{{ ucwords(strtolower($gender_title)) }}. {{ ucwords(strtolower($employee_name)) }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $employer_name }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $letter->box_no }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>{{ strtoupper($letter->location) }}.</strong></div>
                <div>&nbsp;</div>--}}

            </div>
        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
<script type="text/javascript">
    @if ($print)
        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function(){
        $(document).on("contextmenu",function(e){
            return false;
        });
    });
</script>
</body>
</html>