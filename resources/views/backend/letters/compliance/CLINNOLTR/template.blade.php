<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    {{ Html::style(asset_url() . '/nextbyte/css/backend/letter/standard.css') }}
</head>
<body>
<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">WORKERS COMPENSATION FUND</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 100%; vertical-align: top;" colspan="2">
                        <div style="text-align: left;">Telegraphic address "WCF"</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Tel:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Fax:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Email:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Web:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: middle; text-align: center;"><img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>P.O. Box 79655</div>
            <div>GEPF House</div>
            <div>Plot No. 37</div>
            <div>Regent Estate</div>
            <div>Bagamoyo Road</div>
            <div>Dar es Salaam</div>
        </td>
    </tr>
    </tbody>
</table>
@php
    $letterDateCarbon = \Carbon\Carbon::parse($letter->letter_date_label);
    $employer_name = ucwords(strtolower($employer->name));
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $nowCarbon = \Carbon\Carbon::now();
    $visitDateCarbon = \Carbon\Carbon::parse($employer_task->visit_date);
    $visit_date = $visitDateCarbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $letter_date = $letterDateCarbon->format("d<\s\u\p>S</\s\u\p> F Y");
@endphp
<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="0">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>In reply please quote:&nbsp;</strong></div>
                <div>&nbsp;</div>
                <div><strong>Re.No: {{ $letter->reference }}</strong></div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;"><strong>{!! $letter_date !!}</strong></td>
        </tr>
        </tbody>
    </table>
</div>
<div>&nbsp;</div>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{ $letter->salutation }},</div>
            <div>{{ $employer_name }},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            <div>&nbsp;</div>
            <div><strong>REF: NOTICE OF INSPECTION </strong></div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">We would like to inform you that on {!! $visit_date !!}, the Fund shall conduct inspection through its dully Authorized Officers, at your office premises. This inspection is in accordance with the powers conferred upon it under Section 72 (1) of the Workers Compensation Act [Cap 263 Revised Edition 2015].</div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">The objective of this inspection is to ascertain how your institution complies with the legal obligation imposed to you under Part VIII of the Workers Compensation Act [Cap 263 Revised Edition 2015] read together with the Workers Compensation (Payment of Tariff) Regulations GN No. 169 of 2015 and the Workers Compensation (Payment of Tariff) Regulations GN No. 212A of 2016.</div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">Kindly note that, the Authorized Officers will produce their Certificates of Authorization; please provide them access to your premises, staff and all the requested documents. For easy of reference, we have attached an extract from section 8 of the Act which indicates the powers of the Authorized Officers, offences for obstruction of an authorized person and a list of the required documents during the inspection.</div>
            <div>&nbsp;</div>

            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">We thank you and looking forward to your cooperation.</div>
            <div>&nbsp;</div>
            <div class="div_break">
                @include("backend/letters/compliance/includes/signature")

                {{--<div>&nbsp;&nbsp;&nbsp;<strong>Cc:&nbsp;</strong>{{ ucwords(strtolower($gender_title)) }}. {{ ucwords(strtolower($employee_name)) }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $employer_name }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $letter->box_no }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>{{ strtoupper($letter->location) }}.</strong></div>
                <div>&nbsp;</div>--}}

            </div>
        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
<script type="text/javascript">
    @if ($print)
        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function(){
        $(document).on("contextmenu",function(e){
            return false;
        });
    });
</script>
</body>
</html>