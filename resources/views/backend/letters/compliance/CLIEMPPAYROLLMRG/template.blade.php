<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/letter/standard.css") }}
</head>
<body>


    @php

    $employer_name = proper_case_word($employer->name);
    $employer_regno = append_zero_before_num($employer->id,6);
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $acknlg_closure_type = $closure->letter_closure_type_acknlg;
    $nowCarbon = \Carbon\Carbon::now();
    $closure_app_date_carbon = \Carbon\Carbon::parse($closure->application_date);
    $closure_app_date = $closure_app_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $closure_close_date_carbon = \Carbon\Carbon::parse($closure->close_date);
    $closure_close_date = $closure_close_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $approved_date_carbon = \Carbon\Carbon::parse($closure->wf_done_date);
    $approved_date = $approved_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $letterDateCarbon = \Carbon\Carbon::parse($letter->letter_date_label);
    $letter_date = $letterDateCarbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $reopen_date_carbon = isset($closure->specified_reopen_date) ? \Carbon\Carbon::parse($closure->specified_reopen_date) : null;
    $reopen_date = isset($reopen_date_carbon) ? $reopen_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y") : null;
    $has_arrears = ($closure->arrears > 0) ? 1 : 0;
    // $letter_ref_prefix =  ($letter->lang == 'en') ? 'REF:' : 'YAH:';
    $letter_ref_prefix =  'REF:';
    // $lang = $letter->lang;
    $lang = 'en';
    @endphp

    {{--Top Part--}}
    {{-- @if($letter->lang == 'en') --}}
    @include('backend/letters/includes/top_part/top_en', ['letter_reference_no' => $letter->reference, 'letter_date' => $letter_date ])
   {{--  @else
    @include('backend/letters/includes/top_part/top_sw',  ['letter_reference_no' => $letter->reference, 'letter_date' => $letter_date ])
    @endif --}}



    <table style="border-collapse: collapse; width: 100%;" border="0">
        <tbody>
            <tr>
                <td style="width: 100%; text-align: left;">
                    <div>{{ $letter->salutation }},</div>
                    <div>{{ $employer_name }},</div>
                    <div>{{ !empty($letter->box_no) ? 'P.O Box '.$letter->box_no : $letter->box_no }},</div>
                    <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
                    <div>&nbsp;</div>
                    <div><strong> {{ $letter_ref_prefix . ' ' .    $letter_reference }} </strong></div>
                    <div>&nbsp;</div>
                    {{--Start content--}}
                    <div style="text-align: justify !important;" class="div_break">Kindly refer to the above subject matter and your letter {{!empty($reference) ? 'with reference number '.$reference : ''}}  dated {!! \Carbon\Carbon::parse($letter_request_date)->format("d<\s\u\p>S</\s\u\p> F, Y") !!} requesting for merging payrolls.</div>
                    <div>&nbsp;</div>

                    <div style="text-align: justify !important;" class="div_break">However, in the referred letter you requested us to merge payrolls managed by users with email addresses {{$merged_emails}}.</div>
                    <div>&nbsp;</div>
                    <div style="text-align: justify !important;" class="div_break">In view of the above, the Fund would like to inform you that merging of respective payrolls has been effected as requested and you will be able to access the Fund’s online portal through user ID with email address <strong>{{$keep_email}}</strong>. </div>
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>
                    <div style="text-align: justify !important;" class="div_break">Thank you for your cooperation.</div>

                    {{--end content--}}
                    <div>&nbsp;</div>
                    <div class="div_break">
                        @include("backend/letters/compliance/includes/signature")
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    {{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
    <script type="text/javascript">
        @if ($print)
        window.onload = function() {
            window.print();
        };
        window.onafterprint = function(){
            window.close();
        };
        @endif
        $(function(){
            $(document).on("contextmenu",function(e){
                return false;
            });
        });
    </script>
</body>
</html>