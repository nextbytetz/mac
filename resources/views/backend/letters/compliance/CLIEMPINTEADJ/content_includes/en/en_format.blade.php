<div style="text-align: justify !important;" class="div_break">Kindly refer to the above subject matter which informed the Fund that you wish to review interest rectification.</div>
<div>&nbsp;</div>


<div style="text-align: justify !important;" class="div_break">We wish to inform you that based on the current information availed to the Fund, your request to correct interest has been approved and interest has been rectified. New interest amount is {{ number_2_format($booking_interest->new_interest_details['interest_amount']) }} and late months is  {{ $booking_interest->new_interest_details['late_months'] }}.
    </div>

<div>&nbsp;</div>

<div style="text-align: justify !important;" class="div_break"> </div>

<div>&nbsp;</div>

<div>&nbsp;</div>
<div style="text-align: justify !important;" class="div_break">Thank you for your cooperation.</div>