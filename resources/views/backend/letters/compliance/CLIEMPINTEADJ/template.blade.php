<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/letter/standard.css") }}
</head>
<body>


@php

    $employer_name = proper_case_word($employer->name);
    $employer_regno = append_zero_before_num($employer->id,6);
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $nowCarbon = \Carbon\Carbon::now();
    $booking_interest_app_date_carbon = \Carbon\Carbon::parse($booking_interest->application_date);
     $booking_interest_app_date = $booking_interest_app_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
    $approved_date_carbon = \Carbon\Carbon::parse($booking_interest->wf_done_date);
    $approved_date = $approved_date_carbon->format("d<\s\u\p>S</\s\u\p> F Y");
        $letterDateCarbon = \Carbon\Carbon::parse($letter->letter_date_label);
    $letter_date = $letterDateCarbon->format("d<\s\u\p>S</\s\u\p> F Y");

//$has_arrears = ($closure->arrears > 0 || $closure->interest_amount > 0) ? 1 : 0;
    $letter_ref_prefix =  ($letter->lang == 'en') ? 'REF:' : 'YAH:';
$lang = $letter->lang;
@endphp

{{--Top Part--}}
@if($letter->lang == 'en')
    @include('backend/letters/includes/top_part/top_en', ['letter_reference_no' => $letter->reference, 'letter_date' => $letter_date ])
@else
    @include('backend/letters/includes/top_part/top_sw',  ['letter_reference_no' => $letter->reference, 'letter_date' => $letter_date ])
@endif



<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{ $letter->salutation }},</div>
            <div>{{ $employer_name }},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            <div>&nbsp;</div>
            <div><strong> {{ $letter_ref_prefix . ' ' .    $letter_reference }} </strong></div>
            <div>&nbsp;</div>
            {{--Start content--}}

            @if($lang == 'en')
                @include('backend/letters/compliance/CLIEMPINTEADJ/content_includes/en/en_format')
            @else
                @include('backend/letters/compliance/CLIEMPINTEADJ/content_includes/sw/sw_format')
            @endif

            {{--end content--}}
            <div>&nbsp;</div>
            <div class="div_break">
                @include("backend/letters/compliance/includes/signature")

                {{--<div>&nbsp;&nbsp;&nbsp;<strong>Cc:&nbsp;</strong>{{ ucwords(strtolower($gender_title)) }}. {{ ucwords(strtolower($employee_name)) }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $employer_name }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $letter->box_no }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>{{ strtoupper($letter->location) }}.</strong></div>
                <div>&nbsp;</div>--}}

            </div>
        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
<script type="text/javascript">
    @if ($print)
        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function(){
        $(document).on("contextmenu",function(e){
            return false;
        });
    });
</script>
</body>
</html>