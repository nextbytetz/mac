<div class = "row">
    <div class="col-md-12 col-sm-12">
        <h5 class="client-title">
            <i class="icon-circle"></i>
            <!-- Employer header detail -->
            <strong>{{ $letter->codeValue->name }}</strong>
            <small>
                Reference : <span class="underline">{{ $letter->reference }}</span>
            </small>
            <small>
                Status&nbsp;:&nbsp;<span>{!! $letter->status_label !!}</span>
            </small>
            <small>
                Print Status&nbsp;:&nbsp;<span>{!! $letter->print_status_label !!}</span>
            </small>
        </h5>
    </div>
</div>