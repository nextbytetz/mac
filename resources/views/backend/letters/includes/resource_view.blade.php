{{--<div data-plugin="custom-scroll" data-height="500">--}}
<div>
    <table class="table table-striped table-bordered">
        <tbody>
        @foreach($resources as $resource)
            <tr>
                <td><b>{{ $resource['title'] }}</b></td>
                <td><a href="{{ url("/") . $resource['url'] }}">{{ $resource['value'] }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>