


<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">WORKERS COMPENSATION FUND</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 100%; vertical-align: top;" colspan="2">
                        <div style="text-align: left;">Telegraphic address "WCF"</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Tel:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Fax:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Email:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Web:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: middle; text-align: center;"><img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>P.O. Box 79655</div>
            <div>GEPF House</div>
            <div>Plot No. 37</div>
            <div>Regent Estate</div>
            <div>Bagamoyo Road</div>
            <div>Dar es Salaam</div>
        </td>
    </tr>
    </tbody>
</table>


<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="0">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>In reply please quote:&nbsp;</strong></div>
                <div>&nbsp;</div>
                <div><strong>Re.No: {{ $letter_reference_no }}</strong></div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;"><strong>{!! $letter_date !!}</strong></td>
        </tr>
        </tbody>
    </table>
</div>
<div>&nbsp;</div>