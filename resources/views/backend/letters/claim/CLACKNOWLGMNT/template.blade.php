<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/letter/standard.css") }}
</head>
<body>
<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">WORKERS COMPENSATION FUND</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 100%; vertical-align: top;" colspan="2">
                        <div style="text-align: left;">Telegraphic address "WCF"</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Tel:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Fax:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Email:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Web:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: middle; text-align: center;"><img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>P.O. Box 79655</div>
            <div>GEPF House</div>
            <div>Plot No. 37</div>
            <div>Regent Estate</div>
            <div>Bagamoyo Road</div>
            <div>Dar es Salaam</div>
        </td>
    </tr>
    </tbody>
</table>

@php
    $extras = json_decode($letter->extras, true);
    $incident_name = strtolower($incident->incidentType->name);
    $employee_name = $incident->employee->name;
    $employer_name = ucwords(strtolower($incident->employer->name));
    $employer_name_formatted = (isset($extras['employername'])) ? nl2br($extras['employername']) : $employer_name;
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $incident_action = ($incident->incident_type_id == 2) ? "was diagnosed" : "occured";
    $letterDateCarbon = \Carbon\Carbon::parse($letter->letter_date_label);
    $letter_date = $letterDateCarbon->format("d<\s\u\p>S</\s\u\p> F, Y");
@endphp

<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="0">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>In reply please quote:&nbsp;</strong></div>
                <div>&nbsp;</div>
                <div><strong>Re.No: {{ $letter->reference }}</strong></div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;"><strong>{!! $letter_date !!}</strong></td>
        </tr>
        </tbody>
    </table>
</div>
<div>&nbsp;</div>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{ $letter->salutation }},</div>
            <div>{!! $employer_name_formatted !!},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            <div>&nbsp;</div>
            <div><strong>RE: NOTIFICATION OF OCCUPATIONAL {{ strtoupper($incident_name) }} - {{ strtoupper($gender_title) }}. {{ strtoupper($employee_name) }}</strong></div>
            <div>&nbsp;</div>
            @if ($cases['case5'])
                <div style="text-align: justify !important;">Please refer to notification of {{ strtolower($incident_name) }} (WCN-1) submitted to the fund on {{ \Carbon\Carbon::parse($incident->receipt_date)->format("d") }}<sup style='font-size: 11px;'>{{ \Carbon\Carbon::parse($incident->receipt_date)->format("S ") }}</sup>{{ \Carbon\Carbon::parse($incident->receipt_date)->format("F Y") }}.</div>
                <div>&nbsp;</div>
                <div style="text-align: justify !important;">
                    The Fund would like to acknowledge receiving the required documents pertaining to the claim of {{ ucwords(strtolower($employee_name)) }}. We are assessing the claim and you will be notified after the completion of the process.
                </div>
                <div>&nbsp;</div>
            @else
                <div style="text-align: justify !important;">Please refer to the above caption.</div>
                <div>&nbsp;</div>
                <div style="text-align: justify !important;">The Fund would like to acknowledge receiving the notification of {{ strtolower($incident_name) }} (WCN 1) on {{ \Carbon\Carbon::parse($incident->receipt_date)->format("d") }}<sup style="font-size: 11px;">{{ \Carbon\Carbon::parse($incident->receipt_date)->format("S ") }}</sup>{{ \Carbon\Carbon::parse($incident->receipt_date)->format("F Y") }} with respect to the {{ $incident_name }} which {{ $incident_action }} on {{ \Carbon\Carbon::parse($incident->incident_date)->format("d") }}<sup style="font-size: 11px;">{{ \Carbon\Carbon::parse($incident->incident_date)->format("S ") }}</sup>{{ \Carbon\Carbon::parse($incident->incident_date)->format("F Y") }} to your employee, {{ ucwords($gender_title) }}. {{ ucwords(strtolower($employee_name)) }}. @if ($cases['case2']) The reported {{ strtolower($incident_name) }} will be investigated and validated as part of the compensation processing procedures. We will communicate to your office the date on which the Fund's staff will visit your organisation to conduct the investigation.@endif @if ($cases['case3'])The reported {{ strtolower($incident_name) }} will be validated as part of the compensation processing procedures.@endif @if ($cases['case4'])The reported {{ strtolower($incident_name) }} will be investigated and validated as part of the compensation processing procedures. We will communicate to your office the date on which the Fund's staff will visit your organisation to conduct the investigation.@endif</div>
                <div>&nbsp;</div>
                @if ($cases['case1'])
                    <div>{{--Case1--}}</div>
                    @if ($benefit_documents->count())
                        <div style="text-align: justify !important;">You are kindly requested to submit the following documents in order to complete compensation payment process:</div>
                        <ol style="list-style-type: lower-roman;">
                            @foreach($benefit_documents as $benefit_document)
                                <li>&nbsp;{{ $benefit_document->name }}&nbsp;{{ ($benefit_document->description) ? "( {$benefit_document->description} )" : '' }}</li>
                            @endforeach
                        </ol>
                    @endif
                    <div style="text-align: justify !important;">It is our anticipation that you will cooperate with the Fund to finalize the claim processing of the referred employee.{{--endCase1--}}</div>
                @endif
                @if ($cases['case2'])
                    <div>{{--startCase2--}}</div>
                    @if ($benefit_documents->count())
                        <div style="text-align: justify !important;">Upon successful validation you may be required to submit the following documents for further compensation procedures</div>
                        <ol style="list-style-type: lower-roman;">
                            @foreach($benefit_documents as $benefit_document)
                                <li>&nbsp;{{ $benefit_document->name }}&nbsp;{{ ($benefit_document->description) ? "( {$benefit_document->description} )" : '' }}</li>
                            @endforeach
                        </ol>
                    @endif
                    {{--<div style="text-align: justify !important;">We will communicate to your office the date on which the Fund's staff will visit your organisation to conduct the investigation.</div>--}}{{--endCase2--}}
                @endif
                @if ($cases['case3'])
                    <div>{{--startCase3--}}</div>
                    @if ($validation_documents->count())
                        <div style="text-align: justify !important;">You are kindly requested to submit the following documents for validation purpose;</div>
                        <ol style="list-style-type: lower-roman;">
                            @foreach($validation_documents as $validation_document)
                                <li>&nbsp;{{ $validation_document->name }}&nbsp;{{ ($validation_document->description) ? "( {$validation_document->description} )" : '' }}</li>
                            @endforeach
                        </ol>
                    @endif
                    @if ($benefit_documents->count())
                        <div style="text-align: justify !important;">Upon successful validation you may be required to submit the following documents for further compensation procedures;</div>
                        <ol style="list-style-type: lower-roman;">
                            @foreach($benefit_documents as $benefit_document)
                                <li>&nbsp;{{ $benefit_document->name }}&nbsp;{{ ($benefit_document->description) ? "( {$benefit_document->description} )" : '' }}</li>
                            @endforeach
                        </ol>
                    @endif
                    {{--endCase3--}}
                @endif
                @if ($cases['case4'])
                    {{--startCase4--}}
                    @if ($validation_documents->count())
                        <div style="text-align: justify !important;">You are kindly requested to submit the following documents for validation purpose</div>
                        <ol style="list-style-type: lower-roman;">
                            @foreach($validation_documents as $validation_document)
                                <li>&nbsp;{{ $validation_document->name }}&nbsp;{{ ($validation_document->description) ? "( {$validation_document->description} )" : '' }}</li>
                            @endforeach
                        </ol>
                    @endif
                    @if ($benefit_documents->count())
                        <div style="text-align: justify !important;">Upon successful validation you may be required to submit the following documents for further compensation procedures</div>
                        <ol style="list-style-type: lower-roman;">
                            @foreach($benefit_documents as $benefit_document)
                                <li>&nbsp;{{ $benefit_document->name }}&nbsp;{{ ($benefit_document->description) ? "( {$benefit_document->description} )" : '' }}</li>
                            @endforeach
                        </ol>
                    @endif
                    {{--<div style="text-align: justify !important;">We will communicate to your office the date on which the Fund's staff will visit your organisation to conduct the investigation.</div>--}}{{--endCase4--}}
                @endif
            @endif

            {{--<div>&nbsp;</div>--}}
            <div style="text-align: justify !important;">For more information, please do not hesitate to contact us through our customer toll free numbers <strong>0800110028/0800110029</strong>.</div>
            <div>&nbsp;</div>
            <div>Thank you for your cooperation.</div>
            <div>&nbsp;</div>
            <div class="div_break">
                <div>&nbsp;&nbsp;<img src="{{ public_url() }}/template/assets/nextbyte/img/cadm_signature.png" width="140" height="50" /></div>
                <div>&nbsp; &nbsp; Rehema A. Kabongo</div>
                <div><strong>FOR: DIRECTOR GENERAL</strong></div>
                <div>&nbsp;</div>

                @if ($incident->incident_type_id != 3)
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                            <td style="width: 6%;text-align: right;vertical-align: top;">
                                <div><strong>Cc:&nbsp;</strong></div>
                            </td>
                            <td style="width: 60%; text-align: justify;">
                                <div>{{ ucwords(strtolower($gender_title)) }}. {{ ucwords(strtolower($employee_name)) }},</div>
                                <div>{!! $employer_name_formatted !!},</div>
                                <div>{{ $letter->box_no }},</div>
                                <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
                            </td>
                            <td style="width: 30%; text-align: justify;vertical-align: bottom;">
                                {{--<div><strong>Pokea kwa Taarifa</strong></div>--}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div>&nbsp;</div>
                @endif

{{--                @if ($incident->incident_type_id != 3)
                    <div>&nbsp;&nbsp;&nbsp;<strong>Cc:&nbsp;</strong>{{ ucwords(strtolower($gender_title)) }}. {{ ucwords(strtolower($employee_name)) }},</div>
                    <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $employer_name_formatted !!},</div>
                    <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $letter->box_no }},</div>
                    <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>{{ strtoupper($letter->location) }}.</strong></div>
                    <div>&nbsp;</div>
                @endif--}}

                @if ($benefit_documents->count() Or $validation_documents->count())
                    <div><strong>Enclosed:</strong> WCF forms</div>
                @endif

            </div>

        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
<script type="text/javascript">
    @if ($print)
    window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function(){
        $(document).on("contextmenu",function(e){
            return false;
        });
    });
    /* window.onfocus=function(){ window.close();} */
</script>
</body>
</html>