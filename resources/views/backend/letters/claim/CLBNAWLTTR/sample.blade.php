<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    <style>
        @media print {
            /* All your print styles go here */
            #header, #footer, #nav { display: none !important; }
        }
        @media all {
            @page {
                /*size: A4 portrait !important; !* 210 × 297 millimeters *!*/
                size: A4 portrait !important;
            }
            footer {
                position: fixed; bottom: 100px; left: 0px; right: 0px; font-family: Arial, sans-serif; /*background-color: lightblue; height: 50px; */
            }
            .div_break {
                page-break-inside: avoid !important;
            }
            body {
                margin: 5px;
                padding: 15px;
            }
            * {
                font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
                font-size: 12pt;

            }
            td {
                padding: 0px;
            }
            html {
                width: 210mm;
                height: 297mm;
            }
            table {
                border-collapse: collapse;
                width: 100%;
                border: 0px #ffffff;
            }
        }
    </style>
</head>
<body>
<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">MFUKO WA FIDIA KWA WAFANYAKAZI</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Anuani:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span>"WCF"</span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Simu:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Nukushi:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Barua Pepe:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Tovuti:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: middle; text-align: center;"><img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>S.L.P 79655</div>
            <div>Jengo la GEPF</div>
            <div>Kitalu Na. 37</div>
            <div>Barabara ya Bagamoyo</div>
            <div>Regent Estate</div>
            <div>Dar es Salaam</div>
        </td>
    </tr>
    </tbody>
</table>

@php
    $incident_name = strtolower($incident->incidentType->name);
    $employee_name = $incident->employee->name;
    $employer_name = ucwords(strtolower($incident->employer->name));
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $letter_date = sw_date();
    $incident_date = sw_date($incident->incident_date);
@endphp

<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="0">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>Unapojibu tafadhali taja:&nbsp;</strong></div>
                <div>&nbsp;</div>
                <div><strong>Kumb. Na: {{ $letter->reference }}</strong></div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;"><strong>{{ $letter_date }}</strong></td>
        </tr>
        </tbody>
    </table>
</div>
<div>&nbsp;</div>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{ $letter->salutation }},</div>
            <div>{!! $employer_name !!},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            <div>&nbsp;</div>

            {{--<div><strong>YAH: MALIPO YA FIDIA YA ULEMAVU WA MUDA MFUPI WA NDUGU {{ strtoupper($employee_name) }}</strong></div>--}}
            {{--<div><div style="display: inline;float: left;"><strong>YAH:</strong></div><div style="display: inline;float: right;"><strong>MALIPO YA FIDIA YA ULEMAVU WA MUDA MFUPI WA NDUGU {{ strtoupper($employee_name) }}</strong></div></div>--}}

            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td style="width: 6%;text-align: justify;vertical-align: top;">
                            <div><strong>YAH:</strong></div>
                        </td>
                        <td style="width: 94%; text-align: justify;">
                            <div><span><strong>MALIPO YA FIDIA YA ULEMAVU WA MUDA MFUPI WA NDUGU {{ strtoupper($employee_name) }}</strong></span></div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div>&nbsp;</div>
            <div style="text-align: justify !important;">
                Rejea somo tajwa hapo juu na taarifa ya tukio la ajali kazini iliyotokea tarehe {{ $incident_date }}.
            </div>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
                The first paragraph goes here
            </div>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">The second paragraph with listing goes here</div>
            <ol style="list-style-type: decimal;">
                @foreach([1,2,3] as $value)
                    <li>&nbsp;{{ $value }}</li>
                @endforeach
            </ol>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
                Tafadhali zingatia kuwa malipo haya hayapaswi kulipia deni endapo ndugu {{ ucwords($employee_name) }} alikuwa na deni lolote.
            </div>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
                Nashukuru kwa ushirikiano wako.
            </div>
            <div>&nbsp;</div>

            <div class="div_break">
                <div>&nbsp;&nbsp;<img src="{{ public_url() }}/template/assets/nextbyte/img/benefit_award_signature.png" width="140" height="50" /></div>
                <div>&nbsp; &nbsp; Masha J. Mshomba</div>
                <div><strong>MKURUGENZI MKUU</strong></div>
                <div>&nbsp;</div>

                <div><strong>Nakala:&nbsp;</strong></div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ ucwords(strtolower($employee_name)) }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $employer_name }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $letter->box_no }},</div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>{{ strtoupper($letter->location) }}.</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Pokea kwa Taarifa</strong></div>
                <div>&nbsp;</div>

                {{--<div><strong>Enclosed:</strong> WCF forms</div>--}}

            </div>
        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}

<script type="text/javascript">
    @if ($print)
        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function() {
        $(document).on("contextmenu", function(e) {
            return false;
        });
    });
    /* window.onfocus=function(){ window.close();} */
</script>

</body>
</html>