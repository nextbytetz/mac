<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/letter/benefit_award.css") }}
</head>
<body>
<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">MFUKO WA FIDIA KWA WAFANYAKAZI</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Anuani:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span>"WCF"</span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Simu:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Nukushi:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Barua Pepe:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Tovuti:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: middle; text-align: center;"><img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>S.L.P 79655</div>
            <div>Jengo la Victoria</div>
            <div>Kitalu Na. 37</div>
            <div>Barabara ya Bagamoyo</div>
            <div>Regent Estate</div>
            <div>Dar es Salaam</div>
        </td>
    </tr>
    </tbody>
</table>

@php
    $extras = json_decode($letter->extras, true);
    $incident_name = strtolower($incident->incidentType->name);
    $employee_name = strtolower($incident->employee->name);
    $employer_name = isset($extras['employername']) ? nl2br($extras['employername']) : ucwords(strtolower($incident->employer->name));
    $gender_title = ($letter->gender_id == 1) ? "mr" : "ms";
    $letter_date = sw_date($letter->letter_date_label);
    $incident_date = sw_date($incident->incident_date);
@endphp

<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="0">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>Unapojibu tafadhali taja:&nbsp;</strong></div>
                <div>&nbsp;</div>
                <div><strong>Kumb. Na: {{ $letter->reference }}</strong></div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;"><strong>{{ $letter_date }}</strong></td>
        </tr>
        </tbody>
    </table>
</div>
<div>&nbsp;</div>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{ $letter->salutation }},</div>
            <div>{!! $employer_name !!},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            <div>&nbsp;</div>

            {{--<div><strong>YAH: MALIPO YA FIDIA YA ULEMAVU WA MUDA MFUPI WA NDUGU {{ strtoupper($employee_name) }}</strong></div>--}}
            {{--<div><div style="display: inline;float: left;"><strong>YAH:</strong></div><div style="display: inline;float: right;"><strong>MALIPO YA FIDIA YA ULEMAVU WA MUDA MFUPI WA NDUGU {{ strtoupper($employee_name) }}</strong></div></div>--}}

            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 6%;text-align: justify;vertical-align: top;">
                        <div><strong>YAH:</strong></div>
                    </td>
                    <td style="width: 94%; text-align: justify;">
                        <div><span><strong>MALIPO YA FIDIA YA ULEMAVU WA MUDA MFUPI WA NDUGU {{ strtoupper($employee_name) }}</strong></span></div>
                    </td>
                </tr>
                </tbody>
            </table>

            <div>&nbsp;</div>
            <div style="text-align: justify !important;">
                @if ($incident->incident_type_id == 1)
                    Rejea somo tajwa hapo juu na taarifa ya tukio la ajali kazini iliyotokea tarehe {{ $incident_date }}.
                @elseif ($incident->incident_type_id == 2)
                    Rejea somo tajwa hapo juu na taarifa ya ugonjwa uliotokana na kazi uliogundulika tarehe {{ $incident_date }}.
                @endif
            </div>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
                Mfuko umefanya tathmini ya @if ($incident->incident_type_id == 1)tukio la ajali husika @elseif($incident->incident_type_id == 2)taarifa ya ugonjwa husika @endif kwa kuzingatia taarifa za kitabibu na mshahara wa mwezi wa ndugu {{ ucwords($employee_name) }} wakati wa @if ($incident->incident_type_id == 1)tukio la ajali @elseif($incident->incident_type_id == 2)ugonjwa @endif. Kulingana na matakwa ya Sheria ya Fidia kwa Wafanyakazi Sura 263 [marejeo ya mwaka 2015] kifungu Na. 46 (1), napenda kukufahamisha kuwa Mfuko umeandaa tuzo ya kiasi cha shilingi <strong>{{ $benefits['td_amount'] }}</strong> ambapo shilingi <strong>{{ $benefits['ttd_amount'] }}</strong> zilizolipwa kwa hundi nambari <strong>{{ $extras['ttdchequeno'] }}</strong> ya tarehe {{ sw_date($extras['ttdchequedate']) }} ni tuzo ya ulemavu wa muda kamili wakati ndugu {{ ucwords($employee_name) }} akiwa katika kipindi cha mapumziko na shilingi <strong>{{ $benefits['tpd_amount'] }}</strong> zilizolipwa kwa hundi nambari <strong>{{ $extras['tpdchequeno'] }}</strong> ya tarehe {{ sw_date($extras['tpdchequedate']) }} ni tuzo ya ulemavu wa muda wa kiasi wakati akiwa katika kipindi cha kazi nyepesi.
            </div>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
                Tafadhali pokea hundi hiyo ambayo imeambatishwa pamoja na barua hii kama tuzo ya fidia ya ulemavu wa muda mfupi kwa ndugu {{ ucwords($employee_name) }}. Aidha, malipo haya yatafanyika kwa kuzingatia ufafanuzi ufuatao;
            </div>

            <div>
                <ol style="list-style-type: decimal;">
                    <li>
                        Shilingi <strong>{{ $benefits['td_amount'] }}</strong> ni tuzo ya fidia ya ulemavu wa muda mfupi kwa {{ ucwords($employee_name) }} na ambayo yote italipwa kwake endapo hakulipwa mshahara wowote kwa kipindi alipokuwa kwenye mapumziko ya ugonjwa na kazi nyepesi.
                    </li>
                    <li>
                        Endapo ndugu {{ ucwords($employee_name) }} alilipwa sehemu yoyote ya mshahara wake wakati akiwa kwenye mapumziko ya ugonjwa na kazi nyepesi, basi mshahara aliolipwa utapunguzwa kutoka kiwango cha fidia cha shilingi <strong>{{ $benefits['td_amount'] }}</strong>.
                    </li>
                    <li>
                        Hata hivyo, tunapenda kukufahamisha kuwa kama ndugu {{ ucwords($employee_name) }} alilipwa mshahara wakati wa mapumziko ya ugonjwa na kazi nyepesi, mwajiri wake anaweza kumlipa sehemu au kiwango chote cha fidia cha shilling <strong>{{ $benefits['td_amount'] }}</strong> kwa kadiri atakavyoona inafaa kwa kuwa ni hatua ya kujenga mazingira mazuri zaidi kazini. Aidha, Mfuko unawahamasisha waajiri kufanya hivyo ili kuimarisha mahusiano mazuri kati ya waajiri na wafanyakazi.
                    </li>
                </ol>
            </div>

        </td>
    </tr>
    </tbody>
</table>
<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">

            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
                Tafadhali zingatia kuwa malipo haya hayapaswi kulipia deni endapo ndugu {{ ucwords($employee_name) }} alikuwa na deni lolote.
            </div>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
                Nashukuru kwa ushirikiano wako.
            </div>
            <div>&nbsp;</div>

            <div class="div_break">
                @include("backend/letters/claim/CLBNAWLTTR/includes/signature")

                @include("backend/letters/claim/CLBNAWLTTR/includes/mf", ["gov" => 1])

                {{--<div><strong>Enclosed:</strong> WCF forms</div>--}}

            </div>
        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}

<script type="text/javascript">
    @if ($print)
        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function() {
        $(document).on("contextmenu", function(e) {
            return false;
        });
    });
    /* window.onfocus=function(){ window.close();} */
</script>

</body>
</html>