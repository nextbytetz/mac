<table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td style="width: 10%;text-align: justify;vertical-align: top;">
            <div><strong>Nakala:&nbsp;</strong></div>
        </td>
        <td style="width: 60%; text-align: justify;">
            @if ($gov)
                <div>&nbsp;</div>
                <div>Katibu Mkuu,</div>
                <div>Wizara ya Fedha na Mipango,</div>
                <div>Treasury Square Building,</div>
                <div>18 Jakaya Kikwete Road,</div>
                <div>S.L.P 743,</div>
                <div><strong>DODOMA.</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
            @endif
            <div>&nbsp;</div>
            @if (isset($ccemployer))
                <div>{{ $letter->salutation }},</div>
            @else
                <div>{{ ucwords(strtolower($employee_name)) }},</div>
            @endif
            <div>{!! $employer_name !!},</div>
            <div>{{ $letter->box_no }},</div>
            <div><strong>{{ strtoupper($letter->location) }}.</strong></div>
            @if (isset($extras['mf_salutation']))
                <div>&nbsp;</div>
                <div>{{ $extras['mf_salutation'] }},</div>
                <div>{{ $extras['mf_institution'] }},</div>
                <div>{{ $extras['mf_box'] }},</div>
                <div><strong>{{ strtoupper($extras['mf_location']) }}.</strong></div>
            @endif
        </td>
        <td style="width: 30%; text-align: justify;vertical-align: bottom;">
            <div><strong>Pokea kwa Taarifa</strong></div>
        </td>
    </tr>
    </tbody>
</table>