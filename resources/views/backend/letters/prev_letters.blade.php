<div class = "row">
    <div class="col-md-12">
        <br/>
        <div class="row">
            <div class="col-md-4">

                <legend>Letter(s)
                    @if ($letterscount)
                        <a class="pull-right" style="color:blue; font-size: 12px"  href="{!! route('backend.letter.create', ["resource" => $resource, "reference" => $code_value->reference]) !!}">Create New</a>
                    @endif
                </legend>

                <br/>

                {{--new Bank--}}
                <div class="row">
                    <div class="col-md-12">

                        <div class="element-form" >

                            @if ($letterscount)
                                @foreach($letters as $letter)
                                    {{--@php
                                        $document = $doc->document;
                                    @endphp--}}
                                    {{--<li>--}}
                                    <i class="fa fa-file-pdf-o" ></i>
                                    <a  style="color:dodgerblue;" class="letter_attached"  href="#" data-url="@if (!$letter->pdfready) {{ route("backend.letter.preview", $letter->id) }} @else {{ letters_path() . "/" . $letter->id . ".pdf" }}  @endif">{{ $letter->reference }}</a>
                                    |
                                    <a class="" style="color:grey"  href="{!! route('backend.letter.show', $letter->id) !!}">Profile</a>
                                    {{--<span style="font-size: 12px; color:#414141"> {!!   '' . ' : ' . $pollOption->votes  !!}   </span>--}}
                                    {{--<span style="font-size: 12px; color:#414141"> {!!   $pollOption->vote_percent_label  !!} </span>--}}
                                    {{--</li>--}}
                                    <br/>
                                @endforeach

                            @else
                                <span class="tag tag-success">No Previous Letter</span>
                            @endif

                        </div>
                        <div>&nbsp;</div>
                        <div class="element-form">
                            @if ($hasold)
                                <i class="fa fa-file-pdf-o" ></i>
                                <a  style="color:dodgerblue;" class="letter_attached"  href="#" data-url="{{ route("backend.letter.preview_legacy", ['reference' => $reference, 'resource' => $resource]) }}">{{ $code_value->name }}</a>
                            @else
                                <span class="tag tag-success">No Legacy Letter</span>
                            @endif
                        </div>

                    </div>


                </div>

            </div>

            <div class="col-md-8">
                <div class = "row">
                    <div class="col-md-12">
                        {{--Letter Preview--}}
                        <legend>Letter Preview</legend>
                        <br/>
                        {{--Print Letter--}}
                        <span>
                            <a href="#"  class="btn btn-primary site-btn nav_button print_letter" target="_blank">
                                    <i class="icon fa fa-print" aria-hidden="true"></i>
                                Print</a>
                        </span>
                        <div id="letter_frame" style="text-align: center;">
                            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

@push('after-script-end')

    <script  type="text/javascript">

        $(function () {
            /*Documents which pending to be used list*/
            $(".print_letter").click(function() {
                window.frames["letter_preview"].focus();
                window.frames["letter_preview"].print();
            });
            $(".letter_attached").click(function() {
                let $url = $(this).attr('data-url');
                let $letter_frame = $("#letter_frame");
                $letter_frame.find("iframe").remove();
                let $iframe = $('<iframe src="' + $url + '" frameborder="0" name="letter_preview" width=\'100%\' height=\'600px\'></iframe>');
                $letter_frame.append($iframe);
            });
        });
    </script>

@endpush
