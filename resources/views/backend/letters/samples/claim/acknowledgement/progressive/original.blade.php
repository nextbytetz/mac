<!DOCTYPE html>
<html>
<head>
</head>
<body>
<table style="width: 100%;" border="1">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table style="border-collapse: collapse; width: 100%; border: 0px #ffffff;" border="none" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 100%; vertical-align: top;" colspan="2">
                        <div style="text-align: left;">Telegraphic address "WCF"</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Tel:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Fax:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Email:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Web:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: top; text-align: center;">&nbsp;</td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>P.O. Box 79655</div>
            <div>GEPF House</div>
            <div>Plot No. 37</div>
            <div>Regent Estate</div>
            <div>Bagamoyo Road</div>
            <div>Dar es Salaam</div>
        </td>
    </tr>
    </tbody>
</table>
<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="1">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>In reply please quote:&nbsp;</strong></div>
                <div>Ref. No: {AB1/457/1179/03}</div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;">{1st November 2017 - todays date}</td>
        </tr>
        </tbody>
    </table>
</div>
<table style="border-collapse: collapse; width: 100%;" border="1">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>Managing Director,</div>
            <div>{Company name}</div>
            <div>{P.O. Box}</div>
            <div>{District - Region}</div>
            <div>&nbsp;</div>
            <div>RE: NOTIFICATION OF OCCUPATIONAL ACCIDENT - {MR/MS}. {employee}</div>
            <div>&nbsp;</div>
            <div>The Fund would like to acknowledge receiving the notification of {incident} (WCN 1) on {receive date 20th October 2017} in respect of {Mr/Ms}. {employee}. {{--startIfCase2--}} The reported {incident} will be investigated and validated as part of the compensation processing procedures.{{--endIfCase2--}} {{--startIfCase3--}}The reported {incident} will be validated as part of the compensation processing procedures.{{--endIfCase3--}} {{--startIfCase4--}}The reported {incident} will be investigated and validated as part of the compensation processing procedures.{{--endIfCase4--}}</div>
            <div>&nbsp;</div>
            <div>{{--Case1--}}</div>
            <div>You are kindly requested to submit the following documents in order to complete compensation payment process:</div>
            <ol style="list-style-type: lower-roman;">
                <li>&nbsp;</li>
            </ol>
            <div>It is our anticipation that you will cooperate with the Fund to finalize the claim processing of the referred employee.{{endCase1}}</div>
            <div>{{--startCase2--}}</div>
            <div>Upon successful validation you may be required to submit the following documents for further compensation procedures</div>
            <ol style="list-style-type: lower-roman;">
                <li>&nbsp;</li>
            </ol>
            <div>We will visit your office within 30 working days from the date of receiving your notification to conduct investigation.{{endCase2}}</div>
            <div>{{--startCase3--}}</div>
            <div>You are kindly requested to submit the following documents for validation purpose;</div>
            <ol style="list-style-type: lower-roman;">
                <li>&nbsp;</li>
            </ol>
            <div>Upon successful validation you may be required to submit the following documents for further compensation procedures;</div>
            <ol style="list-style-type: lower-roman;">
                <li>&nbsp;</li>
            </ol>
            {{--endCase3--}} {{--startCase4--}}
            <div>You are kindly requested to submit the following documents for validation purpose</div>
            <ol style="list-style-type: lower-roman;">
                <li>&nbsp;</li>
            </ol>
            <div>Upon successful validation you may be required to submit the following documents for further compensation procedures</div>
            <ol style="list-style-type: lower-roman;">
                <li>&nbsp;</li>
            </ol>
            <div>We will visit your office within 30 working days from the date of receiving your notification to conduct investigation.{{--endCase4--}}</div>
            <div>&nbsp;</div>
            <div>For more information, please do not hesitate to contact us through our customer toll free numbers <strong>0800110028/0800110029</strong>.</div>
            <div>&nbsp;</div>
            <div>Thank you for your cooperation.</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp; &nbsp; Rehema A. Kabongo</div>
            <div><strong>FOR: DIRECTOR GENERAL</strong></div>
            <div>&nbsp;</div>
            <div><strong>Cc: </strong>{Mr/Ms}. {employee}</div>
            <div>&nbsp; &nbsp; &nbsp; {Company Name}</div>
            <div>&nbsp; &nbsp; &nbsp; {P.O. Box}</div>
            <div>&nbsp; &nbsp; &nbsp; {District - Region}</div>
            <div>&nbsp;</div>
            <div><strong>Enclosed:</strong> WCF forms</div>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>