{!! Form::model($letter, ['route' => ['backend.letter.updatedispatch', $letter->id], 'name' => 'update_dispatch_info', 'class' => 'update_dispatch_info', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="fileld-layout">
            <label class="required">Letter Date</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('letter_date', null , ['class' => 'form-control datepicker',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off', 'required' => 'required']) !!}
                </div>
                <span class="help-block">
                    {{--<small>Reference of the letter</small>--}}
                </span>
            </div>
        </div>
        <div class="fileld-layout">
            <label>Letter Reference</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('reference', null , ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off', 'required' => 'required']) !!}
                </div>
                <span class="help-block">
                    {{--<small>Reference of the letter</small>--}}
                </span>
            </div>
        </div>
        <div class="fileld-layout">
            <label>Dispatch Number</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('dispatch_number', null , ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                </div>
                <span class="help-block">
                    {{--<small>Reference of the letter</small>--}}
                </span>
            </div>
        </div>
        <div class="fileld-layout">
            <label>Delivery Date</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('delivery_date', null , ['class' => 'form-control datepicker',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                </div>
                <span class="help-block">
                    {{--<small>Reference of the letter</small>--}}
                </span>
            </div>
        </div>
        <div class="fileld-layout">
            <label>Received By</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('received_by', null , ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                </div>
                <span class="help-block">
                    {{--<small>Reference of the letter</small>--}}
                </span>
            </div>
        </div>
        <div class="fileld-layout">
            <label>Receiver Phone Number</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('receiver_phone_number', null , ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                </div>
                <span class="help-block">
                    {{--<small>Reference of the letter</small>--}}
                </span>
            </div>
        </div>
        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-submit" value="Update" />
        </div>
    </div>
</div>
<div>&nbsp;</div>

{!! Form::close() !!}