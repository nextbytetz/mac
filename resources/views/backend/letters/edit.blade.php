@extends('layouts.backend.main', ['title' => "Modify Letter", 'header_title' => "Modify Letter"])

@include('backend.includes.assets.datetimepicker')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */
        .progress-bar {
            background-color: #12CC1A;
            height:20px;
            color: #FFFFFF;
            width:0%;
            -webkit-transition: width .3s;
            -moz-transition: width .3s;
            transition: width .3s;
        }
        .progress-div {
            border:#0FA015 1px solid;
            padding: 5px 0px;
            margin:30px 0px;
            border-radius:4px;
            text-align:center;
        }
        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        <nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
            <span class="navbar-brand mb-0 h5"><span class="underline">{!! $letter->codeValue->name !!}</span></span>
            <span class="navbar-brand mb-0 h5"><strong><a href="{{ url("/") . $letter_info['profile_url'] }}">{{ $letter_info['name'] }}</a></strong></span>
            <span class="navbar-brand mb-0 h5"> Recipient&nbsp;:&nbsp;<small class="underline"> {{ $letter_info['recipient'] }}</small></span>
        </nav>
        <br/>
    </div>
    <legend></legend>



    <div class="row">
        {{--HEADER--}}
        {{--header--}}
        <div>&nbsp;<br/></div>
        {!! Form::model($letter, ['route' => ['backend.letter.update', $letter->id], 'name' => 'update_letter', 'class' => 'update_letter', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('option', $letter->option) !!}
        <div class="col-md-12">
            <div class="row">

                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            {{--reference--}}
                            <div class="fileld-layout">
                                <label>Letter Reference</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('reference', null , ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off', 'required' => 'required']) !!}
                                    </div>
                                    <span class="help-block">
                                        <small>Reference of the letter</small>
                                    </span>
                                </div>
                            </div>
                            {{--box_no--}}
                            <div class="fileld-layout">
                                <label>Postal Box</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('box_no', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off', 'required' => 'required']) !!}
                                    </div>
                                    <span class="help-block">
                                        <small>Post box number e.g. P.O. Box 112</small>
                                    </span>
                                </div>
                            </div>
                            {{--recipient gender--}}
                            <div class="fileld-layout">
                                <label>Recipient Gender</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::select('gender_id', [1 => "Male", 2 => "Female"],  null, ['class' => 'form-control search-select',  'style' => 'border-radius: 3px;width:30%;', 'placeholder' => '']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            {{--salutation--}}
                            <div class="fileld-layout">
                                <label>Salutation</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('salutation', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off', 'required' => 'required']) !!}
                                    </div>
                                    <span class="help-block">
                                    <small>E.g. Managing Director</small>
                                </span>
                                </div>
                            </div>
                            {{--location--}}
                            <div class="fileld-layout">
                                <label>Recipient Location</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('location', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off', 'required' => 'required']) !!}
                                    </div>
                                    <span class="help-block">
                                    <small>E.g. Ilala - Dar es Salaam</small>
                                </span>
                                </div>
                            </div>

                            <div class="fileld-layout">
                                <label>Template Language</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::select('lang', ['en' => "English", 'sw' => "Swahili"],  null, ['class' => 'form-control search-select',  'style' => 'border-radius: 3px;width:30%;', 'placeholder' => '']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        @if (!is_null($letter->extras))
                            <div class="col-md-12">
                                <div class="underline">Extra Information</div>
                                <br/>
                            </div>
                            @php
                                $extras = json_decode($letter->extras, true);
                                $array_count = count($letter_info['extras']);
                                $chunk_size = ($array_count > 1) ? $array_count/2 : 1;
                            @endphp
                            @foreach (array_chunk($letter_info['extras'], $chunk_size) as $perm)

                                <div class="col-md-6">

                                    @foreach ($perm as $p)
{{--
                                        @if ($p['data_type'] == "date")
                                            @php $attributes = ['class' => 'form-control datepicker',  'autocomplete' => 'off', 'required' => 'required'];  @endphp
                                        @else
                                            @php $attributes = ['placeholder' => '', 'class' => 'form-control', 'required' => 'required'];  @endphp
                                        @endif--}}

                                        <div class="fileld-layout">
                                            <label>{{ $p['title'] }}</label>
                                            <div class="form-group">
                                                <div class="input-group">

                                                    @if ($p['data_type'] == "date")
                                                        {{--@php $attributes = ['class' => 'form-control datepicker',  'autocomplete' => 'off', 'required' => 'required'];  @endphp--}}
                                                        {!! Form::text($p['name'], $extras[$p['name']] ?? null, ['class' => 'form-control datepicker',  'autocomplete' => 'off', 'required' => 'required']) !!}
                                                    @elseif ($p['data_type'] == "textarea")
                                                        {{--@php $attributes = ['class' => 'form-control autosize',  'autocomplete' => 'off', 'required' => 'required', 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;'];  @endphp--}}
                                                        {!! Form::textarea($p['name'], $extras[$p['name']] ?? null, ['class' => 'form-control autosize', 'spellcheck' => 'false',  'autocomplete' => 'off', 'required' => 'required', 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                                                    @else
                                                        {{--@php $attributes = ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'required' => 'required'];  @endphp--}}
                                                        {!! Form::text($p['name'], $extras[$p['name']] ?? null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'required' => 'required']) !!}
                                                    @endif


                                                   {{-- {!! Form::text($p['name'], $extras[$p['name']], $attributes) !!}--}}

                                                </div>
                                                <span class="help-block">
                                                    {{--<small>{{ $p['value'] }}</small>--}}
                                                </span>
                                            </div>
                                        </div>

                                    @endforeach

                                </div>

                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="col-md-4">
                    @include("backend/letters/includes/resource_view", ['resources' => $letter_info['resources']])
                    {{-- <div class="pull-right">
                        <a href="{{ $letter_info['profile_url'] }}" class="  btn-secondary site-btn">Cancel</a>
                        <input type="submit" class="btn btn-success btn-submit" value="Create" />
                    </div>--}}
                </div>

            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a href="{{ route("backend.letter.show", $letter->id) }}" class="  btn-secondary site-btn">Cancel</a>
                        <input type="submit" class="btn btn-success btn-submit" value="Update" />
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            /* start: Submitting Form and perform validation on the server side */
            $('body').on('submit', 'form.update_letter', function (e) {
                e.preventDefault();
                var $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    var $name = $(this).attr('name');
                    $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $($form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                var $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        document.location.href =  base_url + "/letter/show/" + $data.id;
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function(index, value) {
                            $($form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $($form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        });
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });
        });
    </script>
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush