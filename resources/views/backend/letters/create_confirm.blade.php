@extends('layouts.backend.main', ['title' => "Confirm Create", 'header_title' => "Confirm Create"])

@push('after-styles-end')

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        <nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
            <span class="navbar-brand mb-0 h5"><span class="underline">{!! $code_value->name !!}</span></span>
            <span class="navbar-brand mb-0 h5"><strong>{{ $letter_info['name'] }}</strong></span>
            <span class="navbar-brand mb-0 h5"> Recipient&nbsp;:&nbsp;<small class="underline"> {{ $letter_info['recipient'] }}</small></span>
        </nav>
        <br/>
    </div>
    <legend></legend>
    <br/>

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs">
                    {{--Letter Profile--}}
                    <li class="nav-item">
                        <a class="nav-link active" id="inspection_process_tab" data-toggle="tab" href="#letter_process" role="tab">
                            <i class="icon fa fa-paperclip" aria-hidden="true"></i>Process
                        </a>
                    </li>
                    {{--Document Centre--}}
                    <li class="nav-item">
                        <a class="nav-link" id="document_centre_tab" data-toggle="tab" href="#previous_letter" role="tab">
                            <i class="icon fa fa-history" aria-hidden="true"></i>Previous Letter(s)
                        </a>
                    </li>
                </ul>
                <div class="nav_tab_contain tab-content">
                    <div class="tab-pane active" id="letter_process" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <p style="text-align: center;">
                                    <span style="color: green;"><i class="icon fa fa-envelope fa-5x"></i></span>
                                </p>
                                <p style="font-size: 16px;">
                                    <strong>{{ $code_value->name }}</strong> has already been created and possibly dispatched,<br/>
                                    <strong>Please review previous letters before continuing</strong>
                                </p>
                                @if ($letterscount)
                                    <p style="font-size: 16px;">
                                        By clicking continue you are are agreeing to create the another letter.
                                    </p>
                                @endif
                                <div style="text-align: center;">
                                    {{ link_to($prevurl, "Cancel", ['class' => 'btn btn-secondary site-btn']) }}
                                    @if ($letterscount)
                                        {{ link_to_route('backend.letter.create', "Continue", ["resource" => $resource, "reference" => $code_value->reference], ['class' => 'btn site-btn save_button']) }}
                                    @endif
                                </div>
                            </div> <!-- /.col-md-8 -->
                        </div> <!-- /.row -->
                    </div>
                    <div class="tab-pane" id="previous_letter" role="tabpanel">
                        @include('backend/letters/prev_letters')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush