<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="modal-title">
        <div class="underline">
            {!! $assign_status['text'] !!}
        </div>
    </h4>
</div>
<div class="modal-body" style="font-size: 14px !important;">
    @if ($wf_track->status == 0)
        {{--Check if user has workflow access at this level--}}
        @if ($user_has_access)
            {!! Form::open(['route' => ['backend.workflow.update_workflow', $wf_track->id], 'name' => 'workflow_process_modal', "class" => "workflow_process_form".$wf_track->id]) !!}
            {!! Form::hidden('assigned', $wf_track->assigned) !!}
            {!! Form::hidden('isselective', $isselective) !!}
            {!! Form::hidden('isselectuser', $isselectuser) !!}
            {!! Form::hidden('is_optional', $is_optional, ['class' => 'is_optional']) !!}
            {{--Check if user has already participated in this workflow before--}}
            @if (!$has_participated)
                @if ($assign_status['status'])
                    {{--check if this level is restrictive, for auto-assigned workflows--}}
                    @if ($isrestrictive And ($wf_track->user_id != access()->id()))
                        <div class="alert alert-success" role="alert">
                            @lang("strings.backend.workflow.miss_restrictive_access")
                        </div>
                    @else
                        {!! Form::hidden('action', "approve_reject") !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="field-layout">
                                    <div class="form-group">
                                        <label class="required" for="status">@lang('labels.backend.workflow.action')</label>
                                        {!! Form::select('status', $statuses, null, ['class' => 'search-select workflow_status_select', 'style' => 'width:100%;border-radius:3px;height:32px;', 'data-track_id' => $wf_track->id]) !!}
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="field-layout reject_to_level">
                                    <div class="form-group">
                                        <label class="required" for="level">Level</label>
                                        {!! Form::select('level', $previous_levels, null, ['class' => 'reject_to_level_select','style'=>'width:100%;border-radius:3px;height:32px;', 'placeholder' => '']) !!}
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                @if ($isselectuser)
                                    <div class="field-layout select_next_user">
                                        <div class="form-group">
                                            <label class="required" for="select_user">Assign User</label>
                                            {!! Form::select('select_user', $select_users, null, ['class' => 'select_next_user_select','style'=>'width:100%;border-radius:3px;height:32px;', 'placeholder' => '']) !!}
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                @else
                                    {!! Form::hidden("select_user", 0) !!}
                                @endif
                                @if ($isselective)
                                    <div class="field-layout selective">
                                        <div class="form-group">
                                            <label class="required" for="level">Choose Level</label>
                                            {!! Form::select('wf_definition', $selective_levels, null, ['class' => 'selective_select','style'=>'width:100%;border-radius:3px;height:32px;', 'placeholder' => '']) !!}
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                @else
                                    {!! Form::hidden("wf_definition", 0) !!}
                                @endif
                                <div class="field-layout next_level_designation">
                                    <div class="form-group">
                                        <label><i class='icon fa fa-mail-forward' aria-hidden='true'></i>&nbsp;Following Level&nbsp;:&nbsp;<span class="underline next_level_designation_content">{{ $next_level_designation }}</span></label>
                                    </div>
                                    <div class="form-group">
                                        <label><i class='icon fa fa-list' aria-hidden='true'></i>&nbsp;Following Level&nbsp;Action:&nbsp;<span class="underline next_level_description_content">{{ $next_level_description }}</span></label>
                                    </div>
                                </div>
                                <div class="field-layout">
                                    <div class="form-group">
                                        <label for="comments">Remarks</label>
                                        {!! Form::textarea('comments', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary wf_action_close" data-dismiss="modal" data-resourceid="{{ $wf_track->resource_id }}">@lang("buttons.general.close")</button>
                            <button type="submit" class="btn btn-primary btn-submit">@lang("buttons.general.crud.update")</button>
                        </div>
                    @endif
                @else
                    {{--!$has_participated--}}
                    @if ($isrestrictive)
                        <div class="alert alert-success" role="alert">
                            @lang("strings.backend.workflow.miss_restrictive_access")
                        </div>
                    @else
                        @lang("strings.backend.workflow.assign?")
                        {!! Form::hidden('action', "assign") !!}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary wf_action_close" data-dismiss="modal" data-resourceid="{{ $wf_track->resource_id }}">@lang("buttons.general.close")</button>
                            <button type="submit" class="btn btn-primary btn-submit">@lang("buttons.general.confirm")</button>
                        </div>
                    @endif

                @endif
            @else
                <div class="alert alert-success" role="alert">
                    @lang('strings.backend.workflow.participated')
                </div>
            @endif
        @else
            <div class="alert alert-success" role="alert">
                @lang('strings.backend.workflow.miss_level_access')
            </div>
        @endif
    @elseif ($wf_track->status == 6)
        <div class="alert alert-success" role="alert">
            @lang('strings.backend.workflow.archived')
        </div>
    @else
        <div class="alert alert-success" role="alert">
            @lang('strings.backend.workflow.forwarded')
        </div>
    @endif
    {!! Form::close() !!}
</div>
