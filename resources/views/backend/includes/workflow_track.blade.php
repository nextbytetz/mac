{{--@include('backend.includes.toastr_assets')--}}
@push('after-styles-end')
{{ Html::style(asset_url() . '/nextbyte/plugins/sweetalert/css/sweetalert.css') }}
{{ Html::style(asset_url() . '/nextbyte/plugins/sweetalert/css/google.css') }}
@endpush

@php
$workflowId = "workflow_track_table" . $resource_id;
$completeWorkflowID = "completed_workflow" . $resource_id;
if (!isset($workflowScriptAlreadyIncluded)) {
    $workflowScriptAlreadyIncluded = false;
}
@endphp
<div class="row">
    <div class="col-md-12">
        <div id="{{ $completeWorkflowID }}"></div>
    </div>
</div>

<div id="archive_content{{ $resource_id }}"></div>
<legend></legend>

{!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%', 'id' => $workflowId], true) !!}

{{--Workflow modals--}}
<div class="modal hide fade" id="workflow_modal" role="dialog" aria-labelledby="workflow_modal" aria-hidden="true">
    <div class="modal-dialog white_modal" role="document">
        <div class="modal-content" id="modal-content">

        </div>
    </div>
</div>


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{!! $dataTable->scripts() !!}

<script>
    $(function() {
        initTable("{{ $completeWorkflowID }}", "{{ $resource_id }}", "{{ $wf_module_group_id }}", "{{ $type }}");
        let $archive_content = $('#archive_content{{ $resource_id }}');
        $.post( base_url + "/workflow/can_archive_workflow/{{ $resource_id }}/{{ $wf_module_group_id }}/{{ $type }}", {}, function( $data ) {
            if ($data.success) {
                $archive_content.empty();
                $archive_content.html($data.view);
            }
        }, "json").done(function($data) {
            if ($data.success) {
                addConfirmPostForms();
            }
        });
    });
</script>

<script>
    @if(!$workflowScriptAlreadyIncluded)
    function initTable($tableId, $resourceId, $wfModuleGroupId, $type) {
        let $div = '#' + $tableId;
        $.get( base_url + "/workflow/get_completed_wf_tracks/" + $resourceId + "/" + $wfModuleGroupId + "/" + $type, {}, function( $data ) {
            $( $div ).empty();
            $( $div ).html( $data );
        }, "html").done(function() {
            /* When Done */
                /*CORE_TEMP.function.PanelCollapse();
                CORE_TEMP.function.PanelFullscreen();*/
                //CORE_TEMP.function.Collapse();
                //CORE_TEMP.function.Close();
                CORE_TEMP.function.Collapse();
            });
    }
    function load_workflow_modal($id) {
        $.post("{!! route('backend.workflow.workflow_modal_content') !!}", {'wf_track_id': $id}, function (data) {
            $("#modal-content").empty();
            $(data).prependTo("#modal-content");
        }, "html").done(function () {
            /* $(".search-select").select2({}); */
            let $body = $("body");
            $body.off("change", ".workflow_status_select").on("change", ".workflow_status_select", function (e) {
                let $status = $(this).val();
                let $wf_track_id = $(this).attr("data-track_id");
                let $next_level_designation = $(".next_level_designation");
                let $reject_to_level = $(".reject_to_level");
                let $reject_to_level_select = $(".reject_to_level_select");
                let $selective = $(".selective");
                let $selective_select = $(".selective_select");
                // let $select_next_user = $form.find(".select_next_user");
                // let $select_next_user_select = $form.find(".select_next_user_select");
                let $next_level_designation_content = $(".next_level_designation_content");
                let $next_level_description_content = $(".next_level_description_content");
                switch($status) {
                    case '1':
                    case '4':
                    if ($status === "1") {
                        $selective.show();
                        $selective_select.prop( "disabled", false );
                    } else {
                        $selective.hide();
                        $selective_select.prop( "disabled", true );
                    }
                    $reject_to_level.hide();
                    $.post( base_url + "/workflow/next_level_designation/" + $wf_track_id + "/" + $status , {}, function( $data ) {
                        /*alert($data.skip);*/
                        if ($data.next_level_designation !== "") {
                            $next_level_designation.show();
                            $next_level_designation_content.empty();
                            $next_level_designation_content.html( $data.next_level_designation );
                            $next_level_description_content.html( $data.next_level_description );
                        } else {
                            $next_level_designation.hide();
                        }
                    }, "json").done(function($data) {});
                    break;
                    case '2':
                    $selective.hide();
                    $selective_select.prop( "disabled", true );
                    $next_level_designation.hide();
                    $reject_to_level.show();
                    $reject_to_level_select.prop( "disabled", false );
                    break;
                    default:
                    $selective.hide();
                    $selective_select.prop( "disabled", true );
                    $next_level_designation.hide();
                    $reject_to_level.hide();
                    $reject_to_level_select.prop( "disabled", true );
                    break;
                }
            });
            $('.workflow_status_select').trigger('change');
            autosize($("textarea.autosize"));
            $body.off('submit', 'form[name=workflow_process_modal]').on('submit', 'form[name=workflow_process_modal]', function(e) {
                e.preventDefault();
                let form = this;
                let $data = $(form).serialize();
                /* start: remove any printed error message in the input controls */
                $(form).find(':input').each(function () {
                    let $name = $(this).attr('name');
                    $("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                $.ajax({
                    data : $data,
                    dataType : "json",
                    method : "POST",
                    url : $(form).attr("action"),
                    beforeSend : function (e) {
                        $(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        if ($data.success) {
                            $("#workflow_modal").modal('hide');
                            $.amaran({
                                'theme'     :'awesome success',
                                'content'   :{
                                    title : "Success",
                                    message: $data.message,
                                    info:'',
                                    icon: 'fa fa-check-square-o'
                                },
                                'position'  :'bottom left',
                                'outEffect' :'slideBottom',
                                'inEffect'  :'slideLeft'
                            });
                            window.LaravelDataTables["workflow_track_table" + $data.resource_id].ajax.reload();
                            initTable("completed_workflow" + $data.resource_id, $data.resource_id, $data.wf_module_group_id, $data.type);

                                                        
                            if ($data.action == 'approve_reject') {
                                refreshInvestigationPage();
                                refreshInstalmentPage();
                            }
                        } else {
                            sweetAlert("Error Updating Workflow", $data.message);
                        }
                    },
                    error: function (data) {
                        let errors = $.parseJSON(data.responseText);
                        /* console.log(data); */
                        $.each(errors, function(index, value) {
                            $("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        });
                    },
                }).done(function() {
                }).fail(function() {
                }).always(function() {
                    $(".btn-submit").prop('disabled', false);
                });
            });
        }).fail(function () {

        }).always(function () {

        });
    }

    function refreshInvestigationPage() {
       let investigation_path_name = location.pathname;
       if (investigation_path_name.indexOf("/claim/investigations/profile/") >= 0){
        setTimeout(function(){ location.reload(); }, 1500);

    }
}

function refreshInstalmentPage() {
   let instalment_path_name = location.pathname;
   if (instalment_path_name.indexOf("/compliance/employer/online/view_installment") >= 0){
    setTimeout(function(){ location.reload(); }, 1500);

}
}
@endif

</script>

@endpush
