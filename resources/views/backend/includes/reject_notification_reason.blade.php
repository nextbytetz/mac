<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="modal-title">Notification Rejection</h4>
</div>
<div class="modal-body" style="font-size: 14px !important;">
    {!! Form::open(['route' => ['backend.claim.notification_report.reject', $notification_report->id], 'name' => 'reject_notification_form']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="field-layout">
                    <div class="form-group">
                        <label for="comments">Reason(s) for Rejecting</label>
                        {!! Form::textarea('comments', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'id' => 'reject_notification_comment']) !!}
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang("buttons.general.close")</button>
            <button type="submit" class="btn btn-primary btn-submit">Submit</button>
        </div>
    {!! Form::close() !!}
</div>
