



@push('after-script-end')

    {{ Html::script(asset_url() . "/nextbyte/plugins/ckeditor5/ckeditor.js") }}

    <script>
        $(function() {

            ClassicEditor.create( document.querySelector('.ckeditor') );

        });
    </script>
@endpush