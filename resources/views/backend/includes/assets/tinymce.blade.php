@push('after-styles-end')

@endpush

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/tinymce/tinymce.min.js") }}
    <script>
        $(function () {
            tinymce.init({
                selector:'textarea.editor',
                branding: false,
                elementpath: false,
                resize: true,
                height : 100,
                skin: 'lightgray',
                statusbar: true,
                images_upload_url: '',
                relative_urls: false,
                automatic_uploads: false,
                forced_root_block: false,
                menubar: false,
                plugins : 'advlist table lists charmap media insertdatetime searchreplace save wordcount fullpage lineheight',
                toolbar: 'undo redo | fontsizeselect | fontselect | lineheightselect | bold italic underline | bullist numlist | indent outdent',
                fontsize_formats: '8pt 10pt 11pt 12pt',
                lineheight_formats: "14pt",
                browser_spellcheck : true
            });
        });
    </script>
@endpush