@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}
@endpush

@push('after-script-end')
    {{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}
    <script>
        $(function () {
            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'Y-m-d',
                weeks: true,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false
            });


            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: true,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false
            });

            jQuery('.datepicker').datetimepicker({
                timepicker:false,
                format:'Y-n-j',
                weeks: true,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false
            });


            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker_before_today').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });

            jQuery('.datepicker_after_today').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                minDate: today_date,
            });

        });
    </script>
@endpush
