@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/timepicki/css/timepicki.css") }}
@endpush

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/timepicki/js/timepicki.js") }}
    <script>
        $(function () {
            $('.timepicker').timepicki({
                step_size_minutes:1,
                overflow_minutes:false,
                increase_direction:'up'
            });
        });
    </script>
@endpush
