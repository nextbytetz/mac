@push('after-styles-end')
{{--{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/dataTables.bootstrap4.min.css") }}--}}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/dataTables.jqueryui.min.css") }}
{{--{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/buttons/buttons.dataTables.min.css") }}--}}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/buttons/buttons.jqueryui.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/custom.css") }}
<style>

</style>
@endpush

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/jquery.dataTables.min.js") }}
{{--{{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/dataTables.bootstrap4.min.js") }}--}}
{{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/dataTables.jqueryui.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/dataTables.buttons.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/buttons/buttons.jqueryui.min.js") }}
{{ Html::script(asset_url() . "/global/plugins/jquery-ui/js/jquery.ui-contextmenu.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/buttons/buttons.html5.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/buttons/buttons.print.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/datatables/js/buttons/buttons.colVis.min.js") }}
{{ Html::script(public_url(). "/vendor/datatables/buttons.server-side.js")}}
@endpush
