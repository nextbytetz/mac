@push('after-styles-end')
{{ Html::style(asset_url() . "/global/plugins/toastr/build/toastr.min.css") }}
{{ Html::style(asset_url() . "/global/plugins/AmaranJS/dist/css/amaran.min.css") }}
{{ Html::style(asset_url() . "/global/plugins/AmaranJS/dist/css/animate.min.css") }}
<style>

</style>
@endpush

@push('after-script-end')
{{ Html::script(asset_url(). "/global/plugins/toastr/build/toastr.min.js") }}
{{ Html::script(asset_url(). "/global/plugins/AmaranJS/dist/js/jquery.amaran.min.js") }}
@endpush
