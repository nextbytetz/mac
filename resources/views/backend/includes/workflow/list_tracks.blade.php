<div class="list-group message-list-group">
    @foreach($wf_tracks as $wf_track)
        <div class="list-group-item" style="page-break-inside: avoid !important;">
            {{--<div class="list-image float-xs-left">
                --}}{{--<img src="../../../assets/global/image/image5-profile.jpg" alt="contact"/>--}}{{--
                <p>Level&nbsp;:&nbsp;</p>
                <p><span class="underline">{{ $wf_track->wfDefinition->level }}</span></p>
                <p>Status&nbsp;:&nbsp;</p>
                <p><span class="underline">{{ $wf_track->status_narration }}</span></p>
            </div>--}}
            <div class="list-message float-xs-left">
                <span class="float-xs-right">{{ $wf_track->getAgingDays() }}&nbsp;day(s)</span>
                {{--<h6></h6>--}}
                <h6>{!! $wf_track->username_completed_formatted !!}</h6>
                <p style="font-weight: bold;">{!! $wf_track->comments !!}</p>
                <p><small>Workflow Date&nbsp;:&nbsp;<span class="underline">{{ $wf_track->receive_date_formatted }}</span> &nbsp;&nbsp;,&nbsp;&nbsp; Forward Date&nbsp;:&nbsp;<span class="underline">{{ $wf_track->forward_date_formatted }}</span> </small></p>
                <p><small>Level Description&nbsp;:&nbsp; <span class="underline">{{ $wf_track->wfDefinition->description }}</span></small></p>
                <p>Level&nbsp;:&nbsp;&nbsp;<label class="tag tag-pill tag-primary">{{ $wf_track->wfDefinition->level }}</label></p>
                <p>Status&nbsp;:&nbsp;&nbsp;&nbsp;<label class="underline">{!! $wf_track->status_narration_label !!}</label></p>
            </div>
        </div>
    @endforeach
</div>