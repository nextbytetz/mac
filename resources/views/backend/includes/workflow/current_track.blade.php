@if ($wf_tracks->count())
    <label class="underline">
        <i class="icon fa fa-hand-o-right fa-2x fadeinout_2s" aria-hidden="true"></i>&nbsp;To attend workflow, click on the button labelled <b>Attend/Assign</b> on the extreme right side of the table. And then complete the action(s) or understand the instruction(s) on the form which will appear below the table.
    </label>
    <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
        <thead class="tbody_per_region table_display_block" >
        <tr style="color: white; background-color: lightskyblue" >
            <th>User Name</th>
            <th>Status</th>
            <th>Receive Date</th>
            <th>Level</th>
            <th>Description</th>
            <th>Aging</th>
            <th>-</th>
        </tr>
        </thead>
        <tbody>
            @foreach($wf_tracks as $wf_track)
                @php
                    $wf_definition_track = $wf_track->wfDefinition;
                @endphp
                <tr>
                    <th>{!! $wf_track->username_formatted !!}</th>
                    <th>{!! $wf_track->status_narration !!}</th>
                    <th>{!! $wf_track->receive_date_formatted !!}</th>
                    <th>{!! $wf_definition_track->level !!}</th>
                    <th>{!! $wf_definition_track->description !!}</th>
                    <th>{!! $wf_track->getAgingDays() !!}</th>
                    <th>
                        <a class="btn btn-secondary btn-sm site-btn wf_action" href="" data-trackid="{{ $wf_track->id }}" data-resourceid="{{ $wf_track->resource_id }}">
                            <i class="icon fa fa-circle-o-notch" aria-hidden="true"></i>&nbsp;
                            @if($wf_track->assigned)
                                Attend
                            @else
                                Assign
                            @endif
                        </a>
                    </th>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif