{{--['resource_id' => 1, 'wf_module_group_id'=> 2, 'type' => 2, 'workflowScriptAlreadyIncluded' => true]--}}
@php
    $workflowId = "workflow_track_table" . $resource_id;
    if (!isset($workflowScriptAlreadyIncluded)) {
        $workflowScriptAlreadyIncluded = false;
    }
@endphp

<div class="row">
    <div class="col-md-12">
        <div id="completed_workflow{{ $resource_id }}"></div>
    </div>
</div>

<div id="archive_content{{ $resource_id }}"></div>
<legend></legend>

{{--<div class = "row">
    <div class="col-md-12" >

    </div>
</div>--}}

<div class = "row">
    <div class="col-md-12">
        <div class="workflow-track-group">
            <div id="current_workflow{{ $resource_id }}"></div>
            <div id="wf_action_message{{ $resource_id }}"></div>
            <div id="wf_action_content{{ $resource_id }}" style="background: #f3f3f5;padding: 3px;border-radius: 3px;"></div>
        </div>
    </div>
</div>

@push('after-script-end')
@if(!$workflowScriptAlreadyIncluded)
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
@endif
<script type="text/javascript">
    @if(!$workflowScriptAlreadyIncluded)
        let $body = $("body");
        function initTable($resourceId, $wfModuleGroupId, $type) {
            $.post( base_url + "/workflow/get_wf_tracks_html/" + $resourceId + "/" + $wfModuleGroupId + "/" + $type, {}, function( $data ) {
                $("#current_workflow" + $resourceId).empty().html($data.current_track);
                $("#completed_workflow" + $resourceId).empty().html($data.completed_tracks);
            }, "json").done(function() {
                /* When Done */
                /*CORE_TEMP.function.PanelCollapse();
                CORE_TEMP.function.PanelFullscreen();
                CORE_TEMP.function.Collapse();
                CORE_TEMP.function.Close();*/
                CORE_TEMP.function.Collapse();
                /*$(".print_letter").click(function() {*/
                $body.off('click', "#completed_workflow" + $resourceId + " .print_workflow_track").on('click', "#completed_workflow" + $resourceId + " .print_workflow_track", function(e) {
                    let $url = $(this).attr('data-url');
                    let $wf_track_frame = $("#wf_track_frame" + $resourceId);
                    $wf_track_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $url + '" frameborder="0" name="wf_track_preview" width=\'100%\' height=\'600px\'></iframe>');
                    $wf_track_frame.append($iframe);
                    window.frames["wf_track_preview"].focus();
                    window.frames["wf_track_preview"].print();
                });
                $body.off('click', '.wf_action').on('click', '.wf_action', function(e) {
                    e.preventDefault();
                    let $this = $(this);
                    let $trackid = $this.data('trackid');
                    let $resourceid = $this.data('resourceid');
                    $this.find('i.fa-circle-o-notch').removeClass("fa-circle-o-notch").addClass('fa-spinner fa-spin');
                    $.post("{!! route('backend.workflow.workflow_modal_content') !!}", {'wf_track_id': $trackid}, function (data) {
                        $("#wf_action_content"+$resourceid).empty();
                        $(data).prependTo("#wf_action_content"+$resourceid);
                    }, "html").done(function () {
                        autosize($("textarea.autosize"));
                        $body.off("click", ".wf_action_close").on("click", ".wf_action_close", function (e) {
                            e.preventDefault();
                            let $this = $(this);
                            $this.closest("form[name=workflow_process_modal]").remove();
                            $("#wf_action_content"+$this.data('resourceid')).empty();
                        });
                        $body.off("change", ".workflow_status_select").on("change", ".workflow_status_select", function (e) {
                            /*alert("Hi");*/
                            let $this = $(this);
                            let $status = $this.val();
                            /*alert($status);*/
                            let $form = $this.closest("form[name=workflow_process_modal]");
                            let $wf_track_id = $this.attr("data-track_id");
                            let $next_level_designation = $form.find(".next_level_designation");
                            let $reject_to_level = $form.find(".reject_to_level");
                            let $reject_to_level_select = $form.find(".reject_to_level_select");
                            let $is_optional = $form.find(".is_optional").val();
                            let $selective = $form.find(".selective");
                            let $selective_select = $form.find(".selective_select");
                            let $select_next_user = $form.find(".select_next_user");
                            let $select_next_user_select = $form.find(".select_next_user_select");
                            let $next_level_designation_content = $form.find(".next_level_designation_content");
                            let $next_level_description_content = $form.find(".next_level_description_content");
                            switch($status) {
                                case '1':
                                case '4':
                                    if ($status === "1") {

                                        if ($is_optional !== '0') {
                                            $selective.show();
                                            $selective_select.prop( "disabled", false );
                                        } else {
                                            $selective.hide();
                                            $selective_select.prop( "disabled", true );
                                        }
                                        $select_next_user.show();
                                        $select_next_user_select.prop( "disabled", false );
                                    } else {
                                        /*status = 4*/
                                        $selective.show();
                                        $selective_select.prop( "disabled", false );
                                        $select_next_user.hide();
                                        $select_next_user_select.prop( "disabled", true );
                                    }
                                    $reject_to_level.hide();
                                    $.post( base_url + "/workflow/next_level_designation/" + $wf_track_id + "/" + $status , {}, function( $data ) {
                                        /*alert($data.skip);*/
                                        if ($data.next_level_designation !== "") {
                                            $next_level_designation.show();
                                            $next_level_designation_content.empty();
                                            $next_level_designation_content.html( $data.next_level_designation );
                                            $next_level_description_content.html( $data.next_level_description );
                                        } else {
                                            $next_level_designation.hide();
                                        }
                                    }, "json").done(function($data) {});
                                    break;
                                case '2':
                                    $selective.hide();
                                    $selective_select.prop( "disabled", true );
                                    $next_level_designation.hide();
                                    $reject_to_level.show();
                                    $reject_to_level_select.prop( "disabled", false );
                                    $select_next_user.hide();
                                    $select_next_user_select.prop( "disabled", true );
                                    break;
                                default:
                                    $selective.hide();
                                    $select_next_user.hide();
                                    $select_next_user_select.prop( "disabled", true );
                                    $selective_select.prop( "disabled", true );
                                    $next_level_designation.hide();
                                    $reject_to_level.hide();
                                    $reject_to_level_select.prop( "disabled", true );
                                    break;
                            }
                        });
                        $body.off('submit', 'form[name=workflow_process_modal]').on('submit', 'form[name=workflow_process_modal]', function(e) {
                            e.preventDefault();
                            let $form = $(this);
                            let $data = $form.serialize();
                            /* start: remove any printed error message in the input controls */
                            $form.find(':input').each(function () {
                                $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            });
                            /* end: remove any printed error message in the input controls */
                            $.ajax({
                                data : $data,
                                dataType : "json",
                                method : "POST",
                                url : $form.attr("action"),
                                beforeSend : function (e) {
                                    $form.find(".btn-submit").prop('disabled', true);
                                },
                                success : function ($data) {
                                    if ($data.success) {
                                        let $wf_action_message = $("#wf_action_message"+$data.resource_id);
                                        $wf_action_message.prepend('<div class="alert alert-success general_error" role="alert">' + $data.message + '</div>'); $wf_action_message.find('.general_error').fadeOut(11000);
                                        $("#wf_action_content"+$data.resource_id).empty();
                                        initTable($data.resource_id, $data.wf_module_group_id, $data.type);
                                    } else {
                                        $form.append('<div class="alert alert-danger general_error" role="alert">' + $data.message + '</div>'); $form.find('.general_error').fadeOut(121000);
                                    }
                                },
                                error: function (data) {
                                    let errors = $.parseJSON(data.responseText);
                                    /* console.log(data); */
                                    $.each(errors, function($index, $value) {
                                        $form.find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                                        if ($index === 'general_error') { $form.prepend('<div class="alert alert-danger general_error" role="alert">' + $value + '</div>'); $('.general_error').fadeOut(41000); }
                                    });
                                },
                            }).done(function() {
                            }).fail(function() {
                            }).always(function() {
                                $form.find(".btn-submit").prop('disabled', false);
                            });
                        });
                        $('.workflow_status_select').trigger('change');
                        $this.find('i.fa-spinner').removeClass("fa-spinner fa-spin").addClass('fa-circle-o-notch');
                    });
                });
            });
        }
        $(function() {

        });
    @endif
    $(function() {
        initTable("{{ $resource_id }}", "{{ $wf_module_group_id }}", "{{ $type }}");
        let $archive_content = $('#archive_content{{ $resource_id }}');
        $.post( base_url + "/workflow/can_archive_workflow/{{ $resource_id }}/{{ $wf_module_group_id }}/{{ $type }}", {}, function( $data ) {
            if ($data.success) {
                $archive_content.empty();
                $archive_content.html($data.view);
            }
        }, "json").done(function($data) {
            if ($data.success) {
                addConfirmPostForms();
            }
        });
    });
</script>
@endpush