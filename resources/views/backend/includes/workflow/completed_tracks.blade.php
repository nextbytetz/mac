@if($wf_tracks)
    @if ($wf_tracks->count())
        <div id="wf_track_frame{{ $resource_id }}" style="text-align: center;display: none;"></div>
        <div class="dashboard-header content">
            {{--<h4 class="page-content-title float-xs-left grey_modal">Workflow Track Overview</h4>--}}
            <div class="dashboard-action">
                <ul class="right-action float-xs-right">
                    <li><a href="javascript:void(0)" data-url="{{ route("backend.workflow.wf_list_tracks.get", ["resource_id" => $resource_id, "wf_module_group_id" => $wf_module_group_id, "type" => $type]) }}" class="print_workflow_track"><span aria-hidden="true" class="icon_printer" style="font-size: 18px;"></span>&nbsp;Print</a></li>
                    <li data-widget="collapse"><a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus" style="font-size: 18px;"></span></a></li>
                </ul>
            </div>
            <legend class="light_grey_bg" >{{ $module->name }}&nbsp;(@lang('labels.backend.legend.workflow_track_overview'))</legend>
            <div class="dashboard-box content-list" style="display: none;">
                @include("backend/includes/workflow/list_tracks")
            </div>
            <div class="dashboard-action">
                <ul class="right-action float-xs-right">
                    <li data-widget="collapse"><a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus" style="font-size: 18px;"></span></a></li>
                    {{--<li data-widget="close"><a href="javascript:void(0)"><span aria-hidden="true" class="icon_close"></span></a></li>--}}
                </ul>
            </div>
        </div>
    @endif
@endif