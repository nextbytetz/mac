{{--WORKFLOW TABLE HEADER --}}
<table class="display" cellspacing="0" width="100%" id ="workflow-table">
    <thead>
    <tr>
        <th>@lang('labels.backend.table.user_name')</th>
        <th>@lang('labels.general.status')</th>
        <th>@lang('labels.backend.table.level_id')</th>
        <th>@lang('labels.backend.table.comments')</th>
        <th>@lang('labels.backend.table.workflow.wf_date')</th>
        <th>@lang('labels.backend.table.workflow.forward_date')</th>
        <th>@lang('labels.backend.table.action')</th>
    </tr>
    </thead>
</table>

<{{--table  class="table table-bordered" id="workflow" width="100%">
    <thead>
    <tr>
        <th >User Name</th>
        <th >Status</th>
        <th >Level Id</th>
        <th >Comments</th>
        <th >Wf Date</th>
        <th >Forward Date</th>
        <th >Action</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
</table>--}}
