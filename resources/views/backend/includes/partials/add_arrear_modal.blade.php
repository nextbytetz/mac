<!-- Modal -->

<div class="modal" id="addarrea" tabindex="-1" role="dialog">
  <form role="form" id="addarreaform">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Arrears</h4>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-xs-12 hidden">
              <div class="form-group">
               <p class="error_employername text-danger hidden" id="employer" ></p>
             </div>
           </div> 
         </div>

         <div class="row">
          <div class="form-group">
            <div class="col-xs-4">
              <label for="salary">Select Payroll:</label>
            </div>
            <div class="col-xs-8">
              <span id="AddFields">
              </span>
              <p class="error_payroll text-danger hidden"></p>
            </div>
          </div>
        </div>

        <div class="row">
         <div class="form-group">
          <div class="col-xs-4">
           <label for="salary">Reason:</label>
         </div>
         <div class="col-xs-8">
          <input type="radio" value="missing_month" name="reason_for_adding" class="reasonForAdding"> &nbsp;Missing Month
          <br>
          <input type="radio" value="wrong_commence" name="reason_for_adding" class="reasonForAdding"> &nbsp;Wrong Commencement
         {{--  <br> 
         <input type="radio" value="forgot_employees" name="reason_for_adding" class="reasonForAdding"> &nbsp;Left Some Employees --}}
         <br> 
         <p class="error_reason_for_adding text-danger hidden"></p>
       </div>
     </div>
   </div>

   <span id="addRows"></span>





 </div>
 <div class="modal-footer">
  <span class="footerButtons">
    <button type="button" class="btn btn-primary" id="submitArrearForm">Save changes</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </span>
  <span class="wait hidden">
    <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  
    &nbsp; <span class="h6"> Please wait .... </span>
  </span>
</div>
</div>
</div>
</form>
</div>



<!-- Modal -->
<div id="removeArrearModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reason for Removing Arrear</h4>
      </div>
      <div class="modal-body">
        <form role="form" id="removeArrearForm">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-12">
               <input type="text" id="arrear_id" name="arrear" hidden="hidden">
               <input type="radio" value="paid" name="reason_for_remove"> &nbsp; Already Paid
             </div>
             <div class="col-xs-12">
               <br>
               <input type="radio" value="commencement" name="reason_for_remove"> &nbsp; Wrong Commencement
             </div>
             {{-- @if(count($employer->closures) > 0)
             <div class="col-xs-12">
              <br>
              <input type="radio" value="closed_business" name="reason_for_remove"> &nbsp; Closed Business
            </div>
            @endif --}}
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="removeArrearSubmit">Remove</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>



{{-- upload arrears employees --}}

<div class="modal" id="uploadArrearEmployeesModal" tabindex="-1" role="dialog">
  <form role="form" id="uploadArrearEmployeesForm">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span class="uploadArrearTitle"></span> </h4>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-xs-12 hidden">
              <div class="form-group">
               <input type="text" name="arrear_id" id="uploadArrearId" class="form-control hidden">
               <p class="error_arrear_id text-danger hidden" ></p>
             </div>
           </div> 
           <div class="col-xs-12 text-center error_div_general hidden">
            {{-- <alert class="error_arrear_general text-danger h6"></alert> --}}
            <div class="alert alert-danger error_arrear_general" role="alert">

            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group">
            <div class="col-xs-6">
              <label for="employee_count">Number Of Employees for <span class="uploadArrearMonth"></span>:</label>
            </div>
            <div class="col-xs-6">
              <input type="text" name="employee_count" id="employee_count" required="required" class="form-control">
              <span class="error_employee_count text-danger hidden"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group">
            <div class="col-xs-6">
              <label for="total_grosspay">Total Grosspay Of All Employees for <span class="uploadArrearMonth"></span>:</label>
            </div>
            <div class="col-xs-6">
              <input type="text" name="total_grosspay" id="total_grosspay" required="required" class="form-control money">
              <span class="error_total_grosspay text-danger hidden"></span>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="form-group">
            <div class="col-xs-6">
              <label for="attachment"><span class="uploadArrearMonth"></span> Employees attachment:</label>
            </div>
            <div class="col-xs-6">
              <input type="file" name="attachment" id="attachment" required="required" class="form-control">
              <span class="error_attachment text-danger hidden"></span>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <span class="footerButtons">
            <button type="button" class="btn btn-primary" id="submitArrearEmployeesForm">Save changes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </span>
          <span class="wait hidden">
            <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  

            &nbsp; <span class="h6"> Please wait .... </span>
          </span>

        </div>
      </div>
    </div>
  </form>
</div>







