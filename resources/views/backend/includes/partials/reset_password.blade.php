<!-- Modal -->
<div id="resetPasswordModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reset Password : <span id="user_name"></span> </h4>
      </div>
      <div class="modal-body">
        <form role="form" id="resetPasswordForm">
          <div class="row">
            <div class="col-xs-12">
             <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                  <label for="salary">New Password:</label>
                </div>
                <div class="col-xs-8">
                 <input type="text" name="user_id" id="reset_user_id" hidden="true" class="hidden">
                    <input type="text" name="reset_user_type" id="reset_user_type" hidden="true" class="hidden">
                 <input type="text" class="form-control" name="reset_password" id="reset_password">
                 
               </div>
             </div>
           </div>
           <br> <p class="error_password text-danger hidden"></p>
         </div>
       </div>
     </form>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" id="btnSubmitResetPw">Reset Password</button>
  </div>
</div>
</div>
</div>



