<!-- =========================== edit employee modal starts here ================================= -->

<div class="modal fade in" tabindex="-1" role="dialog" id="mdlEditData">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Employee</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmDataAdd">
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_ID" hidden="hidden" id="edit_ID" required="required">
                    </div>

                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="firstname">Firstname</label>
                                <input type="text" class="form-control" name="firstname" id="firstname" required="required">
                                <p class="error_firstname text-danger hidden"></p>

                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="middlename">Middlename</label>
                                <input type="text" class="form-control" name="middlename" id="middlename" required="required">
                                <p class="error_middlename text-danger hidden"></p>

                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="lastname">Lastname</label>
                                <input type="text" class="form-control" name="lastname" id="lastname" required="required">
                                <p class="error_lastname text-danger hidden"></p>
                            </div>
                        </div>


                    </div> <!--- row -->


                    <div class="row">
                        <div class="col-xs-6">

                            <!-- DOB Date -->
                            <div class="form-group">
                                <label>Date of Birth<span class="red"> *</span></label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="dob"  id="dob" class="form-control pull-right ">
                                </div>
                                <!-- /.input group -->
                                <p class="error_dob text-danger hidden"></p>

                            </div>
                        </div>


                        <div class="col-xs-6">

                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <input type="text" class="form-control" name="sex" id="sex" required="required"/>
                                <p class="error_sex text-danger hidden"></p>

                            </div>

                        </div>
                    </div> <!--- row -->




                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="salary">Basic Salary</label>
                                <input type="text" class="form-control" name="salary" id="salary" required="required"/>
                                <p class="error_salary text-danger hidden"></p>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="grosspay">Gross Salary</label>
                                <input type="text" class="form-control" name="grosspay" id="grosspay" required="required" />
                                <p class="error_grosspay text-danger hidden"></p>
                            </div>
                        </div>
                    </div> <!-- row -->



                    <div class="row">

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="job_title">Job Title</label>
                                <input type="text" class="form-control" name="job_title" id="job_title" required="required" />
                                <p class="error_job_title text-danger hidden"></p>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="emp_cate">Employment Category</label>
                                <input type="text" class="form-control" name="emp_cate" id="emp_cate" required="required"/>
                                <p class="error_emp_cate text-danger hidden"></p>
                            </div>
                        </div>
                    </div> <!-- row -->

                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-md btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-md btn-primary" id="btnUpdate"><i class="fa fa-save"></i>&nbsp;Update</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- =========================== edit employee modal ends here ================================= -->