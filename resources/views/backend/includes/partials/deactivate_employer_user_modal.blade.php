<!-- Modal -->
<div id="deactivateuser" class="modal fade" style="font-size: 1.2em;" role="dialog">
  <div class="modal-dialog modal-lg" >

    <!-- Modal content-->
    <form id="deactivateduser">

        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Deactivate Online User</h4>
        </div>

        <div class="modal-body">
         <div class="row">
            <div class="col-sm-12">
             <table class="table table-striped table-bordered" id="payroll_summary">
                <tbody>
                    <tr>
                        <td>User</td><td><p id="onlineusername"></p></td>
                    </tr>
                    <tr>
                        <td>Email</td><td><p id="usermail"></p></td>
                    </tr>
                    <tr>
                        <td>Phone</td><td><p id="userphone"></p></td>
                    </tr>
                </table>
                <hr>
            </div>
        </div>  
        <div class="row mb-2">
            <div class="col-sm-3">
             <label for="category-select">Reason for Deactivation:</label>
         </div>
         <div class="col-sm-8">
            <select id="category" class="selectReason form-control" style="width:100%"  name="reason" >
              <option selected="selected" value="">Select Reason</option>
              <option value="Change of Agent">Change of Agent</option>
              <option value="Organization owner decided to manage the account">
              Organization owner decided to manage the account</option>
              <option value="Authorized Personnel Left the organization">Authorized Personnel Left the organization</option>
              <option value="Wrong user on wrong organization">Wrong user on wrong organization</option>
          </select>
          <br> <p class="reason_error text-danger hidden errors"></p>        
      </div>
  </div>
  <div class="row">
    <div class="col-sm-3">
        <label for="remarks">Remarks:</label>
    </div>
    <div class="col-sm-8">
       <textarea name="remarks" id="remarks" class="form-control"></textarea>
       <br> <p class="remarks_error text-danger hidden errors"></p>        
   </div>
</div>

<div class="row">
    <input type="text" hidden="hidden" class="form-control" name="user_id" id="user_id" required="required" />
    <input type="text" hidden="hidden" class="form-control" name="employer_id" id="employer_id" required="required" />
</div>



</div> 

<div class="modal-footer">
    <span class="subBtns">
        <button type="button" class="btn btn-primary" id="btnSubmitDeactivate">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </span>
    <span class="waitLoad hidden">
      <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  
      &nbsp; <span class="h6"> Please wait .... </span>
  </span>
</div>
</div>
</form>

</div>
</div>

<div id="activateUserModal" class="modal fade" style="font-size: 1.2em;" role="dialog">
   <div class="modal-dialog modal-lg">

    <form id="activateUserForm">
     <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Activate Online User</h4>
    </div>
    <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
           <table class="table table-striped table-bordered" id="payroll_summary">
            <tbody>
              <tr>
                <td>User</td><td><p class="onlineusername"></p></td>
            </tr>
            <tr>
                <td>Email</td><td><p class="usermail"></p></td>
            </tr>
            <tr>
                <td>Phone</td><td><p class="userphone"></p></td>
            </tr>
        </table>
        <hr>
    </div>
</div>

<div class="row">
  <div class="col-sm-3">
    <label for="activate_remarks">Remarks:</label>
</div>
<div class="col-sm-8">
    <input type="text" hidden="hidden" class="form-control" name="user_id" id="activate_user_id" required="required" />
    <input type="text" hidden="hidden" class="form-control" name="employer_id" value="{{$employer->id}}" id="activate_employer_id" required="required" />
    <textarea name="activate_remarks" id="activate_remarks" class="form-control"></textarea>
    <br> <p class="activate_remarks_error text-danger hidden errors"></p>        
</div>
</div>
</div>

<div class="modal-footer">
  <span class="subBtns">
    <button type="button" class="btn btn-primary" id="btnSubmitActivate">Submit</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</span>
<span class="waitLoad hidden">
    <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  
    &nbsp; <span class="h6"> Please wait .... </span>
</span>
</div>
</div>
</form>
</div>
</div>


{{-- </div> --}}
