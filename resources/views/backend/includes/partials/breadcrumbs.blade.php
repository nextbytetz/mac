@if ($breadcrumbs)
    <ol class="breadcrumb float-xs-right">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->first)
                <li class="breadcrumb-item"><span class="fs1" aria-hidden="true" data-icon=""></span>
                    <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                </li>
            @elseif ($breadcrumb->last)
                <li class="breadcrumb-item active">{{ $breadcrumb->title }}</li>
            @else
                <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @endif
        @endforeach
    </ol>
@endif
{{--
<ol class="breadcrumb float-xs-right">
    <li class="breadcrumb-item">
        <span class="fs1" aria-hidden="true" data-icon=""></span>
        <a href="#">Home</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Layout</a></li>
    <li class="breadcrumb-item active">Blank</li>
</ol>--}}
