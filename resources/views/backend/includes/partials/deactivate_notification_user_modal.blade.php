<!-- Modal -->
<div id="deactivatenotificationuser" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Deactivate Notification User Form</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form role="form" id="deactivatednotificationuser">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="salary">User Name</label>
                                <input type="text" class="form-control" name="onlineusername" id="notificationusername" disabled="disabled" required="required"/>
                                <p class="error_username text-danger hidden"></p>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="grosspay">Email Address</label>
                                <input type="text" disabled="disabled" class="form-control" name="email" id="notificationusermail" required="required" />
                                <p class="error_email text-danger hidden"></p>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="grosspay">Mobile Phone</label>
                                <input type="text" disabled="disabled" class="form-control" name="email" id="notificationuserphone" required="required" />
                                <p class="error_email text-danger hidden"></p>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="grosspay">Employer Name</label>
                                <input type="text" disabled="disabled" class="form-control" name="email" id="notificationemployername" required="required" />
                                <p class="error_email text-danger hidden"></p>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="category-select">Reason for Deactivation</label>
                                    <select id="notificationcategory" name="notificationreason" >
                                        <option value="Authorized Personnel Left the organization">Authorized Personnel Left the organization</option>
                                        <option value="Wrong user on wrong organization">Wrong user on wrong organization</option>
                                        <option value="User mistakenly approved">User mistakenly approved</option>
                                    </select>
                                </div>

                                <p class="error_reason text-danger hidden"></p>
                                <input type="text" hidden="hidden" class="form-control" name="user_id" id="notificationuser_id" required="required" />
                                <input type="text" hidden="hidden" class="form-control" name="employer_id" id="notificationemployer_id" required="required" />
                            </div>
                        </div>
                </div> <!-- row -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-warning" id="btnnotificationdeactivate" data-dismiss="modal">Deactivate</button>
                </form>
            </div>
        </div>

    </div>
</div>