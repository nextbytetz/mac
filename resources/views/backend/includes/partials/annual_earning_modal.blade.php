<div id="reverseReturnModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Reversing <span class="reason_title"></span></h5>
      </div>
      <div class="modal-body">
        <form role="form" id="reverseReturnForm">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-12">
                <input type="text" id="annual_earning_id" name="annual_earning_id" hidden="hidden" >
                <label for="reverse reason" class="h6">Reversal Reason:</label>
                <textarea  id="reason" name="reverse_reason" class="form-control" placeholder="write reason for reverse this annual earning"></textarea>
                <p class="error_reason text-danger hidden h6" id="error_reason" ></p>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <span class="footerButtons">
              <button type="button" class="btn btn-primary" id="reverseReturnSubmit">Save changes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </span>
            <span class="wait hidden">
              <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  

              &nbsp; <span class="h6"> Please wait .... </span>
            </span>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>
