<div id="activateUwserModal" class="modal fade" style="font-size: 1.2em;" role="dialog">
  <div class="modal-dialog modal-lg">

    <form id="activateUserForm">
     <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Activate Online User</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
           <table class="table table-striped table-bordered" id="payroll_summary">
            <tbody>
              <tr>
                <td>User</td><td><p id="onlineusername"></p></td>
              </tr>
              <tr>
                <td>Email</td><td><p id="usermail"></p></td>
              </tr>
              <tr>
                <td>Phone</td><td><p id="userphone"></p></td>
              </tr>
            </table>
            <hr>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">
            <label for="remarks">Remarks:</label>
          </div>
          <div class="col-sm-8">
           <textarea name="remarks" id="remarks" class="form-control"></textarea>
           <br> <p class="remarks_error text-danger hidden errors"></p>        
         </div>
       </div>
     </div>

     <div class="modal-footer">
      <span class="subBtns">
        <button type="button" class="btn btn-primary" id="btnSubmitActivate">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </span>
      <span class="waitLoad hidden">
        <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  
        &nbsp; <span class="h6"> Please wait .... </span>
      </span>
    </div>
  </div>
</form>
</div>
</div>


</div>