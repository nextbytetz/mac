<!-- Modal -->
<div id="addUserModal" class="modal fade" role="dialog" data-focus="false">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
     <form role="form" id="addUserForm">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Online User - {{isset($employer->name) ? $employer->name : '' }}</h4>
      </div>
      <div class="modal-body grid-column">
       <div class="row ">
        <div class="col-md-12">
          <label for="select">Search User: </label>
          <select class="select-user" style="width:80%" name="user_id"></select>
          <br> <p class="error_user_select text-danger hidden"></p>
        </div>
      </div>
      <div class="row">
        <br>
        <div class="col-md-6">
         <label for="name">Name</label><br>
         <input class="user-name form-control" name="name" disabled="disabled" />
       </div>

       <div class="col-md-6">
         <label for="email">Email</label><br>
         <input class="user-email form-control" name="email" disabled="disabled" />
       </div>
     </div>

     <div class="row">
      <br>
      <div class="col-md-6">
        <label for="phone">Phone</label><br>
        <input class="user-phone form-control" name="phone" disabled="disabled" />
      </div>

      <div class="col-md-6 hidden" id="payroll_section">
        <label for="payroll_section">Select Payroll</label>
        <br>
        <span id="payroll_options"><br></span>
        <br> <p class="error_payroll_options text-danger hidden"></p>
      </div>
    </div>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-success" id="addUserFormSubmit">Add User</button>
  </div>
</form>
</div>
</div>
</div>
