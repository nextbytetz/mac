@push('after-styles-end')

@endpush

<div class="modal fade bd-example-modal-lg" id="create_medical_practitioner" role="dialog" aria-labelledby="create_medical_practitioner" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Medical Practitioner</h4>
            </div>
            {!! Form::open(['route' => ['backend.claim.medical_practitioner.store'], 'method'=>'post',  'id' => 'create_mp', 'name' => 'create_mp']) !!}
            {!! Form::hidden('request_action_type', 1 , []) !!}
            <div class="modal-body">
                @include("backend.includes.registration.claim.create_medical_practitioner_form")
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary site-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary site-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('after-script-end')
    <script>
        $(function() {
            $('body').off('submit', 'form[name=create_mp]').on('submit', 'form[name=create_mp]', function(e) {
                e.preventDefault();
                var form = this;
                var $data = $(form).serialize();
                /* start: remove any printed error message in the input controls */
                $(form).find(':input').each(function () {
                    var $name = $(this).attr('name');
                    $("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                $.ajax({
                    data : $data,
                    dataType : "json",
                    method : "POST",
                    url : $(form).attr("action"),
                    beforeSend : function (e) {
                        $(".site-btn").prop('disabled', true);
                    },
                    success : function (data) {
                        if (data.success) {
                            $("#create_medical_practitioner").modal('hide');
                            $.amaran({
                                'theme'     :'awesome success',
                                'content'   :{
                                    title : "Success",
                                    message: data.message,
                                    info:'',
                                    icon: 'fa fa-check-square-o',
                                },
                                'position'  :'bottom left',
                                'outEffect' :'slideBottom',
                                'inEffect'  :'slideLeft'
                            });
                        } else {
                            sweetAlert("Error Adding Medical Practitioner", data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        /* console.log(data); */
                        $.each(errors, function(index, value) {
                            $("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        });
                    },
                }).done(function() {

                }).fail(function() {

                }).always(function() {
                    $(".site-btn").prop('disabled', false);
                });
            });
        });
    </script>
@endpush