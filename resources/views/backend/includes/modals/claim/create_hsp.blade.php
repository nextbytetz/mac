@push('after-styles-end')

@endpush

<div class="modal fade bd-example-modal-lg"  tabindex="-1" id="create_health_service_provider" role="dialog" aria-labelledby="create_health_service_provider" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="opacity: 1;background: #fff;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="create_health_service_provider">Add Health Provider</h4>
            </div>
            {!! Form::open(['route' => ['backend.claim.health_provider.store'],  'id' => 'create_hsp', 'name' => 'create_hsp']) !!}
            {!! Form::hidden('request_action_type', 1 , []) !!}
            <div class="modal-body">
                @include("backend.includes.registration.claim.create_hsp_form")
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary site-btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary site-btn">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('after-script-end')
    <script>
        $(function() {
            $('.search-select-hsp').select2({
                dropdownParent: $('#create_health_service_provider')
            });
            $('#region_id_hsp').on('change', function (e) {
                var region_id = e.target.value;
                if (region_id) {
                    $(".spin2").show();
                    $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                        $('#district_id_hsp').empty();
                        $("#district_id_hsp").select2("val", "");
                        $('#district_id_hsp').html(data);
                        $(".spin2").hide();
                    });
                }
            });
            $('body').off('submit', 'form[name=create_hsp]').on('submit', 'form[name=create_hsp]', function(e) {
                e.preventDefault();
                var form = this;
                var $data = $(form).serialize();
                /* start: remove any printed error message in the input controls */
                $(form).find(':input').each(function () {
                    var $name = $(this).attr('name');
                    $("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                    $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                $.ajax({
                    data : $data,
                    dataType : "json",
                    method : "POST",
                    url : $(form).attr("action"),
                    beforeSend : function (e) {
                        $(".site-btn").prop('disabled', true);
                    },
                    success : function (data) {
                        $("#create_health_service_provider").modal('hide');
                        if (data.success) {
                            $.amaran({
                                'theme'     :'awesome success',
                                'content'   :{
                                    title : "Success",
                                    message: data.message,
                                    info:'',
                                    icon: 'fa fa-check-square-o',
                                },
                                'position'  :'bottom left',
                                'outEffect' :'slideBottom',
                                'inEffect'  :'slideLeft'
                            });
                        } else {

                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        /* console.log(data); */
                        $.each(errors, function(index, value) {
                            $("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                            $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        });
                    },
                }).done(function() {

                }).fail(function() {

                }).always(function() {
                    $(".site-btn").prop('disabled', false);
                });
            });
        });
    </script>
@endpush