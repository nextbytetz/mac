@php
    $leftuser = access()->user();
@endphp
<aside id="sidebar">
    <div class="sidebar-search">
        <input id="left-menu-search" type="search" class="form-control input-sm" placeholder="Search...">
        <a href="javascript:void(0)"><i class="search-close icon_search"></i></a>
    </div>
    <div class="sidebar-menu">
        <ul class="nav site-menu live-search-list" id="site-menu" data-plugin="custom-scroll" data-suppress-scroll-x="true" data-height="100%">


            {{--<li class="menu-title"><i class="icon_compass_alt"></i><span>@lang('labels.backend.shortcut_menu')</span>--}}
                <ul class="main-menu">
                  <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workflow/reversed']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.workflow.reversed') !!}"><i class="icon fa fa-mail-reply-all"></i>&nbsp;&nbsp;
                        <span>Reversed Workflow</span>&nbsp;&nbsp;
                        <?php
                        $workflow_count = access()->getReversedWorkflowPendingCount();
                        ?>
                        @if ($workflow_count)
                        <span class="badge-summary blink">
                            {!! number_0_format($workflow_count)  !!}
                        </span>
                        @endif
                    </a>
                </li>
                {{--<li id = "hover_grey_faint_bg">
                    <a id="bottom_border_custom"   href="#"><i class="icon_compass_alt"></i>&nbsp;&nbsp;<span>@lang('labels.backend.checker_inbox')</span></a>
                </li> To be added later --}}
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workflow/pending']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.workflow.pending') !!}"><i class="icon fa fa-inbox"></i>&nbsp;&nbsp;
                        <span>@lang('labels.backend.system.workflow.pending')</span>&nbsp;&nbsp;
                        <?php
                        $workflow_count = access()->getWorkflowPendingCount();
                        ?>
                        @if ($workflow_count)
                        <span class="badge-summary-alert">
                            {!! number_0_format($workflow_count)  !!}
                        </span>
                        @endif
                    </a>
                </li>
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workflow/my_pending']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.workflow.my_pending') !!}"><i class="icon fa fa-eye"></i>&nbsp;&nbsp;
                        <span>Assigned Workflow</span>&nbsp;&nbsp;
                        <?php
                        $my_workflow_count = access()->getMyWorkflowPendingCount();
                        $my_workflow_notify_count = access()->getMyWorkflowPendingCount(1);
                        ?>
                        @if ($my_workflow_count)
                        <span class="badge-summary-alert @if ($my_workflow_notify_count) blink @endif">
                            {!! number_0_format($my_workflow_count)  !!}
                        </span>
                        @endif
                    </a>
                </li>
                @if(count($leftuser->SubordinatesIds))
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workflow/subordinates']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.workflow.subordinates') !!}"><i class="icon fa fa-sort-amount-asc"></i>&nbsp;&nbsp;
                        <span>Subordinates Pending</span>&nbsp;&nbsp;
                        <?php
                        $workflow_count = access()->getSubornitesPendingCount();
                        ?>
                        @if ($workflow_count)
                        <span class="badge-summary-alert  @if ($workflow_count >= 50) blink @endif">
                            {!! number_0_format($workflow_count)  !!}
                        </span>
                        @endif
                    </a>
                </li>
                @endif
                {{--                @if (env('TESTING_MODE'))--}}
                @if (true)
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workflow/archived']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.workflow.archived') !!}"><i class="icon fa fa-archive" aria-hidden="true"></i>&nbsp;&nbsp;
                        <span>Archived Workflow</span>&nbsp;&nbsp;
                        <?php
                        $unarchive_ready_count = access()->getMyReadyToUnAchiveWorkflows();
                        ?>
                        @if ($unarchive_ready_count)
                        <span class="badge-summary @if ($unarchive_ready_count) blink @endif">
                            {!! number_0_format($unarchive_ready_count)  !!}
                        </span>
                        @endif
                    </a>
                </li>
                @endif
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workflow/attended']) ? 'active active_link' : '' }}">
                    <a  href="{!! route("backend.workflow.attended") !!}"><i class="icon fa fa-recycle" aria-hidden="true"></i>&nbsp;&nbsp;
                        <span>Attended Workflow</span>&nbsp;&nbsp;
                    </a>
                </li>

                    {{--designation level 1 - 3 DG, Head, Director, Manager ... --}}
                @if (in_array($leftuser->designation_id, [1,2,3,5]))
                    <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workflow/progress']) ? 'active active_link' : '' }}">
                        <a  href="{!! route("backend.workflow.progress") !!}"><i class="icon fa fa-spinner" aria-hidden="true"></i>&nbsp;&nbsp;
                            <span>Workflow Progress</span>&nbsp;&nbsp;
                        </a>
                    </li>

                    <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['report/sla']) ? 'active active_link' : '' }}">
                        <a  href="{!! route("backend.report.sla.index") !!}"><i class="icon fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;
                            @php
                                $checkmyslacount = access()->checkMySlaCount();
                            @endphp
                            <span>SLA</span>&nbsp;&nbsp;@if($checkmyslacount)<span class="tag tag-pill tag-danger">&nbsp;</span>@endif
                        </a>
                    </li>
                @endif

                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['checker']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.checker.index') !!}"><i class="icon fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;
                        <span>Inbox&nbsp;&amp;&nbsp;Tasks</span>&nbsp;&nbsp;
                        <?php
                        $checker_count = access()->getMyCheckerPendingShouldCount();
                        $notify_count = access()->getMyCheckerPendingNotifyCount();
                        ?>
                        @if ($checker_count)
                        <span class="badge-summary @if ($notify_count) blink @endif">
                            {!! number_0_format($checker_count)  !!}
                        </span>
                        @endif
                    </a>
                </li>
                @permission("view_finance_menu")
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['finance/receipt/employer']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.finance.receipt.employer') !!}"><i class="icon fa fa-newspaper-o"></i>&nbsp;&nbsp;<span>@lang('labels.backend.finance.receipt.create')</span></a>
                </li>
                @endauth
                @permission("view_claim_menu")
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['claim/notification_report/choose_employee']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.claim.notification_report.choose_employee') !!}"><i class="icon fa fa-wheelchair-alt"></i>&nbsp;&nbsp;<span>@lang('labels.backend.claim.create_notification')</span></a>
                </li>
                @endauth

                
                {{--Claim - pending / no contribution--}}
                @if((($leftuser->unit_id == 14) || ($leftuser->unit_id == 15 )) && ($leftuser->designation_id <> 5))
                <li class = "hover_grey_faint_bg bottom_border_custom">
                    <a href="{!! route('backend.claim.notification_contribution_track.index') !!}"><i class="icon fa fa-wheelchair"></i>&nbsp;&nbsp;<span>@lang('labels.backend.claim.claim_pending_contributions')</span>
                        @if($leftuser->pendingNotificationContributionTrackCount()  > 0)
                        <span class="tag tag-pill tag-info" style="font-size: 12px;">
                            {!! $leftuser->pendingNotificationContributionTrackCount()  !!}
                        </span>
                        @endif
                    </a>

                </li>
                @endif

                @permission("view_osh_menu")
                @if($leftuser->designation_id >= 4)
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['workplace_risk_assesment/audit/assigned_list']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.workplace_risk_assesment.audit.assigned_list') !!}"><i class="icon fa fa-check-square-o" aria-hidden="true"></i>&nbsp;&nbsp;
                        <span>Assigned Osh Audit</span>&nbsp;&nbsp;
                        <?php
                        $checker_count = access()->getMyPendingOshAuditCount();
                        ?>
                        @if ($checker_count)
                        <span class="badge-summary-alert">
                            {!! number_0_format($checker_count)  !!}
                        </span>
                        @endif
                    </a>
                </li>
                @endif
                @endauth

                @permission("view_compliance_menu")
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['compliance/employer_registration/name_check']) ? 'active active_link' : '' }}">
                    <a  href="{!! route('backend.compliance.employer_registration.name_check') !!}"><i class="icon fa fa-institution"></i>&nbsp;&nbsp;<span>@lang('labels.backend.member.register_employer')</span></a>
                </li>
                @endauth

                {{--Track Claim--}}
                {{--<li class = "hover_grey_faint_bg bottom_border_custom">--}}
                    {{--<a  href="#"><i class="icon fa fa-stack-overflow"></i>&nbsp;&nbsp;<span>@lang('labels.backend.claim.track')</span></a>--}}
                {{--</li>--}}
                @permission("view_legal")
                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['legal']) ? 'active active_link' : '' }}">
                    <a href="{!! route('backend.legal.menu') !!}"><i class="icon fa fa-legal"></i>&nbsp;&nbsp;<span>@lang('labels.backend.legal.title.main')</span></a>
                </li>
                @endauth
                {{--<li class = "hover_grey_faint_bg bottom_border_custom">
                    <a href="{!! route("backend.notification.index") !!}"><i class="icon fa fa-comments-o"></i>&nbsp;&nbsp;
                        <span>@lang('labels.backend.system.notification.title')</span>&nbsp;&nbsp;
                        $noty_count = $leftuser->unreadNotifications()->count();
                        @if ($noty_count > 0)
                            <span class="tag tag-pill tag-info" style="font-size: 12px;">
                            {!! $noty_count !!}
                        </span>
                        @endif
                    </a>
                </li>--}}

                <li class = "hover_grey_faint_bg bottom_border_custom {{ Active::checkUri(['home/statistics']) ? 'active_link' : '' }}">
                    <a href="{!! route('backend.home.general_statistics') !!}"><i class="icon fa fa-signal"></i>&nbsp;&nbsp;<span>@lang('labels.general.general_statistics')</span></a>
                </li>


            </ul>
        {{--</li>--}}

    </ul>
    {{--Image of wcf logo--}}
    {{--<div>&nbsp;</div>--}}
    {{--<div class="pull-center"><img  src="{!! asset_url() . "/nextbyte/img/wcf_big_logo_no_background.png" !!}"></div>--}}
</div>



<div class="sidebar-extra">

    <a href="#"></a>
    <a href="#"></a>
    <a href="{!! route('logout') !!}" class="pull-right"><i class="icon fa fa-sign-out" aria-hidden="true"></i></a>
</div>
</aside>