

<div class = "row" id="wf_track-table-div">
    <div class="col-md-12"  >


        {{--<div>&nbsp;</div>--}}
        {{--<div>&nbsp;</div>--}}

        <table class="display" cellspacing="0" width="100%" id ="wf_track-table">
            <thead>
            <tr>
                <th>User Name</th>
                <th>Status</th>
                <th>Level Id</th>
                <th>Description</th>
                <th>Comments</th>
                <th>Wf Date</th>
                <th>Forward Date</th>
            </tr>
            </thead>

        </table>
    </div>
</div>


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#wf_track-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                sort: false,
                paging: false,
                info:false,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.workflow.wf_tracks.get_deactivated', $notification_report->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'user_id' , name: 'user_id' },
                    { data: 'status' , name: 'status' },
                    { data: 'wf_definition_id' , name: 'wf_definition_id' },
                    { data: 'description' , name: 'description' },
                    { data: 'comments', name: 'from_date' },
                    { data: 'receive_date', name: 'wf_date' },
                    { data: 'forward_date', name: 'forward_date' }

                ],

            } );
        });
    </script>;

@endpush