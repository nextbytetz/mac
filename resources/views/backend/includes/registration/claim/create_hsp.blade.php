{!! Form::open(['route' => ['backend.claim.health_provider.store'],  'id' => 'create_hsp']) !!}
{!! Form::hidden('request_action_type', 1 , []) !!}
@include("backend.includes.registration.claim.create_hsp_form")

<br/>
<hr/>
<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            {!! link_to_route('backend.claim.health_provider.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}