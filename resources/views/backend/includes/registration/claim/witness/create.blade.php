{{--Witness Name--}}
<div class="fileld-layout">
    <label>Name</label>
    <div class="form-group">
        <div class="input-group">
            {!! Form::text('witness_name', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
            <span class="input-group-addon"><i class="icon fa fa-eye"></i></span>
        </div>
    </div>
</div>
{{--Witness Phone Number--}}
<div class="fileld-layout">
    <label>Phone Number</label>
    <div class="form-group">
        <div class="input-group">
            {!! Form::text('witness_phone', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
            <span class="input-group-addon"><i class="icon fa fa-mobile-phone"></i></span>
        </div>
        {!! $errors->first('witness_phone', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
{{--Witness Supervisor's Name--}}
<div class="fileld-layout">
    <label>Supervisor's Name</label>
    <div class="form-group">
        <div class="input-group">
            {!! Form::text('witness_supervisor_name', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
            <span class="input-group-addon"><i class="icon fa fa-male"></i></span>
        </div>
    </div>
</div>
{{--Witness Supervisor's Phone Number--}}
<div class="fileld-layout">
    <label>Supervisor's Phone Number</label>
    <div class="form-group">
        <div class="input-group">
            {!! Form::text('witness_supervisor_phone', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
            <span class="input-group-addon"><i class="icon fa fa-mobile-phone"></i></span>
        </div>
    </div>
</div>
{{--Witness Supervisor's Unit--}}
<div class="fileld-layout">
    <label>Supervisor's Section/Department</label>
    <div class="form-group">
        <div class="input-group">
            {!! Form::text('witness_supervisor_unit', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
            <span class="input-group-addon"><i class="icon fa fa-institution"></i></span>
        </div>
    </div>
</div>