<div class="row">
    {!! Form::hidden('user_id', access()->id(), []) !!}
    @inject('regions', 'App\Repositories\Backend\Sysdef\RegionRepository')
    <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">
        {{--name--}}
        <div class="fileld-layout">
            <label class="required">Facility Name</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('name', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--external--}}
        <div class="fileld-layout">
            <label class="required">Facility Number</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('external_id', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                <small class="form-text text-muted" style="width:100% !important;">Identified registration number of the facility</small>
                {!! $errors->first('external_id', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--common name--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.common_name')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('common_name', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('common_name', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--facility_type--}}
        <div class="fileld-layout">
            <label class="required">@lang('labels.general.facility_type')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('facility_type', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                <small class="form-text text-muted" style="width:100% !important;">E.g. Clinic/Dispensary/Health Center/Hospital</small>
                {!! $errors->first('facility_type', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--ownership--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.ownership')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('ownership', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('ownership', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--zone--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.zone')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('zone', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('zone', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        {{-- start: incident region--}}
        <div class="fileld-layout">
            <label class="required">Region</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('region_id', $regions->getForSelect(), (($health_provider->dist()->count()) ? $health_provider->dist->region->id : null) , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select-hsp','id'=> 'region_id_hsp']) !!}
                    <i class="icon fa fa-spinner fa-spin spin2" aria-hidden="true" style="display: none;"></i>
                </div>
                {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
        {{-- start : incident district--}}
        <div class="fileld-layout">
            <label class="required">District</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('district_id', (($health_provider->dist()->count()) ? $health_provider->dist->region->districts->pluck("name", "id") : []), (($health_provider->dist()->count()) ? $health_provider->district_id : null), ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select-hsp','id'=> 'district_id_hsp']) !!}
                </div>
                {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--ward--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.ward')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('ward', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('ward', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--street--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.street')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('street', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('street', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--council--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.council')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('council', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('council', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{-- Is this health provider contracted--}}
        <div class="fileld-layout">
            <label class="required">Is Contracted</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('is_contracted', [1 => "Yes", 0 => "No"], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select-hsp','id'=> 'is_contracted']) !!}
                </div>
                <small class="form-text text-muted" style="width:100% !important;">Show whether this health service provider has been contracted by WCF</small>
                {!! $errors->first('is_contracted', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>

    </div>
</div>