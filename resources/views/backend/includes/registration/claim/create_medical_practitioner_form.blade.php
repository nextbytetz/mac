<div class="row">
    <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">
        {{--firstname--}}
        <div class="fileld-layout">
            <label class="required">@lang('labels.general.firstname')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('firstname', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('firstname', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--middlename--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.middlename')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('middlename', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('middlename', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--Lastname--}}
        <div class="fileld-layout">
            <label class="required">@lang('labels.general.lastname')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('lastname', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('lastname', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--phone--}}
        <div class="fileld-layout">
            <label class="required">@lang('labels.general.phone')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('phone', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        {{--national_id--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.national_id')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('national_id', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                {!! $errors->first('national_id', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--address--}}
        <div class="fileld-layout">
            <label>@lang('labels.general.address')</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::textarea( 'address', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;height: 60px;', 'cols' => 50, 'rows' => 10]) !!}
                </div>
                {!! $errors->first('address', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
        {{--external_id--}}
        <div class="fileld-layout">
            <label>Practitioner Number</label>
            <div class="form-group">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('external_id', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                </div>
                <small class="form-text text-muted" style="width:100% !important;">Identified professional number</small>
                {!! $errors->first('external_id', '<span class="help-block label label-danger">:message</span>') !!}
                <span class="help-block"></span>
            </div>
        </div>
    </div>
</div>
