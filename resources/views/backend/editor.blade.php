@extends('layouts.backend.main', ['title' => trans('labels.general.editor'), 'header_title' => trans('labels.general.editor')])

@push('after-styles-end')

<style>
    .mce-ico.mce-i-fa {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
</style>

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {{--<button class="editor_save">Save</button>--}}
    <form>
        <textarea id="editor">Next, get a free TinyMCE Cloud API key!</textarea>
    </form>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/tinymce/tinymce.min.js") }}

<script>
    $(function () {
        var $editor = tinymce.init({
            selector:'textarea#editor',
            branding: false,
            elementpath: false,
            resize: 'both',
            height : 300,
            skin: 'lightgray',
            statusbar: true,
            images_upload_url: '',
            relative_urls: false,
            automatic_uploads: false,
            forced_root_block: false,
            plugins : 'advlist autolink link image table lists charmap print preview code media insertdatetime searchreplace save wordcount fullpage lineheight',
            toolbar: 'save | print download | undo redo | fontsizeselect | fontselect | lineheightselect | bold italic underline | bullist numlist | indent outdent | link image',
            fontsize_formats: '8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt',
            lineheight_formats: "14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
            //font_formats: 'Tahoma=serif;Times New Roman=times new roman,times;Verdana=verdana,geneva;',
            save_onsavecallback: function () {
                //Put logics to save the form here ...

                console.log('Saved ...');
            },
            save_oncancelcallback: function () {
                //Put logics to cancell the save ...
                console.log('Save Canceled');
            },
            /*alignleft aligncenter alignright alignjustify*/
            browser_spellcheck : true,
            file_browser_callback: function(field_name, url, type, win) {
                win.document.getElementById(field_name).value = '/public/template/assets/nextbyte/guide/media/';
            },
            file_browser_callback_types: 'file image media',
            setup: function(editor) {
                editor.addButton('download', {
                    text: '',
                    tooltip: "Download",
                    icon: "fa fa-download",
                    onclick: function() {
                        //Download the form here ...
                        console.log('Downloaded ...');
                    }
                });
            }
        });
    });
</script>

@endpush