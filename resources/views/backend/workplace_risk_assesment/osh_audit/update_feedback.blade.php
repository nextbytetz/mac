{{-- {{dd($osh_audit_employer)}} --}}
@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_audit.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_audit.check_list.update_title')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')

<div class="row">
  <div class="col-sm-9">
    <h5 class="client-title">
      <i class="icon-circle"></i>
      <!-- Employer header detail -->
      @if($osh_audit_employer->is_submitted)
      @if($feedback_score['rating']['score'] == 'LOW')
      <span class="tag tag-danger danger_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="Low performer">.</span>
      @elseif($feedback_score['rating']['score']== 'MEDIUM')
      <span class="tag tag-warning warning_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="Medium performer">.</span>
      @else
      <span class="tag tag-success success_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="High performer">.</span>
      @endif
      @endif
      <strong>{{$osh_audit_employer->name}}</strong>
      <small >
        Reg No#:  {{substr($osh_audit_employer->reg_no, -6)}}
      </small>
    </h5>
    
  </div>
</div>
<legend></legend>
<div class="row">
  <br>
  <div class="col-sm-9">
   @if(count($code_values))
   @foreach($code_values as $audit_code_value)
   <div class="col-sm-12">  

   </div>       

   <div class="accordion" id="accordionExample">
    <div class="card">

      <div class="card-header" id="headingOne{{$audit_code_value->id}}">

       <span data-toggle="collapse" data-target="#collapseOne{{$audit_code_value->id}}"> 
        {{ucwords(strtolower($audit_code_value->name))}} 
        <span class="pull-right"><i class="fa fa-plus"></i></span>
      </span>
      @if(!$osh_audit_employer->is_submitted)
      <div style="width:10%;" class="pull-right mr-2"> 
        <button class="update_feedback_btn btn {{($is_codevalue_filled[$audit_code_value->id]) ? 'btn-success' : 'btn-primary' }} btn-sm btn-block" data-feedback_group='{{ucwords(strtolower($audit_code_value->name))}}' data-employer_audit_id='{{$osh_audit_employer->id}}' data-code_value_id='{{$audit_code_value->id}}'> {{($is_codevalue_filled[$audit_code_value->id]) ? 'Edit &nbsp;' : 'Update' }} </button> 
      </div>
      @endif
    </div>

    <div id="collapseOne{{$audit_code_value->id}}" class="collapse" aria-labelledby="headingOne{{$audit_code_value->id}}" data-parent="#accordionExample">
      <div class="card-body">

       <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">
              {{ucwords(strtolower($audit_code_value->name))}} : Questions

            </th>
            <th scope="col">Response</th>
            <th scope="col">Comments</th>
          </tr>
        </thead>
        <tbody>
          @php
          $no=0;
          @endphp
          @foreach ($osh_audit_feedbacks as $audit_feedback)

          @php
          $question = $audit_feedback->oshAuditQuestion;
          $hide = (!is_null($question->parent_id));
          @endphp

          @if($audit_code_value->id == $question->code_value_id)
          <tr>
            <th scope="row">{{$no+=1}}</th>
            <td>{{$question->question}}</td>
            <td>
              {{$audit_feedback->feedback}}
            </td>
            <td>{{$audit_feedback->comment}}</td>
          </tr>
          @endif

          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
@endforeach

<div class="accordion" id="accordionExample">
  <div class="card">

    <div class="card-header" id="headingOne{{$osh_audit_employer->id}}">

     <span data-toggle="collapse" data-target="#collapseOne{{$osh_audit_employer->id}}"> 
       General Comments and Observations 
       <span class="pull-right"><i class="fa fa-plus"></i></span>
     </span>

     @if(!$osh_audit_employer->is_submitted)
     @if(!in_array(false,$is_codevalue_filled))
     <div style="width:10%;" class="pull-right mr-2"> 
      <button class="updateGeneralCommentBtn btn btn-sm btn-primary btn-block" data-feedback_group='General Comments and Observations' data-employer_audit_id='{{$osh_audit_employer->id}}'>
       Update
     </button> 
   </div>
   @else
   <div style="width:10%;" class="pull-right mr-2"> 
    <button disabled="disabled"  class=" btn btn-sm btn-primary btn-block" data-feedback_group='General Comments and Observations' data-employer_audit_id='{{$osh_audit_employer->id}}'>
     Update
   </button> 
 </div>
 @endif
 @endif

</div>

<div id="collapseOne{{$osh_audit_employer->id}}" class="collapse" aria-labelledby="headingOne{{$osh_audit_employer->id}}" data-parent="#accordionExample">
  <div class="card-body">

   <table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">
          General Comments and Observations
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row"></th>
        <td>{{$osh_audit_employer->general_comments}}</td>

      </tr>
    </tbody>
  </table>
</div>
</div>
</div>
</div>

@endif
</div>

<div class="col-sm-3">
  @include("backend/workplace_risk_assesment/osh_audit/includes/feedback_summary")
</div>
</div>
</div>
@include('backend.workplace_risk_assesment.osh_audit.includes.update_feedback_modal')  

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}



<script>
  $(document).ready(function () {




    autosize($("textarea.autosize"));
    $(".collapse.show").each(function(){
      $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    $(".collapse").on('show.bs.collapse', function(){
      $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function(){
      $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });

    let form = $('#updateFeedbackForm');

    $(".general_error").delay(10000).slideUp(3000, function() {
      $(this).alert('close');
    });

    $('.update_feedback_btn').on('click', function(e) {
      e.preventDefault();
      let employer_audit_id = $(this).data('employer_audit_id');
      let code_value_id = $(this).data('code_value_id');
      let feedback_group = $(this).data('feedback_group');
      addQuestionsToModal(employer_audit_id,code_value_id,feedback_group);
    });


    $(".district_selection").select2({
      dropdownParent: $("#updateGeneralCommentModal .modal-body"),
      minimumInputLength: 3,
      cache: true,
    });

    function addQuestionsToModal(employer_audit_id,code_value_id,feedback_group) {
      $('#updateFeedbackModalFields').empty();
      let qn_url = '{!! url("workplace_risk_assesment/audit/") !!}/'+employer_audit_id+'/questions/'+code_value_id;
      $.ajax({
        url: qn_url,
        type : 'GET',
        dataType: 'json',
        success:function(data){
          if (data.success) {
            $('.modalTitle').text(feedback_group);
            $('#updateFeedbackModal').modal('show');
            var a = data.data;
            if (a && a.length) {
              a.forEach(function(entry) {
                let option_select;
                // console.log(entry.feedback);
                if (entry.feedback == 'Yes') {
                 option_select = '<option selected="selected" value="">-- Select Answer --</option><option value="Yes" selected="selected">Yes</option><option value="No" >No</option>';
               } else if(entry.feedback == 'No') {
                option_select = '<option selected="selected" value="">-- Select Answer --</option><option value="Yes">Yes</option><option value="No"  selected="selected">No</option>';
              }else{
               option_select = '<option selected="selected" value="">-- Select Answer --</option><option value="Yes">Yes</option><option value="No" >No</option>';
             }
             let comment = entry.comment ? entry.comment : '';
             $('#updateFeedbackModalFields').append('<div class="row"><div class="col-xs-5"><label for="question" class="required">'+entry.question+'</label></div><div class="col-xs-2"><select class="form-control" id="answer1" name="answers['+entry.id+']">'+option_select+'</select></div><div class="col-xs-5"><textarea class="form-control autosize" rows="1" style="border-radius: 3px; overflow: hidden; overflow-wrap: break-word; resize: none; height: 2em;" name="comments['+entry.id+']" cols="50">'+comment+'</textarea></div><div class="col-xs-5">&nbsp;</div><div class="col-xs-5 text-center"><strong><p class=" error_display text-danger hidden" id="'+entry.id+'"></p></strong></div><div class="col-xs-2">&nbsp;</div></div><legend></legend><br>');
           });
            }
          } else {

          }
        }
      });
    }

    $('#updateFeedbackFormSubmit').on('click', function(e) {
      e.preventDefault();

      let form = $('#updateFeedbackForm');
      let urls = $('#updateFeedbackForm').attr('action');
      $('.general_error').addClass("hidden");
      $(form).find('p.error_display').each(function () {
        $(this).addClass("hidden");
        let p_id = $(this).attr('id');
        // console.log(p_id);
      });

      let options = {
        dataType : 'json',
        data : {},
        url: '{{route("backend.workplace_risk_assesment.audit.update_feedback")}}',
        type : 'POST',
        success : function (data) {
          if (data.success) {
            $('#updateFeedbackModal').modal('hide');
            swal({
              title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
              text: $('.modalTitle').text()+' updated successfully',
              type: 'success',
              showCancelButton: false,
              cancelButtonText: 'Cancel',
              confirmButtonColor: '#347BBD',
              confirmButtonText: 'OK',
              html: true,
              closeOnConfirm: true
            }, function (isConfirmed) {
              location.reload(null,false);
            });
          } else {
            if (data.errors) {

              $('.general_error').removeClass("hidden");
              $('.general_error').text('Request Failed! Something went wrong. Kindly check and try again');
              let error_array =  Object.entries(data.errors);
              for(let [key, value] of error_array){
                let error_id = key.match(/(\d+)/);
                $(form).find('p#'+error_id[0]).removeClass("hidden");
                $(form).find('p#'+error_id[0]).text(value);
              }
            }
            else{
              swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
            }
          }
        },
        error: function (data) {

        }
      };

      $(form).ajaxSubmit(options);
    });



    $('.updateGeneralCommentBtn').on('click', function(e) {
      e.preventDefault();
      let employer_audit_id = $(this).data('employer_audit_id');
      let feedback_group = $(this).data('feedback_group');
      $('#updateGeneralCommentForm').trigger('reset');
      $('#updateGeneralCommentForm').find('p.error_display').each(function () {
        $(this).addClass("hidden");
      });
      $('#modalTitle').text(feedback_group);
      $('#updateGeneralCommentModal').modal('show');
    });


    $('#updateGeneralCommentFormSubmit').on('click', function(e) {
      e.preventDefault();

      let form = $('#updateGeneralCommentForm');
      $('.general_error').addClass("hidden");

      let options = {
        dataType : 'json',
        data : {},
        url: '{{route("backend.workplace_risk_assesment.audit.update_feedback_general_info")}}',
        type : 'POST',
        success : function (data) {
          if (data.success) {
            $('#updateGeneralCommentModal').modal('hide');
            swal({
              title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
              text: $('#modalTitle').text()+' updated successfully',
              type: 'success',
              showCancelButton: false,
              cancelButtonText: 'Cancel',
              confirmButtonColor: '#347BBD',
              confirmButtonText: 'OK',
              html: true,
              closeOnConfirm: true
            }, function (isConfirmed) {
              location.reload(null,false);
            });
          } else {
            if (data.errors) {

              $('.general_error').removeClass("hidden");
              $('.general_error').text('Request Failed! Something went wrong. Kindly check and try again');

              if (data.errors.workplace_name){
                $('#error_workplace_name').removeClass('hidden');
                $('#error_workplace_name').text(data.errors.workplace_name);
              }

              if (data.errors.workplace_address){
                $('#error_workplace_address').removeClass('hidden');
                $('#error_workplace_address').text(data.errors.workplace_address);
              }
              if (data.errors.workplace_location){
                $('#error_workplace_location').removeClass('hidden');
                $('#error_workplace_location').text(data.errors.workplace_location);
              }

              if (data.errors.contact_person){
                $('#error_contact_person').removeClass('hidden');
                $('#error_contact_person').text(data.errors.contact_person);
              }

              if (data.errors.phone){
                $('#error_phone').removeClass('hidden');
                $('#error_phone').text(data.errors.phone);
              }

              if (data.errors.workplace_address){
                $('#error_workplace_address').removeClass('hidden');
                $('#error_workplace_address').text(data.errors.workplace_address);
              }
            }
            else{
              swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
            }
          }
        },
        error: function (data) {

        }
      };

      $(form).ajaxSubmit(options);
    });

  });
</script>;

@endpush
