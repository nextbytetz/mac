@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_audit.title'), 'header_title' => 'Assigned Osh Audit List'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
<style>
	.custom_filter:after {
		background-color: #F5F5F5;
		border: 1px solid #DDDDDD;
		border-radius: 4px 0 4px 0;
		color: #3c5ba4;
		content: "@lang('labels.backend.system.workflow.custom_filter')";
    left: -1px;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
  }

  .custom_filter {
   background-color: #FFFFFF;
   border: 1px solid #DDDDDD;
   border-radius: 4px 4px 4px 4px;
   margin: 5px 0px;
   padding: 39px 19px 14px;
   position: relative;
 }

 div.dt-buttons {
  clear: both;
}

</style>
@endpush

@php

$fn_years = [];
if (count($fin_years)) {
 foreach ($fin_years as $fin_year) {
  $fn_years[$fin_year->fin_year] = $fin_year->fin_year;
}
}

@endphp

@section('content')
<div class="custom_filter">
 {!! Form::open(['role' => 'form', 'route' => ['backend.workplace_risk_assesment.audit.assigned_list'],'method'=>'get','id' => 'checklist-custom']) !!}
 <div class="row">
  <div class="col-md-12 col-sm-12">
   <div class="form-row">
    <div class="form-group col-md-3">
     <label for="financial year">Financial Year</label>
     {!! Form::select('financial_year',  $fn_years, null, ['class' => 'form-control search-select', 'id' => 'financial_year', 'placeholder' => '']) !!}
   </div>
   <div class="form-group col-md-3">
     <label for="period">Audit Period</label>
     {!! Form::select('period', ['0' => 'All', '1' => 'July - December', '2' => 'January - June'], null, ['class' => 'form-control ', 'id' => 'period']) !!}
   </div>
 </div>
 <div class="form-row">
  <div class="form-group col-md-12">
   <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
   <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
 </div>
</div>
</div>
</div>
{!! Form::close() !!}
</div>

<div class="row pt-2">
 <div class="col-md-12">
  {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'assigned_audit_list-table'],true) !!}
</div>
</div>

@endsection

@push('after-script-end')

{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script type="text/javascript">
 $(function() {
  $(".search-select").select2({});
});
</script>
@endpush