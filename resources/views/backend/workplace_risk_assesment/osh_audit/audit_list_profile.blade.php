@extends('layouts.backend.main', ['title' => 'OSH Audit List Profile', 'header_title' => 'OSH Audit List Profile '.$osh_audit->fin_year])

@push('after-styles-end')

@endpush

@include('backend.includes.datatable_assets')

@section('content')

<div class="row">
  <div class="col-md-12">

    <div class="basic_nav_pills nav_basic_tab">
      <ul class="nav nav-tabs">
       <li class="nav-item ">
        <a class="nav-link active"  href="#general" data-toggle="tab">General</a>
      </li>
      @if(!empty($osh_audit->status))
      <li class="nav-item">
        <a class="nav-link"  id = "tab_2_header"  href="#workflow_tab" data-toggle="tab">Workflow</a>
      </li>
      @endif
    </ul>
    <div class="nav_tab_contain tab-content">
      <div id="general" class="nav_tab_pane tab-pane active in">
        @if($initiate_wf)
        <div class="row">
          <div class="col-sm-12">
            <a href='{!! route('backend.workplace_risk_assesment.audit.submit_period_list', $osh_audit->id) !!}' class="btn btn-success pull-right">Initiate Workflow</a>
          </div>
        </div>
        @endif
        <br/>
        <legend></legend>
        <br>
        <div class="row">
         <div class="col-md-12">
           {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'financial_years-table'],true) !!}
         </div>

       </div>
     </div>
     @if(!empty($osh_audit->status))
     <div id="workflow_tab" class="nav_tab_pane tab-pane">
      @php
      $workflowinput = ['resource_id' => $osh_audit->id, 'wf_module_group_id'=> 26, 'type' => -1];
      @endphp
      {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

    </div>
    @endif
  </div>
</div>
</div>



</div>
@stop

@push('after-script-end')

{!! $dataTable->scripts() !!}
<script>
 $(function(){

   if (location.hash !== '') {
    $('a[href="' + location.hash + '"]').tab('show');
    $('a[href="' + location.hash + '"]').trigger('click');
  }


  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var tab = $(e.target).attr('href').substr(1);
    if (history.pushState) {
      history.pushState(null, null, '#' + tab);
    } else {
      location.hash = '#' + tab;
    }
  });


  $('#financial_year_profile-table').DataTable({
    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $(nRow).click(function() {
        // document.location.href = "/workplace_risk_assesment/audit/employer_profile" ;
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });
});

</script>



@endpush