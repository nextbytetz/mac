@extends('layouts.backend.main', ['title' => 'OSH Audit List Profile', 'header_title' => 'OSH Audit List Profile '.$osh_audit->fin_year])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush


@section('content')

<div class="row">
  <div class="col-md-12">

    <div class="basic_nav_pills nav_basic_tab">
      <ul class="nav nav-tabs">
       <li class="nav-item ">
        <a class="nav-link active"  href="#general" data-toggle="tab">General</a>
      </li>
      @if(!empty($osh_audit->status))
      <li class="nav-item">
        <a class="nav-link"  id = "tab_2_header"  href="#workflow_tab" data-toggle="tab">Workflow</a>
      </li>
      @endif
    </ul>
    <div class="nav_tab_contain tab-content">
      <div id="general" class="nav_tab_pane tab-pane active in">
        @if($initiate_wf)
        <div class="row">
          <div class="col-sm-12">
            {!! Form::open(['role' => 'form', 'class' => 'form-inline','id' => 'recommend_list_form','route' => 'backend.workplace_risk_assesment.audit.submit_period_list']) !!}
            <input class="form-control hidden" type="text" name="osh_audit_period_id" value="{{$osh_audit->id}}">
            <input class="form-control hidden" type="text" name="fatal" value="{{$fatal}}">
            <input class="form-control hidden" type="text" name="lti" value="{{$lti}}">
            <input class="form-control hidden" type="text" name="non_conveyance" value="{{$non_conveyance}}">
            <span class="pull-right">
              <input type="submit" id="recommend_list_submit" class="btn btn-success btn-submit " value="Recommend List" />
              <span class="wait hidden">
                <img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 65%; width:20%;" />  
                &nbsp; <span class="h6"> Please wait .... </span>
              </span>
            </span>
            {!! Form::close() !!}
            {{-- <a href='{!! route('backend.workplace_risk_assesment.audit.submit_period_list', $osh_audit->id) !!}' class="btn btn-success pull-right">Recommend List</a> --}}
          </div>
        </div>
        @endif
        <br/>
        <legend></legend>
        <br>
        <div class="row">
         <div class="col-md-12">
          @include("backend/workplace_risk_assesment/osh_audit/includes/list_view_includes")
        </div>

      </div>
    </div>
    @if(!empty($osh_audit->status))
    <div id="workflow_tab" class="nav_tab_pane tab-pane">
      @php
      $workflowinput = ['resource_id' => $osh_audit->id, 'wf_module_group_id'=> 28, 'type' => -1];
      @endphp
      {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
    </div>
    @endif
  </div>
</div>
</div>



</div>
@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script>
 $(function(){

   if (location.hash !== '') {
    $('a[href="' + location.hash + '"]').tab('show');
    $('a[href="' + location.hash + '"]').trigger('click');
  }


  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var tab = $(e.target).attr('href').substr(1);
    if (history.pushState) {
      history.pushState(null, null, '#' + tab);
    } else {
      location.hash = '#' + tab;
    }
  });


  $('#financial_year_profile-table').DataTable({
    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $(nRow).click(function() {
        // document.location.href = "/workplace_risk_assesment/audit/employer_profile" ;
      }).hover(function() {
        $(this).css('cursor','pointer');
      }, function() {
        $(this).css('cursor','auto');
      });
    }
  });


  $('#recommend_list_submit').on('click', function(e) {
    e.preventDefault();

    $('#recommend_list_submit').addClass('hidden');
    $('.wait').removeClass('hidden');

    let form = $('#recommend_list_form');
    let urls = $('#recommend_list_form').attr('action');
    let options = {
      dataType : 'json',
      url: urls,
      type : 'POST',
      success : function (data) {
        $('.wait').addClass('hidden');
        if (data.success) {
          swal({
            title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
            text: '<h6>The recommended {{$osh_audit->fin_year}} employers list for Osh Audit has been submitted for further action and approval</h6>',
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#347BBD',
            confirmButtonText: 'OK',
            html: true,
            closeOnConfirm: true
          }, function (isConfirmed) {
            location.reload(null,false);
          });
        } else {
          $('#recommend_list_submit').removeClass('hidden');
          $('.wait').addClass('hidden');
          if (data.errors) {
            swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly refresh the page and try again','error');
          }
          else{
            swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
          }
        }
      },
      error: function (data) {

      }
    };

    $(form).ajaxSubmit(options);
  });

});

</script>



@endpush