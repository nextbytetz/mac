@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_audit.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_audit.list_title')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush

@section('content')
@php

for($i=0; $i < 1000 ; $i++){
	$count_array[$i] = $i;
}

@endphp
{!! Form::open(['role' => 'form','id' => 'period_employers_form', 'method'=>'get', 'route' => 'backend.workplace_risk_assesment.audit.index']) !!}
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="form-row">
			<div class="form-group col-md-3">
				<label for="financial year">Financial Year</label>
				{!! Form::select('financial_year',  $fin_years, null, ['class' => 'form-control search-select', 'id' => 'financial_year', 'placeholder' => '']) !!}
			</div>
			<div class="form-group col-md-3">
				<label for="fatal"> Fatal </label>
				{!! Form::select('fatal', $count_array, null, ['class' => 'form-control search-select', 'id' => 'period']) !!}
			</div>

			<div class="form-group col-md-3">
				<label for="lti"> Lost Time Injuries </label>
				{!! Form::select('lti', $count_array, null, ['class' => 'form-control search-select', 'id' => 'lti']) !!}
			</div>

			<div class="form-group col-md-3">
				<label for="non_conveyance"> Non Conveyance </label>
				{!! Form::select('non_conveyance', $count_array, null, ['class' => 'form-control search-select', 'id' => 'non_conveyance']) !!}
			</div>

		</div>
		<div class="form-row">
			<div class="form-group col-md-12">
				<input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
				<input type="submit" class="btn btn-success btn-sm btn-submit " value="@lang('buttons.general.search')" />
			</div>
		</div>
	</div>
	{!! Form::close() !!}

	<div class="col-md-12 pt-2">
		{!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'period_employers_table'],true) !!}
	</div>
</div>

@endsection

@push('after-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script type="text/javascript">

	$(function(){

		$(".search-select").select2({});

		$('#period_employers_table').on('click','.periodActivateButton',function(e){
			e.preventDefault();


		});


	});

</script>
@endpush