@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_audit.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_audit.list_title')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')
<div class="row">
	<div class="col-md-12">
		{!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'financial_years-table'],true) !!}
	</div>
</div>

@endsection

@push('after-script-end')

{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

<script type="text/javascript">

	$(function(){

		$('#financial_years-table').on('click','.periodActivateButton',function(e){
			e.preventDefault();
			let osh_audit_period = $(this).data('id');
			let osh_audit_name = $(this).data('name');
			swal({
				title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
				text: "<h6>This will activate "+osh_audit_name+' period</h6>',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Cancel',
				confirmButtonColor: '#f0ad4e',
				confirmButtonText: 'Confirm',
				html: true,
				closeOnConfirm: true
			},
			function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						url : '{{url('workplace_risk_assesment/audit/activate_period/')}}/'+osh_audit_period,
						type: 'GET',
						dataType : 'json',
						success:function(data){
							if (data.success == true) {
								$('#financial_years-table').DataTable().ajax.reload(null,false);
								swal({
									title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
									text: "<h6>The "+osh_audit_name+' period has been activated</h6>',
									type: 'success',
									showCancelButton: false,
									
									confirmButtonText: 'OK',
									html: true,
									closeOnConfirm: true
								});
							}
						}
					});
				}
			});

		});


	});

</script>
@endpush