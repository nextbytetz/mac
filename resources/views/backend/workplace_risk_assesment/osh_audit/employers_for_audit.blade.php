@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_audit.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_audit.list_title')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
<style>
	.custom_filter:after {
		background-color: #F5F5F5;
		border: 1px solid #DDDDDD;
		border-radius: 4px 0 4px 0;
		color: #3c5ba4;
		content: "@lang('labels.backend.system.workflow.custom_filter')";
    left: -1px;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
  }

  .custom_filter {
   background-color: #FFFFFF;
   border: 1px solid #DDDDDD;
   border-radius: 4px 4px 4px 4px;
   margin: 5px 0px;
   padding: 39px 19px 14px;
   position: relative;
 }

 div.dt-buttons {
  clear: both;
}

</style>
@endpush

@php

$fn_years = [];
if (count($fin_years)) {
 foreach ($fin_years as $fin_year) {
  $fn_years[$fin_year->fin_year] = $fin_year->fin_year;
}
}

@endphp

@section('content')
<div class="custom_filter">
 {!! Form::open(['role' => 'form', 'route' => ['backend.workplace_risk_assesment.audit.checklist_view'],'method'=>'get','id' => 'checklist-custom']) !!}
 <div class="row">
  <div class="col-md-12 col-sm-12">
   <div class="form-row">
    <div class="form-group col-md-3">
     <label for="financial year">Financial Year</label>
     {!! Form::select('financial_year',  $fn_years, null, ['class' => 'form-control search-select', 'id' => 'financial_year', 'placeholder' => '']) !!}
   </div>
   <div class="form-group col-md-3">
     <label for="period">Audit Period</label>
     {!! Form::select('period', ['0' => 'All', '1' => 'July - December', '2' => 'January - June'], null, ['class' => 'form-control ', 'id' => 'period']) !!}
   </div>
    				{{-- <div class="form-group col-md-3">
    					<label for="region">District:</label>
    					{!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
    				</div> --}}
    			</div>
    			<div class="form-row">
    				<div class="form-group col-md-12">
    					<input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
    					<input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
    				</div>
    			</div>
    		</div>
    	</div>
    	{!! Form::close() !!}
    </div>

    @permission("assign_staff_for_osh_audit")
    {{--Assign to--}}
    <div style="background-color: #FFFFFF; border: 1px solid #DDDDDD; border-radius: 4px 4px 4px 4px; margin: 5px 0px; padding: 39px 19px 14px; position: relative;">
      <div class="row">
       <div class="col-md-12">
        <div class="form-row">
         {!! Form::open(['role' => 'form','id' => 'assign_staff_form', 'route' => 'backend.workplace_risk_assesment.audit.assign_staff']) !!}
         <div class="form-group offset-md-5 col-md-5 assign_user_select">

          <label for="assigned_user">Assign To:</label>
          {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}

        </div>
        <div class="form-group col-md-2">
          {{ method_field('PUT') }}
          <label for="allocate_submit">&nbsp;</label>
          <input type="submit" class="form-control btn btn-success btn-sm hidden" disabled value="Submit" id="assign_submit">
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endauth

<div class="row">
 <div class="col-md-12">
  {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'employer_for_audit-table'],true) !!}
</div>
</div>

@endsection

@push('after-script-end')

{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script type="text/javascript">
 $(function() {
  $(".search-select").select2({});

  @permission("assign_staff_for_osh_audit")

  $('#employer_for_audit-table').on('change','.select_employers',function(e){
   let selectedEmployers = [];
   $('#employer_for_audit-table .select_employers:checked').each(function(){
    let id = $(this).val();
    selectedEmployers.push(id);
  });
   if(selectedEmployers.length > 0){
    $('#assign_submit').attr('disabled',false);
    $('#assign_submit').removeClass('hidden');
  }else{
    $('#assign_submit').attr('disabled',true);
    $('#assign_submit').addClass('hidden');
  }
});

  $('#assign_submit').on('click', function(e) {
   e.preventDefault();

   let user_name = $('#assigned_user option:selected').text();
   let selected_employers_count = 0;
   let form = $('#assign_staff_form');
   let urls = $('#assign_staff_form').attr('action');

   $(form).find('input[name="selected_employers[]"]').remove();
   $('#employer_for_audit-table .select_employers:checked').each(function(){
    let id = $(this).val();
    $(form).append($('<input>').attr('type', 'hidden').attr('name', 'selected_employers[]').val(id));
    selected_employers_count++;
  });
   swal({
    title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
    text: '<h6>This will assign the '+selected_employers_count+' selected employers to <br><strong>'+user_name+'</strong> for OSH audit!</h6> <h6>Are you sure you want to proceed?</h6>',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancel',
    confirmButtonColor: '#f0ad4e',
    confirmButtonText: 'Confirm',
    html: true,
    closeOnConfirm: true
  }, function (isConfirmed) {
    if (isConfirmed) {
     let options = {
      dataType : 'json',
      data : {},
      type : 'POST',
      success : function (data) {
       if (data.success) {
        $('#employer_for_audit-table .select_employers:checked').attr('checked',false);
        $('#employer_for_audit-table').DataTable().ajax.reload();
        $('#assign_submit').attr('disabled',true);
        $('#assign_submit').addClass('hidden');

        if (selected_employers_count<2) {
         msg = selected_employers_count+' employer have been assigned to '+user_name+' for OSH auditing';
       } else {
         msg = selected_employers_count+' employers have been assigned to '+user_name+' for OSH auditing';
       }
       $.amaran({
         'theme'     :'awesome success',
         'content'   :{
          title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
          message: msg,
          info:'',
          icon: 'fa fa-check-square-o'
        },
        'position'  :'bottom left',
        'outEffect' :'slideBottom',
        'inEffect'  :'slideLeft'
      });
     } else {
      if (data.errors) {
       if (data.errors.assigned_user) {
        swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}',data.errors.assigned_user,'warning');
      }if(data.errors.selected_employers){
        swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}',data.errors.selected_employers,'warning');
      }
    }
    else{
     swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
   }
 }
},
error: function (data) {

}
};
$(form).ajaxSubmit(options);
}
});
 });

  @endauth

});
</script>
@endpush