@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_audit.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_audit.check_list.update_title')])


@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')

<div class="col-md-12 col-sm-12">
  <h5 class="client-title">
    <i class="icon-circle"></i>
    <strong>{{$osh_audit_employer->name}}</strong>
    <small >
      Reg No#:  {{substr($osh_audit_employer->reg_no, -6)}}
    </small>
  </h5>
  <legend></legend>
</div>
{{--  $.each( data.errors, function( key, value ) {
                
            //    if (data.errors.key){
            //     $('.error_'+key).removeClass('hidden');
            //     $('.error_'+key).text(value);
            //   }
            // }); --}}

<div>&nbsp;</div>
{!! Form::open(['role' => 'form', 'id' => 'update_feedback_form', 'route' => 'backend.workplace_risk_assesment.audit.update_feedback']) !!}
<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-danger general_error  hidden" role="alert"></div>
    {!! Form::text('osh_audit_employer_id', $osh_audit_employer->id, ['class' => 'form-control hidden', 'id' => 'osh_audit_employer_id']) !!}

  </div>
  <div class="col-md-12">
   
    @foreach($code_values as $audit_code_value)
    <div class="accordion" id="accordionExample">

      <div class="card">
        <div class="card-header" id="headingOne{{$audit_code_value->id}}">
          <span data-toggle="collapse" data-target="#collapseOne{{$audit_code_value->id}}"> {{ucwords(strtolower($audit_code_value->name))}} <span class="pull-right"><i class="fa fa-plus"></i></span></span>
        </div>
        <div id="collapseOne{{$audit_code_value->id}}" class="collapse" aria-labelledby="headingOne{{$audit_code_value->id}}" data-parent="#accordionExample">
          <div class="card-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">{{ucwords(strtolower($audit_code_value->name))}} : Questions</th>
                  <th scope="col">Response</th>
                  <th scope="col">Comments</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($osh_audit_feedbacks as $audit_feedback)

                @php
                $question = $audit_feedback->oshAuditQuestion;
                $hide = (!is_null($question->parent_id));
                @endphp

                @if($audit_code_value->id == $question->code_value_id)
                <tr>
                  <th scope="row"></th>
                  <td>{!! Form::label( 'question', $question->question, ['class' => 'required'])!!}</td>
                  <td> @if($question->data_type == 'traffic')
                    {!!  Form::select('answers['.$audit_feedback->id.']', ['Yes' => 'Yes', 'No'=> 'No'],  $audit_feedback->feedback, ['class' => 'form-control' , 'placeholder'=> '-- Select Answer --', 'id' => 'answer' . $question->id]) !!}
                    @else
                    {!! Form::textarea('answers['.$audit_feedback->id.']', $audit_feedback->feedback, ['class' => 'form-control autosize',  'rows'=>'1',  'style' => 'border-radius: 3px;']) !!}
                    @endif
                    <strong><p class=" error_display text-danger hidden" id="{{$audit_feedback->id}}"></p></strong>
                  </td>
                  <td>{!! Form::textarea('comments['.$audit_feedback->id.']', $audit_feedback->comment, ['class' => 'form-control autosize',  'rows'=>'1',  'style' => 'border-radius: 3px;']) !!}</td>
                </tr>
                @endif

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>


<legend></legend>
<br/>
{{--Buttons--}}
<div class="row">
  <div class="col-md-12" class="form-inline" >
    <div class="element-form">
      <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
      <div class="col-xl-6 col-lg-6 col-sm-12 col-md-9 col-xs-12">
        <div class="pull-right">
          {!! Form::button("Update",['class' => 'btn btn-primary site-btn save_button', 'id'=>'submit_update_feedback', 'type'=>'submit']) !!}
        </div>
      </div>
    </div>
  </div>
</div>

{!! Form::close() !!}

@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}



<script>
  $(document).ready(function () {

    autosize($("textarea.autosize"));
    $(".collapse.show").each(function(){
      $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    $(".collapse").on('show.bs.collapse', function(){
      $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function(){
      $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });

    let form = $('#update_feedback_form');

    $(".general_error").delay(10000).slideUp(3000, function() {
      $(this).alert('close');
    });

    $('#submit_update_feedback').on('click', function(e) {
      e.preventDefault();

      let form = $('#update_feedback_form');
      let urls = $('#update_feedback_form').attr('action');
      $('.general_error').addClass("hidden");
      $(form).find('p.error_display').each(function () {
        $(this).addClass("hidden");
        let p_id = $(this).attr('id');
        // console.log(p_id);
      });

      let options = {
        dataType : 'json',
        data : {},
        type : 'POST',
        success : function (data) {
          if (data.success) {
            $.amaran({
              'theme'     :'awesome success',
              'content'   :{
                title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
                message: 'Success! Feedback submitted',
                info:'',
                icon: 'fa fa-check-square-o'
              },
              'position'  :'bottom left',
              'outEffect' :'slideBottom',
              'inEffect'  :'slideLeft'
            });
          } else {
            if (data.errors) {

              $('.general_error').removeClass("hidden");
              $('.general_error').text('Request Failed! Something went wrong. Kindly check and try again');
              let error_array =  Object.entries(data.errors);
              for(let [key, value] of error_array){
                let error_id = key.match(/(\d+)/);
                $(form).find('p#'+error_id[0]).removeClass("hidden");
                $(form).find('p#'+error_id[0]).text(value);
              }
            }
            else{
              swal('Sorry {{ucwords(strtolower(access()->user()->lastname))}}','An error has occured! Kindly try again','error');
            }
          }
        },
        error: function (data) {

        }
      };

      $(form).ajaxSubmit(options);
    });

  });
</script>;

@endpush
