    <div class="basic_nav_pills nav_basic_tab">
      <ul class="nav nav-tabs">
       <li class="nav-item ">
        <a class="nav-link active"  href="{{'#quater'.$quaters[0]}}" data-toggle="tab">Quater {{$quaters[0]}}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link"  id = "tab_2_header"  href="{{'#quater'.$quaters[1]}}" data-toggle="tab">Quater {{$quaters[1]}}</a>
      </li>
    </ul>
    <div class="nav_tab_contain tab-content">
      <div id="{{'quater'.$quaters[0]}}" class="nav_tab_pane tab-pane active in">
        <div class="row">
          <div class="col-sm-12 col-md-10">
            <table class="display table-hover table-bordered" cellspacing="0" width="100%" id ="{{'quater_'.$quaters[0].'_table'}}">
              <thead>
                <tr>
                  <th>Employer</th>
                  <th>Reg No#</th>
                  <th>Fatal</th>
                  <th>Lost Time Injuries</th>
                  <th>Non Conveyence Claims</th>
                </tr>
              </thead>
            </table>

          </div>
          <div class="col-sm-none col-md-2">
            @include("backend/workplace_risk_assesment/osh_audit/includes/criteria_summary")
          </div>
        </div>
      </div>
      <div id="{{'quater'.$quaters[1]}}" class="nav_tab_pane tab-pane">
       <div class="row">
        <div class="col-sm-12 col-md-10">
          <table class="display table-hover table-bordered" cellspacing="0" width="100%" id ="{{'quater_'.$quaters[1].'_table'}}">
            <thead>
              <tr>
                <th>Employer</th>
                <th>Reg No#</th>
                <th>Fatal</th>
                <th>Lost Time Injuries</th>
                <th>Non Conveyence Claims</th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="col-sm-none col-md-2">
          @include("backend/workplace_risk_assesment/osh_audit/includes/criteria_summary")
        </div>
      </div>
    </div>
  </div>
</div>



@push('after-script-end')

<script type="text/javascript">

  $(function(){

    @php
    if ($osh_audit->status) {
      $urls1 = url('workplace_risk_assesment/audit/return_quater_submitted_datatable/'.$osh_audit->id.'/'.$quaters[0]);
      $urls2 = url('workplace_risk_assesment/audit/return_quater_submitted_datatable/'.$osh_audit->id.'/'.$quaters[1]);

    } else {
      $urls1  = url('workplace_risk_assesment/audit/return_quater_datatable/'.$osh_audit->id.'/'.$fatal.'/'.$lti.'/'.$non_conveyance.'/'.$quaters[0]);
      $urls2 = url('workplace_risk_assesment/audit/return_quater_datatable/'.$osh_audit->id.'/'.$fatal.'/'.$lti.'/'.$non_conveyance.'/'.$quaters[1]);
      
    }
    @endphp

    $('{{'#quater_'.$quaters[0].'_table'}}').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,

      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{{$urls1}}',
        type : 'get'
      },
      columns: [
      { data: 'employer', name: 'employer'},
      { data: 'regno', name: 'regno'},
      { data: 'fatal', name: 'fatal','orderable': false, 'searchable': false },
      { data: 'lti', name:'lti', 'orderable': false, 'searchable': false },
      { data: 'non_conveyance', name:'non_conveyance', 'orderable': false, 'searchable': false},
      ],
      "rowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).click(function() {
          document.location.href = '{{url("workplace_risk_assesment/audit/")}}/'+aData['osh_audit_period_id']+'/employer_profile/'+aData['employer_id'];
        }).hover(function() {
          $(this).css('cursor','pointer');
        }, function() {
          $(this).css('cursor','auto');
        });
      },
    });


    $('{{'#quater_'.$quaters[1].'_table'}}').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : '{{$urls2}}',
        type : 'get'
      },
      columns: [
      { data: 'employer', name: 'employer'},
      { data: 'regno', name: 'regno'},
      { data: 'fatal', name: 'fatal','orderable': false, 'searchable': false },
      { data: 'lti', name:'lti', 'orderable': false, 'searchable': false },
      { data: 'non_conveyance', name:'non_conveyance', 'orderable': false, 'searchable': false},
      ],
      "rowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).click(function() {
          document.location.href = '{{url("workplace_risk_assesment/audit/")}}/'+aData['osh_audit_period_id']+'/employer_profile/'+aData['employer_id'];
        }).hover(function() {
          $(this).css('cursor','pointer');
        }, function() {
          $(this).css('cursor','auto');
        });
      },
    });

  });

</script>
@endpush