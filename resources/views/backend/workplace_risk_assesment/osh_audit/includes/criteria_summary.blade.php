  <div class="row">
    <div class="grey_modal">
      <table style="width:95%">
        <tr>
          <td  align="center"><h5><strong><span class="light_dark_color">Criteria Used</span></strong></h5></td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg" style="min-height: 15em;">
      <p class="underline" style="padding-bottom: 2%;" >
        <span style="font-weight: lighter;">Fatal : </span> 
        <span style="font-weight: bold;" >
          Atleast {{!is_null($osh_audit->status) ? $osh_audit->fatal : $fatal}}
        </span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">Lost Time Injuries :</span> 
        <span style="font-weight: bold;" > Atleast {{!is_null($osh_audit->status) ? $osh_audit->lost_time_injury : $lti }}</span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">Non Conveyance:</span> 
        <span style="font-weight: bold;" >Atleast {{!is_null($osh_audit->status) ? $osh_audit->non_conveyance : $non_conveyance }}</span>
      </p>
    </div>
  </div>
