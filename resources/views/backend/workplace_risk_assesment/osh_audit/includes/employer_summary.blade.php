        <div class="row">

            <div class="grey_modal">
                <table style="width:100%">
                    <tr>
                        <td  align="center"><h5><b><span class="light_dark_color">Employer Summary</span></b></h5></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row" >
            <div class="light_grey_bg">&nbsp;</div>
            <div class="col-md-12 light_grey_bg" style="min-height: 360px;">
                <p class="underline" style="padding-bottom: 2%;" >
                    <span style="font-weight: lighter;">Period : </span> 
                    <span style="font-weight: bold;" > {{$osh_audit_employer->fin_year}} : {{($osh_audit_employer->period == 1) ? ' July - December ' : 'January - June' }}</span>
                </p>
                <p class="underline" style="padding-top: 2%;">
                    <span style="font-weight: lighter;">Location :</span> 
                    <span style="font-weight: bold;" > {{$osh_audit_employer->region}} - {{$osh_audit_employer->district}}</span>
                </p>
                <p class="underline" style="padding-top: 2%;">
                    <span style="font-weight: lighter;">Fatal Cases :</span> 
                    <span style="font-weight: bold;" > {{empty($osh_audit_employer->number_of_fatal) ? 0 : $osh_audit_employer->number_of_fatal}}</span>
                </p>
                <p class="underline" style="padding-top: 2%;" >
                    <span style="font-weight: lighter;">Lost Working Days :</span> 
                    <span style="font-weight: bold;" >
                        {{empty($osh_audit_employer->number_of_days_lost) ? 0 : $osh_audit_employer->number_of_days_lost}}
                    </span>
                </p>
                <p class="underline" style="padding-top: 2%; padding-bottom: 1%;">
                    <span style="font-weight: lighter;">Claims Exclude Conveyance Accident  :</span> 
                    <span style="font-weight: bold;" >
                        {{empty($osh_audit_employer->not_conveyance_accident) ? 0 : $osh_audit_employer->not_conveyance_accident}}
                    </span>
                </p>
                <p class="underline" style="padding-top: 2%;" >
                    <span style="font-weight: lighter;">Total Employees :</span> <span style="font-weight: bold;" >
                        {{$osh_audit_employer->employee_count}}
                    </span>
                </p>
                <p class="underline" style="padding-top: 2%;" >
                    <span style="font-weight: lighter;"> Rating :</span> <span style="font-weight: bold;" >
                        {{$feedback_score['overall']['percent']}} - 
                        <span class="tag {{$feedback_score['rating']['tag']}} white_color"> {{ucwords(strtolower($feedback_score['rating']['score']))}} Performer</span>
                    </span>
                </p>
            </div>
        </div>
