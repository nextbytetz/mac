  <div class="row">
    <div class="grey_modal">
      <table style="width:100%">
        <tr>
          <td  align="center"><h5><strong><span class="light_dark_color">Feedback Summary</span></strong></h5></td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg" style="min-height: 360px;">
      <p class="underline" style="padding-bottom: 2%;" >
        <span style="font-weight: lighter;">Period :</span> 
        <span style="font-weight: bold;" >
          {{$osh_audit_employer->fin_year}} : {{($osh_audit_employer->period == 1) ? ' July - December ' : 'January - June' }}
        </span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">Workplace Name:</span> 
        <span style="font-weight: bold;" > {{ucwords(strtolower($osh_audit_employer->workplace_name))}}</span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">Workplace Address :</span> 
        <span style="font-weight: bold;" >{{$osh_audit_employer->workplace_address}}</span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">Workplace Location :</span> 
        <span style="font-weight: bold;" >{{$osh_audit_employer->workplace_location}}</span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">Workplace District :</span> 
        <span style="font-weight: bold;" >{{!is_null($osh_audit_employer->district_id) ? $osh_audit_employer->district.' - '.$osh_audit_employer->region : null}}</span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">Contact Person :</span> 
        <span style="font-weight: bold;" >{{$osh_audit_employer->contact_person}}</span>
      </p>
      <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">@lang('labels.general.phone') :</span> 
        <span style="font-weight: bold;" >{{$osh_audit_employer->contact_person_phone}}</span>
      </p>
     {{--  <p class="underline" style="padding-top: 2%;">
        <span style="font-weight: lighter;">@lang('labels.backend.member.business_activities') :</span> 
        <span style="font-weight: bold;" ></span>
      </p> --}}

      @if($osh_audit_employer->is_submitted)
      <p class="underline" style="padding-top: 2%;" >
        <span style="font-weight: lighter;"> Rating :</span> <span style="font-weight: bold;" >
          {{$feedback_score['overall']['percent']}} - 
          <span class="tag {{$feedback_score['rating']['tag']}} white_color"> {{ucwords(strtolower($feedback_score['rating']['score']))}} Performer</span>
        </span>
      </p>
      @endif
    </div>
  </div>
