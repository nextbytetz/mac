  <div class="row">
    <div class="col-md-12">
      <table class="display table-hover table-bordered" cellspacing="0" width="100%" id ="non_conveyance-table">
       <thead>
        <tr>
          <th>Case No#</th>
          <th>Name</th>
          <th>Incident</th>
          <th>Reporting Date</th>
        </tr>
      </thead>
    </table>

  </div>
</div>

@push('after-script-end')


<script  type="text/javascript">
  $(function() {
    $('#non_conveyance-table').DataTable({  
      processing: true,
      serverSide: true,
      stateSave: true,
      searching: true,
      paging: true,
      info: true,
      ajax:{
        url : '{!! route("backend.workplace_risk_assesment.audit.profile_notifications", ['osh_audit_employer_id' => $osh_audit_employer->id, 'type' => 'non_conveyance']) !!}',
        type : 'GET'
      },
      columns: [
      { data: 'filename' , name: 'notification_reports.filename'},
      { data: 'name' , name: 'name'},
      { data: 'incident' , name: 'incident_types.name'},
      { data: 'reporting_date' , name: 'notification_reports.reporting_date'},
      ],
      rowCallback : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).click(function() {
          let notification_report_id = aData.notification_report_id;
          document.location.href = '{{url('claim/notification_report/profile/')}}/'+notification_report_id;
        }).hover(function() {
          $(this).css('cursor','pointer');
        }, function() {
          $(this).css('cursor','auto');
        });
      },
    });
  });
</script>


@endpush