<!-- Modal -->
<div id="updateFeedbackModal" class="modal fade" role="dialog" data-focus="true">
	<div class="modal-dialog modal-lg" style="max-width: 70%;">

		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="updateFeedbackForm">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modalTitle"></h5>
					{{-- <input class="form-control hidden" id="osh_audit_employer_id" name="osh_audit_employer_id" type="text" value="{{$osh_audit_employer->id}}"> --}}
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-5">
							<label for="question"><h6>Question</h6></label>
						</div>
						<div class="col-xs-2">
							<label for="answer"><h6>Answer</h6></label>
						</div>
						<div class="col-xs-5">
							<label for="comment"><h6>Comment</h6></label>
						</div>
						<div class="col-sm-12">
							<div class="alert alert-danger general_error hidden mt-1" role="alert"></div>
						</div>
					</div>

					<legend></legend>
					<br>

					<span id="updateFeedbackModalFields">

					</span>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" id="updateFeedbackFormSubmit">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>



<!-- Modal -->
<div id="updateGeneralCommentModal" class="modal fade" role="dialog" data-focus="false">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="updateGeneralCommentForm">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h6 id="modalTitle"></h4>
					<input class="form-control hidden" id="osh_audit_employer_id" name="osh_audit_employer_id" type="text" value="{{$osh_audit_employer->id}}">
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="alert alert-danger general_error hidden mt-1" role="alert"></div>
						</div>
						<div class="col-xs-5">
							<label for="general_comment">General Comments and Observation</label>
						</div>
						<div class="col-xs-7">
							<textarea class="form-control autosize" rows="1" style="border-radius: 3px; overflow: hidden; overflow-wrap: break-word; resize: none; height: 4em;" name="general_comment" cols="250" placeholder="write comments">{{$osh_audit_employer->general_comments}}</textarea>
							<strong><p class=" error_display text-danger hidden" id="error_general_comment"></p></strong>
						</div>

					</div>


					<div class="row mt-1">
						<div class="col-xs-5">
							<label for="workplace_name">Workplace Name</label>
						</div>
						<div class="col-xs-7">
							<input class="form-control" type="text" name="workplace_name" value="{{$osh_audit_employer->workplace_name}}">
							<strong><p class=" error_display text-danger hidden" id="error_workplace_name"></p></strong>
						</div>

					</div>

					<div class="row mt-1">
						<div class="col-xs-5">
							<label for="workplace_address">Workplace Address</label>
						</div>
						<div class="col-xs-7">
							<input class="form-control" type="text" name="workplace_address" value="{{$osh_audit_employer->workplace_address}}">
							<strong><p class=" text-danger hidden" id="error_workplace_address"></p></strong>
						</div>
					</div>

					<div class="row mt-1">
						<div class="col-xs-5">
							<label for="workplace_district">Workplace District</label>
						</div>
						<div class="col-xs-7">
							<select name="workplace_district" id="workplace_district" class="form-control " style="width: 100%;">
								@foreach($districts as $key => $value)
								@if($key == $osh_audit_employer->district_id)
								<option value="{{$key}}" selected="selected">{{$value}}</option>
								@else
								<option value="{{$key}}">{{$value}}</option>
								@endif
								@endforeach
							</select>
							<strong><p class=" text-danger hidden" id="error_workplace_district"></p></strong>
						</div>
					</div>

					<div class="row mt-1">
						<div class="col-xs-5">
							<label for="workplace_location">Workplace Location</label>
						</div>
						<div class="col-xs-7">
							<textarea class="form-control autosize" rows="1" style="border-radius: 3px; overflow: hidden; overflow-wrap: break-word; resize: none; height: 4em;" name="workplace_location" cols="250" placeholder="write comments">{{$osh_audit_employer->workplace_location}}</textarea>
							<strong><p class=" error_display text-danger hidden" id="error_workplace_location"></p></strong>
						</div>

					</div>

					<div class="row mt-1">
						<div class="col-xs-5">
							<label for="contact_person">Contact Person</label>
						</div>
						<div class="col-xs-7">
							<input class="form-control" type="text" name="contact_person" value="{{$osh_audit_employer->contact_person}}">
							<strong><p class="  text-danger hidden" id="error_contact_person"></p></strong>
						</div>
					</div>

					<div class="row mt-1">
						<div class="col-xs-5">
							<label for="phone">Contact Person phone</label>
						</div>
						<div class="col-xs-7">
							<input class="form-control" type="text" name="phone" value="{{$osh_audit_employer->contact_person_phone}}">
							<strong><p class="  text-danger hidden" id="error_phone"></p></strong>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" id="updateGeneralCommentFormSubmit">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>