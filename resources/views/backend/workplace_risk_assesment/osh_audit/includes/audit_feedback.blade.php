        <div class="nav_tab_pane_header">
          <div class="row">
            <div class="col-md-12" >

              @if( (access()->id() == $osh_audit_employer->user_id) && $osh_audit_employer->status == 2 && $is_period_active)
              <div class="pull-right" >
                <span>
                  <a href="{{url('workplace_risk_assesment/audit/update_feedback/'.$osh_audit_employer->id)}}" class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pencil-square-o"></i>&nbsp;Update Audit Feedback</a>
                </span>
              </div>
              @endif

            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-9">
           <div class="accordion" id="accordionExample">
            @foreach($code_values as $audit_code_value)
            <div class="card">
              <div class="card-header" id="headingOne{{$audit_code_value->id}}">
                <span data-toggle="collapse" data-target="#collapseOne{{$audit_code_value->id}}"> {{ucwords(strtolower($audit_code_value->name))}} <span class="pull-right"><i class="fa fa-plus"></i></span></span>
              </div>
              <div id="collapseOne{{$audit_code_value->id}}" class="collapse" aria-labelledby="headingOne{{$audit_code_value->id}}" data-parent="#accordionExample">
                <div class="card-body p-1">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Question</th>
                        <th scope="col">Response</th>
                        <th scope="col">Comments</th>
                      </tr>
                    </thead>
                    <tbody>
                      @for ($i=0; $i < count($osh_audit_feedbacks)-1; $i++)

                      @php
                      $question = $osh_audit_feedbacks[$i]->oshAuditQuestion;
                      @endphp

                      @if($audit_code_value->id == $question->code_value_id)
                      <tr>
                        <td>{{$question->question}}</td>
                        <td>{{$osh_audit_feedbacks[$i]->feedback}}</td>
                        <td>{{$osh_audit_feedbacks[$i]->comment}}</td>
                      </tr>
                      @endif

                      @endfor
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="col-md-3">
          @include("backend/workplace_risk_assesment/osh_audit/includes/feedback_summary")
        </div>
      </div>


      @push('after-script-end')


      <script  type="text/javascript">
        $(function() {
      // $('#audit_feedback-table').DataTable({});
      $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
      });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
      });
    </script>


    @endpush