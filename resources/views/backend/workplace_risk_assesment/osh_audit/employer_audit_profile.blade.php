       @extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_audit.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_audit.employer_profile')])

       @include('backend.includes.datatable_assets')

       @push('after-styles-end')
       {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
       {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
       {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
       {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
       <style>
        /* start: File Icons CSS */
        #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
        #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
        #folder-tree .file-pdf { background-position: -32px 0 }
        #folder-tree .file-as { background-position: -36px 0 }
        #folder-tree .file-c { background-position: -72px -0px }
        #folder-tree .file-iso { background-position: -108px -0px }
        #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
        #folder-tree .file-cf { background-position: -162px -0px }
        #folder-tree .file-cpp { background-position: -216px -0px }
        #folder-tree .file-cs { background-position: -236px -0px }
        #folder-tree .file-sql { background-position: -272px -0px }
        #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
        #folder-tree .file-h { background-position: -488px -0px }
        #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
        #folder-tree .file-php { background-position: -108px -18px }
        #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
        #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
        #folder-tree .file-rb { background-position: -180px -18px }
        #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
        #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
        #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
        #folder-tree .file-js { background-position: -434px -18px }
        #folder-tree .file-css { background-position: -144px -0px }
        #folder-tree .file-fla { background-position: -398px -0px }
        /* end: File Icon CSS */
        /* start: upload progress bar css */
        .progress-bar {
          background-color: #12CC1A;
          height:20px;
          color: #FFFFFF;
          width:0%;
          -webkit-transition: width .3s;
          -moz-transition: width .3s;
          transition: width .3s;
        }
        .progress-div {
          border:#0FA015 1px solid;
          padding: 5px 0px;
          margin:30px 0px;
          border-radius:4px;
          text-align:center;
        }
        /* end: upload progress bar css */
        tr {
          border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
      </style>
      @endpush

      @section('content')


      <div class = "row">
        <div class="col-md-12 col-sm-12">
          <h5 class="client-title">
            @if($osh_audit_employer->is_submitted)
            <i class="icon-circle"></i>
            <!-- Employer header detail -->
            @if($feedback_score['rating']['score'] == 'LOW')
            <span class="tag tag-danger danger_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="Low performer">.</span>
            @elseif($feedback_score['rating']['score']== 'MEDIUM')
            <span class="tag tag-warning warning_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="Medium performer">.</span>
            @else
            <span class="tag tag-success success_color_custom" data-toggle="tooltip" data-html="true" title="" data-original-title="High performer">.</span>
            @endif
            @endif
            <strong>{{$osh_audit_employer->name}}</strong>

            <small >
              Reg No#:  {{substr($osh_audit_employer->reg_no, -6)}}
            </small>

          </h5>
          <legend></legend>
        </div>


        <div>&nbsp;</div>
        {{--Tabs navigation--}}
        <div class = "row">
          <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
              <ul class="nav nav-tabs">
               <li class="nav-item ">
                <a class="nav-link active"  href="#general" data-toggle="tab">General</a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  id = "tab_2_header"  href="#audit_feedback" data-toggle="tab">Audit Feedback</a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  id = "tab_3_header"  href="#fatal" data-toggle="tab">Fatal</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id ="tab_4_header"  href="#days_lost" data-toggle="tab"> Days Lost</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id = "tab_5_header" href="#non_conveyance" data-toggle="tab"> Claims Excluding Conveyance Accident</a>
              </li>
            </ul>
            <div class="nav_tab_contain tab-content">
              <div id="general" class="nav_tab_pane tab-pane active in">

                <div class="row">
                  <div class="col-md-9">
                    <div class="row">
                      <div class="col-md-12">

                        <table class="table display table-bordered" cellspacing="0" width="100%" id ="workplace_audit_profile-table">
                          <thead>
                            <tr>
                              <th></th>
                              <th>Title</th>
                              <th>Maximum possible score</th>
                              <th>Actual  score</th>
                              <th>Percentage (Traffic light)</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php 
                            $overall_percent = 0; 
                            $overall_possible = 0;
                            $overall_actual = 0;
                            @endphp
                            @for($i=0; $i < count($code_values); $i++) 
                            @if($code_values[$i]->id != 468)
                            @php 
                            $overall_actual += $feedback_score[$code_values[$i]->id]['actual_score']; 
                            $overall_possible += $feedback_score[$code_values[$i]->id]['total_possible']; 
                            $overall_percent += $feedback_score[$code_values[$i]->id]['percent']; 
                            @endphp
                            <tr>
                              <td>{{$i+1}}</td>
                              <td>{{$code_values[$i]->name}}</td>
                              @if($osh_audit_employer->is_submitted)
                              <td>{{$feedback_score[$code_values[$i]->id]['total_possible']}}</td>
                              <td>{{$feedback_score[$code_values[$i]->id]['actual_score']}}</td>
                              <td>{{$feedback_score[$code_values[$i]->id]['percent']}}</td>
                              @else
                              <td></td>
                              <td></td>
                              <td></td>
                              @endif
                            </tr>
                            @endif

                            @endfor
                            <tr>
                              <td colspan="2" class="pl-3"><p class="h5">Overall</p></td>
                              @if($osh_audit_employer->is_submitted)
                              <td><h6>{{$feedback_score['overall']['total_possible']}}</h6></td>
                              <td><h6>{{$feedback_score['overall']['actual_score']}}</h6></td>
                              <td><h6>{{$feedback_score['overall']['percent']}}</h6></td>
                              @else
                              <td></td>
                              <td></td>
                              <td></td>
                              @endif
                            </tr>
                          </tbody>
                        </table>

                      </div>

                    </div> 
                  </div>
                  <div class="col-md-3">
                    {{-- @include("backend/workplace_risk_assesment/osh_audit/includes/employer_summary") --}}
                    @include("backend/workplace_risk_assesment/osh_audit/includes/feedback_summary")
                  </div>
                </div>
              </div>
              <div id="audit_feedback" class="nav_tab_pane tab-pane">
                @include("backend/workplace_risk_assesment/osh_audit/includes/audit_feedback")
              </div>
              <div id="fatal" class="nav_tab_pane tab-pane">
                @include("backend/workplace_risk_assesment/osh_audit/includes/fatal_cases")
              </div>
              <div id="days_lost" class="nav_tab_pane tab-pane">
                @include("backend/workplace_risk_assesment/osh_audit/includes/days_lost")
              </div>
              <div id="non_conveyance" class="nav_tab_pane tab-pane">
                @include("backend/workplace_risk_assesment/osh_audit/includes/non_conveyance")

              </div>

            </div>
          </div>
        </div>

      </div>

    </div>
    @stop


    @push('after-script-end')

    <script  type="text/javascript">

      $(function () {

        if (location.hash !== '') {
          $('a[href="' + location.hash + '"]').tab('show');
          $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          var tab = $(e.target).attr('href').substr(1);
          if (history.pushState) {
            history.pushState(null, null, '#' + tab);
          } else {
            location.hash = '#' + tab;
          }
        });

    // $('#workplace_audit_profile-table').DataTable();

    // $('#audit_feedback-table').DataTable();
    // $('#claims-table').DataTable();
    // $('#days_lost-table').DataTable();
    // $('#fatal_cases-table').DataTable();


  });

</script>;

@endpush

