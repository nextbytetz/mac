@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_data.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_data_management.title').' - '.trans('labels.backend.member.employee.header.index')])
 {{-- @lang('labels.backend.workplace_risk_assesment.osh_data_management.title') --}}

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')
<p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.workplace_risk_assesment.osh_data_management.title')</p>
    {{--Start : Custom filter--}}
    <div class="custom_filter">
        {!! Form::open(['role' => 'form', 'id' => 'search-recall-notification-form']) !!}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form-row">
                    {{--Incident Type--}}
                    <div class="form-group col-md-3">
                        <label for="incident">Incident Type</label>
                        {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident']) !!}
                    </div>
                    {{--Employee Name--}}
                    <div class="form-group col-md-4">
                        <label for="employee">Employee Name</label>
                        {!! Form::select('employee', [], null, ['class' => 'form-control employee-select', 'id' => 'employee']) !!}
                    </div>
                    {{--Employer Name--}}
                    <div class="form-group col-md-4">
                        <label for="employer">Employer Name</label>
                        {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                    </div>
                    {{--Region Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Incident Region:</label>
                        {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    {{--Gender Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Gender:</label>
                        {!! Form::select('gender', $genders, null, ['class' => 'form-control search-select', 'id' => 'gender', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    {{--Category Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Employee Category:</label>
                        {!! Form::select('employment_category', $employment_categories, null, ['class' => 'form-control search-select', 'id' => 'employment_category', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                     {{--Employer Category Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Employer Category:</label>
                        {!! Form::select('employer_category', $employer_categories, null, ['class' => 'form-control search-select', 'id' => 'employer_category', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                     {{--Bussines sector Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Bussines Sector:</label>
                        {!! Form::select('bussines_sector', $bussines_sectors, null, ['class' => 'form-control search-select', 'id' => 'bussines_sector', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    {{--Bussines sector Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Occupation:</label>
                        {!! Form::select('occupation', $occupations, null, ['class' => 'form-control search-select', 'id' => 'occupation', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    {{-- workplaces --}}
                    <div class="form-group col-md-3">
                        <label for="workplaces">Workplace(s):</label>
                        {!! Form::select('workplaces', $workplaces, null, ['class' => 'form-control search-select', 'id' => 'workplaces', 'placeholder' => '']) !!}
                    </div>
                    {{--District Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Incident District:</label>
                        {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                    </div>
                    {{-- search by age from to --}}
                      <div class="form-group col-md-3">
                        <label for="age">Start age:</label>
                        {!! Form::number('start_age', null, ['class' => 'form-control', 'id' => 'start-age', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="age">End age:</label>
                        {!! Form::number('end_age', null, ['class' => 'form-control', 'id' => 'end-age', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>


                </div>
                <input type="hidden" name="export" id="export_0" value="{{0}}">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                        <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="">
    </div>
    {{--End : Custom filter--}}
    <legend></legend>
    <br/>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="display" cellspacing="0" width="100%" id ="notification-report-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>@lang('labels.backend.legal.case_no')</th>
                    <th>@lang('labels.general.gender')</th>
                    <th>@lang('labels.backend.claim.incident_type')</th>
                    <th>@lang('labels.backend.compliance.employee')</th>
                    <th>@lang('labels.backend.compliance.employer')</th>
                    <th>@lang('labels.backend.table.incident_date')</th>
                    <th>@lang('labels.backend.claim.receipt_date')</th>
                    <th>@lang('labels.general.region')</th>
                    <th>@lang('labels.backend.member.employee_category')</th>
                    <th>@lang('labels.backend.member.employer_category')</th>
                    <th>@lang('labels.general.sector')</th>
                    <th>@lang('labels.general.occupation')</th>
                    <th>@lang('labels.general.age')</th>
                    <th>@lang('labels.general.workplaces')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@stop

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(document).ready(function() {
        var url = "{!! url("/") !!}";

        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });

        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district').empty();
                    $("#district").select2("val", "");
                    $('#district').html(data);
                    $("#spin2").hide();
                });
            }
        });

        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 1,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.workplace_risk_assesment.data_management.employee_datatable') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employee */
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $('#start-age').val(null);
            $('#end-age').val(null);
            $(".search-select").val(null).trigger('change.select2');
        });

        var $oTable = $('#notification-report-table').DataTable({
            dom : 'Bfrtip',
            buttons : ['excel','csv','reset', 'reload','colvis'],
            initComplete : function () {
                $oTable.buttons().container().insertBefore('#notification-report-table');
                $("#notification-report-table").css("width","100%");
            },
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.workplace_risk_assesment.data_management.employee_datatable') !!}',
                type : 'get',
                data: function ($d) {
                    $d.incident = $('select[name=incident]').val();
                    $d.employee = $('select[name=employee]').val();
                    $d.workplaces = $('select[name=workplaces]').val();
                    $d.employer_category = $('select[name=employer_category]').val();
                    $d.bussines_sector = $('select[name=bussines_sector]').val();
                    $d.employer = $('select[name=employer]').val();
                    $d.region = $('select[name=region]').val();
                    $d.district = $('select[name=district]').val();
                    $d.gender = $('select[name=gender]').val();
                    $d.occupation = $('select[name=occupation]').val();
                    $d.export = $('#export_0').val();
                    $d.start_age = $('#start-age').val();
                    $d.end_age = $('#end-age').val();
                    $d.employment_category = $('select[name=employment_category]').val();
                }
            },
            columns: [
                // { data: 'employee_id' , name: 'employee_id' ,orderable : true, searchable : false, visible : true},
                { data: 'employee_id' , name: 'employee_id', orderable : false, searchable :  false, visible : true},
                { data: 'filename' , name: 'filename' ,orderable : true, searchable : true},
                { data: 'gender' , name: 'gender' ,orderable : true, searchable : false},
                { data: 'incident_type' , name: 'incident_types.name'},
                { data: 'fullname' , name: 'fullname', orderable : false, searchable : false},
                { data: 'employer' , name: 'employers.name', orderable : true, searchable : true},
                { data: 'incident_date' , name: 'notification_reports.incident_date', orderable : true, searchable : true},
                { data: 'receipt_date' , name: 'notification_reports.receipt_date', orderable : true, searchable : false},
                { data: 'region' , name: 'regions.name',searchable : false},
                { data: 'employment_category' , name: 'employment_category',searchable : false},
                { data: 'employer_category' , name: 'employer_category',searchable : false},
                { data: 'bussines_sector' , name: 'bussines_sector',searchable : false},
                { data: 'occupation' , name: 'occupation',searchable : false},
                { data: 'age' , name: 'age',searchable : false},
                { data: 'workplace_no' , name: 'workplace_no',searchable : false},
                { data: 'firstname' , name: 'employees.firstname', orderable : false, searchable : true, visible : false},
                { data: 'middlename' , name: 'employees.middlename', orderable : false, searchable : true, visible : false},
                { data: 'lastname' , name: 'employees.lastname', orderable : false, searchable : true, visible : false}
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/workplace_risk_assesment/data_management/employee/"  + aData['employee_id'] + "/" + aData['case_no'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
        $('#search-recall-notification-form').on('submit', function($e) {
            $oTable.draw();
            $e.preventDefault();
        });

        
    });
</script>

@endpush