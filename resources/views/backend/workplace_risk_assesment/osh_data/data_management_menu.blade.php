@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.menu_header'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_data.menu_header_title')])

@section('content')

<div style="color:#fff">

 <div class="row">
    <div class="col-sm-6 col-md-6">
        <div class="list-group">
            {{--item 1--}}
            <ul class="list-unstyled">
                <a href="{!! route('backend.workplace_risk_assesment.data_management.employee_demographic') !!}">

                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-database"> </i><large>&nbsp;&nbsp;@lang('labels.backend.workplace_risk_assesment.osh_data.employee_demographic')
                    </large></h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.workplace_risk_assesment.osh_data.description')</p> </li>
                </a>

            </ul>
        </div>
    </div>


    {{--right div--}}
    <div class="col-sm-6 col-md-6">
        <div class="list-group">

            {{--Module for Audit checklist --}}
            <ul class="list-unstyled">
                <a href="{!! route('backend.workplace_risk_assesment.data_management.employer_particulars') !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;@lang('labels.backend.workplace_risk_assesment.osh_data.employer_particulars')
                    </large></h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.workplace_risk_assesment.osh_data.description')</p> </li>
                </a>
            </ul>



        </div>


    </div>
</div>

<div class="row">
    <div class="col-sm-12">&nbsp;</div>
</div>
<div class="row">

    <div class="col-sm-6 col-md-6">
        <div class="list-group">
            {{--item 1--}}
            <ul class="list-unstyled">
                <a href="{!! route('backend.workplace_risk_assesment.data_management.osh_claims') !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-wheelchair header-icon" aria-hidden="true"> </i><large>&nbsp;&nbsp;@lang('labels.backend.workplace_risk_assesment.osh_data.claims')
                    </large></h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.workplace_risk_assesment.osh_data.description')</p> </li>
                </a>

            </ul>
        </div>
    </div>

 <div class="col-sm-6 col-md-6">
        <div class="list-group">
            
        </div>
    </div>
           
        </div>
    </div>

</div>

</div>


@stop

@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {
            /*
             $("#site-header-title").hide();
             */
             $("#preview").click(function() {

             });
         });
     </script>;

     @endpush
