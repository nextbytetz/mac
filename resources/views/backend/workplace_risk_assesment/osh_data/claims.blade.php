@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_data.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_data_management.title').' - '.trans('labels.general.claims')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')
{!! Form::open(['role' => 'form', 'id' => 'search-recall-notification-form','route' => ['backend.workplace_risk_assesment.data_management.osh_claims'],'method'=>'get','name'=>'claims-form']) !!}
 @include('backend.workplace_risk_assesment.osh_data.includes.date_range')

    {{--Start : Custom filter--}}
    <div class="custom_filter">
    {{-- {{dd($data)}} --}}
        
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form">
                    {{--Incident Type--}}
                    <div class="row">
                    <div class="form-group col-md-3">
                        <label for="incident">Incident Type</label>
                        {!! Form::select('incident', ['1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident']) !!}
                    </div>
                    {{--Employee Name--}}
                    <div class="form-group col-md-3">
                        <label for="employee">Employee Name</label>
                        {!! Form::select('employee', [], null, ['class' => 'form-control employee-select', 'id' => 'employee']) !!}
                    </div>
                    {{--Employer Name--}}
                    <div class="form-group col-md-3">
                        <label for="employer">Employer Name</label>
                        {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label for="status">Status</label>
                        {!! Form::select('status', ['-1' => 'All','0' => 'Pending', '1' => 'Approved', '2' => 'Rejected', '3' => 'On progress'], null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                    </div>
                </div>
                    {{--Region Selection--}}
                     <div class="row">
                    <div class="form-group col-md-3">
                        <label for="region">Incident Region:</label>
                        {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="region">Incident District:</label>
                        {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                    </div>
                    {{--Gender Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Gender:</label>
                        {!! Form::select('gender', $genders, null, ['class' => 'form-control search-select', 'id' => 'gender', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    {{-- benefit types --}}
                    <div class="form-group col-md-3">
                        <label for="region">Benefit types:</label>
                        {!! Form::select('benefit_type', $benefit_types, null, ['class' => 'form-control search-select', 'id' => 'benefit_type', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                </div>

                    {{-- if incident type is accident --}}

                    {{--Location Selection--}}
                    <div class="Accident row">   
                    <div class="form-group col-md-3 accident">
                        <label for="region"> Location of accident:</label>
                        {!! Form::select('location_accident', [], null, ['class' => 'form-control search-select', 'id' => 'location_accident', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                     {{--Accident type Selection--}}
                    <div class="form-group col-md-3 accident">
                        <label for="region"> Accident Cause Type:</label>
                        {!! Form::select('accident_cause_type', $accident_types, null, ['class' => 'form-control search-select', 'id' => 'accident_cause', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin3"></i>
                    </div>
                    <div class="form-group col-md-3 accident">
                        <label for="region">Accident Cause Agency:</label>
                        {!! Form::select('accident_cause', $accident_causes, null, ['class' => 'form-control search-select', 'id' => 'accident-causes', 'placeholder' => '']) !!}
                    </div>
                </div>
                    {{--Accident type Selection--}}
                    <div class="row Disease">
                    <div class="form-group col-md-3 disease">
                        <label for="region">Occupational disease Type:</label><br>
                        {!! Form::select('occupational_disease_type', $disease_types, null, ['class' => 'form-control search-select', 'id' => 'occupational_disease_type', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;"></i>
                    </div>
                    <div class="form-group col-md-3 disease">
                        <label for="region">Disease agent type:</label><br>
                        {!! Form::select('disease_agent_type', $disease_agents, null, ['class' => 'form-control search-select', 'id' => 'disease_agent_type', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin4"></i>
                    </div>
                    <div class="form-group col-md-3 disease">
                        <label for="region">disease agents:</label><br>
                        {!! Form::select('disease_agent', [], null, ['class' => 'form-control search-select', 'id' => 'disease_agent', 'placeholder' => '']) !!}
                    </div>
                    <div class="form-group col-md-3 disease">
                        <label for="disease_organ_type">Target organ type:</label><br>
                        {!! Form::select('disease_organ_type', $target_organs, null, ['class' => 'form-control search-select', 'id' => 'disease_organ_type', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin5"></i>
                    </div>
                </div>
                <div class="Disease row">

                    <div class="form-group col-md-4 disease">
                        <label for="disease_organ">Target organs:</label><br>
                        {!! Form::select('disease_organ', [], null, ['class' => 'form-control search-select', 'id' => 'disease_organ', 'placeholder' => '']) !!}
                    </div>
                </div>


                {{-- </div> --}}
                <input type="hidden" name="export" id="export_0" value="{{0}}">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                        <input type="submit" class="btn btn-success btn-sm btn-submit" id="btn-sbmt" value="@lang('buttons.general.search')" />
                        {{-- <button class="btn btn-success btn-sm" id="btn-sbmt">@lang('buttons.general.search')</button> --}}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="">
    </div>
    {{--End : Custom filter--}}
    <legend></legend>
    <br/>
    <div class="row">
    <div class="table table-responsive col-md-12 col-sm-12">
    {!! $dataTable->table(['class' => 'display ', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
</div>
</div>

@stop

@push('after-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {!! $dataTable->scripts() !!} 

<script  type="text/javascript">
    $(document).ready(function() {
        var url = "{!! url("/") !!}";
        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });

        if ($('#incident option:selected').val() == 1) {
             $('.Disease').hide();
             $("#occupational_disease_type").val('');
             $("#occupational_disease").val('');
             $("#disease_organ_type").val('');
             $("#disease_agent_type").val('');
             $("#disease_agent").val('');
             $("#disease_organ").val('');
             $("#benefit_type").val('');
             $('.Accident').show();
        }else if($('#incident option:selected').val() == 2){
             $('.Disease').show();
             $("#location_accident").val('');
             $("#accident_cause_type").val('');
             $("#accident_cause").val('');
             $("#benefit_type").val('');
             $('.Accident').hide();
             
        }else if($('#incident option:selected').val() == 3){
            $('.Disease').hide();
            $('.Accident').hide();
            $("#benefit_type").val('');
            $("#occupational_disease_type").val('');
             $("#occupational_disease").val('');
             $("#disease_organ_type").val('');
             $("#disease_agent_type").val('');
             $("#disease_agent").val('');
             $("#disease_organ").val('');
             $("#location_accident").val('');
             $("#accident_cause_type").val('');
             $("#accident_cause").val('');
        }

        $('#incident').change(function(e){
        if ($('#incident option:selected').val() == 1) {
             $('.Disease').hide();
             $("#benefit_type").val('');
            $("#occupational_disease_type").val('');
             $("#occupational_disease").val('');
             $("#disease_organ_type").val('');
             $("#disease_agent_type").val('');
             $("#disease_agent").val('');
             $("#disease_organ").val('');
             $('.Accident').show();
        }else if($('#incident option:selected').val() == 2){
             $('.Disease').show();
             $("#benefit_type").val('');
              $("#location_accident").val('');
             $("#accident_cause_type").val('');
             $("#accident_cause").val('');
             $('.Accident').hide();

        }else if($('#incident option:selected').val() == 3){
            $('.Disease').hide();
            $('.Accident').hide();
            $("#benefit_type").val('');
            $("#occupational_disease_type").val('');
             $("#occupational_disease").val('');
             $("#disease_organ_type").val('');
             $("#disease_agent_type").val('');
             $("#disease_agent").val('');
             $("#disease_organ").val('');
             $("#location_accident").val('');
             $("#accident_cause_type").val('');
             $("#accident_cause").val('');
        }
        });

        

        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district').empty();
                    $("#district").select2("val", "");
                    $('#district').html(data);
                    $("#spin2").hide();
                });
            }
        });
        // claim/notification_report/return_accident_cause
        $('#accident_cause').on('change', function (e) {
            var accident = e.target.value;
            if (accident) {
                $("#spin2").show();
                $.get("{{ url('/') }}/workplace_risk_assesment/data_management/get_accidents/" + accident, function (data) {
                    $('#accident-causes').empty();
                    $("#accident-causes").select2("val", "");
                    $('#accident-causes').html(data);
                    $("#spin2").hide();
                });
            }
        });

        $('#disease_agent_type').on('change', function (e) {
            var disease = e.target.value;
            if (disease) {
                $("#spin4").show();
                $.get("{{ url('/') }}/workplace_risk_assesment/data_management/get_disease_agents/" + disease, function (data) {
                    $('#disease_agent').empty();
                    $("#disease_agent").select2("val", "");
                    $('#disease_agent').html(data);
                    $("#spin4").hide();
                });
            }
        });

        $('#disease_organ_type').on('change', function (e) {
            var organ = e.target.value;
            if (organ) {
                $("#spin5").show();
                $.get("{{ url('/') }}/workplace_risk_assesment/data_management/get_disease_organs/" + organ, function (data) {
                    $('#disease_organ').empty();
                    $("#disease_organ").select2("val", "");
                    $('#disease_organ').html(data);
                    $("#spin5").hide();
                });
            }
        });
        

        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 1,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employees') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employee */
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $('#start-age').val(null);
            $('#end-age').val(null);
            $(".search-select").val(null).trigger('change.select2');
        });


    });


</script>

@endpush