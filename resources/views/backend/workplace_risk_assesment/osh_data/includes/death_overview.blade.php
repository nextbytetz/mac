
{{--interest-writeoff-overview-table--}}

<legend class="grey_modal" >@lang('labels.backend.legend.death_overview')</legend>
<table class="display" cellspacing="0" width="100%" id ="death-overview-table">
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.backend.table.case_no')</th>
        <th>@lang('labels.backend.table.death_date')</th>
        <th>@lang('labels.backend.table.death_cause')</th>
        <th>@lang('labels.backend.table.report_date')</th>
    </tr>
    </thead>

</table>

@push('death-overview-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#death-overview-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employee.get_death_notification', $employee->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'status', name: 'status' },
                { data: 'odtno' , name: 'odtno' },
                { data: 'death_date' , name: 'death_date' },
                { data: 'death_cause_id' , name: 'death_cause_id' },
                { data: 'reporting_date' , name: 'reporting_date' }

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/claim/notification_report/profile/"  + aData['notification_report_id'];

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }


        } );
    });
</script>;

@endpush