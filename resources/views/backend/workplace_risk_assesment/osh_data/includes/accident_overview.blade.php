
{{--interest-writeoff-overview-table--}}

<legend class="grey_modal" >@lang('labels.backend.legend.accident_overview')</legend>
<table class="display" cellspacing="0" width="100%" id ="accident-overview-table">
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.backend.table.case_no')</th>
        <th>@lang('labels.backend.table.accident_date')</th>
        <th>@lang('labels.backend.table.description')</th>
        <th>@lang('labels.backend.table.accident_type')</th>
    </tr>
    </thead>

</table>

@push('accident-overview-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#accident-overview-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employee.get_accident_notifications', $employee->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'status', name: 'status' },
                { data: 'oacno' , name: 'oacno' },
                { data: 'accident_date' , name: 'accident_date' },
                { data: 'description' , name: 'description' },
                { data: 'accident_type_id' , name: 'accident_type_id' }

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/claim/notification_report/profile/"  + aData['notification_report_id'];

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }


        } );
    });
</script>;

@endpush