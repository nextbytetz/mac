
{{--incidents-overview-table--}}
<div>
<legend class="grey_modal" >Incidents notifications overview</legend>
{!! $dataTable->table(['class' => 'table display table-responsive', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
</div>

@push('after-script-end')
   {!! $dataTable->scripts() !!}           

@endpush