

{{--sidebar employeer summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">
<div class="grey_modal">
    @if($employee_photo!==null)
<table>
           {{--  <tr>
     <td  align="center"><h5><b><span class="light_dark_color">Photo</span></b></h5></td>
            </tr> --}}
            <tr>
                <td>
              <?php echo '<img height=400 width=50 src="data:image/jpeg;base64,'.$employee_photo.'">';?>
            </td></tr>
        </table>

    </div>
    <div>&nbsp;</div>
    <div class="grey_modal">
     @endif
        <table style="width:100%">
            <tr>
     <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>
            
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table>
            {{--name--}}
            <tr>
                <td style="padding-left: 5px" width="105px">@lang('labels.general.name'):</td>
                <td  height="20px"><b>{!! Form::label( 'name',$employee->firstname . " " . $employee->middlename  . " " . $employee->lastname, [ 'id'=> 'name']) !!}</b></td>

            </tr>

            {{--memberno--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.memberno'):</td>
                <td><b>{!! Form::label( 'memberno', $employee->memberno, [ 'id'=> 'memberno']) !!}</b></td>

            </tr>
             <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.nid'):</td>
                <td><b>{!! Form::label( 'national_id', $employee->nid, [ 'id'=> 'nid']) !!}</b></td>

            </tr>

            {{--dob--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.dob'):</td>
                <td ><b>{!! Form::label( 'dob', $employee->dob_formatted, [ 'id'=>
                'dob'])
                !!}</b></td>

            </tr>

{{--employer--}}
            @foreach($employee->employers as $employer)
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.receipt.employer'):</td>
                <td ><b>{!! Form::label( 'employer',$employer->name_formatted, [ 'id'=>
                'dob'])
                !!}</b></td>

            </tr>
            @endforeach

            {{--jOB tITLE--}}
            {{-- <tr>
                <td style="padding-left: 5px">@lang('labels.general.occupation'):</td>
                <td><b>{!! Form::label( 'occupation', ($employee->jobTitle()->count()) ? $employee->jobTitle->name :(($employee->jobTitle()->count()) ? $employee->jobTitle->name : ' '), [ 'id'=> 'occupation']) !!}</b></td>

            </tr> --}}

{{--Department--}}
            <tr>
                    <td style="padding-left: 5px">@lang('labels.general.department'):</td>
                    <td><b>{!! Form::label( 'department',!is_null($employee->department) ? $employee->department : ' ', [ 'id'=> 'department'])
                        !!}</b></td>
                </tr>

            {{--gender--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.gender'):</td>
                <td><b>{!! Form::label( 'gender', ($employee->gender()->count()) ? $employee->gender->name : ' ', [ 'id'=> 'gender'])
                        !!}</b></td>
            </tr>


            {{--marital status--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.marital_status'):</td>
                <td><b>{!! Form::label( 'marital_status', ($employee->maritalStatus()->count()) ? $employee->maritalStatus->name : ' ', [ 'id'=> 'marital_status'])
                        !!}</b></td>
            </tr>


                        {{--phone--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.phone'):</td>
                <td><b>{!! Form::label( 'phone', !is_null($employee->phone) ? $employee->phone : ' ', [ 'id'=> 'phone'])
                        !!}</b></td>

            </tr>

            {{--email--}}
            <tr style="word-wrap: break-word; word-break: break-all;">
                <td style="padding-left: 5px">@lang('labels.general.email'):</td>
                <td><b>{!! Form::label( 'email', !is_null($employee->email) ? $employee->email : ' ', [ 'id'=> 'email'])
                        !!}</b></td>

            </tr>


            {{--employee category--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.employee_category'):</td>
                <td><b>{!! Form::label( 'employee_category', $employee->emp_cate, [])
                        !!}</b></td>
            </tr>

            {{--occupation--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.workplace_risk_assesment.osh_data.occupation'):</td>
                <td><b>{!! Form::label( '', $employee->jobTitle->name, [])
                        !!}</b></td>
            </tr>

            {{--working experince--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.workplace_risk_assesment.osh_data.experience'):</td>
                <td><b>{!! Form::label( '', $experience, [])
                        !!}</b></td>
            </tr>

            {{--workplace incident occured--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.workplaces'):</td>
                <td><b>{!! Form::label( '', $workplace, [])
                        !!}</b></td>
            </tr>


        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush