
{{--workplace table--}}

<div class = "row">
    <div class="col-md-12" >

        <table class="display table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>SN</th>
                <th>Region</th>
                <th>District</th>
                 <th>Ward</th>
                <th>Workplace</th>
            </tr>
            </thead>
            <tbody>
                @php
                 $ct = 1;
                 @endphp
                 @if($workplaces != null)
                  @foreach($workplaces as $w)
                  <tr>
                  <td>{!! $ct++ !!}</td>
                  <td>{!! $w->region !!}</td>
                  <td>{!! $w->district !!}</td>
                  <td>{!! $w->ward !!}</td>
                  <td>{!! $w->workplace_regno.'-'.$w->workplace_name!!}</td>
                  </tr>
                 @endforeach
                 @else
                  <tr>
                  <td colspan="5" align="center"><b>No workplace registered!</b></td>
                  </tr>
                 @endif
            </tbody>
        </table>

    </div>
</div>