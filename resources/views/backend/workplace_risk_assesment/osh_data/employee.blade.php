@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance.employee'), 'header_title' => trans('labels.backend.compliance.employee')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')


    <div class = "row">
        {{--{!! Form::model($employee, ['route' => ['backend.compliance.employee.profile', $employee->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " . $employee->lastname , [ 'id'=> 'name']) !!}</strong>

                <small>
                    @lang('labels.backend.table.member_#'): {!! Form::label( 'reg_no', $employee->memberno, [ 'id'=> 'reg_no']) !!}
                </small>
            </h5>
        </div>
    </div>
        {{--Tabs navigation--}}
        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        {{--General--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#general"
                               data-toggle="tab">@lang('labels.general.general')
                            </a>
                        </li>

                    </ul>
                    <div class="nav_tab_contain tab-content">

                        <div id="general" class="nav_tab_pane tab-pane active in">
                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                      <div class="pull-right">
                                      </div>
                                    </div>
                                </div>
                            </div>


                            {{--main tab content--}}
                            <div class = "row">
                                <div class="col-md-12">

                                    <div class="col-md-9">
                                        {{--accident report overview--}}
                                        @include('backend.workplace_risk_assesment.osh_data.includes.accident_overview')
                                        {{--disease adjust overview--}}
                                        <div>&nbsp;</div>
                                        @include('backend.workplace_risk_assesment.osh_data.includes.disease_overview')
                                        <div>&nbsp;</div>
                                        @include('backend.workplace_risk_assesment.osh_data.includes.death_overview')
                                    </div>
                                    
                                    <div class="col-md-3">
                                        {{--sidebar summary--}}
                                        @include('backend.workplace_risk_assesment.osh_data.includes.side_bar.employee_summary')
                                    </div>
                                </div>
                            </div>
                        </div>

                        @permission("view_employee_salary")
                        <div id="contribution" class="nav_tab_pane tab-pane">
                            {{--contributions--}}
                            @include('backend.operation.compliance.member.employee.includes.contribution')
                        </div>
                        @endauth

                        <div id="history" class="nav_tab_pane tab-pane">

                            {{--add employement history--}}


                            {{--employment history--}}
                            <div class="col-md-12" >
                                <div class="pull-right">
                                    <a href="{!! route('backend.compliance.employee.create_employment_history', $employee->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
                                </div>
                            </div>
                            @include('backend.operation.compliance.member.employee.includes.employment_history')
                        </div>
                        <div id="compensation" class="nav_tab_pane tab-pane">
                            {{--old compensations--}}
                            @include('backend.operation.compliance.member.employee.includes.compensation_history')
                        </div>
                        <div id="dependents" class="nav_tab_pane tab-pane">
                            {{--dependents--}}
                            @include('backend.operation.compliance.member.employee.includes.dependents')
                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')

@stack('contribution-script-end')
@stack('accident-overview-script-end')
@stack('disease-overview-script-end')
@stack('death-overview-script-end')
@stack('employment-history-script-end')
@stack('compensation-history-script-end')
@stack('dependents-script-end')
<script  type="text/javascript">


$(function () {


    if (location.hash !== '') {
        $('a[href="' + location.hash + '"]').tab('show');
        $('a[href="' + location.hash + '"]').trigger('click');
    }


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var tab = $(e.target).attr('href').substr(1);
        if (history.pushState) {
            history.pushState(null, null, '#' + tab);
        } else {
            location.hash = '#' + tab;
        }
    });
});
</script>;

@endpush
