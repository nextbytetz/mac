@extends('layouts.backend.main', ['title' => trans('labels.backend.workplace_risk_assesment.osh_data.title'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_data_management.title').' - '.trans('labels.backend.member.employer.header.index')])

@include('backend.includes.datatable_assets')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@section('content')

    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
    </div>
    <br/>
        {{--Start : Custom filter--}}
    <div class="custom_filter">
        {!! Form::open(['role' => 'form', 'id' => 'search-recall-notification-form']) !!}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form-row">
                    {{--Employer Name--}}
                    <div class="form-group col-md-4">
                        <label for="employer">Employer Name</label>
                        {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                    </div>
                    {{--Region Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Employer Region:</label>
                        {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    {{--District Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Employer District:</label>
                        {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                    </div>
                     {{--Employer Category Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Employer Category:</label>
                        {!! Form::select('employer_category', $employer_categories, null, ['class' => 'form-control search-select', 'id' => 'employer_category', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                     {{--Bussines sector Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Business Sector:</label>
                        {!! Form::select('bussines_sector', $bussines_sectors, null, ['class' => 'form-control search-select', 'id' => 'bussines_sector', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    
                </div>
                <input type="hidden" name="export" id="export_0" value="{{0}}">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                        <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    <div class = "row">
        <div class="col-md-12" >

            <div>&nbsp;</div>

            <table class="display" cellspacing="0" width="100%" id ="employers-table">
                <thead>
                <tr >
                    <th>@lang('labels.backend.table.name')</th>
                    <th>@lang('labels.backend.table.employer.reg_no')</th>
                    <th>@lang('labels.backend.table.employer.date_commenced')</th>
                    <th>@lang('labels.backend.member.employer_category')</th>
                    <th>@lang('labels.general.sector')</th>
                    <th>@lang('labels.backend.table.region')</th>
                    <th>@lang('labels.general.count_incidents')</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $(function() {
        var url = "{{ url('/')}}";

        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });

        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district').empty();
                    $("#district").select2("val", "");
                    $('#district').html(data);
                    $("#spin2").hide();
                });
            }
        });

        $('#incident-region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#incident-district').empty();
                    $("#incident-district").select2("val", "");
                    $('#incident-district').html(data);
                    $("#spin2").hide();
                });
            }
        });

        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 1,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.workplace_risk_assesment.data_management.employee_datatable') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employee */
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $('#start-age').val(null);
            $('#end-age').val(null);
            $(".search-select").val(null).trigger('change.select2');
        });
        var $table = $('#employers-table').DataTable({
            dom : 'Bfrtip',
            buttons : ['excel','reset', 'reload','colvis'],
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.workplace_risk_assesment.data_management.employer_data_datatable') !!}',
                type : 'get',
                data: function ($d) {
                    $d.incident = $('select[name=incident]').val();
                    $d.employee = $('select[name=employee]').val();
                    $d.employer_category = $('select[name=employer_category]').val();
                    $d.bussines_sector = $('select[name=bussines_sector]').val();
                    $d.employer = $('select[name=employer]').val();
                    $d.region = $('select[name=region]').val();
                    $d.district = $('select[name=district]').val();
                    $d.incident_region = $('select[name=incident_region]').val();
                    $d.incident_district = $('select[name=incident_district]').val();
                    $d.gender = $('select[name=gender]').val();
                    $d.occupation = $('select[name=occupation]').val();
                    $d.export = $('#export_0').val();
                    $d.start_age = $('#start-age').val();
                    $d.end_age = $('#end-age').val();
                    $d.employment_category = $('select[name=employment_category]').val();
                }

            },
            columns: [
                { data: 'name' , name: 'name',orderable : true, searchable : true},
                { data: 'reg_no', name: 'reg_no',orderable : true, searchable : true},
                { data: 'doc', name: 'doc',orderable : true, searchable : true},
                { data: 'employer_category', name: 'employer_category',orderable : true, searchable : false},
                { data: 'bussines_sector', name: 'bussines_sector',orderable : true, searchable : false },
                { data: 'region', name: 'region',orderable : true, searchable : false },
                { data: 'counts', name: 'counts',orderable : true, searchable : false }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = "employer/"+ aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });
         $('#search-recall-notification-form').on('submit', function($e) {
            $table.draw();
            $e.preventDefault();
        });

    });

</script>;

@endpush
