@extends('layouts.backend.main', ['title' => trans('labels.general.reports'), 'header_title' => trans('labels.backend.workplace_risk_assesment.osh_reports.welcome')])

{{-- @include('backend.includes.datatable_assets') --}}
@section('content')

{{--  <div id="stocks-div">
@linechart('MyStocks', 'stocks-div')
</div> --}}

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .claim_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.workplace_risk_assesment.osh_reports.osh_claims_summary')";
         font-size: 18px;
        font-weight: bold; 
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .claim_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.osh_report.title')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_employer_category_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.workplace_risk_assesment.osh_reports.osh_audit_dashboard')";
         font-size: 18px;
        font-weight: bold; 
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_employer_category_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_employer_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.workplace_risk_assesment.osh_reports.osh_data_dashboard')";
        font-size: 18px;
        font-weight: bold; 
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_employer_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_employee_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.compliance.employee_summary')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_employee_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
      
                       #male{
                      font-size: 52px;
                      color: red;
                    }
                    .percentmale{
                     margin-left: 180px;
                     /*object-position: center;*/
                      font-size: 38px;
                      position: relative;
                    }
                     .percentfemale{
                    margin-left: 170px;
                      font-size: 38px;
                      position: relative;
                    }
                    .industryvalue{
                     /*margin-left: 50px;*/
                      font-size: 38px;
                    }
                    .roadvalue{
                     margin-left: 120px;
                     /*object-position: center;*/
                     /*position:relative;*/
                      font-size: 38px;
                    }
                   .leftarrow{
                      font-size: 38px;
                      color:#4863A0;
                      position: all;
                      margin-right: 30px;

                    }
                    .rightarrow{
                      font-size: 38px;
                      color:red;

                      position: relative;
                    }
                    #female{
                      font-size:52px;
                      color:#4863A0;"
                      position:absolute;
                    }
                   
        .tbody_per_region {
            height:350px;
            overflow-y:auto;
            overflow-x:auto;
            width: 100%;
        }
        .table_display_block{
            display:block;
        }

        .td_float_left {
            float:left;
        }

  span {
  content: "\27F5";

   }
#rcorners1 {
  border-radius: 22px;
  background: #73AD21;
  text-align: center;
  padding: 5px;
  color: #fff; 
  width: 100px;
  height: 40px;  
}
#rcorners2 {
  border-radius: 22px;
  border: 2px solid #73AD21;
  text-align: center;
  padding: 5px;
  background: #FF8000;
  color: #000;
  width: 100px;
  height: 50px;
}
#rcorners3 {
  border-radius: 22px;
  background: #E10000;
  background-position: left top;
  background-repeat: repeat;
  padding: 5px;
  color: #fff; 
  width: 100px;
  height: 50px;  
}
#rcorners4 {
  border-radius: 22px;
  border: 1px solid #73AD21;
  background: #4863A0;
  color: #fff;
  padding: 5px;
  text-align: center;
  width: 100px;
  height: 50px;
}
#rcorners5 {
  border-radius: 22px;
  border: 1px solid #73AD21;
  padding: 5px;
  width: 100px;
  height: 50px;
}
#rcorners6 {
  border-radius: 22px;
  background: #73AD21;
  text-align: center;
  padding: 5px;
  color: #fff; 
  width: 100px;
  height: 40px;  
}
#rcorners7 {
  border-radius: 22px;
  border: 2px solid #73AD21;
  text-align: center;
  padding: 5px;
  background: #FF8000;
  color: #000;
  width: 100px;
  height: 50px;
}
#rcorners8 {
  border-radius: 22px;
  background: #E10000;
  background-position: left top;
  background-repeat: repeat;
  padding: 5px;
  color: #fff; 
  width: 100px;
  height: 50px;  
}
#rcorners9 {
  border-radius: 22px;
  border: 1px solid #73AD21;
  background: #4863A0;
  color: #fff;
  padding: 5px;
  text-align: center;
  width: 100px;
  height: 50px;
}
#rcorners10 {
  border-radius: 22px;
  background: #73AD21;
  text-align: center;
  padding: 5px;
  color: #fff; 
  width: 100px;
  height: 40px;  
}
#rcorners11 {
  border-radius: 22px;
  border: 2px;
  text-align: center;
  padding: 5px;
  background: #FF8000;
  color: #000;
  width: 100px;
  height: 50px;
}
#rcorners12 {
  border-radius: 22px;
  background: #E10000;
  background-position: left top;
  background-repeat: repeat;
  padding: 5px;
  color: #fff; 
  width: 100px;
  height: 50px;  
}
#rcorners13 {
  border-radius: 22px;
  border: 1px;
  background: #4863A0;
  color: #fff;
  padding: 5px;
  text-align: center;
  width: 100px;
  height: 50px;
}
#rcorners14 {
  border-radius: 22px;
  border: 1px solid #73AD21;
  padding: 5px;
  width: 100px;
  height: 50px;
}
#sketch {
    border:1px solid #d3d3d3;
      
}
    </style>
    @endpush

    @section('content')
    <div class="row">
        {{--style="position: fixed;z-index: 99999"--}}
        <div class="col-md-6">
            {!! Form::open(['class' => 'select_financial_year', 'method' => 'GET', 'id' => 'select_financial_year']) !!}
            <label class="required">Financial Year</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('fin_year_id', fin_year()->getAll(), [], ['class' => 'form-control search-select','style'=>'width:100px', 'id' => 'select_finance_year']) !!}
                </div>
                <span class="help-block">
                    Select for the statistics based on financial year
                </span>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-6">
            {{ link_to_route('backend.home', 'Refresh Statistics', ['refresh_statistics' => 1], ['class' => 'btn btn-sm btn-success pull-right']) }}
        </div>
    </div>
    <!-- Put the page specifically for this page here -->

     
    {{--Osh --}}
   
     <div class="content">
        <div class="dashboard-content">
            <div class="dashboard-header ">
                <div class="dashboard-action">
                    <ul class="right-action float-xs-right">
                        <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06 icon_plus" aria-hidden="true"></span></a></li>
                        <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="dashboard-box claim_summary" >       
             <div class="row">
             </div>
             <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3"> 
                        <div class="widget-box widget-margin">
                            <div class="static-header bg-success text-xs-center">
                              <h5 class="text-secondary">Accepted</h5> </div>
                                <div class="widget-content text-info">
                                 <div class="row">
                                    <div class="col-sm-12 ">
                                    <span class="fa fa-check-circle fa-5x" style="font-size: 36px;color: black"> </span>
                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['processed_claims'][1]['grand_total']}}</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget-box widget-margin">
                                <div class="static-header bg-info  text-xs-center">
                                    <h5 class="text-secondary">On progress</h5>                 </div>
                                    <div class="widget-content text-info">
                                        <div class="row">
                                            <span class="fa fa-cogs fa-5x" style="font-size: 36px;color: black"> </span>
                                            <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'></a>{{($overall_claim_summary['overall_total_notification_received'])-($overall_claim_summary['processed_claims'][1]['grand_total']+$overall_claim_summary['processed_claims'][2]['grand_total'])}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget-box widget-margin">
                                    <div class="static-header bg-danger text-xs-center">
                                        <h5 class="text-secondary">Rejected</h5>  </div>
                                        <div class="widget-content text-info">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <i class="fa fa-times-circle fa-5x" style="color: black;font-size: 38px;"></i>
                                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['processed_claims'][2]['grand_total']}}</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                <div class="widget-box widget-margin">
                                 <div class="static-header text-xs-center" style="background: #4863A0;">
                                        <h5 class="text-secondary">Total Claims</h5>   </div>
                                        <div class="widget-content text-info">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <i class="fa fa-universal-access" style="color:black;font-size: 38px;"></i>
                                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['overall_total_notification_received']}}</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                        <br>
                    </div>
                </div>
            </div>

    <div class="content">
        <div class="dashboard-content">
            <div class="dashboard-header ">
                 <div class="dashboard-action">
                    <ul class="right-action float-xs-right">
                        <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06 icon_plus" aria-hidden="true"></span></a></li>
                        <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="dashboard-box compliance_employer_summary">
             <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3"> 
                        <div class="widget-box widget-margin">
                            <div class="static-header bg-danger text-xs-center">
                              <h5 class="text-secondary">Fatal cases</h5> </div>
                                <div class="widget-content text-info">
                                 <div class="row">
                                    <div class="col-sm-12 ">

                                    <span class="fa fa-coffin fa-5x" style="font-size: 36px;color: black"> </span>
                                    <img src="{!! asset_url(). '/nextbyte/img/coffin.PNG' !!}" style="width:35px;height:40px;"/>
                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['death_overall_total']}}</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-3">
                                <div class="widget-box widget-margin">
                                    <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                                        <h5 class="text-secondary">Accidents</h5>  </div>
                                        <div class="widget-content text-info">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <i class="fa fa-wheelchair fa-5x" style="color: black;font-size: 38px;"></i>
                                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['accident_overall_total']}}</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                <div class="widget-box widget-margin">
                                 <div class="static-header text-xs-center" style="background: #4863A0;">
                                        <h5 class="text-secondary">Diseases</h5>   </div>
                                        <div class="widget-content text-info">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <i class="fa fa-universal-access" style="color:black;font-size: 38px;"></i>
                                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['disease_overall_total']}}</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                            <div class="widget-box widget-margin">
                                <div class="static-header bg-success  text-xs-center">
                                    <h5 class="text-secondary">Lost Time Injuries-Days</h5>                 </div>
                                    <div class="widget-content text-info">
                                        <div class="row">  
                                            <i class="fa fa-bed" aria-hidden="true" style="font-size: 36px;color:black;"></i>
                                            <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$lost_days}}</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                  {{-- statarting of the Graph --}}
              <div class="row">
            <div class="col-md-6">
                <div class="content">
                    <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                    <h5 class="text-secondary">Top 5 Risk Occupations</h5>  </div>
                    <div id="stocks-div"> </div>
                       @barchart('MyStocks','stocks-div')
                       <br>
                      
               </div>
            </div>
            <div class="col-md-6">
                <div class="content">
                   <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                     <h5 class="text-secondary">Incident By Type</h5>  </div>
                 <div id="chart-div"> </div>
                  @piechart('incident_types', 'chart-div')
                     <br>

                </div>
                </div>
            </div>

              {{--End of graph  --}}
        <br>
        <br>
        <div class="row">
           <div class="col-md-6">
                <div class="content">
                   <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                     <h5 class="text-secondary">Body part injured</h5>  </div>
                   @include('backend.workplace_risk_assesment.osh_reports.includes.skeleton')

                </div>
                </div>
            <div class="col-md-6">
                <div class="content">
                   <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                     <h5 class="text-secondary">Incidents by location trends</h5>  </div>
                 <div id="temps_div"> </div>
                @linechart('Temps','temps_div')
                 <p style="margin-left: 20%">   
                      <b id="rcorners12"></b>&nbsp;On Premise
                      <b id="rcorners13"></b>&nbsp;Out of Premise
                  </p>

                </div>
                </div>
            </div>
            <div class="row">
<div class="col-md-6">
                <div class="content">
                    <div class="static-header  text-xs-center" style="background: #B6B5B5;">
                    <h5 class="text-secondary">Incidents by location percentage</h5>  </div>
                    <br>
                  
                 <img src="{!! asset_url(). '/nextbyte/img/industry.png' !!}" style="width:130px;height:80px;"/>
                 <span class="rightarrow">&#10230;</span>
                 <b  class="industryvalue">26</b>
                 
                   {{-- <span class="percentmale">%</span>       --}}
                <span class="pull-right">
                    <b class="roadvalue"> 56</b>
  
                <span class="leftarrow ">&#10229;</span>
              <img src="{!! asset_url(). '/nextbyte/img/road2.png' !!}" style="width:130px;height:80px;"/>      
                </span>
                  <span class="percentmale ">%</span>
                    <span class="percentfemale">%</span>
                    </div>   
                    </div>
            </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="dashboard-content">
            <div class="dashboard-header ">
                <div class="dashboard-action">
                    <ul class="right-action float-xs-right">
                        <li data-widget="collapse"><a href="javascript:void(0)" aria-hidden="true"><span class="icon_minus-06 icon_plus" aria-hidden="true"></span></a></li>
                        <li data-widget="close"><a href="javascript:void(0)"><span class="icon_close" aria-hidden="true"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="dashboard-box compliance_employer_category_summary" >       
             <div class="row">
             </div>
             <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4"> 
                        <div class="widget-box widget-margin">
                            <div class="static-header bg-success text-xs-center">
                                <h5 class="text-secondary">Total Audited Employers</h5> </div>
                                <div class="widget-content text-info">
                                    <div class="row">
                                        <i class="fa fa-check-square-o" style="color:black;font-size: 38px;margin-left: 20px;"></i>
                                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['audit_summary']}}</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget-box widget-margin">
                                <div class="static-header bg-warning text-xs-center">
                                    <h5 class="text-secondary">WorkPlace summary  </h5>                 </div>
                                    <div class="widget-content text-info">
                                        <div class="row">
                                       <i class="fa fa-industry" style="color:black;font-size: 38px;margin-left: 20px;"></i>
                                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['workplaces']}}</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="widget-box widget-margin">
                                    <div class="static-header text-xs-center" style="background: #4863A0;">
                                        <h5 class="text-secondary">Total Employers For Audit</h5>                 </div>
                                        <div class="widget-content text-info">
                                            <div class="row">
                                                <i class="fa fa-list" style="color:black;font-size: 38px;margin-left: 20px;"></i>
                                                    <span  style="font-size: 36px;color: black;margin-left:30%"><a href='#'>{{$overall_claim_summary['total_employer_audit']}}</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
  
            <div class="row ">
             <div id="chart"> </div>
                </div>
                <br>
              {{--   @linechart('MyStocks','chart') --}}
                 <br>
                 {{-- <div id="stocks-div"> </div>
    
                <br>
                @barchart('MyStocks','stocks-div') --}}
 
                               
                            </div>
                        </div>            
                        <br>
                    </div>
                </div>
            </div>
            @endsection

            @push('after-script-end')
            <!-- Custom javascript files for this page -->
            {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.js") }}
            {{ Html::script(asset_url(). "/global/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js") }}
            {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.resize.js") }}
            {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.pie.js") }}
            {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.time.js") }}
            {{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.categories.js") }}
            {{ Html::script(asset_url(). "/global/plugins/flot-spline/js/jquery.flot.spline.min.js") }}
            {{ Html::script(asset_url(). "/global/plugins/arcseldon-jquery.sparkline/dist/jquery.sparkline.js") }}
            {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
            <script>
                $(function () {
                    $(".search-select").select2({});
                    $("#select_finance_year").on("change", function () {
                        $("#select_financial_year").submit();
                    });
                    $(".stackline-bar").sparkline("html", {
                        type: "bar",
                        height: "40px",
                        barWidth: 5,
                        barSpacing: 5,
                        barColor: ['#2dbf7c'],
                        negBarColor: ['#c9302c'],
                        stackedBarColor: ['#087380', "#c9302c"]
                    });
                });
            </script>

            @endpush


