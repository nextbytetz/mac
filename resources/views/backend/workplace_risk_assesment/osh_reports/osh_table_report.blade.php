@extends('layouts.backend.main', ['title' => trans('OSH Reports'), 'header_title' => trans('OSH Reports')])

@include('backend.includes.datatable_assets')

@section('content')
    {{--<div class = "col-md-12">--}}
        {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=> 'Search For Name ...' ]) !!}--}}
    {{--</div>--}}
    <div>&nbsp;</div>
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%','id'=> 'dataTable'], true) !!}
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}

<script>
    {{--Input to search full name on first column--}}
    function searchname() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("name");
        filter = input.value.toUpperCase();
        table = document.getElementById("dataTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

@endpush

