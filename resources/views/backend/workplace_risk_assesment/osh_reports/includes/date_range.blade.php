{{--DATE RANGE TO BE USED IN SEARCH IN THE REPORT--}}
{!! Form::hidden('this_date', getTodayDate(), ['class' =>'attend_date']) !!}
{!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'attend_date']) !!}

<div class="row ">
    <div class="col-md-12 ">
        <div class="element-form" >
            <div class="col-md-1 text-xs-left"><label>@lang('labels.backend.table.from_date'):</label></div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-inline">

                                      <span>      {!!  Form::selectRange('from_day',1,31,isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('d') : null, ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=> 'from_day']) !!}

                        </span>

                        <span>      {!!  Form::selectMonth('from_month',isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('m') : null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month' ,'id'=> 'from_month']) !!}
                        </span>

                        <span>      {!!  Form::selectRange('from_year',Carbon\Carbon::now()->format('Y'),2015,isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('Y') : null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year', 'id'=> 'from_year']) !!}
                        </span>

                    </div>
                    {!! Form::hidden('from_date', null, ['class' =>'from_date']) !!}
                    {!! $errors->first('from_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

        {{--to date--}}
        <div class="element-form"  >
            <div class="col-md-1 text-xs-left"><label>@lang('labels.backend.table.to_date'):</label></div>
            <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <div class="form-inline">

                                      <span>      {!!  Form::selectRange('to_day',1,31,isset($request->from_date) ? \Carbon\Carbon::parse($request->to_date)->format('d') : null, ['class' => 'form-control  search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=>'to_day' ]) !!}

                        </span>

                        <span>      {!!  Form::selectMonth('to_month',isset($request->from_date) ? \Carbon\Carbon::parse($request->to_date)->format('m') : null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month' ,'id'=>'to_month']) !!}
                        </span>

                        <span>      {!!  Form::selectRange('to_year',Carbon\Carbon::now()->format('Y'),2015,isset($request->from_date) ? \Carbon\Carbon::parse($request->to_date)->format('Y') : null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year','id'=>'to_year']) !!}
                        </span>

                    </div>
                    {!! Form::hidden('to_date', null, ['class' =>'to_date']) !!}
                    {!! $errors->first('to_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
  
        <div class="element-form"  >
            <div class="col-md-2">
                <div class="pull-right">
                   
                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                </div>
            </div>
        </div>

        <div>&nbsp;</div>
    </div>
</div>



@push('date-range-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script >
        $(function (){
            $('.search-select').select2();

            $('body').on('submit', 'form[name=report]', function(e) {
                e.preventDefault();
//From Date
                var $day =$('#from_day').val();
                var $year = $('#from_year').val();
                var $month = $('#from_month').val();
                if(($year)&& ($month) && ($day )) {
                    $('input[name=from_date]').val($year + '-' + $month + '-' + $day);
                }else {
                    $("input[name=from_date]").val("");
                }
//          to date
                var $day_to =$('#to_day').val();
                var $year_to = $('#to_year').val();
                var $month_to = $('#to_month').val();
                if(($year_to)&& ($month_to) && ($day_to)) {
                    $('input[name=to_date]').val($year_to + '-' + $month_to + '-' + $day_to);
                }else {
                    $("input[name=to_date]").val("");
                }

//            disable individual dates elements
//
//                 $("#from_day").prop("disabled", true);
//                 $("#from_year").prop("disabled", true);
//                 $("#from_month").prop("disabled", true);
//                 $("#to_day").prop("disabled", true);
//                 $("#to_year").prop("disabled", true);
//                 $("#to_month").prop("disabled", true);

                /*reset export flag*/
                // $('#export_flag').val(0).change();

                this.submit();
            });

        });

    </script>
@endpush