@extends('layouts.backend.main', ['title' => trans('OSH Reports'), 'header_title' => trans('OSH Reports')])


@section('content')

<div style="color:#fff">

 <div class="row">
    <div class="col-sm-6 col-md-6">
        <div class="list-group">
            {{--item 1--}}
            <ul class="list-unstyled">
                <a href="{!! route('backend.workplace_risk_assesment.reports.osh_table_reports') !!}">

                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list"> </i><large>&nbsp;&nbsp;@lang('labels.backend.workplace_risk_assesment.osh_reports.table_title')
                    </large></h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.workplace_risk_assesment.osh_reports.table_description')</p> </li>
                </a>

            </ul>
        </div>
    </div>


    {{--right div--}}
    <div class="col-sm-6 col-md-6">
        <div class="list-group">
            {{--Module for Audit checklist --}}
            <ul class="list-unstyled">
                <a href="{!! route('backend.workplace_risk_assesment.reports.osh_graphical_reports') !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-database"> </i><large>&nbsp;&nbsp;@lang('labels.backend.workplace_risk_assesment.osh_reports.graphical_title')
                    </large></h6>

                    <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.workplace_risk_assesment.osh_reports.graphical_description')</p> </li>
                </a>
            </ul>



        </div>


    </div>
</div>

<div class="row">
    <div class="col-sm-12">&nbsp;</div>
</div>
    </div>

</div>

</div>


@stop

@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {
            /*
             $("#site-header-title").hide();
             */
             $("#preview").click(function() {

             });
         });
     </script>;

     @endpush
