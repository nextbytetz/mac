@extends('layouts.backend.main', ['title' => trans('labels.backend.home'), 'header_title' => trans('labels.general.welcome')])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .claim_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.claim.title')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .claim_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.compliance.title')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_employer_category_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.compliance.employer_category_size_summary')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_employer_category_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_employer_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.compliance.employer_summary')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_employer_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .compliance_employee_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.compliance.employee_summary')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .compliance_employee_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .finance_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.finance.title')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .finance_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .legal_summary:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.report.dashboard.legal.title')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .legal_summary {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    /*.table-fixed {*/
    /*width: 100%;*/
    /*background-color: #f3f3f3;*/
    /*}*/
    .tbody_per_region {
        height:350px;
        overflow-y:auto;
        overflow-x:auto;
        width: 100%;
    }
    .table_display_block{
        display:block;
    }

    .td_float_left {
        float:left;
    }

</style>
@endpush

@section('content')
    <div class="row">
        {{--style="position: fixed;z-index: 99999"--}}
        <div class="col-md-6">
            {!! Form::open(['class' => 'select_financial_year', 'method' => 'GET', 'id' => 'select_financial_year']) !!}
            <label class="required">Financial Year</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('fin_year_id', fin_year()->getAll(), [], ['class' => 'form-control search-select','style'=>'width:100px', 'id' => 'select_finance_year']) !!}
                </div>
                <span class="help-block">
                    Select for the statistics based on financial year
                </span>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-6">
            {{ link_to_route('backend.home', 'Refresh Statistics', ['refresh_statistics' => 1], ['class' => 'btn btn-sm btn-success pull-right']) }}
        </div>
    </div>
    <!-- Put the page specifically for this page here -->
    {{--finance--}}
    @if ($selector == 12)
        @include("backend/dashboard/finance")
    @endif
        {{--claim administration--}}
    @if ($selector == 14)
        @include("backend/dashboard/claim")

    @endif
        {{--claim assessment--}}
    @if ($selector == 17)
        @include("backend/dashboard/claim")
    @endif

        {{--Compliance--}}
    @if ($selector == 15)
        @include("backend/dashboard/compliance")

    @endif
       {{--legal Service--}}
    @if ($selector == 6)
        @include("backend/dashboard/legal")
     @endif

    {{--Director DG - Executive--}}
    @if ($selector == 11)
        @include("backend/dashboard/executive_dashboard")
    @endif

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.js") }}
{{ Html::script(asset_url(). "/global/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js") }}
{{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.resize.js") }}
{{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.pie.js") }}
{{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.time.js") }}
{{ Html::script(asset_url(). "/global/plugins/flot/jquery.flot.categories.js") }}
{{ Html::script(asset_url(). "/global/plugins/flot-spline/js/jquery.flot.spline.min.js") }}
{{ Html::script(asset_url(). "/global/plugins/arcseldon-jquery.sparkline/dist/jquery.sparkline.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function () {
        $(".search-select").select2({});
        $("#select_finance_year").on("change", function () {
            $("#select_financial_year").submit();
        });
        $(".stackline-bar").sparkline("html", {
            type: "bar",
            height: "40px",
            barWidth: 5,
            barSpacing: 5,
            barColor: ['#2dbf7c'],
            negBarColor: ['#c9302c'],
            stackedBarColor: ['#087380', "#c9302c"]
        });
    });
</script>

@endpush
