@extends('layouts.backend.main', ['title' => trans('labels.backend.organization_title'), 'header_title' => trans('labels.general.organization_header')])


@section('content')

        <div style="color:#fff">
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                <ul class="list-unstyled">
                    <a href="{!! route('backend.organization.fiscal_year.index') !!}">
                      <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i><large>&nbsp;&nbsp;@lang('labels.backend.organization_menu.manage_annual_target')</large></h6>

                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.organization_menu.manage_annual_target_summary')</p> </li>
                    </a>

                </ul>



            </div>
        </div>
        </div>




@stop


@push('after-script-end')
<script type="text/javascript">
//    $(document).ready(function() {
//        $("#site-header-title").hide();
//        $("#preview").click(function() {
//
//        });
//    });
</script>;

@endpush