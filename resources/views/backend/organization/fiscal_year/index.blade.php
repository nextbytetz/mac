@extends('layouts.backend.main', ['title' => trans('labels.backend.organization.fiscal_years'), 'header_title' => trans('labels.backend.organization.fiscal_years')])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            {{----------Button Add New -----------}}
            <div class="col-md-12" >
                <div class="pull-right">
                    <a href="{!! route('backend.organization.fiscal_year.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
                </div>
            </div>

            <div>&nbsp;</div>
            <table class="display" cellspacing="0" width="100%" id ="fiscal-years-table">
                <thead>
                <tr>
                    <th>@lang('labels.backend.organization.fiscal_year')</th>
                    <th>@lang('labels.backend.organization.start_year')</th>
                    <th>@lang('labels.backend.organization.end_year')</th>

                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#fiscal-years-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.organization.fiscal_year.get') !!}',
                type : 'get'
            },
            columns: [
                { data: 'fiscal_year', name: 'fiscal_year'},
                { data: 'start_year' , name: 'start_year'},
                { data: 'end_year', name: 'end_year'}
              ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url +  "/organization/fiscal_year/" + aData['id'] +  "/edit"  ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });







</script>;

@endpush
