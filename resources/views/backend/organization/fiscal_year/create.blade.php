@extends('layouts.backend.main', ['title' => trans('labels.backend.organization.add_fiscal_year'), 'header_title' => trans('labels.backend.organization.add_fiscal_year')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::open(['route' => ['backend.organization.fiscal_year.store' ],'method'=>'post',
     'name' => 'create']) !!}


    {{--dates--}}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFLaunchDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('request_action_type', 1 , []) !!}
    <br>


    {{--start year--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.organization.start_year'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                                        <span>      {!!  Form::selectRange('start_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::parse(getWCFLaunchDate())->format('Y'),null, ['class' => 'form-control search-select','style'=>'width:62px',
                        'placeholder' =>
                         'Year', 'id'=>'start_year']) !!}
                        </span>



                    {!! $errors->first('start_year', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
    </div>



<br>




    {{--Annual Target types--}}
    <div class="row">
        <div class="col-md-12 light_grey_bg">
            <div class="form-inline" >
                {{--annual target type checklist--}}
                <div class="col-md-3" align="left">
                    <div class="form-group ">
                        <label> @lang('labels.backend.organization.annual_target_type')</label>
                    </div>
                </div>
                {{--Total--}}
                <div class="col-md-2" align="center">
                    <div class="form-group required">
                        <label> @lang('labels.general.total')</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--loop all annnual types--}}
    @foreach ($annual_target_types as $annual_target_type)
        <div>&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-inline" >
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label( 'checklist', $annual_target_type->name, [ 'id'=>
              'checklist'])!!}
                        </div>
                    </div>
                    {{--feedback--}}
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::input( 'text','target_total' . $annual_target_type->id, null, ['class' => 'form-control days money','style'=>'width:300px', 'id'=>'target_total'.$annual_target_type->id]) !!}
                        </div>
                    </div>

                </div>


            </div>
        </div>
        {!! $errors->first('target_total'. $annual_target_type->id, '<span class="help-block label
label-danger">:message</span>') !!}
    @endforeach

<br>
<br>
    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('organization/fiscal_year   ',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
<script  type="text/javascript">

    $(function () {
        $(".search-select").select2({});

        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });
    });

</script>;


@endpush
