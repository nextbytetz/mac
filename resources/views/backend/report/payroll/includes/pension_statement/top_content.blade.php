<div class="center">
    <img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>

</div>



<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">PENSION STATEMENT</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">


    <tbody>
    <tr>

        <td style="width: 46.3333%; vertical-align: middle; text-align: left;">
            <div><b>{{ strtoupper($member->name) }}</b></div>
            <div>PHONE NO.: {{ $member->phone }}  </div>
            <div>STATUS: {!!  $member->getStatusLabelAttribute($employee_id) !!}  </div>
        </td>
        <td style="width: 13.3333%;"></td>

        <td style="width: 40.3333%; text-align: left;">

            <div><b>Statement Date:</b> {{ short_date_format(getTodayDate()) }}</div>
            <div><b>Statement Period:</b > <label style="font-size: 14px" > {{ short_date_format($start_date) . ' to ' . short_date_format($end_date) }} </label> </div>
            <div><b>Claim Number:</b> {{ $notification_report->filename }}</div>
            <div><b>Currency:</b>  {{ 'TZS' }}  </div>
        </td>
    </tr>

    </tbody>
</table>


<div>&nbsp;</div>