<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <base target="_blank">
    <meta charset="utf-8" />
    <title>{{  'Pension Statement' }}</title>
    {{--<meta name="viewport" content="width=device-width">--}}
    <meta name="author" content="Martin Luhanjo <m.luhanjo@nextbyte.co.tz>"/>
    <meta name="date" content="2020-10-04"/>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/payroll/pension_statement.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
</head>
<body>

<div id="content">
    <div class="center">
        {{--<img id="tanzania" src="{{ asset_url() . "/nextbyte/img/national_emblem.jpg" }}">--}}
        <div class="row">

  @include('backend/report/payroll/includes/pension_statement/top_content')

        </div>
        <br/>
        <table class="table table-striped table-bordered trow_border" style="border: 1px solid #dddddd;" id="">
            <thead>
            <tr class="trow_border" style="background-color: lightgrey; color: #636363;">
                <th class="trow_border column_header_sm">{{ 'Sn'}}</th>
                <th class="trow_border column_header_sm">{{ 'Payroll Month'}}</th>
                <th class="trow_border column_header_sm">{{ 'Bank' }}</th>
                <th class="trow_border column_header_sm">{{ 'Arrears' }}</th>
                <th class="trow_border column_header_sm">{{ 'Unclaimed' }}</th>
                <th class="trow_border column_header_sm">{{ 'Deduction'}}</th>
                <th class="trow_border column_header_sm">{{ 'Months Paid'}}</th>
                <th class="trow_border column_header_sm">{{ 'Monthly Pension' }}</th>
                <th class="trow_border column_header_sm">{{ 'Net Payable Amount' }}</th>
            </tr>
            </thead>
            <tbody>
            @php
                $total_net = 0;
            $sn = 1;
            @endphp
            @foreach($payroll_runs as $payroll_run)
                <tr class="trow_border">
                    <td class=" trow_border row_input" style="text-align:center;">{{ $sn++ }}</td>
                    <td  class =" row_input" style="">{{ month_year_date_format($payroll_run->payroll_month) }}</td>
                    <td class =" trow_border row_input number_input" style="">{{ ($payroll_run->bank_name)  . ' - ' .$payroll_run->run_accountno  }}</td>
                    <td class ="trow_border row_input number_input" style="">{{ number_2_format($payroll_run->arrears_amount) }}</td>
                    <td class =" trow_border row_input number_input" style="">{{ number_2_format($payroll_run->unclaimed_amount) }}</td>
                    <td class =" trow_border row_input number_input" style="">{{ number_2_format($payroll_run->deductions_amount) }}</td>
                    <td class =" trow_border row_input number_input" style="">{{ number_2_format($payroll_run->months_paid) }}</td>
                    <td class ="trow_border  number_input row_input" style="">{{ number_2_format($payroll_run->monthly_pension) }}</td>
                    <td class="trow_border row_input number_input" style="">{{ number_2_format($payroll_run->amount) }}</td>
                </tr>

                @php
                    $total_net = $total_net + $payroll_run->amount;
                @endphp
            @endforeach

            <tr class="trow_border">
                <th style="text-align:center;">{{ 'TOTAL'  }}</th>
                <td style="">{{ ''}}</td>
                <td style="">{{ ''}}</td>
                <td style="">{{ ''}}</td>
                <td style="">{{ ''}}</td>
                <td style="">{{ ''}}</td>
                <td style="">{{ ''}}</td>
                <td style="">{{ ''}}</td>
                <th class="number_input" style="">{{ number_2_format($total_net) }}</th>
            </tr>
            </tbody>
        </table>


    </div>
</div>

@include('backend/report/payroll/includes/pension_statement/bottom_content')


{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
<script type="text/javascript">

        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };

    $(function(){
        $(document).on("contextmenu",function(e){
            return false;
        });
    });
</script>
</body>
</html>