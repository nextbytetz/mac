{{--General info--}}
{{--<br/>--}}
@if(count($total_summary))
    <div class="row">
        {{--left--}}
        {{--left general  summary--}}
        <div class="col-md-6">
            <div class="col-md-12">
                @include('backend/report/payroll/includes/payroll_payment_summary/payment_details_summary')
            </div>
        </div>
        {{--Right summary--}}
        <div class="col-md-6">
            <div class="col-md-12">
                @include('backend/report/payroll/includes/payroll_payment_summary/no_of_beneficiary_summary')
            </div>
        </div>

    </div>
@endif