

<div style="background-color: lightgrey;text-align:center">Number of Beneficiary Processed </div>
<table class="table table-striped table-bordered">
    <tbody>

    <tr>
        <td width="160px">No. of Pensioners</td>
        <th  class="number_input">{!!
                          number_0_format($total_summary['count_pensioner'])
                                !!} </th>
    </tr>

    <tr>
        <td>No. of Dependents</td>
        <th  class="number_input">
            {!!      number_0_format($total_summary['count_dependent'])
                        !!}</th>
    </tr>


    <tr>
        <td>No. of Constant Cares</td>
        <th  class="number_input">
            {!!      number_0_format($total_summary['count_constant_care'])
                        !!}</th>
    </tr>


    <tr>
        <td>No. of Suspended pensioners</td>
        <th  class="number_input">
            {!!               number_0_format($total_summary['count_pensioner_suspended'])
                        !!}</th>
    </tr>

    <tr>
        <td>No. of Suspended Dependents</td>
        <th  class="number_input">
            {!!           number_0_format($total_summary['count_dependent_suspended'])
                        !!}</th>
    </tr>

    <tr>
        <td>No. of Suspended Constant Cares</td>
        <th  class="number_input">
            {!!     number_0_format($total_summary['count_constant_care_suspended'])
                        !!}</th>
    </tr>



    </tbody>
</table>



