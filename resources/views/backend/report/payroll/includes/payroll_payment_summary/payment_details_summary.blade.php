


<div style="background-color: lightgrey;text-align:center">Payments Amount Details</div>
<table class="table table-striped table-bordered">
    <tbody>



    <tr >
        <td width="180px">Arrears Amount  <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total arrears paid on this payroll including accumulation
                    arrears, unclaimed, underpayments."></i></td>
        <th class="number_input">
            {!!         number_2_format($total_summary['arrears_amount'])
                        !!}</th>
    </tr>


    <tr>
        <td>Deductions Amount <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total deductions recovered on this payroll from
                    over-payments of beneficiaries."></i></td>
        <th  class="number_input">
            {!!    number_2_format($total_summary['deduction_amount'])
                        !!}</th>
    </tr>


    <tr>
        <td>Suspended Amount <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total suspended payments of all suspended beneficiaries."></i></td>
        <th  class="number_input">
            {!!      number_2_format($total_summary['suspended_amount'])
                        !!}</th>
    </tr>

    <tr>
        <td>Net Pensioner Amount <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total pensioners' amount."></i></td>
        <th  class="number_input">
            {!!   number_2_format($total_summary['net_pensioner'])!!}</th>
    </tr>


    <tr>
        <td>Net Dependent Amount <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total dependents' amount."></i></td>
        <th  class="number_input">
            {!!   number_2_format($total_summary['net_dependent'])!!}</th>
    </tr>

    <tr>
        <td>Net Constant Care Amount <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total constant care assistants' amount."></i></td>
        <th  class="number_input">
            {!!   number_2_format($total_summary['net_constant_care'])!!}</th>
    </tr>



    <tr>
        <td style="font-size: 16px" >Total Net Payable <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total net payable
                    amount for all beneficiaries eligible on this payroll."></i></td>
        <th class="number_input"  style="font-size: 20px">{!!
                          number_2_format($total_summary['net_amount'])
                                !!} </th>
    </tr>


    </tbody>
</table>

