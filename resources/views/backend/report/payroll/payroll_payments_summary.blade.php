@extends('layouts.backend.main', ['title' => $report_title , 'header_title' => $report_title])

@include('backend/includes/assets/datetimepicker')
@include('backend.includes.datatable_assets')
@include('backend.includes.assets.select2_assets')

@push('after-styles-end')
    <style>
    </style>
@endpush

@section('content')

    {{ Form::open(['route' => ['backend.operation.report.payroll.payroll_payments_summary'],'method'=>'get', 'name' => 'report']) }}
    {{ Form::hidden('search_flag' , 1, []) }}
    {{ Form::hidden('export_flag' , null, ['id' => 'export_flag']) }}
    {{ Form::hidden('export_pdf' , 0, ['id' => 'export_pdf']) }}
    <div class="row">
        <div class="col-md-12 card-body">
            {{--<div class="pull-right">--}}
            {{--                <a href="{{ route('admin.report.reports_by_group', (isset($request['report_group_id'])) ? $request['report_group_id'] : 1) }}">{{ __('label.go_back') }}</a>--}}
            <br/>
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-lg-8" >{{ 'Period Type' }}:</label>


                                {{ Form::select('period_type',['1' => 'Monthly', '2' => 'Quarterly' , '3' => 'Annually'],(isset($request['period_type'])) ? $request['period_type'] : 1, ['style' => 'width:100%', 'placeholder' => 'Period Type','class' => 'form-control search_input select2', 'id'=> 'period_type']) }}

                                {!! $errors->first('period_type', '<span class="badge badge-danger">:message</span>') !!}

                            </div>
                        </div>
                        {{--monthly--}}
                        <div class="col-md-2 period_div div_period_type1" id="div_period_type1">
                            <div class="form-group">
                                <label class="col-lg-8" >{{  'Month' }}:</label>
                                <div class="form-group input-group">
						          <span class="input-group-addon">
						             <span class="input-group-text">
						            	<i class="icon fa fa-calendar"></i>
						              </span>
						           </span>
                                    {{ Form::text('from_date',(isset($request['from_date']) ? short_date_format($request['from_date']) : null) , ['placeholder' => 'Month'  ,'id'=>'from_date', 'class' => 'form-control datepicker_month div_period_type1 period_input','required', 'autocomplete'=>"off",
                                    'style'
                                     =>
                                    'background-color:
                                    white;']) }}
                                </div>
                                {!! $errors->first('from_date', '<span class="badge badge-danger">:message</span>') !!}

                            </div>
                        </div>

                        {{--quarterly--}}
                        <div class="col-md-2 period_div div_period_type2" id="div_period_type2">
                            <div class="form-group">
                                <label class="col-lg-8" >{{  'Fiscal Year' }}:</label>
                                <div class="form-group input-group">

                                    {{ Form::select('fin_year_id',$fiscal_years,(isset($request['fin_year_id'])) ? $request['fin_year_id'] : 0, ['style' => 'width:100%','class' => 'form-control search_input select2 div_period_type2 period_input', 'id'=> 'fin_year_id']) }}
                                </div>
                                {!! $errors->first('fin_year_id', '<span class="badge badge-danger">:message</span>') !!}

                            </div>
                        </div>

                        <div class="col-md-2 period_div div_period_type2" id="div_period_type2">
                            <div class="form-group">
                                <label class="col-lg-8" >{{  'Quarterly' }}:</label>
                                <div class="form-group input-group">

                                    {{ Form::select('quarter',['1' => 'Quarter 1 (Jul - Sep)', '2' => 'Quarter 2 (Oct - Dec)' , '3' => 'Quarter 3 (Jan - Mar)' , '4' => 'Quarter 4 (Apr - Jun)'],(isset($request['quarter'])) ? $request['quarter'] : 0, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search_input select2 div_period_type2 period_input', 'id'=> 'quarter']) }}
                                </div>
                                {!! $errors->first('quarter', '<span class="badge badge-danger">:message</span>') !!}

                            </div>
                        </div>
                        {{--end quarterly--}}

                        {{--Annually--}}
                        <div class="col-md-2 period_div div_period_type3" id="div_period_type3">
                            <div class="form-group">
                                <label class="col-lg-8" >{{  'Fiscal Year' }}:</label>
                                <div class="form-group input-group">

                                    {{ Form::select('fin_year_id',$fiscal_years,(isset($request['fin_year_id'])) ? $request['fin_year_id'] : 0, ['style' => 'width:100%','class' => 'form-control search_input select2 period_input div_period_type3', 'id'=> 'fin_year_id']) }}
                                </div>
                                {!! $errors->first('fin_year_id', '<span class="badge badge-danger">:message</span>') !!}

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <br/>
            <div class="row">
                <div class = "col-md-6">
                    <div class="row">
                        <div class = "col-md-6">
                            <div class="form-group pull-right">
                                {{ Form::button(trans('buttons.general.search'),['class' => 'btn btn-sm btn-primary site-btn save_button button', 'type'=>'submit']) }}
                                <button class="btn btn-sm" id="export_button_pdf" >{{ 'Print PDF' }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br/>
            @include('backend/report/payroll/includes/payroll_payment_summary/payment_summary')




        </div>
    </div>
    {{--<br/>--}}
    {{--<hr class="hr_custom">--}}
    {{ Form::close() }}


    <table class="display" cellspacing="0" width="100%" style="background-color: lightgrey">
        <thead>
        <tr>
            <th style="width: 150px;"></th>
            <th></th>
        </tr>
        </thead>
    </table>
@endsection

@push('after-script-end')

    <script>
        $(function() {
            $(".select2").select2();

            $("button").click(function () {
                $("#report").attr('target', '_self');
                if (this.id == 'export_button') {
                    $('#export_flag').val(1).change();
                    $('#export_pdf').val(0).change();
                }else if(this.id == 'export_button_pdf'){
                    $('#export_flag').val(1).change();
                    $('#export_pdf').val(1).change();
                    $("#report").attr('target', '_blank');
                } else {
                    $('#export_flag').val(0).change();
                    $('#export_pdf').val(0).change();
                }
            });



            $('body').on('submit', 'form[name=report]', function(e) {
                e.preventDefault();
                $('.button').prop('disabled', false);
                this.submit();
            });

            jQuery('.datepicker_month').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
            });


            periodTypeOption();
            $("#period_type").change(function(){
                periodTypeOption();
            });


            function periodTypeOption()
            {
                //code here
                var choice = element_id_value('period_type');

                hide_show('hide_class', 'period_div');
                enable_disable('disable_class', 'period_input');

                hide_show('show_class', 'div_period_type' + choice);
                enable_disable('enable_class',  'div_period_type' + choice);
            }

        });
    </script>
@endpush
