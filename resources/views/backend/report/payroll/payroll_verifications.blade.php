@extends('layouts.backend.main', ['title' => 'Payroll Verifications Report', 'header_title' => 'Payroll Verifications Report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.payroll.payroll_verifications'],
  'method'=>'get',
  'name' => 'report', 'id' => 'report_form']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')

    <div class="row">

        <div class = "col-md-12">
            <div class="row">

                <div class = "col-md-3" id="verify_type_div">

                    {!! Form::select('type' , ['1' => 'Verified', '2' => 'Unverified'],isset($request['type']) ? $request['type'] : 1 ,  ['style' => 'width:100%', 'placeholder' => 'Search for verify type','class' => 'form-control search-select', 'id'=> 'type_id','hidden'=>false]) !!}
                </div>

                {{--search member type--}}
                <div class = "col-md-3" id="member_type_div">

                    {!! Form::select('member_type_id' , ['5' => 'Pensioners/Employee', '4' => 'Dependents', '1' => 'Constant Care Assistant'],$request['member_type_id'] ,  ['style' => 'width:100%', 'placeholder' => 'Search for member type','class' => 'form-control search-select', 'id'=> 'member_type_id','hidden'=>true]) !!}
                </div>




            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>

                {{--verify type option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'type_check',1 , ($request['type_check'] == 1) ? true : true, ['style' => 'width:20px','id' => 'type_check', 'disabled' ]) !!}Verify Type</label>
                </div>

                {{--member type option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'member_type_check',1 , ($request['member_type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'member_type_check' ]) !!}Member Type</label>
                </div>


                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>



            </div>


        </div>


    </div>
    <br/>
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}



@stop

@push('after-script-end')


    @stack('date-range-script-end')
    {!! $dataTable->scripts() !!}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script type="text/javascript">

        // main function search options
        $(function () {
            $(".search-select").select2();

            general_option('member_type_check', 'member_type_div','member_type_id');

            $("#member_type_check").click(function () {
                general_option('member_type_check', 'member_type_div','member_type_id');

            });

            general_option('type_check', 'type_div','type_id');

            $("#type_check").click(function () {
                general_option('type_check', 'type_div','type_id');

            });


            $("#none_check").click(function () {
                none_option();
            });

            // $("#export_button").click(function () {
            //     $('#export_flag').val(1).change();
            //     $('#report_form').submit();
            // })

            $("button").click(function() {
                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();

                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

        });


        //SEARCH OPTIONS-------------------------
        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {
            $( "#member_type_check").prop( "checked", false );
            $("#member_type_div" ).hide();
            $( "#member_type_id").prop( "disabled", true );


        }

    </script>


@endpush

