@extends('layouts.backend.main', ['title' => trans('labels.backend.report.monthly_pension_payroll'), 'header_title' => trans('labels.backend.report.monthly_pension_payroll')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')


    {!! Form::open(['route' => ['backend.operation.report.payroll.monthly_pension'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}

    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                {{--search batch--}}
                <div class = "col-md-2">
                    <label>@lang('labels.general.select_batch'):</label>
                </div>

                {{--search incident type--}}
                <div class = "col-md-5" id="payroll_proc_div">

                    {!! Form::select('payroll_proc_id', $payroll_procs, null, ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_payroll_batch'),'class' => 'form-control search-select', 'id'=> 'payroll_proc_id']) !!}

                </div>

                <div class="element-form"  >
                    <div class="col-md-2">
                        <div class="pull-right">

                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </br>
    {{--Search INPUTS----------------}}
    <div class="row">

        <div class = "col-md-12">
            <div class="row">
                {{--search for bank name--}}
                {{--<div class = "col-md-3" id="bank_div">--}}
                    {{--{!! Form::select('bank_id', $banks, null, ['style' => 'width:100%', 'placeholder' => trans('labels.backend.finance.receipt.search_for_bank_name'),'class' => 'form-control search-select', 'id'=> 'bank_id', 'hidden'=>true]) !!}--}}
                {{--</div>--}}

                {{--Bank--}}
                <div class = "col-md-3" id="bank_div">
                    {!! Form::select('bank_id', ['3' => 'NMB', '4' => 'CRDB', '0' => 'Others'], null, ['style' => 'width:100%', 'placeholder' => trans('labels.backend.finance.receipt.search_for_bank_name'),'class' => 'form-control search-select', 'id'=> 'bank_id', 'hidden'=>true]) !!}
                </div>

                {{--search for member type--}}
                <div class = "col-md-3" id="member_type_div">
                    {!! Form::select('member_type', ['1' => 'Dependents', '2' => 'Pensioners' , '3' => 'Constant Care'], null, ['style' => 'width:100%', 'placeholder' => 'Member type ...','class' => 'form-control search-select', 'id'=> 'member_type_id', 'hidden'=>true]) !!}
                </div>


            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>
                {{--bank name option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'bank_check',1, ($request['bank_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'bank_check']) !!}@lang('labels.backend.table.bank')</label>
                </div>
                {{--Member type option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'member_type_check',1, ($request['member_type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'member_type_check']) !!}Member type</label>
                </div>

                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>









    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}



    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! $total_amount !!}</th>

        </tr>
        </thead>
    </table>

@stop

@push('after-script-end')
    {!! $dataTable->scripts() !!}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>

        // main function search options
        $(function () {
            $(".search-select").select2();
            general_option('bank_check', 'bank_div','bank_id');
            $("#bank_check").click(function () {
                general_option('bank_check', 'bank_div','bank_id');
            });

            general_option('member_type_check', 'member_type_div','member_type_id');
            $("#member_type_check").click(function () {
                general_option('member_type_check', 'member_type_div','member_type_id');
            });


            $("#none_check").click(function () {
                none_option();
            })
        });

        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {
            $( "#bank_check").prop( "checked", false );
            $("#bank_div" ).hide();
            $( "#bank_id").prop( "disabled", true );
            $("#member_type_div" ).hide();
            $( "#member_type_id").prop( "disabled", true );

        }




    </script>


@endpush
