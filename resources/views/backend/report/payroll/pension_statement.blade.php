@extends('layouts.backend.main', ['title' => 'Pension Statement', 'header_title' =>'Pension Statement'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')


    {!! Form::open(['route' => ['backend.operation.report.payroll.pension_statement'],
  'method'=>'get',
  'name' => 'report', 'id'=> 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {{ Form::hidden('export_flag' , null, ['id' => 'export_flag']) }}
    {{ Form::hidden('export_pdf' , 0, ['id' => 'export_pdf']) }}
    <div class="row">
        <div class="col-md-12 card-body">
            {{--<div class="pull-right">--}}
            {{--<a href="{{ route('admin.report.reports_by_group', (isset($request['report_group_id'])) ? $request['report_group_id'] : 1) }}">{{ __('label.go_back') }}</a>--}}
            {{--</div>--}}
            @include('backend/report/includes/date_filter', ['date_filter_type' => 1 ])
            <br/>
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">

                        <div class = "col-md-3" id="general_type_div">
                            {!! Form::select('general_type', ['1' => 'System', '2' => 'Manual' , '3' => 'All'],$request['general_type'] ?? 1, ['style' => 'width:100%', 'placeholder' => 'Choose Payroll Type ...','class' => 'form-control select2 ', 'id'=> 'general_type_id']) !!}
                        </div>

                        {{--Search for dpeendent--}}
                        <div class = "col-md-3" id="dependent_div">
                            {!! Form::select('dependent_id', $dependents,$request['dependent_id'], ['style' => 'width:100%', 'placeholder' => 'Dependent ...','class' => 'form-control dependent-select pdf_trigger', 'id'=> 'dependent_id', 'hidden'=>true]) !!}
                        </div>

                        {{--Search for pensioner--}}
                        <div class = "col-md-3" id="pensioner_div">
                            {!! Form::select('pensioner_id',$pensioners,$request['pensioner_id'], ['style' => 'width:100%', 'placeholder' => 'Pensioner ...','class' => 'form-control pensioner-select pdf_trigger', 'id'=> 'pensioner_id', 'hidden'=>true]) !!}
                        </div>

                        {{--search for member type--}}
                        <div class = "col-md-3" id="member_type_div">
                            {!! Form::select('member_type', ['1' => 'Dependents', '2' => 'Pensioners' , '3' => 'Constant Care'], null, ['style' => 'width:100%', 'placeholder' => 'Member type ...','class' => 'form-control search-select', 'id'=> 'member_type_id', 'hidden'=>true]) !!}
                        </div>

                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label>{{ 'Search for filter'}}:</label>
                        </div>
{{--General type--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'general_type_check',1, ($request['general_type_check'] == 1) ? true : true, ['style' => 'width:20px','id' => 'general_type_check']) !!}Payroll Type</label>
                        </div>

                        {{--Dependent option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'dependent_check',1, ($request['dependent_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'dependent_check']) !!}Dependent</label>
                        </div>

                        {{--Pensioner option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'pensioner_check',1, ($request['pensioner_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'pensioner_check']) !!}Pensioner</label>
                        </div>

                        {{--Member type option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'member_type_check',1, ($request['member_type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'member_type_check']) !!}Member type</label>
                        </div>

                        {{--None option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class = "col-md-8">
                    <div class="row">
                        <div class = "col-md-6">
                            <div class="form-group pull-right">
                                {{ Form::button(trans('buttons.general.search'),['class' => 'btn btn-sm btn-primary site-btn save_button button', 'type'=>'submit']) }}
                                {{ Form::button('Export to Excel',['class' => 'btn btn-success btn-sm site-btn dishonour_button button', 'id' => 'export_button', 'type'=>'submit']) }}
                                <button class="btn btn-sm" id="export_button_pdf" hidden>{{ 'Print Statement' }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    @if(isset($request['search_flag']))
        {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%','id'=> 'dataTable'], true) !!}
    @endif



    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! $total_amount !!}</th>

        </tr>
        </thead>
    </table>

@stop

@push('after-script-end')
    @if(isset($request['search_flag']))
        {!! $dataTable->scripts() !!}
    @endif
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>

        // main function search options
        $(function () {
            $(".search-select").select2();


            $("button").click(function () {
                $("#report").attr('target', '_self');
                if (this.id == 'export_button') {
                    $('#export_flag').val(1).change();
                    $('#export_pdf').val(0).change();
                }else if(this.id == 'export_button_pdf'){
                    $('#export_flag').val(1).change();
                    $('#export_pdf').val(1).change();
                    $("#report").attr('target', '_blank');
                } else {
                    $('#export_flag').val(0).change();
                    $('#export_pdf').val(0).change();
                }
            });

            general_option('dependent_check', 'dependent_div','dependent_id');
            $("#dependent_check").click(function () {
                general_option('dependent_check', 'dependent_div','dependent_id');
            });

            general_option('pensioner_check', 'pensioner_div','pensioner_id');
            $("#pensioner_check").click(function () {
                general_option('pensioner_check', 'pensioner_div','pensioner_id');
            });

            general_option('bank_check', 'bank_div','bank_id');
            $("#bank_check").click(function () {
                general_option('bank_check', 'bank_div','bank_id');
            });

            general_option('member_type_check', 'member_type_div','member_type_id');
            $("#member_type_check").click(function () {
                general_option('member_type_check', 'member_type_div','member_type_id');
            });

            general_option('general_type_check', 'general_type_div','general_type_id');
            $("#general_type_check").click(function () {
                general_option('general_type_check', 'general_type_div','general_type_id');
            });


            $("#none_check").click(function () {
                none_option();
            })


            exportPdfOption();
            $("#dependent_id").change(function(){
//code here
                exportPdfOption();
            });


            $("#pensioner_id").change(function(){
//code here
                exportPdfOption();
            });

        });

        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
                $( "#" +id).val(0).change();
            }
        }

        // none option
        function none_option() {
            $( "#bank_check").prop( "checked", false );
            $("#bank_div" ).hide();
            $( "#bank_id").prop( "disabled", true );
            $("#member_type_div" ).hide();
            $( "#member_type_id").prop( "disabled", true );

        }

        function exportPdfOption()
        {
            var dependent_id = element_id_value('dependent_id');
            var pensioner_id = element_id_value('pensioner_id');
            if((dependent_id != null && dependent_id != '') || (pensioner_id != null && pensioner_id != ''))
            {
                $('#export_button_pdf').prop('hidden', false)
            }else{
                $('#export_button_pdf').prop('hidden', true)
            }
        }


        load_dependents();
        //             dependents
        function load_dependents() {
            $(".dependent-select").select2({
                minimumInputLength: 1,
                multiple: false,
                ajax: {
                           url:  '{!! route('backend.compliance.dependent.get_for_select') !!}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.dependent,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:selecting", function(e) {
                var $selected = e.params.args.data.id;
                /* console.log("select2:select", e); */
                $('input[name=search]').val($selected);
            });
        }


        //             pensioners
        load_pensioners();
        function load_pensioners() {
            $(".pensioner-select").select2({
                minimumInputLength: 1,
                multiple: false,
                ajax: {
                    url:  '{!! route('backend.payroll.pensioner.get_for_select') !!}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.pensioner,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:selecting", function(e) {
                var $selected = e.params.args.data.id;
                /* console.log("select2:select", e); */
                $('input[name=search]').val($selected);
            });
        }


    </script>


@endpush
