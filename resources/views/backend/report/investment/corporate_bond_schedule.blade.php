@extends('layouts.backend.main', ['title' => trans('labels.backend.report.c_schedule'), 'header_title' => trans('labels.backend.report.c_schedule')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')
 {!! Form::open(['route' => ['backend.investment.report.schedule.corporate_bond'],
  'method'=>'get',
  'name' => 'report']) !!}
   <div class="row">
   <div class="col-md-12">
   <div class="col-md-3">
   <input type="number" class="form-control" name="tenure" min="1">
   </div>
   <div class="col-md-3">
   <button class="btn btn-success ">Search</button> 
   </div>
   </div>
   </div>
   <br>
   {!! Form::close() !!}

    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
@endpush
