@extends('layouts.backend.main', ['title' => trans('labels.backend.report.f_schedule'), 'header_title' => trans('labels.backend.report.f_schedule')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
{{--     {!! Form::open(['route' => ['backend.investment.report.schedule.fixed_deposits'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!} --}}
    {{--DATE RANGE--}}
    {{-- @include('backend.report.includes.date_range') --}}

    {{-- {!! Form::close() !!} --}}

    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}


@endpush
