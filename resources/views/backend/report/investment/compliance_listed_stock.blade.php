@extends('layouts.backend.main', ['title' => trans('labels.backend.report.stock'), 'header_title' => trans('labels.backend.report.stock')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.investment.report.compliance_listed_stock'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')

    {!! Form::close() !!}
    {{--<div class = "col-md-12">--}}
        {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=>trans('labels.backend.finance.receipt.search_for_name') ]) !!}--}}
    {{--</div>--}}
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}

@stack('date-range-script-end')

<script>
    {{--Input to search full name on first column--}}
    function searchname() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("name");
        filter = input.value.toUpperCase();
        table = document.getElementById("dataTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>


@endpush
