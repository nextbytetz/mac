@extends('layouts.backend.main', ['title' => trans('labels.backend.report.notification_report_initial_expense'), 'header_title' => trans('labels.backend.report.notification_report_initial_expense')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.notification_report.initial_expense'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , 1, []) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')


    <div class = "col-md-12">
        <div class="row">

            {{--Search inputs--}}
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        {{--search member type--}}
                        <div class = "col-md-3" id="member_type_div">
                            {!! Form::select('member_type_id', $member_types, $request['member_type_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_member_type'),'class' => 'form-control search-select', 'id'=> 'member_type_id','hidden'=>true]) !!}
                        </div>


                        {{--search employer--}}
                        <div class = "col-md-3" id="employer_div">
                            {!! Form::select('employer_id', $employers, $request['employer_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_employer'),'class' => 'form-control employer-select', 'id'=> 'employer_id','hidden'=>true]) !!}
                        </div>


                        {{--search insurance--}}
                        <div class = "col-md-3" id="insurance_div">
                            {!! Form::select('insurance_id', $insurances, $request['insurance_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_insurance'),'class' => 'form-control search-select', 'id'=> 'insurance_id','hidden'=>true]) !!}
                        </div>


                        {{--search type--}}
                        <div class = "col-md-3" id="type_div">

                            {!! Form::select('type' , ['0' => trans('labels.backend.claim.paid_through_insurance'), '1' =>trans('labels.backend.finance.receipt.cash') ],$request['type'] ,  ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_payment_method'),'class' => 'form-control search-select', 'id'=> 'type_id', 'hidden'=>true]) !!}
                        </div>


                        {{--end inputs--------------------}}

                    </div>
                </div>
            </div>

            </br>
            {{--Search checkbox options--}}
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        {{--search options--}}
                        <div class = "col-md-2">
                            <label>@lang('labels.general.search_options'):</label>
                        </div>


                        {{--member type option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'member_type_check',1, ($request['member_type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'member_type_check' ]) !!}@lang('labels.backend.claim.member_type')</label>
                        </div>

                        {{--employer option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'employer_check',1, ($request['employer_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'employer_check' ]) !!}@lang('labels.general.employer')</label>
                        </div>


                        {{--insurance option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'insurance_check',1, ($request['insurance_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'insurance_check' ]) !!}@lang('labels.general.insurance')</label>
                        </div>

                        {{--type => payament method option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'type_check',1, ($request['type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'type_check' ]) !!}@lang('labels.general.payment_method')</label>
                        </div>


                        {{--None option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_radio' ]) !!}@lang('labels.general.none')</label>
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>


    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    @if(isset($dataTable))
        {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
    @endif
@stop

@push('after-script-end')
    @if(isset($dataTable))
        {!! $dataTable->scripts() !!}
    @endif
    @stack('date-range-script-end')

    <script>

        // main function search options
        $(function () {
            // load_employers();
            $(".search-select").select2();
            general_option('member_type_check', 'member_type_div','member_type_id');
            general_option('type_check', 'type_div','type_id');
            general_option('insurance_check', 'insurance_div','insurance_id');
            general_option('employer_check', 'employer_div','employer_id');
//        hide_inputs();
//member
            $("#member_type_check").click(function () {
                general_option('member_type_check', 'member_type_div','member_type_id');
            });

//type
            $("#type_check").click(function () {
                general_option('type_check', 'type_div','type_id');
            });
//insurance
            $("#insurance_check").click(function () {
                general_option('insurance_check', 'insurance_div','insurance_id');
            });
//employer
            $("#employer_check").click(function () {
                general_option('employer_check', 'employer_div','employer_id');
            });
            $("#none_radio").click(function () {
                none_option();
            })

        });


        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_radio" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {
            $( "#member_type_check").prop( "checked", false );
            $("#member_type_div" ).hide();
            $( "#member_type_id").prop( "disabled", true );

            $( "#type_check").prop( "checked", false );
            $("#type_div" ).hide();
            $( "#type_id").prop( "disabled", true );

            $( "#insurance_check").prop( "checked", false );
            $("#insurance_div" ).hide();
            $( "#insurance_id").prop( "disabled", true );

            $( "#employer_check").prop( "checked", false );
            $("#employer_div" ).hide();
            $( "#employer_id").prop( "disabled", true );
        }




        //            Hide search inputs
        function load_employers() {
            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        }

    </script>


@endpush
