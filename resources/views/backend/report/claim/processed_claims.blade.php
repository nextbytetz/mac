@extends('layouts.backend.main', ['title' => trans('labels.backend.report.processed_claims'), 'header_title' => trans('labels.backend.report.processed_claims')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.notification_report.processed'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')


    <div class = "col-md-12">
        <div class="row">

            {{--Search inputs--}}
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">

                        {{--search incident type--}}
                        <div class = "col-md-3" id="incident_type_div">
                            {!! Form::select('incident_type_id', $incident_types, $request['incident_type_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_incident_type'),'class' => 'form-control search-select', 'id'=> 'incident_type_id']) !!}
                        </div>

                        {{--search region--}}
                        <div class = "col-md-3" id="region_div">
                            {!! Form::select('region_id', $regions, $request['region_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_region'),'class' => 'form-control search-select', 'id'=> 'region_id']) !!}
                        </div>


                        {{--search business--}}
                        <div class = "col-md-3" id="business_div">
                            {!! Form::select('business_id', $businesses, $request['business_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_business'),'class' => 'form-control search-select', 'id'=> 'business_id']) !!}
                        </div>


                        {{--search employer--}}
                        <div class = "col-md-3" id="employer_div">
                            {!! Form::select('employer_id', $employers, $request['employer_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_employer'),'class' => 'form-control employer-select', 'id'=> 'employer_id']) !!}
                        </div>


                        {{--end inputs--------------------}}

                    </div>
                </div>
            </div>

            </br>
            {{--Search checkbox options--}}
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        {{--search options--}}
                        <div class = "col-md-2">
                            <label>@lang('labels.general.search_options'):</label>
                        </div>

                        {{--incident option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'incident_type_check', 1,($request['incident_type_check'] == 1) ? true : false , ['style' => 'width:20px','id' => 'incident_type_check' ]) !!}@lang('labels.backend.table.incident_type')</label>
                        </div>

                        {{--region option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'region_check',1, ($request['region_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'region_check' ]) !!}@lang('labels.general.region')</label>
                        </div>

                        {{--business option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'business_check',1, ($request['business_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'business_check' ]) !!}@lang('labels.general.business')</label>
                        </div>


                        {{--employer option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'employer_check',1, ($request['employer_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'employer_check' ]) !!}@lang('labels.general.employer')</label>
                        </div>

                        {{--None option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_radio' ]) !!}@lang('labels.general.none')</label>
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>


    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}

@stack('date-range-script-end')

<script>

    // main function search options
    $(function () {
        load_employers();
        $(".search-select").select2();
        general_option('incident_type_check', 'incident_type_div','incident_type_id');
              general_option('region_check', 'region_div','region_id');
        general_option('business_check', 'business_div','business_id');
        general_option('employer_check', 'employer_div','employer_id');
//        hide_inputs();
//incident
        $("#incident_type_check").click(function () {
            general_option('incident_type_check', 'incident_type_div','incident_type_id');
        });

//region
        $("#region_check").click(function () {
            general_option('region_check', 'region_div','region_id');
        });
//business
        $("#business_check").click(function () {
            general_option('business_check', 'business_div','business_id');
        });
//employer
        $("#employer_check").click(function () {
            general_option('employer_check', 'employer_div','employer_id');
        });
        $("#none_radio").click(function () {
            none_option();
        })



        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });



    });


    //SEARCH OPTIONS-------------------------
    function general_option(check,div, id) {
        if ($("#" + check).is(":checked")) {
            $("#" + div).show();
            $( "#" +id).prop( "disabled", false );
            $( "#none_radio" ).prop( "checked", false );

        }else {
            $("#" + div).hide();
            $( "#" +id).prop( "disabled", true );
        }
    }

    // none option
    function none_option() {
        $( "#incident_type_check").prop( "checked", false );
        $("#incident_type_div" ).hide();
        $( "#incident_type_id").prop( "disabled", true );

        $( "#region_check").prop( "checked", false );
        $("#region_div" ).hide();
        $( "#region_id").prop( "disabled", true );

        $( "#business_check").prop( "checked", false );
        $("#business_div" ).hide();
        $( "#business_id").prop( "disabled", true );

        $( "#employer_check").prop( "checked", false );
        $("#employer_div" ).hide();
        $( "#employer_id").prop( "disabled", true );
    }




    //            Hide search inputs
    function load_employers() {
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
    }
</script>


@endpush
