@extends('layouts.backend.main', ['title' => 'Both Investigated (planned and unplanned)', 'header_title' => 'Both Investigated (planned and unplanned)'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }
    .drop{
        margin-top: 20px;
    }

</style>

@endpush

@section('content')
 <div class="custom_filter">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                {!! Form::open(['role' => 'form', 'id' => 'fin-year-search','route' => ['backend.operation.report.investigated_report_unwritten',105],'method'=>'get']) !!}
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="fin_year">Financial year</label>
                        {!! Form::select('fin_y', $fin_years, null, ['placeholder'=>'Select financial year','class' => 'form-control search-fin_year', 'id' => 'fin_y']) !!}
                    </div>
                    <div class="form-group col-md-2">
                        <label for="fin_year">Employer</label>
                        {!! Form::select('employer-name', $employer_name, null, ['placeholder'=>'','class' => 'form-control search-employer-name', 'id' => 'employer-name']) !!}
                    </div>
                    <div class="form-group col-md-1">
                        <input type="submit" class="btn btn-success btn-sm btn-submit drop" value="@lang('buttons.general.search')" />
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {!! $dataTable->table(['class' => 'display', 'class' => 'table-responsive', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

@stop

@push('after-script-end')


@stack('date-range-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{-- seach select js --}}
<script type="text/javascript">
    $(document).ready(function(){
        $(".search-fin_year").select2({
            allowClear: true,
            debug: true,
            placeholder: "--Select financial year--"
        });
        $(".search-employer-name").select2({
            allowClear: true,
            debug: true,
            placeholder: "--Select employer name--"
        });
    })
    
</script>


@endpush