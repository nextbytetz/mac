@extends('layouts.backend.main', ['title' => 'Report on Approved Investigation Plan', 'header_title' => 'Report on Approved Investigation Plan'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }
    .drop{
        margin-top: 20px;
    }

</style>

@endpush

@section('content')
<div class="custom_filter">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            {!! Form::open(['role' => 'form', 'id' => 'select-search','route' => ['backend.operation.report.investigated_report_unwritten',105],'method'=>'get']) !!}
            <div class="form-row">
                <<div class="form-group col-md-4 allocation_select">
                    <label for="fin_year">Plan Number:</label>
                    {!! Form::select('plane-name', $plan_name, null, ['placeholder'=>'','class' => 'form-control search-plane-name', 'id' => 'plane-name']) !!}
                </div>
                <div class="form-group col-md-4 allocation_select">
                    <label for="fin_year">Employer:</label>
                    {!! Form::select('employer-name', $employer, null, ['placeholder'=>'','class' => 'form-control search-employer-name', 'id' => 'employer-name']) !!}
                </div>
                <div class="form-group col-md-1">
                    <input type="submit" class="btn btn-success btn-sm btn-submit drop" value="@lang('buttons.general.search')" />
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'class' => 'table-responsive', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

@stop

@push('after-script-end')


@stack('date-range-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{-- seach select js --}}
<script type="text/javascript">
    $(document).ready(function(){
        $(".search-plane-name").select2({
            allowClear: true,
            debug: true,
            placeholder: "--Select plane name--"
        });
          $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: " --Select employer name--",
            ajax: {
               url: "{!! route('backend.compliance.employers') !!}",
               dataType: 'json',
               delay: 250,
               data: function (params) {
                return {
                    q: params.term || "",
                    page: params.page || 1
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data.items, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    }),
                    pagination: {
                        more: true
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    }).on("select2:selecting", function($e) {
        let $selected = $e.params.args.data.id;
    });
        $( "#clear_filter" ).click(function() {
        $(".employer-select").val(null).trigger('change.select2');
        $(".search-select").val(null).trigger('change.select2');
    });
    $(".search-plane-name").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "Select plane name",
            ajax: {
               url: "{!! route('backend.claim.investigations.index') !!}",
               dataType: 'json',
               delay: 250,
               data: function (params) {
                return {
                    q: params.term || "",
                    page: params.page || 1
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data.items, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    }),
                    pagination: {
                        more: true
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    }).on("select2:selecting", function($e) {
        let $selected = $e.params.args.data.id;
    });
    $( "#clear_filter" ).click(function() {
        $(".search-plane-name").val(null).trigger('change.select2');
        $(".search-select").val(null).trigger('change.select2');
    });
    })
    
</script>



@endpush


