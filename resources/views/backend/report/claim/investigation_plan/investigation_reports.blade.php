@extends('layouts.backend.main', ['title' => 'General Investigation Plan Reports', 'header_title' => 'General Investigation Plan Reports'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }
    .drop{
        margin-top: 20px;
    }
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

</style>

@endpush

@section('content')

{{--Custom filter--}}
<div class="custom_filter">
    {!! Form::open(['role' => 'form', 'id' => 'search-form', 'method'=>'GET']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="form-row">
                {{--Incident Type--}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="incident">Incident Type</label>
                    {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident']) !!}
                </div>
                {{--Employee Name--}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="employee">Employee Name</label>
                    {!! Form::select('employee', [], null, ['class' => 'form-control employee-select', 'id' => 'employee']) !!}
                </div>
                {{--Employer Name--}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="employer">Employer Name</label>
                    {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                </div>
                {{--Region Selection--}}
                <div class="form-group col-md-4">
                    <label for="region">Incident Region:</label>
                    {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                </div>
                {{--District Selection--}}
                <div class="form-group col-md-4">
                    <label for="region">Incident District:</label>
                    {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                </div>        
                {{--Notification Stages--}}
                <div class="form-group col-md-4">
                    <label for="plan_status">Plans</label>
                    {!! Form::select('plan_status', ['1' => 'Planned', '2' => 'Un Planned'], null, ['class' => 'form-control search-select', 'id' => 'plan_status', 'placeholder' => '']) !!}
                </div>
                {{--Notification Stages--}}
                <div class="form-group col-md-4">
                    <label for="stage">Reports</label>
                    {!! Form::select('report', ['0' => 'All', '1' => 'Written', '2' => 'Un written'], null, ['class' => 'form-control search-select', 'id' => 'report_id', 'placeholder' => '']) !!}
                </div>
                <div class="form-group col-md-4">
                    <label for="questionnaire">Investigation Questionnaire</label>
                    {!! Form::select('questionnaire', ['0' => 'All','1' => 'Filled', '2' => 'Not Filled'], null, ['class' => 'form-control search-select', 'id' => 'questionnaire', 'placeholder' => '']) !!}
                </div>
                {{--Investigation Status--}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="investigation_status">Investigation Status:</label>
                    {!! Form::select('investigation_status', ['0' => 'All','1' => 'Fully Investigated', '2'=> 'Partially Investigated', '3' => 'Not Investigated', ], null, ['class' => 'form-control search-select', 'id' => 'investigation_status']) !!}
                </div> 
                {{--Resource Status--}}
                {{-- <div class="form-group col-md-4 allocation_select">
                    <label for="status">Status:</label>
                    {!! Form::select('status', ['0' => 'All', '1' => 'Allocated', '2' => 'Unallocated', '3' => 'Allocated to User'], null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                </div>    --}}
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <button type="button" class="btn btn-secondary site-btn" id="clear_filter">Clear</button>
                    <button type="submit" class="btn btn-success btn-sm btn-submit">@lang('buttons.general.search')</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<legend></legend>
<br/>
<div class="table-responsive">
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%',
    'id'=>'dataTable'],true) !!}
</div>

@stop
@push('after-script-end')


@stack('date-range-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script type="text/javascript">
    $(document).ready(function(){
        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });
        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district').empty();
                    $("#district").select2("val", "");
                    $('#district').html(data);
                    $("#spin2").hide();
                });
            }
        });
        $("#status").on("change", function () {
            var $status = $(this).val();
            switch($status) {
                case '3':
                /* Selected to choose User Allocated */
                $(".allocated_user_select").show();
                break;
                default:
                $(".allocated_user_select").hide();
                break;
            }
        });
        $('#status').trigger('change');
        let $assign_status_contrl = $('#assign_status');
        $assign_status_contrl.on("change", function () {
            let $assign_status = $(this).val();
            switch($assign_status) {
                case '1':
                $(".assign_user_select").show();
                break;
                default:
                $(".assign_user_select").hide();
                break;
            }
        });
        $assign_status_contrl.trigger('change');
        let $allocation_category_contrl = $('#category');
        $allocation_category_contrl.on("change", function () {
            let $allocation_category = $(this).val();
            let $assign_for_select = $(".assign_for_select");
            switch($allocation_category) {
                case '1':
                $assign_for_select.show();
                $assign_for_select.closest('div').removeClass("col-md-2").addClass('col-md-2');
                $assign_status_contrl.closest('div').removeClass("offset-md-6").addClass('offset-md-4');
                break;
                default:
                $assign_for_select.hide();
                $assign_for_select.closest('div').removeClass("col-md-2");
                $assign_status_contrl.closest('div').removeClass("offset-md-4").addClass('offset-md-6');
                break;
            }
        });
        $allocation_category_contrl.trigger('change');
        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employees') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employee */
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $(".search-select").val(null).trigger('change.select2');

        });

        $('#search-form').on('submit', function($e) {
            $oTable.draw();
            $e.preventDefault();
        });
    });



</script>
@endpush

