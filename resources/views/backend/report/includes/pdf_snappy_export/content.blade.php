<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <base target="_blank">
    <meta charset="utf-8" />
    <title>{{ __('label.report') }}</title>
    {{--<meta name="viewport" content="width=device-width">--}}
    <meta name="author" content="Martin Luhanjo <m.luhanjo@nextbyte.co.tz>"/>
    <meta name="date" content="2020-10-04"/>
    {{ Html::style(url('css/nextbyte/report/pdf_snappy_print_out.css')) }}
</head>
<body>

<div id="content">
    <div class="center">
        {{--<img id="tanzania" src="{{ asset_url() . "/nextbyte/img/national_emblem.jpg" }}">--}}
        <div class="row">

            &nbsp;&nbsp;  &nbsp; <label>{{ strtoupper(__('label.title')) . ' : ' }} </label>   <label class="" style="color:#494949">{{ strtoupper($resource_data['report_title']) }}</label>

        </div>
        <br/>
        <table class="table table-striped table-bordered" id="">
            <thead>
            <tr style="background-color: lightgrey; color: #636363;">
                <th>{{ __('label.sn')}}</th>
                @foreach($resource_data['column_headers'] as $column_header)
                    <th>{{ $column_header }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            @foreach($content_data as $data)
                <tr>
                    @php
                        $key_index = 0;

                    @endphp
                    <td style="text-align:center;">{{ $sn++ }}</td>
                    @foreach($data as $key  => $value)
                        <td style=" {{ ( is_numeric($value) && check_if_is_non_number_column( $resource_data['column_headers'][$key_index]) ==false) ? 'text-align: right;' : 'text-align: left;'  }} ">{{ (is_numeric($value) && check_if_is_non_number_column( $resource_data['column_headers'][$key_index]) ==false) ? number_2_format($value) : $value }}</td>

                        @php
                         $key_index = $key_index + 1;
                        @endphp
                    @endforeach
                </tr>
            @endforeach


            </tbody>
        </table>


    </div>
</div>

<script type="text/javascript">


</script>
</body>
</html>