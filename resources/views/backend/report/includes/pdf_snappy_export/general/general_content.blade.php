<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <base target="_blank">
    <meta charset="utf-8" />
    <title>{{ __('label.report') }}</title>
    {{--<meta name="viewport" content="width=device-width">--}}
    <meta name="author" content="Martin Luhanjo <m.luhanjo@nextbyte.co.tz>"/>
    <meta name="date" content="2020-10-04"/>

    {{ Html::style(asset_url() . "/nextbyte/css/backend/report/pdf_snappy_print_out.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
</head>
<body>

<div id="content">
    <div class="center">
        {{--<img id="tanzania" src="{{ asset_url() . "/nextbyte/img/national_emblem.jpg" }}">--}}
        <div class="row">

            &nbsp;&nbsp;  &nbsp; <label>{{ strtoupper('TITLE') . ' : ' }} </label>   <label class="" style="color:#494949">{{ strtoupper($resource_data['report_title']) }}</label>

        </div>
        <br/>

        <section class="row card">


            <div class="card-body" id="main-content">

                @include($content_include)

            </div>

        </section>

    </div>
</div>

<script type="text/javascript">


</script>
</body>
</html>