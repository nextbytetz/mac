@extends('layouts.backend.main', ['title' => 'SLA List', 'header_title' => 'SLA List'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12">
            <div class="alert-left-border">
                <div class="alert alert-primary alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Note.</strong> Service Level Agreement (SLA) list has the files delayed to be processed more than standard accepted number of processing days.
                </div>
            </div>
        </div>
    </div>

    <div class = "row">
        <div class="col-md-12" >
            <div><span style="font-weight: bold;font-size: 16px;">SLA</span>&nbsp;&nbsp;<a href="{!! route('backend.report.sla.index') !!}"  class="btn btn-xs btn-secondary" ><i class="icon fa fa-refresh"></i>&nbsp;Refresh Data</a></div>

            <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
                <tbody class="tbody_per_region table_display_block" >

                <tr style="color: white; background-color: #495a86" >
                    <th>sn</th>
                    <th>Description</th>
                    <th>Days Exceeded</th>
                </tr>

                @foreach($slas as $sla)
                    @php
                        $countsla = (new \App\Repositories\Backend\Reporting\SlaAlertRepository())->getSummaryCount($sla);
                    @endphp
                    @if ($countsla)
                        <tr>
                            <td style="width: 5px;">{{ $loop->iteration }}</td>
                            <td style="">
                                <a href="{{ route("backend.report.sla", $sla->id) }}">{{ $sla->description }}&nbsp;&nbsp;</a>
                                <span class="badge-summary badge-summary-alert">{{ $countsla }}</span>
                            </td>
                            <td style="">{{ $sla->days_accumulative }}</td>
                        </tr>
                    @endif
                @endforeach

                </tbody>
            </table>

        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush