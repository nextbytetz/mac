@extends('layouts.backend.main', ['title' => 'SLA', 'header_title' => 'SLA'])

@push('after-styles-end')

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <legend>SLA Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $sla->description }}</small></span></legend>

    <!--Include Report-->
    @include('backend/report/configurable/includes/content', [
                                                                'cr' => $sla->cr,
                                                                'cr_params' => [
                                                                    'hasfilter' => 1,
                                                                    'shouldrefresh' => 0,
                                                                    'canbuild' => 1,
                                                                    'buttons' => 1,
                                                                    'query_filter' => $query_filter,
                                                                ],
                                                            ])

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    <script  type="text/javascript">
        $(function() {

        });
    </script>
@endpush