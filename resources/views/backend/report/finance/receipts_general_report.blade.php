@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.header.receipt.retrieve.receipts'), 'header_title' => "Contribution General Report"])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.finance.report.receipt_general_report'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')

    <div class="row">

        <div class = "col-md-12">
            <div class="row">
                {{--search for bank name--}}
                <div class = "col-md-3" id="bank_div">
                    {!! Form::select('bank_id', $banks, null, ['style' => 'width:100%', 'placeholder' => trans('labels.backend.finance.receipt.search_for_bank_name'),'class' => 'form-control search-select', 'id'=> 'bank_id', 'hidden'=>true]) !!}
                </div>
                {{--search payment type--}}
                <div class = "col-md-3" id="payment_type_div">
                    {!! Form::select('payment_type_id', $payment_types, null, ['style' => 'width:100%', 'placeholder' => trans('labels.backend.finance.receipt.search_for_payment_type'),'class' => 'form-control search-select', 'id'=> 'payment_type_id', 'hidden'=>true]) !!}

                </div>

                {{--search region--}}
                <div class = "col-md-3" id="region_div">
                    {!! Form::select('region_id', $regions, $request['region_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_region'),'class' => 'form-control search-select', 'id'=> 'region_id','hidden'=>true]) !!}
                </div>

                {{--search finance codes--}}
                {{--<div class = "col-md-3" id="fin_code_div">
                    {!! Form::select('fin_code_id', $fin_codes, $request['fin_code_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_fin_code'),'class' => 'form-control search-select', 'id'=> 'fin_code_id','hidden'=>true]) !!}
                </div>--}}

                {{--search for status (Pending + Approved)--}}
                <div class = "col-md-3" id="receipt_status_div">
                    {!! Form::select('status_id', ['0' => 'Cancelled', '1' => 'Dishonoured', '2' => 'Pending', '3' => 'Approved'], $request['receipt_status'], ['style' => 'width:100%', 'placeholder' => "Search for Contribution Status",'class' => 'form-control search-select', 'id'=> 'receipt_status','hidden'=>true]) !!}
                </div>

            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>
                {{--bank name option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'bank_check',1, ($request['bank_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'bank_check']) !!}@lang('labels.backend.table.bank')</label>
                </div>
                {{--payment type option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'payment_type_check',1, ($request['payment_type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'payment_type_check' ]) !!}@lang('labels.backend.finance.receipt.payment_type')</label>
                </div>

                {{--region option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'region_check',1, ($request['region_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'region_check' ]) !!}@lang('labels.general.region')</label>
                </div>


                {{--fin code option--}}
                {{--<div class = "col-md-2">
                    <label>{!! Form::checkbox( 'fin_code_check',1, ($request['fin_code_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'fin_code_check' ]) !!}@lang('labels.backend.finance.fin_code')</label>
                </div>--}}

                {{--receipt status option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'receipt_status_check',1, ($request['status_id'] == 1) ? true : false, ['style' => 'width:20px','id' => 'receipt_status_check' ]) !!} Contribution Status </label>
                </div>


                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>

    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>@lang('labels.backend.table.total_amount') </th>
            <th>{!! $total_amount !!}</th>
        </tr>
        </thead>
    </table>

@stop

@push('after-script-end')


@stack('date-range-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script type="text/javascript">

    // main function search options
    $(function () {
        $(".search-select").select2();

        general_option('region_check', 'region_div','region_id');
        general_option('bank_check', 'bank_div','bank_id');
        general_option('payment_type_check', 'payment_type_div','payment_type_id');
        /*general_option('fin_code_check', 'fin_code_div','fin_code_id');*/
        general_option('receipt_status_check', 'receipt_status_div','receipt_status');
        $("#payment_type_check").click(function () {
            general_option('payment_type_check', 'payment_type_div','payment_type_id');

        });

        $("#bank_check").click(function () {
            general_option('bank_check', 'bank_div','bank_id');

        });


        $("#region_check").click(function () {
            general_option('region_check', 'region_div','region_id');

        });

/*        $("#fin_code_check").click(function () {
            general_option('fin_code_check', 'fin_code_div','fin_code_id');

        });*/

        $("#receipt_status_check").click(function () {
            general_option('receipt_status_check', 'receipt_status_div','receipt_status');

        });


        $("#none_check").click(function () {
            none_option();
        })

    });


    //SEARCH OPTIONS-------------------------
    //SEARCH OPTIONS-------------------------
    function general_option(check,div, id) {
        if ($("#" + check).is(":checked")) {
            $("#" + div).show();
            $( "#" +id).prop( "disabled", false );
            $( "#none_check" ).prop( "checked", false );

        }else {
            $("#" + div).hide();
            $( "#" +id).prop( "disabled", true );
        }
    }

    // none option
    function none_option() {
        $( "#bank_check").prop( "checked", false );
        $("#bank_div" ).hide();
        $( "#bank_id").prop( "disabled", true );

        $( "#payment_type_check").prop( "checked", false );
        $("#payment_type_div" ).hide();
        $( "#payment_type_id").prop( "disabled", true );

        $( "#region_check").prop( "checked", false );
        $("#region_div" ).hide();
        $( "#region_id").prop( "disabled", true );

/*        $( "#fin_code_check").prop( "checked", false );
        $("#fin_code_div" ).hide();
        $( "#fin_code_id").prop( "disabled", true );*/

        $( "#receipt_status_check").prop( "checked", false );
        $("#receipt_status_div" ).hide();
        $( "#receipt_status_id").prop( "disabled", true );

    }

</script>


@endpush

