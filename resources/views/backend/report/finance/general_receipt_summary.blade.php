@extends('layouts.backend.main', ['title' => trans('labels.backend.report.general_receipt_summary'), 'header_title' => trans('labels.backend.report.general_receipt_summary')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}


@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.finance.report.receipt.general_receipt_summary'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')

    <div class = "col-md-12">

        <div class="row">
            {{--search for bank name--}}
            <div class="col-md-1">
                <label>@lang('labels.backend.table.bank')</label>
            </div>
            <div class = "col-md-3" id="bank_div">
                {!! Form::select('bank_id', $banks, isset($request['bank_id']) ? $request['bank_id'] : null, ['style' => 'width:100%', 'placeholder' => trans('labels.general.all'),'class' => 'form-control search-select', 'id'=> 'bank_id']) !!}
            </div>
        </div>
    </div>

    <div>&nbsp;</div>



    <div class="contain-inner dashboard-v3">
        <div class="boxes-section-v3">
            <div class="row">
                {{--AMOUNT COLLECTED--}}
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12 boxes-xl">
                    <div class="box-dahsboard-v3">
                        <div class="title-box text-success">@lang('labels.backend.table.amount_collected')</div>
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <span class="total-user">{!! Form::label( '', $amount_collected) !!}</span>
                                {{--<span class="top-arrow"><i class="fa fa-level-up text-primary"></i></span>--}}
                            </div>
                        </div>
                    </div>
                </div>

                {{--NO OF RECEIPTS--}}
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12 boxes-xl">
                    <div class="box-dahsboard-v3">
                        <div class="title-box text-primary">@lang('labels.backend.table.receipts')</div>
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <span class="total-user">{!! Form::label( '0', $receipts_count) !!}</span>
                                {{--<span class="top-arrow"><i class="fa fa-level-up text-primary"></i></span>--}}
                            </div>

                        </div>
                    </div>
                </div>

                {{--CANCELLED RECEIPTS--}}
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12 boxes-xl">
                    <div class="box-dahsboard-v3">
                        <div class="title-box text-danger">@lang('labels.backend.finance.report.cancelled_receipts')</div>
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <span class="total-user">{!! Form::label( '0', $cancelled_receipts_count) !!}</span>
                                {{--<span class="top-arrow"><i class="fa fa-level-up text-primary"></i></span>--}}
                            </div>
                        </div>
                    </div>
                </div>

                {{--DISHOONOURED CHEQUES--}}
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12 boxes-xl">
                    <div class="box-dahsboard-v3">
                        <div class="title-box text-info">@lang('labels.backend.table.dishonoured_cheques')</div>
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <span class="total-user">{!! Form::label( '0', $dishonoured_cheques) !!}</span>
                                {{--<span class="top-arrow"><i class="fa fa-level-up text-primary"></i></span>--}}
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>



    {!! Form::close() !!}


@stop

@push('after-script-end')

@stack('date-range-script-end')


@endpush
