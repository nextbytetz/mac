@extends('layouts.backend.main', ['title' => trans('labels.backend.finance.payment_vouchers'), 'header_title' => trans('labels.backend.finance.payment_vouchers')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.finance.report.payment_vouchers'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}


    <div class = "col-md-12">
        <div class="row">

            {{--Search inputs--}}
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        {{--search status--}}
                        <div class = "col-md-3" id="status_div">

                            {!! Form::select('status_id' , ['0' => 'Pending', '1' => 'Paid'],$request['status_id'] ,  ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_status'),'class' => 'form-control search-select', 'id'=> 'status_id','hidden'=>true]) !!}
                        </div>

                        {{--search member type--}}
                        <div class = "col-md-3" id="member_type_div">
                            {!! Form::select('member_type_id', $member_types, $request['member_type_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_member_type'),'class' => 'form-control search-select', 'id'=> 'member_type_id','hidden'=>true]) !!}
                        </div>

                        {{--RESOURCE INPUT-===============================-}}
                        {{--search employee--}}
                        <div class = "col-md-3" id="employee_div">
                            {!! Form::select('employee_id', [], $request['employee_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_employee'),'class' => 'form-control employee-select', 'id'=> 'employee_id', 'hidden'=>true]) !!}
                        </div>

                        {{--search employer--}}
                        <div class = "col-md-3" id="employer_div">
                            {!! Form::select('employer_id', [], $request['employer_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_employer'),'class' => 'form-control employer-select', 'id'=> 'employer_id','hidden'=>true]) !!}
                        </div>

                        {{--search insurance--}}
                        <div class = "col-md-3" id="insurance_div">
                            {!! Form::select('insurance_id', $insurances, $request['insurance_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_insurance'),'class' => 'form-control search-select', 'id'=> 'insurance_id','hidden'=>true]) !!}
                        </div>

                        {{--search dependents--}}
                        <div class = "col-md-3" id="dependent_div">
                            {!! Form::select('dependent_id', [], $request['dependent_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_dependent'),'class' => 'form-control dependent-select', 'id'=> 'dependent_id','hidden'=>true]) !!}
                        </div>

                        {{--search pensioners--}}
                        <div class = "col-md-3" id="pensioner_div">
                            {!! Form::select('pensioner_id', [], $request['pensioner_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_pensioner'),'class' => 'form-control pensioner-select', 'id'=> 'pensioner_id','hidden'=>true]) !!}
                        </div>

                        <div class="element-form"  >
                            {{--<div class="col-md-2">--}}
                                <div class="pull-right">
                                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                                </div>
                            {{--</div>--}}
                        </div>

                        {{--End resource input----------}}


                        {{--end inputs--------------------}}

                    </div>


                </div>
            </div>

            </br>
            {{--Search checkbox options--}}
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        {{--search options--}}
                        <div class = "col-md-2">
                            <label>@lang('labels.general.search_options'):</label>
                        </div>

                        {{--status option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'status_check',1 , ($request['status_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'status_check' ]) !!}@lang('labels.general.status')</label>
                        </div>


                        {{--member type option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'member_type_check',1, ($request['member_type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'member_type_check' ]) !!}@lang('labels.backend.claim.member_type')</label>
                        </div>

                        {{--None option--}}
                        <div class = "col-md-2">
                            <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_radio' ]) !!}@lang('labels.general.none')</label>
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>


    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}


    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! $total_amount !!}</th>

        </tr>
        </thead>
    </table>

@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}

{{--@stack('date-range-script-end')--}}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>

    // main function search options
    $(function () {

        load_dependents();
        load_employers();
        load_pensioners();
        load_employees();

        $(".search-select").select2();

        general_option('status_check', 'status_div','status_id');
        general_option('member_type_check', 'member_type_div','member_type_id');

//member type);
        member_type_option('member_type_id');
        $("#member_type_check").click(function () {
            general_option('member_type_check', 'member_type_div','member_type_id');
            member_type_option('member_type_id');
        });

//status
        $("#status_check").click(function () {
            general_option('status_check', 'status_div','status_id');

        });

        $("#none_radio").click(function () {
            none_option();
            member_type_option('member_type_id');
        });

        $('#member_type_id').on('change', function (e) {
            var member_type_id = e.target.value;
            switch (member_type_id) {
                case '1':
                    hide_show_option('employer_div','employer_id');
                    break;
                case '2':
                    hide_show_option('employee_div','employee_id');
                    break;
                case '3':
                    hide_show_option('insurance_div','insurance_id');
                    break;
                case '4':
                    hide_show_option('dependent_div','dependent_id');
                    break;
                case '5':
                    hide_show_option('pensioner_div','pensioner_id');
                    break;
                default:
                    hide_show_option('none','none');
            }
        });



    });


    //SEARCH OPTIONS-------------------------
    function general_option(check,div, id) {
        if ($("#" + check).is(":checked")) {
            $("#" + div).show();
            $( "#" +id).prop( "disabled", false );
            $( "#none_radio" ).prop( "checked", false );

        }else {
            $("#" + div).hide();
            $( "#" +id).prop( "disabled", true );
        }
    }

    // none option
    function none_option() {

        $( "#status_check").prop( "checked", false );
        $("#status_div" ).hide();
        $( "#status_id").prop( "disabled", true );

        $( "#member_type_check").prop( "checked", false );
        $("#member_type_div" ).hide();
        $( "#member_type_id").prop( "disabled", true );


    }


    function member_type_option(member_type_id) {
        var choice = $("#" + member_type_id).val();
        switch (choice) {
            case '1':
                hide_show_option('employer_div','employer_id');
                break;
            case '2':
                hide_show_option('employee_div','employee_id');
                break;
            case '3':
                hide_show_option('insurance_div','insurance_id');
                break;
            case '4':
                hide_show_option('dependent_div','dependent_id');
                break;
            case '5':
                hide_show_option('pensioner_div','pensioner_id');
                break;
            default:
                hide_show_option('none','none');
        }

        if ($("#" + member_type_id).is(":disabled")) {
            hide_show_option('none','none');

        }

    }



    // hide show option
    function hide_show_option(show_member_type_div,show_member_type_id) {

        $("#employee_div" ).hide();
        $( "#employee_id").prop( "disabled", true );

        $("#employer_div" ).hide();
        $( "#employer_id").prop( "disabled", true );

        $("#insurance_div" ).hide();
        $( "#insurance_id").prop( "disabled", true );

        $("#dependent_div" ).hide();
        $( "#dependent_id").prop( "disabled", true );

        $("#pensioner_div" ).hide();
        $( "#pensioner_id").prop( "disabled", true );

        $("#"+show_member_type_div ).show();
        $( "#"+ show_member_type_id).prop( "disabled", false );
    }




    //             employees
    function load_employees() {
        $(".employee-select").select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                url: "<?php echo route('backend.compliance.beneficiary_employees'); ?>",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
    }

    //             employers
    function load_employers() {
        $(".employer-select").select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                url: "<?php echo route('backend.compliance.beneficiary_employers'); ?>",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
    }

    //             dependents
    function load_dependents() {
        $(".dependent-select").select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                url: "<?php echo route('backend.compliance.gratuity_dependents'); ?>",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.dependent,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
    }


    //             pensioners
    function load_pensioners() {
        $(".pensioner-select").select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                url: "<?php echo route('backend.payroll.pensioners'); ?>",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.pensioner,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
    }

</script>


@endpush
