<?php

use App\Models\Reporting\ConfigurableReport;

$type = 4;
$dir = __DIR__;

$id = '73';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Processed (Paid and Rejected) vs Unprocessed Pending Claims vs Unprocessed Rejected Claims",
        "description" => "Processed (Paid and Rejected) vs Unprocessed Pending Claims vs Unprocessed Rejected Claims",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 1,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '74';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Investigated vs Not Investigated Claims (Unprocessed Pending Claims)",
        "description" => "Investigated vs Not Investigated Claims (Unprocessed Pending Claims)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 2,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '75';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Validated vs Not Validated Claims (Unprocessed Pending Claims Investigated)",
        "description" => "Validated vs Not Validated Claims (Unprocessed Pending Claims Investigated)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 3,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '76';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Acknowledged vs Unacknowledged Claims",
        "description" => "Acknowledged vs Unacknowledged Claims",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 4,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '77';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "ONS Employers Registered Out of Assigned Employers",
        "description" => "ONS Employers Registered Out of Assigned Employers",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 5,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '78';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Timely Approval of ONS Notifications", //Monthly
        "description" => "Timely Approval of ONS Notifications", //Monthly
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);