<?php

use App\Models\Reporting\ConfigurableReport;

$type = 8;
$dir = __DIR__;

$id = '81';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Level 1 Claim Benefit Processed",
        "description" => "Level 1 Claim Benefit Processed",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => NULL,
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '82';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Level 2 Claim Benefit Processed",
        "description" => "Level 2 Claim Benefit Processed",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => NULL,
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '83';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Level 1 Validated Claims",
        "description" => "Level 1 Validated Claims",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => NULL,
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '84';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Level 2 Validated Claims",
        "description" => "Level 2 Validated Claims",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => NULL,
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);