<?php

use App\Models\Reporting\ConfigurableReport;

$type = 1;
$dir = __DIR__;

ConfigurableReport::query()->updateOrCreate(
    ["id" => 1],
    [
        "name" => "Total Notification Received",
        "description" => "Total Notification Received",
        //"data_query" => file_get_contents("{$dir}/cr1.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 1,
        'view' => 'cr1',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 2],
    [
        "name" => "Paid Claims [First Paid Benefit(s)]",
        "description" => "Paid Claims [First Paid Benefit(s)]",
        //"data_query" => file_get_contents("{$dir}/cr2.sql"),
        "filter" => file_get_contents("{$dir}/cr2_filter.json"),
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 2,
        'view' => 'cr2',
        'isbalance' => 1,
        'istable' => 0,
        'display' => '[1,2,5,6,7,8,9,10,11,12,13]',
        'excludes' => '[4,16,18]',
        'aliases' => '{"10": "File Name"}',
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 3],
    [
        "name" => "Approved Rejected Claims",
        "description" => "Approved Rejected Claims",
        //"data_query" => file_get_contents("{$dir}/cr3.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 3,
        'view' => 'cr3',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 4],
    [
        "name" => "Claims Files (DFPI)",
        "description" => "Claims Files (DFPI)",
        //"data_query" => file_get_contents("{$dir}/cr4.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => 'cr4',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 5],
    [
        "name" => "Claims Processing (DO)",
        "description" => "Claims Processing (DO)",
        //"data_query" => file_get_contents("{$dir}/cr5.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 7,
        'view' => 'cr5',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 6],
    [
        "name" => "Claim Assessment (DMAS)",
        "description" => "Claim Assessment (DMAS)",
        //"data_query" => file_get_contents("{$dir}/cr6.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 8,
        'view' => 'cr6',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 7],
    [
        "name" => "Claims Files (DG)",
        "description" => "Claims Files (DG)",
        //"data_query" => file_get_contents("{$dir}/cr7.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 9,
        'view' => 'cr7',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 8],
    [
        "name" => "Validated Files with Complete Benefit Documents",
        "description" => "Validated Files with Complete Benefit Documents, Registered Benefit Not Initiated",
        //"data_query" => file_get_contents("{$dir}/cr8.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 10,
        'view' => 'cr8',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 9],
    [
        "name" => "Validated Files with Complete Benefit Documents",
        "description" => "Validated Files with Complete Benefit Documents, Benefit Not Registered",
        //"data_query" => file_get_contents("{$dir}/cr9.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 11,
        'view' => 'cr9',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 10],
    [
        "name" => "Validated Files with Incomplete Benefit Documents",
        "description" => "Validated Files with Incomplete Benefit Documents",
        //"data_query" => file_get_contents("{$dir}/cr10.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 12,
        'view' => 'cr10',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 11],
    [
        "name" => "Unvalidated Files",
        "description" => "Unvalidated Files with No Contribution & Validation Documents, Missing Optional Documents e.g PF90, Police Report, Work Permit",
        //"data_query" => file_get_contents("{$dir}/cr11.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 13,
        'view' => 'cr11',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 12],
    [
        "name" => "Unvalidated Files",
        "description" => "Unvalidated Files with No Contribution and Incomplete Validation Documents",
        //"data_query" => file_get_contents("{$dir}/cr12.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr12',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 13],
    [
        "name" => "Unvalidated Files",
        "description" => "Unvalidated Files with Contribution & Validation Documents, Missing Optional Documents e.g PF90, Police Report, Work Permit",
        //"data_query" => file_get_contents("{$dir}/cr13.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 15,
        'view' => 'cr13',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 14],
    [
        "name" => "Unvalidated Files",
        "description" => "Unvalidated Files with Contribution and Incomplete Validation Documents",
        //"data_query" => file_get_contents("{$dir}/cr14.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 16,
        'view' => 'cr14',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);


ConfigurableReport::query()->updateOrCreate(
    ["id" => 22],
    [
        "name" => "Unvalidated Files",
        "description" => "Unvalidated Files with No Contribution and Complete All Validation Documents",
        //"data_query" => file_get_contents("{$dir}/cr22.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 11,
        'view' => 'cr22',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 23],
    [
        "name" => "Unvalidated Files (Initiated For Approval)",
        "description" => "Unvalidated Files with Contribution and Complete All Validation Documents (Initiated For Approval)",
        //"data_query" => file_get_contents("{$dir}/cr23.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr23',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 24],
    [
        "name" => "Pending Rejected Claims (Initiated)",
        "description" => "Pending Rejected Claims (Initiated)",
        //"data_query" => file_get_contents("{$dir}/cr24.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 4,
        'view' => 'cr24',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 25],
    [
        "name" => "Rejected Claims (Not Initiated)",
        "description" => "Pending Rejected Claims (Not Initiated)",
        //"data_query" => file_get_contents("{$dir}/cr25.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 5,
        'view' => 'cr25',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 26],
    [
        "name" => "Claims Files (DFPI) - Archived",
        "description" => "Claims Files (DFPI) - Archived",
        //"data_query" => file_get_contents("{$dir}/cr26.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => 'cr26',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 27],
    [
        "name" => "Claims Processing (DO) - Archived",
        "description" => "Claims Processing (DO) - Archived",
        //"data_query" => file_get_contents("{$dir}/cr27.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 7,
        'view' => 'cr27',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 28],
    [
        "name" => "Claim Assessment (DMAS) - Archived",
        "description" => "Claim Assessment (DMAS) - Archived",
        //"data_query" => file_get_contents("{$dir}/cr28.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 8,
        'view' => 'cr28',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 29],
    [
        "name" => "Unvalidated Files",
        "description" => "Unvalidated Files with Contribution and Complete All Validation Documents",
        //"data_query" => file_get_contents("{$dir}/cr29.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr29',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

$id = '80';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Paid Claims [All Benefits]",
        "description" => "Paid Claims [All Benefits]",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 2,
        'view' => "cr{$id}",
        'isbalance' => 1,
        'istable' => 0,
        'display' => '[1,2,5,6,7,8,9,10,11,12,13]',
        'excludes' => '[4,16,18]',
        'aliases' => '{"10": "File Name"}',
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 88],
    [
        "name" => "Partial Registered Claims in information Dashboard",
        "description" => "Partial Registered Claims in information Dashboard",
        //"data_query" => file_get_contents("{$dir}/cr1.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 15,
        'view' => 'cr88',
        'isbalance' => 1,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);