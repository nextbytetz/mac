<?php

use App\Models\Reporting\ConfigurableReport;

$type = 3;
$dir = __DIR__;

ConfigurableReport::query()->updateOrCreate(
    ["id" => 31],
    [
        "name" => "Employer Closure Workflow",
        "description" => "Employer Closure Workflow",
        //"data_query" => file_get_contents("{$dir}/cr31.sql"),
        "filter" => file_get_contents("{$dir}/cr31_filter.json"),
        "unit_id" => 15,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr31',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer/closure/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 32],
    [
        "name" => "Employer Particular Change Workflow",
        "description" => "Employer Particular Change Workflow",
        //"data_query" => file_get_contents("{$dir}/cr32.sql"),
        "filter" => file_get_contents("{$dir}/cr32_filter.json"),
        "unit_id" => 15,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr32',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer/change_particular/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 33],
    [
        "name" => "Payroll Run Approval Workflow",
        "description" => "Payroll Run Approval Workflow",
        //"data_query" => file_get_contents("{$dir}/cr33.sql"),
        "filter" => file_get_contents("{$dir}/cr33_filter.json"),
        "unit_id" => 15,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr33',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"payroll/run_approval/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 34],
    [
        "name" => "Employers Registration Workflow",
        "description" => "Employers Registration Workflow",
        //"data_query" => file_get_contents("{$dir}/cr34.sql"),
        "filter" => file_get_contents("{$dir}/cr34_filter.json"),
        "unit_id" => 15,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr34',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer_registration/profile/{employer_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '35';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Employer Inspection Task Workflow",
        "description" => "Employer Inspection Task Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/inspection/employer_task/{employer_inspection_task_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '36';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Booking Interest Workflow",
        "description" => "Booking Interest Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer/profile/interest/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '37';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Claim Benefit (Non-Progressive) Workflow",
        "description" => "Claim Benefit (Non-Progressive) Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);


$id = '38';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Claim Benefit (Progressive) Workflow",
        "description" => "Claim Benefit (Progressive) Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '39';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Claim Notification (Progressive) Workflow",
        "description" => "Claim Notification (Progressive) Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '40';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Unregistered Member Notification Workflow",
        "description" => "Unregistered Member Notification Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '41';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Online Employer Verification Workflow",
        "description" => "Online Employer Verification Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer/{resource_id}/verification_profile"}',
        'ismaterialized' => 0,
    ]
);

$id = '42';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Compliance Letters Workflow",
        "description" => "Compliance Letters Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"letter/show/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '43';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Acknowledgement Letter Workflow",
        "description" => "Acknowledgement Letter Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"letter/show/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '44';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Benefit Award Letter Workflow",
        "description" => "Benefit Award Letter Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"letter/show/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '45';
$unit = 12;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Receipts Workflow",
        "description" => "Receipts Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"finance/receipt/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '46';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Payroll Status Change Workflow",
        "description" => "Payroll Status Change Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"payroll/status_change/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '47';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Payroll Beneficiary Update Workflow",
        "description" => "Payroll Beneficiary Update Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"payroll/beneficiary_update/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '48';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Employer Inspection Workflow",
        "description" => "Employer Inspection Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/inspection/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '49';
$unit = 12;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Legacy Receipt Workflow",
        "description" => "Legacy Receipt Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"finance/legacy_receipt/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '50';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Employer Advance Payment Workflow",
        "description" => "Employer Advance Payment Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer/{resource_id}/advance_payment_profile"}',
        'ismaterialized' => 0,
    ]
);

$id = '51';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Employer Closure Reopen Workflow",
        "description" => "Employer Closure Reopen Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer_closure/open/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '52';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Payroll Recovery Workflow",
        "description" => "Payroll Recovery Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"payroll/recovery/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '53';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Payroll Bank Info Update Workflow",
        "description" => "Payroll Bank Info Update Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"payroll/bank_update/profile/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '54';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Portal Incident Workflow",
        "description" => "Portal Incident Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer/{resource_id}/incident_profile"}',
        'ismaterialized' => 0,
    ]
);

$id = '72';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Claim Benefit (Progressive) Workflow (With Payments)",
        "description" => "Claim Benefit (Progressive) Workflow (With Payments)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'ismaterialized' => 1,
    ]
);

$id = '86';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Payment by Installment Workflow",
        "description" => "Payment by Installment Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"compliance/employer/online/view_installment/{installment_id}"}',
        'ismaterialized' => 0,
    ]
);

$id = '87';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Erroneous Contribution Workflow",
        "description" => "Erroneous Contribution Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"report/erroneous_refund_payment"}',
        'ismaterialized' => 0,
    ]
);

$id = '92';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Reminder Letter Workflow",
        "description" => "Reminder Letter Workflow",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => 3,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/wf_query_filter.json"),
        'url' => '{"1":"letter/show/{resource_id}"}',
        'ismaterialized' => 0,
    ]
);