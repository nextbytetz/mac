<?php

use App\Models\Reporting\ConfigurableReport;

$type = 5;
$dir = __DIR__;

$id = '89';
$unit = 17;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Assessed MAE",
        "description" => "Assessed MAE",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => NULL,
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);

$id = '90';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Assessed TD",
        "description" => "Assessed TD",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => NULL,
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);


$id = '91';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Assessed PD",
        "description" => "Assessed PD",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => NULL,
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);