<?php

use App\Models\Reporting\ConfigurableReport;

$type = 9;
$dir = __DIR__;

$id = '101';
$unit = 15;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Legacy Contributions",
        "description" => "Legacy Contributions",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 1,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5, 6]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 0,
    ]
);

