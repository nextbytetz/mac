<?php

use App\Models\Reporting\ConfigurableReport;

$type = 2;
$dir = __DIR__;

ConfigurableReport::query()->updateOrCreate(
    ["id" => 15],
    [
        "name" => "Not Initiated (Accident & Disease)",
        "description" => "Not Initiated Notification Reports (Accident & Disease)",
        //"data_query" => file_get_contents("{$dir}/cr15.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 17,
        'view' => 'cr15',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 16],
    [
        "name" => "Not Initiated (Accident & Disease), Complete Benefit Documents",
        "description" => "Not Initiated Notification Reports (Accident & Disease), Complete Benefit Documents",
        //"data_query" => file_get_contents("{$dir}/cr16.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 18,
        'view' => 'cr16',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 17],
    [
        "name" => "Not Initiated (Death)",
        "description" => "Not Initiated Notification Reports (Death)",
        //"data_query" => file_get_contents("{$dir}/cr17.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 19,
        'view' => 'cr17',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 18],
    [
        "name" => "Not Initiated (Death) , Complete Benefit Documents",
        "description" => "Not Initiated Notification Reports (Death) , Complete Benefit Documents",
        //"data_query" => file_get_contents("{$dir}/cr18.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 20,
        'view' => 'cr18',
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 19],
    [
        "name" => "Pending Missing Contribution Claims",
        "description" => "Pending Missing Contribution Claims",
        //"data_query" => file_get_contents("{$dir}/cr19.sql"),
        "filter" => NULL,
        "unit_id" => 15,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 19,
        'view' => 'cr19',
        'isbalance' => 0,
        'istable' => 1,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 21],
    [
        "name" => "Void Notification",
        "description" => "Void Notification",
        //"data_query" => file_get_contents("{$dir}/cr21.sql"),
        "filter" => NULL,
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 1,
        'view' => 'cr21',
        'isbalance' => 0,
        'istable' => 1,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

ConfigurableReport::query()->updateOrCreate(
    ["id" => 30],
    [
        "name" => "Manual Notifications",
        "description" => "Manual Notifications",
        //"data_query" => file_get_contents("{$dir}/cr30.sql"),
        "filter" => file_get_contents("{$dir}/cr30_filter.json"),
        "unit_id" => 14,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => 'cr30',
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8]',
        'excludes' => '[9,10,11,14,15,16,17,18,19,20,21,22]',
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"claim/manual_notification/{resourceid}/dashboard"}',
        'ismaterialized' => 1,
    ]
);


$id = '56';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Notification Report Received",
        "description" => "Notification Report Received",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{resource_id}", "2":"assessment/impairment/{resource_id}/create"}',
        'ismaterialized' => 1,
    ]
);

$id = '57';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Benefit Workflow Aging (Progressive 1)",
        "description" => "Benefit Workflow Aging (Progressive 1)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{resource_id}"}',
        'ismaterialized' => 1,
    ]
);

$id = '58';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Benefit Workflow Aging (Non-Progressive 1)",
        "description" => "Benefit Workflow Aging (Non-Progressive 1)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{resource_id}"}',
        'ismaterialized' => 1,
    ]
);

$id = '60';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Paid Claim With Disabilities",
        "description" => "Paid Claim With Disabilities",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{resource_id}"}',
        'ismaterialized' => 1,
    ]
);
$id = '59';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Online Notification Application",
        "description" => "Online Notification Application",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"compliance/employer/{resource_id}/incident_profile"}',
        'ismaterialized' => 0,
    ]
);
$id = '61';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Processed Claim (Paid & Rejected)",
        "description" => "Processed Claim (Paid & Rejected)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 1,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 73,
        'ismaterialized' => 1,
    ]
);

$id = '62';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Un-Processed Pending Claim",
        "description" => "Un-Processed Pending Claim",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 2,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 73,
        'ismaterialized' => 1,
    ]
);

$id = '63';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Un-Processed Rejected Claim",
        "description" => "Un-Processed Rejected Claim",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 3,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 73,
        'ismaterialized' => 1,
    ]
);

$id = '64';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Investigated (UnProcessed Pending Claims)",
        "description" => "Investigated (UnProcessed Pending Claims)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 4,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 74,
        'ismaterialized' => 1,
    ]
);

$id = '65';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "UnInvestigated (Un-Processed Pending Claims)",
        "description" => "UnInvestigated (Un-Processed Pending Claims)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 5,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 74,
        'ismaterialized' => 1,
    ]
);

$id = '66';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Validated (Un-Processed Pending Claims, Investigated)",
        "description" => "Validated (Un-Processed Pending Claims, Investigated)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 75,
        'ismaterialized' => 1,
    ]
);

$id = '67';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "UnValidated (Un-Processed Pending, Investigated)",
        "description" => "UnValidated (Un-Processed Pending, Investigated)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 75,
        'ismaterialized' => 1,
    ]
);

$id = '68';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Acknowledged Notification Reports",
        "description" => "Acknowledged Notification Reports",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 76,
        'ismaterialized' => 1,
    ]
);

$id = '69';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Unacknowledged Notification Reports",
        "description" => "Unacknowledged Notification Reports",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => '{"2": "Checklist User"}',
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"claim/notification_report/profile/{incident_id}"}',
        'parent_id' => 76,
        'ismaterialized' => 0,
    ]
);

$id = '70';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Assigned Notification Employers",
        "description" => "Assigned Notification Employers",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"compliance/employer/profile/{employer_id}"}',
        'parent_id' => 77,
        'ismaterialized' => 1,
    ]
);

$id = '71';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Assigned Notification Employers (ONS Registered)",
        "description" => "Assigned Notification Employers (ONS Registered)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"compliance/employer/profile/{employer_id}"}',
        'parent_id' => 77,
        'ismaterialized' => 1,
    ]
);

$id = '79';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Assigned Notification Employers (ONS Not Registered)",
        "description" => "Assigned Notification Employers (ONS Not Registered)",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"compliance/employer/profile/{employer_id}"}',
        'parent_id' => 77,
        'ismaterialized' => 1,
    ]
);

$id = '94';
$unit = 14;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Total Notification Received With Processed Stage",
        "description" => "Total Notification Received With Processed Stage",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"claim/notification_report/profile/{resource_id}", "2":"assessment/impairment/{resource_id}/create"}',
        'parent_id' => NULL,
        'ismaterialized' => 0,
    ]
);

$id = '95';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Online Notification Account Validated",
        "description" => "Online Notification Account Validated",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"claim/notification_report/online_account_validation_profile/{resource_id}"}',
        'parent_id' => NULL,
        'has_fin_year' => 'f',
        'ismaterialized' => 0,
    ]
);

$id = '96';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Total Notification Received With TAT",
        "description" => "Total Notification Received With TAT",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"claim/notification_report/profile/{resource_id}", "2":"assessment/impairment/{resource_id}/create"}',
        'parent_id' => NULL,
        'ismaterialized' => 0,
    ]
);

$id = '97';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Total Notification Received With HCP Details",
        "description" => "Total Notification Received With HCP Details",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 6,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"claim/notification_report/profile/{resource_id}", "2":"assessment/impairment/{resource_id}/create"}',
        'parent_id' => NULL,
        'ismaterialized' => 0,
    ]
);