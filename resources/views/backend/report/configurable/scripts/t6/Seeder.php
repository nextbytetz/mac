<?php

use App\Models\Reporting\ConfigurableReport;

$type = 6;
$dir = __DIR__;

$unit = 12;
ConfigurableReport::query()->updateOrCreate(
    ["id" => 20],
    [
        "name" => "Claims Workflow Pending at Finance",
        "description" => "Claims Workflow Pending at Finance",
        //"data_query" => file_get_contents("{$dir}/cr20.sql"),
        "filter" => NULL,
        "unit_id" => 12,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '1',
        "updated_at" => NULL,
        "sort" => 20,
        'view' => 'cr20',
        'isbalance' => 0,
        'istable' => 1,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);

$id = '93';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Receipts: Cashbook",
        "description" => "Receipts: Cashbook",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 1,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/includes/incident_filter.json"),
        'url' => '{"1":"finance/receipt/{resource_id}/edit"}',
        'parent_id' => NULL,
        'ismaterialized' => 0,
    ]
);

$id = '102';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Receipt: Cash Book",
        "description" => "Receipts report including active, cancelled, dishonoured",
        //"data_query" => file_get_contents("{$dir}/cr20.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 102,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => '[12,13,14]',
        'aliases' => NULL,
        'has_fin_year' => false,
        //'query_filter' => NULL,
        'url' => NULL,
        'ismaterialized' => 1,
    ]
);