<?php

use App\Models\Reporting\ConfigurableReport;

$type = 7;
$dir = __DIR__;

$id = '55';
$unit = 17;
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Impairment Assessment(s) Listing",
        "description" => "Impairment Assessment(s) Listing",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => '[0,1,2,3,4,5,6,7,8,9,10,11,12]',
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"assessment/impairment/{resource_id}/dashboard"}',
        'ismaterialized' => 1,
    ]
);

$id = '85';
ConfigurableReport::query()->updateOrCreate(
    ["id" => $id],
    [
        "name" => "Notification Requiring Rehabilitation",
        "description" => "Notification Requiring Rehabilitation",
        //"data_query" => file_get_contents("{$dir}/cr{$id}.sql"),
        "filter" => file_get_contents("{$dir}/cr{$id}_filter.json"),
        "unit_id" => $unit,
        "created_at" => NULL,
        "configurable_report_type_id" => $type,
        "isdashboard" => '0',
        "updated_at" => NULL,
        "sort" => 14,
        'view' => "cr{$id}",
        'isbalance' => 0,
        'istable' => 0,
        'display' => NULL,
        'excludes' => NULL,
        'aliases' => NULL,
        //'query_filter' => NULL,
        'url' => '{"1":"claim/notification_report/profile/{resource_id}"}',
        'has_fin_year' => 'f',
        'ismaterialized' => 1,
    ]
);