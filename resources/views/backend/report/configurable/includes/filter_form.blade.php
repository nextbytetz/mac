
@if ($filters)
    @php
        $array_count = count($filters);
        $chunk_size = ($array_count > 1) ? ceil($array_count/4) : 1;
    @endphp
    <div class="row">
        @foreach(array_chunk($filters, $chunk_size) as $filter)

            <div class="col-md-3 col-sm-3">
                @foreach($filter as $data)
                    <div class="form-group">
                        <label for="{{ $data['column'] . $cr->id }}"><b>{{ $data['description'] }}</b></label>
                        @if ($data['type'] == 'select')
                            {!! Form::select($data['column'], $data['source']['pluck'], null, ['class' => 'form-control search-select', 'id' => $data['column'] . $cr->id, 'placeholder' => '', 'style="width:100%;"']) !!}
                        @elseif($data['type'] == 'boolean')
                            {!! Form::select($data['column'], ['1' => 'Yes', '0' => 'No'], null, ['class' => 'form-control search-select', 'id' => $data['column'] . $cr->id, 'placeholder' => '', 'style="width:100%;"']) !!}
                        @elseif($data['type'] == 'text')
                            {!! Form::text($data['column'], NULL , ['class' => 'form-control circle-input', 'id' => $data['column'] . $cr->id]) !!}
                        @elseif($data['type'] == 'int')
                            {!! Form::text($data['column'], NULL , ['class' => 'form-control circle-input number', 'id' => $data['column'] . $cr->id]) !!}
                        @elseif($data['type'] == 'daterange')
                            <div class="form-inline">
                                <span><label style="width: 13%;text-align: center;">&nbsp;From&nbsp;</label></span>
                                <span>{!! Form::text($data['column'] . '_from', NULL , ['class' => 'form-control circle-input datepicker', 'id' => $data['column'] . '_from' . $cr->id, 'style' => 'width:25%;']) !!}</span>
                                <span><label style="width: 10%;text-align: center;">To</label></span>
                                <span>{!! Form::text($data['column'] . '_to', NULL , ['class' => 'form-control circle-input datepicker', 'id' => $data['column'] . '_to' . $cr->id, 'style' => 'width:25%;']) !!}</span>
                                {{--<span>{!! Form::hidden($data['column'] ) !!}</span>--}}
                            </div>
                        @elseif($data['type'] == 'selectlazy')
                            {!! Form::select($data['column'], [], null, ['class' => 'form-control ' . $data['column'] . $cr->id . '-select', 'id' => $data['column'] . $cr->id, 'style' => 'width:100%;']) !!}
                        @elseif($data['type'] == 'intrange')
                            <div class="form-inline">
                                <span><label style="width: 13%;text-align: center;">&nbsp;From&nbsp;</label></span>
                                <span>{!! Form::text($data['column'] . '_from', NULL , ['class' => 'form-control circle-input number', 'id' => $data['column'] . '_from' . $cr->id, 'style' => 'width:25%;']) !!}</span>
                                <span><label style="width: 10%;text-align: center;">To</label></span>
                                <span>{!! Form::text($data['column'] . '_to', NULL , ['class' => 'form-control circle-input number', 'id' => $data['column'] . '_to' . $cr->id, 'style' => 'width:25%;']) !!}</span>
                                {{--<span>{!! Form::hidden($data['column'] ) !!}</span>--}}
                            </div>
                        @endif
                    </div>

                @endforeach
            </div>
        @endforeach
        <div class="form-row">
            <div class="form-group col-md-12">
                <button type="button" class="btn btn-secondary site-btn" id="cr-clear-filter-form-{{ $cr->id }}">Clear</button>
                <button type="submit" class="btn btn-success btn-sm btn-submit">@lang('buttons.general.search')</button>
            </div>
        </div>
    </div>

@endif