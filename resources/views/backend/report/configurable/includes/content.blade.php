@include('backend.includes.assets.datetimepicker')
@include('backend.includes.datatable_assets')

@push('after-styles-end')

    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush

@php
    $cr_search_form_id = 'cr-search-form-' . $cr->id;
    $cr_clear_filter_form_id = 'cr-clear-filter-form-' . $cr->id;
    $cr_table_id = 'cr-table-' . $cr->id;
    $cr_headers_id = 'cr-headers-' . $cr->id;
    $cr_refresh_class = 'cr-refresh-' . $cr->id;
    $cr_build_class = 'cr-build-' . $cr->id;
    $cr_download_dt_class = 'cr-download-dt-' . $cr->id;
    $cr_download_all_class = 'cr-download-all-' . $cr->id;
    $cr_refresh_icon_class = 'cr-refresh-icon-' . $cr->id;
    $cr_build_icon_class = 'cr-build-icon-' . $cr->id;
    $cr_open_mode_id = 'cr-open-mode-' . $cr->id;
    $cr_query_filter = $cr_params['query_filter'] ?? [];
    $cr_query_url = $cr_params['url'] ?? 1;
@endphp

{!! Form::open(['role' => 'form', 'id' => $cr_search_form_id]) !!}
@if ($cr_params['hasfilter'])

    <br/>
    @php
        $crRepo = new \App\Repositories\Backend\Reporting\ConfigurableReportRepository();
        $filters = $crRepo->setDataForSelectFilter(json_decode($cr->filter ?? '[]', true));
    @endphp
    <div class="card-accordions">
        <div id="accordionsm" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" style="padding: .4rem .4rem !important;">
                    <h5 class="mb-0">
                        <a class="card-title" data-toggle="collapse" data-parent="#accordion" href="#cr_collapse"><i class='icon fa fa fa-search' aria-hidden='true'></i>&nbsp;{{ $cr->name }}</a>
                    </h5>
                </div>
                <div id="cr_collapse" class="collapse">
                    <div class="card-block">
                        @include('backend/report/configurable/includes/filter_form', ['filters' => $filters])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr/>

@endif
{!! Form::close() !!}

@if ($cr->url)
    <div class="row">
        <div class="col-md-12 pull-right">
            {{--<div class="form-row">--}}
                <div class="form-group">
                    <span class="assign_status_select">
                        <label for="{{ $cr_open_mode_id }}"><b>Open Mode:</b></label>
                        {!! Form::select('cr_open_mode', ['0' => 'Same Tab', '1' => 'New Tab'] , null, ['class' => 'form-control search-select', 'id' => $cr_open_mode_id, 'style' => 'width:30%;']) !!}
                    </span>
                </div>
            {{--</div>--}}
        </div>
    </div>
@endif

<div class="row">
    <div class="col-md-12 col-sm-12">
        @if ($cr_params['buttons'])
            <div class="pull-right" >

                @if ($cr_params['hasfilter'])
                    {{--Download Filter--}}
                    {!! HTML::decode(link_to_route('backend.report.cr_download_dt', "<i class='icon fa fa fa-search-plus' aria-hidden='true'></i>&nbsp;" . "Download Filter", [$cr->id],['class' => 'btn btn-sm btn-secondary ' . $cr_download_dt_class, 'style' => 'font-weight:normal;' ])) !!}
                @endif
                {{--Download All--}}
                {!! HTML::decode(link_to_route('backend.report.cr_download_all', "<i class='icon fa fa-cloud-download' aria-hidden='true'></i>&nbsp;" . "Download All", [$cr->id],['class' => 'btn btn-sm btn-secondary ' . $cr_download_all_class, 'style' => 'font-weight:normal;' ])) !!}
                @if ($cr->ismaterialized)
                    {{--Refresh--}}
                    {!! HTML::decode(link_to_route('backend.report.configurable.refresh', "<i class='icon fa fa-refresh {$cr_refresh_icon_class}' aria-hidden='true'></i>&nbsp;" . "Refresh", [$cr->id],['class' => 'btn btn-sm btn-secondary ' . $cr_refresh_class, 'style' => 'font-weight:normal;' ])) !!}
                @endif
                @if ($cr_params['canbuild'])
                    {{--Build--}}
                    {!! HTML::decode(link_to_route('backend.report.configurable.build', "<i class='icon fa fa-wrench {$cr_build_icon_class}' aria-hidden='true'></i>&nbsp;" . "Build", [$cr->id],['class' => 'btn btn-sm btn-secondary ' . $cr_build_class, 'style' => 'font-weight:normal;' ])) !!}
                @endif
            </div>
            <br/>
            <br/>
        @endif

        <div class="table table-responsive">
            <table class="display" cellspacing="0" width="100%" id = '{{ $cr_table_id }}'>
                <thead>
                    <tr id = '{{ $cr_headers_id }}'></tr>
                </thead>
            </table>
        </div>

    </div>
</div>

@push('after-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script  type="text/javascript">

        $(function() {
            let $url = "{!! url("/") !!}";
            $(".search-select").select2({
                allowClear: true,
                debug: true,
                placeholder: ""
            });
            @if ($cr_params['hasfilter'])
                @foreach($filters as $filter)
                    @if($filter['type'] == 'selectlazy')
                        @php
                            $selectlazy = $filter['source']['selectlazy'];
                            $selectclass = $filter['column'] . $cr->id . '-select';
                        @endphp
                        $(".{{ $selectclass }}").select2({
                            minimumInputLength: 3,
                            multiple: false,
                            allowClear: true,
                            debug: true,
                            placeholder: "",
                            ajax: {
                                url: $url + "/{{ $selectlazy['route'] }}",
                                method: "{{ $selectlazy['method'] }}",
                                dataType: 'json',
                                delay: 250,
                                data: function (params) {
                                    return {
                                        q: params.term || "",
                                        page: params.page || 1
                                    };
                                },
                                processResults: function (data, params) {
                                    params.page = params.page || 1;
                                    return {
                                        results: $.map(data.items, function (item) {
                                            let $text = eval("item.{{ $selectlazy['text'] }}");
                                            return {
                                                text: $text,
                                                id: item.id
                                            };
                                        }),
                                        pagination: {
                                            more: true
                                        }
                                    };
                                },
                                cache: true
                            },
                            escapeMarkup: function (markup) {
                                return markup;
                            }
                        });
                    @endif
                @endforeach
            @endif

            $.post("{!! route('backend.report.cr_headers', $cr->id) !!}", {}, function( $data ) {
                /*console.log($data);*/
            }, "json").done(function ($data) {
                $('#{{ $cr_headers_id }}').empty();
                $.each($data, function($index, $value){
                    /*console.log($value);*/
                    $('#{{ $cr_headers_id }}').append("<th>" + $value.colname + "</th>");
                });
                /*console.log($data);*/
                /*console.log(JSON.stringify($data));*/
                let $oTable = $('#{{ $cr_table_id }}').DataTable({
                    buttons : ['reload', 'colvis'],
                    initComplete : function () {
                        $oTable.buttons().container().insertBefore('#{{ $cr_table_id }}');
                        $("#{{ $cr_table_id }}").css("width","100%");
                    },
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    paging: true,
                    info:true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.report.cr_datatable', $cr->id) !!}',
                        type : 'post',
                        data: function ($d) {
                            $d = gatherDataDt($d);
                        }
                    },
                    columns: $data,
                    'rowCallback': function (nRow, $aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            @if ($cr->url)
                                let $url = '{{ json_decode($cr->url, true)[$cr_query_url] }}';
                                let $curlyRe = /\{([^}]+)\}/g;
                                let $match = $curlyRe.exec($url);
                                let $resourceurl = $url.replace(/\{(.+?)\}/g, function() {
                                    return $aData[$match[1]];
                                });
                                let $click_url = base_url + "/" + $resourceurl;
                                /*alert($click_url);
                                return false;*/
                                switch ($("#{{ $cr_open_mode_id }}").val()) {
                                    case "1":
                                        window.open($click_url, "_blank");
                                        break;
                                    default:
                                        window.open($click_url, "_self");
                                        break;
                                }
                            @endif
                        }).hover(function() {
                            $(this).css('cursor', 'alias');
                        }, function() {
                            $(this).css('cursor', 'auto');
                        });
                    },
                });
                @if ($cr_params['canbuild'])
                    $("a.{{ $cr_build_class }}").on("click", function($e) {
                        $this = $(this);
                        $this.find('i').removeClass("fa-wrench").addClass('fa-spinner fa-spin');
                        $.post("{!! route('backend.report.cr_build_dt', $cr->id) !!}", {}, function( $data ) {
                        }, "json").done(function ($data) {
                            $this.find('i').removeClass("fa-spinner fa-spin").addClass('fa-wrench');
                            $oTable.draw();
                        });
                        $e.preventDefault();
                    });
                @endif
                @if ($cr->ismaterialized)
                    $("a.{{ $cr_refresh_class }}").on("click", function($e) {
                        $this = $(this);
                        $this.find('i').removeClass("fa-refresh").addClass('fa-spinner fa-spin');
                        $.post("{!! route('backend.report.cr_refresh_dt', $cr->id) !!}", {}, function( $data ) {
                        }, "json").done(function ($data) {
                            $this.find('i').removeClass("fa-spinner fa-spin").addClass('fa-refresh');
                            $oTable.draw();
                        });
                        $e.preventDefault();
                    });
                @endif
                $("a.{{ $cr_download_dt_class }}").on("click", function($e) {
                    $this = $(this);
                    $this.find('i').removeClass("fa-search-plus").addClass('fa-spinner fa-spin');
                    let $data = {};
                    $data = gatherDataDt($data);

                    postData("{{ route('backend.report.cr_download_dt', $cr->id) }}", $data)
                        .then($resp => $resp.blob())
                        .then($blob => {
                            const $url = window.URL.createObjectURL($blob);
                            const $a = document.createElement('a');
                            $a.style.display = 'none';
                            $a.href = $url;
                            /*the filename*/
                            $a.download = '{{ $cr->name }}.csv';
                            document.body.appendChild($a);
                            $a.click();
                            window.URL.revokeObjectURL($url);
                            /*console.log('file downloaded!');*/
                            $this.find('i').removeClass("fa-spinner fa-spin").addClass('fa-search-plus');
                        })
                        .catch(() => console.log('oh no!'));
                    $e.preventDefault();
                });
                $("a.{{ $cr_download_all_class }}").on("click", function($e) {
                    $this = $(this);
                    $this.find('i').removeClass("fa-cloud-download").addClass('fa-spinner fa-spin');
                    let $data = {};
                    $data = gatherData($data);
                    /*$.post("{!! route('backend.report.cr_download_all', $cr->id) !!}", $data,function (result)
                    {
                        var blob=new Blob([result]);
                        var link=document.createElement('a');
                        link.href=window.URL.createObjectURL(blob);
                        link.download='{{ $cr->name }}.csv';
                        link.click();
                    }).done(function($data){
                        $this.find('i').removeClass("fa-spinner fa-spin").addClass('fa-cloud-download');
                    });*/
                    postData("{{ route('backend.report.cr_download_all', $cr->id) }}", $data)
                        .then($resp => $resp.blob())
                        .then($blob => {
                            const $url = window.URL.createObjectURL($blob);
                            const $a = document.createElement('a');
                            $a.style.display = 'none';
                            $a.href = $url;
                            /*the filename*/
                            $a.download = '{{ $cr->name }}.csv';
                            document.body.appendChild($a);
                            $a.click();
                            window.URL.revokeObjectURL($url);
                            /*console.log('file downloaded!');*/
                            $this.find('i').removeClass("fa-spinner fa-spin").addClass('fa-cloud-download');
                        })
                        .catch(() => console.log('oh no!'));
                    $e.preventDefault();
                });
                @if ($cr_params['hasfilter'])
                    $('#{{ $cr_search_form_id }}').on('submit', function($e) {
                        $oTable.draw();
                        $e.preventDefault();
                    });
                    $( "#{{ $cr_clear_filter_form_id }}" ).click(function() {
                        /* Clear the Filter Form */
                        $('#{{ $cr_search_form_id }}').find(':input').each(function () {
                            $(this).val('');
                            $(this).val(null).trigger('change.select2');
                            $oTable.draw();
                        });
                    });
                @endif
                @if ($cr->ismaterialized)
                    @if ($cr_params['shouldrefresh'])
                        $("a.{{ $cr_refresh_class }}").trigger('click');
                    @endif
                @endif
            });
            /* start : ensure only numbers are input on monetary boxes */
            $(".number").keydown(function (e) {
                number_only(e)
            });
            /* end : ensure only numbers are input on monetary boxes */
        });
        async function postData($url = '', $data = {}) {
            return await fetch($url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify($data)
            });
        }
        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
        /* end : ensure only numbers are input on monetary boxes */
        function gatherData($data) {
            {{--@foreach ($cr_query_filter as $index => $value)
                $data['{{ $index }}'] = '{!! json_encode($value) !!}';
            @endforeach--}}
            $data['query_filters'] = '{!! json_encode($cr_query_filter) !!}';
            $data['_token'] = $('#{{ $cr_search_form_id }} [name=_token]').val();
            /*console.log($data);*/
            return $data;
        }
        function gatherDataDt($data) {
            /*let $data = {};*/
            $data['hasfilter'] = "{{ $cr_params['hasfilter'] }}";
            $data = gatherData($data);
            $('#{{ $cr_search_form_id }}').find(':input').each(function () {
                let $name = $(this).attr('name');
                $data[$name] = $(this).val();
            });
            /*console.log($data);*/
            return $data;
        }
    </script>

@endpush