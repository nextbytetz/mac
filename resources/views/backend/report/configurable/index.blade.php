@extends('layouts.backend.main', ['title' => $cr->name, 'header_title' => $cr->name])

@push('after-styles-end')

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <legend>Report Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $cr->description }}</small></span></legend>

    <!--Include Report-->
    @include('backend/report/configurable/includes/content', [
                                                                'cr' => $cr,
                                                                'cr_params' => [
                                                                    'hasfilter' => 1,
                                                                    'shouldrefresh' => 0,
                                                                    'canbuild' => 1,
                                                                    'buttons' => 1,
                                                                ],
                                                            ])

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    <script  type="text/javascript">
        $(function() {

        });
    </script>
@endpush