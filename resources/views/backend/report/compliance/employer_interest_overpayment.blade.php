@extends('layouts.backend.main', ['title' => trans('labels.backend.member.employer_interest_overpayment'), 'header_title' => trans('labels.backend.member.employer_interest_overpayment')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}


    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}



    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th style="width:80px">{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! number_0_format($total_count) !!}</th>

        </tr>
        </thead>
    </table>
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}

@endpush


