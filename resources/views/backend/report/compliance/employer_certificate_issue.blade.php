@extends('layouts.backend.main', ['title' => trans('labels.backend.report.new_employees_registered'), 'header_title' => trans('labels.backend.report.employer_registration_certificate_issues')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.employer.certificate_issues'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')

    <div class="row">

        <div class = "col-md-12">
            <div class="row">

                {{--search issue type--}}
                <div class = "col-md-3" id="type_div">

                    {!! Form::select('type', ['0' => trans('labels.general.issue'), '1' => trans('labels.general.reissue')] , $request['type'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_category'),'class' => 'form-control search-select', 'id'=> 'type_id', 'hidden'=>true]) !!}

                </div>


            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>

                {{--issue type option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'type_check',1, ($request['type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'type_check' ]) !!}@lang('labels.backend.member.issue_type')</label>
                </div>



                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>



    </br>


    {!! Form::close() !!}
    {{--<div class = "col-md-12">--}}
    {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=>trans('labels.backend.finance.receipt.search_for_payer')]) !!}--}}
    {{--</div>--}}
    {{--datatable--}}

    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}



    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th style="width:80px">{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! number_0_format($total) !!}</th>

        </tr>
        </thead>
    </table>
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}

@stack('date-range-script-end')

<script>

        // main function search options
        $(function () {
            $(".search-select").select2();

           general_option('type_check', 'type_div','type_id');

            $("#type_check").click(function () {
                general_option('type_check', 'type_div','type_id');

            });


            $("#none_check").click(function () {
                none_option();
            })

        });

        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {

            $( "#type_check").prop( "checked", false );
            $("#type_div" ).hide();
            $( "#type_id").prop( "disabled", true );



        }

</script>

@endpush
