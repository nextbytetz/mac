@extends('layouts.backend.main', ['title' => 'Closed Business Extension Report', 'header_title' => 'Closed/Deregistered Business Extension Report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.closed_business_extension'],
  'method'=>'get',
  'name' => 'report', 'id' => 'report_form']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}

    @include('backend/report/includes/date_filter', ['date_filter_type' => 1 ])

    <div class="row">

        <div class = "col-md-12">
            <div class="row">




            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    {{--<label>@lang('labels.general.search_options'):</label>--}}
                </div>


            </div>
        </div>
    </div>
    <br/>
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}
                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}


    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th style="width:80px">{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! number_0_format($total) !!}</th>

        </tr>
        </thead>
    </table>
@stop

@push('after-script-end')


    @stack('date-range-script-end')
    {!! $dataTable->scripts() !!}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script type="text/javascript">

        // main function search options
        $(function () {
            $(".search-select").select2();


            // general_option('zone_check', 'zone_div','zone_id');
            // $("#zone_check").click(function () {
            //     general_option('zone_check', 'zone_div','zone_id');
            //
            // });
            //
            // $("#none_check").click(function () {
            //     none_option();
            // });

            // $("#export_button").click(function () {
            //     $('#export_flag').val(1).change();
            //     $('#report_form').submit();
            // })

            $("button").click(function() {
                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();

                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

        });


        //SEARCH OPTIONS-------------------------
        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {


            $( "#type_check").prop( "checked", false );
            $("#type_div" ).hide();
            $( "#type_id").prop( "disabled", true );




            $( "#region_check").prop( "checked", false );
            $("#region_div" ).hide();
            $( "#region_id").prop( "disabled", true );



            $( "#zone_check").prop( "checked", false );
            $("#zone_div" ).hide();
            $( "#zone_id").prop( "disabled", true );

        }

    </script>


@endpush

