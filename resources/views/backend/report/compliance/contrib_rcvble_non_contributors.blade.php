@extends('layouts.backend.main', ['title' =>'Non Contributing Employers Report', 'header_title' => 'Non Contributing Employers Report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.contrib_receivable_non_contributors'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')


    {{--<div class = "col-md-12">--}}
    {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=>trans('labels.backend.finance.receipt.search_for_payer')]) !!}--}}
    {{--</div>--}}
    {{--datatable--}}
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    <br/>

    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>@lang('labels.backend.table.total_amount_booked') </th>
            <th>{!! $total_booked !!}</th>
            <th>@lang('labels.backend.table.total_amount_paid') </th>
            <th>{!! $total_paid !!}</th>
            <th>Total Receivable</th>
            <th>{!! $total_receivable !!}</th>

        </tr>
        </thead>

    </table>
    {!! Form::close() !!}
@stop

@push('after-script-end')
    {!! $dataTable->scripts() !!}

    @stack('date-range-script-end')

    <script>
        {{--Input to search full name on first column--}}

        $(function () {
            $(".search-select").select2();

            $("button").click(function() {

                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();
                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

        });



        function searchname() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("name");
            filter = input.value.toUpperCase();
            table = document.getElementById("dataTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }


    </script>

@endpush
