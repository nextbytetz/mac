@extends('layouts.backend.main', ['title' => 'Online Employer Verifications', 'header_title' => 'Online Employer Verifications'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.employer_verifications'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                {{--search region--}}
                <div class = "col-md-3" id="status_div">
                    {!! Form::select('status_id' , [ '0' => 'Pending','1' => 'Approved',  '2' => 'Rejected', '3' => 'Initiated',],$request['status_id'] ,  ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_status'),'class' => 'form-control search-select', 'id'=> 'status_id','hidden'=>true]) !!}
                </div>


            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>

                {{--status option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'status_check',1 , ($request['status_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'status_check' ]) !!}@lang('labels.general.status')</label>
                </div>

                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>

    <br/>
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}


    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th style="width:80px">{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! number_0_format($total) !!}</th>

        </tr>
        </thead>
    </table>
@stop

@push('after-script-end')

    @stack('date-range-script-end')
    {!! $dataTable->scripts() !!}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script type="text/javascript">

        // main function search options
        $(function () {
            $(".search-select").select2();

            general_option('status_check', 'status_div','status_id');
            $("#status_check").click(function () {
                general_option('status_check', 'status_div','status_id');

            });

            $("#none_check").click(function () {
                none_option();
            })




            $("button").click(function() {
                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();

                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

        });

        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {

            $( "#status_check").prop( "checked", false );
            $("#status_div" ).hide();
            $( "#status_id").prop( "disabled", true );
        }

    </script>


@endpush

