@extends('layouts.backend.main', ['title' => trans('labels.backend.member.employees_list'), 'header_title' => trans('labels.backend.member.employees_list')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
    #name {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 12px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

</style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.employer.employees'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}


    <div class="row">

        <div class = "col-md-12">
            <div class="row">
                {{--search employer--}}
                <div class = "col-md-4" id="employer_div">
                    {!! Form::select('employer_id', [], $request['employer_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_employer'),'class' => 'form-control employer-select', 'id'=> 'employer_id','hidden'=>false]) !!}
                </div>

                <div class="element-form"  >
                    {{--<div class="col-md-2">--}}
                        <div class="pull-right">
                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                   {{-- </div>--}}
                </div>
            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>
                {{--employer option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'employer_check',1 , ($request['employer_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'employer_check' ]) !!}@lang('labels.general.employer')</label>
                </div>



                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>

    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}


    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>

            <th style="width:160px">{!! strtoupper(trans('labels.general.employer_name')) !!}:</th>
            <th>{!! $employer_name!!}</th>

            <th style="width:240px">{!! strtoupper(trans('labels.backend.member.no_of_employees')) !!}:</th>
            <th style="width:160px">{!! number_0_format($total) !!}</th>




        </tr>
        </thead>
    </table>
@stop

@push('after-script-end')


@stack('date-range-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script type="text/javascript">

    // main function search options
    $(function () {
        $(".search-select").select2();
        load_employers();
        general_option('employer_check', 'employer_div','employer_id');


        $("#employer_check").click(function () {
            general_option('employer_check', 'employer_div','employer_id');

        });



        $("#none_check").click(function () {
            none_option();
        })





    });


    //SEARCH OPTIONS-------------------------
    //SEARCH OPTIONS-------------------------
    function general_option(check,div, id) {
        if ($("#" + check).is(":checked")) {
            $("#" + div).show();
            $( "#" +id).prop( "disabled", false );
            $( "#none_check" ).prop( "checked", false );

        }else {
            $("#" + div).hide();
            $( "#" +id).prop( "disabled", true );
        }
    }

    // none option
    function none_option() {
        $( "#employer_check").prop( "checked", false );
        $("#employer_div" ).hide();
        $( "#employer_id").prop( "disabled", true );



    }




    //             employers
    function load_employers() {
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "<?php echo route('backend.compliance.employers'); ?>",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function(e) {
            var $selected = e.params.args.data.id;
            /* console.log("select2:select", e); */
            $('input[name=search]').val($selected);
        });
    }



</script>


@endpush

