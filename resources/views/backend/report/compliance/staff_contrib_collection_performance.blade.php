@extends('layouts.backend.main', ['title' => 'Staff Contribution Collection Performance report', 'header_title' => 'Staff contribution collection performance report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.staff_contrib_collection_performance'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}



    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    <br/>
    {!! Form::close() !!}


    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            {{--<th>@lang('labels.backend.table.total_amount_booked') </th>--}}
            {{--<th>{!! $total_booked !!}</th>--}}
            {{--<th>@lang('labels.backend.table.total_amount_paid') </th>--}}
            {{--<th>{!! $total_paid !!}</th>--}}


        </tr>
        </thead>

    </table>

@stop

@push('after-script-end')
    {!! $dataTable->scripts() !!}

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        {{--Input to search full name on first column--}}
        $(function () {

            $('.search-select').select2();


            $("button").click(function() {
                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();

                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });




        });




    </script>

@endpush
