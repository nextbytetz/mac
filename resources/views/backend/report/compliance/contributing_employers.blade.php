@extends('layouts.backend.main', ['title' => 'Contributing Employers', 'header_title' => 'Contributing Employers'])

@include('backend.includes.datatable_assets')
@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.contributing_employers'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}


    <div class="row " id="date_div">
        <div class="col-md-12 ">
            <div class="element-form" >
                {{--<div class="col-md-1 text-xs-left"><label>Date:</label></div>--}}
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-inline">


                            {{--Search from date --}}
                            <div class = "col-md-5" id="date_div">

                                {!! Form::select('date_type', ['1' => 'Receipt Date', '2' => 'Payment Date' , '3' => 'Contribution Month'], $request['date_type'], ['style' => 'width:100%', 'placeholder' => 'Select date type','class' => 'form-control search-select', 'id'=> 'date_id', 'hidden'=>true]) !!}

                            </div>

                            <div class = "col-md-3  input-group" id="date_div">
                                {{--<div class="input-group" style="width:100%;">--}}
                                {!! Form::text('from_date', null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'id' => 'date_id', 'autocomplete' => 'off']) !!}
                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                {{--</div>--}}


                                {!! $errors->first('from_date', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>


                        </div>
                    </div>
                </div>





                <div>&nbsp;</div>
            </div>
        </div>
    </div>

    <br/>





    <div class="row">

        <div class = "col-md-12">
            <div class="row">




                {{--search employer category--}}
                <div class = "col-md-3" id="type_div">

                    {!! Form::select('type', $employer_categories, $request['type'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_category'),'class' => 'form-control search-select', 'id'=> 'type_id', 'hidden'=>true]) !!}

                </div>

                {{--search contribtuing category--}}
                <div class = "col-md-3" id="contributing_category_div">
                    {!! Form::select('general_type', $contributing_categories, $request['general_type'], ['style' => 'width:100%', 'placeholder' => 'Search for contributors','class' => 'form-control search-select', 'id'=> 'contributing_category_id','hidden'=>true]) !!}
                </div>


                {{--search zone--}}
                <div class = "col-md-3" id="zone_div">
                    {!! Form::select('office_zone_id', $office_zones, $request['office_zone_id'], ['style' => 'width:100%', 'placeholder' => 'Search for zone','class' => 'form-control search-select', 'id'=> 'zone_id','hidden'=>true]) !!}
                </div>

                <div class="element-form"  >
                    {{--<div class="col-md-2">--}}
                    <div class="pull-right">
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>


                {{--date option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'date_check',1, ($request['date_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'date_check' ]) !!}By Date</label>
                </div>

                {{--employer category option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'type_check',1, ($request['type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'type_check' ]) !!}Category</label>
                </div>

                {{--contributing category option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'contributing_category_check',1, ($request['contributing_category_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'contributing_category_check' ]) !!}Contributing Category</label>
                </div>
                {{--zone--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'zone_check',1, ($request['zone_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'zone_check' ]) !!}Office Zone</label>
                </div>


                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>

    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'], true) !!}

    {{--SUMMARY--}}
    {{--<table class="display" cellspacing="0" width="100%">--}}
    {{--<thead>--}}
    {{--<tr>--}}
    {{--<th style="width:80px">{!! strtoupper(trans('labels.general.total')) !!}:</th>--}}
    {{--<th>{!! number_0_format($total) !!}</th>--}}

    {{--</tr>--}}
    {{--</thead>--}}
    {{--</table>--}}
@stop

@push('after-script-end')


    {{--@stack('date-range-script-end')--}}
    {!! $dataTable->scripts() !!}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script type="text/javascript">

        // main function search options
        $(function () {
            $(".search-select").select2();


            $('body').on('submit', 'form[name=report]', function(e) {
                e.preventDefault();
                // var   $from_date = new today_date;
                //  $from_date = $('#from_date').val();
                // var dd = $from_date.getDate();
                // var mm = $from_date.getMonth() + 1; //January is 0!
                // var yyyy = $from_date.getFullYear();
                // var from_date_formatted = yyyy + '/' + mm + '/' + dd;
                // $("input[name=from_date]").val(from_date_formatted);
                this.submit();
            });

            /*filter options*/
            general_option('date_check', 'date_div','date_id');
            general_option('type_check', 'type_div','type_id');
            general_option('zone_check', 'zone_div','zone_id');
            general_option('contributing_category_check', 'contributing_category_div','contributing_category_id');


            $("#type_check").click(function () {
                general_option('type_check', 'type_div','type_id');

            });



            $("#date_check").click(function () {
                general_option('date_check', 'date_div','date_id');

            });
//zone
            $("#zone_check").click(function () {
                general_option('zone_check', 'zone_div','zone_id');
            });


            $("#contributing_category_check").click(function () {
                general_option('contributing_category_check', 'contributing_category_div','contributing_category_id');
            });

            $("#none_check").click(function () {
                none_option();
            });




            /*------------Start Date Process ---------*/

            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            /*-----------End Date Process------------*/



        });


        //SEARCH OPTIONS-------------------------
        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {

            $( "#type_check").prop( "checked", false );
            $("#type_div" ).hide();
            $( "#type_id").prop( "disabled", true );

            $( "#contributing_category_check").prop( "checked", false );
            $("#contributing_category_div" ).hide();
            $( "#contributing_category_id").prop( "disabled", true );

            $( "#zone_check").prop( "checked", false );
            $("#zone_div" ).hide();
            $( "#zone_id").prop( "disabled", true );

            $( "#date_check").prop( "checked", false );
            $("#date_div" ).hide();
            $( "#date_id").prop( "disabled", true );

        }




    </script>


@endpush

