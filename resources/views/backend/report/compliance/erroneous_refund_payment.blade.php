@extends('layouts.backend.main', ['title' =>'Erroneous Refund Payment', 'header_title' => 'Erroneous Refund Payment Report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.erroneous_refund_payment'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')


    {{--<div class = "col-md-12">--}}
    {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=>trans('labels.backend.finance.receipt.search_for_payer')]) !!}--}}
    {{--</div>--}}
    {{--datatable--}}
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>

            </div>
        </div>
    </div>

    <br/>

    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>No. of employers </th>
            <th>{!! isset($no_of_employers) ? $no_of_employers : 0 !!}</th>
            <th>@lang('labels.backend.table.total_amount_booked') </th>
            <th>{!! isset($total_booked) ? $total_booked : 0 !!}</th>
            <th>@lang('labels.backend.table.total_amount_paid') </th>
            <th>{!!  isset($total_paid) ? $total_paid : 0  !!}</th>
            <th>Total Receivable</th>
            <th>{!! isset($total_receivable) ? $total_receivable : 0  !!}</th>

        </tr>
        </thead>

    </table>
    {!! Form::close() !!}
@stop

@push('after-script-end')

    {!! $dataTable->scripts() !!}


    @stack('date-range-script-end')

    <script>
        {{--Input to search full name on first column--}}

        $(function () {
            $(".search-select").select2();

            $("button").click(function() {

                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();
                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

        });





    </script>

@endpush
