@extends('layouts.backend.main', ['title' =>'Summary of Contribution Income - Receivable', 'header_title' => 'Summary of Contribution Income - Receivable'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush
@if(env('TESTING_MODE') == 1)
    @php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    @endphp
@endif

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.summary_contrib_income_rcvable'],
  'method'=>'get',
  'name' => 'report']) !!}
    {{ Form::hidden('search_flag', 1, []) }}
    {{--DATE RANGE--}}

    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        <span>
                                                <a  id="refresh_schedules"  href="#refresh_schedules"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Refresh Schedules</a>
                                                <a  id="refresh_data"  href="#refresh_data"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Refresh data</a>
                                                {{--<a  id="vacuum_data"  href="#vacuum_data"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Vacuum data</a>--}}
                                        <a href="{!! route('backend.finance.report.index',3) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>
                                     </span>



                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">

                                    {{--fiscal year--}}
                                    <div class="row ">
                                        <div class="col-md-12 ">
                                            <div class="element-form" >
                                                <div class="col-md-2 text-xs-left required"><label>Fiscal Year: </label> </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="form-inline">

                                                            {!! Form::select('fin_year_id', $fin_years, $summary->fin_year_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'fin_year_id', 'hidden'=>false]) !!}

                                                        </div>

                                                        {!! $errors->first('fin_year', '<span class="help-block label label-danger">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="element-form"  >
                                                <div class="col-md-2">
                                                    <div class="pull-right">

                                                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                                                    </div>
                                                </div>
                                            </div>



                                            <div>&nbsp;</div>
                                        </div>
                                    </div>
                                    <br/>

                                    {{--INCOME--}}
                                    {{--<br/>--}}
                                    <div class="row">
                                        {{--left general  summary--}}
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <legend id="old_bank"  style="background-color: lightgrey;text-align:center">Contribution Income -  {!! $summary->fiscal_year !!}

                                                @if(!isset($rcvable_summary))
                                                    <label style="color:maroon;">
                                                        Not Searched! Click Submit
                                                    </label>
                                                    @endif
                                                </legend>
                                                <table class="table table-striped table-bordered" id="old_bank_table">
                                                    <tbody>

                                                    {{--header--}}
                                                    <tr>
                                                        <th width="20px">Sn</th>
                                                        <th width="200px">Description</th>
                                                        <th width="120px">No. of employers</th>
                                                        <th width="120px">Amount</th>
                                                        <th width="120px">Action</th>

                                                    </tr>

                                                    {{--details--}}
                                                    <tr>
                                                        <th>{!! 1   !!} </th>
                                                        <td>{!! 'Receipts for Contributions'  !!} </td>
                                                        <th>{!! number_0_format($summary->receipt_contrib_employers) !!} </th>
                                                        <th>{!! number_2_format($summary->receipt_contrib)   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/receipts_contrib_summary_income?from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date }}">View</a></th>
                                                    </tr>
                                                    {{--2--}}
                                                    <tr>
                                                        <th>{!! 2   !!} </th>
                                                        <td>{!! 'Receivables Contributors Only'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_contributors_employers'])   !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_contributors'])   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/contribution_receivable?isfiscal=1&from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date }}">View</a></th>
                                                    </tr>

                                                    {{--3--}}
                                                    <tr>
                                                        <th>{!! 3   !!} </th>
                                                        <td>{!! 'Receipts for previous contributions never booked'  !!} </td>
                                                        <th>{!! number_0_format($summary->receipt_prev_contrib_employers)   !!} </th>
                                                        <th>{!! number_2_format($summary->receipt_prev_contrib)   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/receipts_prev_contrib_summary_income?from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date }}">View</a></th>
                                                    </tr>
                                                    {{--4--}}
                                                    <tr>
                                                        <th>{!! 4  !!} </th>
                                                        <td>{!! 'New employers previously not booked (Schedule 5)'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_new_entrants_employers'])   !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_new_entrants'])   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/previous_receivables_new_employers?from_date='. getWCFLaunchDate() .'&to_date='. $fin_year->end_date. '&fin_year=' . $fin_year->id }}">View</a></th>
                                                    </tr>
                                                    {{--5--}}
                                                    <tr>
                                                        <th>{!! 5  !!} </th>
                                                        <td>{!! 'Old employers not contributed (Schedule 6)'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_non_contributors_employers'])   !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_non_contributors'])   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/rcvable_non_contrib_whole_year?isfiscal=1&from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date }}">View</a></th>
                                                    </tr>

                                                    {{--6--}}
                                                    <tr>
                                                        <th>{!! 6  !!} </th>
                                                        <td>{!! 'Receivables for closed business'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_closed_business_employers'])   !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_closed_business'])   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/contrib_rcvable_closed_business?from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date. '&fin_year=' . $fin_year->id }}">View</a></th>
                                                    </tr>

                                                    {{--7--}}
                                                    <tr>
                                                        <th>{!! 7  !!} </th>
                                                        <th>{!! 'Total Contribution  Income'  !!} </th>
                                                        <th>{!! ''  !!} </th>
                                                        <th>{!!  number_2_format($summary->receipt_contrib + $summary->receipt_prev_contrib + $rcvable_summary['receivable_new_entrants'] + $rcvable_summary['receivable_non_contributors'] + $rcvable_summary['receivable_closed_business'] +  $rcvable_summary['receivable_contributors'])  !!} </th>
                                                        <th>{!! ''   !!} </th>
                                                    </tr>


                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>


                                    <br/>

                                    {{--RECEIVABLE--}}

                                    <div class="row">
                                        {{--left general  summary--}}
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <legend id="old_bank"  style="background-color: lightgrey;text-align:center">Contribution Receivable - {!! $summary->fiscal_year !!}</legend>
                                                <table class="table table-striped table-bordered" id="old_bank_table">
                                                    <tbody>

                                                    {{--header--}}
                                                    <tr>
                                                        <th width="20px">Sn</th>
                                                        <th width="200px">Description</th>
                                                        <th width="120px">No. of employers</th>
                                                        <th width="120px">Amount</th>
                                                        <th width="120px">Action</th>

                                                    </tr>


                                                    {{--details--}}
                                                    <tr>
                                                        <th>{!! 1  !!} </th>
                                                        <td>{!! 'Previous Receivable not paid to date'  !!} </td>
                                                        <th>{!! number_0_format($summary->receivable_open_balance_employers)   !!} </th>
                                                        <th>{!! number_2_format($summary->receivable_open_balance) !!} </th>
                                                        <th>{!! ''   !!} </th>
                                                    </tr>
                                                    {{--2--}}
                                                    <tr>
                                                        <th>{!! 2  !!} </th>
                                                        <td>{!! 'Receipt for previous contributions Booked Only'  !!} </td>
                                                        <th>{!! number_0_format($summary->receipt_prev_contrib_booked_employers)   !!} </th>
                                                        <th>{!! number_2_format($summary->receipt_prev_contrib_booked)   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/receipts_prev_contrib_booked?from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date }}">View</a></th>
                                                    </tr>

                                                    {{--3--}}
                                                    <tr>
                                                        <th>{!! 2  !!} </th>
                                                        <td>{!! 'Receivables Contributors Only'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_contributors_employers'])   !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_contributors'])   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/contribution_receivable?isfiscal=1&from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date }}">View</a></th>
                                                    </tr>
                                                    {{--4--}}
                                                    <tr>
                                                        <th>{!! 3  !!} </th>
                                                        <td>{!! 'New employers previously not booked (Schedule 5)'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_new_entrants_employers'])   !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_new_entrants'])   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/previous_receivables_new_employers?from_date='. getWCFLaunchDate() .'&to_date='. $fin_year->end_date. '&fin_year=' . $fin_year->id }}">View</a></th>
                                                    </tr>
                                                    {{--5--}}
                                                    <tr>
                                                        <th>{!! 4  !!} </th>
                                                        <td>{!! 'Old employers not contributed (Schedule 6)'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_non_contributors_employers'])  !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_non_contributors'])  !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/rcvable_non_contrib_whole_year?isfiscal=1&from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date }}">View</a></th>
                                                    </tr>

                                                    {{--6--}}
                                                    <tr>
                                                        <th>{!! 5  !!} </th>
                                                        <td>{!! 'Receivables for closed business'  !!} </td>
                                                        <th>{!! number_0_format($rcvable_summary['receivable_closed_business_employers'])   !!} </th>
                                                        <th>{!! number_2_format($rcvable_summary['receivable_closed_business'])   !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '/contrib_rcvable_closed_business?from_date='. $fin_year->start_date .'&to_date='. $fin_year->end_date. '&fin_year=' . $fin_year->id }}">View</a></th>
                                                    </tr>

                                                    {{--7--}}
                                                    <tr>
                                                        <th>{!! 6  !!} </th>
                                                        <th>{!! 'Total Contribution  Receivable'  !!} </th>
                                                        <th>{!! ''   !!} </th>
                                                        <th>{!! number_2_format($summary->receivable_open_balance + $rcvable_summary['receivable_contributors'] + $rcvable_summary['receivable_new_entrants'] + $rcvable_summary['receivable_non_contributors'] + $rcvable_summary['receivable_closed_business'])   !!} </th>
                                                        <th>{!! ''   !!} </th>
                                                    </tr>


                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>




                        </div>





                    </div>
                </div>
            </div>

        </div>
        </div>




        {!! Form::close() !!}
        @stop

        @push('after-script-end')



            @stack('date-range-script-end')
            {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
            <script>
                {{--Input to search full name on first column--}}

                $(function () {
                    $(".search-select").select2();

                    $(document).on('click', '#refresh_data', function () {
                                       window.open(base_url + "/report/update_receivable_mviews?isschedule=0" , "_self");
                    });

                    $(document).on('click', '#refresh_schedules', function () {
                        var fin_year_id = element_id_value('fin_year_id');
                        window.open(base_url + "/report/update_receivable_mviews?isschedule=1&fin_year_id=" + fin_year_id, "_self");
                    });

                    $(document).on('click', '#vacuum_data', function () {
                        window.open(base_url + "/report/update_receivable_mviews?isschedule=0&&isvacuum=1" , "_self");
                    });

                });






            </script>

    @endpush
