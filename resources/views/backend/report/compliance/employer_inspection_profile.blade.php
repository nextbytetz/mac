@extends('layouts.backend.main', ['title' =>'Employer Inspection Profile Report', 'header_title' => 'Employer Inspection Profile Report'])

@include('backend.includes.datatable_assets')
@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
#name {
    background-position: 10px 10px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 12px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
}

</style>

@endpush

@section('content')

<div class="custom_filter">
    {!! Form::open(['route' => ['backend.operation.report.employer_inspection_profile'], 'id' => 'search-form']) !!}
    
     {{-- @include('backend.report.includes.date_range') --}}
    <div class="row">
        <div class="col-md-12">
            <div class="form-row">

                {{--Employer Name--}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="employer">Employer Name</label>
                    {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                </div>
                {{--Region Selection--}}
                <div class="form-group col-md-4">
                    <label for="region">Inspection Region:</label>
                    {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                </div>
                {{-- inspection level (stage)--}}
                <div class="form-group col-md-4">
                    <label for="stage">Inspection Level(stage):</label>
                    {!! Form::select('stage', [], null, ['class' => 'form-control inspection-level-select', 'id' => 'stage', 'placeholder' => '']) !!}
                </div>        
                {{-- allocated_staffs--}}
                <div class="form-group col-md-4">
                    <label for="inspector">Inspector</label>
                    {!! Form::select('allocated_staff', [], null, ['class' => 'form-control employee-select', 'id' => 'allocated_staff', 'placeholder' => '']) !!}
                </div>
                {{--Inspection Findings--}}
                <div class="form-group col-md-4">
                    <label for="vist_date">visitation Start Date:</label>
                    <div class="input-group" style="width:100%;">
                        {!! Form::text('start_date', null, ['placeholder' => '', 'class' => 'form-control datepicker1 ', 'id'=> 'vist_start_date', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                </div>
               {{-- visitation summary --}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="vist_date">visitation End Date:</label>
                    <div class="input-group" style="width:100%;">
                        {!! Form::text('end_date', null, ['placeholder' => '', 'class' => 'form-control datepicker2', 'id'=> 'vist_end_date', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                </div> 
                
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <button type="button" class="btn btn-secondary site-btn" id="clear_filter">Clear</button>
                    <button type="submit" class="btn btn-success btn-sm btn-submit">@lang('buttons.general.search')</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<legend></legend>
<br/>
<div class="table-responsive">

    {{-- {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%',
    'id'=>'dataTable'],true) !!} --}}

    <table class="display" cellspacing="0" width="100%" id ="employer_inspection_profile-table">
        <thead>
            <tr >
                <th>Task Type</th>
                <th>Employer</th>
                <th>Allocated Staff</th>
                <th>Attended</th>
                <th>Stage</th>
                <th>Region</th>
                <th>Visit Date</th>
                <th>Assessment Review Start Date</th>
                <th>Assessment Review End Date</th>
                <th>Oustanding Contribution</th>
                <th>Outstanding Interest</th> 
                <th>Overpaid Contribution</th>
                <th>Total Outstanding Amount</th>
                <th>Outstanding Contribution Paid</th>
                <th>Outstanding Interest Paid</th>    
           </tr>
       </thead>
   </table>
</div>
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

@stack('date-range-script-end')

<script type="text/javascript">
    $(document).ready(function(){
        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });
        var table = $("dataTable").DataTable();
        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $("#spin2").hide();
                });
            }
        });
        $("#status").on("change", function () {
            var $status = $(this).val();
            switch($status) {
                case '3':
                /* Selected to choose User Allocated */
                $(".allocated_user_select").show();
                break;
                default:
                $(".allocated_user_select").hide();
                break;
            }
        });

        // $allocation_category_contrl.trigger('change');
        jQuery('.datepicker1').datetimepicker({
            timepicker:false,
            format:'Y-m-d',
            weeks: true,
            dayOfWeekStart: 1,
            lazyInit: true,
            scrollInput: false,
            maxDate: new Date(),
            minDate: new Date('2015-07-01')
        });
        jQuery('.datepicker2').datetimepicker({
            timepicker:false,
            format:'Y-m-d',
            weeks: true,
            dayOfWeekStart: 1,
            lazyInit: true,
            scrollInput: false,
            maxDate: 0,
            minDate: new Date('2015-07-01')
        });
        
        /* start : Searching Employer */
    
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.operation.report.allocated_staff') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    console.log(params)
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            console.log(item);
                            return {
                                text: item.allocated_staff,

                                id: item.id

                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employee */

        /* start of inspection level/stage */
          $(".inspection-level-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.operation.report.inspection_stage') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    console.log(data)
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            console.log(item);
                            return {
                                text: item.inspection_stage,

                                id: item.id

                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });



        /*End of inspection level */
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $(".inspection-level-select").val(null).trigger('change.select2');
            $(".search-select").val(null).trigger('change.select2');
            $(".datepicker1").val(null).trigger('change.select2');
            $(".datepicker2").val(null).trigger('change.select2');

        });

        var $oTable = $('#employer_inspection_profile-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            dom: 'Bfrtip',
            buttons: [
            'csv', 'excel', 'reload','colvis',
            ],
            drawCallback : function () {
             // alert('hapa umefika')
                InspectionProfileDetail();
                
            },

            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.operation.report.employer_inspection_profile') !!}',
                type : 'GET',
                  data: function ($data) {
                    $data = gatherDataDt($data, 'search-form');
                  
                    // $data.assign_for = $('select[name=assign_for]').val();
                }
            },
            columns: [
            { data: 'task_type', name: 'task_type', orderable: true, searchable: true},
            { data: 'employer', name: 'employer', orderable: true, searchable: true},
            { data: 'allocated_staff', name: 'allocated_staff', orderable: true, searchable: true},
            { data: 'attended', name: 'attended', orderable: true, searchable: true},
            { data: 'stage', name: 'stage', orderable: true, searchable: true},
            { data: 'region', name: 'region',  orderable: true, searchable: true},
            { data: 'visit_date', name: 'visit_date', orderable: true, searchable: true},
            { data: 'review_start_date', name: 'review_start_date', orderable: true, searchable: true},
            { data: 'review_end_date', name: 'review_end_date', orderable: true, searchable: true},
            { data: 'outstanding_contribution', name:  'outstanding_contribution',  orderable:  true, searchable:  true},
            { data: 'outstanding_interest', name: 'outstanding_interest',  orderable:  true, searchable:  true},
            { data: 'overpaid_contrib', name:  'overpaid_contrib',  orderable:  true, searchable:  true},
            { data: 'total_outstanding_amount', name: 'total_outstanding_amount',  orderable:  true, searchable:  true},
            { data: 'outstanding_contribution_paid', name: 'outstanding_contribution_paid',  orderable:  true, searchable:  true},
            { data: 'outstanding_interest_paid', name:  'outstanding_interest_paid',  orderable:  true, searchable:  true},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    // document.location.href = url +  "/compliance/employer/change_particular/profile/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });
        $('#search-form').on('submit', function($e) {
            $oTable.draw();
            /*alert($('select[name=category]').val());
            return false;*/
            $e.preventDefault();
        });
        
        function InspectionProfileDetail()
        {
            $.ajax({
                url: "{{ route("backend.operation.report.employer_inspection_profile") }}",
                dataType : 'html',
                /*async : false,*/
                method : "GET",
                data : {

                    employer : $('select[name=employer]').val(),
                    allocated_staff : $('select[name=allocated_staff]').val(),
                    region : $('select[name=region]').val(),
                    stage : $('select[name=stage]').val(),
                    visit_date : $('select[name=visit_date]').val(),
                    vist_end_date : $('select[name=vist_end_date]').val()
                }
            }).done(function ($data) {
                let $group = $("#allocation_employer_group");
                $group.empty();
                $group.html($data);
            });
        }

           function gatherDataDt($data, $id) {
        $('#' + $id).find(':input').each(function () {
            let $name = $(this).attr('name');
            $data[$name] = $(this).val();
        });
        return $data;
    }

    });
</script>
@endpush



