@extends('layouts.backend.main', ['title' => 'Staff Monthly Collection report', 'header_title' => 'Staff Monthly Collection report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.staff_monthly_collection_performance'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    <div class="row ">
        <div class="col-md-12 ">
            <div class="element-form" >
                <div class="col-md-1 text-xs-left"><label>Contrib Month:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-inline">

                            <span>      {!!  Form::selectMonth('contrib_month',isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('m') : null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month' ,'id'=> 'contrib_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('contrib_year',Carbon\Carbon::now()->format('Y'),2015,isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('Y') : null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year', 'id'=> 'contrib_year']) !!}
                        </span>

                        </div>
                        {!! Form::hidden('from_date', null, ['class' =>'from_date']) !!}
                        {!! $errors->first('from_date', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



            <div class="element-form"  >
                <div class="col-md-2">
                    <div class="pull-right">

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>

            <div>&nbsp;</div>
        </div>
    </div>



    {{--Start filter--}}
    <div class="row">

        <div class = "col-md-12">
            <div class="row">
                {{--search category--}}
                <div class = "col-md-3" id="category_div">

                    {!! Form::select('category' , ['0' => 'Permanent Officers', '1' => 'Intern'],isset($request['category']) ? $request['category'] : 0 ,  ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_category'),'class' => 'form-control search-select', 'id'=> 'category_id','hidden'=>true]) !!}
                </div>



            </div>
        </div>
    </div>
    <br/>
    {{--search option--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>
                {{--Category option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'category_check',1 , isset($request['category_check']) ?  (($request['category_check'] == 1) ? true : false) : false, ['style' => 'width:20px','id' => 'category_check' ]) !!}Category</label>
                </div>


                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>
    {{--End search option--}}
    <br/>
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    <br/>
    {!! Form::close() !!}


    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
    {{--<table class="display" cellspacing="0" width="100%">--}}
        {{--<thead>--}}
        {{--<tr>--}}
            {{--<th>Total Paid (This month)</th>--}}
            {{--<th>{!! $total_paid !!}</th>--}}
            {{--<th>Total Paid (Previous month)</th>--}}
            {{--<th>{!! $total_paid_prev !!}</th>--}}

        {{--</tr>--}}
        {{--</thead>--}}

    {{--</table>--}}

@stop

@push('after-script-end')
    {!! $dataTable->scripts() !!}

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        {{--Input to search full name on first column--}}
        $(function () {

            $('.search-select').select2();

            $("button").click(function() {
                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();

                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });
            general_option('category_check', 'category_div','category_id');
            $("#category_check").click(function () {
                general_option('category_check', 'category_div','category_id');

            });

            $("#none_check").click(function () {
                none_option();
            });


            //SEARCH OPTIONS-------------------------
            function general_option(check,div, id) {
                if ($("#" + check).is(":checked")) {
                    $("#" + div).show();
                    $( "#" +id).prop( "disabled", false );
                    $( "#none_check" ).prop( "checked", false );

                }else {
                    $("#" + div).hide();
                    $( "#" +id).prop( "disabled", true );
                }
            }


            // none option
            function none_option() {


                $( "#category_check").prop( "checked", false );
                $("#category_div" ).hide();
                $( "#category_id").prop( "disabled", true );


            }

        });



    </script>

@endpush
