@extends('layouts.backend.main', ['title' => $report_title , 'header_title' => $report_title])

@include('backend.includes.datatable_assets')
@include('backend.includes.assets.select2_assets')

@push('after-styles-end')
    <style>
    </style>
@endpush

@section('content')
    {{ Form::open(['route' => ['backend.operation.report.staff_employer_monthly_collection'],'method'=>'get', 'name' => 'report']) }}
    {{ Form::hidden('search_flag' , null, []) }}
    {{ Form::hidden('export_flag' , null, ['id' => 'export_flag']) }}
    <div class="row">
        <div class="col-md-12 card-body">
            {{--<div class="pull-right">--}}
                {{--<a href="{{ route('admin.report.reports_by_group', (isset($request['report_group_id'])) ? $request['report_group_id'] : 1) }}">{{ __('label.go_back') }}</a>--}}
            {{--</div>--}}
            @include('backend/report/includes/date_filter', ['date_filter_type' => 1 ])
            <br/>
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        <div class = "col-md-3 search_input_div" id="user_div">
                            {{ Form::select('user_id',isset($users) ? $users : [],(isset($request['user_id'])) ? $request['user_id'] : 0, ['style' => 'width:100%', 'placeholder' => 'Search For User','class' => 'form-control search_input search-select', 'id'=> 'user_id']) }}
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class = "col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label>{{ 'Search for filter'}}:</label>
                        </div>

                        <div class = "col-md-2">
                            {{ Form::checkbox( 'user_check',1, isset($request['user_check']) ? (($request['user_check'] == 1) ? true : false) : false, ['style' => 'width:20px','id' => 'user_check', 'class' => 'search_check' ]) }}{{ 'User' }}
                        </div>
                        <div class = "col-md-2">
                            {{ Form::checkbox( 'none_check',1, isset($request['none_check']) ? (($request['none_check'] == 1) ? true : false) : false, ['style' => 'width:20px','id' => 'none_check', 'class' => '' ]) }}{{ 'None' }}
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class = "col-md-6">
                    <div class="row">
                        <div class = "col-md-6">
                            <div class="form-group pull-right">
                                {{ Form::button(trans('buttons.general.search'),['class' => 'btn btn-sm btn-primary site-btn save_button button', 'type'=>'submit']) }}
                                {{ Form::button('Export to Excel',['class' => 'btn btn-success btn-sm site-btn dishonour_button button', 'id' => 'export_button', 'type'=>'submit']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <hr class="hr_custom">
    {{ Form::close() }}

    @if(isset($request['search_flag']))
        {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%','id'=> 'dataTable'], true) !!}
    @endif
    <table class="display" cellspacing="0" width="100%" style="background-color: lightgrey">
        <thead>
        <tr>
            <th style="width: 150px;"></th>
            <th></th>
        </tr>
        </thead>
    </table>
@endsection

@push('after-script-end')
    @if(isset($request['search_flag']))
        {!! $dataTable->scripts() !!}
    @endif

    @stack('date-range-script-end')
    <script>
        $(function() {
            $(".search-select").select2();

            $("button").click(function () {
                if (this.id == 'export_button') {
                    $('#export_flag').val(1).change();
                } else {
                    $('#export_flag').val(0).change();
                }
            });

            $('body').on('submit', 'form[name=report]', function(e) {
                e.preventDefault();
                $('.button').prop('disabled', false);
                this.submit();
            });

            $("#none_check").click(function () {
                none_option();
            });

            general_option('user_check','user_div','user_id');
            $("#user_check").click(function () {
                general_option('user_check','user_div','user_id');
            });

            function general_option(check, div, id) {
                if ($("#" + check).is(":checked")) {
                    $("#" + div).show();
                    $("#" + id).prop("disabled", false);
                    $("#none_check").prop("checked", false);
                } else {
                    $("#" + div).hide();
                    $("#" + id).prop("disabled", true);
                }
            }

            function none_option() {
                $(".search_check").prop("checked", false);
                $(".search_input_div").hide();
                $(".search_input").prop("disabled", true);
            }
        });
    </script>
@endpush
