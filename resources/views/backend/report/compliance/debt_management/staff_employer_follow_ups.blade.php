@extends('layouts.backend.main', ['title' =>'Staff Employer Follow Ups Report', 'header_title' => 'Staff Employer Follow Ups Report'])

@include('backend.includes.datatable_assets')
@include('backend.includes.assets.select2_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.staff_employer_follow_ups'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')



    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-3 search_input_div" id="feedback_div">
                    {{ Form::select('feedback_id',isset($feedbacks) ? $feedbacks : [],(isset($request['feedback_id'])) ? $request['feedback_id'] : 0, ['style' => 'width:100%', 'placeholder' => 'Search for Feedback','class' => 'form-control search_input search-select', 'id'=> 'feedback_id']) }}
                </div>

                <div class = "col-md-3 search_input_div" id="user_div">
                    {{ Form::select('user_id',isset($users) ? $users : [],(isset($request['user_id'])) ? $request['user_id'] : 0, ['style' => 'width:100%', 'placeholder' => 'Search for User','class' => 'form-control search_input search-select', 'id'=> 'user_id']) }}
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <label>{{ 'Search Filter' }}:</label>
                </div>

                <div class = "col-md-2">
                    {{ Form::checkbox( 'feedback_check',1, isset($request['feedback_check']) ? (($request['feedback_check'] == 1) ? true : false) : false, ['style' => 'width:20px','id' => 'feedback_check', 'class' => 'search_check' ]) }}{{ 'Feedback' }}
                </div>

                <div class = "col-md-2">
                    {{ Form::checkbox( 'user_check',1, isset($request['user_check']) ? (($request['user_check'] == 1) ? true : false) : false, ['style' => 'width:20px','id' => 'user_check', 'class' => 'search_check' ]) }}{{ 'User' }}
                </div>
                <div class = "col-md-2">
                    {{ Form::checkbox( 'none_check',1, isset($request['none_check']) ? (($request['none_check'] == 1) ? true : false) : false, ['style' => 'width:20px','id' => 'none_check', 'class' => '' ]) }}{{ 'None'  }}
                </div>
            </div>
        </div>
    </div>
    {{--<div class = "col-md-12">--}}
    {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=>trans('labels.backend.finance.receipt.search_for_payer')]) !!}--}}
    {{--</div>--}}
    {{--datatable--}}
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>

            </div>
        </div>
    </div>

    <br/>

    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

    {{--<table class="display" cellspacing="0" width="100%">--}}
        {{--<thead>--}}
        {{--<tr>--}}
            {{--<th>No. of employers </th>--}}
            {{--<th>{!! isset($no_of_employers) ? $no_of_employers : 0 !!}</th>--}}
            {{--<th>@lang('labels.backend.table.total_amount_booked') </th>--}}
            {{--<th>{!! isset($total_booked) ? $total_booked : 0 !!}</th>--}}
            {{--<th>@lang('labels.backend.table.total_amount_paid') </th>--}}
            {{--<th>{!!  isset($total_paid) ? $total_paid : 0  !!}</th>--}}
            {{--<th>Total Receivable</th>--}}
            {{--<th>{!! isset($total_receivable) ? $total_receivable : 0  !!}</th>--}}

        {{--</tr>--}}
        {{--</thead>--}}

    {{--</table>--}}
    {!! Form::close() !!}
@stop

@push('after-script-end')

    {!! $dataTable->scripts() !!}


    @stack('date-range-script-end')

    <script>
        {{--Input to search full name on first column--}}

        $(function () {
            $(".search-select").select2();

            $("button").click(function() {

                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();
                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

            $("#none_check").click(function () {
                none_option();
            });

            general_option('feedback_check','feedback_div','feedback_id');
            $("#feedback_check").click(function () {
                general_option('feedback_check','feedback_div','feedback_id');
            });

            general_option('user_check','user_div','user_id');
            $("#user_check").click(function () {
                general_option('user_check','user_div','user_id');
            });

            function general_option(check, div, id) {
                if ($("#" + check).is(":checked")) {
                    $("#" + div).show();
                    $("#" + id).prop("disabled", false);
                    $("#none_check").prop("checked", false);
                } else {
                    $("#" + div).hide();
                    $("#" + id).prop("disabled", true);
                }
            }

            function none_option() {
                $(".search_check").prop("checked", false);
                $(".search_input_div").hide();
                $(".search_input").prop("disabled", true);
            }
            //
            // function searchname() {
            //     var input, filter, table, tr, td, i;
            //     input = document.getElementById("name");
            //     filter = input.value.toUpperCase();
            //     table = document.getElementById("dataTable");
            //     tr = table.getElementsByTagName("tr");
            //     for (i = 0; i < tr.length; i++) {
            //         td = tr[i].getElementsByTagName("td")[1];
            //         if (td) {
            //             if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            //                 tr[i].style.display = "";
            //             } else {
            //                 tr[i].style.display = "none";
            //             }
            //         }
            //     }
            // }

        });




    </script>

@endpush









