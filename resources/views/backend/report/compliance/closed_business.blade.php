@extends('layouts.backend.main', ['title' => 'Closed Business Report', 'header_title' => 'Closed Business Report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.closed_business'],
  'method'=>'get',
  'name' => 'report', 'id' => 'report_form']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')

    <div class="row">

        <div class = "col-md-12">
            <div class="row">

                {{--search closure type--}}
                <div class = "col-md-3" id="type_div">

                    {!! Form::select('type', ['1' => 'Temporary Closure', '2' => 'Permanent Closure'], $request['type'], ['style' => 'width:100%', 'placeholder' => 'Search for closure type','class' => 'form-control search-select', 'id'=> 'type_id', 'hidden'=>true]) !!}

                </div>

                {{--search region--}}
                <div class = "col-md-3" id="region_div">
                    {!! Form::select('region_id', $regions, $request['region_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_region'),'class' => 'form-control search-select', 'id'=> 'region_id','hidden'=>true]) !!}
                </div>


                {{--search zone--}}
                <div class = "col-md-3" id="zone_div">
                    {!! Form::select('office_zone_id', $office_zones, $request['office_zone_id'], ['style' => 'width:100%', 'placeholder' => 'Search for zone','class' => 'form-control search-select', 'id'=> 'zone_id','hidden'=>true]) !!}
                </div>


            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>


                {{--closure type option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'type_check',1, ($request['type_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'type_check' ]) !!}Closure Type</label>
                </div>

                {{--region option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'region_check',1, ($request['region_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'region_check' ]) !!}@lang('labels.general.region')</label>
                </div>


                {{--zone--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'zone_check',1, ($request['zone_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'zone_check' ]) !!}Office Zone</label>
                </div>

                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>
    <br/>
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    <div>&nbsp;</div>
    {{--datatable--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}


    {{--SUMMARY--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th style="width:80px">{!! strtoupper(trans('labels.general.total')) !!}:</th>
            <th>{!! number_0_format($total) !!}</th>

        </tr>
        </thead>
    </table>
@stop

@push('after-script-end')


    @stack('date-range-script-end')
    {!! $dataTable->scripts() !!}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script type="text/javascript">

        // main function search options
        $(function () {
            $(".search-select").select2();

            general_option('region_check', 'region_div','region_id');
            general_option('type_check', 'type_div','type_id');
            general_option('zone_check', 'zone_div','zone_id');

            $("#type_check").click(function () {
                general_option('type_check', 'type_div','type_id');

            });


            $("#region_check").click(function () {
                general_option('region_check', 'region_div','region_id');

            });


            $("#zone_check").click(function () {
                general_option('zone_check', 'zone_div','zone_id');

            });

            $("#none_check").click(function () {
                none_option();
            });

            // $("#export_button").click(function () {
            //     $('#export_flag').val(1).change();
            //     $('#report_form').submit();
            // })

            $("button").click(function() {
                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();

                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

        });


        //SEARCH OPTIONS-------------------------
        //SEARCH OPTIONS-------------------------
        function general_option(check,div, id) {
            if ($("#" + check).is(":checked")) {
                $("#" + div).show();
                $( "#" +id).prop( "disabled", false );
                $( "#none_check" ).prop( "checked", false );

            }else {
                $("#" + div).hide();
                $( "#" +id).prop( "disabled", true );
            }
        }

        // none option
        function none_option() {


            $( "#type_check").prop( "checked", false );
            $("#type_div" ).hide();
            $( "#type_id").prop( "disabled", true );




            $( "#region_check").prop( "checked", false );
            $("#region_div" ).hide();
            $( "#region_id").prop( "disabled", true );



            $( "#zone_check").prop( "checked", false );
            $("#zone_div" ).hide();
            $( "#zone_id").prop( "disabled", true );

        }

    </script>


@endpush

