@extends('layouts.backend.main', ['title' =>'Receipt Contributions', 'header_title' => 'Receipt Contributions'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.receipt_contributions'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}
    @include('backend.report.includes.date_range')


    {{--<div class = "col-md-12">--}}
    {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=>trans('labels.backend.finance.receipt.search_for_payer')]) !!}--}}
    {{--</div>--}}
    {{--datatable--}}


    <div class="row">

        <div class = "col-md-12">
            <div class="row">



                {{--search contribtuing category--}}
                <div class = "col-md-3" id="contributing_category_div">
                    {!! Form::select('general_type', ['1' => 'Large', '2' => 'Medium', '4' => 'Small'], isset($request['general_type']) ? $request['general_type'] : null, ['style' => 'width:100%', 'placeholder' => 'Search for contributors','class' => 'form-control search-select', 'id'=> 'contributing_category_id','hidden'=>true]) !!}
                </div>

                {{--search region--}}
                <div class = "col-md-3" id="region_div">
                    {!! Form::select('region_id', $regions, $request['region_id'], ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_region'),'class' => 'form-control search-select', 'id'=> 'region_id','hidden'=>true]) !!}
                </div>



            </div>
        </div>
    </div>

    </br>
    {{--search options-------}}


    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class = "col-md-2">
                    <label>@lang('labels.general.search_options'):</label>
                </div>

                {{--contributing category option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'contributing_category_check',1, ($request['contributing_category_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'contributing_category_check' ]) !!}Contributing Category</label>
                </div>

                {{--region option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'region_check',1, ($request['region_check'] == 1) ? true : false, ['style' => 'width:20px','id' => 'region_check' ]) !!}@lang('labels.general.region')</label>
                </div>

                {{--None option--}}
                <div class = "col-md-2">
                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                </div>

            </div>
        </div>
    </div>








    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>





            </div>
        </div>
    </div>

    <br/>
    {{--@if(isset($submit_flag))--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
    {{--@endif--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            {{--<th>@lang('labels.backend.table.total_amount_booked') </th>--}}
            {{--<th>{!! isset($total_booked) ? $total_booked : 0 !!}</th>--}}
            <th>@lang('labels.backend.table.total_amount_paid') </th>
            <th>{!!  isset($total_paid) ? $total_paid : 0  !!}</th>
            {{--<th>Total Receivable</th>--}}
            {{--<th>{!! isset($total_receivable) ? $total_receivable : 0  !!}</th>--}}

        </tr>
        </thead>

    </table>
    {!! Form::close() !!}
@stop

@push('after-script-end')
    {{--@if(isset($submit_flag))--}}
    {!! $dataTable->scripts() !!}
    {{--@endif--}}

    @stack('date-range-script-end')

    <script>
        {{--Input to search full name on first column--}}

        $(function () {
            $(".search-select").select2();

            $("button").click(function() {

                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();
                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });

            general_option('contributing_category_check', 'contributing_category_div','contributing_category_id');
            $("#contributing_category_check").click(function () {
                general_option('contributing_category_check', 'contributing_category_div','contributing_category_id');
            });


            general_option('region_check', 'region_div','region_id');
            $("#region_check").click(function () {
                general_option('region_check', 'region_div','region_id');
            });

            $("#none_check").click(function () {
                none_option();
            });



            //SEARCH OPTIONS-------------------------
            function general_option(check,div, id) {
                if ($("#" + check).is(":checked")) {
                    $("#" + div).show();
                    $( "#" +id).prop( "disabled", false );
                    $( "#none_check" ).prop( "checked", false );

                }else {
                    $("#" + div).hide();
                    $( "#" +id).prop( "disabled", true );
                }
            }

            // none option
            function none_option() {


                $( "#contributing_category_check").prop( "checked", false );
                $("#contributing_category_div" ).hide();
                $( "#contributing_category_id").prop( "disabled", true );


            }



        });



        function searchname() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("name");
            filter = input.value.toUpperCase();
            table = document.getElementById("dataTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }


    </script>

@endpush
