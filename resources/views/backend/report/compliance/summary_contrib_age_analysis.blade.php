@extends('layouts.backend.main', ['title' =>'Summary of Contribution Age Analysis Report', 'header_title' => 'Summary of Contribution Age Analysis Report'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.summary_contrib_age_analysis'],
  'method'=>'get',
  'name' => 'report']) !!}

    {{--DATE RANGE--}}

    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        <span>
                                                                            <a href="{!! route('backend.operation.report.update_receivable_mviews') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Refresh data</a>

                                        <a href="{!! route('backend.finance.report.index',3) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>
                                     </span>



                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-6">
                                    {{--general info--}}
                                    {{--General info--}}
                                    {{--<br/>--}}
                                    <div class="row">
                                        {{--left general  summary--}}
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <legend id="old_bank"  style="background-color: lightgrey;text-align:center">Summary report</legend>
                                                <table class="table table-striped table-bordered" id="old_bank_table">
                                                    <tbody>

                                                    {{--header--}}
                                                    <tr>
                                                        <th width="20px">Sn</th>
                                                        <th width="120px">Age group (Days)</th>
                                                        <th width="120px">No. of employers</th>
                                                        <th width="120px">Total receivable</th>
                                                        <th width="120px">Action</th>

                                                    </tr>

                                                    {{--details--}}
                                                    <tr>
                                                        <th>{!! 1   !!} </th>
                                                        <td>{!! '0 - 30'  !!} </td>
                                                        <th>{!! number_0_format( $summary->employers_30) !!} </th>
                                                        <th>{!! number_2_format($summary->receivable_30) !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '&min_days=0&max_days=30' }}">View</a></th>
                                                    </tr>
                                                    {{----}}
                                                    <tr>
                                                        <th>{!! 2   !!} </th>
                                                        <td>{!! '31 - 90'  !!} </td>
                                                        <th>{!! number_0_format( $summary->employers_90) !!} </th>
                                                        <th>{!! number_2_format($summary->receivable_90) !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '&min_days=31&max_days=90' }}">View</a></th>
                                                    </tr>
                                                    {{----}}
                                                    <tr>
                                                        <th>{!! 3   !!} </th>
                                                        <td>{!! '91 - 180'  !!} </td>
                                                        <th>{!! number_0_format( $summary->employers_180) !!} </th>
                                                        <th>{!! number_2_format($summary->receivable_180) !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '&min_days=91&max_days=180' }}">View</a></th>
                                                    </tr>
                                                    {{----}}
                                                    <tr>
                                                        <th>{!! 4   !!} </th>
                                                        <td>{!! '181 - 365'  !!} </td>
                                                        <th>{!! number_0_format( $summary->employers_365) !!} </th>
                                                        <th>{!! number_2_format($summary->receivable_365) !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '&min_days=181&max_days=365' }}">View</a></th>
                                                    </tr>
                                                    {{--1--}}
                                                    <tr>
                                                        <th>{!! 5 !!} </th>
                                                        <td>{!! '366 - Above'  !!} </td>
                                                        <th>{!! number_0_format( $summary->employers_366) !!} </th>
                                                        <th>{!! number_2_format($summary->receivable_366) !!} </th>
                                                        <th><a  style="color: blue;" target="_blank"  href="{{ $base_url . '&min_days=366&max_days=' }}">View</a></th>
                                                    </tr>

                                                    {{--<tr>--}}
                                                        {{--<th>{!! 6 !!} </th>--}}
                                                        {{--<th>{!! 'Total'  !!} </th>--}}
                                                        {{--<th>{!! '' !!} </th>--}}
                                                        {{--<th>{!! number_2_format($summary->receivable_30 + $summary->receivable_90 + $summary->receivable_180 + $summary->receivable_365 + $summary->receivable_366) !!} </th>--}}
                                                        {{--<th></th>--}}
                                                    {{--</tr>--}}




                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>




                        </div>





                    </div>
                </div>
            </div>

        </div>



        {!! Form::close() !!}
        @stop

        @push('after-script-end')



            @stack('date-range-script-end')
            {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
            <script>
                {{--Input to search full name on first column--}}

                $(function () {
                    $(".search-select").select2();



                });






            </script>

    @endpush
