@extends('layouts.backend.main', ['title' =>'Contribution Receivables - Age Analysis', 'header_title' => 'Contribution Receivables - Age Analysis'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.contribution_age_analysis'],
  'method'=>'get',
  'name' => 'report']) !!}
    {!! Form::hidden('search_flag' , null, []) !!}
    {!! Form::hidden('export_flag' , null, ['id' => 'export_flag']) !!}
    {{--DATE RANGE--}}


    <div class="row ">
        <div class="col-md-12 ">
            <div class="element-form" >
                <div class="col-md-1 text-xs-left"><label>Cutoff Date:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-inline">

                                                 <span>      {!!  Form::selectRange('cutoff_day',1,31,isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('d') : null, ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=> 'from_day']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('cutoff_month',isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('m') : null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month' ,'id'=> 'contrib_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('cutoff_year',Carbon\Carbon::now()->format('Y'),2015,isset($request->from_date) ? \Carbon\Carbon::parse($request->from_date)->format('Y') : null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year', 'id'=> 'contrib_year']) !!}
                        </span>

                        </div>
                        {!! Form::hidden('from_date', null, ['class' =>'from_date']) !!}
                        {!! $errors->first('from_date', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



            <div class="element-form"  >
                <div class="col-md-2">
                    <div class="pull-right">

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>

            <div>&nbsp;</div>
        </div>
    </div>



    <div class="row ">
        <div class="col-md-12 ">
            <div class="element-form" >
                <div class="col-md-1 text-xs-left"><label>No. of days (Aging):</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-inline">

                            <span>         {!! Form::input( 'text','min_days', isset($request['min_days']) ? $request['min_days'] : null , ['class' => 'form-control number','style'=>'width:65px', 'placeholder' => 'Min', ]) !!}
                        </span>
&nbsp; To
                            <span>         {!! Form::input( 'text','max_days',isset($request['max_days']) ? $request['max_days'] : null , ['class' => 'form-control number','style'=>'width:65px', 'placeholder' => 'Max', ]) !!}
                        </span>

                        </div>

                    </div>
                </div>
            </div>





            <div>&nbsp;</div>
        </div>
    </div>











    {{--<div class = "col-md-12">--}}
    {{--{!! Form::input( 'text','name', null, ['class' => 'form-control', 'id'=>'name','onkeyup'=>'searchname()', 'placeholder'=>trans('labels.backend.finance.receipt.search_for_payer')]) !!}--}}
    {{--</div>--}}
    {{--datatable--}}
    {{--Export--}}
    <div class="row">
        <div class = "col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {{--<div class="pull-right">--}}

                    {!! Form::button('Export to excel',['class' => 'btn btn-success site-btn dishonour_button', 'id' => 'export_button', 'type'=>'submit']) !!}

                    {{--</div>--}}
                </div>




            </div>
        </div>
    </div>

    <br/>
    {{--@if(isset($submit_flag))--}}
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
    {{--@endif--}}
    <table class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>@lang('labels.backend.table.total_amount_booked') </th>
            <th>{!! isset($total_booked) ? $total_booked : 0 !!}</th>
            <th>@lang('labels.backend.table.total_amount_paid') </th>
            <th>{!!  isset($total_paid) ? $total_paid : 0  !!}</th>
            <th>Total Receivable</th>
            <th>{!! isset($total_receivable) ? $total_receivable : 0  !!}</th>

        </tr>
        </thead>

    </table>
    {!! Form::close() !!}
@stop

@push('after-script-end')
    {{--@if(isset($submit_flag))--}}
    {!! $dataTable->scripts() !!}
    {{--@endif--}}

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script>
        {{--Input to search full name on first column--}}

        $(function () {
            $(".search-select").select2();

            $("button").click(function() {

                if(this.id == 'export_button'){
                    $('#export_flag').val(1).change();
                    // ('#report_form').submit();
                }else{
                    $('#export_flag').val(0).change();
                }
            });


            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);
            });

        });

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }


        function searchname() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("name");
            filter = input.value.toUpperCase();
            table = document.getElementById("dataTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }


    </script>

@endpush
