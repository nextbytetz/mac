@extends('layouts.backend.main', ['title' =>'Summary Contribution Collection - Quarterly', 'header_title' => 'Summary Contribution Collection - Quarterly'])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        #name {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 12px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

    </style>

@endpush

@section('content')

    {{--FROM AND TO DATE--}}
    {!! Form::open(['route' => ['backend.operation.report.summary_contrib_quarterly'],
  'method'=>'get',
  'name' => 'report']) !!}

    {{--DATE RANGE--}}

    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        <span>
                                                                            {{--<a href="{!! route('backend.operation.report.update_receivable_mviews') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Refresh data</a>--}}

                                            <a href="{!! route('backend.finance.report.index',3) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>
                                     </span>



                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">

                                    @include('backend/report/includes/date_range')
                                    <br/>

                                    {{--Start filter--}}
                                    <div class="row">

                                        <div class = "col-md-12">
                                            <div class="row">
                                                {{--search category--}}
                                                <div class = "col-md-3" id="category_div">

                                                    {!! Form::select('category' , ['1' => 'Contributing Employers', '2' => 'Employer Type (Public/Private)'],isset($request['category']) ? $request['category'] : 1 ,  ['style' => 'width:100%', 'placeholder' => trans('labels.backend.report.search_for_category'),'class' => 'form-control search-select', 'id'=> 'category_id','hidden'=>true]) !!}
                                                </div>
                                                {{--search general type--}}
                                                <div class = "col-md-3" id="general_type_div">

                                                    {!! Form::select('general_type', ['1' => 'Receipt Date', '2' => 'Contribution Month'], isset($request['general_type']) ? $request['general_type'] : 1, ['style' => 'width:100%', 'placeholder' => 'Search for Date type','class' => 'form-control search-select', 'id'=> 'general_type_id', 'hidden'=>true]) !!}

                                                </div>



                                            </div>
                                        </div>
                                    </div>


                                    {{--End filter--}}

                                    <br/>
                                    {{--search option--}}
                                    <div class="row">
                                        <div class = "col-md-12">
                                            <div class="row">
                                                <div class = "col-md-2">
                                                    <label>@lang('labels.general.search_options'):</label>
                                                </div>
                                                {{--Category option--}}
                                                <div class = "col-md-2">
                                                    <label>{!! Form::checkbox( 'category_check',1 , isset($request['category_check']) ?  (($request['category_check'] == 1) ? true : false) : true, ['style' => 'width:20px','id' => 'category_check' ]) !!}Category</label>
                                                </div>

                                                {{--General Type--}}
                                                <div class = "col-md-2">
                                                    <label>{!! Form::checkbox( 'general_type_check',1, isset($request['category_check']) ? (($request['general_type_check'] == 1) ? true : false) : true, ['style' => 'width:20px','id' => 'general_type_check' ]) !!}Date Type</label>
                                                </div>



                                                {{--None option--}}
                                                <div class = "col-md-2">
                                                    <label>{!! Form::checkbox( 'none_check',1, false, ['style' => 'width:20px','id' => 'none_check' ]) !!}@lang('labels.general.none')</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    {{--End search option--}}
                                    <br/>
                                    {{--general info--}}
                                    {{--General info--}}
                                    {{--<br/>--}}
                                    {{--@if(isset($quarter_months))--}}
                                    <div class="row">
                                        {{--left general  summary--}}
                                        {{--<div class="col-md-12">--}}
                                        <div class="col-md-12">
                                            <legend id="old_bank"  style="background-color: lightgrey;text-align:center">Summary report - Amount in TZS '000000'</legend>

                                            @if(isset($request['category']))

                                                {{--Category - contributing--}}
                                                @if($request['category'] == 1)
                                                    <table class="table table-striped table-bordered" id="category_table">
                                                        <tbody>

                                                        {{--header--}}
                                                        <tr>
                                                            <th width="120px">Category</th>
                                                            <th width="120px">Target</th>
                                                            <th width="120px">Total Colection</th>
                                                            <th width="120px">No of Employers</th>
                                                            <th width="120px">Variance in %</th>
                                                        </tr>

                                                        {{--details--}}
                                                        <tr>
                                                            <th>{!! 'Large Contributors'   !!} </th>
                                                            <td>{!! number_0_format($target['large_target']/ 1000000)  !!} </td>
                                                            <td>{!! number_2_format($collection['large_collection'] / 1000000) !!} </td>
                                                            <td>{!! number_0_format($no_of_employers['large_employers'] ) !!} </td>
                                                            <td>{!! ($target['large_target'] > 0 ? number_0_format(100 - ($collection['large_collection'] * 100)/$target['large_target'])  : 0). ' %'  !!} </td>
                                                        </tr>
                                                        {{----}}
                                                        <tr>
                                                            <th>{!! 'Medium Contributors'   !!} </th>
                                                            <td>{!! number_2_format($target['medium_target'] / 1000000) !!} </td>
                                                            <td>{!! number_2_format($collection['medium_collection'] / 1000000) !!} </td>
                                                            <td>{!! number_0_format($no_of_employers['medium_employers'] ) !!} </td>
                                                            <td>{!! ($target['medium_target'] > 0 ? number_0_format(100 - ($collection['medium_collection'] * 100)/$target['medium_target'])  : 0). ' %'  !!} </td>
                                                        </tr>
                                                        {{----}}
                                                        <tr>
                                                            <th>{!! 'Small Contributors'   !!} </th>
                                                            <td>{!! number_2_format($target['small_target']/ 1000000) !!} </td>
                                                            <td>{!! number_2_format($collection['small_collection'] / 1000000) !!} </td>
                                                            <td>{!! number_0_format($no_of_employers['small_employers'] ) !!} </td>
                                                            <td>{!! ($target['small_target'] > 0 ? number_0_format(100 - (($collection['small_collection'] * 100)/$target['small_target'])) : 0). ' %'  !!} </td>
                                                        </tr>
                                                        {{----}}



                                                        </tbody>
                                                    </table>
                                                @endif



                                                {{--Category - Employer type  Public/ private--}}
                                                @if($request['category'] == 2)
                                                    <table class="table table-striped table-bordered" id="category_table">
                                                        <tbody>

                                                        {{--header--}}
                                                        <tr>
                                                            <th width="120px">Category</th>
                                                            <th width="120px">Target</th>
                                                            <th width="120px">Total Colection</th>
                                                            <th width="120px">No of Employers</th>
                                                            <th width="120px">Variance in %</th>
                                                        </tr>

                                                        {{--details--}}
                                                        <tr>
                                                            <th>{!! 'Public'   !!} </th>
                                                            <td>{!! number_0_format($target['public_target']/ 1000000)  !!} </td>
                                                            <td>{!! number_2_format($collection['public_collection'] / 1000000) !!} </td>
                                                            <td>{!! number_0_format($no_of_employers['public_employers'] ) !!} </td>
                                                            <td>{!! ($target['public_target'] > 0 ? number_0_format(100 - ($collection['public_collection'] * 100)/$target['public_target'])  : 0). ' %'  !!} </td>
                                                        </tr>
                                                        {{----}}
                                                        <tr>
                                                            <th>{!! 'Private'   !!} </th>
                                                            <td>{!! number_2_format($target['private_target'] / 1000000) !!} </td>
                                                            <td>{!! number_2_format($collection['private_collection'] / 1000000) !!} </td>
                                                            <td>{!! number_0_format($no_of_employers['private_employers'] ) !!} </td>
                                                            <td>{!! ($target['private_target'] > 0 ? number_0_format(100 - ($collection['private_collection'] * 100)/$target['private_target'])  : 0). ' %'  !!} </td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                @endif




                                            @endif
                                        </div>
                                    </div>
                                    {{--</div>--}}
                                    {{--@endif--}}

                                </div>
                            </div>




                        </div>





                    </div>
                </div>
            </div>

        </div>



        {!! Form::close() !!}
        @stop

        @push('after-script-end')



            @stack('date-range-script-end')
            {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
            <script>
                {{--Input to search full name on first column--}}

                $(function () {
                    $(".search-select").select2();

                    general_option('general_type_check', 'general_type_div','general_type_id');
                    general_option('category_check', 'category_div','category_id');

                    $("#general_type_check").click(function () {
                        general_option('general_type_check', 'general_type_div','general_type_id');

                    });

                    $("#category_check").click(function () {
                        general_option('category_check', 'category_div','category_id');

                    });

                    $("#none_check").click(function () {
                        none_option();
                    });

                    //SEARCH OPTIONS-------------------------
                    function general_option(check,div, id) {
                        if ($("#" + check).is(":checked")) {
                            $("#" + div).show();
                            $( "#" +id).prop( "disabled", false );
                            $( "#none_check" ).prop( "checked", false );

                        }else {
                            $("#" + div).hide();
                            $( "#" +id).prop( "disabled", true );
                        }
                    }


                    // none option
                    function none_option() {

                        $( "#general_type_check").prop( "checked", false );
                        $("#general_type_div" ).hide();
                        $( "#general_type_id").prop( "disabled", true );

                        $( "#category_check").prop( "checked", false );
                        $("#category_div" ).hide();
                        $( "#category_id").prop( "disabled", true );


                    }

                });






            </script>

    @endpush
