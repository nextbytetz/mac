@extends('layouts.backend.main', ['title' => 'Claim Performance Report', 'header_title' => 'Claim Performance Report'])

@push('after-styles-end')

@endpush

@section('content')
    <legend>
        {{ $configurable->name }}
    </legend>
    {{--put child reports--}}
    @if ($configurable->childs()->count())
        <div class="row">
            <div class="offset-md-6 col-md-6">
                <table class="table table-striped table-bordered" style="border: 2px dotted green;">
                    <thead>
                    <tr>
                        <td colspan="1"><b>Detailed Reports</b></td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($configurable->childs as $child)
                            <tr>
                                <td style="text-align: left;"><a class="link" href="{!! route('backend.report.cr', [$child->id]) !!}"><i class="icon fa fa-terminal" aria-hidden="true"></i>&nbsp;<span class="underline">{{ $child->name }}</span></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    <!-- Put the page specifically for this page here -->
    @include('backend/report/configurable/includes/content', [
                'cr' => $configurable,
                'cr_params' => [
                    'hasfilter' => 1,
                    'shouldrefresh' => 1,
                    'canbuild' => 1,
                    'buttons' => 1,
                    ]
                ])

    {{--@include("backend/report/performance/claim/{$configurable->id}")--}}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush