@extends('layouts.backend.main', ['title' => 'Staff Performance', 'header_title' => 'Claim Administration Staff Performance'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    @include('backend/report/performance/claim/staff/content')
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush