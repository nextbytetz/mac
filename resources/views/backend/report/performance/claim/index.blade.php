@extends('layouts.backend.main', ['title' => "Claim Performance Report", 'header_title' => "Claim Performance Report"])

@push('after-styles-end')

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {{--<div class="row">
        <div class="col-md-12">
            <div class="alert-left-border">
                <div class="alert alert-primary alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Note.</strong> Performance is based on amount of contribution collected from allocated employers rational to target receivable amount from allocated employers.
                </div>
            </div>
        </div>
    </div>--}}

    <div class = "row">
        <div class="col-md-12" >
            <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
                <thead class="tbody_per_region table_display_block" >
                    <tr style="color: white; background-color: lightskyblue" >
                        <th>Sn</th>
                        <th>Report Title</th>
                        {{--<th>Action</th>--}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($reports as $report)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            {{--<th></th>--}}
                            <th><a href="{!! route('backend.claim.notification_report.view_performance_report', [$report->id]) !!}">{{ $report->name }}</a></th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush