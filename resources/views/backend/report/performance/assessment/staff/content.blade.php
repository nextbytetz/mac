@php
    $crRepository = new \App\Repositories\Backend\Reporting\ConfigurableReportRepository();
    $staff_performances = [];
    $staff_reports = $crRepository->query()->where('configurable_report_type_id', 5)->get();
    foreach ($staff_reports as $staff_report) {
        $spr_headers_init = $crRepository->buildHeadersForDatatable($staff_report);
        $spr_data_model = $crRepository->getForDatatable($staff_report);
        $spr_total_performance_init = $spr_data_model->avg("aging");
        $spr_rows_init = $spr_data_model->orderBy('aging', 'asc')->get()->toArray();
        //collect reports .....
        $staff_performances[] = [
            'title' => $staff_report->name,
            'headers' => $spr_headers_init,
            'total_performance' => $spr_total_performance_init,
            'rows' => $spr_rows_init,
            'id' => $staff_report->id,
        ];
        //dd(count($spr_headers_init));
    }
    //dd($staff_performances);
@endphp

<div class="row">
    <div class="col-md-12">
        <div class="alert-left-border">
            <div class="alert alert-primary alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Note.</strong> Performance is based number of days spent to assess file (Aging in Days).
            </div>
        </div>
    </div>
</div>
{{--<div class = "row">
    <div class="pull-right">
        <div class="btn-group">
            --}}{{--<a  id="extended_button" href="#"  class="btn btn-xs btn-info" ><i class="icon fa fa-tachometer"></i>&nbsp;View Extended Performance</a>--}}{{--
            --}}{{--@if(($check_pending_level1 == 1 || $check_workflow == 0) && $employer_closure->is_legacy == 0)--}}{{--
            <a href="{!! route('backend.report.configurable.refresh_by_name', 'staff_employer_contrib_performance') !!}"  class="btn btn-xs btn-primary" ><i class="icon fa fa-plus"></i>&nbsp;Refresh Data</a>
        </div>
    </div>
</div>--}}
<hr/>

@foreach($staff_performances as $staff_performance)
    @php
        $spr_title = $staff_performance['title'];
        $spr_headers = $staff_performance['headers'];
        $spr_total_performance = $staff_performance['total_performance'];
        $spr_rows = $staff_performance['rows'];
    @endphp

    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <div><span style="font-weight: bold;font-size: 16px;">{{ $spr_title }}</span>&nbsp;&nbsp;<a href="{!! route('backend.report.configurable.refresh', $staff_performance['id']) !!}"  class="btn btn-xs btn-secondary" ><i class="icon fa fa-refresh"></i>&nbsp;Refresh Data</a></div>
            <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
                <tbody class="tbody_per_region table_display_block" >

                <tr style="color: white; background-color: #495a86" >
                    <th>sn</th>
                    @foreach($spr_headers as $spr_header)
                        <th>{{ $spr_header['colname'] }}</th>
                    @endforeach
                </tr>

                @foreach($spr_rows as $spr_row)
                    <tr>
                        <td style="width: 5px;">{{ $loop->iteration }}</td>
                        @foreach($spr_headers as $spr_header)
                            @php
                                $spr_index = str_replace(' ', '_', strtolower($spr_header['colname']));
                            @endphp
                            @if ($spr_index == 'performance')
                                <td><b>{{ $spr_row[$spr_index] }}&nbsp;%</b></td>
                            @else
                                <td>{{ $spr_row[$spr_index] }}</td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach


                <tr>
                    {{--<td></td>--}}
                </tr>

                <tr style="font-weight: bold;background: #8eb4cb">
                    <td></td>
                    <td>Average Aging <small>(Ignoring Decimals)</small></td>
                    {{--<td colspan="{{ count($spr_headers) - 3 }}"></td>--}}
                    <td><b>{{ number_0_format($spr_total_performance) }}&nbsp;Days</b></td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    <br/>
    <hr/>
    <br/>
@endforeach