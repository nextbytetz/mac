@extends('layouts.backend.main', ['title' => 'Payroll Alert Monitor', 'header_title' => 'Payroll Alert Monitor'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>


    </style>
@endpush

@section('content')


    {{--Count summary --}}
    @include("backend/operation/payroll/pension_administration/alert_monitor/includes/summary")

    <br/>

    {{--Pensioners pending for activation count--}}
    @if($alert_reference == 1)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pensioners_ready_for_payroll_datatable')
    @endif
    {{--Dependents pending for activation count--}}
    @if($alert_reference == 2)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_dependents_ready_for_payroll_datatable')
    @endif

    <!-- Put the page specifically for this page here -->
    @if($alert_reference == 3)
        {{--Child suspensions list--}}
@include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_suspension_for_datatable')
    @endif
    {{--Child alert who approach 18yrs old--}}
    @if($alert_reference == 4)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_child_alert_for_datatable')
    @endif
    {{--Retiree suspensions count--}}
    @if($alert_reference == 5)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_retiree_suspensions_datatable')
    @endif

    {{--Claims which are pending cca registration --}}
    @if($alert_reference == 6)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_claims_pending_cca_datatable')
    @endif
    {{--Documents : pending for verifications --}}
    @if($alert_reference == 7)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pending_documents_need_action',['document_type'=>63 ] )
    @endif
    {{--Documents : pending for bank updates --}}
    @if($alert_reference == 8)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pending_documents_need_action',['document_type'=>64 ])
    @endif
    {{--Documents : pending for beneficiary details updates --}}
    @if($alert_reference == 9)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pending_documents_need_action',['document_type'=>65 ])
    @endif
    {{--Pending payroll recoveries--}}
    {{--@if($alert_reference == 10)--}}
        {{--@include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pending_recoveries_datatable')--}}
    {{--@endif--}}
    {{--Pensioners with missing details--}}
    @if($alert_reference == 11)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_beneficiaries_with_missing_details', ['member_type_id' => 5])
    @endif
    {{--Dependents with missing details--}}
    @if($alert_reference == 12)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_beneficiaries_with_missing_details', ['member_type_id' => 4])
    @endif

    {{--Pending workflow--}}
    @if($alert_reference == 13)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_payroll_pending_workflow')
    @endif

{{--Pensioners verification alerts--}}
    @if($alert_reference == 14)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pensioners_verification_alerts_dt')
    @endif

    {{--Dependents verification alerts--}}
    @if($alert_reference == 15)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_dependents_verification_alerts_dt')
    @endif

    {{--Payroll Beneficiaries on Hold alerts--}}
    @if($alert_reference == 16)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_beneficiaries_on_hold')
    @endif
    {{--All suspended beneficiaries--}}
    @if($alert_reference == 17)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_all_suspended_for_dt')
    @endif

    {{--Child alert who approach education extension deadline--}}
    @if($alert_reference == 18)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_child_alert_for_edu_ext_dt')
    @endif
    {{--child alert who approach disability extension deadline--}}
    @if($alert_reference == 19)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_child_alert_with_extension_dt', ['child_continuation_reason_cv_ref' =>'PAYCHCDIS' ])
    @endif

    {{--Payroll retiree alerts for followups--}}
    @if($alert_reference == 20)
        @include('backend/operation/payroll/pension_administration/alert_monitor/includes/get_pensioners_retiree_alerts_dt', ['member_type_id' =>5])
    @endif

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush