{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="payroll-retiree-suspensions-table">
            <thead>
            <tr >
                <th>Pensioner</th>
                <th>Dob</th>
                <th>Limit</th>
                <th>Suspended Date</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#payroll-retiree-suspensions-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.retiree_suspension.get_pending_for_datatable') !!}',
                    type : 'post',
                    data: function ($d) {
                        $d.isinbox_task = '{{ isset($isinbox_task) ? $isinbox_task : null }}';
                    },
                },
                columns: [
                    { data: 'fullname', name: 'pensioners.firstname', orderable : true, searchable : true},
                    { data: 'dob_formatted' , name: 'pensioners.dob', orderable : true, searchable : true},
                    { data: 'limit_option', name: 'limit_option',  orderable : false, searchable : false},
                    { data: 'suspended_date', name: 'payroll_retiree_suspensions.created_at',  orderable : true, searchable : true},
                    { data: 'fullname', name: 'pensioners.middlename',visible:false, orderable : true, searchable : true},
                    { data: 'fullname', name: 'pensioners.lastname', visible:false, orderable : true, searchable : true},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['pensioner_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush