
<div class="row pull-right">
    <div class="col-md-12">
        @if(env('TESTING_MODE') == 1)
        <a style="color:green;"  id="export_excel"  href="{{ route('backend.payroll.alert_monitor.export_excel', $alert_reference) }}" >Export Excel</a> |
        @endif
        <a style="color:green;"   href="{{ route('backend.payroll.alert_monitor.reset_alerts') }}" >Reset</a>
        <a style="color:green;" href="{{ route('backend.payroll.alert_monitor.refresh_alert_task_checker') }}" >&nbsp;| Reset Inbox & Task</a>
    </div>
</div>

{{--Alert monitor summary --}}
{{--ROW 1--}}
<div class="row">

    <div class="col-md-12">
        {{--<br/>--}}

        <div class="row">

            {{--1 - Pensioners ready for payroll enrolment (Activation)--}}
            @if($summary['pensioners_for_payroll_count'] > 0)
                <div class="col-md-3" >
                    <a  href="{{ route('backend.payroll.alert_monitor.index',1)  }}"
                        data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pensioners ready for payroll activation' }}">
                        <div id="search_div1"  class="grid-column">
                            {{ 'Pensioners pending for payroll' }}&nbsp;<span class="badge-summary">{{ $summary['pensioners_for_payroll_count'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif

            {{--2 - Dependents ready for payroll enrolment (Activation)--}}
            @if($summary['dependents_for_payroll_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',2)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Dependents ready for payroll activation' }}">
                        <div  id="search_div2"  class="grid-column">
                            {{ 'Dependents pending for payroll' }}&nbsp;<span class="badge-summary">{{ $summary['dependents_for_payroll_count'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>

            @endif

            {{--3 - Child suspensions - 18yrs old--}}
            @if($summary['child_suspension_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',3)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Suspensions of Child above 18yrs old who may need to be verified / deactivated' }}">
                        <div id="search_div3"  class="grid-column">
                            {{ 'Payroll Child Suspensions' }}&nbsp;<span class="badge-summary">{{ $summary['child_suspension_count'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif

            {{--4 - Child alerts who approach - 18yrs old--}}
            @if($summary['child_limit_age_alert_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',4)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Children who approach 18yrs old (in next few months) who may need to be verified' }}">
                        <div id="search_div4" class="grid-column">
                            {{ 'Payroll Child  Age limit Alerts' }}&nbsp;<span class="badge-summary">{{ $summary['child_limit_age_alert_count'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif

            {{--5 - Retiree limit age count pending--}}

            @if($summary['retiree_suspension_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',5)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Retirees suspensions (Voluntary and compulsory) which require further actions i.e. verification or deactivation' }}">
                        <div id="search_div5"  class="grid-column">
                            {{ 'Payroll Retiree Suspensions' }}&nbsp;<span class="badge-summary">{{ $summary['retiree_suspension_count'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>

            @endif



            {{--6 - Claims need constant care rehistration--}}
            @if($summary['claims_pending_cca'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',6)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Notification / claims which have pending Constant Care Assistant registration' }}">
                        <div id="search_div6"  class="grid-column">
                            {{ 'Notification/ Claims need Constant Care Assistant registration' }}&nbsp;<span class="badge-summary">{{ $summary['claims_pending_cca'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif

            {{--7 - Documents: Pending Verification--}}
            @if($summary['pending_verification_docs'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',7)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pending uploaded documents for beneficiary verification' }}">
                        <div  id="search_div7"  class="grid-column">
                            {{ 'Pending documents for verifications' }}&nbsp;<span class="badge-summary">{{ $summary['pending_verification_docs'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif


            {{--8- Documents: Pending Bank updates--}}
            @if($summary['pending_bank_update_docs'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',8)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pending documents for beneficiary bank details modifications' }}">
                        <div id="search_div8"  class="grid-column">
                            {{ ' Pending documents for Bank modification' }}&nbsp;<span class="badge-summary">{{ $summary['pending_bank_update_docs'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif

            {{--9- Documents: Pending beneficiary updates--}}
            @if($summary['pending_beneficiary_details_docs'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',9)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pending uploaded documents for beneficiary details modifications' }}">
                        <div id="search_div9"  class="grid-column">
                            {{ 'Pending documents for Beneficiary updates ' }}&nbsp;<span class="badge-summary">{{ $summary['pending_beneficiary_details_docs'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif

            {{--10- Pending Recoveries--}}
            {{--@if($summary['pending_payroll_recoveries'] > 0)--}}
            {{--<div class="col-md-3">--}}
            {{--<a href="{{ route('backend.payroll.alert_monitor.index',10)  }}"--}}
            {{--data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pending payroll recoveries i.e deductions, arrears and unclaimed payments approvals' }}">--}}
            {{--<div class="grid-column">--}}
            {{--{{ 'Pending payroll recoveries' }}&nbsp;<span class="badge-summary">{{ $summary['pending_payroll_recoveries'] }}</span><br/>--}}
            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--@endif--}}

            {{--11- Pensioners with missing details--}}
            @if($summary['pensioners_with_missing_details_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',11)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pensioners with missing details i.e Dob, phone and bank details' }}">
                        <div id="search_div11" class="grid-column">
                            {{ 'Pensioners with missing details' }}&nbsp;<span class="badge-summary">{{ $summary['pensioners_with_missing_details_count'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif

            {{--12- Pensioners with missing details--}}
            @if($summary['dependents_with_missing_details_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',12)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Dependents with missing details i.e Dob, phone and bank details' }}">
                        <div id="search_div12"  class="grid-column">
                            {{ 'Dependents with missing details' }}&nbsp;<span class="badge-summary">{{ $summary['dependents_with_missing_details_count'] }}</span><br/>
                            {{--<span style="color: #7f8c8d">&rdsh;&nbsp;{{ $value['group'] }}</span>--}}
                        </div>
                    </a>
                </div>
            @endif


            {{--13- Payroll pending workflows--}}
            @if($summary['payroll_pending_workflow_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',13)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Payroll pending workflow' }}">
                        <div id="search_div13"  class="grid-column">
                            {{ 'Payroll pending workflow' }}&nbsp;<span class="badge-summary">{{ $summary['payroll_pending_workflow_count'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif


            {{--14- Payroll pensioners verification controllers--}}
            @if($summary['payroll_pensioners_verification_alert_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',14)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Payroll Pensioners/Employee Verification Alerts' }}">
                        <div id="search_div14"  class="grid-column">
                            {{ 'Pensioners verification alerts' }}&nbsp;<span class="badge-summary">{{ $summary['payroll_pensioners_verification_alert_count'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif

            {{--15- Payroll dependents verification controllers--}}
            @if($summary['payroll_dependents_verification_alert_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',15)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Payroll Dependents Verification Alerts' }}">
                        <div id="search_div15"  class="grid-column">
                            {{ 'Dependents verification alerts' }}&nbsp;<span class="badge-summary">{{ $summary['payroll_dependents_verification_alert_count'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif

            {{--16- Payroll Beneficiaries on hold--}}
            @if($summary['payroll_beneficiaries_on_hold_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',16)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pensioners/Dependents archived due to pending queries.' }}">
                        <div id="search_div16"  class="grid-column">
                            {{ 'Payroll Beneficiaries ARCHIVED ' }}&nbsp;<span class="badge-summary">{{ $summary['payroll_beneficiaries_on_hold_count'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif

            {{--17 - Payroll all suspended--}}
            @if($summary['payroll_all_suspended'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',17)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ ' All Pensioners/Dependents Suspended.' }}">
                        <div id="search_div17"  class="grid-column">
                            {{ 'Suspended Payroll Beneficiaries ' }}&nbsp;<span class="badge-summary">{{ $summary['payroll_all_suspended'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif

            {{--18 child_alert_edu_ext_count--}}
            @if($summary['child_alert_edu_ext_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',18)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ ' Child Alerts with Education extensions approaching deadline ' }}">
                        <div id="search_div18"  class="grid-column">
                            {{ 'Child Alerts with Education extension' }}&nbsp;<span class="badge-summary">{{ $summary['child_alert_edu_ext_count'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif

            {{--19 child_alert_disability_ext_count--}}
            @if($summary['child_alert_disability_ext_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',19)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ ' Child Alerts with Disability extensions approaching deadline ' }}">
                        <div id="search_div19"  class="grid-column">
                            {{ 'Child Alerts with Disability extension' }}&nbsp;<span class="badge-summary">{{ $summary['child_alert_disability_ext_count'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif

            {{--20 retiree_alert_count--}}
            {{--@if(env('TESTING_MODE') == 1)--}}
            @if($summary['retiree_pensioners_alerts_count'] > 0)
                <div class="col-md-3">
                    <a href="{{ route('backend.payroll.alert_monitor.index',20)  }}"
                       data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="{{ 'Pensioners approaching retirement Alerts' }}">
                        <div id="search_div20"  class="grid-column">
                            {{ 'Pensioners approaching retirement Alerts' }}&nbsp;<span class="badge-summary">{{ $summary['retiree_pensioners_alerts_count'] }}</span><br/>

                        </div>
                    </a>
                </div>
            @endif
            {{--@endif--}}
        </div>

    </div>
</div>



@push('after-script-end')
    <!-- Custom javascript files for this page -->
    <script>
        $(function() {
            var searched_index = '{{ $alert_reference  }}';
            var searched_div_id = 'search_div' + searched_index;
            $('#'+searched_div_id).css({background: '#dbe9f3'});


            {{--$(document).on('click', '#export_excel', function () {--}}

                {{--window.open(base_url + "/payroll/alert_monitor/export_excel/" + '{{ $alert_reference }}', "_self");--}}
            {{--});--}}

        });
    </script>
@endpush