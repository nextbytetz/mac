
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="pensioners-table">
            <thead>
            <tr >
                <th>Filename</th>
                <th>Fullname</th>
                <th>Benefit Type</th>
                <th>Monthly Pension</th>
                {{--<th>Payment Type</th>--}}

            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#pensioners-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.pensioner.get_ready_for_payroll_datatable') !!}',
                    type : 'post',
                    data: function ($d) {
                        $d.isinbox_task = '{{ isset($isinbox_task) ? $isinbox_task : null }}';
                    },
                },
                columns: [
                    { data: 'filename', name: 'notification_reports.filename'},
                    { data: 'fullname' , name: 'pensioners.firstname', orderable : true, searchable : true},
                    { data: 'benefit_type', name: 'benefit_types.name',  orderable : false, searchable : false},
                    { data: 'mp', name: 'pensioners.monthly_pension_amount',  orderable : true, searchable : true},
                    // { data: 'payment_type', name: 'compensation_payment_types.name' ,orderable : false, searchable : false},
                    /*invisible fields*/
                    { data: 'middlename', name: 'pensioners.middlename' ,  orderable : false, searchable : true, visible: false},
                    { data: 'lastname', name: 'pensioners.lastname' ,  orderable : false, searchable : true,visible: false }
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
