
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="manual-member-arrears-table">
            <thead>
            <tr >
                <th>Member Type</th>
                <th>Case no</th>
                <th>Name</th>
                <th>DOB</th>
                <th>Pending Months (Arrears)</th>
                <th>Monthly Pension</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>

    </div>
</div>




@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#manual-member-arrears-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.manual_member.get_members_with_pending_arrears') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type', name: 'member_type',orderable : false, searchable : false},
                    { data: 'case_no', name: 'case_no'},
                    { data: 'name', name: 'name',  orderable : true, searchable : true},
                    { data: 'dob_formatted', name: 'dob',  orderable : true, searchable : true},
                    { data: 'pending_pay_months' , name: 'pending_pay_months', orderable : true, searchable : true},
                    { data: 'mp_formatted', name: 'mp',  orderable : true, searchable : true},
                    { data: 'action', name: 'action',  orderable : false, searchable : false},
                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
