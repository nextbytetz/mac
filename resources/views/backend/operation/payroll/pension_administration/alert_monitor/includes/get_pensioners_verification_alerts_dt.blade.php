
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="pensioners-table">
            <thead>
            <tr >
                <th>Filename/Case no</th>
                <th>Fullname</th>
                <th>DOB</th>
                <th>Last Verification</th>
                <th>Days left until next verification</th>

            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#pensioners-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.verification.get_pensioners_verification_alerts') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'filename', name: 'filename',  orderable : false, searchable : false},
                    { data: 'member_name' , name: 'b.member_name', orderable : true, searchable : true},
                    { data: 'dob', name: 'dob',  orderable : false, searchable : false},
                    { data: 'lastresponse', name: 'lastresponse',  orderable : false, searchable : false},
                    { data: 'days_left', name: 'days_left',  orderable : false, searchable : false},
                    // { data: 'middlename', name: 'pensioners.middlename' ,  orderable : false, searchable : true, visible: false},
                    // { data: 'lastname', name: 'lastname' ,  orderable : false, searchable : true,visible: false },
                    // { data: 'filename', name: 'notification_reports.filename', visible: false},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['resource_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
