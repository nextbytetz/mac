{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="payroll-all-suspended-table">
            <thead>
            <tr >
                <th>Filename</th>
                <th>Member Type</th>
                <th>Member Name</th>
                <th>Suspended Date</th>
                <th>Last Response</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#payroll-all-suspended-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.get_all_suspended_beneficiaries_dt') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'filename', name: 'b.filename', orderable : true, searchable : true},
                    { data: 'member_type_name', name: 't.name', orderable : true, searchable : true},
                    { data: 'member_name' , name: 'b.member_name', orderable : true, searchable : true},
                    { data: 'suspended_date', name: 'b.suspended_date',  orderable : true, searchable : false},
                    { data: 'lastresponse', name: 'b.lastresponse',  orderable : true, searchable : true},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/beneficiary_profile/" + aData['member_type_id'] + "/" + aData['resource_id'] + "/" + aData['employee_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush