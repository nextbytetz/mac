{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="beneficiaries-missing-table">
            <thead>
            <tr >
                <th>Filename</th>
                <th>Member Name</th>
                <th>Missing Details</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            var member_type = '{!! $member_type_id !!}';

            // if(member_type == 5){
            //     url = url +  "/payroll/beneficiary_profile/"+ member_type +  '/' +  aData['id'] + '/' + aData['employee_id_formatted'] ;
            // }else{
            //     url =  url +  "/compliance/dependent/"+ "profile/" ;
            // }


            $('#beneficiaries-missing-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.alert_monitor.get_beneficiaries_missing_details', $member_type_id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'filename', name: 'b.filename', orderable : true, searchable : true},
                    { data: 'member_name', name: 'b.member_name', orderable : true, searchable : true},
                    { data: 'missing_details' , name: 'missing_details', orderable : true, searchable : true},
                ],

                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href =  url +  "/payroll/beneficiary_profile/"+ member_type +  '/' +  aData['id'] + '/' + aData['employee_id_formatted'] ;

                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush