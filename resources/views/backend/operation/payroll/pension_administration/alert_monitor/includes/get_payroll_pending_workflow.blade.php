
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="pending-workflow-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Member name</th>
                <th>Filename</th>
                <th>Workflow name</th>
                <th>Member Type</th>
                <th>Initiated by</th>
                <th>Pending level</th>
                <th>Pending At</th>
                <th>Created At</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        // $("#recovery_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#pending-workflow-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                searching: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.pending_workflow') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_name', name: 'member_name', orderable : true, searchable : true},
                    { data: 'filename' , name: 'filename', orderable : false, searchable : false},
                    { data: 'workflow_name', name: 'workflow_name',  orderable : true, searchable : true},
                    { data: 'member_type_name', name: 'member_type_name',  orderable : true, searchable : true},
                    { data: 'initiated_by', name: 'initiated_by',  orderable : true, searchable : true},
                    { data: 'pending_level', name: 'pending_level' ,orderable : true, searchable : true},
                    { data: 'pending_user', name: 'pending_user' ,orderable : false, searchable : false},
                    { data: 'created_at', name: 'created_at' ,orderable : false, searchable : false},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href =  url +  "/payroll/beneficiary_profile/"+ aData['member_type_id'] +  '/' +  aData['resource_id'] + '/' + aData['employee_id']
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

            // });

        });
    </script>;

@endpush
