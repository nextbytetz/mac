{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="claims-cca-alerts-table">
            <thead>
            <tr >
                <th>Filename</th>
                <th>Employee</th>
                <th>Incident Type</th>
                <th>Incident Date</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#claims-cca-alerts-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.notification_report.get_claim_pending_cca') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'filename', name: 'filename', orderable : true, searchable : true},
                    { data: 'employee' , name: 'employee', orderable : true, searchable : true},
                    { data: 'incident_type', name: 'incident_type',  orderable : true, searchable : true},
                    { data: 'incident_date', name: 'incident_date',visible:true, orderable : true, searchable : true},
                    { data: 'action', name: 'action',visible:true, orderable : false, searchable : false},

                ],


            });

        });


    </script>;

@endpush