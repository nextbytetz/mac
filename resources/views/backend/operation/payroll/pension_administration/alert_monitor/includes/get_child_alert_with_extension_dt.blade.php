{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="payroll-child-alerts-edu-ext-table">
            <thead>
            <tr >
                <th>Child</th>
                <th>Dob</th>
                <th>Deadline Date</th>
                <th>Days left</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#payroll-child-alerts-edu-ext-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.child_suspension.get_child_alert_ext_dt', $child_continuation_reason_cv_ref) !!}',
                    type : 'post',          data: function ($d) {
                        $d.isinbox_task = '{{ isset($isinbox_task) ? $isinbox_task : null }}';
                    },
                },
                columns: [
                    { data: 'fullname', name: 'firstname', orderable : true, searchable : true},
                    { data: 'dob_formatted' , name: 'dependents.dob', orderable : true, searchable : true},
                    { data: 'deadline_date' , name: 'dependent_employee.deadline_date', orderable : false, searchable : false},
                    { data: 'days_left', name: 'days_left',  orderable : true, searchable : true},
                    { data: 'fullname', name: 'middlename',visible:false, orderable : true, searchable : true},
                    { data: 'fullname', name: 'lastname', visible:false, orderable : true, searchable : true},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/dependent/"+ "profile/" + aData['pivot_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush