
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="pensioners-table">
            <thead>
            <tr >
                <th>Filename/Case no</th>
                <th>Fullname</th>
                <th>DOB</th>
                <th>Age</th>
                <th>Next Followup Date</th>
                <th>Days left until retirement/next followup</th>
                <th>action</th>

            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#pensioners-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.retiree.followup.get_alerts_by_member_type_dt', $member_type_id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'filename', name: 'b.filename',  orderable : true, searchable : true},
                    { data: 'member_name' , name: 'b.member_name', orderable : true, searchable : true},
                    { data: 'dob', name: 'b.dob',  orderable : false, searchable : false},
                    { data: 'age', name: 'age',  orderable : false, searchable : false},
                    { data: 'next_followup_date', name: 'next_followup_date',  orderable : false, searchable : false},
                    { data: 'days_left', name: 'days_left',  orderable : false, searchable : false},
                    { data: 'actions', name: 'actions',  orderable : false, searchable : false},
                    // { data: 'middlename', name: 'pensioners.middlename' ,  orderable : false, searchable : true, visible: false},
                    // { data: 'lastname', name: 'lastname' ,  orderable : false, searchable : true,visible: false },
                    // { data: 'filename', name: 'notification_reports.filename', visible: false},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['resource_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
