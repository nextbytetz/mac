
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="recoveries-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Remark</th>
                <th>Recovery Type</th>
                <th>Total Amount</th>
                <th>Cycles</th>
                <th>Date created</th>
                <th>Date approved</th>
                <th>Initiated By</th>
                        </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        // $("#recovery_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#recoveries-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.recovery.get_pending_for_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'remark', name: 'remark', orderable : true, searchable : true},
                    { data: 'recovery_type' , name: 'recovery_type', orderable : false, searchable : false},
                    { data: 'total_amount_formatted', name: 'total_amount',  orderable : true, searchable : true},
                    { data: 'cycles', name: 'cycles',  orderable : true, searchable : true},
                    { data: 'created_at_formatted', name: 'created_at',  orderable : true, searchable : true},
                    { data: 'wf_done_date_formatted', name: 'wf_done_date' ,orderable : true, searchable : true},
                    { data: 'user', name: 'user' ,orderable : false, searchable : false},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/recovery/"+ "profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        // });

        });
    </script>;

@endpush
