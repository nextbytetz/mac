{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="beneficiaries-hold-table">
            <thead>
            <tr >
                <th>Member Type</th>
                <th>Payee</th>
                <th>Archived Date</th>
                {{--<th>Hold Reason</th>--}}
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#beneficiaries-hold-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.beneficiary.get_held_for_dt') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type_name', name: 't.name', orderable : true, searchable : true},
                    { data: 'member_name', name: 'b.member_name', orderable : true, searchable : true},
                    { data: 'hold_date', name: 'b.hold_date', orderable : true, searchable : true},


                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href =  url +  "/payroll/beneficiary_profile/"+ aData['member_type_id'] +  '/' +  aData['resource_id'] + '/' + aData['employee_id']
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush