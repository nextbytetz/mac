{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="{{ 'pending-documents-table' . $document_type }}">
            <thead>
            <tr >
                <th>Filename</th>
                <th>Member Type</th>
                <th>Payee</th>
                <th>Document Type</th>
                <th>Description</th>
                <th>Folio</th>
                <th>Received Date</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#pending-documents-table' + '{{ $document_type }}').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.get_pending_beneficiary_documents', ['document_type'=> $document_type]) !!}',
                    type : 'post',
                    data: function ($d) {
                        $d.isinbox_task = '{{ isset($isinbox_task) ? $isinbox_task : null }}';
                    },
                },
                columns: [
                    { data: 'filename', name: 'b.filename', orderable : true, searchable : true},
                    { data: 'member_type_name', name: 'member_types.name', orderable : true, searchable : true},
                    { data: 'member_name', name: 'b.member_name', orderable : true, searchable : true},
                    { data: 'document_type_name' , name: 'documents.name', orderable : true, searchable : true},
                    { data: 'description', name: 'document_payroll_beneficiary.description',  orderable : true, searchable : true},
                    { data: 'folio', name: 'document_payroll_beneficiary.folio',  orderable : true, searchable : true},
                    { data: 'received_date', name: 'document_payroll_beneficiary.doc_receive_date',visible:true, orderable : true, searchable : true},
                    // { data: 'payee_name', name: 'pensioners.lastname',visible:false, orderable : true, searchable : true},
                    // { data: 'payee_name', name: 'dependents.firstname',visible:false, orderable : true, searchable : true},
                    // { data: 'payee_name', name: 'dependents.lastname',visible:false, orderable : true, searchable : true},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/get_pending_beneficiary_documents_action/" + aData['member_type_id'] + '/'  + aData['resource_id'] + '/'  + aData['document_id']
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush