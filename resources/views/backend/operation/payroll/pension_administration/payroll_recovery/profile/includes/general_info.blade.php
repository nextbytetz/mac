

{{--General info--}}
<br/>
<div class="row">
    {{--left general  summary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Payroll Recovery Details - {!! $recovery_type->name !!}</legend>
            <table class="table table-striped table-bordered">
                <tbody>

                {{--Payroll recovery--}}
                <tr>
                    <td width="120px">Total Amount</td>
                    <th>{!! Form::label( 'bank',
                           number_2_format($payroll_recovery->total_amount)) !!} </th>
                </tr>

                {{--ccycles--}}
                <tr>
                    <td>Cycles</td>
                    <th>
                        {!!   $payroll_recovery->cycles
                                    !!}</th>
                </tr>

                {{--Amount per cycles--}}
                <tr>
                    <td>Amount Per Cycle</td>
                    <th>
                        {!! number_2_format($amount_per_cycle)
                                    !!}</th>
                </tr>

                @if($recovery_type->reference == 'PRTOVEP')

                    <tr>
                        <td>Last cycle amount</td>
                        <th>
                            {!! number_2_format($last_cycle_amount)
                                        !!}</th>
                    </tr>
                @endif

                </tbody>
            </table>

        </div>
    </div>




    {{--Right summary--}}

    <div class="col-md-6">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">General Details</legend>
            <table class="table table-striped table-bordered">
                <tbody>

                {{--mp--}}
                <tr>
                    <td>Monthly Pension</td>
                    <th>
                    {!! number_2_format($mp)
                    !!}</th>
                </tr>

                {{--bank--}}
                <tr>
                    <td width="120px">Bank</td>
                    <th>{!! Form::label( 'bank',
                           ($resource->bank_branch_id) ? $resource->BankBranch->bank->name : (isset($resource->bank_id) ? $resource->bank->name : ' '), [])
                                !!} </th>
                </tr>

                {{--Branch--}}
                <tr>
                    <td>Branch</td>
                    <th>
                        {!!   isset($resource->bank_branch_id) ? $resource->BankBranch->name : ' '
                                    !!}</th>
                </tr>

                {{--Accountno--}}
                <tr>
                    <td>Account No.</td>
                    <th>
                        {!! $resource->accountno
                                    !!}</th>
                </tr>




                </tbody>
            </table>

        </div>
    </div>



</div>



