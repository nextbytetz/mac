@extends('layouts.backend.main', ['title' =>   $recovery_type->name .  ' Approval Profile', 'header_title' => $recovery_type->name .  ' Approval Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>

        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id,  'employee_id' => $payroll_recovery->employee_id])
    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >

                                        @if($check_if_pending_payment == true)
                                            <span>
                                           {!! HTML::decode(link_to_route('backend.payroll.recovery.open_cancel_page', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Cancel', $payroll_recovery->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to cancel this payroll recovery?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>
                                        @endif


                                        @if($check_pending_level1 == 1 && $payroll_recovery->wf_done == 0)
                                            <span>
                                        <a href="{!! route('backend.payroll.recovery.edit',$payroll_recovery->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;
                                            Edit
                                            Recovery</a>
                                     </span>


                                            {{--<span>--}}
                                            {{--<a href="{!! route('backend.payroll.edit_new_bank',$bank_update->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa--}}
                                            {{--fa-reply"></i>&nbsp;Undo</a>--}}
                                            {{--</span>--}}


                                            <span>

                                           {!! HTML::decode(link_to_route('backend.payroll.recovery.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $payroll_recovery->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this payroll recovery?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                        @endif

                                        @if($member_type_id == 5)

                                            {!!                                          HTML::decode( link_to('payroll/pensioner/profile/' . $resource->id . '#recovery',trans('buttons.general.close'),['id'=> 'close', 'class' => 'tn btn-primary site-btn nav_button icon fa
                                            fa-close', ])) !!}

                                            {{--</a>--}}
                                        @elseif($member_type_id == 4)
                                            {!!                                          HTML::decode( link_to('compliance/dependent/profile/' . $dependent_employee->id . '#recovery',trans('buttons.general.close'),['id'=> 'close', 'class' => 'tn btn-primary site-btn nav_button icon fa
                                            fa-close', ])) !!}
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--general info--}}
                                    @include('backend/operation/payroll/pension_administration/payroll_recovery/profile/includes/general_info')
                                    {{--Remark--}}
                                    <div class="col-md-6">
                                    <span>
                                        <b>Remark:</b> {!! $payroll_recovery->remark !!}
                                    </span>
                                    </div>
                                    <div>&nbsp;</div>
                                    {{--APprovals tabs history--}}
                                    {{--@include('backend/operation/payroll/pension_administration/profile/pensioner/includes/general/approvals')--}}
                                    {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}

                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {

        });
    </script>;

@endpush
