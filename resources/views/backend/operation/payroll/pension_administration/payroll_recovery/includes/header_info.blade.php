<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">


    <span class="navbar-brand mb-0 h5"> {!! ($member_type_id == 5) ? 'Pensioner'  : 'Dependent'!!}&nbsp;:&nbsp;<small class="underline"> <a href="{!! ($member_type_id == 5) ? route('backend.payroll.pensioner.profile', $resource->id)  : route('backend.compliance.dependent.profile', $resource->id) !!}">{!! $resource->name !!}</a></small></span>

    <span class="navbar-brand mb-0 h5"> {!! 'Action: Capture ' . $recovery_type->name !!}</span>
</nav>
<legend></legend>
<br/>