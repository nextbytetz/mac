
@extends('layouts.backend.main', ['title' => 'Cancel payroll recovery', 'header_title' => 'Cancel payroll recovery'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    @include('backend.includes.assets.datetimepicker')
    <style>


    </style>
@endpush

@section('content')


    {!! Form::model($payroll_recovery,['route' => ['backend.payroll.recovery.cancel',$payroll_recovery->id ],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}
    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $payroll_recovery->member_type_id, 'employee_id' => $payroll_recovery->employee_id])
    </div>

    <legend>{!! 'Remark: ' . $payroll_recovery->remark !!}</legend>
    {{--cancel reason--}}
<br/>
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Cancel reason:</label></div>
                <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group text_content">

                        {!! Form::textarea('cancel_reason', null, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                        {!! $errors->first('cancel_reason', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>




    {{--button--}}
    <div class="row" align="center">
        <div class="col-md-12">
            <div class="element-form" >
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                <div class="col-xs-7 col-lg-7 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        {!! link_to('payroll/recovery/profile/' . $payroll_recovery->id ,trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>








    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();




        });
    </script>;

@endpush







