@extends('layouts.backend.main', ['title' => 'Capture  Manual ' . $recovery_type->name, 'header_title' => 'Capture Manual ' . $recovery_type->name])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>


    </style>
@endpush

@section('content')

    {!! Form::model($resource,['route' => ['backend.payroll.recovery.store', $resource->id],'method'=>'post',  'id' => 'create', 'name' => 'create']) !!}
    {{--{!! Form::open(['route' => ['backend.payroll.recovery.store' ],'method'=>'post', 'name' => 'store', 'id' => 'store']) !!}--}}
    <div class = "row">
        @include("backend/operation/payroll/pension_administration/payroll_recovery/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'recovery_type' => $recovery_type, 'employee_id' => $employee_id])

    </div>

    {!! Form::hidden('recovery_type_id', $recovery_type->id) !!}
    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}
    {!! Form::hidden('resource_id', $resource->id) !!}
    {!! Form::hidden('ismanual', 1) !!}
    {!! Form::hidden('manual_payroll_member_id', $manual_payroll_member_id) !!}
    {{--mp--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Monthly Pension:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','mp', number_2_format($mp) , ['class' => 'form-control', 'disabled', ]) !!}
                        {!! $errors->first('mp', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--Total amount--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Total Amount:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','total_amount', number_2_format($total_arrears), ['class' => 'form-control compute_amount_per_cycle money','id' => 'total_amount', ]) !!}
                        {!! $errors->first('total_amount', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">Total amount to be recovered</small>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Pending Months:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','pending_pay_months', $arrears_months, ['class' => 'form-control  number','id' => 'pending_pay_Months','disabled' ]) !!}
                        {!! $errors->first('pending_pay_months', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">No. of arrears months</small>
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--cycles--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Months:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','cycles', 1, ['class' => 'form-control number compute_amount_per_cycle', 'id' => 'cycles' ,'disabled']) !!}
                        {!! $errors->first('cycles', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">Number of months for total amount to be full recovered (added / deducted) from Monthly Pension</small>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Amount per cycle--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Amount Per Month:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','amount', null, ['class' => 'form-control','disabled', 'id' => 'amount_per_cycle' ]) !!}
                        {!! $errors->first('amount', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">Amount to be recovered per month</small>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--Last amount cycle--}}

    <div class="row" >
        <div class="col-md-9" id="last_cycle_amount_div">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Last Month Amount:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','last_cycle_amount', null, ['class' => 'form-control','disabled', 'id' => 'last_cycle_amount' ]) !!}
                        {!! $errors->first('last_cycle_amount', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">Amount to be recovered on the last month (remainder amount)</small>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--remark--}}
    <div class="row" >
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Remarks:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::textarea('remark', $remark, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                        {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}

                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--buttons--}}
    <div class="row" align="center">
        <div class="col-md-12">
            <div class="element-form" >
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        {!! link_to('payroll/alert_monitor/index/' . 6 ,trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();


            $(".save_button").click(function(event) {
                $("#cycles" ).prop("disabled", false);
                store.submit();
            });

            calculateAmountPerCycle();
            $('body').off('keyup', '.compute_amount_per_cycle').on('keyup', '.compute_amount_per_cycle', function(e) {
                calculateAmountPerCycle();
            });

            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });
            /*Hide last cycle amount div*/
            $("#last_cycle_amount_div" ).hide();

            /*Calculate amount per cycles*/
            function calculateAmountPerCycle()
            {
                var $recover_type_ref = '{!! $recovery_type->reference !!}';

                switch ($recover_type_ref){
                    case 'PRTUNDP':
                        calculateForArrears();
                        break;

                    case 'PRTOVEP':
                        calculateForDeduction();
                        break;

                    case 'PRTUNCLP':
                        calculateForArrears();
                        break;
                }
            }


            /*Calculate cycles and amount per cycles for arrears*/
            function calculateForArrears()
            {
                var $mp = '{!! $mp  !!}';
                var $total_amount =$('#total_amount').val();
                $total_amount = $total_amount.replace(/,/g, "");
                var $cycles = $('#cycles').val();
                $cycles = returnCycles($cycles);
                var $amount_per_cycle = 0;
                $amount_per_cycle = ($total_amount / $cycles).toFixed(2);
                $amount_per_cycle  = formatNumber($amount_per_cycle);
                $('#amount_per_cycle').val($amount_per_cycle).change();
            }



            /*Calculate cycles and amount per cycles for deduction*/
            function calculateForDeduction()
            {
                var $mp = '{!! $mp  !!}';
                var $total_amount =$('#total_amount').val();
                $total_amount = $total_amount.replace(/,/g, "");
                var $cycles = $total_amount / $mp;
                var $cycles_ceil = Math.ceil($cycles);
                var $amount_per_cycle = 0;
                // alert( 'Mp:' + $mp + ', total:' + $total_amount);
                $amount_per_cycle = (parseFloat($mp) > parseFloat($total_amount)) ? $total_amount : $mp;

                $amount_per_cycle  = formatNumber($amount_per_cycle);
                $('#amount_per_cycle').val($amount_per_cycle).change();
                $('#cycles').val($cycles_ceil).change();
                $("#cycles" ).prop("disabled", true);
                /*Check and Compute last cycle amount*/
                computeLastCycleAmount($mp, $total_amount, $cycles, $amount_per_cycle);
            }

            /*Compute last cycle amount when cycles has reminder portion*/
            function computeLastCycleAmount($mp, $total_amount, $cycles, $amount_per_cycle)
            {
                var $remain_amount = parseFloat($total_amount) % parseFloat($mp);
                var $last_cycle_amount = 0;

                if ($remain_amount > 0 && $cycles > 1){
                    $last_cycle_amount = $remain_amount.toFixed(2);
                    $('#last_cycle_amount').val(formatNumber($last_cycle_amount)).change();
                    $("#last_cycle_amount_div" ).show();
                }else{
                    $('#last_cycle_amount').val($amount_per_cycle).change();
                    $("#last_cycle_amount_div" ).hide();
                }
            }



            /*Resolve number cycles specified on the textbox*/
            function returnCycles($cycles)
            {
                if($cycles < 1 || $cycles == '')
                {
                    $cycles = 1;
                }
                return $cycles;
            }


            /*Format number with thousand separator*/
            function formatNumber(num) {
                return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
            }




            /* start : mask all money input */
            $('.money').maskMoney({
                precision : 2,
                allowZero : false,
                affixesStay : false
            });

            /* start : ensure only numbers are input on monetary boxes */
            function number_only(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });
    </script>;

@endpush
