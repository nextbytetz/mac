
@extends('layouts.backend.main', ['title' => 'Choose Recovery Type', 'header_title' => 'Choose Recovery Type'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>


    </style>
@endpush

@section('content')


    {!! Form::open(['route' => ['backend.payroll.recovery.create','member_type' => $member_type_id],'method'=>'get', 'name' => 'create']) !!}

    <div class = "row">

        <nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">


            <span class="navbar-brand mb-0 h5"> {!! ($member_type_id == 5) ? 'Pensioner'  : 'Dependent'!!}&nbsp;:&nbsp;<small class="underline"> <a href="{!! ($member_type_id == 5) ? route('backend.payroll.pensioner.profile', $resource->id)  : route('backend.compliance.dependent.profile', $resource->id) !!}">{!! $resource->name !!}</a></small></span>

            <span class="navbar-brand mb-0 h5"> {!! 'Action: ' . 'Choose Recovery Type' !!}</span>
        </nav>
        <legend></legend>
        <br/>
    </div>

    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}
    {!! Form::hidden('resource_id', $resource->id) !!}


    {{--Remark--}}

    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Choose Recovery Type:</label></div>
                <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group text_content">

                        {!! Form::select('recovery_type', $recovery_types, [], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> '']) !!}
                        {!! $errors->first('recovery_type', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>





    {{--button--}}
    <div class="row" align="center">
        <div class="col-md-12">
            <div class="element-form" >
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                <div class="col-xs-7 col-lg-7 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        @if($member_type_id == 5)
                            {!! link_to('payroll/pensioner/profile/' .  $resource->id . '#dependents',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @elseif($member_type_id == 4)
                            {!! link_to('compliance/dependent/profile/' .  $dependent_employee->id . '#dependents',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @endif
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>








    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">


        $(function () {

            $(".search-select").select2();




        });
    </script>;

@endpush
