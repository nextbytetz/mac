@extends('layouts.backend.main', ['title' => 'Process Manual Suspension', 'header_title' => 'Process Manual Suspension'])

@push('after-styles-end')
    @include('backend.includes.assets.datetimepicker')
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::open(['route' => ['backend.payroll.suspension.process'],'method'=>'post',
    'name' => 'create']) !!}
    {!! Form::hidden('today_date', getTodayDate(), ['class' =>'this_date']) !!}

    <div>&nbsp;</div>

    {{--main contants--}}

    {{--Start date--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Run Date:</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="input-group" style="width:100%;">
                            {!! Form::text('run_date', null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>

                        <small class="form-text text-muted" style="width:100% !important;">Start date i.e. start of the verification of beneficiaries</small>
                        {!! $errors->first('run_date', '<span class="help-block label label-danger">:message</span>') !!}

                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Remark:</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {{ Form::textarea('remark',null,['class'=>'form-control', '', 'id' => 'remark','placeholder' => '', 'autocomplete' => 'off']) }}
                        {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>

                </div>
            </div>
        </div>


    </div>


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.payroll.suspension.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">

        $(function () {
            $(".search-select").select2();

            /*------------Start Date Process ---------*/

            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            /*-----------End Date Process------------*/




        });


    </script>;


@endpush
