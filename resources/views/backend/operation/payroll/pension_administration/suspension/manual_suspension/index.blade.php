@extends('layouts.backend.main', ['title' => 'Payroll Manual Suspensions', 'header_title' => 'Payroll Manual Suspensions'])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12" >
                <div class="pull-right">

                    <div class="btn-group">
                        <a href="{!! route('backend.payroll.suspension.create') !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-money"></i>&nbsp;New suspension</a>
                    </div>
                </div>



            </div>
        </div>


    </div>

    <br/>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="payroll-suspensions-table">
                <thead>
                <tr >
                    <th>Run date</th>
                    <th>Run By</th>
                    <th>Remark</th>
                    <th>Created At</th>

                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#payroll-suspensions-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.suspension.get_for_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'run_date_formatted', name: 'run_date', orderable : false, searchable : false},
                    { data: 'user' , name: 'user', orderable : false, searchable : false},
                    { data: 'remark' , name: 'remark', orderable : false, searchable : false},
                    { data: 'created_at_formatted', name: 'created_at',  orderable : true, searchable : true},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/suspension/profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
