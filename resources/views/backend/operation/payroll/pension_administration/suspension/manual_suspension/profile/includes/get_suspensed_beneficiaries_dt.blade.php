{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="manual-suspensions-table">
            <thead>
            <tr >
                <th>Member Type</th>
                <th>Name</th>
                <th>Last Response</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#manual-suspensions-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.suspension.get_suspensions_by_manual_for_dt', $payroll_manual_suspension->id  ) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type', name: 'member_type', orderable : false, searchable : false},
                    { data: 'member_name' , name: 'b.member_name', orderable : true, searchable : true},
                        { data: 'lastresponse', name: 'b.lastresponse',  orderable : true, searchable : true},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/beneficiary_profile/" + aData['member_type_id'] + "/" + aData['resource_id'] + "/" + aData['employee_id']  ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush