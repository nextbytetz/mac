@extends('layouts.backend.main', ['title' => 'Suspension Profile' , 'header_title' => 'Suspension Profile'])

@include('backend/includes/assets/sweetalert_assets')
@include('backend.includes.datatable_assets')
@push('after-styles-end')
    <style>
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item active">
                        <a class="nav-link active" href="#general" data-toggle="tab">General</a>
                    </li>
                </ul>

                {{--Start: Tab Contents--}}
                <div class="nav_tab_contain tab-content">
                    <div id="general" class="nav_tab_pane tab-pane active in">

                        <div class = "row nav_tab_pane_header">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    {{--<a href="{{ route('form_input.edit',$form_input->id) }}" class="btn btn-primary site-btn nav_button"><i class="icon fa fa-edit"></i> {{ 'Edit' }}</a>--}}
                                    {{--{!! HTML::decode(link_to_route('form_input.delete',"<i class='icon fa fa-reply' aria-hidden='true'>/i> " . trans('label.crud.delete'), [$form_input->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this entry', 'class' => 'btn btn-danger btn-xs'])) !!}--}}
                                    <a href="{{ route('backend.payroll.suspension.index') }}" class="btn btn-primary site-btn nav_button"><i class="icon fa fa-close"></i> {{ ' close ' }}</a>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-9">
                                <legend style="background-color: lightgray; color: grey;"> {{ 'General Information' }}</legend>

                              @include('backend/operation/payroll/pension_administration/suspension/manual_suspension/profile/includes/get_suspensed_beneficiaries_dt')

                            </div>

                            <div class="col-md-3">
                                <legend style="background-color: lightgray; color: grey;"> {{ 'Sidebar Summary' }}</legend>
                                <table class="table table-striped table-bordered" id="sidebar_summary">
                                    <tbody>
                                    <tr>
                                        <th width="130px">{{ 'Run date'  }}</th>
                                        <td>{{ short_date_format($payroll_manual_suspension->run_date)  }}</td>
                                    </tr>

                                    <tr>
                                        <th >{{ 'Run by'  }}</th>
                                        <td>{{ ($payroll_manual_suspension->user->username)  }}</td>
                                    </tr>

                                    <tr>
                                        <th >{{ 'Remark'  }}</th>
                                        <td>{{ ($payroll_manual_suspension->remark)  }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script-end')
    <script>
        $(function() {
        });
    </script>
@endpush
