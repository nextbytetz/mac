@extends('layouts.backend.main', ['title' => 'Pensioner Profile', 'header_title' => 'Pensioner Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>


        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row">

        @include("backend/operation/payroll/pension_administration/profile/pensioner/includes/header_info",['pensioner'=> $pensioner])

    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>
                    {{--monthly pension--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "mp_header"
                           href="#mp" data-toggle="tab">Monthly Pension</a>
                    </li>
                    {{--arrears--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "arrears_header"
                           href="#arrears" data-toggle="tab">Arrears</a>
                    </li>
                    {{--deductions--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "deductions_header"
                           href="#deductions" data-toggle="tab">Deductions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id = "unclaimed_header"
                           href="#unclaimed" data-toggle="tab">Unclaimed Payments</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id = "suspended_payment_header"
                           href="#suspended_payment" data-toggle="tab">Suspended Payments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id = "document_center_header"
                           href="#document_center" data-toggle="tab">Document Centre</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id = "constant_care_header"
                           href="#constant_care" data-toggle="tab">Constant Care Assistants</a>
                    </li>
                    @if($pensioner->retireeFollowups()->count() > 0)
                        <li class="nav-item">
                            <a class="nav-link" id = "retiree_followup_header"
                               href="#retiree_followup" data-toggle="tab">Retiree Followups</a>
                        </li>
                    @endif

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >

                                        @include('backend/operation/payroll/pension_administration/profile/includes/action_links', ['resource' => $pensioner, 'member_type_id' => 5, 'notification_report_id' =>
                                        $pensioner->notification_report_id, 'suspense_flag' => $pensioner->suspense_flag , 'isactive' => $pensioner->isactive, 'employee_id' => $pensioner->employee_id, ])



                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--general info--}}
                                    @include('backend/operation/payroll/pension_administration/profile/pensioner/includes/general/general_info')

                                    {{--disease adjust overview--}}
                                    <div>&nbsp;</div>
                                    {{--APprovals tabs history--}}
                                    @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/change_updates_history_tab', ['member_type_id'=> 5, 'resource_id' => $pensioner->id, 'notification_report_id' => $pensioner->notification_report_id, 'employee_id' => $pensioner->employee_id, 'resource' => $pensioner])
                                    <div>&nbsp;</div>
                                    {{--@include('backend.operation.compliance.member.employee.includes.death_overview')--}}
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/payroll/pension_administration/profile/pensioner/includes/general/sidebar_summary')
                                    <br/>
                                    {{--pension summary--}}
                                    @if($check_if_is_paid && isset($pensioner->notification_report_id))
                                        {{--For system notification files--}}
                                        {{--@include('backend/operation/payroll/pension_administration/profile/includes/pension_summary_table', ['firstpay_flag' => $pensioner->firstpay_flag, 'status_label' => (($pensioner->firstpay_flag == 1) ? $pensioner->mp_payment_status_label : null), 'last_payroll_run_date' => $last_payroll_run_date, 'mp_payment_summary' => $mp_payment_summary])--}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>



                    <div id="mp" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/pension_administration/profile/includes/monthly_pension_tab/get_monthly_pensions' ,['member_type_id' => 5, 'resource_id' =>
                         $pensioner->id, 'employee_id' => $pensioner->employee_id])

                        @if($pensioner->manualPayrollRuns()->count() > 0)
                            <br/>

                            @include('backend/operation/payroll/pension_administration/profile/includes/monthly_pension_tab/get_manual_pensions_dt' ,['member_type_id' => 5, 'resource_id' =>
                 $pensioner->id, 'employee_id' => $pensioner->employee_id])
                        @endif

                    </div>
                    <div id="arrears" class="nav_tab_pane tab-pane">
                        {{--arrears--}}
                        @include('backend/operation/payroll/pension_administration/profile/includes/recovery_tab/get_approved_arrears',['member_type_id'=> 5, 'resource_id' => $pensioner->id,
                        'employee_id'=>$pensioner->employee_id])
                    </div>
                    <div id="deductions" class="nav_tab_pane tab-pane">
                        {{--deductions--}}
                        @include('backend/operation/payroll/pension_administration/profile/includes/recovery_tab/get_approved_deductions',['member_type_id'=> 5, 'resource_id' => $pensioner->id,
               'employee_id'=>$pensioner->employee_id])
                    </div>

                    <div id="unclaimed" class="nav_tab_pane tab-pane">
                        {{--unclaimed--}}
                        @include('backend/operation/payroll/pension_administration/profile/includes/recovery_tab/get_approved_unclaims',['member_type_id'=> 5, 'resource_id' => $pensioner->id,
                               'employee_id'=>$pensioner->employee_id])
                    </div>

                    <div id="suspended_payment" class="nav_tab_pane tab-pane">
                        {{--suspended payment--}}
                        @include('backend/operation/payroll/pension_administration/profile/includes/monthly_pension_tab/get_suspended_pensions' ,['member_type_id' => 5, 'resource_id' =>
                         $pensioner->id, 'employee_id' => $pensioner->employee_id, 'resource' => $pensioner])
                    </div>

                    <div id="document_center" class="nav_tab_pane tab-pane">
                        {{--document center--}}
                        @include('backend/operation/payroll/pension_administration/includes/document_center' ,['member_type_id' => 5, 'resource_id' =>
                         $pensioner->id, 'employee_id' => $pensioner->employee_id])
                    </div>


                    <div id="constant_care" class="nav_tab_pane tab-pane">
                        {{--constant care--}}
                        @include('backend/operation/payroll/pension_administration/profile/pensioner/includes/get_constant_care_dt' ,['resource_id' =>
                         $pensioner->id])
                    </div>

                    <div id="retiree_followup" class="nav_tab_pane tab-pane">
                        {{--retiree followup--}}
                        @include('backend/operation/payroll/pension_administration/profile/includes/retiree_followup_tab/retiree_followups_dt' ,['member_type_id' => 5, 'resource_id' =>
                         $pensioner->id, 'employee_id' => $pensioner->employee_id])
                    </div>
                </div>
            </div>
        </div>

    </div>

    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}
    <script  type="text/javascript">


        $(function () {


            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });
        });
    </script>;

@endpush
