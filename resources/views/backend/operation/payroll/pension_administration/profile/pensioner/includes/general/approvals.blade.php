{{--Approvals history--}}


<div class="col-md-12">
    <legend class="grey_modal">Change Updates History</legend>
    <br/>
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <div >
                    <ul class="nav nav-tabs float-xs-left" role="tablist">
                        {{--activate--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#activation"
                               data-toggle="tab">Activations
                            </a>
                        </li>

                        {{--bank--}}
                        <li class="nav-item">
                            <a class="nav-link" id = "bank_update_header"
                               href="#bank_update" data-toggle="tab">Bank Updates</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" id = "suspension_header"
                               href="#suspension" data-toggle="tab">Suspensions</a>
                        </li>
                        {{--deactivate--}}
                        <li class="nav-item">
                            <a class="nav-link" id = "deactivate_header"
                               href="#deactivation" data-toggle="tab">Deactivations</a>
                        </li>
                    </ul>
                </div>

                <div class="nav_tab_contain tab-content">

                    <div id="activation" class="nav_tab_pane tab-pane active in">



                    </div>




                    <div id="bank_update" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_bank_updates_for_beneficiary', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id])
                    </div>


                    <div id="suspension" class="nav_tab_pane tab-pane">

                    </div>


                    <div id="deactivation" class="nav_tab_pane tab-pane">



                    </div>
                </div>
            </div>


        </div>

    </div>


</div>













