

<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="constant-care-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Member Name</th>
                <th>Monthly Pension</th>
                <th>Status</th>

            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $("#constant_care_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#constant-care-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.pensioner.get_constant_cares_by_pensioner_dt',[ 'pensioner' => $resource_id]) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'member_name', name: 'b.member_name', orderable : true, searchable : true},
                        { data: 'monthly_pension_amount', name: 'b.monthly_pension_amount',  orderable : false, searchable : false},
                        { data: 'status', name: 'b.status',  orderable : false, searchable : false},


                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href =  url +  "/payroll/beneficiary_profile/"+ 4 +  '/' +  aData['resource_id'] + '/' + aData['employee_id'] ;

                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                });

            });

        });
    </script>;

@endpush
