<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5">{!! $pensioner->status_label !!}</span>

    @if(isset($pensioner->notification_report_id))
        {{--System notification--}}
    <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'name', $pensioner->notificationReport->incidentType->name) !!}</strong></span>


    <span class="navbar-brand mb-0 h5">@lang('labels.backend.claim.notification_#')&nbsp;:&nbsp;
            <small class="underline"><a href="{!! route('backend.claim.notification_report.profile', $pensioner->notification_report_id) !!}">{!! $pensioner->notificationReport->filename !!}  </a></small>
        </span>
        @else
        {{--manual file--}}
        <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'name', $pensioner->manualNotificationReport->incidentType->name) !!}</strong></span>


        <span class="navbar-brand mb-0 h5">Manual File - Case #:&nbsp;
            <small class="underline"><a href="#">{!! $pensioner->manualNotificationReport->case_no !!}  </a></small>
        </span>

    @endif

    <span class="navbar-brand mb-0 h5"> Pensioner Name&nbsp;:&nbsp;<small class="underline"> <a href="{!! route('backend.compliance.employee.profile', $pensioner->employee_id) !!}">{!! $pensioner->name !!}</a></small></span>


    @include('backend/operation/payroll/pension_administration/includes/alert_monitor_link')

</nav>



<legend></legend>
<br/>