

{{--General info--}}
<br/>

{{--Hold summary if on hold--}}
@if($pensioner->isactive == 3)
    @include('backend/operation/payroll/pension_administration/profile/includes/hold_summary',['hold_reason' => $pensioner->hold_reason, 'member_type_id' => 5, 'resource_id' => $pensioner->id, 'employee_id' => $pensioner->employee_id, 'hold_date' => $pensioner->hold_date])
    <br/>
@endif


<div class="row">
    {{--left general  summary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>
                {{--enrolled date--}}
                <tr>
                    <td width="140px">{!! 'Date of MMI' !!}</td>
                    <th>{!! Form::label( 'start_date',
                          isset($pensioner->start_pay_date) ?  short_date_format($pensioner->start_pay_date) : null, [])
                                !!} </th>
                </tr>
                @if(isset($pensioner->claim_details['pd']))
                <tr>
                    <td width="120px">{!! 'Permanent Disability (PD) %' !!}</td>
                    <th>{!! Form::label( '',
                         isset($pensioner->claim_details['pd']) ? (number_2_format($pensioner->claim_details['pd']) . '%') : null, [])
                                !!} </th>
                </tr>
                @endif

                @if(isset($pensioner->claim_details['monthly_earning']))
                    <tr>
                        <td width="120px">{!! 'GME' !!}</td>
                        <th>{!! Form::label( '',
                         isset($pensioner->claim_details['monthly_earning']) ? number_2_format($pensioner->claim_details['monthly_earning']) : null, [])
                                !!} </th>
                    </tr>
                @endif


                {{--Status--}}
                <tr>
                    <td>Status</td>
                    <td>
                        {!!  $pensioner->status_label .' ' . $pensioner->suspended_date_formatted
                                    !!}
                        @if($pensioner->suspense_flag == 1 && isset($pensioner->suspension_reason) && $pensioner->isactive == 1)
                            <br/>
                            {!! small_helper($pensioner->suspension_reason ) !!}
                        @endif
                    </td>
                </tr>


                <tr>
                    <td>First Pay Status</td>
                    <th>
                        {!! $pensioner->first_pay_status_label
                                    !!}</th>
                </tr>

                <tr>
                    <td>Date of Last verification</td>
                    <th>
                        {!! Form::label( '',
                               short_date_format($pensioner->last_verification_date), [])
                                    !!}</th>
                </tr>


                </tbody>
            </table>

        </div>
    </div>



    {{--Right summary--}}

    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>


                {{--MP--}}
                <tr>
                    <td width="120px">Monthly Pension</td>
                    <th>{!! Form::label( 'enrolled_date',
                            $pensioner->mp_formatted, [])
                                !!} </th>
                </tr>

                {{--Bank--}}
                <tr>
                    <td width="120px">Bank</td>
                    <th>{!! Form::label( 'bank',
                           isset($pensioner->bank_branch_id) ? $pensioner->bankBranch->bank->name : (isset($pensioner->bank_id) ? $pensioner->bank->name : ' '), [])
                                !!} </th>
                </tr>

                {{--Branch--}}
                <tr>
                    <td>Branch</td>
                    <th>
                        {!!   isset($pensioner->bank_branch_id) ? $pensioner->bankBranch->name : ' '
                                    !!}</th>
                </tr>


                <tr>
                    <td>Account No.</td>
                    <th>
                        {!! $pensioner->accountno
                                    !!}</th>
                </tr>




                </tbody>
            </table>

        </div>
    </div>





</div>
