

{{--sidebar Pensioner summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">

            {{--name--}}
            <tr>
                <td style="padding-left: 5px" width="105px">@lang('labels.general.name'):</td>
                <td  height="20px"><b>{!! Form::label( 'name',$pensioner->name, [ 'id'=> 'name']) !!}</b></td>

            </tr>

            {{--memberno--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.memberno'):</td>
                <td><b>{!! Form::label( 'memberno', $pensioner->employee->memberno, [ 'id'=> 'memberno']) !!}</b></td>

            </tr>

            {{--dob--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.dob'):</td>
                <td ><b>{!! Form::label( 'dob', $pensioner->dob_formatted, [ 'id'=>
                'dob'])
                !!}</b></td>

            </tr>
            <tr>
                <td style="padding-left: 5px">Age:</td>
                <td ><b>{!! Form::label( 'dob', $pensioner->age, [ 'id'=>
                'age'])
                !!}</b></td>

            </tr>
            {{--employer--}}

                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.table.receipt.employer'):</td>
                    <td ><b>{!! Form::label( 'employer',$pensioner->employer_name, [ 'id'=>
                'dob'])
                !!}</b></td>

                </tr>

            {{--jOB tITLE--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.occupation'):</td>
                <td><b>{!! Form::label( 'occupation', (($pensioner->employee->jobTitle()->count() > 0) ? $pensioner->employee->jobTitle->name :  ' '), [ 'id'=> 'occupation']) !!}</b></td>

            </tr>

            {{--Department--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.department'):</td>
                <td><b>{!! Form::label( 'department',!is_null($pensioner->employee->department) ? $pensioner->employee->department : ' ', [ 'id'=> 'department'])
                        !!}</b></td>
            </tr>

            {{--gender--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.gender'):</td>
                <td><b>{!! Form::label( 'gender', ($pensioner->employee->gender()->count()) ? $pensioner->employee->gender->name : ' ', [ 'id'=> 'gender'])
                        !!}</b></td>
            </tr>


            {{--marital status--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.marital_status'):</td>
                <td><b>{!! Form::label( 'marital_status', ($pensioner->employee->maritalStatus()->count() > 0) ? $pensioner->employee->maritalStatus->name : ' ', [ 'id'=> 'marital_status'])
                        !!}</b></td>
            </tr>


            {{--phone--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.phone'):</td>
                <td><b>{!! Form::label( 'phone', !is_null($pensioner->phone) ? $pensioner->phone : ' ', [ 'id'=> 'phone'])
                        !!}</b></td>

            </tr>
            <tr>
                <td style="padding-left: 5px">Next of kin Phone:</td>
                <td><b>{!! Form::label( 'phone',($pensioner->next_kin_phone) ?  $pensioner->next_kin_phone : ' ', [ 'id'=> '']) !!}</b></td>

            </tr>
            {{--email--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.email'):</td>
                <td><b>{!! Form::label( 'email', !is_null($pensioner->email) ? $pensioner->email : ' ', [ 'id'=> 'email'])
                        !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Region:</td>
                <td><b>{{ ($pensioner->region_id) ? $pensioner->region->name : ''  }}</b></td>
            </tr>

            <tr>
                <td style="padding-left: 5px">District:</td>
                <td><b>{{ ($pensioner->district_id) ? $pensioner->district->name : ''  }}</b></td>
            </tr>

            {{--employee category--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.employee_category'):</td>
                <td><b>{!! Form::label( 'employee_category', $pensioner->employee->emp_cate, [])
                        !!}</b></td>
            </tr>




        </table>
    </div>
</div>






@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush