

{{--General info--}}
<br/>

{{--Hold summary if on hold--}}
@if($dependent_employee->isactive == 3)
    @include('backend/operation/payroll/pension_administration/profile/includes/hold_summary', ['hold_reason' => $dependent_employee->hold_reason,'member_type_id' => 4, 'resource_id' => $dependent_employee->dependent_id, 'employee_id' => $dependent_employee->employee_id, 'hold_date' => $dependent_employee->hold_date  ])
    <br/>
@endif


<div class="row">
    {{--left general  summary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>




                {{--Check if Notification already completed workflow--}}
                @if(isset($dependent_employee->notification_report_id)  || isset($dependent_employee->manual_notification_report_id))

                    {{--start pay date--}}
                    <tr>
                        <td width="120px">{!! $dependent_employee->dependent_type_id != 7 ? 'Employee Death Date' : 'Date of MMI of employee' !!}</td>
                        <th>{!!  !is_null($dependent->getStartPayDateAttribute($dependent_employee->employee_id)) ?
                            short_date_format($dependent->getStartPayDateAttribute($dependent_employee->employee_id)) : null   !!} </th>
                    </tr>


                    {{--claim details--}}
                    {{--PD--}}
                    @if(isset($dependent->getClaimDetailsAttribute($dependent_employee->employee_id)['pd']) && $dependent_employee->dependent_type_id == 7)
                        <tr>
                            <td width="120px">{!! 'Permanent Disability (PD) %' !!}</td>
                            <th>{!! Form::label( '',
                 (number_2_format($dependent->getClaimDetailsAttribute($dependent_employee->employee_id)['pd']) . '%'), [])
                                !!} </th>
                        </tr>
                    @endif
                    {{--GME--}}
                    @if(isset($dependent->getClaimDetailsAttribute($dependent_employee->employee_id)['monthly_earning']))
                        <tr>
                            <td width="120px">{!! 'GME' !!}</td>
                            <th>{!! Form::label( '',
            number_2_format($dependent->getClaimDetailsAttribute($dependent_employee->employee_id)['monthly_earning']), [])
                                !!} </th>
                        </tr>
                    @endif




                    {{--Status--}}
                    <tr>
                        <td>Status</td>
                        <td>
                            {!! $dependent->getStatusLabelAttribute($dependent_employee->employee_id)  .' ' . $dependent->suspended_date_formatted !!}
                            @if($dependent->suspense_flag == 1 && isset($dependent->suspension_reason) && ($dependent_employee->isactive == 1))
                                <br/>
                                {!! small_helper($dependent->suspension_reason ) !!}
                            @endif

                        </td>
                    </tr>


                    {{--if is MP Receiver--}}
                    @if($dependent_employee->survivor_pension_percent > 0)
                        <tr>
                            <td>First Pay Status (MP)</td>
                            <th>
                                {{--@if($dependent_employee->firstpay_flag == 1)--}}
                                {{--<span class="tag tag-success" data-toggle="tooltip" data-html="true" title="Paid" > Paid   </span>--}}
                                {{--@else--}}
                                {{--<span class="tag tag-warning" data-toggle="tooltip" data-html="true" title="Pending" > Pending   </span>--}}
                                {{--@endif--}}
                                {!! $dependent->getFirstPayStatusLabelAttribute($dependent_employee->employee_id) !!}
                            </th>
                        </tr>
                    @endif

                    {{--Gratuity flag--}}
                    @if($dependent_employee->survivor_gratuity_percent > 0)
                        <tr>
                            <td>Gratuity Pay Status</td>
                            <th>
                                @if($dependent_employee->survivor_gratuity_pay_flag == 1)
                                    <span class="tag tag-success" data-toggle="tooltip" data-html="true" title="Paid" > Paid   </span>
                                @else
                                    <span class="tag tag-warning" data-toggle="tooltip" data-html="true" title="Pending" > Pending   </span>
                                @endif
                            </th>
                        </tr>
                    @endif


                    {{--funeral grant flag--}}
                    @if($dependent_employee->funeral_grant_percent > 0)
                        {{--<tr>--}}
                        {{--<td>Funeral grant Pay Status</td>--}}
                        {{--<th>--}}
                        {{--@if($dependent_employee->funeral_grant_pay_flag == 1)--}}
                        {{--<span class="tag tag-success" data-toggle="tooltip" data-html="true" title="Paid" > Paid   </span>--}}
                        {{--@else--}}
                        {{--<span class="tag tag-warning" data-toggle="tooltip" data-html="true" title="Pending" > Pending   </span>--}}
                        {{--@endif--}}
                        {{--</th>--}}
                        {{--</tr>--}}
                    @endif


                    <tr>
                        <td>Date of Last Verification</td>
                        <th>
                            {!! Form::label( '',
                                    short_date_format($dependent->last_verification_date), [])
                                        !!}</th>
                    </tr>

                @else
                    <tr>
                        <td>Claim Status</td>
                        <th>
                            <span class="tag tag-warning" data-toggle="tooltip" data-html="true" title="Pending" > Pending   </span>

                        </th>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>
    </div>



    {{--Right summary--}}

    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>


                {{--MP--}}
                @if($dependent_employee->survivor_pension_amount > 0)
                    <tr>
                        <td width="180px">Monthly Pension {!! ' (' . $dependent_employee->survivor_pension_percent . '%)' !!}</td>
                        <th>{!! Form::label( 'enrolled_date',
                            number_2_format($dependent_employee->survivor_pension_amount) , [])
                                !!} </th>
                    </tr>

                    {{--Is other dep--}}
                    @if(isset($dependent_employee->isotherdep))

                        @if($dependent_employee->isotherdep == 1)
                            <tr>
                                <td width="180px">Payable Months</td>
                                <th>{{ number_2_format($dependent_employee->pay_period_months) }} </th>
                            </tr>

                            <tr>
                                <td width="180px">Remained Months</td>
                                <th>{{ number_2_format($dependent_employee->recycles_pay_period) }} </th>
                            </tr>
                        @endif
                    @endif

                @endif

                {{--Gratuity--}}
                @if($dependent_employee->survivor_gratuity_percent > 0)
                    <tr>
                        <td width="180px">Gratuity {!! ' (' . $dependent_employee->survivor_gratuity_percent . '%)' !!}</td>
                        <th>{!! Form::label( 'enrolled_date',
                            number_2_format($dependent_employee->survivor_gratuity_amount) , [])
                                !!} </th>
                    </tr>
                @endif



                {{--Funeral grant--}}

                {{--<tr>--}}
                {{--<td width="180px">Funeral grant {!! ' (' . $dependent_employee->funeral_grant_percent . '%)' !!}</td>--}}
                {{--<th>{!! Form::label( 'enrolled_date',--}}
                {{--number_2_format($dependent_employee->funeral_grant) , [])--}}
                {{--!!} </th>--}}
                {{--</tr>--}}


                {{--Bank--}}
                <tr>
                    <td width="120px">Bank</td>
                    <th>{!! Form::label( 'bank',
                isset($dependent->bank_branch_id) ? $dependent->bankBranch->bank->name : ( isset($dependent->bank_id) ? $dependent->bank->name : ' '), [])
                !!} </th>
                </tr>

                {{--Branch--}}
                <tr>
                    <td>Branch</td>
                    <th>
                        {!!   isset($dependent->bank_branch_id) ? $dependent->bankBranch->name : ' '
                        !!}</th>
                </tr>


                <tr>
                    <td>Account No.</td>
                    <th>
                        {!! $dependent->accountno
                        !!}</th>
                </tr>




                </tbody>
            </table>

        </div>
    </div>





</div>
