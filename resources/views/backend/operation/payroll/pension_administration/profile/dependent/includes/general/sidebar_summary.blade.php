

{{--sidebar Dependent summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            {{--name--}}
            <tr>
                <td style="padding-left: 5px" width="105px">@lang('labels.general.name'):</td>
                <td  height="20px"><b>{!! Form::label( 'name',$dependent->name, [ 'id'=> 'name']) !!}</b></td>

            </tr>

            {{--relation--}}
            <tr>
                <td style="padding-left: 5px">Relation:</td>
                <td><b>{!! Form::label( 'relation', $dependent_type->name, [ 'id'=> 'memberno']) !!}</b></td>

            </tr>

            {{--dob--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.dob'):</td>
                <td ><b>{!! Form::label( 'dob', $dependent->dob_formatted, [ ])
                !!}</b></td>

            </tr>


            {{--age--}}
            <tr>
                <td style="padding-left: 5px">Age:</td>
                <td ><b>{!! Form::label( 'age', $dependent->age, [ ])
                !!}</b></td>

            </tr>

            {{--gender--}}

            <tr>
                <td style="padding-left: 5px">Gender:</td>
                <td ><b>{!! Form::label( 'type',$dependent->gender->name, [])
                !!}</b></td>

            </tr>

            {{--Phone--}}
            <tr>
                <td style="padding-left: 5px">Phone:</td>
                <td><b>{!! Form::label( 'phone',( $dependent->phone) ?  $dependent->phone : ' ', [ 'id'=> '']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Next of kin Phone:</td>
                <td><b>{!! Form::label( 'phone',( $dependent->next_kin_phone) ?  $dependent->next_kin_phone : ' ', [ 'id'=> '']) !!}</b></td>

            </tr>

            {{--email--}}
            <tr>
                <td style="padding-left: 5px">Email:</td>
                <td><b>{!! Form::label( 'email',( $dependent->email) ?  $dependent->email : ' ', [ 'id'=> 'department'])
                        !!}</b></td>
            </tr>
            <tr>
                <td style="padding-left: 5px">Region:</td>
                <td><b>{{ ($dependent->region_id) ? $dependent->region->name : ''  }}</b></td>
            </tr>

            <tr>
                <td style="padding-left: 5px">District:</td>
                <td><b>{{ ($dependent->district_id) ? $dependent->district->name : ''  }}</b></td>
            </tr>
            {{--disable--}}
            @if($dependent->isdisabled == 1)
                <tr>
                    <td style="padding-left: 5px">Disability <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Child has disability to be granted survivor grants. "></i>:</td>
                    <td><b>{!! Form::label( 'isdisabled', $dependent->isdisabled == 1 ?  'Yes' : 'No', [ 'id'=> 'isdisabled'])
                        !!}</b></td>
                </tr>
            @endif

            {{--is education--}}
            @if($dependent->iseducation == 1)
                <tr>
                    <td style="padding-left: 5px">Attending School <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Child qualify for education benefit from survivor grants "></i>:</td>
                    <td><b>{!! Form::label( 'iseducation', $dependent->iseducation == 1 ?  'Yes' : 'No', [ 'id'=> 'iseducation'])
                        !!}</b></td>
                </tr>
            @endif


            @if($dependent_employee->deadline_date)
                <tr>
                    <td style="padding-left: 5px">Deadline Date:</td>
                    <td><b>{{ short_date_format($dependent_employee->deadline_date)  }}</b></td>
                </tr>
            @endif
        </table>
    </div>
</div>






@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush