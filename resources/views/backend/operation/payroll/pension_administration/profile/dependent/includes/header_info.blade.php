<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5">{!! $dependent->getStatusLabelAttribute($employee->id) !!}</span>


@if(isset($dependent_employee->notification_report_id))
    {{--system notification--}}
        <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'name', $notification_report->incidentType->name) !!}</strong></span>
    <span class="navbar-brand mb-0 h5">@lang('labels.backend.claim.notification_#')&nbsp;:&nbsp;
            <small class="underline"><a href="{!! route('backend.claim.notification_report.profile', $notification_report->id) !!}">{!! $notification_report->filename !!}  </a></small>
        </span>
    @else
    {{--manual--}}
        <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'name', $manual_notification_report->incidentType->name) !!}</strong></span>
        <span class="navbar-brand mb-0 h5">Manual File - Case #:&nbsp;
            <small class="underline"><a href="#">{!! $manual_notification_report->case_no !!}  </a></small>
        </span>
@endif
    <span class="navbar-brand mb-0 h5"> {!! ($dependent_employee->dependent_type_id != 7) ? 'Deceased'  : 'Pensioner'!!}&nbsp;:&nbsp;<small class="underline"> <a href="{!! route('backend.compliance.employee.profile', $dependent_employee->employee_id) !!}">{!! $employee->name !!}</a></small></span>

    {{--alert monitor--}}
    @include('backend/operation/payroll/pension_administration/includes/alert_monitor_link')

</nav>
<legend></legend>
<br/>