{{--Approvals history--}}


<div class="col-md-12">
    <legend class="grey_modal">Change Updates History</legend>
    <br/>
<div class = "row">
    <div class="col-md-12">

        <div class="nav-tab-vertical-left">
            <ul class="nav nav-tabs float-xs-left" role="tablist">
                {{--activate--}}
                <li class="nav-item">
                    <a class="nav-link active" href="#activation"
                       data-toggle="tab">Activations
                    </a>
                </li>
                {{--deactivate--}}
                <li class="nav-item">
                    <a class="nav-link" id = "deactivate_header"
                       href="#deactivation" data-toggle="tab">Deactivations</a>
                </li>
                {{--bank--}}
                <li class="nav-item">
                    <a class="nav-link" id = "bank_update_header"
                       href="#bank_update" data-toggle="tab">Bank Updates</a>
                </li>
                {{--deductions--}}
                <li class="nav-item">
                    <a class="nav-link" id = "mp_update_header"
                       href="#mp_update" data-toggle="tab">MP Updates</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id = "suspension_header"
                       href="#suspension" data-toggle="tab">Suspensions</a>
                </li>

            </ul>
            <div class="nav_tab_contain tab-content">

                <div id="activation" class="nav_tab_pane tab-pane active in">


                </div>



                <div id="deactivation" class="nav_tab_pane tab-pane">



                </div>
                <div id="bank_update" class="nav_tab_pane tab-pane">

                </div>
                <div id="mp_update" class="nav_tab_pane tab-pane">

                </div>

                <div id="suspension" class="nav_tab_pane tab-pane">

                </div>
            </div>
        </div>
    </div>

</div>
</div>



