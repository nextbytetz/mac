
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="activations-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Remark</th>
                <th>Change Type</th>
                <th>Date created</th>
                <th>Date approved</th>
                <th>Initiated By</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        // $("#activation_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#activations-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.status_change.get_activations_for_beneficiary',['member_type_id' => $member_type_id, 'resource_id' => $resource_id,
                        'employee_id' => $employee_id,]) !!}',
                        type : 'get'
                    },
                    columns: [
                        { data: 'remark', name: 'remark', orderable : true, searchable : true},
                        { data: 'status_change_type' , name: 'status_change_type', orderable : false, searchable : false},
                              { data: 'created_at_formatted', name: 'created_at',  orderable : true, searchable : true},
                        { data: 'wf_done_date_formatted', name: 'wf_done_date' ,orderable : true, searchable : true},
                        { data: 'user', name: 'user' ,orderable : false, searchable : false},
                        { data: 'status', name: 'status' ,orderable : false, searchable : false},

                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = url +  "/payroll/status_change/"+ "profile/" + aData['id'] ;
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                });

            });

        // });
    </script>;

@endpush
