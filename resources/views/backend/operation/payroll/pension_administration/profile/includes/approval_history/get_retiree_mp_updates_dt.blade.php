
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="retiree-mp-updates-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Remark</th>
                <th>Current MP</th>
                <th>Social Security MP</th>
                <th>Deduction/Arrears Amount</th>
                <th>Date approved</th>
                <th>Initiated By</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $("#retiree_mp_update_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#retiree-mp-updates-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.retiree.mp_update.get_mp_updates_by_member_dt',['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id'=> $employee_id ]) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'remark', name: 'remark', orderable : false, searchable : false},
                    { data: 'current_mp', name: 'current_mp', orderable : false, searchable : false},
                    { data: 'social_security_mp' , name: 'social_security_mp', orderable : false, searchable : false},
                    { data: 'deduction_amount', name: 'deduction_amount',  orderable : false, searchable : false},
                    { data: 'wf_done_date', name: 'wf_done_date' ,orderable : false, searchable : false},
                    { data: 'user_id', name: 'user_id' ,orderable : false, searchable : false},
                    { data: 'status', name: 'status' ,orderable : false, searchable : false},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/retiree/mp_update/"+ "profile/" + aData['id']  ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });

        });
    </script>;

@endpush
