<div class = "row">
    <div class="col-md-12" >
        <div class="col-md-12" >
            <div class="pull-right">


                <div class="btn-group">

                        <a href="{!! route('backend.payroll.retiree.followup.create',[$member_type_id, $resource_id, $employee_id]) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-plus"></i>&nbsp;Add New Followup</a>

                </div>
            </div>



        </div>
    </div>


</div>

<br/>
{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="retiree-follow-ups-table">
            <thead>
            <tr >
                <th>Follow up Type</th>
                <th>Contact</th>
                <th>Contact Person</th>
                <th>Follow up Date</th>
                <th>Feedback</th>
                <th>Next Follow up Date</th>
                <th>Staff</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>

    </div>
</div>




@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $("#retiree_followup_header").one("click", function(){
                $('#retiree-follow-ups-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.retiree.followup.get_followups_by_member_dt', [$member_type_id, $resource_id, $employee_id]) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'follow_up_type_cv_id', name: 'follow_up_type_cv_id',  orderable : false, searchable : false},
                        { data: 'contact', name: 'contact',  orderable : false, searchable : false},
                        { data: 'contact_person', name: 'contact_person',  orderable : false, searchable : false},
                        { data: 'date_of_follow_up', name: 'date_of_follow_up' ,orderable : false, searchable : false},
                        { data: 'retiree_feedback_cv_id' , name: 'retiree_feedback_cv_id', orderable : false, searchable : false},
                        { data: 'date_of_next_follow_up' , name: 'date_of_next_follow_up', orderable : false, searchable : false},
                        { data: 'user_id', name: 'user_id',  orderable : false, searchable : false},
                        { data: 'actions', name: 'actions',  orderable : false, searchable : false},
                    ],
                    // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //     $(nRow).click(function() {
                    //         document.location.href = url +  "/payroll/retiree/followup/edit/" + aData['id'] ;
                    //     }).hover(function() {
                    //         $(this).css('cursor','pointer');
                    //     }, function() {
                    //         $(this).css('cursor','auto');
                    //     });
                    // }

                });

            });
        });


    </script>;

@endpush
