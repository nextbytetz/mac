
<div class="row pull-right">
    <div class="col-md-12 ">
        @if($resource->payrollSuspendedRuns()->where('payroll_suspended_runs.ispaid',0)->where('payroll_suspended_runs.employee_id', $employee_id)->count() > 0 )
            <span>
                                        <a href="{!! route('backend.payroll.suspended_runs.reason_remove_pending', ['member_type' => $member_type_id, 'resource' => $resource->id, 'employee_id' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-money"></i>&nbsp;Remove Pending Payments</a>
                                     </span>

        @endif

    </div>
</div>

<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="suspended-mp-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Payroll Month</th>
                <th>Arrears Amount</th>
                <th>Unclaimed Amount</th>
                <th>Deductions Amount</th>
                <th>Months Paid</th>
                <th>Monthly Pension</th>
                <th>Net Payable Amount</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $("#suspended_payment_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#suspended-mp-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.suspended_pensions.get_for_beneficiary',['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' =>
                        $employee_id]) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'run_date_formatted', name: 'payroll_procs.run_date', orderable : true, searchable : true},
                        { data: 'arrears_amount', name: 'payroll_suspended_runs.arrears_amount',  orderable : true, searchable : true},
                        { data: 'unclaimed_amount', name: 'payroll_suspended_runs.unclaimed_amount',  orderable : true, searchable : true},
                        { data: 'deductions_amount', name: 'payroll_suspended_runs.deductions_amount',  orderable : true, searchable : true},
                        { data: 'months_paid', name: 'payroll_suspended_runs.months_paid' ,orderable : true, searchable : true},
                        { data: 'monthly_pension', name: 'payroll_suspended_runs.monthly_pension',  orderable : true, searchable : true},
                        { data: 'amount', name: 'payroll_suspended_runs.amount' ,orderable : true, searchable : true},
                        { data: 'status', name: 'status' ,orderable : false, searchable : false},

                    ],
                    // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //     $(nRow).click(function() {
                    //         document.location.href = url +  "/payroll/bank_update/"+ "profile/" + aData['id'] ;
                    //     }).hover(function() {
                    //         $(this).css('cursor','pointer');
                    //     }, function() {
                    //         $(this).css('cursor','auto');
                    //     });
                    // }

                });

            });

        });
    </script>;

@endpush
