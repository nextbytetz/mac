{{--Action links for pensioner and dependents--}}
@if( $isactive == 1)

    {{--Recovery--}}

    <span>
                                        <a href="{!! route('backend.payroll.recovery.show_choose_type_form', ['member_type' => $member_type_id, 'resource' => $resource->id, 'employee_id' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-money"></i>&nbsp;Payment Recovery</a>
                                     </span>



    {{--Suspend--}}
    <span>
        @if($suspense_flag == 1 )

            <a href="{!! route('backend.payroll.status_change.create', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id, 'status_type_ref' => 'PSCREIN' ]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-play-circle"></i>&nbsp;Reinstate</a>

        @else
            <a href="{!! route('backend.payroll.status_change.create', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id, 'status_type_ref' => 'PSCMASU' ]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pause-circle"></i>&nbsp;Suspend</a>
        @endif


                                         </span>

    {{--Verify--}}
    <span>
                                        <a href="{!! route('backend.payroll.verification.create', ['member_type' => $member_type_id, 'resource' => $resource->id, 'employee_id' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-check-square"></i>&nbsp;Verification</a>
                                     </span>
@endif






{{--@if($isactive != 2 || ($isactive == 2 && (!$resource->region_id || !$resource->district_id)))--}}
{{--@if($isactive )))--}}

    {{--Beneficiary updates--}}
    <span>
                                        <a href="{!! route('backend.payroll.beneficiary_update.create', ['member_type_id' => $member_type_id, 'resource_id' => $resource->id,'employee_id' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Modify Details</a>
                                     </span>

@if($isactive != 2)
    {{--Update bank details--}}
    <span>
                                        <a href="{!! route('backend.payroll.create_new_bank', ['member_type_id' => $member_type_id, 'resource_id' => $resource->id, 'employee_id' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-bank"></i>&nbsp;Update Bank</a>
                                     </span>
@endif

{{--Hold beneficiary--}}



<span>
    @if($isactive == 0 )

        <a href="{!! route('backend.payroll.beneficiary.open_hold_page', ['member_type_id' => $member_type_id, 'resource_id' => $resource->id, 'employee_id' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pause"></i>&nbsp;Archive</a>

    @elseif($isactive == 3)

        {!! HTML::decode(link_to_route('backend.payroll.beneficiary.release_hold', "<i class='icon fa fa-play' aria-hidden='true'></i>&nbsp;" . 'Release From Archive', ['member_type_id' => $member_type_id, 'resource_id' => $resource->id, 'employee_id' => $employee_id], ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to release from archive for this beneficiary?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}

    @endif
              </span>

{{---------Activate/ Deactivate------}}
<span>


    @if($isactive == 0 || ($isactive == 1 && $resource->firstpay_manual == 0 && $resource->has_activation_workflow == false))
        <a href="{!! route('backend.payroll.status_change.create', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id, 'status_type_ref' => 'PSCACT' ]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-youtube-play"></i>&nbsp;Activate</a>
    @elseif($isactive == 1)
        <a href="{!! route('backend.payroll.status_change.create', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id, 'status_type_ref' => 'PSCDEACT' ]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-stop-circle"></i>&nbsp;Deactivate</a>
    @endif
                                        </span>
<span>
{{--Send to e-office--}}
    @if($resource->isdmsposted == 0)
        <a href="{!! route('backend.payroll.beneficiary.post_to_dms', ['member_type' => $member_type_id, 'resource' => $resource->id, 'employee' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-upload"></i>&nbsp;Send to E-office
                                </a>
    @endif


    {{--more links--}}

    @include('backend/operation/payroll/pension_administration/profile/includes/more_links')


</span>