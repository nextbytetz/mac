

{{--Sidebar Pension payment summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Monthly Pension Payment Summary</span></b></h5> </td>

            </tr>
        </table>
    </div>
</div>
@if($firstpay_flag == 1 && isset($mp_payment_summary))
    <div class="row">
        <div class="light_grey_bg">&nbsp;</div>
        <div class="light_grey_bg">
            <table style="width:100%">
                {{--approved and paid--}}
                <tr>
                    <td style="padding-left: 5px" width="140px">MP Total Paid (Approved):</td>
                    <td  height="20px"><b>{!! number_2_format($mp_payment_summary['total_mp_approved_paid']) !!}</b></td>

                </tr>

                {{--Pending recovery--}}
                <tr>
                    <td style="padding-left: 5px">Pending Recovery Amount:</td>
                    <td><b>{!! number_2_format($mp_payment_summary['total_pending_recovery_amount']) !!}</b></td>

                </tr>


                {{--Suspended--}}
                <tr>
                    <td style="padding-left: 5px">MP Total Suspended:</td>
                    <td><b>{!! number_2_format($mp_payment_summary['total_suspended_amount']) !!}</b></td>

                </tr>

                {{--Total eligible--}}
                <tr>
                    <td style="padding-left: 5px">MP Total Eligible:</td>
                    <td><b>{!! number_2_format($mp_payment_summary['total_mp_eligible']) !!}</b></td>

                </tr>

                {{--Total Balance--}}
                <tr>
                    <td style="padding-left: 5px">MP Total Balance:</td>
                    <td><b>{!! number_2_format($mp_payment_summary['mp_balance']) !!}</b></td>
                </tr>
                <tr>
                    <td style="padding-left: 5px"></td>
                    <td>
                        {{--STATUS overpaid/underpaid--}}
                        {!! $status_label !!}
                    </td>
                </tr>
            </table>

            <small>Calculation refer up to last payroll approved - {!! short_date_format($last_payroll_run_date) !!}</small>
        </div>
    </div>


@endif



@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush