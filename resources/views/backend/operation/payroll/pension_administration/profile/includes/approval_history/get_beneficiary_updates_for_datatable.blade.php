
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="beneficiary-updates-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Remark</th>
                <th>Date created</th>
                <th>Date approved</th>
                <th>Initiated By</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $("#beneficiary_update_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#beneficiary-updates-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.beneficiary_update.get_for_beneficiary',['member_type_id' => $member_type_id, 'resource_id' => $resource_id]) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'remark', name: 'remark', orderable : true, searchable : true},
                        { data: 'created_at_formatted', name: 'created_at',  orderable : true, searchable : true},
                        { data: 'wf_done_date_formatted', name: 'wf_done_date' ,orderable : true, searchable : true},
                        { data: 'user', name: 'user' ,orderable : false, searchable : false},
                        { data: 'status', name: 'status' ,orderable : false, searchable : false},

                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = url +  "/payroll/beneficiary_update/"+ "profile/" + aData['id'] ;
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                });

            });

        });
    </script>;

@endpush
