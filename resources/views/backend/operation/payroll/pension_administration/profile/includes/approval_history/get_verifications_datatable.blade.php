{{--SUspensions and Reinstatates of beneficiary i.e. dependendants and pensioners--}}
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="verifications-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Verified By</th>
                <th>Date Verified</th>
                <th>Document Used</th>
                <th>Education</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $("#verification_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#verifications-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.verification.get_verifications_by_beneficiary',['member_type_id' => $member_type_id, 'resource_id' => $resource_id,
                        ]) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'user', name: 'user', orderable : true, searchable : false},
                        { data: 'verification_date_formatted', name: 'verification_date',  orderable : true, searchable : true},
                        { data: 'verification_form', name: 'folionumber' ,orderable : true, searchable : true},
                        { data: 'education', name: 'education' ,orderable : false, searchable : false},


                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            if(aData['payroll_status_change_id'] != null){
                                document.location.href = url +  "/payroll/status_change/"+ "profile/" + aData['payroll_status_change_id'] ;
                            }
                                           }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                });

            });

        });
    </script>;

@endpush
