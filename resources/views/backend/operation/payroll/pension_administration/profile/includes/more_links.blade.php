{{-- MORE LINKS OPTION--}}

<span class="dropdown"> <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" >
    @lang('buttons.general.more')
    </a>
  <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" >
{{--style="margin-left:-115px;"--}}

      {{--employer registration profile--}}

      {{--<span>--}}
      {{--<a class="dropdown-item" href="{!! route('backend.compliance.employer_registration.profile', $employer->id) !!}" >Already Paid - Activate</a>--}}
      {{--<div class="dropdown-divider"></div>--}}
      {{--</span>--}}

      @if($isactive == 0)
          <span>
            @if($member_type_id == 5)
                  <a href='{!! route('backend.payroll.pensioner.merge_manual_pensioner_page',['pensioner' =>  $pensioner->id])  !!}' class="dropdown-item" ><i class="icon fa fa-play"></i>&nbsp; Already Paid Manually - Activate </a>
              @elseif($member_type_id == 4)
                  <a href='{!! route('backend.compliance.dependent.merge_manual_dependent_page',['dependent' =>  $dependent_employee->dependent_id, 'employee' =>  $dependent_employee->employee_id])  !!}' class="dropdown-item" > <i class="icon fa fa-play"></i>&nbsp; Already Paid Manually - Activate </a>
              @endif
              <div class="dropdown-divider"></div>
        </span>
      @endif

      {{--Change Mp--}}
      {{--<span>--}}
      {{--@if($member_type_id == 5)--}}
      {{--<a href="{!! route('backend.payroll.mp_update.create', ['member_type' => 5, 'resource'=> $pensioner->id, 'employee' => $pensioner->employee_id ]) !!}"  class="dropdown-item" ><i class="icon fa fa-edit"></i>&nbsp;Update Monthly Pension--}}
      {{--@elseif($member_type_id == 4)--}}
      {{--<a href="{!! route('backend.payroll.mp_update.create', ['member_type' => 4, 'resource'=> $dependent_employee->dependent_id, 'employee' => $dependent_employee->employee_id ]) !!}"  class="dropdown-item" ><i class="icon fa fa-edit"></i>&nbsp;Update Monthly Pension--}}
      {{--@endif--}}
      {{--</span>--}}

      {{--@if($resource->isdmsposted == 1 &&  $check_if_has_doc == false)--}}
      @if($resource->isdmsposted == 1)
          <span>

              <a href='{!! route('backend.payroll.beneficiary.re_post_to_dms', ['member_type' => $member_type_id, 'resource' => $resource->id, 'employee' => $employee_id])  !!}' class="dropdown-item" ><i class="icon fa fa-upload"></i>Resend to E-office</a>
              <div class="dropdown-divider"></div>
          </span>
      @endif


      <span>
  <a href="{!! route('backend.payroll.status_change.create', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id, 'status_type_ref' => 'PSCDEACT' ]) !!}"  class="dropdown-item" ><i class="icon fa fa-stop-circle"></i>&nbsp;Deactivate
                                </a>
              <div class="dropdown-divider"></div>
      </span>




      {{--Child reinstate continuation--}}
      @if($member_type_id == 4 && $isactive != 0)
          @if($dependent_employee->dependent_type_id == 3  && $resource->age >= 18)
              <span>

                  <a href="{!! route('backend.payroll.status_change.create', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id, 'status_type_ref' => 'PSCCHDREI' ]) !!}"  class="dropdown-item" ><i class="icon fa fa-play"></i>&nbsp;Child Reinstate (Extension)
                                </a>
          <div class="dropdown-divider"></div>

                  {{--TODO need to comment this temp action--}}
                  @if($resource->suspense_flag == 1)
                      <a href="{!! route('backend.payroll.status_change.create', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id, 'status_type_ref' => 'PSCACT' ]) !!}"  class="dropdown-item" ><i class="icon fa fa-youtube-play"></i>&nbsp;Re-Activate Child</a>

                      <div class="dropdown-divider"></div>
                  @endif
</span>
          @endif

      @endif

      <span>
<a href="{!! route('backend.payroll.beneficiary.document_usage_status', ['member_type_id' => $member_type_id, 'resource_id'=>$resource->id, 'employee_id' => $employee_id]) !!}"  class="dropdown-item" ><i class="icon fa fa-file"></i>&nbsp;Document Status (for Approval)
              </a>
<div class="dropdown-divider"></div>
</span>




      {{--CLose--}}
      <span>
@if($member_type_id == 5)
              <a href="{!! route('backend.payroll.pensioner.index') !!}"  class="dropdown-item" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')
              </a>
          @elseif($member_type_id == 4)
              <a href="{!! route('backend.compliance.dependent.index') !!}"  class="dropdown-item" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')
</a>
          @endif
          <div class="dropdown-divider"></div>
</span>


</span>

</span>