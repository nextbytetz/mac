
@extends('layouts.backend.main', ['title' => 'Payroll Documents Status', 'header_title' => 'Payroll Documents Status'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>


    </style>
@endpush


@section('content')

    {{--{!! Form::model($resource,['route' => ['backend.payroll.recovery.create', ],'method'=>'post',  'id' => 'create', 'name' => 'create']) !!}--}}
    {!! Form::open(['route' => ['backend.payroll.beneficiary.document_usage_status', $member_type_id,$resource->id, $employee_id],'method'=>'get', 'name' => 'store', 'id' => 'store']) !!}
    <div class = "row">
        @include("backend/operation/payroll/pension_administration/verification/create/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'suspense_flag' => $resource->suspense_flag])
    </div>

    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('resource_id', $resource->id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}
    {!! Form::hidden('today_date', getTodayDate(), ['class' =>'']) !!}


    <div class = "row">
        <div class="col-md-12">

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Choose Document Type:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::select('document_id', $document_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'document_id' ]) !!}
                                    {!! $errors->first('document_id', '<span class="help-block label label-danger">:message</span>') !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-md-9">
                        <div class="element-form" >
                            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    {!! Form::button('Get Documents',['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>






                <legend>Documents Attached
                </legend>
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >

                            {{--<ul>--}}
                            @foreach($documents as $doc)
                                {{--<li>--}}
                                <i class="fa fa-file-pdf-o" ></i>
                                <a  style="color:dodgerblue;" class="doc_attached"  href="#" id="{!! 'doc'. $doc->pivot->id !!}">{!! (($doc->id != 70) ? $doc->name : $doc->pivot->description) . ' (' . short_date_format($doc->pivot->doc_receive_date) . ')' . (($doc->pivot->ispending == 0) ? ' Used' : ' Not Used Yet') !!}</a>

                                    |

                                    <a class="" style="color:grey"  href="{!! route('backend.payroll.update_beneficiary_doc_use_pending_status', [$doc->pivot->id, (($doc->pivot->ispending == 0) ?  0 : 1),  (($doc->pivot->ispending == 0) ?  1 : 0)]) !!}">{!! 'Update Status' !!}</a>
                                    {{--<span style="font-size: 12px; color:#414141"> {!!   '' . ' : ' . $pollOption->votes  !!}   </span>--}}
                                    {{--<span style="font-size: 12px; color:#414141"> {!!   $pollOption->vote_percent_label  !!} </span>--}}
                                    {{--</li>--}}

                                <br/>
                            @endforeach
                            {{--</ul>--}}
                        </div>

                    </div>
                </div>


            </div>

            <div class="col-md-6">
                {{--Document Preview--}}
                <legend>Document Preview</legend>
                <br/>
                <div id="document_frame" style="text-align: center;">
                    {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                </div>

            </div>
        </div>
    </div>










    {!! Form::close() !!}
@stop


@push('after-script-end')



    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $(".search-select").select2();

            /*Documents which pending to be used list*/
            $(".doc_attached").click(function() {
                var $doc_id = this.id;
                var $pivot_id = $doc_id.substr(3);
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });



            function get_current_document($doc_pivot_id) {

                return $.ajax({
                    url: base_url + "/payroll/beneficiary/current_base64_document/{!! $member_type_id !!}/{!! $resource->id !!}/" + $doc_pivot_id,
                    dataType : 'json',
                    async : false,
                    method : "POST"
                });
            }
        });
    </script>;

@endpush
