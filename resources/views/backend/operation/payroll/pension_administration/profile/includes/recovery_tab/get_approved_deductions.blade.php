
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="approved-deductions-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Remark</th>
                <th>Total Amount</th>
                <th>Cycles</th>
                <th>Remained Cycles</th>
                <th>Recovered Amount</th>
                <th>Balance</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $("#deductions_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";

                $('#approved-deductions-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.recovery.deductions.get_approved_for_beneficiary',['member_type_id' => $member_type_id, 'resource_id' => $resource_id,
                       'employee_id' =>
                        $employee_id]) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'remark', name: 'remark', orderable : true, searchable : true},
                        { data: 'total_amount' , name: 'total_amount', orderable : true, searchable : true},
                        { data: 'cycles', name: 'cycles',  orderable : true, searchable : true},
                        { data: 'recycles', name: 'recycles',  orderable : false, searchable : false},
                        { data: 'recovered_amount', name: 'recovered_amount' ,orderable : false, searchable : false},
                        { data: 'balance_amount', name: 'balance_amount',  orderable : false, searchable : false},
                        { data: 'status', name: 'status' ,orderable : false, searchable : false},

                    ],
                    // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //     $(nRow).click(function() {
                    //         document.location.href = url +  "/payroll/bank_update/"+ "profile/" + aData['id'] ;
                    //     }).hover(function() {
                    //         $(this).css('cursor','pointer');
                    //     }, function() {
                    //         $(this).css('cursor','auto');
                    //     });
                    // }

                });

            });

        });
    </script>;

@endpush
