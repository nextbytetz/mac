
<legend class="grey_modal">Manual Payroll Pensions </legend>


<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="manual-payroll-runs-table">
            <thead>
            <tr >
                <th>Payroll Month</th>
                <th>Bank</th>
                <th>Account No.</th>
                <th>Monthly Pension</th>
                <th>Net Payable</th>
                <th>Remark</th>
            </tr>
            </thead>
        </table>

    </div>
</div>





@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $("#mp_header").one("click", function(){
                dt_payroll =  $('#manual-payroll-runs-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.manual_payroll_run.get_pensions_by_beneficiary_dt', [$member_type_id, $resource_id, $employee_id]) !!}',
                        type : 'post',

                    },

                    columns: [

                        { data: 'payroll_month', name: 'payroll_month',  orderable : false, searchable : false},
                        { data: 'bank_name', name: 'bank_name',  orderable : false, searchable : false},
                        { data: 'accountno', name: 'accountno',  orderable : false, searchable : false},
                        { data: 'mp_system', name: 'mp_system',  orderable : false, searchable : false},
                        { data: 'monthly_pension', name: 'monthly_pension',  orderable : false, searchable : false},
                        { data: 'remark', name: 'remark',  orderable : false, searchable : false},
                        // { data: 'status', name: 'status',  orderable : false, searchable : false},


                    ],
                    // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //     $(nRow).click(function() {
                    //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                    //     }).hover(function() {
                    //         $(this).css('cursor','pointer');
                    //     }, function() {
                    //         $(this).css('cursor','auto');
                    //     });
                    // }

                });
            });









        });


    </script>;

@endpush
