
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="bank-updates-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>New Bank</th>
                <th>New Branch</th>
                <th>New Account No.</th>
                <th>Date created</th>
                <th>Date approved</th>
                <th>Initiated By</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $("#bank_update_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#bank-updates-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.bank_update.get_for_beneficiary',['member_type_id' => $member_type_id, 'resource_id' => $resource_id]) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'new_bank_name', name: 'new_bank_name', orderable : false, searchable : false},
                    { data: 'new_bank_branch' , name: 'new_bank_branch', orderable : false, searchable : false},
                    { data: 'new_accountno', name: 'new_accountno',  orderable : true, searchable : true},
                    { data: 'created_at_formatted', name: 'created_at',  orderable : true, searchable : true},
                    { data: 'wf_done_date_formatted', name: 'wf_done_date' ,orderable : true, searchable : true},
                    { data: 'user', name: 'user' ,orderable : false, searchable : false},
                    { data: 'status', name: 'status' ,orderable : false, searchable : false},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/bank_update/"+ "profile/" + aData['id']  ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });

        });
    </script>;

@endpush
