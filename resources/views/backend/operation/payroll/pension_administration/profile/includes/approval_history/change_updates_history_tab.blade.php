{{--Approvals history--}}


<div class="col-md-12">
    <legend class="grey_modal">Change Updates History</legend>
    <br/>
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <div >
                    <ul class="nav nav-tabs float-xs-left" role="tablist">
                        {{--activate--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#activation"
                               data-toggle="tab">Activations
                            </a>
                        </li>

                        {{--bank--}}
                        <li class="nav-item">
                            <a class="nav-link" id = "bank_update_header"
                               href="#bank_update" data-toggle="tab">Bank Updates</a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" id = "suspension_reinstate_header"
                               href="#suspension_reinstate" data-toggle="tab">Suspensions/Reinstates</a>
                        </li>


                        {{--recovery--}}
                        <li class="nav-item">
                            <a class="nav-link" id = "recovery_header"
                               href="#recovery" data-toggle="tab">Payment Recoveries</a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" id = "beneficiary_update_header"
                               href="#beneficiary_update" data-toggle="tab">Details Update</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" id = "verification_header"
                               href="#verification_tab" data-toggle="tab">Verifications</a>
                        </li>

                        @if($resource->retireeMpUpdates()->count() > 0)
                        <li class="nav-item">
                            <a class="nav-link" id = "retiree_mp_update_header"
                               href="#retiree_mp_update_tab" data-toggle="tab">Retiree Mp Updates</a>
                        </li>
                        @endif
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" id = "mp_update_header"--}}
                               {{--href="#mp_update_tab" data-toggle="tab">MP Updates</a>--}}
                        {{--</li>--}}
                        {{--reinstate--}}
                        {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" id = "reinstate_header"--}}
                        {{--href="#reinstate" data-toggle="tab">Reinstates</a>--}}
                        {{--</li>--}}
                    </ul>
                </div>

                <div class="nav_tab_contain tab-content">

                    <div id="activation" class="nav_tab_pane tab-pane active in">

                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_activations_for_beneficiary', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' => $employee_id,])

                    </div>




                    <div id="bank_update" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_bank_updates_for_beneficiary', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' => $employee_id])
                    </div>


                    <div id="suspension_reinstate" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_susp_reins_for_beneficiary', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' => $employee_id])
                    </div>



                    <div id="recovery" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_recoveries_for_beneficiary', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' => $employee_id])
                    </div>


                    <div id="beneficiary_update" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_beneficiary_updates_for_datatable', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id,'employee_id' => $employee_id])
                    </div>

                    <div id="verification_tab" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_verifications_datatable', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id,'employee_id' => $employee_id])
                    </div>

                    <div id="retiree_mp_update_tab" class="nav_tab_pane tab-pane">

                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_retiree_mp_updates_dt', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' => $employee_id,])

                    </div>

                    <div id="mp_update_tab" class="nav_tab_pane tab-pane">

                        @include('backend/operation/payroll/pension_administration/profile/includes/approval_history/get_mp_updates_for_beneficiary', ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' => $employee_id,])

                    </div>

                </div>
            </div>


        </div>

    </div>


</div>













