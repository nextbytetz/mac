
{{--Custom filter--}}
<div class="custom_filter">
    {{--{!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}--}}
    <div class="row">
        <div class="col-md-12">
            <div class="form-row">
                {{--Member Type--}}
                <div class="form-group col-md-4 member_type">
                    <label for="member_type_id">Member Type</label>
                    {!! Form::select('member_type_id', $member_types, null, ['class' => 'form-control search-select filter_input', 'id' => 'member_type_id', 'placeholder' => '']) !!}
                </div>

                {{--Allocated User--}}
                <div class="form-group col-md-4 incident_type">
                    <label for="incident_type_id">Incident Type:</label>
                    {!! Form::select('incident_type_id', $incident_types, [], ['class' => 'form-control search-select filter_input', 'placeholder' => '', 'id' => 'incident_type_id']) !!}
                </div>

                {{--Case no--}}
                <div class="form-group col-md-4 case no">
                    <label for="case_no">Case No.</label>
                    {!! Form::text('case_no', null, ['class' => 'form-control filter_input ', 'id' => 'case_no']) !!}
                </div>

                <div class="form-group col-md-4 member_name">
                    <label for="member_name">Member Name (Suvivor/Pensioner Name)</label>
                    {!! Form::text('member_name', null, ['class' => 'form-control filter_input', 'id' => 'search_member_name']) !!}
                </div>

                {{--Other criteria--}}
                <div class="form-group col-md-4 other_criteria">
                    <label for="other_criteria">Other Criteria:</label>
                    {!! Form::select('other_criteria', ['1' => 'Not Synced', '2' => 'Synced'], [], ['class' => 'form-control search-select filter_input', 'placeholder' => '', 'id' => 'other_criteria']) !!}
                </div>

{{--selected member--}}
                {{ Form::hidden('selected_member_type_id', null, [ 'id' => 'selected_member_type_id']) }}
                {{ Form::hidden('selected_resource_id', null, ['id' => 'selected_resource_id']) }}
                {{ Form::hidden('selected_employee_id', null, ['id' => 'selected_employee_id']) }}

            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                    <input type="submit"  id="search_filter"  class="btn btn-primary btn-sm btn-submit" value="@lang('buttons.general.search')" />
                </div>
            </div>
        </div>
    </div>
    {{--{!! Form::close() !!}--}}
</div>

@push('after-script-end')


    <script  type="text/javascript">
        $(function() {

            $("#clear_filter").click(function (e) {
                $('.filter_input').val(null).change();
                $('#selected_member_label').text('').change();
            });

        });


    </script>;

@endpush
