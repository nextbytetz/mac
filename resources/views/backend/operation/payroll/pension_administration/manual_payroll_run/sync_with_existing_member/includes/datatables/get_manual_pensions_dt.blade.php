
<div class="row">
    <div class="col-md-12">
        <div class="form-row">
            {!! Form::open(['role' => 'form', 'id' => 'sync-member', 'route' => 'backend.payroll.manual_payroll_run.sync_manual_pensions_to_member']) !!}
            <div class="form-group offset-md-6 col-md-4">


                    <span class="sync_category_select">
                    <label for="sync_category"><b>Sync To:</b></label>
                                  <label id="selected_member_label"></label>
                        {{--{{ Form::hidden('selected_resource_id', null, ['id' => 'selected_resource_id']) }}--}}
                        {{--{{ Form::hidden('selected_employee_id', null, ['id' => 'selected_employee_id']) }}--}}

                        {!! Form::select('sync_category', ['1' => 'Specified By Filter', '2' => 'Synchronize to All Employees/Pensioners Only'], 1, ['class' => 'form-control search-select', 'id' => 'sync_category', 'placeholder' => '']) !!}
                </span>

                <div class="checkbox-squared">
                    {!! Form::checkbox('unsync_flag', false, false, ['id' => 'unsync_flag', 'autocomplete' => 'off']) !!}
                    <label for="unsync_flag"></label>
                    <span>Unsync Selected</span>
                </div>
            </div>


            {{--<div class="col-md-3 assign_user_select">
                <label for="assigned_user">Assign To:</label>
                {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
            </div>--}}
            <div class="form-group col-md-2">
                <label for="allocate_submit">&nbsp;</label>
                <input type="submit" class="form-control btn btn-success btn-sm" value="Submit" id="allocate_submit">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<legend class="grey_modal">Manual Payroll Pensions to sync</legend>


<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="manual-payroll-runs-table">
            <thead>
            <tr >
                <th></th>
                <th>Member Type</th>
                <th>Case no</th>
                <th>Incident Type</th>
                <th>Beneficiary Name</th>
                <th>Employee Name</th>
                {{--<th>Employer Name</th>--}}
                <th>Payroll Month</th>
                <th>Monthly Pension</th>
                <th>Status</th>

                {{--<th>Status</th>--}}

            </tr>
            </thead>
        </table>

    </div>
</div>





@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            dt_payroll =  $('#manual-payroll-runs-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.manual_payroll_run.get_payroll_runs_by_request_dt', $member_type_id) !!}',
                    type : 'post',
                    data: function ($d) {
                        $d.search_member_name = element_id_value('search_member_name');
                        $d.case_no = element_id_value('case_no');
                        $d.member_type_id =   element_id_value('member_type_id');
                        $d.incident_type_id = element_id_value('incident_type_id');
                        $d.other_criteria = element_id_value('other_criteria');
                        $d.resource_id = element_id_value('selected_resource_id');
                    }
                },
                columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                select: {
                    'style': 'multi'
                },
                columns: [
                    {
                        orderable: false,
                        searchable: false,
                        data: 'id',
                        name : 'manual_payroll_runs.id'
                    },
                    { data: 'member_type_id', name: 'member_type_id',orderable : false, searchable : false},
                    { data: 'case_no', name: 'case_no'},
                    { data: 'incident_type', name: 'incident_type'},
                    { data: 'payee_name', name: 'payee_name',  orderable : true, searchable : true},
                    { data: 'employee_name', name: 'employee_name',  orderable : true, searchable : true},
                    // { data: 'employer_name' , name: 'employer_name', orderable : false, searchable : false},
                    { data: 'payroll_month', name: 'payroll_month',  orderable : false, searchable : false},
                    { data: 'monthly_pension', name: 'monthly_pension',  orderable : false, searchable : false},
                    { data: 'status', name: 'status',  orderable : false, searchable : false},
                    // { data: 'status', name: 'status',  orderable : false, searchable : false},


                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });



            $("#sync_category").change(function (e) {
                var choice = element_id_value('sync_category');
                if(choice == '2'){
                    $('#selected_member_label').text(null).val();
                }
            });


            $("#search_filter").click(function (e) {
                dt_payroll.draw();
                e.preventDefault();
            });

            /*Search name*/
            $('body').off('keydown', '#search_member_name').on('keyup', '#search_member_name', function(e) {
                var element_id = this.id;
                var search_name = element_id_value(element_id);
                if(search_name.length > 3)
                {
                    dt_payroll.draw();
                    e.preventDefault();
                }

            });

            /*Case no*/
            $('body').off('keydown', '#case_no').on('keyup', '#case_no', function(e) {
                var element_id = this.id;
                var case_no = element_id_value(element_id);
                if(case_no.length > 1)
                {
                    dt_payroll.draw();
                    e.preventDefault();
                }

            });




            @auth
            $('#sync-member').on('submit', function($e) {
                $e.preventDefault();
                var selected_member = $('#selected_member_label').text();
                var sync_category = $('#sync_category').val();
                var unsync_flag = $('#unsync_flag').is(':checked') ? true : null;
                console.log(unsync_flag);
                var $form = this;
                var $rowsSelected = dt_payroll.column(0).checkboxes.selected();

                if((selected_member != '' && sync_category == '1') || sync_category == '2') {
                    //Remove all previous selected
                    $($form).find("input[name='id[]']").remove();
                    // Iterate over all selected checkboxes
                    $.each($rowsSelected, function ($index, $rowId) {
                        // Create a hidden element
                        $($form).append($('<input>').attr('type', 'hidden').attr('name', 'id[]').val($rowId));
                    });
                    swal({
                        title: "Warning",
                        text: "Are you sure to sync this member(s)",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "Cancel",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Confirm",
                        closeOnConfirm: true
                    }, function ($confirmed) {
                        if ($confirmed) {
                            //$form.submit();
                            //Do the ajax submission ...
                            //route : backend.claim.notification_report.allocation.assign
                            var $options = {
                                dataType: "json",
                                data: {
                                    category: $('#sync_category').val(),
                                    resource_id:  $('#selected_resource_id').val(),
                                    employee_id:  $('#selected_employee_id').val(),
                                    unsync_flag: unsync_flag,
                                },
                                type: "PUT",
                                url: $($form).attr("action"),
                                success: function (data) {
                                    if (data.success) {
                                        dt_payroll.draw();
                                        dt_payroll.column(0).checkboxes.deselectAll();
                                        $('#unsync_flag').attr('checked', false);
                                        $.amaran({
                                            'theme': 'awesome success',
                                            'content': {
                                                title: "Success",
                                                message: data.message,
                                                info: '',
                                                icon: 'fa fa-check-square-o'
                                            },
                                            'position': 'bottom left',
                                            'outEffect': 'slideBottom',
                                            'inEffect': 'slideLeft'
                                        });
                                    } else {
                                        alert(data.message);
                                        /*swal({ title : "Error Assigning User to Resource(s)", text : data.message});*/
                                    }
                                },
                                error: function (data) {

                                }
                            };
                            // pass options to ajaxForm
                            $($form).ajaxSubmit($options);
                        }
                    });
                }else{
                    alert('Make Sure you have selected member you wish to sync!')
                }
            });
            @endauth

        });


    </script>;

@endpush
