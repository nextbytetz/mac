



<div class="col-md-12">

    <div class="col-md-6">

        @include('backend/operation/payroll/pension_administration/manual_payroll_run/sync_with_existing_member/includes/datatables/get_beneficiaries_from_manual_dt')
    </div>

    <div class="col-md-6">
        {{--search filters--}}
        @include('backend/operation/payroll/pension_administration/manual_payroll_run/sync_with_existing_member/includes/search_section')
      <br/>

        @include('backend/operation/payroll/pension_administration/manual_payroll_run/sync_with_existing_member/includes/datatables/get_manual_pensions_dt')
    </div>
</div>