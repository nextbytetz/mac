<div class="row">
    {{--<div class="col-md-12">--}}
        <div class="form-row">
            {{--Member Type--}}
            <div class="form-group col-md-4 sync_status">
                <label for="member_type_id">Member Synced:</label>
               <span class="tag tag-success" > {!! $member_sync_details['member_synced'] !!} </span>
            </div>

            <div class="form-group col-md-4 sync_status">
                <label for="member_type_id">Member Not Synced:</label>
                <span class="tag tag-warning" > {!! $member_sync_details['member_not_synced'] !!} </span>
            </div>
        </div>

    {{--</div>--}}

</div>


            <legend class="grey_modal">Payroll Beneficiaries from Manual Payroll</legend>


<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="beneficiaries-from-manual-table">
            <thead>
            <tr >
                <th>Member Type</th>
                <th>Case no</th>
                {{--<th>Incident Type</th>--}}
                <th>Beneficiary Name</th>
                <th>Employee Name</th>
                <th>Employer Name</th>
                <th>Months Synced</th>
                <th>Action</th>


            </tr>
            </thead>
        </table>

    </div>
</div>





@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            var table =   $('#beneficiaries-from-manual-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.manual_payroll_run.get_beneficiary_from_manual_dt') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type_id', name: 'b.member_type_id',orderable : false, searchable : false},
                    { data: 'filename', name: 'b.filename'},
                    // { data: 'incident_type', name: 'incident_type'},
                    { data: 'member_name', name: 'b.member_name',  orderable : true, searchable : true},
                    { data: 'employee_name', name: 'b.employee_name',  orderable : true, searchable : true},
                    { data: 'employer_name' , name: 'b.employer_name', orderable : false, searchable : false},
                    { data: 'manual_payroll_runs_synced_count' , name: 'manual_payroll_runs_synced_count', orderable : true, searchable : true},
                    {
                        data: "actions", orderable: false, searchable: false, "className": "text-center",
                        render: function (data) {
                            return `
                           <div class="btn-group flex-wrap pull-right">

                            <a style="color:dodgerblue;"  class="select_member" href="#selecttosync" id="select_member"><b>{{'Select to sync'}}</b></a>
&nbsp; &nbsp;

                            </div>`
                        }
                    },



                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }



            });
            $(document).on('click', '.select_member', function () {
                var data = table.row($(this).parents('tr')).data();
               $('#search_member_name').val(data.member_name).change();
               $('#selected_member_type_id').val(data.member_type_id).change();
               $('#search_resource_id').val(data.resource_id).change();
               $('#search_employee_id').val(data.employee_id).change();
               $('#sync_category').val(1).change();
               $('#selected_member_label').text(data.member_name).change();
               /*selected*/
               $('#selected_employee_id').val(data.employee_id).change();
               $('#selected_resource_id').val(data.resource_id).change();
            });

        });


    </script>;

@endpush
