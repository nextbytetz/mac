@extends('layouts.backend.main', ['title' => 'Upload Manual Monthly Pensions of'. (($member_type_id == 5) ? 'Employees (Pensioners)' : 'Survivors'), 'header_title' => 'Upload Manual Monthly Pensions of ' . (($member_type_id == 5) ? 'Employees (Pensioners)' : 'Survivors')])

@include('backend.includes.datatable_assets')

@section('content')

    @include('backend/operation/payroll/pension_administration/manual_payroll_run/upload_bulk/includes/upload_section')

    <br/>
    <br/>
    @include('backend/operation/payroll/pension_administration/manual_payroll_run/upload_bulk/includes/get_uploaded_runs_by_member_type_dt')

@stop


@push('after-script-end')

    <script  type="text/javascript">


    </script>;

@endpush
