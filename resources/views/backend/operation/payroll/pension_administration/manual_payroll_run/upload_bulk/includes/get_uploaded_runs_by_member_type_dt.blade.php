
<legend class="grey_modal">All Uploaded Files - {!! $no_of_uploaded_files !!}</legend>


<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="uploaded-payroll-runs-table">
            <thead>
            <tr >
                <th>Member Type</th>
                <th>Case no</th>
                <th>Incident Type</th>
                <th>Beneficiary Name</th>
                <th>Employee Name</th>
                <th>Employer Name</th>
                <th>Bank</th>
                <th>Account no.</th>
                           <th>Payroll Month</th>
                <th>Monthly Pension</th>
                <th>Status</th>
                <th>Upload Error</th>
                {{--<th>Status</th>--}}

            </tr>
            </thead>
        </table>

    </div>
</div>





@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#uploaded-payroll-runs-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.manual_payroll_run.get_by_member_type_dt', $member_type_id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type_id', name: 'member_type_id',orderable : false, searchable : false},
                    { data: 'case_no', name: 'case_no'},
                    { data: 'incident_type', name: 'incident_type'},
                    { data: 'payee_name', name: 'payee_name',  orderable : true, searchable : true},
                    { data: 'employee_name', name: 'employee_name',  orderable : true, searchable : true},
                    { data: 'employer_name' , name: 'employer_name', orderable : false, searchable : false},
                    { data: 'bank_name' , name: 'bank_name', orderable : false, searchable : false},
                    { data: 'accountno', name: 'accountno',  orderable : false, searchable : false},
                    { data: 'payroll_month', name: 'payroll_month',  orderable : false, searchable : false},
                    { data: 'monthly_pension', name: 'monthly_pension',  orderable : false, searchable : false},
                    { data: 'status', name: 'status',  orderable : false, searchable : false},
                    { data: 'upload_error', name: 'upload_error',  orderable : false, searchable : false},
                    // { data: 'status', name: 'status',  orderable : false, searchable : false},


                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
