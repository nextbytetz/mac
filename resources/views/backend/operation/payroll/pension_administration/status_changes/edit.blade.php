
@extends('layouts.backend.main', ['title' => $status_change_type->name, 'header_title' => $status_change_type->name])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>


    </style>
@endpush




@section('content')

    {!! Form::model($status_change,['route' => ['backend.payroll.status_change.update',$status_change->id ],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}

    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'employee_id' => $employee_id])
    </div>
    {!! Form::hidden('status_change_type_ref', $status_change_type->reference) !!}

    @if($status_change_type->reference == 'PSCCHDREI')
        {{--child continuation--}}
        @include('backend/operation/payroll/pension_administration/status_changes/edit/edit_child_continuation', ['payroll_child_extension' => $status_change->payrollChildExtension])
    @else
        {{--default--}}
        @include('backend/operation/payroll/pension_administration/status_changes/edit/edit_default')
    @endif

    {{--button--}}
    <div class="row" align="center">
        <div class="col-md-12">
            <div class="element-form" >
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                <div class="col-xs-7 col-lg-7 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        {{--@if($member_type_id == 5)--}}
                            {{--{!! link_to('payroll/pensioner/profile/' . $resource->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}--}}
                     {{----}}
                        {{--@elseif($member_type_id == 4)--}}
                            {{--{!! link_to($resource->getProfileUrl($employee_id) . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}--}}
                        {{--@endif--}}
                            {!! link_to('payroll/status_change/profile/' . $status_change->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>








    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();


            /*------------Start Date Process ---------*/

            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            /*-----------End Date Process------------*/
            $("#firstpay_flag").change(function(){
//code here
                suspended_payment_option();
            });
            suspended_payment_option();
            function suspended_payment_option()
            {
                var choice = element_id_value('firstpay_flag');

                if(choice == 1)
                {
                    $('#suspended_payment').text('This Beneficiary has pending suspended payments which will be removed upon approval to exclude this payment during payroll processing');

                    /*Hide/disable arrears and note*/
                    enable_disable('disable_id','arrears_summary');
                    hide_show('hide_id', 'note_div');
                }else{
                    $('#suspended_payment').text('');

                    /*Show arrears/note*/
                    enable_disable('enable_id','arrears_summary');
                    hide_show('show_id', 'note_div');
                }
            }

        });
    </script>;

@endpush







