


{{--Remark--}}

<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Remark:</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group text_content">

                    {!! Form::textarea('remark', null, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                    {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>


{{--For activation and reinstate and not paid first pay--}}
@if($status_change_type->reference == 'PSCACT' || $status_change_type->reference == 'PSCREIN')
    @if(isset($status_change->firstpay_flag))
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Already Paid?:</label></div>
                    <div class="col-xs-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            {{--{{ Form::label('other_dormant_source', null, ['class' =>'required']) }}--}}
                            {!! Form::select('firstpay_flag',['0' => 'No', '1' => 'Yes'], $status_change->firstpay_flag, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'firstpay_flag', ]) !!}
                            {!! small_helper('Specify if beneficiary has already been paid monthly pension.') !!}
                            @if($resource->payrollSuspendedRuns()->where('payroll_suspended_runs.employee_id', $status_change->employee_id)->where('payroll_suspended_runs.ispaid',0)->count() > 0)
                                <label style="background-color: #f3f3f3;" id="suspended_payment"></label>
                            @endif
                            {!! $errors->first('firstpay_flag', '<span class="badge badge-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif
@endif


{{--Activation--}}
@if($status_change_type->reference == 'PSCACT')

    @if($first_pay_flag == 0)

        <div class="row" id="note_div">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Note:</label></div>
                    <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            @include('backend/operation/payroll/pension_administration/status_changes/includes/notice_summary', ['summary' => $arrears_summary])
                            {{ Form::hidden('arrears_summary', $arrears_summary, ['id' => 'arrears_summary']) }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif


@endif



{{--Deactivation--}}
@if($status_change_type->reference == 'PSCDEACT')
    {{--exit date--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Exit Date:</label></div>
                <div class="col-xs-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group text_content">
                        <div class="input-group" style="width:100%;">
                            {!! Form::text('exit_date', short_date_format($status_change->exit_date), ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>

                        <small class="form-text text-muted" style="width:100% !important;">Exit  date i.e. Death date / deactivation date from payroll</small>
                        {!! $errors->first('exit_date', '<span class="help-block label label-danger">:message</span>') !!}

                    </div>
                </div>
            </div>

        </div>
    </div>

@endif
