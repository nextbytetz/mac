{{--Document sidebar validation preview--}}

{{--sidebar document summary table--}}
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    <style>
        .list {
            display: none;
        }
        .list.open {
            display: block;
        }



        /*{*/
        /*font-family: FontAwesome;*/
        /*width: 300px;*/
        /*}*/
        .foldercontainer, .file, .noitems
        {
            display: block;
            padding: 5px 5px 5px 15px;
        }

        .file{
            padding-left: 30px;
        }
        .folder
        {
            /*color: red;*/
        }
        .file
        {
            /*color: green;*/
        }
        .folder, .file
        {
            cursor: pointer;
        }
        .noitems
        {
            display: none;
            pointer-events: none;
        }
        .folder:hover,.file:hover
        {
            background: #f7f4fd;
        }
        .folder:before, .file:before
        {
            padding-right: 10px;
        }

    </style>
@endpush
{{--Pending documents--}}

{{--<div>&nbsp;</div>--}}
<div class = "row">
    <div class="col-md-12">



        {{--Document already validated - used--}}
        <div class="col-md-6">

            {{--<div class="row">--}}

            <div class="grey_modal">
                <table style="width:100%">
                    <tr>
                        <td  align="center"><h5><b><span class="light_dark_color">Document (Evidence selection)</span></b></h5></td>
                    </tr>
                </table>
            </div>
            {{--</div>--}}

            {{--<div class="row">--}}
            <div class="light_grey_bg">&nbsp;</div>
            <div class="light_grey_bg">
                {{--<div id="folder-tree" class="nopadding"></div>--}}

                @php
$doc_evidences_arr = ($status_change->doc_evidence) ? json_decode($status_change->doc_evidence) : [];

                @endphp
                <div id="hierarchy">
                    @foreach($document_types as $document_type)
                        <div class="foldercontainer">
                            <span   class="folder fa fa-folder-open-o" data-isexpanded="true"><b> {{$document_type->name}}  </b> </span>
                            @foreach($resource->getBeneficiaryDocumentsPerType($document_type->id) as $document)
                                <span id="{{  $document->pivot->id }}"  class="file fa fa-file-pdf-o"><label id="{{ 'label' . $document->pivot->id }}">{{ $document->pivot->description  . ' (Folio - '  . $document->pivot->folio . ')'}}</label>

                                        <a style="color:green; {{ in_array($document->pivot->id, $doc_evidences_arr) ? 'display:none;' : '' }}" class="file_action_select" href="#doc" id="{{ 'file_select' . $document->pivot->id }}" > Select this</a>

                                        <a style="color:red; {{ in_array($document->pivot->id, $doc_evidences_arr) ? '' : 'display:none;' }}" class="file_action_deselect" href="#doc" id="{{ 'file_deselect' . $document->pivot->id }}" > De-select this</a>

                                </span>

                            @endforeach


                        </div>
                    @endforeach

                </div>
            </div>

            {{--</div>--}}
        </div>

        <div class="col-md-6">

            {{--<div class="row">--}}

            <div class="grey_modal">
                <table style="width:100%">
                    <tr>
                        <td  align="left"><h5><b><span class="light_dark_color">Documents Selected</span></b></h5></td>
                    </tr>
                </table>
            </div>
            {{--</div>--}}

            {{--<div class="row">--}}
            <div class="light_grey_bg">&nbsp;</div>
            <div class="light_grey_bg">

                <table id="table_selected" class="table table-striped table-bordered ">

                    <tbody>
                    @if($document_evidences)
                    @foreach($document_evidences as $doc_pivot)
                        <tr id="{{ 'tr' . $doc_pivot->id }}">
                            <td>
                                {{ $document->pivot->description  . ' (Folio - '  . $document->pivot->folio . ')' }}
                                {!! Form::hidden('doc_selected' . $doc_pivot->id ,  $doc_pivot->id,  ['id' => 'doc_selected' . $doc_pivot->id]) !!}
                            </td>
                        </tr>

                    @endforeach
                        @endif

                    </tbody>
                </table>


            </div>
        </div>


    </div>

</div>

{{--Preview--}}
<br/>
<div class = "row">
    <div class="col-md-12">
        {{--Document Preview--}}
        <legend>Document Preview</legend>
        <br/>
        <div id="document_frame" style="text-align: center;">
            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
        </div>


    </div>

</div>

@push('after-script-end')

    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}--}}

    <script>
        var $refNode = 0;
        // $("#document_centre_tab").one("click", function() {
        $(function() {



            $(".file").click(function() {
                var $pivot_id = this.id;
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');

                    $document_frame.append($iframe);

                });

            });

            /*select*/
            $(".file_action_select").click(function() {
                var element_id = this.id;
                var pivot_id = element_id.substr(11);
                appendSelectedFile(pivot_id);

                hide_show('hide_id', element_id);
                hide_show('show_id', 'file_deselect' + pivot_id);
            });

            /*deselect*/
            $(".file_action_deselect").click(function() {
                var element_id = this.id;
                var pivot_id = element_id.substr(13);
                deselectFile(pivot_id);

                hide_show('hide_id', element_id);
                hide_show('show_id', 'file_select' + pivot_id);
            });

            function appendSelectedFile(doc_pivot_id)
            {
                var doc_name = $('#label' + doc_pivot_id).text();

                var markup = "<tr  id=\""+"tr" + doc_pivot_id +"\">" +
                    '<td> -> '  + doc_name +'  <input type="hidden" id="doc_selected' + doc_pivot_id + '" name= "doc_selected' + doc_pivot_id + '" value='+ doc_pivot_id +'></td>' +
                    "</tr>";
                $("#table_selected").append(markup);

            }

            /*Deselect*/
            function deselectFile(doc_pivot_id){
                $('#tr'+ doc_pivot_id).remove();
            }

        });
        // });
        function get_current_document($doc_pivot_id) {


            return $.ajax({
                url: base_url + "/payroll/beneficiary/current_base64_document/{!! $member_type_id !!}/{!! $resource->id !!}/" + $doc_pivot_id,
                dataType : 'json',
                async : false,
                method : "POST"
            });
        }

        var hierarchy = document.getElementById("hierarchy");
        hierarchy.addEventListener("click", function(event){
            var elem = event.target;
            if(elem.tagName.toLowerCase() == "span" && elem !== event.currentTarget)
            {
                var type = elem.classList.contains("folder") ? "folder" : "file";
                // if(type=="file")
                // {
                //   // alert(this.id);
                // }
                if(type=="folder")
                {
                    var isexpanded = elem.dataset.isexpanded=="true";
                    if(isexpanded)
                    {
                        elem.classList.remove("fa-folder-o");
                        elem.classList.add("fa-folder");
                    }
                    else
                    {
                        elem.classList.remove("fa-folder");
                        elem.classList.add("fa-folder-o");
                    }
                    elem.dataset.isexpanded = !isexpanded;

                    var toggleelems = [].slice.call(elem.parentElement.children);
                    var classnames = "file,foldercontainer,noitems".split(",");

                    toggleelems.forEach(function(element){
                        if(classnames.some(function(val){return element.classList.contains(val);}))
                            element.style.display = isexpanded ? "none":"block";
                    });
                }
            }
        });



    </script>;

@endpush