

{{--General info--}}
<br/>
<div class="row">
    {{--left general  summary--}}


    <div class="col-md-6">


        <div class="col-md-12">



            <table class="table table-striped table-bordered">
                <tbody>

                {{--Check if Notification already completed workflow--}}
                @if(isset($dependent_employee->notification_report_id ) || isset($dependent_employee->manual_notification_report_id ))

                    {{--start pay date--}}

                    <tr>
                        <td width="120px">{!! $dependent_employee->dependent_type_id != 7 ? 'Employee Death Date' : 'Date of MMI of employee' !!}</td>
                        <th>{!! Form::label( 'start_date',
                          !is_null($dependent->getStartPayDateAttribute($dependent_employee->employee_id)) ?
                            short_date_format($dependent->getStartPayDateAttribute($dependent_employee->employee_id)) : null, [])
                                !!} </th>
                    </tr>

                    {{--Status--}}
                    <tr>
                        <td>Status</td>
                        <th>
                            {!! $dependent->getStatusLabelAttribute($employee->id) !!}

                        </th>
                    </tr>


                    {{--if is MP Receiver--}}
                    @if($dependent_employee->survivor_pension_percent > 0)
                        <tr>
                            <td>First Pay Status (MP)</td>

                            @if(isset($status_change_firstpay_flag))
                                <th>
                                    {!! ($status_change_firstpay_flag == 0) ? 'Not Yet Paid' : 'Already Paid'
                                                !!}</th>
                            @else
                                <th>
                                @if($dependent_employee->firstpay_flag == 1)
                                    <span class="tag tag-success" data-toggle="tooltip" data-html="true" title="Paid" > Paid   </span>
                                @else
                                    <span class="tag tag-warning" data-toggle="tooltip" data-html="true" title="Pending" > Pending   </span>
                                    @endif
                                    </th>
                            @endif




                        </tr>
                    @endif



                    {{--<tr>--}}
                        {{--<td>Last Response</td>--}}
                        {{--<th>--}}
                            {{--{!! Form::label( 'last_response',--}}
                                    {{--$dependent->last_response_formatted, [])--}}
                                        {{--!!}</th>--}}
                    {{--</tr>--}}



                    <tr>
                        <td>Email</td>
                        <th>
                            {!! $dependent->email
                                        !!}</th>
                    </tr>

                @else
                    <tr>
                        <td>Claim Status</td>
                        <th>
                            <span class="tag tag-warning" data-toggle="tooltip" data-html="true" title="Pending" > Pending   </span>

                        </th>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>
    </div>



    {{--Right summary--}}

    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>


                {{--MP--}}
                @if($dependent_employee->survivor_pension_percent > 0)
                    <tr>
                        <td width="180px">Monthly Pension {!! ' (' . $dependent_employee->survivor_pension_percent . '%)' !!}</td>
                        <th>{!! Form::label( 'enrolled_date',
                            number_2_format($dependent_employee->survivor_pension_amount) , [])
                                !!} </th>
                    </tr>
                @endif


                {{--Bank--}}
                <tr>
                    <td width="120px">Bank</td>
                    <th>{!! Form::label( 'bank',
                  isset($dependent->bank_branch_id) ? $dependent->bankBranch->bank->name : ( isset($dependent->bank_id) ? $dependent->bank->name : ' '), [])
                !!} </th>
                </tr>

                {{--Branch--}}
                <tr>
                    <td>Branch</td>
                    <th>
                        {!!   ($dependent->bank_branch_id) ? $dependent->bankBranch->name : ' '
                        !!}</th>
                </tr>


                <tr>
                    <td>Account No.</td>
                    <th>
                        {!! $dependent->accountno
                        !!}</th>
                </tr>


                <tr>
                    <td>Phone</td>
                    <th>
                        {!! $dependent->phone
                                    !!}</th>
                </tr>


                </tbody>
            </table>





        </div>
    </div>





</div>
