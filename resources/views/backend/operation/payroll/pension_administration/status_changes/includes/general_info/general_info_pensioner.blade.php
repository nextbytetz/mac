

{{--General info--}}
<br/>
<div class="row">
    {{--left general  summary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>
                {{--enrolled date--}}
                <tr>
                    <td width="120px">{!! 'Date of MMI' !!}</td>
                    <th>{!! Form::label( 'start_date',
                                 isset($pensioner->start_pay_date) ?  short_date_format($pensioner->start_pay_date) : null, [])
                                !!} </th>
                </tr>



                {{--Status--}}
                <tr>
                    <td>Status</td>
                    <th>
                        {!!  $pensioner->status_label
                                    !!}</th>
                </tr>





                    <tr>
                        <td>First Pay Status</td>
                        @if(isset($status_change_firstpay_flag))
                            <th>
                                {!! ($status_change_firstpay_flag == 0) ? 'Not Yet Paid' : 'Already Paid'
                                            !!}</th>
                            @else
                        <th>
                            {!! $pensioner->first_pay_status_label
                                        !!}</th>
                        @endif
                    </tr>

                {{--<tr>--}}
                {{--<td>Last Response</td>--}}
                {{--<th>--}}
                {{--{!! Form::label( 'last_response',--}}
                {{--$pensioner->last_response_formatted, [])--}}
                {{--!!}</th>--}}
                {{--</tr>--}}

                <tr>
                    <td>Phone</td>
                    <th>
                        {!!
                                $pensioner->phone
                                    !!}</th>
                </tr>


                </tbody>
            </table>

        </div>
    </div>



    {{--Right summary--}}

    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>


                {{--MP--}}
                <tr>
                    <td width="120px">Monthly Pension</td>
                    <th>{!! Form::label( 'enrolled_date',
                            $pensioner->mp_formatted, [])
                                !!} </th>
                </tr>

                {{--Bank--}}
                <tr>
                    <td width="120px">Bank</td>
                    <th>{!! Form::label( 'bank',
                          isset($pensioner->bank_branch_id) ? $pensioner->bankBranch->bank->name : (isset($pensioner->bank_id) ? $pensioner->bank->name : ' '), [])
                                !!} </th>
                </tr>

                {{--Branch--}}
                <tr>
                    <td>Branch</td>
                    <th>
                        {!!  isset($pensioner->bank_branch_id) ? $pensioner->bankBranch->name : ' '
                                    !!}</th>
                </tr>


                <tr>
                    <td>Account No.</td>
                    <th>
                        {!! $pensioner->accountno
                                    !!}</th>
                </tr>

                <tr>
                    <td>Email</td>
                    <th>
                        {!! $pensioner->email
                                    !!}</th>
                </tr>




                </tbody>
            </table>

        </div>
    </div>





</div>
