


<nav class="navbar navbar-light bg-light" style="background-color: #ffe6e9;">


    <span > {!!  'This child is already overage as of today, but was eligible for payroll at time of death of employee, hence dependent will be paid only once to recover all arrears of this amount ' . '<b class=underline>'.    number_2_format($child_overage_eligible_data['arrears']) . '</b>' .  ' for eligible period of '  . '<b class=underline>'.    number_2_format($child_overage_eligible_data['first_pay_months']) . '</b>' . ' months' . ' from death date, '  . '<b class=underline>'.  short_date_format($child_overage_eligible_data['death_date']) . '</b>'  . ' to ' . 'last month of eligibility, ' . '<b class=underline>'.  short_date_format($child_overage_eligible_data['child_deadline_eligible']) . '</b>' . '. Dependent once paid will automatically be deactivated from payroll. Date of birth of dependent is ' . '<b class=underline>'.    short_date_format($child_overage_eligible_data['dob']) . '.'.'</b>'  !!}&nbsp</span>


</nav>