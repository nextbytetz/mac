
<div class="row">

    <div class="col-md-12">
        <div class="col-md-6">

            {{--Continuation Reason type--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Child Extension Reasons:</label></div>
                        <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group">

                                {!! Form::select('child_continuation_reason_cv_ref', $code_value_repo->getCodeValuesReferenceByCodeForSelect(66),null, ['class' => 'form-control search-select', 'id' => 'child_continuation_reason_cv_ref', 'placeholder' => '']) !!}
                                {!! $errors->first('child_continuation_reason_cv_ref', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--Deadline date--}}
            <div class="row child_continuation_div" id="deadline_date_div">
                <div class="col-md-12">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Deadline Date:</label></div>
                        <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group">

                                <div class="input-group" style="width:100%;">
                                    {!! Form::text('deadline_date', null, ['placeholder' => '', 'class' => 'form-control child_continuation_input datepicker1', 'autocomplete' => 'off', 'id' => 'deadline_date']) !!}
                                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                </div>

                                <small class="form-text text-muted" style="width:100% !important;">Deadline date for receiving monthly pension i.e. Date expected to complete school</small>
                                {!! $errors->first('deadline_date', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            {{--school name--}}
            <div class="row child_continuation_div">
                <div class="col-md-12">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>School Name:</label></div>
                        <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group">

                                {!! Form::text('school_name',null, ['class' => 'form-control child_continuation_input', 'id' => 'school_name', 'placeholder' => '']) !!}
                                {!! $errors->first('school_name', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            {{--Education level--}}
            <div class="row child_continuation_div">
                <div class="col-md-12">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Education Level:</label></div>
                        <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group">

                                {!! Form::select('education_level_cv_ref', $code_value_repo->getCodeValuesReferenceByCodeForSelect(69),null, ['class' => 'form-control child_continuation_input search-select', 'id' => 'education_level_cv_ref', 'placeholder' => '']) !!}
                                {!! $errors->first('education_level_cv_ref', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--school grade--}}
            <div class="row child_continuation_div">
                <div class="col-md-12">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>School Grade/Class:</label></div>
                        <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group">
                                {!! Form::text('school_grade',null, ['class' => 'form-control child_continuation_input', 'id' => 'school_grade', 'placeholder' => '']) !!}
                                {!! $errors->first('school_grade', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            {{--Has arrears--}}
            {{--<div class="row arrears_div child_continuation_div" id="hasarrears_div">--}}
            {{--<div class="col-md-12">--}}
            {{--<div class="element-form" >--}}
            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Has Arrears?</label><span class="required_asterik">*</span></div>--}}
            {{--<div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">--}}
            {{--<div class="form-group">--}}
            {{--{!!  Form::select('hasarrears', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select arrears child_continuation_input' ,'placeholder'=> '', 'id'=>'hasarrears']) !!}--}}
            {{--{!! $errors->first('hasarrears', '<span class="help-block label label-danger">:message</span>') !!}--}}
            {{--<small class="form-text text-muted" style="width:100% !important;">Does dependent has any arrears (unpaid) montly pension?</small>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}


            {{--<div class="row pending_months_div child_continuation_div" id="pending_months_div">--}}
            {{--<div class="col-md-12">--}}
            {{--<div class="element-form" >--}}
            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>No. of months pending</label><span class="required_asterik">*</span></div>--}}
            {{--<div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">--}}
            {{--<div class="form-group">--}}
            {{--{!! Form::text('pending_pay_months', null , ['placeholder' => '', 'class' => 'form-control number child_continuation_input', 'autocomplete' => 'off', 'id' => 'pending_pay_months']) !!}--}}
            {{--{!! $errors->first('pending_pay_months', '<span class="help-block label label-danger">:message</span>') !!}--}}
            {{--<small class="form-text text-muted" style="width:100% !important;">No. of months not yet paid to this dependent.</small>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}


            {{--Remark--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Remark:</label></div>
                        <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group text_content">

                                {!! Form::textarea('remark', null, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                                {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{--Arrears summary--}}
            <div class="row" id="child_note_div" {{--style="display:none;"--}}>
                <div class="col-md-12">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Note:</label></div>
                        <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group">
                                @include('backend/operation/payroll/pension_administration/status_changes/includes/notice_summary', ['summary' => $arrears_summary['summary']])
                                {{ Form::hidden('arrears_summary', $arrears_summary['summary'], ['id' => 'arrears_summary']) }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>



        {{--Document--}}
        <div class="col-md-6">

            @include('backend/operation/payroll/pension_administration/status_changes/create/includes/doc_evidence_selection')
        </div>


    </div>

</div>



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">


        $(function () {

            continuationReasonOption();
            $("#child_continuation_reason_cv_ref").change(function(){
//code here
                continuationReasonOption();
            });


            function continuationReasonOption(){
                var choice = element_id_value('child_continuation_reason_cv_ref');
                hide_show('hide_class', 'child_continuation_div');
                enable_disable('disable_class', 'child_continuation_input');

                /*Note*/
                // hide_show('hide_id', 'child_note_div');
                if(choice == 'PAYCHCOSCH'){
                    /*Deadline*/
                    hide_show('show_class', 'child_continuation_div');
                    enable_disable('enable_class', 'child_continuation_input');
                    // /*Has arrears*/
                    // hide_show('show_id', 'hasarrears_div');
                    // enable_disable('enable_id', 'hasarrears');
                    // hasarrears_option();

                    /*Note*/
                    // hide_show('hide_id', 'child_note_div');
                }else if(choice == 'PAYCHCDIS'){
                    /*Note*/
                    // hide_show('show_id', 'child_note_div');
                }

                jQuery('.select2-container').css('width','100%');
            }

            /*Option on choosing has arrears*/
            hasarrears_option();
            $("#hasarrears").on("change", function (e) {
                hasarrears_option();
            });

            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });


            /*has arrears option*/
            function hasarrears_option()
            {
                var hasarrears =$("#hasarrears").val();
                if(hasarrears == 1){
                    $("#pending_pay_months").prop("disabled", false);
                    // $("#remark").prop("disabled", false);
                    $(".pending_months_div").show();
                }else{
                    $("#pending_pay_months").prop("disabled", true);
                    // $("#remark").prop("disabled", true);
                    $(".pending_months_div").hide();
                }
            }


        });
    </script>;

@endpush







