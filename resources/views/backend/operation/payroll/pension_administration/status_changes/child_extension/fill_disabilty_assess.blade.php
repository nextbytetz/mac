
@extends('layouts.backend.main', ['title' => 'Child Extension: Disability', 'header_title' => 'Child Extension: Disability'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    @include('backend.includes.assets.datetimepicker')
    <style>


    </style>
@endpush

@if(env('TESTING_MODE') == 1)
    @php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    @endphp
@endif

@section('content')


    {!! Form::model($status_change,['route' => ['backend.payroll.status_change.store_child_disability_assess',$status_change->id ],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}

    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'employee_id' => $employee_id])
    </div>
    {!! Form::hidden('this_date', getTodayDate(), ['class' => 'this_date']) !!}
    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('resource_id', $resource->id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}


    <div class="row">

        <div class="col-md-12">
            <div class="col-md-6">

                {{--Provision Period type--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Period Provision Type:</label></div>
                            <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="form-group">

                                    {!! Form::select('period_provision_type',['1' => 'Specific Period', '2' => 'Lifetime'],$payroll_child_extension->period_provision_type, ['class' => 'form-control search-select', 'id' => 'period_provision_type', 'placeholder' => '']) !!}
                                    {!! $errors->first('period_provision_type', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--Deadline date--}}
                <div class="row" id="deadline_date_div">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Deadline Date:</label></div>
                            <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="form-group">

                                    <div class="input-group" style="width:100%;">
                                        {!! Form::text('deadline_date', short_date_format($payroll_child_extension->deadline_date), ['placeholder' => '', 'class' => 'form-control child_continuation_input datepicker1', 'autocomplete' => 'off', 'id' => 'deadline_date']) !!}
                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>

                                    <small class="form-text text-muted" style="width:100% !important;">Date for next re-examination of disability</small>
                                    {!! $errors->first('deadline_date', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    {{--button--}}
    <div class="row" align="center">
        <div class="col-md-12">
            <div class="element-form" >
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                <div class="col-xs-7 col-lg-7 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">

                        {!! link_to('payroll/status_change/profile/' . $status_change->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>








    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();

            $("#period_provision_type").change(function(){
//code here
                periodProvisionOption();
            });

            /*------------Start Date Process ---------*/

            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                minDate: today_date,
            });


            /*-----------End Date Process------------*/

            periodProvisionOption();
            function periodProvisionOption()
            {
                var choice = element_id_value('period_provision_type');
                if(choice == '1')
                {
                    enable_disable('enable_id', 'deadline_date');
                    hide_show('show_id', 'deadline_date_div');
                }else{
                    enable_disable('disable_id', 'deadline_date');
                    hide_show('hide_id', 'deadline_date_div');
                }
            }



        });
    </script>;

@endpush







