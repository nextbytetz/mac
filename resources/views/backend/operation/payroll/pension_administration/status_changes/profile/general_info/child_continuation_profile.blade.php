

{{--main tab content--}}
<div class = "row">
    <div class="col-md-12">

        <div class="col-md-6">

            {{--general info--}}
            @include('backend/operation/payroll/pension_administration/status_changes/profile/general_info/child_continuation/child_continuation_general_info')
            {{--disease adjust overview--}}
            <div>&nbsp;</div>
            {{--APprovals tabs history--}}
            {{--@include('backend/operation/payroll/pension_administration/profile/pensioner/includes/general/approvals')--}}
            {{--{!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}--}}
            @include("backend/includes/workflow/wf_track_html", $workflow_input)
        </div>
        <div class="col-md-6">
            <br/>
@include('backend/operation/payroll/pension_administration/status_changes/profile/general_info/child_continuation/document_review')

        </div>
    </div>
</div>