

{{--General info--}}
<br/>
<legend style="background-color: lightgrey">{{ 'Status Change: ' . $status_change->statusChangeType->name    }}

</legend>

@include('backend/operation/payroll/pension_administration/status_changes/includes/notice_summary', ['summary'=> '<b> NOTE: </b>' .  $status_change->arrears_summary])

<div class="row">
    {{--left general  summary--}}

    <div class="col-md-12">

        <table class="table table-striped table-bordered">
            <tbody>
            {{--Status--}}
            <tr>
                <td>Reason</td>
                <th>
                    {!! $status_change->payrollChildExtension->childContinuationReason->name !!}

                </th>
            </tr>
            @if($status_change->deadline_date)
                <tr>
                    <td>Deadline date
                        <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="{!! 'Date expected to finish school or Date to re-examine disability' !!}"></i>
                    </td>
                    <th>
                        {!! short_date_format($status_change->deadline_date ) !!}

                    </th>
                </tr>
            @endif

            @if($status_change->payrollChildExtension->childContinuationReason->reference == 'PAYCHCOSCH')
                <tr>
                    <td>School name</td>
                    <td>
                        {!! $status_change->payrollChildExtension->school_name !!}

                    </td>
                </tr>

                <tr>
                    <td>Education Level</td>
                    <td>
                        {!! $status_change->payrollChildExtension->educationLevel->name !!}

                    </td>
                </tr>

                <tr>
                    <td>School grade</td>
                    <td>
                        {!! $status_change->payrollChildExtension->school_grade !!}

                    </td>
                </tr>

            @elseif($status_change->payrollChildExtension->childContinuationReason->reference == 'PAYCHCDIS')
                @if($status_change->payrollChildExtension->period_provision_type)
                    <tr>
                        <td>Period Provision Type</td>
                        <td>
                            {!! $status_change->payrollChildExtension->period_provision_type_label !!}

                        </td>

                    </tr>
                @endif

            @endif
            <tr>
                <td>Remark</td>
                <td>
                    {!! $status_change->remark !!}

                </td>
            </tr>
            @if($status_change->payrollChildExtension->isletter_prepared == 1)
                <tr>
                    <td>Letter Prepared</td>
                    <td>
                        {!! 'Yes' !!}

                    </td>
                </tr>
            @endif
            </tbody>
        </table>






        <legend>General Information</legend>
        <table class="table table-striped table-bordered">
            <tbody>
            {{--enrolled date--}}
            {{--Check if Notification already completed workflow--}}
            @if(isset($dependent_employee->notification_report_id ) || isset($dependent_employee->manual_notification_report_id ))

                {{--start pay date--}}

                <tr>
                    <td width="120px">{!! $dependent_employee->dependent_type_id != 7 ? 'Employee Death Date' : 'Date of MMI of employee' !!}</td>
                    <th>{!! Form::label( 'start_date',
                          !is_null($dependent->getStartPayDateAttribute($dependent_employee->employee_id)) ?
                            short_date_format($dependent->getStartPayDateAttribute($dependent_employee->employee_id)) : null, [])
                                !!} </th>
                </tr>

                {{--Status--}}
                <tr>
                    <td>Status</td>
                    <th>
                        {!! $dependent->getStatusLabelAttribute($employee->id) !!}

                    </th>
                </tr>


                {{--if is MP Receiver--}}
                @if($dependent_employee->survivor_pension_percent > 0)
                    <tr>
                        <td>First Pay Status (MP)</td>

                        @if(isset($status_change_firstpay_flag))
                            <th>
                                {!! ($status_change_firstpay_flag == 0) ? 'Not Yet Paid' : 'Already Paid'
                                            !!}</th>
                        @else
                            <th>
                                @if($dependent_employee->firstpay_flag == 1)
                                    <span class="tag tag-success" data-toggle="tooltip" data-html="true" title="Paid" > Paid   </span>
                                @else
                                    <span class="tag tag-warning" data-toggle="tooltip" data-html="true" title="Pending" > Pending   </span>
                                @endif
                            </th>
                        @endif




                    </tr>
                @endif

                {{--MP--}}
                @if($dependent_employee->survivor_pension_percent > 0)
                    <tr>
                        <td width="180px">Monthly Pension {!! ' (' . $dependent_employee->survivor_pension_percent . '%)' !!}</td>
                        <th>{!! Form::label( 'enrolled_date',
                            number_2_format($dependent_employee->survivor_pension_amount) , [])
                                !!} </th>
                    </tr>
                @endif

                <tr>
                    <td>Phone</td>
                    <th>
                        {!! $dependent->phone
                                    !!}</th>
                </tr>


            @endif


            </tbody>
        </table>

    </div>
</div>




