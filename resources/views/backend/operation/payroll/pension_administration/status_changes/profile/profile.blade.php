@extends('layouts.backend.main', ['title' =>   $status_change_type->name .  ' Approval Profile', 'header_title' => $status_change_type->name .  ' Approval Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>

        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush



@section('content')


    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id,'employee_id' => $employee_id])
    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >


                                        @if($check_if_can_initiate_disability_assess)
                                            <a href="{!! route('backend.payroll.status_change.fill_child_disability_assess',[ 'payroll_status_change'=> $status_change->id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-wheelchair"></i>&nbsp; Update Disability </a>
                                        @endif

                                            @if($check_if_can_initiate_letter)
                                                {{--<a href="{!! route('backend.payroll.status_change.update_letter_prepared_status',[ 'payroll_status_change'=> $status_change->id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-file-pdf-o"></i>&nbsp; Letter Prepared </a>--}}
                                                {!! HTML::decode(link_to_route('backend.payroll.status_change.update_letter_prepared_status', "<i class='icon fa fa-file' aria-hidden='true'></i>&nbsp;" . 'Letter Prepared', $status_change->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to update letter Status?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            @endif

                                        @if($check_pending_level1 == 1)
                                            <span>
                                                    @if($status_change->statusChangeType->reference == 'PSCVERIF')
                                                    {{--Verification edit--}}
                                                    <a href="{!! route('backend.payroll.verification.edit',[ 'status_change'=> $status_change->payrollVerification->id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;
                                                        @else
                                                            {{--default edit--}}
                                                            <a href="{!! route('backend.payroll.status_change.edit',[ 'status_change'=> $status_change->id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;
                                                                @endif
                                                                Edit
                                            </a>
                                     </span>

                                            <span>
                                           {!! HTML::decode(link_to_route('backend.payroll.status_change.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $status_change->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this Status change?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                        @endif

                                        @if($member_type_id == 5)
                                            {!!                                          HTML::decode( link_to('payroll/pensioner/profile/' . $resource->id . '#activation',trans('buttons.general.close'),['id'=> 'close', 'class' => 'tn btn-primary site-btn nav_button icon fa
                                            fa-close', ])) !!}
                                        @elseif($member_type_id == 4)
                                            {!!                                          HTML::decode( link_to('compliance/dependent/profile/' . $dependent_input['dependent_employee']->id . '#activation',trans('buttons.general.close'),['id'=> 'close', 'class' => 'tn btn-primary site-btn nav_button icon fa
                                            fa-close', ])) !!}
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--main tab content--}}
                        @if($status_change->statusChangeType->reference == 'PSCVERIF')
                            {{--Verification--}}
                            @include('backend/operation/payroll/pension_administration/status_changes/profile/general_info/verification_profile')
                        @elseif(($status_change->statusChangeType->reference == 'PSCCHDREI'))
                            {{--child contnuation--}}
                            @include('backend/operation/payroll/pension_administration/status_changes/profile/general_info/child_continuation_profile', ['dependent' => $resource, 'employee' => $dependent_input['employee'], 'dependent_employee' => $dependent_input['dependent_employee'], 'status_change_firstpay_flag'=> $status_change->firstpay_flag])
                        @else
                            {{--Default profile--}}
                            <div class = "row">
                                <div class="col-md-12">

                                    <div class="col-md-9">
                                        {{--general info--}}
                                        <br/>
                                        <legend style="background-color: lightgrey">{{ 'Status Change: ' . $status_change->statusChangeType->name   }}</legend>
                                        @if(isset($arrears_summary) && ($status_change->statusChangeType->reference == 'PSCACT'))
                                            @include('backend/operation/payroll/pension_administration/status_changes/includes/notice_summary', ['summary'=> '<b> NOTE: </b>' .  $arrears_summary])
                                        @endif

                                        @if($member_type_id == 5)
                                            @include('backend/operation/payroll/pension_administration/status_changes/includes/general_info/general_info_pensioner', ['pensioner' => $resource, 'status_change_firstpay_flag'=> $status_change->firstpay_flag])
                                        @elseif($member_type_id == 4)
                                            @include('backend/operation/payroll/pension_administration/status_changes/includes/general_info/general_info_dependent',  ['dependent' => $resource, 'employee' => $dependent_input['employee'], 'dependent_employee' => $dependent_input['dependent_employee'], 'status_change_firstpay_flag'=> $status_change->firstpay_flag])
                                        @endif

                                        {{--Remark--}}
                                        <div class="col-md-6">
                                    <span>
                                        <b>Remark:</b> {!! $status_change->remark !!}
                                    </span>
                                            <br/>
                                            @if($status_change->exit_date != null)
                                                <span>
                                        <b>Exit Date:</b> {!! short_date_format($status_change->exit_date) !!}
                                    </span>
                                            @endif

                                        </div>
                                        <div>&nbsp;</div>
                                        {{--Approvals tabs history--}}
                                        {{--@include('backend/operation/payroll/pension_administration/profile/pensioner/includes/general/approvals')--}}
                                        {{--{!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}--}}
                                        @include("backend/includes/workflow/wf_track_html", $workflow_input)
                                    </div>
                                    <div class="col-md-3">
                                        {{--sidebar summary--}}
                                        @include('backend/operation/payroll/pension_administration/status_changes/profile/sidebar_summary/sidebar')
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>


                </div>
            </div>
        </div>

    </div>

    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">

        $(function () {

        });
    </script>;

@endpush
