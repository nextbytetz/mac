

{{--General info--}}
<br/>
<legend style="background-color: lightgrey">{{ 'Status Change: ' . $status_change->statusChangeType->name    }}

</legend>

@include('backend/operation/payroll/pension_administration/verification/includes/notice_summary', ['employee_id' => $status_change->employee_id, 'member_type_id' => $status_change->member_type_id, 'resource_id' => $status_change->resource_id])

<div class="row">
    {{--left general  summary--}}

    <div class="col-md-12">
        <table class="table table-striped table-bordered">
            <tbody>
            {{--enrolled date--}}
            <tr>
                <td width="120px">{!! 'Verification Date' !!}</td>
                <th>{{ short_date_format($status_change->payrollVerification->verification_date)  }}</th>
            </tr>



            {{--Status--}}
            <tr>
                <td>Doc Folio No.</td>
                <th>
                    {{ $status_change->payrollVerification->folionumber }}</th>
            </tr>

            @if($status_change->payrollVerification->iseducation == 1)
            <tr>
                <td>Attending School</td>
                <th>
                    {{ 'Yes'}}</th>
            </tr>
@endif
            @if($status_change->payrollVerification->isdisabled == 1)
                <tr>
                    <td>Has Disability?</td>
                    <th>
                        {{ 'Yes'}}</th>
                </tr>
            @endif

            <tr>
                <td>Phone</td>
                <th>
                    {{ $status_change->payrollVerification->phone }}</th>
            </tr>

            <tr>
                <td>Next Kin Phone</td>
                <th>
                    {{ $status_change->payrollVerification->next_kin_phone }}</th>
            </tr>

            <tr>
                <td>Region</td>
                <th>
                    {{ ($status_change->payrollVerification->region_id) ? $status_change->payrollVerification->region->name : '' }}</th>
            </tr>

            <tr>
                <td>District</td>
                <th>
                    {{ ($status_change->payrollVerification->district_id) ? $status_change->payrollVerification->district->name : '' }}</th>
            </tr>

            <tr>
                <td>Remark</td>
                <th>
                    {{ $status_change->remark }}</th>
            </tr>

            </tbody>
        </table>

    </div>
</div>




