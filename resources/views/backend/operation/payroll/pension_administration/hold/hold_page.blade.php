
@extends('layouts.backend.main', ['title' => 'Archive Beneficiary', 'header_title' => 'Archive Beneficiary'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    @include('backend.includes.assets.datetimepicker')
    <style>


    </style>
@endpush

@section('content')


    {!! Form::open(['route' => ['backend.payroll.beneficiary.hold' ],'method'=>'put', 'name' => 'create']) !!}

    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'employee_id' => $employee_id])
    </div>
    {!! Form::hidden('this_date', getTodayDate(), ['class' => 'this_date']) !!}
    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('resource_id', $resource->id) !!}
    {{--{!! Form::hidden('status', $status) !!}--}}
    {!! Form::hidden('employee_id', $employee_id) !!}

    {{--Remark--}}

    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Archive Reason:</label></div>
                <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group text_content">

                        {!! Form::textarea('hold_reason', ($isactive == 3) ? $hold_reason : null, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                        {!! $errors->first('hold_reason', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>




    {{--button--}}
    <div class="row" align="center">
        <div class="col-md-12">
            <div class="element-form" >
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                <div class="col-xs-7 col-lg-7 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        @if($member_type_id == 5)
                            {!! link_to('payroll/pensioner/profile/' . $resource->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @elseif($member_type_id == 4)
                            {!! link_to('compliance/dependent/profile/' . $dependent_employee->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @endif
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>








    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();




        });
    </script>;

@endpush







