{{--Document sidebar validation preview--}}

{{--sidebar document summary table--}}
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    <style>
        .list {
            display: none;
        }
        .list.open {
            display: block;
        }

    </style>
@endpush
{{--Pending documents--}}
@php
$allow_edit = isset($allow_edit) ? $allow_edit : 1;
@endphp
{{--<div>&nbsp;</div>--}}
<div class = "row">
    <div class="col-md-12">

        <div class="col-md-6">
            {{--<div class="row">--}}

            <div class="grey_modal">
                <table style="width:100%">
                    <tr>
                        <td  align="center"><h5><b><span class="light_dark_color">Document - Pending</span></b></h5></td>
                    </tr>
                </table>
            </div>
            {{--</div>--}}

            {{--<div class="row">--}}
            <div class="light_grey_bg">&nbsp;</div>
            <div class="light_grey_bg">

                <ul class="list-unstyled " id="pending_list">
                    @foreach($resource->getPendingDocumentForValidationPerType($document_type) as $document)
                        <li id={!!  $document->pivot->id !!}>
                            &nbsp;&nbsp;  <i class="icon fa fa-file-pdf-o"> - <a href="#" data-toggle="modal" data-target="#open_document_modal" id="open_document_link"   style="color: dodgerblue" class="pull-center underline">  {!! $document->pivot->description . ' (Folio - ' . $document->pivot->folio . ')'  !!}</a></i>
                            @if($document->pivot->ispending == 1)
                                &nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
                            @if($allow_edit)
                                <a style="color: seagreen; font-size: 11px;" class="pull-center underline" href="{!! route('backend.payroll.update_beneficiary_doc_use_status', ['doc_id' => $document->pivot->id, 'status' => 1]) !!}" > Update as used &nbsp; </a>
                                @endif
                            @endif
                        </li>

                    @endforeach
                </ul>



                {{--</div>--}}
            </div>
        </div>

        {{--@include('backend/operation/payroll/pension_administration/includes/open_document_modal')--}}


        {{--Document already validated - used--}}
        <div class="col-md-6">

            {{--<div class="row">--}}

            <div class="grey_modal">
                <table style="width:100%">
                    <tr>
                        <td  align="center"><h5><b><span class="light_dark_color">Document History</span></b></h5></td>
                    </tr>
                </table>
            </div>
            {{--</div>--}}

            {{--<div class="row">--}}
            <div class="light_grey_bg">&nbsp;</div>
            <div class="light_grey_bg">
                {{--<div id="folder-tree" class="nopadding"></div>--}}
                <ul class="list-unstyled" >
                    <li class="folder" class="hide"  id="list_pending_id" data-toggle=".list">  <i class="fa fa-folder-open-o"></i>&nbsp;<a href="#"><b>Documents already used</b></a>
                        @foreach($resource->getDocumentUsedForValidationPerType($document_type) as $used_document)
                            <ul class="list-unstyled list" id="used_list">
                                <li id={!!  $used_document->pivot->id !!}>
                                    &nbsp;&nbsp;  <i class="fa fa-file-pdf-o"></i>&nbsp;<span class="text-success"><a href="#" data-toggle="modal" data-target="#open_document_modal" >{!! $used_document->pivot->description  . ' (Folio - '  . $used_document->pivot->folio . ')'
                                !!}</a></span>
                                </li>
                            </ul>
                        @endforeach
                    </li>
                </ul>


            </div>


            {{--</div>--}}
        </div>

    </div>

</div>

{{--Preview--}}
<br/>
<div class = "row">
    <div class="col-md-12">
        {{--Document Preview--}}
        <legend>Document Preview</legend>
        <br/>
        <div id="document_frame" style="text-align: center;">
            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
        </div>


    </div>

</div>

@push('after-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}

    <script>
        var $refNode = 0;
        // $("#document_centre_tab").one("click", function() {
        $(function() {

            $(function () {
                $('[data-toggle]').on('click', function () {
                    var id = $(this).data("toggle"),
                        $object = $(id),
                        className = "open";
                    if ($object) {
                        if ($object.hasClass(className)) {
                            $object.removeClass(className)
                        } else {
                            $object.addClass(className)
                        }
                    }
                });
            });

            /*Documents which pending to be used list*/
            $("#pending_list li").click(function() {
                var $pivot_id = this.id;
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });

            /*Documents which already been used list*/
            $("#used_list li").click(function() {
                var $pivot_id = this.id;
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });





            var $folder = $('#folder-tree');

            /************************** start: Document Library JS ***************************/
            $folder.jstree({
                'core': {
                    'data': {
                        'url': "{!! route('backend.payroll.beneficiary.document_validated', ['member_type' => $member_type_id, 'resource' => $resource->id, 'document_type' => $document_type ]) !!}",
                        'data': function (node) {
                            return {'id': node.id};
                        }
                    },
                    'check_callback': function (o, n, p, i, m) {
                        if (m && m.dnd && m.pos !== 'i') {
                            return false;
                        }
                        if (o === "move_node" || o === "copy_node") {
                            if (this.get_node(n).parent === this.get_node(p).id) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'force_text': true,
                    'themes': {
                        'responsive': true,
                        'variant': 'small',
                        'stripes': true
                    }
                },
                'sort': function (a, b) {
                    return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
                },
                'types': {
                    'default': {'icon': 'folder'},
                    'file': {'valid_children': [], 'icon': 'file'}
                },
                'unique': {
                    'duplicate': function (name, counter) {
                        return name + ' ' + counter;
                    }
                },
                'plugins': ['state', 'dnd', 'sort', 'types', 'unique']
                /*'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']*/
            });
            $folder.bind("click.jstree", function ($event, $data) {
                var $node = $($event.target).closest("li").attr("id");
                var $type = $folder.jstree().get_selected(true)[0].type;
                if ($type === 'default') {
                    /*should only work on folders*/
                    /*Document Folder Has Been Clicked*/
                    $node = $node.split('/');
                    $node = $node[$node.length - 1];
                    if ($node.length > 0) {
                        //alert(1);
                    } else {
                        //alert(2);
                    }
                } else {
                    /*preview the document here*/
                    /*console.log($node);*/
                    let $document_frame = $("#document_frame");
                    get_current_document($node).done(function ($data) {
                        $document_frame.find("iframe").remove();
                        let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                        $document_frame.append($iframe);
                    });
                }
            });
            /************************** end: Document Library JS ***************************/
        });
        // });
        function get_current_document($doc_pivo_id) {

            /*return $.post( base_url + "/claim/notification_report/current_base64_document/{!! $resource->id !!}/" + $node, {}, function( data ) {
        }, "json");*/
            return $.ajax({
                url: base_url + "/payroll/beneficiary/current_base64_document/{!! $member_type_id !!}/{!! $resource->id !!}/" + $doc_pivo_id,
                dataType : 'json',
                async : false,
                method : "POST"
            });
        }


    </script>;

@endpush