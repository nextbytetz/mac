<legend class="grey_modal" >@lang('labels.backend.claim.document_helper')</legend>
<br/>
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                {{--Document library tree--}}
                <legend>Documents Library</legend>
                <br/>
                <div id="folder-tree" class="nopadding"></div>
                <br>
                <div class="pull-left">
                    {{--Button - Reload Documents from e-office--}}
                    {{--{!! link_to_route('backend.claim.notification_report.reload_documents','Reload from e-Office', [$notification_report->id],[ 'class' => 'btn btn-success site-btn ', ]) !!}--}}
                </div>
            </div>
        </div>


    </div>
    <div class="col-md-8">
        {{--Document Preview--}}
        <legend>Document Preview</legend>
        <br/>
        <div id="document_frame">
            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
        </div>


    </div>
</div>



@push('after-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}

    <script>
        var $refNode = 0;
        // $("#document_centre_tab").one("click", function() {
        $(function() {

            $(function () {
                $('[data-toggle]').on('click', function () {
                    var id = $(this).data("toggle"),
                        $object = $(id),
                        className = "open";
                    if ($object) {
                        if ($object.hasClass(className)) {
                            $object.removeClass(className)
                        } else {
                            $object.addClass(className)
                        }
                    }
                });
            });



            var $folder = $('#folder-tree');

            /************************** start: Document Library JS ***************************/
            $folder.jstree({
                'core': {
                    'data': {
                        'url': "{!! route('backend.payroll.beneficiary.document_center', ['member_type' => $member_type_id, 'resource' => $resource_id ]) !!}",
                        'data': function (node) {
                            return {'id': node.id};
                        }
                    },
                    'check_callback': function (o, n, p, i, m) {
                        if (m && m.dnd && m.pos !== 'i') {
                            return false;
                        }
                        if (o === "move_node" || o === "copy_node") {
                            if (this.get_node(n).parent === this.get_node(p).id) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'force_text': true,
                    'themes': {
                        'responsive': true,
                        'variant': 'small',
                        'stripes': true
                    }
                },
                'sort': function (a, b) {
                    return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
                },
                'types': {
                    'default': {'icon': 'folder'},
                    'file': {'valid_children': [], 'icon': 'file'}
                },
                'unique': {
                    'duplicate': function (name, counter) {
                        return name + ' ' + counter;
                    }
                },
                'plugins': ['state', 'dnd', 'sort', 'types', 'unique']
                /*'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']*/
            });
            $folder.bind("click.jstree", function ($event, $data) {
                var $node = $($event.target).closest("li").attr("id");
                var $type = $folder.jstree().get_selected(true)[0].type;
                if ($type === 'default') {
                    /*should only work on folders*/
                    /*Document Folder Has Been Clicked*/
                    $node = $node.split('/');
                    $node = $node[$node.length - 1];
                    if ($node.length > 0) {
                        //alert(1);
                    } else {
                        //alert(2);
                    }
                } else {
                    /*preview the document here*/
                    /*console.log($node);*/
                    let $document_frame = $("#document_frame");
                    get_current_document($node).done(function ($data) {
                        $document_frame.find("iframe").remove();
                        let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                        $document_frame.append($iframe);
                    });
                }
            });
            /************************** end: Document Library JS ***************************/
        });
        // });
        function get_current_document($doc_pivo_id) {

            /*return $.post( base_url + "/claim/notification_report/current_base64_document/{!! $resource_id !!}/" + $node, {}, function( data ) {
        }, "json");*/
            return $.ajax({
                url: base_url + "/payroll/beneficiary/current_base64_document/{!! $member_type_id !!}/{!! $resource_id !!}/" + $doc_pivo_id,
                dataType : 'json',
                async : false,
                method : "POST"
            });
        }


    </script>;

@endpush