{{--Document sidebar validation preview--}}

{{--sidebar document summary table--}}
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    <style>
        .list {
            display: none;
        }
        .list.open {
            display: block;
        }


    </style>
@endpush
{{--Pending documents--}}

{{--<div>&nbsp;</div>--}}
<div class = "row">
    <div class="col-md-12">



        {{--Document already validated - used--}}
        <div class="col-md-6">

            {{--<div class="row">--}}

            <div class="grey_modal">
                <table style="width:100%">
                    <tr>
                        <td  align="center"><h5><b><span class="light_dark_color">Document Evidence</span></b></h5></td>
                    </tr>
                </table>
            </div>
            {{--</div>--}}

            {{--<div class="row">--}}
            <div class="light_grey_bg">&nbsp;</div>
            <div class="light_grey_bg">
                {{--<div id="folder-tree" class="nopadding"></div>--}}

                @if($document_evidences)
                    <ul class="list-unstyled doc_list" id="doc_list">
                        @foreach($document_evidences as $doc_pivot)
                            <li class="document" id={!!  $doc_pivot->id !!}>
                                &nbsp;&nbsp;  <i class="fa fa-file-pdf-o"></i>&nbsp;<span class="text-success"><a href="#doc" >{!! $doc_pivot->description  . ' (Folio - '  . $doc_pivot->folio . ') : ' . $document_repo->find($doc_pivot->document_id)->name
                                !!}</a></span>
                            </li>
                        @endforeach
                    </ul>

                @endif

            </div>

            {{--</div>--}}
        </div>

        <div class="col-md-6">

        </div>


    </div>

</div>

{{--Preview--}}
<br/>
<div class = "row">
    <div class="col-md-12">
        {{--Document Preview--}}
        <legend>Document Preview</legend>
        <br/>
        <div id="document_frame" style="text-align: center;">
            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
        </div>


    </div>

</div>

@push('after-script-end')

    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}--}}

    <script>
        var $refNode = 0;
        // $("#document_centre_tab").one("click", function() {
        $(function() {



            $(".document").click(function() {
                var $pivot_id = this.id;
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');

                    $document_frame.append($iframe);

                });

            });


        });
        // });
        function get_current_document($doc_pivot_id) {


            return $.ajax({
                url: base_url + "/payroll/beneficiary/current_base64_document/{!! $member_type_id !!}/{!! $resource->id !!}/" + $doc_pivot_id,
                dataType : 'json',
                async : false,
                method : "POST"
            });
        }



    </script>;

@endpush