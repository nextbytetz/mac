
<div class="modal fade center-modal" id="open_document_modal" tabindex="-1" role="dialog" aria-labelledby="largemodal" style="display: none;width:100%;margin-left:0px;" aria-hidden="true">

    <div class="modal-dialog modal-lg" style=" background-color: white" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Document</h4>
            </div>
            <div class="modal-body">

                {{--<div >--}}
                    {{--<iframe src="http://docs.google.com/gview?url=http://www.pdf995.com/samples/pdf.pdf&embedded=true"--}}
                            {{--style="width:700px; height:600px;" frameborder="0"></iframe>--}}
                {{--</div>--}}

                <div id="document_frame" style="text-align: center;">
                    <iframe id="document_preview" name="document_preview" src="" width='100%' style="width:700px; height:600px;"></iframe>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>



