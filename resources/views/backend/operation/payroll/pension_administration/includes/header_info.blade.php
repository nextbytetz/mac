<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">


    <span class="navbar-brand mb-0 h5"> {!! ($member_type_id == 5) ? 'Pensioner'  : 'Dependent'!!}&nbsp;:&nbsp;<small class="underline"> <a href="{!! ($member_type_id == 5) ? route('backend.payroll.pensioner.profile', $resource->id)  : route($resource->getProfileRoute($employee_id)['route'], $resource->getProfileRoute($employee_id)['parameter']) !!}">{!!  $resource->name !!}</a></small></span>

@include('backend/operation/payroll/pension_administration/includes/alert_monitor_link')
</nav>
<legend></legend>
<br/>