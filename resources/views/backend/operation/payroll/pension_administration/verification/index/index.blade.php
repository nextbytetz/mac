@extends('layouts.backend.main', ['title' => $title, 'header_title' => $title ])

@include('backend.includes.datatable_assets')

@section('content')


    @include('backend/operation/payroll/pension_administration/verification/index/includes/action_buttons')

    <br/>
    @if($member_type_id == 5)
        @include('backend/operation/payroll/pension_administration/verification/index/includes/get_pensioners')
    @elseif($member_type_id == 4)
        @include('backend/operation/payroll/pension_administration/verification/index/includes/get_dependents')
    @endif
@stop