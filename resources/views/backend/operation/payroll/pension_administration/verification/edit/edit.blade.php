
@extends('layouts.backend.main', ['title' => 'Payroll Verification', 'header_title' => 'Edit Payroll Verification'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>


    </style>
@endpush



@section('content')

    {!! Form::model($payroll_verification,['route' => ['backend.payroll.verification.update',$payroll_verification->id ],'method'=>'put',  'id' => 'edit', 'name' => 'edit']) !!}
    {{--{!! Form::open(['route' => ['backend.payroll.verification.store' ],'method'=>'post', 'name' => 'store', 'id' => 'store']) !!}--}}
    <div class = "row">
        @include("backend/operation/payroll/pension_administration/verification/create/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'suspense_flag' => $resource->suspense_flag])

    </div>

    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('resource_id', $resource->id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}
    {!! Form::hidden('check_child', $check_child) !!}
    {!! Form::hidden('today_date', getTodayDate(), ['class' =>'']) !!}

    <div class = "row">
        <div class="col-md-12">

            <div class="col-md-6">
                @include('backend/operation/payroll/pension_administration/verification/includes/notice_summary', ['employee_id' => $employee_id, 'member_type_id' => $member_type_id, 'resource_id' => $resource->id])
                {{--last verified--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Last Verified:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::input( 'text','last_response', short_date_format($resource->lastresponse) , ['class' => 'form-control', 'disabled', ]) !!}
                                    {!! $errors->first('last_response', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{--Month since last verifiction--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Months:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::input( 'text','months', $month_since_verification, ['class' => 'form-control compute_amount_per_cycle money','id' => 'months', 'disabled']) !!}
                                    {!! $errors->first('months', '<span class="help-block label label-danger">:message</span>') !!}
                                    <small class="form-text text-muted" style="width:100% !important;">Number of months since last verification</small>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                @if($check_child == 1)
                {{--Education--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Attending School:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::select('iseducation',  ['0' => 'No', '1' => 'Yes'], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> '' ]) !!}
                                    {!! $errors->first('iseducation', '<span class="help-block label label-danger">:message</span>') !!}
                                    <small class="form-text text-muted" style="width:100% !important;">Does survivor still attend school?</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    {{--Disabled--}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Disabled:</label></div>
                                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        {!! Form::select('isdisabled',  ['0' => 'No', '1' => 'Yes'], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'isdisabled' ]) !!}
                                        {!! $errors->first('isdisabled', '<span class="help-block label label-danger">:message</span>') !!}
                                        <small class="form-text text-muted" style="width:100% !important;">Does this child has disability?</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

{{--verification date--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Verification date:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-inline">

                                        <div class="input-group" style="width:100%;">
                                            {!! Form::text('verification_date',  short_date_format( $payroll_verification->verification_date), ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                        </div>
                                        <small class="help-block">
                                            Date when beneficiary was verified.
                                        </small>

                                        {!! $errors->first('verification_date', '<span class="help-block label label-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{--Folionumber--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Folio No.:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::input( 'text','folionumber', null , ['class' => 'form-control number', '', ]) !!}
                                    {!! $errors->first('folionumber', '<span class="help-block label label-danger">:message</span>') !!}
                                    <small class="form-text text-muted" style="width:100% !important;">Folio number of the document used to verify beneficiary information</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Phone:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::input( 'text','phone', $resource->phone , ['class' => 'form-control ', '', ]) !!}
                                    {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                                    <small class="form-text text-muted" style="width:100% !important;">Phone no of payroll beneficiary</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Next Kin Phone:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::input( 'text','next_kin_phone', null , ['class' => 'form-control ', '', ]) !!}
                                    {!! $errors->first('next_kin_phone', '<span class="help-block label label-danger">:message</span>') !!}
                                    <small class="form-text text-muted" style="width:100% !important;">Phone no of next of kin of beneficiary</small>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Region:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::select('region_id', $regions, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'region_id' ]) !!}
                                    {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>District:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    {!! Form::select('district_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'district_id' ]) !!}
                                    {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Remark:</label></div>
                            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group text_content">
                                    {{ Form::textarea('remark',$payroll_verification->payrollStatusChange->remark,['class'=>'form-control', '', 'id' => 'remark','placeholder' => '', 'autocomplete' => 'off']) }}
                                    {!! $errors->first('remark', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{--buttons--}}
                <div class="row" align="center">
                    <div class="col-md-9">
                        <div class="element-form" >
                            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    {!! link_to('payroll/status_change/profile/' . $payroll_verification->payroll_status_change_id ,trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                    {!! Form::button('Verify',['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                @include('backend/operation/payroll/pension_administration/includes/document_sidebar_validation_preview', ['member_type_id' => $member_type_id, 'resource' => $resource,
                'document_type' => $document_type, ])
            </div>
        </div>
    </div>










    {!! Form::close() !!}
@stop


@push('after-script-end')

    @include('backend/includes/assets/auto_fill_sub_category_select', ['child_value' => isset($payroll_verification->district_id ) ? $payroll_verification->district_id : old('district_id')  , 'parent_id' => 'region_id', 'child_id' => 'district_id',
  'child_hideable'   => 0, 'isedit' => 0, 'get_url' => 'getDistricts?region_id='])

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();



            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/



        {{--/*Hide last cycle amount div*/--}}
            {{--$("#phone_reset" ).hide();--}}

            {{--/*Change phone*/--}}
            {{--$("#phone_link").click(function(event) {--}}
                {{--$("#phone" ).prop("disabled", false);--}}
                {{--$("#phone_reset" ).show();--}}
                {{--$("#phone_link" ).hide();--}}
            {{--});--}}

            {{--/*phone reset*/--}}
            {{--$("#phone_reset").click(function(event) {--}}
                {{--var $phone = '{!! $phone !!}';--}}
                {{--$("#phone" ).val($phone).change();--}}
                {{--$("#phone" ).prop("disabled", true);--}}
                {{--$("#phone_reset" ).hide();--}}
                {{--$("#phone_link" ).show();--}}
            {{--});--}}



            /* start : ensure only numbers are input on monetary boxes */
            function number_only(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });
    </script>;

@endpush
