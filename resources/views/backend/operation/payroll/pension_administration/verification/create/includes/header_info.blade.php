

<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    @if($suspense_flag == 1)
        <span class='navbar-brand mb-0 h5 tag-warning' data-toggle='tooltip' data-html='true' title= 'Suspended'>{!! 'Suspended' !!}</span>
    @else
        <span class='navbar-brand mb-0 h5 tag tag-success' data-toggle='tooltip' data-html='true' title= 'Active'>{!! 'Active' !!}</span>
    @endif

     <span class="navbar-brand mb-0 h5"> {!! ($member_type_id == 5) ? 'Pensioner'  : 'Dependent'!!}&nbsp;:&nbsp;<small class="underline"> <a href="{!! ($member_type_id == 5) ? route('backend.payroll.pensioner.profile', $resource->id)  : route('backend.compliance.dependent.profile', $resource->getDependentEmployee($employee_id)->id) !!}">{!! $resource->name !!}</a></small></span>

        {{--alert monitor--}}
        @include('backend/operation/payroll/pension_administration/includes/alert_monitor_link')
</nav>
<legend></legend>
<br/>