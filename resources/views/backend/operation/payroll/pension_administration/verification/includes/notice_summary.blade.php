

@if($member_type_id == 4)
    @if($resource->getCheckIfAgeIsEligible($employee_id) == false)
        <nav class="navbar navbar-light bg-light" style="background-color: #ffe6e9;">
            @if($resource->getDependentEmployee($employee_id)->dependent_type_id == 3)
                <span > <b>NOTE:</b>  {!!  'This child is already overage, Once verification is approved may not be reinstated unless he/she is still attending school.'  !!}&nbsp</span>
            @endif
        </nav>
        <br/>
    @endif
@endif
