

{{--General info--}}
{{--<br/>--}}
<div class="row">
    {{--left general  summary--}}
    <div class="col-md-12">
        <div class="col-md-12">
            <legend id=""  style="background-color: lightgrey;text-align:center">Monthly Pension Details</legend>
            <table class="table table-striped table-bordered" id="old_bank_table">
                <tbody>

                {{--header--}}
                <tr>
                    <td width="120px">Current Monthly Pension</td>
                    <td width="120px">New Monthly Pension</td>


                </tr>

                {{--details--}}
                <tr>
                    <th>{!!
                        number_2_format($payroll_mp_update->old_mp)
                                !!} </th>
                    <th>
                        {!!         number_2_format($payroll_mp_update->new_mp)
                                    !!}</th>

                </tr>





                </tbody>
            </table>

        </div>
    </div>
</div>


@push('after-script-end')

    <script  type="text/javascript">


        $(function () {


        });
    </script>;

@endpush
