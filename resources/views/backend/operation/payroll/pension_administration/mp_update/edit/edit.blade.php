@extends('layouts.backend.main', ['title' => 'Update Monthly Pension', 'header_title' => 'Update Monthly Pension'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>


    </style>
@endpush

@section('content')

    {!! Form::model($payroll_mp_update,['route' => ['backend.payroll.mp_update.update', $payroll_mp_update->id],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}
    {{--{!! Form::open(['route' => ['backend.payroll.recovery.store' ],'method'=>'post', 'name' => 'store', 'id' => 'store']) !!}--}}
    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $payroll_mp_update->member_type_id,'employee_id' => $payroll_mp_update->employee_id])

    </div>


    <div class = "row">
        <div class="col-md-12">

            <div class="col-md-6">

                {{--old_mp--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right required"><label>Current Monthly Pension:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','old_mp_v', number_2_format($payroll_mp_update->old_mp) , ['class' => 'form-control money', 'disabled', ]) !!}
                                    {!! $errors->first('old_mp', '<span class="help-block label label-danger">:message</span>') !!}
                                    {!! Form::hidden('old_mp', $payroll_mp_update->old_mp) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--new mp--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right required"><label>New Monthly Pension:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','new_mp', number_2_format($payroll_mp_update->new_mp) , ['class' => 'form-control money', '', ]) !!}
                                    {!! $errors->first('new_mp', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{--Folio--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                {{--<div class="element-form" >--}}
                {{--@if($enrolled)--}}
                {{--<div class="col-md-3 text-xs-right required"><label>Doc Folio:</label></div>--}}
                {{--@else--}}
                {{--<div class="col-md-3 text-xs-right "><label>Doc Folio:</label></div>--}}
                {{--@endif--}}

                {{--<div class="col-md-8">--}}
                {{--<div class="form-group">--}}
                {{--{!! Form::input( 'text','folionumber', null, ['class' => 'form-control  ','id' => 'email', ]) !!}--}}
                {{--{!! $errors->first('folionumber', '<span class="help-block label label-danger">:message</span>') !!}--}}
                {{--<small class="form-text text-muted" style="width:100% !important;">Folio number of the document used to verify beneficiary information</small>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}


                {{--Remark--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right required"><label>Remark:</label></div>
                            <div class="col-md-8">
                                <div class="form-group text_content">

                                    {!! Form::textarea('remark', $payroll_mp_update->remark, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                                    {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                {{--buttons--}}
                <div class="row" align="center">
                    <div class="col-md-12">
                        <div class="element-form" >
                            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    {!! link_to('payroll/beneficiary_profile/' . $payroll_mp_update->member_type_id . '/' . $resource->id . '/' . $payroll_mp_update->employee_id,trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                {{--@include('backend/operation/payroll/pension_administration/includes/document_sidebar_validation_preview', ['member_type_id' => $member_type_id, 'resource' => $resource,--}}
                {{--'document_type' =>--}}
                {{--$document_type])--}}
            </div>
        </div>
    </div>





    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();

            /* start : mask all money input */
            $('.money').maskMoney({
                precision : 2,
                allowZero : false,
                affixesStay : false
            });

        });
    </script>;

@endpush
