@extends('layouts.backend.main', ['title' => 'Edit Payroll Retiree Followups' , 'header_title' => 'Edit Payroll Retiree Followups'])

@include('backend/includes/assets/datetimepicker')
@include('backend/includes/assets/select2_assets')
@include('backend/includes/assets/sweetalert_assets')
@push('after-styles-end')
    <style>
    </style>
@endpush

@section('content')
    {{ Form::model($payroll_retiree_followup,['route' => ['backend.payroll.retiree.followup.update', $payroll_retiree_followup],'method'=>'put',
 'id' => 'update', 'name' => 'update']) }}
    {{ Form::hidden('payroll_retiree_followup_id', $payroll_retiree_followup->id, []) }}
    {{ Form::hidden('member_type_id', $member_type_id, []) }}
    {{ Form::hidden('resource_id', $resource->id, []) }}
    {{ Form::hidden('employee_id', $employee_id, []) }}
    {{ Form::hidden('action_type', 2, []) }}
    {{ Form::hidden('today', getTodayDate(), []) }}

    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id,'employee_id' => $employee_id])
    </div>


    <div class="pull-right" >
        {{--<br/>--}}
        @if($payroll_retiree_followup->payrollRetireeMpUpdate()->count() == 0)
            <span>
            {!! HTML::decode(link_to_route('backend.payroll.retiree.followup.undo', "<i class='icon fa fa-delete' aria-hidden='true'></i>&nbsp;" . 'Delete', [$payroll_retiree_followup->id], ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to delete this Followup?', 'class' => 'btn btn-primary site-btn delete_button'])) !!}
            </span>
        @endif
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group ">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('follow_up_type_cv_ref', 'Follow Up Type', ['class' =>'required']) }}
                                {{ Form::select('follow_up_type_cv_ref',$code_value_repo->getCodeReferencesForSelectFilteredNotInReferences(9,['FULETT']),$code_value_repo->reference($payroll_retiree_followup->follow_up_type_cv_id),['class'=>'form-control select2', 'required', 'id' => 'follow_type_id','placeholder' => '', 'autocomplete' => 'off']) }}

                                {!! $errors->first('follow_up_type_cv_ref', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group " id="contact_div">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('contact', 'Contact', ['class' =>'required']) }}
                                {{ Form::text('contact',null,['class'=>'form-control', 'id' => 'contact_id','placeholder' => '', 'autocomplete' => 'off']) }}
                                {!! $errors->first('contact', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('contact_person', 'Contact Person', ['class' =>'required']) }}
                                {{ Form::text('contact_person',null,['class'=>'form-control', 'id' => 'contact_person','placeholder' => '', 'autocomplete' => 'off']) }}

                                {!! $errors->first('contact_person', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('date_of_follow_up', 'Date Of Follow Up', ['class' =>'required']) }}
                                <div class="input-group">
                                    {{ Form::text('date_of_follow_up',short_date_format($payroll_retiree_followup->date_of_follow_up),['class'=>'form-control datepicker_before_today', 'id' => 'date_of_follow_up','placeholder' => '', 'autocomplete' => 'off']) }}
                                    <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                                </div>
                                {!! $errors->first('date_of_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('retiree_feedback_cv_ref', 'Retiree Feedback', ['class' =>'required']) }}
                                {{ Form::select('retiree_feedback_cv_ref_esc',$code_value_repo->getCodeValuesReferenceByCodeForSelect(70),$code_value_repo->reference($payroll_retiree_followup->retiree_feedback_cv_id),['class'=>'form-control select2', 'required', 'id' => 'retiree_feedback_cv_ref','placeholder' => '', 'autocomplete' => 'off', 'disabled']) }}
                                {{ Form::hidden('retiree_feedback_cv_ref', $code_value_repo->reference($payroll_retiree_followup->retiree_feedback_cv_id), []) }}
                                {!! $errors->first('retiree_feedback_cv_ref', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group " id="date_of_next_follow_up_div">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('date_of_next_follow_up', 'Date Of Next Follow Up', ['class' =>'required']) }}
                                <div class="input-group">
                                    {{ Form::text('date_of_next_follow_up',($payroll_retiree_followup->date_of_next_follow_up)  ? short_date_format($payroll_retiree_followup->date_of_next_follow_up)  : short_date_format($date_of_next_followup),['class'=>'form-control datepicker_after_today', 'required', 'id' => 'date_of_next_follow_up','placeholder' => '', 'autocomplete' => 'off']) }}
                                    <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                                </div>

                                {!! $errors->first('date_of_next_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-3">
            <div class="element-form">
                <div class="form-group pull-right">
                    {!! link_to($return_url. '#retiree_followup',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ])!!}
                    {{ Form::button(trans('buttons.general.submit'), ['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit', 'style' => 'border-radius: 5px;']) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@push('after-script-end')
    <script>
        $(function() {
            $(".select2").select2();

            retiree_feedback_option();
            $("#retiree_feedback_cv_ref").on("change", function (e) {
                retiree_feedback_option();
            });


            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });

            // Follow up type option
            function follow_up_type_option(follow_type_id, contact_div, contact_id) {

                var choice = $("#" + follow_type_id).val();
//            if is visit
                if (choice == 'FUVSIT' || choice == 'FULETT') {
                    $("#" + contact_div).hide();
                    $('#doc_form_div').show();
                } else {
//            phone call
                    if (choice == 'FUPHONC') {
                        $("#" + contact_id).attr('placeholder','Phone #');
                    }else if(choice == 'FUEMAIL'){
                        $("#" + contact_id).attr('placeholder','Email@yahoo.com');
                    }
                    $("#" + contact_div).show();
                    $('#doc_form_div').hide();
                }
            }


            function retiree_feedback_option()
            {
                var choice = element_id_value('retiree_feedback_cv_ref');
                if(choice == 'PRFFSTLL'){
                    $("#date_of_next_follow_up_div" ).show();
                    enable_disable('enable_id', 'date_of_next_follow_up')
                }else{
                    $("#date_of_next_follow_up_div" ).hide();
                    enable_disable('disable_id', 'date_of_next_follow_up')
                }
            }


        });
    </script>
@endpush
