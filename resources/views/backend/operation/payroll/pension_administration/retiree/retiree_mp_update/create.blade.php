
@extends('layouts.backend.main', ['title' =>'Create Retiree MP update', 'header_title' => 'Create Retiree MP update'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    @include('backend.includes.assets.datetimepicker')
    @include('backend/includes/assets/maskmoney_assets')
    <style>


    </style>
@endpush

@if(env('TESTING_MODE') == 1)
    @php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    @endphp
@endif

@section('content')

    {!! Form::open(['route' => ['backend.payroll.retiree.mp_update.store' ],'method'=>'post', 'name' => 'create']) !!}

    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'employee_id' => $employee_id])
    </div>
    {!! Form::hidden('this_date', getTodayDate(), ['class' => 'this_date']) !!}
    {{ Form::hidden('payroll_retiree_followup_id', $payroll_retiree_followup->id, []) }}
    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('resource_id', $resource->id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}
    {{ Form::hidden('current_mp', $current_mp, []) }}


    <div class="row">

        <div class="col-md-12">
            <div class="col-md-6">


                <div class="form-group ">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
{!! 'Reason: <b>' . $payroll_retiree_followup->retireeFeedback->name  . '</b>' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    {{ Form::label('current_mp', 'Current Mp', ['class' =>'']) }}
                                    {{ Form::text('current_mp',number_2_format($current_mp),['class'=>'form-control money', 'disabled', 'id' => 'current_mp','placeholder' => '', 'autocomplete' => 'off']) }}
                                    {!! $errors->first('current_mp', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    {{ Form::label('social_security_mp', 'Social Security Mp', ['class' =>'required']) }}
                                    {{ Form::text('social_security_mp',null,['class'=>'form-control money20', 'required', 'id' => 'social_security_mp','placeholder' => '', 'autocomplete' => 'off']) }}
                                    {!! $errors->first('social_security_mp', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    {{ Form::label('retirement_date', 'Retirement Date', ['class' =>'required']) }}
                                    <div class="input-group">
                                        {{ Form::text('retirement_date',null,['class'=>'form-control datepicker_before_today', 'required', 'id' => 'retirement_date','placeholder' => '', 'autocomplete' => 'off']) }}
                                        <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                                    </div>
                                    {!! $errors->first('retirement_date', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group text_content">
                                    {{ Form::label('remark', 'Remark', ['class' =>'required']) }}
                                    {{ Form::textarea('remark',null,['class'=>'form-control', 'required', 'id' => 'remark','placeholder' => '', 'autocomplete' => 'off']) }}
                                    {!! $errors->first('remark', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>



            {{--Document--}}
            <div class="col-md-6">

                @include('backend/operation/payroll/pension_administration/includes/document/doc_evidence/doc_evidence_selection')
            </div>


        </div>

    </div>

    {{--button--}}
    <div class="row" align="center">
        <div class="col-md-9">
            <div class="element-form" >
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                <div class="col-xs-7 col-lg-7 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        {!! link_to($return_url. '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>








    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();





        });
    </script>;

@endpush







