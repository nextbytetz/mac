@extends('layouts.backend.main', ['title' =>    'Retiree Monthly Pension update Approval', 'header_title' =>'Retiree Monthly Pension update Approval'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>

        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush



@section('content')


    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id,'employee_id' => $employee_id])
    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >


                                        {{--@if($check_if_can_initiate_letter)--}}
                                            {{--{!! HTML::decode(link_to_route('backend.payroll.status_change.update_letter_prepared_status', "<i class='icon fa fa-file' aria-hidden='true'></i>&nbsp;" . 'Letter Prepared', $status_change->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to update letter Status?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}--}}
                                        {{--@endif--}}

                                        @if($check_pending_level1 == 1)
                                            <span>
                                                                     {{--default edit--}}
                                                <a href="{!! route('backend.payroll.retiree.mp_update.edit',[ 'payroll_retiree_mp_update'=> $payroll_retiree_mp_update->id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;
                                                                Edit
                                            </a>
                                     </span>

                                            <span>
                                           {!! HTML::decode(link_to_route('backend.payroll.retiree.mp_update.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $payroll_retiree_mp_update->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this MP update?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                        @endif

                                            {!!                                          HTML::decode( link_to($return_url. '#general',trans('buttons.general.close'),['id'=> 'close', 'class' => 'tn btn-primary site-btn nav_button icon fa  fa-close', ])) !!}

                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--main tab content--}}
                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-6">

                                    {{--general info--}}
                                    @include('backend/operation/payroll/pension_administration/retiree/retiree_mp_update/profile/includes/general_info')
                                    {{--disease adjust overview--}}
                                    <div>&nbsp;</div>
                                    {{--APprovals tabs history--}}
                                    {{--@include('backend/operation/payroll/pension_administration/profile/pensioner/includes/general/approvals')--}}
                                    {{--{!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}--}}

                                    @include("backend/includes/workflow/wf_track_html", $workflow_input)
                                </div>
                                <div class="col-md-6">
                                    <br/>
                                    @include('backend/operation/payroll/pension_administration/includes/document/doc_evidence/document_review')

                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">

        $(function () {

        });
    </script>;

@endpush
