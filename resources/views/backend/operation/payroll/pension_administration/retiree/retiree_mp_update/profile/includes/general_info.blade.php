

{{--General info--}}
<br/>
<legend style="background-color: lightgrey">{{ 'General Information'  }}

</legend>


@if($payroll_retiree_mp_update->deduction_summary)

<nav class="navbar navbar-light bg-light" style="background-color: #ffe6e9;">


    <span > {!! '<b> NOTE: </b>'  . $payroll_retiree_mp_update->deduction_summary  !!}&nbsp;</span>


</nav>

@endif
<div class="row">
    {{--left general  summary--}}

    <div class="col-md-12">

        <table class="table table-striped table-bordered">
            <tbody>

            {{--Reason--}}
            <tr>
                <td>Reason</td>
                <th>
                    {!! $payroll_retiree_mp_update->payrollRetireeFollowup->retireeFeedback->name !!}

                </th>
            </tr>

            @if(isset($resource->start_pay_date))
                <tr>
                    <td width="140px">{!! 'Date of MMI' !!}</td>
                    <th>{!! Form::label( 'start_date',
                          isset($resource->start_pay_date) ?  short_date_format($resource->start_pay_date) : null, [])
                                !!} </th>
                </tr>
            @endif
            <tr>
                <td>Age</td>
                <th>
                    {!! $resource->age . ' yrs old' !!}

                </th>
            </tr>

            <tr>
                <td>Retirement Date</td>
                <th>
                    {!! short_date_format($payroll_retiree_mp_update->retirement_date) !!}

                </th>
            </tr>

            <tr>
                <td>Current Monthly Pension (WCF)</td>
                <th>
                    {!! number_2_format($payroll_retiree_mp_update->current_mp) !!}

                </th>
            </tr>

            <tr>
                <td>Social Security Monthly Pension</td>
                <th>
                    {!! number_2_format($payroll_retiree_mp_update->social_security_mp) !!}
                </th>
            </tr>


            <tr>
                <td>New Monthly Pension</td>
                <th>
                    {!! number_2_format($payroll_retiree_mp_update->new_mp) !!}
                </th>
            </tr>

            @if($payroll_retiree_mp_update->deduction_amount >= 0)
            <tr>
                <td>Deduction Amount</td>
                <th>
                    {!! number_2_format($payroll_retiree_mp_update->deduction_amount) !!}
                </th>
            </tr>
            @else
            <tr>
                <td>Arrears Amount</td>
                <th>
                    {!! number_2_format(abs($payroll_retiree_mp_update->deduction_arrear_abs)) !!}
                </th>
            </tr>
            @endif
            <tr>
                <td>Remark</td>
                <th>
                    {!! '' !!}
                </th>
            </tr>
            </tbody>
        </table>




    </div>
</div>




