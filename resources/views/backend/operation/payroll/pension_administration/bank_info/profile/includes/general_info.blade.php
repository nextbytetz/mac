

{{--General info--}}
{{--<br/>--}}
<div class="row">
    {{--left general  summary--}}
    <div class="col-md-12">
        <div class="col-md-12">
            <legend id="old_bank"  style="background-color: lightgrey;text-align:center">Old Bank Details- (Click here to view)</legend>
            <table class="table table-striped table-bordered" id="old_bank_table">
                <tbody>

                {{--header--}}
                <tr>
                    <td width="120px">Bank</td>
                    <td width="120px">Branch</td>
                    <td width="120px">Account No.</td>

                </tr>

                {{--details--}}
                <tr>
                    <th>{!! Form::label( 'bank',
                           isset($bank_update->old_bank_branch_id) ? $bank_update->oldBankBranch->bank->name : (isset($bank_update->old_bank_id) ?  $bank_update->oldBank->name : ' '), [])
                                !!} </th>
                    <th>
                        {!!   isset($bank_update->old_bank_branch_id) ? $bank_update->oldBankBranch->name : ' '
                                    !!}</th>
                    <th>
                        {!! $bank_update->old_accountno
                                    !!}</th>
                </tr>





                </tbody>
            </table>

        </div>
    </div>
</div>

<br/>
    {{--new banks summary--}}
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">New Bank Details Approval- {!! $bank_update->status_label !!} </legend>
            <table class="table table-striped table-bordered">
                <tbody>

                {{--Bank--}}
                <tr>
                    <td width="120px">Bank</td>
                    <th>{!! Form::label( 'bank',
                          isset($bank_update->new_bank_branch_id) ? $bank_update->newBankBranch->bank->name : (isset($bank_update->new_bank_id) ?  $bank_update->newBank->name : ' '), [])
                                !!} </th>
                </tr>

                {{--Branch--}}
                <tr>
                    <td>Branch</td>
                    <th>
                        {!!   isset($bank_update->new_bank_branch_id) ? $bank_update->newBankBranch->name : ' '
                                    !!}</th>
                </tr>

                {{--Accountno--}}
                <tr>
                    <td>Account No.</td>
                    <th>
                        {!! $bank_update->new_accountno
                                    !!}</th>
                </tr>


                {{--folio--}}
                <tr>
                    <td>Doc Folio</td>
                    <th>
                        {!! $bank_update->folionumber
                                    !!}</th>
                </tr>



                </tbody>
            </table>

        </div>
    </div>





</div>

@push('after-script-end')

    <script  type="text/javascript">


        $(function () {

            $("#old_bank_table").hide();
            $("#old_bank").click(function(){
                $("#old_bank_table").slideToggle("slow");
            });

        });
    </script>;

@endpush
