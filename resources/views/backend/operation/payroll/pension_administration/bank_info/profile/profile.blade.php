@extends('layouts.backend.main', ['title' => 'Bank Update Profile', 'header_title' => 'Bank Update Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>


        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'employee_id' => $employee_id])
    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >

                                        @if($check_pending_level1 == 1)
                                            <span>
                                        <a href="{!! route('backend.payroll.edit_new_bank',['bank_update'  => $bank_update->id , 'employee' => $employee_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Edit
                                            New Bank</a>
                                     </span>


                                            <span>
                                           {!! HTML::decode(link_to_route('backend.payroll.bank_update.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', ['bank_update' => $bank_update->id, 'employee' => $employee_id], ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this bank update?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                        @endif

                                        @if($member_type_id == 5)

                                            {!!                                          HTML::decode( link_to('payroll/pensioner/profile/' . $resource->id . '#bank_update',trans('buttons.general.close'),['id'=> 'close', 'class' => 'tn btn-primary site-btn nav_button icon fa
                                            fa-close', ])) !!}

                                            {{--</a>--}}
                                        @elseif($member_type_id == 4)
                                            {!!                                          HTML::decode( link_to('compliance/dependent/profile/' . $dependent_employee->id . '#bank_update',trans('buttons.general.close'),['id'=> 'close', 'class' => 'tn btn-primary site-btn nav_button icon fa
                                            fa-close', ])) !!}
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-6">
                                    {{--general info--}}
                                    @include('backend/operation/payroll/pension_administration/bank_info/profile/includes/general_info')
                                    {{--disease adjust overview--}}
                                    <div>&nbsp;</div>
                                    {{--APprovals tabs history--}}
                                    {{--@include('backend/operation/payroll/pension_administration/profile/pensioner/includes/general/approvals')--}}
                                    {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}
                                </div>
                                <div class="col-md-6">
                                    @include('backend/operation/payroll/pension_administration/includes/document_sidebar_validation_preview', ['member_type_id' => $member_type_id, 'resource' => $resource, 'document_type' =>
                                    $document_type])
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {


            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });
        });
    </script>;

@endpush
