
@extends('layouts.backend.main', ['title' => 'Edit New Bank Info', 'header_title' => 'Edit New Bank Info'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>


    </style>
@endpush

@section('content')

    {!! Form::model($bank_update,['route' => ['backend.payroll.update_new_bank', $bank_update->id],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}
    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id, 'employee_id' => $employee_id])
    </div>

    {!! Form::hidden('member_type_id', $member_type_id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}
    {!! Form::hidden('check_if_enrolled', true) !!}

    <div class = "row">
        <div class="col-md-12">

            <div class="col-md-6">

                {{--Bank--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right "><label>Current Bank:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','current_bank',isset($resource->bank_branch_id) ? $resource->bankBranch->bank->name : (isset($resource->bank_id) ? $resource->bank->name : null)  , ['class' => 'form-control current_bank_info', 'disabled', ]) !!}
                                    {!! Form::hidden('old_bank_id',$resource->bank_id) !!}
                                    {!! $errors->first('current_bank', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{--current branch--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right "><label>Current Branch:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','current_branch', isset($resource->bank_branch_id) ? $resource->bankBranch->name : ' ', ['class' => 'form-control current_bank_info','disabled', ]) !!}
                                    {!! Form::hidden('old_bank_branch_id', $resource->bank_branch_id) !!}
                                    {!! $errors->first('current_branch', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{--current accountno--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right "><label>Current Account No.:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','current_accountno', $resource->accountno, ['class' => 'form-control current_bank_info', 'disabled']) !!}
                                    {!! Form::hidden('old_accountno', $resource->accountno)!!}
                                    {!! $errors->first('current_accountno', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <legend>Fill new bank details</legend>
                <br/>

                {{--new Bank--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right required"><label>New Bank:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::select('new_bank', $banks, isset($bank_update->new_bank_id) ? $bank_update->newBank->id : [], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>
                                    'bank_select'])
                                     !!}
                                    <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
                                    {!! $errors->first('new_bank', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{--new branch--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right "><label>New Branch:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::select('new_branch', $bank_branches, $bank_update->new_bank_branch_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>
                                    'bank_branch_select' ]) !!}
                                    {!! $errors->first('new_branch', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{--new accountno--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right required"><label>New Account No.:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','new_accountno', $bank_update->new_accountno, ['class' => 'form-control money']) !!}
                                    {!! $errors->first('new_accountno', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{--folio--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right required"><label>Doc Folio:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','folionumber', $bank_update->folionumber, ['class' => 'form-control ']) !!}
                                    {!! $errors->first('folionumber', '<span class="help-block label label-danger">:message</span>') !!}
                                    <small class="form-text text-muted" style="width:100% !important;">Folio number of the document used to verify beneficiary information</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{--button--}}
                <div class="row" align="center">
                    <div class="col-md-12">
                        <div class="element-form" >
                            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                            <div class="col-xs-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="form-group">
                                    {!! link_to('payroll/bank_update/profile/' . $bank_update->id  ,trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ])  !!}
                                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>


            <div class="col-md-6">
                @include('backend/operation/payroll/pension_administration/includes/document_sidebar_validation_preview', ['member_type_id' => $bank_update->member_type_id, 'resource' => $resource, 'document_type' =>
                $document_type])
            </div>
        </div>
    </div>



    {!! Form::close() !!}
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}


    {{--Bank - Branch select auto fill--}}
    @include('backend/includes/assets/auto_fill_sub_category_select', ['child_value' => old('new_branch'), 'parent_id' => 'bank_select', 'child_id' => 'bank_branch_select',
          'child_hideable'   => 0,'isedit' => 1,  'get_url' => 'getbankbranch?bank_id='])

    <script  type="text/javascript">


        $(function () {

            $(".search-select").select2();



        });
    </script>;

@endpush
