

{{--sidebar document summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Document</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            {{--name--}}
            <tr>
                <td style="padding-left: 5px" width="100%"><i class="icon fa fa-folder-open-o"> - <a href="#" data-toggle="modal" data-target="#open_document_modal" id="open_document_link"   style="color: dodgerblue" class="pull-center underline">  Bank Update document </a></i></td>
                            </tr>

        </table>
    </div>
</div>


@include('backend/operation/payroll/pension_administration/includes/open_document_modal')



@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush