@extends('layouts.backend.main', ['title' => 'Dependents', 'header_title' => 'Dependents'])

@include('backend.includes.datatable_assets')

@section('content')

    @include('backend/operation/payroll/pension_administration/index/includes/action_buttons')
<br/>
    @include('backend/operation/payroll/pension_administration/index/includes/get_dependents')


@stop



