@extends('layouts.backend.main', ['title' => 'Pensioners', 'header_title' => 'Pensioners'])

@include('backend.includes.datatable_assets')

@section('content')


    @include('backend/operation/payroll/pension_administration/index/includes/action_buttons')
    <br/>
    @include('backend/operation/payroll/pension_administration/index/includes/get_pensioners')

@stop