@extends('layouts.backend.main', ['title' => 'Modify Beneficiary details', 'header_title' => 'Modify Beneficiary details'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>


    </style>
@endpush

@section('content')

    {!! Form::model($beneficiary_update,['route' => ['backend.payroll.beneficiary_update.update', $beneficiary_update->id],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}
    {{--{!! Form::open(['route' => ['backend.payroll.recovery.store' ],'method'=>'post', 'name' => 'store', 'id' => 'store']) !!}--}}
    <div class = "row">
        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $beneficiary_update->member_type_id, 'employee_id' => $employee_id])
    </div>
    {!! Form::hidden('member_type_id', $beneficiary_update->member_type_id) !!}
    {!! Form::hidden('employee_id', $employee_id) !!}
    {!! Form::hidden('resource_id', $beneficiary_update->resource_id) !!}
    {!! Form::hidden('check_if_enrolled', true) !!}
    <div class = "row">
        <div class="col-md-12">

            <div class="col-md-6">
                {{--First name--}}
                @if(array_key_exists('firstname', $unserialized_new_values))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right required"><label>First Name:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::input( 'text','firstname', $unserialized_new_values['firstname'] , ['class' => 'form-control', '', ]) !!}
                                        {!! $errors->first('firstname', '<span class="help-block label label-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                {{--middlename name--}}
                @if(array_key_exists('middlename', $unserialized_new_values))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right "><label>Middle Name:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::input( 'text','middlename', $unserialized_new_values['middlename'] , ['class' => 'form-control', '', ]) !!}
                                        {!! $errors->first('middlename', '<span class="help-block label label-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
                {{--lastname--}}
                @if(array_key_exists('lastname', $unserialized_new_values))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right required"><label>Last Name:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::input( 'text','lastname', $unserialized_new_values['lastname'] , ['class' => 'form-control', '', ]) !!}
                                        {!! $errors->first('lastname', '<span class="help-block label label-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                {{--dob--}}
                @if(array_key_exists('dob', $unserialized_new_values))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right required"><label>Date of Birth:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::text('dob', short_date_format( $unserialized_new_values['dob']), ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                                        {!! $errors->first('dob', '<span class="help-block label label-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                {{--Phone--}}
                @if(array_key_exists('phone', $unserialized_new_values))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right required"><label>Phone:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::input( 'text','phone', $unserialized_new_values['phone'], ['class' => 'form-control  ','id' => 'phone', ]) !!}
                                        {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endif

                {{--Email--}}
                @if(array_key_exists('email', $unserialized_new_values))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right"><label>Email:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::input( 'text','email', $unserialized_new_values['email'], ['class' => 'form-control  ','id' => 'email', ]) !!}
                                        {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endif


                {{--Folio--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3 text-xs-right required"><label>Doc Folio:</label></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::input( 'text','folionumber', $beneficiary_update->folionumber, ['class' => 'form-control  ','id' => 'email', ]) !!}
                                    {!! $errors->first('folionumber', '<span class="help-block label label-danger">:message</span>') !!}
                                    <small class="form-text text-muted" style="width:100% !important;">Folio number of the document used to verify beneficiary information</small>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                @if(array_key_exists('region_id', $unserialized_new_values) || array_key_exists('district_id', $unserialized_new_values))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right"><label>Region:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::select('region_id', $regions, $unserialized_new_values['region_id'], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'region_id' ]) !!}
                                        {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="element-form" >
                                <div class="col-md-3 text-xs-right"><label>District:</label></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::select('district_id', [], $unserialized_new_values['district_id'], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'district_id' ]) !!}
                                        {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                @endif


                {{--Remark--}}

                <div class="row">
                    <div class="col-md-12">
                        <div class="element-form" >
                            <div class="col-md-3  text-xs-right required"><label>Remark:</label></div>
                            <div class="col-md-8">
                                <div class="form-group text_content">

                                    {!! Form::textarea('remark', $beneficiary_update->remark, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height:
                                    5px !important'])
                                     !!}
                                    {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{--buttons--}}
                <div class="row" align="center">
                    <div class="col-md-12">
                        <div class="element-form" >
                            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Accountno:</label></div>--}}
                            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    {!! link_to('payroll/beneficiary_update/profile/' . $beneficiary_update->id ,trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                @include('backend/operation/payroll/pension_administration/includes/document_sidebar_validation_preview', ['member_type_id' => $member_type_id, 'resource' => $resource,
                'document_type' =>
                $document_type])
            </div>
        </div>
    </div>



    {!! Form::close() !!}
@stop


@push('after-script-end')
    @include('backend/includes/assets/auto_fill_sub_category_select', ['child_value' => isset($unserialized_new_values['district_id']) ? $unserialized_new_values['district_id'] : old('district_id')  , 'parent_id' => 'region_id', 'child_id' => 'district_id',
'child_hideable'   => 0, 'isedit' => 0, 'get_url' => 'getDistricts?region_id='])

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    <script  type="text/javascript">


        $(function () {
            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $(".search-select").select2();

            /*------------Start Date Process ---------*/

            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            /*-----------End Date Process------------*/
        });
    </script>;

@endpush
