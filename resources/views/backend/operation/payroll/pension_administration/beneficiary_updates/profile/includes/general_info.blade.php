

{{--General info--}}
<br/>
<div class="row">
    {{--left general  summary--}}
    <div class="col-md-12">
        <div class="col-md-12">
            <legend id="old_values"  style="background-color: lightgrey;text-align:center">Old Values - (Click here to view)</legend>
            <table class="table table-striped table-bordered" id="old_values_table">
                <tbody>

                {{--Firstname--}}
                @if(array_key_exists('firstname', $unserialized_old_values))
                    <tr>
                        <td width="120px">First Name:</td>
                        <th>{!! $unserialized_old_values['firstname'] !!} </th>
                    </tr>
                @endif

                {{--Middlename--}}
                @if(array_key_exists('middlename', $unserialized_old_values))
                    <tr>
                        <td>Middle Name:</td>
                        <th>
                            {!! $unserialized_old_values['middlename']
                                        !!}</th>
                    </tr>
                @endif
                {{--Lstname--}}
                @if(array_key_exists('lastname', $unserialized_old_values))
                    <tr>
                        <td>Last Name:</td>
                        <th>
                            {!! $unserialized_old_values['lastname']
                                        !!}</th>
                    </tr>
                @endif
                {{--Dob--}}
                @if(array_key_exists('dob', $unserialized_old_values))
                    <tr>
                        <td>Date of Birth:</td>
                        <th>
                            {!! short_date_format($unserialized_old_values['dob'])
                                        !!}</th>
                    </tr>
                @endif
                {{--Phone--}}
                @if(array_key_exists('phone', $unserialized_old_values))
                    <tr>
                        <td>Phone:</td>
                        <th>
                            {!! $unserialized_old_values['phone']
                                        !!}</th>
                    </tr>
                @endif
                {{--Email--}}
                @if(array_key_exists('email', $unserialized_old_values))
                    <tr>
                        <td>Email:</td>
                        <th>
                            {!! $unserialized_old_values['email']
                                        !!}</th>
                    </tr>
                @endif

                </tbody>
            </table>

        </div>
    </div>
</div>

<br/>

<div class="row">
    {{--Right summary--}}

    <div class="col-md-12">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Beneficiary Update Approval- {!! $beneficiary_update->status_label !!} </legend>
            <table class="table table-striped table-bordered">
                <tbody>

                {{--Firstname--}}
                @if(array_key_exists('firstname', $unserialized_new_values))
                    <tr>
                        <td width="120px">First Name:</td>
                        <th>{!! $unserialized_new_values['firstname'] .  $beneficiary_update->getChangedLabel($unserialized_old_values['firstname'],$unserialized_new_values['firstname']  ) !!} </th>
                    </tr>
                @endif

                {{--Middlename--}}
                @if(array_key_exists('middlename', $unserialized_new_values))
                    <tr>
                        <td>Middle Name:</td>
                        <th>
                            {!! $unserialized_new_values['middlename'] .   $beneficiary_update->getChangedLabel($unserialized_old_values['middlename'],$unserialized_new_values['middlename']  )
                                        !!}</th>
                    </tr>
                @endif
                {{--Lstname--}}
                @if(array_key_exists('lastname', $unserialized_new_values))
                    <tr>
                        <td>Last Name:</td>
                        <th>
                            {!! $unserialized_new_values['lastname'] .  $beneficiary_update->getChangedLabel($unserialized_old_values['lastname'],$unserialized_new_values['lastname']  )
                                        !!}</th>
                    </tr>
                @endif
                {{--Dob--}}
                @if(array_key_exists('dob', $unserialized_new_values))
                    <tr>
                        <td>Date of Birth:</td>
                        <th>
                            {!! short_date_format($unserialized_new_values['dob']) .  $beneficiary_update->getChangedLabel(short_date_format($unserialized_old_values['dob']),short_date_format($unserialized_new_values['dob'])  )
                                        !!}</th>
                    </tr>
                @endif
                {{--Phone--}}
                @if(array_key_exists('phone', $unserialized_new_values))
                    <tr>
                        <td>Phone:</td>
                        <th>
                            {!! $unserialized_new_values['phone'] .  $beneficiary_update->getChangedLabel($unserialized_old_values['phone'],$unserialized_new_values['phone']  )
                                        !!}</th>
                    </tr>
                @endif
                {{--Email--}}
                @if(array_key_exists('email', $unserialized_new_values))
                    <tr>
                        <td>Email:</td>
                        <th>
                            {!! $unserialized_new_values['email'].   $beneficiary_update->getChangedLabel($unserialized_old_values['email'],$unserialized_new_values['email']  )
                                        !!}</th>
                    </tr>
                @endif

                @if(array_key_exists('region_id', $unserialized_new_values))
                    @if(isset($unserialized_new_values['region_id']))
                    <tr>
                        <td>Region</td>
                        <th>
                            {!! \App\Models\Location\Region::query()->find($unserialized_new_values['region_id'])->name .   $beneficiary_update->getChangedLabel($unserialized_old_values['region_id'],$unserialized_new_values['region_id'])
                                        !!}</th>
                    </tr>
                @endif
                @endif

                @if(array_key_exists('district_id', $unserialized_new_values))
                    @if(isset($unserialized_new_values['district_id']))
                        <tr>
                            <td>District</td>
                            <th>
                                {!! \App\Models\Legal\District::query()->find($unserialized_new_values['district_id'])->name .   $beneficiary_update->getChangedLabel($unserialized_old_values['district_id'],$unserialized_new_values['district_id'])
                                            !!}</th>
                        </tr>
                    @endif
                @endif
                {{--folio--}}
                <tr>
                    <td>Doc Folio</td>
                    <th>
                        {!! $beneficiary_update->folionumber
                                    !!}</th>
                </tr>


                </tbody>
            </table>

        </div>
    </div>





</div>

@push('after-script-end')

    <script  type="text/javascript">


        $(function () {

            $("#old_values_table").hide();
            $("#old_values").click(function(){
                $("#old_values_table").slideToggle("slow");
            });

        });
    </script>;

@endpush
