@extends('layouts.backend.main', ['title' => 'Payroll Data Migration', 'header_title' => 'Payroll Data Migration'])


@section('content')

    <div style="color:#fff">
        <h5 class="cancel_button site-btn">Sync Beneficiaries processed from manual files - Notifications do not exist on the system</h5>
        <br/>
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--Manual notification--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.manual_notification.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-file-text-o"></i><large>&nbsp;&nbsp;Manual Notification Reports</large> &nbsp;<span class="badge-summary">{{ $notification_pending['pending_sync'] }}</span></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage and upload bulk manually processed notification files</p> </li>
                    </a>

                </ul>
                <br/>




                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.manual_member.upload_page', 5) !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"></i><large>&nbsp;&nbsp;Upload Manual Pensioners /Employees </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding"> Upload bulk manual payroll members i.e. employees (pensioners)</p> </li>
                    </a>

                </ul>

                <br/>
                {{--Manual MPayroll Member--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.manual_member.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"></i><large>&nbsp;&nbsp;Manual Payroll Members</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage payroll members i.e. survivors and employees (pensioners)</p> </li>
                    </a>

                </ul>


                {{--Manual Monthly Pension Payroll--}}
                {{--<ul class="list-unstyled">--}}
                {{--<a href="{!! route('backend.payroll.pensioner.index') !!}">--}}
                {{--<li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-file-text-o"></i><large>&nbsp;&nbsp;Manual Monthly Pension Payroll</large></h6>--}}

                {{--<p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage and upload bulk  manually processed monthly pension</p> </li>--}}
                {{--</a>--}}

                {{--</ul>--}}
                {{--<br/>--}}


            </div>
        </div>

        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--Sync Manual notification--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.manual_notification.pending_synchronizations") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-random" aria-hidden="true"></i><large>&nbsp;&nbsp;Sync Manual Notification</large>&nbsp;<span class="badge-summary">{{ $notification_pending['pending_sync_member'] }}</span></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Synchronize pending manual Notification with member data from the system</p> </li>
                    </a>
                </ul>
                <br/>


                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.manual_member.upload_page', 4) !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"></i><large>&nbsp;&nbsp;Upload Manual Survivors  </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding"> Upload bulk manual payroll members i.e. survivors / dependents</p> </li>
                    </a>

                </ul>

                <br/>
                {{--Sync Manual Survivors--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.payroll.manual_member.dependents_page") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-user-plus" aria-hidden="true"></i><large>&nbsp;&nbsp;Add Manual Survivors into the System</large>&nbsp;<span class="badge-summary">{{ $manual_member_pending['pending_survivor_sync'] }}</span></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Save manual members i.e. survivors into the system</p> </li>
                    </a>
                </ul>
                <br/>

                {{--Sync Manual payroll runs--}}
                {{--<ul class="list-unstyled">--}}
                {{--<a href="{!! route("backend.payroll.verification.index", 5) !!}">--}}
                {{--<li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-random" aria-hidden="true"></i><large>&nbsp;&nbsp;Sync Manual Pension Payroll</large></h6>--}}

                {{--<p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Synchronize pending manual pension payroll with data from the system</p> </li>--}}
                {{--</a>--}}
                {{--</ul>--}}
                {{--<br/>--}}




            </div>
        </div>


    </div>



    {{--Mergin files already exist in the system--}}


    <div style="color:#fff">

        <div class ="col-md-12">    <h5 class="cancel_button site-btn">Sync Beneficiaries processed from system but payroll processed manually - Notifications exist on the system</h5> <br/></div>

        {{--left div--}}
        <div class="col-sm-6 col-md-6">


            <div class="list-group">
                {{--Merge survivors--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.dependent.dependents_ready_for_merge_manual') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exchange"></i><large>&nbsp;&nbsp;Sync Survivors exist on system</large>&nbsp;<span class="badge-summary">{{ $pending_system_survivor['pending_system_survivor_sync'] }}</span></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Sync survivors exist on the system and already started being paid from manual payroll</p> </li>
                    </a>

                </ul>
                <br/>


                {{----}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.dependent.dependents_already_merged_payroll') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"></i><large>&nbsp;&nbsp;Already synced survivors exist on system</large>&nbsp;</h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Already synced survivors exist on the system and already started being paid from manual payroll</p> </li>
                    </a>

                </ul>
                <br/>

            </div>
        </div>




        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--Merge pensioners--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.pensioner.pensioners_ready_for_merge_manual') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exchange"></i><large>&nbsp;&nbsp;Sync pensioners (employees) exist on system</large>&nbsp;<span class="badge-summary">{{ $pending_system_pensioner['pending_system_pensioner_sync'] }}</span></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Sync pensioners (employees)  exist on the system and already started being paid from manual payroll</p> </li>
                    </a>

                </ul>
                <br/>


                {{--Already Merged pensioners--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.pensioner.pensioners_already_merged_payroll') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"></i><large>&nbsp;&nbsp;Already synced pensioners (employees) exist on system</large>&nbsp;</h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Already synced pensioners (employees)  exist on the system and already started being paid from manual payroll</p> </li>
                    </a>

                </ul>
                <br/>



            </div>
        </div>




    </div>




    {{--Merging System files but workflow still pending (Processesd outside  system) --}}


    <div style="color:#fff">

        <div class ="col-md-12">    <h5 class="cancel_button site-btn">Sync Beneficiaries processed from system but process completed outside the system (Workflow not approved on the system)</h5>
            <br/></div>

        {{--left div--}}
        <div class="col-sm-6 col-md-6">


            <div class="list-group">
                {{--Mark Manual System files--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.manual_system_file.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exchange"></i><large>&nbsp;&nbsp;Mark all system files which processed manually</large>&nbsp;</h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Mark all system files processed manually</p> </li>
                    </a>

                </ul>
                <br/>


                {{--Enroll Pensioner/Employees--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.manual_system_file.enrolled_pensioners_page') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"></i><large>&nbsp;&nbsp;Enrolled
                                    Pensioners / Employees into payroll</large>&nbsp;
                            </h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Enrolled pensioners / employees into payroll who have manual-system file</p> </li>
                    </a>

                </ul>
                <br/>

            </div>
        </div>




        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--Death Incidents --}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.manual_system_file.death_incidents') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exchange"></i><large>&nbsp;&nbsp;Add survivors for death incidents</large>&nbsp;</h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Add survivors for death incidents</p> </li>
                    </a>

                </ul>
                <br/>



                {{--Enroll Dependents--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.manual_system_file.enrolled_survivors_page') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"></i><large>&nbsp;&nbsp;Enrolled Dependents into payroll</large>&nbsp;
                            </h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Enrolled Dependents into payroll who have manual-system file</p> </li>
                    </a>

                </ul>
                <br/>



            </div>
        </div>




    </div>


    {{--@if(env('TESTING_MODE') == 1)--}}
        {{--Migration manual payroll pension payments (Payroll runs from manual) --}}


        <div style="color:#fff">

            <div class ="col-md-12">    <h5 style="background-color: #99adbf" class=" site-btn">Migrate Manual Monthly Pensions for Pensioners/ Employees & Survivors</h5>
                <br/></div>

            {{--left div--}}
            <div class="col-sm-6 col-md-6">


                <div class="list-group">
                    {{--Mark Manual System files--}}
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.payroll.manual_payroll_run.upload_page', 5) !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-upload"></i><large>&nbsp;&nbsp;Upload manual pensions for Employees (PD)</large>&nbsp;</h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Upload manual pensioners for employees/pensioners with PD</p> </li>
                        </a>

                    </ul>
                    <br/>


                    {{--Enroll Pensioner/Employees--}}
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.payroll.manual_payroll_run.upload_page', 4) !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-upload"></i><large>&nbsp;&nbsp;Upload manual pensions for Survivors</large>&nbsp;
                                </h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Upload manual pensions for survivors</p> </li>
                        </a>

                    </ul>
                    <br/>

                </div>
            </div>




            {{--right div--}}

            <div class="col-sm-6 col-md-6">
                <div class="list-group">
                    {{--Death Incidents --}}
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.payroll.manual_payroll_run.open_sync_page') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exchange"></i><large>&nbsp;&nbsp;Sync manual pension to respective payroll beneficiaries</large>&nbsp;</h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Synchronize manuam montly pension to ther respective payroll beneficiaries who exist on the system</p> </li>
                        </a>

                    </ul>
                    <br/>



                    <br/>



                </div>
            </div>




        </div>
    {{--@endif--}}
    {{--Send beneficiary files to e-office--}}


    {{--<div style="color:#fff">--}}

    {{--<div class ="col-md-12">    <h5 class="cancel_button site-btn">Send Beneficiaries to e-office who are not yet posted.</h5>--}}
    {{--<br/></div>--}}

    {{--left div--}}
    {{--<div class="col-sm-6 col-md-6">--}}

    {{--<div class="list-group">--}}
    {{--post beneficiaries files into e-office--}}
    {{--<ul class="list-unstyled">--}}
    {{--<a href="{!! route('backend.payroll.beneficiary.post_bulk_to_dms') !!}">--}}
    {{--<li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exchange"></i><large>&nbsp;&nbsp;Post beneficiaries files to  e-office--}}
    {{--</large>&nbsp;</h6>--}}

    {{--<p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Post Beneficiary files to e-office</p> </li>--}}
    {{--</a>--}}

    {{--</ul>--}}
    {{--<br/>--}}


    {{--</div>--}}
    {{--</div>--}}


    {{--</div>--}}



@stop

@push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             $("#site-header-title").hide();
             */
            $("#preview").click(function() {

            });
        });
    </script>;

@endpush