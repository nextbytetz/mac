@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_dependent'), 'header_title' => trans('labels.backend.member.add_dependent')])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    @include('backend.includes.assets.datetimepicker')
@endpush

@if(env('TESTING_MODE') == 1)
    @php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    @endphp
@endif
@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($employee,['route' => ['backend.payroll.manual_member.store_dependent',$employee->id ],'method'=>'post',
    'name' => 'create']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('employee_id', $employee->id, []) !!}
    {!! Form::hidden('manual_dependent_id', $dependent->id, []) !!}
    <div class="row">

        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " .$employee->lastname , [ 'id'=> 'name']) !!}</strong>

                <small >
                    @lang('labels.backend.table.member_#'): {!! Form::label( 'reg_no', $employee->memberno, [ 'id'=> 'reg_no']) !!}
                </small>

                <small >
                    Dependent: <b>{!! Form::label( '', $dependent->name, [ 'id'=> '']) !!}</b>
                </small>
            </h5>
            <legend></legend>
        </div>
    </div>

    <div>&nbsp;</div>

    {{--main contante--}}

    {{--firstname--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.firstname'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','first_name', null, ['class' => 'form-control']) !!}

                        {!! $errors->first('first_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--middlename--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.middlename'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'middle_name',null, ['class' => 'form-control', ]) !!}
                        {!! $errors->first('middle_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--lastname--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.lastname'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','last_name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('last_name', '<span class="help-block label label-danger">:message</span>') !!}

                    </div>
                </div>
            </div>

            {{--gender--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.gender'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('gender_type', $genders, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('gender_type', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--DOB--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.dob'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {{--<div class="form-inline">--}}

                        <div class="input-group" style="width:100%;">
                            {!! Form::text('dependent_dob',  isset($dependent->dob) ? short_date_format($dependent->dob) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        {!! $errors->first('dependent_dob', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--rELATION--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.relationship'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('dependent_type', $dependent_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'dependent_type']) !!}
                        {!! $errors->first('dependent_type', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>






    {{--phone--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.phone'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','phone_1', isset($dependent->phone) ? $dependent->phone : null, ['class' => 'form-control']) !!}
                        {!! $errors->first('phone_1', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--Telephone--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.telephone'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','tele_phone', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('tele_phone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--email--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.email'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','email_address', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('email_address', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--fax--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.fax'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','fax_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('fax_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--identity id--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.identity_id'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('identity', $identities, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select',  'id'=> 'identity_type']) !!}
                        {!! $errors->first('identity', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--country id--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.country'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('country', $countries, 1, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('country', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--Id No--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" id="id_no" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.id_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','id_no', null, ['class' => 'form-control' ]) !!}
                        {!! $errors->first('id_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--address--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.address'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::textarea( 'address_1', null, [ 'id'=> 'comments',  'class' =>'form-control']) !!}
                        {!! $errors->first('address_1', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--location type--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.location_type'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('location_type', $location_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'location_type']) !!}
                        {!! $errors->first('location_type', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--street--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.street'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','street_name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('street_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--road--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.road'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::input( 'text','road_name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('road', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            {{--plotno--}}
            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.plot_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','plotno', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('plotno', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            {{--block_no--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.block_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::input( 'text','blockno', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('blockno', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



        </div>
    </div>


    {{--surveyed extra--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.surveyed_extra'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','surveyedextra', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('surveyedextra', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--unsurveyed--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form unsurveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.unsurveyed_area'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::textarea('unsurveyedarea', null, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                        {!! $errors->first('unsurveyedarea', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



        </div>
    </div>



    <legend class="grey_modal">Payment details - On completion registration, dependent will be enrolled into payroll</legend>
    <div>&nbsp;</div>


    {{--Payroll details--}}

    <div class="row pensioner_div" >
        <div class="col-md-12">


            {{--bank--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Bank</label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('bank', $banks, isset($dependent) ? $dependent->bank_id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select pd_input', 'id'=> 'bank_select']) !!}
                        <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
                        {!! $errors->first('bank', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--Mp--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Monthly Pension</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('mp',  isset($dependent) ? number_2_format($dependent->monthly_pension) : null, ['placeholder' => '', 'class' => 'form-control  pd_input money', 'autocomplete' => 'off' , 'id' => 'mp']) !!}
                        {!! $errors->first('mp', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>



    <div class="row " >
        <div class="col-md-12">

            {{--branch--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Bank Branch</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('bank_branch', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select pd_input', 'id'=> 'bank_branch_select' ]) !!}
                        {!! $errors->first('bank_branch', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--Other dep--}}
            <div class="element-form ">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Is Other Dependent!</label><span class="">
                                <br/>
                        {{ Form::checkbox('isotherdep',1,1,['class'=>'', 'multiple', 'id' => 'isotherdep']) }}
                            </span></div>

                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('other_dependency_type', ['1' => 'Full' ,/* '2' => 'Partial'*/], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'other_dependency_type' ]) !!}
                        {!! $errors->first('other_dependency_type', '<span class="help-block label label-danger">:message</span>') !!}
                        {!! small_helper('Dependency Type') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>




    <div class="row " >
        <div class="col-md-12">
            {{--ccountno--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Account No.</label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('accountno_dependent',  isset($dependent) ? $dependent->accountno : null, ['placeholder' => '', 'class' => 'form-control pd_input', 'autocomplete' => 'off', 'id' => 'pd']) !!}
                        {!! $errors->first('accountno_dependent', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--IS Paid--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Already Paid</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!!  Form::select('ispaid', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select' ,'placeholder'=> '', 'id'=>'ispaid']) !!}
                        {!! $errors->first('ispaid', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">Has dependent started receiving monthly pension on manual process?</small>
                    </div>
                </div>
            </div>


        </div>
    </div>




    <div class="row " >
        <div class="col-md-12">
            {{----}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {{--{!! Form::text('accountno_dependent',  isset($dependent) ? $dependent->accountno : null, ['placeholder' => '', 'class' => 'form-control pd_input', 'autocomplete' => 'off', 'id' => 'pd']) !!}--}}
                        {{--{!! $errors->first('accountno_dependent', '<span class="help-block label label-danger">:message</span>') !!}--}}
                        {{--</div>--}}
                    </div>
                </div>

                {{--Other dep Months paid--}}
                <div class="element-form arrears_div " id="other_dep_months_paid_div"  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Months Paid</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::text('other_dep_months_paid',  null, ['placeholder' => '', 'class' => 'form-control number arrears', 'autocomplete' => 'off' , 'id' => 'other_dep_months_paid']) !!}
                            {!! $errors->first('other_dep_months_paid', '<span class="help-block label label-danger">:message</span>') !!}
                            {!! small_helper('No of months already paid for other dependent') !!}
                        </div>
                    </div>
                </div>






            </div>
        </div>
    </div>


    <div class="row" >
        <div class="col-md-12">
            {{----}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {{--{!! Form::text('accountno_dependent',  isset($dependent) ? $dependent->accountno : null, ['placeholder' => '', 'class' => 'form-control pd_input', 'autocomplete' => 'off', 'id' => 'pd']) !!}--}}
                        {{--{!! $errors->first('accountno_dependent', '<span class="help-block label label-danger">:message</span>') !!}--}}
                        {{--</div>--}}
                    </div>
                </div>


                {{--Has arrears--}}
                <div class="element-form arrears_div"  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Has Arrears?</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!!  Form::select('hasarrears', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select arrears' ,'placeholder'=> '', 'id'=>'hasarrears']) !!}
                            {!! $errors->first('hasarrears', '<span class="help-block label label-danger">:message</span>') !!}
                            <small class="form-text text-muted" style="width:100% !important;">Does dependent has any arrears (unpaid) montly pension?</small>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="row " >
        <div class="col-md-12">
            {{----}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {{--{!! Form::text('accountno_dependent',  isset($dependent) ? $dependent->accountno : null, ['placeholder' => '', 'class' => 'form-control pd_input', 'autocomplete' => 'off', 'id' => 'pd']) !!}--}}
                        {{--{!! $errors->first('accountno_dependent', '<span class="help-block label label-danger">:message</span>') !!}--}}
                        {{--</div>--}}
                    </div>
                </div>



                {{--pending pay months--}}
                <div class="element-form pending_months_div "  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>No. of months pending</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::text('pending_pay_months', null , ['placeholder' => '', 'class' => 'form-control number ', 'autocomplete' => 'off', 'id' => 'pending_pay_months']) !!}
                            {!! $errors->first('pending_pay_months', '<span class="help-block label label-danger">:message</span>') !!}
                            <small class="form-text text-muted" style="width:100% !important;">No. of months not yet paid to this dependent.</small>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="row " >
        <div class="col-md-12">
            {{----}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {{--{!! Form::text('accountno_dependent',  isset($dependent) ? $dependent->accountno : null, ['placeholder' => '', 'class' => 'form-control pd_input', 'autocomplete' => 'off', 'id' => 'pd']) !!}--}}
                        {{--{!! $errors->first('accountno_dependent', '<span class="help-block label label-danger">:message</span>') !!}--}}
                        {{--</div>--}}
                    </div>
                </div>





                {{--Remark--}}
                <div class="element-form pending_months_div "  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Remark</label><span class="required_asterik"></span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group text_content">
                            {!! Form::textarea('remark', null , ['placeholder' => '', 'class' => 'form-control ', 'autocomplete' => 'off', 'id' => 'remark']) !!}
                            {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                            {{--<small class="form-text text-muted" style="width:100% !important;">No. of months not yet paid to this dependent.</small>--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-10" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.payroll.manual_member.dependents_page',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>




    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--Bank - Branch select auto fill--}}
    @include('backend/includes/assets/auto_fill_sub_category_select', ['child_value' => old('bank_branch'), 'parent_id' => 'bank_select', 'child_id' => 'bank_branch_select',
          'child_hideable'   => 0, 'isedit' => isset($dependent->bank_branch_id) ? 1 : 0, 'get_url' => 'getbankbranch?bank_id='])

    <script  type="text/javascript">
        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();

        $('body').on('submit', 'form[name=create]', function(e) {
            e.preventDefault();
            // Validate date -- paymaent date
            var $day = $('#dob_day').val();
            var $month = $('#dob_month').val();
            var $year = $('#dob_year').val();
            if (($year) && ($month) && ($day )) {
                $('input[name=dob]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=dob]").val("");
            }
            $("#dependency_percentage").prop("disabled", false);
            $("#isotherdep").prop("disabled", false);

            beneficiary_hide_disable_all_options('beneficiary_checkbox','survivor_gratuity_receiver','survivor_pension_receiver','dependency_percentage','funeral_grant_percent');

            this.submit();

        });

        $(function () {
            $(".search-select").select2();


            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/



            $(".surveyed").hide();
            $(".unsurveyed").hide();
            $("#child_representative").hide();
            beneficiary_options('beneficiary_checkbox', 'beneficiary_div');
            $("#beneficiary_checkbox").click(function () {
                beneficiary_options('beneficiary_checkbox', 'beneficiary_div');
            });

            location_type_option('location_type', 'surveyed','unsurveyed');
            $("#location_type").on('change', function (e){
                location_type_option('location_type', 'surveyed','unsurveyed');
            });

            relation_option('dependent_type', 'child_representative', 'dependency_percentage', 'survivor_gratuity_receiver', 'survivor_pension_receiver');
            $("#dependent_type").on("change", function (e) {
                relation_option('dependent_type', 'child_representative','dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
                otherDepOptionByRelation();
                otherDepMonthsPaidOption();
            });

            pension_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
            $("#survivor_pension_receiver").on("change", function (e) {
                pension_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
                relation_option('dependent_type', 'child_representative','dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
                dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            });

            gratuity_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
            $("#survivor_gratuity_receiver").on("change", function (e) {
                gratuity_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
                relation_option('dependent_type', 'child_representative','dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
                dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            });

            dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            $("#dependency_percentage").on("change", function (e) {
                dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            });



            identity_option('identity_type', 'id_no');
            $("#identity_type").on('change', function (e){
                identity_option('identity_type', 'id_no');
            });

            $("#isotherdep").click(function(){
                var check =         $('#isotherdep').prop('checked');
                if(check == true){
                    enable_disable('enable_id','other_dependency_type');
                    // $('#other_dependency_type').val(0).change();
                }else{
                    enable_disable('disable_id','other_dependency_type');
                    $('#other_dependency_type').val(0).change();
                }

                otherDepMonthsPaidOption();
            });

            {{--$('#bank_select').on('change', function (e) {--}}
            {{--$("#spin2").show();--}}
            {{--var bank_id = e.target.value;--}}
            {{--$.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {--}}
            {{--$('#bank_branch_select').empty();--}}
            {{--$("#bank_branch_select").select2("val", "");--}}
            {{--$('#bank_branch_select').html(data);--}}
            {{--$("#spin2").hide();--}}
            {{--});--}}
            {{--});--}}


            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });

            /*Option on choosing ispaid*/
            ispaid_option();
            $("#ispaid").on("change", function (e) {
                $('#hasarrears').val('').change();
                ispaid_option();
                hasarrears_option();
            });

            /*Option on choosing has arrears*/
            hasarrears_option();
            $("#hasarrears").on("change", function (e) {
                hasarrears_option();
            });

        });
        // beneficiary options
        function beneficiary_options(beneficiary_checkbox, beneficiary_div) {
            if ($("#" + beneficiary_checkbox).is(":checked")) {
                $("#" + beneficiary_div).show();
            } else {
                $("#" + beneficiary_div).hide();

            }
        }


        // beneficiary options when beneficiary is hidden
        function beneficiary_hide_disable_all_options(beneficiary_checkbox,survivor_gratuity_receiver,survivor_pension_receiver,dependency_percentage,funeral_grant_percent) {
            if ($("#" + beneficiary_checkbox).is(":checked")) {

            } else {
                $("#" + funeral_grant_percent ).prop("disabled", true);
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);
                $("#" + survivor_pension_receiver ).prop("disabled", true);
                $("#" + dependency_percentage ).prop("disabled", true);
            }
        }


        //Gratuity options -> disable pension percent when entering gratuity percent
        function gratuity_percent_option(survivor_gratuity_receiver,survivor_pension_receiver) {
            var $value = $("#" + survivor_gratuity_receiver).val();
            if ($value == 1) {
                $("#" + survivor_pension_receiver ).prop("disabled", true);

            }
            else {
                $("#" + survivor_pension_receiver).prop("disabled", false);
            }

        }

        //Pension percent options -> disable gratuity percent when entering pension percent
        function pension_percent_option(survivor_gratuity_receiver,survivor_pension_receiver) {
            var $value = $("#" + survivor_pension_receiver).val();
            if ($value == 1) {
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);

            }
            else {
                $("#" + survivor_gratuity_receiver).prop("disabled", false);
            }
        }

        // check if child is selected - fill if need representative
        function relation_option(dependent_type, child_representative,dependency_percentage,survivor_gratuity_receiver,survivor_pension_receiver) {

            var choice = $("#" + dependent_type).val();
//            if is child
            if (choice == 3){
                $("#" + child_representative).show();
                $(".child_inputs").show();
                $(".child_input_fields" ).prop("disabled", false);
            }else{
                $("#" + child_representative).hide();
                $(".child_inputs").hide();
                $(".child_input_fields" ).prop("disabled", true);
            }

            //            if is child or spouse (wife or husband)
            if ((choice == 1) || (choice == 2) || (choice == 3) || (choice == 7)){
                $("#" + dependency_percentage).prop("disabled", true);
                $("#" + dependency_percentage).val('');
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);
                $("#" + survivor_pension_receiver ).prop("disabled", false);
            }else{
                $("#" + dependency_percentage).prop("disabled", false);
                $("#" + survivor_gratuity_receiver ).prop("disabled", false);
                $("#" + survivor_pension_receiver ).prop("disabled", false);
            }
        }

        //Dependency options -> options for dependency percentages
        function dependency_option(dependency_percentage,survivor_gratuity_receiver,survivor_pension_receiver) {
            var $value = $("#" + dependency_percentage).val();
            if ($value == 100) {
                $("#" + survivor_pension_receiver ).prop("disabled", false);
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);

            }
            else if(($value > 0) && ($value < 100)){

                $("#" + survivor_gratuity_receiver ).prop("disabled", false);
                $("#" + survivor_pension_receiver ).prop("disabled", true);

            }

        }




        // check if location type is selected -> survyed and unsurveyed
        function location_type_option(location_type, surveyed,unsurveyed) {

            var choice = $("#"+location_type).val();
            switch (choice) {
                case '1':
                    $("." + surveyed).show();
                    $("." + unsurveyed).hide();
                    break;
                case '2':
                    $("."+surveyed).hide();
                    $("."+unsurveyed).show();
                    break;
                default:
                    $("." + surveyed).hide();
                    $("." + unsurveyed).hide();
            }
        }



        // check if identity type is selected
        function identity_option(identity_type, id_no) {

            var choice = $("#"+identity_type).val();
            switch (choice) {
                case '5':
                    $("#" + id_no).hide();

                    break;

                default:
                    $("#" + id_no).show();
            }
        }


        /*is paid option*/
        function ispaid_option()
        {
            var ispaid =  $("#ispaid").val();
            if(ispaid == 1){
                $(".arrears" ).prop("disabled", false);
                $(".arrears_div").show();
                $('#other_dep_months_paid').val(null).change();
                otherDepMonthsPaidOption();
            }else{
                $(".arrears" ).prop("disabled", true);
                $(".arrears_div").hide();
                $('#other_dep_months_paid').val(0).change();
            }
        }


        /*has arrears option*/
        function hasarrears_option()
        {
            var hasarrears =$("#hasarrears").val();
            if(hasarrears == 1){
                $("#pending_pay_months").prop("disabled", false);
                $("#remark").prop("disabled", false);
                $(".pending_months_div").show();
            }else{
                $("#pending_pay_months").prop("disabled", true);
                $("#remark").prop("disabled", true);
                $(".pending_months_div").hide();
            }
        }

        /*Other Dep months paid*/
        otherDepMonthsPaidOption();
        function otherDepMonthsPaidOption()
        {
            var isotherdep = $('#isotherdep').prop('checked');
            if(isotherdep == true){
                /*enable other dep months paid*/
                enable_disable('enable_id', 'other_dep_months_paid');
                // $('#other_dep_months_paid').val('').change();
                hide_show('show_id', 'other_dep_months_paid_div');

            }else{
                /*disable other dep months paid*/
                enable_disable('disable_id', 'other_dep_months_paid');
                $('#other_dep_months_paid').val('').change();
                hide_show('hide_id', 'other_dep_months_paid_div')
            }
        }


        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }

        //  Hide/ show child inputs
        function hide_show_child_inputs() {

        }

        /*Other dep option by relation*/
        otherDepOptionByRelation();
        function otherDepOptionByRelation()
        {
            var choice = element_id_value('dependent_type');
            switch(choice){
                case '1'://husband
                case '2'://wife
                case '3'://child
                case '7'://constant care
                    $('#isotherdep').attr("checked", false);
                    enable_disable('enable_id', 'isotherdep');
                    enable_disable('disable_id', 'other_dependency_type');
                    $('#other_dependency_type').val(0).change();
                    break;

                default:
                    /*Prompt other dep*/
                    $('#isotherdep').prop("checked", true);
                    enable_disable('disable_id', 'isotherdep');
                    enable_disable('enable_id', 'other_dependency_type');
                    $('#other_dependency_type').val(1).change();
                    break;
            }
        }



    </script>;


@endpush

