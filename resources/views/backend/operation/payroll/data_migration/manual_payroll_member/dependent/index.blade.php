@extends('layouts.backend.main', ['title' => 'Manual Survivors', 'header_title' => 'Manual Survivors'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')



    <br/>
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="manual-survivor-table">
                <thead>
                <tr >
                    <th>Member Type</th>
                    <th>Case no</th>
                    <th>Name</th>
                    <th>DOB</th>
                    <th>Bank</th>
                    <th>Account no.</th>
                    <th>Monthly Pension</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#manual-survivor-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.manual_member.get_uploaded_for_datatable',4) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type', name: 'member_type',orderable : false, searchable : false},
                    { data: 'case_no', name: 'case_no'},
                    { data: 'name', name: 'name',  orderable : true, searchable : true},
                    { data: 'dob_formatted', name: 'dob',  orderable : true, searchable : true},
                    { data: 'bank_name' , name: 'bank_name', orderable : false, searchable : false},
                    { data: 'accountno', name: 'accountno',  orderable : true, searchable : true},
                    { data: 'mp_formatted', name: 'monthly_pension',  orderable : true, searchable : true},
                    { data: 'action', name: 'action',orderable : false, searchable : false},
                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }
                // initComplete: function () {
                //     addDeleteForms();
                // },

            });

        });


    </script>;

@endpush
