


{{--upload section--}}
{!! Form::open(['route' => ['backend.payroll.manual_member.upload_bulk'],'method'=>'post',
  'name' => 'upload' , 'enctype'=>"multipart/form-data"]) !!}
{!! Form::hidden('member_type_id', $member_type_id, []) !!}
<div class = "row">
    <div class="col-md-12" >
        <div class="row">
            <div class="col-md-12 light_grey_bg" style="height:25px">
                <div class="form-inline" >
                    {{--document --}}
                    <div class="col-md-5" align="left">
                        <div class="form-group">
                            <label style="font-size: 15px">Document</label>
                        </div>
                    </div>

                    <div class="col-md-4" align="left">
                        <div class="form-group">
                            <label style="font-size: 15px"> Filename</label>
                        </div>
                    </div>


                    {{--male--}}
                    <div class="col-md-3" align="left">
                        <div class="form-group">
                            <label style="font-size: 15px"> Choose file</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<br/>
<div class="row">
    <div class="col-md-12">
        <div class="form-inline" >
            <div class="col-md-5">
                <div class="form-group">

                    {!! Form::label( 'document_name', (($member_type_id == 5) ? 'Employees (Pensioners)' : 'Survivors') .' File', [ 'class' => '' ])!!}

                </div>
                <br/>
                <span class="excel_template">
                                                  <a target="_blank" style="text-decoration: none;margin-top: 40px; color: blue"
                                                     href= "{!! asset_url() . '/nextbyte/template/template_manual_payroll_member.xlsx'  !!}">Download template | </a>
                                            </span>

                <span class="excel_template">
                                                  <a target="_blank" style="text-decoration: none;margin-top: 40px; color: blue"
                                                     href= "{!! asset_url() . '/nextbyte/template/instruction_manual_payroll_member.xlsx'  !!}">Instruction</a>
                                            </span>

            </div>


            {{--Filename--}}
            <div class="col-md-4">
                <div class="form-group">
                    @if($no_of_uploaded_files > 0)
                        <a target="_blank" style="text-decoration: none"
                           href="{{ url($uploaded_file_url)}}">{!! Form::label( 'document_name', 'Manual payroll member.xlsx', [ 'class' => '' ])!!}</a>
                    @endif
                </div>

            </div>



            {{--choose--}}
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::file('document_file', ['class'=> 'document_file', '', 'aria-describedby' => 'maxSizeHelp']) !!}
                    {{--{!! Form::hidden('document_file', null, []) !!}--}}
                    {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
                <small id="maxSizeHelp" class="form-text text-muted">{{ 'File format - .xlsx' }}</small>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="element-form">
            <div class="form-group pull-right">
                {!! link_to_route('backend.payroll.manual_member.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>
</div>


{!! Form::close() !!}
