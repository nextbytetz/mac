@extends('layouts.backend.main', ['title' => 'Upload Bulk Manual '. (($member_type_id == 5) ? 'Employees (Pensioners)' : 'Survivors'), 'header_title' => 'Upload Bulk  Manual ' . (($member_type_id == 5) ? 'Employees (Pensioners)' : 'Survivors')])

@include('backend.includes.datatable_assets')

@section('content')

    @include('backend/operation/payroll/data_migration/manual_payroll_member/upload_bulk/includes/upload_section')

    <br/>
    <br/>
    @include('backend/operation/payroll/data_migration/manual_payroll_member/upload_bulk/includes/get_uploaded_files')

@stop


@push('after-script-end')

    <script  type="text/javascript">


    </script>;

@endpush
