
<legend class="grey_modal">All Uploaded Files - {!! $no_of_uploaded_files !!}</legend>


<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="uploaded-notification-table">
            <thead>
            <tr >
                <th>Member Type</th>
                <th>Case no</th>
                <th>Name</th>
                <th>DOB</th>
                <th>Bank</th>
                <th>Account no.</th>
                <th>Monthly Pension</th>
                <th>Phone</th>
                {{--<th>Status</th>--}}

            </tr>
            </thead>
        </table>

    </div>
</div>





@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#uploaded-notification-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.manual_member.get_uploaded_for_datatable', $member_type_id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type', name: 'member_type',orderable : false, searchable : false},
                    { data: 'case_no', name: 'case_no'},
                    { data: 'name', name: 'name',  orderable : true, searchable : true},
                    { data: 'dob_formatted', name: 'dob',  orderable : true, searchable : true},
                    { data: 'bank_name' , name: 'bank_name', orderable : false, searchable : false},
                    { data: 'accountno', name: 'accountno',  orderable : true, searchable : true},
                    { data: 'mp_formatted', name: 'monthly_pension',  orderable : true, searchable : true},
                    { data: 'phone', name: 'phone',  orderable : true, searchable : true},
                    // { data: 'status', name: 'status',  orderable : false, searchable : false},


                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
