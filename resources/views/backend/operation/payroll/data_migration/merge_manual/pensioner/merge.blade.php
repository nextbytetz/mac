@extends('layouts.backend.main', ['title' => 'Merge Pensioner', 'header_title' => 'Merge Pensioner'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    @include('backend.includes.assets.datetimepicker')
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($pensioner,['route' => ['backend.payroll.pensioner.merge_manual_pensioner', $pensioner->id],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('action_type', 1, []) !!}
    <div class="row">

        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name',$pensioner->name , [ 'id'=> 'name']) !!}</strong>

                {{--<small >--}}
                    {{--Case no.: {!! Form::label( 'reg_no', $notification_report->case_no, [ 'id'=> 'case_no']) !!}--}}
                {{--</small>--}}
            </h5>
            <legend></legend>
        </div>
    </div>

    <div>&nbsp;</div>

    {{--main contante--}}

    {{--first name--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>First name:</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','firstname', null, ['class' => 'form-control', '']) !!}
                        {{--<small id="" class="form-text text-muted">{{ 'Employee name as registered on manual file' }}</small>--}}
                        {!! $errors->first('firstname', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--Middlename name --}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Middle name:</label><span class=""></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','middlename', null, ['class' => 'form-control', '']) !!}
                        {{--<small id="" class="form-text text-muted">{{ 'Employee name as registered on manual file' }}</small>--}}
                        {!! $errors->first('middlename', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--2--}}
    <div class="row">
        <div class="col-md-12">
            {{--Lastname--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Last Name:</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'lastname', null, ['class' => 'form-control', '']) !!}
                        {{--<small id="" class="form-text text-muted">{{ 'Employee name as registered on the system' }}</small>--}}
                        {!! $errors->first('lastname', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--dob--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>DOB</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {{--{!! Form::input('text', 'dob',(isset($pensioner->dob) ? short_date_format($pensioner->dob) : null), ['class' => 'form-control', '']) !!}--}}
                        {{--<small id="" class="form-text text-muted">{{ 'Employee name as registered on the system' }}</small>--}}
                        {{--{!! $errors->first('dob', '<span class="help-block label label-danger">:message</span>') !!}--}}

                        <div class="input-group" style="width:100%;">
                            {!! Form::text('dob',  isset($pensioner->dob) ? short_date_format($pensioner->dob) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        {!! $errors->first('dob', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--3--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Phone:</label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'phone',null, ['class' => 'form-control', '']) !!}
                        {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--email--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Email:</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('email', null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                        {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--<div>      <label>{!! Form::checkbox( 'check_correct',1, false, ['style' => 'width:20px','id' => 'check_correct' ]) !!}Are all data correct?</label>--}}
    {{--{!! $errors->first('check_correct', '<span class="help-block label label-danger">:message</span>') !!}--}}
    {{--</div>--}}

    <legend class="grey_modal pd_div">Pensioner details - On completion registration, pensioner will be enrolled into payroll</legend>
    <div>&nbsp;</div>


    {{--Pensioner details--}}
    {!! Form::hidden('pensioner_id', isset($pensioner) ? $pensioner->id : null, ['class' =>'']) !!}
    <div class="row pensioner_div" >
        <div class="col-md-12">


            {{--bank--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Bank</label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('bank', $banks, isset( $pensioner->bank_branch_id) ?  $pensioner->bankBranch->bank->id  : $pensioner->bank_id , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select pd_input', 'id'=> 'bank_select']) !!}
                        <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
                        {!! $errors->first('bank', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--Mp--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Monthly Pension</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('mp',  isset($pensioner) ? number_2_format($pensioner->monthly_pension_amount) : null, ['placeholder' => '', 'class' => 'form-control  pd_input money', 'autocomplete' => 'off' , 'id' => 'mp']) !!}
                        {!! $errors->first('mp', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



        </div>
    </div>



    <div class="row pensioner_div" >
        <div class="col-md-12">

            {{--branch--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Bank Branch</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('bank_branch', $bank_branches, $pensioner->bank_branch_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select pd_input', 'id'=> 'bank_branch_select' ]) !!}
                        {!! $errors->first('bank_branch', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--IS Paid--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Already Paid</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!!  Form::select('ispaid', ['0' => 'No', '1' => 'Yes'],  1, ['class' => 'form-control search-select' ,'placeholder'=> '', 'id'=>'ispaid', 'disabled']) !!}
                        {!! $errors->first('ispaid', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">Has dependent started receiving monthly pension on manual process?</small>
                    </div>
                </div>
            </div>



        </div>
    </div>




    <div class="row pensioner_div" >
        <div class="col-md-12">

            {{--ccountno--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Account No.</label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('accountno',  isset($pensioner) ? $pensioner->accountno : null, ['placeholder' => '', 'class' => 'form-control pd_input', 'autocomplete' => 'off', 'id' => 'pd']) !!}
                        {!! $errors->first('accountno', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



            {{--Has arrears--}}
            <div class="element-form arrears_div"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Has Arrears?</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!!  Form::select('hasarrears', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select arrears' ,'placeholder'=> '', 'id'=>'hasarrears']) !!}
                        {!! $errors->first('hasarrears', '<span class="help-block label label-danger">:message</span>') !!}
                        <small class="form-text text-muted" style="width:100% !important;">Does dependent has any arrears (unpaid) monthly pension?</small>
                    </div>
                </div>
            </div>


        </div>
    </div>


    <div class="row " >
        <div class="col-md-12">
            {{----}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label><span class="required_asterik"></span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                    </div>
                </div>

                {{--pending pay months--}}
                <div class="element-form pending_months_div "  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>No. of months pending</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::text('pending_pay_months', null , ['placeholder' => '', 'class' => 'form-control number ', 'autocomplete' => 'off', 'id' => 'pending_pay_months']) !!}
                            {!! $errors->first('pending_pay_months', '<span class="help-block label label-danger">:message</span>') !!}
                            <small class="form-text text-muted" style="width:100% !important;">No. of months not yet paid to this dependent.</small>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

        <div class="row " >
            <div class="col-md-12">
                {{----}}
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label></label><span class="required_asterik"></span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">

                        </div>
                    </div>

                    {{--Remark--}}
                    {{--<div class="element-form pending_months_div "  >--}}
                        {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Remark</label><span class="required_asterik">*</span></div>--}}
                        {{--<div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">--}}
                            {{--<div class="form-group text_content">--}}
                                {{--{!! Form::textarea('remark', null , ['placeholder' => '', 'class' => 'form-control ', 'autocomplete' => 'off', 'id' => 'remark']) !!}--}}
                                {{--{!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}--}}
                                {{--<small class="form-text text-muted" style="width:100% !important;">No. of months not yet paid to this dependent.</small>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                </div>
            </div>

        </div>




    {{--Buttons--}}
    <div class="row">
        <div class="col-md-10" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.payroll.pensioner.pensioners_ready_for_merge_manual',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>




    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}

    {{--Bank - Branch select auto fill--}}
    @include('backend/includes/assets/auto_fill_sub_category_select', ['child_value' => old('bank_branch'), 'parent_id' => 'bank_select', 'child_id' => 'bank_branch_select',
          'child_hideable'   => 0, 'isedit' => 1, 'get_url' => 'getbankbranch?bank_id='])


    <script  type="text/javascript">
        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();

        $('body').on('submit', 'form[name=create]', function(e) {
            e.preventDefault();


            this.submit();

        });

        $(function () {
            $(".search-select").select2();


            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            pensioner_option();
            pd_option();
            $("#incident_type").on("change", function (e) {
                pd_option();
                pensioner_option()
            });


            $('#bank_select').on('change', function (e) {
                $("#spin2").show();
                var bank_id = e.target.value;
                $.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {
                    $('#bank_branch_select').empty();
                    $("#bank_branch_select").select2("val", "");
                    $('#bank_branch_select').html(data);
                    $("#spin2").hide();
                });
            });



            /*Option on choosing ispaid*/
            ispaid_option();
            $("#ispaid").on("change", function (e) {
                $('#hasarrears').val('').change();
                ispaid_option();
                hasarrears_option();
            });

            /*Option on choosing has arrears*/
            hasarrears_option();
            $("#hasarrears").on("change", function (e) {
                hasarrears_option();
            });

            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });

        });


        //pd option - dependent on chosen incident type - oNLY for accident and disease these filed should be applicable
        function pd_option() {
            var $value = $("#incident_type").val();
            if ($value == 3) {
                /*death*/
                $(".pd_input" ).prop("disabled", true);
                $(".pd_div" ).hide();
            }else{
                /*accident/disease*/
                $(".pd_input" ).prop("disabled", false);
                $(".pd_div" ).show();
            }


        }

        // pensiner options
        function pensioner_option() {
            if ( $("#incident_type").val() != 3) {
                $(".pensioner_div").show();
            } else {
                $(".pensioner_div").hide();

            }
        }


        /*is paid option*/
        function ispaid_option()
        {
            var ispaid =  $("#ispaid").val();
               if(ispaid == 1){
                $(".arrears" ).prop("disabled", false);
                $(".arrears_div").show();
            }else{
                $(".arrears" ).prop("disabled", true);
                $(".arrears_div").hide();
            }
        }


        /*has arrears option*/
        function hasarrears_option()
        {
            var hasarrears =$("#hasarrears").val();
            if(hasarrears == 1){
                $("#pending_pay_months").prop("disabled", false);
                $("#remark").prop("disabled", false);
                $(".pending_months_div").show();
            }else{
                $("#pending_pay_months").prop("disabled", true);
                $("#remark").prop("disabled", true);
                $(".pending_months_div").hide();
            }
        }



        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            allowZero : false,
            affixesStay : false
        });

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
    </script>;


@endpush

