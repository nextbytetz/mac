@extends('layouts.backend.main', ['title' => 'Survivors exist on system - already merged', 'header_title' => 'Survivors exist on system - already merged'])

@include('backend.includes.datatable_assets')

@section('content')


    <br/>
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="dependent-table">
                <thead>
                <tr >
                    <th>Filename</th>
                    <th>Survivor Name</th>
                    <th>Employee</th>
                    <th>Relation</th>
                    <th>Monthly Pension</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>


@stop


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#dependent-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.dependent.get_already_merged_payroll') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'filename', name: 'notification_reports.filename'},
                    { data: 'fullname' , name: 'dependents.firstname', orderable : true, searchable : true},
                    { data: 'employee_name' , name: 'employees.firstname', orderable : true, searchable : false},
                    { data: 'type_name', name: 'dependent_types.name',  orderable : false, searchable : false},
                    { data: 'survivor_pension_amount', name: 'dependent_employee.survivor_pension_amount',  orderable : true, searchable : true},
                    { data: 'status', name: 'status',  orderable : false, searchable : false},
                    { data: 'action', name: 'action',  orderable : false, searchable : false},
                    /*invisible fields*/
                    { data: 'middlename', name: 'dependents.middlename' ,  orderable : false, searchable : true, visible: false},
                    { data: 'lastname', name: 'dependents.lastname' ,  orderable : false, searchable : true,visible: false },
                    { data: 'emp_firstname', name: 'employees.firstname' ,  orderable : false, searchable : true, visible: false},
                    { data: 'emp_middlename', name: 'employees.middlename' ,  orderable : false, searchable : true, visible: false},
                    { data: 'emp_lastname', name: 'employees.lastname' ,  orderable : false, searchable : true,visible: false }
                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
