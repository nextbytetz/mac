@extends('layouts.backend.main', ['title' => 'Monthly Pension Payroll Approval', 'header_title' => 'Monthly Pension Payroll Approval Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>


        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row">
        @include("backend/operation/payroll/run/profile/includes/header_info")
    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>
                    {{--pensioners--}}
                    <li class="nav-item">
                        <a class="nav-link" href="#pensioners" id="pensioners_header"
                           data-toggle="tab">Pensioners Payments
                        </a>
                    </li>
                    {{--dependents--}}
                    <li class="nav-item">
                        <a class="nav-link" href="#dependents" id="dependents_header"
                           data-toggle="tab">Dependents Payments
                        </a>
                    </li>
                    {{--suspended_payments--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#suspended_payments" id="suspended_header"--}}
                    {{--data-toggle="tab">Suspended Payments--}}
                    {{--</a>--}}
                    {{--</li>--}}

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >


                                        @if($check_finance_level == 1 && $payroll_run_approval->ispayprocessed == 0)

                                            <span>
                                           {!! HTML::decode(link_to_route('backend.payroll.pension.run_approval.process_payment', "<i class='icon fa fa-money' aria-hidden='true'></i>&nbsp;" . 'Process Payment', $payroll_run_approval->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to process payment for this payroll?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>
                                        @endif

                                        @if($check_pending_level1 == 1 || $check_workflow == 0)
                                            {{--<span>--}}
                                            {{--<a href="{!! route('backend.payroll.edit_new_bank',$bank_update->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Edit--}}
                                            {{--New Bank</a>--}}
                                            {{--</span>--}}
                                            @if($payroll_run_approval->run_status == 1)
                                                <span>
                                           {!! HTML::decode(link_to('#', "<i class=\"icon fa fa-confirm\"></i>&nbsp;Initiate", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Payroll Approval Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $payroll_run_approval->id, 'data-route' => route('backend.payroll.pension.run_approval.initiate', $payroll_run_approval->id)])) !!}
                                            </span>

                                            @endif

                                            <span>
                                           {!! HTML::decode(link_to_route('backend.payroll.pension.run_approval.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $payroll_run_approval->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this payroll process?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>


                                        @endif
                                        {{--Send to e-office--}}
                                        @if($payroll_run_approval->isdmsposted == 0 && $payroll_run_approval->wf_done == 1)
                                            <a href="{!! route('backend.payroll.post_to_dms', ['payroll_run_approval' => $payroll_run_approval->id, ]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-upload"></i>&nbsp;Send to E-office
                                            </a>
                                        @endif



                                        {{--close--}}

                                        <a href="{!! route('backend.payroll.pension.run_approval.index') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>

                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--general info--}}
                                    @include('backend/operation/payroll/run/profile/includes/general_info')
                                    {{--disease adjust overview--}}
                                    <div>&nbsp;</div>
                                    {{--APprovals tabs history--}}
                                    {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/payroll/run/profile/includes/sidebar_summary')
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--pensioners payments--}}
                    <div id="pensioners" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/run/profile/includes/get_mp_pensioners' ,['member_type_id' => 5, 'payroll_run_approval_id' =>
                         $payroll_run_approval->id])
                    </div>
                    {{--dependents payments--}}
                    <div id="dependents" class="nav_tab_pane tab-pane">
                        @include('backend/operation/payroll/run/profile/includes/get_mp_dependents' ,['member_type_id' => 4, 'payroll_run_approval_id' =>
                                              $payroll_run_approval->id])
                    </div>

                    {{--susendeed payments--}}
                    {{--<div id="suspended_payments" class="nav_tab_pane tab-pane">--}}
                    {{--@include('backend/operation/payroll/pension_administration/profile/includes/monthly_pension_tab/get_monthly_pensions' ,['member_type_id' => 5, 'resource_id' =>--}}
                    {{--$pensioner->id, 'notification_report_id' => $pensioner->notification_report_id])--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

    </div>

    @include("backend.system.workflow.includes.initiate_modal")

    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {


            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });
        });
    </script>;

@endpush
