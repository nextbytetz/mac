
<div class="modal fade top-modal" id="bank_allocations_modal" tabindex="-1" role="dialog" aria-labelledby="largemodal" style="display: none;width:100%" aria-hidden="true">

    <div class="modal-dialog modal-lg" style=" background-color: white" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Bank Payments Allocations</h4>
            </div>
            <div class="modal-body">

                <div class = "row">
                    <div class="col-md-12" >
                        <br/>
                        <div class="col-md-5" >
                            <legend style="background-color: lightgrey;text-align:center">Previous Payroll</legend>
                            <table style="width:100%" class="table table-striped table-bordered">
                                {{--header--}}
                                <tr style="background-color: #eaeaea; font-weight: bold">
                                    <td style="padding-left: 5px" width="160px">Bank Name</td>
                                    <td  height="20px">Amount</td>

                                </tr>
                                @if(isset($previous_payroll_approval))
                                    @foreach($previous_payroll_approval->getBanksInPayrollRunQuery($previous_payroll_approval->id)->get() as $bank )
                                        <tr>
                                            <td style="padding-left: 5px" >{!! $bank->name !!}</td>
                                            <td>{!! number_2_format($previous_payroll_approval->getAmountByBankInPayrollRun($previous_payroll_approval->id, $bank->id))  !!}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>
                        </div>


                        <div class="col-md-7" >
                            <legend style="background-color: lightgrey;text-align:center">Current Payroll</legend>
                            <table style="width:100%" class="table table-striped table-bordered">
                                {{--header--}}
                                <tr style="background-color: #eaeaea; font-weight: bold">
                                    <td style="padding-left: 5px" width="160px">Bank Name</td>
                                    <td  height="20px">Amount</td>
                                    <td>Action</td>
                                </tr>
                                @foreach($payroll_run_approval->getBanksInPayrollRunQuery($payroll_run_approval->id)->get() as $bank )
                                    <tr>
                                        <td style="padding-left: 5px" >{!! $bank->name !!}</td>
                                        <td>{!! number_2_format($payroll_run_approval->getAmountByBankInPayrollRun($payroll_run_approval->id, $bank->id))  !!}</td>
                                        @if($payroll_run_approval->payrollRunVouchers()->count() > 0)
                                        <td><a href="{!! route('backend.payroll.pension.run_approval.print_payment_voucher', ['bank' => $bank->id, 'payroll_run_approval' => $payroll_run_approval->id]) !!}" target="_blank" style="color: dodgerblue" class="pull-center underline">Print Pv</a></td>
                                            @else
                                            <td></td>
                                            @endif
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>
