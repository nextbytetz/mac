

{{--sidebar Dependent summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Beneficiary Summary:</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>

    <div class="light_grey_bg">
        <table style="width:100%">
            {{--@if(!$check_if_first_payroll)--}}
                {{--new Pensioners--}}
                <tr>
                    <td style="padding-left: 5px" width="140px">New Pensioners <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="New pensioners to be paid for the first time on
                this payroll"></i>:</td>
                    <td  height="20px"> <b>{!! $data_summary['new_pensioners'] !!}</b></td>
                    <td > <a href="#" data-toggle="modal" data-target="#new_pensioners_modal" id="new_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a> </td>
                </tr>

                {{--new dependenents--}}
                <tr>
                    <td style="padding-left: 5px">New Dependents <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="New dependents to be paid for the first time on
                this payroll"></i>:</td>
                    <td><b>{!! $data_summary['new_dependents'] !!}</b></td>
                    <td><a href="#" data-toggle="modal" data-target="#new_dependents_modal" id="new_dependents_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
                </tr>
            {{--@endif--}}
            {{--Pensioner Terminated from this payroll--}}
            <tr>
                <td style="padding-left: 5px" width="140px">Pensioners Terminated <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Pensioners removed from this
                payroll through termination/deactivation but were active on previous payroll"></i>:</td>
                <td><b>{!! $data_summary['terminated_pensioners'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#terminated_pensioners_modal" id="terminated_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--Dependent Terminated from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Dependents Terminated <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Dependents removed from this
                payroll through termination/deactivation but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['terminated_dependents'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#terminated_dependents_modal" id="terminated_dependents_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>


            {{--Pensioner Suspended from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Pensioners Suspended <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Pensioners removed from this
                payroll through suspension but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['suspended_pensioners'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#suspended_pensioners_modal" id="suspended_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>
            {{--Dependents Suspended from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Dependents Suspended <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Dependent removed from this
                payroll through suspension but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['suspended_dependents'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#suspended_dependents_modal" id="suspended_dependents_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>



        </table>
    </div>
</div>


{{--Modals - Summary data--}}

@include('backend/operation/payroll/run/profile/includes/summary_data/get_new_pensioners')
@include('backend/operation/payroll/run/profile/includes/summary_data/get_new_dependents')
@include('backend/operation/payroll/run/profile/includes/summary_data/get_terminated_pensioners')
@include('backend/operation/payroll/run/profile/includes/summary_data/get_terminated_dependents')
@include('backend/operation/payroll/run/profile/includes/summary_data/get_suspended_pensioners')
@include('backend/operation/payroll/run/profile/includes/summary_data/get_suspended_dependents')

@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush