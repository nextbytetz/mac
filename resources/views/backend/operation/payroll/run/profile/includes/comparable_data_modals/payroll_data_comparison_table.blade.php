


<legend style="background-color: lightgrey;text-align:center">Number of Beneficiary Processed </legend>

<table class="table table-striped table-bordered">
    <tbody>

    <tr>
        <td width="160px">No. of Pensioners</td>
        <th>{!!
                          number_0_format($payroll_run_approval->no_of_pensioners)
                                !!} </th>
    </tr>

    <tr>
        <td>No. of Dependents</td>
        <th>
            {!!    number_0_format($payroll_run_approval->no_of_dependents)
                        !!}</th>
    </tr>



    <tr>
        <td>No. of Suspended pensioners</td>
        <th>
            {!!         number_0_format($payroll_run_approval->no_of_suspended_pensioners)
                        !!}</th>
    </tr>

    <tr>
        <td>No. of Suspended Dependents</td>
        <th>
            {!!    number_0_format($payroll_run_approval->no_of_suspended_dependents)
                        !!}</th>
    </tr>

    <tr>
        <td>No. of New Payees</td>
        <th>
            {!!    number_0_format($payroll_run_approval->new_payees_count)
                        !!}</th>
    </tr>

    </tbody>
</table>

<br/>

<legend style="background-color: lightgrey;text-align:center">Payments Amount Details</legend>
<table class="table table-striped table-bordered">
    <tbody>

    <tr>
        <td width="160px">Total Net Payable</td>
        <th>{!!
                          number_2_format($payroll_run_approval->total_net_amount)
                                !!} </th>
    </tr>

    <tr>
        <td>Total Arrears</td>
        <th>
            {!!         number_2_format($payroll_run_approval->total_arrears_amount)
                        !!}</th>
    </tr>


    <tr>
        <td>Total Deductions</td>
        <th>
            {!!    number_2_format($payroll_run_approval->total_deductions_amount)
                        !!}</th>
    </tr>


    <tr>
        <td>Total Suspended Amount</td>
        <th>
            {!!    number_2_format($payroll_run_approval->total_suspended_amount)
                        !!}</th>
    </tr>



    </tbody>
</table>