
<div class = "row">
    <div class="col-md-12" >
        <br/>
        <table class="display" cellspacing="0" width="100%" id ="dependents-table">
            <thead>
            <tr >
                {{--<th>Sn</th>--}}
                <th>Payroll Month</th>
                <th>Full name</th>
                <th>Bank Details</th>
                <th>Arrears Amount</th>
                <th>Unclaimed Amount</th>
                <th>Deductions Amount</th>
                <th>Months Paid</th>
                <th>Monthly Pension</th>
                <th>Net Payable Amount</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>
        <legend style="background-color: gray;text-align:center; color:whitesmoke;font-weight: bold">{!! 'Total Net Payable Amount: ' !!} <label style="font-size:24px; color:white;">{!!   number_2_format($payroll_run_approval->getTotalNetAmountByMemberType(4)) !!}</label>  </legend>
    </div>

</div>




@push('after-script-end')

    <script  type="text/javascript">
        $("#dependents_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#dependents-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.pension.run_approval.get_mp_payments',['member_type_id' => $member_type_id, 'payroll_run_approval_id' => $payroll_run_approval_id, 'is_constant_care' => 0  ]) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'run_date_formatted', name: 'payroll_procs.run_date', orderable : true, searchable : true},
                        { data: 'dependent_name' , name: 'dependents.firstname', orderable : true, searchable : true},
                        { data: 'bank_details' , name: 'banks.name', orderable : true, searchable : true},
                        { data: 'arrears_amount', name: 'payroll_runs.arrears_amount',  orderable : true, searchable : true},
                        { data: 'unclaimed_amount', name: 'payroll_runs.unclaimed_amount',  orderable : true, searchable : true},
                        { data: 'deductions_amount', name: 'payroll_runs.unclaimed_amount',  orderable : true, searchable : true, visible:true},
                        { data: 'months_paid', name: 'payroll_runs.months_paid' ,orderable : true, searchable : true, visible:true},
                        { data: 'monthly_pension', name: 'payroll_runs.monthly_pension',  orderable : true, searchable : true, visible:true},
                        { data: 'amount', name: 'payroll_runs.amount' ,orderable : true, searchable : true, visible:true},
                        { data: 'status', name: 'status' ,orderable : false, searchable : false, visible:true},

                        { data: 'dependent_name' , name: 'dependents.lastname', orderable : true, searchable : true, visible:false},
                        // { data: 'dependent_name' , name: 'dependents.middlename', orderable : true, searchable : true, visible:false},
                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = url +  "/payroll/beneficiary_profile/"+ aData['member_type_id'] +'/'  + aData['resource_id'] + '/'  + aData['employee_id']   ;
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                });

            });

        });
    </script>;

@endpush
