@extends('layouts.backend.main', ['title' => 'Process Pension Payroll', 'header_title' => 'Process Pension Payroll'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::open(['route' => ['backend.payroll.pension.process'],'method'=>'get',
    'name' => 'create']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('claim_start_date', getClaimStartDate(), ['class' =>'claim_date']) !!}
    <div>&nbsp;</div>

    {{--main contants--}}

    {{--bquarter--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Run Month:</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="form-inline">
                                <span>      {!!  Form::selectMonth('run_month',((isset($next_run_date)) ? $next_run_date->format('m') : Carbon\Carbon::now()->format('m')), ['class' => 'form-control
                                 search-select',
                                'style'=>'width:98px',
                                'placeholder' =>
                         'Month', 'id'=>'run_month', '']) !!}
                        </span>

                                <span>      {!!  Form::selectRange('run_year',Carbon\Carbon::now()->format('Y'),2015,((isset($next_run_date)) ? $next_run_date->format('Y') : Carbon\Carbon::now()->format('Y')), ['class' => 'form-control search-select','style'=>'width:64px',
                        'placeholder' =>
                         'Year', 'id'=>'run_year', 'disabled']) !!}
                        </span>

                            </div>
                            {!! Form::hidden('run_date') !!}
                            {!! $errors->first('run_date', '<span class="help-block label
                            label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



    {{--Buttons--}}
    <div class="row">
        <div class="col-md-4" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.payroll.pension.run_approval.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">

        $(function () {
            $(".search-select").select2();

            $('body').on('submit', 'form[name=create]', function(e) {
                e.preventDefault();
                // Validate date -- run date

                var $month = $('#run_month').val();
                var $year = $('#run_year').val();
                if (($year) && ($month) ) {
                    $('input[name=run_date]').val($year + '-' + $month + '-' + 1);
                }else {
                    $("input[name=run_date]").val("");
                }


                this.submit();

            });





        });


    </script>;


@endpush
