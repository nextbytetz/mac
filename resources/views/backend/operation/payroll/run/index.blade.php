@extends('layouts.backend.main', ['title' => 'Pension Payroll Run Approvals', 'header_title' => 'Pension Payroll Run Approvals'])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12" >
                <div class="pull-right">


                    <div class="btn-group">
                        <a href="{!! route('backend.payroll.pension.run') !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-money"></i>&nbsp;Run New Payroll</a>
                    </div>
                </div>



            </div>
        </div>


    </div>

    <br/>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="payroll-approvals-table">
                <thead>
                <tr >
                    <th>Payroll Month</th>
                    <th>No. of Pensioners</th>
                    <th>No. of Dependents</th>
                    <th>Total Payable AMount</th>
                    <th>Status</th>

                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#payroll-approvals-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.pension.run_approval.get_for_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'run_date_formatted', name: 'run_date_formatted', orderable : false, searchable : false},
                    { data: 'no_of_pensioners' , name: 'no_of_pensioners', orderable : true, searchable : true},
                    { data: 'no_of_dependents', name: 'no_of_dependents',  orderable : true, searchable : true},
                    { data: 'total_net_amount', name: 'total_net_amount',  orderable : true, searchable : true},
                    { data: 'status', name: 'status' ,orderable : false, searchable : false},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/run_approval/"+ "profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
