

{{--sidebar Dependent summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Beneficiary Summary:</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>

    <div class="light_grey_bg">
        <table style="width:100%">
            {{--@if(!$check_if_first_payroll)--}}
                {{--new Pensioners--}}
                <tr>
                    <td style="padding-left: 5px" width="140px">New Pensioners <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="New pensioners to be paid for the first time on
                this payroll"></i>:</td>
                    <td  height="20px"> <b>{!! $data_summary['new_pensioners'] !!}</b></td>
                    <td > <a href="#" data-toggle="modal" data-target="#new_pensioners_modal" id="new_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a> </td>
                </tr>

                {{--new dependenents--}}
                <tr>
                    <td style="padding-left: 5px">New Dependents <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="New dependents to be paid for the first time on
                this payroll"></i>:</td>
                    <td><b>{!! $data_summary['new_dependents'] !!}</b></td>
                    <td><a href="#" data-toggle="modal" data-target="#new_dependents_modal" id="new_dependents_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
                </tr>

            {{--new constant cares--}}
            <tr>
                <td style="padding-left: 5px">New Constant Cares <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="New constant cares assistants to be paid for the first
                time on
                this payroll"></i>:</td>
                <td><b>{!! $data_summary['new_constant_cares'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#new_constant_cares_modal" id="new_constant_cares_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--@endif--}}
            {{--Pensioner Terminated from this payroll--}}
            <tr>
                <td style="padding-left: 5px" width="140px">Pensioners Terminated <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Pensioners removed from this
                payroll through termination/deactivation but were active on previous payroll"></i>:</td>
                <td><b>{!! $data_summary['terminated_pensioners'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#terminated_pensioners_modal" id="terminated_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--Dependent Terminated from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Dependents Terminated <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Dependents removed from this
                payroll through termination/deactivation but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['terminated_dependents'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#terminated_dependents_modal" id="terminated_dependents_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--COnstanr cares Terminated from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Constant Cares Terminated <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Constant cares assistants removed from this
                payroll through termination/deactivation but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['terminated_constant_cares'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#terminated_constant_cares_modal" id="terminated_constant_cares_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--Pensioner Suspended from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Pensioners Suspended <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Pensioners removed from this
                payroll through suspension but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['suspended_pensioners'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#suspended_pensioners_modal" id="suspended_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>
            {{--Dependents Suspended from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Dependents Suspended <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Dependent removed from this
                payroll through suspension but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['suspended_dependents'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#suspended_dependents_modal" id="suspended_dependents_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--Constant cares Suspended from this payroll--}}
            <tr>
                <td style="padding-left: 5px">Constant Cares Suspended <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Constant cares removed from this
                payroll through suspension but were active on previous payroll"></i>:</td>
                <td><b>{!!  $data_summary['suspended_constant_cares'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#suspended_constant_cares_modal" id="suspended_constant_cares_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--Benefciaries with changed bank details on this payroll--}}
            <tr>
                <td style="padding-left: 5px">Beneficiaries (Bank details changed)<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Payroll beneficiaries with new bank details on this payroll"></i>:</td>
                <td><b>{!!  $data_summary['member_with_change_bank_details'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#members_change_bank_modal" id="members_change_bank_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--Benefciaries Arrears--}}
            <tr>
                <td style="padding-left: 5px">Existing Beneficiaries With Arrears<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Existing Payroll beneficiaries with arrears on this payroll but not new payees"></i>:</td>
                <td><b>{!!  $data_summary['member_with_arrears'] !!}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#members_with_arrears_modal" id="members_with_arrears_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>

            {{--Children overage with no extension--}}
            <tr>
                <td style="padding-left: 5px">Children Overage Not Granted Extension<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Children already overage without approved extension"></i>:</td>
                <td><b>{{--{!!  $data_summary['children_overage_no_extension'] !!}--}}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#child_overage_no_extension_modal" id="child_overage_no_extension_link"   style="color: dodgerblue" class="pull-center underline">Click to View </a></td>
            </tr>

            {{--Other dependents in eligible--}}
            <tr>
                <td style="padding-left: 5px">Other dependents not eligible (Completed their cycles)<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Other dependents e.g. Parents who have finished their 24 months but still on payroll wrongly."></i>:</td>
                <td><b>{{--{!!  $data_summary['otherdependents_not_eligible'] !!}--}}</b></td>
                <td><a href="#" data-toggle="modal" data-target="#otherdependents_not_eligible_modal" id="otherdependents_not_eligible_link"   style="color: dodgerblue" class="pull-center underline">Click to View </a></td>
            </tr>
        </table>
    </div>
</div>


{{--Modals - Summary data--}}

@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_new_pensioners')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_new_dependents')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_new_constant_cares')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_terminated_pensioners')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_terminated_dependents')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_terminated_constant_cares')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_suspended_pensioners')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_suspended_dependents')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_suspended_constant_cares')
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_members_with_changed_bank_details')

{{--Beneficiaries with arrears modal--}}
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_members_general_modal_for_dt', ['modal_link' => 'members_with_arrears_link' , 'modal_id' => 'members_with_arrears_modal', 'modal_title' => 'Beneficiaries With Arrears', 'table_name' => 'beneficiaries-arrears-table', 'route' => route('backend.payroll.pension.run_approval.get_payees_with_arrears_dt', $payroll_run_approval->id)])

{{--Children overage no extension--}}
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_members_general_modal_for_dt', ['modal_link' => 'child_overage_no_extension_link' , 'modal_id' => 'child_overage_no_extension_modal', 'modal_title' => 'Children Overage Not Granted Extension', 'table_name' => 'child-overage-no-ext-table', 'route' => route('backend.payroll.pension.run_approval.get_child_overage_no_extension_dt', $payroll_run_approval->id)])

{{--Other dep not eligible--}}
@include('backend/operation/payroll/run/profile_new/includes/summary_data/get_members_general_modal_for_dt', ['modal_link' => 'otherdependents_not_eligible_link' , 'modal_id' => 'otherdependents_not_eligible_modal', 'modal_title' => 'Other dependents not Eligible', 'table_name' => 'otherdependents-not-eligible-table', 'route' => route('backend.payroll.pension.run_approval.get_otherdep_not_eligible_dt', $payroll_run_approval->id)])

@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush