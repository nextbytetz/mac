

{{--Pending tasks table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Pending Tasks <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Pending tasks to be completed at this stage"></i></span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>

    <div class="light_grey_bg">
        <table style="width:100%">
            {{--members for supplier id--}}
            @if($check_finance_level == 1 && $payroll_run_approval->ispayprocessed == 0 && $data_summary['members_for_erp_supplier_id'])
            <tr>
                <td style="padding-left: 5px" width="140px">Members for ERP Supplier id <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Members need to be created in Erp befor process payment"></i>:</td>
                <td  height="20px"></td>
                <td > <a href="#" data-toggle="modal" data-target="#members_for_supplier_id_modal" id="members_for_supplier_id_link"   style="color: dodgerblue" class="pull-center underline"> View </a> </td>
            </tr>

@endif





        </table>
    </div>
</div>


{{--Modals - Summary data--}}

@include('backend/operation/payroll/run/profile_new/includes/pending_tasks/get_members_with_no_supplier_id')


@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush