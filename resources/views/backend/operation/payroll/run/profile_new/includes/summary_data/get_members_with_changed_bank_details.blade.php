
<div class="modal fade top-modal" id="members_change_bank_modal" tabindex="-1" role="dialog" aria-labelledby="largemodal" style="display: none;width:100%" aria-hidden="true">

    <div class="modal-dialog modal-lg" style=" background-color: white" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Beneficiaries with changed Bank details</h4>
            </div>
            <div class="modal-body">

                <div class = "row">
                    <div class="col-md-12" >
                        <br/>
                        <table class="display" cellspacing="0" width="100%" id ="members-changed-bank-table">
                            <thead>
                            <tr >
                                {{--<th>Sn</th>--}}
                                <th>Member Type</th>
                                <th>Beneficiary</th>
                                <th>Current Bank Details</th>
                                <th>Previour Bank Details</th>
                                <th>Relation</th>

                            </tr>
                            </thead>
                        </table>
                        {{--<legend style="background-color: lightgrey;text-align:center">{!! 'Total Net Payable Amount: ' . number_2_format($payroll_run_approval->getTotalNetAmountByMemberType(5)) !!}</legend>--}}
                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>


@push('after-script-end')

<script  type="text/javascript">
    $("#members_change_bank_link").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#members-changed-bank-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.pension.run_approval.get_members_with_changed_bank',['payroll_run_approval_id' => $payroll_run_approval->id]) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'member_type_name' , name: 'pc.member_type_name', orderable : true, searchable : true},
                    { data: 'member_name', name: 'pc.member_name',  orderable : true, searchable : true},
                    { data: 'bank_details_current', name: 'bank_details_current',  orderable : false, searchable : false},
                    { data: 'bank_details_previous', name: 'bank_details_current',  orderable : false, searchable : false},
                    { data: 'relationship', name: 'pc.relationship',  orderable : false, searchable : false},
                           ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/beneficiary_profile/"+ aData['member_type_id'] +'/'  + aData['resource_id'] + '/'  + aData['employee_id']   ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });

    });
</script>;

@endpush
