
<div class="modal fade top-modal" id="new_constant_cares_modal" tabindex="-1" role="dialog" aria-labelledby="largemodal" style="display: none;width:100%" aria-hidden="true">

    <div class="modal-dialog modal-lg" style=" background-color: white" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">New Constant Care Assistants</h4>
            </div>
            <div class="modal-body">

                <div class = "row">
                    <div class="col-md-12" >
                        <br/>
                        <table class="display" cellspacing="0" width="100%" id ="new-cca-table">
                            <thead>
                            <tr >
                                {{--<th>Sn</th>--}}
                                <th>Full name</th>
                                <th>Arrears Amount</th>
                                <th>Unclaimed Amount</th>
                                <th>Deductions Amount</th>
                                <th>Months Paid</th>
                                <th>Monthly Pension</th>
                                <th>Net Payable Amount</th>

                            </tr>
                            </thead>
                        </table>
                        {{--<legend style="background-color: lightgrey;text-align:center">{!! 'Total Net Payable Amount: ' . number_2_format($payroll_run_approval->getTotalNetAmountByMemberType(5)) !!}</legend>--}}
                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>


@push('after-script-end')

    <script  type="text/javascript">
        $("#new_constant_cares_link").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#new-cca-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.payroll.pension.run_approval.get_new_payees_for_datatable',['member_type_id' => 4, 'payroll_run_approval_id' => $payroll_run_approval->id, 'is_constant_care' => 1 ]) !!}',
                        type : 'post'
                    },
                    columns: [

                        { data: 'dependent_name' , name: 'dependents.firstname', orderable : true, searchable : true},
                                             { data: 'arrears_amount', name: 'payroll_runs.arrears_amount',  orderable : true, searchable : true},
                        { data: 'unclaimed_amount', name: 'payroll_runs.unclaimed_amount',  orderable : true, searchable : true},
                        { data: 'deductions_amount', name: 'payroll_runs.unclaimed_amount',  orderable : true, searchable : true},
                        { data: 'months_paid', name: 'payroll_runs.months_paid' ,orderable : true, searchable : true},
                        { data: 'monthly_pension', name: 'payroll_runs.monthly_pension',  orderable : true, searchable : true},
                        { data: 'amount', name: 'payroll_runs.amount' ,orderable : true, searchable : true},

                        { data: 'dependent_name' , name: 'dependents.lastname', orderable : true, searchable : true, visible:false},
                        { data: 'dependent_name' , name: 'dependents.middlename', orderable : true, searchable : true, visible:false},
                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = url +  "/payroll/beneficiary_profile/"+ aData['member_type_id'] +'/'  + aData['resource_id'] + '/'  + aData['employee_id']   ;
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                });

            });

        });
    </script>;

@endpush
