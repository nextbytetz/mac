

        <legend style="background-color: lightgrey;text-align:center">Number of Beneficiary Processed </legend>
        <table class="table table-striped table-bordered">
            <tbody>

            <tr>
                <td width="160px">No. of Pensioners</td>
                <th>{!!
                          number_0_format($payroll_run_approval->no_of_pensioners)
                                !!} </th>
            </tr>

            <tr>
                <td>No. of Dependents</td>
                <th>
                    {!!    number_0_format($payroll_run_approval->no_of_dependents)
                                !!}</th>
            </tr>


            <tr>
                <td>No. of Constant Cares</td>
                <th>
                    {!!    number_0_format($payroll_run_approval->no_of_constant_cares)
                                !!}</th>
            </tr>


            <tr>
                <td>No. of Suspended pensioners</td>
                <th>
                    {!!         number_0_format($payroll_run_approval->no_of_suspended_pensioners)
                                !!}</th>
            </tr>

            <tr>
                <td>No. of Suspended Dependents</td>
                         <th>
                    {!!    number_0_format($payroll_run_approval->no_of_suspended_dependents)
                                !!}</th>
            </tr>

            <tr>
                <td>No. of Suspended Constant Cares</td>
                <th>
                    {!!    number_0_format($payroll_run_approval->no_of_suspended_constant_cares)
                                !!}</th>
            </tr>



            </tbody>
        </table>



