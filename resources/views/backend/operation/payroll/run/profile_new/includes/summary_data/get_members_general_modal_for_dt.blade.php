
<div class="modal fade top-modal" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-labelledby="largemodal" style="display: none;width:100%" aria-hidden="true">

    <div class="modal-dialog modal-lg" style=" background-color: white" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">{{ $modal_title }}</h4>
            </div>
            <div class="modal-body">

                <div class = "row">
                    <div class="col-md-12" >
                        <br/>
                        <table class="display" cellspacing="0" width="100%" id ="{{ $table_name }}">
                            <thead>
                            <tr >
                                {{--<th>Sn</th>--}}
                                <th>Member Type</th>
                                <th>Beneficiary</th>
                                <th>Relation</th>
                                <th>Arrears</th>
                                <th>Unclaimed</th>
                                <th>Deduction</th>
                                <th>Months Paid</th>
                                <th>MP</th>
                                <th>Net Payable</th>

                            </tr>
                            </thead>
                        </table>
                        {{--<legend style="background-color: lightgrey;text-align:center">{!! 'Total Net Payable Amount: ' . number_2_format($payroll_run_approval->getTotalNetAmountByMemberType(5)) !!}</legend>--}}
                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>


@push('after-script-end')

    <script  type="text/javascript">
        $("#"+ '{{ $modal_link }}').one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#' + '{{ $table_name }}').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! $route !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'member_type_name' , name: 'member_type_name', orderable : true, searchable : false},
                        { data: 'member_name', name: 'b.member_name',  orderable : true, searchable : true},
                        { data: 'relationship', name: 'b.relationship',  orderable : false, searchable : false},
                        { data: 'arrears_amount', name: 'payroll_runs.arrears_amount',  orderable : false, searchable : false},
                        { data: 'unclaimed_amount', name: 'payroll_runs.unclaimed_amount',  orderable : false, searchable : false},
                        { data: 'deductions_amount', name: 'payroll_runs.deductions_amount',  orderable : false, searchable : false},
                        { data: 'months_paid', name: 'payroll_runs.months_paid',  orderable : false, searchable : false},
                        { data: 'monthly_pension', name: 'payroll_runs.monthly_pension',  orderable : false, searchable : false},
                        { data: 'amount', name: 'payroll_runs.amount',  orderable : false, searchable : false},
                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = url +  "/payroll/beneficiary_profile/"+ aData['member_type_id'] +'/'  + aData['resource_id'] + '/'  + aData['employee_id']   ;
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                });

            });

        });
    </script>;

@endpush
