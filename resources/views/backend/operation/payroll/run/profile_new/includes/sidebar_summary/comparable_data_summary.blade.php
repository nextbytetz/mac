

{{--sidebar Dependent summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Comparable Data Summary <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Compare Data from previous payroll"></i></span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>

    <div class="light_grey_bg">
        <table style="width:100%">
            {{--new Pensioners--}}
            <tr>
                <td style="padding-left: 5px" width="140px">Bank Payments <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Bank payments allocations"></i>:</td>
                <td  height="20px"></td>
                <td > <a href="#" data-toggle="modal" data-target="#bank_allocations_modal" id="bank_allocations_link"   style="color: dodgerblue" class="pull-center underline"> View </a> </td>
            </tr>

            {{--new dependenents--}}
            <tr>
                <td style="padding-left: 5px">Payroll comparison from previous <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Compare current monthly pension
                payroll to previous monthly pension payroll"></i>:</td>
                <td></td>
                <td><a href="#" data-toggle="modal" data-target="#payroll_data_modal" id="payroll_data_link"   style="color: dodgerblue" class="pull-center underline"> View </a></td>
            </tr>






        </table>
    </div>
</div>


{{--Modals - Summary data--}}

@include('backend/operation/payroll/run/profile_new/includes/comparable_data_modals/bank_allocations')
@include('backend/operation/payroll/run/profile_new/includes/comparable_data_modals/payroll_data')


@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush