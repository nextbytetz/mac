
<div class="modal fade top-modal" id="payroll_data_modal" tabindex="-1" role="dialog" aria-labelledby="largemodal" style="display: none;width:100%" aria-hidden="true">

    <div class="modal-dialog modal-lg" style=" background-color: white" >

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Payroll Data Comparison from Previous Payroll</h4>
            </div>
            <div class="modal-body">

                <div class = "row">
                    <div class="col-md-12" >
                        <br/>
                        <div class="col-md-5" >
                            <legend style="background-color: lightgrey;text-align:center">Previous Payroll</legend>
                            <br/>
                            @if(isset($previous_payroll_approval))
                                @include('backend/operation/payroll/run/profile_new/includes/comparable_data_modals/payroll_data_comparison_table', ['payroll_run_approval' =>
                                $previous_payroll_approval])


                            @endif


                        </div>


                        <div class="col-md-7" >
                            <legend style="background-color: lightgrey;text-align:center">Current Payroll</legend>
               <br/>
                            @include('backend/operation/payroll/run/profile_new/includes/comparable_data_modals/payroll_data_comparison_table', ['payroll_run_approval' => $payroll_run_approval])
                        </div>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            </div>
        </div>
    </div>
</div>
