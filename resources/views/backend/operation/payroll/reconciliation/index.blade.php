@extends('layouts.backend.main', ['title' => 'Payroll Reconciliations', 'header_title' => 'Payroll Reconciliations'])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12" >
                <div class="pull-right">


                    <div class="btn-group">
                        <a href="{!! route('backend.payroll.reconciliation.process') !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-exchange"></i>&nbsp;Process Reconciliation</a>
                    </div>
                </div>



            </div>
        </div>


    </div>

    <br/>
@include('backend/operation/payroll/reconciliation/includes/get_all_for_datatable')

@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {


        });


    </script>;

@endpush
