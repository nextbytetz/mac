

{{--General info--}}
<br/>
<div class="row">
    {{--left general  summary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Payroll Reconciliation Details</legend>
            <table class="table table-striped table-bordered">
                <tbody>
                {{--status--}}
                <tr>
                    <td>Status</td>
                    <th>
                        {!! $payroll_reconciliation->status_label
                                    !!}</th>
                </tr>
                {{--Amount--}}
                <tr>
                    <td width="120px">Amount</td>
                    <th>{!! Form::label( 'amount',
                           number_2_format($payroll_reconciliation->amount)) !!} </th>
                </tr>

                {{--type--}}
                <tr>
                    <td>Type</td>
                    <th>
                        {!!   $payroll_reconciliation->type_label
                                    !!}</th>
                </tr>

                {{--remark--}}
                <tr>
                    <td>Remark</td>
                    <th>
                        {!! $payroll_reconciliation->remark
                                    !!}</th>
                </tr>


                </tbody>
            </table>

        </div>
    </div>




    {{--Right summary--}}

    <div class="col-md-6">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">General Details</legend>
            <table class="table table-striped table-bordered">
                <tbody>

                {{--mp--}}
                <tr>
                    <td>Monthly Pension</td>
                    <th>
                        {!! number_2_format($mp)
                        !!}</th>
                </tr>

                {{--bank--}}
                <tr>
                    <td width="120px">Bank</td>
                    <th>{!! Form::label( 'bank',
                           ($resource->bank_branch_id) ? $resource->BankBranch->bank->name : ' ', [])
                                !!} </th>
                </tr>

                {{--Branch--}}
                <tr>
                    <td>Branch</td>
                    <th>
                        {!!   isset($resource->bank_branch_id) ? $resource->BankBranch->name : ' '
                                    !!}</th>
                </tr>

                {{--Accountno--}}
                <tr>
                    <td>Account No.</td>
                    <th>
                        {!! $resource->accountno
                                    !!}</th>
                </tr>




                </tbody>
            </table>

        </div>
    </div>



</div>



