@extends('layouts.backend.main', ['title' =>   'Payroll Reconciliation Profile', 'header_title' => 'Payroll Reconciliation Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>

        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row ">

        @include("backend/operation/payroll/pension_administration/includes/header_info",['resource'=> $resource, 'member_type_id' => $member_type_id])

    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >

                                        @if($check_pending_level1 == 1 || $check_workflow == 0)



                                            @if($payroll_reconciliation->status == 0)
                                                <span>
                                           {!! HTML::decode(link_to('#', "<i class='icon fa fa-confirm'></i>&nbsp;Initiate", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Payroll Approval Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $payroll_reconciliation->id, 'data-route' => route('backend.payroll.reconciliation.initiate', $payroll_reconciliation->id)])) !!}
                                            </span>
                                            {{--<span>--}}
                                                {{--{!! HTML::decode(link_to('#', "<i class=\"icon fa fa-confirm\"></i>&nbsp;Initiate", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Payroll Approval Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $payroll_reconciliation->id, 'data-route' => route('backend.payroll.reconciliation.initiate', $payroll_reconciliation->id)])) !!}--}}
                                                {{--</span>--}}
                                            @endif




                                            {{--<span>--}}
                                        {{--<a href="{!! route('backend.payroll.recovery.edit',$payroll_reconciliation->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;--}}
                                            {{--Edit--}}
                                            {{--</a>--}}
                                     {{--</span>--}}




                                            <span>

                                           {!! HTML::decode(link_to_route('backend.payroll.reconciliation.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $payroll_reconciliation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this payroll reconciliation?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                        @endif

                                            <span>
                                        <a href="{!! route('backend.payroll.reconciliation.index') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;
                                            Close</a>
                                     </span>

                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--general info--}}
                                    @include('backend/operation/payroll/reconciliation/profile/includes/general_info')
                                    {{--Remark--}}

                                    <div>&nbsp;</div>

                                    {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                                           @include('backend/operation/payroll/pension_administration/profile/includes/pension_summary_table', ['firstpay_flag' => 1, 'status_label' => (($member_type_id == 4 ) ? $resource->getMpPaymentStatusLabel( $employee_id) : $resource->mp_payment_status_label), 'last_payroll_run_date' => $last_payroll_run_date, 'mp_payment_summary' => $mp_payment_summary])
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
    @include("backend.system.workflow.includes.initiate_modal")
    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {

        });
    </script>;

@endpush
