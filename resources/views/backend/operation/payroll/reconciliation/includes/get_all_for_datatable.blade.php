{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="payroll-reconciliations-table">
            <thead>
            <tr >
                <th>Beneficiary</th>
                <th>Member Type</th>
                <th>Filename</th>
                <th>Remark</th>
                <th>Amount</th>
                <th>Type</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#payroll-reconciliations-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.payroll.reconciliation.get_all_for_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'beneficiary', name: 'pensioners.firstname', orderable : true, searchable : true},
                    { data: 'member_type_name' , name: 'member_types.name', orderable : true, searchable : true},
                    { data: 'filename' , name: 'notification_reports.filename', orderable : true, searchable : true},
                    { data: 'remark', name: 'remark',  orderable : true, searchable : true},
                    { data: 'amount_formatted', name: 'payroll_reconciliations.amount',  orderable : true, searchable : true},
                    { data: 'type', name: 'type' ,orderable : false, searchable : false},
                    { data: 'status', name: 'status' ,orderable : false, searchable : false},
                    { data: 'beneficiary', name: 'pensioners.lastname', orderable : false, searchable : false , visible:false},
                    { data: 'beneficiary', name: 'dependents.firtsname', orderable : false, searchable : false , visible:false},
                    { data: 'beneficiary', name: 'dependents.lastname', orderable : false, searchable : false , visible:false},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/payroll/reconciliation/"+ "profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush

