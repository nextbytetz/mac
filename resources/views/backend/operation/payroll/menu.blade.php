@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.payroll_administration.index'), 'header_title' => trans('labels.backend.claim.payroll_administration.index')])


@section('content')

    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--register notification--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.pensioner.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-user"></i><large>&nbsp;&nbsp;Pension Administration</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage pensioners, deceased and dependants monthly pensions</p> </li>
                    </a>

                </ul>
                <br/>


                {{--Verification--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.payroll.verification.index", 5) !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-check-square" aria-hidden="true"></i><large>&nbsp;&nbsp;Verification</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Verify payroll beneficiaries</p> </li>
                    </a>
                </ul>
                <br/>




                {{--Payroll data migration--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.payroll.data_migration.menu") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-retweet" aria-hidden="true"></i><large>&nbsp;&nbsp;Payroll Data Migration</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Synchronize manually processed files with system </p> </li>
                    </a>

                </ul>
                <br/>



            </div>
        </div>

        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--Payroll process--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.pension.run_approval.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-ticket"> </i><large>&nbsp;&nbsp;@lang('labels.backend.claim_menu.run_monthly_pension_payroll')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.claim_menu.run_monthly_pension_payroll_summary')</p> </li>
                    </a>

                </ul>



                <br/>

                {{--Payroll Reconciliations--}}
                @if(env('TESTING_MODE') == 1)
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.payroll.reconciliation.index') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exchange"> </i><large>&nbsp;&nbsp;Pension Payroll
                                        Reconciliation Alerts</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Process monthly pension reconciliation for payroll beneficiaries</p> </li>
                        </a>

                    </ul>
                    <br/>
                @endif

                {{--Suspension--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.payroll.suspension.index") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-pause" aria-hidden="true"></i><large>&nbsp;&nbsp;Manual Suspension</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Process bulk suspension after beneficiaries verification</p> </li>
                    </a>

                </ul>
                <br/>


                {{--Payroll ALert Monitor--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.payroll.alert_monitor.index',1) !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-bell"> </i><large>&nbsp;&nbsp;Payroll Alert Monitor</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage all alerts in payroll which require further action.</p> </li>
                    </a>

                </ul>
                <br/>

            </div>
        </div>


    </div>


@stop

@push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             $("#site-header-title").hide();
             */
            $("#preview").click(function() {

            });
        });
    </script>;

@endpush