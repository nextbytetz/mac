@extends('layouts.backend.main', ['title' => 'Investigation Plan', 'header_title' => 'Investigation Plans'])

@push('after-styles-end')

@endpush

@include('backend.includes.datatable_assets')

@section('content')

    @include("backend/operation/claim/investigation_plan/includes/menu")

    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%'], true) !!}
        </div>
    </div>

@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
@endpush
