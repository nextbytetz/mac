@extends('layouts.backend.main', ['title' => "Notification Files", 'header_title' => "Notification Files"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .custom_filter {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
    @endpush

    @section('content')

{{die}}


    <div class="row">
        @include("backend/operation/claim/investigation_plan/includes/header_info")
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="custom_filter">
                                    {!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                {{--Employer Name--}}
                                                <div class="form-group col-md-6 allocation_select">
                                                    <label for="employer">Employer Name</label>
                                                    {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                                                </div>
                                                {{--Region Selection--}}
                                                <div class="form-group col-md-4">
                                                    <label for="region">Employer Region:</label>
                                                    {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                                                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                                                </div>
                                                {{--Resource Status--}}
                                                <div class="form-group col-md-6 allocation_select">
                                                    <label for="status">Status:</label>
                                                    {!! Form::select('status', ['0' => 'All', '1' => 'Allocated', '2' => 'Unallocated', '3' => 'Allocated to User', '4' => 'Attended (Allocated)', '5' => 'Unattended (Allocated)'], null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                                                </div>
                                                {{--Allocated User--}}
                                                <div class="form-group col-md-4 allocated_user_select">
                                                    <label for="allocated_user">User:</label>
                                                    {!! Form::select('user_id', $users, null, ['class' => 'form-control search-select', 'id' => 'allocated_user']) !!}
                                                </div>

                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                                                    <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <legend></legend>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="display" id = "resource-allocation-table" width="100%">
               <thead>
                <tr >
                    <th></th>
                    <th>Case No</th>
                    <th>Incident Type</th>
                    <th>Employee</th>
                    <th>Employer</th>
                    <th>Allocated Staff</th>
                    <th>Attended</th>
                    <th>Stage</th>
                    <th>Region</th>
                    <th>Notification Date</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
<script>
    $(function () {
        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });
        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    let $districtId = $('#district');
                    $districtId.empty();
                    $districtId.select2("val", "");
                    $districtId.html(data);
                    $("#spin2").hide();
                });
            }
        });
        let $statusId = $("#status");
        $statusId.on("change", function () {
            var $status = $(this).val();
            let $allocatedUserSelectClass = $(".allocated_user_select");
            switch($status) {
                case '3':
                /* Selected to choose User Allocated */
                $allocatedUserSelectClass.show();
                break;
                default:
                $allocatedUserSelectClass.hide();
                break;
            }
        });
        $statusId.trigger('change');
        let $assign_status_contrl = $('#assign_status');
        $assign_status_contrl.on("change", function () {
            let $assign_status = $(this).val();
            let $assignUserSelectClass = $(".assign_user_select");
            switch($assign_status) {
                case '1':
                $assignUserSelectClass.show();
                break;
                default:
                $assignUserSelectClass.hide();
                break;
            }
        });
        $assign_status_contrl.trigger('change');
        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                {{-- url: "{!! route('backend.claim.investigation_plan.assign_investigators') !!}", --}}
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $(".search-select").val(null).trigger('change.select2');
        });
        $('#resource-allocation-table').DataTable({});
        var $oTable = $('#resource-allocation1-table').DataTable({
            dom : 'Bfrtip',
            buttons : ['reload', 'colvis','export'],
            initComplete : function () {
                $oTable.buttons().container().insertBefore('#resource-allocation-table');
            },
            drawCallback : function () {
                getAllocationInspectionTypeGroup();
                getAllocationStaffGroup();
            },
            processing: true,
            serverSide: true,
            info : true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: "{!! route("backend.claim.investigations.notification_files") !!}",
                type : "GET",
                data: function ($d) {
                    $d.employee = $('select[name=employee]').val();
                    $d.employer = $('select[name=employer]').val();
                    $d.status = $('select[name=status]').val();
                    $d.user_id = $('select[name=user_id]').val();
                    $d.region = $('select[name=region]').val();
                    $d.district = $('select[name=district]').val();
                    $d.stage = $('select[name=stage]').val();
                }
            },
            columnDefs: [
            {
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }
            ],
            select: {
                'style': 'multi'
            },
            columns: [
            {   
                orderable: false,
                searchable: false,
                data: 'id',
                name : 'employer_inspection_task.id'
            },
            {data: 'filename', name: 'filename.name', visible: true},
            {data: 'incident_type', name: 'a.name', visible: true},
            {data: 'employee', name: 'employees.name', searchable: true, orderable: true, visible:true},
            {data: 'employer', name: 'employers.name', searchable: true, orderable: true},
            {data: 'allocated_user', name: 'users.firstname'},
            {data: 'user_attended', name: 'employer_inspection_task.attended', searchable: false, orderable: true, visible: true},
            {data: 'stage', name: 'b.name', visible: true},
            {data: 'region', name: 'regions.name', searchable: true, orderable: true, visible: false},
            {data: 'notification_date', name: 'incident_date.name', searchable: true, orderable: true, visible: false},
                    // {data: 'created_at', name: 'employer_inspection_task.created_at', searchable: false, orderable: true, visible: false},
                    ],
                    'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                        $('td:not(:first-child)', $nRow).click(function() {
                            window.open(base_url + "/claim/investigations/show/" + $aData['id'], "_self");
                        }).hover(function() {
                            $(this).css('cursor', 'alias');
                        }, function() {
                            $(this).css('cursor', 'auto');
                        });
                    }
                });
        $('#search-form').on('submit', function($e) {
            $oTable.draw();
            $e.preventDefault();
        });
        @if ($can_assign)
        $('#assign-user').on('submit', function($e) {
            $e.preventDefault();
            var $form = this;
            var $rowsSelected = $oTable.column(0).checkboxes.selected();
                //Remove all previous selected
                $($form).find("input[name='id[]']").remove();
                // Iterate over all selected checkboxes
                $.each($rowsSelected, function($index, $rowId) {
                    // Create a hidden element
                    $($form).append($('<input>').attr('type', 'hidden').attr('name', 'id[]').val($rowId));
                });
                swal({
                    title: "Warning",
                    text: "Are you sure to allocate selected files to the selected assigned user",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function ($confirmed) {
                    if ($confirmed) {
                        //$form.submit();
                        //Do the ajax submission ...
                        //route : backend.claim.notification_report.allocation.assign
                        var $options = {
                            dataType : "json",
                            data : {},
                            type : "PUT",
                            url : $($form).attr("action"),
                            success : function (data) {
                                if (data.success) {
                                    $oTable.draw();
                                    $oTable.column(0).checkboxes.deselectAll();
                                    $.amaran({
                                        'theme'     :'awesome success',
                                        'content'   :{
                                            title : "Success",
                                            message: data.message,
                                            info:'',
                                            icon: 'fa fa-check-square-o'
                                        },
                                        'position'  :'bottom left',
                                        'outEffect' :'slideBottom',
                                        'inEffect'  :'slideLeft'
                                    });
                                } else {
                                    alert(data.message);
                                    /*swal({ title : "Error Assigning User to Resource(s)", text : data.message});*/
                                }
                            },
                            error: function (data) {

                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
        @endauth
    });
</script>
@endpush
