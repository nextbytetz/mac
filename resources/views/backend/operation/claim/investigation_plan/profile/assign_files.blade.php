@extends('layouts.backend.main', ['title' => "Assign Case To Investigators", 'header_title' => "Assign Case To Investigators"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";

        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
    .dataTables_wrapper .dataTables_length select {
        padding-right: 10px;
    }
</style>
@endpush

@section('content')
<div class="row">
    @include("backend/operation/claim/investigation_plan/includes/header_info")
</div>
<div class="row">
    <div class="col-md-12">
        <div id="bottom_tab-1" >
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="custom_filter">
                                {!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-row">
                                            <div class="row">
                                                <div class="form-group col-md-5 allocation_select">
                                                    <label for="employer">Employer Name</label>
                                                    {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                                                </div>
                                                <div class="form-group col-md-3 allocation_select">
                                                    <label for="status">Status:</label>
                                                    {!! Form::select('status', ['0' => 'All', '1' => 'Assigned', '2' => 'Unassigned', '3' => 'Assigned to User'], null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                                                </div>
                                                <div class="form-group col-md-3 allocated_user_select">
                                                    <label for="allocated_user">Assigned User:</label>
                                                    {!! Form::select('user_id', $plan_investigators, null, ['class' => 'form-control search-select', 'id' => 'allocated_user']) !!}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-5">
                                                    <label for="region">Employer Region:</label>
                                                    {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                                                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                                                </div>
                                                <div class="form-group col-md-3 allocation_select">
                                                    <label for="district">Incident District:</label>
                                                    {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                                                </div>
                                                <div class="form-group col-md-3 allocation_select">
                                                    <label for="incident">Incident Type</label>
                                                    {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                                                <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<legend></legend>
<br/>

<div class="row">
    <div class="col-md-12">
        <div class="form-row">
            {!! Form::model($profile, ['route' => ['backend.claim.investigations.assign_files', $profile->id], 'name' => 'update_investigation_plan', 'method' => 'POST', 'id' => 'assign-user','role' => 'form']) !!}
            {{ method_field('PUT') }}
            {!! Form::hidden('plan_id', $profile->id) !!}
            <div class="form-group offset-md-6 col-md-4">

                <span class="assign_user_select">
                    <label for="assigned_user"><b>Assign To:</b></label>
                    {!! Form::select('assigned_user', $plan_investigators, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
                    <span class="help-block label label-danger error_field assigned_user_error hidden"></span>
                    <span class="help-block label label-danger error_field id_error hidden"></span>
                </span>
            </div>
            <div class="form-group col-md-2">
                <label for="allocate_submit">&nbsp;</label>
                <input type="submit" class="form-control btn btn-success btn-sm" value="Assign Files" id="allocate_submit">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--Resource Datatable--}}
<div class="row">
    <div class="col-md-12">
        <table class="display" id = "files-allocation-table" width="100%">
            <thead>
                <tr >
                    <th></th>
                    <th>Case No</th>
                    <th>Incident Type</th>
                    <th>Employee</th>
                    <th>Employer</th>
                    <th>Incident Date</th>
                    <th>Reporting Date</th>
                    <th>Receipt Date</th>
                    <th>Registration Date</th>
                    <th>District</th>
                    <th>Region</th>
                    <th>Assigned Investigator</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
<script>
    $(function () {
        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });
        $('#region').on('change', function (e) {
            let region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    let $districtId = $('#district');
                    $districtId.empty();
                    $districtId.select2("val", "");
                    $districtId.html(data);
                    $("#spin2").hide();
                });
            }
        });
        let $statusId = $("#status");
        $statusId.on("change", function () {
            let $status = $(this).val();
            let $allocatedUserSelectClass = $(".allocated_user_select");
            switch($status) {
                case '3':
                $allocatedUserSelectClass.show();
                break;
                default:
                $allocatedUserSelectClass.hide();
                break;
            }
        });
        $statusId.trigger('change');
        let $assign_status_contrl = $('#assign_status');
        $assign_status_contrl.on("change", function () {
            let $assign_status = $(this).val();
            let $assignUserSelectClass = $(".assign_user_select");
            switch($assign_status) {
                case '1':
                $assignUserSelectClass.show();
                break;
                default:
                $assignUserSelectClass.hide();
                break;
            }
        });
        $assign_status_contrl.trigger('change');
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
             url: "{!! route('backend.compliance.employers') !!}",
             dataType: 'json',
             delay: 250,
             data: function (params) {
                return {
                    q: params.term || "",
                    page: params.page || 1
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data.items, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    }),
                    pagination: {
                        more: true
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    }).on("select2:selecting", function($e) {
        let $selected = $e.params.args.data.id;
    });
    $( "#clear_filter" ).click(function() {
        $(".employer-select").val(null).trigger('change.select2');
        $(".employee-select").val(null).trigger('change.select2');
        $(".search-select").val(null).trigger('change.select2');
    });
    let $oTable = $('#files-allocation-table').DataTable({
        // dom : 'Blfrtip',
        dom: "<'row mb-1'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons : ['colvis', 'excel', 'csv'],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        initComplete : function () {
            $oTable.buttons().container().insertBefore('#files-allocation-table');
        },
        drawCallback : function () {
            // getAllocationInspectionTypeGroup();
            // getAllocationStaffGroup();
        },
        processing: true,
        serverSide: true,
        info : true,
        stateSaveCallback: function (settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
        },
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
        },
        ajax: {
            url: "{!! route("backend.claim.investigations.get_files_allocation",$profile->id) !!}",
            method : "GET",
            data: function ($d) {
                $d.employee = $('select[name=employee]').val();
                $d.employer = $('select[name=employer]').val();
                $d.status = $('select[name=status]').val();
                $d.incident = $('select[name=incident]').val();
                $d.user_id = $('select[name=user_id]').val();
                $d.region = $('select[name=region]').val();
                $d.district = $('select[name=district]').val();
                $d.stage = $('select[name=stage]').val();
            }
        },
        columnDefs: [
        {
            'targets': 0,
            'checkboxes': {
                'selectRow': true
            }
        }
        ],
        select: {
            'style': 'multi'
        },
        columns: [
        {   
            orderable: false,
            searchable: false,
            data: 'id',
            name : 'notification_reports.filename'
        },
        {data: 'filename', name: 'notification_reports.filename', visible: true},
        {data: 'incident', name: 'incident_types.name'},
        {data: 'employee', name: 'employees.firstname'},
        {data: 'employer', name: 'employers.name'},
        {data: 'incident_date' , name: 'notification_reports.incident_date', orderable : true, searchable : true},
        {data: 'reporting_date', name: 'notification_reports.reporting_date'},
        {data: 'receipt_date', name: 'notification_reports.receipt_date'},
        {data: 'registration_at', name: 'notification_reports.created_at'},
        {data: 'district', name: 'districts.name'},
        {data: 'region', name: 'regions.name'},
        {data: 'allocated', name: 'users.firstname',searchable: false,orderable: false},
        {data: 'action', name: 'action', searchable: false, orderable: false,visible:true},
        ],
        'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
            // $('td:not(:first-child)', $nRow).click(function() {
            //     window.open(base_url + "/claim/investigations/investigator_assign/" + $aData['id'], "_self");
            // }).hover(function() {
            //     $(this).css('cursor', 'alias');
            // }, function() {
            //     $(this).css('cursor', 'auto');
            // });
        }
    });
    $('#search-form').on('submit', function($e) {
        $oTable.draw();
        $e.preventDefault();
    });


    $('#files-allocation-table').on('click','.btn-remove-file',function(e){
      e.preventDefault();
      var remove_id = $(this).data('remove');
      swal({
        title: "Are you sure want to remove this allocated file?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirm",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true,
    },
    function(isConfirm) {
        if (isConfirm) {
           $.ajax({
            url :'{{url("claim/investigations/remove_assigned_file/")}}/'+remove_id,
            type : 'GET',
            datatype : 'json',
            success:function(data){
                if (data.success) {
                    $.amaran({
                        'theme'     :'awesome success',
                        'content'   :{
                            title : "Success",
                            message: data.message,
                            info:'',
                            icon: 'fa fa-check-square-o'
                        },
                        'position'  :'bottom left',
                        'outEffect' :'slideBottom',
                        'inEffect'  :'slideLeft'
                    });
                }else{
                    swal('Sorry! {{access()->user()->firstname}}', 'Failed to remove file!', 'warning');
                }
                $oTable.ajax.reload();
            }

        });

       }

   });
  });
   // ($can_assign)
   $('#assign-user').on('submit', function($e) {
    $e.preventDefault();
    let $form = this;
    let selected_user = $('#assigned_user option:selected').text() ? $('#assigned_user option:selected').text() : 'the selected user';
    $('.error_field').addClass('hidden');
    $('.error_field').text('');
    let $rowsSelected = $oTable.column(0).checkboxes.selected();
    $($form).find("input[name='id[]']").remove();
    $.each($rowsSelected, function($index, $rowId) {
        $($form).append($('<input>').attr('type', 'hidden').attr('name', 'id[]').val($rowId));
    });
    swal({
        title: "Warning",
        text: "Are you sure you want to allocate selected files to "+selected_user,
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Cancel",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirm",
        closeOnConfirm: true
    }, function ($confirmed) {
        if ($confirmed) {
            let $options = {
                dataType : "json",
                data : {},
                type : "PUT",
                url : $($form).attr("action"),
                success : function (data) {
                    if (data.success) {
                        $oTable.draw();
                        $oTable.column(0).checkboxes.deselectAll();
                        $.amaran({
                            'theme'     :'awesome success',
                            'content'   :{
                                title : "Success",
                                message: data.message,
                                info:'',
                                icon: 'fa fa-check-square-o'
                            },
                            'position'  :'bottom left',
                            'outEffect' :'slideBottom',
                            'inEffect'  :'slideLeft'
                        });
                    } else {
                        swal('Sorry! {{access()->user()->firstname}}', 'Failed to assign! '+data.message, 'warning');
                    }
                },
                error: function ($data) {
                    let $errors = $.parseJSON($data.responseText);
                    $.each($errors, function( index, value ) {
                        $('.'+index+'_error').removeClass('hidden');
                        $('.'+index+'_error').text(value);
                    });
                }
            };
            $($form).ajaxSubmit($options);
        }
    });
});

   

});

</script>
@endpush