@extends('layouts.backend.main', ['title' => "Investigation Plan", 'header_title' => "Investigation Plan"])
@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style type="text/css">
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush
@section('content')

<div class="row">
    @include("backend/operation/claim/investigation_plan/includes/header_info")
</div>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tab-pills-image">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                        <i class="icon fa fa-info-circle" aria-hidden="true"></i>General Information
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="notification_file_tab" data-toggle="tab" href="#notification_file" role="tab">
                        <i class="icon fa fa-briefcase" aria-hidden="true"></i>Assigned Notifications
                    </a>
                </li>
                @if($profile->total_budget > 0)
                <li class="nav-item">
                    <a class="nav-link" id="plan_budget_tab" data-toggle="tab" href="#plan_budget" role="tab">
                        <i class="icon fa fa-folder-open" aria-hidden="true"></i>Plan Budget
                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" id="workflow_history_tab" data-toggle="tab" href="#workflow_history" role="tab">
                        <i class="icon fa fa-history" aria-hidden="true"></i>Workflow History
                    </a>
                </li>
                {{-- @endif --}}
            </ul>
            <div class="nav_tab_contain tab-content">
                <div class="tab-pane active" id="general_information" role="tabpanel">
                   <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >
                            @if($profile->can_assign)
                            <div class="pull-right" >
                                @if(empty($profile->plan_status))
                                <span>
                                    {!! HTML::decode(link_to_route('backend.claim.investigations.submit_for_review', "<i class='icon fa fa-paper-plane' aria-hidden='true'></i>&nbsp; Submit For Review",[$profile->id],['data-method' => 'confirm','data-type'=>'warning', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'This will submit this plan for review and approval!', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                </span>
                                @endif
                                <span>
                                    <button class="btn btn-secondary site-btn assign_investigators_btn"><i class='icon fa fa-users' aria-hidden='true'></i>&nbsp;Add Investigator(s)</button>
                                </span>
                                <span>
                                    {!! HTML::decode(link_to_route('backend.claim.investigations.assign_files', "<i class='icon fa fa-user' aria-hidden='true'></i>&nbsp; Allocate Notification(s) To Investigators", [$profile->id],['class' => 'btn btn-info site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                                </span>
                                <span>
                                    {!! HTML::decode(link_to_route('backend.claim.investigations.edit', "<i class='icon fa fa-ellipsis-v' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.claim_menu.investigation.edit'),[$profile->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                                </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class = "row">
                    <div class="col-md-12">
                        <div class="col-md-8">

                            <table class="table table-bordered mb-1 mt-2">

                                <thead>
                                 <tr class="table-active">
                                   <td  width="10%"></td>
                                   <td  width="20%" style="text-align: right;"><span class="h6">Planned</span></td>
                                   <td  width="20%" style="text-align: right;"><span class="h6">Assigned</span></td>
                                   <td  width="20%" style="text-align: right;"><span class="h6">Remain</span></td>
                               </tr>
                           </thead>
                           <tbody>
                               <tr class="table-info">
                                <td width="10%"><span class="h6">Investigators&nbsp;</span></td>
                                <td style="text-align: right;">{!!  $profile->number_of_investigators !!}</td>
                                <td style="text-align: right;">{!! count($profile->investigators) !!}</td>
                                <td style="text-align: right;">{!! (int)$profile->number_of_investigators - (count($profile->investigators)) !!}</td>
                            </tr>
                            <tr class="table-success">
                                <td width="10%"><span class="h6">Notifications&nbsp;</span></td>
                                <td style="text-align: right;">{!!  $profile->number_of_files !!}</td>
                                <td style="text-align: right;">{!! count($profile->notifications) !!}</td>
                                <td style="text-align: right;">{!! $profile->number_of_files - (count($profile->notifications)) !!}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="mb-2">
                        @if(!empty($profile->plan_status) && empty($profile->wf_done))
                        <legend class="grey_info mt-2" >Active Workflow</legend>
                        <div id="workflow" class="nav_tab_pane tab-pane">
                            @php
                            $workflowinput = ['resource_id' => $profile->id, 'wf_module_group_id'=> 32, 'type' => $type];
                            @endphp
                            @include("backend/includes/workflow/wf_track_html", $workflowinput)
                            {{-- {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!} --}}
                        </div>
                        @endif
                    </div>

                    <legend class="grey_modal" >Assigned Investigators</legend>
                    <table class="display" cellspacing="0" width="100%" id ="investigators_datatable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>No# Of Files</th>
                            </tr>
                        </thead>
                    </table>

                </div>
                <div class="col-md-4">
                    @include("backend/operation/claim/investigation_plan/includes/plan_summary")
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="notification_file" role="tabpanel">
        <div class="col-md-12 pills-height mt-2">
            <div class="custom_filter">
             <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-row">
                                <div class="row">
                                    <div class="form-group col-md-5 allocation_select">
                                        <label for="employer">Employer Name</label>
                                        {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer', 'style' => 'width:100%']) !!}
                                    </div>
                                    <div class="form-group col-md-3 allocation_select">
                                        <label for="status">Status:</label>
                                        {!! Form::select('status', ['0' => 'All', '1' => 'Assigned', '2' => 'Unassigned', '3' => 'Assigned to User'], null, ['class' => 'form-control search-form-select', 'id' => 'status', 'style' => 'width:100%']) !!}
                                    </div>
                                    <div class="form-group col-md-3 allocated_user_select">
                                        <label for="allocated_user">Assigned User:</label>
                                        {!! Form::select('user_id', $plan_investigators, null, ['class' => 'form-control search-form-select', 'id' => 'allocated_user', 'style' => 'width:100%']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-5">
                                        <label for="region">Employer Region:</label>
                                        {!! Form::select('region', $regions, null, ['class' => 'form-control search-form-select', 'id' => 'region', 'placeholder' => '', 'style' => 'width:100%']) !!}
                                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                                    </div>
                                    <div class="form-group col-md-3 allocation_select">
                                        <label for="district">Incident District:</label>
                                        {!! Form::select('district', [], null, ['class' => 'form-control search-form-select', 'id' => 'district', 'placeholder' => '', 'style' => 'width:100%']) !!}
                                    </div>
                                    <div class="form-group col-md-3 allocation_select">
                                        <label for="incident">Incident Type</label>
                                        {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-form-select', 'id' => 'incident', 'style' => 'width:100%']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                                    <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                                </div>
                            </div>
                        </div>
                    </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <table class="display" id="allocated-files" width="100%">
            <thead>
                <tr >
                    <th>Case No</th>
                    <th>Incident Type</th>
                    <th>Employee</th>
                    <th>Employer</th>
                    <th>Incident Date</th>
                    <th>Reporting Date</th>
                    <th>Receipt Date</th>
                    <th>Registration Date</th>
                    <th>District</th>
                    <th>Region</th>
                    <th>Assigned Investigator</th>
                    <th>Investigation Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@if($profile->total_budget > 0)
<div class="tab-pane" id="plan_budget" role="tabpanel">
    <div class="row">
        <div class="col-md-12 pills-height">
            <div data-plugin="scrollbar" data-height="1500">
                <div class="row">
                    <div class="col-md-8 mt-2" id="preview_budget">

                    </div>
                    <div class="col-md-4">
                       @include("backend/operation/claim/investigation_plan/includes/plan_summary")
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
@endif
<div class="tab-pane" id="workflow_history" role="tabpanel">
    <div class="row">
        <div class="col-md-12 pills-height">
            <div data-plugin="scrollbar" data-height="1500">
                <div class="row">
                    <div class="col-md-8">
                        @if(!empty($profile->plan_status) && $profile->wf_done)
                        <legend class="grey_info mt-2" >Workflow</legend>
                        <div id="plan_workflow" class="nav_tab_pane tab-pane">
                            @php
                            $workflowinput = ['resource_id' => $profile->id, 'wf_module_group_id'=> 32, 'type' => $type];
                            @endphp
                            @include("backend/includes/workflow/wf_track_html", $workflowinput)
                            {{-- {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!} --}}
                        </div>
                        @else
                        <div class="alert-left-border mt-2">
                            <div class="alert alert-primary alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    {{-- <span aria-hidden="true">&times;</span> --}}
                                </button>
                                <strong>No workflow history.</strong> This investigation plan has no associated workflow recorded.
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                       @include("backend/operation/claim/investigation_plan/includes/plan_summary")
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
{{-- @endif --}}
</div>
</div>
</div>
<div class="modal hide fade" id="assign_investigators_modal" role="dialog" aria-labelledby="assign_investigators_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg white_modal" role="document">
        <div class="modal-content" id="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h6 class="modal-title" id="modal-title">Add Investigator(s) to this Plan</h6>
            </div>
            <div class="modal-body" style="font-size: 14px !important;">
                {!! Form::open(['url' => route('backend.claim.investigations.assign_investigators'), 'name' => 'assign_investigators_form', 'id' => 'assign_investigators_form']) !!}
                {!! Form::hidden("number_of_investigators") !!}
                {!! Form::hidden("plan_id") !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="field-layout">
                            <div class="form-group">
                                <label for="comments">Investigators</label>
                                {!! Form::select('investigators[]',  $investigators, $assigned_investigators, ['class' => 'search-select investigators', 'style' => 'width:100%', 'placeholder' => '', 'multiple' => 'true']) !!}
                                <span class="help-block label label-danger error_field investigators_error hidden"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn  site-btn btn-secondary" data-dismiss="modal">@lang("buttons.general.close")</button>
                    <button type="submit" class="btn btn-primary btn-submit" >Continue</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>
</div>
@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script>
    $(function () {

        $(".search-select").select2({
            multiple: true,
            dropdownParent: $('#assign_investigators_modal'),
        });

        previewBudget();

        $(".search-form-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });

        $('#region').on('change', function (e) {
            let region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    let $districtId = $('#district');
                    $districtId.empty();
                    $districtId.select2("val", "");
                    $districtId.html(data);
                    $("#spin2").hide();
                });
            }
        });
        let $statusId = $("#status");
        $statusId.on("change", function () {
            let $status = $(this).val();
            let $allocatedUserSelectClass = $(".allocated_user_select");
            switch($status) {
                case '3':
                $allocatedUserSelectClass.show();
                break;
                default:
                $allocatedUserSelectClass.hide();
                break;
            }
        });


        $statusId.trigger('change');
        let $assign_status_contrl = $('#assign_status');
        $assign_status_contrl.on("change", function () {
            let $assign_status = $(this).val();
            let $assignUserSelectClass = $(".assign_user_select");
            switch($assign_status) {
                case '1':
                $assignUserSelectClass.show();
                break;
                default:
                $assignUserSelectClass.hide();
                break;
            }
        });
        $assign_status_contrl.trigger('change');
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
             url: "{!! route('backend.compliance.employers') !!}",
             dataType: 'json',
             delay: 250,
             data: function (params) {
                return {
                    q: params.term || "",
                    page: params.page || 1
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data.items, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    }),
                    pagination: {
                        more: true
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    }).on("select2:selecting", function($e) {
        let $selected = $e.params.args.data.id;
    });
    $( "#clear_filter" ).click(function() {
        $(".employer-select").val(null).trigger('change.select2');
        $(".employee-select").val(null).trigger('change.select2');
        $(".search-select").val(null).trigger('change.select2');
    });
    let $oTable = $('#allocated-files').DataTable({
        dom : 'frtip',
        buttons : ['colvis', 'excel', 'csv'],
        initComplete : function () {
            $oTable.buttons().container().insertBefore('#allocated-files');
        },
        drawCallback : function () {

        },
        processing: true,
        serverSide: true,
        info : true,
        autoWidth: true,
        stateSaveCallback: function (settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
        },
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
        },
        ajax: {
            url: "{!! route("backend.claim.investigations.get_allocated_files",$profile->id) !!}",
            method : "GET",
            data: function ($d) {
              $d.employee = $('select[name=employee]').val();
              $d.employer = $('select[name=employer]').val();
              $d.status = $('select[name=status]').val();
              $d.incident = $('select[name=incident]').val();
              $d.user_id = $('select[name=user_id]').val();
              $d.region = $('select[name=region]').val();
              $d.district = $('select[name=district]').val();
              $d.stage = $('select[name=stage]').val();
              $d.plan_id = {{$profile->id}};
          }
      },
      columns: [
      {data: 'filename', name: 'notification_reports.filename', visible: true},
      {data: 'incident', name: 'incident_types.name'},
      {data: 'employee', name: 'employees.firstname'},
      {data: 'employer', name: 'employers.name'},
      {data: 'incident_date' , name: 'notification_reports.incident_date', orderable : true, searchable : true},
      {data: 'reporting_date', name: 'notification_reports.reporting_date'},
      {data: 'receipt_date', name: 'notification_reports.receipt_date'},
      {data: 'registration_at', name: 'notification_reports.created_at'},
      {data: 'district', name: 'districts.name'},
      {data: 'region', name: 'regions.name'},
      {data: 'allocated', name: 'users.firstname',searchable: false,orderable: false},
      {data: 'investigation_status', name: 'investigation_status',searchable: false,orderable: false},
      ],
      'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
        $('td', $nRow).click(function() {
            window.open(base_url + "/claim/notification_report/profile/" + $aData['notification_report_id'], "_blank");
        }).hover(function() {
            $(this).css('cursor', 'alias');
        }, function() {
            $(this).css('cursor', 'auto');
        });
    }

});
    $('#search-form').on('submit', function($e) {
        $oTable.draw();
        $e.preventDefault();
    });


    let $body = $('body');
    $body.on('click', 'button.assign_investigators_btn', function ($e) {
        $e.preventDefault();
        let $form = $('form[name=assign_investigators_form]');
        let $modal = $('#assign_investigators_modal');
        $form.find("input[name^='number_of_investigators']").val({{$profile->number_of_investigators}});
        $form.find("input[name^='plan_id']").val({{$profile->id}});
        $modal.modal('show');
    });
    $body.on('submit', 'form[name=assign_investigators_form]', function(e) {
       e.preventDefault();
       let $form = this;
       $('.error_field').addClass('hidden');
       $('.error_field').text('');
       if (!$(".investigators option:selected").length) {
        $('investigators_error').removeClass('hidden');
        $('investigators_error').text('Please select investigator');
    }else{
       let $options = {
        dataType : "json",
        type : "POST",
        url : $($form).attr("action"),
        success : function (data) {
            $($form).find(".btn-submit").prop('disabled', false);
            if (data.success) {
             $('#assign_investigators_modal').modal('hide');
             swal('Dear {{access()->user()->firstname}}','Investigators have been updated successfully','success');
             setTimeout(function() { location.reload(); }, 2000);
         }
     },
     error: function ($data) {
        let $errors = $.parseJSON($data.responseText);
        $.each($errors, function( index, value ) {
            $('.'+index+'_error').removeClass('hidden');
            $('.'+index+'_error').text(value);
        });
    },
};
$($form).ajaxSubmit($options);
}

});

    let investigators_datatable =  $('#investigators_datatable').DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        searching: true,
        paging: true,
        info:true,
        autoWidth: true,
        stateSaveCallback: function (settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
        },
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
        },
        ajax:{
            url : '{!! route('backend.claim.investigations.investigators_datatable',$profile->id) !!}',
            type : 'GET'
        },
        columns: [
        { data: 'user' , name: 'user'},
        { data: 'number_of_files' , name: 'number_of_files'},
        ],
        columnDefs: [
        {
            targets: -1,
            className: 'dt-body-right'
        }
        ],
        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td', nRow).click(function() {
             redirectInvestigatorClick(aData['user_id']);
         }).hover(function() {
            $(this).css('cursor','pointer');
        }, function() {
            $(this).css('cursor','auto');
        });
     }
 });


    function redirectInvestigatorClick(clicked_investigator) {
        $('#notification_file_tab').trigger('click');
        $("#status").val(3).trigger('change');
        $("#allocated_user").val(clicked_investigator).trigger('change');
        $('#notification_file_tab').trigger('click');
        $('#submit-search-form').trigger('click');
    }


    if (location.hash !== '') {
        let $linkhref = $('a[href="' + location.hash + '"]');
        $linkhref.tab('show');
        $linkhref.trigger('click');
    }
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var tab = $(e.target).attr('href').substr(1);
        if (history.pushState) {
            history.pushState(null, null, '#' + tab);
        } else {
            location.hash = '#' + tab;
        }
    });


    function previewBudget() {
        let $iframe = $('<iframe src="{!!url("/") . "/public/ViewerJS/#../storage/investigation_plan/".$profile->id.'/'.$profile->budget_attachment!!}" frameborder="0"  width=\'100%\' height=\'600px\' allowfullscreen></iframe>');
        $('#preview_budget').append($iframe);
    }

});
</script>
@endpush