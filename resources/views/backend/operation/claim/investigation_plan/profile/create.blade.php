@extends('layouts.backend.main', ['title' => 'Create Investigation Plan', 'header_title' => 'Create Investigation Plan'])
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush
@include('backend.includes.assets.datetimepicker')


@section('content')
{!! Form::open(['route' => 'backend.claim.investigations.store', 'name' => 'create_investigation_profile', 'enctype'=>'multipart/form-data']) !!}
{!! Form::hidden('user_id', access()->id()) !!}
{!! Form::hidden('max_files',$limits['max_files']) !!}
{!! Form::hidden('max_officers', $limits['max_officers']) !!}
{!! Form::hidden('investigation_category', '', ['id'=>'investigation_category']) !!}
<div class="row">
    <div class="col-md-5">
       <div class="fileld-layout">
        <label class="required">Investigation Type</label>
        <div class="form-group mt-1">
            <div class="input-group">
                {!! Form::select('type', $types, null, ['class' => 'search-select form-control', 'style' => 'width:50%']) !!}
                {!! $errors->first('type', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
    </div>       

    <div class="fileld-layout pt-1">
        <label class="required">Investigation Category</label>
        <div class="form-group mt-1">
            <div class="input-group">
                {!! Form::select('category', $categories, null, ['class' => 'search-select form-control', 'id'=>'category', 'style' => 'width:50%']) !!}
                {!! $errors->first('category', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
    </div>

    <div class="filed-layout conduct_option hidden">
        <label class="required pb-1 pt-1">How would you conduct this investigation!?</label>
        <div class="form-group pl-3">
            <div class="input-group">
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="conduct_option" id="alone_option" value="alone" checked>
                  <label class="form-check-label" for="alone_option">
                    Alone
                </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="conduct_option" id="other_option" value="others">
              <label class="form-check-label" for="other_option">
               With other investigator(s)
           </label>
       </div>
   </div>
   {!! $errors->first('conduct_option', '<span class="help-block label label-danger">:message</span>') !!}
</div>
</div>


<div class="filed-layout mt-2">
    <label class="required">Number Of Investigators</label>
    <div class="form-group mt-1">
        <div class="input-group" style="width:50%;">
            {!! Form::number('number_of_investigators', null, ['placeholder' => '', 'class' => 'form-control number_of_investigators', 'max'=>$limits['max_officers']]) !!}
        </div>
        {!! $errors->first('number_of_investigators', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>

<div class="fileld-layout pt-1">
    <label class="required">Does the plan have any budget!?</label>
    <div class="form-group mt-1">
        <div class="input-group">
            {!! Form::select('budget_option', [1=>'Yes', 2=> 'No'], 1, ['class' => 'form-control', 'id'=>'budget_option', 'style' => 'width:50%']) !!}
            {!! $errors->first('budget_option', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>
</div>

<div class="filed-layout budget_option">
    <label class="required">Total Budget</label>
    <div class="form-group mt-1">
        <div class="input-group" style="width:50%;">
            {!! Form::number('total_budget', null, ['placeholder' => '', 'class' => 'form-control total_budget']) !!}
        </div>
        {!! $errors->first('total_budget', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>


</div>

<div class="col-md-6">
 <div class="fileld-layout">
    <label class="required">Investigation Reason</label>
    <div class="form-group mt-1">
        <div class="input-group">
            {!! Form::textarea('reason', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px; min-width:50%;']) !!}
            {!! $errors->first('reason', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>
</div>

<div class="filed-layout pt-1">
    <label class="required">@lang('labels.backend.claim_menu.investigation.start_date')</label>
    <div class="form-group">
        <div class="input-group" style="width:50%;">
            {!! Form::text('start_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
        </div>
        {!! $errors->first('start_date', '<span class="help-block label label-danger">:message</span>') !!}
        {!! Form::hidden('today_date', getTodayDate()) !!}
    </div>
</div>

<div class="filed-layout pt-1">
    <label class="required">@lang('labels.backend.claim_menu.investigation.end_date')</label>
    <div class="form-group">
        <div class="input-group" style="width:50%;">
            {!! Form::text('end_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
        </div>
        {!! $errors->first('end_date', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>


<div class="filed-layout pt-1">
    <label class="required">Number Of Files</label>
    <div class="form-group">
        <div class="input-group" style="width:50%;">
            {!! Form::number('number_of_files', null, ['placeholder' => '', 'class' => 'form-control', 'max'=>$limits['max_files']]) !!}
        </div>
        {!! $errors->first('number_of_files', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>

<div class="filed-layout pt-1 budget_option">
    <label class="required">Budget Attachment</label>
    <div class="form-group mt-1">
        <div class="input-group" style="width:50%;">
            {!! Form::file('budget_attachment', ['class' => 'form-control budget_attachment', 'accept'=>'application/pdf']) !!}
        </div>
        {!! $errors->first('budget_attachment', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-md btn-submit" value="@lang('buttons.general.crud.create')" />
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
<script>
    $(function () {
     $(".search-select").select2({
        placeholder: "",
        allowClear: true,
    });
     autosize($("textarea.autosize"));
     selectedCategory();
     selectedBudgetOption();

     
     $('#category').on('change',function(e){ 
        selectedCategory();
    }); 

     $('input[type=radio][name=conduct_option]').on('change',function(e){ 
        selectedCategory();
    }); 

     $('#budget_option').on('change',function(e){ 
        selectedBudgetOption();
    }); 

     function selectedCategory() {
        let selected_category= $('#category option:selected').text();
        if (selected_category == 'Individual Investigation') {
            $('.conduct_option').removeClass('hidden');
            let selected_option = $('input[type=radio][name=conduct_option]:checked').val();
            if (selected_option == 'alone') {
                $(".number_of_investigators").prop("readonly", true);
                $(".number_of_investigators").val(1);
            } else {
                $(".number_of_investigators").prop("readonly", false);
                $(".number_of_investigators").val('');
            }
        } else {
            $('.conduct_option').addClass('hidden');
            $(".number_of_investigators").prop("readonly", false);
            $(".number_of_investigators").val('');
        }
        $("#investigation_category").val(selected_category);
    }


    function selectedBudgetOption() {
       let budget_option = $('#budget_option option:selected').val();
       if (budget_option == 2) {
        $(".budget_option").addClass("hidden");
    } else {
        $(".budget_option").removeClass("hidden");
    }
}


});
</script>
@endpush