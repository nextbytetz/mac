@extends('layouts.backend.main', ['title' => "Investigation Officers", 'header_title' => "Investigation Officers"])
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style type="text/css">
    .site-btn{
        padding: .75rem 1.5rem;
        font-size: 1rem;
        border-radius: 0rem;
    }
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <label class="h5 mb-2">Investigation Co-ordinator</label>
        {!! Form::open(['route' => ['backend.claim.investigations.defaults'], 'class' => 'form-inline investigation_default']) !!}
        <div class="form-group" style="width: 90%;">
            {!! Form::select('investigation_masters[]', $claim_users, $investigation_masters, ['class' => 'form-control search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'investigation_masters']) !!}
        </div>

        <div class="clearfix"></div> 
        <br>
        <div class="clearfix"></div> 
        
        <label class="h5 mt-2">Authorised Investigators </label>
        
        <div class="form-group" style="width: 90%;">
            {!! Form::select('authorised_investigators[]', $users, $authorised_investigators, ['class' => 'form-control search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'authorised_investigators']) !!}
        </div><br>

        <div class="form-group">
            <button class="btn btn-success site-btn mt-2 mb-1" type="submit">@lang('buttons.general.crud.update')</button>
        </div>

        {!! Form::close() !!}
        <div class="clearfix"></div>

    </div>
</div>

<hr/>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
<script>
    $(function(){
        $(".search-select").select2({});

        $('body').on('submit', 'form.investigation_default', function (e) {
            e.preventDefault();
            var $form = this;

            var $options = {
                dataType : "json",
                type : "POST",
                url : $($form).attr("action"),
                success : function (data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    if (data.success) {
                        $("<div class='alert alert-success'>Success, All changes have been updated!</div>").appendTo($($form)).delay(2000).fadeOut();
                    }
                },
                error: function ($data) {
                    var $errors = $.parseJSON($data.responseText);
                },
            };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });

    });
</script>
@endpush