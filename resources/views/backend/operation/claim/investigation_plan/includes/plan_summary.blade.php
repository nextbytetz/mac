<div class="row mt-2">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Investigation Plan Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%" class="summary">
            <tr class="underline mb-1" >
                <td class="pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Created By :</h6>
                </td>
                <td>
                    <h6 style="font-weight: bold;">{!! $profile->created_by_name !!}</h6>
                </td>
            </tr>

            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Category  :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!!  $profile->plan_category !!}</h6>
                </td>
            </tr>

            @if($profile->individual_conduction_mode)
            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Conduction Mode :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!!  $profile->individual_conduction_mode !!}</h6>
                </td>
            </tr>   
            @endif

            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">No# Of Investigators:</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!!  $profile->number_of_investigators !!}</h6>
                </td>
            </tr>


            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">No# Of Files :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!!  $profile->number_of_files !!}</h6>
                </td>
            </tr>
            <tr class="underline" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Start Date :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! $profile->start_date_formatted !!}</h6>
                </td>
            </tr>

            <tr class="underline" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">End Date :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! $profile->end_date_formatted !!}</h6>
                </td>
            </tr>

            <tr class="underline">
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Duration :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! $profile->duration !!} days</h6>
                </td>
            </tr>
            @if($profile->total_budget > 0)
            <tr class="underline">
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Total Budget :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! number_format($profile->total_budget,2) !!}</h6>
                </td>
            </tr>
            @endif
            <tr >
                <td class="pt-1 pl-1 pb-1" width="50%">
                    <h6 style="font-weight: lighter;">Reason :</h6>
                </td>
                <td class="pt-1 pb-1">
                    <h6 style="font-weight: bold;">{!! $profile->investigation_reason !!}</h6>
                </td>
            </tr>

        </table>
    </div>
</div>