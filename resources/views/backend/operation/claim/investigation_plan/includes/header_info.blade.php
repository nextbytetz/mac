<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5"><strong>Investigation Plan</strong></span>
    <span class="navbar-brand mb-0 h5">
        <small >
            Investigation_# &nbsp;:&nbsp;
            <span class="underline"> 
                <a href="{{route('backend.claim.investigations.profile',$profile->id)}}">
                    {!! $profile->plan_name !!}        
                </a> 
            </span>
            Status: {!! $profile->plan_stage_label !!}
        </small>
    </span>
</nav>
<legend></legend>
<br/>