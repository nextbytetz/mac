 <br/>
 <div class="row">
 	<div class="col-md-12">
 		@if(!empty($profile->plan_status) && empty($profile->wf_done))
 		<legend class="grey_info" >Workflow</legend>
 		<div id="workflow" class="nav_tab_pane tab-pane">
 			@php
 			$workflowinput = ['resource_id' => $profile->id, 'wf_module_group_id'=> 32, 'type' => 0];
 			@endphp
 			{!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
 		</div>
 		@endif
 	</div>
 </div>