<div class="nav_tab_pane_header">
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
    {{--             <span>
                    {!! HTML::decode(link_to_route('backend.claim.investigations.current', "<i class='icon fa fa-laptop' aria-hidden='true'></i>&nbsp;" . "System Raised", [],['class' => 'btn btn-warning site-btn', 'style' => 'font-weight:bold;' ])) !!}
                </span>
                <span>
                    {!! HTML::decode(link_to_route('backend.claim.investigations.index', "<i class='icon fa fa-database' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.claim_menu.investigation.profile.heading'), [],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                </span>
                <span>
                    {!! HTML::decode(link_to_route('backend.claim.investigations.current', "<i class='icon fa fa-ellipsis-v' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.claim_menu.investigation.current'), [],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                </span>
                <span>
                    {!! HTML::decode(link_to_route('backend.claim.investigations.index', "<i class='icon fa fa-list' aria-hidden='true'></i>&nbsp;" . __('labels.backend.claim_menu.investigation.title'), [],['class' => 'btn btn-info site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                </span> --}}
                @if($is_coordinator)
                <span>
                    {!! HTML::decode(link_to_route('backend.claim.investigations.create', "<i class='icon fa fa-plus-square-o' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.claim_menu.investigation.new'), [],['class' => 'btn btn-success site-btn', 'style' => 'font-weight:bold;' ])) !!}
                </span>
                @endif
                {{-- <span>
                    {!! HTML::decode(link_to_route('backend.claim.investigations.edit', "<i class='icon fa fa-ellipsis-v' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.claim_menu.investigation.edit'), [],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                </span> --}}
            </div>
        </div>
    </div>
</div>
<br/>


.{{-- $request->plan_id, --}}