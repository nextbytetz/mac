@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.edit_health_provider'), 'header_title' => trans('labels.backend.claim.edit_health_provider')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@section('content')

    {!! Form::model($health_provider,['route' => ['backend.claim.health_provider.update', $health_provider->id], 'method'=>'put', 'id' => 'update']) !!}

    {!! Form::hidden('request_action_type', 2 , []) !!}
    {!! Form::hidden('health_provider_id', $health_provider->id , []) !!}

    <div class="row">
        <div class="col-md-12">
            {{--Delete health service provider--}}
            <div class="pull-right">
                {{ link_to_route('backend.claim.health_provider.destroy', trans('buttons.general.crud.delete'), $health_provider->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_health_provider_delete'), 'class' => 'btn btn-danger btn-round-right']) }}
            </div>
        </div>
    </div>
    @include("backend.includes.registration.claim.edit_hsp_form")

    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.claim.health_provider.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">
    $(function () {
        $(".search-select-hsp").select2({});
        $('#region_id_hsp').on('change', function (e) {
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                $(".spin2").show();
                $('#district_id_hsp').empty();
                $("#district_id_hsp").select2("val", "");
                $('#district_id_hsp').html(data);
                $(".spin2").hide();
            });
        });
        @if ($health_provider->region_id And !$health_provider->district_id)
            $('#region_id_hsp').trigger("change");
        @endif

    });
</script>;


@endpush
