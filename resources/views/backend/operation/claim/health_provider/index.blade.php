@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.health_provider'), 'header_title' => trans('labels.backend.claim.health_provider')])

@push('after-styles-end')

@endpush

@include('backend.includes.datatable_assets')

@section('content')

    <div class="col-md-12">
        <div class="pull-right">
            {{--upload bulk--}}
            <a href="{!! route('backend.claim.health_provider.upload_bulk_page') !!}"  class="btn btn-primary dishonour_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('buttons.general.upload_bulk')</a>
            {{--Add new--}}
            <a href="{!! route('backend.claim.health_provider.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('buttons.general.add_new')</a>
        </div>
    </div>
    <div>&nbsp;</div>
    {{--<br/>--}}
    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%'], true) !!}
        </div>
    </div>
@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
@endpush