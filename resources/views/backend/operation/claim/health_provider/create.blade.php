@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.add_health_provider'), 'header_title' => trans('labels.backend.claim.add_health_provider')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

@include("backend.includes.registration.claim.create_hsp")

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $(function () {
        $(".search-select-hsp").select2({});
        $('#region_id_hsp').on('change', function (e) {
            $(".spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                $('#district_id_hsp').empty();
                $("#district_id_hsp").select2("val", "");
                $('#district_id_hsp').html(data);
                $(".spin2").hide();
            });
        });
        $('#region_id_hsp').trigger("change");
    });
</script>;
@endpush
