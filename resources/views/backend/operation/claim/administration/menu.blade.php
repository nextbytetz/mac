@extends('layouts.backend.main', ['title' => 'Notification Administration', 'header_title' => 'Notification Administration'])


@section('content')

    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">

                {{--item1--}}
                <ul class="list-unstyled">
                    <a href="{{ route('backend.claim.manual_notification.upload_all') }}">
                    {{--<a href="#">--}}
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-group"> </i><large>&nbsp;&nbsp;Upload Manual Notifications</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Upload bulk manual notifications</p> </li>
                    </a>

                </ul>
                <br/>

                {{--item2--}}
                <ul class="list-unstyled">
                    <a href="{{ route('backend.claim.notification_report.performance_report') }}">
                    {{--<a href="#">--}}
                        {{--<a href="#">--}}
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-pie-chart"> </i><large>&nbsp;&nbsp;Regional Performance Report</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Regional Performance Reports</p> </li>
                    </a>

                </ul>
                <br/>

            </div>
        </div>

        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">

                {{--item1--}}
                <ul class="list-unstyled">
                    <a href="{{ route('backend.claim.manual_notification.list_all') }}">
                    {{--<a href="#">--}}
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tasks"> </i><large>&nbsp;&nbsp;All Manual Notifications</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">List and view all manual notifications</p> </li>
                    </a>

                </ul>
                <br/>

                {{--item2--}}
                <ul class="list-unstyled">
                    <a href="{{ route('backend.claim.notification_report.staff_performance_report') }}">
                        {{--<a href="#">--}}
                        {{--<a href="#">--}}
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-circle-o-notch"> </i><large>&nbsp;&nbsp;Staff Performance Report</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Staff Performance Reports</p> </li>
                    </a>

                </ul>
                <br/>

            </div>
        </div>

    </div>


@stop

@push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>;

@endpush