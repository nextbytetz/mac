@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_dependent'), 'header_title' => trans('labels.backend.member.add_dependent')])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    @include('backend.includes.assets.datetimepicker')
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($employee,['route' => ['backend.claim.claim_accrual.dependent.store_new','employee' => $employee->id, 'notification_report_id' => $notification_report_id ],'method'=>'post', 'name' => 'create']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('employee_id', $employee->id, []) !!}
    {!! Form::hidden('death_date', $death_date, []) !!}
    {!! Form::hidden('action_type', 1, []) !!}
    <div class="row">
        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " .$employee->lastname , [ 'id'=> 'name']) !!}</strong>

                <small >
                    @lang('labels.backend.table.member_#'): {!! Form::label( 'reg_no', $employee->memberno, [ 'id'=> 'reg_no']) !!}
                </small>
            </h5>
            <legend></legend>
        </div>
    </div>

    <div>&nbsp;</div>

    {{--main contante--}}

    {{--firstname--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.firstname'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','first_name', null, ['class' => 'form-control']) !!}

                        {!! $errors->first('first_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--middlename--}}

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.middlename'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'middle_name',null, ['class' => 'form-control', ]) !!}
                        {!! $errors->first('middle_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--lastname--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.lastname'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','last_name', null, ['class' => 'form-control']) !!}

                        {!! $errors->first('last_name', '<span class="help-block label label-danger">:message</span>') !!}


                    </div>
                </div>
            </div>

            {{--gender--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.gender'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('gender_type', $genders, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('gender_type', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--DOB--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.dob'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {{--<div class="form-inline">--}}

                        {{--<span>      {!!  Form::selectRange('dob_day',1,31,null, ['class' => 'form-control search-select','style'=>'width:54px', 'placeholder' =>--}}
                        {{--'Day', 'id'=>'dob_day']) !!}--}}

                        {{--</span>--}}

                        {{--<span>      {!!  Form::selectMonth('dob_month',null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>--}}
                        {{--'Month', 'id'=>'dob_month']) !!}--}}
                        {{--</span>--}}

                        {{--<span>      {!!  Form::selectRange('dob_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(80)->format('Y'),null, ['class' => 'form-control search-select','style'=>'width:64px',--}}
                        {{--'placeholder' =>--}}
                        {{--'Year', 'id'=>'dob_year']) !!}--}}
                        {{--</span>--}}

                        {{--</div>--}}
                        {{--{!! Form::hidden('dob') !!}--}}

                        <div class="input-group" style="width:100%;">
                            {!! Form::text('dependent_dob', null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        {!! $errors->first('dependent_dob', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--rELATION--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.relationship'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('dependent_type', $dependent_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'dependent_type']) !!}
                        {!! $errors->first('dependent_type', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Child representative--}}
    <div class="row" id="child_representative">
        <div class="col-md-12">

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.claim.child_representative'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('parent_id', $other_dependents, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'parent_id']) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>




    {{--phone--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.phone'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','phone_1', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('phone_1', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--Telephone--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.telephone'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','tele_phone', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('tele_phone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--email--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.email'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','email_address', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('email_address', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--fax--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.fax'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','fax_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('fax_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--identity id--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.identity_id'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('identity', $identities, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select',  'id'=> 'identity_type']) !!}
                        {!! $errors->first('identity', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--country id--}}

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.country'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('country', $countries, 1, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('country', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--Id No--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form" id="id_no" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.id_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','id_no', '', ['class' => 'form-control' ]) !!}
                        {!! $errors->first('id_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--address--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.address'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::textarea( 'address_1', null, [ 'id'=> 'comments',  'class' =>'form-control']) !!}
                        {!! $errors->first('address_1', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--location type--}}

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.location_type'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('location_type', $location_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'location_type']) !!}
                        {!! $errors->first('location_type', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--street--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.street'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','street_name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('street_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--road--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.road'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::input( 'text','road_name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('road', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            {{--plotno--}}
            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.plot_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','plotno', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('plotno', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            {{--block_no--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.block_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::input( 'text','blockno', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('blockno', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



        </div>
    </div>


    {{--surveyed extra--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.surveyed_extra'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','surveyedextra', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('surveyedextra', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--unsurveyed--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form unsurveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.unsurveyed_area'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::textarea('unsurveyedarea', null, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                        {!! $errors->first('unsurveyedarea', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



        </div>
    </div>




    <div>      <label>{!! Form::checkbox( 'beneficiary',1, false, ['style' => 'width:20px','id' => 'beneficiary_checkbox' ]) !!}@lang('labels.general.is_beneficiary')</label></div>

    <div>&nbsp;</div>


    <div class = "row" id = "beneficiary_div" >
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">


                    {{--dependency percentage--}}
                    {{--<div class="element-form" >--}}
                    {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.dependency_percentage'):</label></div>--}}
                    {{--<div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">--}}
                    {{--<div class="form-group text_content" >--}}
                    {{--{!! Form::input( 'text','dependency_percentage', null, ['class' => 'form-control', 'id'=>'dependency_percentage']) !!}--}}
                    {{--{!! $errors->first('dependency_percentage', '<span class="help-block label label-danger">:message</span>') !!}--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}


                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Dependency Type:</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group " >
                                {!! Form::select('other_dependency_type', ['1' => 'Full' , '2' => 'Partial'], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'other_dependency_type' ]) !!}
                                {!! $errors->first('other_dependency_type', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>




                    {{--bank name--}}

                    <div class="element-form"  >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.bank'):</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::select('bank', $banks, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'bank_select']) !!}
                                <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
                                {!! $errors->first('bank', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            {{--percentage survivor pension--}}
            <div class="row">
                <div class="col-md-12">

                    {{--survivor_pension_receiver--}}
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.survivor_pension_receiver'):</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group text_content" >

                                {!!  Form::select('survivor_pension_receiver', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select' ,'placeholder'=> '', 'id'=>'survivor_pension_receiver']) !!}
                                {!! $errors->first('survivor_pension_receiver', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>




                    {{--branch name--}}

                    <div class="element-form"  >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.branch'):</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                            <div class="form-group">
                                {!! Form::select('bank_branch', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'bank_branch_select' ]) !!}
                                {!! $errors->first('bank_branch', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>


                </div>
            </div>




            <div class="row">
                <div class="col-md-12">



                    {{--survivor_gratuity_receiver--}}
                    {{--<div class="element-form" >--}}
                    {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.survivor_gratuity_receiver'):</label></div>--}}
                    {{--<div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">--}}
                    {{--<div class="form-group text_content" >--}}

                    {{--{!!  Form::select('survivor_gratuity_receiver', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select' ,'placeholder'=> '', 'id'=>'survivor_gratuity_receiver']) !!}--}}
                    {{--{!! $errors->first('survivor_gratuity_receiver', '<span class="help-block label label-danger">:message</span>') !!}--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--is disabled--}}
                    <div class="element-form child_inputs_escaped" >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right  required"><label>Disability:</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Does child has any disability to grant him / her survivor grants. "></i></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                            <div class="form-group text_content" >

                                {!!  Form::select('isdisabled', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select child_input_fields' ,'placeholder'=> '', 'id'=>'isdisabled']) !!}
                                {!! $errors->first('isdisabled', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>


                    {{--accountno--}}

                    <div class="element-form"  >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.accountno'):</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','accountno_dependent', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('accountno_dependent', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>


                </div>
            </div>


            {{--funeral_grant_percent--}}

            {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}

            {{--percentage funeral grant--}}
            {{--<div class="element-form" >--}}
            {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.funeral_grant_percent'):</label></div>--}}
            {{--<div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">--}}
            {{--<div class="form-group text_content" >--}}
            {{--{!! Form::input( 'text','funeral_grant_percent', null, ['class' => 'form-control' , 'id'=>'funeral_grant_percent']) !!}--}}
            {{--{!! $errors->first('funeral_grant_percent', '<span class="help-block label label-danger">:message</span>') !!}--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--</div>--}}



            {{--For child --}}
            <div class="row">
                <div class="col-md-12">

                    <div class="element-form child_inputs"  >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Birth Certificate no.:</label></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::input( 'text','birth_certificate_no', null, ['class' => 'form-control child_input_fields']) !!}
                                {!! $errors->first('birth_certificate_no', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>


                    {{--iseducation--}}
                    <div class="element-form child_inputs" >
                        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Education:</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Does child qualify for education benefit from survivor grants "></i></div>
                        <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group text_content" >

                                {!!  Form::select('iseducation', ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select child_input_fields' ,'placeholder'=> '', 'id'=>'iseducation']) !!}
                                {!! $errors->first('iseducation', '<span class="help-block label label-danger">:message</span>') !!}

                            </div>
                        </div>
                    </div>

                </div>
            </div>



            {{--Child inputs--}}
            <div class="row ">
                <div class="col-md-12">



                </div>
            </div>




        </div>


    </div>

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-10" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('claim_accrual/profile/' . $notification_report_id . '#dependents',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>




    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--Bank - Branch select auto fill--}}
    @include('backend/includes/assets/auto_fill_sub_category_select', ['child_value' => old('bank_branch'), 'parent_id' => 'bank_select', 'child_id' => 'bank_branch_select',
          'child_hideable'   => 0, 'isedit' => 0, 'get_url' => 'getbankbranch?bank_id='])


    <script  type="text/javascript">
        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();

        $('body').on('submit', 'form[name=create]', function(e) {
            e.preventDefault();
            // Validate date -- paymaent date
            var $day = $('#dob_day').val();
            var $month = $('#dob_month').val();
            var $year = $('#dob_year').val();
            if (($year) && ($month) && ($day )) {
                $('input[name=dob]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=dob]").val("");
            }
            $("#dependency_percentage").prop("disabled", false);
            $("#other_dependency_type").prop("disabled", false);

            beneficiary_hide_disable_all_options('beneficiary_checkbox','survivor_gratuity_receiver','survivor_pension_receiver','dependency_percentage','funeral_grant_percent');

            this.submit();

        });

        $(function () {
            $(".search-select").select2();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/



            $(".surveyed").hide();
            $(".unsurveyed").hide();
            $("#child_representative").hide();
            beneficiary_options('beneficiary_checkbox', 'beneficiary_div');
            $("#beneficiary_checkbox").click(function () {
                beneficiary_options('beneficiary_checkbox', 'beneficiary_div');
            });

            location_type_option('location_type', 'surveyed','unsurveyed');
            $("#location_type").on('change', function (e){
                location_type_option('location_type', 'surveyed','unsurveyed');
            });

            relation_option('dependent_type', 'child_representative', 'dependency_percentage', 'survivor_gratuity_receiver', 'survivor_pension_receiver');
            $("#dependent_type").on("change", function (e) {
                relation_option('dependent_type', 'child_representative','dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            });

            pension_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
            $("#survivor_pension_receiver").on("change", function (e) {
                pension_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
                relation_option('dependent_type', 'child_representative','dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
                // dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            });

            gratuity_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
            $("#survivor_gratuity_receiver").on("change", function (e) {
                gratuity_percent_option('survivor_gratuity_receiver','survivor_pension_receiver');
                relation_option('dependent_type', 'child_representative','dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
                // dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            });

            dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            $("#dependency_percentage").on("change", function (e) {
                dependency_option('dependency_percentage','survivor_gratuity_receiver','survivor_pension_receiver');
            });



            identity_option('identity_type', 'id_no');
            $("#identity_type").on('change', function (e){
                identity_option('identity_type', 'id_no');
            });


            $("#other_dependency_type").on('change', function (e){
                $('#survivor_pension_receiver').prop('disabled',true);
            });

            {{--$('#bank_select').on('change', function (e) {--}}
            {{--$("#spin2").show();--}}
            {{--var bank_id = e.target.value;--}}
            {{--$.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {--}}
            {{--$('#bank_branch_select').empty();--}}
            {{--$("#bank_branch_select").select2("val", "");--}}
            {{--$('#bank_branch_select').html(data);--}}
            {{--$("#spin2").hide();--}}
            {{--});--}}
            {{--});--}}

        });
        // beneficiary options
        function beneficiary_options(beneficiary_checkbox, beneficiary_div) {
            if ($("#" + beneficiary_checkbox).is(":checked")) {
                $("#" + beneficiary_div).show();
            } else {
                $("#" + beneficiary_div).hide();

            }
        }


        // beneficiary options when beneficiary is hidden
        function beneficiary_hide_disable_all_options(beneficiary_checkbox,survivor_gratuity_receiver,survivor_pension_receiver,dependency_percentage,funeral_grant_percent) {
            if ($("#" + beneficiary_checkbox).is(":checked")) {

            } else {
                $("#" + funeral_grant_percent ).prop("disabled", true);
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);
                $("#" + survivor_pension_receiver ).prop("disabled", true);
                $("#" + dependency_percentage ).prop("disabled", true);
                $("#other_dependency_type" ).prop("disabled", true);
            }
        }


        //Gratuity options -> disable pension percent when entering gratuity percent
        function gratuity_percent_option(survivor_gratuity_receiver,survivor_pension_receiver) {
            var $value = $("#" + survivor_gratuity_receiver).val();
            if ($value == 1) {
                $("#" + survivor_pension_receiver ).prop("disabled", true);

            }
            else {
                $("#" + survivor_pension_receiver).prop("disabled", false);
            }

        }

        //Pension percent options -> disable gratuity percent when entering pension percent
        function pension_percent_option(survivor_gratuity_receiver,survivor_pension_receiver) {
            var $value = $("#" + survivor_pension_receiver).val();
            if ($value == 1) {
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);

            }
            else {
                $("#" + survivor_gratuity_receiver).prop("disabled", false);
            }
        }

        // check if child is selected - fill if need representative
        function relation_option(dependent_type, child_representative,dependency_percentage,survivor_gratuity_receiver,survivor_pension_receiver) {

            var choice = $("#" + dependent_type).val();
//            if is child
            if (choice == 3){
                $("#" + child_representative).show();
                $(".child_inputs").show();
                $(".child_input_fields" ).prop("disabled", false);
            }else{
                $("#" + child_representative).hide();
                $(".child_inputs").hide();
                $(".child_input_fields" ).prop("disabled", true);
            }

            //            if is child or spouse (wife or husband)
            if ((choice == 1) || (choice == 2) || (choice == 3) || (choice == 7)){
                $("#" + dependency_percentage).prop("disabled", true);
                $("#" + dependency_percentage).val('');
                $("#other_dependency_type").prop("disabled", true);
                $("#other_dependency_type").val(0).change();
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);
                $("#" + survivor_pension_receiver ).prop("disabled", false);
            }else{
                $("#" + dependency_percentage).prop("disabled", false);
                $("#other_dependency_type").prop("disabled", false);
                $("#" + survivor_gratuity_receiver ).prop("disabled", false);
                $("#" + survivor_pension_receiver ).prop("disabled", true);
                /*father/mother*/
                if(choice == 4 || choice == 5){
                    $("#other_dependency_type").val(1).change();
                }
            }
        }

        //Dependency options -> options for dependency percentages
        function dependency_option(dependency_percentage,survivor_gratuity_receiver,survivor_pension_receiver) {
            var $value = $("#" + dependency_percentage).val();
            if ($value == 100) {
                $("#" + survivor_pension_receiver ).prop("disabled", false);
                $("#" + survivor_gratuity_receiver ).prop("disabled", true);

            }
            else if(($value > 0) && ($value < 100)){

                $("#" + survivor_gratuity_receiver ).prop("disabled", false);
                $("#" + survivor_pension_receiver ).prop("disabled", true);

            }

        }




        // check if location type is selected -> survyed and unsurveyed
        function location_type_option(location_type, surveyed,unsurveyed) {

            var choice = $("#"+location_type).val();
            switch (choice) {
                case '1':
                    $("." + surveyed).show();
                    $("." + unsurveyed).hide();
                    break;
                case '2':
                    $("."+surveyed).hide();
                    $("."+unsurveyed).show();
                    break;
                default:
                    $("." + surveyed).hide();
                    $("." + unsurveyed).hide();
            }
        }



        // check if identity type is selected
        function identity_option(identity_type, id_no) {

            var choice = $("#"+identity_type).val();
            switch (choice) {
                case '5':
                    $("#" + id_no).hide();

                    break;

                default:
                    $("#" + id_no).show();
            }
        }

        //  Hide/ show child inputs
        function hide_show_child_inputs() {

        }




    </script>;


@endpush

