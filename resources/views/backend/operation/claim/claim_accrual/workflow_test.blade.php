@extends('layouts.backend.main', ['title' =>' Accrual', 'header_title' => 'Accrual'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
	tr {
		border-bottom:1pt solid rgba(0, 0, 0, 0.12);
	}
</style>
@endpush

@section('content')

<div class = "row">
	<div class="col-md-12">

		@if(empty($accrual->status))
		<div class="col-xs-12 pb-1">
			<form id="recommend_investment_form">
				<input type="text" name="investment_budget_id" class="hidden">
				<button type="submit" id="recommend_investment_submit" class="btn btn-success btn-submit pull-right">Accrue Claim</button>
				<span class="wait hidden pull-right">
					<img src="{!! asset_url() . '/data/ajax-loading.gif' !!}" style="height: 75%; width:20%;" />  
					&nbsp; <span class="h6"> Please wait .... </span>
				</span>
			</form>
		</div>
		@endif

		@if(!empty($accrual->status))
		<div id="workflow" class="nav_tab_pane tab-pane">
			@php
			$workflowinput = ['resource_id' => $accrual->id, 'wf_module_group_id'=> 31, 'type' => 2];
			@endphp
			{!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
		</div>
		@endif

	</div>

</div>

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script  type="text/javascript">

	$('#recommend_investment_submit').on('click', function(e) {
		e.preventDefault();

		$('#recommend_investment_submit').addClass('hidden');
		$('.wait').removeClass('hidden');

		swal({
			title: 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
			text: '<h6>This will submit for accrual </h6>',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Cancel',
			confirmButtonColor: '#75E0A2',
			confirmButtonText: 'Confirm',
			html: true,
			closeOnConfirm: true
		}, function (isConfirmed) {
			if (isConfirmed) {
				let submit_options = $.ajax({
					url : '{!! url("claim_accrual/initiate_wf/".$accrual->id) !!}',
					type: 'GET',
					datatype : 'json',
					success:function(data){
						if (data.success) {
							$('.wait').addClass('hidden');
							$.amaran({
								'theme'     :'awesome success',
								'content'   :{
									title : 'Dear {{ucwords(strtolower(access()->user()->lastname))}}',
									message: 'Accrual successfully Recommended',
									info:'',
									icon: 'fa fa-check-square-o'
								},
								'position'  :'bottom left',
								'outEffect' :'slideBottom',
								'inEffect'  :'slideLeft'
							});
							setTimeout(function(){location.reload(); }, 2000);
						}else{
							$('#recommend_investment_submit').removeClass('hidden');
							$('.wait').addClass('hidden');
							// setTimeout(function(){location.reload(); }, 2000);
						}
					}
				});
				$('#recommend_investment_form').ajaxSubmit(submit_options);
			}else{
				$('#recommend_investment_submit').removeClass('hidden');
				$('.wait').addClass('hidden');
			}
		});

	});

</script>

@endpush
