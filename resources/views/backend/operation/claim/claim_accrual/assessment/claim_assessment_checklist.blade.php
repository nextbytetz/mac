@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.claim_assessment'), 'header_title' => trans('labels.backend.claim.claim_assessment')])

@include('backend.includes.datatable_assets')

{{--@include('backend.includes.toastr_assets')--}}
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')

    <div class = "row">
        {!! Form::model($medical_expense, ['route' => ['backend.claim.claim_accrual.notification_report.claim_assessment', $medical_expense->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put' , 'name' => 'claim_assessment_checklist']) !!}
        @if (!$user_has_access)
            <div class="alert alert-info">
                @lang("strings.backend.workflow.miss_access_to_assess_claim")
            </div>
        @endif
        {!! Form::hidden('incident_date', $medical_expense->notificationReport->incident_date) !!}
        {!! Form::hidden('today_date', getTodayDate()) !!}

        {{--HEADER--}}
        @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$medical_expense->notificationReport])


        {{--main tab content--}}
        <div class = "row">
            <div class="col-md-12">


                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        {{--General--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#general"
                               data-toggle="tab">@lang('labels.general.general')
                            </a>
                        </li>
                        {{--employment-history--}}
                       {{--  <li class="nav-item">
                            <a class="nav-link"  id = "tab_2_header"  href="#employment-history" data-toggle="tab">@lang('labels.backend.member.employment_history')</a>
                        </li> --}}

                    </ul>

                    <div class="nav_tab_contain tab-content">
                        <div id="general" class="nav_tab_pane tab-pane active in">

                            {{--main tab content--}}
                            <div class = "row">
                                <div class="col-md-12">

                                    <div class="col-md-9">

                                        {{--Type of duty Claimed--}}
                                        @if ($user_has_access)

                                            <legend class="grey_modal" id="type_of_duty_overview" >@lang('labels.backend.claim.type_of_duty')</legend>
                                            @include('backend.operation.claim.claim_accrual.includes.claimed_current_disability_state')
                                        @endif



                                        <div>&nbsp;</div>
                                        {{--Type of duty Assessment--}}
                                        <legend class="grey_modal" id="type_of_duty_assessment" >@lang('labels.backend.claim.type_of_duty_assessment')</legend>
                                        @include('backend.operation.claim.claim_accrual.includes.claim_assessment_type_of_duty',['notification_report' => $medical_expense->notificationReport])
                                        <div>&nbsp;</div>
                                        {{--Claim - compensation expense overview--}}
                                        <legend class="grey_modal" id="claim_assessment_expense_overview" >@lang('labels.backend.claim.claim_assessment_expense_overview')</legend>
                                        @include('backend.operation.claim.claim_accrual.includes.claim_assessment_expense_overview',['notification_report' => $medical_expense->notificationReport])
                                        <div>&nbsp;</div>
                                        {{--show only if is employee--}}
                                        {{--@if ($medical_expense->member_type_id  == 2)--}}
                                        {{--Compuattion of Permamanent Disablement--}}
                                        <legend class="grey_modal" id="computation_of_permanent_disablement" >@lang('labels.backend.claim.computation_of_permanent_disablement')</legend>
                                        @include('backend.operation.claim.claim_accrual.includes.claim_assessment_computation_permanent_disablement',['notification_report' => $medical_expense->notificationReport])
                                        {{--@endif--}}

                                        <div>&nbsp;</div>
                                        {{--Total Claim - Compensation Amount--}}
                                        <legend class="grey_modal" id="total_claim_compensation_overview" >@lang('labels.backend.claim.total_claim_compensation_overview')</legend>
                                        @include('backend.operation.claim.claim_accrual.includes.claim_assessment_total_claim_compensation_overview',['notification_report' => $medical_expense->notificationReport])

                                    </div>


                                    <div class="col-md-3">
                                        {{--sidebar summary--}}
                                        @include('backend.operation.claim.notification_report.includes.sidebar_summary', ['notification_report' => $medical_expense->notificationReport])
                                        <div>&nbsp;</div>

                                        @include('backend.operation.claim.notification_report.includes.sidebar_incident_summary',['notification_report' => $medical_expense->notificationReport])
                                    </div>
                                </div>

                            </div>
                            {{--Buttons--}}
                            @if ($user_has_access)
                                <div class="row">
                                    <div class="col-md-5" class="form-inline" >
                                        <div class="element-form">
                                            <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                                            <div class="pull-right">

                                                {!! link_to_route('backend.claim.claim_accrual.profile',trans('buttons.general.cancel'), [$medical_expense->notification_report_id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit' ]) !!}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div>&nbsp;</div>

                        </div>

                        <div id="employment-history" class="nav_tab_pane tab-pane">
                            {{--emloyement history--}}
                            <div class = "row">
                                <div class="col-md-12">
                                    <legend class="grey_modal" id="employment_history_overview" >@lang('labels.backend.claim.employment_history_overview')</legend>
                                    @include('backend.operation.claim.notification_report.includes.employment_history',['notification_report' => $medical_expense->notificationReport])

                                </div>
                            </div>

                        </div>



                    </div>


                </div>
            </div>




            {!! Form::close() !!}
        </div>
    </div>
@stop


@push('after-script-end')

    @stack('medical-expenses-overview-script-end')
    @stack('investigation-report-script-end')
    @stack('investigators-script-end')
    @stack('health-provider-services-script-end')
    @stack('disability-state-script-end')
    @stack('health-state-script-end')
    @stack('employment-history-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}


    <script  type="text/javascript">

        var percent_impairment_scale =  parseFloat({!! sysdefs()->data()->percentage_imparement_scale !!});
        var percent_of_earning =  parseFloat({!! sysdefs()->data()->percentage_of_earning !!});
        var gross_monthly_earning = parseFloat({!! $monthly_earning_approved !!});
        var constant_factor_month =  parseInt({!! sysdefs()->data()->constant_factor_compensation !!});
        var no_of_days_for_a_month = parseFloat({!! sysdefs()->data()->no_of_days_for_a_month_in_assessment !!});
        var min_days_eligible_assessment =  parseFloat({!! sysdefs()->data()->days_eligible_for_claim_assessment !!});
        var min_compensation_amt =  parseFloat({!! sysdefs()->data()->minimum_monthly_pension !!});
        var min_salary_payable = parseFloat(min_compensation_amt / (0.01 * percent_of_earning));
        var max_compensation_amt = parseFloat({!! sysdefs()->data()->maximum_monthly_pension !!});
        var max_salary_payable = parseFloat(max_compensation_amt / (0.01 * percent_of_earning));

        var ppd_lumpsum = 0;
        var monthly_pension = 0;
        var pd = 0;
        var total_claimed_expense = $('#total_claimed_expense').val();
        var total_claimed_amount = 0;
        var total_compensation_amount = 0;
        var ttd_id = parseInt({!! $benefit_type_ids['ttd'] !!});
        var tpd_id = parseInt({!! $benefit_type_ids['tpd'] !!});
        var day_hours = parseInt({!! sysdefs()->data()->day_hours !!});
        var ttd_days_assessed = 0;
        var tpd_days_assessed = 0;
        var ttd_amount_assessed = 0;
        var tpd_amount_assessed = 0;
        var total_td_days = 0;
        var mae_amount_assessed =  parseFloat($('#mae_assessed').val());


        $(function () {
            $('.search-select').select2();

            $("#monthly_pension_row").hide();

            $("#date_of_mmi_row").hide();
            /**
             * Overview slide toggle show /hide assessment section =================
             */
            $("#employment_history_overview").click(function(){
                $("#employment-history-div").slideToggle("slow");
            });

            $("#type_of_duty_overview").click(function(){
                $("#disability-state-div").slideToggle("slow");
            });

            $("#type_of_duty_assessment").click(function(){
                $(".type-of-duty-assessment-div").slideToggle("slow");
            });

            $("#claim_assessment_expense_overview").click(function(){
                $("#claim-assessment-expense-overview-div").slideToggle("slow");
            });

            $("#computation_of_permanent_disablement").click(function(){
                $("#computation-of-permanent-disablement-div").slideToggle("slow");
            });

// ----end---------------------------------

            //initialize
            calculate_lumpsum();
            get_total_claimed_amount();

            get_compensation_expense();
            get_total_compensation_amount();
            display_compensated_expense();

            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });

            $('body').off('keyup', '.percent_entry').on('keyup', '.percent_entry', function(e) {

                percent_entry(e);
                calculate_lumpsum();
                get_total_claimed_amount();
                get_total_compensation_amount();
                display_compensated_expense();
            });

            $('body').off('keyup', '.disability_state').on('keyup', '.disability_state', function(e) {
                get_compensation_expense();
                get_total_compensation_amount();
                display_compensated_expense();
            });



            $('body').on('submit', 'form[name=claim_assessment_checklist]', function(e) {
                e.preventDefault();
//Date of MMI
                var $day =$('#mmidate').val();
                var $year = $('#mmiyear').val();
                var $month = $('#mmimonth').val();
                if(($year)&& ($month) && ($day )) {
                    $('input[name=date_of_mmi]').val($year + '-' + $month + '-' + $day);
                }else {
                    $("input[name=date_of_mmi]").val("");
                }
                this.submit();
            });


        });

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
        function percent_entry(e) {
            var percent = $('.percent_entry').val();
            if ( percent > 100
                && e.keyCode != 46 // delete
                && e.keyCode != 8 // backspace
            ) {
                e.preventDefault();
                $('.percent_entry').val(0);
                amaran_notify("{!! trans("exceptions.backend.claim.percentage_pd_dont_exceed_100") !!}", "{!! trans("labels.general.warning") !!}", "warning");
            }
        }



        function calculate_lumpsum() {
//        var pi = $('#pi').val();
            var monthly_pension_payable = 0;
            if ( $('#pd').val()) {
                pd = $('#pd').val();

                //if pi < 30
                if(pd <= percent_impairment_scale){
                    $("#ppd_lumpsum_row").show();
                    $("#monthly_pension_row").hide();
                    $("#date_of_mmi_row").hide();
                    $("#constant_factor_row").show();
                    constant_factor_month =  parseInt({!! sysdefs()->data()->constant_factor_compensation !!});
                    ppd_lumpsum = ((pd *0.01) * (percent_of_earning * 0.01) * constant_factor_month * gross_monthly_earning);

                    $('#ppd_lumpsum').text(addCommas(ppd_lumpsum.toFixed(2)));
                    return ppd_lumpsum;
                }else {
                    $("#ppd_lumpsum_row").hide();
                    $("#monthly_pension_row").show();
                    $("#date_of_mmi_row").show();
                    $("#constant_factor_row").hide();
                    constant_factor_month = 1;
                    monthly_pension = ((pd *0.01) * (percent_of_earning * 0.01) * constant_factor_month * gross_monthly_earning);
                    monthly_pension_payable = findMonthlyPensionPayable(monthly_pension, pd);
                    $('#monthly_pension').text(addCommas(monthly_pension_payable.toFixed(2)));
                    return monthly_pension_payable;
                }

            }



        }

        function get_total_claimed_amount() {
            //if pi < 30
            if(pd <= percent_impairment_scale){
                ppd_lumpsum = parseFloat(ppd_lumpsum);
            }else {
                ppd_lumpsum = 0;
            }
            total_claimed_amount = (parseFloat(total_claimed_expense) + ppd_lumpsum );
            $('#total_claimed_amount').text(addCommas(total_claimed_amount.toFixed(2)));
            return  total_claimed_amount;
        }
        function get_total_compensation_amount() {
            //if pi < 30
            if(pd <= percent_impairment_scale){
                ppd_lumpsum = parseFloat(ppd_lumpsum);
            }else {
                ppd_lumpsum = 0;
            }
            total_compensation_amount = mae_amount_assessed + ttd_amount_assessed + tpd_amount_assessed + ppd_lumpsum;
            $('#total_compensation_amount').text(addCommas(total_compensation_amount.toFixed(2)));
            return  total_compensation_amount;
        }
        /**
         * Get total compensation expense depending on no of days inserted
         * @returns {string}
         */

        function get_compensation_expense() {
            ttd_days_assessed = 0;
            tpd_days_assessed = 0;
            total_td_days = parseFloat(get_total_assessed_td_days());
            $('.disability_state').each(function() {
                var $id = this.id;
                var $notification_disability_state_id = $id.substr(4);
                var $benefit_type_id = parseInt($('#benefit_type' + $notification_disability_state_id ).val());
                var state_percent_of_ed_ld =  parseFloat($('#percent_of_ed_ld' + $notification_disability_state_id ).val());
                var state_days = 0;


                if ($('#days' + $notification_disability_state_id).val() !== "") {
                    state_days = parseFloat($('#days' + $notification_disability_state_id).val());
                }

                if ($benefit_type_id == ttd_id) {
                    ttd_days_assessed +=  (state_percent_of_ed_ld * 0.01 * state_days);
                }else if($benefit_type_id == tpd_id){
                    tpd_days_assessed += state_percent_of_ed_ld * 0.01 * state_days;
                }

                /*check if qualify for claim assessment for temporary disablement*/
                if (!(total_td_days >= min_days_eligible_assessment))
                {
                    ttd_days_assessed = 0;
                    tpd_days_assessed = 0;
                }

                ttd_amount_assessed = findFormulaTtdTpd(ttd_days_assessed, 'ttd');
                tpd_amount_assessed = findFormulaTtdTpd(tpd_days_assessed, 'tpd');

//            mae_amount_assessed =  parseFloat($('#mae_amount').val());
                total_compensation_amount = mae_amount_assessed + ttd_amount_assessed + tpd_amount_assessed + ppd_lumpsum;

            });
        }


        /**
         *  Total temporary disablement days - assessed
         *
         */
        function get_total_assessed_td_days(){
            total_td_days = 0;
            var state_days = 0;
            var total_days = 0;
            $('.disability_state').each(function() {
                var $id = this.id;
                var $notification_disability_state_id = $id.substr(4);
                var $benefit_type_id = parseInt($('#benefit_type' + $notification_disability_state_id ).val());
                state_days = 0;

                if ($('#days' + $notification_disability_state_id).val() !== "") {
                    state_days = $('#days' + $notification_disability_state_id).val();
                }

                if (($benefit_type_id == ttd_id) || $benefit_type_id == tpd_id) {
                    total_days += parseFloat(state_days);
                }
            });

            return total_days;
        }





        function display_compensated_expense() {
            $('#ttd_amount_assessed').text(addCommas(ttd_amount_assessed.toFixed(2)));
            $('#ttd_assessed').val(ttd_amount_assessed);
            $('#tpd_amount_assessed').text(addCommas(tpd_amount_assessed.toFixed(2)));
            $('#tpd_assessed').val(tpd_amount_assessed);
            $('#ttd_tpd_amount_assessed').text(addCommas((tpd_amount_assessed + ttd_amount_assessed).toFixed(2)));
            $('#mae_ttd_tpd_amount_assessed').text(addCommas((mae_amount_assessed + ttd_amount_assessed + tpd_amount_assessed).toFixed(2)));
        }

        /* Formula for finding TTD amount and TPD AMount */
        function findFormulaTtdTpd($total_days, $disablement_type) {
            /*Old Formula*/
            // return    parseFloat((percent_of_earning * 0.01) * ($total_days) * ((gross_monthly_earning / no_of_days_for_a_month)));
            amount = 0;
            if ((gross_monthly_earning < min_salary_payable) && $disablement_type == 'ttd' ){
                amount = (min_compensation_amt / no_of_days_for_a_month) * $total_days;
            }else{

                /* Check when is above the max i.e. Applicable for TTD and TPD*/
                if (gross_monthly_earning > max_salary_payable ){
                    amount = (max_compensation_amt / no_of_days_for_a_month) * $total_days;
                }else{
                    /* If Monthly earning is between Max and Min Payable*/
                    amount = ((gross_monthly_earning  * (0.01 * percent_of_earning)) / no_of_days_for_a_month) * $total_days;
                }
            }
            return parseFloat(amount);
        }


        /* Computer monthly pension payable for PPD and PTD*/
        function findMonthlyPensionPayable($monthly_pension_actual,$pd){
            var mp_payable = $monthly_pension_actual;
            /*For PTD - Check for Min and Max payable*/
            if($pd == 100){

                if ($monthly_pension_actual > max_compensation_amt){
                    mp_payable = max_compensation_amt;
                }else if ($monthly_pension_actual < min_compensation_amt){
                    mp_payable = min_compensation_amt;
                }
            }else{
                /*For PPD check for Max Payable only */
                if ($monthly_pension_actual > max_compensation_amt) {
                    mp_payable = max_compensation_amt;
                }
            }
            return parseFloat(mp_payable);
        }

        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }



        function amaran_notify(message, title, type) {
            var icon;
            if(typeof type === "undefined") {
                type = 'success';
            }
            switch(type) {
                case "success":
                    icon = "fa fa-check-square-o";
                    break;
                case "warning":
                    icon = "icon fa fa-warning";
                    break;
                case "error":
                    icon = "icon fa fa-ban";
                    break;
                default:
                    icon = "fa fa-check-square-o";
            }
            $.amaran({
                'theme'     :'awesome ' + type,
                'content'   :{
                    title:title,
                    message:message,
                    info:'',
                    icon:icon,
                },
                'position'  :'bottom left',
                'outEffect' :'slideBottom',
                'inEffect'  :'slideLeft'
            });
        }

    </script>;

@endpush
