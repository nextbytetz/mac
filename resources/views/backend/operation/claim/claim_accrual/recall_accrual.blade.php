@extends('layouts.backend.main', ['title' => trans('labels.backend.report.claim_accrual_received'), 'header_title' => trans('labels.backend.report.claim_accrual_received')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')

    {{--Start : Custom filter--}}
    <div class="custom_filter">
        {!! Form::open(['role' => 'form', 'id' => 'search-recall-notification-form']) !!}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form-row">
                    {{--Incident Type--}}
                    <div class="form-group col-md-3">
                        <label for="incident">Incident Type</label>
                        {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident']) !!}
                    </div>
                    {{--Notification Stages--}}
                    <div class="form-group col-md-6">
                        <label for="stage">Stage</label>
                        {!! Form::select('stage', $stages, null, ['class' => 'form-control search-select', 'id' => 'incident_stage', 'placeholder' => '']) !!}
                    </div>
                    {{--Employee Name--}}
                    <div class="form-group col-md-4">
                        <label for="employee">Employee Name</label>
                        {!! Form::select('employee', [], null, ['class' => 'form-control employee-select', 'id' => 'employee']) !!}
                    </div>
                    {{--Employer Name--}}
                    <div class="form-group col-md-4">
                        <label for="employer">Employer Name</label>
                        {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                    </div>
                    {{--Region Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Incident Region:</label>
                        {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    {{--District Selection--}}
                    <div class="form-group col-md-3">
                        <label for="region">Incident District:</label>
                        {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                        <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    {{--End : Custom filter--}}
    <legend></legend>
    <br/>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="display" cellspacing="0" width="100%" id ="notification-report-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>@lang('labels.backend.legal.case_no')</th>
                    <th>@lang('labels.backend.claim.incident_type')</th>
                    <th>@lang('labels.backend.compliance.employee')</th>
                    <th>@lang('labels.backend.compliance.employer')</th>
                    <th>@lang('labels.backend.table.incident_date')</th>
                    <th>@lang('labels.backend.claim.receipt_date')</th>
                    <th>@lang('labels.general.region')</th>
                    <th>Checklist User</th>
                    <th>Level</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@stop

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";

        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });

        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district').empty();
                    $("#district").select2("val", "");
                    $('#district').html(data);
                    $("#spin2").hide();
                });
            }
        });

        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 1,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employees') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employee */
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $(".search-select").val(null).trigger('change.select2');
        });

        var $oTable = $('#notification-report-table').DataTable({
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                $oTable.buttons().container().insertBefore('#notification-report-table');
                $("#notification-report-table").css("width","100%");
            },
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.claim_accrual.accrual_notification_report.get') !!}',
                type : 'post',
                data: function ($d) {
                    $d.incident = $('select[name=incident]').val();
                    $d.employee = $('select[name=employee]').val();
                    $d.employer = $('select[name=employer]').val();
                    $d.region = $('select[name=region]').val();
                    $d.district = $('select[name=district]').val();
                    $d.stage = $('select[name=stage]').val();
                }
            },
            columns: [
                { data: 'case_no' , name: 'notification_reports.id' ,orderable : true, searchable : false},
                { data: 'filename' , name: 'filename' ,orderable : true, searchable : true},
                { data: 'incident_type' , name: 'incident_types.name'},
                { data: 'fullname' , name: 'fullname', orderable : false, searchable : false},
                { data: 'employer' , name: 'employers.name', orderable : true, searchable : true},
                { data: 'incident_date' , name: 'notification_reports.incident_date', orderable : true, searchable : true},
                { data: 'receipt_date' , name: 'notification_reports.receipt_date', orderable : true, searchable : true},
                { data: 'region' , name: 'regions.name'},
                { data: 'checklistuser' , name: 'users.firstname', orderable : true, searchable : true},
                { data: 'current_wf_level' , name: 'current_wf_level', orderable : true, searchable :  false, visible : true},
                { data: 'firstname' , name: 'employees.firstname', orderable : false, searchable : true, visible : false},
                { data: 'middlename' , name: 'employees.middlename', orderable : false, searchable : true, visible : false},
                { data: 'lastname' , name: 'employees.lastname', orderable : false, searchable : true, visible : false},
                
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/claim_accrual/profile/"  + aData['case_no'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
        $('#search-recall-notification-form').on('submit', function($e) {
            $oTable.draw();
            $e.preventDefault();
        });
    });
</script>

@endpush