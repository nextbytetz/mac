@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.add_medical_expense'), 'header_title' => trans('labels.backend.claim.add_medical_expense')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_report,['route' => ['backend.claim.claim_accrual.notification_report.store_medical_expense',$notification_report->id ],'method'=>'post',
    'id' => 'create']) !!}

    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])

    <div>&nbsp;</div>

    {{--main contants--}}

    {{--member type--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.member_type'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('member_type_id', $member_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'member_type_id']) !!}
                        {!! $errors->first('member_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>

    {{--insurance--}}

    <div class="row" id="insurance_div" >
        <div class="col-md-12" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.insurance'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12 ">
                    <div class="form-group" >
                        {!! Form::select('insurance_id', $insurances, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'insurance_id']) !!}
                        {!! $errors->first('insurance_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Amount--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.amount'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('amount', null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control number money', ]) !!}
                        {!! $errors->first('amount', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {{--{!! link_to_route('backend.claim.notification_report.profile',trans('buttons.general.cancel'), [$notification_report->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}--}}
                        {!! link_to('claim_accrual/profile/' . $notification_report->id . '#medical_expense',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
<script  type="text/javascript">
    $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();




    $(function () {
        $(".search-select").select2({});
         member_type_option('member_type_id', 'insurance_id', 'insurance_div');

        $("#member_type_id").on('change keyup', function () {
            member_type_option('member_type_id','insurance_id', 'insurance_div');
        });

        $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
            number_only(e);

        });


    });

    //Member type options -> disable and hide insurance textbox if others are selected
    function member_type_option(member_type_id, insurance_id, insurance_div) {

        var member_type = $("#" + member_type_id).val();

        if (member_type == 3) {
            $("#" + insurance_id).prop("disabled", false);
            $("#" + insurance_div).prop("hidden", false);
        }
        else {

            $("#" + insurance_id).prop("disabled", true);
            $("#" + insurance_div).prop("hidden", true);
        }

    }

    /* start : mask all money input */
    $('.money').maskMoney({
        precision : 2,
        allowZero : true,
        affixesStay : false
    });

    /* start : ensure only numbers are input on monetary boxes */
    function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
</script>;


@endpush
