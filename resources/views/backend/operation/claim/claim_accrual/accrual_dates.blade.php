@extends('layouts.backend.main', ['title' => 'Add / Update Accrual Dates', 'header_title' => 'Add / Update Accrual Dates'])

{{--@include('backend.includes.toastr_assets')--}}
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush


@section('content')
{{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
        <form class="form-group" method="POST" action="{{route('backend.claim.claim_accrual.notification_report.update_accrual_dates')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">

            <div>&nbsp;</div>
            <div class="row">
                <div class="col-md-12">
                 <div class="col-md-4">
                    <label>Start Date</label>
                    <input type="date" name="from_date" class="form-control accrual_date" value="{{$from_date}}">
                </div>
                <div class="col-md-4">
                    <label>End Date</label>
                    <input type="date" name="to_date" class="form-control accrual_date" value="{{$to_date}}">
                </div>
                <div class="col-md-2">
                    <br>
                    {!! link_to('claim/notification_report/defaults',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                    {{-- {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!} --}}
                    <button class="tn btn-primary site-btn save_button">Submit</button>
                </div>
            </div>
        </div>
    </form>

    {{--Buttons--}}
  {{--   <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-4 col-lg-4 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-10 col-lg-10 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('claim_accrual/profile/#current_employee_state',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div> --}}


    {{-- {!! Form::close() !!} --}}
{{--</section>--}}

@stop


@push('after-script-end')

{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script type="text/javascript">
    // var dtToday = new Date();
  //   var month = dtToday.getMonth() + 1;     // getMonth() is zero-based
  //   var day = dtToday.getDate();
  //   var year = dtToday.getFullYear();
  //   if(month < 10)
  //     month = '0' + month.toString();
  // if(day < 10)
  //     day = '0' + day.toString();
 // max="30/06/{{$last_fin}}"
 var last = "{{$last_fin}}";
  var minDate = last + "-06-01";
  var maxDate = last + "-06-30";
  console.log(minDate)
  console.log(maxDate)

  // var currentDate = moment(minDate);
  // var futureMonth = moment(currentDate).add(12, 'M');

  $('.accrual_date').attr('min', minDate); 
  $('.accrual_date').attr('max', maxDate);  

//   $('.accrual_date').click(function(e){
//       var payment_phase = $(this).closest("tr").find("input[name='payment_phase']").val();
//       if (payment_phase == 1) {
//         var currentDate = moment(minDate);
//         var futureMonth = moment(currentDate).add(2, 'M');

//         $('.accrual_date').attr('min', minDate); 
//         $('.accrual_date').attr('max', futureMonth.format('YYYY-MM-DD')); 
//     }else{
//         var currentDate = moment(minDate);
//         var futureMonth = moment(currentDate).add(12, 'M');
//         $('.accrual_date').attr('min', minDate); 
//         $('.accrual_date').attr('max', futureMonth.format('YYYY-MM-DD')); 
//     }
// })
</script>


@endpush
