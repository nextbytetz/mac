@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.edit_current_employee_state'), 'header_title' => trans('labels.backend.claim.edit_current_employee_state')])

{{--@include('backend.includes.toastr_assets')--}}
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush


@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_report,['route' => ['backend.claim.claim_accrual.notification_report.update_current_employee_state',$notification_report->id ],'method'=>'put',
    'id' => 'update', 'name' => 'update']) !!}

    {!! Form::hidden('this_date', getTodayDate() ) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate()) !!}
    {!! Form::hidden('incident_date' , $incident_date, []) !!}
    {!! Form::hidden('incident_type' , $notification_report->incident_type_id, []) !!}




    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])


    {{--main contents--}}


    {{--HEALTH STATE CHECKLIST--}}

{{-- {{dd($medical_expenses)}} --}}
    <div>&nbsp;</div>
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.backend.claim.medical_expense'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group ">
                        {!! Form::select('medical_expense', $medical_expenses, $medical_expense->id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('medical_expense', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>




    {{--DISABILITY STATE CHECKLIST--}}

    {{--header for disability state--}}
      <div class="row">
        <div class="col-md-12 light_grey_bg">
            <div class="form-inline" >
                {{--Health state checklist--}}
                <div class="col-md-3" align="left">
                    <div class="form-group">
                        <label> @lang('labels.backend.claim.health_disability_checklist')</label>
                    </div>
                </div>
                {{--state--}}
                <div class="col-md-3" align="left">
                    <div class="form-group">
                        <label> @lang('labels.general.state')</label>
                    </div>
                </div>



                {{--no of days--}}
                <div class="col-md-3" align="left">
                    <div class="form-group">

                        <label> @lang('labels.backend.claim.no_of_days')</label>

                    </div>
                </div>

                {{--hours worked per day--}}
                <div class="col-md-3" align="left">
                    <div class="form-group" >
                        <label > @lang('labels.backend.claim.percent_of_ed_ld')</label>
                    </div>
                </div>
                </div>
            </div>
        </div>




    {{--DISABILITY --}}
    @foreach ($disability_state_checklists as $disability_state_checklist)
    @if($disability_state_checklist->id != 4)
        <div>&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-inline">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label( 'checklist', $disability_state_checklist->name, [ 'id'=>
              'checklist'])!!}
                        </div>
                    </div>
                    {{--disability_state--}}
                    <div class="col-md-3">
                        <div class="form-group">
                            {!!  Form::select('disability_state' . $disability_state_checklist->id , ['0' => 'No', '1' => 'Yes'],  ($medical_expense->accrualNotificationDisabilityStates->where('disability_state_checklist_id',$disability_state_checklist->id)->count()) ? 1 : 0, ['class' => 'form-control search-select disability_state' ,'placeholder'=> '', 'style'=>'width:120px', 'id'=>'disability_state'.$disability_state_checklist->id]) !!}
                        </div>
                    </div>


                    {{--days--}}
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::input( 'text','days' . $disability_state_checklist->id, ($medical_expense->accrualNotificationDisabilityStates->where('disability_state_checklist_id',$disability_state_checklist->id)->count()) ? $medical_expense->accrualNotificationDisabilityStates->where('disability_state_checklist_id',$disability_state_checklist->id)->first()->days : 0, ['class' => 'form-control days number fillable'. $disability_state_checklist->id ,'style'=>'width:150px', 'id'=>'days'.$disability_state_checklist->id]) !!}
                            {!! $errors->first('days'. $disability_state_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                    </div>


                    {{--percent_of_ed_ld--}}
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::input( 'text','percent_of_ed_ld' . $disability_state_checklist->id, ($medical_expense->accrualNotificationDisabilityStates->where('disability_state_checklist_id',$disability_state_checklist->id)->count()) ? $medical_expense->accrualNotificationDisabilityStates->where('disability_state_checklist_id',$disability_state_checklist->id)->first()->percent_of_ed_ld : 0, ['class' => 'form-control percent_of_ed_ld number fillable'. $disability_state_checklist->id ,'style'=>'width:150px', 'id'=>'percent_of_ed_ld'.$disability_state_checklist->id]) !!}
                            {!! $errors->first('percent_of_ed_ld'. $disability_state_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                             </div>
            </div>
        </div>
        </div>
        @endif
    @endforeach

    <div>&nbsp;</div>
    <div>&nbsp;</div>
    {{--headers health state--}}

    <div class="row">
        <div class="col-md-12 light_grey_bg">
            <div class="form-inline" >
                {{--Health state checklist--}}
                <div class="col-md-3" align="left">
                    <div class="form-group">
                        <label> @lang('labels.backend.claim.health_state_checklist')</label>
                    </div>
                </div>
                {{--state--}}
                <div class="col-md-2" align="center">
                    <div class="form-group">
                        <label> @lang('labels.general.state')</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--loop all $health_state_checklists--}}
    @foreach ($health_state_checklists as $health_state_checklist)
        <div>&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-inline" >
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label( 'checklist', $health_state_checklist->name, [ 'id'=>
              'checklist'])!!}
                        </div>
                    </div>
                    {{--state--}}
                    <div class="col-md-2">
                        <div class="form-group">
                            {!!  Form::select('health_state' . $health_state_checklist->id , ['0' => 'No', '1' => 'Yes'],  ($medical_expense->notificationReport->accrualNotificationHealthStates->where('health_state_checklist_id',$health_state_checklist->id)->count()) ? $medical_expense->notificationReport->accrualNotificationHealthStates->where('health_state_checklist_id',$health_state_checklist->id)->first()->state : 0 , ['class' => 'form-control search-select health_state' ,'placeholder'=> '', 'style'=>'width:120px', 'id'=>'health_state'.$health_state_checklist->id]) !!}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    @endforeach



    <div>&nbsp;</div>
    {{--Buttons--}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-4 col-lg-4 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-10 col-lg-10 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('claim_accrual/profile/' . $notification_report->id . '#current_employee_state',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')

{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script>
    var day_hours = parseInt({!! sysdefs()->data()->day_hours !!});

    $(function (){
        $('.search-select').select2();
        $('body').on('submit', 'form[name=update]', function(e) {
            e.preventDefault();

            $(".percent_of_ed_ld" ).prop("disabled", false);

            this.submit();
        });


        $(function () {

            $('.disability_state').each(function() {
                var $id = this.id;
                var $disability_state_id = $id.substr(16);

                disability_option('disability_state'+ $disability_state_id, 'fillable' + $disability_state_id,$disability_state_id, 'percent_of_ed_ld'+$disability_state_id);
                hours_option('percent_of_ed_ld'+$disability_state_id);
                $("#disability_state" + $disability_state_id).change(function () {
                    disability_option('disability_state'+ $disability_state_id, 'fillable' + $disability_state_id, $disability_state_id,'percent_of_ed_ld'+$disability_state_id);
                });


                $('body').off('keyup', "#percent_of_ed_ld" + $disability_state_id).on('keyup', "#percent_of_ed_ld" + $disability_state_id, function(e) {
                    hours_option('percent_of_ed_ld'+$disability_state_id);
                });

                $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                    number_only(e);

                });


            });

        });

        //Disability options -> disable and hide
        function disability_option(disability_state, fillable,disability_state_id,percent_of_ed_ld) {
            var $disability_state = $("#" + disability_state).val();
            if ($disability_state == 1) {
                $("." + fillable ).prop("disabled", false);

                if (disability_state_id == 1 || disability_state_id == 2){
                    $("#" + percent_of_ed_ld).val(100);
                    $("#" + percent_of_ed_ld ).prop("disabled", true);
                }else if(disability_state_id == 3){
                    $("#" + percent_of_ed_ld).val(50);
                }

            }
            else {
                $("." + fillable).prop("disabled", true);
            }

        }



        //hours_worked_per_day options -> should not exceed 8 hours
        function hours_option(percent_of_ed_ld) {
            var percent = $("#" + percent_of_ed_ld).val();
            if (percent > 100) {
                $("#" + percent_of_ed_ld).val('');
                amaran_notify("{!! trans("exceptions.backend.claim.percentage_ed_ld_dont_exceed_100") !!}", "{!! trans("labels.general.warning") !!}", "warning");
            }
        }

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }



        function amaran_notify(message, title, type) {
            var icon;
            if(typeof type === "undefined") {
                type = 'success';
            }
            switch(type) {
                case "success":
                    icon = "fa fa-check-square-o";
                    break;
                case "warning":
                    icon = "icon fa fa-warning";
                    break;
                case "error":
                    icon = "icon fa fa-ban";
                    break;
                default:
                    icon = "fa fa-check-square-o";
            }
            $.amaran({
                'theme'     :'awesome ' + type,
                'content'   :{
                    title:title,
                    message:message,
                    info:'',
                    icon:icon,
                },
                'position'  :'bottom left',
                'outEffect' :'slideBottom',
                'inEffect'  :'slideLeft'
            });
        }


    });




</script>


@endpush
