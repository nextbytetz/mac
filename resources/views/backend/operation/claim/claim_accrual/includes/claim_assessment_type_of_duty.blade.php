{{--DISABILITY --}}
@foreach ($notification_disability_states as $notification_disability_state)
<div class="row type-of-duty-assessment-div">&nbsp;</div>

<div class="row type-of-duty-assessment-div" >
    {{--<div class="col-md-12">--}}
        <div class="form-inline">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label( 'checklist', $notification_disability_state->disabilityStateChecklist->name, [ 'id'=>
                    'checklist'])!!}
                </div>
            </div>

            {!! Form::input( 'text','days'.$notification_disability_state->id , ($medical_expense->accrualNotificationDisabilityStateAssessments->where('accrual_notification_disability_state_id',$notification_disability_state->id)->count()) ? $medical_expense->accrualNotificationDisabilityStateAssessments->where('accrual_notification_disability_state_id',$notification_disability_state->id)->first()->days : null, ['class' => 'form-control number disability_state','id'=>'days'.$notification_disability_state->id , 'placeholder'=>trans('labels.backend.claim.no_of_days'), 'disabled'=> !$user_has_access  ]) !!}
            {!! $errors->first('days'. $notification_disability_state->id, '<span class="help-block label label-danger">:message</span>') !!}

            {!! Form::hidden( 'benefit_type'.$notification_disability_state->id , $notification_disability_state->disabilityStateChecklist->benefit_type_id, ['class' => 'form-control number disability_state', 'id'=>'benefit_type'.$notification_disability_state->id ,  'placeholder'=>trans('labels.backend.claim.no_of_days')]) !!}
            {!! Form::hidden( 'percent_of_ed_ld'.$notification_disability_state->id , $notification_disability_state->percent_of_ed_ld, ['class' => 'form-control number disability_state', 'id'=>'percent_of_ed_ld'.$notification_disability_state->id ,  'placeholder'=>trans('labels.backend.claim.percent_of_ed_ld')]) !!}



        </div>
    </div>

    @endforeach

