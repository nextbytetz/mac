{{--Total Claim Compensation Summary--}}

<div class="row" id="total-claim-compensation-overview">

    <div class="col-md-12">
        <div>&nbsp;</div>
        <table class="table table-striped table-bordered" style="width:100%">
            <tbody>
            {{--Header--}}
            <tr>
                {{--<th>@lang('labels.backend.claim.total_claimed_amount')</th>--}}
                <th>@lang('labels.backend.claim.total_compensation_amount')</th>

            </tr>


            <tr>
                {{--Total claimed--}}
                {{--<th>     <span id = "total_claimed_amount" class="underline" style="font-weight: bold;">--}}
                                            {{--0--}}
                                        {{--</span></th>--}}


                {{--Total compensation--}}
                <th>     <span id = "total_compensation_amount" class="underline" style="font-weight: bold;">
                                            0
                                        </span></th>
            </tr>


            </tbody></table>

    </div>
</div>