{{--Notification Report -> MORE LINKS OPTION--}}

<span class="dropdown"> <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" >
    @lang('buttons.general.more')
      </a>
  <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" >

      {{--Acknowledgement letter--}}
      <a class="dropdown-item" href="{!! route('backend.claim.notification_report.ackwlgletter', $notification_report->id)
                                        !!}">Acknowledgement Letter</a>
                  <div class="dropdown-divider"></div>
      {{--check if is level 1--}}
      @if($check_workflow == 0 || $check_if_is_level1_pending == 1)

          @if ($notification_report->status == 0)
              @if ($notification_report->need_investigation == 0)
                  {{--Investigate--}}
                  {{ link_to_route('backend.claim.notification_report.investigate', trans('buttons.general.investigate'), $notification_report->id, ['data-method' => 'confirm','data-type'=>'success', 'confirm-button-color'=> '#2e74ff', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.claim.activate_investigation_confirm'), 'class' => 'dropdown-item']) }}


              @else
                  {{--Cancel Investigate--}}
                  {{ link_to_route('backend.claim.notification_report.cancel_investigation', trans('buttons.general.cancel_investigation'), $notification_report->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.cancel_investigation_confirm'), 'class' => 'dropdown-item']) }}

              @endif
              <div class="dropdown-divider"></div>
          @endif



          {{--Acknowledge--}}
          {{--<a class="dropdown-item" href="#">@lang('buttons.backend.claim.acknowledge')</a>--}}

          {{--For accident--}}
          @if ($notification_report->incident_type_id == 1)
              {{--Add new Witness--}}
              <a class="dropdown-item" href="{!! route('backend.claim.notification_report.create_accident_witness', $notification_report->id)
                                        !!}">@lang('buttons.backend.claim.add_witness')</a>
              <div class="dropdown-divider"></div>
          @endif
          @if ($notification_report->need_verification == 1 && $notification_report->status == 0 && !$notification_report->isverified)
              <a>
          {{--Verify--}}
                  {{ link_to_route('backend.claim.notification_report.verify', trans('buttons.general.verify'), $notification_report->id, ['data-method' => 'confirm','data-type'=>'success', 'confirm-button-color'=> '#2e74ff', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.claim.verify_notification_report_confirm'), 'class' => 'dropdown-item']) }}
      </a>
              <div class="dropdown-divider"></div>
          @endif

          {{--Contribution tracks--}}
          @if (($notification_report->notificationContributionTracks()->count()))
              <a class="dropdown-item" href="{!! route('backend.claim.notification_contribution_track.profile', $notification_report->id)
                                        !!}">@lang('buttons.backend.claim.contribution_tracks')</a>
              <div class="dropdown-divider"></div>
          @endif


          @if(!$check_workflow)
              <a>
          {{--Reject--}}
                  {{--{{ link_to_route('backend.claim.notification_report.reject', trans('buttons.general.reject'), $notification_report->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_notification_report_rejection'), 'class' => 'dropdown-item']) }}--}}
                  {{ link_to('#', 'Reject', ['data-toggle' => 'modal', 'data-target' => '#reject_notification_modal', 'class' => 'dropdown-item']) }}

                </a>
              <div class="dropdown-divider"></div>
          @endif

          {{--          @if(!$notification_report->claim)--}}
          <a>
          {{--Undo Notification--}}
              {{ link_to_route('backend.claim.notification_report.undo', trans('labels.general.undo'), $notification_report->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_notification_report_undo'), 'class' => 'dropdown-item']) }}
      </a>
          <div class="dropdown-divider"></div>
          {{--@endif--}}


          {{--Reverse status of Notification--}}
          @if($notification_report->status != 0)
              <a>
                           {{ link_to_route('backend.claim.notification_report.reverse_status', 'Reverse Status', $notification_report->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'You are about to reverse status of notification report! Are you sure?', 'class' => 'dropdown-item']) }}
      </a>
              <div class="dropdown-divider"></div>
          @endif


          {{--end check if has workflow--}}
      @endif

      {{-- @if(!$notification_report->isdmsposted)--}}
      @if(true)
          {{--Doctors Moodification--}}
          <a class="dropdown-item" href="{!! route('backend.claim.notification_report.send_to_dms', $notification_report->id) !!}">Send to eOffice</a>
          <div class="dropdown-divider"></div>
      @endif

      {{--Doctors Moodification--}}
      @if($notification_report->status == 1 && $notification_report->wf_done == 0 )
          <a class="dropdown-item" href="{!! route('backend.claim.notification_report.doctors_modification', $notification_report->id)
                                        !!}">@lang('buttons.backend.claim.doctor_modification')</a>
          <div class="dropdown-divider"></div>
      @endif

      {{--Approve Appeal of rejection--}}
      @if($notification_report->status == 2 && $notification_report->wf_done == 1 )
          <a>
                           {{ link_to_route('backend.claim.notification_report.approve_rejection_appeal_page', 'Approve Appeal', $notification_report->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'You are about to Approve rejection appeal of notification report! Are you sure?', 'class' => 'dropdown-item']) }}
      </a>
          <div class="dropdown-divider"></div>
      @endif
      {{--backend.claim.notification_report.close_incident--}}

      @if($notification_report->status == 2 Or $notification_report->status == 1)
          <a class="dropdown-item" href="{!! route('backend.claim.notification_report.close_incident', $notification_report->id) !!}">Close Incident</a>
          <div class="dropdown-divider"></div>
      @endif

      <a class="dropdown-item" href="{{ route("backend.claim.notification_report.do_manual_payment", $notification_report->id) }}">Update Manual Payment</a>
      <div class="dropdown-divider"></div>

      {{--osh data--}}
          <a class="dropdown-item" href="{{ route("backend.claim.notification.update_osh_data", $notification_report->id) }}">Update OSH Data</a>
          <div class="dropdown-divider"></div>


      @if ($notification_report->haspaidmanual)
          <a>
           {{ link_to_route('backend.claim.notification_report.undo_manual_payment.post', 'Undo Manual Payment', $notification_report->id, ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'Are you sure to undo Manual Payment?', 'class' => 'dropdown-item']) }}
            </a>
          <div class="dropdown-divider"></div>
      @endif

      @if ($notification_report->impairment_assessment_id)
          <a class="dropdown-item" href="{{ route("backend.assessment.impairment.dashboard", $notification_report->id) }}"><i class="icon fa fa-stethoscope" aria-hidden="true"></i>&nbsp;Impairment Assessment Dashboard
        </a>
          <div class="dropdown-divider"></div>
      @else
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route("backend.assessment.impairment.create", $notification_report->id) }}"><i class="icon fa fa-plus-square-o" aria-hidden="true"></i>&nbsp;Create Impairment Assessment
        </a>
          <div class="dropdown-divider"></div>
      @endif

      {{--Close--}}
      <a class="dropdown-item" href="{!! route('backend.compliance.employee.profile', $notification_report->employee_id) !!}" >@lang('buttons.general.close')</a>
  </span>

</span>