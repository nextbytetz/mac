
{{--dependents table--}}
<div class="col-md-12" >
    <div class="pull-right">
        @if($notification_report->allocated == auth()->user()->id)
        <a href="{!! route('backend.claim.claim_accrual.dependent.create_new', [$employee->id, $notification_report->id]) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
        @endif
    </div>
</div>


<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="dependents-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.name')</th>
                <th>@lang('labels.backend.table.relation')</th>
                <th>@lang('labels.backend.table.dob')</th>
                <th>@lang('labels.backend.table.bank')</th>
                <th>@lang('labels.backend.table.branch')</th>
                <th>@lang('labels.backend.table.accountno')</th>
                <th>@lang('labels.backend.table.survivor_pension_percent')</th>
                <th>@lang('labels.backend.table.survivor_gratuity_percent')</th>
                <th>@lang('labels.backend.table.funeral_grant_percent')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>


@push('dependents-script-end')

<script  type="text/javascript">
    // $("#tab_6_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            var notification_report_id = "{!! $notification_report->id !!}";
            $('#dependents-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.claim_accrual.employee.get_dependents', [$employee->id, $notification_report->id]) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'name' , name: 'name' },
                    { data: 'dependent_type_id', name: 'dependent_type_id' },
                    { data: 'dob', name: 'dob' },
                    { data: 'bank_id', name: 'bank_id'},
                    { data: 'bank_branch_id', name: 'bank_branch_id'},
                    { data: 'accountno', name: 'accountno' },
                    { data: 'survivor_pension_percent', name: 'survivor_pension_percent' },
                    { data: 'survivor_gratuity_percent', name: 'survivor_gratuity_percent'},
                    { data: 'funeral_grant_percent', name: 'funeral_grant_percent'}

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url + "/claim_accrual/employee/dependent/" + aData['accrual_dependent_id'] + "/edit/" +  aData['employee_id'] + "/" + notification_report_id;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                },

                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    function commaSeparateNumber(val){
                        while (/(\d+)(\d{3})/.test(val.toString())){
                            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                        return val ;
                    }

                    $( api.column( 3 ).footer() ).html(
                        ''+ commaSeparateNumber(total.toFixed(2))
                    );
                }
            } );

        });

    // });
</script>;

@endpush