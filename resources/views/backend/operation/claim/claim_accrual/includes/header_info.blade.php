<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5">{!! $notification_report->status_label !!}</span>
    <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'name', $notification_report->incidentType->name) !!}</strong></span>

    @if ($notification_report->employee_id)
        <span class="navbar-brand mb-0 h5">@lang('labels.backend.claim.notification_#')&nbsp;:&nbsp;
            <small class="underline"><a href="{!! route('backend.claim.claim_accrual.profile', $notification_report->id) !!}">{!! $notification_report->filename !!}  </a></small>
        </span>
    @endif

    @if ($notification_report->progressive_stage >= 4)
        <span class="navbar-brand mb-0 h5">@lang('labels.backend.claim.claim_#')&nbsp;:&nbsp;<small class="underline"> {!!  $notification_report->claim->id !!}</small></span>
    @endif
    @if ($notification_report->employee_id)
        @php
            $link = route('backend.compliance.employee.profile', $notification_report->employee_id);
        @endphp
    @else
        @php
            $link = "#";
        @endphp
    @endif
    <span class="navbar-brand mb-0 h5"> @lang('labels.backend.claim.employee')&nbsp;:&nbsp;<small class="underline"> <a href="{!! $link !!}">{!! $notification_report->employee->name !!}</a></small></span>
</nav>
<legend></legend>
<br/>