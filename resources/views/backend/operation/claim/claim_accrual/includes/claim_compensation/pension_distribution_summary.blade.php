@if(($notification_report->accrualClaim->pd_formatted > sysdefs()->data()->percentage_imparement_scale) || in_array($notification_report->incident_type_id, [3,4,5]))
    <legend class="grey_modal" >Monthly Pension</legend>
    <table class="table table-striped table-bordered" style="width:100%">
        <tbody>
        {{--Header--}}
        <tr>
            <th>@lang('labels.backend.claim.compensation_type')</th>
            {{--<th>@lang('labels.general.amount')</th>--}}
            <th>@lang('labels.general.payable_amount')</th>
        </tr>


        {{--monthly_pension For PPD / PTD--}}
        @if($notification_report->accrualClaim->pd_formatted > sysdefs()->data()->percentage_imparement_scale)
            <tr>
                <td>@lang('labels.backend.claim.monthly_pension') </td>

                {{--<th>{!!  number_format( (isset($compensation_summary['monthly_pension']) ? $compensation_summary['monthly_pension'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
                <th>{!!  number_format( (isset($compensation_summary['monthly_pension_payable']) ? $compensation_summary['monthly_pension_payable'] : 0) , 2 , '.' , ',' ) !!}</th>

            </tr>
        @endif


        {{--For DEATH INCIDENT--}}
        @if (in_array($notification_report->incident_type_id, [3,4,5]))
            <tr>
                <td>@lang('labels.backend.claim.monthly_pension') </td>
                {{--<th>{!!  number_format( (isset($compensation_summary['monthly_pension']) ? $compensation_summary['monthly_pension'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
                <th>{!!  number_format( (isset($compensation_summary['monthly_pension_payable']) ? $compensation_summary['monthly_pension_payable'] : 0) , 2 , '.' , ',' ) !!}</th>
            </tr>
        @endif
        </tbody></table>
@endif

{{--@if (($notification_report->incident_type_id == 3) || ($notification_report->claim->pd > sysdefs()->data()->percentage_imparement_scale))--}}
@if (in_array($notification_report->incident_type_id, [3,4,5]) )
    <legend class="grey_modal" >@lang('labels.backend.claim.monthly_pension_distribution')</legend>
    </br>
    <table class="table table-striped table-bordered" style="width:100%">
        <tbody>
        {{--Header--}}
        <tr>
            <th>@lang('labels.general.name')</th>
            <th>@lang('labels.general.receiver_type')</th>
            <th>@lang('labels.general.relationship')</th>
            <th>@lang('labels.backend.table.survivor_pension_percent')</th>
            <th>@lang('labels.backend.claim.monthly_pension')</th>
        </tr>
        {{--dependents--}}



        @foreach($dependents->dependentsWithPivot($notification_report->employee_id)->where('survivor_pension_flag', 1)->get() as $dependent)

            <tr>
                <td>{!! $dependent->fullname !!}</td>
                <td>@lang('labels.general.dependent')</td>
                <td>{!! $dependent->type_name !!}</td>
                <th>{!! $dependent->survivor_pension_percent !!}</th>
                <th>{!! number_format( ( ($dependent->survivor_pension_percent * 0.01) * (isset($compensation_summary['monthly_pension_payable']) ? $compensation_summary['monthly_pension_payable'] : 0)) , 2 , '.' , ',' ) !!}</th>
            </tr>
        @endforeach

        {{--FOR PTD--}}
        {{--pensioners--}}
        @if($notification_report->employee->accrualPensioners->last())
            <tr>
                <td>{!! $notification_report->employee->name !!}</td>
                <td>@lang('labels.general.pensioner')</td>
                <td></td>
                <th>{!! 100 !!}</th>
                <th>{!! number_format(  $notification_report->employee->pensioners->last()->monthly_pension_amount , 2 , '.' , ',' ) !!}</th>
            </tr>
        @endif


        </tbody></table>
@endif


