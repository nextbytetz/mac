<div class="row">
{{--<div class="col-md-12" >--}}
    <legend class="grey_modal" >@lang('labels.backend.claim.health_state')</legend>

    {{--<div class="col-md-12" >--}}
        {{--<div class="pull-right">--}}

        {{--</div>--}}
    {{--</div>--}}

    {{--<div>&nbsp;</div>--}}
    {{--<div>&nbsp;</div>--}}

    <table class="display" cellspacing="0" width="100%" id ="health-state-table">
        <thead>
        <tr>
            {{--<th>@lang('labels.backend.claim.medical_expense')</th>--}}
                     <th>@lang('labels.general.name')</th>

        </tr>
        </thead>

    </table>
{{--</div>--}}
</div>


@push('health-state-script-end')

<script  type="text/javascript">
    $("#tab_5_header").one("click", function(){
    $(function() {
        var url = "{!! url("/") !!}";
        $('#health-state-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.claim_accrual.notification_report.get_health_state', $notification_report->id) !!}',
                type : 'get'
            },
            columns: [
                           { data: 'health_state_checklist_id' , name: 'health_state_checklist_id' }

            ],
            // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //     $(nRow).click(function() {
            //         document.location.href = url + "/claim/notification_report/current_employee_state/"  + aData['medical_expense_id'] + "/edit";
            //
            //     }).hover(function() {
            //         $(this).css('cursor','pointer');
            //     }, function() {
            //         $(this).css('cursor','auto');
            //     });
            // }

        } );
    });
    });
</script>

@endpush