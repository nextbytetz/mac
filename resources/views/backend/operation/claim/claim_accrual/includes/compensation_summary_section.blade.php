{{--Compensation section--}}

{{--Lumpsum--}}
{{-- @include('backend/operation/claim/claim_accrual/includes/claim_compensation/lumpsum_payable_summary') --}}


{{--Monthly pension--}}
@include('backend/operation/claim/claim_accrual/includes/claim_compensation/pension_distribution_summary')

{{--gratuity--}}
@include('backend/operation/claim/claim_accrual/includes/claim_compensation/survivor_gratuity_summary')


{{--funeral grant--}}
@include('backend/operation/claim/claim_accrual/includes/claim_compensation/funeral_grant_summary')
