@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.add_health_provider_service'), 'header_title' => trans('labels.backend.claim.add_health_provider_service')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush


@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_report,['route' => ['backend.claim.claim_accrual.notification_report.store_health_provider_service',$notification_report->id ],'method'=>'post',
    'id' => 'update', 'name' => 'store_health_provider_service']) !!}

    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'attend_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'attend_date']) !!}
    {!! Form::hidden('incident_date' , $incident_date, []) !!}
    {!! Form::hidden('incident_type' , $notification_report->incident_type_id, []) !!}





    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])

    <div>&nbsp;</div>

    {{--main contents--}}


    {{--HEALTH PROVIDER SERVICE--}}

    {{--medical expense--}}

    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.medical_expense'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('medical_expense_id', $medical_expenses, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}

                        {!! $errors->first('medical_expense_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--rank--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.rank'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.rank_description')  "></i></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','rank', null, ['class' => 'form-control number' ]) !!}
                        {!! $errors->first('rank', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>





    {{--Health provider -select--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.claim.health_provider'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('health_provider_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select health-provider-select', 'id'=>'health_provider_select']) !!}

                        {!! $errors->first('health_provider_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--medical practitioner -select--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.medical_practitioner'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('medical_practitioner_id', [], null,  ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control practitioner-select', 'id'=>'medical_practitioner_select']) !!}
                        {!! $errors->first('medical_practitioner_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>




    {{--attend date--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.attend_date'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                      <span>      {!!  Form::selectRange('attenddate',1,31,null, ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=> 'attenddate']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('attendmonth',null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month' ,'id'=> 'attendmonth']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('attendyear',Carbon\Carbon::now()->format('Y'),2015,null, ['class' => 'form-control search-select','style'=>'width:63px',
                        'placeholder' =>
                         'Year', 'id'=> 'attendyear']) !!}
                        </span>
                            {!! Form::hidden('attend_date', null, ['class' =>'attend_date']) !!}
                            {!! $errors->first('attend_date', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>

            {{--dismiss date--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.dismiss_date'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                      <span>      {!!  Form::selectRange('dismissdate',1,31,null, ['class' => 'form-control  search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=>'dismissdate' ]) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('dismissmonth',null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month' ,'id'=>'dismissmonth']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('dismissyear',Carbon\Carbon::now()->format('Y'),2015,null, ['class' => 'form-control search-select','style'=>'width:63px',
                        'placeholder' =>
                         'Year','id'=>'dismissyear']) !!}
                        </span>
                            {!! Form::hidden('dismiss_date', null, ['class' =>'attend_date']) !!}
                            {!! $errors->first('dismiss_date', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--headers--}}
    <div>&nbsp;</div>
    <div class="row">
        <div class="col-md-12 light_grey_bg">
            <div class="form-inline" >
                {{--Health service checklist--}}
                <div class="col-md-2" align="center">
                    <div class="form-group">
                        <label> @lang('labels.backend.claim.health_service_checklist')</label>
                    </div>
                </div>
                {{--feedback--}}
                <div class="col-md-2" align="center">
                    <div class="form-group">
                        <label> @lang('labels.general.feedback')</label>
                    </div>
                </div>

                {{--amount--}}
                <div class="col-md-2" align="center">
                    <div class="form-group" >
                        <label > @lang('labels.backend.table.amount')</label>
                    </div>
                </div>

                {{--to date--}}
                <div class="col-md-6" align="center">
                    <div class="form-group">

                        <label> @lang('labels.backend.table.to_date')</label>

                    </div>


                    {{--from date--}}
                    <div class="col-md-3" align="center">
                        <div class="form-group">

                            <label> @lang('labels.backend.table.from_date')</label>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--loop all $health_service_checklists--}}
    @foreach ($health_service_checklists as $health_service_checklist)
        <div>&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-inline" >
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label( 'checklist', $health_service_checklist->name, [ 'id'=>
              'checklist'])!!}
                        </div>
                    </div>
                    {{--feedback--}}
                    <div class="col-md-2">
                        <div class="form-group">
                            {!!  Form::select('feedback' . $health_service_checklist->id , ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select feedback' ,'placeholder'=> '', 'style'=>'width:120px', 'id'=>'feedback'.$health_service_checklist->id]) !!}
                        </div>
                    </div>

                    {{--amount--}}
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::input( 'text','amount' . $health_service_checklist->id, null, ['class' => 'form-control number money amount' ,'style'=>'width:150px', 'id'=>'amount'.$health_service_checklist->id]) !!}
                            {!! $errors->first('amount'. $health_service_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                    </div>


                    {{--from date--}}
                    <div class="col-md-3">
                        <div class="form-group">


                                          <span>      {!!  Form::selectRange('fromday' . $health_service_checklist->id ,1,31,null, ['class' => 'form-control fromday search-select from_to_date'.$health_service_checklist->id,'style'=>'width:55px', 'id' => 'fromday' . $health_service_checklist->id, 'placeholder' =>
                         'Day']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('frommonth' . $health_service_checklist->id ,null, ['class' => 'form-control frommonth search-select from_to_date'.$health_service_checklist->id,'style'=>'width:98px', 'id' => 'frommonth' . $health_service_checklist->id, 'placeholder' =>
                         'Month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('fromyear' . $health_service_checklist->id ,Carbon\Carbon::now()->format('Y'),2015,null, ['class' => 'form-control fromyear search-select from_to_date'.$health_service_checklist->id,'style'=>'width:63px', 'id' => 'fromyear' . $health_service_checklist->id,
                        'placeholder' =>
                         'Year']) !!}
                        </span>
                            <span>
                            {!! Form::hidden('from_date' . $health_service_checklist->id, null, ['class' =>'date']) !!}
                        </span>

                        </div>
                        {!! $errors->first('from_date' . $health_service_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}

                    </div>



                    {{--to date--}}
                    <div class="col-md-3">
                        <div class="form-group">


                                          <span>      {!!  Form::selectRange('today' . $health_service_checklist->id ,1,31,null, ['class' => 'form-control today search-select from_to_date'.$health_service_checklist->id,'style'=>'width:55px', 'id' => 'today' . $health_service_checklist->id, 'placeholder' =>
                         'Day']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('tomonth' . $health_service_checklist->id ,null, ['class' => 'form-control tomonth search-select from_to_date'.$health_service_checklist->id,'style'=>'width:98px', 'id' => 'tomonth' . $health_service_checklist->id, 'placeholder' =>
                         'Month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('toyear' . $health_service_checklist->id ,Carbon\Carbon::now()->format('Y'),2015,null, ['class' => 'form-control toyear search-select from_to_date'.$health_service_checklist->id,'style'=>'width:63px', 'id' => 'toyear' . $health_service_checklist->id,
                        'placeholder' =>
                         'Year']) !!}
                        </span>
                            <span>
                            {!! Form::hidden('to_date' . $health_service_checklist->id, null, ['class' =>'date']) !!}
                        </span>

                        </div>
                        {!! $errors->first('to_date' . $health_service_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}
                    </div>



                </div>
            </div>
        </div>
    @endforeach
    <div>&nbsp;</div>
    {{--Buttons--}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-4 col-lg-4 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-10 col-lg-10 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('claim_accrual/profile/' . $notification_report->id . '#medical',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')

{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}

<script>
    $(function () {
        $('.search-select').select2();
        $('body').on('submit', 'form[name=store_health_provider_service]', function(e) {
            e.preventDefault();
//Attend Date
            var $day =$('#attenddate').val();
            var $year = $('#attendyear').val();
            var $month = $('#attendmonth').val();
            if(($year)&& ($month) && ($day )) {
                $('input[name=attend_date]').val($year + '-' + $month + '-' + $day);
            }else {
            $("input[name=attend_date]").val("");
        }
//          Dismisss date
            var $day_dismiss =$('#dismissdate').val();
            var $year_dismiss = $('#dismissyear').val();
            var $month_dismiss = $('#dismissmonth').val();
            if(($year_dismiss)&& ($month_dismiss) && ($day_dismiss)) {
                $('input[name=dismiss_date]').val($year_dismiss + '-' + $month_dismiss + '-' + $day_dismiss);
            }else {
                $("input[name=dismiss_date]").val("");
            }


            $('.fromday').each(function() {
                var $id = this.id;
                var $health_checklist_id = $id.substr(7);
                var $year = $('#fromyear' + $health_checklist_id ).val();
                var $month = $('#frommonth' + $health_checklist_id ).val();

                if(($year)&& ($month) && ($(this).val())) {
                    $('input[name=from_date' + $health_checklist_id + ']').val($year + '-' + $month + '-' + $(this).val());
                }else {
                    $('input[name=from_date' + $health_checklist_id + ']').val("");
                }
            });

            $('.today').each(function() {
                var $id = this.id;
                var $health_checklist_id = $id.substr(5);
                var $year = $('#toyear' + $health_checklist_id ).val();
                var $month = $('#tomonth' + $health_checklist_id ).val();
                if(($year)&& ($month) && ($(this).val())) {
                    $('input[name=to_date' + $health_checklist_id + ']').val($year + '-' + $month + '-' + $(this).val());
                } else {
                        $('input[name=to_date' + $health_checklist_id + ']').val("");
                    }
            });

            this.submit();
        });


        $(function () {
            $('.feedback').each(function() {
                var $id = this.id;
                var $health_checklist_id = $id.substr(8);

            feedback_option('feedback'+$health_checklist_id, $health_checklist_id);

            $("#feedback"+$health_checklist_id).change(function () {
                feedback_option('feedback'+$health_checklist_id, $health_checklist_id);
            });
            });

            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });


        });

        //feedback options -> disable and hide
        function feedback_option(feedback_id, $health_checklist_id) {

            var $feedback = $("#" + feedback_id).val();
            if ($feedback == 1) {
                $("#amount" + $health_checklist_id).prop("disabled", false);
                $(".from_to_date" + $health_checklist_id).prop("disabled", false);
            }
            else {
                $("#amount" + $health_checklist_id).prop("disabled", true);
                $(".from_to_date" + $health_checklist_id).prop("disabled", true);
            }

        }

//dynamically fill medical practitioner after selecting haelth provider
        /*$('#health_provider_select').on('change', function (e) {
            $("#spin2").show();
            var health_provider_id = e.target.value;
            $.get("{{ url('/') }}/getMedicalPractitioners?health_provider_id=" + health_provider_id, function (data) {
                $('#medical_practitioner_select').empty();
                $("#medical_practitioner_select").select2("val", "");
                $('#medical_practitioner_select').html(data);
                $("#spin2").hide();
            });
        });*/


        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            allowZero : true,
            affixesStay : false
        });

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }

        //Lazy loading on medical praxtitioner
        $(function(){
//        $(".search-select").select2({});
            $(".practitioner-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.claim.medical_practitioners') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.full_name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        });

   /*     //Lazy loading on health providers    */
        $(function(){
//        $(".search-select").select2({});
            $(".health-provider-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.claim.registered_health_providers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.full_info,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        });



    });


</script>


@endpush
