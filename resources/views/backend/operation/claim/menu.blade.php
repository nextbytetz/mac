@extends('layouts.backend.main', ['title' => trans('labels.general.claim_header'), 'header_title' => trans('labels.general.claim_header')])


@section('content')

    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--register notification--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.notification_report.choose_employee') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-wheelchair-alt"></i><large>&nbsp;&nbsp;Register Notification</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Create new notification incident received from employee and/or employer</p> </li>
                    </a>

                </ul>
                <br/>
                {{--resource allocation--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.notification_report.allocation") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users" aria-hidden="true"></i><large>&nbsp;&nbsp;Resource Allocation</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Allocate and reallocate officers responsible for handling notifications and investigations</p> </li>
                    </a>

                </ul>


                <br/>
                {{--item 3- Insurance--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.insurance.index") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-plus-square
"> </i><large>&nbsp;&nbsp;@lang('labels.backend.claim.insurances')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.claim.insurance_summary')</p> </li>
                    </a>

                </ul>

                <br/>
                {{--Information Dashboard--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.notification_report.information_dashboard") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-area-chart"> </i><large>&nbsp;&nbsp;Information Dashboard</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">General Notification & Claim Progressive Reports</p> </li>
                    </a>

                </ul>
                <br/>
                {{--Claim Assessment--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.assessment.menu") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-stethoscope"> </i><large>&nbsp;&nbsp;Assessment</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Claim assessment functions e.g Impairment assessment, performance reports</p> </li>
                    </a>

                </ul>

                {{--@if (env("TESTING_MODE"))--}}
                    <br/>
                    {{--Online Notifications--}}
                    <ul class="list-unstyled">
                        <a href="{!! route("backend.claim.notification_report.online_requests") !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-cloud"> </i><large>&nbsp;&nbsp;Online Notification Registration</large></h6>
                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage online notifications submitted by employers</p> </li>
                        </a>

                    </ul>

                    <br/>
                    {{--Online Notifications--}}
                    <ul class="list-unstyled">
                        <a href="{!! route("backend.claim.notification_report.online_account_validation") !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-street-view"> </i><large>&nbsp;&nbsp;Online Notifications Account</large></h6>
                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Validate employer accounts used for registering notifications online</p> </li>
                        </a>

                    </ul>
                {{--@endif--}}


            </div>
        </div>

        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--Recall Notification --item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.notification_report.recall') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-search"> </i><large>&nbsp;&nbsp;@lang('labels.backend.claim_menu.recall_notification')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.claim_menu.recall_notification_summary')</p> </li>
                    </a>

                </ul>
                <br/>
                {{--medical practitioners--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.medical_practitioner.index") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-user-md"> </i><large>&nbsp;&nbsp;@lang('labels.backend.claim.medical_practitioner')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.claim.medical_practitioner_summary')</p> </li>
                    </a>

                </ul>

                <br/>
                {{--health providers--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.health_provider.index") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-hospital-o"> </i><large>&nbsp;&nbsp;@lang('labels.backend.claim.health_provider')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.claim.health_provider_summary')</p> </li>
                    </a>

                </ul>
                <br/>
                {{-- HCP /HSP Billing --}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.assessment.hcp_hsp.menu") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list"> </i><large>&nbsp;&nbsp;HCP / HSP Billing</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('View / Approve HCP / HSP Billing')</p> </li>
                    </a>

                </ul>
                <br/>
                {{--Payroll Administration--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.payroll.menu") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-money"> </i><large>&nbsp;&nbsp;{!! __('labels.backend.claim.payroll_administration.index') !!}</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.claim.payroll_administration.summary')</p> </li>
                    </a>

                </ul>
                <br/>
                {{--Claim Administration--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.notification_report.administration") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-adjust"> </i><large>&nbsp;&nbsp;Notification Administration</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage notification functions e.g Data Migration, performance reports</p> </li>
                    </a>

                </ul>


                <br/>
                {{--Notification Defaults--}}
                <ul class="list-unstyled">
                    <a href="{!! route("backend.claim.notification_report.defaults") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-terminal" aria-hidden="true"></i><large>&nbsp;&nbsp;Default Settings</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage default settings for notifications allocation and investigation officers</p> </li>
                    </a>

                </ul>

            </div>
        </div>

 <div class="col-sm-12 col-md-12">

            <br>
            <h5 class="cancel_button site-btn">CLAIMS ACCRUAL,INVESTIGATION PLAN AND FOLLOW UPS</h5>
            <br>
            <div class="col-sm-6 col-md-6">
                <div class="list-group">
                    {{--Claims Accrual--}}
                <ul class="list-unstyled">
                    {{-- {!! route("backend.claim.claim_accrual.recall") !!} --}}
                    <a>
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-handshake-o" aria-hidden="true"></i><large>&nbsp;&nbsp;Claims Accrual</large></h6>
                  {{-- fa-spin fa-1x --}}
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Files that will be  processed into another financial Year</p> </li>
                    </a>

                </ul>
                    <br>
                    <ul class="list-unstyled">
                        <a href="{!! route("backend.claim.investigations.index") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-file-powerpoint-o" aria-hidden="true"></i><large>&nbsp;&nbsp;Investigation Plan</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage and monitor investigation plans</p> </li>
                    </a>

                    </ul>


                </div>
            </div>

            <div class="col-sm-6 col-md-6">
                <div class="list-group">
                    <ul class="list-unstyled">
                        <a href="{!! route("backend.claim.follow_ups.get_follow_ups") !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-eye" aria-hidden="true"></i><large>&nbsp;&nbsp;Notifications Follow up</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Manage notifications follow ups</p> </li>
                    </a>

                    </ul>
                    <br>
                </div>
            </div>

        </div>

    </div>


@stop

@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {
        /*
         $("#site-header-title").hide();
         */
        $("#preview").click(function() {

        });
    });
</script>;

@endpush