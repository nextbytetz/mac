@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.edit_old_compensation'), 'header_title' => trans('labels.backend.claim.edit_old_compensation')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($employee_old_compensation,['route' => ['backend.compliance.employee.update_old_compensation',$employee_old_compensation->id ],'method'=>'put',
    'name' => 'update']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    <div class="row">

        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name', $employee_old_compensation->employee->firstname . " " .  $employee_old_compensation->employee->middlename . " " . $employee_old_compensation->employee->lastname, [ 'id'=> 'name']) !!}</strong>

                <small >
                    @lang('labels.backend.table.member_#'): {!! Form::label( 'reg_no',  $employee_old_compensation->employee->memberno , [ 'id'=> 'reg_no']) !!}
                </small>
            </h5>
            <legend></legend>
        </div>
    </div>

    <div>&nbsp;</div>

    <div class="pull-right">
        {{ link_to_route('backend.compliance.employee.delete_old_compensation', trans('buttons.general.crud.delete'), $employee_old_compensation->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_old_compensation_delete'), 'class' => 'btn btn-danger btn-round-right']) }}
    </div>

    {{--incident type--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.incident_type'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                        {!! Form::select('incident_type_id', $incident_types,$employee_old_compensation->incident_type_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('incident_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--who paid--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.claim.who_paid'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','who_paid', null, ['class' => 'form-control', ]) !!}

                        {!! $errors->first('who_paid', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--Amount--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.amount'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','amount', $employee_old_compensation->amount, ['class' => 'form-control', ]) !!}

                        {!! $errors->first('amount', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--payment date--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.payment_date'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                          <span>      {!!  Form::selectRange('payment_day',1,31,\Carbon\Carbon::parse($employee_old_compensation->payment_date)->format('d'), ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' =>
                         'Day' ,'id'=>'payment_day']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('payment_month',\Carbon\Carbon::parse($employee_old_compensation->payment_date)->format('m'), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' =>
                         'Month' ,'id'=>'payment_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('payment_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(50)->format('Y'),\Carbon\Carbon::parse($employee_old_compensation->payment_date)->format('Y'), ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year' ,'id'=>'payment_year']) !!}
                        </span>

                        </div>
                        {!! Form::hidden('payment_date') !!}
                        {!! $errors->first('payment_date', '<span class="help-block label
label-danger">:message</span>') !!}
                    </div>



                </div>
            </div>

        </div>
    </div>





    {{--Buttons--}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">


                        {!! link_to('compliance/employee/profile/' .  $employee_old_compensation->employee_id . '#compensation',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">

    $(function(){
        $(".search-select").select2({});
    });
    $('body').on('submit', 'form[name=update]', function(e) {
        e.preventDefault();
        // Validate date -- paymaent date
        var $day = $('#payment_day').val();
        var $month = $('#payment_month').val();
        var $year = $('#payment_year').val();
        if (($year !== "") && ($month !== "") && ($day !== "" )) {
            $('input[name=payment_date]').val($year + '-' + $month + '-' + $day);
        }else {
            $("input[name=payment_date]").val("");
        }


        this.submit();

    });



</script>;


{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush
