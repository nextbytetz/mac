@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.insurances'), 'header_title' => trans('labels.backend.claim.insurances')])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            {{----------Button Add New -----------}}
            <div class="col-md-12" >
                <div class="pull-right">
                    <a href="{!! route('backend.claim.insurance.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
                </div>
            </div>

            <div>&nbsp;</div>
            <table class="display" cellspacing="0" width="100%" id ="insurances-table">
                <thead>
                <tr>
                    <th>@lang('labels.general.name')</th>
                    <th>@lang('labels.general.external_id')</th>

                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#insurances-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.insurance.get') !!}',
                type : 'get'
            },
            columns: [
                { data: 'name', name: 'name'},
                { data: 'external_id' , name: 'external_id'},

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url +  "/claim/insurance/" + aData['id'] +  "/edit"  ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });







</script>;

@endpush
