@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.add_new_insurance'), 'header_title' => trans('labels.backend.claim.add_new_insurance')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::open(['route' => ['backend.claim.insurance.store'],'method'=>'post',  'id' => 'create']) !!}
    {!! Form::hidden('request_action_type', 1 , []) !!}

    {{--main contants--}}
    {{--user id--}}
    {!! Form::hidden('user_id', access()->id(), []) !!}

    {{--name--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.name'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','name', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--external--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.external_id'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','external_id', null, ['class' => 'form-control', ]) !!}

                        {!! $errors->first('external_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>





    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.claim.insurance.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();



    $(function () {
        $(".search-select").select2({});
    });



</script>;


@endpush
