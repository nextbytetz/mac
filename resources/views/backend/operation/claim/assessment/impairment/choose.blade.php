@extends('layouts.backend.main', ['title' => 'Choose Incident', 'header_title' => 'Choose Incident'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <br/>
            {{--user should click the module group to open--}}
            <div class="alert alert-success alert-icon-left" role="alert">
                <i class="fa fa-hand-o-down"></i>
                <div class="float-xs-left">
                    <strong>Search & Click</strong> on the notification in the table to create new impairment assessment.
                </div>
            </div>
        </div>
    </div>

    <!-- Put the page specifically for this page here -->
    <legend>Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $cr->description }}</small></span></legend>

    <!--Include Report-->
    @include('backend/report/configurable/includes/content', [
                'cr' => $cr,
                'cr_params' => [
                    'hasfilter' => 1,
                    'shouldrefresh' => 0,
                    'canbuild' => 1,
                    'buttons' => 1,
                    'url' => 2,
                    'query_filter' => [
                        [
                            'and' => ['status' => ["in", [0,1,3]], 'incident_type_id' => ["in", [1,2,4,5]]],
                        ],
                    ]
                ]
            ])

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    <script>

    </script>
@endpush