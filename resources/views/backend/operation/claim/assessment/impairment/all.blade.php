@extends('layouts.backend.main', ['title' => 'All Impairment', 'header_title' => 'All Impairment'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
        <div class="row">
            <div class="col-md-12" >
                <div class="pull-right" >
                    {{--employer inspection profiles--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.assessment.impairment.choose', "<i class='icon fa fa-plus-square-o' aria-hidden='true'></i>&nbsp;Add new", [],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <br/>

    <!-- Put the page specifically for this page here -->
    <legend>Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $cr->description }}</small></span></legend>

    <!--Include Report-->
    @include('backend/report/configurable/includes/content', [
                                                                'cr' => $cr,
                                                                'cr_params' => [
                                                                    'hasfilter' => 1,
                                                                    'shouldrefresh' => 1,
                                                                    'canbuild' => 1,
                                                                    'buttons' => 1
                                                                ],
                                                            ])
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    <script>

    </script>
@endpush