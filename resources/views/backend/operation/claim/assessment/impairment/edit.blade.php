@extends('layouts.backend.main', ['title' => 'Edit Impairment Assessment', 'header_title' => 'Edit Impairment Assessment'])

@push('after-styles-end')
    <style>
        .mce-ico.mce-i-fa {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
    </style>
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{--{{ Html::style(asset_url() . "/nextbyte/plugins/ckeditor5.20/sample.css") }}--}}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <div class="row">
        @include('backend/operation/claim/notification_report/includes/header_info', ['notification_report' => $incident])
    </div>
    {!! Form::model($impairment, ['route' => ['backend.assessment.impairment.update', $impairment->id], 'name' => 'edit_impairment_assessment', 'class' => 'edit_impairment_assessment', 'enctype' => 'multipart/form-data']) !!}
    {!! Form::hidden('this_date', getTodayDate()) !!}
    <div class="row">

        <div class="offset-md-2 col-md-8">

            <div class="row">

                <div class="col-md-6">
                    <div class="fileld-layout">
                        <label class="required">Location</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::textarea('location', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                            </div>
                            <span class="help-block" style="width:100% !important;">
		                    <small>Location of assessment</small>
	                </span>

                        </div>
                    </div>

                    <div class="fileld-layout">
                        <label class="required">Region</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::select('region_id', $regions, null, ['class' => 'form-control search-select', 'style' => 'width:50%', 'placeholder' => '']) !!}
                            </div>
                            <span class="help-block" style="width:100% !important;"></span>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">

                    <div class="fileld-layout">
                        <label class="required">Assessment Date</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('date', null, ['class' => 'form-control datepicker',  'style' => 'border-radius: 3px;width:50%;', 'autocomplete' => 'off']) !!}
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="fileld-layout">
                        <label class="required">Assessor</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::select('assessor', $users, null, ['class' => 'form-control search-select', 'style' => 'width:50%', 'placeholder' => '']) !!}
                            </div>
                            <span class="help-block" style="width:100% !important;"></span>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-2 col-md-8">

            <div class="fileld-layout">
                <label class="required" for="editor">Assessment Description</label>
                <div class="form-group">
                    <textarea id="editor" name="comments">{!! $impairment->comments !!}</textarea>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="offset-md-2 col-md-8">
            <div style="text-align: center;">
                <a href="{{ route("backend.assessment.impairment.choose") }}" class="btn-secondary site-btn">Cancel</a>
                <input type="submit" class="btn btn-success btn-submit" value="Update" />
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/ckeditor5.20/ckeditor.js") }}--}}
    <script>

        $(function () {

            $(".search-select").select2({});
            autosize($("textarea.autosize"));

            let $editor = tinymce.init({
                selector:'textarea#editor',
                branding: false,
                elementpath: false,
                resize: 'both',
                height : 300,
                skin: 'lightgray',
                statusbar: true,
                images_upload_url: '',
                relative_urls: false,
                automatic_uploads: false,
                forced_root_block: false,
                plugins : 'advlist autolink link image table lists charmap print preview code media insertdatetime searchreplace save wordcount fullpage lineheight',
                toolbar: 'print download | undo redo | fontsizeselect | fontselect | lineheightselect | bold italic underline | bullist numlist | indent outdent | link image',
                fontsize_formats: '8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt',
                lineheight_formats: "14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
                //font_formats: 'Tahoma=serif;Times New Roman=times new roman,times;Verdana=verdana,geneva;',
                save_onsavecallback: function () {
                    //Put logics to save the form here ...

                    console.log('Saved ...');
                },
                save_oncancelcallback: function () {
                    //Put logics to cancell the save ...
                    console.log('Save Canceled');
                },
                /*alignleft aligncenter alignright alignjustify*/
                browser_spellcheck : true,
                file_browser_callback: function(field_name, url, type, win) {
                    win.document.getElementById(field_name).value = '/public/template/assets/nextbyte/guide/media/';
                },
                file_browser_callback_types: 'file image media',
                setup: function(editor) {
                    editor.addButton('download', {
                        text: '',
                        tooltip: "Download",
                        icon: "fa fa-download",
                        onclick: function() {
                            //Download the form here ...
                            console.log('Downloaded ...');
                        }
                    });
                }
            });
            $('body').on('submit', 'form.edit_impairment_assessment', function (e) {
                e.preventDefault();
                let $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                let $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    beforeSend : function ($e) {
                        $($form).find(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        if ($data.success) {
                            document.location.href =  $data.redirect_url;
                        }
                    },
                    error: function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        let errors = $.parseJSON($data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function($index, $value) {
                            $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        });
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);

            });
        });
    </script>
@endpush