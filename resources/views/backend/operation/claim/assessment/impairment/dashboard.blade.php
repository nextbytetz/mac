@extends('layouts.backend.main', ['title' => 'Impairment Assessment Profile', 'header_title' => 'Impairment Assessment Profile'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include('backend/operation/claim/notification_report/includes/header_info', ['notification_report' => $incident])
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs">

                    {{--Inspection Process--}}
                    <li class="nav-item">
                        <a class="nav-link active" id="inspection_process_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                    {{--General Information--}}
                    <li class="nav-item">
                        <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#document_centre" role="tab">
                            <i class="icon fa fa-folder-open" aria-hidden="true"></i>Document Centre
                        </a>
                    </li>
                </ul>
                {{--<div class="divider15"></div>--}}
                {{--<legend>{!! $inspection->next_stage !!}</legend>--}}
                <div class="nav_tab_contain tab-content">
                    <div class="tab-pane active" id="general_information" role="tabpanel">
                        @include("backend/operation/claim/assessment/impairment/includes/dashboard_menu")
                        {{--                        <legend class="grey_info" >Active Workflow</legend>
                                                <div class = "row">
                                                    <div class="col-md-12">
                                                        @include("backend/operation/compliance/inspection/includes/current_workflow")
                                                    </div>
                                                </div>--}}
                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    {{-- task list overview --}}
                                    <br/>
                                    @include('backend/operation/claim/assessment/impairment/includes/dashboard_entry', ['readonly' => 0])
                                </div>
                                <div class="col-md-3">
                                    <br/>
                                    {{--sidebar summary--}}
                                    {{--Incident Summary--}}
                                    @include("backend.operation.claim.notification_report.includes.progressive.incident_summary")
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="document_centre" role="tabpanel">
                        <div class="col-md-12">
                            <br/>
                            @include("backend/system/document/includes/document_center", ['resource' => $incident, 'reference' => "DRNOTIFCNINCDNT", 'url_selector' => 0])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    <!-- Custom javascript files for this page -->
    <script>
        $(function () {
            if (location.hash !== '') {
                let $linkhref = $('a[href="' + location.hash + '"]');
                $linkhref.tab('show');
                $linkhref.trigger('click');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });
        });
    </script>
@endpush