<div class="nav_tab_pane_header">
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >

                {{--Update Info--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.assessment.impairment.create', "<i class='icon fa fa-plus-square-o' aria-hidden='true'></i>&nbsp;" . "Add New", [$incident->id],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>

                {{--Update Status--}}{{--
                <span>
                    {!! HTML::decode(link_to_route('backend.compliance.inspection.task.assign', "<i class='icon fa fa-tasks' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.task.assign'), [$inspection->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.task.assign_confirm'), 'class' => 'btn btn-primary site-btn nav_button']))  !!}
                </span>--}}

                @include("backend/operation/claim/assessment/impairment/includes/dashboard_more_links")
            </div>
        </div>
    </div>
</div>