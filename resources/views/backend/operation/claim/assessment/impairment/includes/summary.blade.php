<div class="row">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Summary</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">

    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">

        <h6 class="underline" style="font-weight: lighter;">Created By :</h6>
        <div style="font-weight: bold;">{{ $impairment->user_id }}</div>

        <h6 class="underline" style="font-weight: lighter;">Is Updated :</h6>
        <div>
            @if ($impairment->edits)
                <span class="tag tag-success white_color">Yes</span>
            @else
                <span class="tag tag-danger white_color">No</span>
            @endif
        </div>

        @if ($impairment->edits)
            <h6 class="underline" style="font-weight: lighter;">Edit History :</h6>
            <div style="font-weight: bold;">-</div>
        @endif

    </div>

</div>
