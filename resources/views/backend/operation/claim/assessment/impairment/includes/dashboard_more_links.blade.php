<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >More</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">

        {{--close--}}
        <span>
            <a href="{!! route('backend.assessment.impairment.all') !!}"  class="dropdown-item" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
        </span>

     </span>
</span>