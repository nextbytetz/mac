<div class="row">
    <div class="col-md-12">
        @php
            //dd($impairments);
        @endphp
        @foreach($impairments as $impairment)
                <div class="dashboard-header content">
                    {{--<h4 class="page-content-title float-xs-left grey_modal">Workflow Track Overview</h4>--}}
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus" style="font-size: 18px;"></span></a></li>
                            {{--<li data-widget="close"><a href="javascript:void(0)"><span aria-hidden="true" class="icon_close"></span></a></li>--}}
                        </ul>
                    </div>
                    <legend class="light_grey_bg" >{{ $impairment->assessorOfficer->name }}&nbsp;&nbsp;<small>Assessment Date</small>&nbsp;{{ $impairment->date }}</legend>
                    <div class="dashboard-box content-list" style="display: none;">
                        <div class="list-group message-list-group">

                                <div class="list-group-item">

                                    <div class="list-message float-xs-left">
                                        @if (!$readonly)
                                            <span class="float-xs-right">
                                                {!! HTML::decode(link_to_route('backend.assessment.impairment.edit', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . "Edit", [$impairment->id],['class' => 'btn btn-sm btn-secondary', 'style' => 'font-weight:bold;' ])) !!}
                                            &nbsp;
                                                {!! HTML::decode(link_to_route('backend.assessment.impairment.delete', "<i class='icon fa fa-remove' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.delete'), [$impairment->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'Are you sure to delete Impairment Assessment', 'class' => 'btn btn-sm btn-danger']))  !!}
                                            </span>
                                        @endif

                                        {{--<h6></h6>--}}
                                        {{--<h6>{!! $wf_track->username_completed_formatted !!}</h6>--}}
                                        <div>{!! $impairment->comments !!}</div>
                                        <br/>
                                        <div><small>Assessment Region&nbsp;:&nbsp;<span class="underline">{{ $impairment->region()->count() ? $impairment->region->name : '-' }}</span></small></div>
                                        <div><small>User Created&nbsp;:&nbsp;<span class="underline">{{ $impairment->officerCreated->name }}</span></small></div>
                                        @if ($impairment->edits)
                                            <div>Modifications:</div>
                                          @php
                                           $edits = json_decode($impairment->edits);
                                          @endphp
                                        @foreach ($edits as $edit)
                                                <div><small>{{ $edit->user }}&nbsp;:&nbsp; <span class="underline">{{ $edit->date }}</span></small></div>
                                        @endforeach

                                        @endif
                                        {{--<p>Level&nbsp;:&nbsp;&nbsp;<label class="tag tag-pill tag-primary">{{ $wf_track->wfDefinition->level }}</label></p>
                                        <p>Status&nbsp;:&nbsp;&nbsp;&nbsp;<label class="underline">{!! $wf_track->status_narration_label !!}</label></p>--}}
                                    </div>
                                </div>

                        </div>
                    </div>
                    <div class="dashboard-action">
                        <ul class="right-action float-xs-right">
                            <li data-widget="collapse"><a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus" style="font-size: 18px;"></span></a></li>
                            {{--<li data-widget="close"><a href="javascript:void(0)"><span aria-hidden="true" class="icon_close"></span></a></li>--}}
                        </ul>
                    </div>
                </div>
        @endforeach
    </div>
</div>