@extends('layouts.backend.main', ['title' => "PD Calculator", 'header_title' => "PD Calculator"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="offset-md-3 col-md-6">
            {{--PD Calculator (First Schedule)--}}
            <legend class="grey_modal" id="pd_calculator" >PD Calculator (Guideline)</legend>
            <br/>
            <div class="underline">Volume One</div>
            <br/>
            {{--Whole Person Impairment (WPI)--}}
            <div class="fileld-layout">
                <label>Whole Person Impairment (WPI)</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('wpi', NULL, ['class' => 'form-control', 'id' => 'wpi', 'validate-message' => 'Please fill Whole Person Impairment (WPI)']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Should range from 0 - 100</small>
                    {!! $errors->first('wpi', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="underline">Volume Two (Disability Conversion Sheets)</div>
            <br/>
            {{--PD Body Part Injury--}}
            <div class="fileld-layout">
                <label class="required">PD Body Part Injured => FEC Rank</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('pd_injury_id', $pd_body_part_injuries, NULL, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'pd_injury_id', 'validate-message' => 'Please fill PD Body Part Injury']) !!}
                    </div>
                    {!! $errors->first('pd_injury_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--PD Impairment--}}
            <div class="fileld-layout">
                <label class="required">PD Impairment</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('pd_impairment_id', $pd_impairments, NULL, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'pd_impairment_id', 'validate-message' => 'Please fill PD Impairment']) !!}
                    </div>
                    {!! $errors->first('pd_impairment_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--Age--}}
            <div class="fileld-layout">
                <label class="required">Age</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('age_display', NULL, ['class' => 'form-control', 'id' => 'old_val']) !!}
                    </div>
                </div>
            </div>
            {{--PD Occupation--}}
            <div class="fileld-layout">
                <label class="required">PD Occupation</label>
                <div class="form-group">
                    {!! Form::select('occupation_id',  [], NULL, ['class' => 'pd-occupation-select', 'style' => 'width:100%', 'id' => 'pd-occupation-select', 'validate-message' => 'Please fill PD Occupation']) !!}
                    {!! $errors->first('occupation_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

            <span class="help-block label label-danger" id="calculate_pd_error_msg" style="display: none;"></span>
            <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                <tr>
                    <th width="300px"><a href="#" id="calculate_pd_schedule_one" class="btn btn-small btn-secondary">Calculate PD</a></th>
                    <th class="underline" id="schedule_one_calculated_pd" style="font-size: 54px;"></th>
                </tr>
                <tr>
                    <th colspan="2" id="derived_schedule_one_calculated_pd" style="text-align: center;"></th>
                </tr>
                </tbody>
            </table>

            <hr/>

        </div>
    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function () {
        let $body = $('body');
        let $pd_occupation_select = $(".pd-occupation-select");
        $('.search-select').select2();
        $body.on('click', 'a#calculate_pd_schedule_one', function (e) {
            e.preventDefault();
            let $wpi = $("#wpi");
            let $pd_injury_id = $("#pd_injury_id");
            let $pd_impairment_id = $("#pd_impairment_id");
            let $old = $("#old_val");
            let $wpi_val = $wpi.val();
            if ($wpi_val == 0) {
                $("#schedule_one_calculated_pd").text(0);
                $("#derived_schedule_one_calculated_pd").text("");
            } else if ($wpi_val >= 0 && $wpi_val <= 100) {
                if (validate_entry($wpi.val(), $wpi) && validate_entry($pd_injury_id.val(), $pd_injury_id) && validate_entry($pd_occupation_select.val(), $pd_occupation_select) && validate_entry($pd_impairment_id.val(), $pd_impairment_id)) {
                    /* start : Calculate PD from Conversion Sheets  */
                    $.post("{!! route('backend.claim.notification_report.pd_assessment.schedule_one_calculator') !!}", {wpi:$wpi.val(),pd_injury_id:$pd_injury_id.val(),occupation_id:$pd_occupation_select.val(),pd_impairment_id:$pd_impairment_id.val(),age:$old.val()}, function( $data ) {
                        $("#schedule_one_calculated_pd").text($data.pd);
                        $("#derived_schedule_one_calculated_pd").text($data.derived_pd);
                    }, "json").done(function () {

                    }).fail(function () {

                    });
                    /* end : Calculate PD from Conversion Sheets  */
                }
            } else {
                $("#schedule_one_calculated_pd").text("");
                $("#derived_schedule_one_calculated_pd").text("");
            }

        });
        $pd_occupation_select.select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.claim.notification_report.pd_assessment.occupations') !!}",
                dataType: 'json',
                delay: 250,
                method: 'post',
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
    })
    function validate_entry($selected_entries, $control) {
        if (!$selected_entries) {
            $control.addClass('form-error');

            let $calculate_pd_error_msg = $("#calculate_pd_error_msg");
            $calculate_pd_error_msg.show();
            $calculate_pd_error_msg.text($control.attr("validate-message"));
            setTimeout(
                function() {
                    $control.removeClass('form-error');
                    $calculate_pd_error_msg.text("").hide();
                },
                2000
            );
            return false;
        } else {
            return true;
        }
    }
</script>
@endpush