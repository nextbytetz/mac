@extends('layouts.backend.main', ['title' => 'Assessment', 'header_title' => 'Assessment'])


@section('content')

    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">

                {{--item1--}}
                <ul class="list-unstyled">
                    <a href="{{ route('backend.assessment.impairment.all') }}">
                    {{--<a href="#">--}}
                        {{--<a href="#">--}}
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-italic"> </i><large>&nbsp;&nbsp;Impairment Assessment</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Pre-assessment data</p> </li>
                    </a>

                </ul>
                <br/>

                {{--item2--}}
                <ul class="list-unstyled">
                    <a href="{{ route('backend.assessment.performance_report') }}">
                    {{--<a href="#">--}}
                        {{--<a href="#">--}}
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-pie-chart"> </i><large>&nbsp;&nbsp;Performance Report</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Assessed Benefits, Pending Benefits for assessments</p> </li>
                    </a>

                </ul>
                <br/>

            </div>
        </div>

        {{--right div--}}

        <div class="col-sm-6 col-md-6">
            <div class="list-group">

                {{--item1--}}
                <ul class="list-unstyled">
                    <a href="{{ route('backend.assessment.pd_calculator') }}">
                    {{--<a href="#">--}}
                        {{--<a href="#">--}}
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-calculator"> </i><large>&nbsp;&nbsp;PD Calculator</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Simulate PD Calculation</p> </li>
                    </a>

                </ul>
                <br/>

            </div>
        </div>

    </div>


@stop

@push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>;

@endpush