
<div class = "row">
    <div class="col-md-12">
        <div class="col-md-8">
            <div class = "row">
                <div class="col-md-12">
                    {{--Document Preview--}}
                    <legend>Document Preview</legend>
                    <br/>
                    <div id="document_frame" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>


                </div>

            </div>
        </div>
        <div class="col-md-4">
           @if($billing->current_wf_level == 14 && $billing->payment_advice == 1)
           <span>
            <a href="{!! route("backend.assessment.hcp_hsp.update_payment_advice", ['hsp_billing_id'=>$billing->id,'level'=>$billing->current_wf_level]) !!}"  class="btn btn-primary site-btn nav_button">
                <i class="icon fa fa-print" aria-hidden="true"></i>Print</a>
            </span>
            @endif
            @if($billing->current_wf_level == 13 && $billing->payment_advice == 0)
            <span>
                {{-- {!! route("backend.assessment.hcp_hsp.update_payment_advice", ['hsp_billing_id'=>$billing->id,'level'=>$billing->current_wf_level]) !!} --}}
                <a href="{!! route("backend.assessment.hcp_hsp.update_payment_advice", ['hsp_billing_id'=>$billing->id,'level'=>$billing->current_wf_level]) !!}"  class="btn btn-secondary site-btn">
                    <i class="icon fa fa-file-pdf-o" aria-hidden="true"></i>Make PDF</a>
                </span>
                @endif
                @include('backend.operation.claim.assessment.hcp_hsp.billing.includes.sidebar_summary')
            </div>
        </div>
    </div>





    @push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script  type="text/javascript">


        $(function () {

            $(".search-select").select2();

            let $document_frame = $("#document_frame");
            get_attached_document().done(function ($data) {
                $document_frame.find("iframe").remove();
                let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                $document_frame.append($iframe);
            });

            function get_attached_document() {
                return $.ajax({
                    url: base_url + "/assessment/hcp_hsp/payment_advice/{{$billing->id}}",
                    dataType : 'json',
                    async : false,
                    method : "POST"
                });
            }


        });
    </script>;

    @endpush
