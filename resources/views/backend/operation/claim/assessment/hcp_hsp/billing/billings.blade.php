@extends('layouts.backend.main', ['title' => trans('HCP /HSP Billing'), 'header_title' => trans('HCP / HSP Billing')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')
    <legend></legend>
    <br/>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="display" cellspacing="0" width="100%" id ="notification-report-table">
                <thead>
                <tr>
                    <th>Service Provider</th>
                    <th>Period</th>
                    <th>total billed Amount</th>
                    <th>total files</th>
                    <th>total vetted Amount</th>
                    <th>total vetted files</th>
                    <th>Amount Difference</th>
                    <th>batch no</th>
                    <th>Status</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@stop

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";

        var $oTable = $('#notification-report-table').DataTable({
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                $oTable.buttons().container().insertBefore('#notification-report-table');
                $("#notification-report-table").css("width","100%");
            },
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route("backend.assessment.hcp_hsp.get_billing") !!}',
                type : 'get',
                data: function ($d) {
                }
            },

            columns: [
                { data: 'shdescription' , name: 'shdescription' ,orderable : true, searchable : true},
                { data: 'date' , name: 'date' ,orderable : true, searchable : true},
                { data: 'total_amount_claimed' , name: 'total_amount_claimed' ,orderable : true, searchable : true},
                { data: 'total_files_claimed' , name: 'total_files_claimed'},
                { data: 'total_amount_vetted' , name: 'total_amount_vetted', orderable : false, searchable : false},
                { data: 'total_files_vetted' , name: 'total_files_vetted', orderable : true, searchable : true},
                { data: 'price_difference' , name: 'price_difference', orderable : true, searchable : true},
                { data: 'batchno' , name: 'batchno'},
                { data: 'status' , name: 'status', orderable : false, searchable : true},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = '{!! url("assessment/hcp_hsp/billing_profile") !!}/' + aData['billing_id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
        $('#search-recall-notification-form').on('submit', function($e) {
            $oTable.draw();
            $e.preventDefault();
        });
    });
</script>

@endpush