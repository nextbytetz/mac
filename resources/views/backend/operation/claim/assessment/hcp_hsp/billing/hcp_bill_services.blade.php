@extends('layouts.backend.main', ['title' => trans('HCP / HSP Services'), 'header_title' => trans('HCP / HSP Services')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
<style>
    /* start: upload progress bar css */
    .progress-bar {
        background-color: #12CC1A;
        height:20px;
        color: #FFFFFF;
        width:0%;
        -webkit-transition: width .3s;
        -moz-transition: width .3s;
        transition: width .3s;
    }
    .progress-div {
        border:#0FA015 1px solid;
        padding: 5px 0px;
        margin:30px 0px;
        border-radius:4px;
        text-align:center;
    }
    /* end: upload progress bar css */
    tr {
        border-bottom:1pt solid rgba(0, 0, 0, 0.12);
    }
</style>
@endpush

@section('content')


<div class = "row">
    {{-- @include("backend/operation/claim/assessment/hcp_hsp/includes/header_info",['notification_report'=> $notification_report]) --}}
</div>
{{--Tabs navigation--}}
<div class = "row">
    <div class="col-md-12">

        <div class="basic_nav_pills nav_basic_tab" >
            <ul class="nav nav-tabs">
                {{--General--}}
                <li class="nav-item">
                    <a class="nav-link active" href="#general"
                    data-toggle="tab">Services
                    </a>
                </li>
            </ul>
        <div class="nav_tab_contain tab-content">
            <div id="general" class="nav_tab_pane tab-pane active in">
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >

                            <div class="pull-right" >
                                {{-- <span>
                                 <a href="" class="btn btn-secondary site-btn nav_button"><i class='icon fa fa-check-circle' aria-hidden='true'></i> Submit</a>
                             </span> --}}

                         </div>
                    </div>
                </div>
            </div>
            <div class = "row">
                <div class="col-md-12">

                    <div class="col-md-9">
                        @include('backend.operation.claim.assessment.hcp_hsp.billing.includes.vetting_billing')
                    </div>
                    <div class="col-md-3">
                        @include('backend.operation.claim.assessment.hcp_hsp.billing.includes.vetting_sidebar_summary')
                    </div>
                </div>
            </div>
        </div>  
    </div>

</div>
</div>

</div>

@stop


@push('after-script-end')

@stack('medical-expenses-overview-script-end')
@stack('investigation-report-script-end')
@stack('investigators-script-end')
@stack('health-provider-services-script-end')
@stack('disability-state-script-end')
@stack('health-state-script-end')
@stack('bank-details-script-end')
@stack('dependents-script-end')
@stack('vetting-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
{{ Html::script(asset_url(). "/nextbyte/plugins/clipboard/clipboard.min.js") }}

<script  type="text/javascript">

       // $(function () {
           // $(document).ready(function(){
            $(".search-select").select2();

            new ClipboardJS('.copy_approval_note');

            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show').trigger('click');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var $tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + $tab);
                } else {
                    location.hash = '#' + $tab;
                }
            });
    </script>

    @endpush
