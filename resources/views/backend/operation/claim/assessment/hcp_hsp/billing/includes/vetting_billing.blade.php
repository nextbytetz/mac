
@push('after-styles-end')
<style type="text/css">
    #vetting_table input,#vetting_table textarea, #price_difference, #quantity_difference, #vetted_amount, #vetted_items ,#vetting_table select {
      font-size:14px;
      padding:5px 5 5px 5px;
      display:block;
      width:100%;
      border:none;
      background: transparent;

  }

  #vetting_table input:focus, #vetting_table textarea:focus, #vetted_amount input:focus,#price_difference input:focus, #quantity_difference input:focus, #vetted_items input:focus, #vetting_table select:focus  { outline:none; border-bottom:1px solid #757575;}
  input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend class="grey_modal" >@lang('Vetting')</legend>
        </div>
        <div>&nbsp;</div>
        <div class="col-md-12">
            <div class="light_grey_bg" style="padding-top: 10px;">
                <div class="container table-responsive">
                    <table class="table table-striped table-bordered table-responsive" id="vetting_table" style="width:100%">
                        <tbody>
                            <tr>
                                <th>Item Name</th>
                                <th>Quantity</th>
                                <th>Billed Unit Cost</th>
                                <th>Total Billed Cost</th>
                                <th>Unit Pricelist</th>
                                <th>Total Pricelist</th>
                                <th>Vetted Quantity</th>
                                <th>Vetted Amount</th>
                                <th>Deducted Amount</th>
                                <th>Remarks</th>
                                <th>Include In Bills</th>
                            </tr>
                            @php
                            $bill_repo = new App\Repositories\Backend\Operation\Claim\HspHcpBillingRepository();
                            $total_hsp_qt = 0;
                            $total_hsp_amount = 0.00;
                            $total_hcp_amount = 0.00;
                            $total_vetted_qt = 0;
                            $total_vetted_amount = 0.00;

                            @endphp
                            @foreach($services as $service)
                            @php
                            $facility = $bill_repo->getFacility($service);
                            $total_hsp_qt += $service->quantity;
                            $total_hsp_amount += $service->price;
                            $total_hcp_amount += ($facility->unit_price * $service->quantity);
                            $total_vetted_qt += $service->vetted_quantity;
                            $total_vetted_amount += $service->vetted_price;

                            @endphp
                            <tr>
                                <td>
                                    {{$facility->item_name}}
                                </td>
                                <td>{{$service->quantity}}</td>
                                <td>{{number_format($service->unit_price, 2)}}</td>
                                <td><input type="text" class="billed_price" value="{{number_format($service->price, 2)}}" disabled></td>
                                <td><input type="text" class="unit_price" value="{{number_format($facility->unit_price, 2)}}" disabled></td>
                                <td><input type="text" class="facility_price" value="{{number_format(($facility->unit_price * $service->quantity) , 2)}}" disabled></td> 
                                <td><input type="number" serviceId="{{$service->id}}" name="vetted_quantity" class="vetted vQt" value="{{$service->vetted_quantity}}" {{$wf_level != 6  ? 'disabled' : ''}}></td>
                                <td><input type="text" serviceId="{{$service->id}}" name="vetted_price" class="number money vetted vP" value="{{number_format($service->vetted_price, 2)}}" disabled></td>
                                <td><input type="text" serviceId="{{$service->id}}" name="pricedifference" class="number money price_difference" value="{{number_format($service->price_defference, 2)}}" disabled></td>
                                <td><textarea type="text" serviceId="{{$service->id}}" name="remarks" class="vetted_remarks" {{$wf_level != 6  ? 'disabled' : ''}}>{{$service->remarks}}</textarea></td>

                                <td><input type="checkbox" serviceId="{{$service->id}}" name="is_workrelated" class="work_related" data-toggle="tooltip" data-placement="top" title="Check to include in bills" {{$service->is_workrelated ? 'checked' : ''}} {{$wf_level != 6  ? 'disabled' : ''}}></td>
                            </tr>
                            @endforeach
                            <tr>
                                <th>Totals</th>
                                <th>{{$total_hsp_qt}}</th>
                                <th></th>
                                <th>{{number_format($total_hsp_amount, 2)}}</th>
                                <th></th>
                                <th>{{number_format($total_hcp_amount, 2)}}</th>
                                <th><input type="text" id="vettedPrice" class="" value="{{$total_vetted_qt}}" disabled></th>
                                <th><input type="text" id="vettedAmount" class="money" value="{{number_format($total_vetted_amount, 2)}}" disabled></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                <td colspan="11">
                                    <div class="pull-right" >
                                        <span>
                                           <button class="btn btn-secondary site-btn nav_button" id="submit_review" {{$wf_level != 6 ? 'disabled' : ''}}><i class='icon fa fa-check-circle' aria-hidden='true' ></i> Submit</button>
                                       </span>
                                   </div>
                               </td>
                           </tr>
                       </tbody></table>
                   </div>
               </div>
           </div>
       </div>
   </div>

   @push('vetting-script-end')
   {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
   {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
   {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

   <script  type="text/javascript">
       $(document).ready(function(){
        calculateTotal(Value = "", name = "")

        $('table tr td #submit_review').click( function(e){
            e.preventDefault()

            var detail_id = "{{$bill_details->id}}";
            var urls = "{{url('assessment/hcp_hsp/get_total_bill_items')}}/" + detail_id;
            $.ajax({
                url: urls,
                method: 'get',
                success: function(data){

                    $('#vetted_amount').val(data.price)
                    $('#vetted_items').val(data.vetted_items)
                    swal({
                        title: "Attention! {{access()->user()->firstname}}",
                        text: "<strong> Are you sure you want to Submit Bill of TZS " + data.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " with " + data.vetted_items + " items for Reviewed !?</strong>",
                        type: "info",
                        html:true,
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonColor: "#E24848",
                        cancelButtonColor: "#10C888",
                        confirmButtonText: "YES",
                        cancelButtonText: "NO"
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url : '{{ url("assessment/hcp_hsp/post_total_bill_items") }}'+'/'+detail_id,
                                type : 'GET',
                                datatype : 'json',
                                success:function(data){
                                    if (data.success == true) {
                                        swal({
                                            title: "Success!",
                                            text: data.message,
                                            type: "success",
                                            confirmButtonClass: 'btn btn-primary',
                                            confirmButtonText: 'OK!'
                                        },
                                        function(){ location.reload(); });
                                    }
                                    else{
                                        swal({
                                            title: "Error Perfoming Request",
                                            text: data.message,
                                            type: "error",
                                            showCancelButton: false,
                                            buttonsStyling: false,
                                            confirmButtonClass: "btn btn-danger",
                                            confirmButtonText: 'OK!'
                                        },
                                        function(){ location.reload(); });
                                    }
                                }

                            });
                        }
                    });
                }
            })            
        });

        $('.work_related').change(function (e) {
             var Value = e.currentTarget.value;
             var Id = e.target.attributes[1].nodeValue
             if(this.checked) {
                 Value = true;
                 this.setAttribute('checked','checked')
             }else{
                Value = false;
                this.removeAttribute('checked')

             }

             postVettedTotal(Id, Value, name);

        });
        $('#vetting_table').on('keyup', '.vQt', function(e){
            var current_value = e.currentTarget.value;
            current_value = (current_value == '' ||  current_value == NaN) ? 0 : current_value
            var Id = e.target.attributes[1].nodeValue
            var name = e.target.attributes[2].nodeValue  
            var unit_price = $(this).parent().siblings('tr td').next('td').next('td').find('.unit_price').val()
            var total_price = 0 
            var price_difference = 0;
            var total_billed_price = $(this).parent().siblings('tr td').next('td').next('td').next('td').find('.billed_price').val()
            
            total_price = parseFloat(unit_price.replace(/,/g, "")) * current_value
            price_difference = parseFloat(total_billed_price.replace(/,/g, "")) - parseFloat(total_price)

            $(this).closest('td').next('td').find('.vP').val(total_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
            $(this).closest('td').next('td').next('td').find('.price_difference').val(price_difference.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
            if ($(this).closest('td').next('td').next('td').next('td').next('td').find('.work_related').attr('checked')) {
                postVettedTotal(Id, true, name);
            }else{
                postVettedTotal(Id, false, name);
            }

            postVettedQA(Id, current_value, 'vetted_quantity');
            postVettedQA(Id, total_price, 'vetted_price');

            setTimeout(function(){ 
                calculateTotal(Value, 'vetted_quantity')
                calculateTotal(Value, 'vetted_price')
            }, 50);
        })

        $('#vetting_table').on('blur','.vetted_remarks', function (e) {
            var Value = e.currentTarget.value;
            var Id = e.target.attributes[1].nodeValue
            var name = e.target.attributes[2].nodeValue            
            postVettedRemarks(Id, Value, name);            

        });


        // $('#vetting_table').on('blur','.vetted', function (e) {
        //     var Value = e.currentTarget.value;
        //     var Id = e.target.attributes[1].nodeValue
        //     var name = e.target.attributes[2].nodeValue
            
        //     postVettedQA(Id, Value, name);
        //     setTimeout(function(){ calculateTotal(Value, name) }, 50);
            

        // });

        function postVettedTotal(Id,Value){
            var detail_id = "{{$bill_details->id}}";
            var urls = "{{url('assessment/hcp_hsp/post_inclusion_services')}}/" + Id + "/" + detail_id;
            var token = "{{csrf_token()}}";
            $.ajax({
                url: urls,
                method: 'post',
                data: {
                    'Value' : Value,
                    '_token' : token
                },
                success: function(data){
                    $('#vetted_amount').val(data.price)
                    $('#vetted_items').val(data.vetted_items)
                    $('#price_difference').val(data.price_difference)
                    // $('#quantity_difference').val(data.quantity_difference)
                }
            })
        }


        function calculateTotal(Value, name){
            var detail_id = "{{$bill_details->id}}";
            var urls = "{{url('assessment/hcp_hsp/calculate_total_service')}}/" + detail_id;
            if (Value !== "") {
                $.ajax({
                    url: urls,
                    method: 'get',
                    success: function(data){
                        $('#vettedPrice').val(data.quantity)
                        $('#vettedAmount').val(data.price)
                    }
                })
            }else{
                $.ajax({
                    url: urls,
                    method: 'get',
                    success: function(data){
                        $('#vettedPrice').val(data.quantity)
                        $('#vettedAmount').val(data.price)
                        
                    }
                })
            }
        }

        function postVettedRemarks(Id,Value,name){
            var detail_id = "{{$bill_details->id}}";
            var urls = "{{url('assessment/hcp_hsp/post_remarks')}}/" + Id + "/" + detail_id;
            var token = "{{csrf_token()}}";
            $.ajax({
                url: urls,
                method: 'post',
                data: {
                    'Value' : Value,
                    'name' :name,
                    '_token' : token
                },
                success: function(data){
                    // $('#vetted_amount').val(data.price)
                }
            })
        }

        function postVettedQA(Id,Value,name){
            var detail_id = "{{$bill_details->id}}";
            var urls = "{{url('assessment/hcp_hsp/post_bill_services')}}/" + Id + "/" + detail_id;
            var token = "{{csrf_token()}}";
            if (Value !== "") {
                $.ajax({
                    url: urls,
                    method: 'post',
                    data: {
                        'Value' : Value,
                        'name' :name,
                        '_token' : token
                    },
                    success: function(data){
                        $('#vetted_amount').val(data.price)
                        $('#price_difference').val(data.price_difference)
                        // $('#quantity_difference').val(data.quantity_difference)

                    }
                })
            }
        }

        $(".number").keydown(function (e) { number_only(e)});
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });
        function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
        return;
    }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
})
</script>

@endpush