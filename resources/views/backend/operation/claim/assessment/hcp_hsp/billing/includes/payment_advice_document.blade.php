<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <meta charset="utf-8" />
    <title>Annual Return - {{$employerProfile[1]['name']}}</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="WCF PORTAL"/>
    <meta name="keywords" content="certificate"/>
    <meta name="date" content="2017-08-04"/>

    {{ Html::style(asset_url() . "/nextbyte/css/payment_advice.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
    {{ Html::style(asset_url() . "/global/css/bootstrap.min.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
    {{ Html::style(asset_url() . "/nextbyte/css/bill.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}


    <style type="text/css">
        body .container{
            /*font-family: Arial-BoldMT;*/
            background-color: #ffffff;
        }
        @page { 
            margin: 1% 2%; 
            size: A4 potrait;
        }
        .dotted{
          border-bottom: 0.08em dashed #000000;
          text-decoration: none; 
      }


    /*.borderless .table {
        border-bottom:0px !important;
        }*/
        .borderless {
            border: 0px !important;
        }

    </style>

</head>
<body>
    <div class="container">
        <div class="row">
         <div class="col-sm-12 text-center pt-2 mt-2" >
            <img src="{!! asset_url() . "/nextbyte/img/wcf_big_logo_no_background.png"!!}" style="height: 90px;">
        </div>
    </div>
    <div class="row">
     <div class="col-sm-12">
        <div class="center">
            <div class="h6 font-weight-bold" style="font-family: Arial-BoldMT; ">Payment Advice To Health Care / Service Provider</div>
            <hr class="style1" style="width: 10%;">
        </div>
    </div>
</div>
<div class="row  mb-2 mt-2">
    <div class="col-sm-12 mb-2"  style="font-family: ArialMT;">
        <span class="font-weight-bold">Name Of Service Provider: </span>{!!$hsp_bill->ApiStakeholder->shdescription!!}.
    </div>
</div>

<div class="row">
    <div class="col-sm-12" style="font-family: ArialMT;">

        <table class="table table-bordered align-middle text-align-center">
            <thead>
                <tr >
                    <th scope="col" class="align-middle">Batch No#</th>
                    <th scope="col" class="align-middle">Period</th>
                    <th scope="col" class="align-middle" style="text-align: center;">Billed Amount</th>
                    <th scope="col" class="align-middle" style="text-align: center;">Vetted/Payable Amount</th>
                    <th scope="col" class="align-middle" style="text-align: center;">Deducted Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   <td class="align-middle" style="text-align: center;">{{$hsp_bill->batchno}}</td>
                   <td>{{\Carbon\Carbon::parse($hsp_bill->year.'-'.$hsp_bill->month)->format('F, Y')}}</td>
                   <td class="align-middle" style="text-align: center;">{{number_format($hsp_bill->total_amount_claimed,2)}}</td>
                   <td class="align-middle" style="text-align: center;">{{number_format($hsp_bill->total_amount_vetted,2)}}</td>
                   <td class="align-middle" style="text-align: center;">
                    <?php $difference = $hsp_bill->total_amount_claimed-$hsp_bill->total_amount_vetted;?>
                    {{ $difference > 0 ? number_format($difference,2) : number_format(0,2)}}
                </td>
            </tr>
        </tbody>
    </table>
</div>
</div>

<div class="row pt-1 text-justify" style="font-family: 'Times New Roman';">
    <div class="col-sm-12">
        <p class="font-weight-bold">Note:</p>
        <p class="h6">You are requested to issue the control number / submit a fiscalised receipt to finalize payment for the vetted / payable amount. The requested control number / fiscalised receipt should be scanned and submitted through email of the sender.</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">&nbsp;</div>
</div>
<div class="row" >
    <div class="col-sm-12" style="font-family: ArialMT;">
        <table class="table borderless" >
            <tr>
                <td class="borderless" width="70%">Bill vetted by: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{!empty($vetted_by) ? $vetted_by : ''}} </td>
                <td class="borderless dotted">Date: &nbsp; <hr class="style9" style="width: 70%;"></td>
            </tr>
            <tr>
                <td class="borderless" width="70%">Bill reviewed by:  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{{!empty($reviewed_by) ? $reviewed_by : ''}}</td>
                <td class="borderless">Date: &nbsp; <hr class="style9" style="width: 70%;"></td>
            </tr>
            <tr>
                <td class="borderless" width="70%">Payment prepared by:  &nbsp;&nbsp;{{!empty($prepared_by) ? $prepared_by : ''}}</td>
                <td class="borderless">Date:  &nbsp; <hr class="style9" style="width: 70%;"></td>
            </tr>
        </table>

    </div>
</div>

</div>




<script type="text/javascript">
    {{-- window.onload = function() { --}}
        // window.print();
        // window.close();
    // };
    //     window.onfocus=function(){ window.close();}

</script>
</body>
</html>