@extends('layouts.backend.main', ['title' => trans('HCP / HSP Profile'), 'header_title' => trans('HCP / HSP Profile')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
<style>
    /* start: upload progress bar css */
    .progress-bar {
        background-color: #12CC1A;
        height:20px;
        color: #FFFFFF;
        width:0%;
        -webkit-transition: width .3s;
        -moz-transition: width .3s;
        transition: width .3s;
    }
    .progress-div {
        border:#0FA015 1px solid;
        padding: 5px 0px;
        margin:30px 0px;
        border-radius:4px;
        text-align:center;
    }
    /* end: upload progress bar css */
    tr {
        border-bottom:1pt solid rgba(0, 0, 0, 0.12);
    }
</style>
@endpush

@section('content')


<div class = "row">
    {{-- @include("backend/operation/claim/assessment/hcp_hsp/includes/header_info",['notification_report'=> $notification_report]) --}}
</div>
{{--Tabs navigation--}}
<div class = "row">
    <div class="col-md-12">

        <div class="basic_nav_pills nav_basic_tab" >
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#general"
                    data-toggle="tab">@lang('labels.general.general')
                </a></li>
                @if($billing->wf_status && $billing->wf_done)
                <li class="nav-item">
                    <a class="nav-link" id="workflow_history_tab" data-toggle="tab" href="#workflow_history" role="tab">
                        <i class="icon fa fa-history" aria-hidden="true"></i>Workflow History
                    </a>
                </li>
                @endif
                @if($billing->wf_done || $billing->current_wf_level >= 13)
                <li class="nav-item">
                    <a class="nav-link" href="#payment_advice" data-toggle="tab">
                        <i class="icon fa fa-file-text-o" aria-hidden="true"></i>&nbsp;Payment Advice
                    </a>
                </li>
                @endif
            </ul>
            <div class="nav_tab_contain tab-content">
                <div id="general" class="nav_tab_pane tab-pane active in">
                    <div class="nav_tab_pane_header">
                        <div class="row">
                            <div class="col-md-12" >
                                @if($billing->wf_status == 0)
                                <div class="pull-right" >
                                    <span>
                                        <a href="{!! route('backend.assessment.hcp_hsp.initiate_workflow',$billing->id)!!}" class="btn btn-primary site-btn nav_button"><i class='icon fa fa-check-circle' aria-hidden='true'></i> Initiate Workflows</a>
                                    </span>

                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class = "row">
                        <div class="col-md-12">
                            <div class="col-md-9">
                                @if($billing->wf_status > 0 && $billing->wf_done == 0)
                                <div class="dashboard-header content">
                                    <div class="dashboard-action">
                                        <ul class="right-action float-xs-right">
                                            <li data-widget="collapse">
                                                <a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus-06" style="font-size: 18px;"></span></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <legend class="light_grey_bg">Workflow Track</legend>
                                    <div class="dashboard-box content-list" style="display: block;">
                                        {{-- @if(!empty($profile->plan_status) && empty($profile->wf_done)) --}}
                                        <div id="workflow" class="nav_tab_pane tab-pane">
                                            @php
                                            $workflowinput = ['resource_id' =>1, 'wf_module_group_id'=> 35, 'type' => 0];
                                            @endphp
                                            @include("backend/includes/workflow/wf_track_html", $workflowinput)
                                            {{-- {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!} --}}

                                        </div>
                                        {{-- @endif --}}
                                    </div>
                                    <div class="dashboard-action">
                                        <ul class="right-action float-xs-right">
                                           <li data-widget="collapse">
                                            <a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus-06" style="font-size: 18px;"></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endif

                            @include('backend.operation.claim.assessment.hcp_hsp.billing.includes.billing_summaries')
                        </div>
                        <div class="col-md-3">
                            @include('backend.operation.claim.assessment.hcp_hsp.billing.includes.sidebar_summary')
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="workflow_history" role="tabpanel">
                <div class="row">
                    <div class="col-md-12 pills-height">
                        <div data-plugin="scrollbar" data-height="1500">
                            <div class="row">
                                <div class="col-md-8">
                                    @if(!empty($billing->wf_status) && $billing->wf_done)
                                    <legend class="grey_info mt-2" >Workflow</legend>
                                    <div id="plan_workflow" class="nav_tab_pane tab-pane">
                                        @php
                                        $workflowinput = ['resource_id' => $billing->id, 'wf_module_group_id'=> 35, 'type' => 0];
                                        @endphp
                                        @include("backend/includes/workflow/wf_track_html", $workflowinput)
                                        {{-- {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!} --}}
                                    </div>
                                    @else
                                    <div class="alert-left-border mt-2">
                                        <div class="alert alert-primary alert-dismissible fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                {{-- <span aria-hidden="true">&times;</span> --}}
                                            </button>
                                            <strong>No workflow history.</strong> This Bill has no associated workflow recorded.
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    @include('backend.operation.claim.assessment.hcp_hsp.billing.includes.sidebar_summary')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($billing->wf_done || $billing->current_wf_level >=13)
            <div id="payment_advice" class="nav_tab_pane tab-pane">
                <div class="row">
                    <div class="col-md-12">
                        @include('backend.operation.claim.assessment.hcp_hsp.billing.includes.payment_advice')
                    </div>
                </div>
            </div>
            @endif

        </div>  
    </div>

</div>
{{-- </div>

</div> --}}

@stop


@push('after-script-end')

@stack('medical-expenses-overview-script-end')
@stack('investigation-report-script-end')
@stack('investigators-script-end')
@stack('health-provider-services-script-end')
@stack('disability-state-script-end')
@stack('health-state-script-end')
@stack('bank-details-script-end')
@stack('dependents-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
{{ Html::script(asset_url(). "/nextbyte/plugins/clipboard/clipboard.min.js") }}

<script  type="text/javascript">

    $(".search-select").select2();

    new ClipboardJS('.copy_approval_note');

    if (location.hash !== '') {
        $('a[href="' + location.hash + '"]').tab('show').trigger('click');
    }
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var $tab = $(e.target).attr('href').substr(1);
        if (history.pushState) {
            history.pushState(null, null, '#' + $tab);
        } else {
            location.hash = '#' + $tab;
        }
    });
</script>

@endpush
