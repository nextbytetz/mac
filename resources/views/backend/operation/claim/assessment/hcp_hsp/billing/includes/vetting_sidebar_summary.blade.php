

{{--sidebar notification summary table--}}


<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Vetting Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            <tr>
                <td style="padding-left: 5px" width="130px">Patient:</td>
                <td  height="20px"><b>{!! Form::label( 'patient_names', $bill_details->patient_names, [ 'id'=> 'patient_names']) !!}</b></td>

            </tr>



            <tr>
                <td style="padding-left: 5px">Memeber No:</td>
                <td><b>{!! Form::label( 'memberno', $bill_details->memberno, [ 'id'=> 'memberno']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">WCF authorizationNo:</td>
                <td><b>{!! Form::label( 'wcf_authno', $bill_details->wcf_authno, [ 'id'=> 'wcf_authno']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">HSP authorizationNo: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills from HSP"></i></td>
                <td><b>{!! $bill_details->sh_authno !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Visit date: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Visiti date"></i></td>
                <td><b>{!! $bill_details->visit_date !!}</b></td>
            </tr>

           <tr>
                <td style="padding-left: 5px">Total Items: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills from HSP"></i></td>
                <td><b>{!! Form::label( '', $bill_details->bill_items ? $bill_details->bill_items : 0, [ 'id'=> 'bill_items']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Total Vetted Items: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills vetted by WCF"></i></td>
                <td><b><input type="text" name="vetted_items" value="{{$bill_details->vetted_items}}" id="vetted_items" disabled></b></td>
            </tr>
            {{-- <tr>
                <td style="padding-left: 5px">Item Difference: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills vetted by WCF"></i></td>
                <td><b><input type="text" name="quantity_difference" value="{{$bill_details->quantity_difference}}" id="quantity_difference" disabled></b></td>

            </tr> --}}

            <tr>
                <td style="padding-left: 5px">Total Bills: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills from HSP"></i></td>
                <td><b>{!! Form::label( '', number_format($bill_details->bill_amount, 2), [ 'id'=> 'bill_amount']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Total Vetted Bills: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills vetted by WCF"></i></td>
                <td><b><input type="text" name="vetted_amount" class="number money" value="{{$bill_details->vetted_amount}}" id="vetted_amount" disabled></b></td>
            </tr>
            <tr>
                <td style="padding-left: 5px">Bill Difference: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills Difference"></i></td>
                <td><b><input type="text" name="price_difference" class="number money" value="{{$bill_details->price_difference}}" id="price_difference" disabled></b></td>

            </tr>

        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush