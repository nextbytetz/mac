

{{--sidebar notification summary table--}}


<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Billing Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            <tr>
                <td style="padding-left: 5px" width="130px">Stakeholder:</td>
                <td  height="20px"><b>{!! Form::label( 'stakeholder', $billing->shdescription, [ 'id'=> 'stakeholder']) !!}</b></td>

            </tr>



            <tr>
                <td style="padding-left: 5px">Batch No:</td>
                <td><b>{!! Form::label( 'batchno', $billing->batchno, [ 'id'=> 'batchno']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Month:</td>
                <td><b>{!! Form::label( 'date', \Carbon\Carbon::parse($billing->date)->format('M Y'), [ 'id'=> 'date']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Total Bills: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills from HSP"></i></td>
                <td><b>{!! number_format($billing->total_amount_claimed, 2) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Total Files: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total files from HSP"></i></td>
                <td><b>{!! $billing->total_files_claimed !!}</b></td>
            </tr>



            <tr>
                <td style="padding-left: 5px">Total Vetted Bills: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total bills vetted by WCF"></i></td>
                <td><b>{!! number_format($billing->total_amount_vetted, 2) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">Total vetted Files: <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="total files vetted by WCF"></i></td>
                <td><b>{!! $billing->total_files_vetted !!}</b></td>
            </tr>

        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush