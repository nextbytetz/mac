@extends('layouts.backend.main', ['title' => ucwords(strtolower($facility->name)), 'header_title' => ucwords(strtolower($facility->name))])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
<style>
    /* start: upload progress bar css */
    .progress-bar {
        background-color: #12CC1A;
        height:20px;
        color: #FFFFFF;
        width:0%;
        -webkit-transition: width .3s;
        -moz-transition: width .3s;
        transition: width .3s;
    }
    .progress-div {
        border:#0FA015 1px solid;
        padding: 5px 0px;
        margin:30px 0px;
        border-radius:4px;
        text-align:center;
    }
    /* end: upload progress bar css */
    tr {
        border-bottom:1pt solid rgba(0, 0, 0, 0.12);
    }
</style>
@endpush

@section('content')


<div class = "row">
    {{-- @include("backend/operation/claim/assessment/hcp_hsp/includes/header_info",['notification_report'=> $notification_report]) --}}
</div>
{{--Tabs navigation--}}
<div class = "row">
    <div class="col-md-12">

        <div class="basic_nav_pills nav_basic_tab" >
            <ul class="nav nav-tabs">
                {{--General--}}
                <li class="nav-item">
                    <a class="nav-link active" href="#items"
                    data-toggle="tab">Items
                </a>
            </li>

        </ul>
        <div class="nav_tab_contain tab-content">
            <div id="items" class="nav_tab_pane tab-pane active in">
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="pull-right" >

                            </div>
                        </div>
                    </div>
                </div>
                <div class = "row">
                    <div class="col-md-12">
                        <div>&nbsp;</div>
                        <div class="col-md-9">
                            <table class="display" cellspacing="0" width="100%" id ="items-table">
                                <thead>
                                    <tr >
                                        <th>Item Name</th>
                                        <th>Item Code</th>
                                        <th>Dosage</th>
                                        <th>Maximum Quantity</th>
                                        <th>Unit Price</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        <div class="col-md-3">
                            @include('backend.operation.claim.assessment.hcp_hsp.includes.facility_summary')
                        </div>
                    </div>
                </div>
            </div>  
        </div>

    </div>
</div>

</div>

@stop


@push('after-script-end')

@stack('medical-expenses-overview-script-end')
@stack('investigation-report-script-end')
@stack('investigators-script-end')
@stack('health-provider-services-script-end')
@stack('disability-state-script-end')
@stack('health-state-script-end')
@stack('bank-details-script-end')
@stack('dependents-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
{{ Html::script(asset_url(). "/nextbyte/plugins/clipboard/clipboard.min.js") }}

<script  type="text/javascript">

       // $(function () {
         $(document).ready(function(){
            $(".search-select").select2();

            new ClipboardJS('.copy_approval_note');

            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show').trigger('click');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var $tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + $tab);
                } else {
                    location.hash = '#' + $tab;
                }
            });

            $('#items-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route("backend.assessment.hcp_hsp.get_facility_items", $facility->id) !!}',
                    type : 'get'
                },
                columns: [
                { data: 'item_name' , name: 'item_name'},
                { data: 'item_code', name: 'item_code'},
                { data: 'Maximum_quantity', name: 'Maximum_quantity'},
                { data: 'unit_price', name: 'unit_price'},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = "{{url('assessment/hcp_hsp/facility/')}}/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });
        });
    </script>;

    @endpush
