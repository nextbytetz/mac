@extends('layouts.backend.main', ['title' => trans('HCP /HSP Authorization'), 'header_title' => trans('HCP / HSP Authorization')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')
    <legend></legend>
    <br/>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="display" cellspacing="0" width="100%" id ="notification-report-table">
                <thead>
                <tr>
                    <th>@lang('labels.backend.legal.case_no')</th>
                    <th>MemberNo</th>
                    <th>AuthorizationNo</th>
                    <th>@lang('labels.backend.claim.incident_type')</th>
                    <th>@lang('labels.backend.compliance.employee')</th>
                    <th>@lang('labels.backend.compliance.employer')</th>
                    <th>@lang('labels.general.region')</th>
                    <th>@lang('labels.general.district')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@stop

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";

        var $oTable = $('#notification-report-table').DataTable({
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                $oTable.buttons().container().insertBefore('#notification-report-table');
                $("#notification-report-table").css("width","100%");
            },
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route("backend.assessment.hcp_hsp.get") !!}',
                type : 'get',
                data: function ($d) {
                }
            },

            columns: [
                { data: 'filename' , name: 'filename' ,orderable : true, searchable : true},
                { data: 'member_no' , name: 'member_no' ,orderable : true, searchable : true},
                { data: 'authorization_no' , name: 'authorization_no' ,orderable : true, searchable : true},
                { data: 'incident_type' , name: 'incident_types.name'},
                { data: 'employee' , name: 'employee', orderable : false, searchable : false},
                { data: 'employer' , name: 'employers.name', orderable : true, searchable : true},
                { data: 'region' , name: 'region'},
                { data: 'district' , name: 'district', orderable : false, searchable : true},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = '{!! url("assessment/hcp_hsp/profile") !!}/' + aData['Id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
        $('#search-recall-notification-form').on('submit', function($e) {
            $oTable.draw();
            $e.preventDefault();
        });
    });
</script>

@endpush