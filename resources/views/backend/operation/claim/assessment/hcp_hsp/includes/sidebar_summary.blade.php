

{{--sidebar notification summary table--}}


<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.claim.employee_summary')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            <tr>
                <td style="padding-left: 5px" width="130px">@lang('labels.general.employee_name'):</td>
                <td  height="20px"><b>{!! Form::label( 'employee_name', $employee->name, [ 'id'=> 'employee_name']) !!}</b></td>

            </tr>



            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.memberno'):</td>
                <td><b>{!! Form::label( 'memberno', $employee->memberno, [ 'id'=> 'memberno']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">@lang('labels.general.dob'):</td>
                <td><b>{!! Form::label( 'dob', $employee->dob_formatted, [ 'id'=> 'dob']) !!}</b></td>

            </tr>

            {{--age--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.age'): <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.age_description')  "></i></td>
                <td><b>{!! Form::label( 'age', $age, [ 'id'=> 'age']) !!}</b></td>

            </tr>

                       <tr>
                <td style="padding-left: 5px">@lang('labels.general.gender'):</td>
                <td><b>{!! Form::label( 'gender', ($employee->gender()->count()) ? $employee->gender->name : ' ', [ 'id'=> 'gender']) !!}</b></td>

            </tr>



            <tr>
                <td style="padding-left: 5px">@lang('labels.general.occupation'):</td>
                <td><b>{!! Form::label( 'occupation', ($employee->jobTitle()->count()) ? $employee->jobTitle->name : ' ', [ 'id'=> 'occupation']) !!}</b></td>

            </tr>




            {{--employer--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.employer'):</td>
                <td><b>{!! Form::label( 'employer', ($employee->employers) ? $employer->name : ' ', [ 'id'=> 'employer']) !!}</b></td>

            </tr>


            {{--employer reg no--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.employer_reg_no'):</td>
                <td><b>{!! Form::label( 'incident_type',($employee->employers) ?  $employer->reg_no : ' ', [ 'id'=> 'incident_type']) !!}</b></td>

            </tr>

        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush