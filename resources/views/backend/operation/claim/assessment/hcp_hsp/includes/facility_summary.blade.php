<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Facility Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            <tr >
                <td style="padding-left: 5px" width="130px">Facility name:</td>
                <td  height="20px"><b>{!! Form::label( 'name', $facility->name, [ 'id'=> 'name']) !!}</b></td>
            </tr>

            <tr>
                <td style="padding-left: 5px">Facility code:</td>
                <td><b>{!! Form::label( 'Facility Code', $facility->facility_code, [ 'id'=> 'facility_code']) !!}</b></td>
            </tr>

            <tr>
                <td style="padding-left: 5px">Stakeholder:</td>
                <td><b>{!! Form::label( 'stakeholder', $facility->shdescription, [ 'id'=> 'stakeholder']) !!}</b></td>
            </tr>
        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush