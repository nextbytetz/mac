<div class="row">
    <div class="col-md-12">
        <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="display" cellspacing="0" width="100%" id ="notification-report-table">
                <thead>
                <tr>
                    <th>Member No</th>
                    <th>Treatment File</th>
                    <th>Patient Names</th>
                    <th>WCF authorizationNo</th>
                    <th>HSP authorizationNo</th>
                    <th>Visit date</th>
                    <th>Billing Amount</th>
                    <th>Status</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    </div>
</div>

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";

        var $oTable = $('#notification-report-table').DataTable({
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                $oTable.buttons().container().insertBefore('#notification-report-table');
                $("#notification-report-table").css("width","100%");
            },
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route("backend.assessment.hcp_hsp.get_auth_billing", $billing->authorizationNo) !!}',
                type : 'get',
                data: function ($d) {
                }
            },

            columns: [
                { data: 'memberno' , name: 'memberno' ,orderable : true, searchable : true},
                { data: 'treatment_file' , name: 'treatment_file' ,orderable : true, searchable : true},
                { data: 'patient_names' , name: 'patient_names' ,orderable : true, searchable : true},
                { data: 'wcf_authno' , name: 'wcf_authno'},
                { data: 'sh_authno' , name: 'sh_authno', orderable : false, searchable : false},
                { data: 'visit_date' , name: 'visit_date', orderable : true, searchable : true},
                { data: 'bill_amount' , name: 'bill_amount', orderable : true, searchable : true},
                { data: 'status' , name: 'status', orderable : true, searchable : true},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = '{!! url("assessment/hcp_hsp/vetting_profile") !!}/' + aData['billing_detail_id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        });
        $('#search-recall-notification-form').on('submit', function($e) {
            $oTable.draw();
            $e.preventDefault();
        });
    });
</script>

@endpush