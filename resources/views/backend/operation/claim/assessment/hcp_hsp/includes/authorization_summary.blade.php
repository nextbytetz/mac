
<div class="row">
    <div class="col-md-12">

        <div class="col-md-12">
            <legend class="grey_modal" >@lang('Authorization Summary')</legend>
        </div>
        <div>&nbsp;</div>
        <div class="col-md-12">
            <div class="light_grey_bg" style="padding-top: 10px;">
                <div class="container">
                    <table class="table table-striped table-bordered" style="width:100%">
                        <tbody>
                            <tr>
                                <td>Member No</td>
                                <th>{!! isset($billing) ? $billing->memberno : '' !!}</th>
                            </tr>
                            <tr>
                                <td>Authorization No</td>
                                <th>{!! isset($billing) ? $billing->authorizationNo : '' !!}</th>
                            </tr>
                            <tr>
                                <td>Product Code</td>
                                <th>{!! isset($billing) ? $billing->porductcode : '' !!}</th>
                            </tr>
                            <tr>
                                <td>Facility Code</td>
                                <th>{!! isset($billing) ? $billing->facility_code : '' !!}</th>
                            </tr>
                            <tr>
                                <td>Visit type</td>
                                <th>{!! isset($billing) ? $billing->Visit_type : '' !!}</th>
                            </tr>
                            <tr>
                                <td>Scheme Id</td>
                                <th>{!! isset($billing) ? $billing->scheme_id : '' !!}</th>
                            </tr>
                           
                        </tbody></table>
                    </div>
                </div>


            </div>
        </div>
    </div>