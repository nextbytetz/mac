
@extends('layouts.backend.main', ['title' => 'HCP / HSP Facilities', 'header_title' => 'HCP / HSP Facilities'])

@include('backend.includes.datatable_assets')

@section('content')

<div class="nav_tab_pane_header">
    {{--Header Bar--}}
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >

            </div>
        </div>
    </div>
</div>
<br/>

<div class = "row">
    <div class="col-md-12" >

        <div>&nbsp;</div>

        <table class="display" cellspacing="0" width="100%" id ="facilities-table">
            <thead>
                <tr >
                    <th>@lang('labels.backend.table.name')</th>
                    <th>Facility Code</th>
                    <th>Stakeholder</th>
                </tr>
            </thead>
        </table>

    </div>
</div>

@stop

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        $('#facilities-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route("backend.assessment.hcp_hsp.get_facilities") !!}',
                type : 'get'

            },
            columns: [
            { data: 'name' , name: 'name'},
            { data: 'facility_code', name: 'facility_code'},
            { data: 'shdescription', name: 'shdescription'},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = "{{url('assessment/hcp_hsp/facility/')}}/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });

</script>;

@endpush
