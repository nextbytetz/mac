@extends('layouts.backend.main', ['title' => 'HSP - HCP', 'header_title' => 'HSP - HCP'])


@section('content')

<div style="color:#fff">
    <div class="col-sm-6 col-md-6">
        <div class="list-group">
            <ul class="list-unstyled">
                <a href="{!! route("backend.assessment.hcp_hsp.index") !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-user-md"> </i><large>&nbsp;&nbsp;Authorization</large></h6>

                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                            View / authorize HCP / HSP
                        </p> 
                    </li>
                </a>

            </ul>
            <br/>
            <ul class="list-unstyled">
                <a href="{!! route("backend.assessment.hcp_hsp.facilities") !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-hospital-o"> </i><large>&nbsp;&nbsp;Facilities</large></h6>

                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                        View All  HCP / HSP Facilities and Their Codes</p> 
                    </li>
                </a>

            </ul>
            <br/>
        </div>
    </div>

    <div class="col-sm-6 col-md-6">
        <div class="list-group">
            <ul class="list-unstyled">
                <a href="{!! route("backend.assessment.hcp_hsp.billings") !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tasks"> </i><large>&nbsp;&nbsp;Billing</large></h6>

                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                        Process / Vet HCP HSP billing</p> 
                    </li>
                </a>
            </ul>
            <br/>
          {{--   <ul class="list-unstyled">
                <a href="{!! route("backend.assessment.hcp_hsp.billings") !!}">
                    <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tasks"> </i><large>&nbsp;&nbsp;HCP / HSP Billing</large></h6>

                        <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                        Process / Vet HCP HSP billing</p> 
                    </li>
                </a>
            </ul> --}}
            <br/>
        </div>
    </div>

</div>
@stop

@push('after-script-end')
<script type="text/javascript">
    $(document).ready(function() {

       $("#preview").click(function() {

       });
   });
</script>;

@endpush