@extends('layouts.backend.main', ['title' => "Edit " . $benefit->name . " Payment", 'header_title' => "Edit " . $benefit->name . " Payment"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush


@include('backend.includes.assets.datetimepicker')

{{--start : Registration Modals--}}
@include('backend.includes.modals.claim.create_hsp')
@include("backend.includes.modals.claim.create_medical_practitioner")
{{--end : Registration Modals--}}

@section('content')
    <!-- Put the page specifically for this page here -->
    {!! Form::open(['route' => ['backend.claim.notification_report.update_mae_refund_member', $incident->id, $eligible->id],  'method'=>'put']) !!}

    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'attend_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'attend_date']) !!}
    {!! Form::hidden('incident_date' , $incident_date) !!}
    {!! Form::hidden('incident_type' , $incident->incident_type_id, []) !!}

    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report' => $incident])

    <div>&nbsp;</div>

    {{--main contents--}}

    {{--HEALTH PROVIDER SERVICE--}}

    {{--medical expense--}}

    <div class="row">
        <div class="col-md-12">

            {{--expense amount--}}
            <div class="element-form"  >
                <div class="col-md-2 text-xs-right required"><label>Expense Amount:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input( 'text','expense_amount', $eligible->medicalExpense->amount, ['class' => 'form-control number' ]) !!}
                        {!! $errors->first('expense_amount', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="element-form" >
                <div class="col-md-2 text-xs-right required"><label>Member Type:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::select('member_type_id', $member_types, $eligible->member_type_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}

                        {!! $errors->first('member_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--Health provider -select--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-md-2 text-xs-right"><label>@lang('labels.backend.claim.health_provider'):</label><span class="required_asterik">*</span></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::select('health_provider_id', $health_providers, $notification_health_provider->health_provider_id, ['style' => 'width:60%', 'placeholder' => '','class' => 'form-control search-select health-provider-select', 'id'=>'health_provider_select']) !!}
                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_health_service_provider']) !!}</span>
                        {!! $errors->first('health_provider_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--medical practitioner -select--}}
            <div class="element-form"  >
                <div class="col-md-2 text-xs-right"><label>@lang('labels.backend.claim.medical_practitioner'):</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::select('medical_practitioner_id', $medical_practitioners, $medical_practitioner_id,  ['style' => 'width:60%', 'placeholder' => '','class' => 'form-control practitioner-select', 'id'=>'medical_practitioner_select']) !!}
                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_medical_practitioner']) !!}</span>
                        {!! $errors->first('medical_practitioner_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{--attend date--}}
            <div class="element-form" >
                <div class="col-md-2 text-xs-right"><label>@lang('labels.backend.claim.attend_date'):</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::text('attend_date', $notification_health_provider->attend_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off',]) !!}
                        {!! $errors->first('attend_date', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--dismiss date--}}
            <div class="element-form"  >
                <div class="col-md-2 text-xs-right"><label>@lang('labels.backend.claim.dismiss_date'):</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::text('dismiss_date', $notification_health_provider->dismiss_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off',]) !!}
                        {!! $errors->first('dismiss_date', '<span class="help-block label label-danger">:message</span>') !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--headers--}}
    <div>&nbsp;</div>
    <div class="row">
        <legend class="light_grey_bg">Assessed Amount</legend>
        <div class="col-md-12 light_grey_bg">
            <div class="form-inline" >
                {{--Health service checklist--}}
                <div class="col-md-2" align="center">
                    <div class="form-group">
                        <label> @lang('labels.backend.claim.health_service_checklist')</label>
                    </div>
                </div>
                {{--feedback--}}
                <div class="col-md-2" align="center">
                    <div class="form-group">
                        <label> @lang('labels.general.feedback')</label>
                    </div>
                </div>

                {{--amount--}}
                <div class="col-md-2" align="center">
                    <div class="form-group" >
                        <label > @lang('labels.backend.table.amount')</label>
                    </div>
                </div>

                {{--to date--}}
                <div class="col-md-6" align="center">
                    <div class="form-group">
                        <label> @lang('labels.backend.table.to_date')</label>
                    </div>

                    {{--from date--}}
                    <div class="col-md-3" align="center">
                        <div class="form-group">
                            <label> @lang('labels.backend.table.from_date')</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--loop all $health_service_checklists--}}
    @foreach ($health_service_checklists as $health_service_checklist)
        @php
            if ($notification_health_provider->notificationHealthProviderServices()->where('health_service_checklist_id', $health_service_checklist->id)->count()) {
                $notification_health_provider_service = $notification_health_provider->notificationHealthProviderServices()->where('health_service_checklist_id', $health_service_checklist->id)->first();
                $feedback = $notification_health_provider_service->feedback;
                $amount = $notification_health_provider_service->amount;
                $from_date = $notification_health_provider_service->from_date;
                $to_date = $notification_health_provider_service->to_date;
            } else {
                $feedback = NULL;
                $amount = NULL;
                $from_date = NULL;
                $to_date = NULL;
            }
        @endphp
        <div>&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-inline" >
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label( 'checklist', $health_service_checklist->name, [ 'id'=>'checklist'])!!}
                        </div>
                    </div>
                    {{--feedback--}}
                    <div class="col-md-2">
                        <div class="form-group">
                            {!!  Form::select('feedback' . $health_service_checklist->id , ['0' => 'No', '1' => 'Yes'],  $feedback, ['class' => 'form-control search-select feedback' ,'placeholder'=> '', 'style'=>'width:120px', 'id'=>'feedback'.$health_service_checklist->id]) !!}
                        </div>
                    </div>

                    {{--amount--}}
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::input( 'text','amount' . $health_service_checklist->id, $amount, ['class' => 'form-control number money amount' ,'style'=>'width:150px', 'id'=>'amount'.$health_service_checklist->id]) !!}
                            {!! $errors->first('amount'. $health_service_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                    </div>


                    {{--from date--}}
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('from_date' . $health_service_checklist->id, $from_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off', 'id' => 'fromyear' . $health_service_checklist->id]) !!}
                            {!! $errors->first('to_date' . $health_service_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                    </div>

                    {{--to date--}}
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('to_date' . $health_service_checklist->id, $to_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off', 'id' => 'toyear' . $health_service_checklist->id]) !!}
                            {!! $errors->first('to_date' . $health_service_checklist->id, '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endforeach
    <div>&nbsp;</div>
    {{--Put general remarks here--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form"  >
                <div class="col-md-2 text-xs-right required"><label>Remarks:</label></div>
                <div class="col-md-10">
                    <div class="form-group">
                        {!! Form::textarea( 'remarks', $notification_health_provider->remarks, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>&nbsp;</div>
    {{--Buttons--}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-4 col-lg-4 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-10 col-lg-10 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('claim/notification_report/profile/' . $incident->id . '#medical',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button("Update",['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function () {

            autosize($("textarea.autosize"));
            $('.search-select').select2();

            $('.feedback').each(function() {
                var $id = this.id;
                var $health_checklist_id = $id.substr(8);

                feedback_option('feedback'+$health_checklist_id, $health_checklist_id);

                $("#feedback"+$health_checklist_id).change(function () {
                    feedback_option('feedback'+$health_checklist_id, $health_checklist_id);
                });
            });

            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);
            });

            $(".practitioner-select").select2({
                minimumInputLength: 3,
                ajax: {
                    url: "{!! route('backend.claim.medical_practitioners') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.full_name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
            $(".health-provider-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.claim.registered_health_providers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.full_info,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

        });

        //feedback options -> disable and hide
        function feedback_option(feedback_id, $health_checklist_id) {

            var $feedback = $("#" + feedback_id).val();
            if ($feedback == 1) {
                $("#amount" + $health_checklist_id).prop("disabled", false);
                $(".from_to_date" + $health_checklist_id).prop("disabled", false);
            }
            else {
                $("#amount" + $health_checklist_id).prop("disabled", true);
                $(".from_to_date" + $health_checklist_id).prop("disabled", true);
            }

        }

        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            allowZero : true,
            affixesStay : false
        });

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
    </script>
@endpush