@extends('layouts.backend.main', ['title' => "Create " . $benefit->name . " Payment", 'header_title' => "Create " . $benefit->name . " Payment"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        .sub_expenses:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "MAE Costs";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .sub_expenses {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush

@include('backend.includes.assets.datetimepicker')

{{--start : Registration Modals--}}
@include('backend.includes.modals.claim.create_hsp')
{{--end : Registration Modals--}}

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="row">
        @include("backend/operation/claim/notification_report/includes/header_info",['notification_report' => $incident])
    </div>

    {!! Form::open(['route' => ['backend.claim.notification_report.store_mae_refund_member_multiple', $incident->id, $benefit->id],  'method'=>'put', 'name' => 'store_mae_refund_member_multiple']) !!}

    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'attend_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'attend_date']) !!}
    {!! Form::hidden('incident_date' , $incident_date, []) !!}
    {!! Form::hidden('incident_type' , $incident->incident_type_id) !!}
    {!! Form::hidden('assessment' , $assessment) !!}

    <div class="row">
        <div class="offset-md-2 col-md-4">

            {{--Choose Member Type--}}
            <div class="fileld-layout">
                <label class="required">Member Type</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('member_type_id', $member_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>

            {{--Date of Receiving the Expenses--}}
            <div class="fileld-layout">
                <label>Receive Date</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('receive_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <span class="help-block">
                        {{--<span>Date hired to the current employer</span>--}}
                        <small class="form-text text-muted" style="width:100% !important;">Date when MAE expense was received</small>
                    </span>
                </div>
            </div>

        </div>
        <div class="col-md-4">

            {{--Total Expense Amount--}}
            <div class="fileld-layout">
                <label class="required">Total Expense Amount</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','amount', null, ['class' => 'form-control number', 'readonly' => 'readonly', 'id' => 'expense_amount']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Total expense amount from MAE Costs</small>
                    </span>
                </div>
            </div>

            {{--Total Assessed Expense Amount--}}
            <div class="fileld-layout">
                <label class="required">Total Assessed Expense Amount</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::input( 'text','assessed_amount', null, ['class' => 'form-control number', 'readonly' => 'readonly', 'id' => 'assessed_expense_amount']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Total assessed expense amount from MAE Assessed Costs</small>
                    </span>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="offset-md-2 col-md-4">
            <div class="fileld-layout">
                <label>General Remarks</label>
                <div class="form-group">
                    {!! Form::textarea( 'general_remarks', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none']) !!}
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;"></small>
                    </span>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            {{--Add new hsp--}}
            <div class="fileld-layout">
                <label class="required">Can't find Health Provider?</label>
                <span style="width: 40%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_health_service_provider']) !!}</span>
            </div>
        </div>

    </div>

    <div class="sub_expenses">
        <div class="row">
            <div class="col-md-12">
                <div class="fileld-layout">
                    <div class="form-group">
                        <div class="form-inline">
                            <span class="add_sub_expense">
                                <a href="#" id="add_sub_expense_entry" data-toggle='tooltip' data-html='true' title='Add medical expense entry'><i class="icon fa fa-2x fa-plus" aria-hidden="true" style="color:darkblue"></i>&nbsp;&nbsp;Add Expense Entry</a>
                            </span>
                        </div>
                        <small class="form-text text-muted" style="width:100% !important;">Can add multiple medical expenses</small>
                    </div>
                </div>
                <div id="sub_expense_entries">
                    @include("backend.operation.claim.notification_report.includes.progressive.benefits.mae_refund_member.add_multiple", ['count' => 0, 'assessment' => $assessment])
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            {{--<div class="text-center" style="align-items: center;">--}}
            <div class="pull-right">
                {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$incident->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button("Update", ['class' => 'btn btn-primary site-btn save_button btn-submit', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    <script>
        $(function () {
            autosize($("textarea.autosize"));
            $('.search-select').select2();
            let $mae_sub_entry_count = 0;
            let $assessment = "{{ $assessment }}";
            let $body = $('body');
            $body.on('click', 'a#add_sub_expense_entry', function (e) {
                e.preventDefault();
                $mae_sub_entry_count += 1;
                /* start : Retrieve mae sub entry from the server  */
                $.post("{!! route('backend.claim.notification_report.mae_sub_entries') !!}", {'count' : $mae_sub_entry_count, 'assessment' : $assessment}, function( data ) {
                    $( data ).appendTo( "#sub_expense_entries" );
                }, "html").done(function () {

                    $(".search-select").select2({});

                    $body.off('keyup', '.sub_amount').on('keyup', '.sub_amount', function(e) {
                        sub_amount_total();
                    });

                    $body.off('keyup', '.sub_assessed_amount').on('keyup', '.sub_assessed_amount', function(e) {
                        sub_assessed_amount_total();
                    });

                    $(".health-provider-select").select2({
                        minimumInputLength: 3,
                        multiple: false,
                        ajax: {
                            url: "{!! route('backend.claim.registered_health_providers') !!}",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: params.term || "",
                                    page: params.page || 1
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: $.map(data.items, function (item) {
                                        return {
                                            text: item.full_info,
                                            id: item.id
                                        };
                                    }),
                                    pagination: {
                                        more: true
                                    }
                                };
                            },
                            cache: true
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        }
                    });
                });
                /* end : Retrieve mae sub entry from the server  */
            });

            /* start: on click mae sub entry remove link */
            $body.on('click', 'a.remove_sub_expense_entry', function (e) {
                e.preventDefault();
                let $id = this.id;
                $mae_sub_entry_count -= 1;
                $("#sub_expense_entry_group" + $id).remove();
                sub_amount_total();
                sub_assessed_amount_total();
            });
            /* end: on click mae sub entry remove link */

            $(".health-provider-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.claim.registered_health_providers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.full_info,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

            $body.off('keyup', '.sub_amount').on('keyup', '.sub_amount', function(e) {
                sub_amount_total();
            });

            $body.off('keyup', '.sub_assessed_amount').on('keyup', '.sub_assessed_amount', function(e) {
                sub_assessed_amount_total();
            });

            $body.on('submit', 'form[name=store_mae_refund_member_multiple]', function ($e) {
                $e.preventDefault();
                let $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    // let $name = $(this).attr('name');
                    $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                let $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    beforeSend : function (e) {
                        $($form).find(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        if ($data.success) {
                            document.location.href = $data.url;
                        } else {
                            sweetAlert("Error Updating Checklist", $data.message);
                        }
                    },
                    error: function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        let $errors = $.parseJSON($data.responseText);
                        /*console.log($errors);*/
                        $.each($errors, function($index, $value) {
                            $($form).find("input[name^='" + $index + "']").closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                            $($form).find("select[name^='" + $index + "']").closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        });
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });

        });

        function sub_amount_total() {
            let $value = 0;
            let $sum = 0;
            $('.sub_amount').each(function() {
                $value = $(this).val();
                if ($value) {
                    $sum += parseFloat($value.split(",").join(""));
                }
            });
            $('#expense_amount').val($sum);
        }

        function sub_assessed_amount_total() {
            let $value = 0;
            let $sum = 0;
            $('.sub_assessed_amount').each(function() {
                $value = $(this).val();
                if ($value) {
                    $sum += parseFloat($value.split(",").join(""));
                }
            });
            $('#assessed_expense_amount').val($sum);
        }
    </script>
@endpush