@extends('layouts.backend.main', ['title' => $benefit->name . " Payment Assessment", 'header_title' => $benefit->name . " Payment Assessment"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . '/nextbyte/plugins/sweetalert/css/sweetalert.css') }}
    {{ Html::style(asset_url() . '/nextbyte/plugins/sweetalert/css/google.css') }}

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        <div class="col-md-12">
            {!! Form::open(['route' => ['backend.claim.notification_report.update_pd_assessment', $incident->id, $eligible->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put' , 'name' => 'claim_assessment_checklist']) !!}
            @if (!$user_has_access)
                <div class="alert alert-info">
                    @lang("strings.backend.workflow.miss_access_to_assess_claim")
                </div>
            @endif
            {!! Form::hidden('incident_date', $incident->incident_date) !!}
            {!! Form::hidden('today_date', getTodayDate()) !!}
            {!! Form::hidden('dob', $dob) !!}
            {!! Form::hidden('old_val', $age, ['id' => 'old_val']) !!}

            @if ($age <= 15)
                <div class="alert alert-danger blink" role="alert">
                    <strong>Warning</strong> Age is below 15, please check for rectification.
                </div>
            @endif

            {{--HEADER--}}
            @include("backend/operation/claim/notification_report/includes/header_info",['notification_report' => $incident])


            {{--main tab content--}}
            <div class = "row">
                <div class="col-md-12">

                    <div class="basic_nav_pills nav_basic_tab">
                        <ul class="nav nav-tabs" role="tablist">
                            {{--General--}}
                            <li class="nav-item">
                                <a class="nav-link active" href="#general" data-toggle="tab" role="tab">
                                    @lang('labels.general.general')
                                </a>
                            </li>

                            {{--Impairment Assessment--}}
                            @if ($incident->impairment_assessment_id)
                                <li class="nav-item">
                                    <a class="nav-link" href="#impairment_assessment" data-toggle="tab" role="tab">
                                        Impairment Assessment
                                    </a>
                                </li>
                            @endif

                            {{--Amputation Table (Schedule 2)--}}
                            {{--<li class="nav-item">
                                <a class="nav-link" href="#schedule_2" data-toggle="tab">
                                    Amputation Table (Second Schedule)
                                </a>
                            </li>--}}

                            {{--PD Calculator (Schedule 1)--}}
                            {{--<li class="nav-item">
                                <a class="nav-link" href="#schedule_1" data-toggle="tab">
                                    PD Calculator (First Schedule)
                                </a>
                            </li>--}}

                        </ul>

                        <div class="tab-content">

                            <div id="general" class="tab-pane active"  role="tabpanel" >

                                {{--main tab content--}}
                                <div class = "row">
                                    <div class="col-md-6">
                                        <div style="display: none">
                                            @if ($user_has_access)
                                                <legend class="grey_modal" id="type_of_duty_overview" >@lang('labels.backend.claim.type_of_duty')</legend>
                                                @include('backend/operation/claim/notification_report/includes/progressive/benefits/td/claimed_current_disability_state')
                                            @endif

                                            <div>&nbsp;</div>
                                            {{--Type of duty Assessment--}}
                                            <legend class="grey_modal" id="type_of_duty_assessment" >@lang('labels.backend.claim.type_of_duty_assessment')</legend>
                                            @include('backend/operation/claim/notification_report/includes/progressive/benefits/td/claim_assessment_type_of_duty',['notification_report' => $incident])
                                            <div>&nbsp;</div>
                                            {{--Claim - compensation expense overview--}}
                                            <legend class="grey_modal" id="claim_assessment_expense_overview" >@lang('labels.backend.claim.claim_assessment_expense_overview')</legend>
                                            @include('backend/operation/claim/notification_report/includes/progressive/benefits/td/claim_assessment_expense_overview',['notification_report' => $incident])
                                            <div>&nbsp;</div>
                                        </div>
                                        {{--Type of duty Claimed--}}
                                        {{--show only if is employee--}}
                                        {{--@if ($medical_expense->member_type_id  == 2)--}}

                                        <legend class="grey_modal" id="doctors_modification" >Doctor's Modification</legend>

                                        {{--Body Party Injury--}}
                                        <div class="fileld-layout">
                                            <label class="required">Body Part Injured</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('body_part_injury_id[]', $body_part_injuries, $body_part_injuries_values, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'body_part_injury_id']) !!}
                                                    <i class="icon fa fa-spinner fa-spin spin2" aria-hidden="true" style="display: none;"></i>
                                                </div>
                                                <small class="form-text text-muted" style="width:100% !important;">Can select multiple entries</small>
                                                {!! $errors->first('body_part_injury_id', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        {{--lost body part--}}
                                        <div class="fileld-layout">
                                            <label class="required">Has Lost Body Part?</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('lost_body_part', ['1' => 'Yes', '0' => 'No'], $lost_body_part, ['class' => 'search-select lost_body_part_select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                                                </div>
                                                {!! $errors->first('lost_body_part', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        {{--List Lost Body Part/Function--}}
                                        <div class="fileld-layout lost_body_part">
                                            <label class="required">List Lost Body Parts/Function Loss</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('lost_body_part_id[]', $lost_body_parties, $lost_body_parties_values, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'lost_body_part_id']) !!}
                                                </div>
                                                <small class="form-text text-muted" style="width:100% !important;">Can select multiple entries</small>
                                                {!! $errors->first('lost_body_part_id', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        {{--Nature Of Incident--}}
                                        <div class="fileld-layout">
                                            <label class="required">Nature Of Injury</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('nature_of_incident_cv_id', $natures_of_incident, $incident->nature_of_incident_cv_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                                                </div>
                                                {!! $errors->first('nature_of_incident_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>

                                        {{--Diagnosis--}}
                                        <div class="fileld-layout">
                                            <label>Diagnosis</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::text('diagnosis', $claim->diagnosis, ['class' => 'form-control']) !!}
{{--                                                    <span class="input-group-addon"><i class="icon fa fa-money"></i></span>--}}
                                                </div>
                                                {{--<small class="form-text text-muted" style="width:100% !important;">Should range from 0 - 100</small>--}}
                                                {!! $errors->first('diagnosis', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>


                                        <br/>

                                        {{--Compuattion of Permamanent Disablement--}}
                                        <legend class="grey_modal" id="computation_of_permanent_disablement" >@lang('labels.backend.claim.computation_of_permanent_disablement')</legend>
                                        <br/>
                                        {{--Computation Method--}}
                                        <div class="fileld-layout">
                                            <label class="required">Computation Method</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('computation_method', ['1' => 'Impairment Assessment Guideline', '2' => 'Schedule Two', '3' => 'Both Schedules'], $assessment->method, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                                                </div>
                                                {!! $errors->first('computation_method', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        @include('backend/operation/claim/notification_report/includes/progressive/benefits/td/claim_assessment_computation_permanent_disablement',['notification_report' => $incident])
                                        <legend></legend>
                                        <br/>
                                        {{--Need Rehabilitation--}}
                                        <div class="fileld-layout">
                                            {{--<label class="required">Need Rehabilitation?</label>--}}
                                            <div class="form-group">
                                                <div class="checkbox-squared">
                                                    {!! Form::checkbox('need_rehabilitation', 1, $claim->need_rehabilitation, ['id' => 'need_rehabilitation', 'autocomplete' => 'off']) !!}
                                                    <label for="need_rehabilitation"></label>
                                                    <span>Need Rehabilitation?</span>
                                                </div>
                                            </div>
                                        </div>
                                        {{--Need Constant Care Attendant?--}}
                                        <div class="fileld-layout">
                                            {{--<label class="required">Need Rehabilitation?</label>--}}
                                            <div class="form-group">
                                                <div class="checkbox-squared">
                                                    {!! Form::checkbox('need_cca', 1, $claim->need_cca, ['id' => 'need_cca', 'autocomplete' => 'off']) !!}
                                                    <label for="need_cca"></label>
                                                    <span>Need Constant Care Attendant?</span>
                                                </div>
                                            </div>
                                        </div>
                                        {{--@endif--}}

                                        <div>&nbsp;</div>
                                        {{--Total Claim - Compensation Amount--}}
                                        <legend class="grey_modal" id="total_claim_compensation_overview" >@lang('labels.backend.claim.total_claim_compensation_overview')</legend>
                                        @include('backend.operation.claim.notification_report.includes.claim_assessment_total_claim_compensation_overview',['notification_report' => $incident])

                                    </div>

                                    <div class="col-md-6">
                                        {{--PD Calculator (First Schedule)--}}
                                        <legend class="grey_modal" id="pd_calculator" >PD Calculator (Guideline)</legend>
                                        <br/>
                                        <div class="underline">Volume One</div>
                                        <br/>
                                        {{--Whole Person Impairment (WPI)--}}
                                        <div class="fileld-layout">
                                            <label>Whole Person Impairment (WPI)</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::text('wpi', $assessment->wpi, ['class' => 'form-control', 'id' => 'wpi', 'validate-message' => 'Please fill Whole Person Impairment (WPI)']) !!}
                                                    <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                                                </div>
                                                <small class="form-text text-muted" style="width:100% !important;">Should range from 0 - 100</small>
                                                {!! $errors->first('wpi', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="underline">Volume Two (Disability Conversion Sheets)</div>
                                        <br/>
                                        {{--PD Body Part Injury--}}
                                        <div class="fileld-layout">
                                            <label class="required">PD Body Part Injured => FEC Rank</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('pd_injury_id', $pd_body_part_injuries, $assessment->pd_injury_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'pd_injury_id', 'validate-message' => 'Please fill PD Body Part Injury']) !!}
                                                </div>
                                                {!! $errors->first('pd_injury_id', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        {{--PD Impairment--}}
                                        <div class="fileld-layout">
                                            <label class="required">PD Impairment</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('pd_impairment_id', $pd_impairments, $assessment->pd_impairment_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'pd_impairment_id', 'validate-message' => 'Please fill PD Impairment']) !!}
                                                </div>
                                                {!! $errors->first('pd_impairment_id', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        {{--Age--}}
                                        <div class="fileld-layout">
                                            <label class="required">Age</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::text('age_display', $age, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        {{--PD Occupation--}}
                                        <div class="fileld-layout">
                                            <label class="required">PD Occupation</label>
                                            <div class="form-group">
                                                {!! Form::select('occupation_id',  ($assessment->occupation()->count()) ? $assessment->occupation()->get()->pluck("name_info", "id")->all() : [], $assessment->occupation_id, ['class' => 'pd-occupation-select', 'style' => 'width:100%', 'id' => 'pd-occupation-select', 'validate-message' => 'Please fill PD Occupation']) !!}
                                                {!! $errors->first('occupation_id', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>

                                        <span class="help-block label label-danger" id="calculate_pd_error_msg" style="display: none;"></span>
                                        <table class="table table-striped table-bordered" style="width:100%">
                                            <tbody>
                                                <tr>
                                                    <th width="300px"><a href="#" id="calculate_pd_schedule_one" class="btn btn-small btn-secondary">Calculate PD</a></th>
                                                    <th class="underline" id="schedule_one_calculated_pd" style="font-size: 54px;"></th>
                                                </tr>
                                                <tr>
                                                    <th colspan="2" id="derived_schedule_one_calculated_pd" style="text-align: center;"></th>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <br/>

                                        {{--Amputation Table (Second Schedule)--}}
                                        <legend class="grey_modal" id="amputation_table" >Schedule Two</legend>
                                        <br/>
                                        {{--<table class="display" cellspacing="0" width="100%" id ="pd-amputation-table">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Percent</th>
                                            </tr>
                                            </thead>
                                        </table>--}}
                                        {{--PD Amputation--}}
                                        <div class="fileld-layout">
                                            <label class="required">PD Amputation</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::select('pd_amputation_id', $pd_amputations, $assessment->pd_amputation_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                                                </div>
                                                {!! $errors->first('pd_amputation_id', '<span class="help-block label label-danger">:message</span>') !!}
                                            </div>
                                        </div>
                                        <br/>

                                        <legend class="grey_modal">General Comments</legend>
                                        <div class="fileld-layout">
                                            <label>&nbsp;</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    {!! Form::textarea( 'pd_comments', $assessment->pd_comments, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none']) !!}
                                                </div>
                                                <small class="form-text text-muted" style="width:100% !important;">Description for the calculated PD</small>
                                                {{--
                                                {!! $errors->first('accident_place', '<span class="help-block label label-danger">:message</span>') !!}--}}
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{--Buttons--}}
                                @if ($user_has_access)
                                    <div class="row">
                                        <div class="col-md-offset-6 col-md-6" class="form-inline" >
                                            <div class="element-form">
                                                {{--<div class="text-xs-right"></div>--}}
                                                <div class="pull-right">
                                                    {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$incident->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit' ]) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div>&nbsp;</div>

                            </div>

                            @if ($incident->impairment_assessment_id)
                                <div id="impairment_assessment" class="tab-pane" role="tabpanel">
                                    @include('backend/operation/claim/assessment/impairment/includes/dashboard_entry', ['impairments' => $incident->impairmentAssessments, 'readonly' => 1])
                                </div>
                            @endif

                            {{--<div id="schedule_2">

                            </div>

                            <div id="schedule_1">

                            </div>--}}

                        </div>


                    </div>
                </div>




                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

    <script  type="text/javascript">

        var percent_impairment_scale =  parseFloat({!! sysdefs()->data()->percentage_imparement_scale !!});
        var percent_of_earning =  parseFloat({!! sysdefs()->data()->percentage_of_earning !!});
        var gross_monthly_earning = parseFloat({!! $monthly_earning_approved !!});
        var constant_factor_month =  parseInt({!! sysdefs()->data()->constant_factor_compensation !!});
        var no_of_days_for_a_month = parseFloat({!! sysdefs()->data()->no_of_days_for_a_month_in_assessment !!});
        var min_days_eligible_assessment =  parseFloat({!! sysdefs()->data()->days_eligible_for_claim_assessment !!});
        var min_compensation_amt =  parseFloat({!! sysdefs()->data()->minimum_monthly_pension !!});
        var min_salary_payable = parseFloat(min_compensation_amt / (0.01 * percent_of_earning));
        var max_compensation_amt = parseFloat({!! sysdefs()->data()->maximum_monthly_pension !!});
        var max_salary_payable = parseFloat(max_compensation_amt / (0.01 * percent_of_earning));

        var ppd_lumpsum = 0;
        var monthly_pension = 0;
        var pd = 0;
        var total_claimed_expense = $('#total_claimed_expense').val();
        var total_claimed_amount = 0;
        var total_compensation_amount = 0;
        var ttd_id = parseInt({!! $benefit_type_ids['ttd'] !!});
        var tpd_id = parseInt({!! $benefit_type_ids['tpd'] !!});
        var day_hours = parseInt({!! sysdefs()->data()->day_hours !!});
        var ttd_days_assessed = 0;
        var tpd_days_assessed = 0;
        var ttd_amount_assessed = 0;
        var tpd_amount_assessed = 0;
        var total_td_days = 0;
        var mae_amount_assessed =  0;


        $(function () {
            autosize($("textarea.autosize"));
            $('.search-select').select2();

            $("#monthly_pension_row").hide();

            let $body = $('body');
            let $pd_occupation_select = $(".pd-occupation-select");

            $body.on('click', 'a#calculate_pd_schedule_one', function (e) {
                e.preventDefault();
                let $wpi = $("#wpi");
                let $pd_injury_id = $("#pd_injury_id");
                let $pd_impairment_id = $("#pd_impairment_id");
                let $old = $("#old_val");
                let $wpi_val = $wpi.val();
                if ($wpi_val == 0) {
                    $("#schedule_one_calculated_pd").text(0);
                    $("#derived_schedule_one_calculated_pd").text("");
                } else if ($wpi_val >= 0 && $wpi_val <= 100) {
                    if (validate_entry($wpi.val(), $wpi) && validate_entry($pd_injury_id.val(), $pd_injury_id) && validate_entry($pd_occupation_select.val(), $pd_occupation_select) && validate_entry($pd_impairment_id.val(), $pd_impairment_id)) {
                        /* start : Calculate PD from Conversion Sheets  */
                        $.post("{!! route('backend.claim.notification_report.pd_assessment.schedule_one_calculator') !!}", {wpi:$wpi.val(),pd_injury_id:$pd_injury_id.val(),occupation_id:$pd_occupation_select.val(),pd_impairment_id:$pd_impairment_id.val(),age:$old.val()}, function( $data ) {
                            $("#schedule_one_calculated_pd").text($data.pd);
                            $("#derived_schedule_one_calculated_pd").text($data.derived_pd);
                        }, "json").done(function () {

                        }).fail(function () {

                        });
                        /* end : Calculate PD from Conversion Sheets  */
                    }
                } else {
                    $("#schedule_one_calculated_pd").text("");
                    $("#derived_schedule_one_calculated_pd").text("");
                }

            });

            let $body_part_injury_id = $("#body_part_injury_id");
            let $lost_body_part_select = $(".lost_body_part_select");
            let $lost_body_part_id = $("#lost_body_part_id");

            /*let $oTable = $('#pd-amputation-table').DataTable({
                buttons : ['reload'],
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: true,
                paging: true,
                info:false,
                ajax:{
                    url : '{!! route('backend.claim.notification_report.pd_assessment.amputation') !!}',
                    type : 'post',
                },
                columns: [
                    { data: 'name' , name: 'name'},
                    { data: 'percent' , name: 'percent'},
                ]
            });*/

            $pd_occupation_select.select2({
                minimumInputLength: 3,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.claim.notification_report.pd_assessment.occupations') !!}",
                    dataType: 'json',
                    delay: 250,
                    method: 'post',
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

            $body_part_injury_id.on('change', function ($e) {
                $(".spin2").show();
                let $list = $(this).val(); /* return in form of list. 1,3,5,7*/
                $.get("{{ url('/') }}/getLostBodyParties?body_party_injury=" + $list, function ($data) {
                    $lost_body_part_id.empty();
                    $lost_body_part_id.select2("val", "");
                    $lost_body_part_id.html($data);
                    $(".spin2").hide();
                });
            });
            $lost_body_part_select.on('change', function ($e) {
                let $choice = $lost_body_part_select.val();
                let $lost_body_part = $(".lost_body_part");
                switch ($choice) {
                    case '1':
                        $lost_body_part.show();
                        break;
                    case '0':
                        $lost_body_part.hide();
                        break;
                    default:
                        $lost_body_part.hide();
                        break;
                }
            });
            $lost_body_part_select.trigger("change");

            $("#date_of_mmi_row").hide();
            /**
             * Overview slide toggle show /hide assessment section =================
             */
            $("#employment_history_overview").click(function(){
                $("#employment-history-div").slideToggle("slow");
            });

            $("#type_of_duty_overview").click(function(){
                $("#disability-state-div").slideToggle("slow");
            });

            $("#type_of_duty_assessment").click(function(){
                $(".type-of-duty-assessment-div").slideToggle("slow");
            });

            $("#claim_assessment_expense_overview").click(function(){
                $("#claim-assessment-expense-overview-div").slideToggle("slow");
            });

            $("#computation_of_permanent_disablement").click(function(){
                $("#computation-of-permanent-disablement-div").slideToggle("slow");
            });

// ----end---------------------------------

            //initialize
            calculate_lumpsum();
            get_total_claimed_amount();

            get_compensation_expense();
            get_total_compensation_amount();
            display_compensated_expense();

            $body.off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });

            $body.off('keyup', '.percent_entry').on('keyup', '.percent_entry', function(e) {

                percent_entry(e);
                calculate_lumpsum();
                get_total_claimed_amount();
                get_total_compensation_amount();
                display_compensated_expense();
            });

            $body.off('keyup', '.disability_state').on('keyup', '.disability_state', function(e) {
                get_compensation_expense();
                get_total_compensation_amount();
                display_compensated_expense();
            });



            $body.on('submit', 'form[name=claim_assessment_checklist]', function(e) {
                e.preventDefault();
//Date of MMI
                var $day =$('#mmidate').val();
                var $year = $('#mmiyear').val();
                var $month = $('#mmimonth').val();
                let $form = this;

                if(($year)&& ($month) && ($day )) {
                    $('input[name=date_of_mmi]').val($year + '-' + $month + '-' + $day);
                } else {
                    $("input[name=date_of_mmi]").val("");
                }

                let $pd = $("#pd").val();
                let $minimum_allowed_cca = {{ $minimum_allowed_cca }};
                let $need_cca = $("#need_cca");

                if ($pd >= $minimum_allowed_cca && !$need_cca.is(':checked')) {
                    swal({
                        title: "Warning",
                        text: "Does this member need Constant Care Attendant?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "No",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function ($confirmed) {
                        if ($confirmed) {
                            $need_cca.prop('checked', true).trigger('change');
                        }
                        $form.submit();
                    });
                } else {
                    $form.submit();
                }

            });


        });

        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }

        function percent_entry(e) {
            var percent = $('.percent_entry').val();
            if ( percent > 100
                && e.keyCode != 46 // delete
                && e.keyCode != 8 // backspace
            ) {
                e.preventDefault();
                $('.percent_entry').val(0);
                amaran_notify("{!! trans("exceptions.backend.claim.percentage_pd_dont_exceed_100") !!}", "{!! trans("labels.general.warning") !!}", "warning");
            }
        }

        function calculate_lumpsum() {
//        var pi = $('#pi').val();
            var monthly_pension_payable = 0;
            var max_monthly_pension = parseFloat('{{ sysdefs()->data()->maximum_monthly_pension }}');
            if ( $('#pd').val()) {
                pd = $('#pd').val();

                //if pi < 30
                if(pd <= percent_impairment_scale){
                    $("#ppd_lumpsum_row").show();
                    $("#monthly_pension_row").hide();
                    $("#date_of_mmi_row").hide();
                    $("#constant_factor_row").show();
                    constant_factor_month =  parseInt({!! sysdefs()->data()->constant_factor_compensation !!});

                    ppd_lumpsum = ((pd *0.01) * (percent_of_earning * 0.01) * constant_factor_month * gross_monthly_earning);
                    ppd_lumpsum_max = ((pd *0.01) * constant_factor_month * max_monthly_pension);
                    /*get payable on max limit*/
                    if(parseFloat(ppd_lumpsum) > parseFloat(ppd_lumpsum_max))
                    {
                        ppd_lumpsum = ppd_lumpsum_max;
                    }

                    $('#ppd_lumpsum').text(addCommas(ppd_lumpsum.toFixed(2)));
                    return ppd_lumpsum;
                }else {
                    $("#ppd_lumpsum_row").hide();
                    $("#monthly_pension_row").show();
                    $("#date_of_mmi_row").show();
                    $("#constant_factor_row").hide();
                    constant_factor_month = 1;
                    monthly_pension = ((pd *0.01) * (percent_of_earning * 0.01) * constant_factor_month * gross_monthly_earning);
                    monthly_pension_payable = findMonthlyPensionPayable(monthly_pension, pd);
                    $('#monthly_pension').text(addCommas(monthly_pension_payable.toFixed(2)));
                    return monthly_pension_payable;
                }

            }
        }

        function get_total_claimed_amount() {
            //if pi < 30
            if(pd <= percent_impairment_scale){
                ppd_lumpsum = parseFloat(ppd_lumpsum);
            }else {
                ppd_lumpsum = 0;
            }
            total_claimed_amount = (parseFloat(total_claimed_expense) + ppd_lumpsum );
            $('#total_claimed_amount').text(addCommas(total_claimed_amount.toFixed(2)));
            return  total_claimed_amount;
        }

        function get_total_compensation_amount() {
            //if pi < 30
            if(pd <= percent_impairment_scale){
                ppd_lumpsum = parseFloat(ppd_lumpsum);
            }else {
                ppd_lumpsum = 0;
            }
            total_compensation_amount = mae_amount_assessed + ttd_amount_assessed + tpd_amount_assessed + ppd_lumpsum;
            $('#total_compensation_amount').text(addCommas(total_compensation_amount.toFixed(2)));
            return  total_compensation_amount;
        }

        /**
         * Get total compensation expense depending on no of days inserted
         * @returns {string}
         */
        function get_compensation_expense() {
            ttd_days_assessed = 0;
            tpd_days_assessed = 0;
            total_td_days = parseFloat(get_total_assessed_td_days());
            $('.disability_state').each(function() {
                var $id = this.id;
                var $notification_disability_state_id = $id.substr(4);
                var $benefit_type_id = parseInt($('#benefit_type' + $notification_disability_state_id ).val());
                var state_percent_of_ed_ld =  parseFloat($('#percent_of_ed_ld' + $notification_disability_state_id ).val());
                var state_days = 0;


                if ($('#days' + $notification_disability_state_id).val() !== "") {
                    state_days = parseFloat($('#days' + $notification_disability_state_id).val());
                }

                if ($benefit_type_id == ttd_id) {
                    ttd_days_assessed +=  (state_percent_of_ed_ld * 0.01 * state_days);
                }else if($benefit_type_id == tpd_id){
                    tpd_days_assessed += state_percent_of_ed_ld * 0.01 * state_days;
                }

                /*check if qualify for claim assessment for temporary disablement*/
                if (!(total_td_days >= min_days_eligible_assessment))
                {
                    ttd_days_assessed = 0;
                    tpd_days_assessed = 0;
                }

                ttd_amount_assessed = findFormulaTtdTpd(ttd_days_assessed, 'ttd');
                tpd_amount_assessed = findFormulaTtdTpd(tpd_days_assessed, 'tpd');

//            mae_amount_assessed =  parseFloat($('#mae_amount').val());
                total_compensation_amount = mae_amount_assessed + ttd_amount_assessed + tpd_amount_assessed + ppd_lumpsum;

            });
        }


        /**
         *  Total temporary disablement days - assessed
         *
         */
        function get_total_assessed_td_days(){
            total_td_days = 0;
            var state_days = 0;
            var total_days = 0;
            $('.disability_state').each(function() {
                var $id = this.id;
                var $notification_disability_state_id = $id.substr(4);
                var $benefit_type_id = parseInt($('#benefit_type' + $notification_disability_state_id ).val());
                state_days = 0;

                if ($('#days' + $notification_disability_state_id).val() !== "") {
                    state_days = $('#days' + $notification_disability_state_id).val();
                }

                if (($benefit_type_id == ttd_id) || $benefit_type_id == tpd_id) {
                    total_days += parseFloat(state_days);
                }
            });

            return total_days;
        }

        function display_compensated_expense() {
            $('#ttd_amount_assessed').text(addCommas(ttd_amount_assessed.toFixed(2)));
            $('#ttd_assessed').val(ttd_amount_assessed);
            $('#tpd_amount_assessed').text(addCommas(tpd_amount_assessed.toFixed(2)));
            $('#tpd_assessed').val(tpd_amount_assessed);
            $('#ttd_tpd_amount_assessed').text(addCommas((tpd_amount_assessed + ttd_amount_assessed).toFixed(2)));
            $('#mae_ttd_tpd_amount_assessed').text(addCommas((mae_amount_assessed + ttd_amount_assessed + tpd_amount_assessed).toFixed(2)));
        }

        /* Formula for finding TTD amount and TPD AMount */
        function findFormulaTtdTpd($total_days, $disablement_type) {
            /*Old Formula*/
            // return    parseFloat((percent_of_earning * 0.01) * ($total_days) * ((gross_monthly_earning / no_of_days_for_a_month)));
            amount = 0;
            if ((gross_monthly_earning < min_salary_payable) && $disablement_type == 'ttd' ){
                amount = (min_compensation_amt / no_of_days_for_a_month) * $total_days;
            }else{

                /* Check when is above the max i.e. Applicable for TTD and TPD*/
                if (gross_monthly_earning > max_salary_payable ){
                    amount = (max_compensation_amt / no_of_days_for_a_month) * $total_days;
                }else{
                    /* If Monthly earning is between Max and Min Payable*/
                    amount = ((gross_monthly_earning  * (0.01 * percent_of_earning)) / no_of_days_for_a_month) * $total_days;
                }
            }
            return parseFloat(amount);
        }

        /* Computer monthly pension payable for PPD and PTD*/
        function findMonthlyPensionPayable($monthly_pension_actual,$pd) {
            var mp_payable = $monthly_pension_actual;
            /*For PTD - Check for Min and Max payable*/
            if($pd == 100){

                if ($monthly_pension_actual > max_compensation_amt){
                    mp_payable = max_compensation_amt;
                }else if ($monthly_pension_actual < min_compensation_amt){
                    mp_payable = min_compensation_amt;
                }
            }else{
                /*For PPD check for Max Payable only */
                if ($monthly_pension_actual > max_compensation_amt) {
                    mp_payable = max_compensation_amt;
                }
            }
            return parseFloat(mp_payable);
        }

        function validate_entry($selected_entries, $control) {
            if (!$selected_entries) {
                $control.addClass('form-error');

                let $calculate_pd_error_msg = $("#calculate_pd_error_msg");
                $calculate_pd_error_msg.show();
                $calculate_pd_error_msg.text($control.attr("validate-message"));
                setTimeout(
                    function() {
                        $control.removeClass('form-error');
                        $calculate_pd_error_msg.text("").hide();
                        },
                    2000
                );
                return false;
            } else {
                return true;
            }
        }

        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        function amaran_notify(message, title, type) {
            var icon;
            if(typeof type === "undefined") {
                type = 'success';
            }
            switch(type) {
                case "success":
                    icon = "fa fa-check-square-o";
                    break;
                case "warning":
                    icon = "icon fa fa-warning";
                    break;
                case "error":
                    icon = "icon fa fa-ban";
                    break;
                default:
                    icon = "fa fa-check-square-o";
            }
            $.amaran({
                'theme'     :'awesome ' + type,
                'content'   :{
                    title:title,
                    message:message,
                    info:'',
                    icon:icon,
                },
                'position'  :'bottom left',
                'outEffect' :'slideBottom',
                'inEffect'  :'slideLeft'
            });
        }

    </script>;
@endpush