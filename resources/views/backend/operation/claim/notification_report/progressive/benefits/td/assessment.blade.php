@extends('layouts.backend.main', ['title' => $benefit->name . " Payment Assessment", 'header_title' => $benefit->name . " Payment Assessment"])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
<style>
    .documents_used:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Documents Used";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .documents_used {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
    .assessment_list:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Assessment List";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .assessment_list {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@include('backend.includes.assets.datetimepicker')

@include('backend.includes.modals.claim.create_hsp')

@section('content')
<!-- Put the page specifically for this page here -->
<div class = "row">
    <div class="col-md-12">

        @if (!$user_has_access)
            <div class="alert alert-info">
                @lang("strings.backend.workflow.miss_access_to_assess_claim")
            </div>
        @endif

        {{--HEADER--}}
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])

        {{--main tab content--}}
        <div class = "row">
            <div class="col-md-12">


                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        {{--General--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#general"
                                data-toggle="tab">@lang('labels.general.general')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="previous_td_assessments_tab" data-toggle="tab" href="#previous_td_assessments" role="tab">
                                {{--<i class="icon fa fa-info" aria-hidden="true"></i>--}}
                                Previous TD Assessments
                            </a>
                        </li>
                </ul>

                <div class="nav_tab_contain tab-content">
                    <div id="general" class="nav_tab_pane tab-pane active in">

                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                {{--Type of duty Claimed--}}
                                @if ($user_has_access)
                                <legend class="grey_modal" id="type_of_duty_overview" >@lang('labels.backend.claim.type_of_duty')&nbsp;(Disability State Claimed)</legend>
                                @include('backend/operation/claim/notification_report/includes/progressive/benefits/td/claimed_current_disability_state')
                                @endif

                                <div>&nbsp;</div>
                                {{--Type of duty Assessment--}}
                                <legend class="grey_modal" id="type_of_duty_assessment" >@lang('labels.backend.claim.type_of_duty_assessment')</legend>
                                @include("backend/operation/claim/notification_report/includes/progressive/benefits/td/add_assessment")
                                {{--removed for new implementation--}}
                                {{--@include('backend/operation/claim/notification_report/includes/progressive/benefits/td/claim_assessment_type_of_duty', ['notification_report' => $incident])--}}

                                <div>&nbsp;</div>

                            </div>

                        </div>
                        {{--Buttons--}}
                        @if ($user_has_access)
                        <div class="row">
                            <div class="col-md-offset-6 col-md-6">
                                <div class="element-form">
                                    {{--<div class="text-xs-right"></div>--}}
                                    <div class="pull-right">
                                        {!! link_to_route('backend.claim.notification_report.profile', "Done", [$incident->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn save_button', ]) !!}
                                        {{--{!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button submit_td_assessment']) !!}--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div>&nbsp;</div>

                    </div>

                    <div class="tab-pane" id="previous_td_assessments" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                @include("backend/operation/claim/notification_report/includes/progressive/td_compensation_summary", ['tdprocessed' => 1])
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>


    </div>
</div>
</div>
@endsection
{{--<div id="preview_doc_modal" class="modal fade" role="dialog" data-focus="false">
    <div class="modal-dialog modal-lg" style="width:1250px !important;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title preview_modal_title" ></h4>
        </div>
        <div class="modal-body grid-column preview_modal_body">
         

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
    </div>
</div>
</div>--}}
@push('after-script-end')
<!-- Custom javascript files for this page -->

{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). '/nextbyte/plugins/forms/js/jquery.form.min.js') }}


<script  type="text/javascript">
    let $body = $('body');
    $(function () {
        autosize($("textarea.autosize"));
        $('.search-select').select2();
        let $type_of_document_select  = $('#type_of_document_select');
        $type_of_document_select.on('change', function (e) {
            e.preventDefault();
            selectedDocumentType($(this).val());
        });
        /*$type_of_document_select.trigger("change");*/
        documentPreviewEvent();
        loadDocumentsUsed("{{ $eligible->id }}");
        loadAssessmentList("{{ $eligible->id }}");
        $body.on('submit', 'form[name=add_claim_assessment]', function ($e) {
            $e.preventDefault();
            let $form = this;
            $($form).find(':input').each(function () {
                $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            let $options = {
                dataType : "json",
                type : "POST",
                url : $($form).attr("action"),
                beforeSend : function ($e) {
                    $($form).find(".btn-submit").prop('disabled', true);
                },
                success : function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    /*console.log($data);*/
                    if ($data.success) {
                        $($form).find(':input').each(function () {
                            $(this).clearInputs();
                        });
                        loadAssessmentList("{{ $eligible->id }}");
                    }
                },
                error: function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    var errors = $.parseJSON($data.responseText);
                    /*console.log(errors);*/
                    $.each(errors, function($index, $value) {
                        $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        if ($index === 'general_error') { $($form).prepend('<div class="alert alert-danger general_error" role="alert">' + $value + '</div>'); $('.general_error').fadeOut(11000); }
                    });
                }
            };
            $($form).ajaxSubmit($options);
        });
        $body.on('submit', 'form[name=add_document_used_td]', function ($e) {
            $e.preventDefault();
            let $form = this;
            $($form).find(':input').each(function () {
                $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            let $options = {
                dataType : "json",
                type : "POST",
                url : $($form).attr("action"),
                beforeSend : function ($e) {
                    $($form).find(".btn-submit").prop('disabled', true);
                },
                success : function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    /*console.log($data);*/
                    if ($data.success) {
                        reloadDocumentUsedForm("{{ $eligible->id }}");
                    }
                },
                error: function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    var errors = $.parseJSON($data.responseText);
                    /*console.log(errors);*/
                    $.each(errors, function($index, $value) {
                        $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        if ($index === 'general_error') { $($form).prepend('<div class="alert alert-danger general_error" role="alert">' + $value + '</div>'); $('.general_error').fadeOut(11000); }
                    });
                }
            };
            $($form).ajaxSubmit($options);
        });
        $body.on('click', 'a.remove_td_document_used', function (e) {
            e.preventDefault();
            let $id = $(this).data("id");
            /*alert($id);*/
            var $aborted = !window.confirm('Are you sure you want to remove this?');
            if (!$aborted) {
                $.post( base_url + "/claim/notification_report/delete_td_document_used/{{ $eligible->id }}/" + $id , {}, function( $data ) {
                    if ($data.success) {
                        $("#td_document_used_group" + $id).remove();
                    } else {
                        alert($data.message);
                    }
                }, "json").done(function() {
                    reloadDocumentUsedForm("{{ $eligible->id }}");
                });
            }
        });
        $body.on('click', 'a.remove_td_assessment', function (e) {
            e.preventDefault();
            let $id = $(this).data("id");
            /*alert($id);*/
            var $aborted = !window.confirm('Are you sure you want to remove this?');
            if (!$aborted) {
                $.post( base_url + "/claim/notification_report/delete_td_assessment/{{ $eligible->id }}/" + $id , {}, function( $data ) {
                    $("#td_assessment_group" + $id).remove();
                }, "json").done(function() {
                    loadAssessmentList("{{ $eligible->id }}");
                });

            }
        });
        $(".health-provider-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.claim.registered_health_providers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.full_info,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $(".show-computation-summary").click(function(e) {
            e.preventDefault();
            let $this = $(this);
            let $eligible = $this.data('eligible');
            let $computation_summary = $(".computation-summary[data-eligible='"+$eligible+"']");
            let $hideText = $this.find('.hide-text');
            let $showText = $this.find('.show-text');
            // display summary action here for eligible ...
            $.post( base_url + "/claim/notification_report/" + $eligible + "/eligible_computation_summary", {}, function( $data ) {
                $computation_summary.toggleClass('hidden');
                $computation_summary.html($data);
            }, "html").done(function() {
                $('[data-toggle="popover"]').popover();
            });
            // toggle the text Show/Hide for the link
            $hideText.toggleClass('hidden');
            $showText.toggleClass('hidden');
        });
    });
    function selectedDocumentType($doc_id) {
        $.ajax({
            url : "{{url('/claim/document_select_option/'.$incident->id.'/')}}/" + $doc_id,
            type: 'GET',
            datatype : 'json',
            success: function($data) {
                if ($data.success) {
                    $('#document_used').html("").append($data.options);
                }else{
                    $('#document_used').html("<option value=''>Select document type first</option>");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }});
    }
    function get_current_document($node) {
        $refNode = $node;
        return $.ajax({
            url: base_url + "/claim/notification_report/current_base64_document/{!! $incident->id !!}/" + $node,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }
    function loadDocumentsUsed($eligible) {
        $.post( base_url + "/claim/notification_report/list_td_document_used/" + $eligible , {}, function( $data ) {
            /*alert("Hi");*/
            $("#documents_used").empty().html($data.assessment_documents);
            $('#type_of_document_select').empty().append($data.document_available);
        }, "json").done(function() {
            documentPreviewEvent();
        });
    }
    function loadAssessmentList($eligible) {
        $.post( base_url + "/claim/notification_report/list_td_assessment/" + $eligible , {}, function( $data ) {
            $("#assessment_list").empty().html($data.assessments);
            /**
                ttd_amount_assessed
                tpd_amount_assessed
                mae_ttd_tpd_amount_assessed
             */
            $("#ttd_amount_assessed").empty().html(addCommas(parseFloat($data.compensation.ttd_amount).toFixed(2)));
            $("#tpd_amount_assessed").empty().html(addCommas(parseFloat($data.compensation.tpd_amount).toFixed(2)));
            $("#mae_ttd_tpd_amount_assessed").empty().html(addCommas(parseFloat($data.compensation.total_lumpsum).toFixed(2)));
        }, "json").done(function() {

        });
    }
    function reloadDocumentUsedForm($eligible)
    {
        let $form = $("form[name=add_document_used_td]");
        $($form).find(':input').each(function () {
            $(this).clearInputs();
        });
        loadDocumentsUsed($eligible);
    }
    function viewDocument($val)
    {
        let $document_frame = $("#document_frame");
        get_current_document($val).done(function ($data) {
            $document_frame.find("iframe").remove();
            let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\' allowfullscreen></iframe>');
            $document_frame.append($iframe);
        });
    }
    function documentPreviewEvent()
    {
        $body.off('click', '.document_preview').on('click', '.document_preview', function($e) {
            $e.preventDefault();
            let $id = $(this).data('id');
            let $document_used = $id;
            if (!$id) {
                $document_used = $("#document_used").val();
            }
            viewDocument($document_used);
        });
    }
    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

</script>;
@endpush