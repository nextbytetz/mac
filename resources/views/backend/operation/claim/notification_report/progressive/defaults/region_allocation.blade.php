<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Checklist User</th>
            <th>Allocated Regions</th>
            <th>Allocated Districts</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($checklist_users as $checklist_user)
            @php
                $i = $checklist_user->user_id;
            @endphp
            <tr>
                <td width="30%"><strong><label>{{ $checklist_user->user->name }}</label></strong></td>
                <td width="35%">
                {!! Form::select('regions' . $i . '[]', $regions, $checklist_user->regions()->pluck('region_id')->all(), ['class' => 'form-control search-select checklist_user_region', 'style'=>'width:100%', 'multiple' => 'multiple']) !!}
                </td>
                <td width="35%">
                    {!! Form::select('districts' . $i . '[]', $districts, $checklist_user->regions()->pluck('district_id')->all(), ['data-placeholder' => '','class'=>'form-control search-select checklist_user_district', 'multiple']) !!}
                </td>

            </tr>
        @endforeach
    </tbody>
</table>

<div class="message"></div>
<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm input-radius" value="@lang('buttons.general.crud.update')" />
</div>
<div class="clearfix"></div>

<br/>
<div class='underline'>Unassigned Region(s)</div>
<div style="font-weight: bold;">
    {!! $missing_regions !!}
</div>
<br/>
<div class='underline'>Unassigned District(s)</div>
<div>
    {!! $missing_districts !!}
</div>
<br/>

{{--
@foreach ($checklist_users as $user)
    @php
        $i = $user->id;
    @endphp

    <div class="fileld-layout" id="region_allocation{!! $i !!}" style="padding-left:20px; border-left: 2px solid {!! $color[$i % 2] !!};">
        <div class="form-group">
            <br/>
            <div class="form-inline">
                <span>
                    Checklist User:
                </span>
                <span>
                    <strong><label style="'width:40%'">{{ $user->name }}</label></strong>
                </span>
                <span>
                    Allocated Regions:
                </span>
                <span>
                   {!! Form::select('regions' . $i, $regions, null, ['class' => 'form-control search-select checklist_user_region', 'style'=>'width:60%', 'id' => 'checklist_user_region' . $i, 'multiple' => 'multiple']) !!}
               </span>
                <span class="help-block"></span>
            </div>
        </div>
    </div>
@endforeach--}}
