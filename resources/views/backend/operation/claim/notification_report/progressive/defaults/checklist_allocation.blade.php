@extends('layouts.backend.main', ['title' => 'Checklist Allocation', 'header_title' => 'Checklist Allocation'])

@push('after-styles-end')
    {{ Html::style(asset_url() . '/nextbyte/plugins/select2/css/select2.min.css') }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h5 class="client-title">
                <i class="icon fa fa-terminal"></i>
                <!-- User header detail -->
                {{--<span>{!! $user->available_label !!} </span>--}}

                <strong> <a href="#"> Checklist Allocation </a></strong>

                {{--<small >
                    Status:
                </small>--}}
            </h5>
        </div>
    </div>
    <legend></legend>
    <br/>

    {!! Form::open(['route' => ['backend.claim.notification_report.post_checklist_allocation', 1], 'class' => 'checklist_allocation']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="fileld-layout">
                    <label>Checklist Users</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('checklist_users[]', $users, $checklist_users_values, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'checklist_users']) !!}
                        </div>
                        <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;">Can select multiple users</small>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="message"></div>
        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-sm input-radius" value="@lang('buttons.general.crud.update')" />
        </div>
        <div class="clearfix"></div>
        <br/>
        <hr/>
    {!! Form::close() !!}

    {!! Form::open(['route' => ['backend.claim.notification_report.post_checklist_allocation', 2], 'class' => 'checklist_allocation']) !!}
        <div class="row">
            <div class="col-md-12">

                <div id="checklist_region_allocation"></div>

            </div>
        </div>
    {!! Form::close() !!}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). '/nextbyte/plugins/select2/js/select2.min.js') }}
    {{ Html::script(asset_url(). '/nextbyte/plugins/forms/js/jquery.form.min.js') }}
    <script>
        $(function() {
            $(".search-select").select2({});
            loadRegionAllocation();

            $('body').on('submit', 'form.checklist_allocation', function (e) {
                e.preventDefault();
                let $form = this;

                let $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    success : function (data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        if (data.success) {
                            $("<div class='alert alert-success'>Success, All changes have been updated!</div>").appendTo($($form).find(".message")).delay(2000).fadeOut();
                        }
                        loadRegionAllocation();
                    },
                    error: function ($data) {
                        let $errors = $.parseJSON($data.responseText);
                    },
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });
        });
        function loadRegionAllocation() {
            $.post("{!! route('backend.claim.notification_report.checklist_allocation_region') !!}", {}, function( $data ) {
                $( "#checklist_region_allocation" ).html( $data );
            }, "html").done(function () {
                $(".search-select").select2({});
            });
        }
    </script>
@endpush