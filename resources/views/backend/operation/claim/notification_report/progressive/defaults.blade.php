@extends('layouts.backend.main', ['title' => 'Notification Defaults', 'header_title' => 'Notification Defaults'])


@section('content')

    <div class="row" style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">

                {{--item1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.notification_report.checklist_allocation') !!}">
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-group"> </i><large>&nbsp;&nbsp;Notification Allocation</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Configure how notifications will be allocated to claim officers</p> </li>
                    </a>

                </ul>

            </div>
        </div>

        {{--right div--}}

        <div class="col-sm-6 col-md-6">



            <div class="list-group">

                {{--item1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.investigations.defaults') !!}">
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-binoculars"> </i><large>&nbsp;&nbsp;Investigation Officers</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Add and Manage Investigation Officers</p> </li>

                    </a>
                </ul>

            </div>

            <div class="list-group">

                {{--item1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.claim.allocate_payroll_defaults') !!}">
                        <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-group"> </i><large>&nbsp;&nbsp;Payroll Allocation</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Configure how payroll tasks will be allocated to payroll officers</p> </li>
                    </a>

                </ul>
            </div>

        </div>

        <div class="row" style="color:#fff">
            {{--left div--}}
            <div class="col-sm-6 col-md-6">
                <div class="list-group">

                    {{--item1--}}
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.claim.claim_accrual.notification_report.accrual_dates') !!}">
                            <li  class="border-less list-group-item">  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-calendar"> </i><large>&nbsp;&nbsp;Accrual Default Dates </large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Configure Start, end dates for accrual </p> </li>
                        </a>

                    </ul>

                </div>
            </div>

        </div>


        @stop

        @push('after-script-end')
            <script type="text/javascript">
                $(document).ready(function() {

                });
            </script>;

    @endpush