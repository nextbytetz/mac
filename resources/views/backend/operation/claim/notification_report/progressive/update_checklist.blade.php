@extends('layouts.backend.main', ['title' => "Update Notification Checklist", 'header_title' => "Update Notification Checklist"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        .employee_particulars:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Employee Particulars";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .employee_particulars {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
        .incident_particulars:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Incident Particulars";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .incident_particulars {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
        .medical_treatments:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Medical Treatments";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .medical_treatments {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush

@include('backend.includes.assets.datetimepicker')

{{--start : Registration Modals--}}
@include('backend.includes.modals.claim.create_hsp')
@include("backend.includes.modals.claim.create_medical_practitioner")
{{--end : Registration Modals--}}

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])
    </div>
    {!! Form::open(['route' => ['backend.claim.notification_report.update_checklist.put', $incident->id], 'name' => 'update_notification_checklist', 'method' => 'PUT']) !!}
        {!! Form::hidden('incident_type_id', $incident->incident_type_id) !!}
        {!! Form::hidden('reporting_date', $incident->reporting_date) !!}
        {!! Form::hidden('incident_date', $incident->incident_date) !!}
        {!! Form::hidden('this_date', getTodayDate()) !!}
        {!! Form::hidden('maximum_initial_treatment_date', $maximum_initial_treatment_date) !!}

        <div class="incident_particulars">

            @include("backend/operation/claim/notification_report/includes/progressive/details/update")

            @if (in_array($incident->incident_type_id, [1,4]))
                {{--Accident--}}
                @include("backend/operation/claim/notification_report/includes/progressive/checklist/accident/update")
            @elseif (in_array($incident->incident_type_id, [2,5]))
                {{--Disease--}}
                @include("backend/operation/claim/notification_report/includes/progressive/checklist/disease/update")
            @elseif ($incident->incident_type_id == 3)
                {{--Death--}}
                @include("backend/operation/claim/notification_report/includes/progressive/checklist/death/update")
            @endif
        </div>

        <hr/>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$incident->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                    {!! Form::button("Update", ['class' => 'btn btn-primary site-btn save_button btn-submit', 'type'=>'submit']) !!}
                </div>
            </div>
        </div>

    {!! Form::close() !!}
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script>
    $(function() {

        let $body = $('body');

        $('#region_id').on('change', function (e) {
            $("#spin2").show();
            let region_id = e.target.value;
            $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                $('#district_id').empty();
                $("#district_id").select2("val", "");
                $('#district_id').html(data);
                $("#spin2").hide();
            });
        });

        @if($incident->incident_type_id != 3)

            let $same_hospital_select = $(".same_hospital_select");
            let $return_to_work_select = $(".return_to_work_select");
            let $on_treatment_select = $(".on_treatment_select");
            let $member_type_id = $(".member_type_id");

            $same_hospital_select.select2();
            let $treatment_count = "{{ $incident->treatments()->count() }}";

            $member_type_id.on('change', function (e) {
                let $choice = $(this).val();
                switch ($choice) {
                    case '3':
                    case 3:
                        $(this).closest(".medical_expense").children(".insurance_id").show();
                        break;
                    default:
                        $(this).closest(".medical_expense").children(".insurance_id").hide();
                        break;
                }
            });
            $member_type_id.trigger("change");

            $return_to_work_select.on('change', function (e) {
                let $choice = $return_to_work_select.val();
                let $return_to_work = $(".return_to_work");
                switch ($choice) {
                    case '1':
                        $return_to_work.show();
                        break;
                    case '0':
                        $return_to_work.hide();
                        break;
                    default:
                        $return_to_work.hide();
                        break;
                }
            });
            $return_to_work_select.trigger("change");

            $on_treatment_select.on('change', function (e) {
                let $choice = $on_treatment_select.val();
                let $on_treatment = $(".on_treatment");
                let $same_hospital_select = $('.same_hospital_select');
                switch ($choice) {
                    case '1':
                        $on_treatment.show();
                        break;
                    case '0':
                        $on_treatment.hide();
                        $same_hospital_select.val(1).trigger('change.select2').trigger("change");
                        break;
                    default:
                        $on_treatment.hide();
                        $same_hospital_select.val(1).trigger('change.select2').trigger("change");
                        break;
                }
            });
            $on_treatment_select.trigger("change");

            let $has_ld = $(".has_ld");
            $has_ld.on("change", function ($e) {
                let $choice = $(this).val();
                switch ($choice) {
                    case '0':
                        $(this).closest(".form-group").children(".light_duty_days").hide();
                        $(this).closest(".form-group").children(".light_duty_days").find(".light_duty_days_input").val("");
                        break;
                    case '1':
                        $(this).closest(".form-group").children(".light_duty_days").show();
                        break;
                    default:
                        $(this).closest(".form-group").children(".light_duty_days").hide();
                        $(this).closest(".form-group").children(".light_duty_days").find(".light_duty_days_input").val("");
                        break;
                }
            });
            $has_ld.trigger("change");

            let $has_hospitalization = $(".has_hospitalization");
            $has_hospitalization.on("change", function ($e) {
                let $choice = $(this).val();
                switch ($choice) {
                    case '0':
                        $(this).closest(".form-group").children(".hospitalization_days").hide();
                        $(this).closest(".form-group").children(".hospitalization_days").find(".hospitalization_days_input").val("");
                        break;
                    case '1':
                        $(this).closest(".form-group").children(".hospitalization_days").show();
                        break;
                    default:
                        $(this).closest(".form-group").children(".hospitalization_days").hide();
                        $(this).closest(".form-group").children(".hospitalization_days").find(".hospitalization_days_input").val("");
                        break;
                }
            });
            $has_hospitalization.trigger("change");

            let $has_ed =  $(".has_ed");
            $has_ed.on("change", function ($e) {
                let $choice = $(this).val();
                switch ($choice) {
                    case '0':
                        $(this).closest(".form-group").children(".excuse_from_duty_days").hide();
                        $(this).closest(".form-group").children(".excuse_from_duty_days").find(".excuse_from_duty_days_input").val("");
                        break;
                    case '1':
                        $(this).closest(".form-group").children(".excuse_from_duty_days").show();
                        break;
                    default:
                        $(this).closest(".form-group").children(".excuse_from_duty_days").hide();
                        $(this).closest(".form-group").children(".excuse_from_duty_days").find(".excuse_from_duty_days_input").val("");
                        break;
                }
            });
            $has_ed.trigger("change");

            $same_hospital_select.on('change', function (e) {
                let $choice = $(".same_hospital_select").val();
                switch ($choice) {
                    case '0':
                        $(".add_medical_treatment").show();
                        break;
                    case '1':
                        $(".add_medical_treatment").hide();
                        break;
                    default:
                        $(".add_medical_treatment").hide();
                        break;
                }
            });
            $same_hospital_select.trigger("change");

            $body.on('click', 'a#add_medical_treatment', function (e) {
                e.preventDefault();
                $treatment_count += 1;
                /* start : Retrieve notification treatments entries from the server  */
                $.post("{!! route('backend.claim.notification_report.checklist_treatment_entries') !!}", {'count' : $treatment_count}, function( data ) {
                    $( data ).prependTo( "#treatment_entries" );
                }, "html").done(function () {
                    let $member_type_id = $(".member_type_id");
                    let $has_ld = $(".has_ld");
                    let $has_hospitalization = $(".has_hospitalization");
                    let $has_ed =  $(".has_ed");
                    $(".search-select").select2({});
                    jQuery('.datepicker').datetimepicker({
                        timepicker:false,
                        format:'Y-n-j',
                        weeks: true,
                        dayOfWeekStart: 1,
                        lazyInit: true,
                        scrollInput: false
                    });
                    $(".practitioner-select").select2({
                        minimumInputLength: 3,
                        ajax: {
                            url: "{!! route('backend.claim.medical_practitioners') !!}",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: params.term || "",
                                    page: params.page || 1
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: $.map(data.items, function (item) {
                                        return {
                                            text: item.full_name,
                                            id: item.id
                                        };
                                    }),
                                    pagination: {
                                        more: true
                                    }
                                };
                            },
                            cache: true
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        }
                    });
                    $(".health-provider-select").select2({
                        minimumInputLength: 3,
                        multiple: false,
                        ajax: {
                            url: "{!! route('backend.claim.registered_health_providers') !!}",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: params.term || "",
                                    page: params.page || 1
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: $.map(data.items, function (item) {
                                        return {
                                            text: item.full_info,
                                            id: item.id
                                        };
                                    }),
                                    pagination: {
                                        more: true
                                    }
                                };
                            },
                            cache: true
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        }
                    });

                    $member_type_id.on('change', function (e) {
                        let $choice = $(this).val();
                        switch ($choice) {
                            case '3':
                            case 3:
                                $(this).closest(".medical_expense").children(".insurance_id").show();
                                break;
                            default:
                                $(this).closest(".medical_expense").children(".insurance_id").hide();
                                break;
                        }
                    });
                    $member_type_id.trigger("change");

                    $has_ld.on("change", function ($e) {
                        let $choice = $(this).val();
                        switch ($choice) {
                            case '0':
                                $(this).closest(".form-group").children(".light_duty_days").hide();
                                $(this).closest(".form-group").children(".light_duty_days").find(".light_duty_days_input").val("");
                                break;
                            case '1':
                                $(this).closest(".form-group").children(".light_duty_days").show();
                                break;
                            default:
                                $(this).closest(".form-group").children(".light_duty_days").hide();
                                $(this).closest(".form-group").children(".light_duty_days").find(".light_duty_days_input").val("");
                                break;
                        }
                    });
                    $has_ld.trigger("change");

                    $has_hospitalization.on("change", function ($e) {
                        let $choice = $(this).val();
                        switch ($choice) {
                            case '0':
                                $(this).closest(".form-group").children(".hospitalization_days").hide();
                                $(this).closest(".form-group").children(".hospitalization_days").find(".hospitalization_days_input").val("");
                                break;
                            case '1':
                                $(this).closest(".form-group").children(".hospitalization_days").show();
                                break;
                            default:
                                $(this).closest(".form-group").children(".hospitalization_days").hide();
                                $(this).closest(".form-group").children(".hospitalization_days").find(".hospitalization_days_input").val("");
                                break;
                        }
                    });
                    $has_hospitalization.trigger("change");

                    $has_ed.on("change", function ($e) {
                        let $choice = $(this).val();
                        switch ($choice) {
                            case '0':
                                $(this).closest(".form-group").children(".excuse_from_duty_days").hide();
                                $(this).closest(".form-group").children(".excuse_from_duty_days").find(".excuse_from_duty_days_input").val("");
                                break;
                            case '1':
                                $(this).closest(".form-group").children(".excuse_from_duty_days").show();
                                break;
                            default:
                                $(this).closest(".form-group").children(".excuse_from_duty_days").hide();
                                $(this).closest(".form-group").children(".excuse_from_duty_days").find(".excuse_from_duty_days_input").val("");
                                break;
                        }
                    });
                    $has_ed.trigger("change");
                });
                /* end : Retrieve notification treatments entries from the server  */
            });

            /* start: on click treatment entry remove link */
            $body.on('click', 'a.remove_treatment_entry', function (e) {
                e.preventDefault();
                let $id = this.id;
                $("#treatment_entry_group" + $id).remove();
            });
            /* end: on click treatment entry remove link */
        @endif

        $body.on('submit', 'form[name=update_notification_checklist]', function ($e) {
            $e.preventDefault();
            let $form = this;
            /* start: remove any printed error message in the input controls */
            $($form).find(':input').each(function () {
                // let $name = $(this).attr('name');
                $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            let $options = {
                dataType : "json",
                type : "POST",
                url : $($form).attr("action"),
                beforeSend : function (e) {
                    $($form).find(".btn-submit").prop('disabled', true);
                },
                success : function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    if ($data.success) {
                        document.location.href = $data.url;
                    } else {
                        sweetAlert("Error Updating Checklist", $data.message);
                    }
                },
                error: function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    let $errors = $.parseJSON($data.responseText);
                    console.log($errors);
                    $.each($errors, function($index, $value) {
                        $($form).find("input[name^='" + $index + "']").closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        $($form).find("select[name^='" + $index + "']").closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                    });
                }
            };
            // pass options to ajaxForm
            $($form).ajaxSubmit($options);
        });

        $(".practitioner-select").select2({
            minimumInputLength: 3,
            ajax: {
                url: "{!! route('backend.claim.medical_practitioners') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.full_name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $(".health-provider-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.claim.registered_health_providers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.full_info,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
    });
</script>
@endpush