@extends('layouts.backend.main', ['title' => "Attend Notification", 'header_title' => "Attend Notification"])

@push('after-styles-end')
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])
    </div>
    <div class="row">

        <div class="col-md-6 offset-md-3">
            <p style="text-align: center;">
                <span style="color: darkred;"><i class="icon fa fa-close fa-5x"></i></span>
            </p>
            <p>
                Notification has not been attended,
            </p>
            <p>
                Click the attend button to start working on the Notification Checklist. This notification has been allocated to <span style="color: green;">{{ $incident->allocatedUser->name or "None" }}</span>
            </p>
            <div style="text-align: center;">
                {!! link_to_route('backend.checker.index', "Cancel", [], ['class' => 'btn btn-secondary site-btn']) !!}
                <span>
                    {!! Html::decode(link_to_route('backend.claim.notification_report.attend.post', "Attend", $incident->id, ['data-method' => 'post', 'class' => 'btn site-btn save_button'])) !!}
                </span>
            </div>
        </div> <!-- /.col-md-8 -->

    </div> <!-- /.row -->
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
<script type="text/javascript">
    $(function() {

    });
</script>
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
{{--{{ Html::script(asset_url() . "/global/js/backend/backend.js") }}--}}
@endpush