@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.notification_report_profile'), 'header_title' => trans('labels.backend.claim.notification_report_profile')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/jQuerySmartWizard/css/smart_wizard.css") }}
    {{ Html::style(asset_url() . '/nextbyte/plugins/jQuerySmartWizard/css/smart_wizard_theme_arrows.css') }}
    <style>
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
        .employee_particulars:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Employee Particulars";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .employee_particulars {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
        .incident_particulars:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Incident Particulars";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .incident_particulars {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        @include('backend/operation/claim/notification_report/includes/header_info', ['notification_report' => $incident])
    </div>
    {{--Tabs navigation--}}

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            {{--<h4 class="page-content-title">Nav pills tabs with image</h4>--}}
            <div class="divider15"></div>
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="notification_process_tab" data-toggle="tab" href="#notification_process" role="tab">
                            <i class="icon fa fa-cubes" aria-hidden="true"></i>Notification Process
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="document_centre_tab" data-toggle="tab" href="#document_centre" role="tab">
                            <i class="icon fa fa-folder-open" aria-hidden="true"></i>Document Centre
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="compensation_summary_tab" data-toggle="tab" href="#compensation_summary" role="tab">
                            <i class="icon fa fa-money" aria-hidden="true"></i>Compensation Summary
                        </a>
                    </li>
                    {{--<li class="nav-item">
                        <a class="nav-link" id="bank_details_tab" data-toggle="tab" href="#bank_details" role="tab">
                            <i class="icon fa fa-bank" aria-hidden="true"></i>Bank Details
                        </a>
                    </li>--}}
                    <li class="nav-item">
                        <a class="nav-link" id="workflow_history_tab" data-toggle="tab" href="#workflow_history" role="tab">
                            <i class="icon fa fa-inbox" aria-hidden="true"></i>Workflow History
                        </a>
                    </li>
                    @if($in_follow_up)
                    <li class="nav-item">
                        <a class="nav-link" id="follow_ups_tab" data-toggle="tab" href="#follow_ups" role="tab">
                            <i class="icon fa fa-blind" aria-hidden="true"></i>Follow Ups
                        </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" id="my_follow_ups_tab" data-toggle="tab" href="#my_follow_ups" role="tab">
                            <i class="icon fa fa-volume-control-phone" aria-hidden="true"></i>User Follow Ups
                        </a>
                    </li>
                    @if($exists_online)
                        <li class="nav-item">
                            <a class="nav-link" id="online_document_tab" data-toggle="tab" href="#online_document" role="tab">
                                <i class="icon fa fa-cloud" aria-hidden="true"><div></div></i> Verify Online Documents
                            </a>
                        </li>
                    @endif
                </ul>

                {{--<div class="divider15"></div>--}}
                <legend>{!! $incident->next_stage !!}</legend>

                <div class="tab-content">
                    <div class="tab-pane active" id="notification_process" role="tabpanel">
                        {{--Put notification process content here ...--}}
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    {{--navigation bar for links--}}
                                    <div class="nav_tab_pane_header">
                                        <div class="row">
                                            <div class="col-md-12" >
                                                <div class="pull-right" >

                                                    {!! Html::decode($incident->initialize_workflow) !!}
                                                    {{--{!! Html::decode($incident->initialize_interrupting_workflow) !!}--}}

                                                    @if ($incident->can_create_benefit)
                                                        <span>
                                                            {{-- MAE Approval Workflow--}}

                                                        </span>
                                                        @include("backend.operation.claim.notification_report.includes.progressive.create_benefit_more_links")
                                                    @endif
                                                    {{--@if ($incident->initialize_interrupting_workflow)--}}
                                                    @if (true)
                                                        @include("backend.operation.claim.notification_report.includes.progressive.interrupting_workflow_more_links")
                                                    @endif
                                                    @include('backend.operation.claim.notification_report.includes.progressive.notification_process_more_links')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--main tab content--}}
                                    <div class = "row">
                                        <div class="col-md-12">
                                            <legend class="grey_info" >Active Workflow</legend>
                                            @if ($incident->progressive_stage >= 4 && $incident_stage_code == 13)
                                                {{--active benefit workflow content (Benefit Workflow)--}}
                                                @include('backend.operation.claim.notification_report.includes.progressive.current_benefit_workflow')
                                            @else
                                                {{--active workflow content (Enforceable & Interrupting/On Demand Workflow)--}}
                                                @include('backend.operation.claim.notification_report.includes.progressive.current_workflow')
                                            @endif

                                            {{--notification process wizard--}}
                                            <!-- SmartWizard html -->
                                                <div id="smartwizard">
                                                    <ul>
                                                        <li @if ($incident->progressive_stage >= 1) class="done" @endif><a href="#filling_checklist"><i class="icon fa fa-tasks" aria-hidden="true"></i>&nbsp;Filling Checklist<br /><small>Medical Care & Employee State</small></a></li>
                                                        <li @if ($incident->progressive_stage >= 2) class="done" @endif><a href="#investigation"><i class="icon fa fa-paw" aria-hidden="true"></i>&nbsp;Investigation<br /><small>Phone/Physical Investigation</small></a></li>
                                                        <li @if ($incident->progressive_stage >= 3) class="done" @endif><a href="#approval"><i class="icon fa fa-reorder" aria-hidden="true"></i>&nbsp;Notification Approval<br /><small>Processing Checklist Summary</small></a></li>
                                                        <li @if ($incident->progressive_stage >= 4) class="done" @endif><a href="#incident_review"><i class="icon fa fa-credit-card" aria-hidden="true"></i>&nbsp;Assessment<br /><small>Benefit Payment Process</small></a></li>
                                                    </ul>

                                                    <div>
                                                        <div id="filling_checklist" class="">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <br/>
                                                                    <div class="pull-right" >
                                                                            <span>
                                                                                {{--Update Checklist--}}
                                                                                {!! HTML::decode(link_to_route('backend.claim.notification_report.update_checklist', "<i class='icon fa fa-plus-square-o' aria-hidden='true'></i>&nbsp;" . "Update Checklist", [$incident->id],['class' => 'btn  btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
                                                                            </span>
                                                                        @if ($incident->can_complete_checklist)
                                                                            {{--complete notification checklist [when in Filling Checklist Stage]--}}
                                                                            <span>
                                                                                {!! Html::decode(link_to_route('backend.claim.notification_report.complete_checklist', "<i class='icon fa fa-mail-forward' aria-hidden='true'></i>&nbsp;Complete Checklist" , [$incident->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to complete the checklist?", 'class' => 'btn btn-secondary site-btn'])) !!}
                                                                            </span>

                                                                        @endif
                                                                    </div>
                                                                    <br/>
                                                                    <legend>Overview</legend>
                                                                    <br/>
                                                                    @include("backend.operation.claim.notification_report.includes.progressive.checklist.show")
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="investigation" class="">
                                                            @if ($incident->progressive_stage >= 2)
                                                                <div class="row">
                                                                    <div class="col-md-12">

                                                                        @include("backend/operation/claim/notification_report/includes/progressive/investigation/show")
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div id="approval" class="">
                                                            {{--processing checklist summary--}}
                                                            @if ($incident->progressive_stage >= 3)
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <br/>
                                                                        <legend>Overview</legend>
                                                                        <br/>
                                                                        @include("backend.operation.claim.notification_report.includes.progressive.checklist.summary")
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div id="incident_review" class="">
                                                            {{--benefit payment process--}}
                                                            @if ($incident->progressive_stage >= 4)
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        @include("backend.operation.claim.notification_report.includes.progressive.benefit_process")
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="general_information" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    {{--Put general information content here ...--}}
                                    {{--navigation bar for links--}}
                                    <div class="nav_tab_pane_header">
                                        <div class="row">
                                            <div class="col-md-12" >
                                                <div class="pull-right" >
                                                    @if ($incident->source == 2 || $incident->source == 3)
                                                        {!! Html::decode($incident->online_application_profile_button) !!}
                                                        <span></span>
                                                    @endif
                                                    {!! Html::decode($incident->modify_notification_button) !!}
                                                    <span></span>

                                                    @include('backend.operation.claim.notification_report.includes.progressive.general_info_more_links')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--main tab content--}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{--Notification Stagings Logs--}}
                                            <legend class="grey_modal" >Staging Logs</legend>
                                            <div class="label label-info">Notification <b>{{ $incident->filename }}</b> Status </div>
                                            <legend></legend>
                                            @include("backend.operation.claim.notification_report.includes.progressive.stage_comments", ['stages' => $incident->stages()])
                                        </div>
                                        <div class="col-md-6">
                                            {{--Employee Summary--}}
                                            @include("backend.operation.claim.notification_report.includes.progressive.employee_summary")

                                            {{--Incident Summary--}}
                                            @include("backend.operation.claim.notification_report.includes.progressive.incident_summary")

                                            {{--Incident Specific Particulars--}}
                                            @include("backend.operation.claim.notification_report.includes.progressive.incident_particular")

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="document_centre" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    {{--Put document centre content here / Document Centre Show ...--}}
                                    @include("backend.operation.claim.notification_report.includes.progressive.document_centre")
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="compensation_summary" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    {{--Put compensation summary content here ...--}}
                                    @if(count($compensation_summary))
                                        @include('backend/operation/claim/notification_report/includes/progressive/claim_compensation_summary')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="tab-pane" id="bank_details" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    --}}{{--Put bank details content here ...--}}{{--

                                </div>
                            </div>
                        </div>
                    </div>--}}
                    <div class="tab-pane" id="workflow_history" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    {{--Put workflow history content here ...--}}
                                    @include('backend.operation.claim.notification_report.includes.progressive.workflow_history')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="follow_ups" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    @if($in_follow_up)
                                    @include('backend.operation.claim.notification_report.includes.progressive.follow_ups.index')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="my_follow_ups" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    @include('backend.operation.claim.notification_report.includes.progressive.follow_ups.my_follow_ups.index')
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($exists_online)
                        <div class="tab-pane" id="online_document" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12 pills-height">
                                    <div data-plugin="scrollbar" data-height="1500">
                                        @include('backend.operation.claim.notification_report.includes.progressive.online_document')
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('backend.system.workflow.includes.initiate_modal')
    @if ($incident->progressive_stage >= 4)
    @include('backend.system.workflow.includes.initiate_benefit_modal')
    @endif
    @include("backend/operation/claim/notification_report/includes/progressive/follow_ups/includes/modals/workflow_modal")
    @include("backend/operation/claim/notification_report/includes/progressive/follow_ups/includes/modals/remarks_modal")

@endsection

    @push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/jQuerySmartWizard/js/jquery.smartWizard.min.js") }}
    {{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/clipboard/clipboard.min.js") }}
    {{--{{ Html::script(asset_url(). "/fontawesome/fontawesome.js") }}--}}
    <script  type="text/javascript">
        $(function() {

        new ClipboardJS('.copy_approval_note');

        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show').trigger('click');
        }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var $tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + $tab);
            } else {
                location.hash = '#' + $tab;
            }
        });
        $('#smartwizard').smartWizard({
            selected: {{ ($incident->progressive_stage > 4) ? 3 : $incident->progressive_stage - 1 }},
            theme: 'arrows',
            transitionEffect:'fade',
            toolbarSettings: {
                toolbarPosition: 'none'
            }
        });
        @if ($incident->progressive_stage >= 4)
            $(".show-computation-summary").click(function(e) {
                e.preventDefault();
                let $this = $(this);
                let $eligible = $this.data('eligible');
                let $computation_summary = $(".computation-summary[data-eligible='"+$eligible+"']");
                let $hideText = $this.find('.hide-text');
                let $showText = $this.find('.show-text');
                // display summary action here for eligible ...
                $.post( base_url + "/claim/notification_report/" + $eligible + "/eligible_computation_summary", {}, function( $data ) {
                    $computation_summary.toggleClass('hidden');
                    $computation_summary.html($data);
                }, "html").done(function() {
                    $('[data-toggle="popover"]').popover();
                });
                // toggle the text Show/Hide for the link
                $hideText.toggleClass('hidden');
                $showText.toggleClass('hidden');
            });
        @endif
    });

</script>
@endpush