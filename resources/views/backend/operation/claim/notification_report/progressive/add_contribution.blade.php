@extends('layouts.backend.main', ['title' => "Add Contribution", 'header_title' => "Add Contribution"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::open(['route' => ['backend.claim.notification_report.post_contribution', $incident->id],'method'=>'put',  'id' => 'add_contribution', 'name' => 'add_contribution']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {{--{!! Form::hidden('contrib_month', $contrib_month->format("Y-n-28"), ['class' => 'contribution_month_no_format']) !!}--}}
    {!! Form::hidden('employer_id', $employer->id) !!}
    {!! Form::hidden('incident_type_id', $incident->incident_type_id) !!}
    {!! Form::hidden('action', "store") !!}

    {{--header--}}
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])
    </div>

    {{--main contants--}}
    <br>

    {{--rctno--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right required"><label>@lang('labels.backend.finance.receipt.receipt_no'):</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input( 'text','rctno', null, ['class' => 'form-control', ]) !!}

                        {!! $errors->first('rctno', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--salary--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right required"><label>@lang('labels.general.salary'):</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input( 'text','basic_pay', null, ['class' => 'form-control money', ]) !!}

                        {!! $errors->first('basic_pay', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--grosspay--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right required"><label>@lang('labels.backend.compliance.grosspay'):</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::input( 'text','gross_pay', null, ['class' => 'form-control money']) !!}
                        {!! $errors->first('gross_pay', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--employee hired on incident month--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right"><label>Employee Hired on Incident Month?:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::select('hired_on_incident_month', ["0" => "No", "1" => "Yes"], null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'hired_on_incident_month']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--employee hired on incident month--}}
    <div class="row" id="datehired"  style="display: none;">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right required"><label>Employee Hire Date:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::text('datehired', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        {{--<span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>--}}
                    </div>
                    {!! $errors->first('datehired', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
    {{--incident month--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right required"><label>Incident Month:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><b>{{ $incident_month_formatted }}</b></label>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @if ($incident->incident_type_id == 2)
        {{--employee hired on incident month--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-md-6 text-xs-right"><label>Is Employee Still at Work:</label></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::select('employee_still_at_work', ["0" => "No", "1" => "Yes"], null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'employee_still_at_work']) !!}
                        </div>
                        {!! $errors->first('employee_still_at_work', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        {{--contribution month retired month--}}
        <div class="row" id="contrib_month_retired" style="display: none;">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-md-6 text-xs-right required"><label>Last Contribution Before Retirement (Date):</label></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::text('contrib_month_retired', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('contrib_month_retired', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        {{--contribution month--}}
        <div class="row" id="contrib_month_atwork" style="display: none;">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-md-6 text-xs-right required"><label>@lang('labels.backend.finance.receipt.contrib_month'):</label></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::select('contrib_month_atwork', $contrib_months, $contrib_month->format("Y-n-28"), ['class' => 'search-select', 'style' => 'width:100%']) !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @else
        {{--contribution month--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-md-6 text-xs-right required"><label>@lang('labels.backend.finance.receipt.contrib_month'):</label></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::select('contrib_month', $contrib_months, $contrib_month->format("Y-n-28"), ['class' => 'search-select', 'style' => 'width:100%']) !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif


    {{--grosspay--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right required"><label>@lang('labels.general.employer'):</label></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <b>{{ $employer->name }}</b>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-md-6 text-xs-right"><label>Remarks:</label></div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::textarea('remarks', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-7" class="form-inline" >
            <div class="element-form">
                <div class="col-md-8 text-xs-right"></div>
                <div class="col-md-4">
                    <div class="pull-right">

                        {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$incident->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    {{--</section>--}}

@stop

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}

<script  type="text/javascript">

    $(function () {

        $(".search-select").select2();

        let $hired_on_incident_month = $("#hired_on_incident_month");
        let $datehired = $("#datehired");
        let $contribution_month = $(".contribution_month");
        let $contribution_month_no_format = $(".contribution_month_no_format");
        $hired_on_incident_month.on('change', function (e) {
            let $choice = $(this).val();
            switch ($choice) {
                case "1":
                    $datehired.show();
                    $contribution_month.val("{{ $incident_month_formatted }}");
                    $contribution_month_no_format.val("{{ $incident_month->format("Y-n-28") }}");
                    break;
                default:
                    $datehired.hide();
                    $contribution_month.val("{{ $contrib_month_formatted }}");
                    $contribution_month_no_format.val("{{ $contrib_month->format("Y-n-28") }}");
                    break;
            }
        });
        $hired_on_incident_month.trigger("change");

        @if ($incident->incident_type_id == 2)
            let $employee_still_at_work = $("#employee_still_at_work");
            let $contrib_month_retired = $("#contrib_month_retired");
            let $contrib_month_atwork = $("#contrib_month_atwork");
            $employee_still_at_work.on('change', function (e) {
                let $choice = $(this).val();
                switch ($choice) {
                    case "1":
                        $contrib_month_atwork.show();
                        $contrib_month_retired.hide();
                        break;
                    case "0":
                        $contrib_month_atwork.hide();
                        $contrib_month_retired.show();
                        break;
                    default:
                        $contrib_month_atwork.hide();
                        $contrib_month_retired.hide();
                        break;
                }
            });
            $employee_still_at_work.trigger("change");
        @endif

        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });

        $('body').on('submit', 'form[name=add_contribution]', function(e) {
            e.preventDefault();
            // Validate date -- paymaent date
            this.submit();
        });

    });

</script>
@endpush
