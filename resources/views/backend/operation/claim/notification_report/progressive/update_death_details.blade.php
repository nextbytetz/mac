@extends('layouts.backend.main', ['title' => "Update Death Details", 'header_title' => "Update Death Details"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        .incident_particulars:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Incident Particulars";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .incident_particulars {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])
    </div>
    {!! Form::open(['route' => ['backend.claim.notification_report.update_death_details.put', $incident->id], 'name' => 'update_death_details', 'method' => 'PUT']) !!}
    {!! Form::hidden('incident_type_id', $incident->incident_type_id) !!}
    {!! Form::hidden('reporting_date', $incident->reporting_date) !!}
    {!! Form::hidden('incident_date', $incident->incident_date) !!}
    {!! Form::hidden('this_date', getTodayDate()) !!}

    <div class="incident_particulars">

        @include("backend/operation/claim/notification_report/includes/progressive/details/death_details")

    </div>

    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$incident->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button("Update", ['class' => 'btn btn-primary site-btn save_button btn-submit', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

    <script>
        $(function() {

            let $body = $('body');
            let $region_id = $('#region_id');
            $region_id.on('change', function (e) {
                $("#spin2").show();
                let region_id = e.target.value;
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district_id').empty();
                    $("#district_id").select2("val", "");
                    $('#district_id').html(data);
                    $("#spin2").hide();
                });
            });
            $region_id.trigger("change");
            //$('#district_id').val("{{ $incident->district_id }}");

            $body.on('submit', 'form[name=update_death_details]', function ($e) {
                $e.preventDefault();
                let $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    // let $name = $(this).attr('name');
                    $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                let $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    beforeSend : function (e) {
                        $($form).find(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        if ($data.success) {
                            document.location.href = $data.url;
                        } else {
                            sweetAlert("Error Updating Checklist", $data.message);
                        }
                    },
                    error: function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        let $errors = $.parseJSON($data.responseText);
                        console.log($errors);
                        $.each($errors, function($index, $value) {
                            $($form).find("input[name^='" + $index + "']").closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                            $($form).find("select[name^='" + $index + "']").closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        });
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });
        });
    </script>
@endpush