@extends('layouts.backend.main', ['title' => "Notification Created", 'header_title' => "Notification Created"])

@push('after-styles-end')

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $notification_report])
    </div>
    <div class="row">

        <div class="col-md-6 offset-md-3">
            <p style="text-align: center;">
                <span style="color: green;"><i class="fa fa-check-square fa-5x"></i></span>
            </p>
            <p>
                Notification has been registered,
            </p>
            <p>
                The newly created notification is pending for user's action. Current this application has been allocated to <span style="color: green;">{{ $notification_report->allocatedUser->name or "None" }}</span>
            </p>
            <div style="text-align: center;">
                {{ link_to_route('backend.claim.notification_report.choose_employee', "Register New", [], ['class' => 'btn btn-secondary site-btn']) }}
                {{ link_to_route('backend.claim.notification_report.profile', "Open Profile", [$notification_report->id], ['class' => 'btn site-btn save_button']) }}
            </div>
        </div> <!-- /.col-md-8 -->

    </div> <!-- /.row -->
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush