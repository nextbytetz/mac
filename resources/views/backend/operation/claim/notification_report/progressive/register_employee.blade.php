@extends('layouts.backend.main', ['title' => "Register Employee", 'header_title' => "Register Employee"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

    {!! Form::open(['route' => ['backend.claim.notification_report.post_register_employee', $incident->id],'method'=>'put',  'id' => 'register_employee', 'name' => 'register_employee']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden("has_employer", $incident->has_employer) !!}

    {{--header--}}
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])
    </div>

    {{--main contants--}}
    <br>
    @php
        /*if (isset($errors)) {
            print_r($errors);
        }*/
    @endphp
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                @if (!$incident->has_employer)
                    <span>
                        {{--create employer shortcut link--}}
                            {!! HTML::decode(link_to_route('backend.compliance.employer_registration.name_check', "<i class='icon fa fa-plus-square' aria-hidden='true'></i>&nbsp;" . "Employer Registration", [],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:normal;', 'target' => '_blank' ])) !!}
                    </span>
                @endif
                <span>
                    {{--create employee shortcut link--}}
                    {!! HTML::decode(link_to_route('backend.compliance.employee.create', "<i class='icon fa fa-plus-square-o' aria-hidden='true'></i>&nbsp;" . "Employee Registration", [],['class' => 'btn  btn-secondary site-btn', 'style' => 'font-weight:normal;', 'target' => '_blank' ])) !!}
                </span>
            </div>
        </div>
    </div>
    <legend></legend>
<br/>
    <div class="row">
        <div class="col-md-6">
            {{--Choose Employer--}}
            <div class="fileld-layout">
                <label class="required"> Employer </label>
                <div class="form-group">
                    @if ($incident->has_employer)
                        <label><b>{{ $incident->employer->name }}</b></label>
                        {!! Form::hidden("employer_id", $incident->employer_id) !!}
                        {!! Form::hidden("employer", $incident->employer_id, ['id' => 'employer']) !!}
                    @else
                        <div class="input-group">
                            {!! Form::select('employer_id', [], null, ['class' => 'employer-select', 'style' => 'width:100%']) !!}
                        </div>
                        <small class="form-text text-muted" style="width:100% !important;">Select employer first to select employee</small>
                        {!! $errors->first('employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                        {!! Form::hidden("employer", null, ['id' => 'employer']) !!}
                    @endif

                </div>
            </div>
            {{--Choose Employee--}}
            <div class="fileld-layout">
                <label class="required"> Employee </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('employee_id', [], null, ['class' => 'employee-select', 'style' => 'width:100%']) !!}
                    </div>
                    {!! $errors->first('employee_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            {{--Employee Incident Summary--}}
            @php
                $employee = $incident->employee;
                $employer = $incident->employer;
            @endphp

            <div class="row">
                <div class="col-md-12">
                    <div class="grey_modal">
                        <table style="width:100%">
                            <tr>
                                <td  align="center"><h6><b><span class="light_dark_color">Employee Summary</span></b></h6></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {{--<div class="light_grey_bg">&nbsp;</div>--}}
                    <div class="light_grey_bg" style="padding: 5px;">
                        <div class="underline" style="font-weight: lighter;">@lang('labels.general.employer')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $employer->name }}</span></div>
                        <div class="underline" style="font-weight: lighter;">@lang('labels.general.employee_name')&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $employee->name }}</span></div>
                        <div class="underline" style="font-weight: lighter;">@lang('labels.backend.table.employer_reg_no')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $employer->reg_no }}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    {{--Buttons--}}
    <div class="row">
        <div class="col-md-7" class="form-inline" >
            <div class="element-form">
                <div class="col-md-8 text-xs-right"></div>
                <div class="col-md-4">
                    <div class="pull-right">

                        {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$incident->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    {{--</section>--}}

@stop

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">

        $(function () {

            $(".employee-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employees_notification_employer') !!}",
                    dataType: 'json',
                    delay: 250,
                    type: "post",
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1,
                            employer: $('#employer').val()
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.employee,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:select", function(e) {
                /*alert($(this).val());*/
                $('#employer').val($(this).val());
            });


            $('body').on('submit', 'form[name=register_employee]', function(e) {
                e.preventDefault();
                // Validate date -- paymaent date
                this.submit();
            });

        });

    </script>
@endpush
