@extends('layouts.backend.main', ['title' => "Resource Allocation", 'header_title' => "Resource Allocation"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
<style>
    .custom_filter:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.system.workflow.custom_filter')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .custom_filter {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@section('content')
<!-- Put the page specifically for this page here -->
{{--header summary--}}
<div class="row">
    {{--left contrib ummary--}}
    <div class="col-md-12">
        {{--<div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                    <td width="180px" colspan="2">Notification Checklist</td>
                </tr>

                <tr>
                    <td>Available&nbsp;:</td>
                    <th></th>
                </tr>

                <tr>
                    <td>Allocated&nbsp;</td>
                    <th></th>
                </tr>
                <tr>
                    <td>Unallocated&nbsp;</td>
                    <th></th>
                </tr>

                </tbody></table>

        </div>--}}
        <div class="basic_nav_pills nav_basic_tab">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <legend class="nav-link active" data-toggle="tab">Allocation Status</legend>
                </li>
            </ul>
            <div class="nav_tab_contain tab-content">
                <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                    <div class="row">
                        <div class="col-md-5">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td width="200px"></td>
                                    <td><p class="underline" style="font-weight: bold;">Checklist</p></td>
                                    <td><p class="underline" style="font-weight: bold;">Investigation</p></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td width="200px">Available&nbsp;:</td>
                                    <td style="text-align: center;">{{ $resource_status['available_nc'] }}</td>
                                    <td style="text-align: center;">{{ $resource_status['available_ni'] }}</td>
                                </tr>
                                <tr>
                                    <td width="200px">Allocated&nbsp;:</td>
                                    <td style="text-align: center;">{{ $resource_status['allocated_nc'] }}</td>
                                    <td style="text-align: center;">{{ $resource_status['allocated_ni'] }}</td>
                                </tr>
                                <tr>
                                    <td width="200px">Unallocated&nbsp;:</td>
                                    <td style="text-align: center;">{{ $resource_status['unallocated_nc'] }}</td>
                                    <td style="text-align: center;">{{ $resource_status['unallocated_ni'] }}</td>
                                </tr>
                                <tr>
                                    <td width="200px">Attended (Allocated)&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Allocated resource(s) which has been attended"></i>&nbsp;:</td>
                                    <td style="text-align: center;">{{ $resource_status['attended_nc'] }}</td>
                                    <td style="text-align: center;">{{ $resource_status['attended_ni'] }}</td>
                                </tr>
                                <tr>
                                    <td width="200px">Unattended (Allocated)&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Allocated resource(s) which has not been attended"></i>&nbsp;:</td>
                                    <td style="text-align: center;">{{ $resource_status['unattended_nc'] }}</td>
                                    <td style="text-align: center;">{{ $resource_status['unattended_ni'] }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <div class="underline">Employer Allocation Group</div>
                            <div id="allocation_employer_group" data-plugin="custom-scroll" data-height="250">

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="underline">Staff Allocation Group</div>
                            <div id="allocation_staff_group" data-plugin="custom-scroll" data-height="250">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--Custom filter--}}
<div class="custom_filter">
    {!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="form-row">
                {{--Allocation Category--}}
                <div class="form-group col-md-3 allocation_select">
                    <label for="category">Allocation Category</label>
                    {!! Form::select('category', ['0' => 'Notification Checklist', '1' => 'Notification Investigation',], null, ['class' => 'form-control search-select', 'id' => 'category', 'placeholder' => '']) !!}
                </div>
                {{--Incident Type--}}
                <div class="form-group col-md-3 allocation_select">
                    <label for="incident">Incident Type</label>
                    {!! Form::select('incident', ['0' => 'All', '1' => 'Occupational Accident', '2' => 'Occupational Disease', '3' => 'Occupational Death'], null, ['class' => 'form-control search-select', 'id' => 'incident']) !!}
                </div>
                {{--Notification Stages--}}
                <div class="form-group col-md-6">
                    <label for="stage">Stage</label>
                    {!! Form::select('stage', $stages, null, ['class' => 'form-control search-select', 'id' => 'incident_stage', 'placeholder' => '']) !!}
                </div>
                {{--Employee Name--}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="employee">Employee Name</label>
                    {!! Form::select('employee', [], null, ['class' => 'form-control employee-select', 'id' => 'employee']) !!}
                </div>
                {{--Employer Name--}}
                <div class="form-group col-md-4 allocation_select">
                    <label for="employer">Employer Name</label>
                    {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                </div>
                {{--Region Selection--}}
                <div class="form-group col-md-3">
                    <label for="region">Incident Region:</label>
                    {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                </div>
                {{--District Selection--}}
                <div class="form-group col-md-3">
                    <label for="region">Incident District:</label>
                    {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                </div>
                {{--Resource Status--}}
                <div class="form-group col-md-3 allocation_select">
                    <label for="status">Status:</label>
                    {!! Form::select('status', ['0' => 'All', '1' => 'Allocated', '2' => 'Unallocated', '3' => 'Allocated to User', '4' => 'Attended (Allocated)', '5' => 'Unattended (Allocated)'], null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                </div>
                {{--Allocated User--}}
                <div class="form-group col-md-3 allocated_user_select">
                    <label for="allocated_user">User:</label>
                    {!! Form::select('user_id', $users, null, ['class' => 'form-control search-select', 'id' => 'allocated_user']) !!}
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <button type="button" class="btn btn-secondary site-btn" id="clear_filter">Clear</button>
                    <button type="submit" class="btn btn-success btn-sm btn-submit">@lang('buttons.general.search')</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<legend></legend>
<br/>
@permission("resource_allocation")
{{--Assign to--}}
<div class="row">
    <div class="col-md-12">
        <div class="form-row">
            {!! Form::open(['role' => 'form', 'id' => 'assign-user', 'route' => 'backend.claim.notification_report.allocation.assign']) !!}
            <div class="form-group col-md-4">
                <span class="assign_status_select">
                    <label for="assign_status"><b>Assign Status:</b></label>
                    {!! Form::select('assign_status', ['1' => 'Assign To User', '2' => 'Reallocate Evenly'] , null, ['class' => 'form-control search-select', 'id' => 'assign_status', 'placeholder' => '']) !!}
                </span>
                <span class="assign_user_select">
                    <label for="assigned_user"><b>Assign To:</b></label>
                    {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
                </span>
            </div>
            <div class="form-group col-md-2">
                <span class="assign_for_select">
                    <label for="assign_for"><b>Assign For:</b></label>
                    {!! Form::select('assign_for', ['0' => 'Notification Checklist', '1' => 'Notification Investigation'] , null, ['class' => 'form-control search-select', 'id' => 'assign_for', 'placeholder' => '']) !!}
                </span>
            </div>
            {{--<div class="col-md-3 assign_user_select">
                <label for="assigned_user">Assign To:</label>
                {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
            </div>--}}
            <div class="form-group col-md-2">
                <label for="allocate_submit">&nbsp;</label>
                <button type="submit" class="form-control btn-sm btn-secondary site-btn" id="allocate_submit"><i class='icon fa fa-arrow-right' aria-hidden='true'></i>&nbsp;Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endauth

{{--Resource Datatable--}}
<div class="row">
    <div class="col-md-12">
        <table class="display" id = "resource-allocation-table" width="100%">
            <thead>
            <tr >
                <th></th>
                <th>Case#</th>
                <th>Employee</th>
                <th>Employer</th>
                <th>Incident Type</th>
                <th>Allocated Staff</th>
                <th>Attended</th>
                <th>Stage</th>
                <th>Category</th>
                <th>Notification Date</th>
                <th>Registration Date</th>
                <th>Region</th>
                <th>District</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script>
    /*let $postData = function ($d) {
        $d.category = $('select[name=category]').val();
        $d.incident = $('select[name=incident]').val();
        $d.employee = $('select[name=employee]').val();
        $d.employer = $('select[name=employer]').val();
        $d.status = $('select[name=status]').val();
        $d.user_id = $('select[name=user_id]').val();
        $d.region = $('select[name=region]').val();
        $d.district = $('select[name=district]').val();
        $d.stage = $('select[name=stage]').val();
    };*/
/*    let $postData = {
        category : $('select[name=category]').val(),
        incident : $('select[name=incident]').val(),
        employee : $('select[name=employee]').val(),
        employer : $('select[name=employer]').val(),
        status : $('select[name=status]').val(),
        user_id : $('select[name=user_id]').val(),
        region : $('select[name=region]').val(),
        district : $('select[name=district]').val(),
        stage : $('select[name=stage]').val()
    };*/
/*    let $postData = function () {
        return {
            category : $('select[name=category]').val(),
            incident : $('select[name=incident]').val(),
            employee : $('select[name=employee]').val(),
            employer : $('select[name=employer]').val(),
            status : $('select[name=status]').val(),
            user_id : $('select[name=user_id]').val(),
            region : $('select[name=region]').val(),
            district : $('select[name=district]').val(),
            stage : $('select[name=stage]').val()
        };
    };*/
    $(function() {
        $(".search-select").select2({
            allowClear: true,
            debug: true,
            placeholder: ""
        });
        $('#region').on('change', function (e) {
            var region_id = e.target.value;
            if (region_id) {
                $("#spin2").show();
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district').empty();
                    $("#district").select2("val", "");
                    $('#district').html(data);
                    $("#spin2").hide();
                });
            }
        });
        $("#status").on("change", function () {
            var $status = $(this).val();
            switch($status) {
                case '3':
                    /* Selected to choose User Allocated */
                    $(".allocated_user_select").show();
                    break;
                default:
                    $(".allocated_user_select").hide();
                    break;
            }
        });
        $('#status').trigger('change');
        let $assign_status_contrl = $('#assign_status');
        $assign_status_contrl.on("change", function () {
            let $assign_status = $(this).val();
            switch($assign_status) {
                case '1':
                    $(".assign_user_select").show();
                    break;
                default:
                    $(".assign_user_select").hide();
                    break;
            }
        });
        $assign_status_contrl.trigger('change');
        let $allocation_category_contrl = $('#category');
        $allocation_category_contrl.on("change", function () {
            let $allocation_category = $(this).val();
            let $assign_for_select = $(".assign_for_select");
            switch($allocation_category) {
                case '1':
                    $assign_for_select.show();
                    $assign_for_select.closest('div').removeClass("col-md-2").addClass('col-md-2');
                    $assign_status_contrl.closest('div').removeClass("offset-md-6").addClass('offset-md-4');
                    break;
                default:
                    $assign_for_select.hide();
                    $assign_for_select.closest('div').removeClass("col-md-2");
                    $assign_status_contrl.closest('div').removeClass("offset-md-4").addClass('offset-md-6');
                    break;
            }
        });
        $allocation_category_contrl.trigger('change');
        /* start : Searching Employer */
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employer */
        /* start : Searching Employee */
        $(".employee-select").select2({
            minimumInputLength: 3,
            multiple: false,
            allowClear: true,
            debug: true,
            placeholder: "",
            ajax: {
                url: "{!! route('backend.compliance.employees') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.employee,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        }).on("select2:selecting", function($e) {
            var $selected = $e.params.args.data.id;
        });
        /* end : Searching Employee */
        $( "#clear_filter" ).click(function() {
            /* Clear the Filter Form */
            $(".employer-select").val(null).trigger('change.select2');
            $(".employee-select").val(null).trigger('change.select2');
            $(".search-select").val(null).trigger('change.select2');
        });
        var $oTable = $('#resource-allocation-table').DataTable({
            /*dom : 'Bfrtip',*/
            buttons : ['reload', 'colvis'],
            initComplete : function () {
                $oTable.buttons().container().insertBefore('#resource-allocation-table');
            },
            drawCallback : function () {
                getAllocationEmployerGroup();
                getAllocationStaffGroup();
            },
            processing: true,
            serverSide: true,
            info : true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: "{!! route('backend.claim.notification_report.allocation.get') !!}",
                method : "PUT",
                data: function ($data) {
                    $data = gatherDataDt($data, 'search-form');
                    /*$data.category = $('select[name=category]').val();
                    $data.incident = $('select[name=incident]').val();
                    $data.employee = $('select[name=employee]').val();
                    $data.employer = $('select[name=employer]').val();
                    $data.status = $('select[name=status]').val();
                    $data.user_id = $('select[name=user_id]').val();
                    $data.region = $('select[name=region]').val();
                    $data.district = $('select[name=district]').val();
                    $data.stage = $('select[name=stage]').val();*/
                    $data.assign_for = $('select[name=assign_for]').val();
                }
            },
            columnDefs: [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ],
            select: {
                'style': 'multi'
            },
            columns: [
                {
                    orderable: false,
                    searchable: false,
                    data: 'id',
                    name : 'notification_reports.id'
                },
                {data: 'filename', name: 'notification_reports.filename', visible: false},
                {data: 'employee', name: 'employee', searchable: false, orderable: false},
                {data: 'employer', name: 'employer_id', searchable: false, orderable: true},
                {data: 'incident', name: 'incident_types.name'},
                {data: 'allocated', name: 'allocated', searchable: false, orderable: false},
                {data: 'attended_status', name: 'attended_status', searchable: false, orderable: false},
                {data: 'stage', name: 'code_values.name', visible: false},
                {data: 'category', name: 'notification_reports.notification_staging_cv_id', searchable: false, orderable: true, visible: false},
                {data: 'receipt_date', name: 'notification_reports.receipt_date', searchable: false, orderable: true, visible: false},
                {data: 'created_at', name: 'notification_reports.created_at', searchable: false, orderable: true, visible: false},
                {data: 'region', name: 'regions.name', searchable: true, orderable: true, visible: false},
                {data: 'district', name: 'districts.name', searchable: true, orderable: true, visible: false}
            ],
            'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                $('td:not(:first-child)', $nRow).click(function() {
                    window.open(base_url + "/claim/notification_report/profile/" + $aData['id'], "_self");
                }).hover(function() {
                    $(this).css('cursor', 'alias');
                }, function() {
                    $(this).css('cursor', 'auto');
                });
            }
        });

        $('#search-form').on('submit', function($e) {
            $oTable.draw();
            /*alert($('select[name=category]').val());
            return false;*/
            $e.preventDefault();
        });

        @permission("resource_allocation")
            $('#assign-user').on('submit', function($e) {
                $e.preventDefault();
                let $form = this;
                let $rowsSelected = $oTable.column(0).checkboxes.selected();
                //Remove all previous selected
                $($form).find("input[name='id[]']").remove();
                //put a wait sign
                $($form).find('i').removeClass("fa-arrow-right").addClass('fa-spinner fa-spin');
                $($form).find('button').prop("disabled", true);
                // Iterate over all selected checkboxes
                $.each($rowsSelected, function($index, $rowId) {
                    // Create a hidden element
                    $($form).append($('<input>').attr('type', 'hidden').attr('name', 'id[]').val($rowId));
                });
                swal({
                    title: "Warning",
                    text: "Are you sure to allocate selected files to the selected assigned user",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function ($confirmed) {
                    if ($confirmed) {
                        //$form.submit();
                        //Do the ajax submission ...
                        //route : backend.claim.notification_report.allocation.assign
                        let $data = {};
                        $data = gatherDataDt($data, 'search-form');
                        $data['category'] = $('select[name=category]').val();
                        $data['assign_for'] = $('select[name=assign_for]').val();
                        var $options = {
                            dataType : "json",
                            data : $data,
                            type : "PUT",
                            method : "PUT",
                            url : $($form).attr("action"),
                            success : function (data) {
                                if (data.success) {
                                    $oTable.draw();
                                    $oTable.column(0).checkboxes.deselectAll();
                                    $.amaran({
                                        'theme'     :'awesome success',
                                        'content'   :{
                                            title : "Success",
                                            message: data.message,
                                            info:'',
                                            icon: 'fa fa-check-square-o'
                                        },
                                        'position'  :'bottom left',
                                        'outEffect' :'slideBottom',
                                        'inEffect'  :'slideLeft'
                                    });
                                    $('select[name=assign_for]').val(null).trigger('change.select2');
                                } else {
                                    alert(data.message);
                                    /*swal({ title : "Error Assigning User to Resource(s)", text : data.message});*/
                                }
                                $($form).find('i').removeClass("fa-spinner fa-spin").addClass('fa-arrow-right');
                                $($form).find('button').prop("disabled", false);
                            },
                            error: function (data) {
                                $($form).find('i').removeClass("fa-spinner fa-spin").addClass('fa-arrow-right');
                                $($form).find('button').prop("disabled", false);
                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    } else {
                        $($form).find('i').removeClass("fa-spinner fa-spin").addClass('fa-arrow-right');
                        $($form).find('button').prop("disabled", false);
                    }
                });

            });
        @endauth
    });
    function gatherDataDt($data, $id) {
        $('#' + $id).find(':input').each(function () {
            let $name = $(this).attr('name');
            $data[$name] = $(this).val();
        });
        return $data;
    }
    function getAllocationStaffGroup()
    {
        $.ajax({
            url: "{{ route("backend.claim.notification_report.allocation_staff_group") }}",
            dataType : 'html',
            /*async : false,*/
            method : "POST",
            data : {
                category : $('select[name=category]').val(),
                incident : $('select[name=incident]').val(),
                employee : $('select[name=employee]').val(),
                employer : $('select[name=employer]').val(),
                status : $('select[name=status]').val(),
                user_id : $('select[name=user_id]').val(),
                region : $('select[name=region]').val(),
                district : $('select[name=district]').val(),
                stage : $('select[name=stage]').val()
            }
        }).done(function ($data) {
            let $group = $("#allocation_staff_group");
            $group.empty();
            $group.html($data);
        });
    }
    function getAllocationEmployerGroup()
    {
        $.ajax({
            url: "{{ route("backend.claim.notification_report.allocation_employer_group") }}",
            dataType : 'html',
            /*async : false,*/
            method : "POST",
            data : {
                category : $('select[name=category]').val(),
                incident : $('select[name=incident]').val(),
                employee : $('select[name=employee]').val(),
                employer : $('select[name=employer]').val(),
                status : $('select[name=status]').val(),
                user_id : $('select[name=user_id]').val(),
                region : $('select[name=region]').val(),
                district : $('select[name=district]').val(),
                stage : $('select[name=stage]').val()
            }
        }).done(function ($data) {
            let $group = $("#allocation_employer_group");
            $group.empty();
            $group.html($data);
        });
    }
</script>
@endpush