@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.modify_death_notification'), 'header_title' => trans('labels.backend.claim.modify_death_notification')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_report, ['route' => ['backend.claim.notification_report.update', $notification_report->id ],'method'=>'put',
   'name'=>'update']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('claim_start_date', getClaimStartDate(), ['class' =>'claim_date']) !!}
    {!! Form::hidden('employee_id', $notification_report->employee_id,[]) !!}
    {!! Form::hidden('notification_report_id', $notification_report->id,[]) !!}
    {!! Form::hidden('incident_type_id', $notification_report->incident_type_id, ['class' =>'claim_date']) !!}
    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])

    <div>&nbsp;</div>

    {{--main contants--}}


    {{--death cause--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.death_cause'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('death_cause_id', $death_causes, $notification_report->death->death_cause_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('death_cause_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>



    {{--death date--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.death_date'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                            <span>  {!!  Form::selectRange('incident_day',1,31,\Carbon\Carbon::parse($notification_report->death->death_date)->format('d'), ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day',  'id'=>'incident_day']) !!}
                        </span>

                            <span>      {!!  Form::selectMonth('incident_month',\Carbon\Carbon::parse($notification_report->death->death_date)->format('m'), ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month',  'id'=>'incident_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('incident_year',Carbon\Carbon::now()->format('Y'),2015,\Carbon\Carbon::parse($notification_report->death->death_date)->format('Y'), ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year',  'id'=>'incident_year']) !!}
                        </span>
                            {!! Form::hidden('incident_date') !!}
                            {!! $errors->first('incident_date', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>

    {{--reporting date--}}

    <div class="row">
        <div class="col-md-12">

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.reporting_date'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.reporting_date_description')  "></i></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                          <span>      {!!  Form::selectRange('reporting_day',1,31,\Carbon\Carbon::parse($notification_report->death->reporting_date)->format('d'), ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day',  'id'=>'reporting_day']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('reporting_month',\Carbon\Carbon::parse($notification_report->death->reporting_date)->format('m'), ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month', 'id'=>'reporting_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('reporting_year',Carbon\Carbon::now()->format('Y'),2015,\Carbon\Carbon::parse($notification_report->death->reporting_date)->format('Y'), ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year', 'id'=>'reporting_year']) !!}
                        </span>
                            {!! Form::hidden('reporting_date') !!}
                            {!! $errors->first('reporting_date', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    {{--receipt date--}}

    <div class="row">
        <div class="col-md-12">

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.receipt_date'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.receipt_date_description')"></i></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                          <span>      {!!  Form::selectRange('receipt_day',1,31,\Carbon\Carbon::parse($notification_report->death->receipt_date)->format('d'), ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=>'receipt_day']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('receipt_month',\Carbon\Carbon::parse($notification_report->death->receipt_date)->format('m'), ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month', 'id'=>'receipt_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('receipt_year',Carbon\Carbon::now()->format('Y'),2015,\Carbon\Carbon::parse($notification_report->death->receipt_date)->format('Y'), ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year',  'id'=>'receipt_year']) !!}
                        </span>
                            {!! Form::hidden('receipt_date') !!}
                            {!! $errors->first('receipt_date', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- start: incident region--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Incident Region :</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('region_id', $regions, $region_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                        {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end: incident region--}}

    {{-- start : incident district--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Incident District :</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('district_id', $districts, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                        {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end : incident district--}}

    {{--death palce--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.death_place'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','death_place', $notification_report->death->death_place, ['class' => 'form-control']) !!}
                        {!! $errors->first('death_place', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>

    {{--certificate_number--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.certificate_number'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','certificate_number', $notification_report->death->certificate_number, ['class' => 'form-control']) !!}
                        {!! $errors->first('certificate_number', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>


    {{--districts--}}
{{--    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.district'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('district_id', $districts,$notification_report->death->district_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>--}}



    {{--incident Exposures ore events--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.incident_exposure'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('incident_exposure_cv_id', $incident_exposures, $notification_report->incident_exposure_cv_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('incident_exposure_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--INITIAL EXPENSEEEE--}}
    {{--Member type--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.initial_expense'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.initial_expense_description')  "></i></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('initial_expense_member_type_id', $member_types, $notification_report->initial_expense_member_type_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'initial_expense_member_type_id']) !!}
                        {!! $errors->first('initial_expense_member_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Payment methods--}}

    <div class="row" id="payment_methods">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.payment_method'):</label></div>
                <div class = "col-md-2">
                    <label>{!! Form::radio( 'pay_method',1 , ($notification_report->is_cash == 1) ? true : false, ['id' => 'is_cash' ]) !!}@lang('labels.backend.claim.is_cash')</label>
                    </br>
                    {!! $errors->first('pay_method', '<span class="help-block label label-danger">:message</span>') !!}
                </div>

                <div class = "col-md-2">
                    <label>{!! Form::radio( 'pay_method',2 , ($notification_report->is_cash == 1) ? false : true, ['id' => 'is_insurance' ]) !!}@lang('labels.backend.claim.is_insurance')</label>
                </div>

            </div>
        </div>
    </div>

    </br>
    {{--insurance type--}}
    <div class="row" id="insurance_div">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.insurance'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('pay_through_insurance_id', $insurances, $notification_report->pay_through_insurance_id , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'pay_through_insurance_id']) !!}
                        {!! $errors->first('pay_through_insurance_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Employer Id--}}
    <div class="row" >
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.employer'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('employer_id', $employers, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--date hired date--}}
    {{--date hired date--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.backend.member.date_hired'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                            <span>  {!!  Form::selectRange('hired_day',1,31,isset($date_hired) ? \Carbon\Carbon::parse($date_hired)->format('j') : null, ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=>'hired_day']) !!}
                        </span>

                            <span>      {!!  Form::selectMonth('hired_month',isset($date_hired) ?  \Carbon\Carbon::parse($date_hired)->format('n') : null , ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month' ,'id'=>'hired_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('hired_year',    Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(50)->format('Y'), isset($date_hired) ?  \Carbon\Carbon::parse($date_hired)->format('Y') : null , ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year' , 'id'=>'hired_year']) !!}
                        </span>

                        </div>
                        {!! Form::hidden('date_hired') !!}
                        {!! $errors->first('date_hired', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



        </div>
    </div>




    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.claim.notification_report.profile',trans('buttons.general.cancel'), [$notification_report->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();


    $('.search-select').select2();
    $('body').on('submit', 'form[name=update]', function(e) {
        e.preventDefault();
        // Validate date -- incident date
        var $day = $('#incident_day').val();
        var $month = $('#incident_month').val();
        var $year = $('#incident_year').val();
        if (($year) && ($month) && ($day )) {
            $('input[name=incident_date]').val($year + '-' + $month + '-' + $day);
        }else {
            $("input[name=incident_date]").val("");
        }

        // Validate date -- reporting_date
        var $reporting_day = $('#reporting_day').val();
        var $reporting_month = $('#reporting_month').val();
        var $reporting_year = $('#reporting_year').val();
        if (($reporting_year ) && ($reporting_month) && ($reporting_day )) {
            $('input[name=reporting_date]').val($reporting_year + '-' + $reporting_month + '-' + $reporting_day);
        } else{
            $("input[name=reporting_date]").val("");
        }


        // Validate date -- receipt_date
        var $receipt_day = $('#receipt_day').val();
        var $receipt_month = $('#receipt_month').val();
        var $receipt_year = $('#receipt_year').val();
        if (($receipt_year) && ($receipt_month ) && ($receipt_day )) {
            $('input[name=receipt_date]').val($receipt_year + '-' + $receipt_month + '-' + $receipt_day);
        }else{
            $("input[name=receipt_date]").val("");
        }




        // Validate date -- date_hired
        var $hired_day = $('#hired_day').val();
        var $hired_month = $('#hired_month').val();
        var $hired_year = $('#hired_year').val();
        if (($hired_year) && ($hired_month) && ($hired_day)) {
            $('input[name=date_hired]').val($hired_year + '-' + $hired_month + '-' + $hired_day);
        }else{
            $("input[name=date_hired]").val("");
        }




        this.submit();

    });

    $(function () {
        $("#payment_methods").hide();
        $("#insurance_div").hide();

        $('#region_id').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                $('#district_id').empty();
                $("#district_id").select2("val", "");
                $('#district_id').html(data);
                $("#spin2").hide();
            });
        });

        member_type_option('initial_expense_member_type_id','payment_methods', 'is_cash', 'is_insurance', 'insurance_div','pay_through_insurance_id');

        $("#initial_expense_member_type_id").on("change", function (e) {
            member_type_option('initial_expense_member_type_id','payment_methods', 'is_cash', 'is_insurance', 'insurance_div','pay_through_insurance_id');

        });

        cash_option('pay_through_insurance_id','is_cash','is_insurance','insurance_div');

        $("#is_cash").on("change", function (e) {
            cash_option('pay_through_insurance_id','is_cash','is_insurance','insurance_div');
        });
        insurance_option('pay_through_insurance_id','is_cash','is_insurance','insurance_div');
        $("#is_insurance").on("change", function (e) {
            insurance_option('pay_through_insurance_id','is_cash','is_insurance','insurance_div');
        });



    });


    function member_type_option(initial_expense_member_type_id,payment_methods, is_cash, is_insurance,insurance_div,pay_through_insurance_id) {

        var choice = $("#" + initial_expense_member_type_id).val();
//           employer / employee
        if ((choice == 1) || (choice == 2)) {
            $("#" + payment_methods).show();
            $("#" + insurance_div ).hide();

        } else if(choice == 6) {
            $("#" + payment_methods).hide();
            $("#" + is_insurance).prop("checked", false);
            $("#" + is_cash).prop("checked", false);
            $("#" + insurance_div ).show();
            $("#" + pay_through_insurance_id ).prop("disabled", false);

        }
    }
    function insurance_option( pay_through_insurance_id,is_cash,is_insurance,insurance_div) {

        if ($("#" + is_insurance).is(":checked")) {
            $("#" + pay_through_insurance_id ).prop("disabled", false);
            $("#" + insurance_div).show();
            $("#" + is_cash ).prop("checked", false);
        }
    }


    function cash_option( pay_through_insurance_id,is_cash,is_insurance,insurance_div) {

        if ($("#" + is_cash).is(":checked")) {
            $("#" + pay_through_insurance_id ).prop("disabled", true);
            $("#" + insurance_div).hide();
            $("#" + is_insurance ).prop("checked", false);
        }

    }
</script>;


@endpush
