{{--Survivor gratuity summary--}}

{{--Dependents gratuity Distribution--}
{{--For DEATH INCIDENT--}}
@if (in_array($notification_report->incident_type_id, [3,4,5]) && $dependents->dependentsWithPivot($notification_report->employee_id)->where('survivor_gratuity_flag', 1)->count() > 0)
    <legend class="grey_modal" >@lang('labels.backend.claim.dependents_gratuity_distribution')</legend>
    </br>
    <table class="table table-striped table-bordered" style="width:100%">
        <tbody>
        {{--Header--}}
        <tr>
            <th>@lang('labels.general.name')</th>
            <th>@lang('labels.general.receiver_type')</th>
            <th>@lang('labels.general.relationship')</th>
            <th>@lang('labels.backend.table.survivor_gratuity_percent')</th>
            <th>@lang('labels.backend.table.survivor_gratuity_amount')</th>
        </tr>
        {{--dependents--}}
        @foreach($dependents->dependentsWithPivot($notification_report->employee_id)->where('survivor_gratuity_flag', 1)->get() as $dependent)
            <tr>
                <td>{!! $dependent->fullname !!}</td>
                <td>@lang('labels.general.dependent')</td>
                <td>{!! $dependent->type_name !!}</td>
                <th>{!! $dependent->survivor_gratuity_percent !!}</th>
                <th>{!! number_format( ($dependent->survivor_gratuity_amount > 0) ? $dependent->survivor_gratuity_amount : ( ($dependent->survivor_gratuity_percent * 0.01) * $compensation_summary['lump_sum_payable'])  , 2 , '.' , ',' ) !!}</th>
            </tr>
        @endforeach



        </tbody></table>

@endif

{{--END Dependents gratuity distribution----------------------}}

