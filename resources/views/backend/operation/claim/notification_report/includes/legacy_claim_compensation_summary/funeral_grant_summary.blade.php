{{--{--Dependents Funeral grant Distribution--}--}}
{{--For DEATH INCIDENT--}}
@if (in_array($notification_report->incident_type_id, [3,4,5]))
    <legend class="grey_modal" >@lang('labels.backend.claim.funeral_grant_distribution')</legend>
    </br>
    <table class="table table-striped table-bordered" style="width:100%">
        <tbody>
        {{--Header--}}
        <tr>
            <th>@lang('labels.general.name')</th>
            <th>@lang('labels.general.receiver_type')</th>
            <th>@lang('labels.general.relationship')</th>
            <th>@lang('labels.backend.table.funeral_grant_percent')</th>
            <th>@lang('labels.backend.table.funeral_grant')</th>
        </tr>
        {{--dependents--}}

        @foreach($dependents->dependentsWithPivot($notification_report->employee_id)->where('funeral_grant','>', 0)->get() as $dependent)
            <tr>
                <td>{!! $dependent->fullname !!}</td>
                <td>@lang('labels.general.dependent')</td>
                <td>{!! $dependent->type_name !!}</td>
                <th>{!! $dependent->funeral_grant_percent !!}</th>
                <th>{!! number_format( ( ($dependent->funeral_grant )) , 2 , '.' , ',' ) !!}</th>
            </tr>
        @endforeach


        </tbody></table>
@endif

{{--END funeral grants distribution----------------------}}
