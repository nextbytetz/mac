{{--Compensation section--}}

{{--Lumpsum--}}
@include('backend/operation/claim/notification_report/includes/legacy_claim_compensation_summary/lumpsum_payable_summary')


{{--Monthly pension--}}
@include('backend/operation/claim/notification_report/includes/legacy_claim_compensation_summary/pension_distribution_summary')

{{--gratuity--}}
@include('backend/operation/claim/notification_report/includes/legacy_claim_compensation_summary/survivor_gratuity_summary')


{{--funeral grant--}}
@include('backend/operation/claim/notification_report/includes/legacy_claim_compensation_summary/funeral_grant_summary')
