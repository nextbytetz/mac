
{{--LUMPSUM PAYABLE SUMMARY--}}

@include("backend.operation.claim.notification_report.includes.progressive.benefits.min_max_payable_benefit_amount")

{{--Total summary--}}
   <table class="table table-striped table-bordered" style="width:100%">
        <tbody>
        {{--Header--}}
        <tr>
            <th>@lang('labels.backend.claim.compensation_type')</th>
            {{--<th>@lang('labels.general.amount')</th>--}}
            <th>@lang('labels.general.payable_amount')</th>
        </tr>
        {{--Mae--}}
        <tr>
            <td>@lang('labels.backend.claim.amount_of_mae')</td>
            {{--<th>{!!  number_format( (isset($compensation_summary['total_mae']) ? $compensation_summary['total_mae'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
            <th>{!!number_format( (isset($compensation_summary['total_mae']) ? $compensation_summary['total_mae'] : 0), 2 , '.' , ',' ) !!}</th>
        </tr>

        {{--ttd--}}
        <tr>
            <td>@lang('labels.backend.claim.amount_of_ttd') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.amount_of_ttd_description')  "></i></td>
            {{--<th>{!!  number_format( (isset($compensation_summary['ttd_amount']) ? $compensation_summary['ttd_amount'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
            <th>{!! number_format( (isset($compensation_summary['payable_ttd_amount']) ? $compensation_summary['payable_ttd_amount'] : 0) , 2 , '.' , ',' ) !!}</th>
        </tr>

        {{--tpd--}}
        <tr>
            <td>@lang('labels.backend.claim.amount_of_tpd') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.amount_of_tpd_description')  "></i></td>
            {{--<th>{!! number_format( (isset($compensation_summary['tpd_amount']) ? $compensation_summary['tpd_amount'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
            <th>{!!number_format( (isset($compensation_summary['payable_tpd_amount']) ? $compensation_summary['payable_tpd_amount'] : 0) , 2 , '.' , ',' )  !!}</th>
        </tr>
        {{--ppd_lumpsum / monthly pension--}}
        {{--ppd_lumpsum--}}
        @if(($notification_report->claim->pd_formatted > 0) && ($notification_report->claim->pd_formatted <= sysdefs()->data()->percentage_imparement_scale) )
            <tr>
                <td>@lang('labels.backend.claim.ppd_lumpsum') </td>
                {{--<th>{!!  number_format( (isset($compensation_summary['ppd_lumpsum']) ? $compensation_summary['ppd_lumpsum'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
                <th>{!!  number_format( (isset($compensation_summary['ppd_lumpsum_payable']) ? $compensation_summary['ppd_lumpsum_payable'] : 0) , 2 , '.' , ',' ) !!}</th>

            </tr>
        @endif

        {{--Total from Lumpsum Payment i.e. MAE, TTD, TPD and PPD < 30--}}
        <tr>
            <th>TOTAL COMPENSATION PAYMENT</th>
            {{--<th>{!!  number_format( (isset($compensation_summary['ppd_lumpsum']) ? $compensation_summary['ppd_lumpsum'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
            <th>{!!  number_format( (isset($compensation_summary['total_lumpsum']) ? $compensation_summary['total_lumpsum'] : 0) , 2 , '.' , ',' ) !!}</th>

        </tr>


        </tbody></table>

{{--end  compensation summary ------------------}}
