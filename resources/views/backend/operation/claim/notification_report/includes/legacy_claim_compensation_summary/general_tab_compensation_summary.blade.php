{{--Summary link--}}

<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend class="grey_modal" >@lang('labels.backend.claim.compensation_summary')</legend>
        </div>
        <div>&nbsp;</div>
        {{--COMPENSATION SUMMARY ON TOTAL FOR EACH BENEFIT============--}}
        <div class="col-md-12">

            @include('backend/operation/claim/notification_report/includes/legacy_claim_compensation_summary/compensation_summary_section')

        </div>

    </div>

</div>