
{{--medical-expenses-overview-table--}}

<legend class="grey_modal" >Assessment Checklist</legend>
<table class="display" cellspacing="0" width="100%" id ="medical-expenses-entities-table">
    <thead>
    <tr>

        <th>@lang('labels.backend.table.member_type')</th>
        <th>@lang('labels.backend.table.name')</th>
           <th>@lang('labels.backend.table.action')</th>
    </tr>
    </thead>

</table>

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#medical-expenses-entities-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.notification_report.assessment_opener', $notification_report->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'member_type_id' , name: 'member_type_id' },
                { data: 'name' , name: 'name' },
                          { data: 'action_buttons', name: 'action_buttons' }
            ],



        } );
    });
</script>;

@endpush