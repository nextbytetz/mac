
<div class="col-md-12" >
    <legend class="grey_modal" >@lang('labels.backend.legend.health_provider_service_referrals')</legend>

    <div class="col-md-12" >
        <div class="pull-right">

            {{--add new--}}
            @if($check_workflow == 0 || $check_if_is_level1_pending == 1)
                <span>
        <a href="{!! route('backend.claim.notification_report.create_health_provider_service', $notification_report->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
        </span>
            @endif
        </div>
    </div>

    <div>&nbsp;</div>
    {{--<div>&nbsp;</div>--}}

    <table class="display" cellspacing="0" width="100%"class="display" cellspacing="0" width="100%" id ="health-provider-services-table">
        <thead>
        <tr>

            <th>@lang('labels.backend.claim.health_provider')</th>
            <th>@lang('labels.backend.claim.health_service_checklist')</th>
            <th>@lang('labels.backend.table.from_date')</th>
            <th>@lang('labels.backend.table.to_date')</th>
            <th>@lang('labels.backend.table.amount')</th>
            <th>@lang('labels.backend.claim.medical_practitioner')</th>
            <th>@lang('labels.backend.claim.medical_expense')</th>
            <th>@lang('labels.backend.claim.referral')</th>
        </tr>
        </thead>

    </table>
</div>


@if($notification_report->notificationHealthProviders()->doesntHave('notificationHealthProviderServices')->count() > 0)
<div>&nbsp;</div>
    <div class="col-md-12" >
        <legend class="grey_modal" >Notification Health Providers (With no Service)</legend>

        <table class="display" cellspacing="0" width="100%"class="display" cellspacing="0" width="100%" id ="health-provider-no-services-table">
            <thead>
            @foreach($notification_report->notificationHealthProviders()->doesntHave('notificationHealthProviderServices')->get() as $nhp)
                <tr>

                    <th>{{$nhp->medicalExpense->compensated_entity_name_with_amount}}</th>
                    <th><a style="color:blue;" href="{{ route('backend.claim.notification_report.edit_health_provider_service', $nhp->id) }}"> Edit </a></th>
                </tr>
            @endforeach
            </thead>

        </table>
    </div>
@endif


@push('health-provider-services-script-end')

    <script  type="text/javascript">

        $("#tab_4_header").one("click", function(){
            $(function() {
                var url = "{!! url("/") !!}";
                $('#health-provider-services-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    searching: true,
                    paging: true,
                    info:true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.claim.notification_report.get_notification_health_provider_services', $notification_report->id) !!}',
                        type : 'get'
                    },
                    columns: [
                        { data: 'health_provider' , name: 'health_provider' },
                        { data: 'health_service_checklist_id' , name: 'health_service_checklist_id' },
                        { data: 'from_date', name: 'from_date' },
                        { data: 'to_date', name: 'to_date' },
                        { data: 'amount', name: 'amount'},
                        { data: 'medical_practitioner', name: 'medical_practitioner'},
                        { data: 'medical_expense', name: 'medical_expense'},
                        { data: 'rank', name: 'rank'}
                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = url + "/claim/notification_report/health_provider_service/"  + aData['notification_health_provider_id'] + "/edit";

                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    }

                } );
            });
        });
    </script>;

@endpush