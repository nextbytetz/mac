{{--Incident Not Death--}}

<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend class="grey_modal" >@lang('labels.backend.claim.compensation_summary')</legend>
        </div>
        <div>&nbsp;</div>
        {{--COMPENSATION SUMMARY ON TOTAL FOR EACH BENEFIT============--}}
        <div class="col-md-12">


            @include('backend/operation/claim/notification_report/includes/legacy_claim_compensation_summary/compensation_summary_section')




            {{--PAYMENT HISTORY -================--}}

            @if (($notification_report->claimCompensations()->count()))
                <legend class="grey_modal" >@lang('labels.general.payment_summary_overview')</legend>
                </br>
                <table class="table table-striped table-bordered" style="width:100%">
                    <tbody>
                    {{--Header--}}
                    <tr>
                        <th>@lang('labels.general.name')</th>
                        <th>@lang('labels.backend.claim.benefit_type')</th>
                        <th>@lang('labels.backend.claim.compensation_type')</th>
                        <th>@lang('labels.general.amount')</th>
                    </tr>
                    {{--claim compensations--}}
                    @foreach($notification_report->claimCompensations as $claim_compensation)
                        <tr>
                            <td>{!! $claim_compensation->compensated_entity_name !!}</td>
                            <td>{!! $claim_compensation->benefitType->name !!}</td>
                            <th>{!! $claim_compensation->compensationPaymentType->name !!}</th>
                            <th>{!! number_format( $claim_compensation->amount , 2 , '.' , ',' ) !!}</th>
                        </tr>
                    @endforeach
                    </tbody></table>
            @endif
            {{--end Payment History--------------}}

        </div>

    </div>
</div>