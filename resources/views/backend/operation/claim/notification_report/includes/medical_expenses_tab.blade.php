


<div class="col-md-12">




    {{--medical-expenses-tab-table--}}

    <legend class="grey_modal" >Medical Expense Overview</legend>


    {{--Add New--}}

    <div class="pull-right">

        <span>
        <a href="{!! route('backend.claim.claim_accrual.notification_report.create_medical_expense', $notification_report->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-medkit"></i>&nbsp;@lang('buttons.backend.claim.add_medical_expense')</a>
</span>

    </div>

    <table class="display" cellspacing="0" width="100%" id ="medical-expenses-tab-table">
        <thead>
        <tr>
            <th>@lang('labels.backend.table.member_type')</th>
            <th>@lang('labels.backend.table.name')</th>
            <th>@lang('labels.backend.claim.medical_expense')</th>

        </tr>
        </thead>

    </table>




</div>









{{--</div>--}}


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#medical-expenses-tab-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                paging: false,
                info:false,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.notification_report.medical_expenses_overview', $notification_report->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'member_type_id' , name: 'member_type_id' },
                    { data: 'name' , name: 'name' },
                    { data: 'amount', name: 'amount' },
//
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url + "/claim/notification_report/medical_expense/"    + aData['id'] + '/edit';
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }


            } );
        });
    </script>;

@endpush