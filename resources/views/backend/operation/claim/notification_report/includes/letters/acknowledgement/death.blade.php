<!DOCTYPE html>
<html>
<head>
</head>
<body>
<div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="color: #0000ff;"><strong><span style="font-size: 18pt;">WORKERS COMPENSATION FUND</span><br /></strong><br /></span></span></div>
<div style="padding-left: 30px;">
    <table style="width: 1012px; height: 178px; border: 0px #ffffff; border-collapse: collapse;" border="none" cellspacing="0" cellpadding="0">
        <tbody style="padding-left: 30px;">
        <tr style="height: 147px; padding-left: 30px;">
            <td style="width: 304px; height: 147px; padding-left: 30px;">
                <p style="margin-bottom: 0in; line-height: 100%;"><span style="font-family: arial, helvetica, sans-serif; font-size: 16pt; line-height: 16pt;"><span lang="en-US">Telegraphic address&nbsp; &nbsp; &ldquo;WCF&rdquo;<br /></span><span lang="en-US">Tel:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; +255 22 2926107<br /></span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; +255 22 2926108<br /><span lang="en-US">Fax:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+255 22 2926109<br /></span><span lang="en-US">Email:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<a href="mailto:info@wcf.go.tz">info@wcf.go.tz<br /></a></span><span lang="en-US">Web:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<a href="http://www.wcf.go.tz">www.wcf.go.tz</a><br /></span><span lang="en-US"><strong>In reply please quote:<br /></strong></span></span></p>
                <p style="margin-bottom: 0in; line-height: 100%;" align="justify"><span style="font-family: arial, helvetica, sans-serif; font-size: 16pt; line-height: 16pt;"><span lang="en-US"><strong>Ref. No: {{ $notification_report->filename }}/{{ $folio }}<br /></strong></span></span></p>
            </td>
            <td style="width: 294px; height: 147px; padding-left: 30px;"><span style="font-family: arial, helvetica, sans-serif; font-size: 16pt; line-height: 16pt;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src="{{ url("/") }}/public/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" />&nbsp;&nbsp;&nbsp;</span></td>
            <td style="width: 320px; height: 147px; padding-left: 30px;">
                <p style="text-indent: 0.5in; margin-bottom: 0in; padding-left: 30px;"><span style="font-family: arial, helvetica, sans-serif; font-size: 16pt; line-height: 16pt;"><span lang="en-US">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;P.O. Box 79655<br /></span><span style="text-indent: 0.5in;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;GEPF House<br /></span><span style="text-indent: 0.5in;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Plot No. 37<br /></span><span style="text-indent: 0.5in;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Regent Estate</span><span style="text-indent: 0.5in;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bagamoyo Road</span></span></p>
                <span style="line-height: 16pt; font-family: arial, helvetica, sans-serif; font-size: 16pt;"><span style="text-indent: 0.5in;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Dar es Salaam</span>&nbsp;</span><br />
                <p style="margin-bottom: 0in; line-height: 100%; padding-left: 30px;" align="justify"><span style="font-family: arial, helvetica, sans-serif; font-size: 16pt; line-height: 16pt;"><span lang="en-US"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{ \Carbon\Carbon::now()->format("d") }}<sup>{{ \Carbon\Carbon::now()->format("S") }}</sup></strong></span><span lang="en-US"><strong> {{ \Carbon\Carbon::now()->format("F Y") }} </strong></span></span></p>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;"><br /></span></span>
    <table style="width: 1011px; border: 0px #ffffff; border-collapse: collapse;" border="none" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td style="width: 1017px;">&nbsp; &nbsp; &nbsp; &nbsp; <span style="font-size: 16pt;">Managing Director,</span>
                <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">{{ $notification_report->employer->name }},</span></div>
                <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">P.O. Box {{ $notification_report->employer->box_no }},</span></div>
                <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;"><strong>{{ $notification_report->employer->region_label }}.<br /><br />REF: NOTIFICATION&nbsp; OF&nbsp; OCCUPATIONAL DEATH - {{ strtoupper($notification_report->employee->name) }}<br /><br /></strong>Please refer to the above caption.<br /><br /></span></div>
                <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">The Fund would like to acknowledge receiving the notification of death (WCN 1) which occurred on {{ \Carbon\Carbon::parse($notification_report->incident_date)->format("d") }}<sup>{{ \Carbon\Carbon::parse($notification_report->incident_date)->format("S") }}</sup>{{ \Carbon\Carbon::parse($notification_report->incident_date)->format(" F Y") }} to your&nbsp;employee, {{ $notification_report->employee->name }}. The reported death will be investigated as part of the compensation processing procedures.<br /><br /></span></div>
                @if (count($pending_documents))
                    <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">Please submit to the Fund the following documents;</span></div>
                @endif
                <div style="padding-left: 30px;">
                    <ol style="list-style-type: lower-roman; padding-left: 30px;">
                        @foreach ($pending_documents as $document)
                            <li style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">{{ $document->name . ((!is_null($document->description)) ? '  (' . $document->description . ')' : '')  }},</li>
                        @endforeach
                    </ol>
                    <span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">We will communicate to your office the date on which the Fund's staff will visit your organisation to conduct the investigation.<br /><br /></span></div>
                <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">Thank you for your cooperation.<br /><br /><img src="{{ url("/") }}/public/template/assets/nextbyte/img/rehema_kabongo_signature.png" width="190" height="120" /><br /></span></div>
                <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;">Rehema Kabongo</span></div>
                <div style="padding-left: 30px;"><span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;"><strong>FOR: DIRECTOR GENERAL</strong></span></div>
            </td>
        </tr>
        </tbody>
    </table>
    <span style="font-size: 16pt; font-family: arial, helvetica, sans-serif;"><br /></span></div>
</body>
</html>