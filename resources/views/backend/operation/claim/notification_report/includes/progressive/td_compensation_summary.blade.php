@if (isset($compensation_summary['td']))
    @if (count($compensation_summary['td']))
        <div class="underline">TD Payment</div>
        @foreach ($compensation_summary['td'] as $value)
            @php
                $showsummary = true;
                if ($value['processed'] == 0) {
                    $tdprocessed = $tdprocessed ?? 0;
                    if ($tdprocessed) {
                        $showsummary = false;
                    }
                }
            @endphp
            @if ($showsummary)
                <div class="computation-group">
                    @if (!empty($value['letter_status_label']))
                        <legend><span style="font-size: medium;"><b>&#x27A5;&nbsp;</b>&nbsp;{!! $value['letter_status_label'] !!}&nbsp;<span
                                        class='underline'><a
                                            href="{{ route("backend.letter.process", [$value['eligible'], "CLBNAWLTTR"]) }}"
                                            class="btn btn-secondary btn-sm site-btn">Award Letter</a></span></span>
                        </legend>
                    @endif
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/td/compensation_summary_entry", ['value' => $value])
                    <a href="#" data-eligible="{{ $value['eligible'] }}"
                       class="show-computation-summary small underline">
                        (
                        <span class="show-text" style="color: green;">{{ trans('labels.general.show') }} Computation Summary</span>
                        <span class="hide-text hidden" style="color: red;">{{ trans('labels.general.hide') }} Computation Summary</span>
                        )
                    </a>
                    <div class="computation-summary hidden" data-eligible="{{ $value['eligible'] }}"></div>
                </div>
                <br/>
            @endif
        @endforeach
        <div>&nbsp;</div>
    @endif
@endif