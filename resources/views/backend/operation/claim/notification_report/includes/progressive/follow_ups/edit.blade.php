@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_follow_up'), 'header_title' => trans('labels.backend.member.edit_follow_up')])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
{{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.claim.follow_ups.update_follow_up', $claim_followup_update->id], 'name' => 'edit_follow_up', 'id' => 'edit_follow_up', 'class' => 'edit_follow_up', 'method' => 'post',  'enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('notification_report_id', $notification->id, ['class' =>'']) !!}
   {{--  {!! Form::hidden('employer_id',$employer->id, ['class' =>'']) !!}
    {!! Form::hidden('needs_correction', $follow_up->needs_correction, ['class' =>'']) !!}
    {!! Form::hidden('staff_employer_id',isset( $staff_employer) ?  $staff_employer->id : null, ['class' =>'']) !!}
    {!! Form::hidden('start_date', isset( $staff_employer) ? $staff_employer->start_date : null, ['class' =>'']) !!}
    {!! Form::hidden('end_date', isset( $staff_employer) ? $staff_employer->end_date : null, ['class' =>'']) !!} --}}
    {!! Form::hidden('action_type', 2, ['class' =>'']) !!}
    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/progressive/follow_ups/includes/header_info", ['employee'=> $employee, 'notification' => $notification])
    <br/>

    {{--<div>--}}

        <br/>
    {{--</div>--}}
    <div class="pull-right" >
        <br/>
        @if($notification->allocated == auth()->user()->id)
        @if($claim_follow_up->review_status == 2 or is_null($claim_follow_up->review_status))
        {!! HTML::decode(link_to_route('backend.claim.follow_ups.delete_follow_up', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Delete follow up', $claim_followup_update->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to delete this follow up?', 'class' => 'btn btn-primary site-btn delete_button'])) !!}
        @endif
        @endif
    </span>
</div>


<div class="row">

    {{--<div class ="col-md-12">--}}

        <div class ="col-md-6">
            {{--Create form--}}
            @include("backend/operation/claim/notification_report/includes/progressive/follow_ups/includes/edit_form")

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-9 form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">
                                {!! link_to('claim/notification_report/profile/' . $notification->id . '#follow_ups',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                @if($notification->allocated == auth()->user()->id)
                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                                @endif


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        @if(isset($document_attached))
        {{--doc review--}}
        <div class ="col-md-6">

            <div class = "row">
                <div class="col-md-12">
                    {{--Document Preview--}}
                    <legend>Document Preview</legend>

                    <ol>
                        <li>
                            <a  id="{!! 'doc'. $document_attached->id !!}" class="doc_attached" style="color: blue" href="#">View Document</a>
                            {!! Form::hidden('doc_pivot_id', $document_attached->id, ['class' =>'']) !!}
                        </li>

                    </ol>
                    <br/>
                    <div id="document_frame" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>


                </div>

            </div>
        </div>
        @endif

    </div>

{{--</div>--}}




{!! Form::close() !!}
{{--</section>--}}

@stop



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{--<script  type="text/javascript">--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}


    <script>
        $(document).ready(function(){
            var form = $('.edit_follow_up');

            @if(auth()->user()->id != $notification->allocated)

            $(form[0].elements).not(':disabled')
            .addClass('test-ajax-disabled')
            .attr('disabled', true);
            @else
            @if($claim_follow_up->review_status == 2 or is_null($claim_follow_up->review_status))

            $(form).find('.test-ajax-disabled')
            .removeClass('test-ajax-disabled')
            .attr('disabled', false);
            @else
            $(form[0].elements).not(':disabled')
            .addClass('test-ajax-disabled')
            .attr('disabled', true);

            @endif
            @endif

            $('#edit_follow_up').submit(function(e){
                e.preventDefault()
                $('.save_button').attr('disabled', true)
                this.submit()
            })

            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                minDate: today_date,
            });
            /*-----------End Date Process------------*/

            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });

            feedback_option();
            $("#feedback_cv_id").on("change", function (e) {
                feedback_option();
            });

            reminder_email_option();
            $("#reminder_date").on("change", function (e) {
                reminder_email_option();
            });


            /*Documents which pending to be used list*/
            $(".doc_attached").click(function() {
                var $doc_id = this.id;
                var $pivot_id = $doc_id.substr(3);
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });






            function get_current_document($doc_pivot_id) {
                return $.ajax({
                    url: base_url + "/compliance/employer/staff_relation/preview/document/"+ $doc_pivot_id,
                    dataType : 'json',
                    async : false,
                    method : "POST"
                });
            }




        });
        getDistrictWard();
        $('.district_select').change(function(e){
            getDistrictWard();

        })

        function getDistrictWard(){

         // var dstid = $(th).val();
         var dstid = $( "#district_id option:selected" ).val();
         var postcode = "{{isset($postcode) ? $postcode->postcode : null}}";

         console.log()
         if (dstid.length > 0) {
            var url = "{{url('wards')}}/" + dstid;
            $.ajax({
               type: 'GET',
               url: url,
               success:function(response){
                console.log(response)
                var getresponse = '';
                var getresponse = '<option selected disabled hidden>-- Select Ward --</option>';
                for(var i in response){
                    var selected = '';
                    if (response[i].postcode == postcode) {
                        selected = 'selected';
                    }else{
                        selected = null;
                    }
                    getresponse +=
                    '<option value="'+response[i].postcode+'" '+selected+' style="width:150px;" >'+response[i].ward_name+'</option>';
                }

                $('.wards-select').empty();
                $('.wards-select').append(getresponse); 
                $('.wards').show();
            }
        }); 
        }else{
          $('.wards').hide();  
          $('.workplace').hide();
      }
  }

//         // Follow up type option
//         function follow_up_type_option(follow_type_id, contact_div, contact_id) {

//             var choice = $("#" + follow_type_id).val();
// //            if is visit
// if (choice == 'FUVSIT' || choice == 'FULETT') {
//     $("#contact_div" ).hide();
//     $('#doc_form_div').show();
// } else {
// //            phone call
// if (choice == 'FUPHONC') {
//     $("#" + contact_id).attr('placeholder','Phone #');
// }else if(choice == 'FUEMAIL'){
//     $("#" + contact_id).attr('placeholder','Email@yahoo.com');
// }
// $("#contact_div" ).show();
// $('#doc_form_div').hide();

// }
// }
function follow_up_type_option(follow_type_id, contact_div, contact_id, email_div) {

    var choice = $("#" + follow_type_id).val();
        //            if is visit
        if (choice == 'FUVSIT') {
            $("#" + contact_div).show();
            $("#" + email_div).show();
            $("#location_div").show();
            $("#location_description_div").show();
            $("#" + contact_id).attr('placeholder','Phone #');
            $("#email_id").attr('placeholder','Email@yahoo.com');
                // $('#doc_form_div').show();


            }else if ( choice == 'FULETT') {
                $("#" + contact_div).hide();
                $("#" + email_div).hide();
                $('#doc_form_div').show();

                $("#location_div").hide();
                $("#location_description_div").hide();
            } else {
            //            phone call
            if (choice == 'FUPHONC') {
                $("#" + contact_div).show();
                $("#" + contact_id).attr('placeholder','Phone #');
                $("#" + email_div).hide();
                $("#location_div").hide();
                $("#location_description_div").hide();
            }else if(choice == 'FUEMAIL'){
                // $("#" + contact_id).attr('placeholder','Email@yahoo.com');
                $("#" + contact_div).hide();
                $("#" + email_div).show();
                $("#email_id").attr('placeholder','Email@yahoo.com');
                $("#location_div").hide();
                $("#location_description_div").hide();
            }
            // $("#" + contact_div).show();
            // $('#doc_form_div').hide();
        }
    }


        // feedback option
        function feedback_option() {

            var choice = $("#feedback_cv_id" ).val();
//            if is others
if (choice == 'SEFOTH') {
    $("#remark_div").addClass("required");

} else {
    $("#remark_div").removeClass("required");
}
}



function reminder_email_option()
{
    var choice = $('#reminder_date').val();
    // if(choice.length > 0){
    //     $('#reminder_email_div').show();
    //     $('#reminder_email').prop('disabled', false);
    // }else{
    //     $('#reminder_email_div').hide();
    //     $('#reminder_email').prop('disabled', true);
    // }
}


$( ".delete_button" ).submit(function( event ) {
    event.preventDefault();
    swal({
        title: 'Are you sure?',
        text: "Please click confirm to delete this item",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true
    }, function() {
        $(".delete_button").off("submit").submit();
//     });).then(function() {

// }, function(dismiss) {

//     if (dismiss === 'cancel') {
//         swal('Cancelled', 'Delete Cancelled :)', 'error');
//     }
})
});

</script>;


@endpush