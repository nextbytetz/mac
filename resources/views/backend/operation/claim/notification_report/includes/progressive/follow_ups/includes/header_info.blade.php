
<div class = "row">
    {{--{!! Form::model($employee, ['route' => ['backend.compliance.employee.profile', $employee->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
    <div class="col-md-12">
        <h5 class="client-title">
            <i class="icon-circle"></i>
            <!-- Employer header detail -->
            <strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " . $employee->lastname , [ 'id'=> 'name']) !!}</strong>

            <small >
                @lang('labels.backend.table.member_#'): {!! Form::label( 'reg_no', $employee->memberno, [ 'id'=> 'reg_no']) !!}
            </small>,
            <strong> {!! Form::label( 'name', $notification->incidentType->name , [ 'id'=> 'name']) !!}</strong>

            <small >
                Case_#: {!! Form::label( 'case_no', $notification->filename, [ 'id'=> 'case_no']) !!}
            </small>
        </h5>
        <legend></legend>
    </div>
</div>