@php
    $workflow = $incident->workflows()->where("wf_done", 0)->orderByDesc("notification_workflows.id")->limit(1);
@endphp
@if ($workflow->count())
@php
    $workflowEntry = $workflow->first();
    $wfModule = $workflowEntry->wfModule;
    //$type = $wfModule->type;
    $moduleGroup = $wfModule->wfModuleGroup->id;
    $workflowinput = ['resource_id' => $workflowEntry->id, 'wf_module_group_id'=> $moduleGroup, 'type' => $wfModule->type];
@endphp
    @include("backend/includes/workflow/wf_track_html", $workflowinput)
    {{--{!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}--}}
    <br/>
@else
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <i class="fa fa-exclamation-circle"></i><strong>Info</strong> No active workflow recently initiated
    </div>
@endif