@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/7.33.1/css/sweetalert2.min.css") }}
<style>
    /* start: File Icons CSS */
    #online-folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
    #online-folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
    #online-folder-tree .file-pdf { background-position: -32px 0 }
    #online-folder-tree .file-as { background-position: -36px 0 }
    #online-folder-tree .file-c { background-position: -72px -0px }
    #online-folder-tree .file-iso { background-position: -108px -0px }
    #online-folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
    #online-folder-tree .file-cf { background-position: -162px -0px }
    #online-folder-tree .file-cpp { background-position: -216px -0px }
    #online-folder-tree .file-cs { background-position: -236px -0px }
    #online-folder-tree .file-sql { background-position: -272px -0px }
    #online-folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
    #online-folder-tree .file-h { background-position: -488px -0px }
    #online-folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
    #online-folder-tree .file-php { background-position: -108px -18px }
    #online-folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
    #online-folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
    #online-folder-tree .file-rb { background-position: -180px -18px }
    #online-folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
    #online-folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
    #online-folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
    #online-folder-tree .file-js { background-position: -434px -18px }
    #online-folder-tree .file-css { background-position: -144px -0px }
    #online-folder-tree .file-fla { background-position: -398px -0px }
    /* end: File Icon CSS */
</style>
@endpush
<br/>
<div class="row">
    <div class="col-md-12">
        <legend>Additional Optional Documents</legend>
        <div  style="border: 1px dotted #000;padding: 2px;">
            <div class="row">
                <div class="col-md-12">
                    @foreach (array_chunk($optional_docs->toArray(), $optional_docs->count()/4) as $docs)
                        <div class="col-md-3">
                            <ul style="margin:0;padding:0;list-style:none;">
                                @foreach($docs as $doc)
                                    <li>
                                        <input type="checkbox" value="{{$doc['id']}}" name="optional_docs[]" {{in_array($doc['id'], $opted_docs) ? 'checked' : ""}} id="document-{{ $doc['id'] }}" data-incident="{{ $incident->id }}" class="optional_docs" />
                                        <label for="document-{{ $doc['id'] }}"> {{ $doc['name'] }} </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12 pills-height">

        <div class="row">
            <div class="col-md-4">

                <legend>Online Attached document</legend>
                <div id="online-folder-tree" class="nopadding"></div>

                <br/>
                <br/>
            </div>
            <div class="col-md-8">
                {{--Document Preview--}}
                <legend>Online Document Preview</legend>
                <br/>
                <div id="online_document_frame">
                    {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                </div>
            </div>

        </div>
    </div>
</div>

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/7.33.1/js/sweetalert2.min.js") }}
<script>
    $(function() {
        let $online_folder = $('#online-folder-tree');
        /************************** start: Document Library JS ***************************/
        $online_folder.jstree({
            'core': {
                'data': {
                    'url': "{!! route('backend.compliance.employer.notification_report_document_library', $incident->id) !!}",
                    'data': function (node) {
                        return {'id': node.id};
                    }
                },
                'check_callback': function (o, n, p, i, m) {
                    if (m && m.dnd && m.pos !== 'i') {
                        return false;
                    }
                    if (o === "move_node" || o === "copy_node") {
                        if (this.get_node(n).parent === this.get_node(p).id) {
                            return false;
                        }
                    }
                    return true;
                },
                'force_text': false,
                'themes': {
                    'responsive': true,
                    'variant': 'small',
                    'stripes': true
                }
            },
            'sort': function (a, b) {
                return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            'contextmenu': {
                'items': function ($node) {
                    var $menu = {};
                    if (this.get_type($node) === "file") {
                        $menu = {
                            "validate" : {
                                "label" : "Validate",
                                "action" : function ($data) {
                                    /*console.log(node.original);*/
                                    if ($node.original.isverified === 0 || $node.original.isverified === 2) {
                                        Swal({
                                            title: $node.original.document_name,
                                            text: "Verifying Document",
                                            type: "info",
                                            showCloseButton: true,
                                            focusConfirm: false,
                                            showCancelButton: true,
                                            cancelButtonText: "Unverify",
                                            cancelButtonColor: "#dd733e",
                                            confirmButtonColor: "#5add42",
                                            confirmButtonText: "Verify",
                                            reverseButtons: true,
                                        }).then(($result) => {
                                            let $status = 0;
                                            let $send = 1;
                                            if ($result.value) {
                                                /*console.log("Verified");*/
                                                $status = 1;
                                            } else if ($result.dismiss === Swal.DismissReason.cancel) {
                                                /*console.log("Unverified");*/
                                                $status = 2;
                                            }
                                            verify_document($node.original.id, $send, $status).done(function ($data) {
                                                /*console.log($data);*/
                                                /* refresh jstree */
                                                if ($data.success) {
                                                    $online_folder.jstree("refresh");
                                                }
                                            });
                                        });
                                    } else {
                                        Swal('Document already verified.')
                                    }
                                }
                            }
                        };
                    }
                    return $menu;
                }
            },
            'types': {
                'default': {'icon': 'folder'},
                'file': {'valid_children': [], 'icon': 'file'}
            },
            'unique': {
                'duplicate': function (name, counter) {
                    return name + ' ' + counter;
                }
            },
            'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']
        });
        $online_folder.bind("click.jstree", function ($event, $data) {
            var $node = $($event.target).closest("li").attr("id");
            var $type = $online_folder.jstree().get_selected(true)[0].type;
            if ($type === 'default') {
                /*should only work on folders*/
                /*Document Folder Has Been Clicked*/
                $node = $node.split('/');
                $node = $node[$node.length - 1];
                if ($node.length > 0) {
                    //alert(1);
                } else {
                    //alert(2);
                }
            } else {
                /*preview the document here*/
                /*console.log($node);*/
                let $online_document_frame = $("#online_document_frame");
                get_current_document_from_path($node).done(function ($data) {
                    /*console.log($data);*/
                    $online_document_frame.find("iframe").remove();
                    let $iframe = $('<iframe allowfullscreen src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $online_document_frame.append($iframe);
                });
            }
        });
        $('.optional_docs').change(function() {
            let $check = 0;
            let $documentId = $(this).val();
            if ($(this).is(":checked")) {
                $check = 1;
            } else {
                $check = 0;
            }
            optional_document($check, $documentId).done(function ($data) {
                /*console.log($data);*/
            });
        });
    });

    function get_current_document_from_path($node) {
        $refNode = $node;
        /*return $.post( base_url + "/claim/notification_report/current_base64_document/{!! $incident->id !!}/" + $node, {}, function( data ) {
    }, "json");*/
        return $.ajax({
            url: base_url + "/claim/notification_report/current_online_document_from_path/{!! $incident->id !!}/" + $node,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }
    function verify_document($node, $send, $status) {
        return $.ajax({
            url: base_url + "/claim/notification_report/verify_branch_online_document/{!! $incident->id !!}/" + $node + "/" + $send + "/" + $status,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }
    function optional_document($check, $documentId) {
        return $.ajax({
            url: base_url + "/compliance/employer/{!! $incident->id !!}/notification_report_optional_docs/" + $check + "/" + $documentId,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }

</script>
@endpush