@extends('layouts.backend.main', ['title' => 'Claim Follow Up', 'header_title' => $user->name. ' - Claims Follow Up'])

@push('after-styles-end')

@endpush

@include('backend.includes.datatable_assets')

@section('content')

    {{-- @include("backend/operation/claim/investigation_plan/includes/menu") --}}

<br/>
{{--Table--}}
<div class = "row">
	<div class="col-md-12" >
		<table class="display" cellspacing="0" width="100%" id ="main-relation-follow-ups-table">
			<thead>
				<tr >
					<th>Employee</th>
					<th>Case no</th>
					<th>Status</th>
					<th>District</th>
					<th>Start date</th>
					<th>End Date</th>
					<th>Missing Documents</th>
					<th>Follow Ups</th>
					<th>Attendance Status</th>
					<th>Review Status</th>
				</tr>
			</thead>
		</table>

	</div>
</div>
@endsection




@push('after-script-end')


<script  type="text/javascript">
	$(function() {
		var url = "{!! url("/") !!}";
		// $("#followups_header").one("click", function(){
			$('#main-relation-follow-ups-table').DataTable({
				processing: true,
				serverSide: true,
				stateSave: true,
				stateSaveCallback: function (settings, data) {
					localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
				},
				stateLoadCallback: function (settings) {
					return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
				},
				ajax:{
					url : '{!! route('backend.claim.follow_ups.get_user_follow_ups', $user->id) !!}',
					type : 'get'
				},
				columns: [
				{ data: 'employee', name: 'employee',  orderable : false, searchable : false},
				{ data: 'case_no', name: 'case_no',  orderable : true, searchable : true},
				{ data: 'status', name: 'status',  orderable : true, searchable : true},
				{ data: 'district', name: 'district',  orderable : true, searchable : true},
				{ data: 'start_date', name: 'start_date',  orderable : true, searchable : true},
				{ data: 'end_date', name: 'end_date' ,orderable : true, searchable : true},
				{ data: 'missing_documents', name: 'missing_documents' ,orderable : true, searchable : true},
				{ data: 'follow_ups', name: 'follow_ups' ,orderable : true, searchable : true},
				{ data: 'attendance_status', name: 'attendance_status' ,orderable : true, searchable : true},
				{ data: 'review_status', name: 'review_status',  orderable : true, searchable : false},
				],
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).click(function() {
						// console.log(aData['user_id'])
						// return false;
						document.location.href = url +  "/claim/notification_report/profile/" + aData['notification_report_id'] + "#follow_ups" ;
					}).hover(function() {
						$(this).css('cursor','pointer');
					}, function() {
						$(this).css('cursor','auto');
					});
				}

			});
		});

	// });


</script>;

@endpush