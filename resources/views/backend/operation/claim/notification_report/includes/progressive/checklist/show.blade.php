<div class="employee_particulars">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right" >
                @if (1)
                    @if ($incident->can_register_employee)
                        {{--register employee for notification--}}
                        {!! HTML::decode(link_to_route('backend.claim.notification_report.register_employee', "<i class='icon fa fa-money' aria-hidden='true'></i>&nbsp;" . "Register Employee", [$incident->id],['class' => 'btn  btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
                    @endif
                    @if ($incident->can_add_contribution)
                        {{--add contribution information--}}
                        {!! HTML::decode(link_to_route('backend.claim.notification_report.add_contribution', "<i class='icon fa fa-money' aria-hidden='true'></i>&nbsp;" . "Add Contribution", [$incident->id],['class' => 'btn  btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
                    @endif
                    @if ($incident->can_edit_contribution)
                        {{--edit contribution information--}}
                        {!! HTML::decode(link_to_route('backend.claim.notification_report.edit_contribution', "<i class='icon fa fa-money' aria-hidden='true'></i>&nbsp;" . "Edit Contribution", [$incident->id],['class' => 'btn  btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
                    @endif
                    @if ($incident->can_edit_employee)
                            {{--update employee--}}
                        {!! HTML::decode(link_to_route('backend.claim.notification_report.update_employee_checklist', "<i class='icon fa fa-pencil-square-o' aria-hidden='true'></i>&nbsp;" . "Update Employee", [$incident->id],['class' => 'btn  btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
                    @endif
                @endif

            </div>
        </div>
    </div>
    <legend></legend>
    @include("backend/operation/claim/notification_report/includes/progressive/checklist/employee", ['employee' => $employee])
</div>
<div class="incident_particulars">
    @if ($incident->has_checklist)
        @if (in_array($incident->incident_type_id, [1,4]))
            {{--Accident--}}
            @include("backend/operation/claim/notification_report/includes/progressive/checklist/accident/show")
            @endif

        @if (in_array($incident->incident_type_id, [2,5]))
            {{--Disease--}}
            @include("backend/operation/claim/notification_report/includes/progressive/checklist/disease/show")
            @endif
        @if (in_array($incident->incident_type_id, [3,4,5]))
            {{--Death--}}
            @include("backend/operation/claim/notification_report/includes/progressive/checklist/death/show")
        @endif
    @else
        <div class="alert-left-border">
            <div class="alert alert-primary alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Checklist Not Updated.</strong> This notification has no associated checklist recorded.
            </div>
        </div>
    @endif
</div>