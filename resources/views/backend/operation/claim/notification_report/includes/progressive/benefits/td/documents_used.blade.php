{{--"dnr.description",
"dnr.doc_date",
"dnr.doc_receive_date",
"dnr.doc_create_date",
"d.name as document",--}}
@foreach($assessment_documents as $assessment_document)
    <div class="row" id="td_document_used_group{{ $assessment_document->id }}">
            <div class="col-md-12">
                <div class="grid-column">
                    <span class="underline">{{ $assessment_document->description }}</span>&nbsp;
                    <span class="pull-right">
                        <a href="#" data-id="{{ $assessment_document->id }}" class="remove_td_document_used" data-toggle='tooltip' data-html='true' title='Remove document used for TD'><i class="icon fa fa-2x fa-remove" aria-hidden="true" style="color:darkred;"></i></a>
                    </span><br/>

                    <span><b>Prepared</b>&nbsp;:&nbsp;{{ $assessment_document->doc_date }}</span>&nbsp;
                    <span><b>Received</b>&nbsp;:&nbsp;{{ $assessment_document->doc_receive_date }}</span>&nbsp;
                    <span><b>Created</b>&nbsp;:&nbsp;{{ $assessment_document->doc_create_date }}</span>&nbsp;
                    <span><b>Hospital</b>&nbsp;:&nbsp;{{ $assessment_document->hospital }}</span>&nbsp;
                    <span><button class="btn btn-sm btn-secondary document_preview" data-id="{{ $assessment_document->document_notification_report_id }}"><i class="fa fa-eye-slash preview_icon"></i></button></span>

                </div>
            </div>

    </div>
@endforeach