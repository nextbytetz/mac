@php
    $death = $incident->death;
@endphp
<div class="row">
    <div class="col-md-12">
        <legend>General</legend>
        <br/>


        <div class="light_grey_bg" style="padding: 5px;border-radius: 4px;">
            <div class="underline" style="font-weight: lighter;">Cause of Death&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($death->deathCause()->count()) ? $death->deathCause->name : "-") }}</span></div>
            @if ($death->accidentCause()->count())
                <div class="underline" style="font-weight: lighter;">Cause of Accident&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->accidentCause->name }}</span></div>
            @endif
            <div class="underline" style="font-weight: lighter;">{{ $death->incident_date_description }}&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->incident_date_formatted Or "-" }}</span></div>
            <div class="underline" style="font-weight: lighter;">Date of Death&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->death_date_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">Date of Hire&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $incident->datehired_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">Date Last Worked&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $incident->last_work_date_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">Paid Salary?&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $incident->paid_month_before_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;border: dotted #e6f5f0 2px !important;background-color: #d3dfd0;">Gross Salary&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $incident->monthly_earning_checklist_formatted !!}</span></div>

            <div class="underline" style="font-weight: lighter;">Death Incident Exposure&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($death->exposure()->count()) ? $death->exposure->name : "-") }}</span></div>

            <div class="underline" style="font-weight: lighter;">Burial Permit No&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->burial_permit_no Or "-" }}</span></div>
            <div class="underline" style="font-weight: lighter;">Death Certificate No&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->death_certificate_no Or "-" }}</span></div>
            <div class="underline" style="font-weight: lighter;">Name of Hospital where death was confirmed&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->healthProvider->fullname_info Or "-" }}</span></div>
            <div class="underline" style="font-weight: lighter;">Name of Medical practitioner who confirmed death&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->medicalPractitioner->fullname_with_external_id Or "-" }}</span></div>
            <div class="underline" style="font-weight: lighter;">Date when death was confirmed&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $death->confirm_date_formatted }}</span></div>

        </div>
        <br/>

    </div>
</div>