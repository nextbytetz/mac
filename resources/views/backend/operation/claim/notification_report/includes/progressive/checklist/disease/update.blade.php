@php
    $disease= $incident->disease;
@endphp

    <div class="medical_treatments">
        <div class="row">
            <div class="col-md-12">
                <div class="fileld-layout">
                    <div class="form-group">
                        <div class="form-inline">
                            @if ($incident->treatments()->count())
                                {{--$incident->treatments()->count()--}}
                                <span>
                                    <label class="required">Still Attending Medical Treatment?</label>&nbsp;&nbsp;
                                </span>
                                <span>
                                    {!! Form::select('on_treatment', [1 => 'Yes', 0 => 'No'], $disease->on_treatment, ['class' => 'search-select on_treatment_select', 'style' => 'width:5%']) !!}&nbsp;&nbsp;
                                </span>
                                <span class="on_treatment">
                                    <label class="required">Same Hospital?</label>&nbsp;&nbsp;
                                </span>
                                <span class="on_treatment">
                                    {!! Form::select('same_hospital', [1 => 'Yes', 0 => 'No'], null, ['class' => 'same_hospital_select', 'style' => 'width:5%']) !!}&nbsp;&nbsp;
                                </span>
                                <span class="add_medical_treatment">
                                    <label>Add Medical Treatment</label>&nbsp;&nbsp;
                                </span>
                                <span class="add_medical_treatment">
                                    <a href="#" id="add_medical_treatment" data-toggle='tooltip' data-html='true' title='Add medical treatment entry'><i class="icon fa fa-2x fa-plus" aria-hidden="true" style="color:darkblue"></i></a>
                                </span>
                            @else
                                <span class="add_medical_treatment">
                                    <label>Add Medical Treatment</label>&nbsp;&nbsp;
                                </span>
                                <span class="add_medical_treatment">
                                    <a href="#" id="add_medical_treatment" data-toggle='tooltip' data-html='true' title='Add medical treatment entry'><i class="icon fa fa-2x fa-plus" aria-hidden="true" style="color:darkblue"></i></a>
                                </span>
                            @endif

                        </div>
                        <small class="form-text text-muted" style="width:100% !important;">Can add multiple medical treatments from different hospitals if still attending medical treatments</small>
                    </div>
                </div>
                <div id="treatment_entries">
                    {{--@include('backend/finance/receipt/includes/monthly_contribution_entry')--}}
                    @if ($incident->treatments()->count())
                        @include("backend.operation.claim.notification_report.includes.progressive.checklist.treatments.list", ['treatments' => $incident->treatments, 'count' => 0, 'insurances' => $insurances, 'member_types' => $member_types, 'health_state_checklists' => $health_state_checklists])
                    @else
                        @include("backend.operation.claim.notification_report.includes.progressive.checklist.treatments.add", ['count' => 0, 'insurances' => $insurances, 'member_types' => $member_types, 'health_state_checklists' => $health_state_checklists])
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="row">
        <div class="col-md-4">
            {{--Returned to work--}}
            <div class="fileld-layout">
                <label class="required">Returned to Work?</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('return_to_work', [0 => 'No', 1 => 'Yes'], $disease->return_to_work, ['class' => 'search-select return_to_work_select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;"></small>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            {{--Return Date--}}
            <div class="fileld-layout return_to_work">
                <label class="required">Return to Work Date</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('return_to_work_date', $disease->return_to_work_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <span class="help-block">
                        {{--<span>Date hired to the current employer</span>--}}
                        <small class="form-text text-muted" style="width:100% !important;"></small>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            {{--Job Title On Return To Work--}}
            <div class="fileld-layout return_to_work">
                <label class="required">Occupation on Return to Work</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('job_title_id', $job_titles, $disease->job_title_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>