@php
    $hideoption = $hideoption ?? 0;
    $claim = $incident->claim;
    $incident_assessment = $assessment->assessment;
@endphp
<div class="row">
    <div class="col-md-11">
        {{--put a benefit summary here--}}
        <span class="pull-left">
                {{--Put label for status here--}}
            {!! $assessment->processed_label !!}
            {!! $assessment->isinitiated_label !!}
            {!! $assessment->letter_status_label !!}
            </span>
        @if (!$hideoption)
            <span class="pull-right">

                @if ($assessment->isinitiated)
                    <span class="dropdown">
                <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonLinks" data-toggle="dropdown" >Options</a>
                    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonLinks" style="z-index: 99999999">

                        {{--Put pd assessment button here--}}
                        {!! Html::decode(link_to_route('backend.claim.notification_report.pd_assessment', "<i class='icon fa fa-filter' aria-hidden='true'></i>&nbsp;" . "Assessment", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                        <div class="dropdown-divider"></div>

                        @if ($incident->claim->pd <= sysdefs()->data()->percentage_imparement_scale)
                            {{--Process PD Lumpsum Payment--}}
                            {!! Html::decode(link_to_route('backend.claim.notification_report.process_benefit_payment', "<i class='icon fa fa-money' aria-hidden='true'></i>&nbsp;Process Payment" , [$incident->id, $assessment->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to process Payment", 'class' => 'dropdown-item'])) !!}
                            {{--<div class="dropdown-divider"></div>--}}
                        @endif
                        @if ($assessment->ispayprocessed Or $assessment->iszeropaid)
                            <div class="dropdown-divider"></div>
                            {{--Put award letter button here--}}
                            {!! Html::decode(link_to_route('backend.letter.process', "<i class='icon fa fa-newspaper-o' aria-hidden='true'></i>&nbsp;" . "Award Letter", [$assessment->id, "CLBNAWLTTR"],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                        @endif

                    </span>
                </span>
                @endif

                @if (!$assessment->isinitiated)
                    {{--initiate workflow approval for payment--}}
                    {!! Html::decode(link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate", ['class' => 'btn btn-secondary site-btn initiate_benefit_workflow', 'data-description' => 'Initiate PD Payment Workflow', 'data-incident' => $incident->id, 'data-benefit' => $assessment->benefit->id, 'data-eligible' => $assessment->id, 'data-route' => route("backend.claim.notification_report.initiate_benefit_workflow")])) !!}
                @endif

            </span>
        @endif
        <br/>
        <br/>

        @php
            $pd_compensation_summary_entry = (new \App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository())->progressiveCompensationApprove($incident, 0, $assessment->id);
            $pd_comments = $incident_assessment->pd_comments;
            //dd($td_compensation_summary_entry);
        @endphp

        {{--start : Put Summary Tables Here--}}
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td>Percent of Permanent Disability (PD%)</td>
                <th>{{ number_2_format($assessment->incident->claim->pd) }}</th>
            </tr>
            </tbody>
        </table>

        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td>Nature of Injury</td>
                <th>{{ $incident->natureOfIncident()->count() ? $incident->natureOfIncident->name : '' }}</th>
                <td>WPI</td>
                <th>{{ $incident_assessment->wpi }}</th>
            </tr>
            <tr>
                <td>Body Part Injured</td>
                <th>{{ implode(", ", ($incident->injuries()->count()) ? $incident->injuries()->select([DB::raw("body_part_injuries.name")])->pluck("name")->all() : []) }}</th>
                <td>FEC</td>
                @if ($incident_assessment->pdInjury()->count())
                    @php
                        $fec_rank = $incident_assessment->pdInjury;
                    @endphp
                    <th>{{ ($fec_rank) ? $fec_rank->name . ' => ' . $fec_rank->rank : '' }}</th>
                @else
                    <th>-</th>
                @endif
            </tr>
            <tr>
                <td>Lost Body Parts</td>
                <th>{{ implode(", ", ($incident->lostBodyParties()->count()) ? $incident->lostBodyParties()->select([DB::raw("body_part_injuries.name")])->pluck("name")->all() : []) }}</th>
                <td>PD Impairment</td>
                <th>{{ $incident_assessment->pdImpairment()->count() ? $incident_assessment->pdImpairment->name : "" }}</th>

            </tr>
            <tr>
                <td>Computation Method</td>
                <th>-</th>
                <td>PD Occupation</td>
                <th>{{ $incident_assessment->occupation()->count() ? $incident_assessment->occupation->name : "" }}</th>
            </tr>
            <tr>
                <td>Need for rehabilitation</td>
                <th>{!! $claim->need_rehabilitation_label !!}</th>
                <td>Age</td>
                <th>{{ $incident_assessment->age }}</th>
            </tr>
            <tr style="border: dotted #e6f5f0 2px !important;background-color: #e9e9e4;">
                <td>Need for Constant Care Attendant</td>
                <th>{!! $claim->need_cca_label !!}</th>
                <td>Date of MMI</td>
                <th>{{ $claim->date_of_mmi }}</th>
            </tr>
            <tr>
                <td>PD Amputation</td>
                <th>{{ $incident_assessment->pdAmputation()->count() ? $incident_assessment->pdAmputation->name : "" }}</th>
                <td></td>
                <th></th>
            </tr>
            </tbody>
        </table>

        @if (!is_null($pd_comments))
            <div class="underline">General Comments</div>
            <div>{{ $pd_comments }}</div>
            <br/>
        @endif


        <legend class="grey_modal" >@lang('labels.backend.claim.compensation_summary')</legend>
        @include("backend.operation.claim.notification_report.includes.progressive.benefits.min_max_payable_benefit_amount")
        @include("backend/operation/claim/notification_report/includes/progressive/benefits/pd/compensation_summary_entry", ['value' => count($pd_compensation_summary_entry['pd']) ? $pd_compensation_summary_entry['pd'] : [], 'claim' => $incident->claim, 'sysdefs' => sysdefs()->data() ])


        {{--end : Put Summary Tables Here--}}

    </div>
    <div class="col-md-1">
            <span class="pull-right">
                {{--if it is not initiated, can be deleted--}}
                @if (!$assessment->isinitiated)
                    {!! HTML::decode(link_to_route('backend.claim.notification_report.delete_benefit_payment', "<i class='icon fa fa-trash fa-2x' aria-hidden='true' style='color:darkred;'></i>", [$incident->id, $assessment->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to delete this benefit payment?", 'data-toggle' => 'tooltip', 'data-html' => 'true', 'title' => 'Delete Benefit Payment']))  !!}
                @endif
            </span>
    </div>
</div>