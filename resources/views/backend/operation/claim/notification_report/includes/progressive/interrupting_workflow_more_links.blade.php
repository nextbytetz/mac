<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >On Demand Workflow</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">

        {{--Incorrect Contribution Workflow--}}
        {{--@if ($incident->initialize_incorrect_contribution_workflow)--}}
        @if (true)
            {!! Html::decode($incident->initialize_incorrect_contribution_link) !!}
            <div class="dropdown-divider"></div>
            {!! Html::decode($incident->initialize_transfer_to_death_link) !!}
        @endif

        {{--@if ($incident->initialize_incorrect_employee_info_workflow)
            <div class="dropdown-divider"></div>
            {!! Html::decode($incident->initialize_incorrect_employee_info_link) !!}
        @endif--}}

     </span>
</span>