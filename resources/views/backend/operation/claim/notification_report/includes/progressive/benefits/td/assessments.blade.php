{{--"dnr.description",
"dnr.doc_date",
"dnr.doc_receive_date",
"dnr.doc_create_date",
"d.name as document",--}}
@foreach($assessments as $assessment)
    <div class="row" id="td_assessment_group{{ $assessment->id }}">
        <div class="col-md-12">
            <div class="grid-column">
                <span class="underline">{{ $assessment->notificationDisabilityState->disabilityStateChecklist->name }}</span>&nbsp;
                <span class="pull-right">
                    <a href="#" data-id="{{ $assessment->id }}" class="remove_td_assessment" data-toggle='tooltip' data-html='true' title='Remove assessmented TD'><i class="icon fa fa-2x fa-remove" aria-hidden="true" style="color:darkred;"></i></a>
                </span><br/>
                <span><b>From Date</b>&nbsp;:&nbsp;{{ $assessment->from_date }}</span>&nbsp;
                <span><b>To Date</b>&nbsp;:&nbsp;{{ $assessment->to_date }}</span>&nbsp;
                <span><b>Days</b>&nbsp;:&nbsp;{{ $assessment->days }}</span>&nbsp;
                <span><b>Remarks</b>&nbsp;:&nbsp;{{ $assessment->remarks }}</span>&nbsp;

            </div>
        </div>

    </div>
@endforeach