@php
    $color = [
        0 => 'blue',
        1 => 'grey',
    ];
@endphp

@foreach ($maeSubs as $maeSub)

    @php
        $i = str_random(5);
    @endphp

    <!-- start : sub expense line entry -->
    <div class="row" id="sub_expense_entry_group{!! $i !!}">
        <div class="col-md-12" style="padding-left:20px; border-left: 2px solid {!! $color[$count % 2] !!};">
            {!! Form::hidden("mae_sub_id" . $i, $maeSub->id) !!}
            <legend>{{ $count + 1 }}</legend>
            <br/>

            <div class="row">
                <div class="col-md-4">
                    {{--health provider--}}
                    <div class="fileld-layout">
                        <label>Treatment Hospital</label>
                        <div class="form-group">
                            <div class="input-group">
                                @if ($assessment)
                                    {!! Form::select('health_provider_id' . $i, $maeSub->healthProvider()->get()->pluck("fullname_info", "id")->all(), $maeSub->health_provider_id, ['placeholder' => '','class' => 'form-control search-select health-provider-select', 'readonly' => 'readonly']) !!}
                            {{--</div>--}}
                                @else
                                    {!! Form::select('health_provider_id' . $i, $maeSub->healthProvider()->get()->pluck("fullname_info", "id")->all(), $maeSub->health_provider_id, ['placeholder' => '','class' => 'form-control search-select health-provider-select']) !!}
                                @endif
                            </div>

                            <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;"></small>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    {{--receipt number input--}}
                    <div class="fileld-layout">
                        <label class="required">Receipt Number</label>
                        <div class="form-group">
                            <div class="input-group">
                                @if ($assessment)
                                    {!! Form::text('rctno' . $i, $maeSub->rctno, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'readonly' => 'readonly']) !!}
                                @else
                                    {!! Form::text('rctno' . $i, $maeSub->rctno, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                @endif

                                <span class="input-group-addon"><i class="icon fa fa-qrcode"></i></span>
                            </div>
                            <span class="help-block">
                            {{--<span>Date when accident occurred</span>--}}
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    {{--sub entry amount--}}
                    <div class="fileld-layout">
                        <label class="required">Amount</label>
                        <div class="form-group">
                            <div class="input-group">
                                @if ($assessment)
                                    {!! Form::text('sub_amount' . $i, $maeSub->amount, ['placeholder' => '', 'class' => 'form-control sub_amount', 'autocomplete' => 'off', 'readonly' => 'readonly']) !!}
                                @else
                                    {!! Form::text('sub_amount' . $i, $maeSub->amount, ['placeholder' => '', 'class' => 'form-control sub_amount', 'autocomplete' => 'off']) !!}
                                @endif

                                <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                            </div>
                            <span class="help-block">
                            {{--<span>Date when accident occurred</span>--}}
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    {{--assessed amount--}}
                    <div class="fileld-layout">
                        <label>Assessed Amount</label>
                        <div class="form-group">
                            <div class="input-group">
                                @if ($assessment)
                                    {!! Form::text('sub_assessed_amount' . $i, $maeSub->assessed_amount, ['placeholder' => '', 'class' => 'form-control sub_assessed_amount', 'autocomplete' => 'off']) !!}
                                @else
                                    {!! Form::text('sub_assessed_amount' . $i, $maeSub->assessed_amount, ['placeholder' => '', 'class' => 'form-control sub_assessed_amount', 'readonly' => 'readonly', 'autocomplete' => 'off']) !!}
                                @endif

                                <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                            </div>
                            <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;"></small>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    @if ($count > 0)
                        <span class="pull-right">
                        <a href="#" id="{!! $i !!}" class="remove_sub_expense_entry" data-toggle='tooltip' data-html='true' title='Remove medical expense entry'><i class="icon fa fa-2x fa-remove" aria-hidden="true" style="color:darkred;"></i></a>
                    </span>
                    @endif
                </div>
            </div>

            @if ($assessment)
                <div class="row">
                    <div class="col-md-11">
                        <div class="fileld-layout">
                            <label>Remarks</label>
                            <div class="form-group">
                                {!! Form::textarea( 'remarks' . $i, $maeSub->remarks, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none']) !!}
                                <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;"></small>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>

        <div>&nbsp;</div>
    </div>
    <!-- end : sub expense line entry -->

    @php
        $count += 1;
    @endphp
@endforeach
