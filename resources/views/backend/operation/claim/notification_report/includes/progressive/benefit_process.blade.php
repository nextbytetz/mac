<br/>
<div class="row">
    <div class="col-md-6">
        <small class="pull-right">{!! link_to_route('backend.claim.notification_report.recheck_benefit_status','Recheck Benefit Status', [$incident->id],[ 'class' => 'btn btn-sm btn-success site-btn ', ]) !!}</small>
        <br/>
        <legend>Documents Status Per Possible Benefit</legend>
        <br/>
        <div class="row">
            @foreach ($benefits as $benefit)
                <div class="col-md-6">
                    <div class="underline">{{ $benefit->name }}</div>
                    @php
                        $docs = $benefit->documents();
                    @endphp
                    <ul style="margin:0;padding:0;list-style:none;">
                        @foreach ($docs as $doc)
                            <li>
                                {{--<input type="checkbox" data-doc-id="{{ $doc->id }}" value="{{ $doc->name }}" name="documents[]"  {{in_array($doc->id, $uploaded_documents) ? 'checked' : ""}} onclick="return false;">&nbsp;{{ $doc->name }}--}}
                                @if (in_array($doc->id, $uploaded_documents))
                                    <i class="icon fa fa-check-square-o" aria-hidden="true"></i>&nbsp;
                                @else
                                    <i class="icon fa fa-square-o" aria-hidden="true"></i>
                                @endif
                                {{ $doc->name }}
                            </li>
                        @endforeach
                    </ul>
                    <br/>
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-md-6">
        <legend>Registered Uninitiated Compensation Payment</legend>
        <br/>
        {{--Put all pending compensations assessment here--}}
        @php
            $assessment_model = $incident->benefits()->where("processed", 0)->where("isinitiated", 0)->whereNull("parent_id")->orderByDesc("notification_eligible_benefits.id");
        @endphp

        @if ($assessment_model->count())
            @php
                $assessments = $assessment_model->get();
            @endphp

            @foreach ($assessments as $assessment)
                @if ($assessment->benefit_type_claim_id == 1)
                    {{--MAE Refund (Employee/Employer)--}}
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/mae_refund_member/summary", ['assessment' => $assessment])
                @endif
                {{--@if ($assessment->benefit_type_claim_id == 2)
                    --}}{{--MAE Refund (HCP/HSP)--}}{{--
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/mae_refund_thirdparty/summary", ['assessment' => $assessment])
                @endif--}}
                @if ($assessment->benefit_type_claim_id == 3)
                    {{--Temporary Disablement--}}
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/td/summary", ['assessment' => $assessment])
                @endif
                @if ($assessment->benefit_type_claim_id == 4)
                    {{--Temporary Disablement Refund--}}
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/td_refund/summary", ['assessment' => $assessment])
                @endif
                @if ($assessment->benefit_type_claim_id == 5)
                    {{--Permanent Disablement--}}
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/pd/summary", ['assessment' => $assessment])
                @endif
                @if ($assessment->benefit_type_claim_id == 6)
                    {{--Funeral Grant--}}
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/funeral_grant/summary", ['assessment' => $assessment])
                @endif
                @if ($assessment->benefit_type_claim_id == 7)
                    {{--Survivors--}}
                    @include("backend/operation/claim/notification_report/includes/progressive/benefits/survivor/summary", ['assessment' => $assessment])
                @endif
            @endforeach
        @else
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="fa fa-exclamation-circle"></i><strong>Info</strong> No Entry
            </div>
        @endif

    </div>
</div>

<br/>