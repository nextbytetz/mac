@php
    $accident = $incident->accident;
@endphp
<div class="row">
    <div class="col-md-12">

        <legend>Notification & Claim Details</legend>
        <br/>
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td>Date of Accident:</td>
                <th>{{ $accident->accident_date_formatted }}</th>
            </tr>
            <tr>
                <td>Whether employer  notify the Fund within 7 days of occurrence of the incidence:</td>
                <th><span class='tag tag-success white_color'>Yes</span></th>
            </tr>
            <tr>
                <td>Whether the claim submitted to the Fund within 12 months from the date of occurrence of the incidence:</td>
                <th><span class='tag tag-success white_color'>Yes</span></th>
            </tr>
            </tbody>
        </table>

        <legend>Contribution Summary</legend>
        <br/>
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td>Whether one month before accident contribution were remitted:</td>
                <th>{!! $incident->contrib_before_status !!}</th>
            </tr>
            <tr>
                <td>Whether the contribution in the month of accident was remitted:</td>
                <th>{!! $incident->contrib_on_status !!}</th>
            </tr>
            </tbody>
        </table>

    </div>
</div>