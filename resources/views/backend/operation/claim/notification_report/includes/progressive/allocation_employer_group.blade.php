@if ($allocation->count())
    @php
        $allocations = $allocation->get();
    @endphp
    <div>
            <br/>
            <table class="table table-striped table-bordered">
                <tbody>
                @foreach($allocations as $allocation)
                    <tr>
                        <td><b>{{ $allocation->name }}</b></td>
                        <td>{{ $allocation->employers }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    </div>
@else
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Employer Allocation Group.</strong>
        </div>
    </div>
@endif