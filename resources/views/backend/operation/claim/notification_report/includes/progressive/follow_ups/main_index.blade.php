@extends('layouts.backend.main', ['title' => 'Claim Follow Up', 'header_title' => 'Claim Follow Up'])

@push('after-styles-end')
{{ Html::style(asset_url() . "/datatable_assets/jquery.dataTables.min.css") }}
{{ Html::style(asset_url() . "/datatable_assets/buttons.dataTables.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style type="text/css">
	.search-select {
		height: 300px;
		overflow-y: auto;
	}
</style>
@endpush

@include('backend.includes.datatable_assets')

@section('content')

{{-- @include("backend/operation/claim/investigation_plan/includes/menu") --}}

<br/>
{{--Table--}}
<div class="custom_filter">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="form">
				<div class="row">
					<div class="form-group col-md-2">
						<label for="incident">Month</label>
						<input type="month" name="follow_up_date" id="month" class="form-control">
					</div>
					<div class="form-group col-md-2">
						<label for="incident">Staff</label>
						<select id="staff" type="select" class="form-control search-select">
							<option value="-1">-- Select Staff --</option>
							@foreach($staffs as $staff)
							<option value="{{$staff->id}}">{{$staff->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<div class="col-md-6">
							<input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
							<input type="submit" class="btn btn-success btn-sm btn-submit" id="btn-sbmt" value="@lang('buttons.general.search')" />
						</div>
						<div class="col-md-6">
							<a href="" class="btn btn-sm btn-success pull-right" id="export">Export</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class = "row">
		<div class="col-md-12" >
			<table class="display" cellspacing="0" width="100%" id ="main-relation-follow-ups-table">
				<thead style="background-color: lightskyblue;">
					<tr>
						<th>Sn</th>
						<th>Staff</th>
						<th>Total Files</th>
						<th>Target File/ Current week</th>
						<th>1st week attended</th>
						<th>1st week missed</th>
						<th>1st week Overall</th>
						<th>2nd week attended</th>
						<th>2nd week missed</th>
						<th>2nd week Overall</th>
						<th>3rd week attended</th>
						<th>3rd week missed</th>
						<th>3rd week Overall</th>
						<th>4th week attended</th>
						<th>4th week missed</th>
						<th>4th week Overall</th>
						<th>Overall</th>
					</tr>
				</thead>
			</table>

		</div>
	</div>
	@endsection



	@push('after-script-end')
	
	{{ Html::script(asset_url() . "/datatable_assets/jquery-3.5.1.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/jquery.dataTables.min.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/dataTables.buttons.min.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/buttons.flash.min.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/jszip.min.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/pdfmake.min.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/vfs_fonts.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/buttons.html5.min.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/buttons.print.min.js") }}
	{{ Html::script(asset_url() . "/datatable_assets/buttons.colVis.min.js") }}

	{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}


	<script  type="text/javascript">
		$(document).ready( function(){
			$('.search-select').select2({
			// placeholder : 'select staff'
		});

			$( "#clear_filter" ).click(function() {
				$('#month').val(null);
				// $('#staff option:selected').empty();
				$('#staff').val(-1).trigger('change');
				// $('#staff').empty()
				});

			$('#export').click(function(e){
				e.preventDefault()
				var $month = $('#month').val();
				var $staff = $('#staff option:selected').val();
				let my_url = '{!! url('claim/follow_ups/export_follow_up_perfomance?follow_up_date=') !!}' + $month + '&staff_id=' + $staff;
				window.location.href = my_url;
			})

			var url = "{!! url("/") !!}";
			var $month = $('#month').val();
			var $staff = $('#staff option:selected').val();

			var follow_up_datatable = $('#main-relation-follow-ups-table').DataTable({
				processing: true,
				serverSide: true,
				stateSave: false,
				searching: false,
				paging: false,
				stateSaveCallback: function (settings, data) {
					localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
				},
				stateLoadCallback: function (settings) {
					return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
				},
				dom: 'Bfrtip',
				buttons: ['colvis'],
				"lengthMenu": [50],
				"order": [[ 16, "desc" ]],
				"columnDefs": [
				{ className: "dt-right", "targets": [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16] },
				],

				ajax:{
					url : '{!! url('claim/follow_ups/get_main_follow_up_datatable?follow_up_date=') !!}' + $month + '&staff_id=' + $staff,
					type : 'get'
				},
				columns: [
				{ data: 'DT_Row_Index', name: 'DT_Row_Index',  orderable : true, searchable : false},
				{ data: 'staff', name: 'staff',  orderable : true, searchable : false},
				{ data: 'total_files', name: 'total_files',  orderable : true, searchable : false},
				{ data: 'files_per_week', name: 'files_per_week',  orderable : true, searchable : false},
				{ data: '1st_week_updated', name: '1st_week_updated' ,orderable : true, searchable : false},
				{ data: '1st_week_missed', name: '1st_week_missed' ,orderable : true, searchable : false, visible :false},
				{ data: '1st_week', name: '1st_week' ,orderable : true, searchable : false},
				{ data: '2nd_week_updated', name: '2nd_week_updated' ,orderable : true, searchable : false},
				{ data: '2nd_week_missed', name: '2nd_week_missed' ,orderable : true, searchable : false, visible :false},
				{ data: '2nd_week', name: '2nd_week' ,orderable : true, searchable : false},
				{ data: '3rd_week_updated', name: '3rd_week_updated' ,orderable : true, searchable : false},
				{ data: '3rd_week_missed', name: '3rd_week_missed' ,orderable : true, searchable : false, visible :false},
				{ data: '3rd_week', name: '3rd_week' ,orderable : true, searchable : false},
				{ data: '4th_week_updated', name: '4th_week_updated' ,orderable : true, searchable : false},
				{ data: '4th_week_missed', name: '4th_week_missed' ,orderable : true, searchable : false, visible :false},
				{ data: '4th_week', name: '4th_week' ,orderable : true, searchable : false},
				{ data: 'overall', name: 'overall',  orderable : true, searchable : false},
				],
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).click(function() {
						document.location.href = url +  "/claim/follow_ups/user_follow_ups/" + aData['user_name'] ;
					}).hover(function() {
						$(this).css('cursor','pointer');
					}, function() {
						$(this).css('cursor','auto');
					});
				}

			});

			$('#btn-sbmt').click(function(e){
				e.preventDefault();
				let $month = $('#month').val();
				var $staff = $('#staff option:selected').val();

				let my_url = '{!! url('claim/follow_ups/get_main_follow_up_datatable?follow_up_date=') !!}' + $month + '&staff_id=' + $staff;
				follow_up_datatable.ajax.url(my_url).load();
			})
		});



	</script>;

	@endpush