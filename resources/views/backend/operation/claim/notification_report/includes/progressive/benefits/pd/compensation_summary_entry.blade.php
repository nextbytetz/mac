@if(($claim->pd_formatted > 0) And ($claim->pd_formatted <= $sysdefs->percentage_imparement_scale) )
    <div class="underline">PD Payment</div>
    {{--PD less than or equal to 30--}}
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Compensation Type</th>
            <th>Payable Amount</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>@lang('labels.backend.claim.ppd_lumpsum')</td>
            @if (count($value))
                <td>{!!  number_2_format($value['ppd_lumpsum_payable']) !!}</td>
                <td>{!! $value['status'] !!}</td>
            @else
                <td>-</td>
                <td>Pending</td>
            @endif

        </tr>
        </tbody>
    </table>
@elseif ($claim->pd_formatted > $sysdefs->percentage_imparement_scale)
    <legend class="grey_modal" >Monthly Pension</legend>
    <table class="table table-striped table-bordered" style="width:100%">
        <thead>
            {{--Header--}}
            <tr>
                <th>@lang('labels.backend.claim.compensation_type')</th>
                {{--<th>@lang('labels.general.amount')</th>--}}
                <th>@lang('labels.general.payable_amount')</th>
                <th>Status</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>@lang('labels.backend.claim.monthly_pension') </td>
                @if (count($value))
                    <td>{!! number_2_format($value['monthly_pension_payable']) !!}</td>
                    <td>{!! $value['status'] !!}</td>
                @else
                    <td>-</td>
                    <td>Pending</td>
                @endif
            </tr>
            @if ($claim->need_cca)
                <tr>
                    <td>@lang('labels.backend.claim.assistant_monthly_pension') </td>
                    @if (count($value))
                        <td>{!! number_2_format($value['monthly_pension_assistant']) !!}</td>
                        <td>{!! $value['status'] !!}</td>
                    @else
                        <td>-</td>
                        <td>Pending</td>
                    @endif
                </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="underline">PD Payment</div>
    {{--PD less than or equal to 30--}}
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Compensation Type</th>
                <th>Payable Amount</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>@lang('labels.backend.claim.ppd_lumpsum')</td>
                <td>-</td>
                <td>Pending</td>
            </tr>
        </tbody>
    </table>
@endif