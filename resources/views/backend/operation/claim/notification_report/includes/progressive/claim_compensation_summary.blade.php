<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend class="grey_modal">@lang('labels.backend.claim.compensation_summary')</legend>
        </div>
        <div>&nbsp;</div>
        {{--COMPENSATION SUMMARY ON TOTAL FOR EACH BENEFIT============--}}
        <div class="col-md-12">

            {{--ppd_lumpsum / monthly pension--}}
            {{--ppd_lumpsum--}}
            {{--@if(($incident->claim->pd_formatted > 0) && ($incident->claim->pd_formatted <= sysdefs()->data()->percentage_imparement_scale) )
                <tr>
                    <td>@lang('labels.backend.claim.ppd_lumpsum') </td>
                    <th>{!!  number_format( (isset($compensation_summary['ppd_lumpsum_payable']) ? $compensation_summary['ppd_lumpsum_payable'] : 0) , 2 , '.' , ',' ) !!}</th>
                </tr>
            @endif--}}
            @if (isset($compensation_summary['mae']))
                @if (count($compensation_summary['mae']))
                    <div class="underline">MAE Payment</div>

                    @foreach ($compensation_summary['mae'] as $value)
                        <div class="computation-group">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Compensation Type</th>
                                    <th>Payable Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if (!empty($value['letter_status_label']))
                                    <legend><span style="font-size: medium;"><b>&#x27A5;&nbsp;</b>&nbsp;{!! $value['letter_status_label'] !!}&nbsp;<span
                                                    class='underline'><a
                                                        href="{{ route("backend.letter.process", [$value['eligible'], 'CLBNAWLTTR']) }}"
                                                        class="btn btn-secondary btn-sm site-btn">Award Letter</a></span></span>
                                    </legend>
                                @endif
                                <tr>
                                    <td>@lang('labels.backend.claim.amount_of_mae')</td>
                                    <td>{{ number_2_format($value['assessed_amount']) }}</td>
                                    <td>{!! $value['status'] !!}</td>
                                </tr>
                                </tbody>
                            </table>

                            <a href="#" data-eligible="{{ $value['eligible'] }}"
                               class="show-computation-summary small underline">
                                (
                                <span class="show-text" style="color: green;">{{ trans('labels.general.show') }} Computation Summary</span>
                                <span class="hide-text hidden" style="color: red;">{{ trans('labels.general.hide') }} Computation Summary</span>
                                )
                            </a>
                            <div class="computation-summary hidden" data-eligible="{{ $value['eligible'] }}"></div>
                        </div>
                        <br/>

                    @endforeach

                    <div>&nbsp;</div>
                @endif
            @endif

            @include("backend/operation/claim/notification_report/includes/progressive/td_compensation_summary", ['processed' => 0])

            @if ($incident->claim()->count())
                <div class="underline">PD Lumpsum</div>
                @if (($incident->claim->pd_formatted > 0) And ($incident->claim->pd_formatted <= sysdefs()->data()->percentage_imparement_scale))

                    @if (!empty($compensation_summary['pd']['letter_status_label']))
                        <div class="computation-group">
                            <legend><span style="font-size: medium;"><b>&#x27A5;&nbsp;</b>&nbsp;{!! $compensation_summary['pd']['letter_status_label'] !!}&nbsp;<span
                                            class='underline'><a
                                                href="{{ route("backend.letter.process", [$compensation_summary['pd']['eligible'], "CLBNAWLTTR"]) }}"
                                                class="btn btn-secondary btn-sm site-btn">Award Letter</a></span></span>
                            </legend>
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Compensation Type</th>
                                    <th>Payable Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>@lang('labels.backend.claim.ppd_lumpsum')</td>
                                    <td>{!!  number_format( (isset($compensation_summary['pd']['ppd_lumpsum_payable']) ? $compensation_summary['pd']['ppd_lumpsum_payable'] : 0) , 2 , '.' , ',' ) !!}</td>
                                    <td>{!! $compensation_summary['pd']['status'] !!}</td>
                                </tr>
                                </tbody>
                            </table>
                            <a href="#" data-eligible="{{ $compensation_summary['pd']['eligible'] }}"
                               class="show-computation-summary small underline">
                                (
                                <span class="show-text" style="color: green;">{{ trans('labels.general.show') }} Computation Summary</span>
                                <span class="hide-text hidden" style="color: red;">{{ trans('labels.general.hide') }} Computation Summary</span>
                                )
                            </a>
                            <div class="computation-summary hidden" data-eligible="{{ $compensation_summary['pd']['eligible'] }}"></div>
                        </div>
                        <br/>
                    @endif
                @endif

                @if(($incident->claim->pd_formatted > 0) And ($incident->claim->pd_formatted <= sysdefs()->data()->percentage_imparement_scale) )
                    {{--<div class="underline">PD Lumpsum</div>--}}

                @endif

                @if ($incident->claim->pd_formatted == 0)
                    {{--<div class="underline">PD Lumpsum</div>--}}
                    @if (count($compensation_summary['pd']))
                        <div class="computation-group">
                            <legend><span style="font-size: medium;"><b>&#x27A5;&nbsp;</b>&nbsp;{!! $compensation_summary['pd']['letter_status_label'] !!}&nbsp;<span
                                            class='underline'><a
                                                href="{{ route("backend.letter.process", [$compensation_summary['pd']['eligible'], "CLBNAWLTTR"]) }}"
                                                class="btn btn-secondary btn-sm site-btn">Award Letter</a></span></span>
                            </legend>
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Compensation Type</th>
                                    <th>Payable Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>@lang('labels.backend.claim.ppd_lumpsum')</td>
                                    <td>0</td>
                                    <td>{!! $compensation_summary['pd']['status'] Or 'Cancelled' !!}</td>
                                </tr>
                                </tbody>
                            </table>
                            <a href="#" data-eligible="{{ $compensation_summary['pd']['eligible'] }}"
                               class="show-computation-summary small underline">
                                (
                                <span class="show-text" style="color: green;">{{ trans('labels.general.show') }} Computation Summary</span>
                                <span class="hide-text hidden" style="color: red;">{{ trans('labels.general.hide') }} Computation Summary</span>
                                )
                            </a>
                            <div class="computation-summary hidden" data-eligible="{{ $compensation_summary['pd']['eligible'] }}"></div>
                        </div>
                        <br/>
                    @endif
                @endif

                {{--end  compensation summary ------------------}}

                {{-- Total Monthly Pension Summary, For Death & PD >= 30 --}}

                @if(($incident->claim->pd_formatted > sysdefs()->data()->percentage_imparement_scale) || (in_array($incident->incident_type_id, [3,4,5])))

                    @if (in_array($incident->incident_type_id, [3,4,5]))
                        @if (!empty($compensation_summary['death']['letter_status_label']))
                            <legend><span style="font-size: medium;"><b>&#x27A5;&nbsp;</b>&nbsp;{!! $compensation_summary['death']['letter_status_label'] !!}&nbsp;<span
                                            class='underline'><a
                                                href="{{ route("backend.letter.process", [$compensation_summary['death']['eligible'], "CLBNAWLTTR"]) }}"
                                                class="btn btn-secondary btn-sm site-btn">Award Letter</a></span></span>
                            </legend>
                        @endif
                    @else
                        @if (!empty($compensation_summary['pd']['letter_status_label']))
                            <legend><span style="font-size: medium;"><b>&#x27A5;&nbsp;</b>&nbsp;{!! $compensation_summary['pd']['letter_status_label'] !!}&nbsp;<span
                                            class='underline'><a
                                                href="{{ route("backend.letter.process", [$compensation_summary['pd']['eligible'], "CLBNAWLTTR"]) }}"
                                                class="btn btn-secondary btn-sm site-btn">Award Letter</a></span></span>
                            </legend>
                        @endif
                    @endif

                    <legend class="grey_modal">Monthly Pension</legend>
                    <div class="computation-group">
                        <table class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            {{--Header--}}
                            <tr>
                                <th>@lang('labels.backend.claim.compensation_type')</th>
                                {{--<th>@lang('labels.general.amount')</th>--}}
                                <th>@lang('labels.general.payable_amount')</th>
                            </tr>
                            </thead>

                            <tbody>
                            {{--monthly_pension For PPD / PTD--}}
                            @if($incident->claim->pd_formatted > sysdefs()->data()->percentage_imparement_scale)
                                <tr>
                                    <td>@lang('labels.backend.claim.monthly_pension') </td>

                                    {{--<th>{!!  number_format( (isset($compensation_summary['pd']['monthly_pension']) ? $compensation_summary['pd']['monthly_pension'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
                                    @if (in_array($incident->incident_type_id, [3,4,5]))
                                        <th>{!!  number_format( (isset($compensation_summary['death']['monthly_pension_payable']) ? $compensation_summary['death']['monthly_pension_payable'] : 0) , 2 , '.' , ',' ) !!}</th>
                                    @else
                                        <th>{!!  number_format( (isset($compensation_summary['pd']['monthly_pension_payable']) ? $compensation_summary['pd']['monthly_pension_payable'] : 0) , 2 , '.' , ',' ) !!}</th>
                                    @endif

                                </tr>
                            @endif

                            {{--For DEATH INCIDENT--}}
                            @if (in_array($incident->incident_type_id, [3,4,5]))
                                <tr>
                                    <td>@lang('labels.backend.claim.monthly_pension') </td>
                                    {{--<th>{!!  number_format( (isset($compensation_summary['death']['monthly_pension']) ? $compensation_summary['death']['monthly_pension'] : 0) , 2 , '.' , ',' ) !!}</th>--}}
                                    <th>{!!  number_format( (isset($compensation_summary['death']['monthly_pension_payable']) ? $compensation_summary['death']['monthly_pension_payable'] : 0) , 2 , '.' , ',' ) !!}</th>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                        @if (in_array($incident->incident_type_id, [1,2]))
                            <a href="#" data-eligible="{{ $compensation_summary['pd']['eligible'] }}"
                               class="show-computation-summary small underline">
                                (
                                <span class="show-text" style="color: green;">{{ trans('labels.general.show') }} Computation Summary</span>
                                <span class="hide-text hidden" style="color: red;">{{ trans('labels.general.hide') }} Computation Summary</span>
                                )
                            </a>
                            <div class="computation-summary hidden" data-eligible="{{ $compensation_summary['pd']['eligible'] }}"></div>
                        @endif
                    </div>
                    <br/>
                @endif

                {{--End of Total monthly pension summary-------------}}

                {{-- monthly_pension_distribution=====================-}}
                {{--For DEATH INCIDENT--}}
                {{--@if (($notification_report->incident_type_id == 3) || ($notification_report->claim->pd > sysdefs()->data()->percentage_imparement_scale))--}}
                @if (in_array($incident->incident_type_id, [3,4,5]))
                    <legend class="grey_modal">@lang('labels.backend.claim.monthly_pension_distribution')</legend>
                    </br>
                    <table class="table table-striped table-bordered" style="width:100%">
                        <tbody>
                        {{--Header--}}
                        <tr>
                            <th>@lang('labels.general.name')</th>
                            <th>@lang('labels.general.receiver_type')</th>
                            <th>@lang('labels.general.relationship')</th>
                            <th>@lang('labels.backend.table.survivor_pension_percent')</th>
                            <th>@lang('labels.backend.claim.monthly_pension')</th>
                        </tr>
                        {{--dependents--}}

                        <?php
                        $dependents = new \App\Repositories\Backend\Operation\Compliance\Member\DependentRepository();
                        ?>

                        @foreach($dependents->dependentsWithPivot($incident->employee_id)->where('survivor_pension_flag', 1)->get() as $dependent)

                            <tr>
                                <td>{!! $dependent->fullname !!}</td>
                                <td>@lang('labels.general.dependent')</td>
                                <td>{!! $dependent->type_name !!}</td>
                                <th>{!! $dependent->survivor_pension_percent !!}</th>
                                <th>{!! number_format( ( ($dependent->survivor_pension_percent * 0.01) * (isset($compensation_summary['death']['monthly_pension_payable']) ? $compensation_summary['death']['monthly_pension_payable'] : 0)) , 2 , '.' , ',' ) !!}</th>
                            </tr>
                        @endforeach

                        {{--FOR PTD--}}
                        {{--pensioners--}}
                        @if($incident->employee->pensioners->last())
                            <tr>
                                <td>{!! $incident->employee->name !!}</td>
                                <td>@lang('labels.general.pensioner')</td>
                                <td></td>
                                <th>{!! 100 !!}</th>
                                <th>{!! number_format(  $incident->employee->pensioners->last()->monthly_pension_amount , 2 , '.' , ',' ) !!}</th>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                @endif

                {{--END monthly pension distribution----------------------}}

                {{--Dependents gratuity Distribution--}
                {{--For DEATH INCIDENT--}}
                @if (in_array($incident->incident_type_id, [3,4,5]))
                    <legend class="grey_modal">@lang('labels.backend.claim.dependents_gratuity_distribution')</legend>
                    </br>
                    <table class="table table-striped table-bordered" style="width:100%">
                        <tbody>
                        {{--Header--}}
                        <tr>
                            <th>@lang('labels.general.name')</th>
                            <th>@lang('labels.general.receiver_type')</th>
                            <th>@lang('labels.general.relationship')</th>
                            <th>@lang('labels.backend.table.survivor_gratuity_percent')</th>
                            <th>@lang('labels.backend.table.survivor_gratuity_amount')</th>
                        </tr>
                        {{--dependents--}}
                        @foreach($dependents->dependentsWithPivot($incident->employee_id)->where('survivor_gratuity_flag', 1)->get() as $dependent)
                            <tr>
                                <td>{!! $dependent->fullname !!}</td>
                                <td>@lang('labels.general.dependent')</td>
                                <td>{!! $dependent->type_name !!}</td>
                                <th>{!! $dependent->survivor_gratuity_percent !!}</th>
                                <th>{!! number_format( ($dependent->survivor_gratuity_amount > 0) ? $dependent->survivor_gratuity_amount : ( ($dependent->survivor_gratuity_percent * 0.01) * $compensation_summary['death']['lump_sum_payable'])  , 2 , '.' , ',' ) !!}</th>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                @endif

                {{--END Dependents gratuity distribution----------------------}}

                {{--Dependents Funeral grant Distribution--}
    {{--For DEATH INCIDENT--}}
                @if (in_array($incident->incident_type_id, [3,4,5]))
                    <legend class="grey_modal">@lang('labels.backend.claim.funeral_grant_distribution')</legend>
                    </br>
                    <table class="table table-striped table-bordered" style="width:100%">
                        <tbody>
                        {{--Header--}}
                        <tr>
                            <th>@lang('labels.general.name')</th>
                            <th>@lang('labels.general.receiver_type')</th>
                            <th>@lang('labels.general.relationship')</th>
                            <th>@lang('labels.backend.table.funeral_grant_percent')</th>
                            <th>@lang('labels.backend.table.funeral_grant')</th>
                        </tr>
                        {{--dependents--}}

                        @foreach($dependents->dependentsWithPivot($incident->employee_id)->where('funeral_grant','>', 0)->get() as $dependent)
                            <tr>
                                <td>{!! $dependent->fullname !!}</td>
                                <td>@lang('labels.general.dependent')</td>
                                <td>{!! $dependent->type_name !!}</td>
                                <th>{!! $dependent->funeral_grant_percent !!}</th>
                                <th>{!! number_format( ( ($dependent->funeral_grant )) , 2 , '.' , ',' ) !!}</th>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                @endif

                {{--END funeral grants distribution----------------------}}

                {{--PAYMENT HISTORY -================--}}

                @if ($incident->claimCompensations()->count())
                    <legend class="grey_modal">@lang('labels.general.payment_summary_overview')</legend>
                    </br>
                    <table class="table table-striped table-bordered" style="width:100%">
                        <tbody>
                        {{--Header--}}
                        <tr>
                            <th>@lang('labels.general.name')</th>
                            <th>@lang('labels.backend.claim.benefit_type')</th>
                            <th>@lang('labels.backend.claim.compensation_type')</th>
                            <th>@lang('labels.general.amount')</th>
                        </tr>
                        {{--claim compensations--}}
                        @foreach($incident->claimCompensations as $claim_compensation)
                            <tr>
                                <td>{!! $claim_compensation->compensated_entity_name !!}</td>
                                <td>{!! $claim_compensation->benefitType->name !!}</td>
                                <th>{!! $claim_compensation->compensationPaymentType->name !!}</th>
                                <th>{!! number_format( $claim_compensation->amount , 2 , '.' , ',' ) !!}</th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
                {{--end Payment History--------------}}


            @endif

        </div>
    </div>
</div>