<div class="modal hide fade" id="followupmodalWorkflow" role="dialog" aria-labelledby="followupmodalWorkflow" aria-hidden="true">
	<div class="modal-dialog white_modal" role="document">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">
				<i class="fa fa-random"></i>&nbsp;&nbsp;Follow Up Assessment History</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-content" id="modal-content" style="max-height: calc(80vh - 200px);overflow-y: auto;">
				<ul class="list-group" id="AddFields">


				</ul>
			</div>
		</div>
	</div>