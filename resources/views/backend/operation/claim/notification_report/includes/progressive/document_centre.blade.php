<legend class="grey_modal" >@lang('labels.backend.claim.document_helper')</legend>
<br/>
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                {{--Document library tree--}}
                <legend>Documents Library</legend>
               {{-- <br/>--}}
                <div>
                    <b>C:</b><code>Document Create Date (MAC)</code>
                </div>
                <div>
                    <b>R:</b><code>Document Receive Date (WCF)</code>
                </div>
                <div id="folder-tree" class="nopadding"></div>
                <br>
                <div class="pull-left">
                    {{--Button - Reload Documents from e-office--}}
                    {!! link_to_route('backend.claim.notification_report.reload_documents','Reload from e-Office', [$incident->id],[ 'class' => 'btn btn-success site-btn ', ]) !!}
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                {{--Pending Document List--}}
                @if(count($pending_documents))
                    {{--<legend>Pending Documents</legend>
                    <br/>--}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="grey_info">
                                <table style="width:100%">
                                    <tr>
                                        <td  align="center"><h5><b><span class="light_green_color">@lang('labels.backend.claim.pending_documents')</span></b></h5></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="light_grey_bg">&nbsp;</div>
                            <div class="light_grey_bg">
                                <table style="width:100%">
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($pending_documents as $pending_document)
                                        <tr>
                                            <td style="padding:5px; width:40px">{!! $i !!}</td>
                                            <td>{!! $pending_document->name !!}</td>
                                        </tr>
                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-8">
        {{--Document Preview--}}
        <legend>Document Preview</legend>
        <br/>
        <div id="notification_document_frame">
            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
        </div>


    </div>
</div>


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}

<script>
    var $refNode = 0;
    $("#document_centre_tab").one("click", function() {
        $(function() {
            var $folder = $('#folder-tree');

            /************************** start: Document Library JS ***************************/
            $folder.jstree({
                'core': {
                    'data': {
                        'url': "{!! route('backend.claim.notification_report.documents_library', $incident->id) !!}",
                        'data': function (node) {
                            return {'id': node.id};
                        }
                    },
                    'check_callback': function (o, n, p, i, m) {
                        if (m && m.dnd && m.pos !== 'i') {
                            return false;
                        }
                        if (o === "move_node" || o === "copy_node") {
                            if (this.get_node(n).parent === this.get_node(p).id) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'force_text': false,
                    'themes': {
                        'responsive': true,
                        'variant': 'small',
                        'stripes': true
                    }
                },
                'sort': function (a, b) {
                    return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
                },
                'types': {
                    'default': {'icon': 'folder'},
                    'file': {'valid_children': [], 'icon': 'file'}
                },
                'unique': {
                    'duplicate': function (name, counter) {
                        return name + ' ' + counter;
                    }
                },
                'plugins': ['state', 'dnd', 'sort', 'types', 'unique']
                /*'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']*/
            });
            $folder.bind("click.jstree", function ($event, $data) {
                var $node = $($event.target).closest("li").attr("id");
                var $type = $folder.jstree().get_selected(true)[0].type;
                if ($type === 'default') {
                    /*should only work on folders*/
                    /*Document Folder Has Been Clicked*/
                    $node = $node.split('/');
                    $node = $node[$node.length - 1];
                    if ($node.length > 0) {
                        //alert(1);
                    } else {
                        //alert(2);
                    }
                } else {
                    /*preview the document here*/
                    /*console.log($node);*/
                    let $notification_document_frame = $("#notification_document_frame");
                    get_current_document($node).done(function ($data) {
                        /*console.log($data.url);*/
                        $notification_document_frame.find("iframe").remove();
                        let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\' allowfullscreen></iframe>');
                        $notification_document_frame.append($iframe);
                    });
                }
            });
            /************************** end: Document Library JS ***************************/
        });
    });
    function get_current_document($node) {
        $refNode = $node;
        /*return $.post( base_url + "/claim/notification_report/current_base64_document/{!! $incident->id !!}/" + $node, {}, function( data ) {
        }, "json");*/
        return $.ajax({
            url: base_url + "/claim/notification_report/current_base64_document/{!! $incident->id !!}/" + $node,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }
    function get_other_document($sign) {
        /*use $refNode here to reference document*/
        /* return the new id to be assigned in $refNode, get base64 and id */
        return $.post( base_url + "/claim/notification_report/other_base64_document/{!! $incident->id !!}/" + $refNode + "/" + $sign, {}, function( data ) {
        }, "json");
    }

</script>
@endpush