<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h6><b><span class="light_dark_color">Incident Summary</span></b></h6></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{--<div class="light_grey_bg">&nbsp;</div>--}}
        <div class="light_grey_bg" style="padding: 5px;">
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.incident_type')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $incident->incidentType->name }}</span></div>
            <div class="underline" style="font-weight: lighter;">Acknowledgement Letter Issued&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->acknowledgement_letter_status !!}</span></div>
            {{--accident--}}
            @if (in_array($incident->incident_type_id, [1,4]))
                @php
                    $accident = $incident->accident;
                @endphp
                {{--accident date--}}
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.accident_date')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $accident->accident_date_formatted !!}</span></div>
                {{--Reporting Date--}}
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.reporting_date')&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.reporting_date_description')  "></i>&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $accident->reporting_date_formatted !!}</span></div>
                {{--Receipt Date--}}
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.receipt_date')&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.receipt_date_description')"></i>&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $accident->receipt_date_formatted !!}</span></div>
                {{--Accident Time--}}
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.accident_time')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $accident->accident_time !!}</span></div>
                {{--Activity Performed--}}
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.activity_performed')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! (!empty($accident->activity_performed)) ? $accident->activity_performed : '-' !!}</span></div>
                {{--Accident Place--}}
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.accident_place')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! (!empty($accident->accident_place)) ? $accident->accident_place : '-' !!}</span></div>
                {{--Description--}}
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.table.description')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! (!empty($accident->description)) ? $accident->description : '-' !!}</span></div>
            @endif

            {{--disease--}}
            @if (in_array($incident->incident_type_id, [2,5]))
                @php
                    $disease = $incident->disease;
                @endphp
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.diagnosis_date')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $disease->diagnosis_date_formatted !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.reporting_date')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $disease->reporting_date_formatted !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.receipt_date')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $disease->receipt_date_formatted !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.table.disease_name')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $disease->name !!}</span></div>
            @endif

            {{--death--}}
            @if (in_array($incident->incident_type_id, [3,4,5]))
                @php
                    $death = $incident->death;
                @endphp
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.death_date')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $death->death_date_formatted !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.reporting_date')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $death->reporting_date_formatted !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.receipt_date')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $death->receipt_date_formatted !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.death_place')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $death->death_place !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.death_cause')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $death->deathCause->name !!}</span></div>
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.certificate_number')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $death->certificate_number Or "-" !!}</span></div>
            @endif

            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.need_investigation')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->need_investigation_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.is_verified')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->verification_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.is_investigated')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->investigation_validity_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.incident_exposure')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! (($incident->incidentExposure()->count()) ? $incident->incidentExposure->name : '-') !!}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.body_part_injury')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ implode(", ", ($incident->injuries()->count()) ? $incident->injuries()->select([DB::raw("body_part_injuries.name")])->pluck("name")->all() : []) }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.nature_of_incident')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! (($incident->natureOfIncident()->count()) ? $incident->natureOfIncident->name : '-') !!}</span></div>

            {{--Witness => ONLY FOR Accident Incident--}}
            @if (in_array($incident->incident_type_id, [1,4]))
                <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.witness')&nbsp;:&nbsp;</div>
                @if ($accident->witnesses()->count())
                    @foreach ($accident->witnesses as $witness)
                        <div style="font-weight: bold;">{!! $witness->fullname !!}&nbsp;<a href="{!! route('backend.claim.notification_report.edit_accident_witness', $witness->id) !!}"><i class="icon fa fa-edit" aria-hidden="true"></i></a></div>
                    @endforeach
                @else
                    <div class="label label-info">No accident witness</div>
                @endif
            @endif

            <div class="underline" style="font-weight: lighter;">@lang('labels.general.prepared_by')&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->user->username !!}</span></div>
        </div>
    </div>
</div>