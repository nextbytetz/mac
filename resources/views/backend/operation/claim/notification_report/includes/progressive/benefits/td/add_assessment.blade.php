<div class="row">
    <div class="col-md-12 mt-1">
        <nav class="navbar navbar-light bg-light" style="background-color: #eceeef; border-radius: 0px;">
            <div class="row">
                <legend>Add Document Reference</legend>
                <br/>
                {!! Form::open(['route' => ['backend.claim.notification_report.add_td_document_used', $eligible->id], 'name' => 'add_document_used_td']) !!}
                <div class="col-sm-2">
                    <div class="fileld-layout">
                        <label class="required">Document Used For Assesment</label>
                        <div class="form-group">
                            {{--{!! Form::select('document_id', $document_available, NULL, ['class' => 'form-control','id' => 'type_of_document_select','style'=>'width:100%; background-color: #fff !important;', 'placeholder' => '']) !!}--}}
                            <select name="document_id" id="type_of_document_select" class="form-control" style='width:100%; background-color: #fff !important;'>

                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="fileld-layout">
                        <label class="required">Select Document</label>
                        <div class="form-group">
                            <select id="document_used" name="document_used" class="form-control" style='width:100%; background-color: #fff !important;'>
                                <option value=''>Select document type first</option>
                            </select>
                            <span class="help-block" style="width:100% !important;">
                                <button class="btn btn-sm btn-secondary document_preview" data-id="0"><i class="fa fa-eye-slash preview_icon"></i> &nbsp;Preview Document</button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="fileld-layout">
                        <label>Document Date</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('document_date', NULL, ['placeholder' => '', 'class' => 'form-control datepicker','id' => 'document_date','autocomplete' => 'off']) !!}
                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="fileld-layout">
                        <label>HCP</label>
                        <div class="form-group">
                            {!! Form::select('health_provider_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control health-provider-select']) !!}
                            <span class="help-block" style="width:100% !important;">
                                <a class="btn btn-secondary btn-sm site-btn" data-toggle="modal" data-target="#create_health_service_provider"><i class="fa fa-plus-square-o"></i> &nbsp;Add HCP</a>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <label> &nbsp;</label><br>
                    <button class="btn btn-sm btn-success document_used_preview btn-submit" data-id="0" type="submit"><i class="fa fa-plus"></i> &nbsp;Use Document</button>
                </div>

                {!! Form::close() !!}
            </div>
            <div class="row">
                <legend>Add Assessment List&nbsp;<span class="small"><span>Hint;&nbsp;(Incident Date)</span>&nbsp;<span class="underline">{{ $incident->incident_date }}</span>&nbsp;(Monthly Earning)&nbsp;<span class="underline">{{ $incident->monthly_earning_formatted }}</span></span></legend>
                <br/>
                {!! Form::open(['route' => ['backend.claim.notification_report.add_td_assessment', $eligible->id] , 'name' => 'add_claim_assessment']) !!}
                {!! Form::hidden('incident_date', $incident->incident_date) !!}
                {!! Form::hidden('end_month_date', $end_month_date) !!}
                {!! Form::hidden('today_date', getTodayDate()) !!}

                <div class="col-sm-2">
                    <div class="fileld-layout">
                        <label class="required">Disability State</label>
                        <div class="form-group">
                            {!! Form::select('disability_state_checklist_id', $disability_state_checklists, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control']) !!}
                            <span class="help-block"></span>
                        </div>

                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="fileld-layout">
                        <label class="required">From Date</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('from_date', NULL, ['placeholder' => '', 'class' => 'form-control datepicker','autocomplete' => 'off']) !!}
                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="fileld-layout">
                        {{--<label class="required">To Date</label>--}}
                        <label class="required">Number of Days</label>
                        <div class="form-group">
                            <div class="input-group">
                                {{--{!! Form::text('to_date', NULL, ['placeholder' => '', 'class' => 'form-control datepicker','autocomplete' => 'off']) !!}--}}
                                {!! Form::text('days', NULL, ['placeholder' => '', 'class' => 'form-control','autocomplete' => 'off']) !!}
                                <span class="input-group-addon"><i class="icon fa fa-calendar-o"></i></span>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="fileld-layout">
                        <label>Remarks</label>
                        <div class="form-group">
{{--                            <div class="input-group">--}}
                            {!! Form::textarea( 'remarks', NULL, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none']) !!}
{{--                            </div>--}}
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label> &nbsp;</label><br>
                    <button class="btn btn-sm btn-success btn-submit" type="submit"><i class="fa fa-plus"></i> &nbsp;Add Assesment</button>
                </div>

                {!! Form::close() !!}
            </div>

        </nav>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="documents_used" id="documents_used">

        </div>
        <div class="assessment_list" id="assessment_list">

            {{--{!! Form::open(['route' => ['backend.claim.notification_report.update_td_assessment', $incident->id, $eligible->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put' , 'name' => 'claim_assessment_checklist']) !!}

            {!! Form::close() !!}--}}

        </div>
        {{--Claim - compensation expense overview--}}
        <legend class="grey_modal" id="claim_assessment_expense_overview" >@lang('labels.backend.claim.claim_assessment_expense_overview')</legend>
        @include('backend/operation/claim/notification_report/includes/progressive/benefits/td/claim_assessment_expense_overview',['notification_report' => $incident])
    </div>
    <div class="col-md-6">
        <legend>Document Preview</legend>
        <br/>
        <div id="document_frame">

        </div>
    </div>
</div>