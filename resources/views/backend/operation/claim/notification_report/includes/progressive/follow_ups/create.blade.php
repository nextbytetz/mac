@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_new_follow_up'), 'header_title' => trans('labels.backend.member.add_new_follow_up')])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
{{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.claim.follow_ups.store_follow_up'], 'name' => 'create_follow_up', 'id' => 'create_follow_up', 'class' => '', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('notification_report_id',$notification->id, ['class' =>'']) !!}
    {{-- {!! Form::hidden('staff_employer_id',isset( $staff_employer) ?  $staff_employer->id : null, ['class' =>'']) !!}
    {!! Form::hidden('start_date', isset( $staff_employer) ? $staff_employer->start_date : null, ['class' =>'']) !!}
    {!! Form::hidden('end_date', isset( $staff_employer) ? $staff_employer->end_date : null, ['class' =>'']) !!} --}}
    {!! Form::hidden('action_type', 1, ['class' =>'']) !!}
    {{--HEADER--}}
    {{-- @include("backend.operation.compliance.member.employee.includes.header_info",['employee'=> $employee]) --}}
    @include("backend/operation/claim/notification_report/includes/progressive/follow_ups/includes/header_info", ['employee'=> $employee,  'notification' => $notification])
    <br/>

    {{--<div>--}}

        <br/>
    {{--</div>--}}
    @include("backend/operation/claim/notification_report/includes/progressive/follow_ups/includes/create_form")


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">
                        {!! link_to('claim/notification_report/profile/' . $notification->id . '#follow_ups',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
{{--</section>--}}

@stop



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{--<script  type="text/javascript">--}}



    <script>
        $(function () {
            // $('.district_select').select2({});
            // $('.wards-select').select2({});
            $(".search-select").select2({});
            $('#create_follow_up').submit(function(e){
                e.preventDefault()
                $('.save_button').attr('disabled', true)
                this.submit()
            })

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                minDate: today_date,
            });
            /*-----------End Date Process------------*/

            follow_up_type_option('follow_type_id', 'contact_div','contact_id','email_div');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id','email_div');
            });

            feedback_option();
            $("#feedback_cv_id").on("change", function (e) {
                feedback_option();
            });

            reminder_email_option();
            $("#reminder_date").on("change", function (e) {
                reminder_email_option();
            });

        });

        // Follow up type option
        function follow_up_type_option(follow_type_id, contact_div, contact_id, email_div) {

            var choice = $("#" + follow_type_id).val();
        //            if is visit
        if (choice == 'FUVSIT') {
            $("#" + contact_div).show();
            $("#" + email_div).show();
            $("#location_div").show();
            $("#location_description_div").show();
            $("#" + contact_id).attr('placeholder','Phone #');
            $("#email_id").attr('placeholder','Email@yahoo.com');
                // $('#doc_form_div').show();


            }else if ( choice == 'FULETT') {
                $("#" + contact_div).hide();
                $("#" + email_div).hide();
                $('#doc_form_div').show();

                $("#location_div").hide();
                $("#location_description_div").hide();
            } else {
            //            phone call
            if (choice == 'FUPHONC') {
                $("#" + contact_div).show();
                $("#" + contact_id).attr('placeholder','Phone #');
                $("#" + email_div).hide();
                $("#location_div").hide();
                $("#location_description_div").hide();
            }else if(choice == 'FUEMAIL'){
                // $("#" + contact_id).attr('placeholder','Email@yahoo.com');
                $("#" + contact_div).hide();
                $("#" + email_div).show();
                $("#email_id").attr('placeholder','Email@yahoo.com');
                $("#location_div").hide();
                $("#location_description_div").hide();
            }
            // $("#" + contact_div).show();
            // $('#doc_form_div').hide();
        }
    }



        // feedback option
        function feedback_option() {

            var choice = $("#feedback_cv_id" ).val();
            //            if is others
            if (choice == 'SEFOTH') {
                $("#remark_div").addClass("required");

            } else {
                $("#remark_div").removeClass("required");
            }
        }


        function reminder_email_option()
        {
            // var choice = $('#reminder_date').val();
            // if(choice.length > 0){
            //     $('#reminder_email_div').show();
            //     $('#reminder_email').prop('disabled', false);
            // }else{
            //     $('#reminder_email_div').hide();
            //     $('#reminder_email').prop('disabled', true);
            // }
        }

        getDistrictWard();
        $('.district_select').change(function(e){
            getDistrictWard();

        })

        function getDistrictWard(){

         // var dstid = $(th).val();
         var dstid = $( "#district_id option:selected" ).val();
         if (dstid.length > 0) {
            var url = "{{url('wards')}}/" + dstid;
            $.ajax({
               type: 'GET',
               url: url,
               success:function(response){
                var getresponse = '';
                var getresponse = '<option selected disabled hidden>-- Select Ward --</option>';
                for(var i in response){
                    getresponse +=
                    '<option value="'+response[i].postcode+'" style="width:150px;">'+response[i].ward_name+'</option>';
                }

                $('.wards-select').empty();
                $('.wards-select').append(getresponse); 
                $('.wards').show();
            }
        }); 
        }else{
          $('.wards').hide();  
          $('.workplace').hide();
      }
  }


</script>;


@endpush