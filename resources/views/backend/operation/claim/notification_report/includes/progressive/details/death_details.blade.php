@include("backend/operation/claim/notification_report/includes/progressive/details/death", ["update_source" => 2])

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});
            let $body = $('body');
            let $death_cause_id = $("#death_cause_id");
            let $incident_date_title = $("#incident_date_title");
            let $accident_cause = $("#accident_cause");
            let $disease_cause = $("#disease_cause");
            $death_cause_id.on('change', function (e) {
                let $choice = $death_cause_id.val();
                switch ($choice) {
                    case '1':
                        /*Occupational accident*/
                        $incident_date_title.html("Date of Accident");
                        $accident_cause.show();
                        $disease_cause.hide();
                        break;
                    case '2':
                        /*Occupational disease*/
                        $incident_date_title.html("Date of Diagnosis");
                        $accident_cause.hide();
                        $disease_cause.show();
                        break;
                    default:
                        $incident_date_title.html("Incident Date");
                        $accident_cause.hide();
                        $disease_cause.hide();
                        break;
                }
            });
            $death_cause_id.trigger("change");
        });
    </script>
@endpush