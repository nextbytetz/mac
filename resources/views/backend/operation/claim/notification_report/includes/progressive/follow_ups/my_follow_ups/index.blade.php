@if($incident->allocated == auth()->user()->id)
<div class = "row">
	<div class="col-md-12" >
		<div class="col-md-12" >


			<div class="pull-left">


				<div class="btn-group">
					{{-- <a style="color: blue;" href="{!! route('backend.compliance.employer.staff_relation.export_employer_follow_ups',[$staff_employer->id ]) !!}"  class="" >Export to excel</a> --}}
					export

				</div>
			</div>

			<div class="pull-right">


				<div class="btn-group">

					{{--@if(($check_pending_level1 == 1 || $check_workflow == 0) && $employer_closure->is_legacy == 0)--}}
					{{-- <a href="{!! route('backend.claim.follow_ups.create_follow_up', $incident->id) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-plus"></i>&nbsp;Add New</a> --}}
					{{--@endif--}}
				</div>
			</div>



		</div>
	</div>


</div>
@endif
<br/>
{{--Table--}}
<div class = "row">
	<div class="col-md-12" >
		<table class="display" cellspacing="0" width="100%" id ="relation-my-follow-ups-table">
			<thead>
				<tr >
					<th>Staff</th>
					<th>Employee</th>
					<th>Case no</th>
					<th>Status</th>
					<th>District</th>
					<th>Start date</th>
					<th>End date</th>
					<th>Missing Docs</th>
					<th>Follow Ups</th>
					<th>Attendance Status</th>
					<th>Review Status</th>
				</tr>
			</thead>
		</table>

	</div>
</div>




@push('after-script-end')

<script  type="text/javascript">
	$(function() {
		var url = "{!! url("/") !!}";
		// $("#followups_header").one("click", function(){
			$('#relation-my-follow-ups-table').DataTable({
				processing: true,
				serverSide: true,
				stateSave: true,
				stateSaveCallback: function (settings, data) {
					localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
				},
				stateLoadCallback: function (settings) {
					return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
				},
				ajax:{
					url : '{!! route('backend.claim.follow_ups.my_follow_ups', $incident->allocated) !!}',
					type : 'get'
				},
				columns: [
				{ data: 'user_id', name: 'user_id',  orderable : true, searchable : false},
				{ data: 'employee', name: 'employee',  orderable : true, searchable : false},
				{ data: 'case_no', name: 'case_no',  orderable : true, searchable : true},
				{ data: 'status', name: 'status',  orderable : false, searchable : false},
				{ data: 'district', name: 'district',  orderable : true, searchable : true},
				{ data: 'start_date', name: 'start_date',  orderable : true, searchable : false},
				{ data: 'end_date', name: 'end_date' ,orderable : true, searchable : false},
				{ data: 'missing_docs', name: 'missing_docs',  orderable : true, searchable : false},
				{ data: 'followup_counts', name: 'followup_counts',  orderable : true, searchable : false},
				{ data: 'attendance_status', name: 'attendance_status',  orderable : true, searchable : false},
				{ data: 'review_status', name: 'review_status',  orderable : true, searchable : false},
				
				],
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).click(function() {
						// console.log(aData)
						// return false;
						// claim/notification_report/profile/5377#filling_checklist
						document.location.href = url +  "/claim/notification_report/profile/" + aData['notification_report_id'] + "#follow_ups" ;
					}).hover(function() {
						$(this).css('cursor','pointer');
					}, function() {
						$(this).css('cursor','auto');
					});
				}

			});
		});

	// });


</script>;

@endpush
