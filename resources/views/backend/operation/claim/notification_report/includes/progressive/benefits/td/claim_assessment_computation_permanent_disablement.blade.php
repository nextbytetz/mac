{{--Computation of permananent disablement--}}

<div class="row" id="computation-of-permanent-disablement-div">

    <div class="col-md-12">
        <div>&nbsp;</div>
        <table class="table table-striped table-bordered" style="width:100%">
            <tbody>
            {{--Header--}}
            <tr>
                <th width="300px">@lang('labels.backend.claim.compensation_factor')</th>
                <th>@lang('labels.general.value')</th>

            </tr>

            {{--percent of permanent impairment--}}
            <tr>
                <td>@lang('labels.backend.claim.percent_of_permanent_disability') </td>
                {{--%--}}
                <td>       {!! Form::input( 'text','pd', ($incident->claim()->count()) ?  number_0_format($incident->claim->pd) : 0, ['class' => 'form-control number percent_entry', 'id'=> 'pd' ,'disabled'=> !$user_has_access ]) !!}</td>

            </tr>

            {{--percent of earning--}}
            <tr>
                <td>@lang('labels.backend.claim.percent_of_earning') </td>
                {{--%--}}
                <td> <span id = "percent_of_earnings" >
                        {!! Form::label( 'percent_of_earnings', number_0_format(sysdefs()->data()->percentage_of_earning), [ ]) !!}
                                        </span></td>
                {!! Form::hidden( 'percent_of_earning', sysdefs()->data()->percentage_of_earning,['id'=> 'percent_of_earning']) !!}

            </tr>
            {{--Gross month earning earning--}}
            <tr>
                <td>@lang('labels.backend.claim.gross_monthly_earning') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.gross_monthly_earning_description')  "></i> </td>
                {{--%--}}
                <td>{!! Form::label( 'monthly_earning',  number_format( $monthly_earning_approved, 2 , '.' , ',' ), [ 'id'=> 'monthly_earning']) !!} </td>
                {!! Form::hidden( 'gross_monthly_earning', $monthly_earning_approved,['id'=> 'gross_monthly_earning']) !!}
            </tr>

            {{--constant factor months--}}
            <tr id="constant_factor_row">
                <td>@lang('labels.backend.claim.constant_factor_months') </td>
                {{--%--}}
                <td>{!! Form::label( 'constant_factor', sysdefs()->data()->constant_factor_compensation, [ 'id'=> 'constant_factor']) !!}</td>
                {!! Form::hidden( 'constant_factor_month', sysdefs()->data()->constant_factor_compensation,['id'=> 'constant_factor_month']) !!}

            </tr>

            {{--PPD LUMP SUM--}}
            <tr id = "ppd_lumpsum_row">
                <th>
                    @lang('labels.backend.claim.ppd_lumpsum')
                </th>
                {{--%--}}
                <th>     <span id = "ppd_lumpsum" class="underline" style="font-weight: bold;">
                                            0
                                        </span></th>

            </tr>

            {{--MONTHly pension--}}
            <tr id = "monthly_pension_row">
                <th>
                    @lang('labels.backend.claim.monthly_pension')
                </th>
                {{--%--}}
                <th>     <span id = "monthly_pension" class="underline" style="font-weight: bold;">
                                            0
                                        </span></th>

            </tr>


            {{--Date of MMI--}}
            <tr id = "date_of_mmi_row">
                <th>
                    Date of MMI
                    <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Date of Maximum Medical Improvement "></i>
                </th>
                {{--%--}}
                <th>
                    <div  class="form-inline" style="font-weight: bold;">

                        <span>      {!!  Form::selectRange('mmidate',1,31,(isset($date_of_mmi) ? \Carbon\Carbon::parse($date_of_mmi)->format('j') : null  ), ['class' => 'form-control search-select','style'=>'width:55px', 'placeholder' =>
                         'Day', 'id'=> 'mmidate']) !!}

                        </span>

                        <span>      {!!  Form::selectMonth('mmimonth',(isset($date_of_mmi) ? \Carbon\Carbon::parse($date_of_mmi)->format('n') : null  ), ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month' ,'id'=> 'mmimonth']) !!}
                        </span>

                        <span>      {!!  Form::selectRange('mmiyear',Carbon\Carbon::now()->format('Y'),2017,(isset($date_of_mmi) ? \Carbon\Carbon::parse($date_of_mmi)->format('Y') : null  ), ['class' => 'form-control search-select','style'=>'width:63px',
                        'placeholder' =>
                         'Year', 'id'=> 'mmiyear']) !!}
                        </span>
                        {!! Form::hidden('date_of_mmi', null) !!}


                    </div>
                    {!! $errors->first('date_of_mmi', '<span class="help-block label label-danger">:message</span>') !!}
                </th>

            </tr>


            </tbody>
        </table>

    </div>
</div>