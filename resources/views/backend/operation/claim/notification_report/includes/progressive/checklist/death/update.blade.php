@php
    $death = $incident->death;
@endphp

    <div class="medical_treatments">
        <div class="row">
            <div class="col-md-4">
                {{--health service provider select--}}
                <div class="fileld-layout">
                    <label class="required">Name of Hospital</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('health_provider_id', $death->healthProvider()->get()->pluck("fullname_info", "id")->all(), $death->health_provider_id, ['style' => 'width:80%', 'placeholder' => '','class' => 'form-control search-select health-provider-select']) !!}
                            <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_health_service_provider']) !!}</span>
                        </div>
                        <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;">Name of Hospital where death was confirmed</small>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                {{--treating medical practiotioners--}}
                <div class="fileld-layout">
                    <label>Medical Practitioner</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('medical_practitioner_id', $death->medicalPractitioner()->get()->pluck("fullname_with_external_id", "id")->all(), $death->medical_practitioner_id,  ['style' => 'width:80%','class' => 'form-control practitioner-select']) !!}
                            <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_medical_practitioner']) !!}</span>
                        </div>
                        <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;">Name of Medical practitioner who confirmed death</small>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                {{--confirmation date--}}
                @if(0)
                    <div class="fileld-layout">
                        <label class="required">Death Confirmation Date</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('confirm_date', $death->confirm_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;">Date when death was confirmed</small>
                        </span>
                        </div>
                    </div>
                @else
                    {!! Form::hidden("confirm_date", $death->incident_date) !!}
                @endif

            </div>
        </div>
    </div>

    <div>&nbsp;</div>
