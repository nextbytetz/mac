@php
    $hideoption = $hideoption ?? 0;
@endphp
<div class="row">
    <div class="col-md-11">
        {{--put a benefit summary here--}}
        <span class="pull-left">
                {{--Put label for status here--}}
            {!! $assessment->processed_label !!}
            {!! $assessment->isinitiated_label !!}
            {!! $assessment->letter_status_label !!}
        </span>
        @if (!$hideoption)
        <span class="pull-right">

            @if ($assessment->isinitiated)
                <span class="dropdown">
                <a class="btn btn-primary site-btn nav_button dropdown-toggle" id="dropdownMenuButtonLinks"
                   data-toggle="dropdown">Options</a>
                    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonLinks"
                          style="z-index: 99999999">

                        {{--<a class="dropdown-item" href="{!! route('backend.claim.notification_report.ackwlgletter', $incident->id)!!}"><i class="icon fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Rejection Letter</a>--}}
                        @if (!$assessment->processed)
                            {{--Put edit button here--}}
                            {!! Html::decode(link_to_route('backend.claim.notification_report.edit_benefit_payment', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . "Update", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                            <div class="dropdown-divider"></div>
                        @endif
                        {{--Put assessment button here--}}
                        {!! Html::decode(link_to_route('backend.claim.notification_report.assess_mae_refund_member', "<i class='icon fa fa-medkit' aria-hidden='true'></i>&nbsp;" . "Assessment", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                        <div class="dropdown-divider"></div>
                        {{--Process Payment--}}
                        {!! Html::decode(link_to_route('backend.claim.notification_report.process_mae_payment', "<i class='icon fa fa-money' aria-hidden='true'></i>&nbsp;Process Payment" , [$incident->id, $assessment->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to process Payment", 'class' => 'dropdown-item'])) !!}
                        @if ($assessment->ispayprocessed Or $assessment->iszeropaid)
                            <div class="dropdown-divider"></div>
                            {{--Put award letter button here--}}
                            {!! Html::decode(link_to_route('backend.letter.process', "<i class='icon fa fa-newspaper-o' aria-hidden='true'></i>&nbsp;" . "Award Letter", [$assessment->id, "CLBNAWLTTR"],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                        @endif
                    </span>
                </span>
            @endif

            @if (!$assessment->isinitiated)
                {{--initiate workflow approval for payment--}}
                {!! Html::decode(link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate", ['class' => 'btn btn-secondary site-btn initiate_benefit_workflow', 'data-description' => 'Initiate MAE Refund (Employee/Employer) Workflow', 'data-incident' => $incident->id, 'data-benefit' => $assessment->benefit->id, 'data-eligible' => $assessment->id, 'data-route' => route("backend.claim.notification_report.initiate_benefit_workflow")])) !!}

                {{--MAE Edit--}}
                {!! Html::decode(link_to_route('backend.claim.notification_report.edit_benefit_payment', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . "Update", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'btn btn-secondary site-btn'])) !!}

            @endif

        </span>
        @endif
        <br/>
        <br/>

        @php
            $mae_expense = $assessment->medicalExpense;
        @endphp

        {{--start : Put Summary Tables Here--}}
        <div class="underline">Total Amount</div>
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td>Amount</td>
                <th>{{ number_2_format($mae_expense->amount) }}</th>
                <td>Member Type</td>
                <th>{{ $assessment->memberType->name }}</th>
            </tr>
            <tr>
                <td>Assessed Amount</td>
                <th>{{ number_2_format($mae_expense->assessed_amount) }}</th>
                <td>Created</td>
                <th>{{ $assessment->created_at_formatted }}</th>
            </tr>
            <tr>
                <td>User</td>
                <th>{{ $assessment->user->name }}</th>
            </tr>

            @if (!$mae_expense->has_subs)
                @php
                    $notification_health_provider = $assessment->medicalExpense->notificationHealthProviders()->limit(1)->first();
                @endphp
                <tr>
                    <td>Hospital</td>
                    <th>{{ $notification_health_provider->healthProvider->fullname_info }}</th>
                    <td>Medical Practitioner</td>
                    <th>{{ (($notification_health_provider->notificationHealthProviderPractitioner()->count()) ? $notification_health_provider->notificationHealthProviderPractitioner->medicalPractitioner->fullname_with_external_id : "-") }}</th>
                </tr>
                <tr>
                    <td>Attend Date</td>
                    <th>{{ $notification_health_provider->attend_date_formatted }}</th>
                    <td>Dismiss Date</td>
                    <th>{{ $notification_health_provider->dismiss_date_formatted }}</th>
                </tr>
                <tr>
                    <td>Assessed Amount</td>
                    <th>{{ $assessment->medicalExpense->assessed_amount_formatted }}</th>
                </tr>
            @endif

            </tbody>

        </table>

        @if ($mae_expense->has_subs)
            @php
                $mae_subs = $mae_expense->maeSubs;
            @endphp
            <div class="underline">Sub Amounts</div>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Hospital</th>
                    <th>Receipt Number</th>
                    <th>Amount</th>
                    <th>Assessed Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($mae_subs as $mae_sub)
                    @if ($mae_sub->healthProvider()->count())
                        @php
                            $health_provider = $mae_sub->healthProvider->name;
                        @endphp
                    @else
                        @php
                            $health_provider = "-";
                        @endphp
                    @endif
                    <tr>
                        <td>{{ $health_provider }}</td>
                        <td>{{ $mae_sub->rctno }}</td>
                        <td>{{ $mae_sub->amount_formatted }}</td>
                        <td>
                            {{ $mae_sub->assessed_amount_formatted }}
                            &nbsp;&nbsp;
                            @if (!is_null($mae_sub->remarks))
                                <small class="underline" data-toggle="popover" data-trigger="hover" data-placement="top"
                                       data-content="{{ $mae_sub->remarks }}" style="cursor: pointer;">Remarks</small>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

        @if (!$mae_expense->has_subs)
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Health Service Checklist</th>
                    <th>Amount</th>
                    <th>From Date</th>
                    <th>To Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($notification_health_provider->notificationHealthProviderServices as $notificationHealthProviderService)
                    <tr>
                        <td>{{ $notificationHealthProviderService->healthServiceChecklist->name }}</td>
                        <td>{{ number_2_format($notificationHealthProviderService->amount) }}</td>
                        <td>{{ $notificationHealthProviderService->from_date_formatted }}</td>
                        <td>{{ $notificationHealthProviderService->to_date_formatted }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

        {{--end : Put Summary Tables Here--}}
        <legend class="grey_modal">@lang('labels.backend.claim.compensation_summary')</legend>
        <div class="underline">Summary</div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Beneficiary</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $mae_expense->compensated_entity_name }}</td>
                <td>{{ $mae_expense->assessed_amount_formatted }}&nbsp;<i class='icon fa fa-question-circle'
                                                                          aria-hidden='true' data-toggle="tooltip"
                                                                          data-placement="top" title=""
                                                                          data-original-title="Total amount after assessment"></i>
                </td>
            </tr>
            </tbody>
        </table>

    </div>
    <div class="col-md-1">
            <span class="pull-right">
                {{--if it is not initiated, can be deleted--}}
                @if (!$assessment->isinitiated)
                    {!! HTML::decode(link_to_route('backend.claim.notification_report.delete_benefit_payment', "<i class='icon fa fa-trash fa-2x' aria-hidden='true' style='color:darkred;'></i>", [$incident->id, $assessment->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to delete this benefit payment?", 'data-toggle' => 'tooltip', 'data-html' => 'true', 'title' => 'Delete Benefit Payment']))  !!}
                @endif
            </span>
    </div>
</div>