@php
    $hideoption = $hideoption ?? 0;
@endphp
<div class="row">
    <div class="col-md-11">
        {{--put a benefit summary here--}}
        <span class="pull-left">
                {{--Put label for status here--}}
            {!! $assessment->processed_label !!}
            {!! $assessment->isinitiated_label !!}
            {!! $assessment->letter_status_label !!}
            </span>
        @if (!$hideoption)
        <span class="pull-right">

            @if ($assessment->isinitiated)
                <span class="dropdown">
                <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonLinks" data-toggle="dropdown" >Options</a>
                    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonLinks" style="z-index: 99999999">

                        {{--<a class="dropdown-item" href="{!! route('backend.claim.notification_report.ackwlgletter', $incident->id)!!}"><i class="icon fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Rejection Letter</a>--}}
                        @if (!$assessment->processed)
                            {{--Put edit button for assessment here--}}
                            {!! Html::decode(link_to_route('backend.claim.notification_report.edit_benefit_payment', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . "Update", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                            <div class="dropdown-divider"></div>
                        @endif
                        {{--TD Assessment--}}
                        {!! Html::decode(link_to_route('backend.claim.notification_report.td_assessment', "<i class='icon fa fa-filter' aria-hidden='true'></i>&nbsp;" . "Assessment", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                        @if ($assessment->assessed)
                            <div class="dropdown-divider"></div>
                            {{--Undo Approved Assessment--}}
                            {!! Html::decode(link_to_route('backend.claim.notification_report.undo_approved_assessment', "<i class='icon fa fa-undo' aria-hidden='true'></i>&nbsp;Undo Approved Assessment" , [$incident->id, $assessment->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to undo approved assessment", 'class' => 'dropdown-item'])) !!}
                        @endif
                        @if ($assessment->assessed)
                            <div class="dropdown-divider"></div>
                            {{--Process Payment--}}
                            {!! Html::decode(link_to_route('backend.claim.notification_report.process_benefit_payment', "<i class='icon fa fa-money' aria-hidden='true'></i>&nbsp;Process Payment" , [$incident->id, $assessment->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to process Payment", 'class' => 'dropdown-item'])) !!}
                        @endif
                        {{--<div class="dropdown-divider"></div>--}}
                        @if ($assessment->ispayprocessed Or $assessment->iszeropaid)
                            <div class="dropdown-divider"></div>
                            {{--Put award letter button here--}}
                            {!! Html::decode(link_to_route('backend.letter.process', "<i class='icon fa fa-newspaper-o' aria-hidden='true'></i>&nbsp;" . "Award Letter", [$assessment->id, "CLBNAWLTTR"],['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}
                        @endif
                        @if (!$assessment->assessed)
                            <div class="dropdown-divider"></div>
                            {{--Cancel Benefit Payment--}}
                            {{--{!! Html::decode(link_to_route('backend.letter.process', "<i class='icon fa fa-scissors' aria-hidden='true'></i>&nbsp;" . "Cancel Benefit", [$assessment->id, "CLBNAWLTTR"], ['class' => 'dropdown-item', 'style' => 'font-weight:normal;' ])) !!}--}}
                            {!! Html::decode(link_to_route('backend.claim.notification_report.cancel_benefit_payment', "<i class='icon fa fa-scissors' aria-hidden='true'></i>&nbsp;Cancel Benefit" , [$incident->id, $assessment->benefit->id, $assessment->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to cancel benefit?", 'class' => 'dropdown-item'])) !!}
                        @endif
                    </span>
                </span>
            @endif

            @if (!$assessment->isinitiated)
                {{--initiate workflow approval for payment--}}
                {!! Html::decode(link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate", ['class' => 'btn btn-secondary site-btn initiate_benefit_workflow', 'data-description' => 'Initiate TD Payment Workflow', 'data-incident' => $incident->id, 'data-benefit' => $assessment->benefit->id, 'data-eligible' => $assessment->id, 'data-route' => route("backend.claim.notification_report.initiate_benefit_workflow")])) !!}

                {{--TD Edit--}}
                {!! Html::decode(link_to_route('backend.claim.notification_report.edit_benefit_payment', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . "Update", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'btn btn-secondary site-btn'])) !!}
            @endif

            {{--@if (!is_null($assessment->notification_workflow_id))
                --}}{{--Put assessment button here--}}{{--
                {!! Html::decode(link_to_route('backend.claim.notification_report.assess_td_payment', "<i class='icon fa fa-user-md' aria-hidden='true'></i>&nbsp;" . "Assessment", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
            @endif--}}

            </span>
        @endif
        <br/>
        <br/>

        @php
            $td_compensation_summary_entry = (new \App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository())->progressiveCompensationApprove($incident, 0, $assessment->id);
            //dd(count($td_compensation_summary_entry['td']));
        @endphp

        {{--start : Put Summary Tables Here--}}
        <div class="underline">Health Disability States</div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Health Disability</th>
                <th>Days As per WCP-5</th>
                <th>Percent Of ED/LD</th>
                <th>Assessed Days</th>
            </tr>
            </thead>

            <tbody>
            @foreach($assessment->notificationDisabilityStates as $notificationDisabilityState)

                @php
                    //$notification_disability_state_assessment = $notificationDisabilityState->notificationDisabilityStateAssessment;
                    $notification_disability_state_assessments = $notificationDisabilityState->notificationDisabilityStateAssessments;
                @endphp


                    <tr>
                        <td>
                            {{ $notificationDisabilityState->disabilityStateChecklist->name }}<br/>
                            <small>From&nbsp;:&nbsp;{{ $notificationDisabilityState->from_date_formatted Or "-" }}&nbsp;To&nbsp;:&nbsp;{{ $notificationDisabilityState->to_date_formatted Or "-" }}</small>
                        </td>
                        <td>{{ $notificationDisabilityState->days }}</td>
                        <td>{{ $notificationDisabilityState->percent_of_ed_ld }}</td>
                        <td>
                            @foreach($notification_disability_state_assessments as $notification_disability_state_assessment)
                                {{ $notification_disability_state_assessment->days Or "-" }}
                                &nbsp;&nbsp;
                                @if (!is_null($notification_disability_state_assessment->remarks))
                                    <small class="underline" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ $notification_disability_state_assessment->remarks }}" style="cursor: pointer;">Remarks</small>
                                @endif
                                <br/>
                                <small>From&nbsp;:&nbsp;{{ $notification_disability_state_assessment->from_date_formatted Or "-" }}&nbsp;To&nbsp;:&nbsp;{{ $notification_disability_state_assessment->to_date_formatted Or "-" }}</small>
                                <br/>
                            @endforeach
                        </td>
                    </tr>

            @endforeach
            </tbody>
        </table>

        <div class="underline">Health State</div>
        <table class="table table-striped table-bordered">
            <tbody>
            @foreach($assessment->notificationHealthStates as $notificationHealthState)
                <tr>
                    <th>{{ $notificationHealthState->healthStateChecklist->name }}</th>
                </tr>
            @endforeach
            </tbody>
        </table>


        <legend class="grey_modal" >@lang('labels.backend.claim.compensation_summary')</legend>
        @include("backend.operation.claim.notification_report.includes.progressive.benefits.min_max_payable_benefit_amount")
        <div class="underline">TD Payment</div>
        @include("backend/operation/claim/notification_report/includes/progressive/benefits/td/compensation_summary_entry", ['value' => count($td_compensation_summary_entry['td']) ? $td_compensation_summary_entry['td'][0] : [] ])

        {{--end : Put Summary Tables Here--}}

    </div>
    <div class="col-md-1">
            <span class="pull-right">
                {{--if it is not initiated, can be deleted--}}
                @if (!$assessment->isinitiated)
                    {{--{!! HTML::decode(link_to_route('backend.claim.notification_report.delete_benefit_payment', "<i class='icon fa fa-trash fa-2x' aria-hidden='true' style='color:darkred;'></i>", [$incident->id, $assessment->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to delete this benefit payment?", 'data-toggle' => 'tooltip', 'data-html' => 'true', 'title' => 'Delete Benefit Payment']))  !!}--}}
                @endif
            </span>
    </div>
</div>