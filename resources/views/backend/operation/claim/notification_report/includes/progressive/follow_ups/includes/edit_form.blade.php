{{--create Form--}}

{{--main contents--}}
<br/>



{{--follow up type--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.follow_up_type'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::select('follow_up_type_cv_id', $follow_up_types, $claim_followup_update->followUpType->reference, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'follow_type_id']) !!}
                    {!! $errors->first('follow_up_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>

{{--contact--}}
<div class="row" id="contact_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.contact'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.contact_tooltip_description')  "></i></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact', $claim_followup_update->contact, ['class' => 'form-control', 'id'=>'contact_id']) !!}

                    {!! $errors->first('contact', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row" id="email_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.email'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.contact_tooltip_description')  "></i></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','email', $claim_followup_update->email, ['class' => 'form-control', 'id'=>'email_id']) !!}

                    {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>


{{--contact person--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.contact_person'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact_person', $claim_followup_update->contact_person, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_person', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

{{--date_of_follow_up--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.date_of_follow_up'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                {{--<div class="row">--}}
                <div class="form-group">

                    <div class="form-inline">

                        <div class="input-group" style="width:100%;">
                            {!! Form::text('date_of_follow_up',  short_date_format($claim_followup_update->date_of_follow_up), ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        {{--<span class="help-block">--}}
                        {{--<p>Date of closing the business</p>--}}
                        {{--</span>--}}
                    </div>
                </div>
                {!! $errors->first('date_of_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>


{{--remark--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right" id="remark_div"><label>Remark:</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group text_content">
                    {!! Form::textarea('remark', $claim_followup_update->remark, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                    {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row" id="location_div" style="display: none;">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="row">
                    <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right" id="location"><label>Location:</label></div>
                    <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-sm-12">
                            <div class="col-sm-8">
                                 <div class="form-group">
                                {!! Form::select('district', $districts, isset($postcode) ? $postcode->district_id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select district_select', 'id'=> 'district_id']) !!}

                                {!! $errors->first('district', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>

                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                <select class="search-select wards-select" style="width: 100%;display: none;" name="ward" id="ward_id">

                                </select>
                                {!! $errors->first('ward', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row" id="location_description_div" style="display: none;">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right" id="location_description"><label>location description:</label></div>
                <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::textarea('location_description', $claim_followup_update->location_description, ['class' => 'form-control' , 'placeholder' => 'ie. Location, Plot, Building, Post Address..' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                        {!! $errors->first('location_description', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>

{{-- <div class="row" id="reminder_email_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Employer reminder email:</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Email of employer to be used for this reminder. "></i></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','reminder_email', $follow_up->reminder_email, ['class' => 'form-control', 'id'=>'reminder_email']) !!}

                    {!! $errors->first('reminder_email', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>
 --}}


{{--Re-attach file input--}}
{{-- <div class="row" id="doc_form_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Re-attach Document (PDF):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::file('document_file') !!}
                    {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>

    </div>
</div> --}}