@php
    $accident = $incident->accident;
@endphp
<div class="row">
    <div class="col-md-12">
        <legend>General</legend>
        <br/>

        <div class="light_grey_bg" style="padding: 5px;border-radius: 4px;">
            <div class="underline" style="font-weight: lighter;">Cause of Accident&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($accident->cause()->count()) ? $accident->cause->name : "-") }}</span></div>
            <div class="underline" style="font-weight: lighter;">Date of Accident&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $accident->accident_date_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">Date of Hire&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $incident->datehired_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">Date Last Worked&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $incident->last_work_date_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">Paid Salary?&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $incident->paid_month_before_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;border: dotted #e6f5f0 2px !important;background-color: #d3dfd0;">Gross Salary&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $incident->monthly_earning_checklist_formatted !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Salary Being Continued?&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $accident->salary_being_continued_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Salary Being Continued in Full&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $accident->salary_full_continued_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Body Part Injury&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $incident->body_part_injury_list !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Has Lost Body Part?&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $accident->lost_body_part_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Lost Body Parts&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $incident->lost_body_parts_list !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Accident Incident Exposure&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($accident->exposure()->count()) ? $accident->exposure->name : "-") }}</span></div>
            <div class="underline" style="font-weight: lighter;">Returned to Work&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $accident->return_to_work_formatted !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Return to Work Date&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $accident->return_to_work_date_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">Job title on Return to Work&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($accident->jobTitle()->count()) ? $accident->jobTitle->name : "-") }}</span></div>
        </div>
        <br/>

        @if ($incident->treatments()->count())
            <legend>Medical Treatment(s)</legend>
            <br/>

            @foreach ($incident->treatments as $treatment)

                <p style="font-size: medium;"><b>&#x27A5;&nbsp;{{ $loop->iteration }}</b></p>

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Hospital</th>
                            <th>Practitioner</th>
                            {{--<th>ED</th>
                            <th>Hospitalization</th>--}}
                            <th>Attended</th>
                            <th>Dismissed</th>
                        </tr>
                    </thead>
                    <tbody>

                        @php
                            $treatment_ed = $treatment->disabilities()->where("disability_state_checklist_id", 2);
                            $treatment_hospitalization = $treatment->disabilities()->where("disability_state_checklist_id", 1);
                            $treatment_ld = $treatment->disabilities()->where("disability_state_checklist_id", 3);
                        @endphp

                        <tr>
                            <td>{{ $treatment->healthProvider->fullname_info }}</td>
                            <td>{!! $treatment->practitioners_list !!}</td>
                            {{--<td>{{ (($treatment_ed->count()) ? $treatment_ed->first()->pivot->days : "") }}</td>
                            <td>{{ (($treatment_hospitalization->count()) ? $treatment_hospitalization->first()->pivot->days : "") }}</td>--}}
                            <td>{{ $treatment->initial_treatment_date_formatted }}</td>
                            <td>{{ $treatment->last_treatment_date_formatted }}</td>
                        </tr>

                    </tbody>
                </table>

                <div class="light_grey_bg" style="padding: 5px;border-radius: 4px;">
                    <div class="underline" style="font-weight: lighter;">Has Hospitalization?&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $treatment->has_hospitalization_status !!}</span></div>
                    @if ($treatment->has_hospitalization)
                        <div class="underline" style="font-weight: lighter;">Hospitalization Days&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($treatment_hospitalization->count()) ? $treatment_hospitalization->first()->pivot->days : "") }}</span></div>
                    @endif
                    <div class="underline" style="font-weight: lighter;">Has Light Duty?&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $treatment->has_ld_status !!}</span></div>
                    @if ($treatment->has_ld)
                        <div class="underline" style="font-weight: lighter;">Light Duty (LD) Days&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($treatment_ld->count()) ? $treatment_ld->first()->pivot->days : "") }}</span></div>
                    @endif
                    <div class="underline" style="font-weight: lighter;">Has Excuse from Duty?&nbsp;:&nbsp;<span style="font-weight:bold;">{!! $treatment->has_ed_status !!}</span></div>
                    @if ($treatment->has_ed)
                        <div class="underline" style="font-weight: lighter;">Excuse from Duty(ED) Days&nbsp;:&nbsp;<span style="font-weight:bold;">{{ (($treatment_ed->count()) ? $treatment_ed->first()->pivot->days : "") }}</span></div>
                    @endif
                    @if ($treatment->memberType()->count())
                        <div class="underline" style="font-weight: lighter;">Medical Expense Paid By&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $treatment->memberType->name }}</span></div>
                        @if ($treatment->member_type_id == 3)
                            <div class="underline" style="font-weight: lighter;">Insurance&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $treatment->insurance->name }}</span></div>
                        @endif
                    @endif
                    @php
                        $health_state_vals = $treatment->health_states ?: "{}";
                        $health_state_checklists_vals = json_decode($health_state_vals, true);
                    @endphp
                    @if (count($health_state_checklists_vals))
                        <div>Health State Checklist</div>
                        @foreach ($health_state_checklists_vals as $index => $value)
                            {{--<div>{{ $index }}</div>--}}
                            @if(strpos($index, '_') !== false)
                                <div>=> &nbsp; {{ $value }}<span style="font-weight:bold;"></span></div>
                            @endif

                        @endforeach
                    @endif

                </div>
                <legend></legend>

            @endforeach

        @endif
    </div>
</div>