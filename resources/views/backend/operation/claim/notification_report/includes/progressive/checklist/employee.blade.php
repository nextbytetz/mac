@if ($employee->id)
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                    <td>Identity ID</td>
                    <th>{{ $employee->id_no Or "-" }}</th>
                    <td>Employment Status</td>
                    <th>{{ $employee->external_id Or "-" }}</th>
                </tr>
                <tr>
                    <td>Employment Status</td>
                    <th>{{ ((($employee->employeeCategory()->count()) ? $employee->employeeCategory->name : "-")) }}</th>
                    <td>Job Title</td>
                    <th>{{ ((($employee->jobTitle()->count()) ? $employee->jobTitle->name : "-")) }}</th>
                </tr>
                <tr style="border: dotted #e6f5f0 2px !important;background-color: #e9e9e4;">
                    <td>Date of Birth</td>
                    <th>{{ $employee->dob_formatted }}</th>
                    <td>@lang('labels.general.age')</td>
                    <th>{{ $age }}</th>
                </tr>
                <tr>
                    <td>Gender</td>
                    <th>{{ ((($employee->gender()->count()) ? $employee->gender->name : "-")) }}</th>
                    <td>Marital Status</td>
                    <th>{{ ((($employee->maritalStatus()->count()) ? $employee->maritalStatus->name : "-")) }}</th>
                </tr>
                <tr>
                    <td>Next of Kin</td>
                    <th>{{ $employee->next_of_kin Or "-" }}</th>
                    <td>Next of Kin Cellphone</td>
                    <th>{{ $employee->next_of_kin_phone Or "-" }}</th>
                </tr>

                <tr style="border: dotted #e6f5f0 2px !important;background-color: #d3dfd0;">
                    <td>@lang('labels.backend.claim.gross_monthly_earning')&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.gross_monthly_earning_description')  "></i></td>
                    <th><span style="font-weight: bold;">{{ $monthly_earning }}</span></th>
                    <td>Monthly Earning Remarks</td>
                    <th>{{ $incident->monthly_earning_remarks Or "-" }}</th>
                </tr>

                <tr>
                    <td>Nationality</td>
                    <th>{{ ((($employee->country()->count()) ? $employee->country->name : "-")) }}</th>
                    @if ($employee->country_id And $employee->country_id > 1)
                        <td>Passport No</td>
                        <th>{{ $employee->passport_no Or "-" }}</th>
                        <td>Work Permit No</td>
                        <th>{{ $employee->work_permit_no Or "-" }}</th>
                        <td>Residence Permit No</td>
                        <th>{{ $employee->residence_permit_no Or "-" }}</th>
                    @endif
                </tr>

                @if ($employee->location_type_id == 1)
                    {{--Surveyed Location--}}
                    <tr>
                        <td>Street</td>
                        <th>{{ $employee->street Or "-" }}</th>
                        <td>Road</td>
                        <th>{{ $employee->road Or "-" }}</th>
                        <td>Plot No</td>
                        <th>{{ $employee->plot_no Or "-" }}</th>
                        <td>Block No</td>
                        <th>{{ $employee->block_no Or "-" }}</th>
                        <td>Surveyed Extra</td>
                        <th>{{ $employee->surveyed_extra Or "-" }}</th>
                    </tr>
                @elseif ($employee->location_type_id == 2)
                    {{--Unsurveyed Location--}}
                    <td>Unsurveyed Area</td>
                    <th>{{ $employee->unsurveyed_area Or "-" }}</th>
                @else
                    {{--Unregistered Location--}}
                    <td>Unsurveyed Area</td>
                    <th>{{ $employee->unsurveyed_area Or "-" }}</th>
                @endif


                </tbody>
            </table>
        </div>
    </div>
@else
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Info.</strong> Employee has not been registered.
        </div>
    </div>
@endif