<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonGeneral" data-toggle="dropdown" >@lang('buttons.general.more')</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonGeneral" style="z-index: 99999999">

        {{--For accident--}}
        @if (in_array($incident->incident_type_id, [1,4]))
            {{--Add new Witness--}}
            <a class="dropdown-item" href="{!! route('backend.claim.notification_report.create_accident_witness', $incident->id) !!}"><i class="icon fa fa-users" aria-hidden="true"></i>&nbsp;@lang('buttons.backend.claim.add_witness')</a>
            <div class="dropdown-divider"></div>
        @endif

        @if($incident_stage_code == 13)
            {{--Update Details--}}
            <a class="dropdown-item" href="{!! route('backend.claim.notification_report.update_details', $incident->id) !!}"><i class="icon fa fa-pencil" aria-hidden="true"></i>&nbsp;Update Details</a>
            <div class="dropdown-divider"></div>
        @endif

        @if(in_array($incident->incident_type_id, [1,2,4,5]))
            {{--Update Death Details--}}
            <a class="dropdown-item" href="{!! route('backend.claim.notification_report.update_death_details', $incident->id) !!}"><i class="icon fa fa-stop" aria-hidden="true"></i>&nbsp;Update Death Details</a>
            <div class="dropdown-divider"></div>
        @endif

        {{--@if (!is_null($incident->employee_id) And !$incident->isdmsposted)--}}
        @if (true)
            {{--Send to E-office--}}
            <a class="dropdown-item" href="{!! route('backend.claim.notification_report.send_to_dms', $incident->id) !!}"><i class="icon fa fa-send-o" aria-hidden="true"></i>&nbsp;Send to eOffice</a>
        @endif


        @if ($incident->progressive_stage == 1)
            <div class="dropdown-divider"></div>
            {{--Undo Notification--}}
            {!! Html::decode(link_to_route('backend.claim.notification_report.undo', "<i class='icon fa fa-ban' aria-hidden='true'></i>&nbsp;" . trans('labels.general.undo') . " (Delete Case)", $incident->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_notification_report_undo'), 'class' => 'dropdown-item'])) !!}
        @else
            @if (($incident_stage_code == 13 Or $incident_stage_code == 23))
                <div class="dropdown-divider"></div>
                {{--Void Notification--}}
                {!! Html::decode($incident->initialize_void_notification_link) !!}
            @endif
        @endif

        @if ($incident_stage_code == 23)
            {{--Void/Undo Notification Rejection--}}
            <div class="dropdown-divider"></div>
            {!! Html::decode($incident->initialize_undo_notification_rejection_link) !!}
        @endif

        {{--Close--}}
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{!! route('backend.claim.menu') !!}" ><i class="icon fa fa-outdent" aria-hidden="true"></i>&nbsp;Exit</a>

  </span>

</span>