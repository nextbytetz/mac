<br/>
<div class="row">
    <div class="col-md-12">
        <legend>Workflow History</legend>

        @if ($incident->workflows()->count())
            @php
                $workflow_histories = $incident->workflows;
            @endphp
            @foreach($workflow_histories as $workflow_history)
                @php
                    $whwfmodule = $workflow_history->wfModule;
                @endphp
                @include("backend.includes.workflow.completed_tracks", ["wf_tracks" => $workflow_history->wfTracks, "module" => $whwfmodule, "resource_id" => $workflow_history->id, "wf_module_group_id" => $whwfmodule->wf_module_group_id, "type" => $whwfmodule->type])

            @endforeach
        @else
            <div class="alert-left-border">
                <div class="alert alert-primary alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>No workflow history.</strong> This notification has no associated workflow recorded.
                </div>
            </div>
        @endif

    </div>
</div>