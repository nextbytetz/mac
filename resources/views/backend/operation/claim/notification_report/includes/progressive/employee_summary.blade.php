@php
$employee = $incident->employee;
$employer = $incident->employer;
@endphp

<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h6><b><span class="light_dark_color">Employee Summary</span></b></h6></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{--<div class="light_grey_bg">&nbsp;</div>--}}
        <div class="light_grey_bg" style="padding: 5px;">
            <div class="underline" style="font-weight: lighter;">@lang('labels.general.employee_name')&nbsp;:&nbsp;<span style="font-weight:bold;">{{ $employee->name }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.member.memberno')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $employee->memberno }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.general.dob')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $employee->dob_formatted }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.general.age')&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="@lang('labels.backend.claim.age_description')"></i>&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $age }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.general.gender')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $gender }}</span></div>
            <div class="underline" style="font-weight: lighter;">Job Title&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $jobtitle }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.general.employer')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $employer->name }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.table.employer_reg_no')&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $employer->reg_no }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.claim.gross_monthly_earning')&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.gross_monthly_earning_description')  "></i>&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $monthly_earning }}</span></div>
            <div class="underline" style="font-weight: lighter;">@lang('labels.backend.finance.receipt.contrib_month')&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.contrib_month_description')"></i>&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $contrib_month }}</span></div>
            <div class="underline" style="font-weight: lighter;">Next of Kin&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $employee->next_of_kin Or "-" }}&nbsp;{{ $employee->next_of_kin_phone Or "-" }}</span></div>
        </div>
    </div>
</div>