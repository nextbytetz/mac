<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonLinks" data-toggle="dropdown" >@lang('buttons.general.more')</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonLinks" style="z-index: 99999999">

        @if ($incident_stage_code == 13)
            {{--Acknowledgement letter--}}
            {{--<a class="dropdown-item" href="{!! route('backend.claim.notification_report.ackwlgletter', $incident->id)!!}"><i class="icon fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Acknowledgement Letter</a>--}}
            @if($incident->is_acknowledgment_sent)
                @if($incident->progressive_stage < 4)
                {{--@if(true)--}}
                    <a class="dropdown-item" href="{!! route('backend.letter.process', ['resource' => $incident->id, 'reference' => 'CLRMNDRLETTER'])!!}"><i class="icon fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Reminder Letter</a>
                @endif
                <a class="dropdown-item" href="{!! route('backend.letter.process', ['resource' => $incident->id, 'reference' => 'CLACKNOWLGMNT'])!!}"><i class="icon fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Acknowledgement Letter</a>
            @else
                <a class="dropdown-item" href="{!! route('backend.letter.process', ['resource' => $incident->id, 'reference' => 'CLACKNOWLGMNT'])!!}"><i class="icon fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Acknowledgement Letter</a>
            @endif
            {{--Rejection letter--}}
            {{--@if ($incident->can_issue_rejection_letter)
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{!! route('backend.claim.notification_report.ackwlgletter', $incident->id)!!}"><i class="icon fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Rejection Letter</a>
            @endif--}}

            {{--Acceptance letter [After Notification Approval]--}}
            {{--@if ($incident->progressive_stage >= 4)
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#"><i class="icon fa fa-paperclip" aria-hidden="true"></i>&nbsp;Acceptance Letter</a>
            @endif--}}

            @if ($incident->can_re_investigate)
                {{--Initialize Re-filling Investigation (Manual Procedure) [After Notification Approval]--}}
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#"><i class="icon fa fa-paw" aria-hidden="true"></i>&nbsp;Re-fill Investigation</a>
            @endif

            @if ($incident->can_close_incident)
                {{--Close Incident [After On Progress At least Paid]--}}
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#"><i class="icon fa fa-recycle" aria-hidden="true"></i>&nbsp;Close Incident</a>
            @endif

            @if ($incident->can_start_reassessment)
                {{--Start Reassessment|Appeal [After Closed Notification]--}}
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#"><i class="icon fa fa-thumb-tack" aria-hidden="true"></i>&nbsp;Appeal</a>
            @endif

            {{--Undo Notification Approval--}}
            @if ($incident->progressive_stage >= 3)
                <div class="dropdown-divider"></div>
                {!! Html::decode(link_to_route('backend.claim.notification_report.undo_approval', "<i class='icon fa fa-undo' aria-hidden='true'></i>&nbsp;Undo Approval" , [$incident->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to undo Notification Approval?", 'class' => 'dropdown-item'])) !!}
            @endif

            {{--Undo Benefit Process--}}
            @if ($incident->progressive_stage >= 4)
                <div class="dropdown-divider"></div>
                {!! Html::decode(link_to_route('backend.claim.notification_report.undo_benefit', "<i class='icon fa fa-refresh' aria-hidden='true'></i>&nbsp;Undo Benefit Process" , [$incident->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'Are you sure to undo Benefit Process?', 'class' => 'dropdown-item'])) !!}
            @endif

            {{--Update Manual Payment--}}
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route("backend.claim.notification_report.do_manual_payment", $incident->id) }}"><i class="icon fa fa-rouble" aria-hidden="true"></i>&nbsp;Update Manual Payment
        </a>

            @if ($incident->haspaidmanual)
                <div class="dropdown-divider"></div>
                {{--Undo Manual Payment--}}
                {!! Html::decode(link_to_route('backend.claim.notification_report.undo_manual_payment.post', "<i class='icon fa fa-undo' aria-hidden='true'></i>&nbsp;Undo Manual Payment" , [$incident->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to undo Manual Payment?", 'class' => 'dropdown-item'])) !!}
            @endif

        @endif
            <div class="dropdown-divider"></div>
            {{--osh data--}}
            <a class="dropdown-item" href="{{ route("backend.claim.notification.update_osh_data", $incident->id) }}"><i class="icon fa fa-edit" aria-hidden="true"></i>Update OSH Data</a>



        @if ($incident->impairment_assessment_id)
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route("backend.assessment.impairment.dashboard", $incident->id) }}"><i class="icon fa fa-stethoscope" aria-hidden="true"></i>&nbsp;Impairment Assessment Dashboard
        </a>
            @else
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route("backend.assessment.impairment.create", $incident->id) }}"><i class="icon fa fa-plus-square-o" aria-hidden="true"></i>&nbsp;Create Impairment Assessment
        </a>
            @endif

        {{--Close--}}
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{!! route('backend.claim.menu') !!}" ><i class="icon fa fa-outdent" aria-hidden="true"></i>&nbsp;Exit</a>

  </span>

</span>