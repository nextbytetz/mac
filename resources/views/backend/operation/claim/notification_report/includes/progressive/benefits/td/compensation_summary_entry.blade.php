<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Compensation Type</th>
        <th>Payable Amount</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @if (count($value))
        <tr>
            <td>@lang('labels.backend.claim.amount_of_ttd') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.amount_of_ttd_description')  "></i></td>
            <td>{{ number_2_format($value['ttd_amount']) }}</td>
            <td>{!! $value['status'] !!}</td>
        </tr>
        <tr>
            <td>@lang('labels.backend.claim.amount_of_tpd') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.amount_of_tpd_description')"></i></td>
            <td>{{ number_2_format($value['tpd_amount']) }}</td>
            <td>{!! $value['status'] !!}</td>
        </tr>
        <tr>
            <td><b>TOTAL TD PAYMENT</b></td>
            <td>{{ number_2_format($value['total_lumpsum']) }}</td>
            <td>{!! $value['status'] !!}</td>
        </tr>
    @endif

    </tbody>
</table>