{{--DISABILITY --}}
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/backend/receipt/employer.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/xdan/css/jquery.datetimepicker.min.css") }}
@endpush
{{-- @include('backend.includes.assets.datetimepicker') --}}
{{-- {{dd($eligible)}} --}}
@php $type_of_duty=[];
foreach ($notification_disability_states as $not){
  $type_of_duty[$not->disabilityStateChecklist->id] = $not->disabilityStateChecklist->name;
}
@endphp
<div class="row">
  <div class="col-md-12 mt-1">
    <nav class="navbar navbar-light bg-light" style="background-color: #eceeef; border-radius: 0px;">
     <div class="fileld-layout">
      <div class="form-group mt-1">
        <div class="form-inline">
          <div class="row">
            <div class="col-sm-2">
              <label class="required">Document Used For Assesment</label>
              {!! Form::select('type_of_document', $incident->documents->pluck('name', 'id')->all(), $document_used['document_id'], ['class' => 'form-control','id' => 'type_of_document_select','style'=>'width:100%; background-color: #fff !important;']) !!}
            </div>
            <div class="col-sm-3">
              &nbsp;<label class="required">Select Document</label>
              <select id="document_used" name="document_used" class="form-control" style='width:100%; background-color: #fff !important; '>
                <option value="">Select document type first</option>
              </select>
            </div>
            <div class="col-sm-2">
              &nbsp;<label class="required">Document Used Date</label>
              <div class="input-group">
                {!! Form::text('document_date', $document_used['document_date'], ['placeholder' => '', 'class' => 'form-control','id' => 'document_date','autocomplete' => 'off']) !!}
                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
              </div>
            </div>
            <div class="col-sm-3">
              &nbsp;<label class="required">HCP</label>
              {!! Form::select('hcp_name', [], null, ['class' => 'form-control','id' => 'hcp_name','style'=>'width:100%;', 'style'=>'background-color: #fff !important;', 'style'=>'width:100%;']) !!}
            </div>
            <div class="col-sm-2">
              <label> &nbsp;</label><br>
              <button class="btn btn-sm btn-success document_used_preview {{ !empty($document_used['document_date']) ? '' : 'hidden' }}"><i class="fa fa-eye-slash preview_icon"></i> &nbsp;Preview Document</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
</div>

<div class="col-md-12 mt-1">
  <nav class="navbar navbar-light bg-light pt-2" style="background-color: #DCE6E6; border-radius: 0px;">
    @foreach ($notification_disability_states as $notification_disability_state)
    <div class="type-of-duty-assessment-div">&nbsp;</div>

    @php
    if ($eligible->notificationDisabilityStateAssessments()->where('notification_disability_state_id', $notification_disability_state->id)->count()) {
      $disability_state_assessment = $eligible->notificationDisabilityStateAssessments->where('notification_disability_state_id',$notification_disability_state->id)->first();
      $disability_state_assessment_days = $disability_state_assessment->days;
      $disability_state_assessment_remarks = $disability_state_assessment->remarks;
    } else {
      $disability_state_assessment_days = NULL;
      $disability_state_assessment_remarks = NULL;
    }
    @endphp
    <div class="form-inline">
      <div class="type-of-duty-assessment-div" >
        {{--<div class="col-md-12">--}}
          {{--<div class="col-md-3">--}}
            <div class="form-group">
              {!! Form::label( 'days'. $notification_disability_state->id, $notification_disability_state->disabilityStateChecklist->name, [ 'id'=> 'checklist'])!!}
              {!! Form::input( 'text','assessed_days' . $notification_disability_state->id , $disability_state_assessment_days, ['class' => 'form-control number disability_state','id'=>'days'.$notification_disability_state->id , 'placeholder'=>trans('labels.backend.claim.no_of_days'), 'disabled'=> !$user_has_access  ]) !!}
              {!! $errors->first('assessed_days'. $notification_disability_state->id, '<span class="help-block label label-danger">:message</span>') !!}
            </div>
          {{--</div>--}}
          {{--<div class="col-md-3">--}}
            <div class="form-group">
              {!! Form::label( 'remarks' . $notification_disability_state->id, "Remark(s)") !!}
              {!! Form::textarea( 'remarks' . $notification_disability_state->id, $disability_state_assessment_remarks, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none', 'id' => 'remarks' . $notification_disability_state->id]) !!}
              {!! $errors->first('remarks' . $notification_disability_state->id, '<span class="help-block label label-danger">:message</span>') !!}
            </div>
          {{--</div>--}}

          {!! Form::hidden( 'benefit_type' . $notification_disability_state->id , $notification_disability_state->disabilityStateChecklist->benefit_type_id, ['class' => 'form-control number disability_state', 'id' => 'benefit_type' . $notification_disability_state->id ,  'placeholder'=>trans('labels.backend.claim.no_of_days')]) !!}
          {!! Form::hidden( 'percent_of_ed_ld' . $notification_disability_state->id , $notification_disability_state->percent_of_ed_ld, ['class' => 'form-control number disability_state', 'id'=> 'percent_of_ed_ld' . $notification_disability_state->id ,  'placeholder'=>trans('labels.backend.claim.percent_of_ed_ld')]) !!}
        </div>

      </div>
      @endforeach
    </nav>
  </div>

</div>


@push('after-script-end')

<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/receipt/employer.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/xdan/js/jquery.datetimepicker.full.min.js") }}
<script>

  $(function() { 

   jQuery('#document_date').datetimepicker({
    timepicker:false,
    format:'Y-n-j',
    weeks: true,
    dayOfWeekStart: 1,
    minDate: new Date('2015-7-1'),
    lazyInit: true,
    scrollInput: false,
    maxDate: new Date()
  });


   $('#mySelect2').val('1'); 
   $('#mySelect2').trigger('change'); 

   $(".search-select").select2({});
   autosize($("textarea.autosize"));
   selectedDocumentType();
   returnAppend();


   $('#type_of_document_select').on('change', function (e) {
    e.preventDefault();
    selectedDocumentType();
  });

   $('#document_used').on('change', function (e) {
    e.preventDefault();
    selectedDocumentPreview();
    $('#document_date').val('');
    @if(!empty($document_used['document_id']))
    if($('#document_used option:selected').val() == {{$document_used['document_notification_report_id']}}){
     $('#document_date').val('{{$document_used['document_date']}}');
     $('.document_used_preview').removeClass('hidden');
   }else{
     $('#document_date').val('');
     $('.document_used_preview').addClass('hidden');
   }
   @else
   $('#document_date').val('');
   $('.document_used_preview').addClass('hidden');
   @endif
 });

   $('.document_used_preview').on('click', function (e) {
     e.preventDefault();
     selectedDocumentPreview();
   });

   function selectedDocumentPreview() {
    $.ajax({
      url : "{{url('/claim/notification_report/current_base64_document/'.$incident->id.'/')}}/"+$('#document_used option:selected').val(),
      type: 'POST',
      datatype : 'json',
      success: function(data) {
        if (data.success) {
          $('.preview_modal_title').text($('#document_used option:selected').text()+' Preview');
          $('#preview_doc_modal').modal('show');
          let $document_frame = $(".preview_modal_body");
          $document_frame.find("iframe").remove();
          let $iframe = $('<iframe src="'+data.url+'" frameborder="0"  width=\'100%\' height=\'600px\' allowfullscreen></iframe>');
          $document_frame.append($iframe);

        }else{

        }
      },
      error: function (xhr, ajaxOptions, thrownError) {

      }});
  }


  function selectedDocumentType() {
    let doc_id = $('#type_of_document_select option:selected').val();
    $.ajax({
      url : "{{url('/claim/document_select_option/'.$incident->id.'/')}}/"+doc_id,
      type: 'GET',
      datatype : 'json',
      success: function(data) {
        if (data.success) {
         $('.unhide_this').removeClass('hidden');
         $('#document_used').html('');
         $('#document_used').append(data.options);
         @if(!empty($document_used['document_id']))
         if(doc_id == {{$document_used['document_id']}}){
          $("#document_used").val("{{$document_used['document_notification_report_id']}}");
        }
        @endif
      }else{
        $('#document_used').html('');
        $('.unhide_this').addClass('hidden');
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
     $('.unhide_this').addClass('hidden');
   }});
  }

  $(".document_used_preview").mouseup(function() {
   $(".preview_icon").toggleClass("fa-eye fa-eye-slash");
 }).mousedown(function() {
  $('.preview_icon').toggleClass("fa-eye-slash fa-eye");
});

 $( ".document_used_preview" ).hover(
  function() {
    $( '.preview_icon' ).toggleClass("fa-eye fa-eye-slash");
  }, function() {
    $( '.preview_icon' ).toggleClass("fa-eye-slash fa-eye");
  }
  );

 $("#hcp_name").select2({
  minimumInputLength: 3,
  multiple: false,
  ajax: {
    url: "{!! route('backend.claim.registered_health_providers') !!}",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        q: params.term || "",
        page: params.page || 1
      };
    },
    processResults: function (data, params) {
      params.page = params.page || 1;
      return {
        results: $.map(data.items, function (item) {
          return {
            text: item.full_info,
            id: item.id
          };
        }),
        pagination: {
          more: true
        }
      };
    },
    cache: true
  },
  escapeMarkup: function (markup) {
    return markup;
  }
});

 @if(!empty($document_used['health_provider_id']))
 let $option = $("<option selected></option>").val('{{$document_used['health_provider_id']}}').text("{{$document_used['hcp_name']}}");
 $('#hcp_name').append($option).trigger('change');
 @endif


 $('#add_contribution_entry').click(function(e) {
  e.preventDefault();
  let selected_type_of_duty = $('#type_of_duty_select option:selected').val();
  returnAppend(selected_type_of_duty);
});

 $('#duty_selected').on('click', '.remove_item', function(e) {
  e.preventDefault();
  $(this).parent().remove();
});


 jQuery.fn.outerHTML = function() {
  return jQuery('<div />').append(this.eq(0).clone()).html();
};

function returnAppend(disability_id = null) {
  urlz = "{{url('/claim/return_append_inputs/'.$eligible->id)}}";
  if (disability_id) {
    urlz += '/'+disability_id;
  } 
  $.ajax({
    url : urlz,
    type: 'GET',
    datatype : 'json',
    success: function(data) {
      if (data.success) {
       $("#duty_selected").append(data.append);
     }else{

     }
   },
   error: function (xhr, ajaxOptions, thrownError) {

   }});
}


});


</script>
@endpush