@push('after-styles-end')
    <style>
        .assessment_summary{{ $assessment->id }}:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "{{ $assessment->benefit->name }}";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .assessment_summary{{ $assessment->id }} {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush
<div class="assessment_summary{{ $assessment->id }}">
    @include("backend/operation/claim/notification_report/includes/progressive/benefits/mae_refund_member/summary_entry", ['assessment' => $assessment])
</div>