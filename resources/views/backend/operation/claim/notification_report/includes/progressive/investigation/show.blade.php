@if ($incident->investigationStaffs()->count())

    <br/>
    <div class="pull-right" >
        <span>
            @if ($incident->can_edit_investigation)
                {{--update investigation--}}
                {!! HTML::decode(link_to_route('backend.claim.notification_report.edit_investigation_report', "<i class='icon fa fa-pencil-square-o' aria-hidden='true'></i>&nbsp;" . "Update Investigation", [$incident->id],['class' => 'btn  btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
            @endif
            @if ($incident->can_fill_investigation)
                {{--complete investigation--}}
                {!! Html::decode(link_to_route('backend.claim.notification_report.complete_investigation_report', "<i class='icon fa fa-mail-forward' aria-hidden='true'></i>&nbsp;Complete Investigation" , [$incident->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to complete filling Investigation Report?", 'class' => 'btn  btn-secondary site-btn'])) !!}

            @endif
        </span>
    </div>
    <br/>
    <legend>Overview</legend>
    <br/>

    @php
        $notificationInvestigators = $incident->investigationStaffs;
    @endphp

    @foreach($notificationInvestigators as $notificationInvestigator)

        <legend>Investigation Officer&nbsp;:&nbsp;<span class="underline">{{ $notificationInvestigator->user->name }}</span>&nbsp;&nbsp;&nbsp;&nbsp;<small>Last Updated &nbsp;:&nbsp; <span class="underline">{{ $notificationInvestigator->updated_at_formatted }}</span>&nbsp;,&nbsp; Type &nbsp;:&nbsp; <span class="underline">{{ $notificationInvestigator->type_formatted }}</span></small></legend>

        <br/>

        @php
            $feedbacks = $notificationInvestigator->feedbacks;
        @endphp

        {{--Show the investigation feedback in comparison with notification saved information--}}
        {{--loop through investigation feedbacks--}}
        <table class="table table-striped table-bordered">
            <tbody>
            @foreach($feedbacks as $feedback)
                @php
                    $question = $feedback->investigationQuestion;
                @endphp
                @if ($question->id == 16)
                    <tr>
                        <td>{{ $question->name }}</td>
                        <th>{{ $feedback->feedback Or "-" }}</th>
                    </tr>
                @else
                @if ($question->id == 34)
                   <tr>
                        <td>{{ $question->name }}</td>
                        @if ($feedback->code_value_id != null)
                        <th>{{ $feedback->codeValue->name. ' - '. $feedback->feedback }}</th>
                        @else
                        <th>{{ $feedback->feedback Or "-" }}</th>
                        @endif
                        <th>{{ $feedback->remarks Or "-" }}</th>
                    </tr>
                @elseif ($question->id == 33)
                   <tr>
                        <td>{{ $question->name }}</td>
                        @if ($feedback->code_value_id != null)
                        <th>{{ $feedback->codeValue->name. ' - '. $feedback->feedback }}</th>
                        @else
                        <th>{{ $feedback->feedback Or "-" }}</th>
                        @endif
                        <th>{{ $feedback->remarks Or "-" }}</th>
                    </tr>
                @elseif ($question->id == 32)
                   <tr>
                        <td>{{ $question->name }}</td>
                        @if ($feedback->code_value_id != null)
                        <th>{{ $feedback->codeValue->name. ' - '. $feedback->feedback }}</th>
                        @else
                        <th>{{ $feedback->feedback Or "-" }}</th>
                        @endif
                        <th>{{ $feedback->remarks Or "-" }}</th>
                    </tr>
                 @elseif ($question->id == 31)
                   <tr>
                        <td>{{ $question->name }}</td>
                        @if ($feedback->code_value_id != null)
                        <th>{{ $feedback->codeValue->name. ' - '. $feedback->feedback }}</th>
                        @else
                        <th>{{ $feedback->feedback Or "-" }}</th>
                        @endif
                        <th>{{ $feedback->remarks Or "-" }}</th>
                    </tr>
                @else

                    <tr>
                        <td>{{ $question->name }}</td>
                        <th>{{ $feedback->feedback Or "-" }}</th>
                        <th>{{ $feedback->remarks Or "-" }}</th>
                    </tr>
                @endif
                @endif

            @endforeach
            </tbody>
        </table>
        <br/>
    @endforeach

    {{--Upload images if incident is accident or disease--}}
    @if ($incident->need_upload_investigation_images)
        @push('after-styles-end')
            {{ Html::style(asset_url() . "/nextbyte/plugins/krajeev4.5.0/css/fileinput.min.css") }}
            {{ Html::style(asset_url() . "/nextbyte/plugins/krajeev4.5.0/themes/explorer-fa/theme.min.css") }}
        @endpush

        {{--upload images--}}
        <div class="row">
            <div class="col-md-12">
                <div class="element-form" >
                    <div class="col-xl-4 col-lg-4  col-md-3 col-sm-12 col-xs-4 text-xs-right required">Sample Image(s) of injured employee</div>
                    <div class="col-md-7">
                        {!! Form::file('images[]', ['id' => 'investigation_images', 'multiple']) !!}
                    </div>
                </div>
            </div>
        </div>
        <br/>

        @push('after-script-end')
            {{ Html::script(asset_url(). "/nextbyte/plugins/krajeev4.5.0/js/plugins/sortable.min.js") }}
            {{ Html::script(asset_url(). "/nextbyte/plugins/krajeev4.5.0/js/fileinput.min.js") }}
            {{ Html::script(asset_url(). "/nextbyte/plugins/krajeev4.5.0/themes/explorer-fa/theme.min.js") }}
            <script>
                $(document).ready(function () {
                    var $images = <?php echo json_encode(array_map(function ($data) use ($incident) { return $data['source']; }, $incident->assetInvestigationDocuments())) ?>;
                    var $configs = <?php echo json_encode($incident->assetInvestigationDocuments()) ?>;
                    var $needEdit = false;
                    @if ($incident->can_fill_investigation)
                        $needEdit = true;
                            @endif

                    var $imageel = $("#investigation_images");
                    $imageel.fileinput({
                        theme: 'explorer-fa',
                        allowedFileExtensions: ['jpg', 'png', 'gif', 'pdf', 'jpeg'],
                        uploadUrl: "{{ route("backend.claim.notification_report.upload_investigation_document", $incident->id) }}",
                        browseClass: "btn btn-secondary site-btn btn-block",
                        showCaption: false,
                        showRemove: false,
                        showUpload: false,
                        initialPreviewShowDelete: $needEdit, //to be set on condition
                        showBrowse: $needEdit, //to be set on condition
                        dropZoneEnabled: $needEdit, //to be set on condition
                        overwriteInitial: false,
                        initialPreviewAsData: true,
                        fileActionSettings : {
                            downloadIcon : "<i class=\"icon fa fa-download\" aria-hidden=\"true\"></i>"
                        },
                        uploadExtraData: function($previewId, $index) {
                            var $obj = {'index' : $index};
                            return $obj;
                        },
                        progressCompleteClass : "progress-bar bg-success progress-bar-success el-radius",
                        initialPreview: $images,
                        initialPreviewConfig: $configs,
                        initialPreviewDownloadUrl: base_url + "/claim/notification_report/download_investigation_document/{{ $incident->id }}/{key}"
                    }).on('fileimagesloaded', function($event, $files, $label) {
                        $imageel.fileinput('upload');
                    }).on('fileuploaded', function($event, $data, previewId, index) {
                        var $response = $data.response;
                        /*console.log($response);*/
                    }).on("filebeforedelete", function() {
                        var $aborted = !window.confirm('Are you sure you want to delete this file?');
                        return $aborted;
                    });

                });
            </script>
        @endpush
    @endif

@else
    <br/>
    <br/>
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Investigation Feedback.</strong> No User has been assigned for investigating this incident..
        </div>
    </div>
@endif