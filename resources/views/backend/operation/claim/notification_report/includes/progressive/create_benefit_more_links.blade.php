<span class="dropdown">
    <a class="btn btn-success site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonCreateBenefit" data-toggle="dropdown" >Benefit Payment</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonCreateBenefit" style="z-index: 99999999">
        @if ($incident->mae()->count())
            {{--@if ($incident->mae_refund_member_ready)--}}
                    {{-- Initiate MAE Refund Employee/Employer Payment --}}
                    <span>
                    <a class="dropdown-item" href="{!! route('backend.claim.notification_report.initiate_benefit_payment', ['incident' => $incident->id, 'benefit' => 1]) !!}"><i class="icon fa fa-medkit" aria-hidden="true"></i>&nbsp;MAE Refund (Employee/Employer) Payment</a>
                    <div class="dropdown-divider"></div>
                    </span>
            {{--@endif--}}
        @endif

            @if ($incident->td()->count())
                        @if ($incident->td->td_ready)
                                {{-- Initiate TD Payment --}}
                                <span>
            <a class="dropdown-item" href="{!! route('backend.claim.notification_report.initiate_benefit_payment', ['incident' => $incident->id, 'benefit' => 3]) !!}"><i class="icon fa fa-user-md" aria-hidden="true"></i>&nbsp;TD Payment</a>
            <div class="dropdown-divider"></div>
            </span>
                        @endif
                        @if ($incident->td->td_refund_ready)
                                {{--Initiate TD Refund Payment --}}
                                <span>
            <a class="dropdown-item" href="{!! route('backend.claim.notification_report.initiate_benefit_payment', ['incident' => $incident->id, 'benefit' => 4]) !!}"><i class="icon fa fa-money" aria-hidden="true"></i>&nbsp;TD Refund Payment</a>
            <div class="dropdown-divider"></div>
            </span>
                        @endif
            @endif


        @if ($incident->pd_ready And is_null($incident->pd_eligible_id))
             {{--Initiate PD Payment--}}
            <span>
                {!! HTML::decode(link_to_route('backend.claim.notification_report.initiate_benefit_payment', "<i class='icon fa fa-wheelchair' aria-hidden='true'></i>&nbsp;Initiate PD Payment", [$incident->id, 5], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to initiate PD Payment?", 'data-toggle' => 'tooltip', 'data-html' => 'true', 'title' => 'Initiate PD Payment', 'class' => 'dropdown-item']))  !!}
            <div class="dropdown-divider"></div>
            </span>
        @endif

        @if ($incident->survivor_ready And is_null($incident->survivor_eligible_id))
             {{--Initiate Survivor Payment, no need for editing, only need initiating --}}
            <span>
            <a class="dropdown-item" href="{!! route('backend.claim.notification_report.initiate_benefit_payment', ['incident' => $incident->id, 'benefit' => 7]) !!}"><i class="icon fa fa-group" aria-hidden="true"></i>&nbsp;Survivor Payment</a>
            <div class="dropdown-divider"></div>
            </span>
        @endif

    </span>
</span>