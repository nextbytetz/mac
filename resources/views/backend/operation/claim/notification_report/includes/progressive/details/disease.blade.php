@php
    $disease = $incident->disease;
@endphp
<div class="row">
    <div class="col-md-4" style="padding-right:20px; border-right: 1px solid #ddd;">
        {{--cause of disease--}}
        <div class="fileld-layout">
            <label class="required">Cause of Disease</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('disease_cause_cv_id', $disease_causes, $disease->disease_cause_cv_id, ['class' => 'search-select', 'style' => 'width:100%', 'style' => 'width:100%', 'placeholder' => '']) !!}
                </div>
                <span class="help-block"></span>
            </div>
        </div>
        {{--date of diagnosis--}}
        <div class="fileld-layout">
            <label class="required">Date of Diagnosis</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('diagnosis_date', $disease->diagnosis_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block">
                        {{--<span>Date when diseaseoccurred</span>--}}
                    <small class="form-text text-muted" style="width:100% !important;">Date when disease was diagnosed</small>
                    </span>
            </div>
        </div>
        {{--reporting date--}}
        <div class="fileld-layout">
            <label class="required">Reporting Date</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('reporting_date', $disease->reporting_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block">
                        {{--<span>Date when accident occurred</span>--}}
                    <small class="form-text text-muted" style="width:100% !important;">Date of Reporting to Employer</small>
                    </span>
            </div>
        </div>
        {{--date of hire--}}
        {{--<div class="fileld-layout">
            <label class="required">Date of Hire</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('datehired', $incident->datehired, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block">
                    <small class="form-text text-muted" style="width:100% !important;">Date hired to the current employer</small>
                    </span>
            </div>
        </div>--}}
        {{--date last worked before diagnosis--}}
        @if(0)
            <div class="fileld-layout">
                <label class="required">Date Last Worked</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('last_work_date', $incident->last_work_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Last day worked before the diagnosis</small>
                    </span>
                </div>
            </div>
        @else
            {!! Form::hidden("last_work_date", $incident->last_work_date) !!}
        @endif

    </div>
    <div class="col-md-4" style="padding-right:20px; border-right: 1px solid #ddd;">
        {{--paid salary before incident (diagnosis date)--}}
        @if(0)
            <div class="fileld-layout">
                <label class="required">Paid Salary?</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('paid_month_before', [1 => 'Yes', 0 => 'No'], $incident->paid_month_before, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">If salary was paid in the last month before diagnosis</small>
                    </span>
                </div>
            </div>
            {{--Gross Salary Per Month--}}
            <div class="fileld-layout">
                <label>Gross Salary</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('monthly_earning_checklist', $incident->monthly_earning_checklist, ['placeholder' => '', 'class' => 'form-control']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Gross Salary Per Month before the disease</small>
                    </span>
                </div>
            </div>
            {{--salary being continued--}}
            <div class="fileld-layout">
                <label class="required">Salary Being Continued?</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('salary_being_continued', [1 => 'Yes', 0 => 'No'], $disease->salary_being_continued, ['class' => 'search-select salary_being_continued_select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">If Salary has been given after disease</small>
                    </span>
                </div>
            </div>
            {{--salary being continued in full--}}
            <div class="fileld-layout salary_being_continued">
                <label class="required">Salary Being Continued in Full?</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('salary_full_continued', [1 => 'Yes', 0 => 'No'], $disease->salary_full_continued, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;"></small>
                    </span>
                </div>
            </div>
        @else
            {!! Form::hidden("paid_month_before", 1) !!}
            {!! Form::hidden("monthly_earning_checklist", $incident->monthly_earning) !!}
            {!! Form::hidden("salary_being_continued", 1) !!}
            {!! Form::hidden("salary_full_continued", 1) !!}
        @endif

        <!--Region-->
        <div class="fileld-layout">
            <label class="required">Notification Region</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('region_id', $regions, $region_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                </div>
            </div>
        </div>
        <!--District-->
        <div class="fileld-layout">
            <label class="required">Notification District</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('district_id', $districts, $district_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        {{--Body Party Injury--}}
        <div class="fileld-layout">
            <label class="required">Body Part Injury</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('body_part_injury_id[]', $body_part_injuries, $body_part_injuries_values, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'body_part_injury_id']) !!}
                    <i class="icon fa fa-spinner fa-spin spin2" aria-hidden="true" style="display: none;"></i>
                </div>
                <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Can select multiple entries</small>
                    </span>
            </div>
        </div>
        {{--lost body part--}}
        <div class="fileld-layout">
            <label class="required">Has Lost Body Part?</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('lost_body_part', ['1' => 'Yes', '0' => 'No'], $disease->lost_body_part, ['class' => 'search-select lost_body_part_select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                </div>
                <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;"></small>
                    </span>
            </div>
        </div>
        {{--List Lost Body Part/Function--}}
        <div class="fileld-layout lost_body_part">
            <label class="required">List Lost Body Parts</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('lost_body_part_id[]', $lost_body_parties, $lost_body_parties_values, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'lost_body_part_id']) !!}
                </div>
                <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Can select multiple entries. Selection Based on Body Part Injury.</small>
                    </span>
            </div>
        </div>
        {{--disease incident exposure--}}
        @if(0)
            <div class="fileld-layout">
                <label>Disease Incident Exposure</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('disease_incident_exposure_cv_id', $disease_incident_exposures, $disease->disease_incident_exposure_cv_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
        @else
            {!! Form::hidden("disease_incident_exposure_cv_id", NULL) !!}
        @endif

    </div>
</div>