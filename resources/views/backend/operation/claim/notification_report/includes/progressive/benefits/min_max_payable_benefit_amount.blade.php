{{--Minimum and Maximum Compensation amount--}}
<table class="table table-striped table-bordered" style="width:100%">
    <tbody>
    {{--Header--}}
    <tr>
        <td>Minimum monthly compensation</td>
        <th>{!! number_2_format( sysdefs()->data()->minimum_monthly_pension) !!}</th>
        <td>Maximum monthly compensation</td>
        <th>{!! number_2_format( sysdefs()->data()->maximum_monthly_pension) !!}</th>
    </tr>


    </tbody>
</table>
{{--end on min /max--}}