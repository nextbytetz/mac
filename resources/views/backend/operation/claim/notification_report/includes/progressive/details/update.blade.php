@if (in_array($incident->incident_type_id, [1,4]))
    {{--Accident--}}
    @include("backend/operation/claim/notification_report/includes/progressive/details/accident")
@elseif (in_array($incident->incident_type_id, [2,5]))
    {{--Disease--}}
    @include("backend/operation/claim/notification_report/includes/progressive/details/disease")
@elseif ($incident->incident_type_id == 3)
    {{--Death--}}
    @include("backend/operation/claim/notification_report/includes/progressive/details/death", ["update_source" => 1])
@endif

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
<script>
    $(function() {
        $(".search-select").select2({});
        let $body = $('body');
        @if(in_array($incident->incident_type_id , [3,4,5]))
            let $death_cause_id = $("#death_cause_id");
            let $incident_date_title = $("#incident_date_title");
            let $accident_cause = $("#accident_cause");
            let $disease_cause = $("#disease_cause");
            $death_cause_id.on('change', function (e) {
                let $choice = $death_cause_id.val();
                switch ($choice) {
                    case '1':
                        /*Occupational accident*/
                        $incident_date_title.html("Date of Accident");
                        $accident_cause.show();
                        $disease_cause.hide();
                        break;
                    case '2':
                        /*Occupational disease*/
                        $incident_date_title.html("Date of Diagnosis");
                        $accident_cause.hide();
                        $disease_cause.show();
                        break;
                    default:
                        $incident_date_title.html("Incident Date");
                        $accident_cause.hide();
                        $disease_cause.hide();
                        break;
                }
            });
            $death_cause_id.trigger("change");
        @else
            let $body_part_injury_id = $("#body_part_injury_id");
            let $salary_being_continued_select = $(".salary_being_continued_select");
            $salary_being_continued_select.on('change', function (e) {
                let $choice = $salary_being_continued_select.val();
                let $salary_being_continued = $(".salary_being_continued");
                switch ($choice) {
                    case '1':
                        $salary_being_continued.show();
                        break;
                    case '0':
                        $salary_being_continued.hide();
                        break;
                    default:
                        $salary_being_continued.hide();
                        break;
                }
            });
            $salary_being_continued_select.trigger("change");
            let $lost_body_part_select = $(".lost_body_part_select");
            $lost_body_part_select.on('change', function (e) {
                let $choice = $lost_body_part_select.val();
                let $lost_body_part = $(".lost_body_part");
                switch ($choice) {
                    case '1':
                        $lost_body_part.show();
                        break;
                    case '0':
                        $lost_body_part.hide();
                        break;
                    default:
                        $lost_body_part.hide();
                        break;
                }
            });
            $lost_body_part_select.trigger("change");
            let $lost_body_part_id = $("#lost_body_part_id");
            $body_part_injury_id.on('change', function ($e) {
                $(".spin2").show();
                let $list = $(this).val(); /* return in form of list. 1,3,5,7*/
                $.get("{{ url('/') }}/getLostBodyParties?body_party_injury=" + $list, function ($data) {
                    $lost_body_part_id.empty();
                    $lost_body_part_id.select2("val", "");
                    $lost_body_part_id.html($data);
                    $(".spin2").hide();
                });
            });
        @endif
    });
</script>
@endpush