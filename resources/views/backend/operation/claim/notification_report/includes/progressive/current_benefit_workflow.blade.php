@php
    $color = [
        0 => 'blue',
        1 => 'grey',
    ];
@endphp

@if ($benefit_approvals_model->count())

    @php
        $benefit_approvals = $benefit_approvals_model->get();
        $workflowScriptAlreadyIncluded = false;
    @endphp

    @foreach ($benefit_approvals as $benefit_approval)
        @push('after-styles-end')
            <style>
                .benefit_workflow_summary{{ $benefit_approval->id }}:after {
                    background-color: #F5F5F5;
                    border: 1px solid #DDDDDD;
                    border-radius: 4px 0 4px 0;
                    color: #3c5ba4;
                    content: "{{ $benefit_approval->benefit->name }} Workflow";
                    /* font-size: 12px;
                    font-weight: bold; */
                    left: -1px;
                    padding: 3px 7px;
                    position: absolute;
                    top: -1px;
                }
                .benefit_workflow_summary{{ $benefit_approval->id }} {
                    background-color: #FFFFFF;
                    border: 1px solid #DDDDDD;
                    border-radius: 4px 4px 4px 4px;
                    margin: 5px 0px;
                    padding: 39px 19px 14px;
                    position: relative;
                }
            </style>
        @endpush

        {{--workflow track entry--}}
        @php
            $approval_note = "";
            $workflowEntry = $benefit_approval->workflow;
            /*dd($workflowEntry);*/
            $wfModule = $workflowEntry->wfModule;
            //$type = $wfModule->type;
            $moduleGroup = $wfModule->wfModuleGroup->id;

            $workflow = new \App\Services\Workflow\Workflow(['wf_module_group_id' => $moduleGroup, 'type' => $wfModule->type, 'resource_id' => $workflowEntry->id]);
            if ($workflow->currentWfDefinition()->has_note) {
                $approval_note = (new \App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository())->getPayNoteForApproval($incident, $benefit_approval->id);
            }

            $workflowinput = ['resource_id' => $workflowEntry->id, 'wf_module_group_id'=> $moduleGroup, 'type' => $wfModule->type, 'workflowScriptAlreadyIncluded' => $workflowScriptAlreadyIncluded];
        @endphp

        <div class="benefit_workflow_summary{{ $benefit_approval->id }}" style="padding-left:20px; border-left: 4px solid {!! $color[$loop->iteration % 2] !!};">
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs">
                    {{--Letter Profile--}}
                    <li class="nav-item">
                        <a class="nav-link active" id="benefit_process{{ $benefit_approval->id }}_tab" data-toggle="tab" href="#benefit_process{{ $benefit_approval->id }}" role="tab">
                            <i class="icon fa fa-fire" aria-hidden="true"></i>Benefit Process
                        </a>
                    </li>
                    @if ($benefit_approval->ispayprocessed)
                        <li class="nav-item">
                            <a class="nav-link" id="benefit_document_centre{{ $benefit_approval->id }}_tab" data-toggle="tab" href="#benefit_document_centre{{ $benefit_approval->id }}" role="tab">
                                <i class="icon fa fa-paperclip" aria-hidden="true"></i>Payment Attachment(s)
                            </a>
                        </li>
                    @endif

                </ul>
                <div class="nav_tab_contain tab-content">
                    <div class="tab-pane active" id="benefit_process{{ $benefit_approval->id }}" role="tabpanel">
                        @if ($benefit_approval->benefit_type_claim_id == 1)
                            {{--MAE Refund (Employee/Employer)--}}
                            @include("backend/operation/claim/notification_report/includes/progressive/benefits/mae_refund_member/summary_entry", ['assessment' => $benefit_approval])
                        @endif
                        @if ($benefit_approval->benefit_type_claim_id == 3)
                            {{--Temporary Disablement--}}
                            @include("backend/operation/claim/notification_report/includes/progressive/benefits/td/summary_entry", ['assessment' => $benefit_approval])
                        @endif
                        @if ($benefit_approval->benefit_type_claim_id == 4)
                            {{--Temporary Disablement Refund--}}
                            {{--@include("backend/operation/claim/notification_report/includes/progressive/benefits/td_refund/summary_entry", ['assessment' => $benefit_approval])--}}
                            @include("backend/operation/claim/notification_report/includes/progressive/benefits/td/summary_entry", ['assessment' => $benefit_approval])
                        @endif
                        @if ($benefit_approval->benefit_type_claim_id == 5)
                            {{--Permanent Disablement--}}
                            @include("backend/operation/claim/notification_report/includes/progressive/benefits/pd/summary_entry", ['assessment' => $benefit_approval])
                        @endif
                        @if ($benefit_approval->benefit_type_claim_id == 7)
                            {{--Survivors--}}
                            @include("backend/operation/claim/notification_report/includes/progressive/benefits/survivor/summary_entry", ['assessment' => $benefit_approval])
                        @endif
                        <hr/>

                        @if (!empty($approval_note))
                            <div>
                                <b class="underline">Payment Approval Note</b>&nbsp;&nbsp;<small><button  data-clipboard-action="copy" data-clipboard-target="#approval_note{{ $benefit_approval->id }}" href="#" class="btn btn-sm btn-secondary copy_approval_note" type="button">Copy Note</button></small>
                            </div>
                            <div id="approval_note{{ $benefit_approval->id }}">{!! $approval_note !!}</div>
                            <hr/>
                        @endif

                        @include("backend/includes/workflow/wf_track_html", $workflowinput)
                    </div>
                    @if ($benefit_approval->ispayprocessed)
                        <div class="tab-pane" id="benefit_document_centre{{ $benefit_approval->id }}" role="tabpanel">
                            <br/>
                            @include("backend/system/document/includes/document_center", ['resource' => $benefit_approval, 'reference' => "DRCLMBNFT", 'docs_attached' => (new \App\Repositories\Backend\Operation\Claim\DocumentResourceRepository())->getDocsAttachedNonRecurring($benefit_approval, "DRCLMBNFT"), 'url_selector' => 0])
                        </div>
                    @endif

                </div>
            </div>

            {{--{!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}--}}

            <br/>

        </div>

        @php
            $workflowScriptAlreadyIncluded = true;
        @endphp

    @endforeach

@else
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <i class="fa fa-exclamation-circle"></i><strong>Info</strong> No Benefit Workflow recently initiated
    </div>
@endif