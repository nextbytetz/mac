@if(!empty($claim_follow_up))
<div class = "row">
	<div class="col-md-12" >
		<div class="col-md-12" >


			<div class="pull-left">


				<div class="btn-group">
					{{-- <a style="color: blue;" href="{!! route('backend.compliance.employer.staff_relation.export_employer_follow_ups',[$staff_employer->id ]) !!}"  class="" >Export to excel</a> --}}
					export

				</div>
			</div>

			<div class="pull-right">

				<div class="btn-group">
					@if(!is_null($claim_follow_up->review_status))
					<a href="#" id="history"  class="btn btn-xs btn-info site-btn" ><i class="icon fa fa-random"></i>&nbsp;Assessment History</a>
					@endif

					
					@permission('review_claim_follow_up')
					@if(!is_null($claim_follow_up->review_status))
					@if($claim_follow_up->review_status == 0 or $claim_follow_up->review_status == 3)
					{!! HTML::decode(link_to_route('backend.claim.follow_ups.approve_follow_up', "<i class='icon fa fa-check' aria-hidden='true'></i>&nbsp;" . 'Approve', $incident->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to approve this follow up?', 'class' => 'btn btn-success btn-xs site-btn submit_button'])) !!}

					
					<a href="#" id="reverse"  class="btn btn-xs btn-success site-btn delete_button" ><i class="icon fa fa-mail-reply-all"></i>&nbsp;Reverse</a>
					@endif
					@endif

					@endauth
					@if($incident->allocated == auth()->user()->id)
					@if($claim_follow_up->review_status == 2)
					{!! HTML::decode(link_to_route('backend.claim.follow_ups.resubmit_follow_up', "<i class='icon fa fa-repeat' aria-hidden='true'></i>&nbsp;" . 'Resubmit', $incident->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to resubmit this follow up?', 'class' => 'btn btn-success btn-xs site-btn submit_button'])) !!}
					@endif

					@if(is_null($claim_follow_up->review_status) && $claim_follow_up->followup_count > 0)
					{!! HTML::decode(link_to_route('backend.claim.follow_ups.complete_follow_up', "<i class='icon fa fa-mail-forward' aria-hidden='true'></i>&nbsp;" . 'Complete Follow Up', $incident->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to complete this follow up?', 'class' => 'btn btn-success btn-xs site-btn submit_button'])) !!}
					@endif

					@if(!$follow_up_date_exceeded)
					@if($claim_follow_up->review_status == 2 or is_null($claim_follow_up->review_status))
					<a href="{!! route('backend.claim.follow_ups.create_follow_up', $incident->id) !!}"  class="btn btn-xs btn-primary site-btn save_button" ><i class="icon fa fa-plus"></i>&nbsp;Add New</a>
					@endif
					@endif
					@endif
				</div>
			</div>



		</div>
	</div>


</div>
@endif
<br/>
{{--Table--}}
<div class = "row">
	<div class="col-md-12" >
		<table class="display" cellspacing="0" width="100%" id ="relation-follow-ups-table">
			<thead>
				<tr >
					{{-- <th>Remark</th> --}}
					<th>Follow up Type</th>
					<th>Contact Person</th>
					<th>Contact</th>
					
					<th>Follow up Date</th>
					<th>Staff</th>
				</tr>
			</thead>
		</table>

	</div>
</div>






@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

<script  type="text/javascript">
	$(function() {
		var url = "{!! url("/") !!}";
		// $("#followups_header").one("click", function(){
			$('#relation-follow-ups-table').DataTable({
				processing: true,
				serverSide: true,
				stateSave: true,
				stateSaveCallback: function (settings, data) {
					localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
				},
				stateLoadCallback: function (settings) {
					return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
				},
				ajax:{
					url : '{!! route('backend.claim.follow_ups.get_follow_up_datatable', $incident->id) !!}',
					type : 'get'
				},
				columns: [
				// { data: 'remark', name: 'remark',  orderable : false, searchable : false},
				{ data: 'follow_up_type', name: 'follow_up_type_cv_id',  orderable : false, searchable : false},
				
				{ data: 'contact_person', name: 'contact_person',  orderable : true, searchable : true},
				{ data: 'contact', name: 'contact',  orderable : true, searchable : true},
				{ data: 'date_of_follow_up', name: 'date_of_follow_up' ,orderable : true, searchable : true},
				{ data: 'user_id', name: 'user_id',  orderable : false, searchable : false},
				],
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					$(nRow).click(function() {
						// console.log(aData)
						// return false;
						document.location.href = url +  "/claim/follow_ups/edit/" + aData['claim_followup_id'] + "/" + aData['followup_id'] ;
					}).hover(function() {
						$(this).css('cursor','pointer');
					}, function() {
						$(this).css('cursor','auto');
					});
				}

			});

			$('#history').click( function(e){
				$('#AddFields').empty();
				$.ajax({
					url : '{!! route('backend.claim.follow_ups.review_history_follow_up', $incident->id) !!}',
					type : 'GET',
					dataType: 'json',
					success:function(data){
						if (data.success) {
							$('#followupmodalWorkflow').modal('show');
							var a = data.data;
							if (a && a.length) {
								a.forEach(function(entry) {
									let comments = '';
									if (entry.remark !== null) {
										comments = '<p>Remark&nbsp;:&nbsp;<b>'+entry.remark+'</b></p>';
									}

									$('#AddFields').append('<li class="list-group-item"><p>User&nbsp;:&nbsp;'+entry.firstname+' '+entry.lastname+'</p>'+comments+'<p>status&nbsp;:&nbsp;<b>'+entry.status_description+'</b></p><p><small>Created Date&nbsp;:&nbsp;<span class="underline">'+(entry.created_at)+'</span></p></small></li>');
								});
							}else {
								$('#AddFields').append('<li class="list-group-item text-danger"><p>No History For this Follow Up</p></li>');
							}
						} else {
							console.log('else')
							swal('Sorry {{ucwords(strtolower(access()->user()->firstname))}}','An error has occured! No history Yet','error');
						}
					}
				});
			})

			$('#reverse').click( function(e){
				$('#reverseModal').modal('show');

			})

			$('#reverse_button').click( function(e){
				e.preventDefault()
				
				let reason = $('#reverse_reason').val()
				if (reason !== "") {
					
					$.ajax({
						url : '{!! route('backend.claim.follow_ups.reverse_follow_up', $incident->id) !!}',
						type : 'GET',
						data : {'remark' : reason},
						dataType: 'json',
						success:function(data){
							console.log(data)
							if (data.success) {
								{{-- swal('Hi, {{ucwords(strtolower(access()->user()->lastname))}}','Follow up successfully reversed','success'); --}}

								swal({title: 'Hi, {{ucwords(strtolower(access()->user()->firstname))}}', text: "Follow up successfully reversed!", type: "success"},
									function(){ 
										location.reload();
									}
									);
							} else {
								swal('Sorry {{ucwords(strtolower(access()->user()->firstname))}}','An error has occured! No history Yet','error');
							}
						}
					});
				} else {
					swal('Sorry {{ucwords(strtolower(access()->user()->firstname))}}','An error has occured! Please describe the reversing reason','error');
				}
				

			})


			
		});

	// });


</script>;

@endpush
