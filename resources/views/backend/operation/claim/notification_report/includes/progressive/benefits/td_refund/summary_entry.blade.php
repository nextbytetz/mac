<div class="row">
    <div class="col-md-11">
        {{--put a benefit summary here--}}
        <span class="pull-left">
                {{--Put label for status here--}}
            {!! $assessment->processed_label !!}
            {!! $assessment->isinitiated_label !!}
            </span>
        <span class="pull-right">
                @if (!$assessment->isinitiated)
                {{--initiate workflow approval for payment--}}
                {!! Html::decode(link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate", ['class' => 'btn btn-secondary site-btn initiate_benefit_workflow', 'data-description' => 'Initiate TD Refund Workflow', 'data-incident' => $incident->id, 'data-benefit' => $assessment->benefit->id, 'data-eligible' => $assessment->id, 'data-route' => route("backend.claim.notification_report.initiate_benefit_workflow")])) !!}
            @endif
            @if ($assessment->processed == 0)
                {{--Put edit button for assessment here--}}
                {!! Html::decode(link_to_route('backend.claim.notification_report.edit_benefit_payment', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . "Update", [$incident->id, $assessment->benefit->id, $assessment->id],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:normal;' ])) !!}
            @endif
            </span>
        <br/>
        <br/>

        {{--start : Put Summary Tables Here--}}

        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td></td>
                <th></th>
            </tr>
            </tbody>
        </table>

        {{--end : Put Summary Tables Here--}}

    </div>
    <div class="col-md-1">
            <span class="pull-right">
                {{--if it is not initiated, can be deleted--}}
                @if (!$assessment->isinitiated)
                    {!! HTML::decode(link_to_route('backend.claim.notification_report.delete_benefit_payment', "<i class='icon fa fa-trash fa-2x' aria-hidden='true' style='color:darkred;'></i>", [$incident->id, $assessment->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to delete this benefit payment?", 'data-toggle' => 'tooltip', 'data-html' => 'true', 'title' => 'Delete Benefit Payment']))  !!}
                @endif
            </span>
    </div>
</div>