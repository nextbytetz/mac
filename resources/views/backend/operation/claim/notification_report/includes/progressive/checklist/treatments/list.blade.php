@php
    $color = [
        0 => 'blue',
        1 => 'grey',
    ];
@endphp

@foreach($treatments as $treatment)
    @php
        $i = str_random(5);
    @endphp
    <!-- start : medical treatment line entry -->
    <div class="row" id="treatment_entry_group{!! $i !!}">
        <div class="col-md-12" style="padding-left:20px; border-left: 2px solid {!! $color[$count % 2] !!};">
            <div class="row">
                <div class="col-md-12">
                    <legend></legend>
                    <br/>
                    <div class="row">
                        <div class="col-md-5">
                            {{--health service provider select--}}
                            <div class="fileld-layout">
                                <label class="required">Name of treating Hospital</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::select('health_provider_id' . $i, $treatment->healthProvider()->get()->pluck("fullname_info", "id")->all(), $treatment->health_provider_id, ['style' => 'width:80%', 'placeholder' => '','class' => 'form-control search-select health-provider-select']) !!}
                                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_health_service_provider']) !!}</span>
                                    </div>
                                    <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;"></small>
                        </span>
                                </div>
                            </div>
                            {{--treating medical practiotioners--}}
                            <div class="fileld-layout">
                                <label>Name of treating Medical Practitioner</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::select('medical_practitioner_id' . $i . '[]', $treatment->practitioners()->get()->pluck("fullname_with_external_id", "id")->all(), $treatment->practitioners()->select([DB::raw("medical_practitioners.id as id")])->pluck("id")->all(),  ['style' => 'width:80%','class' => 'form-control practitioner-select', 'multiple' => 'true']) !!}
                                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_medical_practitioner']) !!}</span>
                                    </div>
                                    <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;">Can select multiple names</small>
                        </span>
                                </div>
                            </div>

                            <div class="medical_expense">
                                {{--who paid the medical expense--}}
                                <div class="fileld-layout">
                                    <label class="required">Who Paid Medical Expense?</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            {!! Form::select('member_type_id' . $i, $member_types, $treatment->member_type_id,  ['style' => 'width:80%', 'class' => 'form-control search-select member_type_id', 'placeholder' => '']) !!}
                                        </div>
                                        <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;">Member Type</small>
                        </span>
                                    </div>
                                </div>

                                {{--insurance--}}
                                <div class="fileld-layout insurance_id" style="display: none;">
                                    <label class="required">Insurance</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            {!! Form::select('insurance_id' . $i, $insurances, $treatment->member_id,  ['style' => 'width:80%', 'class' => 'form-control search-select', 'placeholder' => '']) !!}
                                        </div>
                                        <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;"></small>
                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-2">
                            {{--initial treatment date--}}
                            <div class="fileld-layout">
                                <label class="required">Initial Treatment Date</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('initial_treatment_date' . $i, $treatment->initial_treatment_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>
                                    <span class="help-block">
                            {{--<span>Date when accident occurred</span>--}}
                                <small class="form-text text-muted" style="width:100% !important;">Initial Treatment Date</small>
                        </span>
                                </div>
                            </div>
                            {{--last date attended hospital for treatment--}}
                            <div class="fileld-layout">
                                <label>Last date attended</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('last_treatment_date' . $i, $treatment->last_treatment_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>
                                    <span class="help-block">
                            <small class="form-text text-muted" style="width:100% !important;">Last date attended hospital for treatment</small>
                        </span>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-4">
                            {{--excuse from duty days--}}
                            <div class="fileld-layout">
                                <div class="form-group">

                                    <div style="display:inline;">
                                        <label class="required">Has Excuse from Duty?</label>
                                        {!! Form::select('has_ed' . $i, [1 => 'Yes', 0 => 'No'], $treatment->has_ed, ['class' => 'form-control search-select has_ed', 'style' => 'width:45%', 'placeholder' => '']) !!}
                                        <span class="help-block">
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                            </span>
                                    </div>
                                    <div style="display:inline;" class="excuse_from_duty_days">
                                        <label class="required">Excuse from Duty(ED) Days</label>
                                        <div class="input-group" style="width: 40%;">
                                            {!! Form::text('excuse_from_duty_days' . $i, (($treatment->disabilities()->where("disability_state_checklist_id", 2)->count()) ? $treatment->disabilities()->where("disability_state_checklist_id", 2)->first()->pivot->days : null), ['placeholder' => '', 'class' => 'form-control excuse_from_duty_days_input']) !!}
                                            <span class="input-group-addon"><i class="icon fa fa-blind"></i></span>
                                        </div>
                                        <span class="help-block">
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                            </span>
                                    </div>

                                </div>
                            </div>
                            {{--hospitalization days--}}
                            <div class="fileld-layout">
                                <div class="form-group">

                                    <div style="display:inline;">
                                        <label class="required">Has Hospitalization?</label>
                                        {!! Form::select('has_hospitalization' . $i, [1 => 'Yes', 0 => 'No'], $treatment->has_hospitalization, ['class' => 'form-control search-select has_hospitalization', 'style' => 'width:45%', 'placeholder' => '']) !!}
                                        <span class="help-block">
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                            </span>
                                    </div>
                                    <div style="display:inline;" class="hospitalization_days">
                                        <label class="required">Hospitalization Days</label>
                                        <div class="input-group" style="width: 40%;">
                                            {!! Form::text('hospitalization_days' . $i, (($treatment->disabilities()->where("disability_state_checklist_id", 1)->count()) ? $treatment->disabilities()->where("disability_state_checklist_id", 1)->first()->pivot->days : null), ['placeholder' => '', 'class' => 'form-control hospitalization_days_input']) !!}
                                            <span class="input-group-addon"><i class="icon fa fa-hospital-o"></i></span>
                                        </div>
                                        <span class="help-block">
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                            </span>
                                    </div>

                                </div>
                            </div>
                            {{--light duty days--}}
                            <div class="fileld-layout">
                                <div class="form-group">
                                    <div style="display:inline;">
                                        <label class="required">Has Light Duty?</label>
                                        {!! Form::select('has_ld' . $i, [1 => 'Yes', 0 => 'No'], $treatment->has_ld, ['class' => 'form-control search-select has_ld', 'style' => 'width:45%', 'placeholder' => '']) !!}
                                        <span class="help-block">
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                            </span>
                                    </div>
                                    <div style="display:inline;" class="light_duty_days">
                                        <label class="required">Light Duty (LD) Days</label>
                                        <div class="input-group" style="width: 40%;">
                                            {!! Form::text('light_duty_days' . $i, (($treatment->disabilities()->where("disability_state_checklist_id", 3)->count()) ? $treatment->disabilities()->where("disability_state_checklist_id", 3)->first()->pivot->days : null), ['placeholder' => '', 'class' => 'form-control light_duty_days_input']) !!}
                                            <span class="input-group-addon"><i class="icon fa fa-sign-language"></i></span>
                                        </div>
                                        <span class="help-block">
                                <small class="form-text text-muted" style="width:100% !important;"></small>
                            </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-1">
                            @if ($count > 0)
                                <span class="pull-right">
                        <a href="#" id="{!! $i !!}" class="remove_treatment_entry" data-toggle='tooltip' data-html='true' title='Remove medical treatment entry'><i class="icon fa fa-2x fa-remove" aria-hidden="true" style="color:darkred;"></i></a>
                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div>&nbsp;</div>
            </div>

            {{--headers health state--}}
            <div class="row">
                <div class="col-md-12 light_grey_bg">
                    <div class="form-inline" >
                        {{--Health state checklist--}}
                        <div class="col-md-3" align="left">
                            <div class="form-group">
                                <label> @lang('labels.backend.claim.health_state_checklist')</label>
                            </div>
                        </div>
                        {{--state--}}
                        <div class="col-md-2" align="center">
                            <div class="form-group">
                                <label class="required"> @lang('labels.general.state')</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @php
                $health_state_vals = $treatment->health_states ?: "{}";
                $health_state_checklists_vals = json_decode($health_state_vals, true);
            @endphp

            {{--loop all $health_state_checklists--}}
            @foreach ($health_state_checklists as $health_state_checklist)
                <div>&nbsp;</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-inline" >
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label( 'checklist', $health_state_checklist->name, [ 'id'=> 'checklist'])!!}
                                </div>
                            </div>
                            {{--feedback--}}
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!!  Form::select('health_state' . $health_state_checklist->id . $i , ['0' => 'No', '1' => 'Yes'],  $health_state_checklists_vals[$health_state_checklist->id] ?? NULL, ['class' => 'form-control search-select health_state' ,'placeholder'=> '', 'style'=>'width:120px']) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>
    @php
        $count += 1;
    @endphp
@endforeach
