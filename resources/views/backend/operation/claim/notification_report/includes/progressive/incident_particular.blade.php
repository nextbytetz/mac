<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h6><b><span class="light_dark_color">Incident Particulars</span></b></h6></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{--<div class="light_grey_bg">&nbsp;</div>--}}
        <div class="light_grey_bg" style="padding: 5px;">
            <div class="underline" style="font-weight: lighter;">Checklist Score&nbsp;:&nbsp;<span style="font-weight: normal;">{!! $incident->checklist_score_status !!}</span></div>
            <div class="underline" style="font-weight: lighter;">Checklist User&nbsp;:&nbsp;<span style="font-weight: normal;"><b>{{ $incident->checklistUser->name }}</b> </span></div>
            <div class="underline" style="font-weight: lighter;">Source&nbsp;:&nbsp;<span style="font-weight: normal;"><b>{!! $incident->source_formatted !!}</b> </span></div>
        </div>
    </div>
</div>