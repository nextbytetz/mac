@php
    $update_source = $update_source ?? 1;
    if ($update_source == 1) {
        $death = $incident->death;
    } elseif ($update_source == 2) {
        if ($incident->death_claim && in_array($incident->incident_type_id, [1,2])) {
            $death = json_decode($incident->death_claim);
        } elseif ($incident->death) {
            $death = $incident->death;
        } else {
            $death = NULL;
        }
    }
    if ($death) {
        $death_certificate_no = $death->death_certificate_no;
        $burial_permit_no = $death->burial_permit_no;
        $death_incident_exposure_cv_id = $death->death_incident_exposure_cv_id;
        $death_place = $death->death_place;
        $death_date = $death->death_date;
        $death_incident_date = $death->incident_date;
        $disease_cause_cv_id = $death->disease_cause_cv_id;
        $accident_cause_cv_id = $death->accident_cause_cv_id;
        $death_cause_id = $death->death_cause_id;
    } else {
        $death_certificate_no = NULL;
        $burial_permit_no = NULL;
        $death_incident_exposure_cv_id = NULL;
        $death_place = NULL;
        $death_date = NULL;
        $death_incident_date = NULL;
        $disease_cause_cv_id = NULL;
        $accident_cause_cv_id = NULL;
        $death_cause_id = NULL;
    }
    /*dd($death);*/
@endphp
<div class="row">
    {!! Form::hidden("update_source", $update_source) !!}
    <div class="col-md-4" style="padding-right:20px; border-right: 1px solid #ddd;">
        {{--cause of death--}}
        <div class="fileld-layout">
            <label class="required">Cause of Death</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('death_cause_id', $death_causes, $death_cause_id, ['class' => 'search-select', 'style' => 'width:100%', 'id' => 'death_cause_id', 'placeholder' => '']) !!}
                </div>
                <span class="help-block"></span>
            </div>
        </div>
        {{--cause of accident--}}
        <div class="fileld-layout" style="display: none;" id="accident_cause">
            <label class="required">Cause of Accident</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('accident_cause_cv_id', $accident_causes, $accident_cause_cv_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                </div>
                <span class="help-block"></span>
            </div>
        </div>
        {{--cause of disease--}}
        <div class="fileld-layout" style="display: none;" id="disease_cause">
            <label class="required">Cause of Disease</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('disease_cause_cv_id', $disease_causes, $disease_cause_cv_id, ['class' => 'search-select', 'style' => 'width:100%', 'style' => 'width:100%', 'placeholder' => '']) !!}
                </div>
                <span class="help-block"></span>
            </div>
        </div>
        {{--Incident Date--}}
        <div class="fileld-layout">
            <label id="incident_date_title">Incident Date</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('death_incident_date', $death_incident_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;"></small>
                    </span>
            </div>
        </div>
        {{--date of death--}}
        <div class="fileld-layout">
            <label class="required">Date of Death</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('death_date', $death_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block">
                        {{--<span>Date when death occurred</span>--}}
                    <small class="form-text text-muted" style="width:100% !important;">Date when death occurred</small>
                    </span>
            </div>
        </div>
        {{--reporting date--}}
        <div class="fileld-layout">
            <label class="required">Reporting Date</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('reporting_date', $incident->reporting_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block">
                        {{--<span>Date when accident occurred</span>--}}
                    <small class="form-text text-muted" style="width:100% !important;">Date of Reporting to Employer</small>
                    </span>
            </div>
        </div>
        {{--date of hire--}}
        {{--<div class="fileld-layout">
            <label class="required">Date of Hire</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('datehired', $incident->datehired, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block">
                    <small class="form-text text-muted" style="width:100% !important;">Date hired to the current employer</small>
                    </span>
            </div>
        </div>--}}
        {{--date last worked--}}
        @if(0)
            {{--<div class="fileld-layout">
                <label class="required">Date Last Worked</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('last_work_date', $incident->last_work_date, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Last day worked before the death</small>
                    </span>
                </div>
            </div>--}}
        @else
            {!! Form::hidden("last_work_date", $incident->last_work_date) !!}
        @endif

    </div>
    <div class="col-md-4" style="padding-right:20px; border-right: 1px solid #ddd;">
        {{--paid salary before incident--}}
        @if(0)
            {{--<div class="fileld-layout">
                <label class="required">Paid Salary?</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('paid_month_before', [1 => 'Yes', 0 => 'No'], $incident->paid_month_before, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">If salary was paid in the last month before death</small>
                    </span>
                </div>
            </div>--}}
            {{--Gross Salary Per Month--}}
            {{--<div class="fileld-layout">
                <label>Gross Salary</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('monthly_earning_checklist', $incident->monthly_earning_checklist, ['placeholder' => '', 'class' => 'form-control']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-money"></i></span>
                    </div>
                    <span class="help-block">
                        <small class="form-text text-muted" style="width:100% !important;">Gross Salary Per Month before the death</small>
                    </span>
                </div>
            </div>--}}
        @else
            {!! Form::hidden("paid_month_before", 1) !!}
            {!! Form::hidden("monthly_earning_checklist", $incident->monthly_earning) !!}
        @endif


        <!--Region-->
        <div class="fileld-layout">
            <label class="required">Notification Region</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('region_id', $regions, $region_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                    <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                </div>
            </div>
        </div>
        <!--District-->
        <div class="fileld-layout">
            <label class="required">Notification District</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('district_id', $districts, $district_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        {{--death incident exposure--}}
        <div class="fileld-layout">
            <label>Death Incident Exposure</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('death_incident_exposure_cv_id', $death_incident_exposures, $death_incident_exposure_cv_id, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                </div>
                <span class="help-block"></span>
            </div>
        </div>
        {{--Burial Permit No--}}
        <div class="fileld-layout">
            <label>Burial Permit No</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('burial_permit_no', $burial_permit_no, ['placeholder' => '', 'class' => 'form-control']) !!}
                    {{--<span class="input-group-addon"><i class="icon fa fa-money"></i></span>--}}
                </div>
                <span class="help-block">
                    <small class="form-text text-muted" style="width:100% !important;"></small>
                </span>
            </div>
        </div>
        {{--Death Certificate No--}}
        <div class="fileld-layout">
            <label>Death Certificate No</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('death_certificate_no', $death_certificate_no, ['placeholder' => '', 'class' => 'form-control']) !!}
                    {{--<span class="input-group-addon"><i class="icon fa fa-money"></i></span>--}}
                </div>
                <span class="help-block">
                    <small class="form-text text-muted" style="width:100% !important;"></small>
                </span>
            </div>
        </div>
        {{--Death Certificate No--}}
        <div class="fileld-layout">
            <label>Death Place</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('death_place', $death_place, ['placeholder' => '', 'class' => 'form-control']) !!}
                    {{--<span class="input-group-addon"><i class="icon fa fa-money"></i></span>--}}
                </div>
                <span class="help-block">
                    <small class="form-text text-muted" style="width:100% !important;"></small>
                </span>
            </div>
        </div>
    </div>
</div>