@if ($incident->progressive_stage >= 3)
    @if (in_array($incident->incident_type_id, [1,4]))
        {{--Accident--}}
        @include("backend/operation/claim/notification_report/includes/progressive/checklist/accident/summary")
    @endif
    @if (in_array($incident->incident_type_id, [2,5]))
        {{--Disease--}}
        @include("backend/operation/claim/notification_report/includes/progressive/checklist/disease/summary")
    @endif
    @if (in_array($incident->incident_type_id, [3,4,5]))
        {{--Death--}}
        @include("backend/operation/claim/notification_report/includes/progressive/checklist/death/summary")
    @endif
@else
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Nothing to show.</strong> This notification has not completed the investigation report..
        </div>
    </div>
@endif