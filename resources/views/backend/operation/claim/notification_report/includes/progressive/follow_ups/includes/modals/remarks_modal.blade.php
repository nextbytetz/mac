<div class="modal hide fade" id="reverseModal" role="dialog" aria-labelledby="reverseModal" aria-hidden="true">
	<div class="modal-dialog white_modal" role="document">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">
				<i class="fa fa-list"></i>&nbsp;&nbsp;Kindly describe reversed reason..</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-content">

				<form id="reverse_form" class="form-group">
					<textarea class="form-control" id="reverse_reason"></textarea>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm site-btn btn-secondary" data-dismiss="modal">@lang("buttons.general.close")</button>
				<button type="submit" class="btn btn-sm btn-danger site-btn btn-submit" id="reverse_button">Reverse</button>
			</div>
		</div>
	</div>