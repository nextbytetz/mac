
{{--Employement history table--}}
{{--<div>&nbsp;</div>--}}
<div class = "row" id ="employment-history-div">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        <table class="display" cellspacing="0" width="100%" id ="employment-history-table">
            <thead>
            <tr>
                <th>@lang('labels.general.job_title')</th>
                <th>@lang('labels.general.department')</th>
                <th>@lang('labels.backend.table.receipt.employer')</th>
                {{--<th>@lang('labels.general.insurance')</th>--}}
                <th>@lang('labels.backend.table.from_date')</th>
                <th>@lang('labels.backend.table.to_date')</th>
            </tr>
            </thead>

        </table>
    </div>
</div>


@push('employment-history-script-end')

<script  type="text/javascript">

        $(function() {
            var url = "{!! url("/") !!}";
            $('#employment-history-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                info: false,
                paging: false,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employee.get_employment_histories', $notification_report->employee_id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'job_title_id' , name: 'job_title_id' },
                    { data: 'department', name: 'department' },
                    { data: 'employer', name: 'employer' },
//                    { data: 'insurance', name: 'insurance' },
                    { data: 'from_date', name: 'from_date'},
                    { data: 'to_date', name: 'to_date'}
                ]



            } );

        });

</script>;

@endpush