{{--Incident Not Death--}}

<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            {!! Form::model($notification_report,['route' => ['backend.claim.notification_report.update_bank_details',$notification_report->id],'method'=>'put',
'id' => 'update']) !!}


        </div>

        {{--Employee=========backed up===--}}
        {{--@if(count($notification_report->medicalExpenses()->where('member_type_id',2)))--}}
        <div class="col-md-12" >
            <legend class="grey_modal" id="employee" >@lang('labels.backend.compliance.employee')</legend>
            <div>&nbsp;</div>

            <table class="table table-striped table-bordered"  id="employee-bank-details-div" style="width:100%">
                <tbody>
                {{--Header--}}
                <tr>
                    <th>@lang('labels.general.name')</th>
                    <th>@lang('labels.backend.finance.receipt.bank')</th>
                    <th>@lang('labels.backend.table.branch')</th>
                    <th>@lang('labels.backend.table.accountno')</th>
                </tr>
                {{--employee details--}}
                {{--@foreach($notification_report->medicalExpenses()->where('member_type_id',2) as $medical_expense)--}}
                    <tr>

                        <td>{!! $notification_report->employee->name  !!}</td>
                        {!! Form::hidden('employee_id', $notification_report->employee->id  ) !!}


                        <td>{!!  Form::select('employee_bank_id', $banks, ($notification_report->employee->bankBranch()->count()) ? $notification_report->employee->bankBranch->bank->id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'employee_bank_select'])  !!}</td>
                        {!! $errors->first('employee_bank_id', '<span class="help-block label label-danger">:message</span>') !!}



                        <td>      {!! Form::select('employee_bank_branch_id', ($notification_report->employee->bank_branch_id) ? $bank_branches : [], $notification_report->employee->bank_branch_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'employee_bank_branch_select' ]) !!}
                            {!! $errors->first('employee_bank_branch_id', '<span class="help-block label label-danger">:message</span>') !!}
</td>

                                 <td>{!! Form::input( 'text','employee_accountno', ($notification_report->employee->accountno) ? $notification_report->employee->accountno : ' ', ['class' => 'form-control', 'id'=> 'accountno']) !!}
                            {!! $errors->first('employee_accountno', '<span class="help-block label label-danger">:message</span>') !!}</td>

                    </tr>


                {{--@endforeach--}}

                </tbody></table>
            </div>
{{--@endif--}}


        {{--employer============--}}
        {{--@if(count($notification_report->medicalExpenses()->where('member_type_id',1)))--}}
        <div class="col-md-12">
            <legend class="grey_modal"  id="employer">@lang('labels.backend.compliance.employer')</legend>
            <div>&nbsp;</div>
            <table class="table table-striped table-bordered" id="employer-bank-details-div"  style="width:100%">
                <tbody>
                {{--Header--}}
                <tr>
                    <th>@lang('labels.general.name')</th>
                    <th>@lang('labels.backend.finance.receipt.bank')</th>
                    <th>@lang('labels.backend.table.branch')</th>
                    <th>@lang('labels.backend.table.accountno')</th>
                </tr>
                {{--employee details--}}
                {{--@foreach($notification_report->medicalExpenses()->where('member_type_id',1) as $medical_expense)--}}
                    <tr>

                        <td>{!! ($notification_report->employer()->count()) ? $notification_report->employer->name  : ' ' !!}</td>
                        {!! Form::hidden('employer_id', ($notification_report->employer()->count()) ? $notification_report->employer->id : ' '  ) !!}

                                               <th>{!!  Form::select('employer_bank_id', $banks, ($notification_report->employer()->count()) ? (($notification_report->employer->bankBranch) ? $notification_report->employer->bankBranch->bank->id : null) : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'employer_bank_select'])  !!}</th>
                        {!! $errors->first('employer_bank_id', '<span class="help-block label label-danger">:message</span>') !!}

                                      <td>      {!! Form::select('employer_bank_branch_id', ($notification_report->employer()->count()) ? (( $notification_report->employer->bank_branch_id) ? $bank_branches : []) : [],  ($notification_report->employer()->count()) ? $notification_report->employer->bank_branch_id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'employer_bank_branch_select' ]) !!}
                            {!! $errors->first('employer_bank_branch_id', '<span class="help-block label label-danger">:message</span>') !!}
                        </td>

                                 <td>{!! Form::input( 'text','employer_accountno', ($notification_report->employer()->count()) ? $notification_report->employer->accountno : ' ' , ['class' => 'form-control', 'id'=> 'accountno']) !!}
                            {!! $errors->first('employer_accountno', '<span class="help-block label label-danger">:message</span>') !!}</td>

                    </tr>


                {{--@endforeach--}}

                </tbody></table>
        </div>
{{--@endif--}}


        {{--insurance============--}}
        @if(($notification_report->medicalExpenses()->where('member_type_id',3)->count()))
        <div class="col-md-12">
            <legend class="grey_modal" id="insurance" >@lang('labels.general.insurance')</legend>
            <div>&nbsp;</div>
            <table class="table table-striped table-bordered"  id="insurance-bank-details-div" style="width:100%">
                <tbody>
                {{--Header--}}
                {{--employee details--}}
                @foreach($notification_report->medicalExpenses()->where('member_type_id',3)->get() as $medical_expense)
                <tr>
                    <th>@lang('labels.general.name')</th>
                    <th>@lang('labels.backend.finance.receipt.bank')</th>
                    <th>@lang('labels.backend.table.branch')</th>
                    <th>@lang('labels.backend.table.accountno')</th>
                </tr>
                <tr>
                        <td>{!! $medical_expense->insurance->name  !!}</td>
                        {!! Form::hidden('insurance_id', $medical_expense->insurance->id  ) !!}

                        <td>{!!  Form::select('insurance_bank_id'.$medical_expense->insurance->id , $banks, ($medical_expense->insurance->bankBranch()->count()) ? $medical_expense->insurance->bankBranch->bank_id : null , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select insurance', 'id'=> 'insurance_bank_select'.$medical_expense->insurance->id ])  !!}</td>
                        {!! $errors->first('insurance_bank_id'.$medical_expense->insurance->id , '<span class="help-block label label-danger">:message</span>') !!}

                        <td>      {!! Form::select('insurance_bank_branch_id'.$medical_expense->insurance->id , ($medical_expense->insurance->bank_branch_id) ? $bank_branches : [], $medical_expense->insurance->bank_branch_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'insurance_bank_branch_select'.$medical_expense->insurance->id  ]) !!}
                            {!! $errors->first('insurance_bank_branch_id'.$medical_expense->insurance->id , '<span class="help-block label label-danger">:message</span>') !!}
                        </td>

                        <td>{!! Form::input( 'text','insurance_accountno'.$medical_expense->insurance->id , ($medical_expense->insurance) ? $medical_expense->insurance->accountno : ' ', ['class' => 'form-control insurance', 'id'=> 'accountno']) !!}
                            {!! $errors->first('insurance_accountno'.$medical_expense->insurance->id , '<span class="help-block label label-danger">:message</span>') !!}</td>


                    </tr>


                @endforeach

                </tbody></table>








        </div>
@endif





        {{------------WCF============--}}
        @if(($notification_report->medicalExpenses()->where('member_type_id',1)->where('resource_id',4038)->count()))
        <div class="col-md-12">
            <legend class="grey_modal"  id="wcf">WCF</legend>
            <div>&nbsp;</div>
            <table class="table table-striped table-bordered" id="wcf-bank-details-div"  style="width:100%">
                <tbody>
                {{--Header--}}
                @foreach($notification_report->medicalExpenses()->where('member_type_id',1)->where('resource_id',4038)->get() as $medical_expense)
                <tr>
                    <th>@lang('labels.general.name')</th>
                    <th>@lang('labels.backend.finance.receipt.bank')</th>
                    <th>@lang('labels.backend.table.branch')</th>
                    <th>@lang('labels.backend.table.accountno')</th>
                </tr>

                <tr>

                    <td>{!! ($medical_expense->employer()->count()) ? $medical_expense->employer->name  : ' ' !!}</td>
                    {!! Form::hidden('wcf_id', ($medical_expense->employer()->count()) ? $medical_expense->employer->id : ' '  ) !!}

                    <th>{!!  Form::select('wcf_bank_id', $banks, ($medical_expense->employer()->count()) ? (($medical_expense->employer->bankBranch) ? $medical_expense->employer->bankBranch->bank->id : null) : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select wcf', 'id'=> 'wcf_bank_select'])  !!}</th>
                    {!! $errors->first('wcf_bank_id', '<span class="help-block label label-danger">:message</span>') !!}

                    <td>      {!! Form::select('wcf_bank_branch_id', ($medical_expense->employer()->count()) ? (( $medical_expense->employer->bank_branch_id) ? $bank_branches : []) : [],  ($medical_expense->employer()->count()) ? $medical_expense->employer->bank_branch_id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'wcf_bank_branch_select' ]) !!}
                        {!! $errors->first('wcf_bank_branch_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </td>

                    <td>{!! Form::input( 'text','wcf_accountno', ($medical_expense->employer()->count()) ? $medical_expense->employer->accountno : ' ' , ['class' => 'form-control', 'id'=> 'accountno']) !!}
                        {!! $errors->first('wcf_accountno', '<span class="help-block label label-danger">:message</span>') !!}</td>

                </tr>


                @endforeach

                </tbody></table>
        </div>
        @endif













        {{--Dependents--}}

        @if(($notification_report->employee->dependents()->where(function ($query){
    $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
})->count()))
            <div class="col-md-12">
                <legend class="grey_modal" id="dependent" >@lang('labels.general.dependent')</legend>
                <div>&nbsp;</div>
                <table class="table table-striped table-bordered"  id="dependent-bank-details-div" style="width:100%">
                    <tbody>
                    {{--Header--}}
                    {{--dependent details--}}
                    <tr>
                        <th>@lang('labels.general.name')</th>
                        <th>@lang('labels.backend.finance.receipt.bank')</th>
                        <th>@lang('labels.backend.table.branch')</th>
                        <th>@lang('labels.backend.table.accountno')</th>
                    </tr>
                    @foreach($notification_report->employee->dependents()->where(function ($query){
    $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
})->get() as $dependent)

                        <tr>
                            <td>{!! $dependent->name  !!}</td>
                            {!! Form::hidden('dependent_id', $dependent->id  ) !!}

                            <td>{!!  Form::select('dependent_bank_id'.$dependent->id , $banks, ($dependent->bank_branch_id) ? $dependent->bankBranch->bank->id : null, ['style' => 'width:100%', 'placeholder' => '',
                            'class' =>
                             'form-control search-select dependent', 'id'=> 'dependent_bank_select'.$dependent->id ])  !!}</td>
                            {!! $errors->first('dependent_bank_id'.$dependent->id , '<span class="help-block label label-danger">:message</span>') !!}

                            <td>      {!! Form::select('dependent_bank_branch_id'.$dependent->id , ($dependent->bank_branch_id) ? $bank_branches : [],  ($dependent->bank_branch_id) ? $dependent->bank_branch_id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'dependent_bank_branch_select'.$dependent->id  ]) !!}
                                {!! $errors->first('dependent_bank_branch_id'.$dependent->id , '<span class="help-block label label-danger">:message</span>') !!}
                            </td>

                            <td>{!! Form::input( 'text','dependent_accountno'.$dependent->id , ($dependent->accountno) ? $dependent->accountno : ' ', ['class' => 'form-control dependent', 'id'=> 'accountno']) !!}
                                {!! $errors->first('dependent_accountno'.$dependent->id , '<span class="help-block label label-danger">:message</span>') !!}</td>


                        </tr>


                    @endforeach

                    </tbody></table>








            </div>
        @endif

        @if($check_workflow == 0 || $check_if_is_level1_pending == 1)

        {{--Buttons--}}
        <div class="row">
            <div class="col-md-9" class="form-inline" >
                <div class="element-form">
                    <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                    <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                        <div class="pull-right">

                            {!! link_to_route('backend.claim.notification_report.profile',trans('buttons.general.cancel'), [$notification_report->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
            </br>
        @endif
        {!! Form::close() !!}
    </div>
</div>


@push('bank-details-script-end')

<script  type="text/javascript">
    $(function() {

        $("#employee").click(function(){
            $("#employee-bank-details-div").slideToggle("slow");
        });

        $("#employer").click(function(){
            $("#employer-bank-details-div").slideToggle("slow");
        });
        $("#insurance").click(function(){
            $("#insurance-bank-details-div").slideToggle("slow");
        });
        $("#dependent").click(function(){
            $("#dependent-bank-details-div").slideToggle("slow");
        });

        $("#wcf").click(function(){
            $("#wcf-bank-details-div").slideToggle("slow");
        });

        $('#employee_bank_select').on('change', function (e) {
            $("#spin2").show();
            var bank_id = e.target.value;
            $.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {
                $('#employee_bank_branch_select').empty();
                $("#employee_bank_branch_select").select2("val", "");
                $('#employee_bank_branch_select').html(data);
                $("#spin2").hide();
            });
        });

        $('#employer_bank_select').on('change', function (e) {
            $("#spin2").show();
            var bank_id = e.target.value;
            $.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {
                $('#employer_bank_branch_select').empty();
                $("#employer_bank_branch_select").select2("val", "");
                $('#employer_bank_branch_select').html(data);
                $("#spin2").hide();
            });
        });

        $('.insurance').each(function() {
            var $id = this.id;
            var $insurance_id = $id.substr(21);
            $('#insurance_bank_select'+ $insurance_id).on('change', function (e) {
                $("#spin2").show();
                var bank_id = e.target.value;
                $.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {
                    $('#insurance_bank_branch_select'+$insurance_id).empty();
                    $("#insurance_bank_branch_select"+$insurance_id).select2("val", "");
                    $('#insurance_bank_branch_select'+$insurance_id).html(data);
                    $("#spin2").hide();
                });
            });
        });


        $('.dependent').each(function() {
            var $id = this.id;
            var $dependent_id = $id.substr(21);
            $('#dependent_bank_select'+ $dependent_id).on('change', function (e) {
                $("#spin2").show();
                var bank_id = e.target.value;
                $.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {
                    $('#dependent_bank_branch_select'+$dependent_id).empty();
                    $("#dependent_bank_branch_select"+$dependent_id).select2("val", "");
                    $('#dependent_bank_branch_select'+$dependent_id).html(data);
                    $("#spin2").hide();
                });
            });
        });


        $('.wcf').each(function() {
            var $id = this.id;
            var $wcf_id = $id.substr(21);
            $('#wcf_bank_select'+ $wcf_id).on('change', function (e) {
                $("#spin2").show();
                var bank_id = e.target.value;
                $.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {
                    $('#wcf_bank_branch_select'+$wcf_id).empty();
                    $("#wcf_bank_branch_select"+$wcf_id).select2("val", "");
                    $('#wcf_bank_branch_select'+$wcf_id).html(data);
                    $("#spin2").hide();
                });
            });
        });



    });


</script>;

@endpush