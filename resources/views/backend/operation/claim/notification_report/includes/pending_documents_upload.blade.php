

{{--sidebar notification summary table--}}

@if(count($pending_documents))
<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.claim.pending_documents')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">


            <?php
            $i = 1;
            ?>

            @foreach($pending_documents as $pending_document)

                <tr>
                    <td style="padding:5px; width:40px">{!! $i   !!}</td>
                    <td>{!! $pending_document->name !!}</td>

                </tr>

                <?php
                $i++;
                ?>

            @endforeach



        </table>
    </div>
</div>


@endif



@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush