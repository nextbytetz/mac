

{{--sidebar notification / incident summary table--}}


<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Rejection Appeal Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">

            <tr>
                <td style="padding-left: 5px" width="130px">Approval Reason:</td>
                <td><b>{!! Form::label( 'appeal_reason', $notification_report->notificationReportAppeal->reason, [ 'id'=> 'appeal_reason']) !!}</b></td>
            </tr>
            <tr>
                <td style="padding-left: 5px" width="130px">Approved by:</td>
                <td><b>{!! $notification_report->notificationReportAppeal->userApproved->username
                        !!}</b>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 5px" width="130px">Approved date:</td>
                <td><b>{!! short_date_format($notification_report->notificationReportAppeal->created_at)
                        !!}</b>
                </td>
            </tr>

        </table>
    </div>
</div>






@push('sidebar-script-end')

    <script  type="text/javascript">

    </script>;

@endpush