
{{--investigation report / feedback--}}
<legend class="grey_modal" >@lang('labels.backend.legend.investigation_report')</legend>

<div class="col-md-12" >
    <div class="pull-right">
        @if($check_workflow == 0 || $check_if_is_level1_pending == 1)
            {{--Assign Investigators--}}
      {{--       <span>
        <a href="{!! route('backend.claim.notification_report.assign_investigators',  $notification_report->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-user-plus"></i>&nbsp;@lang('buttons.backend.claim.assign_investigator')</a>
        </span> --}}
        @endif
        {{--Investigate--}}
        {{--<span>--}}
        {{--<a href="{!! route('backend.claim.notification_report.profile', $notification_report->id) !!}"  class="btn  site-btn save_button" ><i class="icon fa fa-search"></i>&nbsp;@lang('labels.general.investigate')</a>--}}
        {{--</span>--}}

    </div>
</div>


<table class="display" cellspacing="0" width="100%" id ="investigation-report-table">
    <thead>
    <tr>

        <th style="width: 50%;">@lang('labels.general.question')</th>
        <th style="width: 50%;">@lang('labels.general.feedback')</th>

    </tr>
    </thead>

</table>

@push('investigation-report-script-end')

<script  type="text/javascript">
    $("#tab_3_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#investigation-report-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                paging: false,
                info:false,
                "bSort": false,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.notification_report.get_investigation_feedbacks', $notification_report->id) !!}',
                    type : 'get'

                },
                columns: [
                    { data: 'investigation_question_id' , name: 'investigation_question_id' },
                    { data: 'feedback' , name: 'feedback' }
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url + "/claim/notification_report/investigation_report/"  + aData['notification_report_id'] + "/edit";

                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }




            } );
        });
    });
</script>;

@endpush