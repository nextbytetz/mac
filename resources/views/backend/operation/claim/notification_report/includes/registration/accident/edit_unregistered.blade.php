{!! Form::model($incident, ['route' => ['backend.claim.notification_report.update_progressive', $incident->id ], 'method' => 'put', 'id' => 'create_notification',  'name' => 'create_notification']) !!}
{!! Form::hidden('this_date', getTodayDate(), ['class' => 'this_date']) !!}
{!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' => 'wcf_date']) !!}
{!! Form::hidden('claim_start_date', getClaimStartDate(), ['class' => 'claim_date']) !!}
{!! Form::hidden('incident_type_id', $incident_type->id) !!}
{!! Form::hidden('employee_id', 0) !!}
{!! Form::hidden('employer_id', $employer) !!}

<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5"><strong> Uregistered Employee</strong></span>
    <span class="navbar-brand mb-0 h5">@lang('labels.backend.table.member_#'):<small class="underline"> ................</small></span>
    <span class="navbar-brand mb-0 h5">Incident Type:<small class="underline"> Accident</small></span>
</nav>
<legend></legend>
<br/>

<div class="image-blockquote">
    {{--main contants--}}
    <div class="row">

        <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">
            {{--Employee Name--}}
            <div class="fileld-layout">
                <label class="required">Employee Name</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('employee_name', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
                    </div>
                    {!! $errors->first('employee_name', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--Employer Name--}}
            <div class="fileld-layout">
                <label class="required">Employer Name</label>
                <div class="form-group">
                    @if ($employer)
                        <label class="underline" style="font-size: 14px;font-weight: bold;">{{ $employer_name }}</label>
                        {!! Form::hidden("employer_name", $employer_name) !!}
                    @else
                        <div class="input-group">
                            {!! Form::text('employer_name', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
                        </div>
                        {!! $errors->first('employer_name', '<span class="help-block label label-danger">:message</span>') !!}
                    @endif
                </div>
            </div>
            {{--accident type--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.accident_type')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('accident_type_id', $accident_types, [$accident->accident_type_id], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                    </div>
                    {!! $errors->first('accident_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--accident date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.accident_date')</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('incident_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('incident_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--reporting date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.reporting_date')</label>&nbsp;
                {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.reporting_date_description')"></i>--}}
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('reporting_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">@lang('labels.backend.claim.reporting_date_description')</small>
                    {!! $errors->first('reporting_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--receipt date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.receipt_date')</label>&nbsp;
                {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.receipt_date_description')"></i>--}}
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('receipt_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">@lang('labels.backend.claim.receipt_date_description')</small>
                    {!! $errors->first('receipt_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--Accident time--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.accident_time')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('accident_time', $accident->accident_time, ['class' => 'form-control input-radius timepicker', 'style' => 'width:30%']) !!}
                    </div>
                    {!! $errors->first('accident_time', '<span class="help-block label label-danger">:message</span>') !!}
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            {{-- start: incident region--}}
            <div class="fileld-layout">
                <label class="required">Incident Region &nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Region which incident occurred"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('region_id', $regions, $incident->district->region->id, ['style' => 'width:100%', 'placeholder' => '', 'class' => 'form-control search-select', 'id'=> 'region_id']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Region which incident occurred</small>
                    {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- start : incident district--}}
            <div class="fileld-layout">
                <label class="required">Incident District</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('district_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control','id'=> 'district_id']) !!}
                    </div>
                    {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--accident place--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.accident_place')&nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Place of accident (street, ward, city)"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'accident_place', $accident->accident_place, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Place of accident (street, ward, city)</small>
                    {!! $errors->first('accident_place', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--activity_performed--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.activity_performed')&nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Specific activity the employee was performing at the time of accident"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'activity_performed', $accident->activity_performed, ['class' => 'form-control editor',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Specific activity the employee was performing at the time of accident</small>
                    {!! $errors->first('activity_performed', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- incident description --}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.general.descriptions')&nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Briefly describe sequence of events and specify object which directly produced/caused the injury"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'description', $accident->description, ['class' => 'form-control editor',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Briefly describe sequence of events and specify object which directly produced/caused the injury</small>
                    {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--<div class="incident_witness">
                @include("backend.includes.registration.claim.witness.create")
            </div>--}}
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.claim.notification_report.profile',trans('buttons.general.cancel'), [$incident->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button(trans('buttons.general.crud.update'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>
</div>


{!! Form::close() !!}
{{--</section>--}}