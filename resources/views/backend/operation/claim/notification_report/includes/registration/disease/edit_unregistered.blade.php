{!! Form::model($incident, ['route' => ['backend.claim.notification_report.update_progressive', $incident->id ], 'method' => 'put', 'id' => 'create_notification','name' => 'create_notification']) !!}
{!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
{!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
{!! Form::hidden('claim_start_date', getClaimStartDate(), ['class' =>'claim_date']) !!}
{!! Form::input( 'text','incident_type_id', $incident_type->id, ['class' => 'form-control', 'hidden'=> true]) !!}
{!! Form::hidden('employee_id', 0) !!}
{!! Form::hidden('employer_id', $employer) !!}

<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5"><strong> Unregistered Employee </strong></span>
    <span class="navbar-brand mb-0 h5">@lang('labels.backend.table.member_#'):<small class="underline"> ................</small></span>
    <span class="navbar-brand mb-0 h5">Incident Type:<small class="underline"> Disease </small></span>
</nav>
<legend></legend>
<br/>

<div class="image-blockquote">
    <div class="row">
        <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">
            {{--Employee Name--}}
            <div class="fileld-layout">
                <label class="required">Employee Name</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('employee_name', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
                    </div>
                    {!! $errors->first('employee_name', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--Employer Name--}}
            <div class="fileld-layout">
                <label class="required">Employer Name</label>
                <div class="form-group">
                    @if ($employer)
                        <label class="underline" style="font-size: 14px;font-weight: bold;">{{ $employer_name }}</label>
                        {!! Form::hidden("employer_name", $employer_name) !!}
                    @else
                        <div class="input-group">
                            {!! Form::text('employer_name', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
                        </div>
                        {!! $errors->first('employer_name', '<span class="help-block label label-danger">:message</span>') !!}
                    @endif
                </div>
            </div>
            {{--occupational disease name--}}
            <div class="fileld-layout">
                <label class="required">Occupational disease diagnosed</label>
                <div class="form-group">
                    <div class="input-group" style="width:100%;">
                        {!! Form::text('disease_name', $disease->name, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
                    </div>
                    {!! $errors->first('disease_name', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--diagnosis date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.diagnosis_date')</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('incident_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('incident_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--reporting date--}}
            <div class="fileld-layout">
                <label class="required">Reporting Date&nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Date of reporting disease to employer"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('reporting_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Date of reporting disease to employer</small>
                    {!! $errors->first('reporting_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--receipt date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.receipt_date')&nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.receipt_date_description')"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('receipt_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">@lang('labels.backend.claim.receipt_date_description')</small>
                    {!! $errors->first('receipt_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>


        </div>
        <div class="col-md-6">
            {{-- start: incident region--}}
            <div class="fileld-layout">
                <label class="required">Incident Region &nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Region which incident occured"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('region_id', $regions, $incident->district->region->id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Region which incident occured</small>
                    {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- start : incident district--}}
            <div class="fileld-layout">
                <label class="required">Incident District</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('district_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                    </div>
                    {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- incident description --}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.general.descriptions')&nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Briefly describe sequence of activities associated to the disease diagnosed"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'description', $disease->description, ['class' => 'form-control editor',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Briefly describe sequence of activities associated to the disease diagnosed</small>
                    {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--health service provider select--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.health_provider')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('health_provider_id', $health_provider, $disease->health_provider_id, ['style' => 'width:80%', 'placeholder' => '','class' => 'form-control search-select health-provider-select', 'id'=>'health_provider_select']) !!}
                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_health_service_provider']) !!}</span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Name of the Hospital where the diagnosis was established</small>
                    {!! $errors->first('health_provider_id', '<p class="help-block label label-danger">:message</p>') !!}
                </div>
            </div>
            {{--medical practitioner select--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.medical_practitioner')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('medical_practitioner_id', $medical_practitioner, $disease->medical_practitioner_id,  ['style' => 'width:80%', 'placeholder' => '','class' => 'form-control practitioner-select', 'id'=>'medical_practitioner_select']) !!}
                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_medical_practitioner']) !!}</span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Name medical practitioner who diagnosed the disease</small>
                    {!! $errors->first('medical_practitioner_id', '<p class="help-block label label-danger">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$incident->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}
{{--</section>--}}