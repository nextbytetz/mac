{!! Form::open(['route' => ['backend.claim.notification_report.store_new', $employee->id ], 'method'=>'post', 'id' => 'create', 'name'=>'store_new']) !!}
{!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
{!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
{!! Form::hidden('claim_start_date', getClaimStartDate(), ['class' =>'claim_date']) !!}
{!! Form::hidden('incident_type_id', $incident_type->id) !!}
{!! Form::hidden('employee_id', $employee->id,[]) !!}

<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5"><strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " .$employee->lastname) !!}</strong></span>
    <span class="navbar-brand mb-0 h5">@lang('labels.backend.table.member_#'): <small class="underline">{!! Form::label( 'reg_no', $employee->memberno) !!}</small></span>
    <span class="navbar-brand mb-0 h5">Incident Type:<small class="underline"> Death</small></span>
</nav>
<legend></legend>
<br/>

<div class="image-blockquote">
    <div class="row">
        <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">
            {{--death cause--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.death_cause')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('death_cause_id', $death_causes, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                    </div>
                    {!! $errors->first('death_cause_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--death date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.death_date')</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('incident_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('incident_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--reporting date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.reporting_date')</label>&nbsp;
                {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Date of reporting to the employer" ></i>--}}
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('reporting_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Date of reporting to the employer</small>
                    {!! $errors->first('reporting_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--receipt date--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.receipt_date')</label>&nbsp;
                {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="@lang('labels.backend.claim.receipt_date_description')"></i>--}}
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('receipt_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">@lang('labels.backend.claim.receipt_date_description')</small>
                    {!! $errors->first('receipt_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--activity_performed--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.activity_performed')</label>
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Specific activity the employee was performing at the time of accident"></i>--}}
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'activity_performed', null, ['class' => 'form-control editor',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Specific activity the deceased employee was performing when event/exposure occurred</small>
                    {!! $errors->first('activity_performed', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- incident description --}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.general.descriptions')&nbsp;</label>
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Briefly describe sequence of events and specify object which directly produced/caused the injury"></i>--}}
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'description', null, ['class' => 'form-control editor',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Briefly describe sequence of events /exposure and specify object/exposure which directly produced the accident/ disease that led to death</small>
                    {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
        <div class="col-md-6">
            {{-- start: death occurence (On premise / Out of Premise)--}}
            <div class="fileld-layout">
                <label class="required"> Death Occurrence &nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Location where death occurred"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('incident_occurrence_cv_id', $incident_occurrences, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Location where death occurred</small>
                    {!! $errors->first('incident_occurrence_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- start: incident region--}}
            <div class="fileld-layout">
                <label class="required">Incident Region &nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Region which incident occured"></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('region_id', $regions, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Region which incident occured</small>
                    {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- start : incident district--}}
            <div class="fileld-layout">
                <label class="required">Incident District</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('district_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                    </div>
                    {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--death place--}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.claim.death_place')&nbsp;
                    {{--<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Place where Death occurred (street, ward, city) "></i>--}}
                </label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea( 'death_place', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'autocomplete' => 'none']) !!}
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Place where Death occurred (street, ward, city)</small>
                    {!! $errors->first('death_place', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{-- Employer Id --}}
            <div class="fileld-layout">
                <label class="required">@lang('labels.general.employer')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('employer_id', $employers, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                    </div>
                    {!! $errors->first('employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--hired date--}}
            <div class="fileld-layout">
                <label>@lang('labels.backend.member.date_hired')</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('date_hired', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('date_hired', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            {{--health service provider select--}}
            <div class="fileld-layout">
                <label>@lang('labels.backend.claim.health_provider')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('health_provider_id', [], null, ['style' => 'width:80%', 'placeholder' => '','class' => 'form-control health-provider-select', 'id'=>'health_provider_select']) !!}
                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_health_service_provider']) !!}</span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Name of hospital where death was confirmed</small>
                    {!! $errors->first('health_provider_id', '<p class="help-block label label-danger">:message</p>') !!}
                </div>
            </div>
            {{--medical practitioner select--}}
            <div class="fileld-layout">
                <label>@lang('labels.backend.claim.medical_practitioner')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('medical_practitioner_id', [], null,  ['style' => 'width:80%', 'placeholder' => '','class' => 'form-control practitioner-select', 'id'=>'medical_practitioner_select']) !!}
                        <span style="width: 20%">&nbsp;{!! link_to('', "Add New", ['class' => 'btn btn-secondary btn-sm site-btn', 'data-toggle' => 'modal', 'data-target' => '#create_medical_practitioner']) !!}</span>
                    </div>
                    <small class="form-text text-muted" style="width:100% !important;">Name of Medical practitioner who confirmed death</small>
                    {!! $errors->first('medical_practitioner_id', '<p class="help-block label label-danger">:message</p>') !!}
                </div>
            </div>
            {{--start : next of kin information--}}
            @if (empty($employee->next_of_kin))
                <div class="next_of_kin">
                    {{--next of kin name--}}
                    <div class="fileld-layout">
                        <label>Name</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('next_of_kin', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
                            </div>
                            {!! $errors->first('next_of_kin', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                    {{--next of kin phone--}}
                    <div class="fileld-layout">
                        <label>Phone Number</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('next_of_kin_phone', null, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'none']) !!}
                            </div>
                            {!! $errors->first('next_of_kin_phone', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            @endif
            {{--end : next of kin information--}}

        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.compliance.employee.choose_incident_type', trans('buttons.general.cancel'), [$employee->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button(trans('buttons.general.submit'), ['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}