{{--Bottom tab navigation--}}

<div class = "row">
    <div class="col-md-12">
        <div class="basic_nav_pills nav_basic_tab">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#workflow" data-toggle="tab">@lang('labels.backend.legend.workflow')</a>
                </li>
            </ul>
            <div class="nav_tab_contain tab-content">
                <div id="workflow" class="nav_tab_pane tab-pane active in">
                    {{--main workflow tracks--}}
                    @include('backend.includes.workflow_table_header')
                </div>
            </div>
        </div>
    </div>
</div>