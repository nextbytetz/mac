<legend class="grey_modal" >@lang('labels.backend.claim.document_helper')</legend>
<br/>
{{--<div class="row">--}}
<div class="col-md-6 col-sm-6">
    <legend>{!! trans('labels.backend.claim.library') !!}</legend>
    <div id="folder-tree" class="nopadding"></div>

    <br>
    {{--Button - Reload Documents from e-office--}}
    <div class="pull-left">
        {{--<input type="submit" class="btn btn-success btn-sm" value="Reload from e-Office" />--}}
        {!! link_to_route('backend.claim.notification_report.reload_documents','Reload from e-Office', [$notification_report->id],[ 'class' => 'btn btn-success site-btn ', ]) !!}
    </div>

</div>

@if($check_workflow == 0 || $check_if_is_level1_pending == 1)
    <div class="col-md-6 col-sm-6">
        @if (env('SELF_DMS', 0))
            <legend>{!! trans('labels.backend.claim.upload') !!}</legend>
            {!! Form::open(['route' => ['backend.claim.notification_report.upload_documents', $notification_report->id], 'class' => 'upload_document', 'enctype' => 'multipart/form-data']) !!}
            <div class="fileld-layout">
                <br/>
                <label>@lang('labels.backend.claim.upload_selected')</label>
                <div class="form-group">
                    {!! Form::file('document_file') !!}
                    <span class="help-block">
                <p></p>
            </span>

                </div>

                <div class="progress-div">
                    <div class="progress-bar"></div>
                </div>

            </div>
            {!! Form::hidden('document_id', 0) !!}
            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.upload')" />
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <br/>
            <br/>
        @endif

        <div class="col-md-12">

            @include('backend.operation.claim.notification_report.includes.pending_documents_upload')
        </div>


    </div>
@endif
{{--</div>--}}

@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    <!-- Custom javascript files for this page -->

    <script>

        $("#tab_2_header").one("click", function(){
            $(function() {
                var $self_dms = {!! env('SELF_DMS') !!};
                var $folder = $('#folder-tree');

                /************************** start: Document Library JS ***************************/
                $folder.jstree({
                    'core': {
                        'data': {
                            'url': "{!! route('backend.claim.notification_report.documents_library', $notification_report->id) !!}",
                            'data': function (node) {
                                return {'id': node.id};
                            }
                        },
                        'check_callback': function (o, n, p, i, m) {
                            if (m && m.dnd && m.pos !== 'i') {
                                return false;
                            }
                            if (o === "move_node" || o === "copy_node") {
                                if (this.get_node(n).parent === this.get_node(p).id) {
                                    return false;
                                }
                            }
                            return true;
                        },
                        'force_text': true,
                        'themes': {
                            'responsive': true,
                            'variant': 'small',
                            'stripes': true
                        }
                    },
                    'sort': function (a, b) {
                        return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
                    },
                    'contextmenu': {
                        'items': function (node) {
                            var $menu = {};
                            // if (this.get_type(node) === "file") {
                            if($self_dms == 1){
                                //Get file from MAC file system.
                                $menu = {
                                    "download" : {
                                        "label" : "{!! trans("labels.general.download") !!}",
                                        "action" : function ($data) {
                                            {{--window.open( base_url + "/claim/notification_report/download_document/{!! $notification_report->id !!}/" + node.id);--}}
                                            window.open( base_url + "/claim/notification_report/download_document_general/{!! $notification_report->id !!}/" + node.id);
                                        }
                                    },
                                    "open" : {
                                        "label" : "{!! ucfirst(strtolower(trans("labels.general.open"))) !!}",
                                        "action" : function ($data) {
                                            {{--window.open(base_url + "/claim/notification_report/open_document/{!! $notification_report->id !!}/" + node.id, '_blank');--}}
                                            window.open(base_url + "/claim/notification_report/open_document_general/{!! $notification_report->id !!}/" + node.id, '_blank');
                                        }
                                    },
                                    {{--"delete" : {--}}
                                    {{--"label" : "{!! trans("labels.general.delete") !!}",--}}
                                    {{--"action" : function ($data) {--}}

                                    {{--}--}}
                                    {{--},--}}
                                };
                            } else {
                                //Get file from e-Office file system
                                $menu = {
                                    "download" : {
                                        "label" : "{!! trans("labels.general.download") !!}",
                                        "action" : function ($data) {
                                            {{--window.open( base_url + "/claim/notification_report/download_document_dms/{!! $notification_report->id !!}/" + node.id);--}}
                                            window.open( base_url + "/claim/notification_report/download_document_general/{!! $notification_report->id !!}/" + node.id);
                                        }
                                    },
                                    "open" : {
                                        "label" : "{!! ucfirst(strtolower(trans("labels.general.open"))) !!}",
                                        "action" : function ($data) {
                                            {{--window.open(base_url + "/claim/notification_report/open_document_dms/{!! $notification_report->id !!}/" + node.id, '_blank');--}}
                                            window.open(base_url + "/claim/notification_report/open_document_general/{!! $notification_report->id !!}/" + node.id, '_blank');
                                        }
                                    },
                                };
                            };



                            if (node.type == 'file'){
                                return $menu;
                            }


                        }
                    },
                    'types': {
                        'default': {'icon': 'folder'},
                        'file': {'valid_children': [], 'icon': 'file'}
                    },
                    'unique': {
                        'duplicate': function (name, counter) {
                            return name + ' ' + counter;
                        }
                    },
                    'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']
                })
                    .on('delete_node.jstree', function (e, data) {
                        $.ajax({url: "{!! route('backend.claim.notification_report.destroy_document', $notification_report->id) !!}", type: 'POST', data: {'id': data.node.id}})
                            .done(function (d) {
                                if (!d.success) {
                                    data.instance.refresh();
                                }
                            })
                            .fail(function () {
                                data.instance.refresh();
                            });
                    });
                $folder.bind("click.jstree", function (event, data) {
                    var node = $(event.target).closest("li").attr("id");
                    var type = $folder.jstree().get_selected(true)[0].type;
                    if (type === 'default') {
                        /*should only work on folders*/
                        node = node.split('/');
                        node = node[node.length - 1];
                        if (node.length > 0) {
                            $('input[name="document_id"]').val(node);
                        } else {
                            $('input[name="document_id"]').val(0);
                        }
                    } else {
                        $('input[name="document_id"]').val(0);
                    }


                });
                /************************** end: Document Library JS ***************************/

                /* start: Submitting Form and perform validation on the server side */
                $('body').on('submit', 'form.upload_document', function (e) {
                    e.preventDefault();
                    if ( $('input[name="document_id"]').val() == 0) {
                        alert("{!! trans("exceptions.backend.storage.select_document") !!}");
                        return false;
                    }
                    var $id = this.id;
                    var form = this;
                    var $data = $(form).serialize();
                    var $options = {
                        dataType : "json",
                        type : "POST",
                        url : $(form).attr("action"),
                        beforeSend : function (e) {
                            $(form).find(".progress-bar").width('0%');
                            $(form).find(".btn-submit").prop('disabled', true);

                        },
                        success : function (data) {
                            $(form).find(".btn-submit").prop('disabled', false);
                            if (data.success) {
                                $folder.jstree("refresh");
                            }
                        },
                        error: function (data) {
                            $(form).find(".progress-bar").width('0%');
                            $(form).find(".btn-submit").prop('disabled', false);
                        },
                        uploadProgress : function (event, position, total, percentComplete) {
                            $(form).find(".progress-bar").width(percentComplete + '%');
                            $(form).find(".progress-bar").html('<div class="progress-status">' + percentComplete + ' %</div>')
                        },
                    };
                    // pass options to ajaxForm
                    $(form).ajaxSubmit($options);
                });
            });
        });
    </script>
@endpush