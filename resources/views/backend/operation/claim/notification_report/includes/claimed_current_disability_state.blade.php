{{--CLAImed current disability state--}}

<div class="row" id="disability-state-div">
    {{--CLAIMED- Current Disability State--}}
    {{--<div class="col-md-6">--}}
    <div class="col-md-12">
        <div>&nbsp;</div>
        <table class="display" cellspacing="0" width="100%">
            <tbody>
            {{--Header--}}
            <tr class="light_grey_bg">
                <th>@lang('labels.backend.claim.medical_expense')</th>
                <th>@lang('labels.backend.claim.disability_state')</th>
                <th>@lang('labels.backend.claim.percent_of_ed_ld')</th>
                <th>@lang('labels.backend.claim.no_of_days')</th>
                <th>@lang('labels.backend.table.from_date')</th>
                <th>@lang('labels.backend.table.to_date')</th>
            </tr>
              @foreach($notification_disability_states as $notification_disability_state)
                  <tr>
                      <td >{!! $notification_disability_state->medicalExpense->compensated_entity_name !!}</td>
                      <td >{!! $notification_disability_state->disabilityStateChecklist->name !!}</td>
                      <td >{!! $notification_disability_state->percent_of_ed_ld !!}</td>
                      <td >{!! \Carbon\Carbon::parse($notification_disability_state->from_date)->diffInDays(\Carbon\Carbon::parse($notification_disability_state->to_date)) !!}</td>
                      <td >{!! $notification_disability_state->from_date_formatted !!}</td>
                      <td >{!! $notification_disability_state->to_date_formatted !!}</td>
                  </tr>
              @endforeach
            </tbody>
        </table>

    </div>
</div>