

<div class = "row" id="disability-state-div">
<div class="col-md-12"  >


{{--<div>&nbsp;</div>--}}
{{--<div>&nbsp;</div>--}}

<table class="display" cellspacing="0" width="100%" id ="disability-state-table">
    <thead>
    <tr>
        <th>@lang('labels.backend.claim.medical_expense')</th>
        <th>@lang('labels.backend.claim.disability_state')</th>
        <th>@lang('labels.backend.claim.percent_of_ed_ld')</th>
        <th>@lang('labels.backend.claim.no_of_days')</th>
        <th>@lang('labels.backend.table.from_date')</th>
        <th>@lang('labels.backend.table.to_date')</th>
    </tr>
    </thead>

</table>
</div>
</div>


@push('disability-state-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#disability-state-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            sort: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.notification_report.get_disability_states', $notification_report->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'medical_expense' , name: 'medical_expense' },
                { data: 'disability_state_checklist_id' , name: 'disability_state_checklist_id' },
                { data: 'percent_of_ed_ld' , name: 'percent_of_ed_ld' },
                { data: 'days' , name: 'days' },
                { data: 'from_date', name: 'from_date' },
                { data: 'to_date', name: 'to_date' }

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/claim/notification_report/current_employee_state/"  + aData['medical_expense_id'] + "/edit";

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        } );
    });
</script>;

@endpush