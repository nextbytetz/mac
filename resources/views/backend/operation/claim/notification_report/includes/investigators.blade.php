

{{--sidebar notification summary table--}}


<div class="row">


    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.claim.investigators')</span></b></h5></td>

            </tr>
        </table>
    </div>


<table class="display" cellspacing="0" width="100%" id ="investigators-table">
    <thead>

    <tr>

        <td align="center" class="light_grey_bg"><b>@lang('labels.general.full_name')</b></td>

    </tr>

    </thead>
</table>

</div>




@push('investigators-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#investigators-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.notification_report.get_investigators', $notification_report->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'user_id' , name: 'user_id' }
                           ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/claim/notification_report/investigator/"  + aData['id'] + "/edit";

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }


        } );
    });
</script>;

@endpush