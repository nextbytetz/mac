{{--CLAIM ASSESSMENT EXPENSE OVERVIEW--}}

<div class="row" id="claim-assessment-expense-overview-div">
    {{--CLAIMED- ASSESSED EXPENSE--}}
    {{--<div class="col-md-6">--}}
        <div class="col-md-12">
            <div>&nbsp;</div>
            <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                {{--Header--}}
                <tr>
                    <th width="400px">@lang('labels.backend.claim.type_of_expense')</th>
                    {{--<th>@lang('labels.backend.claim.claimed_expense')</th>--}}
                    <th>@lang('labels.backend.claim.assessed_expense')</th>
                </tr>
                {{--amount of mae--}}
                <tr>
                    <td>@lang('labels.backend.claim.amount_of_mae') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.amount_of_mae_description')  "></i></td>
                    {{--claimed--}}
                    {{--<td>{!! Form::label( 'mae', number_format($claimed_expense['mae'], 2 , '.' , ',' ), [ ]) !!}--}}
                        {{--{!! Form::hidden( 'mae_amount', $claimed_expense['mae'],['id'=> 'mae_amount']) !!}--}}
                    {{--</td>--}}
                    {{--assessed--}}
                    <td>{!! Form::label( 'mae', number_format($claimed_expense['mae_assessed'], 2 , '.' , ',' ), [ ]) !!}
                        {!! Form::hidden( 'mae', $claimed_expense['mae_assessed'],['id'=> 'mae_assessed']) !!}
                                           </td>
                </tr>
                {{--amount of ttd--}}
                <tr>
                    <td>@lang('labels.backend.claim.amount_of_ttd') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.amount_of_ttd_description')  "></i></td>
                    {{--claimed--}}

                    {{--<td>{!! Form::label( 'ttd', number_format($claimed_expense['ttd_amount'], 2 , '.' , ',' ), [ ]) !!}--}}
                        {{--{!! Form::hidden( 'ttd_amount', $claimed_expense['ttd_amount'],['id'=> 'ttd_amount']) !!}--}}
                    {{--</td>--}}

                    {{--assessed--}}
                    <td>  <span id = "ttd_amount_assessed" >
{!! Form::hidden( 'ttd_assessed', null,['id'=> 'ttd_assessed']) !!}
                                        </span></td>
                </tr>
                {{--amount of tpd--}}
                <tr>
                    <td>@lang('labels.backend.claim.amount_of_tpd') <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.amount_of_tpd_description')  "></i></td>
                    {{--claimed--}}

                    {{--<td>{!! Form::label( 'tpd', number_format($claimed_expense['tpd_amount'], 2 , '.' , ',' ), [ ]) !!}--}}
                        {{--{!! Form::hidden( 'tpd_amount', $claimed_expense['tpd_amount'],['id'=> 'tpd_amount']) !!}--}}
                    {{--</td>--}}


                    {{--assessed--}}
                    <td><span id = "tpd_amount_assessed">
{!! Form::hidden( 'tpd_assessed', null,['id'=> 'tpd_assessed']) !!}
                                        </span></td>
                </tr>
                {{--amount of ttd + tpd--}}
                <tr>
                    <td>@lang('labels.backend.claim.ttd_tpd')</td>
                    {{--claimed--}}


                    {{--<td>{!! Form::label( 'ttd_tpd', number_format(($claimed_expense['tpd_amount']+ $claimed_expense['ttd_amount']), 2 , '.' , ',' ), [ ]) !!}--}}
                        {{--{!! Form::hidden( 'ttd_tpd_amount', ($claimed_expense['tpd_amount']+$claimed_expense['ttd_amount']),['id'=> 'ttd_tpd_amount']) !!}--}}
                    {{--</td>--}}

                    {{--assessed--}}
                    {{--assessed--}}
                    <td><span id = "ttd_tpd_amount_assessed" >

                                        </span></td>
                </tr>
                {{--amount of mae + ttd + tpd--}}
                <tr>
                    <th>@lang('labels.backend.claim.mae_ttd_tpd')</th>
                    {{--claimed--}}
                    {{--<th>{!! Form::label( 'total_claimed_expenses', number_format($claimed_expense['total_claimed_expense'], 2 , '.' , ',' ), [ 'class'=>'underline'  ]) !!}--}}
                        {{--{!! Form::hidden( 'total_claimed_expense', $claimed_expense['total_claimed_expense'],['id'=> 'total_claimed_expense']) !!}--}}
                    {{--</th>--}}
                    {{--assessed--}}
                    <th><span id = "mae_ttd_tpd_amount_assessed" class="underline"  >

                                        </span></th>
                </tr>


                </tbody></table>

        </div>
    </div>