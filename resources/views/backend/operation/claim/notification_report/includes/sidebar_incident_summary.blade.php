

{{--sidebar notification / incident summary table--}}


<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.claim.incident_summary')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">


            <tr>
                <td style="padding-left: 5px" width="130px">@lang('labels.backend.claim.incident_type'):</td>
                <td><b>{!! Form::label( 'incident_type', $notification_report->incidentType->name, [ 'id'=> 'incident_type']) !!}</b></td>

            </tr>
            <tr>
                <td style="padding-left: 5px" width="130px">Acknowledgement Letter Issued:</td>
                <td><b>{!! $notification_report->acknowledgement_letter_status
                        !!}</b>
                </td>
            </tr>
            {{--accident--}}
            @if ($notification_report->incident_type_id == 1)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.accident_date'):</td>
                    <td ><b>{!! Form::label( 'accident_date', $notification_report->accident->accident_date_formatted, [ 'id'=>
                'accident_date'])
                !!}</b></td>

                </tr>

            {{--Reporting Date--}}
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.reporting_date'):<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.reporting_date_description')  "></i></td>
                    <td ><b>{!! Form::label( 'reporting_date', $notification_report->accident->reporting_date_formatted, [ 'id'=>
                'reporting_date'])
                !!} </b></td>
                </tr>

            {{--Receipt Date--}}
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.receipt_date'):<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.receipt_date_description')"></i></td>
                    <td ><b>{!! Form::label( 'receipt_date', $notification_report->accident->receipt_date_formatted, [ 'id'=>
                'receipt_date'])
                !!}</b></td>
                </tr>

            {{--Accident Time--}}
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.accident_time'):</td>
                    <td ><b>{!! Form::label( 'accident_time', $notification_report->accident->accident_time, [ 'id'=>
                'accident_time'])
                !!}</b></td>
                </tr>

            {{--Activity Performed--}}
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.activity_performed'):</td>
                    <td ><b>{!! Form::label( 'activity_performed', isset($notification_report->accident->activity_performed) ? $notification_report->accident->activity_performed : ' ', [ 'id'=>
                'activity_performed'])
                !!}</b></td>
                </tr>

            {{--Accident Place--}}
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.accident_place'):</td>
                    <td ><b>{!! Form::label( 'accident_place', ($notification_report->accident()->count()) ? $notification_report->accident->accident_place : ' ', [ 'id'=>
                'accident_place'])
                !!}</b></td>
                </tr>
            {{--Description--}}
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.table.description'):</td>
                    <td ><b>{!! Form::label( 'description', ($notification_report->accident()->count()) ? $notification_report->accident->description : ' ', [ 'id'=>
                'description'])
                !!}</b></td>
                </tr>
            @endif

            {{--DISEASE--}}
            @if ($notification_report->incident_type_id == 2)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.diagnosis_date'):</td>
                    <td ><b>{!! Form::label( 'diagnosis_date', $notification_report->disease->diagnosis_date_formatted, [ 'id'=>
                'diagnosis_date'])
                !!}</b></td>
                </tr>

                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.reporting_date'):</td>
                    <td ><b>{!! Form::label( 'reporting_date', $notification_report->disease->reporting_date_formatted, [ 'id'=>
                'reporting_date'])
                !!}</b></td>
                </tr>

                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.receipt_date'):</td>
                    <td ><b>{!! Form::label( 'receipt_date', $notification_report->disease->receipt_date_formatted, [ 'id'=>
                'receipt_date'])
                !!}</b></td>
                </tr>

                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.table.disease_name'):</td>
                    <td ><b>{!! Form::label( 'disease_name', $notification_report->disease->name, [ 'id'=>
                'disease_name'])
                !!}</b></td>
                </tr>
            @endif

            {{--death--}}
            @if (in_array($notification_report->incident_type_id, [3,4,5]))
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.death_date'):</td>
                    <td ><b>{!! Form::label( 'death_date', $notification_report->death->death_date_formatted, [ 'id'=>
                'death_date'])
                !!}</b></td>

                </tr>
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.reporting_date'):</td>
                    <td ><b>{!! Form::label( 'reporting_date', $notification_report->death->reporting_date_formatted, [ 'id'=>
                'reporting_date'])
                !!}</b></td>
                </tr>

                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.receipt_date'):</td>
                    <td ><b>{!! Form::label( 'receipt_date', $notification_report->death->receipt_date_formatted, [ 'id'=>
                'receipt_date'])
                !!}</b></td>

                </tr>

                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.death_place'):</td>
                    <td ><b>{!! Form::label( 'death_place', $notification_report->death->death_place, [ 'id'=>
                'death_place'])
                !!}</b></td>

                </tr>

                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.death_cause'):</td>
                    <td ><b>{!! Form::label( 'death_cause', $notification_report->death->deathCause->name, [ 'id'=>
                'death_cause'])
                !!}</b></td>
                </tr>
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.certificate_number'):</td>
                    <td ><b>{!! Form::label( 'certificate_number', $notification_report->death->certificate_number, [ 'id'=>
                'certificate_number'])
                !!}</b></td>
                </tr>
            @endif



            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.claim.need_investigation'):</td>
                <td><b>{!! Form::label( 'need_investigation',  $notification_report->need_investigation == 1 ? 'Yes' : 'No', [ 'id'=> 'need_investigation'])
                        !!}</b></td>
            </tr>

                 @if ($notification_report->need_verification == 1)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.is_verified'):</td>
                    <td><b>{!! $notification_report->verification_status
                        !!}</b></td>

                </tr>
            @endif
            @if ($notification_report->need_investigation == 1)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.is_investigated'):</td>
                    <td><b>{!! $notification_report->investigation_validity_status
                        !!}</b></td>

                </tr>
            @endif
            {{--is Acknowledge--}}

            {{--<tr>--}}
                {{--<td style="padding-left: 5px">@lang('labels.backend.claim.is_acknowledged'):</td>--}}
                {{--<td><b>{!! $notification_report->acknowledgement_status--}}
                        {{--!!}</b></td>--}}

            {{--</tr>--}}


            {{--Incident Exposure--}}

            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.claim.incident_exposure'):</td>
                <td><b>{!! ($notification_report->incidentExposure()->count()) ? $notification_report->incidentExposure->name : ' '
                        !!}</b></td>
            </tr>


            {{--Body Part injury--}}

            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.claim.body_part_injury'):</td>
                <td><b>{!! ($notification_report->bodyPartInjury()->count()) ? $notification_report->bodyPartInjury->name : ' '
                        !!}</b></td>
            </tr>




            {{---Nature of Incident --}}

            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.claim.nature_of_incident'):</td>
                <td><b>{!! ($notification_report->natureOfIncident()->count()) ? $notification_report->natureOfIncident->name : ' '
                        !!}</b></td>
            </tr>



            {{--Witness => ONLY FOR Accident Incident--}}
            @if ($notification_report->incident_type_id == 1)
                @foreach($notification_report->accident->accidentWitnesses as $accident_witness)
                    <tr>
                        <td style="padding-left: 5px">@lang('labels.backend.claim.witness'):</td>
                        <td> <a href="{!! route('backend.claim.notification_report.edit_accident_witness',
                                        $accident_witness->witness->id) !!}"><b>{!! $accident_witness->witness->fullname
                        !!}</b></a></td>

                    </tr>
                @endforeach
            @endif


            {{--Prepared By--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.prepared_by'):</td>
                <td> <a href="#"><b>{!! $notification_report->user->username
                        !!}</b><a/></td>

            </tr>


        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush