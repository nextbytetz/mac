@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.change_investigator'), 'header_title' => trans('labels.backend.claim.change_investigator')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_investigator,['route' => ['backend.claim.notification_report.change_investigator',$notification_investigator->id ],'method'=>'put',
    'id' => 'update']) !!}


    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_investigator->notificationReport])


    <div>&nbsp;</div>

    {{--main contents--}}

    {{--user id 1--}}
    <div class="row">
        <div class="col-md-12">
{{--Delete investigaor--}}
            <div class="pull-right">
                {{ link_to_route('backend.claim.notification_report.delete_investigator', trans('buttons.general.crud.delete'), $notification_investigator->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_investigator_delete'), 'class' => 'btn btn-danger btn-round-right']) }}
            </div>


            {{--Investigator--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.investigator'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('investigator', $users, $notification_investigator->user_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'investigator']) !!}
                        {!! $errors->first('investigator', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>





    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('claim/notification_report/profile/' . $notification_investigator->notification_report_id . '#investigation',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {{--{!! link_to('compliance/employee/profile/' . $employee->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}--}}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">

    $(function () {
        $(".search-select").select2({});
    });

</script>;


@endpush
