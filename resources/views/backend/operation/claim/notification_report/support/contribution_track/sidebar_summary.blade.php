

{{--sidebar notification summary table--}}


<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            <tr>
                <td style="padding-left: 5px" width="130px">@lang('labels.general.employee_name'):</td>
                <td  height="20px"><b>{!! Form::label( 'employee_name', $notification_report->employee->name, [ 'id'=> 'employee_name']) !!}</b></td>

            </tr>



            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.memberno'):</td>
                <td><b>{!! Form::label( 'memberno', $notification_report->employee->memberno, [ 'id'=> 'memberno']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">@lang('labels.general.dob'):</td>
                <td><b>{!! Form::label( 'dob', $notification_report->employee->dob_formatted, [ 'id'=> 'dob']) !!}</b></td>

            </tr>


            <tr>
                <td style="padding-left: 5px">@lang('labels.general.gender'):</td>
                <td><b>{!! Form::label( 'gender', ($notification_report->employee->gender()->count()) ? $notification_report->employee->gender->name : ' ', [ 'id'=> 'gender']) !!}</b></td>

            </tr>



            <tr>
                <td style="padding-left: 5px">@lang('labels.general.occupation'):</td>
                <td><b>{!! Form::label( 'occupation', ($notification_report->employee->jobTitle()->count()) ? $notification_report->employee->jobTitle->name : ' ', [ 'id'=> 'occupation']) !!}</b></td>

            </tr>




            {{--employer--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.employer'):</td>
                <td><b>{!! Form::label( 'employer', ($notification_report->employer()->count()) ? $notification_report->employer->name : ' ', [ 'id'=> 'employer']) !!}</b></td>

            </tr>


            {{--employer reg no--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.employer_reg_no'):</td>
                <td><b>{!! Form::label( 'incident_type',($notification_report->employer()->count()) ?  $notification_report->employer->reg_no : ' ', [ 'id'=> 'incident_type']) !!}</b></td>

            </tr>

            {{--gross monthly earning--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.claim.gross_monthly_earning'): <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.gross_monthly_earning_description')  "></i></td>
                <td><b>{!! Form::label( 'monthly_earning', ($notification_report->monthly_earning) ? number_format( $notification_report->monthly_earning, 2 , '.' , ',' ): ' ' , [ 'id'=> 'monthly_earning']) !!}</b></td>

            </tr>


            {{--contribution monthly--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.finance.receipt.contrib_month'): <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.claim.contrib_month_description')  "></i></td>
                <td><b>{!! Form::label( 'contrib_month',  $contrib_month , [ 'id'=> 'contrib_month']) !!}</b></td>

            </tr>


            <tr>
                <td style="padding-left: 5px" width="130px">@lang('labels.backend.claim.incident_type'):</td>
                <td><b>{!! Form::label( 'incident_type', $notification_report->incidentType->name, [ 'id'=> 'incident_type']) !!}</b></td>

            </tr>
            {{--inxcdient--}}
                         <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.claim.incident_date'):</td>
                    <td ><b>{!! Form::label( 'incident_date', $notification_report->incident_date_formatted, [ 'id'=>
                'incident_date'])
                !!}</b></td>

                </tr>



        </table>
    </div>
</div>





