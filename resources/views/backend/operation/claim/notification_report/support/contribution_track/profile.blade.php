@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.notification_contribution_tracks'), 'header_title' => trans('labels.backend.claim.notification_contribution_tracks')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {{--{!! Form::model($notification_report,['route' => ['backend.claim.notification_report.store_medical_expense',$notification_report->id ],'method'=>'post',--}}
    {{--'id' => 'create']) !!}--}}

    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=> $notification_report])

    <div>&nbsp;</div>
    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
        <div class="row">
            <div class="col-md-12" >
                <div class="pull-right" >


                    @if($check_if_pending_compliance == 1 and access()->user()->unit_id == 15)
                        <span>
                        {{--Update gross pay--}}
                            <a href="{!! route('backend.claim.notification_report.edit_monthly_earning', $notification_report->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-money"></i>&nbsp;@lang('labels.backend.claim.update_gross_pay')
                        </a>
                    </span>
                    @endif

                    {{--close--}}
                    <span>
                        <a href="{!! route('backend.claim.notification_contribution_track.index') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
                    </span>

                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-9">
            {{--contribution tracks--}}

            @include('backend.operation.claim.notification_report.support.contribution_track.tracks')
        </div>


        {{--Side bar summary--}}

        <div class="col-md-3">
            {{--sidebar summary--}}
            @include('backend.operation.claim.notification_report.support.contribution_track.sidebar_summary')





        </div>


    </div>






    {{--{!! Form::close() !!}--}}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
<script  type="text/javascript">
    $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();




    $(function () {
        $(".search-select").select2({});
        member_type_option('member_type_id', 'insurance_id', 'insurance_div');

        $("#member_type_id").on('change keyup', function () {
            member_type_option('member_type_id','insurance_id', 'insurance_div');
        });

        $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
            number_only(e);

        });


    });

    //Member type options -> disable and hide insurance textbox if others are selected
    function member_type_option(member_type_id, insurance_id, insurance_div) {

        var member_type = $("#" + member_type_id).val();

        if (member_type == 3) {
            $("#" + insurance_id).prop("disabled", false);
            $("#" + insurance_div).prop("hidden", false);
        }
        else {

            $("#" + insurance_id).prop("disabled", true);
            $("#" + insurance_div).prop("hidden", true);
        }

    }

    /* start : mask all money input */
    $('.money').maskMoney({
        precision : 2,
        affixesStay : false
    });

    /* start : ensure only numbers are input on monetary boxes */
    function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
</script>;


@endpush
