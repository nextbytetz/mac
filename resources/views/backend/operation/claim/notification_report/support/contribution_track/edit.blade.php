@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.edit_notification_contribution_tracks'), 'header_title' => trans('labels.backend.claim.edit_notification_contribution_tracks')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($track,['route' => ['backend.claim.notification_contribution_track.update',$track->id ],'method'=>'put',
    'id' => 'update']) !!}


    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])

    <div>&nbsp;</div>

    {{--main contants--}}

    @if($track->unit_id == 14)
        <div class="row" >
            <div class="col-md-12" >
                <div class="element-form">
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.actions'):</label></div>
                    <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12 ">
                        <div class="form-group" >
                            {!!  Form::select('action', [  '0' => null,  '1' => 'Approve', '2' => 'Respond'],  null, ['class' => 'form-control search-select' ,]) !!}
                            {!! Form::hidden('actions', null, []) !!}
                            {!! $errors->first('actions', '<span class="help-block label
                            label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>


            </div>
        </div>
    @endif

    {{--Comment--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.comments'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::textarea( 'comments',null, [ 'id'=> 'comments',  'class' =>'form-control']) !!}
                        {!! $errors->first('comments', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.claim.notification_contribution_track.profile',trans('buttons.general.cancel'), [$notification_report->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
<script  type="text/javascript">
    $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();




    $(function () {
        $(".search-select").select2({});

    });


</script>;


@endpush
