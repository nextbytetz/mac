


<div>&nbsp;</div>
<table class="display" cellspacing="0" width="100%" id ="track-table">
    <thead>
    <tr>
        <th>@lang('labels.general.user')</th>
        <th>@lang('labels.general.department')</th>
        <th>@lang('labels.general.from')</th>
        <th>@lang('labels.backend.table.receive_date')</th>
        <th>@lang('labels.backend.table.workflow.forward_date')</th>
        <th>@lang('labels.general.comments')</th>

    </tr>
    </thead>
</table>




@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#track-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            info: false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.notification_contribution_track.get', $notification_report->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'user_id', name: 'user_id'},
                { data: 'unit_id', name: 'unit_id'},
                { data: 'from_user_id', name: 'from_user_id'},
                { data: 'receive_date', name: 'created_at'},
                { data: 'forwarded_date', name: 'forward_date'},
                { data: 'comments', name: 'comments'},

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url +  "/claim/notification_contribution_track/" + aData['id'] +
                        "/edit"  ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });







</script>;

@endpush
