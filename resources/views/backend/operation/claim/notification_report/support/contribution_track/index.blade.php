@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.pending_notification_contribution'), 'header_title' => trans('labels.backend.claim.pending_notification_contribution')])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >

            <div>&nbsp;</div>
            <table class="display" cellspacing="0" width="100%" id ="track-table">
                <thead>
                <tr>
                    <th>@lang('labels.general.employee_name')</th>
                    <th>@lang('labels.general.employer_name')</th>
                    <th>@lang('labels.backend.claim.incident_type')</th>
                    <th>@lang('labels.backend.claim.incident_date')</th>
                    <th>@lang('labels.backend.finance.receipt.contrib_month')</th>
                    <th>@lang('labels.general.from')</th>

                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#track-table').DataTable({
            processing: true,
            serverSide: false,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.claim.notification_contribution_track.get_pending') !!}',
                type : 'get'
            },
            columns: [
                { data: 'employee', name: 'employee'},
                { data: 'employer', name: 'employer'},
                { data: 'incident_type_id', name: 'incident_type_id'},
                { data: 'incident_date', name: 'incident_date'},
                { data: 'contrib_month', name: 'contrib_month'},
                { data: 'user_id', name: 'user_id'},
                { data: 'id', name: 'id', orderable : false, searchable : true,visible: false},

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url +  "/claim/notification_contribution_track/profile/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });







</script>;

@endpush
