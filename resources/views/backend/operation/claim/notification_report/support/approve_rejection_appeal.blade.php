@extends('layouts.backend.main', ['title' => 'Approve Rejection Appeal', 'header_title' => 'Approve Rejection Appeal'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_report,['route' => ['backend.claim.notification_report.approve_rejection_appeal',$notification_report->id ],'method'=>'post',
    'id' => 'update']) !!}



    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])


    <div>&nbsp;</div>

    {{--main contants--}}

    {{--Reason--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form"  >
                <div class="col-xl-1 col-lg-1 col-md-3 col-sm-16 col-xs-4 text-xs-right required"><label>Reason:</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::textarea( 'reason',null, [ 'id'=> 'reason',  'class' =>'form-control']) !!}
                        {!! $errors->first('reason', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-5" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.claim.notification_report.profile',trans('buttons.general.cancel'), [$notification_report->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script  type="text/javascript">

        $(function () {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();


        });




    </script>;


@endpush
