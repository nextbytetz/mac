@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.edit_witness'), 'header_title' => trans('labels.backend.claim.edit_witness')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($witness, ['route' => ['backend.claim.notification_report.update_accident_witness', $witness->id], 'method'=>'put', 'name' => 'update_accident_witness']) !!}
    {!! Form::hidden('witness_id', 2 ) !!}
    {!! Form::hidden('request_action_type', 2 ) !!}

    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report' => $notification_report])

    <div class="image-blockquote">

        <div class="row">
            <div class="col-md-12">
                {{--Delete witness--}}
                <div class="pull-right">
                    {{ link_to_route('backend.claim.notification_report.delete_accident_witness', trans('buttons.general.crud.delete'), $witness->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_witness_delete'), 'class' => 'btn btn-danger btn-round-right']) }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 offset-md-3">
                @include("backend.includes.registration.claim.witness.edit")
            </div>
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$notification_report->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    {{--</section>--}}
@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">
    $(function () {

    });
</script>;

@endpush
