@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.investgation_report_feedback'), 'header_title' => trans('labels.backend.claim.investgation_report_feedback')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
{{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
        {!! Form::model($notification_report, ['route' => ['backend.claim.notification_report.update_investigation_report', $notification_report->id ], 'method'=>'put', 'id' => 'update', 'name' => 'update_investigation_report', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('when_incident_occurred' , $incident_date) !!}
        @if ($notification_report->isprogressive)
        {!! Form::hidden("notification_investigator_id", $investigator->id) !!}
        @else
        {!! Form::hidden("notification_investigator_id", 0) !!}
        @endif


        {{--HEADER--}}
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report'=>$notification_report])

        <div>&nbsp;</div>

        {{--main contants--}}

        {{--loop all investigation feedbacks--}}
        @foreach ($investigation_feedbacks as $investigation_feedback)
        @php
        $question = $investigation_feedback->investigationQuestion;
        $hide = (!is_null($question->parent_id));
        @endphp
        @if($question->data_type == 'text')

        {{--TEXT--}}
        <div class="row" id="question{{$question->parent_id}}">
            <div class="col-md-12">
                <div class="element-form" >
                    <div class="col-xl-4 col-lg-4  col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}</div>
                    <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text', 'feedback' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @elseif ($question->data_type == 'select_type' && $question->data_type_source == 'district_wards')
        <div class="row" id="question{{$question->parent_id}}">
            <div class="col-md-12">
                <div class="element-form" >
                    <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}
                    </div>
                    <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">

                            {!!  Form::select('district' . $investigation_feedback->id , $districts,  $investigation_feedback->feedback, ['class' => 'form-control search-select district_select' , 'placeholder'=> '-- Select district --', 'id' => 'district' . $question->id]) !!}
                            {!! $errors->first('district' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                            
                        </div>
                        <div class="form-group">
                            <div class="row" >
                                <div class="col-md-6 wards" style="display: none;width: 40%;">
                                    <select class="search-select wards-select" style="width: 100%;" name="postcode">
                                        
                                    </select>
                                </div>
                                <div class="col-md-6 workplace pull-right" style="display: none;width: 60%;">
                                    {!! Form::input( 'text', 'workplace' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control workplaces', 'placeholder'=> 'Enter New workplace..']) !!}{!! $errors->first('district_wards' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                                    
                                </div>
                                <br><br>
                                <div class="col-md-6 pull-right">
                                    <select class="workplace-names form-control pull-right" name="w_names" style="display: none;width: 250px;margin-left: -50px;">
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">Remarks</div>
                    <div class="col-md-3">
                        {!! Form::textarea('remarks' . $investigation_feedback->id, $investigation_feedback->remarks, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                    </div>
                </div>
            </div>
        </div>
        @elseif ($question->data_type == 'select_type' && $question->data_type_source == 'accident_cause')
        <div class="row" id="question{{$question->parent_id}}">
            <div class="col-md-12">
                <div class="element-form" >
                    <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}
                    </div>
                    <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                         @if($investigation_feedback->code_id != null)
                         {!! Form::select('accident_cause' . $investigation_feedback->id,$accident_causes, old('accident_cause' . $question->id,$investigation_feedback->code_id), ['class' => 'form-control search-select accident-cause','id'=>'accident_cause' . $question->id]) !!}
                         {!! $errors->first('accident_cause' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                         @else
                         {!!  Form::select('accident_cause' . $investigation_feedback->id , $accident_causes,  $investigation_feedback->feedback, ['class' => 'form-control search-select accident-cause' , 'placeholder'=> '-- Select Accident Cause --', 'id' => 'accident_cause' . $question->id]) !!}
                         {!! $errors->first('accident_cause' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                         @endif
                         
                         <input type="hidden" name="accident_name_cause" id="accident-cause" value="">
                     </div>
                     <div class="form-group specific-accident-cause-class" style="display: none;">
                         <select class="form-control search-select specific-accident-cause" name="specific_accident" style="width:400px;">
                             
                         </select> 
                         
                     </div>
                     
                 </div>
                 <div class="col-md-1">Remarks</div>
                 <div class="col-md-3">
                    {!! Form::textarea('remarks' . $investigation_feedback->id, $investigation_feedback->remarks, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                </div>
            </div>
        </div>
    </div>
    @elseif ($question->data_type == 'select_type' && $question->data_type_source == 'disease_type')
    <div class="row" id="question{{$question->parent_id}}">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}
                </div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">


                        @if($investigation_feedback->code_value_id != null)
                        {!! Form::select('disease_type' . $investigation_feedback->id,$disease_types, old('disease_type' . $question->id,$investigation_feedback->code_value_id), ['class' => 'form-control search-select disease-types','id'=>'disease_type' . $question->id]) !!}
                        {!! $errors->first('disease_type' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                        @else
                        {!!  Form::select('disease_type' . $investigation_feedback->id , $disease_types,  $investigation_feedback->feedback, ['class' => 'form-control search-select disease-types' , 'placeholder'=> '-- Select  Disease Type --', 'id' => 'disease_type' . $question->id]) !!}
                        {!! $errors->first('disease_type' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                        @endif

                        <input type="hidden" name="disease_name_type" id="disease-types" value="">
                        
                    </div>
                    
                </div>
                <div class="col-md-1">Remarks</div>
                <div class="col-md-3">
                    {!! Form::textarea('remarks' . $investigation_feedback->id, $investigation_feedback->remarks, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                </div>
            </div>
        </div>
    </div>
    @elseif ($question->data_type == 'select_type' && $question->data_type_source == 'disease_agent')
    <div class="row" id="question{{$question->parent_id}}">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}
                </div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        @if($investigation_feedback->code_value_id != null)

                        {!!  Form::select('disease_agent' . $investigation_feedback->id , $disease_agents, old('disease_agent' . $question->id,$investigation_feedback->code_value_id), ['class' => 'form-control search-select disease-agents' , 'placeholder'=> '-- Select  Disease Agent --', 'id' => 'disease_agent' . $question->id]) !!}
                        {!! $errors->first('disease_agent' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                        @else
                        {!!  Form::select('disease_agent' . $investigation_feedback->id , $disease_agents,  $investigation_feedback->feedback, ['class' => 'form-control search-select disease-agents' , 'placeholder'=> '-- Select  Disease Agent --', 'id' => 'disease_agent' . $question->id]) !!}
                        {!! $errors->first('disease_agent' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                        @endif
                        
                    </div>
                    <div class="form-group specific-disease-agent-class" style="display: none;">
                     <select class="form-control search-select specific-disease-agent" name="specific_agent" style="width:400px;">
                         
                     </select> 
                     
                 </div>
                 
             </div>
             <div class="col-md-1">Remarks</div>
             <div class="col-md-3">
                {!! Form::textarea('remarks' . $investigation_feedback->id, $investigation_feedback->remarks, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
            </div>
        </div>
    </div>
</div>
@elseif ($question->data_type == 'select_type' && $question->data_type_source == 'target_organ')
<div class="row" id="question{{$question->parent_id}}">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}
            </div>
            <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">

                    @if($investigation_feedback->code_value_id != null)

                    {!!  Form::select('target_organ' . $investigation_feedback->id , $target_organs, old('target_organ' . $question->id,$investigation_feedback->code_value_id), ['class' => 'form-control search-select target-organs' , 'placeholder'=> '-- Select  Target Organ --', 'id' => 'target_organ' . $question->id]) !!}
                    {!! $errors->first('target_organ' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @else
                    {!!  Form::select('target_organ' . $investigation_feedback->id , $target_organs,  $investigation_feedback->feedback, ['class' => 'form-control search-select target-organs' , 'placeholder'=> '-- Select  Target Organ --', 'id' => 'target_organ' . $question->id]) !!}
                    {!! $errors->first('target_organ' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @endif
                    
                </div>
                <div class="form-group specific-target-organ-class" style="display: none;">
                 <select class="form-control search-select specific-target-organ" name="specific_organ" style="width:400px;">
                     
                 </select> 
                 
             </div>
             
         </div>
         <div class="col-md-1">Remarks</div>
         <div class="col-md-3">
            {!! Form::textarea('remarks' . $investigation_feedback->id, $investigation_feedback->remarks, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
        </div>
    </div>
</div>
</div>
<br/>
@elseif ($question->data_type == 'boolean')
{{--BOOLEAN--}}
<div class="row" id="question{{$question->parent_id}}">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}
            </div>
            <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    @if (is_null($question->parent_id))
                    {!!  Form::select('boolean' . $investigation_feedback->id , ['No' => 'No', 'Yes' => 'Yes'],  $investigation_feedback->feedback, ['class' => 'form-control search-select boolean_select' , 'placeholder'=> '', 'id' => 'boolean' . $question->id]) !!}
                    {!! $errors->first('boolean' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @else
                    {!!  Form::select('feedback' . $investigation_feedback->id , ['No' => 'No', 'Yes' => 'Yes'],  $investigation_feedback->feedback, ['class' => 'form-control search-select boolean_select' , 'placeholder'=> '', 'id' => 'boolean' . $question->id]) !!}
                    {!! $errors->first('feedback' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @endif
                </div>
            </div>
            <div class="col-md-1">Remarks</div>
            <div class="col-md-3">
                {!! Form::textarea('remarks' . $investigation_feedback->id, $investigation_feedback->remarks, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
            </div>
        </div>
    </div>
</div>
<br/>
@elseif ($question->data_type == 'date')
{{--DATE--}}
<div class="row" id="question{{$question->parent_id}}">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}</div>
            <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    @if ($question->id == 1)
                    {!! Form::input( 'text', 'investigation_date' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    {!! $errors->first('investigation_date' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @elseif ($question->id == 19)
                    {!! Form::input( 'text', 'incident_date' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    {!! $errors->first('incident_date' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @else
                    @if (is_null($question->parent_id))
                    {!! Form::input( 'text', 'date' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    {!! $errors->first('date' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @else
                    {!! Form::input( 'text', 'feedback' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                    {!! $errors->first('feedback' . $investigation_feedback->id, '<span class="help-block label label-danger">:message</span>') !!}
                    @endif

                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@elseif ($question->data_type == 'bigtext')
<div class="row" id="question{{$question->parent_id}}">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-4 col-lg-4  col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}</div>
            {{--<div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">--}}
                <div class="col-md-7">
                    <div class="form-group">
                        {{--{!! Form::input( 'text','feedback' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control']) !!}--}}
                        {!! Form::textarea('feedback' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif ($question->data_type == 'select')
    {{--select from some other tables--}}
    <div class="row" id="question{{$question->parent_id}}">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-4 col-lg-4  col-md-3 col-sm-12 col-xs-4 text-xs-right">{!! Form::label( 'question', $question->name, ['class' => 'required'])!!}</div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text', 'feedback' . $investigation_feedback->id, $investigation_feedback->feedback, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif ($question->data_type == 'image')
    {{--disabled on this page--}}

    @endif
    {{--BOOLEAN--}}

    @endforeach

    <legend></legend>
    <br/>
    {{--Buttons--}}
    <div class="row">
        <div class="col-md-12" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-6 col-lg-6 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">
                        @if ($notification_report->isprogressive)
                        {!! link_to('claim/notification_report/profile/' . $notification_report->id ,trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @else
                        {!! link_to('claim/notification_report/profile/' . $notification_report->id . '#investigation',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @endif
                        {!! Form::button("Update",['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
{{--</section>--}}

@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}

<script>
    $(document).ready(function () {

        var accident = $('.accident-cause option:selected').val();
        if (accident) {
            if (accident.length > 0) {
                var url = "{{url('claim/notification_report/return_accident_cause')}}/" + accident;

                $.ajax({
                 type: 'get',
                 url: url,
                 success:function(response){
                    if (response.length > 0) {
                        @foreach ($investigation_feedbacks as $investigation_feedback)
                        @php
                        $question = $investigation_feedback->investigationQuestion;
                        $hide = (!is_null($question->parent_id));
                        @endphp
                        @if ($question->data_type == 'select_type' && $question->data_type_source == 'accident_cause')
                        var accident_id = "{{$investigation_feedback->code_value_id}}";

                        @endif

                        @endforeach
                        
                        var resp = '';
                        for(var i in response){
                            var value = accident_id == response[i].id ? 'selected' : "";
                            resp +=
                            '<option value="'+response[i].id+'" '+value+' style="width:400px;">'+response[i].name+'</option>';
                        }
                        $('.specific-accident-cause').empty();
                        $('.specific-accident-cause').append(resp);
                        $('.specific-accident-cause-class').show();

                    }else{
                        
                        $('.specific-accident-cause-class').hide();
                        $('.specific-accident-cause').empty();
                    }
                }
            });
            } else {
                $('.specific-accident-cause-class').hide();
                $('.specific-accident-cause').empty();
            }
        }


        var input = $('.disease-types option:selected').text();
        $('#disease-types').val(input);

        var organ = $('.target-organs option:selected').val();
        if (organ){
            if (organ.length > 0) {
                var url = "{{url('claim/notification_report/return_disease_organ')}}/" + organ;

                $.ajax({
                 type: 'get',
                 url: url,
                 success:function(response){
                    if (response.length > 0) {
                      @foreach ($investigation_feedbacks as $investigation_feedback)
                      @php
                      $question = $investigation_feedback->investigationQuestion;
                      $hide = (!is_null($question->parent_id));
                      @endphp
                      @if ($question->data_type == 'select_type' && $question->data_type_source == 'target_organ')
                      var agent_id = "{{$investigation_feedback->disease_exposure_agent_id}}";

                      @endif

                      @endforeach
                      
                      var resp = '';
                      for(var i in response){
                        var value = agent_id == response[i].id ? 'selected' : "";
                        resp +=
                        '<option value="'+response[i].id+'" '+value+' style="width:400px;">'+response[i].name+'</option>';
                    }
                    $('.specific-target-organ').empty();
                    $('.specific-target-organ').append(resp);
                    $('.specific-target-organ-class').show();

                }else{
                    
                    $('.specific-target-organ-class').hide();
                    $('.specific-target-organ').empty();
                }
            }
        });
            } else {
                $('.specific-target-organ-class').hide();
                $('.specific-target-organ').empty();
            }
        }
        

        
        var agent = $('.disease-agents option:selected').val();
        if (agent) {
            if (agent.length > 0) {
                var url = "{{url('claim/notification_report/return_disease_agent')}}/" + agent;

                $.ajax({
                 type: 'get',
                 url: url,
                 success:function(response){
                    if (response.length > 0) {
                        @foreach ($investigation_feedbacks as $investigation_feedback)
                        @php
                        $question = $investigation_feedback->investigationQuestion;
                        $hide = (!is_null($question->parent_id));
                        @endphp
                        @if ($question->data_type == 'select_type' && $question->data_type_source == 'disease_agent')
                        var agent_id = "{{$investigation_feedback->disease_exposure_agent_id}}";

                        @endif

                        @endforeach
                        
                        var resp = '';
                        for(var i in response){
                            var value = agent_id == response[i].id ? 'selected' : "";
                            resp +=
                            '<option value="'+response[i].id+'" '+value+' style="width:400px;">'+response[i].name+'</option>';
                        }
                        $('.specific-disease-agent').empty();
                        $('.specific-disease-agent').append(resp);
                        $('.specific-disease-agent-class').show();

                    }else{
                        
                        $('.specific-disease-agent-class').hide();
                        $('.specific-disease-agent').empty();
                    }
                }
            });
            } else {
                $('.specific-disease-agent-class').hide();
                $('.specific-disease-agent').empty();
            }
        }


        

        $(".search-select").select2({});
        var $bool_select = $(".boolean_select");

        $bool_select.select2({}).on('change', function (e) {
            var $choice = $(this).val();
            var $id = $(this).attr('id').substring(7);
            var $qnselect = $("#question" + $id);
            switch ($choice) {
                case 'Yes':
                $qnselect.show();
                break;
                case 'No':
                $qnselect.hide();
                break;
                default:
                $qnselect.hide();
                break;
            }
        });
        $bool_select.trigger("change");

        autosize($("textarea.autosize"));

        $('.district_select').change(function(e){
            var dstid = $(this).val();
            if (dstid.length > 0) {
                var url = "{{url('wards')}}/" + dstid;
                $.ajax({
                 type: 'GET',
                 url: url,
                 success:function(response){
                    var getresponse = '';
                    var getresponse = '<option selected disabled hidden>-- Select Ward --</option>';
                    for(var i in response){
                        getresponse +=
                        '<option value="'+response[i].postcode+'" style="width:150px;">'+response[i].ward_name+'</option>';
                    }

                    $('.wards-select').empty();
                    $('.wards-select').append(getresponse); 
                    $('.wards').show();
                }
            }); 
            }else{
              $('.wards').hide();  
              $('.workplace').hide();
          }
          
      })

        $('.wards-select').change(function(e){
            var postcode = $('.wards-select option:selected').val();
            var $employer_id = "{{$notification_report->employer_id}}";
            var url = "{{url('check_workplace')}}/" + postcode;
            var token = "{{csrf_token()}}";

            $.ajax({
             type: 'post',
             url: url,
             data: {
                '_token' : token,
                'employer_id' : $employer_id,
            },
            success:function(response){
                if (response.length > 0) {

                 var resp = '<option value="0" selected disabled hidden>-- Select available workplaces --</option>';
                 for(var i in response){
                    resp +=
                    '<option value="'+response[i].id+'" style="width:150px;">'+response[i].workplace_name+'</option>';
                }
                $('.workplace-names').empty();
                $('.workplace-names').append(resp);
                $('.workplaces').val('');
                $('.workplace').show();
                $('.workplace-names').show();

            }else{
                
                $('.workplace-names').hide();
                $('.workplace-names').empty();
                $('.workplaces').val('');
                $('.workplace').show();
            }
        }
    }); 
        })

        $('.accident-cause').change(function(e){
            var agent = $('.accident-cause option:selected').val();
            if (agent.length > 0) {
                var url = "{{url('claim/notification_report/return_accident_cause')}}/" + agent;

                $.ajax({
                 type: 'get',
                 url: url,
                 success:function(response){
                    if (response.length > 0) {
                        var resp = '';
                        for(var i in response){
                            resp +=
                            '<option value="'+response[i].id+'" style="width:400px;">'+response[i].name+'</option>';
                        }
                        $('.specific-accident-cause').empty();
                        $('.specific-accident-cause').append(resp);
                        $('.specific-accident-cause-class').show();

                    }else{
                        
                        $('.specific-accident-cause-class').hide();
                        $('.specific-accident-cause').empty();
                    }
                }
            });
            } else {
                $('.specific-accident-cause-class').hide();
                $('.specific-accident-cause').empty();
            }
            

        })

        $('.disease-agents').change(function(e){
            var agent = $('.disease-agents option:selected').val();
            if (agent.length > 0) {
                var url = "{{url('claim/notification_report/return_disease_agent')}}/" + agent;

                $.ajax({
                 type: 'get',
                 url: url,
                 success:function(response){
                    if (response.length > 0) {
                        var resp = '';
                        for(var i in response){
                            resp +=
                            '<option value="'+response[i].id+'" style="width:400px;">'+response[i].name+'</option>';
                        }
                        $('.specific-disease-agent').empty();
                        $('.specific-disease-agent').append(resp);
                        $('.specific-disease-agent-class').show();

                    }else{
                        
                        $('.specific-disease-agent-class').hide();
                        $('.specific-disease-agent').empty();
                    }
                }
            });
            } else {
                $('.specific-disease-agent-class').hide();
                $('.specific-disease-agent').empty();
            }
            

        })

        $('.target-organs').change(function(e){
            var organ = $('.target-organs option:selected').val();
            if (organ.length > 0) {
                var url = "{{url('claim/notification_report/return_disease_organ')}}/" + organ;

                $.ajax({
                 type: 'get',
                 url: url,
                 success:function(response){
                    if (response.length > 0) {
                        var resp = '';
                        for(var i in response){
                            resp +=
                            '<option value="'+response[i].id+'" style="width:400px;">'+response[i].name+'</option>';
                        }
                        $('.specific-target-organ').empty();
                        $('.specific-target-organ').append(resp);
                        $('.specific-target-organ-class').show();

                    }else{
                        
                        $('.specific-target-organ-class').hide();
                        $('.specific-target-organ').empty();
                    }
                }
            });
            } else {
                $('.specific-target-organ-class').hide();
                $('.specific-target-organ').empty();
            }
            

        })


        $('.disease-types').change(function(e){
            var input = $('.disease-types option:selected').text();
            $('#disease-types').val(input);
            
        })
        
        $('.accident-cause').change(function(e){
            var input = $('.accident-cause option:selected').text();
            $('#accident-cause').val(input);
            
        })
        

        $('.workplaces').keyup(function(e){
            $('.workplace-names').val(0);
        })


        $(document).find('.workplace-names').change(function(e){
            $('.workplaces').val($('.workplace-names option:selected').text());
        })

    });
</script>;

@endpush
