@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.add_witness'), 'header_title' => trans('labels.backend.claim.add_witness')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_report, ['route' => ['backend.claim.notification_report.store_accident_witness', $notification_report->id ],'method'=>'post', 'name' => 'store_accident_witness']) !!}
    {!! Form::hidden('request_action_type', 1) !!}

    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $notification_report])

    <div class="image-blockquote">
    <div class="row">
        <div class="col-md-6 offset-md-3">

                @include("backend.includes.registration.claim.witness.create")


        </div>
    </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.claim.notification_report.profile', trans('buttons.general.cancel'), [$notification_report->id], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    {{--</section>--}}

@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $(function () {

    });
</script>;

@endpush
