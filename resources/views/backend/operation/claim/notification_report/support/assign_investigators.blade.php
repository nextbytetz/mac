@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.assign_investigators'), 'header_title' => trans('labels.backend.claim.assign_investigators')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($notification_report,['route' => ['backend.claim.notification_report.store_investigators',$notification_report->id ],'method'=>'post',
    'id' => 'create']) !!}



    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])

    <div>&nbsp;</div>

    {{--main contants--}}

    {{--user id 1--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.investigator'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('investigator[]', $users, null, ['style' => 'width:100%','class' => 'form-control search-select', 'id'=> 'investigator', 'multiple' => 'true']) !!}
                        {!! $errors->first('investigator', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>


    {{--user id 2--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="element-form" >--}}
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.claim.investigator_2'):</label></div>--}}
                {{--<div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">--}}
                    {{--<div class="form-group">--}}
                        {{--{!! Form::select('investigator_2', $users, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'investigator_2']) !!}--}}
                        {{--{!! $errors->first('investigator_2', '<span class="help-block label label-danger">:message</span>') !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


        {{--</div>--}}
    {{--</div>--}}




    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.claim.notification_report.profile',trans('buttons.general.cancel'), [$notification_report->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $(function () {
        $(".search-select").select2({
            allowClear: true,
            placeholder: ""

        });
    });


</script>;


@endpush
