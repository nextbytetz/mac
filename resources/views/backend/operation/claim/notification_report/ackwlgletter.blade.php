@extends('layouts.backend.main', ['title' => "Acknowledgement Letter", 'header_title' => "Acknowledgement Letter"])

@push('after-styles-end')

<style>
    .mce-ico.mce-i-fa {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    .tinymce-content p {
        padding: 0;
        margin: 2px 0;
    }
    td {
        padding:0; margin:0;
    }
</style>

@endpush

@section('content')
    <div class = "row">
        {{--{!! Form::model($notification_report, ['route' => ['backend.claim.notification_report.profile', $notification_report->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'get']) !!}--}}
        @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=> $notification_report])
        {{--{!! Form::close() !!}--}}
    </div>
    <!-- Put the page specifically for this page here -->
    {{--<button class="editor_save">Save</button>--}}
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => ['backend.claim.notification_report.ackwlgletter.save', $notification_report->id], 'class' => 'save_acknowledgement_letter', 'method' => 'PUT', 'id' => "save_acknowledgement_letter", 'enctype' => 'multipart/form-data']) !!}
                <textarea name="acknowledgement_letter" id="acknowledgement_letter">
                    {{--@include("backend/operation/claim/notification_report/includes/letters/acknowledgement/sample",['notification_report'=> $notification_report, 'pending_documents' => $pending_documents])--}}
                    @if (!$notification_report->letter()->count())
                        @if (in_array($notification_report->incident_type_id, [1,4]))
                            @include("backend/operation/claim/notification_report/includes/letters/acknowledgement/accident",['notification_report'=> $notification_report, 'pending_documents' => $pending_documents, 'folio' => $folio])
                        @elseif(in_array($notification_report->incident_type_id, [2,5]))
                            @include("backend/operation/claim/notification_report/includes/letters/acknowledgement/disease",['notification_report'=> $notification_report, 'pending_documents' => $pending_documents, 'folio' => $folio])
                        @elseif($notification_report->incident_type_id == 3)
                            @include("backend/operation/claim/notification_report/includes/letters/acknowledgement/death",['notification_report'=> $notification_report, 'pending_documents' => $pending_documents, 'folio' => $folio])
                        @endif
                    @else
                        {!! $notification_report->letter->acknowledgement !!}
                    @endif
                </textarea>
            {!! Form::close() !!}

        </div>
    </div>


@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/tinymce/tinymce.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script>
    $(function () {
        var $editor = tinymce.init({
            selector:'textarea#acknowledgement_letter',
            branding: false,
            elementpath: false,
            resize: 'height',
            //content_style: "body {padding: 4mm, width: 206mm; margin: 0;}",
            height : 1400,
            //width : '206mm', /*210mm size of A4, skipping 4mm on both sides*/
            skin: 'lightgray',
            statusbar: true,
            images_upload_url: '',
            relative_urls: false,
            automatic_uploads: false,
            forced_root_block: false,
            plugins : 'advlist autolink link image table lists charmap print preview code media insertdatetime searchreplace save wordcount fullpage lineheight textcolor',
            toolbar: 'save | print download | undo redo | fontsizeselect | fontselect | lineheightselect | bold italic underline | bullist numlist | indent outdent | link image | forecolor backcolor',
            fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
            lineheight_formats: "2pt 4pt 6pt 8pt 10pt 12pt 14pt 16pt",
            //font_formats: 'Tahoma=serif;Times New Roman=times new roman,times;Verdana=verdana,geneva;',
            save_onsavecallback: function () {
                //Put logics to save the form here ...
                $(".save_acknowledgement_letter").submit();
            },
            save_oncancelcallback: function () {
                //Put logics to cancell the save ...
                console.log('Save Canceled');
            },
            init_instance_callback: function (editor) {
                editor.on('ExecCommand', function (e) {
                    if (e.command === 'mcePrint') {
                        //alert('Print');
                        return $.post( "{!! route('backend.claim.notification_report.ackwlgletter.issue', $notification_report->id) !!}", {}, function( data ) {
                            /* console.log(data); */
                        }, "json");
                    }
                });
            },
            /*alignleft aligncenter alignright alignjustify*/
            browser_spellcheck : true,
            file_browser_callback: function(field_name, url, type, win) {
                win.document.getElementById(field_name).value = '/public/template/assets/nextbyte/img/';
            },
            file_browser_callback_types: 'file image media',
            setup: function(editor) {
                editor.addButton('download', {
                    text: '',
                    tooltip: "Download",
                    icon: "fa fa-download",
                    onclick: function() {
                        //Download the form here ...
                        console.log('Downloaded ...');
                    }
                });
            }
        });
        $('body').on('submit', 'form.save_acknowledgement_letter', function (e) {
            e.preventDefault();
            var form = this;
            var $options = {
                dataType : "json",
                type : "POST",
                url : $(form).attr("action"),
                success : function (data) {
                    if (data.success) {
                        console.log('Saved in database ...');
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    /* console.log(errors); */
                    $.each(errors, function(index, value) {

                    });
                }
            };
            // pass options to ajaxForm
            $(form).ajaxSubmit($options);
        });
    });
</script>

@endpush