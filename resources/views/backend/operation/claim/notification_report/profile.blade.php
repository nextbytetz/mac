@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.notification_report_profile'), 'header_title' => trans('labels.backend.claim.notification_report_profile')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/folder-tree.css") }}
    <style>
        /* start: upload progress bar css */
        .progress-bar {
            background-color: #12CC1A;
            height:20px;
            color: #FFFFFF;
            width:0%;
            -webkit-transition: width .3s;
            -moz-transition: width .3s;
            transition: width .3s;
        }
        .progress-div {
            border:#0FA015 1px solid;
            padding: 5px 0px;
            margin:30px 0px;
            border-radius:4px;
            text-align:center;
        }
        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row">
        {{--{!! Form::model($notification_report, ['route' => ['backend.claim.notification_report.profile', $notification_report->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'get']) !!}--}}
        @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=> $notification_report])
        {{--{!! Form::close() !!}--}}
    </div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab" >
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>
                    {{--document centre--}}
                    <li class="nav-item">
                        <a class="nav-link"  id = "tab_2_header"  href="#document" data-toggle="tab">@lang('labels.backend.claim.document_centre')</a>
                    </li>
                    @if($notification_report->need_investigation == 1)
                        {{--investigation--}}
                        <li class="nav-item">
                            <a class="nav-link" id = "tab_3_header"
                               href="#investigation" data-toggle="tab">@lang('labels.backend.claim.investigation')</a>
                        </li>
                    @endif
                    {{--medical Expense--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "medical_expense_tab"
                           href="#medical_expense" data-toggle="tab">Medical Expense</a>
                    </li>

                    {{--medical care--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "tab_4_header"
                           href="#medical" data-toggle="tab">@lang('labels.backend.claim.medical_care')</a>
                    </li>

                    {{--Current Employee State--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "tab_5_header"
                           href="#current_employee_state" data-toggle="tab">@lang('labels.backend.claim.current_employee_state')</a>
                    </li>


                    {{--Claim Compensation Summary--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "tab_6_header"
                           href="#claim_compensation_summary" data-toggle="tab">@lang('labels.backend.claim.compensation_summary')</a>
                    </li>


                    {{--Bank Details--}}
                    <li class="nav-item">
                        <a class="nav-link" id = "bank_details_header"
                           href="#bank_details" data-toggle="tab">@lang('labels.backend.claim.bank_details')</a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">
                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >

                                        @if($check_workflow == 0 || $check_if_is_level1_pending == 1)

                                            {{--Approve--}}
                                            @if(!$check_workflow)
                                            <span>

                                           {!! HTML::decode(link_to_route('backend.claim.notification_report.approve_to_claim', "<i class='icon fa fa-check-circle' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.initiate'), $notification_report->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.claim.approve_to_claim_confirm'), 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                            @endif


                                            {{--Document register--}}
                                            <span>
                                                <a href="{!! route('backend.claim.notification_report.edit_documents', $notification_report->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-folder-open-o"></i>&nbsp;@lang('buttons.backend.claim.document_register')</a>
                                            </span>


                                            {{--add medical expense--}}
                                            {{--<span>--}}
                                            {{--<a href="{!! route('backend.claim.notification_report.create_medical_expense', $notification_report->id)--}}
                                            {{--!!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-medkit"></i>&nbsp;@lang('buttons.backend.claim.add_medical_expense')</a>--}}
                                            {{--</span>--}}


                                            {{--Modify application--}}
                                            <span>
                                        <a href="{!! route('backend.claim.notification_report.modify', $notification_report->id)
                                        !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pencil-square-o"></i>&nbsp;@lang('buttons.backend.claim.modify_notification')</a>
    </span>

                                            {{--end check if level 1 pending--}}
                                        @endif
                                        {{--More--}}
                                        @include('backend.operation.claim.notification_report.includes.more_links')

                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--assessment_opener overview--}}
                                    @include('backend.operation.claim.notification_report.includes.assessment_opener')

                                    <div>&nbsp;</div>
                                    {{--main workflow tracks--}}
                                    <div>&nbsp;</div>

                                    <div>&nbsp;</div>

                                    {{--Approved--}}
                                    {{--DO Note & Workflow Track Overview--}}
                                    @if ($notification_report->status == 1 Or $notification_report->status == 0)
                                        @php
                                            $workflow = new \App\Services\Workflow\Workflow(['wf_module_group_id' => 3, 'type' => 1, 'resource_id' => $notification_report->id]);
                                            /*$approval_note = (new \App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository())->getLegacyPayNoteForApproval($notification_report);*/
                                            if ($workflow->hasActiveWfDefinition()) {
                                                if ($workflow->currentWfDefinition()->has_note) {
                                                    $approval_note = (new \App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository())->getLegacyPayNoteForApproval($notification_report);
                                                }
                                            }

                                            $workflowinput = ['resource_id' => $notification_report->id, 'wf_module_group_id'=> 3, 'type' => -1];
                                        @endphp

                                        {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
                                    @endif
                                    {{--Rejected--}}
                                    @if ($notification_report->status == 2)
                                        @php
                                            $workflowinput = ['resource_id' => $notification_report->id, 'wf_module_group_id'=> 4, 'type' => -1];
                                        @endphp
                                        {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
                                    @endif


                                    {{--Workflow tracks for Reversed/Appealed- OLD (Deactivated)--}}
                                    @if (($notification_report->notificationReportAppeal()->count()) || $notification_report->is_reversed == 1 )

                                        @include('backend.operation.claim.notification_report.includes.workflow_track_deactivated', ['notification_report_id' => $notification_report->id])
                                    @endif

                                    <br/>

                                    @if (!empty($approval_note))
                                        <div>
                                            <b class="underline">System Generated Payment Note</b>&nbsp;&nbsp;<small><button  data-clipboard-action="copy" data-clipboard-target="#approval_note" href="#" class="btn btn-sm btn-secondary copy_approval_note" type="button">Copy Note</button></small>
                                            <div id="approval_note">{!! $approval_note !!}</div>
                                        </div>


                                        <hr/>
                                    @endif
                                    {{--compensation summary--}}
                                    {{--<br/>--}}
                                    @if (($notification_report->claim()->count()))
                                        @include('backend/operation/claim/notification_report/includes/legacy_claim_compensation_summary/general_tab_compensation_summary')
                                    @endif
                                    {{--end compensation summary--}}


                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend.operation.claim.notification_report.includes.sidebar_summary')
                                    <div>&nbsp;</div>
                                    {{--Incident summary--}}
                                    @include('backend.operation.claim.notification_report.includes.sidebar_incident_summary')
                                    <div>&nbsp;</div>
                                    {{--approval appeal summary--}}
                                    @if (($notification_report->notificationReportAppeal()->count()))
                                        @include('backend.operation.claim.notification_report.includes.appeal_approval_summary', ['notification_report' => $notification_report])
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--document--}}
                    <div id="document" class="nav_tab_pane tab-pane">
                        <div class = "row">
                            <div class="col-md-12">
                                {{-- Document Centre Show --}}
                                @include('backend/operation/claim/notification_report/includes/document_centre')
                            </div>
                        </div>
                    </div>



                    {{--investigation--}}
                    <div id="investigation" class="nav_tab_pane tab-pane">
                        <div class = "row">
                            <div class="col-md-12">
                                {{--<div class="row">--}}
                                {{--investigations--}}
                                <div class="col-md-9">
                                    {{--investigation reportd --}}
                                    @include('backend.operation.claim.notification_report.includes.investigation_report')

                                    {{--<div>&nbsp;</div>--}}
                                    {{--main workflow tracks--}}
                                    {{--@include('backend.operation.claim.notification_report.includes.bottom_tab_navigation')--}}

                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary ->Investigators--}}
                                    @include('backend.operation.claim.notification_report.includes.investigators')
                                </div>
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>



                    {{--Medical Expense--}}
                    <div id="medical_expense" class="nav_tab_pane tab-pane">
                        <div class = "row">
                            <div class="col-md-12">
                                {{-- Document Centre Show --}}
                                @include('backend/operation/claim/notification_report/includes/medical_expenses_tab')
                            </div>
                        </div>
                    </div>


                    {{--Medical--}}
                    <div id="medical" class="nav_tab_pane tab-pane">
                        <div class = "row">
                            <div class="col-md-12">

                                {{--health care provider--}}
                                @include('backend.operation.claim.notification_report.includes.health_provider_service_index')

                            </div>
                        </div>
                    </div>
                    {{--current employee state--}}
                    <div id="current_employee_state" class="nav_tab_pane tab-pane">
                        <div class = "row">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    {{--Disability State--}}
                                    <legend class="grey_modal" >@lang('labels.backend.claim.disability_state')</legend>
                                    <div class="pull-right">
                                        {{--add new--}}
                                        @if($check_workflow == 1 && $check_if_is_assessment_level_pending == 1)
                                            <span>
        <a href="{!! route('backend.claim.notification_report.create_current_employee_state', $notification_report->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
        </span>
                                        @endif
                                    </div>
                                    @include('backend.operation.claim.notification_report.includes.current_employee_disability_state')
                                </div>
                                <div class="col-md-3">
                                    {{--health State--}}
                                    @include('backend.operation.claim.notification_report.includes.current_employee_health_state')
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--compensation_summary--}}
                    <div id="claim_compensation_summary" class="nav_tab_pane tab-pane">
                        @if (($notification_report->Claim))

                            @include('backend.operation.claim.notification_report.includes.claim_compensation_summary')
                        @endif
                    </div>


                    {{--bank details--}}
                    <div id="bank_details" class="nav_tab_pane tab-pane">

                        @include('backend.operation.claim.notification_report.includes.bank_details')

                    </div>

                </div>
            </div>
        </div>

    </div>

    @if(!$check_workflow)
        <div class="modal hide fade" id="reject_notification_modal" role="dialog" aria-labelledby="workflow_modal" aria-hidden="true">
            <div class="modal-dialog white_modal" role="document">
                <div class="modal-content" id="modal-content">
                    @include("backend/includes/reject_notification_reason", ['notification_report' => $notification_report])
                </div>
            </div>
        </div>
    @endif

@stop


@push('after-script-end')

    @stack('medical-expenses-overview-script-end')
    @stack('investigation-report-script-end')
    @stack('investigators-script-end')
    @stack('health-provider-services-script-end')
    @stack('disability-state-script-end')
    @stack('health-state-script-end')
    @stack('bank-details-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/clipboard/clipboard.min.js") }}

    <script  type="text/javascript">

       // $(function () {
       $(document).ready(function(){
            $(".search-select").select2();

           new ClipboardJS('.copy_approval_note');

            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show').trigger('click');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var $tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + $tab);
                } else {
                    location.hash = '#' + $tab;
                }
            });

            $('body').on('submit', 'form[name=reject_notification_form]', function(e) {
                e.preventDefault();
                var $form = this;
                swal({
                    title: "Warning",
                    text: "Are you sure to reject this notification?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        var $comment = $("#reject_notification_comment");
                        if (validate_entry($comment.val(), $comment)) {
                            // submit form
                            $form.submit();
                        }
                    }
                });
            });

        });
        function validate_entry(selected_entries, control) {
            if (selected_entries === '') {
                control.addClass('form-error');
                setTimeout(
                    function() { control.removeClass('form-error'); },
                    2000
                );
                return false;
            } else {
                return true;
            }
        }
    </script>;

@endpush
