@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.modify_accident_notification'), 'header_title' => trans('labels.backend.claim.modify_accident_notification')])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/timepicki/css/timepicki.css") }}
    <style>
        .incident_witness:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Incident Witness";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }

        .incident_witness {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
        .next_of_kin:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Next of Kin Information";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .next_of_kin {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush

@include('backend.includes.assets.datetimepicker')
@include('backend.includes.assets.tinymce')

@section('content')
    {{--<section id="content-wrapper">--}}

    @if(empty($employee))
        @include("backend.operation.claim.notification_report.includes.registration.accident.edit_unregistered")
    @else
        @include("backend.operation.claim.notification_report.includes.registration.accident.edit_registered")
    @endif

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/timepicki/js/timepicki.js") }}
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/autocomplete/js/jquery.disableAutoFill.min.js") }}--}}

    <script  type="text/javascript">
        $(function () {

            autosize($("textarea.autosize"));
            $('.search-select').select2();
            $('#district_id').select2();
            /*$('.notification_form').disableAutoFill();*/

            $('body').on('submit', 'form[name=create_notification]', function (e) {
                e.preventDefault();
                this.submit();
            });
            $('.timepicker').timepicki({
                step_size_minutes:1,
                overflow_minutes:false,
                start_time: [{!! $accident_time->format('h') !!}, {!! $accident_time->format('i') !!}, "{!! $accident_time->format('A') !!}"],
                increase_direction:'up'
            });

            $('#region_id').on('change', function (e) {
                $("#spin2").show();
                var region_id = e.target.value;
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district_id').empty();
                    $("#district_id").select2("val", "");
                    $('#district_id').html(data);
                    $("#spin2").hide();
                }).done(function() {
                    $('#district_id').val({{ $incident->district->id }}).trigger('change.select2');
                });
            });
            $('#region_id').trigger('change');
        });
    </script>;


@endpush
