@extends('layouts.backend.main', ['title' => "Update Manual Payment", 'header_title' => "Update Manual Payment"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */
        .progress-bar {
            background-color: #12CC1A;
            height:20px;
            color: #FFFFFF;
            width:0%;
            -webkit-transition: width .3s;
            -moz-transition: width .3s;
            transition: width .3s;
        }
        .progress-div {
            border:#0FA015 1px solid;
            padding: 5px 0px;
            margin:30px 0px;
            border-radius:4px;
            text-align:center;
        }
        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])
    </div>
    <div class="row">
        {{--HEADER--}}
        {{--header--}}
        <div>&nbsp;<br/></div>
        {!! Form::model($incident, ['route' => ['backend.claim.notification_report.do_manual_payment.post', $incident->id], 'name' => 'update_manual_payment', 'class' => 'update_manual_payment', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('incident_id', $incident->id) !!}
        {!! Form::hidden('incident_type_id', $incident->incident_type_id) !!}
        <div class="col-md-12">
            <div class="row">
                @if($incident->incident_type_id == 1 Or $incident->incident_type_id == 2)
                    {{--Accident and Disease--}}
                    <div class="col-md-6">
                        <div class="fileld-layout">
                            <label>TTD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_ttd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                </div>
                                <span class="help-block">
                                        <p>Temporary Total Disablement Paid</p>
                                    </span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>PD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_pd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                </div>
                                <span class="help-block">
                                        <p>Percentage Disability Assessed</p>
                                    </span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>MAE</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_mae', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                </div>
                                <span class="help-block">
                                        <p>Medical Aid Expense Paid</p>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="fileld-layout">
                            <label>TPD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_tpd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                </div>
                                <span class="help-block">
                                        <p>Temporary Partial Disablement Paid</p>
                                    </span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>PPD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_ppd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                </div>
                                <span class="help-block">
                                        <p>Permanent Partial Disablement Paid</p>
                                    </span>
                            </div>
                        </div>
                    </div>
                @else
                    {{--Death--}}
                    <div class="col-md-6">
                        <div class="fileld-layout">
                            <label>Funeral Grant</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_funeral_grant', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                </div>
                                <span class="help-block">
                                        <p>Funeral Grant Paid</p>
                                    </span>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a href="{{ route("backend.claim.notification_report.profile", $incident->id) }}" class="  btn-secondary site-btn">Cancel</a>
                        <input type="submit" class="btn btn-success btn-submit" value="Update" />
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            /* start: Submitting Form and perform validation on the server side */
            $('body').on('submit', 'form.update_manual_payment', function (e) {
                e.preventDefault();
                var $form = this;
                swal({
                    title: "Warning",
                    text: "Are you sure to Update Manual Payment?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        /* start: remove any printed error message in the input controls */
                        $($form).find(':input').each(function () {
                            var $name = $(this).attr('name');
                            $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                        });
                        /* end: remove any printed error message in the input controls */
                        var $options = {
                            dataType : "json",
                            type : "POST",
                            url : $($form).attr("action"),
                            success : function (data) {
                                $($form).find(".btn-submit").prop('disabled', false);
                                document.location.href =  base_url + "/claim/notification_report/profile/{{ $incident->id }}";
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                /* console.log(errors); */
                                $.each(errors, function(index, value) {
                                    $($form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                    $($form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                });
                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
        });
    </script>
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush