@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.register_disease_notification'), 'header_title' => trans('labels.backend.claim.register_disease_notification')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
<style>
    .next_of_kin:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "Next of Kin Information";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }
    .next_of_kin {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
</style>
@endpush

@include('backend.includes.assets.datetimepicker')
@include('backend.includes.assets.tinymce')

{{--start : Registration Modals--}}
@include('backend.includes.modals.claim.create_hsp')
@include("backend.includes.modals.claim.create_medical_practitioner")
{{--end : Registration Modals--}}

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    @if(empty($employee))
        @include("backend.operation.claim.notification_report.includes.registration.disease.create_unregistered")
    @else
        @include("backend.operation.claim.notification_report.includes.registration.disease.create_registered")
    @endif

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/tinymce/tinymce.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">

    $('body').on('submit', 'form[name=store_new]', function(e) {
        e.preventDefault();
        this.submit();
    });

    $(function () {
        $('.search-select').select2();
        $('#region_id').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                $('#district_id').empty();
                $("#district_id").select2("val", "");
                $('#district_id').html(data);
                $("#spin2").hide();
            });
        });
        $('#region_id').trigger("change");
        $(".practitioner-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.claim.medical_practitioners') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.full_name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $(".health-provider-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.claim.registered_health_providers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.full_info,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });

    });

</script>;


@endpush
