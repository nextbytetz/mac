@extends('layouts.backend.main', ['title' => "Online Notification Account Validated", 'header_title' => "Online Notification Account Validated"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <!-- Put the page specifically for this page here -->
    <legend>Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $cr->description }}</small></span></legend>

    <!--Include Report-->
    @include('backend/report/configurable/includes/content', [
                                                                'cr' => $cr,
                                                                'cr_params' => [
                                                                    'hasfilter' => 1,
                                                                    'shouldrefresh' => 1,
                                                                    'canbuild' => 1,
                                                                    'buttons' => 1
                                                                ],
                                                            ])

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->

@endpush