@extends('layouts.backend.main', ['title' => "Online Notification Account Pending Validation", 'header_title' => "Online Notification Account Pending Validation"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12">
            {{--HEADER--}}
           {{-- @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])--}}
            <div class="nav_tab_pane_header">
                <div class="row">
                    <div class="col-md-12" >

                        <div class="pull-right" >
                            @include("backend.operation.claim.notification_report.portal.includes.account_links")
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>

            @if ($count)
                {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
            @else
                <span class="underline" style="font-weight: 400;font-size: 14px;">No online notification application account pending for validation</span>
            @endif

        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {!! $dataTable->scripts() !!}
@endpush