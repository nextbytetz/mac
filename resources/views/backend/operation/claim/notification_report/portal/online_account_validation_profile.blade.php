@extends('layouts.backend.main', ['title' => "Online Notification Account Profile", 'header_title' => "Online Notification Account Profile"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{--{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/7.33.1/css/sweetalert2.min.css") }}--}}
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    <style>
        /* start: File Icons CSS */
        #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
        #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
        #folder-tree .file-pdf { background-position: -32px 0 }
        #folder-tree .file-as { background-position: -36px 0 }
        #folder-tree .file-c { background-position: -72px -0px }
        #folder-tree .file-iso { background-position: -108px -0px }
        #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
        #folder-tree .file-cf { background-position: -162px -0px }
        #folder-tree .file-cpp { background-position: -216px -0px }
        #folder-tree .file-cs { background-position: -236px -0px }
        #folder-tree .file-sql { background-position: -272px -0px }
        #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
        #folder-tree .file-h { background-position: -488px -0px }
        #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
        #folder-tree .file-php { background-position: -108px -18px }
        #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
        #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
        #folder-tree .file-rb { background-position: -180px -18px }
        #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
        #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
        #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
        #folder-tree .file-js { background-position: -434px -18px }
        #folder-tree .file-css { background-position: -144px -0px }
        #folder-tree .file-fla { background-position: -398px -0px }
        /* end: File Icon CSS */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {{--HEADER--}}
    {{--@include("backend.operation.compliance.member.employer.includes.header_info", ['employer'=> $incident->employer])--}}
    <div class = "row">
        {{--{!! Form::model($employee, ['route' => ['backend.compliance.employee.profile', $employee->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        <div class="col-md-12 col-sm-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name', $claim->firstname . " " . $claim->middlename . " " . $claim->lastname , [ 'id'=> 'name']) !!}</strong>
                <small>
                    ID_#&nbsp;:&nbsp;<span class="underline">{!! Form::label( 'reg_no', $claim->id, [ 'id'=> 'reg_no']) !!}</span>
                </small>
                <small>
                    Phone&nbsp;:&nbsp;<span class="underline">{{ $claim->phone }}</span>
                </small>
                <small>
                    Employer&nbsp;:&nbsp;<a href="{{ route("backend.compliance.employer.profile", $claim->resource_id) }}"><span class="underline">{{ $claim->employer->name }}</span></a>
                </small>
            </h5>
        </div>
    </div>
    {{--<br/>--}}
    {{--<br/>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="divider15"></div>
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="notification_process_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#document_centre" role="tab">
                            <i class="icon fa fa-folder-open" aria-hidden="true"></i>Document Centre
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">

                <div class="tab-pane active" id="general_information" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12 pills-height">

                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >
                                            {{--Action Links--}}
                                                <span>
                                                    <a href="{{ route("backend.compliance.employer.profile", $claim->resource_id) }}"  class="btn btn-primary site-btn nav_button" >
                                                        <i class="icon fa fa-level-up" aria-hidden="true"></i>
                                                    Employer Profile</a>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {{--Notification Stagings Logs--}}
                                            <legend class="grey_modal" >Account Activity</legend>
                                            {{--<div class="label label-info">Notification <b>{{ $incident->NotificationReport->filename }}</b> </div>--}}
                                            <legend></legend>
                                            <div id="activities_contents">
                                                @include("backend/operation/claim/notification_report/portal/includes/account_activity", ['activities' => $claim->activities()])
                                            </div>
                                        </div>
                                    </div>
                                    {{--<legend class="grey_info" >@lang('labels.backend.member.verification.requested')&nbsp;:&nbsp;<strong>{{ $incident->user_formatted }}</strong></legend>--}}
                                    <br/>
                                    @if (!$claim->validated And $claim->isactive)
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="#" class="btn btn-block btn-danger input-radius deactivate_account">Deactivate</a>
                                                {{--{!! Html::decode(link_to_route('backend.claim.notification_report.online_account_validate_action', "<i class='icon fa fa-close' aria-hidden='true'></i>&nbsp;Deactivate" , ["claim" => $claim->id, "action" => 0], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to deactivate this user?", 'class' => 'btn btn-block btn-danger input-radius'])) !!}--}}
                                            </div>
                                            <div class="col-md-6">
                                                {{--<a href="#" class="btn btn-block btn-success input-radius">Validate</a>--}}
                                                {!! Html::decode(link_to_route('backend.claim.notification_report.online_account_validate_action', "<i class='icon fa fa-check-square-o' aria-hidden='true'></i>&nbsp;Validate" , ["claim" => $claim->id, "action" => 1], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure to validate this user?", 'class' => 'btn btn-block btn-success input-radius'])) !!}
                                            </div>
                                        </div>
                                    @endif

                                    <br/>
                                    <div style="display: none;" id="deactivate_reason_div">
                                        <div class="underline">Deactivate Reason</div>
                                        <div  style="border: 1px dotted #000;padding: 2px;">
                                            <br/>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-row">
                                                        {!! Form::open(['role' => 'form', 'id' => 'incident_comment_form', 'route' => ['backend.claim.notification_report.online_account_validate_action', $claim->id, 0], 'class' => 'deactivate_reason_form']) !!}
                                                        <div class="form-group col-md-10">
                                                            <label for="account_activity">Please Select:</label>
                                                            {!! Form::select('account_activity_cv_id', $deactivate_reasons, null, ['style' => 'width:100%', 'class' => 'form-control search-select', 'id' => 'account_activity']) !!}
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label for="account_activity_submit">&nbsp;</label>
                                                            <input type="submit" class="form-control btn btn-success btn-sm btn-block" value="Submit" id="account_activity_submit">
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-md-4">
                                    @include("backend/operation/claim/notification_report/portal/includes/account_summary")
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="document_centre" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12 pills-height">

                            <div class="row">
                                <div class="col-md-4">

                                    <legend>Attached document</legend>
                                    <div id="folder-tree" class="nopadding"></div>

                                    <br/>
                                    <br/>
                                </div>
                                <div class="col-md-8">
                                    {{--Document Preview--}}
                                    <legend>Document Preview</legend>
                                    <br/>
                                    <div id="document_frame">
                                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/7.33.1/js/sweetalert2.min.js") }}--}}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

    <script>
        $(function() {
            var $folder = $('#folder-tree');

            $(".search-select").select2({});

            $("a.deactivate_account").click(function() {
                $("#deactivate_reason_div").show();
                //alert("You've clicked the link.");
            });
            $('body').on('submit', 'form.deactivate_reason_form', function ($e) {
                $e.preventDefault();
                let $form = this;
                $form.submit();
            });

            /************************** start: Document Library JS ***************************/
            $folder.jstree({
                'core': {
                    'data': {
                        'url': "{!! route('backend.claim.notification_report.online_account_document_library', $claim->id) !!}",
                        'data': function (node) {
                            return {'id': node.id};
                        }
                    },
                    'check_callback': function (o, n, p, i, m) {
                        if (m && m.dnd && m.pos !== 'i') {
                            return false;
                        }
                        if (o === "move_node" || o === "copy_node") {
                            if (this.get_node(n).parent === this.get_node(p).id) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'force_text': false,
                    'themes': {
                        'responsive': true,
                        'variant': 'small',
                        'stripes': true
                    }
                },
                'sort': function (a, b) {
                    return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
                },
                'types': {
                    'default': {'icon': 'folder'},
                    'file': {'valid_children': [], 'icon': 'file'}
                },
                'unique': {
                    'duplicate': function (name, counter) {
                        return name + ' ' + counter;
                    }
                },
                'plugins': ['state', 'dnd', 'sort', 'types', 'unique']
            });
            $folder.bind("click.jstree", function ($event, $data) {
                var $node = $($event.target).closest("li").attr("id");
                var $type = $folder.jstree().get_selected(true)[0].type;
                if ($type === 'default') {
                    /*should only work on folders*/
                    /*Document Folder Has Been Clicked*/
                    $node = $node.split('/');
                    $node = $node[$node.length - 1];
                    if ($node.length > 0) {
                        //alert(1);
                    } else {
                        //alert(2);
                    }
                } else {
                    /*preview the document here*/
                    /*console.log($node);*/
                    let $document_frame = $("#document_frame");
                    get_current_document_from_path($node).done(function ($data) {
                        /*console.log($data);*/
                        $document_frame.find("iframe").remove();
                        let $iframe = $('<iframe allowfullscreen src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                        $document_frame.append($iframe);
                    });
                }
            });
        });
        function get_current_document_from_path($node) {
            $refNode = $node;
            /*return $.post( base_url + "/claim/notification_report/current_base64_document/{!! $claim->id !!}/" + $node, {}, function( data ) {
        }, "json");*/
            return $.ajax({
                url: base_url + "/claim/notification_report/current_online_account_document_from_path/{!! $claim->id !!}/" + $node,
                dataType : 'json',
                async : false,
                method : "POST"
            });
        }
    </script>

@endpush