{{--Pending List--}}
<span>
    <a href="{!! route('backend.claim.notification_report.online_account_validation') !!}"  class="btn btn-success site-btn nav_button" ><i class="icon fa fa-navicon"></i>&nbsp;Pending Validation</a>
</span>

{{--Worked List--}}
<span>
    <a href="{!! route('backend.claim.notification_report.online_account_validated') !!}"  class="btn btn-secondary site-btn nav_button" ><i class="icon fa fa-check"></i>&nbsp;Validated</a>
</span>