@if ($activities->count())
    <div data-plugin="custom-scroll" data-height="100">
        {{--data-height="100"--}}
        <table class="table table-striped table-bordered">
            <tbody>
                @foreach($activities->get() as $activity)
                    @php
                        $description = $activity->description;
                    @endphp
                    <tr>
                        <td><b>{{ $activity->created_at_formatted }}</b></td>
                        <td><b>{{ $activity->staff->name }}</b></td>
                        <td>{{ $description->code->name }}</td>
                        <td>{{ $description->name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Activity.</strong> This account has no any associated activities.
        </div>
    </div>
@endif
