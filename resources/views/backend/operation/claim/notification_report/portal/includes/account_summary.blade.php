<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h5><b><span class="light_dark_color">Account Summary</span></b></h5></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 light_grey_bg">
        {{--<div class="light_grey_bg">--}}
        <br/>
        <h5 class="underline text-center">{!! $claim->validated_formatted !!}</h5>
        <h5 class="underline text-center">{!! $claim->confirmed_formatted !!}</h5>
        <h5 class="underline text-center">{!! $claim->active_formatted !!}</h5>
        <div class="underline">First Name&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $claim->firstname !!}</span></div>
        <div class="underline">Middle Name&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $claim->middlename !!}</span></div>
        <div class="underline">Last Name&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $claim->lastname !!}</span></div>

        <div class="underline">Email&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $claim->email }}&nbsp;&nbsp;</span></div>

        <div class="underline">Phone&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $claim->phone }}</span></div>

        <div class="underline">Submitted&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $claim->created_at_formatted }}</span></div>

        {{--</div>--}}
    </div>
</div>