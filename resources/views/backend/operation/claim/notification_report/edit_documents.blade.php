@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.edit_document'), 'header_title' => trans('labels.backend.claim.edit_document')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {{--HEADER--}}
    @include("backend/operation/claim/notification_report/includes/header_info",['notification_report'=>$notification_report])
    {!! Form::open(['route' => ['backend.claim.notification_report.update_documents', $notification_report->id], 'id' => 'edit_documents']) !!}
    {{--{!! Form::open(['route' => ['backend.claim.notification_report.reload_documents', $notification_report->id], 'id' => 'edit_documents']) !!}--}}
    <div class="all-form-section">
        <div class="row">
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p><strong>@lang('labels.backend.claim.document.grouped_documents')</strong></p>
                            @if ($document_groups->count())
                                <div id="document-tree">
                                    <ul>
                                        @foreach ($document_groups as $group)
                                            <li>{!! $group->name !!}
                                                @if ($group->documents->count())
                                                    <ul>
                                                        @foreach ($group->documents as $document)
                                                            <li id="{!! $document->id !!}">
                                                                <a data-toggle="tooltip" data-html="true" title="{!! $document->description !!}">{!! $document->name !!}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @else
                                <p>@lang('labels.backend.claim.document.no_groups')</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::hidden('documents') !!}
    {!! Form::hidden('source', 1) !!}
    <hr/>
    <br/>
    {{--<div class="pull-right">--}}
        {{--<input type="submit" class="btn btn-success btn-sm" value="@lang('buttons.general.crud.update')" />--}}
    {{--</div>--}}
    <div class="clearfix"></div>
    {!! Form::close() !!}
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/notification/document.js") }}
<script>
    $(function(){
        @foreach ($notification_report_documents as $value)
                    $('#document-tree').jstree('check_node', '#{!! $value !!}');
        @endforeach
    });
</script>
@endpush