@extends('layouts.backend.main', ['title' => "Close Incident", 'header_title' => "Close Incident"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */
        .progress-bar {
            background-color: #12CC1A;
            height:20px;
            color: #FFFFFF;
            width:0%;
            -webkit-transition: width .3s;
            -moz-transition: width .3s;
            transition: width .3s;
        }
        .progress-div {
            border:#0FA015 1px solid;
            padding: 5px 0px;
            margin:30px 0px;
            border-radius:4px;
            text-align:center;
        }
        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class = "row">
        @include("backend/operation/claim/notification_report/includes/header_info", ['notification_report' => $incident])
    </div>
    <div class="row">
        {{--HEADER--}}
        {{--header--}}
        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.claim.notification_report.close_incident.post', $incident->id], 'name' => 'close_incident', 'class' => 'close_incident', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('incident_id', $incident->id) !!}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="fileld-layout">
                        <label>Reason for Closing Incident</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::textarea('reason', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                            </div>
                            <span class="help-block">
                                        <p>Specify reason(s) for closing an incident</p>
                                    </span>
                        </div>
                    </div>

                </div>

            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a href="{{ route("backend.claim.notification_report.profile", $incident->id) }}" class="  btn-secondary site-btn">Close</a>
                        <input type="submit" class="btn btn-success btn-submit" value="Save" />
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            /* start: Submitting Form and perform validation on the server side */
            $('body').on('submit', 'form.close_incident', function (e) {
                e.preventDefault();
                var $form = this;
                swal({
                    title: "Warning",
                    text: "Are you sure to close this Incident?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        /* start: remove any printed error message in the input controls */
                        $($form).find(':input').each(function () {
                            var $name = $(this).attr('name');
                            $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                        });
                        /* end: remove any printed error message in the input controls */
                        var $options = {
                            dataType : "json",
                            type : "POST",
                            url : $($form).attr("action"),
                            success : function (data) {
                                $($form).find(".btn-submit").prop('disabled', false);
                                document.location.href =  base_url + "/claim/notification_report/profile/{{ $incident->id }}";
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                /* console.log(errors); */
                                $.each(errors, function(index, value) {
                                    $($form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                    $($form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                });
                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
        });
    </script>
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush