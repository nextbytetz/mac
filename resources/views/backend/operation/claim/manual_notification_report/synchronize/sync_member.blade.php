@extends('layouts.backend.main', ['title' => "Sync Member with manual notification", 'header_title' => "Sync Member with manual notification"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <!-- Put the page specifically for this page here -->

    {!! Form::model($notification_report,['route' => ['backend.claim.manual_notification.sync_member', $notification_report->id],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}
    <p>
        1. Search Employer using, <code><b>Employer name as it appear on manual notification file</b>, use at least four letters for Employer name to search.</code>
    </p>
    <p>
        2. Search Employee using, <code><b>Employee name as it appear on manual notification file</b>,  use at least  four letters for Employee name to search.</code>
    </p>
    <hr/>
    {!! Form::hidden('return_url', $return_url) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="grid-column">
                <label>Employer - <b>{!! $notification_report->employer_name !!}</b></label>
                {!! Form::select('employer', [], null, ['class' => 'employer-select', 'style' => 'width:100%', 'id' => 'employer-select']) !!}
                {!! $errors->first('employer', '<span class="help-block label label-danger">:message</span>') !!}
                <br/>
                <br/>
                <div class="checkbox-squared">
                    {!! Form::checkbox('employer_unavailable', '1', false, ['id' => 'employer_unavailable_check', 'class' => 'employer_unavailable_check']) !!}
                    <label for="employer_unavailable_check"></label>
                    <span>
                        Register Employer
                        {{-- <small>(On checking this option, the notification will be forwarded to compliance for member registration)</small>--}}
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="grid-column employee_grid">
                <label>Employee - <b>{!! $notification_report->employee_name !!}</b></label>
                {!! Form::select('employee', [], null, ['class' => 'employee-select', 'style' => 'width:100%', 'id' => 'employee-select']) !!}
                {!! $errors->first('employee', '<span class="help-block label label-danger">:message</span>') !!}
                <br/>
                <br/>
                <div class="checkbox-squared">
                    {!! Form::checkbox('employee_unavailable', '1', false, ['id' => 'employee_unavailable_check', 'class' => 'employee_unavailable_check']) !!}
                    <label for="employee_unavailable_check"></label>
                    <span>Register Employee<small>(On checking this option, the notification will be forwarded to compliance for member registration)</small></span>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            {!! Form::submit(trans('labels.general.continue'), ['class' => 'btn btn-success btn-save btn-block']) !!}
        </div>
    </div>
    {!! Form::close() !!}
@endsection


@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script>
        let $employee_unavailable = 0;
        let $employer_unavailable = 0;
        $(function () {

            let $body = $('body');

            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        if (data.total_count === 0) {
                            $employer_unavailable = 1;
                        } else {
                            $employer_unavailable = 0;
                        }
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:select", function(e) {
                $employer_unavailable = 0;
                $('.employer_unavailable_check').removeAttr('checked');
            });

            $(".employee-select").select2({
                minimumInputLength: 3,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.compliance.employees_notification_employer') !!}",
                    dataType: 'json',
                    delay: 250,
                    type: "post",
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1,
                            employer: $(".employer-select").val()
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        if (data.total_count === 0) {
                            $employee_unavailable = 1;
                        } else {
                            $employee_unavailable = 0;
                        }
                        $('.employee_unavailable_check').removeAttr('checked');
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.employee,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:select", function(e) {
                $employee_unavailable = 0;
                $('.employee_unavailable_check').removeAttr('checked');
            });

            $body.on('change', '.employee_unavailable_check', function(e) {
                let $employee = $(".employee-select").val();
                let $checkbox = $(this);
                if (this.checked) {
                    if ($employee) {
                        prevent_check(e, $checkbox);
                    } else if (!$employee_unavailable && !$employee) {
                        prevent_check(e, $checkbox);
                        $.amaran({
                            'theme'     :'awesome warning',
                            'content'   :{
                                title:"Can't select this option",
                                message:"Please search first, system need to be sure that you attempted to search an employee and found no result.",
                                info:'',
                                icon:"icon fa fa-warning"
                            },
                            'position'  :'bottom left',
                            'outEffect' :'slideBottom',
                            'inEffect'  :'slideLeft',
                            'delay': 9000
                        });
                    }
                }
            });
            $body.on('change', '.employer_unavailable_check', function(e) {
                let $employer = $(".employer-select").val();
                let $checkbox = $(this);
                if (this.checked) {
                    if ($employer) {
                        prevent_check(e, $checkbox);
                        $(".employee_grid").show();
                    } else if (!$employer_unavailable && !$employer) {
                        prevent_check(e, $checkbox);
                        $(".employee_grid").show();
                        $.amaran({
                            'theme'     :'awesome warning',
                            'content'   :{
                                title:"Can't select this option",
                                message:"Please search first, system need to be sure that you attempted to search an employer and found no result.",
                                info:'',
                                icon:"icon fa fa-warning"
                            },
                            'position'  :'bottom left',
                            'outEffect' :'slideBottom',
                            'inEffect'  :'slideLeft',
                            'delay': 9000
                        });
                    } else {
                        $(".employee_grid").hide();
                    }
                } else {
                    $(".employee_grid").show();
                }
            });
        });
        $(".employer_unavailable_check").trigger("change");
        /* start: Prevent the checkbox from being checked when the conditions don't allow */
        function prevent_check($e, $target) {
            setTimeout(function() {
                $target.removeAttr('checked');
            }, 0);
            $e.preventDefault();
            $e.stopPropagation();
        }
        /* end: Prevent the checkbox from being checked when the conditions don't allow */
    </script>
@endpush