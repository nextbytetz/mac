
<legend class="grey_modal">All Uploaded Files - {!! $no_of_uploaded_files !!}</legend>


<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="uploaded-notification-table">
            <thead>
            <tr >
                <th>Case no</th>
                <th>Employee</th>
                <th>Employer</th>
                <th>Incident Type</th>
                <th>Incident Date</th>
                <th>Reporting Date</th>
                <th>Notification Date</th>
                <th>Date of MMI</th>
                <th>Sync Status</th>
            </tr>
            </thead>
        </table>

    </div>
</div>





@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#uploaded-notification-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.manual_notification.get_for_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'case_no', name: 'case_no'},
                    { data: 'employee', name: 'employee_name',  orderable : true, searchable : true},
                    { data: 'employer', name: 'employer_name',  orderable : true, searchable : true},
                    { data: 'incident_type' , name: 'incident_type', orderable : false, searchable : false},
                                 { data: 'incident_date_formatted', name: 'incident_date',  orderable : true, searchable : true},
                    { data: 'reporting_date_formatted', name: 'reporting_date',  orderable : true, searchable : true},
                    { data: 'notification_date_formatted', name: 'notification_date',  orderable : true, searchable : true},
                    { data: 'date_of_mmi_formatted', name: 'date_of_mmi',  orderable : true, searchable : true},
                    { data: 'status', name: 'status',  orderable : false, searchable : false},


                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
