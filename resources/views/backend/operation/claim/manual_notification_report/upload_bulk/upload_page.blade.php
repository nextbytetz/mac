@extends('layouts.backend.main', ['title' => 'Upload Bulk Notification Files', 'header_title' => 'Upload Bulk Notification Files'])

@include('backend.includes.datatable_assets')

@section('content')

@include('backend/operation/claim/manual_notification_report/upload_bulk/includes/upload_section')

<br/>
<br/>
@include('backend/operation/claim/manual_notification_report/upload_bulk/includes/get_uploaded_files')

@stop


@push('after-script-end')

    <script  type="text/javascript">


    </script>;

@endpush
