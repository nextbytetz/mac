@extends('layouts.backend.main', ['title' => 'All Manual Notifications', 'header_title' => 'All Manual Notifications'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <!-- Put the page specifically for this page here -->
    <legend>Description&nbsp;:&nbsp;&nbsp;<span class="underline"><small>{{ $cr->description }}</small></span></legend>

    <!--Include Report-->
    @include('backend/report/configurable/includes/content', [
                                                                'cr' => $cr,
                                                                'cr_params' => [
                                                                    'hasfilter' => 1,
                                                                    'shouldrefresh' => 1,
                                                                    'canbuild' => 1,
                                                                    'buttons' => 1
                                                                ],
                                                            ])
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    <script>

    </script>
@endpush