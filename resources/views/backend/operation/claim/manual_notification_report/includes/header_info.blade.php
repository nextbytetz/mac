<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5"><span  class='tag square-tag spice-color'  data-toggle='tooltip' data-html='true' title='{{ $cr->claim_status }}'> {{ $cr->claim_status }}</span></span>
    <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'name', $cr->incident_type) !!} </strong></span>

    <span class="navbar-brand mb-0 h5">
        <small >
            Case No (#)&nbsp;:&nbsp;<span class="underline"><a href="{!! route('backend.claim.manual_notification.dashboard', $incident->id) !!}">AB1/457/{!! $cr->case_no !!}</a> </span>
        </small>
        <small>
            Employer&nbsp;:<span class="underline"> <a href="{!! route('backend.compliance.employer.profile', $cr->employer_id) !!}">{{ $cr->employer }}</a>  </span>
        </small>
        <small>
            Employee&nbsp;:<span class="underline">  <a href="{!! route('backend.compliance.employee.profile', $cr->employee_id) !!}">{{ $cr->employee }}</a></span>
        </small>
    </span>
</nav>
<legend></legend>
<br/>