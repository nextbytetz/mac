<table class="table table-striped table-bordered">
    <tbody>
    <tr>
        <td>Hospital</td>
            <th>{!!  implode_collection_name($incident->hcps) !!} </th>
        <td>Incident District</td>
        <th>{{ ($incident->district_id) ? $incident->incidentDistrict->name :  $incident->district }}</th>
    </tr>

    @if ($incident->incident_type_id == 1 || $incident->incident_type_id == 2)
    <tr>

            <td>Incident District</td>
        <th>{{ ($incident->district_id) ? $incident->incidentDistrict->name :  $incident->district }}</th>
            <td>Bodily location</td>
            <th>{{($incident->bodily_location_cv_id) ? $incident->bodilyLocation->name : '-' }}</th>

    </tr>
    @endif

    @if ($incident->incident_type_id == 1 || $incident->incident_type_id == 2)
    <tr>

            <td>No. of days lost as Full day</td>
            <th>{{ $incident->day_off Or '-' }}</th>
            <td>Rehabilitation (REH)</td>
            <th>{{ $incident->rehabilitation Or '-' }}</th>

    </tr>
    @endif

    @if($incident->incident_type_id == 1 || $incident->incident_type_id == 2)
    <tr style="border: dotted #e6f5f0 2px !important;background-color: #e9e9e4;">

            <td>No. of days lost as Light duty</td>
            <th>{{ $incident->light_duties Or '-' }}</th>
            <td>Percentage of Disability</td>
            <th>{{ $incident->man_pd Or '-' }}</th>

    </tr>
    @endif

    @if($incident->incident_type_id == 2)
    <tr>

            <td>Name of disease diagnosed</td>
            <th>{{ $incident->disease_diagnosed Or '-' }}</th>
            <td>Disease caused by agent</td>
            <th>{{ $incident->disease_agent Or '-' }}</th>

    </tr>
    @endif

    @if ($incident->incident_type_id == 2)
    <tr>

            <td>Type of occupational disease</td>
            <th>{{ $incident->disease_type Or '-' }}</th>
            <td>Disease by target organ</td>
            <th>{{ $incident->disease_target_organ Or '-' }}</th>

    </tr>
    @endif

    @if ($incident->incident_type_id == 1)
    <tr>

            <td>Cause of accident: (According to type)</td>
            <th>{{  ($incident->accident_cause_type_cv_id) ? $incident->accidentCauseType->name : '-' }}</th>
            <td>Cause of accident (Acording to Agency)</td>
            <th>{{  ($incident->accident_cause_agency_cv_id) ? $incident->accidentCauseAgency->name : '-'}}</th>

    </tr>
    @endif

    </tbody>
</table>