<div class="row">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Notification Summary</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">Is Synchronized :</h6>
        <div>
            @if ($incident->ismembersynced)
                <span class="tag tag-success white_color">Yes</span>
            @else
                <span class="tag tag-danger white_color">No</span>
            @endif
        </div>
        <h6 class="underline" style="font-weight: lighter;">Information Updated :</h6>
        <div>
            @if ($incident->isupdated)
                <span class="tag tag-success white_color">Yes</span>
            @else
                <span class="tag tag-danger white_color">No</span>
            @endif
        </div>
        <h6 class="underline" style="font-weight: lighter;">Incident Date :</h6>
        <div style="font-weight: bold;">{{ $cr->incident_date }}</div>
        <h6 class="underline" style="font-weight: lighter;">Reporting Date :</h6>
        <div style="font-weight: bold;">{{ $cr->reporting_date }}</div>
        <h6 class="underline" style="font-weight: lighter;">Notification Date :</h6>
        <div style="font-weight: bold;">{{ $cr->notification_date }}</div>
        <h6 class="underline" style="font-weight: lighter;">Registration Date :</h6>
        <div style="font-weight: bold;">{{ $cr->registration_date }}</div>
        <h6 class="underline" style="font-weight: lighter;">Date of MMI :</h6>
        <div style="font-weight: bold;">{{ $cr->date_of_mmi }}</div>
        @if ($cr->reject_reason_cv_id)
            <h6 class="underline" style="font-weight: lighter;">Reject Reason :</h6>
            <div style="font-weight: bold;">{{ $cr->reject_reason }}</div>
        @endif

        @if ($incident->attended_by)
            <h6 class="underline" style="font-weight: lighter;">Attended By :</h6>
            <div style="font-weight: bold;">{{ $incident->attendedBy->username }}</div>
        @endif

    </div>
</div>
