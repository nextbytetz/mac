<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >More</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">

{{--        --}}{{--edit--}}{{--
        @if ($inspection->can_edit)
            <span>
                <a href="{!! route('backend.compliance.inspection.edit', $inspection->id) !!}"  class="dropdown-item" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>
            </span>
        @endif

        --}}{{--complete--}}{{--
        @if ($inspection->can_complete)
            <span>
                <div class="dropdown-divider"></div>
                {!! HTML::decode(link_to_route('backend.compliance.inspection.complete', "<i class='icon fa fa-check-square-o' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.complete'), [$inspection->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.complete_confirm'), 'class' => 'dropdown-item']))  !!}
            </span>
        @endif--}}
        @if($cr->isdmsposted == 0)
            <span>

              <a href='{!! route('backend.claim.manual_notification.resend_eoffice', $incident->id)  !!}' class="dropdown-item" ><i class="icon fa fa-upload"></i>&nbsp;Resend to E-office</a>
              <div class="dropdown-divider"></div>
          </span>
        @endif


        <span>
       @if(isset($incident->attended_by))
                @if($incident->attended_by == access()->id())
                    <a href='{!! route('backend.claim.manual_notification.update_attendant', ['manual_notification' => $incident->id, 'action_type' => 0])  !!}' class="dropdown-item" ><i class="icon fa fa-unlink"></i>&nbsp;Unattend this File</a>
                    <div class="dropdown-divider"></div>
                @endif
            @else
                <a href='{!! route('backend.claim.manual_notification.update_attendant', ['manual_notification' => $incident->id, 'action_type' => 1])  !!}' class="dropdown-item" ><i class="icon fa fa-link"></i>&nbsp;Attend this File</a>
                <div class="dropdown-divider"></div>
            @endif

          </span>


            <span>
                    <a href='{!! route('backend.claim.manual_notification.update_has_payroll', ['manual_notification_report' => $incident->id, 'status' => ($incident->has_payroll != 1) ? 1 : 0])  !!}' class="dropdown-item" ><i class="icon fa fa-edit"></i>&nbsp;{{ ($incident->has_payroll != 1) ? 'Add to Payroll' : 'Remove from Payroll'   }}</a>
              <div class="dropdown-divider"></div>
            </span>

        @if(env('TESTING_MODE') == 1)
            <span>
                    <a href='{!! route('backend.claim.manual_notification.update_status', ['manual_notification_report' => $incident->id, 'status' => 0])  !!}' class="dropdown-item" ><i class="icon fa fa-edit"></i>&nbsp;{{  'Suspend'  }}</a>
              <div class="dropdown-divider"></div>
            </span>

        @endif

        {{--close--}}
        <span>
            <a href="{!! route('backend.claim.manual_notification.list_all') !!}"  class="dropdown-item" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
        </span>

     </span>
</span>