<div class="nav_tab_pane_header">
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
                @if (!$cr->issynced)
                    {{--synchronize claim--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.claim.manual_notification.sync_member_page', "<i class='icon fa fa-recycle' aria-hidden='true'></i>&nbsp;" . "Synchronize", [$cr->resourceid, 1],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                @endif

                {{--Update Info--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.claim.manual_notification.updateinfo', "<i class='icon fa fa-caret-square-o-up' aria-hidden='true'></i>&nbsp;" . "Update Info", [$cr->resourceid],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>

                {{--Update Manual Payment--}}
                @if($incident->status_cv_id)
                    @if($incident->status->reference == 'MANNOTST01')
                        <span>
                        {!! HTML::decode(link_to_route('backend.claim.manual_notification.do_manual_payment', "<i class='icon fa fa-rouble' aria-hidden='true'></i>&nbsp;" . "Update Payment", [$cr->resourceid],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                    @endif
                @endif

                @if($incident->isupdated == 1)
                    <span>
                        {!! HTML::decode(link_to_route('backend.claim.manual_notification.update_osh_data', "<i class='icon fa fa-caret-square-o-up' aria-hidden='true'></i>&nbsp;" . "Update OSH Data", [$cr->resourceid],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                @endif
                {{--Update Status--}}{{--
                <span>
                    {!! HTML::decode(link_to_route('backend.compliance.inspection.task.assign', "<i class='icon fa fa-tasks' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.task.assign'), [$inspection->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.task.assign_confirm'), 'class' => 'btn btn-primary site-btn nav_button']))  !!}
                </span>--}}

                @include("backend/operation/claim/manual_notification_report/includes/dashboard_more_links")
            </div>
        </div>
    </div>
</div>