<div class="row">

    <div class="offset-md-2 col-md-8">
        <div  style="overflow: auto;white-space: nowrap;">
            <div class="underline">Sample Data</div>
            <table class="table table-responsive-md table-sm mb-0">
                <thead>
                <tr>
                    @foreach($keys as $key)
                        <th>{{ $key }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach ($samples as $sample)
                    <tr>
                        @foreach($keys as $key)
                            <td>{{ $sample[$key] }}</td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <br/>
    </div>

    <div class="offset-md-1 col-md-10">

        <?php
            $color = [
                0 => 'blue',
                1 => 'grey',
            ];
        ?>

        <legend>Map Columns</legend>
        @foreach(array_chunk($for_mappings, 3, true) as $chunks)
            <br/>
            <div class="fileld-layout" style="padding-left:20px; border-left: 2px solid {!! $color[$loop->iteration % 2] !!};border-bottom: 1px solid #d2d7da;">
                <div class="form-group">
                    <div class="form-inline">
                        @php
                            /*logger($chunks);*/
                        @endphp
                        @foreach($chunks as $key => $value)
                            <span>
                                &nbsp;&nbsp;<label class="required" style="width:10%;">{{ $value }}</label>&nbsp;&nbsp;
                            </span>
                            <span>
                                {!! Form::select($key . '_map', $keysforselect, null, ['class' => 'form-control form-control-sm search-select', 'style' => "border-radius:3px;width:18%;", 'placeholder' => '']) !!}
                            </span>

                        @endforeach
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
        @endforeach
        <br/>

    </div>

</div>
