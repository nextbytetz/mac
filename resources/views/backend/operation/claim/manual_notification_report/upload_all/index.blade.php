@extends('layouts.backend.main', ['title' => 'Upload Manual Notifications', 'header_title' => 'Upload Manual Notifications'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .input-file-container {
        position: relative;
        width: 225px;
    }

    h6 {
        width: 100%;
        text-align: center;
        border-bottom: 1px solid rgba(162, 162, 162, 0.99);
        line-height: 0.1em;
        margin: 10px 0 20px;
    }

    h6 span {
        background:#fff;
        padding:0 10px;
    }

    .js .input-file-trigger {
        display: block;
        padding: 14px 45px;
        background: #39D2B4;
        color: #fff;
        font-size: 1em;
        transition: all .4s;
        cursor: pointer;
    }
    .js .input-file {
        position: absolute;
        top: 0; left: 0;
        width: 225px;
        opacity: 0;
        padding: 14px 0;
        cursor: pointer;
    }
    .js .input-file:hover + .input-file-trigger,
    .js .input-file:focus + .input-file-trigger,
    .js .input-file-trigger:hover,
    .js .input-file-trigger:focus {
        background: #34495E;
        color: #39D2B4;
    }
    .file-return {
        margin: 0;
    }
    .file-return:not(:empty) {
        margin: 1em 0;
    }
    .js .file-return {
        font-style: italic;
        font-size: .9em;
        font-weight: bold;
    }
    .js .file-return:not(:empty):before {
        content: "Selected file: ";
        font-style: normal;
        font-weight: normal;
    }

</style>
@endpush

@section('content')
    {{ Form::open(['route' => 'backend.claim.manual_notification.upload_all', 'autocomplete' => 'off', 'id' =>'upload_all_manual_notification_form' ,'enctype'=>'multipart/form-data', 'class' => 'upload_all_manual_notification_form', 'name' => 'upload_all_manual_notification_form']) }}
        <!-- Put the page specifically for this page here -->

        <div class="row">
            <div class="offset-md-4 col-md-4">
                <div class="fileld-layout">
                    <label>Has Payroll?</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('has_payroll', ['' => '', 1 => "Yes", 0 => "No", 2 => "Not Apply"],  null, ['class' => 'form-control search-select',  'style' => 'border-radius: 3px;width:100%;', 'placeholder' => '']) !!}
                        </div>
                        <label class="help-block"></label>
                    </div>
                </div>
            </div>
        </div>

        <h6><span class="line">Upload a File <b>(Manual Notification Standard Template)</b></span></h6>
        <div class="row">
            <div class="offset-md-4 col-md-4">
                <div class="form-group">
                    <input name="contacts_file" class="input-file" id="fileUploader" type="file" onchange="document.getElementById('loadImg').style.display='inline';">
                    <label class="input-file-trigger" style="text-align: center;">Select File (xls/xlsx)</label>
                    <label class="required_asterik" style="text-align: center;"><small>First row should be a header and data should be in the first sheet</small></label>
                    <p class="file-return" style="font-size: larger"></p>
                    <label class="help-block"></label>
                </div>

                <div id="loadImg" style="text-align: center; display: none;">
                    <div ><img src="{!! asset_url(). "/nextbyte/img/loadingGif.gif" !!}" width="150"></div>
                </div>
            </div>
        </div>

        <div id="file_preview"  class="scroll-content scroll-horizontal"></div>

        <div class="row">
            <div class="col-md-12">
                <input type="hidden" value="" name="json_data">
                <input type="hidden" value="0" name="file_preview">
                <input type="hidden" value="{{ $reference }}" name="reference">
            </div>
        </div>

        <img src="{!! asset_url(). "/nextbyte/img/preloader.gif" !!}" id="gif" style="display: none; margin: 0 auto; width: 200px; visibility: hidden;">
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <div style="text-align: center;">
                    <a href="{{ route('backend.claim.notification_report.administration') }}" class="  btn-secondary site-btn">Cancel</a>
                    <input type="submit" class="btn btn-success btn-submit" value="Upload" />
                </div>
            </div>
        </div>

    {{ Form::close() }}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {!! Html::script(asset_url(). '/nextbyte/plugins/convertExcelToJSON/js/xlsx.full.min.js') !!}
    {!! Html::script(asset_url(). '/nextbyte/plugins/forms/js/jquery.form.min.js') !!}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script>
    $(function() {
        $(".search-select").select2();
        let fileInput  = document.querySelector( ".input-file" ),
            button     = document.querySelector( ".input-file-trigger" ),
            the_return = document.querySelector(".file-return");
        $(document).ready(function(){$("#fileUploader").change(function(evt){var selectedFile=evt.target.files[0];var reader=new FileReader();reader.onload=function(event){document.getElementById("loadImg").style="display: none";var data=event.target.result;var workbook=XLSX.read(data,{type:'binary'});let sheetsList = workbook.SheetNames;let XL_row_object=XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetsList[0]], {defval:""});let json_object=JSON.stringify(XL_row_object);$("input[name='json_data']").val(json_object);$("input[name=file_preview]").val(1);$( "#upload_all_manual_notification_form" ).submit();};reader.onerror=function(event){console.error("File could not be read! Code "+event.target.error.code)};reader.readAsBinaryString(selectedFile);})});
        $('.upload_all_manual_notification_form').on('submit', function(e) {
            e.preventDefault();
            let $gif = $('#gif');
            let $file_preview = $("#file_preview");
            let $file_preview_input = $("input[name=file_preview]");
            $gif.css('visibility', 'visible');
            $gif.css('display', 'block');
            let $form = this;
            $($form).find(".btn-submit").prop('disabled', true);
            let $options = {
                dataType : "json",
                type : "POST",
                url : $($form).attr("action"),
                beforeSend : function (e) {
                    $($form).find(':input').each(function () {
                        $(this).closest(".form-group").removeClass("has-danger").find(".help-block").html("");
                    });
                },
                success : function ($data) {
                    $gif.css('visibility', 'hidden');
                    $gif.css('display', 'none');
                    $($form).find(".btn-submit").prop('disabled', false);
                    if ($data.success) {
                        switch ($data.success) {
                            case 2:
                                let $select_variables = $('#select-variables');
                                $file_preview.html($data.data.view);
                                $("input[name=json_data]").val('');
                                $(".search-select").select2();
                                break;
                            default:
                                document.location = $data.redirect_url;
                                break;
                        }
                    } else {
                        alert($data.message);
                    }
                    $file_preview_input.val(0);
                },
                error: function (data) {
                    $gif.css('visibility', 'hidden');
                    $gif.css('display', 'none');
                    $($form).find(".btn-submit").prop('disabled', false);
                    let $errors = $.parseJSON(data.responseText);
                    /*console.log($errors);*/
                    $.each($errors, function($index, $value) {
                        $($form).find(':input[name="' + $index + '"]').each(function () {
                            $(this).closest(".form-group").addClass("has-danger").find(".help-block").append("&nbsp;&nbsp;<p class='tag tag-danger'>" + $value + "</p>");
                        });
                    });
                    $file_preview_input.val(0);
                }
            };
            $($form).ajaxSubmit($options);
        });
        document.querySelector("html").classList.add('js');
        button.addEventListener( "keydown", function( event ) {
            if ( event.keyCode == 13 || event.keyCode == 32 ) {
                fileInput.focus();
            }
        });
        button.addEventListener( "click", function( event ) {
            fileInput.focus();
            return false;
        });
        fileInput.addEventListener( "change", function( event ) {
            the_return.innerHTML = document.getElementById("fileUploader").files[0].name;
        });
    });
    function showLoading() {
        document.getElementById("loadImg").style = "visibility: visible";
    }
    function hideLoading() {
        document.getElementById("loadImg").style = "visibility: hidden";
    }
</script>

@endpush