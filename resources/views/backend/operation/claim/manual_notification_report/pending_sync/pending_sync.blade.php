@extends('layouts.backend.main', ['title' => 'Pending notification synchronizations', 'header_title' => 'Pending notification synchronizations'])

@include('backend.includes.datatable_assets')

@section('content')




    @include('backend/operation/claim/manual_notification_report/pending_sync/includes/get_pending_sync_datatable')

   @stop