@extends('layouts.backend.main', ['title' => 'Manual Notification Files', 'header_title' => 'Manual Notification Files'])

@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12" >
                <div class="pull-right">


                    <div class="btn-group">

                        {{--<a href="{!! route('backend.claim.manual_notification.upload_page') !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-upload"></i>&nbsp;Upload Bulk</a>--}}

                    </div>
                </div>



            </div>
        </div>


    </div>

    <br/>
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="manual-notification-table">
                <thead>
                <tr >
                    <th>Case no</th>
                    <th>Incident Type</th>
                    <th>Employee</th>
                    <th>Employer</th>
                    <th>Incident Date</th>
                    <th>Sync Status</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#manual-notification-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.manual_notification.get_for_payroll_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'case_no', name: 'case_no'},
                    { data: 'incident_type' , name: 'incident_type', orderable : false, searchable : false},
                    { data: 'employee', name: 'employee_name',  orderable : true, searchable : true},
                    { data: 'employer', name: 'employer_name',  orderable : true, searchable : true},
                    { data: 'incident_date_formatted', name: 'incident_date',  orderable : true, searchable : true},
                    { data: 'status', name: 'status',  orderable : false, searchable : false},
                    { data: 'action', name: 'action',  orderable : false, searchable : false},
                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
