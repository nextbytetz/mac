@extends('layouts.backend.main', ['title' => 'Update OSH Data', 'header_title' => 'Update OSH Data'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/claim/manual_notification_report/includes/header_info")
    </div>
    {!! Form::model($incident, ['route' => ['backend.claim.manual_notification.post_osh_data', $incident->id], 'name' => 'update_manual_notification_osh', 'class' => 'update_manual_notification_osh', 'enctype' => 'multipart/form-data']) !!}
    {!! Form::hidden('rejection_cv_id', $rejection_cv_id) !!}
    {!! Form::hidden('incident_type_id', $incident->incident_type_id) !!}
    {!! Form::hidden('status_cv_ref', $status_cv_ref) !!}
    {!! Form::hidden('this_date', getTodayDate()) !!}
    <div class="row">
        <div class="offset-md-2 col-md-4">
            {{--Incident Type--}}

            {{--HCP NAME--}}
            <div class="fileld-layout">
                {{--<label class="{{ ($status_cv_ref == 'MANNOTST01') ? 'required' : '' }}">Hospital</label>--}}
                <label>Hospital</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('hcps[]',$health_providers, $hcp_ids, ['class' => 'form-control hcp-select', 'style' => 'width:100%', 'id' => 'hcp', 'multiple' => true ]) !!}
                    </div>
                    <div><small>HCP Name(s) treated the employee</small></div>
                    {!! $errors->first('hcp_name', '<span class="help-block label label-danger">:message</span>') !!}
                    <span class="help-block"></span>
                </div>
            </div>

            @if ($incident->incident_type_id == 1 || $incident->incident_type_id == 2)
                <div class="fileld-layout">
                    <label>Nature of Injuries</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('injury_nature',$cv_repo->getCodeValuesByCodeForSelect(3), $incident->injury_nature_cv_id, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'injury_nature']) !!}
                        </div>
                        {!! $errors->first('injury_nature', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>No. of days lost as Full day</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('day_off', null, ['class' => 'form-control number',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('day_off', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>No. of days lost as Light duty</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('light_duties', null, ['class' => 'form-control number',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('light_duties', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
            @endif

            @if ($incident->incident_type_id == 1 || $incident->incident_type_id == 2 || in_array($incident->incident_type_id , [3,4,5]))
                <div class="fileld-layout">
                    <label>Cause of accident: (According to type)</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('accident_cause_type', $accident_cause_types, $incident->accident_cause_type_cv_id, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'accident_cause_type']) !!}
                        </div>
                        {!! $errors->first('accident_cause_type', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="fileld-layout">
                    <label>Cause of accident (Acording to Agency)</label>
                    <div class="form-group">
                        <div class="input-group">

                            {!! Form::select('accident_cause_agency', $accident_cause_agencies, $incident->accident_cause_agency_cv_id, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'accident_cause_agency']) !!}
                        </div>
                        {!! $errors->first('accident_cause_agency', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
            @endif



            @if ($incident->incident_type_id == 2)
                <div class="fileld-layout">
                    <label>Name of disease diagnosed</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('disease_diagnosed', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('disease_diagnosed', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>Type of occupational disease</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('disease_type', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('disease_type', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
            @endif
        </div>


        <div class="col-md-4">
            {{--Incident Date--}}


            @if ($incident->incident_type_id == 1 || $incident->incident_type_id == 2)
                <div class="fileld-layout">
                    <label>Percentage of Disability</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('pd', $incident->man_pd, ['class' => 'form-control number',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('pd', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>Rehabilitation (REH)</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('rehabilitation', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('rehabilitation', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>Bodily location</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('bodily_location',$cv_repo->getCodeValuesByCodeForSelect(2), $incident->bodily_location_cv_id, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'bodily_location']) !!}
                        </div>
                        {!! $errors->first('bodily_location', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
            @endif

            @if($incident->incident_type_id == 2)
                <div class="fileld-layout">
                    <label>Disease caused by agent</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('disease_agent', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}

                        </div>
                        {!! $errors->first('disease_agent', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>Disease by target organ</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('disease_target_organ', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        {!! $errors->first('disease_target_organ', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="help-block"></span>
                    </div>
                </div>
            @endif



        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="offset-md-2 col-md-8">
            <div class="pull-right">
                <a href="{{ route("backend.claim.manual_notification.dashboard", $incident->id) }}" class="btn-secondary site-btn">Cancel</a>
                <input type="submit" class="btn btn-success btn-submit" value="Update" />
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function () {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            $('body').on('submit', 'form.update_manual_notification_information', function (e) {
                e.preventDefault();
                let $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                var $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    beforeSend : function ($e) {
                        $($form).find(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        if ($data.success) {
                            document.location.href =  $data.redirect_url;
                        }
                    },
                    error: function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        let errors = $.parseJSON($data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function($index, $value) {
                            $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        });
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });


            //for number check
            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);
            });

            function number_only(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }


               $(".hcp-select").select2({
                minimumInputLength: 3,
                multiple: true,
                ajax: {
                    url: "{!! route('backend.claim.registered_health_providers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.full_info,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        });
    </script>
@endpush