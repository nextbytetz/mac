@extends('layouts.backend.main', ['title' => 'Update Manual Payment', 'header_title' => 'Update Manual Payment'])

@include('backend/includes/assets/sweetalert_assets')

@push('after-styles-end')
    {{--{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}--}}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/claim/manual_notification_report/includes/header_info")
    </div>
    <div class="row">
        {!! Form::model($incident, ['route' => ['backend.claim.manual_notification.post_manual_payment', $incident->id], 'name' => 'update_manual_report_payment', 'class' => 'update_manual_report_payment', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('incident_id', $incident->id) !!}
        {!! Form::hidden('incident_type_id', $incident->incident_type_id) !!}
        <div class="col-md-12">


            <div class="row">
                <div class="col-md-6">
                    <div class="fileld-layout">
                        <label>GME</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('man_gme', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                {!! $errors->first('man_gme', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                            <span class="help-block">
                                        <p>Manual GME</p>
                                    </span>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @if($incident->incident_type_id == 1 Or $incident->incident_type_id == 2)
                    {{--Accident and Disease--}}
                    <div class="col-md-6">
                        <div class="fileld-layout">
                            <label>TTD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_ttd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                    {!! $errors->first('man_ttd', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                                <span class="help-block">
                                        <p>Temporary Total Disablement Paid</p>
                                    </span>

                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>PD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_pd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                    {!! $errors->first('man_pd', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                                <span class="help-block">
                                        <p>Percentage Disability Assessed</p>
                                    </span>

                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>MAE</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_mae', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                    {!! $errors->first('man_mae', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                                <span class="help-block">
                                        <p>Medical Aid Expense Paid</p>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="fileld-layout">
                            <label>TPD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_tpd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                    {!! $errors->first('man_tpd', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                                <span class="help-block">
                                        <p>Temporary Partial Disablement Paid</p>
                                    </span>
                            </div>
                        </div>
                        <div class="fileld-layout">
                            <label>PPD</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_ppd', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                    {!! $errors->first('man_ppd', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                                <span class="help-block">
                                        <p>Permanent Partial Disablement Paid</p>
                                    </span>
                            </div>
                        </div>
                    </div>
                @else
                    {{--Death--}}
                    <div class="col-md-6">
                        <div class="fileld-layout">
                            <label>Funeral Grant</label>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! Form::text('man_funeral_grant', null, ['class' => 'form-control',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                                    {!! $errors->first('man_funeral_grant', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                                <span class="help-block">
                                        <p>Funeral Grant Paid</p>
                                    </span>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a href="{{ route("backend.claim.manual_notification.dashboard", $incident->id) }}" class="  btn-secondary site-btn">Cancel</a>
                        <input type="submit" class="btn btn-success btn-submit" value="Update" />
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function () {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            $('body').on('submit', 'form.update_manual_report_payment', function (e) {
                e.preventDefault();
                       var $form = this;

                swal({
                    title: "Warning",
                    text: "Are you sure to Update Manual Payment?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        /* start: remove any printed error message in the input controls */
                        // $($form).find(':input').each(function () {
                        //     $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                        // });
                        // /* end: remove any printed error message in the input controls */
                        // let $options = {
                        //     dataType : "json",
                        //     type : "POST",
                        //     url : $($form).attr("action"),
                        //     success : function ($data) {
                        //         $($form).find(".btn-submit").prop('disabled', false);
                        //         document.location.href =  $data.return_url;
                        //     },
                        //     error: function (data) {
                        //         $($form).find(".btn-submit").prop('disabled', false);
                        //         var errors = $.parseJSON(data.responseText);
                        //         /* console.log(errors); */
                        //         $.each(errors, function($index, $value) {
                        //             $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        //         });
                        //     }
                        // };
                        // // pass options to ajaxForm
                        // $($form).ajaxSubmit($options);

                        $form.submit();

                    }
                });
            });
        });
    </script>
@endpush