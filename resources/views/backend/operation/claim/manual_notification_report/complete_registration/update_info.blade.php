@extends('layouts.backend.main', ['title' => 'Update Information', 'header_title' => 'Update Information'])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/claim/manual_notification_report/includes/header_info")
    </div>
    {!! Form::model($incident, ['route' => ['backend.claim.manual_notification.postupdateinfo', $incident->id], 'name' => 'update_manual_notification_information', 'class' => 'update_manual_notification_information', 'enctype' => 'multipart/form-data']) !!}
    {!! Form::hidden('rejection_cv_id', $rejection_cv_id) !!}
    {!! Form::hidden('this_date', getTodayDate()) !!}
    <div class="row">
        <div class="offset-md-2 col-md-4">
            {{--Incident Type--}}
            <div class="fileld-layout">
                <label class="required">Incident Type</label>
                <span class="underline" style="font-weight: bold;">{{ $incident->incident_type }}</span>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('incident_type_id', $incident_types, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'incident_type']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;"></span>
                </div>
            </div>

            {{--Reporting Date--}}
            <div class="fileld-layout">
                <label>Reporting Date</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('reporting_date', null, ['class' => 'form-control datepicker',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>

            {{--Registration Date--}}
            <div class="fileld-layout">
                <label>Registration Date</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('registration_date', null, ['class' => 'form-control datepicker',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>

            {{--Claim Status--}}
            <div class="fileld-layout">
                <label class="required">Claim Status</label>
                <span class="underline" style="font-weight: bold;">{{ $incident->claim_status }}</span>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('status_cv_id', $claim_status, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;"></span>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            {{--Incident Date--}}
                <div class="fileld-layout">
                    <label>Incident Date</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('incident_date', null, ['class' => 'form-control datepicker',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>

            {{--Notification Date--}}
                <div class="fileld-layout">
                    <label>Notification Date</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('notification_date', null, ['class' => 'form-control datepicker',  'style' => 'border-radius: 3px;', 'autocomplete' => 'off']) !!}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>

            {{--District--}}
            <div class="fileld-layout">
                <label class="required">Incident District</label>
                <span class="underline" style="font-weight: bold;">{{ $incident->district }}</span>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('district_id', $districts, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'district']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;"></span>
                </div>
            </div>


            {{--Rejection Reason--}}
            <div class="fileld-layout">
                <label>Rejection Reason</label>
                <span class="underline" style="font-weight: bold;">{{ $incident->reject_reason }}</span>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('reject_reason_cv_id', $rejection_reason, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;"></span>
                </div>
            </div>

        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="offset-md-2 col-md-8">
            <div class="pull-right">
                <a href="{{ route("backend.claim.manual_notification.dashboard", $incident->id) }}" class="btn-secondary site-btn">Cancel</a>
                <input type="submit" class="btn btn-success btn-submit" value="Update" />
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function () {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            $('body').on('submit', 'form.update_manual_notification_information', function (e) {
                e.preventDefault();
                let $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                var $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    beforeSend : function ($e) {
                        $($form).find(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        if ($data.success) {
                            document.location.href =  $data.redirect_url;
                        }
                    },
                    error: function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        let errors = $.parseJSON($data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function($index, $value) {
                            $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        });
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });


            //for number check
            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);
            });

            function number_only(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });
    </script>
@endpush