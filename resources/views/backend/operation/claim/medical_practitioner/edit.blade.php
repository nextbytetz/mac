@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.edit_medical_practitioner'), 'header_title' => trans('labels.backend.claim.edit_medical_practitioner')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($medical_practitioner,['route' => ['backend.claim.medical_practitioner.update', $medical_practitioner->id], 'method'=>'put', 'id' => 'update']) !!}
    {!! Form::hidden('request_action_type', 2 , []) !!}
    {!! Form::hidden('medical_practitioner_id', $medical_practitioner->id , []) !!}

    <div class="row">
        <div class="col-md-12">
            {{--Delete medical practitioner--}}
            <div class="pull-right">
                {{ link_to_route('backend.claim.medical_practitioner.destroy', trans('buttons.general.crud.delete'), $medical_practitioner->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.claim.confirm_medical_practitioner_delete'), 'class' => 'btn btn-danger btn-round-right']) }}
            </div>
        </div>
    </div>
    @include("backend.includes.registration.claim.create_medical_practitioner_form")

    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.claim.medical_practitioner.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
<script  type="text/javascript">

    $(function () {
        autosize($("textarea.autosize"));
    });

</script>;


@endpush
