@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.add_medical_practitioner'), 'header_title' => trans('labels.backend.claim.add_medical_practitioner')])

@push('after-styles-end')

@endpush

@section('content')
    @include("backend.includes.registration.claim.create_medical_practitioner")
@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
<script  type="text/javascript">

    $(function () {
        autosize($("textarea.autosize"));
    });

</script>;


@endpush
