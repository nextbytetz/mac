@extends('layouts.backend.main', ['title' => 'Enrolled Survivors', 'header_title' => 'Enrolled Survivors to Payroll'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')



    <br/>
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="enrolled-survivor-table">
                <thead>
                <tr >
                    <th>Name</th>
                    <th>Employee</th>
                    <th>DOB</th>
                    <th>Monthly Pension</th>
                    <th>Relationship</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#enrolled-survivor-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.manual_system_file.get_enrolled_survivors') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'fullname', name: 'dependents.firstname'},
                    { data: 'employee_name', name: 'dependents.employee_name'},
                    { data: 'dob_formatted' , name: 'dependents.dob', orderable : false, searchable : false},
                    { data: 'mp', name: 'dependent_employee.survivor_pension_amount',  orderable : true, searchable : true},
                    { data: 'type_name', name: 'dependent_types.name',  orderable : true, searchable : true},
                    { data: 'fullname', name: 'dependents.lastname',  orderable : true, searchable : true, visible:false},
                    { data: 'fullname', name: 'dependents.middlename',  orderable : true, searchable : true, visible:false},
                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }
                // initComplete: function () {
                //     addDeleteForms();
                // },

            });

        });


    </script>;

@endpush
