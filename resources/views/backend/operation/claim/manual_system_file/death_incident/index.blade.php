@extends('layouts.backend.main', ['title' => 'Manual System Files- Death', 'header_title' => 'Manual System Files - Death'])

@include('backend.includes.datatable_assets')

@section('content')

    <br/>
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="manual-system-file-table">
                <thead>
                <tr >
                    <th>Filename</th>
                    <th>Incident Type</th>
                    <th>Employee</th>
                    <th>Employer</th>
                    <th>Incident Date</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#manual-system-file-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.manual_system_file.get_death_incidents') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'filename', name: 'notification_reports.filename'},
                    { data: 'incident_type' , name: 'incident_types.name', orderable : false, searchable : false},
                    { data: 'fullname', name: 'employees.firstname',  orderable : true, searchable : true},
                    { data: 'employer', name: 'employers.name',  orderable : true, searchable : true},
                    { data: 'incident_date_formatted', name: 'notification_reports.incident_date',  orderable : true, searchable : true},
                    { data: 'action', name: 'action',  orderable : false, searchable : false},
                    { data: 'fullname', name: 'employees.lastname',  orderable : true, searchable : true, visible:false},
                    { data: 'fullname', name: 'employees.middlename',  orderable : true, searchable : true, visible:false},
                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
