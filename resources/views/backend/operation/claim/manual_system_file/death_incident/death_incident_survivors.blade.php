@extends('layouts.backend.main', ['title' => 'Survivors', 'header_title' => 'Survivors'])

@include('backend.includes.datatable_assets')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name',isset($notification_report->employee_id) ? $notification_report->employee->name : ' ' , [ 'id'=> 'name']) !!}</strong>

                <small >
                    Case no.: {!! Form::label( 'reg_no', $notification_report->filename, [ 'id'=> 'case_no']) !!}
                </small>
            </h5>
            <legend></legend>
        </div>
    </div>

    <br/>

{{--Button--}}
    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12" >
                {{--Restribute--}}
                <span class="pull-left">
    {!!  'Gross Monthly Earning: ' . '<b class="underline">'  . number_2_format($mp_data_summary['gme']) . '</b>'   . ', ' . 'Monthly Pension Payable: '  .'<b class="underline">'  . number_2_format($mp_data_summary['monthly_pension']) . '</b>'  !!}

                    &nbsp;
                    <a style="color:blue" class="underline" href="{{ route('backend.compliance.dependent.recalculate_mp_distribution_manual', $notification_report->id) }}" > Redistribute Monthly Pension   <i class="icon fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="{!! 'Recalculate and distribute monthly pension to all dependents on this notification. This is applicable if all dependents are not yet enrolled into payroll (Activated)' !!}"></i> </a>
</span>


                <div class="pull-right">


                    <div class="btn-group">

                        <a href="{!! route('backend.claim.manual_system_file.create_dependent', $notification_report->id) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-upload"></i>&nbsp;Add New Survivor</a>

                    </div>
                </div>



            </div>
        </div>


    </div>

    <br/>

    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="manual-system-file-table">
                <thead>
                <tr >
                    <th>Name</th>
                    <th>Relationship</th>
                    <th>Monthly Pension Amount</th>
                    <th>Pension Percent</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#manual-system-file-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.claim.manual_system_file.get_survivors', $notification_report->id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'fullname', name: 'dependents.firstname'},
                    { data: 'type_name' , name: 'dependent_types.name', orderable : false, searchable : false},
                    { data: 'mp', name: 'dependent_employee.survivor_pension_amount',  orderable : true, searchable : true},
                    { data: 'survivor_pension_percent', name: 'dependent_employee.survivor_pension_percent',  orderable : true, searchable : true},
                    { data: 'action', name: 'action',  orderable : false, searchable : false},
                    { data: 'fullname', name: 'dependents.lastname',  orderable : true, searchable : true, visible:false},
                    { data: 'fullname', name: 'dependents.middlename',  orderable : true, searchable : true, visible:false},
                ],
                // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                //     $(nRow).click(function() {
                //         document.location.href = url +  "/payroll/pensioner/"+ "profile/" + aData['id'] ;
                //     }).hover(function() {
                //         $(this).css('cursor','pointer');
                //     }, function() {
                //         $(this).css('cursor','auto');
                //     });
                // }

            });

        });


    </script>;

@endpush
