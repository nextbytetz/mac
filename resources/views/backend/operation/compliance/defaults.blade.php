@extends('layouts.backend.main', ['title' => "Compliance Defaults", 'header_title' => "Compliance Defaults"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h5 class="client-title">
                <i class="icon fa fa-terminal"></i>
                <!-- User header detail -->
                {{--<span>{!! $user->available_label !!} </span>--}}

                <strong> <a href="#"> Compliance Defaults </a></strong>

                {{--<small >
                    Status:
                </small>--}}
            </h5>
        </div>
    </div>
    <legend></legend>
    <br/>

    <div class="row">
        <div class="col-md-12">
            {{--Configuration Number 1--}}
            <label style="font-weight: bolder;">Employer Inspection Master</label>
            {!! Form::open(['route' => ['backend.compliance.post_defaults', "CDEMPLYIMSR"], 'class' => 'form-inline compliance_defaults']) !!}
            <div class="form-group" style="width: 90%;">
                {!! Form::select('inspection_masters[]', $users, $inspection_masters, ['class' => 'form-control search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'inspection_masters']) !!}
            </div>
            <div class="form-group">
                <button class="btn btn-secondary site-btn" type="submit">@lang('buttons.general.crud.update')</button>
                {{--<small class="form-text text-muted" style="width:100% !important;">Can select multiple users</small>--}}
            </div>
            {!! Form::close() !!}
            <div class="clearfix"></div>

            {{--Configuration Number 2--}}
            <label style="font-weight: bolder;">Authorised Inspectors <small>(For Employers)</small></label>
            {!! Form::open(['route' => ['backend.compliance.post_defaults', "CDAUTHRSDINSPCTR"], 'class' => 'form-inline form-group compliance_defaults']) !!}
            <div class="form-group" style="width: 90%;">
                {!! Form::select('authorised_inspectors[]', $users, $authorised_inspectors, ['class' => 'form-control search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'authorised_inspectors']) !!}
            </div>
            <div class="form-group">
                <button class="btn btn-secondary site-btn" type="submit">@lang('buttons.general.crud.update')</button>
                {{--<small class="form-text text-muted" style="width:100% !important;">Can select multiple users</small>--}}
            </div>
            {!! Form::close() !!}
            <div class="clearfix"></div>
            <label style="font-weight: bolder;">Officers for Employer Temporary De-registartions Re Open Alert <small>(For Employers)</small></label>
            {!! Form::open(['route' => ['backend.compliance.post_defaults', "CDAEMPREOPUS"], 'class' => 'form-inline form-group compliance_defaults']) !!}
            <div class="form-group" style="width: 90%;">
                {!! Form::select('temp_deregistration_reopen_officers[]', $users, $temp_deregistration_reopen_officers, ['class' => 'form-control search-select', 'style' => 'width:100%', 'multiple' => 'true', 'id' => 'temp_deregistration_reopen_officers']) !!}
            </div>
            <div class="form-group">
                <button class="btn btn-secondary site-btn" type="submit">@lang('buttons.general.crud.update')</button>
                {{--<small class="form-text text-muted" style="width:100% !important;">Can select multiple users</small>--}}
            </div>
            {!! Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>

    <hr/>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    <script>
        $(function(){
            $(".search-select").select2({});

            $('body').on('submit', 'form.compliance_defaults', function (e) {
                e.preventDefault();
                var $form = this;

                var $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    success : function (data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        if (data.success) {
                            $("<div class='alert alert-success'>Success, All changes have been updated!</div>").appendTo($($form)).delay(2000).fadeOut();
                        }
                    },
                    error: function ($data) {
                        var $errors = $.parseJSON($data.responseText);
                    },
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });

        });
    </script>
@endpush