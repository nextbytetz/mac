@extends('layouts.backend.main', ['title' => "Attend Employer Task", 'header_title' => "Attend Employer Task"])

@push('after-styles-end')
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/compliance/inspection/employer/includes/header_info")
    </div>
    <div class="row">

        <div class="col-md-6 offset-md-3">
            <p style="text-align: center;">
                <span style="color: darkred;"><i class="icon fa fa-close fa-5x"></i></span>
            </p>
            <p>
                Employer task has not been attended,
            </p>
            <p>
                Click the attend button to start working on the Employer Inspection Task. This Inspection has been allocated to <span style="color: green;">{{ $employer_task->allocatedUser->name or "None" }}</span>
            </p>
            <div style="text-align: center;">
                {!! link_to_route('backend.checker.index', "Cancel", [], ['class' => 'btn btn-secondary site-btn']) !!}
                <span>
                    {!! Html::decode(link_to_route('backend.compliance.inspection.employer_task.attend.post', "Attend", $employer_task->id, ['data-method' => 'post', 'class' => 'btn site-btn save_button'])) !!}
                </span>
            </div>
        </div> <!-- /.col-md-8 -->

    </div> <!-- /.row -->
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    <script type="text/javascript">
        $(function() {

        });
    </script>
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}

@endpush