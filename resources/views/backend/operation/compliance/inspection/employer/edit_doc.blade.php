@extends('layouts.backend.main', ['title' => "Update Doc", 'header_title' => "Update Doc"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <div class="row">
        @include("backend/operation/compliance/inspection/employer/includes/header_info")
    </div>

    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.inspection.employer_task.update_document'], 'name' => 'employer_inspection_task_edit_doc', 'class' => 'edit_doc', 'method' => 'put','enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('employer_inspection_task_id', $employer_task->id, ['class' =>'']) !!}
    {!! Form::hidden('document_id', $uploaded_document->pivot->document_id, ['class' =>'']) !!}
    {!! Form::hidden('action_type', 2, ['class' =>'']) !!}
    {{--HEADER--}}

    {{--document types--}}
    <div class="row">
        <div class="pull-right" >
            {{--<br/>--}}
            <span>
            {!! HTML::decode(link_to_route('backend.compliance.inspection.employer_task.delete_document', "<i class='icon fa fa-delete' aria-hidden='true'></i>&nbsp;" . 'Delete', $uploaded_document->pivot->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to delete this Document?', 'class' => 'btn btn-primary site-btn delete_button'])) !!}
            </span>
        </div>
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Document type:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! $uploaded_document->name . ' - ' .  $uploaded_document->pivot->name!!}

                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--document title--}}
    @if($uploaded_document->pivot->document_id == 70)
        <div class="row option_div" id="doc_title_div">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Document title:</label></div>
                    <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text','document_title', $uploaded_document->pivot->description, ['class' => 'form-control option_input', 'id' => 'document_title']) !!}
                            {!! $errors->first('document_title', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif

    {{--Attach file input--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Attach document (PDF):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::file('document_file') !!}
                        {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>




    {{--date_of_reference--}}
    <div class="row option_div" id="date_ref_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Date of reference:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">

                        <div class="form-inline">

                            <div class="input-group" style="width:100%;">
                                {!! Form::text('date_reference',  isset($uploaded_document->date_reference) ? short_date_format($uploaded_document->date_reference) : null, ['placeholder' => '', 'class' => 'form-control option_input datepicker1', 'autocomplete' => 'off', 'id' => 'date_reference']) !!}

                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            {{--<span class="help-block">--}}
                            {{--<p>Date of closing the business</p>--}}
                            {{--</span>--}}
                        </div>
                    </div>
                    {!! $errors->first('date_reference', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
    </div>



    <br/>

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.inspection.employer_task.show', trans('buttons.general.cancel'), [$employer_task->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script>
        let $document_type = '{!! $uploaded_document->pivot->document_id !!}';
        $(function () {
            $(".search-select").select2({});

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                // maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            documentOption();
            $("#document_type").on("change", function (e) {
                documentOption();
            });

        });
        function documentOption() {
            let choice = $document_type;
            let $doc_title_div = $("#doc_title_div");
            switch (choice){
                case '79':
                    /* Other Inspection Documents */
                    $doc_title_div.show();
                    break;
                default:
                    $doc_title_div.hide();
                    break;
            }
        }
    </script>
@endpush