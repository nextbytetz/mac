<div class="payroll">
    {{--@if ($employer_task->progressive_stage < 5)--}}
    @if (true)
        {!! HTML::decode(link_to_route('backend.compliance.inspection.employer_task.manage_payroll', "<i class='icon fa fa-refresh' aria-hidden='true'></i>&nbsp;" . "Manage Payrolls", [$employer_task->id],['class' => 'btn btn-sm btn-secondary pull-right', 'style' => 'font-weight:normal;' ])) !!}
    @endif
    <br/>
    <br/>
    <table class="table table-striped table-bordered">
        <tbody>
            @foreach ($payrolls as $payroll)
                <tr>
                    <td><b>{{ $payroll->name }}</b></td>
                    <td style="text-align: center;"><a href="{!! route('backend.compliance.inspection.employer_task.assessment_template', ["employer_task" => $employer_task->id, "inspection_task_payroll" => $payroll->id]) !!}"  class="dropdown-item" ><i class="icon fa fa-download" aria-hidden="true"></i>&nbsp;Download Assessment Template</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

