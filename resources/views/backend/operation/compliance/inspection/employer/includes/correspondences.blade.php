@if ($employer_task->letters()->count())
    @foreach($employer_task->letters as $letter)
        <legend><span style="font-size: medium;"><b>&#x27A5;&nbsp;</b>&nbsp;{!! $letter->print_status_label !!}&nbsp;<span class='underline'><a href="{{ route("backend.letter.show", $letter->id) }}" class="btn btn-secondary btn-sm site-btn">{{ $letter->codeValue->name }}</a></span></span></legend>
    @endforeach
@else
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Letter.</strong> This inspection has no associated letter.
        </div>
    </div>
@endif