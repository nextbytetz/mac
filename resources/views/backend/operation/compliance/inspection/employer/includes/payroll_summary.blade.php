@if ($payrolls->count() > 1)
    @foreach ($payrolls as $payroll)

        @php
            $payroll_summary = (new \App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository())->assessmentSummary($employer_task, $payroll->id);
        @endphp

        @if (count($payroll_summary))
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td width="300px" colspan="2">Preliminary Findings Summary Report&nbsp;&nbsp;(<b>{{ $payroll->name }}</b>)</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: right;">Oustanding Contribution&nbsp;:</td>
                    <td style="text-align: left;"><b>{{ $payroll_summary['underpaid'] }}</b></td>
                </tr>
                <tr>
                    <td style="text-align: right;">Outstanding Interest&nbsp;:</td>
                    <td style="text-align: left;"><b>{{ $payroll_summary['total_interest'] }}</b> </td>
                </tr>
                <tr>
                    <td style="text-align: right;">Overpaid Contribution&nbsp;:</td>
                    <td style="text-align: left;"><b>{{ $payroll_summary['overpaid'] }}</b></td>
                </tr>
                <tr>
                    <td style="text-align: right;"><strong>Total Outstanding Amount</strong>&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=""></i>&nbsp;:</td>
                    <td style="text-align: left;"><b>{{ $payroll_summary['accumulative_to_be_paid'] }}</b>  </td>
                </tr>
                </tbody>
            </table>
        @endif
    @endforeach
@endif
