<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >On Demand Workflow</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">

        {{-- Defaulted Employer Workflow --}}
        {{--commented as they are enforceable workflows--}}
{{--        @if ($employer_task->progressive_stage >= 6)
            <div class="dropdown-divider"></div>
            {!! Html::decode($employer_task->initialize_defaulted_employer_link) !!}
        @endif--}}

        {{-- Payment Installment Workflow --}}
        {{--commented as they are enforceable workflows--}}
{{--        @if ($employer_task->progressive_stage >= 6)
            <div class="dropdown-divider"></div>
            {!! Html::decode($employer_task->initialize_payment_installment_link) !!}
        @endif--}}

        {{-- Refund Overpayment Workflow --}}
        @if ($employer_task->progressive_stage >= 6 And $employer_task->hasoverpay)
            <div class="dropdown-divider"></div>
            {!! Html::decode($employer_task->initialize_refund_over_payment_link) !!}
        @endif

        {{-- Employer Task Cancellation Workflow --}}
        @if ($employer_task->progressive_stage < 4)
            <div class="dropdown-divider"></div>
            {!! Html::decode($employer_task->initialize_task_cancellation_link) !!}
        @endif

     </span>
</span>