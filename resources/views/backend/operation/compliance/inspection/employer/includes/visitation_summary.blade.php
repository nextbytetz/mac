<div class="wcf_employer">
    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td><b>Visit Date</b></td>
                <td>{{ $employer_task->visit_date_formatted }}</td>
            </tr>
            <tr>
                <td><b>Inspection Date (Exit Meeting Date)</b></td>
                <td>{{ $employer_task->exit_date_formatted }}</td>
            </tr>
            <tr>
                <td><b>Employer Commencement Date</b></td>
                <td>{{ $employer_task->doc_formatted }}</td>
            </tr>
            <tr>
                <td><b>Previous Inspection Date (If any)</b></td>
                <td>{{ $employer_task->last_inspection_date_formatted }}</td>
            </tr>
            <tr>
                <td><b>Assessment Review Start Date</b></td>
                <td>{{ $employer_task->review_start_date_formatted }}</td>
            </tr>
            <tr>
                <td><b>Assessment Review End Date</b></td>
                <td>{{ $employer_task->review_end_date_formatted }}</td>
            </tr>
            <tr>
                <td><b>Findings</b></td>
                <td>{{ $employer_task->findings Or "-" }}</td>
            </tr>
            <tr>
                <td><b>Resolutions</b></td>
                <td>{{ $employer_task->resolutions Or "-" }}</td>
            </tr>
        </tbody>
    </table>
</div>