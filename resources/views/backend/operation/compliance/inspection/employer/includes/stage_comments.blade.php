@if ($stages->count())
    <div data-plugin="custom-scroll" data-height="500">
        @foreach ($stages->get() as $stage)
            <br/>
            <p style="font-size: medium;"><b>&#x27A5;&nbsp;{{ $loop->iteration }}</b>&nbsp;Stage:&nbsp;{{ $loop->remaining + 1 }}</p>
            @php
                $comments = json_decode($stage->pivot->comments);
            @endphp
            <table class="table table-striped table-bordered">
                <tbody>
                @foreach($comments as $key => $value)
                    @if ($key == "currentStageCvId")
                        @continue
                    @endif
                    <tr @if ($key == "currentStage") style="border: dotted #e6f5f0 2px !important;background-color: #d3dfd0;"  @endif>
                        <td><b>{{ $key }}</b></td>
                        <td>{{ $value }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
@else
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Stages.</strong> This Employer Inspection Task has no any associated stage recorded.
        </div>
    </div>
@endif