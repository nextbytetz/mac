<div class="row">
    <div class="col-md-12">

        @foreach ($payrolls as $payroll)

            @php
                $payroll_assessments = clone $assessments;
            @endphp

            <div class="dashboard-header content">
                {{--<h4 class="page-content-title float-xs-left grey_modal">Workflow Track Overview</h4>--}}
                <div class="dashboard-action">
                    <ul class="right-action float-xs-right">
                        <li data-widget="collapse"><a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus" style="font-size: 18px;"></span></a></li>
                        {{--<li data-widget="close"><a href="javascript:void(0)"><span aria-hidden="true" class="icon_close"></span></a></li>--}}
                    </ul>
                </div>
                <legend class="light_grey_bg" >
                    {{ $payroll->name }}&nbsp;( Payroll ) &nbsp;&nbsp;&nbsp; {!! $payroll->has_assessment_label !!}

                    @if ($employer_task->progressive_stage >= 5)
                        &nbsp;&nbsp;&nbsp;
                        {!! $payroll->response_letter_status_label !!} &nbsp;&nbsp;
                        @if(!$payroll->iscompliant)
                            <span class='underline'><a href="{{ route("backend.letter.process", [$payroll->id, "CLDNDNOLTR"]) }}" class="btn btn-secondary btn-sm">Demand Notice Letter</a></span>
                            &nbsp;&nbsp;&nbsp;
                            {!! $payroll->reminder_letter_status_label !!} &nbsp;&nbsp;
                            <span class='underline'><a href="{{ route("backend.letter.process", [$payroll->id, "CLEMPRMDLTR"]) }}" class="btn btn-secondary btn-sm">Reminder Letter</a></span>
                        @else
                            <span class='underline'><a href="{{ route("backend.letter.process", [$payroll->id, "CLLTRFCMPLNC"]) }}" class="btn btn-secondary btn-sm">Letter of Compliance</a></span>
                        @endif
                    @endif

                </legend>
                <div class="dashboard-box content-list" style="display: none;">
                    {{--<div class="list-group message-list-group">

                    </div>--}}
                    @if ($payroll_assessments->where("inspection_task_payroll_id", $payroll->id)->count())
                        <br/>
                        <div class="underline">
                            Assessment Schedule&nbsp;&nbsp;<a href="{{ route("backend.compliance.inspection.employer_task.download_assessment_schedule", ["employer_task" => $employer_task->id, "inspection_task_payroll" => $payroll->id]) }}" class="btn btn-secondary btn-sm site-btn">Download</a>
                        </div>
                        <div  class="table-scroll">
                            {{--<div class="always-visible" data-height="500" data-plugin="custom-scroll" data-use-both-wheel-axes="true">--}}
                            <div  class="table-wrap">
                                {{--data-height="500" data-min="800"--}}

                                {{--PD less than or equal to 30--}}
                                <table class="table table-striped table-bordered main-table">
                                    <thead>
                                        <tr>
                                            <th>Month</th>
                                            <th>Employees Declared</th>
                                            <th>Employees Verified</th>
                                            <th>Employees Difference</th>
                                            <th>Salary</th>
                                            <th>Allowances</th>
                                            {{--<th>Non-Fixed Allowances</th>--}}
                                            <th>Gross Declared</th>
                                            <th>Gross Verified</th>
                                            <th>Actual Payment</th>
                                            <th>Verified Contr</th>
                                            <th>Under Payment</th>
                                            <th>Over Payment</th>
                                            <th>Payment Date</th>
                                            <th>Delayed Months Paid Contr</th>
                                            <th>Delayed Months Unpaid Contr</th>
                                            <th>Interest for Unpaid Contr</th>
                                            <th>Interest for Delayed Paid Contr</th>
                                            <th>Accumulative Outstanding Contr</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($payroll_assessments->where("inspection_task_payroll_id", $payroll->id)->get() as $assessment)
                                            <tr>
                                                <th>{{ $assessment->contrib_month_formatted }}</th>
                                                <td>{{ $assessment->member_count }}</td>
                                                <td>{{ $assessment->member_count_verified }}</td>
                                                <td>{{ $assessment->member_count_difference }}</td>
                                                <td>{{ number_2_format($assessment->salary) }}</td>
                                                <td>{{ number_2_format($assessment->fixed_allowance) }}</td>
                                                {{--<td>{{ number_2_format($assessment->non_fixed_allowance) }}</td>--}}
                                                <td>{{ number_2_format($assessment->payroll_declared) }}</td>
                                                <td>{{ number_2_format($assessment->payroll_verified) }}</td>
                                                <td>{{ number_2_format($assessment->contrib_amount) }}</td>
                                                <td>{{ number_2_format($assessment->contrib_amount_verified) }}</td>
                                                <td>{{ number_2_format($assessment->underpaid) }}</td>
                                                <td>{{ number_2_format($assessment->overpaid) }}</td>
                                                <td>{{ $assessment->payment_date }}</td>
                                                <td>{{ $assessment->months_delayed_paid }}</td>
                                                <td>{{ $assessment->months_delayed_unpaid }}</td>
                                                <td>{{ number_2_format($assessment->interest_on_unpaid) }}</td>
                                                <td>{{ number_2_format($assessment->interest_on_paid) }}</td>
                                                <td>{{ number_2_format($assessment->accumulative_to_be_paid) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <br/>
                        @php
                            $assessment_summary = (new \App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository())->assessmentSummary($employer_task, $payroll->id);
                        @endphp
                        @include('backend/operation/compliance/inspection/employer/includes/assessment_summary', ['assessment_summary' => $assessment_summary])

                    @else
                        <div class="alert-left-border">
                            <div class="alert alert-primary alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Nothing to display.</strong> Assessment for this inspection has not been initialized.
                            </div>
                        </div>
                    @endif
                </div>
                <div class="dashboard-action">
                    <ul class="right-action float-xs-right">
                        <li data-widget="collapse"><a aria-hidden="true" href="javascript:void(0)"><span aria-hidden="true" class="icon_minus-06 icon_plus" style="font-size: 18px;"></span></a></li>
                        {{--<li data-widget="close"><a href="javascript:void(0)"><span aria-hidden="true" class="icon_close"></span></a></li>--}}
                    </ul>
                </div>
            </div>

        @endforeach

    </div>
</div>
