<?php
$color = [
    0 => 'blue',
    1 => 'grey',
];
?>
@for ($x = 1; $x <= $selected_entries; $x++)
    <?php
    $i = str_random(5);
    ?>
    <div class="fileld-layout" id="payroll_entry_group{!! $i !!}" style="padding-left:20px; border-left: 2px solid {!! $color[$x % 2] !!};">
        <div class="form-group">
            <legend></legend><br/>
            <div class="form-inline">
                <span>
                    <label class="required">  Payroll Name :</label>&nbsp;&nbsp;
                </span>
                <span>
                    {!! Form::text('payroll_name' . $i, null, ['class' => 'form-control payroll_name', 'style' => "border-radius:3px;width:20%;", 'id' => 'payroll_name' . $i]) !!}&nbsp;&nbsp;
                </span>
                <span>
                    Payroll Manager/Online User:
                </span>
                <span>
                    {!! Form::select('payroll_id' . $i, $users, null, ['class' => 'form-control search-select payroll_id','style'=>'width:20%', 'placeholder' => '', 'id' => 'payroll_id' . $x]) !!}
                </span>
                <span>
                    Review Start Date:
                </span>
                <span>
                               {!! Form::text('review_start_date' . $i, null, ['placeholder' => '', 'class' => 'form-control datepicker','style'=>'width:10%', 'autocomplete' => 'off']) !!}
                           </span>
                <span>
                    Review End Date:
                </span>
                <span>
                              {!! Form::text('review_end_date' . $i, null, ['placeholder' => '', 'class' => 'form-control datepicker','style'=>'width:10%', 'autocomplete' => 'off']) !!}
                          </span>
                <span class="help-block"></span>
                <span class="pull-right">
                    <a href="#" id="{!! $i !!}" class="remove_payroll_entry"><i class="icon fa fa-2x fa-remove" aria-hidden="true" style="color:darkred;"></i></a>
                </span>
            </div>
        </div>
    </div>
@endfor