@if (count($assessment_summary))
    {{--<br/>--}}
    <table class="table table-striped table-bordered" style="border: 2px dotted green;">
        <thead>
            <tr>
                <td width="300px" colspan="2" ><b>Preliminary Findings Summary Report (Aggregate)</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: right;">Oustanding Contribution&nbsp;:</td>
                <td style="text-align: left;"><b>{{ $assessment_summary['underpaid'] }}</b></td>
            </tr>
            <tr>
                <td style="text-align: right;">Outstanding Interest&nbsp;:</td>
                <td style="text-align: left;"><b>{{ $assessment_summary['total_interest'] }}</b> </td>
            </tr>
            <tr>
                <td style="text-align: right;">Overpaid Contribution&nbsp;:</td>
                <td style="text-align: left;"><b>{{ $assessment_summary['overpaid'] }}</b></td>
            </tr>
            <tr>
                <td style="text-align: right;"><strong>Total Outstanding Amount</strong>&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=""></i>&nbsp;:</td>
                <td style="text-align: left;"><b>{{ $assessment_summary['accumulative_to_be_paid'] }}</b>  </td>
            </tr>
        </tbody>
    </table>
@endif