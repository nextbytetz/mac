<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >More</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">

        {{--@if ($employer_task->progressive_stage < 5)--}}
        @if ($employer_task->progressive_stage == 3 Or $employer_task->progressive_stage == 4)
            {{--upload assessment schedule--}}
            <span>
                <a href="{!! route('backend.compliance.inspection.employer_task.upload_assessment_template', $employer_task->id) !!}"  class="dropdown-item" ><i class="icon fa fa-upload"></i>&nbsp;Upload Assessment Schedule</a>
            </span>
        @endif

        {{--download assessment template--}}
        {{--<div class="dropdown-divider"></div>
        <span>
            <a href="{!! route('backend.compliance.inspection.employer_task.assessment_template', $employer_task->id) !!}"  class="dropdown-item" ><i class="icon fa fa-download" aria-hidden="true"></i>&nbsp;Download Assessment Template</a>
        </span>--}}
        {{--skip stage--}}
        <span>
            <div class="dropdown-divider"></div>
            {!! HTML::decode(link_to_route('backend.compliance.inspection.employer_task.skip_stage', "<i class='icon fa fa-mail-forward' aria-hidden='true'></i>&nbsp;Skip Stage", [$employer_task->id], ['data-method' => 'confirm_post', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => "Are you sure you want to skip this stage ?", 'class' => 'dropdown-item']))  !!}
        </span>

        {{--complete employer task--}}
        <div class="dropdown-divider"></div>
        <span>
                <a href="{!! route('backend.compliance.inspection.task.show', $employer_task->id) !!}"  class="dropdown-item" ><i class="icon fa fa-check-square-o"></i>&nbsp;Complete Task</a>
        </span>

        {{--cancel employer task--}}
        <div class="dropdown-divider"></div>
        <span>
                <a href="{!! route('backend.compliance.inspection.task.show', $employer_task->id) !!}"  class="dropdown-item" ><i class="icon fa fa-ban"></i>&nbsp;Cancel Task</a>
        </span>

        {{--close--}}
        <div class="dropdown-divider"></div>
        <span>
                <a href="{!! route('backend.compliance.inspection.task.show', $task->id) !!}"  class="dropdown-item" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
        </span>

     </span>
</span>