@php

    $workflow = $employer_task->processes()->where("wf_done", 0)->limit(1);
@endphp

@if ($workflow->count())
    @php
        $workflowEntry = $workflow->first();
        $wfModule = $workflowEntry->wfModule;
        //$type = $wfModule->type;
        $moduleGroup = $wfModule->wfModuleGroup->id;
        $workflow = new \App\Services\Workflow\Workflow(['wf_module_group_id' => $moduleGroup, 'type' => $wfModule->type, 'resource_id' => $workflowEntry->id]);
        if ($workflow->currentWfDefinition()->has_note) {
            $approval_note = (new \App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository())->getMinuteNoteForAssessmentApproval($employer_task, $assessment_summary);
        }
        $workflowinput = ['resource_id' => $workflowEntry->id, 'wf_module_group_id'=> $moduleGroup, 'type' => $wfModule->type];
    @endphp

    @if (!empty($approval_note))
        <br/>
        <div>
            <b class="underline">Inspection Minute Note</b>&nbsp;&nbsp;<small><button  data-clipboard-action="copy" data-clipboard-target="#inspection_minute_note{{ $employer_task->id }}" href="#" class="btn btn-sm btn-secondary copy_inspection_minute_note" type="button">Copy Note</button></small>
        </div>
        <div id="inspection_minute_note{{ $employer_task->id }}">{!! $approval_note !!}</div>
        <hr/>
    @endif

    {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

    <br/>

@else
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <i class="fa fa-exclamation-circle"></i><strong>Info</strong> No active workflow recently initiated
    </div>
@endif