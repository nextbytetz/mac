<div class="row">
    <div class="col-md-6">
        {{--@if (!is_null($employer_task->iscompliant) And ($employer_task->iscompliant == 0))--}}
        @if (true)

            <div class="grey_modal">Employer Contributions Overview</div>
            <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td colspan="2">Outstanding Payment Report</td>
                            <td><b>Paid (After and/or Before)</b></td>
                            <td><b>Balance</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="300px">Oustanding Contribution&nbsp;:</td>
                        <td style="text-align: center;">{{ $assessment_summary['underpaid'] }}</td>
                        <td>{{ $remittance['contribution'] }}</td>
                        <td>{{ $remittance['contribution_balance'] }}</td>
                    </tr>
                    <tr>
                        <td width="300px">Outstanding Interest&nbsp;:</td>
                        <td style="text-align: center;"> {{ $assessment_summary['total_interest'] }}</td>
                        <td>{{ $remittance['interest'] }}</td>
                        <td>{{ $remittance['interest_balance'] }}</td>
                    </tr>
                    <tr>
                        <td width="300px"><strong>Total</strong>&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=""></i>&nbsp;:</td>
                        <td style="text-align: center;"> {{ $assessment_summary['accumulative_to_be_paid'] }} </td>
                        <td>{{ $remittance['total_paid'] }}</td>
                        <td>{{ $remittance['total_balance'] }}</td>
                    </tr>
                </tbody>
            </table>
        @endif

        @if ($employer_task->hasoverpay)
                <div class="grey_modal">Refund Overview</div>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td colspan="2">Overpayment Report</td>
                    <td><b>Paid</b></td>
                    <td><b>Balance</b></td>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="300px">Overpaid Contribution&nbsp;:</td>
                        <td style="text-align: center;">{{ $assessment_summary['overpaid'] }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="300px"><strong>Total</strong>&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=""></i>&nbsp;:</td>
                        <td style="text-align: center;"> {{ $assessment_summary['overpaid'] }} </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        @endif
    </div>

    {{--@if ($employer_task->iscompliant == 0)--}}
    @if (true)
        <div class="col-md-6">
            <div class="grey_modal">Monthly Contributions</div>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>Month</td>
                        <td>Contrib Before</td>
                        <td>Contrib After</td>
                        <td>Interest Before</td>
                        <td>Interest After</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $contribmonths = $remittance['contribmonths'];
                    @endphp
                    @foreach($contribmonths as $contribmonth)
                        <tr>
                            <td><b>{{ $contribmonth->contrib_month_formatted }}</b></td>
                            <td>{{ number_2_format($contribmonth->paid_before) }}</td>
                            <td>{{ number_2_format($contribmonth->paid_after) }}</td>
                            <td>{{ number_2_format($contribmonth->interest_before) }}</td>
                            <td>{{ number_2_format($contribmonth->interest_after) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>