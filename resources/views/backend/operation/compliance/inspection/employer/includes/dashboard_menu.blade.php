<div class="nav_tab_pane_header">
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
                @if ($employer_task->progressive_stage < 5)
                    {{--Update Visitation--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.employer_task.post_update_visitation', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . "Update Visitation", [$employer_task->id],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                @endif
                {{--initialize workflow--}}
                <span>
                    {!! Html::decode($employer_task->initialize_workflow) !!}
                </span>
                @if ($employer_task->progressive_stage >= 2)
                    @include("backend/operation/compliance/inspection/employer/includes/interrupting_workflow_more_links")
                    @include("backend/operation/compliance/inspection/employer/includes/letter_menu")
                @endif

                @include("backend/operation/compliance/inspection/employer/includes/dashboard_more_links")

            </div>
        </div>
    </div>
</div>