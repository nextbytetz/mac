<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5">{!! $employer_task->stage_label !!}</span>
    <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'inspection_type', $task->inspectionType->name) !!}</strong></span>

    <span class="navbar-brand mb-0 h5">
         <small >
                    @lang('labels.backend.compliance_menu.inspection.inspection_#'): {!! Form::label( 'inspection_id', $inspection->filename) !!}
                </small>
                <small >
                    @lang('labels.backend.compliance_menu.inspection.task.task_#'): {!! Form::label( 'inspection_id', $task->id) !!}
                </small>
        <small >
                    Employer:&nbsp;<a href="{{ route('backend.compliance.employer.profile', $employer->id) }}">{{ $employer->name }}</a>
                </small>
    </span>

</nav>
<legend></legend>
@if ($employer_task->file_error == 1)
    {{--generic file error--}}
    <div class="row">
        <div class="col-md-12">
            <div class="alert-left-border">
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{!! $employer_task->upload_error !!}</strong>
                </div>
            </div>
        </div>
    </div>
@elseif ($employer_task->file_error == 2)
    {{--uploaded file error--}}
    <div class="row">
        <div class="col-md-12">
            <div class="alert-left-border">
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{ $employer_task->upload_error }}</strong>&nbsp;Download Error Report&nbsp;&nbsp;&nbsp;<a href="{{ route("backend.compliance.inspection.employer_task.download_assessment_error", $employer_task->id) }}" class="btn btn-secondary btn-sm site-btn">Download</a>
                </div>
            </div>
        </div>
    </div>
@endif
<br/>