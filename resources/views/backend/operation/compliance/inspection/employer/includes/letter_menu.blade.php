<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >Letter</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">


        {{--inspection notice letter--}}
        <span>
                <a href="{{ route("backend.letter.process", [$employer_task->id, "CLINNOLTR"]) }}"  class="dropdown-item" ><i class="icon fa fa-newspaper-o"></i>&nbsp;Notice Letter</a>
        </span>

        @if ($employer_task->progressive_stage >= 5)
            {{--response letter (demand notice letter/letter of compliance)--}}
            <div class="dropdown-divider"></div>
            @if($employer_task->iscompliant)
                {{--compliant employer--}}
                <span>
                    <a href="{{ route("backend.letter.process", [$employer_task->id, "CLLTRFCMPLNC", "consolidated"]) }}"  class="dropdown-item" ><i class="icon fa fa-newspaper-o"></i>&nbsp;Letter of Compliance</a>
                </span>
            @else
                {{--has outstanding--}}
                {{--demand notice letter--}}
                <span>
                    <a href="{{ route("backend.letter.process", [$employer_task->id, "CLDNDNOLTR", "consolidated"]) }}"  class="dropdown-item" ><i class="icon fa fa-newspaper-o"></i>&nbsp;Demand Notice Letter</a>
                </span>
                {{--reminder letter--}}
                <div class="dropdown-divider"></div>
                <span>
                    <a href="{{ route("backend.letter.process", [$employer_task->id, "CLEMPRMDLTR", "consolidated"]) }}"  class="dropdown-item" ><i class="icon fa fa-newspaper-o"></i>&nbsp;Reminder Letter</a>
                </span>
            @endif
            @if ($employer_task->hasoverpay)
                {{--has overpayment--}}
                <div class="dropdown-divider"></div>
                <span>
                    <a href="{{ route("backend.letter.process", [$employer_task->id, "CLRFNDOVRPYMNT"]) }}"  class="dropdown-item" ><i class="icon fa fa-newspaper-o"></i>&nbsp;Refund Letter</a>
                </span>
            @endif

        @endif

     </span>
</span>