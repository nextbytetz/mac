@extends('layouts.backend.main', ['title' => 'Employer Inspection Task', 'header_title' => 'Employer Inspection Task'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . '/nextbyte/plugins/select2/css/select2.min.css') }}
    {{ Html::style(asset_url() . '/nextbyte/plugins/sweetalert/css/sweetalert.css') }}
    {{ Html::style(asset_url() . '/nextbyte/plugins/sweetalert/css/google.css') }}
    <style>
        .wcf_employer:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Visitation Summary";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .wcf_employer {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
        .payroll:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "Assessment Payrolls";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .payroll {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
        sup {
            font-size: 11px;
        }

        /* start : fixed column & header table */
        div .table-wrap {
            /*max-width: 40em;*/
            width: 100%;
            max-height: 40em;
            overflow: scroll;
            position: relative;
        }
        .table-wrap table {
            position: relative;
            border-collapse: collapse;
        }
        .table-wrap td, th {
            padding: 0.25em;
            /*padding:5px 10px;*/
            white-space:nowrap;
            vertical-align:top;
        }
        .table-wrap thead th {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            top: 0;
            background: #000;
            color: #8c8eff;
        }
        .table-wrap thead th:first-child {
            left: 0;
            z-index: 1;
        }
        .table-wrap tbody th {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            left: 0;
            background: #fafff4;
            border-right: 1px solid #09cc73;
        }
        /* end : fixed column & header table */

    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/compliance/inspection/employer/includes/header_info")
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs">
                    {{--employer task process--}}
                    <li class="nav-item">
                        <a class="nav-link active" id="inspection_process_tab" data-toggle="tab" href="#employer_task_process" role="tab">
                            <i class="icon fa fa-briefcase" aria-hidden="true"></i>Employer Task Process
                        </a>
                    </li>
                    {{--General Information--}}
                    <li class="nav-item">
                        <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                    {{--Assessment Schedule--}}
                    <li class="nav-item">
                        <a class="nav-link" id="assessment_schedule_tab" data-toggle="tab" href="#assessment_schedule" role="tab">
                            <i class="icon fa fa-calculator" aria-hidden="true"></i>Assessment Schedule
                        </a>
                    </li>
{{--                    @if (!$employer_task->iscompliant Or $employer_task->hasoverpay)--}}
                    @if (true)
                        {{--Assessment Schedule--}}
                        <li class="nav-item">
                            <a class="nav-link" id="remittance_tab" data-toggle="tab" href="#remittance" role="tab">
                                <i class="icon fa fa-money" aria-hidden="true"></i>Remittance
                            </a>
                        </li>
                    @endif
                    {{--Document Centre--}}
                    <li class="nav-item">
                        <a class="nav-link" id="document_centre_tab" data-toggle="tab" href="#document_centre" role="tab">
                            <i class="icon fa fa-folder-open" aria-hidden="true"></i>Document Centre
                        </a>
                    </li>
                    {{--Follow-ups--}}
                    <li class="nav-item">
                        <a class="nav-link" id="followups_tab" data-toggle="tab" href="#followups" role="tab">
                            <i class="icon fa fa-paw" aria-hidden="true"></i>Follow Ups
                        </a>
                    </li>
                    {{--Workflow History--}}
                    <li class="nav-item">
                        <a class="nav-link" id="workflow_history_tab" data-toggle="tab" href="#workflow_history" role="tab">
                            <i class="icon fa fa-inbox" aria-hidden="true"></i>Workflow History
                        </a>
                    </li>
                </ul>
                {{--<div class="divider15"></div>--}}
                <legend>{!! $employer_task->next_stage !!}</legend>
                <div class="nav_tab_contain tab-content">
                    <div class="tab-pane active" id="employer_task_process" role="tabpanel">
                        @include("backend/operation/compliance/inspection/employer/includes/dashboard_menu")

                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    {{-- employer task list overview --}}
                                    <br/>
                                    <legend class="grey_info" >Active Workflow</legend>
                                    @include("backend/operation/compliance/inspection/employer/includes/current_workflow")

                                    @include("backend/operation/compliance/inspection/employer/includes/visitation_summary")

                                    <div class="row">
                                        <div class="col-md-6">
                                            @include("backend/operation/compliance/inspection/employer/includes/payroll")
                                        </div>
                                        <div class="col-md-6">
                                            @if ($employer_task->progressive_stage >= 4)
                                                @include("backend/operation/compliance/inspection/employer/includes/assessment_summary")
                                                <hr/>
                                                @include("backend/operation/compliance/inspection/employer/includes/payroll_summary")
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/compliance/inspection/employer/summary')
                                    {{--@include('backend/operation/compliance/inspection/summary')--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="general_information" role="tabpanel">
                        <div class="col-md-6">
                            {{--Inpection Stagings Logs--}}
                            <legend class="grey_modal" >Staging Logs</legend>
                            <div class="label label-info">Employer Inspection Task <b>{{ $employer_task->file_name_label }}</b> Status </div>
                            <legend></legend>
                            @include("backend/operation/compliance/inspection/employer/includes/stage_comments", ['stages' => $employer_task->stages()])
                        </div>
                        <div class="col-md-6">
                           {{-- Write all the letters to employer--}}
                            <legend class="grey_modal" >Correspondences</legend>
                            @include("backend/operation/compliance/inspection/employer/includes/correspondences")
                        </div>
                    </div>
                    <div class="tab-pane" id="assessment_schedule" role="tabpanel">
                        @include('backend/operation/compliance/inspection/employer/includes/assessment')
                    </div>
{{--                    @if (($employer_task->iscompliant == 0) Or $employer_task->hasoverpay)--}}
                    @if (true)
                        <div class="tab-pane" id="remittance" role="tabpanel">
                            @include('backend/operation/compliance/inspection/employer/includes/remittance')
                        </div>
                    @endif
                    <div class="tab-pane" id="followups" role="tabpanel">
                        @include('backend/operation/compliance/member/employer/staff_relationship/general_followups/includes/follow_ups_by_employer_dt', ['employer_id' => $employer_task->employer_id])
                    </div>
                    <div class="tab-pane" id="document_centre" role="tabpanel">
                        <br/>
                        @include("backend/operation/compliance/inspection/employer/includes/document_center")
                    </div>
                    <div class="tab-pane" id="workflow_history" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    {{--Put workflow history content here ...--}}
                                    @include("backend/operation/compliance/inspection/employer/includes/workflow_history")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("backend.system.workflow.includes.initiate_modal")
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/clipboard/clipboard.min.js") }}
<script>
    $(document).ready(function () {

        new ClipboardJS('.copy_inspection_minute_note');

        if (location.hash !== '') {
            let $linkhref = $('a[href="' + location.hash + '"]');
            $linkhref.tab('show');
            $linkhref.trigger('click');
        }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });
</script>
@endpush