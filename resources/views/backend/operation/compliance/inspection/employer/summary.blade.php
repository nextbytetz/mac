<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Task Summary</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">Status :</h6>
        <div style="font-weight: bold;">{!! $employer_task->status_label !!}</div>
        <h6 class="underline" style="font-weight: lighter;">Visit Date :</h6>
        <div style="font-weight: bold;">{{ $employer_task->visit_date_formatted }}</div>
        <h6 class="underline" style="font-weight: lighter;">Inspector :</h6>
        <div style="font-weight: bold;">{{ ($employer_task->allocatedUser()->count()) ? $employer_task->allocatedUser->name : "-" }}</div>
        <h6 class="underline" style="font-weight: lighter;">Last Review Date :</h6>
        <div style="font-weight: bold;">{{ $employer_task->last_review_end_date_label }}</div>
        <h6 class="underline" style="font-weight: lighter;">Commencement Date :</h6>
        <div style="font-weight: bold;">{{ $employer_task->doc_formatted }}</div>

    </div>
</div>
