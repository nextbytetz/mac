@extends('layouts.backend.main', ['title' => 'Manage Payrolls', 'header_title' => 'Manage Payrolls'])

@push('after-styles-end')
    {{ Html::style(asset_url() . '/nextbyte/plugins/select2/css/select2.min.css') }}
<style>

</style>
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <?php
        $color = [
            0 => 'blue',
            1 => 'grey',
        ];
    ?>
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include('backend/operation/compliance/inspection/employer/includes/header_info')
    </div>
    {!! Form::model($employer_task, ['route' => ['backend.compliance.inspection.employer_task.post_payroll', $employer_task->id], 'name' => 'update_inspection_payroll', 'class' => 'update_inspection_payroll']) !!}
    {!! Form::hidden("review_start", $employer_task->review_start_date) !!}
    {!! Form::hidden("review_end", $employer_task->review_end_date) !!}
    <div class="row">
        <div class="col-md-12">
            @if ($employer_task->progressive_stage < 5)
                <div class="fileld-layout">
                    <div class="form-group">
                        <div class="form-inline">
                        <span>
                            <label class="required">Number of Payrolls</label>&nbsp;&nbsp;
                        </span>
                            <span>
                            {!! Form::text('payroll_counts', null, ['class' => 'form-control number', 'style' => "border-radius:3px;width:5%;", 'id' => 'payroll_counts']) !!}&nbsp;&nbsp;
                        </span>
                            <span>
                            <a href="#" id="add_payroll_entry"><i class="icon fa fa-2x fa-plus" aria-hidden="true" style="color:darkblue"></i></a>
                        </span>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
            @endif
            @foreach ($payrolls as $payroll)
                @php
                    $i = $payroll->id;
                @endphp

                <div class="fileld-layout" id="payroll_entry_group{!! $i !!}" style="padding-left:20px; border-left: 2px solid {!! $color[$i % 2] !!};">
                    <div class="form-group">
                        <legend></legend><br/>
                        <div class="form-inline">
                <span>
                    <label class="required">  Payroll Name :</label>&nbsp;&nbsp;
                </span>
                            <span>
                    {!! Form::text('payroll_name' . $i, $payroll->name, ['class' => 'form-control payroll_name', 'style' => "border-radius:3px;width:20%;", 'id' => 'payroll_name' . $i]) !!}&nbsp;&nbsp;
                </span>
                            <span>
                    Payroll Manager/Online User:
                </span>
                            <span>
                    {!! Form::select('payroll_id' . $i, $users, $payroll->payroll_id, ['class' => 'form-control search-select payroll_id','style'=>'width:20%', 'placeholder' => '', 'id' => 'payroll_id' . $i]) !!}
                </span>
                            <span>
                    Review Start Date:
                </span>
                           <span>
                               {!! Form::text('review_start_date' . $i, (!$payroll->review_start_date) ? $employer_task->review_start_date : $payroll->review_start_date, ['placeholder' => '', 'class' => 'form-control datepicker','style'=>'width:10%', 'autocomplete' => 'off']) !!}
                           </span>
                            <span>
                    Review End Date:
                </span>
                          <span>
                              {!! Form::text('review_end_date' . $i, (!$payroll->review_end_date) ? $employer_task->review_end_date : $payroll->review_end_date , ['placeholder' => '', 'class' => 'form-control datepicker','style'=>'width:10%', 'autocomplete' => 'off']) !!}
                          </span>
                    <span class="help-block"></span>
                    @if (!$payroll->isdefault && ($employer_task->progressive_stage < 5))
                        <span class="pull-right">
                            <a href="#" id="{!! $i !!}" class="remove_payroll_entry"><i class="icon fa fa-2x fa-remove" aria-hidden="true" style="color:darkred;"></i></a>
                        </span>
                    @endif
                        </div>
                    </div>
                </div>
            @endforeach
            <div id="payroll_entries">

            </div>
            <div class="form-group">
                <div class="pull-right">
                    <a href="{!! route('backend.compliance.inspection.employer_task.show', $employer_task->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-thumbs-up"></i>&nbsp;Done</a>
{{--                    @if ($employer_task->progressive_stage < 5)--}}
                    @if (true)
                        <button class="btn btn-secondary site-btn" type="submit">@lang('buttons.general.crud.update')</button>
                    @endif
                </div>
                {{--<small class="form-text text-muted" style="width:100% !important;">Can select multiple users</small>--}}
            </div>
            <br/>
            <br/>
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script>
    $(function() {
        $(".search-select").select2({});
        let payroll_counts = $("#payroll_counts");
        let $body = $('body');
        /* start : on click payroll entry add link */
        $body.on('click', 'a#add_payroll_entry', function ($e) {
            $e.preventDefault();
            selected_entries = payroll_counts.val();
            if (validate_entry(selected_entries, payroll_counts)) {
                $.post("{!! route('backend.compliance.inspection.employer_task.payroll_entries', $employer_task->id) !!}", {'selected_entries' : selected_entries}, function( data ) {
                    $( data ).prependTo( "#payroll_entries" );
                }, "html").done(function () {
                    $(".search-select").select2({});
                    jQuery('.datepicker').datetimepicker({
                        timepicker:false,
                        format:'Y-n-j',
                        weeks: true,
                        dayOfWeekStart: 1,
                        lazyInit: true,
                        scrollInput: false
                    });
                });
            }
        });
        $body.on('submit', 'form.update_inspection_payroll', function (e) {
            e.preventDefault();
            var $form = this;
            $($form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            var $options = {
                dataType : "json",
                type : "POST",
                url : $($form).attr("action"),
                success : function (data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    if (data.success) {
                        $("<div class='alert alert-success'>Success, All changes have been updated!</div>").appendTo($($form)).delay(2000).fadeOut();
                    }
                },
                error: function ($data) {
                    var $errors = $.parseJSON($data.responseText);
                    /*console.log(errors);*/
                    $.each($errors, function(index, value) {
                        $("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });
                },
            };
            // pass options to ajaxForm
            $($form).ajaxSubmit($options);
        });
        $body.on('click', 'a.remove_payroll_entry', function (e) {
            e.preventDefault();
            var id = this.id;
            $("#payroll_entry_group" + id).remove();
        });
    });
    function validate_entry(selected_entries, control) {
        if (selected_entries === '') {
            control.addClass('form-error');
            setTimeout(
                function() { control.removeClass('form-error'); },
                2000
            );
            return false;
        } else {
            return true;
        }
    }
</script>
@endpush