@extends('layouts.backend.main', ['title' => "Update Employer Task Visitation", 'header_title' => "Update Employer Task Visitation"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <div class="row">
        @include("backend/operation/compliance/inspection/employer/includes/header_info")
    </div>
    <!-- Put the page specifically for this page here -->
    {!! Form::model($employer_task, ['route' => ['backend.compliance.inspection.employer_task.post_update_visitation', $employer_task->id], 'name' => 'update_employer_task_visitation']) !!}
    {!! Form::hidden('inspection_start', $inspection->start_date) !!}
    {!! Form::hidden('inspection_end', ($inspection->end_date) ? $inspection->end_date : \Carbon\Carbon::now()->format("Y-n-j")) !!}
    {!! Form::hidden('wcf_start_date', getWCFLaunchDate()) !!}
    {!! Form::hidden('today_date', getTodayDate()) !!}
    {!! Form::hidden('last_review_end_date', $employer_task->last_review_end_date) !!}
    <div class="row">

        <div class="col-md-4" style="padding-right:20px; border-right: 1px solid #ddd;">

            <div class="filed-layout">
                <label class="required">Visit Date</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('visit_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('visit_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="filed-layout">
                <label class="required">Review Start Date</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('review_start_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('review_start_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="filed-layout">
                <label class="required">Review End Date</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('review_end_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('review_end_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="fileld-layout">
                <label class="required">Findings</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea('findings', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('findings', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
        <div class="col-md-4">

            <div class="filed-layout">
                <label>Inspection Date</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('last_inspection_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('last_inspection_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="filed-layout">
                <label>Exit Meeting Date</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('exit_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('exit_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="filed-layout">
                <label>Date of Commencement</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('doc', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="fileld-layout">
                <label class="required">Resolutions</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea('resolutions', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('resolutions', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
        <div class="col-md-4">
            @include('backend/operation/compliance/inspection/employer/summary')
        </div>

    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="Update" />
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function () {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            /* start: Submitting Form and perform validation on the server side */
            $('body').on('submit', 'form[name=update_employer_task_visitation]', function (e) {
                e.preventDefault();
                var $form = this;
                $form.submit();
            });
            /* end: Submitting Form and perfom validation on the server side */
        });
    </script>
@endpush