<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h5">{!! $inspection->status_label !!}</span>
    <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'name', $inspection->inspectionType->name) !!}</strong></span>

    <span class="navbar-brand mb-0 h5">
        <small >
            @lang('labels.backend.compliance_menu.inspection.inspection_#')&nbsp;:&nbsp;<span class="underline">{!! $inspection->file_name_label !!}</span>
        </small>
        <small>
            {!! $inspection->complete_status_label !!}
        </small>
        <small>
            {!! $inspection->iscancelled_label !!}
        </small>
    </span>
</nav>
<legend></legend>
<br/>