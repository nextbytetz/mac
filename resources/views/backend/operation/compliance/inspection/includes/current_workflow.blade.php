@if ($inspection->can_display_workflow)
    @if ($inspection->isinitiated)
        @php
            $wfModule = $inspection->wfModule;
            //$type = $wfModule->type;
            $moduleGroup = $wfModule->wfModuleGroup->id;
            $workflowinput = ['resource_id' => $inspection->id, 'wf_module_group_id'=> $moduleGroup, 'type' => $wfModule->type];
        @endphp
        {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
        <br/>
    @else
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <i class="fa fa-exclamation-circle"></i><strong>Info</strong> No active workflow recently initiated
        </div>
    @endif
@endif