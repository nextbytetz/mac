<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >More</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">

        {{--edit--}}
        @if ($inspection->can_edit)
            <span>
                <a href="{!! route('backend.compliance.inspection.edit', $inspection->id) !!}"  class="dropdown-item" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>
            </span>
        @endif

        {{--complete--}}
        @if ($inspection->can_complete)
            <span>
                <div class="dropdown-divider"></div>
                {!! HTML::decode(link_to_route('backend.compliance.inspection.complete', "<i class='icon fa fa-check-square-o' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.complete'), [$inspection->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.complete_confirm'), 'class' => 'dropdown-item']))  !!}
            </span>
        @endif
        {{--cancel--}}
        @if ($inspection->can_cancel)
            <span>
                <div class="dropdown-divider"></div>
                {!! HTML::decode(link_to_route('backend.compliance.inspection.cancel', "<i class='icon fa fa-scissors' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.cancel'), [$inspection->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.cancel_confirm'), 'class' => 'dropdown-item']))  !!}
                <div class="dropdown-divider"></div>
            </span>
        @endif
        {{--close--}}
        <span>
            <a href="{!! route('backend.compliance.inspection.index') !!}"  class="dropdown-item" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
    </span>

     </span>
</span>