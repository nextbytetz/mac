<div class="nav_tab_pane_header">
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
                {{--resource allocation--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.allocation', "<i class='icon fa fa-users' aria-hidden='true'></i>&nbsp;" . "Resource Allocation", [$inspection->id],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>

                @if ($inspection->progressive_stage < 2 And $inspection->can_edit_task)
                    {{--assign tasks--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.task.assign', "<i class='icon fa fa-tasks' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.task.assign'), [$inspection->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.task.assign_confirm'), 'class' => 'btn btn-primary site-btn nav_button']))  !!}
                    </span>
                @endif

                {{--assign inspectors--}}
                <span>
                                            @if($inspection->can_assign_inspectors)
                        <span>
                                                    <a href="{!! route('backend.compliance.inspection.assign_inspectors', ['inspection' => $inspection->id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-users"></i>&nbsp;Assign Inspectors</a>
                                                </span>
                    @endif
                                        </span>
                {{--initialize workflow--}}
                <span>
                                            {!! Html::decode($inspection->initialize_workflow) !!}
                                        </span>

                @if ($inspection->can_edit)
                    {{--new task--}}
                    <span>
                                                <a href="{!! route('backend.compliance.inspection.task.create', ['inspection' => $inspection->id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.backend.compliance_menu.inspection.task.new')</a>
                                            </span>
                @endif
                @include("backend.operation.compliance.inspection.includes.dashboard_more_links")
            </div>
        </div>
    </div>
</div>