<div class="nav_tab_pane_header">
    {{--Header Bar--}}
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
                {{--resource allocation--}}
{{--                <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.current', "<i class='icon fa fa-users' aria-hidden='true'></i>&nbsp;" . "Resource Allocation", [],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>--}}
                {{--system raised inspection--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.current', "<i class='icon fa fa-laptop' aria-hidden='true'></i>&nbsp;" . "System Raised", [],['class' => 'btn btn-warning site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                {{--employer inspection profiles--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.profile.index', "<i class='icon fa fa-database' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.profile.heading'), [],['class' => 'btn btn-secondary site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                {{--current inspections--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.current', "<i class='icon fa fa-ellipsis-v' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.current'), [],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                {{--all inspections--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.index', "<i class='icon fa fa-list' aria-hidden='true'></i>&nbsp;" . __('labels.backend.compliance_menu.inspection.title'), [],['class' => 'btn btn-info site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                {{--new inspection profile--}}
                {{--a graphical or other representation of information relating to particular characteristics of something, recorded in quantified form.--}}
                {{--new inspection--}}
                <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.create', "<i class='icon fa fa-plus-square-o' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.new'), [],['class' => 'btn btn-success site-btn', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
            </div>
        </div>
    </div>
</div>
<br/>