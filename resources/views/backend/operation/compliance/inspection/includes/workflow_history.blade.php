<br/>
<div class="row">
    <div class="col-md-12">
        <legend>Workflow History</legend>

        @if ($inspection->progressive_stage >= 2)
            @php
                $whwfmodule = $inspection->wfModule;
            @endphp
            @include("backend.includes.workflow.completed_tracks", ["wf_tracks" => $workflow_history->wfTracks, "module" => $whwfmodule, "resource_id" => $inspection->id, "wf_module_group_id" => $whwfmodule->wf_module_group_id, "type" => $whwfmodule->type])
        @else
            <div class="alert-left-border">
                <div class="alert alert-primary alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>No workflow history.</strong> This inspection has no associated workflow recorded.
                </div>
            </div>
        @endif

    </div>
</div>