@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.dashboard'), 'header_title' => trans('labels.backend.compliance_menu.inspection.dashboard')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/compliance/inspection/includes/header_info")
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs">

                    {{--Inspection Process--}}
                    <li class="nav-item">
                        <a class="nav-link active" id="inspection_process_tab" data-toggle="tab" href="#inspection_process" role="tab">
                            <i class="icon fa fa-briefcase" aria-hidden="true"></i>Inspection Process
                        </a>
                    </li>
                    {{--General Information--}}
                    <li class="nav-item">
                        <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                    {{--Workflow History--}}
                    <li class="nav-item">
                        <a class="nav-link" id="workflow_history_tab" data-toggle="tab" href="#workflow_history" role="tab">
                            <i class="icon fa fa-folder-open" aria-hidden="true"></i>Workflow History
                        </a>
                    </li>
                </ul>
                {{--<div class="divider15"></div>--}}
                <legend>{!! $inspection->next_stage !!}</legend>
                <div class="nav_tab_contain tab-content">
                    <div class="tab-pane active" id="inspection_process" role="tabpanel">
                        @include("backend/operation/compliance/inspection/includes/dashboard_menu")
                        <legend class="grey_info" >Active Workflow</legend>
                        <div class = "row">
                            <div class="col-md-12">
                                @include("backend/operation/compliance/inspection/includes/current_workflow")
                            </div>
                        </div>
                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    {{-- task list overview --}}
                                    @include('backend/operation/compliance/inspection/task_list')
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/compliance/inspection/summary')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="general_information" role="tabpanel">
                        <div class="col-md-6">
                            {{--Inpection Stagings Logs--}}
                            <legend class="grey_modal" >Staging Logs</legend>
                            <div class="label label-info">Inspection <b>{{ $inspection->file_name_label }}</b> Status </div>
                            <legend></legend>
                            @include("backend/operation/compliance/inspection/includes/stage_comments", ['stages' => $inspection->stages()])
                        </div>
                    </div>
                    <div class="tab-pane" id="workflow_history" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 pills-height">
                                <div data-plugin="scrollbar" data-height="1500">
                                    {{--Put workflow history content here ...--}}
                                    @include("backend/operation/compliance/inspection/includes/workflow_history")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include("backend.system.workflow.includes.initiate_modal")
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script>
    $(function () {
        if (location.hash !== '') {
            let $linkhref = $('a[href="' + location.hash + '"]');
            $linkhref.tab('show');
            $linkhref.trigger('click');
        }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });
</script>
@endpush