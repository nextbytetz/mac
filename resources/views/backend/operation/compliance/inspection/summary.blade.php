<div class="row">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.compliance_menu.inspection.summary')</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.type') :</h6>
        <div style="font-weight: bold;">{!! $inspection->inspectionType->name !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.start_date') :</h6>
        <div style="font-weight: bold;">{!! $inspection->start_date !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.end_date') :</h6>
        <div style="font-weight: bold;">{!! $inspection->end_date_formatted !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.duration') :</h6>
        <div style="font-weight: bold;">{!! $inspection->duration_formatted !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.comments') :</h6>
        <div style="font-weight: bold;">{!! $inspection->comments !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.completed') :</h6>
        <div style="font-weight: bold;">{!! $inspection->completed_label !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.staffs') :</h6>
        @if ($inspection->users()->count())
            @foreach ($inspection->users as $user)
                <?php
                $count = $user->inspectionTasks()->where("inspection_id", $inspection->id)->count();
                ?>
                <div><i class="icon fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;{!! $user->name !!} &nbsp;&nbsp; (&nbsp; {!! trans_choice("labels.backend.compliance_menu.inspection.task.count", $count, ['count' => $count]) !!} &nbsp;)</div>
            @endforeach
        @else
            <div style="font-weight: bold;">@lang('labels.general.none')</div>
        @endif
    </div>
</div>
