@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.edit'), 'header_title' => trans('labels.backend.compliance_menu.inspection.edit')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@include('backend.includes.assets.datetimepicker')

@section('content')
    <!-- Put the page specifically for this page here -->
    {!! Form::model($inspection, ['route' => ['backend.compliance.inspection.update', $inspection->id], 'name' => 'update_inspection', 'method' => 'PUT']) !!}
    {!! Form::hidden('user_id', access()->id()) !!}
    <div class="row">
        @include("backend/operation/compliance/inspection/includes/header_info")
    </div>
    <legend></legend>
    <br/>
    <div class="row">
        <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">

            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.type')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('inspection_type', $inspection_types, $inspection_type->reference, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'inspection_type']) !!}
                    </div>
                    {!! $errors->first('inspection_type', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="fileld-layout" id="inspection_category" style="display: none;">
                <label class="required">Category</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('trigger_category', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'trigger_category']) !!}
                    </div>
                    {!! $errors->first('trigger_category', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="filed-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.start_date')</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('start_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('start_date', '<span class="help-block label label-danger">:message</span>') !!}
                    {!! Form::hidden('today_date', getTodayDate()) !!}
                    {!! Form::hidden('previous_start_date', $start_date->format("Y-n-j")) !!}
                </div>
            </div>

            <div class="filed-layout">
                <label>@lang('labels.backend.compliance_menu.inspection.end_date')</label>
                <div class="form-group">
                    <div class="input-group" style="width:50%;">
                        {!! Form::text('end_date', null, ['placeholder' => '', 'class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    {!! $errors->first('end_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

{{--            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.staffs')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('staffs[]', $inspection_staffs, $inspection_staffs_ids, ['class' => 'staffs-select', 'style' => 'width:100%']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;">
		                <p>{{ trans('labels.backend.compliance_menu.inspection.staffs_helper') }}</p>
                        {!! $errors->first('staffs', '<p class="help-block label label-danger">:message</p>') !!}
	                </span>
                </div>
            </div>--}}

        </div>
        <div class="col-md-6">
            <div class="fileld-layout">
                <label>@lang('labels.backend.compliance_menu.inspection.comments')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea('comments', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    {!! $errors->first('comments', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                {!! link_to_route('backend.compliance.inspection.show',trans('buttons.general.cancel'), [$inspection->id],['id'=> 'cancel', 'class' => 'btn btn-primary btn-sm cancel_button', ]) !!}
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.update')" />
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
<script>
    $(function () {
        $(".search-select").select2({});
        autosize($("textarea.autosize"));
        let $inspectionType = $('#inspection_type');
        let $triggerCategory = $("#trigger_category");
        let $inspectionCategory = $("#inspection_category");
        $inspectionType.on('change', function (e) {
            let $inspectionTypeValue = e.target.value;
            switch ($inspectionTypeValue) {
                case "ITROUTINE":
                case "ITSPECIAL":
                    $("#spin2").show();
                    $inspectionCategory.show();
                    $.post("{{ url('/') }}/getCodeValues?reference=" + $inspectionTypeValue, function (data) {
                        $triggerCategory.empty();
                        $triggerCategory.select2("val", "");
                        $triggerCategory.html(data);
                        $("#spin2").hide();
                        $triggerCategory.val("{{ $inspection->trigger_category_cv_id }}").trigger('change.select2');
                    });
                    /*alert("hi");*/
                    break;
                default:

                    $inspectionCategory.hide();
                    break;
            }
        });
        $inspectionType.trigger("change");

        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form[name=update_inspection]', function (e) {
            e.preventDefault();
            var $form = this;

            $form.submit();
        });
        /* end: Submitting Form and perfom validation on the server side */
        /* start : Lazy loading the list of staffs */
/*        $(".staffs-select").select2({
            minimumInputLength: 2,
            multiple: true,
            ajax: {
                url: "{!! route('backend.users.search') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.staff,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        {{--$('.staffs-select').val({!! json_encode($inspection_staffs_ids) !!});--}}
        $('.staffs-select').trigger('change.select2');*/
        /* end : Lazy loading the list of staffs */
    });
</script>
@endpush