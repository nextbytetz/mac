<legend class="grey_modal" >@lang('labels.backend.compliance_menu.inspection.task.list')</legend>
<table class="display" cellspacing="0" width="100%" id ="inspection_task_table">
    <thead>
    <tr>
        <th></th>
        <th>Type</th>
        <th>@lang('labels.general.name')</th>
        <th>@lang('labels.backend.compliance_menu.inspection.task.deliverable')</th>
        <th>@lang('labels.backend.compliance_menu.inspection.completed')</th>
    </tr>
    </thead>
</table>

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        $('#inspection_task_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.inspection.task.get', $inspection->id) !!}',
                type : 'post',
                data : {inspection_id: "{!! $inspection->id !!}"},
            },
            columns: [
                { data: 'id', name: 'inspection_tasks.id', visible: false },
                { data: 'inspection_type' , name: 'code_values.name' },
                { data: 'name' , name: 'inspection_tasks.name' },
                { data: 'deliverable' , name: 'inspection_tasks.deliverable', orderable: false},
                { data: 'completed' , name: 'completed', orderable: false, searchable: false },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/compliance/inspection/task/"  + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        } );
    });
</script>;

@endpush