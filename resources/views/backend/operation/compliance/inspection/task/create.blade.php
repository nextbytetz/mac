@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.task.create'), 'header_title' => trans('labels.backend.compliance_menu.inspection.task.create')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="row">
        <div class="col-md-8 col-sm-8" style="padding-right:20px; border-right: 1px solid #ddd;">
            {!! Form::open(['route' => 'backend.compliance.inspection.task.store', 'name' => 'create_inspection_task', 'class' => 'create_inspection_task', 'enctype' => 'multipart/form-data']) !!}
            {!! Form::hidden('inspection_id', $inspection->id) !!}
            {!! Form::hidden("last_id", 0) !!}
            {{--{!! Form::hidden('inspection_type', $inspection->inspection_type_cv_id) !!}--}}

            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.type')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('inspection_type', $inspection_types, null, ['class' => 'form-control search-select', 'style' => 'width:100%', 'placeholder' => '', 'id' => 'inspection_type']) !!}
                        <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                    </div>
                    <span class="help-block" style="width:100% !important;">
                        {!! $errors->first('inspection_type', '<span class="help-block label label-danger">:message</span>') !!}
	                </span>

                </div>
            </div>

            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.task.name')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea('name', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;">
		                <small>{{ trans('labels.backend.compliance_menu.inspection.task.name_helper') }}</small>
                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
	                </span>

                </div>
            </div>

            <div class="fileld-layout">
                <label>@lang('labels.backend.compliance_menu.inspection.task.deliverable')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::textarea('deliverable', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;">
		                <small>{{ trans('labels.backend.compliance_menu.inspection.task.deliverable_helper') }}</small>
                        {!! $errors->first('deliverable', '<span class="help-block label label-danger">:message</span>') !!}
	                </span>

                </div>
            </div>

{{--            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.staffs')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('staffs[]', [], null, ['class' => 'staffs-select', 'style' => 'width:100%']) !!}
                    </div>
                    <span class="help-block" style="width:100% !important;">
		                <p>{{ trans('labels.backend.compliance_menu.inspection.task.staffs_helper') }}</p>
                        {!! $errors->first('staffs', '<p class="help-block label label-danger">:message</p>') !!}
	                </span>
                </div>
            </div>--}}
            <div class="fileld-layout">
                <label>@lang('labels.backend.compliance_menu.inspection.task.target_employer')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('target_employer', ['1' => 'Employer Profile', '0' => 'Individual Employers', '2' => 'Import from Excel'], null, ['class' => 'form-control search-select employer_category_select','style'=>'width:100%', 'placeholder' => '']) !!}
                    </div>
                    <span class="help-block">
                        <small>{{ trans('labels.backend.compliance_menu.inspection.task.target_employer_helper') }}</small>
                        {!! $errors->first('target_employer', '<span class="help-block label label-danger">:message</span>') !!}
                    </span>
                </div>
            </div>
            <div id="import_from_excel" style="display: none;">
                <div class="fileld-layout">
                    <label>Employer File</label>
                    <div class="form-group">
                        {!! Form::file('document_file', ['class' => 'btn btn-secondary btn-save btn-block']) !!}
                        <span class="help-block">
                            <span>File containing list of employers to be inspected</span>
                        </span>
                    </div>
                </div>
                @include("backend/operation/compliance/inspection/task/includes/employer_attachment_description")
            </div>
            {{--@if($inspection->inspection_type_id <> 3)--}}
                {{--registered employers--}}
                {{--mark_change1--}}
                <div id="individual_select" style="display: none;">
                    <div class="fileld-layout">
                        <label>@lang('labels.backend.compliance_menu.inspection.task.target')</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::select('employer[]', [], null, ['class' => 'employer-select', 'id' => 'employer_select', 'style' => 'width:100%']) !!}
                            </div>
                            <span class="help-block" style="width:100% !important;">
                                {!! $errors->first('employer', '<span class="help-block label label-danger">:message</span>') !!}
	                </span>
                        </div>
                    </div>
                </div>
            {{--@endif--}}
            {{--New Added (mark_change_1)--}}
            {{--Unregistered individual--}}
            {{--If inspection is follow up type--}}
            {{--mark_change1--}}
{{--            @if($inspection->inspection_type_id == 3)
                <div id="individual_select" style="display: none;">
                    <div class="fileld-layout">
                        <label>@lang('labels.backend.compliance_menu.inspection.task.target')</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::select('employer[]', [], null, ['class' => 'unregistered-employer-select', 'id' => 'unregistered_employer_select', 'style' => 'width:100%']) !!}
                            </div>
                            <span class="help-block" style="width:100% !important;">
		                <p></p>
                                {!! $errors->first('employer', '<span class="help-block label label-danger">:message</span>') !!}
	                </span>
                        </div>
                    </div>
                </div>
            @endif--}}


            <div id="profile_select" style="display: none;">
                <div class="fileld-layout">
                    <label>@lang('labels.backend.compliance_menu.inspection.profile.select')</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('inspection_profile_id', $profiles, null, ['class' => 'profile-select', 'id' => 'profile_select', 'style' => 'width:100%', 'placeholder' => ""]) !!}
                        </div>
                        <span class="help-block" style="width:100% !important;">
		                <small>{{ trans('labels.backend.compliance_menu.inspection.profile.select_helper') }}</small>
                            {!! $errors->first('inspection_profile_id', '<span class="help-block label label-danger">:message</span>') !!}
	                     </span>
                    </div>
                </div>
                <div class="fileld-layout">
                    <label>@lang('labels.backend.compliance_menu.inspection.task.target_count')</label>
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::text('employer_count', null, ['class' => 'form-control', 'id' => 'employer_count', 'style' => "border-radius:3px;"]) !!}
                            <span class="input-group-addon">
                                <i class="icon fa fa-users" aria-hidden="true"></i>
                            </span>
                        </div>
                        <span class="help-block">
                            {!! $errors->first('employer_count', '<span class="help-block label label-danger">:message</span>') !!}
                            {{--<span>{{ trans('labels.backend.finance.receipt.reference_helper') }}</span>--}}
                        </span>
                    </div>
                </div>
            </div>

            {{--<div class="fileld-layout">--}}
            {{--<label>@lang('labels.backend.compliance_menu.inspection.task.other_target')</label>--}}
            {{--<div class="form-group">--}}
            {{--<div class="input-group">--}}
            {{--{!! Form::select('other_employers[]', [], null, ['class' => 'other-employer-select', 'style' => 'width:100%', 'multiple' => 'multiple']) !!}--}}
            {{--</div>--}}
            {{--<span class="help-block" style="width:100% !important;">--}}
            {{--<p>{{ trans('labels.backend.compliance_menu.inspection.task.other_target_helper') }}</p>--}}
            {{--{!! $errors->first('other_employers', '<p class="help-block label label-danger">:message</p>') !!}--}}
            {{--</span>--}}
            {{--</div>--}}
            {{--</div>--}}

            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        {!! link_to_route('backend.compliance.inspection.show',trans('buttons.general.cancel'), [$inspection->id],['id'=> 'cancel', 'class' => 'btn btn-primary btn-sm cancel_button', ]) !!}
                        <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.create')" />
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-4 col-sm-4">
            {{--sidebar summary--}}
            @include('backend/operation/compliance/inspection/summary')
        </div>
    </div>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
<script>
    $(function(){
        autosize($("textarea.autosize"));
        $(function () {
            /** start : Other employer create multiple inputs  */
            $(".other-employer-select").select2({
                tags: true
            });
            /** end : Other employer create multiple inputs */
            $(".search-select").select2();

            $('body').on('submit', 'form.create_inspection_task', function (e) {
                e.preventDefault();
                var $form = this;
                /* start: remove any printed error message in the input controls */
                $($form).find(':input').each(function () {
                    $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                });
                /* end: remove any printed error message in the input controls */
                var $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    beforeSend : function ($e) {
                        $($form).find(".progress-bar").width('0%');
                        $($form).find(".btn-submit").prop('disabled', true);
                    },
                    success : function ($data) {
                        $($form).find(".btn-submit").prop('disabled', false);
                        /*console.log($data);*/
                        if (!$data.success) {
                            swal({title : "Message", text : $data.message, html : true});
                        } else {
                            document.location.href =  $data.return_url;
                        }
                    },
                    error: function ($data) {
                        $($form).find(".progress-bar").width('0%');
                        $($form).find(".btn-submit").prop('disabled', false);
                        var errors = $.parseJSON($data.responseText);
                        /* console.log(errors); */
                        $.each(errors, function($index, $value) {
                            $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        });
                    },
                    uploadProgress : function (event, position, total, percentComplete) {
                        $($form).find(".progress-bar").width(percentComplete + '%');
                        $($form).find(".progress-bar").html('<div class="progress-status">' + percentComplete +' %</div>')
                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });

            let $employer_category_select = $('.employer_category_select');
            $employer_category_select.on("change", function () {
                var $category = $(this).val();
                let $import_from_excel = $("#import_from_excel");
                let $individual_select = $("#individual_select");
                let $$employer_select = $("#employer_select");
                let $profile_select = $("#profile_select");
                let $inspection_profile = $("#inspection_profile_id");
                let $employer_count = $("#employer_count");
                switch($category) {
                    case '0':
                        /* Individual Category */
                        $individual_select.show();
                        $$employer_select.prop('disabled', false);
                        $profile_select.hide();
                        $inspection_profile.prop('disabled', true);
                        $employer_count.prop('disabled', true);
                        $import_from_excel.hide();
                        break;
                    case '1':
                        /*Inspection Profile*/
                        $individual_select.hide();
                        $$employer_select.prop('disabled', true);
                        $profile_select.show();
                        $inspection_profile.prop('disabled', false);
                        $employer_count.prop('disabled', false);
                        $import_from_excel.hide();
                        break;
                    case '2':
                        /* Import from Excel */
                        $individual_select.hide();
                        $$employer_select.prop('disabled', true);
                        $profile_select.hide();
                        $inspection_profile.prop('disabled', true);
                        $employer_count.prop('disabled', true);
                        $import_from_excel.show();
                        break;
                    default:
                        /*None*/
                        $individual_select.hide();
                        $$employer_select.prop('disabled', true);
                        $profile_select.hide();
                        $inspection_profile.prop('disabled', true);
                        $employer_count.prop('disabled', true);
                        $import_from_excel.hide();
                }
            });
            $employer_category_select.trigger('change');
            /* start : Inspection Profile List **/
            $(".profile-select").select2({
                placeholder: "",
                allowClear: true,
            });
            /* end : Inspection Profile List **/
            /** start : Lazy loading the list of staffs */
/*            $(".staffs-select").select2({
                minimumInputLength: 1,
                multiple: true,
                ajax: {
                    url: "{!! route('backend.compliance.inspection.task.users.search') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page,
                            inspection_id: "{!! $inspection->id !!}",
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.staff,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });*/
            /** end : Lazy loading the list of staffs */
            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: true,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

            //New added (mark_change1)
/*            $(".unregistered-employer-select").select2({
                minimumInputLength: 1,
                multiple: true,
                ajax: {
                    url: "{!! route('backend.compliance.unregistered_unassigned_employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });*/


        });
    });
</script>
@endpush