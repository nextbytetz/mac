<legend class="grey_modal" >@lang('labels.backend.compliance_menu.inspection.task.specific')</legend>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="fileld-layout">
            <label>@lang('labels.backend.compliance_menu.inspection.task.filter')</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('staff_filter', $target_staffs, $user_id, ['class' => 'form-control staff_filter_select','style'=>'width:100%', 'placeholder' => trans('labels.backend.compliance_menu.inspection.task.filter_input')]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/*print_r($user_id);*/
if ($user_id != 0) {
    $unregistered_employers = $task->unregisteredEmployers()->where("assign_to", $user_id)->where('inspection_task_id',
        $task->id)
    ->get();
} else {
    $unregistered_employers = $task->unregisteredEmployers;
}
$code_values = new \App\Repositories\Backend\Sysdef\CodeValueRepository();
$follow_up_types = $code_values->query()->where('code_id',9)->get()->pluck('name','id')
?>

<div class="other_employer">
    @if (count($unregistered_employers))
        <div class="row">
            <div class="col-md-12">

                <div id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach ($unregistered_employers as $unregistered_employer)
                        <?php
                        $id = $unregistered_employer->id;
//                        $visit_date = $employer->pivot->visit_date;
                        $status = $unregistered_employer->register_status_label;
                        ?>
                        <div class="card">
                            <div class="card-header">
                                <h7 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#accordion" href="#collapse{!! $id !!}">
                                        {!! $unregistered_employer->name !!}
                                        <span id="status{!! $id !!}">
                                            @if ($unregistered_employer->is_registered == '1')
                                                <span class='tag tag-success' data-toggle='tooltip' data-html='true' title="@lang('labels.general.complete')"> @lang('labels.general.complete')  </span>
                                            @else
                                                <span class='tag tag-warning' data-toggle='tooltip' data-html='true' title="@lang('labels.general.incomplete')"> @lang('labels.general.incomplete') .</span>
                                            @endif
                                        </span>
                                    </a>
                                </h7>

                                <div class="pull-right" >

                                    {{--unregistered Employer profile link--}}
                                    <span>

                                              <a href="{!! route('backend.compliance.unregistered_employer.profile', $unregistered_employer->id) !!}" >@lang('labels.general.profile')</a>
                                    </span>

                                </div>


                            </div>
                            <div id="collapse{!! $id !!}" class="collapse">
                                <div class="card-block">
                                    {!! Form::model($unregistered_employer, ['route' => ['backend.compliance.unregistered_employer.follow_up_store',$unregistered_employer->id ], 'class' => 'update_unregistered_employer', 'method' => 'post', 'name' => 'create','id' =>
                                    $id,
                                    'enctype' =>
                                    'multipart/form-data']) !!}
                                    @if($unregistered_employer->is_registered == 0)
                                    @include("backend/operation/compliance/inspection/task/unregistered_employer_form")
                                    @endif
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    @else
        @lang("labels.general.none")
    @endif
</div>



@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function(){
        $(".search-select").select2({});
        var staff_filter = $(".staff_filter_select").select2({});
        autosize($("textarea.autosize"));
        $(".staff_filter_select").on("change", function () {
            var $user_id = $(this).val();
            document.location.replace(base_url + "/compliance/inspection/task/{!! $task->id !!}?user_id=" + $user_id);
        });



            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            $('body').on('submit', 'form[name=create]', function(e) {
                e.preventDefault();
// Validate date -- date commenced date
                var $day = $('#follow_up_day').val();
                var $month = $('#follow_up_month').val();
                var $year = $('#follow_up_year').val();
                if (($year) && ($month) && ($day )) {
                    $('input[name=date_of_follow_up]').val($year + '-' + $month + '-' + $day);
                }else {
                    $("input[name=date_of_follow_up]").val("");
                }

                this.submit();

            });

            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });


        });

// Follow up type option
        function follow_up_type_option(follow_type_id, contact_div, contact_id) {

            var choice = $("#" + follow_type_id).val();
//            if is visit
            if (choice == 93) {
                $("#" + contact_div).hide();
            } else {
//            phone call
                if (choice == 91) {
                    $("#" + contact_id).attr('placeholder', 'Phone #');
                } else if (choice == 92) {
                    $("#" + contact_id).attr('placeholder', 'Email@yahoo.com');
                }
                $("#" + contact_div).show();

            }
        }

</script>;





@endpush