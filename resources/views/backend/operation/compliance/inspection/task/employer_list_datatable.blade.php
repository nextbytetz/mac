<legend class="grey_modal" >@lang('labels.backend.compliance_menu.inspection.task.specific')</legend>
<table class="display" cellspacing="0" width="100%" id ="employer-inspection-task-table">
    <thead>
    <tr>
        <th>Employer</th>
        <th>Visit Date</th>
        <th>Inspector</th>
    </tr>
    </thead>
</table>

@push('after-script-end')
    <script>
        $(function () {
            let $oTable = $('#employer-inspection-task-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: false,
                info: true,
                ajax:{
                    url : '{!! route('backend.compliance.inspection.task.get_employers_datatable', $task->id) !!}',
                    data : {user_id : $(".staff_filter_select").val()},
                    type : 'post'
                },
                columns: [
                    { data: 'employer', name: 'employers.name'},
                    { data: 'visit_date' , name: 'employer_inspection_task.visit_date'},
                    { data: 'staff' , name: 'users.firstname'},
                    { data: 'id' , name: 'employer_inspection_task.id', searchable: false, orderable: false, visible: false}
                ],
                'rowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href =  base_url + "/compliance/inspection/employer_task/" + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }
            });
        });
    </script>
@endpush