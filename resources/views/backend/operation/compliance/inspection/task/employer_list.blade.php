<legend class="grey_modal" >@lang('labels.backend.compliance_menu.inspection.task.specific')</legend>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="fileld-layout">
            <label>@lang('labels.backend.compliance_menu.inspection.task.filter')</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('staff_filter', $target_staffs, $user_id, ['class' => 'form-control staff_filter_select','style'=>'width:100%', 'placeholder' => trans('labels.backend.compliance_menu.inspection.task.filter_input')]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/*print_r($user_id);*/
if ($user_id != 0) {
    $employers = $task->employers()->where("user_id", $user_id)->get();
    $otherEmployers = $task->otherEmployers()->where("user_id", $user_id)->get();
} else {
    $employers = $task->employers;
    $otherEmployers = $task->otherEmployers;
}
?>

<div class="wcf_employer">
    @if (count($employers))
        <div class="row">
            <div class="col-md-12">

                <div id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach ($employers as $employer)
                        <?php
                        $id = $employer->pivot->id;
                        $visit_date = $employer->pivot->visit_date;
                        $status = $employer->pivot->status;
                        ?>
                        <div class="card">
                            <div class="card-header">
                                <h7 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#accordion" href="#collapse{!! $id !!}">
                                        {!! $employer->name !!}&nbsp;&nbsp;(&nbsp;{!! $employer->reg_no !!}&nbsp;)
                                        <span id="status{!! $id !!}">
                                            @if ($employer->pivot->status == '1')
                                                <span class='tag tag-success' data-toggle='tooltip' data-html='true' title="@lang('labels.general.complete')"> @lang('labels.general.complete')  </span>
                                            @else
                                                <span class='tag tag-warning' data-toggle='tooltip' data-html='true' title="@lang('labels.general.incomplete')"> @lang('labels.general.incomplete') .</span>
                                            @endif
                                        </span>
                                    </a>
                                </h7>
                            </div>
                            <div id="collapse{!! $id !!}" class="collapse">
                                <div class="card-block">
                                    {!! Form::model($employer->pivot, ['route' => ['backend.compliance.inspection.task.update_employer', $id], 'class' => 'update_employer', 'method' => 'PUT', 'id' => $id, 'enctype' => 'multipart/form-data']) !!}
                                    @include("backend/operation/compliance/inspection/task/employer_form")
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    @else
        @lang("labels.general.none")
    @endif
</div>


<div class="other_employer">
    @if (count($otherEmployers))
        <div class="row">
            <div class="col-md-12">

                <div id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach ($otherEmployers as $employer)
                        <?php
                        $id = $employer->id;
                        $visit_date = $employer->visit_date;
                        $status = $employer->status;
                        ?>
                        <div class="card">
                            <div class="card-header">
                                <h7 class="mb-0">
                                    <a class="card-title" data-toggle="collapse" data-parent="#accordion" href="#collapse{!! $id !!}">
                                        {!! $employer->employer_name !!}&nbsp;&nbsp;
                                        <span id="status{!! $id !!}">
                                            @if ($employer->status == '1')
                                                <span class='tag tag-success' data-toggle='tooltip' data-html='true' title="@lang('labels.general.complete')">@lang('labels.general.complete')</span>
                                            @else
                                                <span class='tag tag-warning' data-toggle='tooltip' data-html='true' title="@lang('labels.general.incomplete')">@lang('labels.general.incomplete')</span>
                                            @endif
                                        </span>
                                    </a>
                                </h7>
                            </div>
                            <div id="collapse{!! $id !!}" class="collapse">
                                <div class="card-block">
                                    {!! Form::model($employer, ['route' => ['backend.compliance.inspection.task.update_employer', $id], 'class' => 'update_employer', 'method' => 'PUT', 'id' => $id, 'enctype' => 'multipart/form-data']) !!}
                                    @include("backend/operation/compliance/inspection/task/employer_form")
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    @else
        @lang("labels.general.none")
    @endif
</div>

@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>

</script>
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function() {
        $(".search-select").select2({});
        var staff_filter = $(".staff_filter_select").select2({});
        autosize($("textarea.autosize"));
        $(".staff_filter_select").on("change", function () {
            var $user_id = $(this).val();
            document.location.replace(base_url + "/compliance/inspection/task/{!! $task->id !!}?user_id=" + $user_id);
        });
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form.update_employer', function (e) {
            e.preventDefault();
            var $id = this.id;
            /* alert($id); */
            var form = this;
            var $visit_date = $(form).find("select[name=visit_day]").val();
            var $visit_month = $(form).find("select[name=visit_month]").val();
            var $visit_year = $(form).find("select[name=visit_year]").val();
            if ($visit_date && $visit_month && $visit_year) {
                $(form).find("input[name=visit_date]").val($visit_year + '-' + $visit_month + '-' + $visit_date);
            } else {
                $(form).find("input[name=visit_date]").val("");
            }
            var $data = $(form).serialize();
            /* start: remove any printed error message in the input controls */
            $(form).find(':input').each(function () {
                var $name = $(this).attr('name');
                $(form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                $(form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            var $options = {
                dataType : "json",
                type : "POST",
                url : $(form).attr("action"),
                beforeSend : function (e) {
                    $(form).find(".progress-bar").width('0%');
                    $(form).find(".btn-submit").prop('disabled', true);
                },
                success : function (data) {
                    $(form).find(".btn-submit").prop('disabled', false);
                    if (data.success) {
                        $("<div class='alert alert-success'>{!! trans('alerts.backend.inspection_task.employer.updated') !!}</div>").appendTo($(form).find(".message")).delay(2000).fadeOut();
                        if (data.status == '1') {
                            $("#status" + $id).html("<span class='tag tag-success' data-toggle='tooltip' data-html='true' title=\"@lang('labels.general.complete')\"> @lang('labels.general.complete')</span>");
                        }
                    }
                },
                error: function (data) {
                    $(form).find(".progress-bar").width('0%');
                    $(form).find(".btn-submit").prop('disabled', false);
                    var errors = $.parseJSON(data.responseText);
                    /* console.log(errors); */
                    $.each(errors, function(index, value) {
                        $(form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                        $(form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                    });
                },
                uploadProgress : function (event, position, total, percentComplete) {
                    $(form).find(".progress-bar").width(percentComplete + '%');
                    $(form).find(".progress-bar").html('<div class="progress-status">' + percentComplete +' %</div>')
                },
            };
            // pass options to ajaxForm
            $(form).ajaxSubmit($options);
        });
    });
</script>
@endpush