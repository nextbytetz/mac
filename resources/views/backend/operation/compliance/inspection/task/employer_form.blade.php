{{--{!! Form::hidden('user_id', access()->id()) !!}--}}
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="fileld-layout">
            <label>@lang('labels.backend.compliance_menu.inspection.task.finding')</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::textarea('findings', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;']) !!}
                </div>
                <span class="help-block">
                                                            <p>{{ trans('labels.backend.compliance_menu.inspection.task.finding_helper') }}</p>
                                                        </span>
            </div>
        </div>
        <div class="filed-layout">
            <label class="required">@lang('labels.backend.compliance_menu.inspection.task.visit_date')</label>
            <div class="form-group">
                <div class="form-inline">
                                                            <span>
                                                                {!!  Form::selectRange('visit_day',1 , 31, (is_null($visit_date)? $visit_date :\Carbon\Carbon::parse($visit_date)->format("j")), ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}</span>
                    <span>
                                                                {!!  Form::selectMonth('visit_month', (is_null($visit_date)? $visit_date :\Carbon\Carbon::parse($visit_date)->format("n")), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                                                            </span>
                    <span>
                                                                {!!  Form::selectRange('visit_year', \Carbon\Carbon::now()->format('Y'), $max_year , (is_null($visit_date)? $visit_date :\Carbon\Carbon::parse($visit_date)->format("Y")), ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                                                            </span>
                    <span>
                                                                {!! Form::hidden('inspection_start', $inspection_start) !!}
                        {!! Form::hidden('inspection_end', $inspection_end) !!}
                        {!! Form::hidden('visit_date') !!}
                                                            </span>
                </div>
                <span class="help-block">
                                                            <p>{{ trans('labels.backend.compliance_menu.inspection.task.visit_date_helper') }}</p>
                                                        </span>
            </div>
        </div>
        <div class="fileld-layout">
            <label>@lang('labels.backend.compliance_menu.inspection.task.upload')</label>
            <div class="form-group">
                {!! Form::file('contribution_file') !!}
                <span class="help-block">
                                                            <p>{{ trans('labels.backend.compliance_menu.inspection.task.upload_helper') }}</p>
                                                        </span>
            </div>
            {{-- start : Displaying the progress of uploading file--}}
            <div class="progress-div">
                <div class="progress-bar"></div>
            </div>
            {{-- end : Displaying the progress of uploading file--}}
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="fileld-layout">
            <label>@lang('labels.backend.compliance_menu.inspection.task.resolution')</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::textarea('resolutions', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                </div>
                <span class="help-block">
                                                            <p>{{ trans('labels.backend.compliance_menu.inspection.task.resolution_helper') }}</p>
                                                        </span>
            </div>
        </div>
        <div class="fileld-layout">
            <label class="required">@lang('labels.backend.compliance_menu.inspection.task.status')</label>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::select('status', ['1' => 'Completed', '0' => 'Incomplete'], $status, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Status']) !!}
                </div>
                <span class="help-block">
                                                            <p>{{ trans('labels.backend.compliance_menu.inspection.task.status_helper') }}</p>
                                                        </span>
            </div>
        </div>
    </div>
</div>
<div class="message">
</div>
<br/>
<hr/>
<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.update')" />
        </div>
    </div>
</div>