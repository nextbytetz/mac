@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.task.dashboard'), 'header_title' => trans('labels.backend.compliance_menu.inspection.task.dashboard')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style>
    .wcf_employer:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.compliance_menu.inspection.registered')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .wcf_employer {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }

    .other_employer:after {
        background-color: #F5F5F5;
        border: 1px solid #DDDDDD;
        border-radius: 4px 0 4px 0;
        color: #3c5ba4;
        content: "@lang('labels.backend.compliance_menu.inspection.unregistered')";
        /* font-size: 12px;
        font-weight: bold; */
        left: -1px;
        padding: 3px 7px;
        position: absolute;
        top: -1px;
    }

    .other_employer {
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px 4px 4px 4px;
        margin: 5px 0px;
        padding: 39px 19px 14px;
        position: relative;
    }
    /* start: upload progress bar css */
    .progress-bar {
        background-color: #12CC1A;
        height:20px;
        color: #FFFFFF;
        width:0%;
        -webkit-transition: width .3s;
        -moz-transition: width .3s;
        transition: width .3s;
    }
    .progress-div {
        border:#0FA015 1px solid;
        padding: 5px 0px;
        margin:30px 0px;
        border-radius:4px;
        text-align:center;
    }
    /* end: upload progress bar css */
</style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/compliance/inspection/task/includes/header_info")
        {{--<div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Inspection header detail -->
                <strong> {!! Form::label( 'inspection_type', $task->inspection->inspectionType->name) !!}</strong>

                <small >
                    @lang('labels.backend.compliance_menu.inspection.inspection_#'): {!! Form::label( 'inspection_id', $task->inspection->id) !!}
                </small>
                <small >
                    @lang('labels.backend.compliance_menu.inspection.task.task_#'): {!! Form::label( 'inspection_id', $task->id) !!}
                </small>
            </h5>
        </div>--}}
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs">
                    {{--General Information--}}
                    <li class="nav-item">
                        <a class="nav-link active" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                </ul>
                <div class="nav_tab_contain tab-content">
                    <div class="tab-pane active" id="general_information" role="tabpanel">
                        @include("backend/operation/compliance/inspection/task/includes/dashboard_menu")
                        {{--main tab content--}}
                        <legend class="grey_modal" >@lang('labels.backend.compliance_menu.inspection.task.specific')</legend>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="fileld-layout">
                                    <label>@lang('labels.backend.compliance_menu.inspection.task.filter')</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            {!! Form::select('staff_filter', $target_staffs, $user_id, ['class' => 'form-control staff_filter_select','style'=>'width:100%']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    {{-- employer task list overview --}}
                                    <br/>
                                    @include("backend/operation/compliance/inspection/task/employer_list_datatable")
                                    {{--mark_change1--}}
                                    {{--@if($task->inspection->inspection_type_id == 3)
                                        @include("backend/operation/compliance/inspection/task/unregistered_employer_list")
                                    @else
                                        @include("backend/operation/compliance/inspection/task/employer_list")
                                    @endif--}}
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/compliance/inspection/task/summary')
                                    @include('backend/operation/compliance/inspection/summary')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
<script>

</script>
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script>
    $(function () {
        let $staff_filter = $(".staff_filter_select");
        $staff_filter.select2();
        $staff_filter.on("change", function () {
            var $user_id = $(this).val();
            document.location.replace(base_url + "/compliance/inspection/task/{!! $task->id !!}?user_id=" + $user_id);
        });
    })
</script>
@endpush