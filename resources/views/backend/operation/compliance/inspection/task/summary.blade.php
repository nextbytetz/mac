<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.compliance_menu.inspection.task.summary')</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.task.no_employers') :</h6>
        <div style="font-weight: bold;">{!! $task->employers()->count() + $task->unregisteredEmployers()->count() !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.task.name') :</h6>
        <div style="font-weight: bold;">{!! $task->name !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.task.deliverable') :</h6>
        <div style="font-weight: bold;">{!! $task->deliverable_label !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.completed') :</h6>
        <div style="font-weight: bold;">{!! $task->completed_label !!}</div>
        <h6 class="underline" style="font-weight: lighter;">Inspection Type :</h6>
        <div style="font-weight: bold;">{!! $task->inspectionType->name !!}</div>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.task.staffs') :</h6>
        @if ($task->allocations()->count())
            @foreach ($allocations as $user)
                <?php
                $count = $user->employerInspectionTasks()->where("inspection_task_id", $task->id)->count();
                ?>
                <div><i class="icon fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;{!! $user->name !!} &nbsp;&nbsp; (&nbsp; {!! trans_choice("labels.backend.compliance_menu.inspection.task.count_employers", $count, ['count' => $count]) !!} &nbsp;)</div>
            @endforeach
        @else
            <div style="font-weight: bold;">@lang('labels.general.none')</div>
        @endif
    </div>
</div>
