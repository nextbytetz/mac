<span class="dropdown">
    <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButtonWorkflow" data-toggle="dropdown" >More</a>
    <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonWorkflow" style="z-index: 99999999">

        @if ($inspection->can_edit_task)
        {{--delete--}}
        <span>
            {!! HTML::decode(link_to_route('backend.compliance.inspection.task.destroy', "<i class='icon fa fa-trash' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.delete'), [$task->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.task.delete'), 'class' => 'dropdown-item']))  !!}
        </span>

        {{--edit--}}
        <div class="dropdown-divider"></div>
        <span>
                <a href="{!! route('backend.compliance.inspection.task.edit', $task->id) !!}"  class="dropdown-item" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>
        </span>
        @endif

        {{--close--}}
        <div class="dropdown-divider"></div>
        <span>
                <a href="{!! route('backend.compliance.inspection.show', $inspection->id) !!}"  class="dropdown-item" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
        </span>

     </span>
</span>