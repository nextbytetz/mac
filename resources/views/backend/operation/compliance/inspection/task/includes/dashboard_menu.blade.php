<div class="nav_tab_pane_header">
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
                {{--assign tasks--}}
                {{--commented as it currently has no use--}}
                {{--<span>
                                            {!! HTML::decode(link_to_route('backend.compliance.inspection.task.assign', "<i class='icon fa fa-tasks' aria-hidden='true'></i>&nbsp;" . trans('labels.backend.compliance_menu.inspection.task.assign'), [$task->id], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.task.assign_confirm'), 'class' => 'btn btn-primary site-btn nav_button']))  !!}
                                        </span>--}}
                {{--Download Employers--}}

                    <span>
                        <a href="{!! route('backend.compliance.inspection.task.download_employers', ['task' => $task->id, 'user_id' => $user_id]) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-download" aria-hidden="true"></i>&nbsp;@lang('labels.backend.compliance_menu.inspection.task.download')</a>
                    </span>


                @include("backend/operation/compliance/inspection/task/includes/dashboard_more_links")
            </div>
        </div>
    </div>
</div>