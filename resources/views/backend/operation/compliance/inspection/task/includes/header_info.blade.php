<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    {{--<span class="navbar-brand mb-0 h5">{!! $inspection->status_label !!}</span>--}}
    <span class="navbar-brand mb-0 h5"><strong>{!! Form::label( 'inspection_type', $inspection->inspectionType->name) !!}</strong></span>

    <span class="navbar-brand mb-0 h5">
         <small >
                    @lang('labels.backend.compliance_menu.inspection.inspection_#'): {!! Form::label( 'inspection_id', $inspection->filename) !!}
                </small>
                <small >
                    @lang('labels.backend.compliance_menu.inspection.task.task_#'): {!! Form::label( 'inspection_id', $task->id) !!}
                </small>
    </span>

</nav>
<legend></legend>
<br/>