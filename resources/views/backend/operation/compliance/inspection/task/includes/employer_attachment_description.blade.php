<br/>
<hr/>

<h4>@lang('labels.backend.file.instruction')</h4>
<div class="row">
    <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-pink"></i>
           <i class="fa fa-info fa-stack-1x fa-inverse"></i>
       </span>
    </div>
    <div class='col-md-11'>
        <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.title')</u></h4>
        Microsoft Excel file xlsx <br/>
    <!--					@lang('labels.backend.file.format.file_helper')-->
    </div>
</div>
<hr/>
<div class="row">
    <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-orange"></i>
           <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
       </span>
    </div>
    <div class='col-md-11'>
        <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.content')</u></h4>
        @lang('labels.backend.file.format.content_helper') <br/>
        @lang('labels.backend.file.format.column')  &nbsp;&nbsp;<span class="label label-success">regno</span>
    </div>
</div>
<hr/>
<div class="row">
    <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-yellow"></i>
           <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
       </span>
    </div>
    <div class='col-md-10'>
        <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.example')</u></h4>
        @lang('labels.backend.file.format.example_helper') <br/><br/>
        <img src="{{ asset_url() . '/nextbyte/img/inspection_employers.png' }}" height="auto" width="auto">
    </div>
</div>
<br/>