{{--Create follow up --}}
{{--Create form--}}
{!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
{!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
@include("backend.operation.compliance.member.unregistered_employer.follow_up.includes.create_form",['unregistered_employer'=>$unregistered_employer])

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.update')" />
        </div>
    </div>
</div>