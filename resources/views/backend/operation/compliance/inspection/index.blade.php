@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.title'), 'header_title' => trans('labels.backend.compliance_menu.inspection.title')])

@push('after-styles-end')

@endpush

@include('backend.includes.datatable_assets')

@section('content')

    @include("backend/operation/compliance/inspection/includes/menu")

    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%'], true) !!}
        </div>
    </div>

@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
@endpush