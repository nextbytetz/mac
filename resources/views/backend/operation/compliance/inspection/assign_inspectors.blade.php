@extends('layouts.backend.main', ['title' => "Assign Inspectors", 'header_title' => "Assign Inspectors"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
    <style>
        .select2-results {
            min-height: 80px;
            max-height: 80px;
            overflow-y: auto;
        }
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/compliance/inspection/includes/header_info")
    </div>
    <div class = "row">
            <div class="col-md-9">

                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['route' => ['backend.compliance.inspection.post_inspectors', $inspection->id], 'id' => 'add_inspector']) !!}
                        <div>
                            Search inspectors...
                        </div>
                        {{-- user allocation --}}
                        <div class="grid-column">
                            {!! Form::select('inspectors[]', $inspectors, null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                            {!! $errors->first('inspectors', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                        {!! Form::submit("Add", ['class' => 'btn btn-success btn-save btn-block']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>

                <br/>
                <hr/>
                <div class="col-md-12">
                    <br/>
                    <div class="pull-right">

                        {!! Form::open(['route' => ['backend.compliance.inspection.remove_inspectors', $inspection->id], 'id' => 'remove_selected']) !!}
                        <input type='hidden' name='_method' value='delete'>
                        <button type="submit" class="btn btn-danger  btn-round-left">
                            <i class="icon fa fa-close" aria-hidden="true"></i>&nbsp;Remove Selected
                        </button>
                        {!! Form::close() !!}

                    </div>
                    <br/>
                    <br/>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div>Click on the user to select as Lead Inspector</div>
                        <table class="display" cellspacing="0" width="100%" id ="allocate-inspectors-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Staff</th>
                                <th>Identification</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <br/>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <a href="{!! route('backend.compliance.inspection.show', $inspection->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-thumbs-up"></i>&nbsp;Done</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {{--sidebar summary--}}
                @include('backend/operation/compliance/inspection/summary')
            </div>
    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
    <script>
        $(function () {
            $(".search-select").select2({
                multiple: true,
            });
            let $oTable = $('#allocate-inspectors-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: false,
                info: true,
                ajax:{
                    url : '{!! route('backend.compliance.inspection.get_inspectors_datatable', $inspection->id) !!}',
                    type : 'post'
                },
                columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                select: {
                    'style': 'multi'
                },
                columns: [
                    {
                        orderable:      false,
                        searchable:      false,
                        data:           'id'
                    },
                    { data: 'fullname', name: 'users.firstname'},
                    { data: 'useridentity' , name: 'users.thirdparty_id'}
                ],
                'rowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:first-child)', nRow).click(function() {
                        /*document.location.href =  base_url + "/compliance/closed_business/" + aData['id'] + "/show ";*/
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }
            });
            $('#add_inspector').on('submit', function(e) {
                e.preventDefault();
                var $form = this;
                var $options = {
                    dataType : "json",
                    type : "POST",
                    url : $($form).attr("action"),
                    success : function (data) {
                        if (data.success) {
                            $oTable.draw();
                        } else {
                            alert(data.message);
                            /*swal({ title : "Error Assigning User to Resource(s)", text : data.message});*/
                        }
                    },
                    error: function (data) {

                    }
                };
                // pass options to ajaxForm
                $($form).ajaxSubmit($options);
            });
            $('#remove_selected').on('submit', function(e) {
                e.preventDefault();
                var $form = this;
                var $rows_selected = $oTable.column(0).checkboxes.selected();
                //Remove all previous selected
                $($form).find("input[name='id[]']").remove();
                // Iterate over all selected checkboxes
                $.each($rows_selected, function(index, rowId){
                    // Create a hidden element
                    $($form).append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'id[]')
                            .val(rowId)
                    );
                });
                swal({
                    title: "Warning",
                    text: "Are you sure to remove from the list of assigned inspectors?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        var $options = {
                            dataType : "json",
                            /*type : "PUT",*/
                            url : $($form).attr("action"),
                            success : function (data) {
                                if (data.success) {
                                    $oTable.draw();
                                } else {
                                    alert(data.message);
                                    swal({ title : "Error Removing User from Inspection", text : data.message});
                                }
                            },
                            error: function (data) {

                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
        });
    </script>
@endpush