<legend class="grey_modal" >@lang('labels.backend.compliance_menu.inspection.profile.list')</legend>
<br/>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" cellspacing="0" width="100%" id ="profile_employer_list_table">
            <thead>
            <tr>
                <th>@lang('labels.backend.compliance_menu.inspection.profile.registration')</th>
                <th>@lang('labels.general.name')</th>
                <th>@lang('labels.backend.compliance_menu.inspection.profile.region')</th>
                <th>@lang('labels.backend.compliance_menu.inspection.profile.business_title')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

@push('after-script-end')

<script>
    $(function() {
        $('#profile_employer_list_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:true,
            dom:"Bfrtip",
            buttons:['reload'],
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.inspection.profile.get', $profile->id) !!}',
                type : 'get',
            },
            columns: [
                { data: 'reg_no' , name: 'reg_no' },
                { data: 'name' , name: 'name' },
                { data: 'region' , name: 'region', orderable: false, searchable: false },
                { data: 'business' , name: 'business', orderable: false, searchable: false },
            ]
        } );
    });
</script>;

@endpush