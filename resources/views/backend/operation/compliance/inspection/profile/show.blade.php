@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.profile.dashboard'), 'header_title' => trans('labels.backend.compliance_menu.inspection.profile.dashboard')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Inspection Profile header detail -->
                <small >
                    @lang('labels.general.name'): <span class="underline">{!! $profile->name !!}</span>
                </small>
            </h5>
        </div>
    </div>
    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
        <div class="row">
            <div class="col-md-12" >
                <div class="pull-right" >
                    {{--Delete--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.profile.destroy', "<i class='icon fa fa-trash' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.delete'), [$profile->id], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance_menu.inspection.profile.delete'), 'class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;']))  !!}
                    </span>
                    {{--Edit--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.profile.edit', "<i class='icon fa fa-edit' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.crud.edit'), [$profile->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                    {{--Close--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.inspection.profile.index', "<i class='icon fa fa-close' aria-hidden='true'></i>&nbsp;" . trans('labels.general.close'), [],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class = "row">
        <div class="col-md-12">
            <div class="col-md-9">
                {{-- employer task list overview --}}
                @include("backend/operation/compliance/inspection/profile/employer_list")

                <br>
                @include("backend/operation/compliance/inspection/profile/unregistered_employer_list")
            </div>
            <div class="col-md-3">
                {{--sidebar summary--}}
                @include('backend/operation/compliance/inspection/profile/summary')
            </div>
        </div>
    </div>
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script>
    $(function(){

    });
</script>
@endpush