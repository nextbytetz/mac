@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.profile.title'), 'header_title' => trans('labels.backend.compliance_menu.inspection.profile.title')])

@push('after-styles-end')

@endpush

@include('backend.includes.datatable_assets')

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="{!! route('backend.compliance.inspection.profile.create') !!}"   class="btn btn-primary save_button btn-sm" >&nbsp;@lang('labels.backend.compliance_menu.inspection.profile.new')  &nbsp; <i class="icon fa fa-plus-circle"></i></a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%'], true) !!}
        </div>
    </div>
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{!! $dataTable->scripts() !!}
@endpush