<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.compliance_menu.inspection.profile.summary')</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.profile.name') :</h6>
        <p style="font-weight: bold;">{!! $profile->name !!}</p>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.compliance_menu.inspection.profile.information') :</h6>
        <p style="font-weight: bold;">{!! $profile->info !!}</p>
    </div>
</div>