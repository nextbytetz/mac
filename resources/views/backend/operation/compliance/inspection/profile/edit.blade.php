@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.profile.edit'), 'header_title' => trans('labels.backend.compliance_menu.inspection.profile.edit')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {!! Form::model($profile, ['route' => ['backend.compliance.inspection.profile.update', $profile->id], 'name' => 'update_inspection_profile', 'method' => 'PUT']) !!}
    <div class="row">
        <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">
            <div class="field-layout">
                <div class="form-group">
                    <label class="required" for="name">@lang('labels.backend.compliance_menu.inspection.profile.name')</label>
                    <div class="input-group">
                        {!! Form::text('name', null, ['class' => 'form-control', 'style' => "border-radius:3px;", 'id' => 'receipt_amount']) !!}
                        <span class="help-block" style="width:100% !important;">
		                <p>{{ trans('labels.backend.compliance_menu.inspection.profile.name_helper') }}</p>
                            {!! $errors->first('name', '<p class="help-block label label-danger">:message</p>') !!}
	                </span>
                    </div>
                </div>
            </div>
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.profile.region')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('region[]', $regions, $region_ids, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true']) !!}
                        {!! $errors->first('region', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="fileld-layout">
                <label class="required">@lang('labels.backend.compliance_menu.inspection.profile.business')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('business[]', $businesses, $business_ids, ['class' => 'search-select', 'style' => 'width:100%', 'multiple'=>'true']) !!}
                        {!! $errors->first('business', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.update')" />
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function () {
        $(".search-select").select2({
            placeholder: "",
            allowClear: true,
        });
    });
</script>
@endpush