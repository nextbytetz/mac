@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.inspection.profile.create'), 'header_title' => trans('labels.backend.compliance_menu.inspection.profile.create')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    {!! Form::open(['route' => 'backend.compliance.inspection.profile.store', 'name' => 'create_inspection_profile']) !!}
    {!! Form::hidden('user_id', access()->id()) !!}
    <div class="row">
        <div class="col-md-6" style="padding-right:20px; border-right: 1px solid #ddd;">
            <div class="field-layout">
                <div class="form-group">
                    <label class="required" for="name">@lang('labels.backend.compliance_menu.inspection.profile.name')</label>
                    <div class="input-group">
                        {!! Form::text('name', null, ['class' => 'form-control', 'style' => "border-radius:3px;", 'id' => 'receipt_amount']) !!}
                    <span class="help-block" style="width:100% !important;">
                    <p>{{ trans('labels.backend.compliance_menu.inspection.profile.name_helper') }}</p>
                        {!! $errors->first('name', '<p class="help-block label label-danger">:message</p>') !!}
	                </span>
                    </div>
                </div>
            </div>
            <div class="fileld-layout">
                <label>Employer Region</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('region[]', $regions, null, ['class' => 'search-select', 'style' => 'width:100%', 'multiple' => 'true']) !!}
                        {!! $errors->first('region', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="fileld-layout">
                <label>Notification/Claim with No Contribution</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('claim_no_contribution', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                        {!! $errors->first('claim_no_contribution', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="fileld-layout">
                <label>Number of Outstanding Months Greater Than</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('outstanding_months', null, ['class' => 'form-control', 'style' => "border-radius:3px;width:50%;"]) !!}&nbsp;&nbsp;
                        {!! $errors->first('outstanding_months', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6">
            <div class="fileld-layout">
                <label>@lang('labels.backend.compliance_menu.inspection.profile.business')</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('business[]', $businesses, null, ['class' => 'search-select', 'style' => 'width:100%', 'multiple'=>'true']) !!}
                        {!! $errors->first('business', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="fileld-layout">
                <label>Online Status</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('online_status',  ['0' => 'Not Registered', '1' => 'Registered'], null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                        {!! $errors->first('online_status', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="fileld-layout">
                <div class="form-group">
                    <div class="form-inline">
                        <span>
                            <label>Number of Employees :</label>&nbsp;&nbsp;
                        </span>
                        <span>
                            {!! Form::select('no_employees_operator', ['0' => '<=', '1' => '>='], null, ['class' => 'form-control search-select','style'=>'width:10%', 'placeholder' => '',]) !!}
                        </span>
                        <span>
                            than
                        </span>
                        <span>
                            {!! Form::text('no_employees_value', null, ['class' => 'form-control', 'style' => "border-radius:3px;width:40%;"]) !!}&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
            </div>

            <div class="fileld-layout">
                <label>Has Dishonoured Cheques</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('has_dishonoured_cheques', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                        {!! $errors->first('has_dishonoured_cheques', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="fileld-layout">
                <label>Employer Sector</label>
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::select('employer_sector', ['ECPUB' => 'Public', 'ECPRI' => 'Private'], null, ['class' => 'search-select', 'style' => 'width:100%', 'placeholder' => '']) !!}
                        {!! $errors->first('employer_sector', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <br/>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.create')" />
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function () {
        $(".search-select").select2({
            placeholder: "",
            allowClear: true,
        });
    });
</script>
@endpush