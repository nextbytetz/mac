@extends('layouts.backend.main', ['title' => "Inspection Resource Allocation", 'header_title' => "Inspection Resource Allocation"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    <style>
        .custom_filter:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "@lang('labels.backend.system.workflow.custom_filter')";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .custom_filter {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        @include("backend/operation/compliance/inspection/includes/header_info")
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <legend class="nav-link active" data-toggle="tab">Allocation Status</legend>
                    </li>
                </ul>
                <div class="nav_tab_contain tab-content">
                    <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-4">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <td width="200px"></td>
                                                <td><p class="underline" style="font-weight: bold;">Employers</p></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td width="200px">Available&nbsp;:</td>
                                                <td style="text-align: center;">{{ $resource_status['available_eit'] }}</td>
                                            </tr>
                                            <tr>
                                                <td width="200px">Allocated&nbsp;:</td>
                                                <td style="text-align: center;">{{ $resource_status['allocated_eit'] }}</td>
                                            </tr>
                                            <tr>
                                                <td width="200px">Unallocated&nbsp;:</td>
                                                <td style="text-align: center;">{{ $resource_status['unallocated_eit'] }}</td>
                                            </tr>
                                            <tr>
                                                <td width="200px">Attended (Allocated)&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Allocated resource(s) which has been attended"></i>&nbsp;:</td>
                                                <td style="text-align: center;">{{ $resource_status['attended_eit'] }}</td>
                                            </tr>
                                            <tr>
                                                <td width="200px">Unattended (Allocated)&nbsp;<i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Allocated resource(s) which has not been attended"></i>&nbsp;:</td>
                                                <td style="text-align: center;">{{ $resource_status['unattended_eit'] }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="underline">Inspection Type Allocation Group</div>
                                        <div id="allocation_inspection_type_group" data-plugin="custom-scroll" data-height="250">

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="underline">Staff Allocation Group</div>
                                        <div id="allocation_staff_group" data-plugin="custom-scroll" data-height="250">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom_filter">
                                            {!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-row">
                                                        {{--Employer Name--}}
                                                        <div class="form-group col-md-4 allocation_select">
                                                            <label for="employer">Employer Name</label>
                                                            {!! Form::select('employer', [], null, ['class' => 'form-control employer-select', 'id' => 'employer']) !!}
                                                        </div>
                                                        {{--Region Selection--}}
                                                        <div class="form-group col-md-3">
                                                            <label for="region">Employer Region:</label>
                                                            {!! Form::select('region', $regions, null, ['class' => 'form-control search-select', 'id' => 'region', 'placeholder' => '']) !!}
                                                            <i class="icon fa fa-spinner fa-spin" aria-hidden="true" style="display: none;" id="spin2"></i>
                                                        </div>
                                                        {{--District Selection--}}
                                                        <div class="form-group col-md-3">
                                                            <label for="region">Employer District:</label>
                                                            {!! Form::select('district', [], null, ['class' => 'form-control search-select', 'id' => 'district', 'placeholder' => '']) !!}
                                                        </div>
                                                        {{--Employer Task Stages--}}
                                                        <div class="form-group col-md-6">
                                                            <label for="stage">Stage</label>
                                                            {!! Form::select('stage', $stages, null, ['class' => 'form-control search-select', 'id' => 'incident_stage', 'placeholder' => '']) !!}
                                                        </div>
                                                        {{--Resource Status--}}
                                                        <div class="form-group col-md-3 allocation_select">
                                                            <label for="status">Status:</label>
                                                            {!! Form::select('status', ['0' => 'All', '1' => 'Allocated', '2' => 'Unallocated', '3' => 'Allocated to User', '4' => 'Attended (Allocated)', '5' => 'Unattended (Allocated)'], null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                                                        </div>
                                                        {{--Allocated User--}}
                                                        <div class="form-group col-md-3 allocated_user_select">
                                                            <label for="allocated_user">User:</label>
                                                            {!! Form::select('user_id', $users, null, ['class' => 'form-control search-select', 'id' => 'allocated_user']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                                                            <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                {{--sidebar summary--}}
                                @include('backend/operation/compliance/inspection/summary')
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <legend></legend>
    <br/>

    @if ($can_assign)
        {{--Assign to--}}
        <div class="row">
            <div class="col-md-12">
                <div class="form-row">
                    {!! Form::open(['role' => 'form', 'id' => 'assign-user', 'route' => ['backend.compliance.inspection.allocation.assign', $inspection->id]]) !!}
                    <div class="form-group offset-md-6 col-md-4">
                    <span class="assign_status_select">
                        <label for="assign_status"><b>Assign Status:</b></label>
                        {!! Form::select('assign_status', ['1' => 'Assign To User', '2' => 'Reallocate Evenly'] , null, ['class' => 'form-control search-select', 'id' => 'assign_status', 'placeholder' => '']) !!}
                    </span>
                    <span class="assign_user_select">
                        <label for="assigned_user"><b>Assign To:</b></label>
                        {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
                    </span>
                    </div>
                    {{--<div class="col-md-3 assign_user_select">
                        <label for="assigned_user">Assign To:</label>
                        {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
                    </div>--}}
                    <div class="form-group col-md-2">
                        <label for="allocate_submit">&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success btn-sm" value="Submit" id="allocate_submit">
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endif

    {{--Resource Datatable--}}
    <div class="row">
        <div class="col-md-12">
            <table class="display" id = "resource-allocation-table" width="100%">
                <thead>
                <tr >
                    <th></th>
                    <th>Task Type</th>
                    <th>Employer</th>
                    <th>Allocated Staff</th>
                    <th>Attended</th>
                    <th>Stage</th>
                    <th>Region</th>
                    <th>District</th>
                    <th>Registration Date</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    <script>
        $(function () {
            $(".search-select").select2({
                allowClear: true,
                debug: true,
                placeholder: ""
            });
            $('#region').on('change', function (e) {
                var region_id = e.target.value;
                if (region_id) {
                    $("#spin2").show();
                    $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                        let $districtId = $('#district');
                        $districtId.empty();
                        $districtId.select2("val", "");
                        $districtId.html(data);
                        $("#spin2").hide();
                    });
                }
            });
            let $statusId = $("#status");
            $statusId.on("change", function () {
                var $status = $(this).val();
                let $allocatedUserSelectClass = $(".allocated_user_select");
                switch($status) {
                    case '3':
                        /* Selected to choose User Allocated */
                        $allocatedUserSelectClass.show();
                        break;
                    default:
                        $allocatedUserSelectClass.hide();
                        break;
                }
            });
            $statusId.trigger('change');
            let $assign_status_contrl = $('#assign_status');
            $assign_status_contrl.on("change", function () {
                let $assign_status = $(this).val();
                let $assignUserSelectClass = $(".assign_user_select");
                switch($assign_status) {
                    case '1':
                        $assignUserSelectClass.show();
                        break;
                    default:
                        $assignUserSelectClass.hide();
                        break;
                }
            });
            $assign_status_contrl.trigger('change');
            /* start : Searching Employer */
            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:selecting", function($e) {
                var $selected = $e.params.args.data.id;
            });
            $( "#clear_filter" ).click(function() {
                /* Clear the Filter Form */
                $(".employer-select").val(null).trigger('change.select2');
                $(".employee-select").val(null).trigger('change.select2');
                $(".search-select").val(null).trigger('change.select2');
            });
            var $oTable = $('#resource-allocation-table').DataTable({
                /*dom : 'Bfrtip',*/
                buttons : ['reload', 'colvis'],
                initComplete : function () {
                    $oTable.buttons().container().insertBefore('#resource-allocation-table');
                },
                drawCallback : function () {
                    getAllocationInspectionTypeGroup();
                    getAllocationStaffGroup();
                },
                processing: true,
                serverSide: true,
                info : true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax: {
                    url: "{!! route("backend.compliance.inspection.allocation.get", $inspection->id) !!}",
                    method : "PUT",
                    data: function ($d) {
                        $d.employer = $('select[name=employer]').val();
                        $d.status = $('select[name=status]').val();
                        $d.user_id = $('select[name=user_id]').val();
                        $d.region = $('select[name=region]').val();
                        $d.district = $('select[name=district]').val();
                        $d.stage = $('select[name=stage]').val();
                    }
                },
                columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                select: {
                    'style': 'multi'
                },
                columns: [
                    {
                        orderable: false,
                        searchable: false,
                        data: 'id',
                        name : 'employer_inspection_task.id'
                    },
                    {data: 'task_type', name: 'a.name', visible: true},
                    {data: 'employer', name: 'employers.name', searchable: true, orderable: true},
                    {data: 'allocated_user', name: 'users.firstname'},
                    {data: 'user_attended', name: 'employer_inspection_task.attended', searchable: false, orderable: true},
                    {data: 'stage', name: 'b.name', visible: true},
                    {data: 'region', name: 'regions.name', searchable: true, orderable: true, visible: false},
                    {data: 'district', name: 'districts.name', searchable: true, orderable: true, visible: false},
                    {data: 'created_at', name: 'employer_inspection_task.created_at', searchable: false, orderable: true, visible: false},
                ],
                'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                    $('td:not(:first-child)', $nRow).click(function() {
                        window.open(base_url + "/compliance/inspection/employer_task/" + $aData['id'], "_self");
                    }).hover(function() {
                        $(this).css('cursor', 'alias');
                    }, function() {
                        $(this).css('cursor', 'auto');
                    });
                }
            });
            $('#search-form').on('submit', function($e) {
                $oTable.draw();
                $e.preventDefault();
            });
        @if ($can_assign)
            $('#assign-user').on('submit', function($e) {
                $e.preventDefault();
                var $form = this;
                var $rowsSelected = $oTable.column(0).checkboxes.selected();
                //Remove all previous selected
                $($form).find("input[name='id[]']").remove();
                // Iterate over all selected checkboxes
                $.each($rowsSelected, function($index, $rowId) {
                    // Create a hidden element
                    $($form).append($('<input>').attr('type', 'hidden').attr('name', 'id[]').val($rowId));
                });
                swal({
                    title: "Warning",
                    text: "Are you sure to allocate selected files to the selected assigned user",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function ($confirmed) {
                    if ($confirmed) {
                        //$form.submit();
                        //Do the ajax submission ...
                        //route : backend.claim.notification_report.allocation.assign
                        var $options = {
                            dataType : "json",
                            data : {},
                            type : "PUT",
                            url : $($form).attr("action"),
                            success : function (data) {
                                if (data.success) {
                                    $oTable.draw();
                                    $oTable.column(0).checkboxes.deselectAll();
                                    $.amaran({
                                        'theme'     :'awesome success',
                                        'content'   :{
                                            title : "Success",
                                            message: data.message,
                                            info:'',
                                            icon: 'fa fa-check-square-o'
                                        },
                                        'position'  :'bottom left',
                                        'outEffect' :'slideBottom',
                                        'inEffect'  :'slideLeft'
                                    });
                                } else {
                                    alert(data.message);
                                    /*swal({ title : "Error Assigning User to Resource(s)", text : data.message});*/
                                }
                            },
                            error: function (data) {

                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
            @endauth
        });
        function getAllocationStaffGroup()
        {
            $.ajax({
                url: "{{ route("backend.compliance.inspection.allocation_staff_group", $inspection->id) }}",
                dataType : 'html',
                /*async : false,*/
                method : "POST",
                data : {
                    employer : $('select[name=employer]').val(),
                    status : $('select[name=status]').val(),
                    user_id : $('select[name=user_id]').val(),
                    region : $('select[name=region]').val(),
                    district : $('select[name=district]').val(),
                    stage : $('select[name=stage]').val()
                }
            }).done(function ($data) {
                let $group = $("#allocation_staff_group");
                $group.empty();
                $group.html($data);
            });
        }
        function getAllocationInspectionTypeGroup()
        {
            $.ajax({
                url: "{{ route("backend.compliance.inspection.allocation_inspection_type_group", $inspection->id) }}",
                dataType : 'html',
                /*async : false,*/
                method : "POST",
                data : {
                    employer : $('select[name=employer]').val(),
                    status : $('select[name=status]').val(),
                    user_id : $('select[name=user_id]').val(),
                    region : $('select[name=region]').val(),
                    district : $('select[name=district]').val(),
                    stage : $('select[name=stage]').val()
                }
            }).done(function ($data) {
                let $group = $("#allocation_inspection_type_group");
                $group.empty();
                $group.html($data);
            });
        }
    </script>
@endpush