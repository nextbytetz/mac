@extends('layouts.backend.main', ['title' => trans('labels.general.compliance_header'), 'header_title' => trans('labels.general.compliance_header')])


@section('content')

    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">
                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.receipt.search') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-upload"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.received_contribution')
                                </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.compliance_menu.received_contribution_summary')</p> </li>
                    </a>

                </ul>

                {{--item 2 - Receipt contributions before electronic--}}
                &nbsp;   <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.legacy_receipt.employer') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-list-alt"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.receive_contribution_interest_before_jul_2017')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.receive_contribution_interest_before_jul_2017_summary')</p> </li>
                    </a>

                </ul>


                {{--Dishonoured cheque--}}
                &nbsp;  <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.inspection.get.dishonoured_cheques') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-money"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.dishonoured_cheques')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.compliance_menu.dishonoured_cheques')</p> </li>
                    </a>

                </ul>

                {{--unregistered employer--}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.unregistered_employer.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-institution"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.unregistered_employer')</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.compliance_menu.unregistered_employer_summary')</p> </li>
                    </a>
                </ul>

                {{-- closed business list from TRA --}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.closed_business') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding">
                                <i class="icon fa fa-level-down" aria-hidden="true"></i>
                                <large>&nbsp;&nbsp;Closed Business from TRA</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                Associate closed business from TRA database with the existing employers
                            </p>
                        </li>
                    </a>
                </ul>

                {{--employer relationship--}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding">
                                <i class="icon fa fa-users" aria-hidden="true"></i>
                                <large>&nbsp;Employers Debt Management</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                Debt Management for employers assigned to staff
                            </p>
                        </li>
                    </a>
                </ul>


                {{--Compliance Defaults--}}
                &nbsp;  <ul class="list-unstyled">
                    {{--<li  class="list-group-item border-less" >--}}
                    <li  class="border-less" >
                        <a href="{!! route('backend.compliance.defaults') !!}">
                            <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-terminal" aria-hidden="true"></i><strong>&nbsp;&nbsp;Defaults </strong></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Set Default Configurations & Selections </p>

                        </a>
                    </li>
                </ul>

            </div>
        </div>


        {{--right div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">


                {{--Item1 Received contribution before jul 2017--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.legacy_receipt.received_contributions') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-upload"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.received_contribution_before_jul_2017')
                                </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.compliance_menu.received_contribution_before_jul_2017_summary')</p> </li>
                    </a>
                </ul>

                {{--Item 2 Recall receipt before 2017--}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.finance.legacy_receipt.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-reply-all"> </i><large>&nbsp;&nbsp;@lang('labels.backend.finance_menu.recall_receipt_before_jul_2017')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.finance_menu.recall_receipt_before_jul_2017_summary')</p> </li>
                    </a>

                </ul>

                @if (true)
                    {{--item 3: Inspection--}}
                    &nbsp;  <ul class="list-unstyled">
                        <a href="{!! route("backend.compliance.inspection.index") !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-connectdevelop"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.inspection.title')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.compliance_menu.inspection.helper')</p> </li>
                        </a>
                    </ul>
                @endif

                {{--item 4 employer registrations--}}

                &nbsp;  <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer_registration.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-institution"> </i><large>&nbsp;&nbsp;@lang('labels.backend.member.employer_registrations')</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">@lang('labels.backend.compliance_menu.employer_registration_summary')</p> </li>
                    </a>

                </ul>

                {{--Manage Booking Groups--}}

                &nbsp;  <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.booking_group.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-building-o" aria-hidden="true"></i><large>&nbsp;&nbsp;Manage Booking Groups </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Create booking group, manage uploading of contribution distribution </p> </li>
                    </a>

                </ul>

{{--Employer Tempo closure follow ups--}}
                &nbsp;  <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.closure.tempo_closure_status_followup_page') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-building-o" aria-hidden="true"></i><large>&nbsp;&nbsp;Employer Temporary Closures  Alerts for Follow ups </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Employer temporary closure alerts; businesses which closed more than 6 months ago. </p> </li>
                    </a>

                </ul>

                  &nbsp;  <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.rates.index') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-money" aria-hidden="true"></i><large>&nbsp;&nbsp;Manage Contribution / Interest Rates </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Contribution / Interest Rates </p> </li>
                    </a>

                </ul>


                {{--Employe closures alert--}}
                {{--&nbsp;  <ul class="list-unstyled">--}}
                    {{--<a href="{!! route('backend.compliance.employer.closure.eoffice_alerts') !!}">--}}
                        {{--<li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-building-o" aria-hidden="true"></i><large>&nbsp;&nbsp;Employer Closure Alerts </large></h6>--}}

                            {{--<p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Employer closure alerts from e-office </p> </li>--}}
                    {{--</a>--}}

                {{--</ul>--}}


            </div>
        </div>

        {{--right div--}}
        @permission("view_ades_services")
        <div class="col-sm-12 col-md-12">

            <br>
            <h5 class="cancel_button site-btn">ADES Services</h5>
            <br>
            <div class="col-sm-6 col-md-6">
                <div class="list-group">
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.status') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-signal"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.employer_status')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    @lang('labels.backend.compliance_menu.employer_status_summary')
                                </p>
                            </li>
                        </a>

                    </ul>
                    <br>
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.newemployer') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-registered"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.newemployer')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    @lang('labels.backend.compliance_menu.newemployer_summary')
                                </p>
                            </li>
                        </a>

                    </ul>


                </div>
            </div>

            <div class="col-sm-6 col-md-6">
                <div class="list-group">
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.payesdl') !!}">


                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-calculator"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.payesdl')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    @lang('labels.backend.compliance_menu.payesdl_summary')
                                </p>
                            </li>
                        </a>

                    </ul>
                    <br>
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.closed') !!}">


                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-window-close"> </i><large>&nbsp;&nbsp;@lang('labels.backend.compliance_menu.closed')</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    @lang('labels.backend.compliance_menu.closed_summary')
                                </p>
                            </li>
                        </a>

                    </ul>
                </div>
            </div>

        </div>
        @endauth


        <div class="col-sm-12 col-md-12">

            <br>
            <h5 class="cancel_button site-btn">Large Contributors File</h5>
            <br>
            <div class="col-sm-6 col-md-6">
                <div class="list-group">
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.LargeContributor') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-check-circle"> </i><large>&nbsp;&nbsp;{{-- @lang('labels.backend.compliance_menu.employer_status') --}}Registered Online</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    {{--  @lang('labels.backend.compliance_menu.employer_status_summary') --}}
                                    Registered Online
                                </p>
                            </li>
                        </a>

                    </ul>
                    <br>
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.LargeContributorCertificate') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-exclamation-triangle"> </i><large>&nbsp;&nbsp;{{-- @lang('labels.backend.compliance_menu.newemployer') --}} Not Registred Online</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    {{--   @lang('labels.backend.compliance_menu.newemployer_summary') --}}

                                    Not Registred Online
                                </p>
                            </li>
                        </a>

                    </ul>


                </div>
            </div>

            <div class="col-sm-6 col-md-6">
                <div class="list-group">
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.LargeContributorCertificate') !!}">


                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-file-pdf-o"> </i><large>&nbsp;&nbsp;{{-- @lang('labels.backend.compliance_menu.payesdl') --}} Issued Certificates</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    {{--  @lang('labels.backend.compliance_menu.payesdl_summary') --}}
                                    Issued Certificates
                                </p>
                            </li>
                        </a>

                    </ul>
                    <br>
                    <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.closed') !!}">


                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-question-circle"> </i><large>&nbsp;&nbsp;{{-- @lang('labels.backend.compliance_menu.closed') --}} Follow up status</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">
                                    {{-- @lang('labels.backend.compliance_menu.closed_summary') --}}
                                    Follow up status
                                </p>
                            </li>
                        </a>

                    </ul>
                </div>
            </div>

        </div>




    </div>





@stop

@push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             $("#site-header-title").hide();
             */
            $("#preview").click(function() {

            });
        });
    </script>;

@endpush
