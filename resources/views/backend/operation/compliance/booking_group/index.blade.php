@extends('layouts.backend.main', ['title' => "Manage Booking Groups", 'header_title' => "Manage Booking Groups"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    <style>
        .select2-results {
            min-height: 80px;
            max-height: 80px;
            overflow-y: auto;
        }
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
        <div class="row">
            <div class="col-md-12" >
                <div class="pull-right" >
                    {{--upload manual contribution--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.booking_group.upload_manual', "<i class='icon fa fa-book' aria-hidden='true'></i>&nbsp;" . "Upload Manual Contribution", [],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                    {{--new inspection--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.booking_group.pending_upload', "<i class='icon fa fa-upload' aria-hidden='true'></i>&nbsp;" . "Upload Pending Electronic", [],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <br/>

    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['route' => 'backend.compliance.booking_group.store']) !!}
            <p>
                Search using, <code><b>Registration Number e.g <i class="underline">005408</i></b> </code> or <code><b>Employer name e.g <i class="underline">Accountant General's Department</i> </b>, use at least first four digits for Registration Number or four letters for Employer name.</code>
            </p>
            <div class="row">
                <div class="col-md-12">
                    <div class="grid-column">
                        {!! Form::select('employer', [], null, ['class' => 'employer-select', 'style' => 'width:100%']) !!}
                        {!! $errors->first('employer', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {!! Form::submit("Add", ['class' => 'btn btn-success btn-save btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}

        </div>
        <div class="col-md-12">
            <br/>
            <div class="pull-right">

                {!! Form::open(['route' => 'backend.compliance.booking_group.destroy', 'id' => 'remove_selected']) !!}
                <input type='hidden' name='_method' value='delete'>
                <button type="submit" class="btn btn-danger  btn-round-left">
                    <i class="icon fa fa-close" aria-hidden="true"></i>&nbsp;Remove Selected
                </button>
                {!! Form::close() !!}

            </div>
            <br/>
            <br/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="display" cellspacing="0" width="100%" id ="bookinggroup-table">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Registration Number</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    <script>
        $(function () {
            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    type : 'get',
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: false
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
            var $table = $('#bookinggroup-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: false,
                info: true,
                ajax:{
                    url : '{!! route('backend.compliance.booking_group.datatable') !!}',
                    type : 'post'
                },
                columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                select: {
                    'style': 'multi'
                },
                columns: [
                    {
                        orderable:      false,
                        searchable:      false,
                        data:           'id'
                    },
                    { data: 'name', name: 'name', searchable: false, orderable: false},
                    { data: 'reg_no' , name: 'reg_no'}
                ],
                'rowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:first-child)', nRow).click(function() {
                        /*document.location.href =  base_url + "/compliance/closed_business/" + aData['id'] + "/show ";*/
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }
            });
            // Handle button events
            $('#remove_selected').on('submit', function(e) {
                e.preventDefault();
                var $form = this;
                var $rows_selected = $table.column(0).checkboxes.selected();
                //Remove all previous selected
                $($form).find("input[name='id[]']").remove();
                // Iterate over all selected checkboxes
                $.each($rows_selected, function(index, rowId){
                    // Create a hidden element
                    $($form).append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'id[]')
                            .val(rowId)
                    );
                });
                swal({
                    title: "Warning",
                    text: "Are you sure to remove from the list of booking groups?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        $form.submit();
                    }
                });
            });
        });
    </script>

@endpush