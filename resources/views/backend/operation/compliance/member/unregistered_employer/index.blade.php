@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance_menu.unregistered_employer'), 'header_title' => trans('labels.backend.compliance_menu.unregistered_employer')])

@include('backend.includes.datatable_assets')

@section('content')
    {{----------Buttonr -----------}}
    {{--<div class="row">--}}
    <div class="col-md-12" >
        <div class="pull-right">

            {{--Upload Bulk Inactive--}}
            {{--<a href="{!! route('backend.compliance.unregistered_employer.upload_bulk_inactive') !!}"  class="btn btn-warning  btn-submit" ><i class="icon fa fa-plus-square"></i>&nbsp;@lang('labels.general.upload_bulk_inactive')</a>--}}

            {{--Upload Bulk active--}}
            <a href="{!! route('backend.compliance.unregistered_employer.upload_bulk') !!}"  class="btn btn-success  btn-submit" ><i class="icon fa fa-plus-square"></i>&nbsp;@lang('labels.general.upload_bulk_active')</a>

            {{--Add New Taxpayers--}}
            <a href="{!! route('backend.compliance.unregistered_employer.add_tra') !!}"  class="btn btn-info btn-submit" ><i class="icon fa fa-plus-square"></i>&nbsp;Add New From TRA</a>

            {{--Add New--}}
            <a href="{!! route('backend.compliance.unregistered_employer.create') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
        </div>
    </div>
    <div>&nbsp;</div>
    {{--</div>--}}

    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

        </div>
    </div>

@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
@endpush
