@extends('layouts.backend.main', ['title' => trans('labels.backend.member.unregistered_employer_profile'), 'header_title' => trans('labels.backend.member.unregistered_employer_profile')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@include('backend.includes.datatable_assets')


@section('content')


    <div class = "row">
        {{--{!! Form::model($employer, ['route' => ['backend.finance.receipt.dishonour', $employer->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}

        {{--Show alert info if employer with this tin is already registered not from Unregistered--}}
        @if ($check_tin == 1 && $unregistered_employer->is_registered == 0)
            <div class="alert alert-info">
                @lang("strings.backend.member.tin_already_registered")
            </div>
        @endif


        {{--HEADER--}}
        @include("backend.operation.compliance.member.unregistered_employer.includes.header_info",['unregistered_employer'=>$unregistered_employer])

        {{--Tabs navigation--}}
        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        {{--General--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#general"
                               data-toggle="tab">@lang('labels.general.general')
                            </a>
                        </li>


                    </ul>
                    {{--tab general--}}
                    <div class="nav_tab_contain tab-content">
                        <div id="general" class="nav_tab_pane tab-pane active in">
                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >

                                            @if ($unregistered_employer->is_registered == 0)

                                                {{--register--}}
                                                <span>

                                             {!! HTML::decode(link_to_route('backend.compliance.unregistered_employer.register',"<i class='icon fa fa-check-circle' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.register'), $unregistered_employer->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.compliance.register_employer_confirm'), 'class' => 'btn btn-primary site-btn nav_button'])) !!}
    </span>




                                                {{--modify--}}
                                                <span>
                                        <a href="{!! route('backend.compliance.unregistered_employer.edit', $unregistered_employer->id)
                                        !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pencil-square-o"></i>&nbsp;@lang('buttons.general.modify')</a>
    </span>


                                                @if($unregistered_employer->assign_to < 1)
                                                    {{--Assign staff--}}

                                                    <span>
                                        <a href="{!! route('backend.compliance.unregistered_employer.assign_staff_page', $unregistered_employer->id)
                                        !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-user"></i>&nbsp;@lang('buttons.backend.member.employer.assign_staff')</a>
    </span>

                                                    {{--Self staff assign--}}
                                                    {{--<span>--}}

                                            {{--{{ link_to_route('backend.compliance.unregistered_employer.self_assign_staff', trans('buttons.backend.member.employer.self_staff_assign'), $unregistered_employer->id, ['data-method' => 'confirm','data-type'=>'success', 'confirm-button-color'=> '#2e74ff', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.compliance.assign_unregistered_employer_confirm'), 'class' => 'btn btn-primary site-btn nav_button fa fa-user']) }}--}}
    {{--</span>--}}
                                                @endif

                                            @endif
                                            {{--More--}}
                                            @include('backend.operation.compliance.member.unregistered_employer.includes.more_links')

                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{--main tab content--}}
                            <div class = "row">
                                <div class="col-md-12">


                                    <div class="col-md-9">
                                        {{--follow-ups--}}
                                        <legend class="grey_modal" >@lang('labels.backend.member.follow_ups_overview')</legend>

                                        {{--Add New FOllow up--}}
                                        @if ($unregistered_employer->is_registered == 0)
                                            <div class="pull-right">

            <span>
        <a href="{!! route('backend.compliance.unregistered_employer.follow_up_create', $unregistered_employer->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
        </span>
                                                {{--@endif--}}
                                            </div>
                                        @endif
                                        {!! $follow_ups_datatable->with(['unregistered_employer_id' => $unregistered_employer->id])->render('backend.operation.compliance.member.unregistered_employer.includes.follow_ups') !!}


                                    </div>
                                    <div class="col-md-3">
                                        {{--sidebar summary--}}
                                        @include('backend.operation.compliance.member.unregistered_employer.includes.sidebar_summary')
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
            </div>

        </div>

        {{--{!! Form::close() !!}--}}
    </div>
@stop


@push('after-script-end')
@stack('follow-ups-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">

    $(function () {


        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });

</script>;

@endpush
