{{--create Form--}}

{!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
{!! Form::hidden('wcf_start_date', getWCFLaunchDate(), ['class' =>'wcf_date']) !!}
{!! Form::hidden('inspection_start_date', ($unregistered_employer->inspection_task_id) ? $unregistered_employer->inspectionTask->inspection->start_date : getWCFLaunchDate(), ['class'
=>'inspection_start_date']) !!}
{{--main contents--}}

{{--description--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.descriptions'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group text_content">
                    {!! Form::textarea('description', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                    {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>


{{--follow up type--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.follow_up_type'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('follow_up_type_cv_id', $follow_up_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'follow_type_id']) !!}

                    {!! $errors->first('follow_up_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>

{{--contact--}}
<div class="row" id="contact_div">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.contact'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.contact_tooltip_description')  "></i></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact', null, ['class' => 'form-control', 'id'=>'contact_id']) !!}

                    {!! $errors->first('contact', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>


{{--contact person--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.contact_person'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact_person', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_person', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

{{--date_of_follow_up--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.date_of_follow_up'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                {{--<div class="row">--}}
                <div class="form-group">
                    <div class="form-inline">

                        {!!  Form::selectRange('follow_up_day',1,31,null, ['class' => 'form-control  search-select','style'=>'width:58px', 'placeholder' =>
      'Day', 'id'=>'follow_up_day']) !!}


                        <span>      {!!  Form::selectMonth('follow_up_month',null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month', 'id'=>'follow_up_month']) !!}
                        </span>


                        <span>      {!!  Form::selectRange('follow_up_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::parse(getWCFStartDate())->format('Y'),null, ['class' => 'form-control search-select','style'=>'width:62px',
                        'placeholder' =>
                         'Year', 'id'=>'follow_up_year']) !!}
                        </span>

                    </div>
                </div>
                {!! Form::hidden('date_of_follow_up') !!}
                {!! $errors->first('date_of_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>

{{--feedback--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.feedback'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <div class="form-group text_content" >
                        {!! Form::textarea( 'feedback', null, [ 'id'=> 'address',  'class' =>'form-control']) !!}
                        {!! $errors->first('feedback', '<span class="help-block label
                           label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




