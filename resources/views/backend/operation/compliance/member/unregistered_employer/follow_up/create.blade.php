@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_new_follow_up'), 'header_title' => trans('labels.backend.member.add_new_follow_up')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($unregistered_employer,['route' => ['backend.compliance.unregistered_employer.follow_up_store',$unregistered_employer->id ],'method'=>'post',
     'name' => 'create']) !!}

    {{--main contents--}}

    {{--HEADER--}}
    @include("backend.operation.compliance.member.unregistered_employer.includes.header_info",['unregistered_employer'=>$unregistered_employer])


    <br>

    {{--Create form--}}
    @include("backend.operation.compliance.member.unregistered_employer.follow_up.includes.create_form",['unregistered_employer'=>$unregistered_employer])



    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.unregistered_employer.profile',trans('buttons.general.cancel'), [$unregistered_employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{--<script  type="text/javascript">--}}



<script>
    $(function () {
        $(".search-select").select2({});

        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();

        $('body').on('submit', 'form[name=create]', function(e) {
            e.preventDefault();
// Validate date -- date commenced date
            var $day = $('#follow_up_day').val();
            var $month = $('#follow_up_month').val();
            var $year = $('#follow_up_year').val();
            if (($year) && ($month) && ($day )) {
                $('input[name=date_of_follow_up]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=date_of_follow_up]").val("");
            }

            this.submit();

        });

        follow_up_type_option('follow_type_id', 'contact_div','contact_id');
        $("#follow_type_id").on("change", function (e) {
            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
        });


    });

    // Follow up type option
    function follow_up_type_option(follow_type_id, contact_div, contact_id) {

        var choice = $("#" + follow_type_id).val();
//            if is visit
        if (choice == 93) {
            $("#" + contact_div).hide();
        } else {
//            phone call
            if (choice == 91) {
                $("#" + contact_id).attr('placeholder','Phone #');
            }else if(choice == 92){
                $("#" + contact_id).attr('placeholder','Email@yahoo.com');
            }
            $("#" + contact_div).show();

        }
    }

</script>;


@endpush