@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_follow_up'), 'header_title' => trans('labels.backend.member.edit_follow_up')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($unregistered_employer_follow_up,['route' => ['backend.compliance.unregistered_employer.follow_up_update',$unregistered_employer_follow_up->id ],'method'=>'put',
     'name' => 'update']) !!}


    {{--dates--}}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('inspection_start_date', isset($unregistered_employer_follow_up->unregisteredEmployer->inspection_task_id) ? $unregistered_employer_follow_up->unregisteredEmployer->inspectionTask->inspection->start_date: getWCFLaunchDate(), ['class' =>'inspection_start_date']) !!}



    {{--HEADER--}}
    @include("backend.operation.compliance.member.unregistered_employer.includes.header_info",['unregistered_employer'=>$unregistered_employer_follow_up->unregisteredEmployer])
    {{--main contents--}}
<br>
    {{--Delete follow up--}}
    <div class="pull-right">
        {{ link_to_route('backend.compliance.unregistered_employer.follow_up_delete', trans('buttons.general.crud.delete'), $unregistered_employer_follow_up->id, ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance.confirm_follow_up_delete'), 'class' => 'btn btn-danger btn-round-right']) }}
    </div>



    {{--description--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.descriptions'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::textarea('description', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                        {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--follow up type--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.follow_up_type'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('follow_up_type_cv_id', $follow_up_types, $unregistered_employer_follow_up->follow_up_type_cv_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'follow_type_id']) !!}

                        {!! $errors->first('follow_up_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--contact--}}
    <div class="row" id="contact_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.contact'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.contact_tooltip_description')  "></i></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','contact', null, ['class' => 'form-control', 'id'=>'contact_id']) !!}

                        {!! $errors->first('contact', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--contact person--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.contact_person'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','contact_person', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('contact_person', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--date_of_follow_up--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.date_of_follow_up'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">
                        <div class="form-inline">

                            {!!  Form::selectRange('follow_up_day',1,31,\Carbon\Carbon::parse($unregistered_employer_follow_up->date_of_follow_up)->format('d'), ['class' => 'form-control  search-select','style'=>'width:58px', 'placeholder' =>
          'Day', 'id'=>'follow_up_day']) !!}


                            <span>      {!!  Form::selectMonth('follow_up_month',\Carbon\Carbon::parse($unregistered_employer_follow_up->date_of_follow_up)->format('m'), ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month', 'id'=>'follow_up_month']) !!}
                        </span>


                            <span>      {!!  Form::selectRange('follow_up_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::parse(getWCFStartDate())->format('Y'),\Carbon\Carbon::parse($unregistered_employer_follow_up->date_of_follow_up)->format('Y'), ['class' => 'form-control search-select','style'=>'width:62px',
                        'placeholder' =>
                         'Year', 'id'=>'follow_up_year']) !!}
                        </span>

                        </div>
                    </div>
                    {!! Form::hidden('date_of_follow_up') !!}
                    {!! $errors->first('date_of_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
    </div>

    {{--feedback--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.feedback'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-group text_content" >
                            {!! Form::textarea( 'feedback', null, [ 'id'=> 'address',  'class' =>'form-control']) !!}
                            {!! $errors->first('feedback', '<span class="help-block label
                               label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('compliance/unregistered_employer/profile/' . $unregistered_employer_follow_up->unregistered_employer_id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">




    $(function () {
        $(".search-select").select2({});

        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();



        $('.save_button').on('click', function(e) {
            // Validate date -- date follow up date
            var $day = $('#follow_up_day').val();
            var $month = $('#follow_up_month').val();
            var $year = $('#follow_up_year').val();
            if (($year) && ($month) && ($day)) {
                $('input[name=date_of_follow_up]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=date_of_follow_up]").val("");
            }

        });


        follow_up_type_option('follow_type_id', 'contact_div','contact_id');
        $("#follow_type_id").on("change", function (e) {
            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
        });


    });

    // Follow up type option
    function follow_up_type_option(follow_type_id, contact_div, contact_id) {

        var choice = $("#" + follow_type_id).val();
//            if is visit
        if (choice == 93) {
            $("#" + contact_div).hide();
        } else {
//            phone call
            if (choice == 91) {
                $("#" + contact_id).attr('placeholder','Phone #');
            }else if(choice == 92){
                $("#" + contact_id).attr('placeholder','Email@yahoo.com');
            }
            $("#" + contact_div).show();

        }
    }

</script>;


@endpush
