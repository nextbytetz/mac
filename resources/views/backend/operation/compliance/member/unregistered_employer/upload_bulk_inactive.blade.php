@extends('layouts.backend.main', ['title' => trans('labels.backend.member.upload_inactive_unregistered_employers'), 'header_title' => trans('labels.backend.member.upload_inactive_unregistered_employers')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')

{{ Html::style(asset_url() . "/nextbyte/plugins/formstone/css/upload.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/formstone/css/themes/light/upload.css") }}

<style>
    .filelists {
        margin: 20px 0;
    }

    .filelists h5 {
        margin: 10px 0 0;
    }

    .filelists .cancel_all {
        color: red;
        cursor: pointer;
        clear: both;
        font-size: 10px;
        margin: 0;
        text-transform: uppercase;
    }

    .filelist {
        margin: 0;
        padding: 10px 0;
    }

    .filelist li {
        background: #fff;
        border-bottom: 1px solid #ECEFF1;
        font-size: 14px;
        list-style: none;
        padding: 5px;
        position: relative;
    }

    .filelist li:before {
        display: none !important;
    }
    /* main site demos */

    .filelist li .bar {
        background: #eceff1;
        content: '';
        height: 100%;
        left: 0;
        position: absolute;
        top: 0;
        width: 0;
        z-index: 0;
        -webkit-transition: width 0.1s linear;
        transition: width 0.1s linear;
    }

    .filelist li .content_upload {
        display: block;
        overflow: hidden;
        position: relative;
        z-index: 1;
    }

    .filelist li .file {
        color: #455A64;
        float: left;
        display: block;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 50%;
        white-space: nowrap;
    }

    .filelist li .progress {
        color: #B0BEC5;
        display: block;
        float: right;
        font-size: 10px;
        text-transform: uppercase;
    }

    .filelist li .cancel {
        color: red;
        cursor: pointer;
        display: block;
        float: right;
        font-size: 10px;
        margin: 0 0 0 10px;
        text-transform: uppercase;
    }

    .filelist li.error .file {
        color: red;
    }

    .filelist li.error .progress {
        color: red;
    }

    .filelist li.error .cancel {
        display: none;
    }
</style>

@endpush

@section('content')

    <div class="nav_tab_pane_header">
        {{--Header Bar--}}
        <div class="row">
            <div class="col-md-12" >
                <div class="pull-right" >
                    {{--close--}}
                    <span>
                        <a href="{!! route('backend.compliance.unregistered_employer.index') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class = "row">

        <div class="col-md-12">
            <br/>
            <label class="control-label">@lang('labels.backend.finance.receipt.select_file')</label>
            {!! Form::open(['route' => ['backend.compliance.unregistered_employer.store_uploaded_bulk_inactive'], 'class' => 'form demo_form', 'role' => 'form', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="file_upload" data-upload-options='{"action":"{!! route('backend.compliance.unregistered_employer.store_uploaded_bulk_inactive') !!}"}'>
            </div>

            <div class="filelists">
                <h5>Complete</h5>
                <ol class="filelist complete">
                </ol>
                <h5>Queued</h5>
                <ol class="filelist queue">
                </ol>
                <span class="cancel_all">Cancel All</span>
            </div>

            {!! Form::close() !!}
        </div>

    </div>

    <br/>
    <div class="row">
        <div class="col-md-12" class="form-inline" >
        </div>
    </div>

    <br/>
    <br/>
    <h4>@lang('labels.backend.file.instruction')</h4>
    <div class="row">
        <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-pink"></i>
					    <i class="fa fa-info fa-stack-1x fa-inverse"></i>
					</span>
        </div>
        <div class='col-md-11'>
            <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.title')</u></h4>
            Microsoft Excel file xlsx, xls <br/>
        <!--					@lang('labels.backend.file.format.file_helper')-->
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-orange"></i>
					    <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
					</span>
        </div>
        <div class='col-md-11'>
            <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.content')</u></h4>
            @lang('labels.backend.file.format.content_helper') <br/>
            @lang('labels.backend.file.format.column')  &nbsp;<span class="label label-success">name</span>&nbsp;<span class="label label-success">tin</span> <span class="label label-success">doc</span> <span class="label label-success">business_activity</span> <span class="label label-success">sector</span>  <span class="label label-success">no_of_employees</span>  <span class="label label-success">phone</span> <span class="label label-success">telephone</span><span class="label label-success">email</span> <span class="label label-success">p_o_box</span> <span class="label label-success">region</span> <span class="label label-success">postal_city</span> <span class="label label-success">street</span> <span class="label label-success">plot_no</span><span class="label label-success">block_no</span>   &nbsp;, @lang('labels.backend.file.format.doc_helper')
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-yellow"></i>
					    <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
					</span>
        </div>
        <div class='col-md-12'>
            <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.example')</u></h4>
            @lang('labels.backend.file.format.example_helper') <br/><br/>
            <img src="{{ asset_url() . '/nextbyte/img/upload_unregistered_sample.png' }}" height="auto" width="auto">
        </div>
    </div>

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/formstone/js/core.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/formstone/js/upload.js") }}
<script  type="text/javascript">
    $(function () {
        $(".file_upload").upload({
            maxSize: 1073741824,
            postKey: 'linked_file',
            beforeSend: onBeforeSend
        }).on("start.upload", onStart)
            .on("complete.upload", onComplete)
            .on("filestart.upload", onFileStart)
            .on("fileprogress.upload", onFileProgress)
            .on("filecomplete.upload", onFileComplete)
            .on("fileerror.upload", onFileError)
            .on("chunkstart.upload", onChunkStart)
            .on("chunkprogress.upload", onChunkProgress)
            .on("chunkcomplete.upload", onChunkComplete)
            .on("chunkerror.upload", onChunkError)
            .on("queued.upload", onQueued);

        $(".filelist.queue").on("click", ".cancel", onCancel);
        $(".cancel_all").on("click", onCancelAll);

        function onCancel(e) {
            console.log("Cancel");
            var index = $(this).parents("li").data("index");
            $(this).parents("form").find(".upload").upload("abort", parseInt(index, 10));
        }

        function onCancelAll(e) {
            console.log("Cancel All");
            $(this).parents("form").find(".upload").upload("abort");
        }

        function onBeforeSend(formData, file) {
            console.log("Before Send");
            formData.append("test_field", "test_value");
            // return (file.name.indexOf(".jpg") < -1) ? false : formData; // cancel all jpgs
            return formData;
        }

        function onQueued(e, files) {
            console.log("Queued");
            var html = '';
            for (var i = 0; i < files.length; i++) {
                html += '<li data-index="' + files[i].index + '"><span class="content_upload"><span class="file">' + files[i].name + '</span><span class="cancel">Cancel</span><span class="progress">Queued</span></span><span class="bar"></span></li>';
            }

            $(this).parents("form").find(".filelist.queue")
                .append(html);
        }

        function onStart(e, files) {
            console.log("Start");
            $(this).parents("form").find(".filelist.queue")
                .find("li")
                .find(".progress").text("Waiting");
        }

        function onComplete(e) {
            console.log(e);
            // All done!
        }

        function onFileStart(e, file) {
            console.log("File Start");
            $(this).parents("form").find(".filelist.queue")
                .find("li[data-index=" + file.index + "]")
                .find(".progress").text("0%");
        }

        function onFileProgress(e, file, percent) {
            console.log("File Progress");
            var $file = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");

            $file.find(".progress").text(percent + "%")
            $file.find(".bar").css("width", percent + "%");
        }

        function onFileComplete(e, file, response) {
            console.log("File Complete" + " " + response);
            if (response.trim() === "" || response.toLowerCase().indexOf("error") > -1) {
                $(this).parents("form").find(".filelist.queue")
                    .find("li[data-index=" + file.index + "]").addClass("error")
                    .find(".progress").text(response.trim());
            }
            else {
                var $target = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
                $target.find(".file").text(file.name);
                $target.find(".progress").remove();
                $target.find(".cancel").remove();
                $target.appendTo($(this).parents("form").find(".filelist.complete"));
            }
        }

        function onFileError(e, file, error) {
            console.log("File Error");
            $(this).parents("form").find(".filelist.queue")
                .find("li[data-index=" + file.index + "]").addClass("error")
                .find(".progress").text("Error: " + error);
        }

        function onChunkStart(e, file) {
            console.log("Chunk Start");
        }

        function onChunkProgress(e, file, percent) {
            console.log("Chunk Progress");
        }

        function onChunkComplete(e, file, response) {
            console.log("Chunk Complete");
        }

        function onChunkError(e, file, error) {
            console.log("Chunk Error");
        }
    });
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content")
        }
    });

</script>;
@endpush
