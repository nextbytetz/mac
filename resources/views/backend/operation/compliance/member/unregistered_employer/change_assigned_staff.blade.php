@extends('layouts.backend.main', ['title' => trans('labels.backend.member.change_staff_assignment'), 'header_title' => trans('labels.backend.member.change_staff_assignment')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($unregistered_employer,['route' => ['backend.compliance.unregistered_employer.assigned_staff_update',$unregistered_employer->id ],'method'=>'put',
    'id' => 'update']) !!}


    {{--HEADER--}}
    @include("backend.operation.compliance.member.unregistered_employer.includes.header_info",['unregistered_employer'=>$unregistered_employer])


    <div>&nbsp;</div>

    {{--main contents--}}


    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">

            {{--Assign to--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.assigned_to'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('assigned_to', $users, $unregistered_employer->assign_to, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'assigned_to']) !!}
                        {!! $errors->first('assigned_to', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>





    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('compliance/unregistered_employer/profile/' . $unregistered_employer->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ])!!}
                                               {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">

    $(function () {
        $(".search-select").select2({});
    });

</script>;


@endpush
