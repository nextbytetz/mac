@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_unregistered_employer'), 'header_title' => trans('labels.backend.member.edit_unregistered_employer')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($unregistered_employer,['route' => ['backend.compliance.unregistered_employer.update', $unregistered_employer->id ],'method'=>'put',
    'name' => 'update']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}



    {{--HEADER--}}
    @include("backend.operation.compliance.member.unregistered_employer.includes.header_info",['unregistered_employer'=>$unregistered_employer])
    {{--main contents--}}

    <br>
    {{--name--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.name'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                                            {!! Form::textarea('name', $unregistered_employer->name, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--Date Commenced--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.employer.date_commenced'):</label></div>
                <div class="col-xs-4 col-lg-4 col-md-4 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">
                        <div class="form-inline">

                            {!!  Form::selectRange('commenced_day',1,31, ($unregistered_employer->doc) ? \Carbon\Carbon::parse($unregistered_employer->doc)->format('d') : '', ['class' => 'form-control  search-select','style'=>'width:55px', 'placeholder' =>
          'Day', 'id'=>'commenced_day']) !!}


                            <span>      {!!  Form::selectMonth('commenced_month',($unregistered_employer->doc) ? \Carbon\Carbon::parse($unregistered_employer->doc)->format('m') : '', ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month', 'id'=>'commenced_month']) !!}
                        </span>


                            <span>      {!!  Form::selectRange('commenced_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(180)->format('Y'),($unregistered_employer->doc) ? \Carbon\Carbon::parse($unregistered_employer->doc)->format('Y') : '', ['class' => 'form-control search-select','style'=>'width:62px',
                        'placeholder' =>
                         'Year', 'id'=>'commenced_year']) !!}
                        </span>

                        </div>
                    </div>
                    {!! Form::hidden('doc') !!}
                    {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
                    {{--</div>--}}
                </div>
            </div>

        </div>
    </div>




    <div class="row">
        <div class="col-md-12">
            {{--tin--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.tin_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','tin', null, ['class' => 'form-control number']) !!}
                        {!! $errors->first('tin', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--phone--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.phone'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','phone', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>




      <div class="row">
        <div class="col-md-12">

            {{--Telephone--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.telephone'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','telephone', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('telephone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--email--}}


            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.email'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','email', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>


    {{--box--}}
    <div class="row">
        <div class="col-md-12">


            {{--box--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.po_box'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','box_no', $unregistered_employer->address, ['class' => 'form-control']) !!}
                        {!! $errors->first('box_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



            {{--fax--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.fax'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','fax', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('fax', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>






    <div class="row">
        <div class="col-md-12">


            {{--country--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.country'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('country_id', $countries, ($unregistered_employer->district_id) ? $unregistered_employer->district->region->country->id : null , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'country_id']) !!}
                        {!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>




            {{--region id--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.region'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('region_id', $regions, ($unregistered_employer->district_id) ? $unregistered_employer->district->region->id : null , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                        {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>




        </div>
    </div>



    <div class="row">
        <div class="col-md-12">


            {{--district--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.backend.table.district'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('district_id', $districts, ($unregistered_employer->district_id) ? $unregistered_employer->district_id : null , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                        {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--location type id--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.location_type'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('location_type_id', $location_types, ($unregistered_employer->location_type_id) ? $unregistered_employer->location_type_id : null , ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'location_type']) !!}
                        {!! $errors->first('location_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--street--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.street'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','street', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('street', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--road--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.road'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::input( 'text','road', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('road', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            {{--plotno--}}
            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.plot_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','plot_no', $unregistered_employer->plot_number, ['class' => 'form-control']) !!}
                        {!! $errors->first('plot_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            {{--block_no--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.block_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::input( 'text','block_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('block_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>

    </div>

    {{--surveyed extra--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.surveyed_extra'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','surveyed_extra', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('surveyed_extra', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--unsurveyed--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form unsurveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.unsurveyed_area'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::textarea('unsurveyed_area', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                        {!! $errors->first('unsurveyed_area', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


        </div>
    </div>


    {{--sector--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.backend.member.nature_of_business'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('sector', $business_sectors, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'sector']) !!}
                        {!! $errors->first('sector', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



            {{--business_activities--}}
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.member.business_activities'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::textarea('business_activity', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                        {!! $errors->first('business_activity', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--No. of EMployees--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.no_of_employees'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','no_of_employees', null, ['class' => 'form-control number']) !!}
                        {!! $errors->first('sector_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>






    {{--Buttons--}}
    <div class="row">
        <div class="col-md-10" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.unregistered_employer.profile',trans('buttons.general.cancel'), [$unregistered_employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>




    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $(function () {
        $(".search-select").select2();
        $(".surveyed").hide();
        $(".unsurveyed").hide();
//      $('#smartwizard').smartWizard();


        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();


        location_type_option('location_type', 'surveyed','unsurveyed');
        $("#location_type").on('change', function (e){
            location_type_option('location_type', 'surveyed','unsurveyed');
        });


        $('#country_id').on('change', function (e) {
            $("#spin2").show();
            var country_id = e.target.value;
            $.get("{{ url('/') }}/getRegions?country_id=" + country_id, function (data) {
                $('#region_id').empty();
                $("#region_id").select2("val", "");
                $('#region_id').html(data);
                $("#spin2").hide();
            });
        });


        $('#region_id').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                $('#district_id').empty();
                $("#district_id").select2("val", "");
                $('#district_id').html(data);
                $("#spin2").hide();
            });
        });



        $('body').on('submit', 'form[name=update]', function(e) {
            e.preventDefault();
            // Validate date -- date commenced date
            var $day = $('#commenced_day').val();
            var $month = $('#commenced_month').val();
            var $year = $('#commenced_year').val();
            if (($year) && ($month) && ($day )) {
                $('input[name=doc]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=doc]").val("");
            }

            this.submit();

        });


        $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
            number_only(e);

        });
    });


    // check if location type is selected -> survyed and unsurveyed
    function location_type_option(location_type, surveyed,unsurveyed) {

        var choice = $("#"+location_type).val();
        switch (choice) {
            case '1':
                $("." + surveyed).show();
                $("." + unsurveyed).hide();
                break;
            case '2':
                $("."+surveyed).hide();
                $("."+unsurveyed).show();
                break;
            default:
                $("." + surveyed).hide();
                $("." + unsurveyed).hide();
        }
    }



    /* start : ensure only numbers are input on monetary boxes */
    function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }


</script>;
@endpush

