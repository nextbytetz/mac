{{--Notification Report -> MORE LINKS OPTION--}}

<span class="dropdown"> <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" >
    @lang('buttons.general.more')
      </a>
  <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">

    @if ($unregistered_employer->is_registered == 0)
          {{--Change Assignment--}}
          @if($unregistered_employer->assign_to > 0)
              <a class="dropdown-item" href="{!! route('backend.compliance.unregistered_employer.assigned_staff_edit', $unregistered_employer->id)
                                        !!}">@lang('buttons.backend.member.employer.change_staff_assignment')</a>
          @endif
              <a>
                  {{--mark_as_registered--}}

                  {{ link_to_route('backend.compliance.unregistered_employer.mark_registered', trans('buttons.backend.member.employer.mark_as_registered'), $unregistered_employer->id, ['data-method' => 'confirm','data-type'=>'success', 'confirm-button-color'=> '#2e74ff', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.compliance.mark_registered_confirm'), 'class' => 'dropdown-item']) }}
      </a>

          <a>
                  {{--Undo unregistered--}}
              {{ link_to_route('backend.compliance.unregistered_employer.undo', trans('labels.general.undo'), $unregistered_employer->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance.undo_unregistered_confirm'), 'class' => 'dropdown-item']) }}
      </a>
      @endif
      {{--Close--}}
      <a class="dropdown-item" href="{!! route('backend.compliance.unregistered_employer.index') !!}" >@lang('buttons.general.close')</a>
  </span>

</span>