

{{--sidebar employer summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%">
            <tr>
                <td style="padding-left: 5px" width="105px">@lang('labels.general.name'):</td>
                <td  height="20px"><b>{!! Form::label( 'name', $unregistered_employer->name_formatted, [ 'id'=> 'name']) !!}</b></td>

            </tr>

            {{--tin--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.tin_no'):</td>
                <td><b>{!! Form::label( 'tin', ($unregistered_employer->tin) ? $unregistered_employer->tin : ' ', [ 'id'=> 'tin']) !!}</b></td>

            </tr>

{{--doc--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.employer.date_commenced'):</td>
                <td><b>{!! Form::label( 'tin', ($unregistered_employer->doc) ? $unregistered_employer->date_of_commencement_formatted : ' ', [ 'id'=> 'doc']) !!}</b></td>

            </tr>


{{--phone--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.phone'):</td>
                <td><b>{!! Form::label( 'phone', ($unregistered_employer->phone) ? $unregistered_employer->phone : ' ', [ 'id'=> 'phone'])
                        !!}</b></td>

            </tr>

            {{--telephone--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.telephone'):</td>
                <td><b>{!! Form::label( 'telephone', ($unregistered_employer->telephone) ? $unregistered_employer->telephone : ' ', [ 'id'=> 'telephone'])
                        !!}</b></td>

            </tr>
            {{--email--}}
            <tr style="word-wrap: break-word; word-break: break-all;">
                <td style="padding-left: 5px">@lang('labels.general.email'):</td>
                <td><b>{!! Form::label( 'email', ($unregistered_employer->email) ? $unregistered_employer->email : ' ', [ 'id'=> 'email'])
                        !!}</b></td>

            </tr>

            {{--po box--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.po_box'):</td>
                <td><b>{!! Form::label( 'box_no', ($unregistered_employer->box_no) ? $unregistered_employer->box_no :(( $unregistered_employer->address) ?  $unregistered_employer->address : ' '), [ 'id'=> 'box_no'])
                        !!}</b></td>

            </tr>


            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.district'):</td>
                <td><b>{!! Form::label( 'district', ($unregistered_employer->district_id) ? $unregistered_employer->district->name : (($unregistered_employer->location) ? $unregistered_employer->location :' '), [ 'id'=> 'district'])
                        !!}</b></td>
            </tr>

            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.region'):</td>
                <td><b>{!! Form::label( 'region', ($unregistered_employer->district_id) ? $unregistered_employer->district->region->name : (($unregistered_employer->postal_city) ? $unregistered_employer->postal_city : ' ' ), [ 'id'=> 'region'])
                        !!}</b></td>
            </tr>

            <tr>
                <td style="padding-left: 5px">@lang('labels.general.country'):</td>
                <td><b>{!! Form::label( 'country', ($unregistered_employer->district_id) ? $unregistered_employer->district->region->country->name : ' ', [ 'id'=> 'country'])
                        !!}</b></td>

            </tr>


            {{--no of employees--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.no_of_employees'):</td>
                <td><b>{!! Form::label( 'sector', ($unregistered_employer->no_of_employees) ? $unregistered_employer->no_of_employees : ' ', [ 'id'=> 'no_of_employees'])
                        !!}</b></td>

            </tr>




            {{--sector--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.nature_of_business'):</td>
                <td><b>{!! Form::label( 'sector', ($unregistered_employer->sector) ? $unregistered_employer->sector : ' ', [ 'id'=> 'sector'])
                        !!}</b></td>

            </tr>



            {{--main_activities--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.business_activities'):</td>
                <td><b>{!! Form::label( 'business_activity', ($unregistered_employer->business_activity) ? $unregistered_employer->business_activity : ' ', [ 'id'=> 'business_activity'])
                        !!}</b></td>

            </tr>






            {{--Assigned To--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.assigned_to'):</td>
                <td><b>{!! Form::label( 'assigned_to', ($unregistered_employer->assignedStaff()->count()) ? $unregistered_employer->assignedStaff->name : ' ', [ 'id'=> 'assigned_to'])
                        !!}</b></td>

            </tr>


            {{--Assigned Date--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.date_assigned'):</td>
                <td><b>{!! Form::label( 'assigned_to', ($unregistered_employer->date_assigned) ? $unregistered_employer->date_assigned_formatted : ' ', [ 'id'=> 'date_assigned'])
                        !!}</b></td>

            </tr>


        </table>
    </div>
</div>






@push('sidebar-script-end')

<script  type="text/javascript">

</script>;

@endpush