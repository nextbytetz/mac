@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_contribution_track'), 'header_title' => trans('labels.backend.member.edit_contribution_track')])

@section('after-styles-end')

@endsection

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($contribution_track,['route' => ['backend.compliance.contribution_track.update',$contribution_track->id],'method'=>'put',
    'id' => 'update_contribution_track']) !!}
    <div class="row">

        <div class="col-md-12 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->

                {{--<strong> Contribution: {!! Form::label( 'name', $receipt_code->id, [ 'id'=> 'id']) !!}</strong>--}}

                <small >
                    @lang('labels.backend.finance.header.receipt.retrieve.index'): {!! Form::label( 'rctno', ($contribution_track->receipt_code->receipt()->count()) ? $contribution_track->receipt_code->receipt->rctno : ' ', [
                    'id'=>
                    'rctno']) !!}
                </small>
                          </h5>
            <legend></legend>
        </div>


        <div>&nbsp;</div>

        <div class="row">
            {{--comments--}}
            <div class="col-md-7" >
                <div class="element-form"  >
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.comments'):</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'comments', null, [ 'id'=> 'comments',  'class' =>'form-control']) !!}
                            {!! $errors->first('comments', '<span class="help-block label
                               label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--todoc--}}
        <div class="row">
            <div class="col-md-7" >
                <div class="element-form"  >
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.todo'):</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content_todo">
                            {!! Form::textarea( 'todo', null, [ 'id'=> 'todo','class' =>'form-control']) !!}
                            {!! $errors->first('todo', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--status--}}
        <div class="row">
            <div class="col-md-7" >
                <div class="element-form"  >
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.status'):</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-8 col-lg-8 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" >
                            {!!  Form::select('status', ['0' => 'To Start', '1' => 'Completed'],  null, ['class' => 'form-control' ]) !!}
                            {{--'class' =>'form-control']) !!}--}}
                            {!! $errors->first('status', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>


        {{--Buttons--}}
        <div class="row">
            <div class="col-md-6" class="form-inline" >
                <div class="element-form">
                    <div class="col-xl-3 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                    <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                        <div class="pull-right">

                            {!! link_to_route('backend.finance.receipt_code.show',trans('buttons.general.cancel'), [ $contribution_track->receipt_code_id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}
        {{--</section>--}}


        @stop


        @push('after-script-end')
        <script  type="text/javascript">
            $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();
            //todoc
            $('#text_content_todo').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

        </script>;


    @endpush
