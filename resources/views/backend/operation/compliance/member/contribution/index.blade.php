@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance.title'), 'header_title' => trans('labels.backend.compliance_menu.received_contribution')])

@include('backend.includes.datatable_assets')

@section('content')
    {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%'], true) !!}
@stop

@push('after-script-end')
    {!! $dataTable->scripts() !!}
@endpush
