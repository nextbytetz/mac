
{{--contribution tracks table--}}


<table class="display" cellspacing="0" width="100%" id ="contribution-tracks-table">
    <thead>
    <tr>

        <th>@lang('labels.backend.table.user_name')</th>
        <th>@lang('labels.backend.table.comments')</th>
        <th>@lang('labels.backend.table.todo')</th>
        <th>@lang('labels.backend.table.status')</th>
        <th>@lang('labels.backend.table.created_at')</th>
        <th>@lang('labels.backend.table.action')</th>
    </tr>
    </thead>

</table>




@push('contribution-tracks-script-end')

<script  type="text/javascript">
    $(function() {
        $('#contribution-tracks-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.receipt_code.contribution_tracks', $receipt_code->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'user_id' , name: 'user_id' },
                { data: 'comments' , name: 'comments' },
                { data: 'todo', name: 'todo' },
                { data: 'status', name: 'status' },
                { data: 'created_at', name: 'create_at'},
                { data: 'action', name: 'action'}
                           ]

        } );
    });
</script>;

@endpush