<div class="basic_nav_pills nav_basic_tab">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            {{--<a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.finance.receipt.contribution_track')</a>--}}
            <legend class="nav-link active" data-toggle="tab">&nbsp;&nbsp;Reconciliation</legend>
        </li>
    </ul>
    <div class="nav_tab_contain tab-content">
        <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
            <div class="row">
                <div class="col-md-6">
                    <div class="underline">Pay Status&nbsp;{!! $receipt_code->pay_status_label !!}</div>
                    <br/>
                    @if (!$receipt_code->pay_status)
                        {{--not reconciled--}}

                    @else
                        @if ($duplicates)
                            @if ($receipt_code->pay_status == 1)
                                <div>Has Duplicate Contribution:</div>
                            @elseif ($receipt_code->pay_status == 2)
                                <div>Is a Duplicate Contribution of:</div>
                            @endif
                            <table class="table table-striped table-bordered">
                                <tbody>
                                @foreach($duplicates as $duplicate)
                                    <tr>
                                        <td>Receipt No.</td>
                                        <th><a href="{{ route("backend.finance.receipt_code.show", $duplicate->id) }}" target="_blank" class="active_link">{{ $duplicate->receipt->rctno }}</a></th>
                                        <td>Payment Date</td>
                                        <th>{{ $duplicate->receipt->rct_date }}</th>
                                        <td>Amount</td>
                                        <th>{{ $duplicate->receipt->amount_comma }}</th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else

                        @endif
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="underline">Member Count&nbsp;{!! $receipt_code->member_diff_label !!}</div>
                    <br/>
                    @if($receipt_code->hasmemberdiff == 3)
                        {{--not reconciled--}}

                    @else
                        @if ($recent_receipt_code)
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Recent Receipt No.</td>
                                        <th><a href="{{ route("backend.finance.receipt_code.show", $recent_receipt_code->id) }}" target="_blank" class="active_link">{{ $recent_receipt_code->receipt->rctno }}&nbsp;({{ $recent_receipt_code->contrib_month_formatted }})&nbsp;</a></th>
                                        <td>Member Count</td>
                                        <th>{{ $recent_receipt_code->member_count }}</th>
                                        <td>This Member Count</td>
                                        <th>{{ $receipt_code->member_count }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        @else

                        @endif
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
<br/>