{{--Botom Tabs navigation--}}

<div class="basic_nav_pills nav_basic_tab">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.finance.receipt.uploaded_members')</a>
        </li>
    </ul>
    <div class="nav_tab_contain tab-content">
        <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <table class="display" cellspacing="0" width="100%" id ="uploaded_members_table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.table.employee')</th>
                            @permission("view_employee_salary", 1)
                                <th>@lang('labels.backend.table.contrib_amount')</th>
                                <th>@lang('labels.backend.table.salary')</th>
                                <th>@lang('labels.backend.table.grosspay')</th>
                            @endauth
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@push('contribution-tracks-script-end')

<script  type="text/javascript">
   $(function() {
       var url = "{!! url("/") !!}";
       let $cols;
       @permission("view_employee_salary", 1)
           $cols = [
               { data: 'employee' , name: 'employee', searchable: false },
               { data: 'member_amount_formatted' , name: 'employee_amount', searchable: false },
               { data: 'basicpay_formatted', name: 'salary', searchable: false },
               { data: 'grosspay_formatted', name: 'grosspay', searchable: false },
               { data: 'firstname', name: 'employees.firstname', visible: false },
               { data: 'middlename', name: 'employees.middlename', visible: false },
               { data: 'lastname', name: 'employees.lastname', visible: false },
           ];
       @else
           $cols = [
               { data: 'employee' , name: 'employee', searchable: false },
               { data: 'firstname', name: 'employees.firstname', visible: false },
               { data: 'middlename', name: 'employees.middlename', visible: false },
               { data: 'lastname', name: 'employees.lastname', visible: false },
           ];

       @endauth
        $('#uploaded_members_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.receipt_code.member_contributions', $receipt_code->id) !!}',
                type : 'get'
            },
            columns: $cols,
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/compliance/employee/profile/" + aData['employee_id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            },
        } );
    });
</script>;

@endpush