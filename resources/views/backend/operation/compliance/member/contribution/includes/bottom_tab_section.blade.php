{{--Botom Tabs navigation--}}

    <div class="basic_nav_pills nav_basic_tab">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.finance.receipt.contribution_track')</a>
            </li>
        </ul>
        <div class="nav_tab_contain tab-content">
            <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                <br/>
                {{--main contribution tracks--}}

                    <div class="col-md-12">
                        <div class="pull-right" >
                            {{--add new contribution track--}}
                            <span>
				                       {!! link_to_route('backend.compliance.contribution_track.create_new',trans('buttons.general.add_track'), [$receipt_code->id],['id'=> 'add_track', 'class'=>'icon fa fa-plus-circle', 'class' => 'btn btn-primary site-btn save_button', ]) !!}
						</span>
                        </div>
                    </div>



                <div class="row">
                    <div class="col-md-12">
                        @include('backend.operation.compliance.member.contribution.includes.contribution_tracks')
                    </div>
                </div>
            </div>
        </div>


    </div>

