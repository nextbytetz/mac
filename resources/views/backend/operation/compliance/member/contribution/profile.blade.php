@extends('layouts.backend.main', ['title' => trans('labels.backend.member.contribution'), 'header_title' => trans('labels.backend.member.contribution')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/global/plugins/jquery-powertip/css/jquery.powertip.min.css") }}
@endpush

@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">
        {!! Form::model($receipt_code, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
        <div class="col-md-9 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <span>{!! $receipt_code->workflow_status_label !!} </span>
                <strong> ID&nbsp;:&nbsp; {!! Form::label( 'name', $receipt_code->id, [ 'id'=> 'id']) !!}</strong>
                <small>
                    @lang('labels.backend.finance.receipt.receipt_no') : {!! Form::label( 'rctno', ($receipt_code->receipt()->count()) ? $receipt_code->receipt->rctno : ' ', [ 'class'=> 'underline']) !!}
                </small>

            </h5>
        </div>

        {{--Main Content--}}
        {{--menu bar--}}
        <div class = "row">
            <div class="col-md-12">
                {{--menu Bar--}}
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="pull-right" >
                                {{--upload contribution--}}
                                <span>
                                    @if ($receipt_code->allowUploadContributionFile() == 1)
                                        <a href="{!! route('backend.finance.receipt_code.linked_file', $receipt_code->id) !!}"
                                           class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-upload"></i>&nbsp;@lang('buttons.general.upload_linked_file')
                                        </a>
                                    @endif
						        </span>
                                {{--load contribution--}}
                                <span>
                                      @if ($receipt_code->allowUploadContribution() == 1)
                                        <a href="{!! route('backend.finance.contribution.register', $receipt_code->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-check-circle"></i>&nbsp;@lang('buttons.general.load_contribution')
                                        </a>
                                    @endif
						        </span>
                                {{--close--}}
                                {{--Edit Contribution--}}
                                {{--<span>
						        <a href="{!! route('backend.finance.receipt_code.edit', $receipt_code->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit" aria-hidden="true"></i>&nbsp;@lang('buttons.general.crud.edit')
                                </a>
						        </span>--}}
                                <span>
						        <a href="{!! route('backend.finance.receipt.profile_by_request', $receipt_code->receipt_id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')
                                </a>
						        </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div>&nbsp;</div>

        {{--Contribution summary--}}
        <div class = "row">
            <div class="col-md-12">
                @include('backend.operation.compliance.member.contribution.includes.summary')
            </div>
        </div>


        {{--Uploaded Members--}}
        <div class = "row">
            <div class="col-md-12">
                @include('backend.operation.compliance.member.contribution.includes.uploaded_members')
            </div>
        </div>


        <div>&nbsp;</div>
        {{--Bottom tab navigation--}}

        <div class = "row">
            <div class="col-md-12">
                {{--main contribution tracks--}}
                {{--<div class="row">--}}
                @include('backend.operation.compliance.member.contribution.includes.bottom_tab_section')
                {{--</div>--}}
            </div>
        </div>

        {{--</div>--}}

        {{--</div>--}}

        {{--</div>--}}


        {!! Form::close() !!}
    </div>







@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/core.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/csscoordinates.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/displaycontroller.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/placementcalculator.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/tooltipcontroller.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/utility.js") }}
{{ Html::script(asset_url(). "/global/js/global/tooltip.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/contribution-progress.js") }}

@stack('contribution-tracks-script-end')

{{--<script  type="text/javascript">--}}

{{--$(function() {--}}
{{--$('#workflow-table').DataTable({--}}
{{--processing: true,--}}
{{--serverSide: true,--}}
{{--stateSave: true,--}}
{{--stateSaveCallback: function (settings, data) {--}}
{{--localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));--}}
{{--},--}}
{{--stateLoadCallback: function (settings) {--}}
{{--return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));--}}
{{--},--}}
{{--ajax:{--}}
{{--url : '{!! route('backend.workflow.wf_tracks.get) !!}',--}}
{{--type : 'get'--}}


{{--},--}}
{{--columns: [--}}
{{--{ data: 'user_id' , name: 'user_id'},--}}
{{--{ data: 'status', name: 'status'},--}}
{{--{ data: 'level_id', name: 'level_id'},--}}
{{--{ data: 'comments', name: 'comments' },--}}
{{--{ data: 'receive_date', name: 'receive_date' },--}}
{{--{ data: 'forward_date', name: 'forward_date' },--}}
{{--{ data: 'action', name: 'action' }--}}
{{--]--}}

{{--});--}}

{{--});--}}
{{--</script>;--}}


@endpush
