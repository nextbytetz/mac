{{-- {{dump($agent_summary)}} --}}
@extends('layouts.backend.main', ['title' => "Agents Employers", 'header_title' => "Employers managed online by this agent" ])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
<table class="table table-bordered">
      <tr>
        <thead rowspan="6"><h3>Agent Details Summary</h3></th>   

      </tr>
     <tr>
        <td>Agent Name:</td>
                <td>{{$agent_summary->agent_name}}</td>
                <td>Email:</td>
                 <td>{{$agent_summary->email}}</td>
                <td>Phone</td>
               
                <td>{{$agent_summary->phone}}</td>
              
            </tr>
</table>
    {{--datatable--}}
   {{--  {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!} --}}
    <br>
   <table class="table table-bordered">
    <tr>
        <thead rowspan="6"><h3>List of employers</h3></th>   

      </tr>

    <thead class="thead-dark">
        <tr>
            <th scope="col">EmployerName:</th>
            <th scope="col">Registration Number:</th>
             <th scope="col">TIN:</th>
             <th scope="col">Email:</th>
              <th scope="col">Phone:</th>
            <th scope="col">RegionName:</th>
        </tr>
    </thead>

    <tbody>
        @foreach($agents as $agent)
            <tr>
                <td>{{$agent->employer_name}}</td>
                <td>{{$agent->reg_no}}</td>
                <td>{{$agent->tin}}</td>
                <td>{{$agent->employer_email}}</td>
                <td>{{$agent->employers_phone}}</td>
                <td>{{$agent->region_name}}</td>
            </tr>
        @endforeach
    </tbody>

</table>

@stop

@push('after-script-end')

   {{--  {!! $dataTable->scripts() !!} --}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script type="text/javascript">

        // main function search options
        $(function () {
            $(".search-select").select2();
        });

    </script>


@endpush
