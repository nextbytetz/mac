@extends('layouts.backend.main', ['title' => trans('labels.backend.member.contribution'), 'header_title' => trans('labels.backend.member.contribution')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/global/plugins/jquery-powertip/css/jquery.powertip.min.css") }}
@endpush

@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">
        {!! Form::model($legacy_receipt_code, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
        <div class="col-md-9 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <span>{!! $legacy_receipt_code->workflow_status_label !!} </span>
                <strong> ID&nbsp;:&nbsp; {!! Form::label( 'name', $legacy_receipt_code->id, [ 'id'=> 'id']) !!}</strong>
                <small>
                    @lang('labels.backend.finance.receipt.receipt_no') : {!! Form::label( 'rctno', ($legacy_receipt_code->legacyReceipt()->count()) ? $legacy_receipt_code->legacyReceipt->rctno : ' ', [ 'class'=> 'underline']) !!}
                </small>

            </h5>
        </div>

        {{--Main Content--}}
        {{--menu bar--}}
        <div class = "row">
            <div class="col-md-12">
                {{--menu Bar--}}
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="pull-right" >
                                {{--upload contribution--}}
                                <span>
                                    @if ($legacy_receipt_code->allowUploadContributionFile() == 1)
                                        <a href="{!! route('backend.finance.legacy_receipt_code.linked_file', $legacy_receipt_code->id) !!}"
                                           class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-upload"></i>&nbsp;@lang('buttons.general.upload_linked_file')
                                        </a>
                                    @endif
						        </span>
                                {{--load contribution--}}
                                <span>
                                      @if ($legacy_receipt_code->allowUploadContribution() == 1)
                                        <a href="{!! route('backend.finance.contribution_legacy.register', $legacy_receipt_code->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-check-circle"></i>&nbsp;@lang('buttons.general.load_contribution')
                                        </a>
                                    @endif
						        </span>

                                {{--close--}}
                                <span>
						        <a href="{!! route('backend.finance.legacy_receipt.profile', $legacy_receipt_code->legacy_receipt_id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')
                                </a>
						        </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div>&nbsp;</div>
        {{--Contribution summary--}}
        <div class = "row">
            <div class="col-md-12">
                @include('backend.operation.compliance.member.contribution_legacy.includes.summary')

            </div>

        </div>

        {{--Uploaded Members--}}
        <div class = "row">
            <div class="col-md-12">
                @include('backend.operation.compliance.member.contribution_legacy.includes.uploaded_members')
            </div>
        </div>
        {{--</div>--}}

        {{--</div>--}}

        {{--</div>--}}

        {{--</div>--}}


        {!! Form::close() !!}
    </div>







@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/core.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/csscoordinates.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/displaycontroller.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/placementcalculator.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/tooltipcontroller.js") }}
{{ Html::script(asset_url(). "/global/plugins/jquery-powertip/src/utility.js") }}
{{ Html::script(asset_url(). "/global/js/global/tooltip.js") }}
{{ Html::script(asset_url(). "/nextbyte/js/backend/contribution-progress.js") }}

@stack('uploaded-members-script-end')
{{--<script  type="text/javascript">--}}

{{--$(function() {--}}
{{--$('#workflow-table').DataTable({--}}
{{--processing: true,--}}
{{--serverSide: true,--}}
{{--stateSave: true,--}}
{{--stateSaveCallback: function (settings, data) {--}}
{{--localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));--}}
{{--},--}}
{{--stateLoadCallback: function (settings) {--}}
{{--return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));--}}
{{--},--}}
{{--ajax:{--}}
{{--url : '{!! route('backend.workflow.wf_tracks.get) !!}',--}}
{{--type : 'get'--}}


{{--},--}}
{{--columns: [--}}
{{--{ data: 'user_id' , name: 'user_id'},--}}
{{--{ data: 'status', name: 'status'},--}}
{{--{ data: 'level_id', name: 'level_id'},--}}
{{--{ data: 'comments', name: 'comments' },--}}
{{--{ data: 'receive_date', name: 'receive_date' },--}}
{{--{ data: 'forward_date', name: 'forward_date' },--}}
{{--{ data: 'action', name: 'action' }--}}
{{--]--}}

{{--});--}}

{{--});--}}
{{--</script>;--}}


@endpush
