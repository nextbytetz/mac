

{{--[Includes => receipt_code profile] Contribution Summary table--}}

<div class="row">
    {{--left contrib ummary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                    <td width="180px">@lang('labels.backend.finance.receipt.received_from')</td>
                    <th>{!! Form::label( 'payer', $legacy_receipt_code->legacyReceipt->payer, [ 'id'=> 'payer']) !!}</th>
                </tr>

                <tr>
                    <td>@lang('labels.backend.finance.receipt.contrib_month')</td>
                    <th>{!! Form::label( 'contrib_month', $legacy_receipt_code->contrib_month_formatted, [ 'id'=>
                'contrib_month']) !!}</th>
                </tr>

                <tr>
                    <td>@lang('labels.backend.table.receipt.contrib_amount')</td>
                    <th>{!! Form::label( 'contrib_amount', $legacy_receipt_code->amount_formatted, [ 'id'=> 'contrib_amount']) !!}</th>
                </tr>
                <tr>
                    <td>@lang('labels.backend.table.linked_file')</td>
                    <th>{!! $legacy_receipt_code->linked_file_status_label !!} @if ($legacy_receipt_code->error == 1)  {!! link_to_route('backend.finance.legacy_receipt_code.download_error', trans('labels.general.download_error'), [$legacy_receipt_code->id], ['class' => 'btn btn-secondary btn-sm pull-right text-red']) !!} @endif &nbsp;&nbsp; @if (!is_null($legacy_receipt_code->upload_error)) <i class='btn btn-secondary btn-sm pull-right text-red'  data-toggle="popover" data-placement="bottom" data-content="{!! $legacy_receipt_code->upload_error !!}">@lang('labels.general.error')</i> @endif </th>
                </tr>

                </tbody></table>

        </div>
    </div>

    {{--right receipt summary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                    <td width="180px">@lang('labels.backend.finance.receipt.receipt_no')</td>
                    <th>{!! Form::label( 'rctno', $legacy_receipt_code->legacyReceipt->rctno, [ 'id'=> 'rctno']) !!}</th>
                </tr>

                <tr>
                    <td>@lang('labels.backend.finance.receipt.receipt_date')</td>
                    <th>{!! Form::label( 'rct_date', $legacy_receipt_code->legacyReceipt->rct_date, [ 'id'=> 'rct_date']) !!}</th>
                </tr>

                <tr>
                    <td>@lang('labels.backend.finance.receipt.amount')</td>
                    <th>{!! Form::label( 'rct_amount', $legacy_receipt_code->legacyReceipt->amount_formatted, [ 'id'=> 'rct_amount'])
                    !!}</th>
                </tr>
                <tr>
                    <td>@lang('labels.backend.finance.receipt.currency')</td>
                    <th>{!! Form::label( 'currency', ($legacy_receipt_code->legacyReceipt->currency()->count())  ?     $legacy_receipt_code->legacyReceipt->currency->code : ' ' , [ 'id'=> 'currency'])
                    !!}</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>



<div class="basic_nav_pills nav_basic_tab">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            {{--<a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.finance.receipt.contribution_track')</a>--}}
            <legend class="nav-link active" data-toggle="tab">@lang('labels.backend.finance.receipt.analysis')&nbsp;&nbsp;{!! $legacy_receipt_code->analysisStatus() !!}</legend>
        </li>
    </ul>
    <div class="nav_tab_contain tab-content">
        <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
            <div class="row">
                <div class="col-md-12">
{{--                    <legend class="underline">@lang('labels.backend.finance.receipt.analysis')&nbsp;&nbsp;{!! $receipt_code->analysisStatus() !!}</legend>
                    <br/>--}}
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <td width="180px"></td>
                            <td><p class="underline">@lang('labels.backend.finance.receipt.total_contribution')</p></td>
                            <td><p class="underline">@lang('labels.backend.table.member_count')</p></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="180px">@lang('labels.backend.table.registered_data')</td>
                            <td>{!! $legacy_receipt_code->amount_formatted !!}</td>
                            <td>{!! $legacy_receipt_code->member_count !!}</td>
                        </tr>
                        <tr>
                            <td width="180px">@lang('labels.backend.table.imported_data')</td>
                            <td>{!! $legacy_receipt_code->amount_imported_formatted !!}</td>
                            <td>{!! $legacy_receipt_code->total_rows !!}</td>
                        </tr>
                        <tr>
                            <td width="180px">@lang('labels.backend.table.difference')</td>
                            <td><p data-percent-diff = "{!! $legacy_receipt_code->amountDiffPercentLabel() !!}" style="background-color: #{!! blend_hex('ffffff', 'ff2e29', ($legacy_receipt_code->amountDiffPercentLabel() / 100)) !!}; padding: 4px; color : black; border-radius: 4px; text-align: center"><b>{!! $legacy_receipt_code->amountDiff() !!}</b></p></td>
                            <td>{!! $legacy_receipt_code->diff_member !!}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>





@push('contribution_summary-script-end')

<script  type="text/javascript">

</script>;

@endpush