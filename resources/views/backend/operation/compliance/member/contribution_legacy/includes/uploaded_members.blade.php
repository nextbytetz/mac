{{--Botom Tabs navigation--}}

<div class="basic_nav_pills nav_basic_tab">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.finance.receipt.uploaded_members')</a>
        </li>
    </ul>
    <div class="nav_tab_contain tab-content">
        <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <table class="display" cellspacing="0" width="100%" id ="uploaded_members_table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.table.employee')</th>
                            <th>@lang('labels.backend.table.contrib_amount')</th>
                            <th>@lang('labels.backend.table.grosspay')</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@push('uploaded-members-script-end')

<script  type="text/javascript">
   $(function() {
       var url = "{!! url("/") !!}";
        $('#uploaded_members_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.finance.legacy_receipt_code.member_contributions', $legacy_receipt_code->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'employee' , name: 'employee' },
                { data: 'member_amount_formatted' , name: 'employee_amount' },
                { data: 'grosspay_formatted', name: 'grosspay' },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/compliance/employee/profile/" + aData['employee_id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            },
        } );
    });
</script>;

@endpush