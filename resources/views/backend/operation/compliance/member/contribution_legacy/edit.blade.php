@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_contribution'), 'header_title' => trans('labels.backend.member.edit_contribution')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                ID&nbsp;:&nbsp; {!! Form::label( 'name', $legacy_receipt_code->id, [ 'id'=> 'id']) !!} &nbsp;&nbsp;&nbsp;
                <small >
                    @lang('labels.backend.finance.receipt.receipt_no') : {!! Form::label( 'rctno', ($legacy_receipt_code->legacyReceipt()->count()) ? $legacy_receipt_code->legacyReceipt->rctno : ' ', [ 'class'=> 'underline']) !!} &nbsp;&nbsp;
                </small>
                <small>
                    @lang('labels.backend.finance.receipt.amount') : {!! Form::label( 'amount', $legacy_receipt_code->amount_formatted, [ 'class'=> 'underline']) !!} &nbsp;&nbsp;
                </small>
                <small>
                    @lang('labels.backend.table.receipt.contrib_month') : {!! Form::label( 'amount', $legacy_receipt_code->contrib_month_formatted, [ 'class'=> 'underline']) !!}
                </small>
            </h5>
            <legend>
            </legend>
        </div>
    </div>
    <br/>
    {!! Form::model($legacy_receipt_code, ['route' => ['backend.finance.legacy_receipt_code.update', $legacy_receipt_code->id], 'name' => 'edit_contribution']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="filed-layout">
                <label class="required">@lang('labels.backend.table.receipt.contrib_month')</label>
                <div class="form-group">
                    <div class="form-inline">
                                    <span>
                                        {!! Form::hidden('receipt_date', 28) !!}
                                    </span>
                                    <span>
                                        {!!  Form::selectMonth('receipt_month',$month, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                                    </span>
                                    <span>
                                        {!!  Form::selectRange('receipt_year', $year_threshold, 2015 , $year, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                                    </span>
                                    <span>
                                        {!! Form::hidden('rct_date') !!}
                                    {!! Form::hidden('today_date', $this_date) !!}
                                    {!! Form::hidden('threshold_date', $contribution_date_threshold) !!}
                                    </span>
                    </div>
                    {!! $errors->first('rct_date', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <hr/>
            <div class="pull-right">
                {!! link_to_route('backend.finance.legacy_receipt_code.show', trans('buttons.general.cancel'), [$legacy_receipt_code->id], ['class' => 'btn btn-danger btn-sm cancel_button']) !!}
                <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.crud.update')" />
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function(){
        $(".search-select").select2({});
        /* start: Submitting Form and perform validation on the server side */
        $('body').on('submit', 'form[name=edit_contribution]', function (e) {
            e.preventDefault();
            var $form = this;
            var $receipt_date = $("input[name=receipt_date]").val();
            var $receipt_month = $("select[name=receipt_month]").val();
            var $receipt_year = $("select[name=receipt_year]").val();
            if ($receipt_date && $receipt_month && $receipt_year) {
                $("input[name=rct_date]").val($receipt_year + '-' + $receipt_month + '-' + $receipt_date);
            } else {
                $("input[name=rct_date]").val("");
            }
            //alert($form);
            $form.submit();
        });
        /* end: Submitting Form and perfom validation on the server side */
    });
</script>
@endpush