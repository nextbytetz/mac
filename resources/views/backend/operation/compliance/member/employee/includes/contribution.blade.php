
{{--Contribution table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="contribution-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.receipt.contrib_month')</th>
                <th>@lang('labels.backend.compliance.basicpay')</th>
                <th>@lang('labels.backend.compliance.grosspay')</th>
                <th>@lang('labels.backend.compliance.member_amount')</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="3" style="text-align:right">Total:</th>
                <th></th>
            </tr>
            </tfoot>
        </table>
        <br/>
        <legend class="grey_modal" >@lang('labels.backend.member.employer.legacy_contribution')</legend>
        <br/>
        <table class="display" cellspacing="0" width="100%" id = "legacy-contribution-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.receipt.contrib_month')</th>
                <th>@lang('labels.backend.compliance.basicpay')</th>
                <th>@lang('labels.backend.compliance.grosspay')</th>
                <th>@lang('labels.backend.compliance.member_amount')</th>
            </tr>
            </thead>
            <tfoot>
            {{--<tr>
                <th colspan="3" style="text-align:right">Total:</th>
                <th></th>
            </tr>--}}
            </tfoot>
        </table>

    </div>
</div>


@push('contribution-script-end')

<script  type="text/javascript">
    $("#tab_2_header").one("click", function(){
        $(function() {

            $('#legacy-contribution-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employee.legacy_contributions', $employee->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'contribution_month_formatted' , name: 'contrib_month' },
                    { data: 'salary', name: 'salary' ,  orderable : false, searchable : false},
                    { data: 'gross_pay_formatted', name: 'grosspay', orderable : false, searchable : false},
                    { data: 'member_amount', name: 'employee_amount', orderable : false, searchable : false}
                ]
            });

            {{--var url = "{!! url("/") !!}";--}}
            $('#contribution-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employee.contributions', $employee->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'paydate_formatted' , name: 'contrib_month' },
                    { data: 'basicpay', name: 'basicpay' ,orderable : false, searchable : false },
                    { data: 'grosspay', name: 'grosspay' , orderable : false, searchable : false},
                    { data: 'member_amount', name: 'member_amount', orderable : false, searchable : false}

                ],
//                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = url + "/finance/receipt_code/" + aData['id'];
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//                },

                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    function commaSeparateNumber(val){
                        while (/(\d+)(\d{3})/.test(val.toString())){
                            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                        return val ;
                    }

                    $( api.column( 3 ).footer() ).html(
                        ''+ commaSeparateNumber(total.toFixed(2))
                    );
                }
            } );

        });

    });
</script>;

@endpush