
{{--interest-writeoff-overview-table--}}

<legend class="grey_modal" >Manual Notifications Overview</legend>
<table class="display" cellspacing="0" width="100%" id ="manual-notification-overview-table">
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.backend.table.case_no')</th>
        <th>@lang('labels.backend.table.death_date')</th>
        <th>@lang('labels.backend.table.death_cause')</th>
        <th>@lang('labels.backend.table.report_date')</th>
    </tr>
    </thead>

</table>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#manual-notification-overview-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                paging: false,
                info:false,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employee.get_manual_notifications', $employee->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'status', name: 'status' },
                    { data: 'case_no' , name: 'case_no' },
                    { data: 'incident_date' , name: 'incident_date' },
                    { data: 'reporting_date' , name: 'reporting_date' },
                    { data: 'reporting_date' , name: 'reporting_date' }

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url + "/claim/manual_notification/"  + aData['id'] + '/dashboard';

                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }


            } );
        });
    </script>;

@endpush