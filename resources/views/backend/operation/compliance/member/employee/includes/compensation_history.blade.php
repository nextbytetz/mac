
{{--Compensation history table--}}
{{--<div>&nbsp;</div>--}}
<div class="col-md-12" >
    <div class="pull-right">
        <a href="{!! route('backend.compliance.employee.create_old_compensation', $employee->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>

         </div>
</div>


<div class = "row">
    <div class="col-md-12" >

        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="compensation-history-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.incident_type')</th>
                <th>@lang('labels.backend.table.who_paid')</th>
                <th>@lang('labels.backend.table.payment_date')</th>
                <th>@lang('labels.backend.table.amount_paid')</th>

            </thead>

        </table>
    </div>
</div>


@push('compensation-history-script-end')

<script  type="text/javascript">
    $("#tab_4_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#compensation-history-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                info: false,
                paging: false,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employee.get_employment_old_compensations', $employee->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'incident_type_id' , name: 'incident_type_id' },
                    { data: 'who_paid', name: 'who_paid' },
                    { data: 'payment_date', name: 'payment_date' },
                    { data: 'amount', name: 'amount' }

                                 ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url + "/compliance/employee/old_compensation/" + aData['id']+ "/edit";
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }


            } );

        });

    });
</script>;

@endpush