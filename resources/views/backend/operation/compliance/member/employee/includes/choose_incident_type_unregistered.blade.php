{!! Form::open(['route' => ['backend.claim.notification_report.register', 0 ], 'method'=>'get', 'id' => 'create']) !!}
<div class="row">

    <div class="col-md-12">
        <h5 class="client-title">
            <i class="icon-circle"></i>
            <!-- Employer header detail -->
            <strong>Unregistered Employee,</strong>

            <small >

            </small>
        </h5>
        <legend></legend>
    </div>
</div>

<div>&nbsp;</div>

{{--incident Type--}}
<div class="row">
    <div class="col-md-8">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.incident_type'):</label><span class="required_asterik">*</span></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('incident_type_id', $incident_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                    {!! $errors->first('incident_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::hidden("employer", session('employer')) !!}
</div>

{{--Buttons--}}
<div class="row">
    <div class="col-md-8" class="form-inline" >
        <div class="element-form">
            <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
            <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                <div class="pull-right">
                    {!! link_to_route('backend.claim.notification_report.choose_employee', __('buttons.general.cancel'), [], ['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button']) !!}

                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                </div>
            </div>
        </div>
    </div>
</div>


{!! Form::close() !!}
{{--</section>--}}