
{{--interest-writeoff-overview-table--}}

<legend class="grey_modal" >@lang('labels.backend.legend.disease_overview')</legend>
<table class="display" cellspacing="0" width="100%" id ="disease-overview-table">
    <thead>
        <tr>
            <th></th>
            <th>@lang('labels.backend.table.case_no')</th>
            <th>@lang('labels.backend.table.disease_name')</th>
            <th>@lang('labels.backend.table.diagnosis_date')</th>
            <th>@lang('labels.backend.table.report_date')</th>
        </tr>
    </thead>

</table>

@push('disease-overview-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#disease-overview-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employee.get_disease_notifications', $employee->id) !!}',
                type : 'get'
            },
            columns: [
            { data: 'status', name: 'status' },
            { data: 'odsno' , name: 'odsno' },
            { data: 'name' , name: 'name' },
            { data: 'diagnosis_date' , name: 'diagnosis_date' },
            { data: 'reporting_date' , name: 'reporting_date' }

            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {

                    if (aData['is_accrued'] == true && aData['accrued_status'] == 1) {
                        document.location.href = url + "/claim_accrual/profile/"  + aData['notification_report_id'];
                    }else{
                        document.location.href = url + "/claim/notification_report/profile/"  + aData['notification_report_id'];
                    }

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }


        } );
    });
</script>;

@endpush