
{{--dependents table--}}
<div class="col-md-12" >
    <div class="pull-right">
        <a href="{!! route('backend.compliance.dependent.create_new', $employee->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
    </div>
</div>


<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="dependents-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.name')</th>
                <th>@lang('labels.backend.table.relation')</th>
                <th>@lang('labels.backend.table.dob')</th>
                <th>@lang('labels.backend.table.bank')</th>
                <th>@lang('labels.backend.table.branch')</th>
                <th>@lang('labels.backend.table.accountno')</th>
                <th>@lang('labels.backend.table.survivor_pension_percent')</th>
                <th>@lang('labels.backend.table.survivor_gratuity_percent')</th>
                <th>@lang('labels.backend.table.funeral_grant_percent')</th>
                <th>Other Dependency Type</th>
                {{--<th>Firstname</th>--}}
            </tr>
            </thead>
        </table>
    </div>
</div>


@push('dependents-script-end')

<script  type="text/javascript">
    $("#tab_5_header").one("click", function(){
        $(function() {
            var url = "{!! url("/") !!}";
            $('#dependents-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employee.get_dependents', $employee->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'name' , name: 'name', orderable:true, searchable:true },
                    { data: 'dependent_type_id', name: 'dependent_type_id',  orderable:false, searchable:false },
                    { data: 'dob', name: 'dob',  orderable:false, searchable:false },
                    { data: 'bank_id', name: 'bank_id', orderable:false, searchable:false},
                    { data: 'bank_branch_id', name: 'bank_branch_id', orderable:false, searchable:false},
                    { data: 'accountno', name: 'accountno' , orderable:false, searchable:false},
                    { data: 'survivor_pension_percent', name: 'survivor_pension_percent', orderable:false, searchable:false},
                    { data: 'survivor_gratuity_percent', name: 'survivor_gratuity_percent', orderable:false, searchable:false},
                    { data: 'funeral_grant_percent', name: 'funeral_grant_percent', orderable:false, searchable:false},
                    { data: 'other_dependency_type', name: 'other_dependency_type', orderable:false, searchable:false},
                    // { data: 'firstname' , name: 'firstname', orderable:true, searchable:true, visible:false }
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url + "/compliance/employee/dependent/" + aData['dependent_id'] + "/edit/" +  aData['employee_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                },

                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    function commaSeparateNumber(val){
                        while (/(\d+)(\d{3})/.test(val.toString())){
                            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                        return val ;
                    }

                    $( api.column( 3 ).footer() ).html(
                        ''+ commaSeparateNumber(total.toFixed(2))
                    );
                }
            } );

        });

    });
</script>;

@endpush