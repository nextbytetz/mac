@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance.employee'), 'header_title' => trans('labels.backend.compliance.employee')])

@include('backend.includes.datatable_assets')

@section('content')

<div class = "row">
    {{--{!! Form::model($employee, ['route' => ['backend.compliance.employee.profile', $employee->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
    <div class="col-md-8 col-sm-8">
        <h5 class="client-title">
            <i class="icon-circle"></i>
            <!-- Employer header detail -->
            <strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " . $employee->lastname , [ 'id'=> 'name']) !!}</strong>

            <small>
                @lang('labels.backend.table.member_#'): {!! Form::label( 'reg_no', $employee->memberno, [ 'id'=> 'reg_no']) !!}
            </small>
        </h5>
    </div>
</div>
{{--Tabs navigation--}}
<div class = "row">
    <div class="col-md-12">

        <div class="basic_nav_pills nav_basic_tab">
            <ul class="nav nav-tabs">
                {{--General--}}
                <li class="nav-item">
                    <a class="nav-link active" href="#general"
                    data-toggle="tab">@lang('labels.general.general')
                </a>
            </li>
            @permission("view_employee_salary", 1)
            {{--contributions--}}
            <li class="nav-item">
                <a class="nav-link"  id = "tab_2_header"  href="#contribution" data-toggle="tab">@lang('labels.general.contributions')</a>
            </li>
            @endauth
            {{--employement history--}}
            <li class="nav-item">
                <a class="nav-link" id = "tab_3_header"
                href="#history" data-toggle="tab">@lang('labels.backend.compliance.employment_history')</a>
            </li>
            {{--compensation history--}}
            <li class="nav-item">
                <a class="nav-link" id = "tab_4_header"
                href="#compensation" data-toggle="tab">@lang('labels.backend.compliance.compensation_history')</a>
            </li>
            {{--dependants--}}
            <li class="nav-item">
                <a class="nav-link" id = "tab_5_header"
                href="#dependents" data-toggle="tab">@lang('labels.backend.compliance.dependants')</a>
            </li>

            @if($employee->is_removed)
            <li class="nav-item">
                <a class="nav-link" id = "tab_5_header"
                href="#removed" data-toggle="tab">Removal History</a>
            </li>
            @endif

        </ul>
        <div class="nav_tab_contain tab-content">

            <div id="general" class="nav_tab_pane tab-pane active in">
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >

                            <div class="pull-right" >
                                {{--edit--}}
                                {{--<span>--}}
                                    {{--<a href="{!! route('backend.finance.currency.create') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>--}}
                                {{--</span>--}}
                                {{--add employee--}}
                                {{--<span>--}}
                                    {{--<a href="{!! route('backend.finance.currency.create') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-user-plus"></i>&nbsp;@lang('buttons.backend.member.employer.add_employee')</a>--}}
                                {{--</span>--}}



                                {{--new notification--}}
                                <span>
                                    <a href="{!! route('backend.compliance.employee.choose_incident_type',
                                    $employee->id)
                                    !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('buttons.backend.member.new_notification')</a>
                                </span>



                                <span>
                                    <a href="{!! route('backend.compliance.employee.add_contribution', $employee->id)
                                    !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-money"></i>&nbsp;@lang('buttons.backend.member.employee.add_contribution')</a>
                                </span>


                                {{--modify--}}
                                <span>
                                    <a href="{!! route('backend.compliance.employee.edit', $employee->id)
                                    !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pencil-square-o"></i>&nbsp;@lang('buttons.backend.member.employee.edit')</a>
                                </span>


                                {{--online approval--}}

                                {{--<span>--}}
                                    {{--<a href="{!! route('backend.compliance.employee.profile',--}}
                                    {{--$employee->id)--}}
                                    {{--!!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-envelope-o"></i>&nbsp;@lang('buttons.general.online_approval')</a>--}}
                                {{--</span>--}}

                                {{--edit--}}
                                {{--<span>--}}
                                    {{--<a href="{!! route('backend.finance.currency.create') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.crud.edit')</a>--}}
                                {{--</span>--}}


                                {{--close--}}
                                <span>
                                  <a href="{!! route('backend.compliance.employee.index') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
                              </span>


                          </div>
                      </div>
                  </div>
              </div>


              {{--main tab content--}}
              <div class = "row">
                <div class="col-md-12">

                    <div class="col-md-9">
                        {{--accident report overview--}}
                        @include('backend.operation.compliance.member.employee.includes.accident_overview')
                        {{--disease adjust overview--}}
                        <div>&nbsp;</div>
                        @include('backend.operation.compliance.member.employee.includes.disease_overview')
                        <div>&nbsp;</div>
                        @include('backend.operation.compliance.member.employee.includes.death_overview')
                        @if($employee->manualNotificationReports()->count() > 0)
                        <div>&nbsp;</div>
                        @include('backend.operation.compliance.member.employee.includes.manual_notification_overview')
                        @endif
                    </div>

                    <div class="col-md-3">
                        {{--sidebar summary--}}
                        @include('backend.operation.compliance.member.employee.includes.sidebar_summary')
                    </div>
                </div>
            </div>
        </div>

        @permission("view_employee_salary", 1)
        <div id="contribution" class="nav_tab_pane tab-pane">
            {{--contributions--}}
            @include('backend.operation.compliance.member.employee.includes.contribution')
        </div>
        @endauth

        <div id="history" class="nav_tab_pane tab-pane">

            {{--add employement history--}}


            {{--employment history--}}
            <div class="col-md-12" >
                <div class="pull-right">
                    <a href="{!! route('backend.compliance.employee.create_employment_history', $employee->id) !!}"  class="btn site-btn save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
                </div>
            </div>
            @include('backend.operation.compliance.member.employee.includes.employment_history')
        </div>
        <div id="compensation" class="nav_tab_pane tab-pane">
            {{--old compensations--}}
            @include('backend.operation.compliance.member.employee.includes.compensation_history')
        </div>
        <div id="dependents" class="nav_tab_pane tab-pane">
            {{--dependents--}}
            @include('backend.operation.compliance.member.employee.includes.dependents')
        </div>
        @if($employee->is_removed)
        <div id="removed" class="nav_tab_pane tab-pane">
            {{-- @include('backend.operation.compliance.member.employee.includes.dependents') --}}
            <div class = "row">
                <div class="col-md-12" >

                    <table class="display" cellspacing="0" width="100%" id ="remove-table">
                        <thead>
                            <tr >
                                <th>Employer</th>
                                <th>Date Removed</th>
                                <th>Removal Reason</th>
                                {{-- <th>Attachment</th> --}}
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
        @endif
    </div>
</div>
</div>

</div>

{{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')

@stack('contribution-script-end')
@stack('accident-overview-script-end')
@stack('disease-overview-script-end')
@stack('death-overview-script-end')
@stack('employment-history-script-end')
@stack('compensation-history-script-end')
@stack('dependents-script-end')
<script  type="text/javascript">


    $(function () {

        $('#remove-table').DataTable({
           processing: true,
           serverSide: true,
           stateSave: true,
           stateSaveCallback: function (settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
        },
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
        },
        ajax:{
            url : '{!! route('backend.compliance.employee.get_removals',$employee->id) !!}',
            type : 'get',
            data: function ($data) {
            }
        },
        columns: [
        { data: 'employer', name: 'employers.name', orderable : false, searchable : false},
        { data: 'removed_on', name: 'employee_removal_history.created_at', orderable : true, searchable : false},
        { data: 'remove_reason', name: 'employee_removal_history.remove_reason' ,orderable : false, searchable : false},
        // { data: 'remove_attachment', name: 'employee_employer.remove_attachment' ,orderable : false, searchable : false},
        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).click(function() {
            }).hover(function() {
                $(this).css('cursor','pointer');
            }, function() {
                $(this).css('cursor','auto');
            });
        }
    });


        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });
</script>;

@endpush
