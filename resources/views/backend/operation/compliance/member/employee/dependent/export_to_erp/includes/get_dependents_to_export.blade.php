
<div class = "row">
    <div class="col-md-12" >

        <table class="display" cellspacing="0" width="100%" id ="dependents-table">
            <thead>
            <tr >
                {{--<th>Filename</th>--}}
                <th>Fullname</th>
                <th>DOB</th>
                <th>Employee</th>
                <th>Relation</th>

            </tr>
            </thead>
        </table>

    </div>
</div>




@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#dependents-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.dependent.get_dependent_for_export_erp') !!}',
                    type : 'post'
                },
                columns: [
                    // { data: 'filename', name: 'notification_reports.filename'},
                    { data: 'fullname' , name: 'firstname', orderable : true, searchable : true},
                    { data: 'dob_formatted' , name: 'dob', orderable : true, searchable : true},
                    { data: 'employee_name' , name: 'employees.firstname', orderable : true, searchable : true},
                    { data: 'type_name', name: 'type_name',  orderable : false, searchable : false},
                            /*invisible fields*/
                    { data: 'middlename', name: 'middlename' ,  orderable : false, searchable : true, visible: false},
                    { data: 'lastname', name: 'lastname' ,  orderable : false, searchable : true,visible: false },
                    { data: 'emp_firstname', name: 'employees.firstname' ,  orderable : false, searchable : true, visible: false},
                    { data: 'emp_middlename', name: 'employees.middlename' ,  orderable : false, searchable : true, visible: false},
                    { data: 'emp_lastname', name: 'employees.lastname' ,  orderable : false, searchable : true,visible: false },
                    // { data: 'filename', name: 'manual_notification_reports.case_no', orderable : false, searchable : true,visible: false},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/dependent/"+ "create_erp_supplier/" + aData['pivot_id']  ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>

@endpush
