
@extends('layouts.backend.main', ['title' => 'New ERP Supplier Registration form', 'header_title' => 'New ERP Supplier Registration form'])


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

@endpush

@section('content')
    <div class="row">


        <div class="col-md-12 col-sm-12">

            {!! Form::open(['route' => 'backend.compliance.dependent.store_erp_supplier','method' => 'post']) !!}


            {!! Form::hidden('dependent_id', $dependent->id) !!}
            {!! Form::hidden('employee_id', $employee->id) !!}
            {!! Form::hidden('supplier_id_no_delimiter',  $dependent->getMemberNoForErpApiWithNoDelimiter($employee->id)) !!}

            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.supplier_name'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('supplier_name', $dependent->name, ['class' => 'form-control' ,'readonly', 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.supplier_number'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('supplier_id', $dependent->getMemberNoForErpApi($employee->id), ['class' => 'form-control' ,'readonly','style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('supplier_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.supplier_tin_number'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('tin', $employer->tin, ['class' => 'form-control' ,'readonly','style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('tin', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.supplier_vendor_site'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('vendor_site_code', '', ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('vendor_site_code', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.supplier_address_line1'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('address_line1', '', ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('address_line1', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>



        <div class="col-md-12 col-sm-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.supplier_address_line2'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('address_line2', '', ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('address_line2', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.finance.receipt.supplier_city'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('city',$region->name, ['class' => 'form-control' ,'readonly', 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('city', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>





        <div class="col-md-12 col-sm-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.supplier_liability_account'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">
                        {!! Form::text('liability_account', '01.001.AB.000000.43181104.000.000', ['class' => 'form-control' ,'readonly', 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                        {!! $errors->first('liability_account', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="element-form">
                <div class="col-md-3 col-sm-12"><p>&nbsp;</p></div>
                <div class="col-md-3 col-sm-6">
                    <div class="pull-left">
                        {!! Form::submit('Create Supplier', ['class' => 'btn btn-primary site-btn save_button']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">



            {!! Form::close() !!}

        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}


    <script type="text/javascript">

        $('document').ready(function(){


            $("#btnSbmt").click(function(e){
                e.preventDefault();
                var frm = $('#frmPay');
                $.fn.serializeIncludeDisabled = function () {
                    let disabled = this.find(":input:disabled").removeAttr("disabled");
                    let serialized = this.serialize();
                    disabled.attr("disabled", "disabled");
                    return serialized;
                };

                swal({
                        html: true,
                        title: "Are you sure want to Generate Control number for <br> TZS. "+$('#contribution').val(),
                        type: "info",
                        showCancelButton: true,
                        confirmButtonClass: "btn-info",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url : '{{route('backend.finance.generatebill')}}',
                                type : 'POST',
                                datatype : 'json',
                                data : frm.serializeIncludeDisabled(),
                                success:function(data){
                                    if (!data.error) {
                                        swal({
                                                title: "Success!",
                                                text: 'Your request has been sent! You will receive sms shortly',
                                                type: "success",
                                                confirmButtonClass: 'btn btn-primary',
                                                confirmButtonText: 'OK!'
                                            },
                                            function(){ location.reload(); });
                                    }
                                    else{
                                        swal({
                                                title: "Error Perfoming Request",
                                                text: data.message,
                                                type: "error",
                                                showCancelButton: false,
                                                buttonsStyling: false,
                                                confirmButtonClass: "btn btn-danger",
                                                confirmButtonText: 'OK!'
                                            },
                                            function(){ location.reload(); });
                                    }
                                }

                            });

                        }
                    });
            });

        });

    </script>





    <script>
        (function (window, document, $) {

        })(window, document, jQuery);
    </script>

@endpush