@extends('layouts.backend.main', ['title' => 'Dependents', 'header_title' => 'Dependents'])

@include('backend.includes.datatable_assets')

@section('content')

    @include('backend/operation/compliance/member/employee/dependent/export_to_erp/includes/get_dependents_to_export')


@stop



