@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_employment_history'), 'header_title' => trans('labels.backend.member.add_employment_history')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($employee,['route' => ['backend.compliance.employee.store_employment_history',$employee->id ],'method'=>'post',
    'name' => 'store_employment_history']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    <div class="row">

        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " .$employee->lastname , [ 'id'=> 'name']) !!}</strong>

                <small >
                    @lang('labels.backend.table.member_#'): {!! Form::label( 'reg_no', $employee->memberno, [ 'id'=> 'reg_no']) !!}
                </small>
            </h5>
            <legend></legend>
        </div>
    </div>

    <div>&nbsp;</div>

    {{--Job Title--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.job_title'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                        {!! Form::select('job_title_id', $job_titles, null , ['style' => 'width:100%', 'placeholder' => ' ','class' => 'form-control search-select']) !!}
                                           {!! $errors->first('job_title_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--Department--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.department'):</label></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','department_name', null, ['class' => 'form-control', ]) !!}

                        {!! $errors->first('department_name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--Employer id from the database--}}
    <div class="row" >
        <div class="col-md-9" >

            <div class="element-form">
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.receipt.employer'):</label><span class="required_asterik">*</span></div>

            </div>
            <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('employer_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control employer-select', 'id' => 'select_employer']) !!}
                    {!! $errors->first('employer_id', '<span class="help-block label label-danger">:message</span>') !!}

                    <label>{!! Form::checkbox( 'other_employer_checkbox',1, false, ['style' => 'width:20px','id' => 'other_employer_checkbox' ]) !!}@lang('labels.general.other')</label>
                </div>
            </div>

        </div>
    </div>


    {{--Other Employer if dont exist in selection above--}}
    <div class="row" id = "other_employer_div" >
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.other_employer'):</label></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','other_employer', null, ['class' => 'form-control',]) !!}
                        {!! $errors->first('other_employer', '<span class="help-block label label-danger">:message</span>') !!}

                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--insurance id from the database--}}
    <div class="row" >
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.insurance'):</label></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('insurance_id', $insurance, null,[ 'style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'select_insurance', 'allowClear'=> true]) !!}
                        {!! $errors->first('insurance_id', '<span class="help-block label label-danger">:message</span>') !!}
                        <label>{!! Form::checkbox( 'other_insurance_checkbox',1, false,['style' => 'width:20px', 'id'=> 'other_insurance_checkbox' ]) !!}@lang('labels.general.other')</label>
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--Other insurance if dont exist in selection above--}}
    <div class="row" id ="other_insurance_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.other_insurance'):</label></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','other_insurance', null, ['class' => 'form-control', ]) !!}

                        {!! $errors->first('other_insurance', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--from date--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.from_date'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                          <span>      {!!  Form::selectRange('min_day',1,31,null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' =>
                         'Day', 'id'=>'min_day']) !!}
                        </span>

                            <span>      {!!  Form::selectMonth('min_month',null, ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month', 'id'=>'min_month']) !!}

                        </span>

                            <span>      {!!  Form::selectRange('min_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(50)->format('Y'),null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year','id'=>'min_year']) !!}
                        </span>
                        </div>
                    </div>
                    {!! Form::hidden('min_date') !!}
                    {!! $errors->first('min_date', '<span class="help-block label
label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>


    {{--To date--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.to_date'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                          <span>      {!!  Form::selectRange('max_day',1,31,null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' =>
                         'Day', 'id'=>'max_day']) !!}
                        </span>

                            <span>      {!!  Form::selectMonth('max_month',null, ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month', 'id'=>'max_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('max_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(50)->format('Y'),null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year', 'id'=>'max_year']) !!}
                        </span>

                        </div>
                    </div>
                    {!! Form::hidden('max_date') !!}
                    {!! $errors->first('max_date', '<span class="help-block label
label-danger">:message</span>') !!}

                </div>
            </div>

        </div>
    </div>



    {{--activity--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.activity_performed'):</label></div>
                <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'activity', null, [ 'id'=> 'activity_performed',  'class' =>'form-control']) !!}
                            {!! $errors->first('activity', '<span class="help-block label
                               label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>





    {{--Buttons--}}
    <div class="row">
        <div class="col-md-9" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                                    {!! link_to('compliance/employee/profile/' . $employee->id . '#history',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();


    $(function(){
        $(".search-select").select2({});

               $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
    });


// main function for employer and insurance options
    $(function () {
        employer_options('other_employer_checkbox','other_employer_div','select_employer');
        insurance_options('other_insurance_checkbox','other_insurance_div','select_insurance');
        $("#other_employer_checkbox").click(function () {
            employer_options('other_employer_checkbox','other_employer_div','select_employer');
        });

        $("#other_insurance_checkbox").click(function () {
            insurance_options('other_insurance_checkbox','other_insurance_div','select_insurance');
        });
    });
// employer options
    function employer_options(other_employer_checkbox,other_employer_div,select_employer) {
        if ($("#" + other_employer_checkbox).is(":checked")) {
            $("#" + other_employer_div).show();
                     $( "#"+select_employer ).prop( "disabled", true );

        } else {
            $("#" + other_employer_div).hide();

            $( "#"+select_employer ).prop( "disabled", false );

        }
    }

// insurance options
    function insurance_options(other_insurance_checkbox,other_insurance_div,select_insurance) {
        if ($("#" + other_insurance_checkbox).is(":checked")) {
            $("#" + other_insurance_div).show();
            $( "#"+select_insurance ).prop( "disabled", true );

        } else {
            $("#" + other_insurance_div).hide();

            $( "#"+select_insurance ).prop( "disabled", false );

        }
    }

    $('body').on('submit', 'form[name=store_employment_history]', function(e) {
        e.preventDefault();
        // Validate date -- from date
        var $day = $('#min_day').val();
        var $month = $('#min_month').val();
        var $year = $('#min_year').val();
        if (($year) && ($month) && ($day)) {
            $('input[name=min_date]').val($year + '-' + $month + '-' + $day);
        }else {
            $("input[name=min_date]").val("");
        }

        // Validate date -- to date
        var $max_day = $('#max_day').val();
        var $max_month = $('#max_month').val();
        var $max_year = $('#max_year').val();
        if (($max_year) && ($max_month) && ($max_day )) {
            $('input[name=max_date]').val($max_year + '-' + $max_month + '-' + $max_day);
        } else{
            $("input[name=max_date]").val("");
        }

        this.submit();

    });



</script>;


@endpush
