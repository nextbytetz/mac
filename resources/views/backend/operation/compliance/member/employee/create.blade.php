@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_employee'), 'header_title' => trans('labels.backend.member.add_employee')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::open(['route' => ['backend.compliance.employee.store' ],'method'=>'post',
    'name' => 'create']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}

    {{--main contante--}}

    {{--firstname--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.firstname'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','firstname', null, ['class' => 'form-control']) !!}

                        {!! $errors->first('firstname', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--middlename--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.middlename'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input('text', 'middlename',null, ['class' => 'form-control', ]) !!}
                        {!! $errors->first('middlename', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--lastname--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.lastname'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','lastname', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('lastname', '<span class="help-block label label-danger">:message</span>') !!}

                    </div>
                </div>
            </div>

            {{--gender--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.gender'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('gender_id', $genders, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('gender_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--DOB--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.dob'):</label><span class="required_asterik">*</span></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                          <span>      {!!  Form::selectRange('dob_day',1,31,null, ['class' => 'form-control search-select','style'=>'width:54px', 'placeholder' =>
                         'Day', 'id'=>'dob_day']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('dob_month',null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month', 'id'=>'dob_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('dob_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(80)->format('Y'),null, ['class' => 'form-control search-select','style'=>'width:64px',
                        'placeholder' =>
                         'Year', 'id'=>'dob_year']) !!}
                        </span>

                        </div>
                        {!! Form::hidden('dob') !!}
                        {!! $errors->first('dob', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--employee category--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.employee_category'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('employee_category_cv_id', $employee_categories, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'employee_category_cv_id']) !!}
                        {!! $errors->first('employee_category_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--   {{--marital status--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.marital_status'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('marital_status_cv_id', $marital_statuses, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'marital_status_cv_id']) !!}
                        {!! $errors->first('marital_status_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--job title id--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.job_title'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('job_title_id', $job_titles, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'job_title_id']) !!}
                        {!! $errors->first('job_title_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--identies--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.identity_type'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('identity_id', $identities, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'identity_id']) !!}
                        {!! $errors->first('identity_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--id_no--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.id_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','id_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('id_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--national id--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.national_id'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','nid', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('nid', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--basicpay--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.basicpay'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','basicpay', null, ['class' => 'form-control money']) !!}
                        {!! $errors->first('basicpay', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>





    {{--allowance--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.allowance'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','allowance', null, ['class' => 'form-control money']) !!}
                        {!! $errors->first('allowance', '<span class="help-block label label-danger">:message</span>')
                         !!}
                    </div>
                </div>
            </div>

            {{--department--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.department'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','department', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('department', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--email--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.general.email'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','email', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--phone--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.phone'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','phone', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>




    {{--fax--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.fax'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','fax', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('fax', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--employer--}}
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.employer'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('employer[]', [], null, ['class' => 'employer-select', 'id' => 'employer_select', 'style' => 'width:100%']) !!}
                        {!! $errors->first('employer', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--no_of_children--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>No of Children:</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','no_of_children', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('no_of_children', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--country/nationality--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Nationality:</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('country_id', $countries, null, ['style' => 'width:100%', 'class' => 'form-control search-select', 'id'=>'country_id']) !!}
                        {!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            {{--passport_no--}}
            <div class="element-form foreigner">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Passport No:</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','passport_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('passport_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--work_permit_no--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form foreigner"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Work Permit No:</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','work_permit_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('work_permit_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--residence_permit_no--}}
            <div class="element-form foreigner" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Residence Permit No:</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group " >
                        {!! Form::input( 'text','residence_permit_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('residence_permit_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--location type id--}}
    <div class="row">
        <div class="col-md-12">

                <div class="element-form"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.location_type'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('location_type_id', $location_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'location_type']) !!}
                        {!! $errors->first('location_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


{{--unsurveyed area--}}
        <div class="element-form unsurveyed" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.unsurveyed_area'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group text_content" >
                    {!! Form::textarea('unsurveyed_area', null, ['class' => 'form-control',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; min-height: 5px !important']) !!}
                    {!! $errors->first('unsurveyed_area', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

        </div>

        </div>


  {{--street--}}
    <div class="row">
        <div class="col-md-12">
            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.street'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','street', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('street', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>


            {{--road--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.road'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group " >
                        {!! Form::input( 'text','road', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('road', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--plotno--}}
    <div class="row">
        <div class="col-md-12">
                  <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.plot_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','plot_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('plot_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            {{--block_no--}}
            <div class="element-form surveyed" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.block_no'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content" >
                        {!! Form::input( 'text','block_no', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('block_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>



        </div>
    </div>


    {{--surveyed extra--}}
    <div class="row">
        <div class="col-md-12">

            <div class="element-form surveyed"  >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.surveyed_extra'):</label></div>
                <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','surveyed_extra', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('surveyed_extra', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


{{--buttons--}}
    <div class="row">
        <div class="col-md-10" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('compliance/employee',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>




    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}
<script  type="text/javascript">

    $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();

    $(function () {
        $(".search-select").select2();
        $(".surveyed").hide();
        $(".unsurveyed").hide();
        //$(".foreigner").hide();

        location_type_option('location_type', 'surveyed','unsurveyed');
        $("#location_type").on('change', function (e) {
            location_type_option('location_type', 'surveyed','unsurveyed');
        });

        $("#country_id").on('change', function (e) {
            var $choice = $("#country_id").val();
            switch ($choice) {
                case '1':
                    $(".foreigner").hide();
                    break;
                default:
                    $(".foreigner").show();
                    break;
            }
        });
        $("#country_id").trigger("change");

        $('body').on('submit', 'form[name=create]', function(e) {
            e.preventDefault();
            // Validate date -- paymaent date
            var $day = $('#dob_day').val();
            var $month = $('#dob_month').val();
            var $year = $('#dob_year').val();
            if (($year) && ($month) && ($day )) {
                $('input[name=dob]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=dob]").val("");
            }

            this.submit();

        });



        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });




        // check if location type is selected -> survyed and unsurveyed
        function location_type_option(location_type, surveyed,unsurveyed) {

            var choice = $("#"+location_type).val();
            switch (choice) {
                case '1':
                    $("." + surveyed).show();
                    $("." + unsurveyed).hide();
                    break;
                case '2':
                    $("."+surveyed).hide();
                    $("."+unsurveyed).show();
                    break;
                default:
                    $("." + surveyed).hide();
                    $("." + unsurveyed).hide();
            }
        }

        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: true,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });

    });


</script>;

@endpush

