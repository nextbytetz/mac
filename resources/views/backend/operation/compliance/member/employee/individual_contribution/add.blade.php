@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_contribution'), 'header_title' => trans('labels.backend.member.add_contribution')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($employee,['route' => ['backend.compliance.employee.store_contribution', $employee->id],'method'=>'post',  'id' => 'create', 'name' => 'create']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}

{{--header--}}
    @include("backend.operation.compliance.member.employee.includes/header_info", ['employee'=>$employee])

    {{--main contants--}}
<br>

    {{--rctno--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.receipt_no'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','rctno', null, ['class' => 'form-control', ]) !!}

                        {!! $errors->first('rctno', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--salary--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.salary'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','basic_pay', null, ['class' => 'form-control money', ]) !!}

                        {!! $errors->first('basic_pay', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--grosspay--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.compliance.grosspay'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','gross_pay', null, ['class' => 'form-control money']) !!}
                        {!! $errors->first('gross_pay', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--contribution month--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.finance.receipt.contrib_month'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                                          <span>      {!!  Form::selectRange('contrib_month_day',1,31, 28, ['class' => 'form-control search-select','style'=>'width:65px', 'placeholder' =>
                         'Day', 'id'=>'contrib_month_day', 'disabled' => 'disabled']) !!}

                        </span>

                            <span>      {!!  Form::selectMonth('contrib_month_month',null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' =>
                         'Month', 'id'=>'contrib_month_month']) !!}
                        </span>

                            <span>      {!!  Form::selectRange('contrib_month_year',Carbon\Carbon::now()->format('Y'),2015,null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year', 'id'=>'contrib_month_year']) !!}
                        </span>

                        </div>
                        {!! Form::hidden('contrib_month') !!}
                        {!! $errors->first('contrib_month', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--grosspay--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.employer'):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('employer_id', $employers, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



      {{--Buttons--}}
      <div class="row">
        <div class="col-md-7" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to('compliance/employee/profile/' . $employee->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>



    {!! Form::close() !!}
    {{--</section>--}}

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}

<script  type="text/javascript">
    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();



    $(function () {

        $(".search-select").select2();

        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });

        $('body').on('submit', 'form[name=create]', function(e) {
            e.preventDefault();
            // Validate date -- paymaent date
            var $day = $('#contrib_month_day').val();
            var $month = $('#contrib_month_month').val();
            var $year = $('#contrib_month_year').val();
            if (($year) && ($month) && ($day )) {
                $('input[name=contrib_month]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=contrib_month]").val("");
            }

            this.submit();



            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });


        });



    });



</script>;


@endpush
