@extends('layouts.backend.main', ['title' => trans('labels.backend.claim.choose_incident_type'), 'header_title' => trans('labels.backend.claim.choose_incident_type')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    @if ($employee_id == 0)
        {{--Choose incident for unregistered employee, 0 for not existing--}}
        @include("backend.operation.compliance.member.employee.includes.choose_incident_type_unregistered")
    @else
        {{--Choose incident for registered employee--}}
        @include("backend.operation.compliance.member.employee.includes.choose_incident_type_employee")
    @endif

@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
    $(function() {
        $(".search-select").select2({});
    });
</script>;


@endpush
