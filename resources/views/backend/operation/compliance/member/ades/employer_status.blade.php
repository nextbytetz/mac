@extends('layouts.backend.main', ['title' => 'Check Employer Status', 'header_title' => 'Check Employer Status'])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jQuery-Smart-Wizard/styles/smart_wizard.min.css") }}

@endpush

@section('content') {{-- //['backend.compliance.status'] --}}

{{-- {!! Form::open(['route' => '\Backend\Operation\Compliance\Member\EmployerController@employerStatusShow' ,'method'=>'post','name' => 'create']) !!}
--}}

{!! Form::open(['route' => ['backend.compliance.show.status'], 'method' => 'POST']) !!}
<div style="padding:0px; margin:0px; border-top:1px !important;" >
    <div class="row" >

        <div class="col-md-12">
            @if(isset($error))
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{$error}}.
          </div>
          @endif
          <div class="element-form"  >
            {{Form::label('tin', 'Employer TIN No#;')}}
            <div class="form-group">
                {{Form::text('tin','', ['class'=>'form-control', 'placeholder' =>'insert TIN here', 'required' => 'required'])}}
            </div>
        </div>

    </div>


</div>
</div>

{{--Buttons--}}
<br>
<div class="row">
    <div class="col-md-10" class="form-inline" >
        <div class="element-form">
            <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
            text-xs-right"></div>
            <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                <div class="pull-right">

                    {!! link_to('compliance/status',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                    {!! Form::button(trans('buttons.general.search'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                </div>
            </div>
        </div>
    </div>

</div>
{!! Form::close() !!}

@stop

@if($parsed_json !== null)
    <?php
        //dd($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerName']);
    ?>
@section('content-ades')
<div class="content content-switch content-form-layout">
    <h5>Employer Status Detail</h5>
    <br/>
    <div class="table-responsive">
      <table class="table table-bordered">
          <tbody>
            {{-- Employer Basic informations --}}

            <tr class="cancel_button site-btn">
              <th colspan="2" >Employer Basic Information</th>
          </tr>

           <tr class="cancel_button site-btn">
            @if($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerName'] != '')
            <th colspan="2" > Taxpayer Name:{{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerName']}}</th>
            @else
            <th colspan="2" > Name: 
              {{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['FirstName'].' '.$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['MiddleName'].' '.
              $parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['LastName']}} </th>
              @endif

            </tr>
          <tr>
              <td>TIN </td>
              <td>{{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerId']}}</td>
          </tr>
          <tr>
              <td>Trading Name </td>
              <td>{{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TradingName']}}</td>
          </tr>
          <tr>
            <td>Date of Registration </td>
            <td>{{date('jS F, Y',strtotime($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['DateOfRegistration']))}}</td>
        </tr>
        <tr>
            <td>Number Of Employees</td>
            <td>{{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['NumberOfEmployees']}}</td>
        </tr>
        <tr>
            <td>Employer Status</td>
            <td>
                @if($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code'] == 'Active')
                <span class="btn btn-xs btn-success">
                    {{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code']}}
                </span>

                @else
                <span class="btn btn-xs btn-danger">
                    {{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code']}}
                </span>
                @endif
            </td>
        </tr>

        {{-- Address informations starts --}}

        <tr class="cancel_button site-btn">
            <th colspan="2" >Employer Address Information</th>
        </tr>
        <tr>
            <td>Plot / Street</td>
            <td>
                Plot No. 
                {{ 
                    $parsed_json['Payload'][0]['GeneralInformation']['AddressInformation']['PlotNumber']
                }} 
                ,
                {{
                    $parsed_json['Payload'][0]['GeneralInformation']['AddressInformation']['Street']
                }}
            </td>
        </tr>

        <tr>
            <td>P. O Box</td>
          <td>{{$parsed_json['Payload'][0]['GeneralInformation']['AddressInformation']['PostalAddress']}}</td>
      </tr>

      <tr>
          <td>District</td>
          <td>{{$parsed_json['Payload'][0]['GeneralInformation']['AddressInformation']['District']}}</td>
      </tr>
      <tr>
        <td>Region</td>
        <td>{{$parsed_json['Payload'][0]['GeneralInformation']['AddressInformation']['Region']}}</td>
    </tr>
    {{-- Address informations end --}}

    {{-- Contact Information --}}

    <tr class="cancel_button site-btn">
        <th colspan="2" >Employer Contact Information</th>
    </tr>
    <tr>
        <td>Telephone</td>
        <td>
            {{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Tel']}}
            /
            {{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Tel']}}

        </td>
    </tr>
    <tr>
      <td>Mobile</td>
      <td>{{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Mobile']}}</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>{{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Email']}}</td>
</tr>

{{-- Contact informations end --}}

</tbody>
</table>
</div> 
</div>
@stop

@endif



