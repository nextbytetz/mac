@extends('layouts.backend.main', ['title' => 'Closed Business from TRA', 'header_title' => 'Closed Business from TRA'])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jQuery-Smart-Wizard/styles/smart_wizard.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datepicker/css/bootstrap-datepicker.min.css") }}
<style type="text/css">
  .datepicker-switch,.datepicker .next,.datepicker .prev{

    color:#4c66a4;
  }
</style>
@endpush

@section('content') {{-- //['backend.compliance.status'] --}}

{{-- {!! Form::open(['route' => '\Backend\Operation\Compliance\Member\EmployerController@employerStatusShow' ,'method'=>'post','name' => 'create']) !!}
--}}
<div class="col-md-12">
  @if(isset($error))
  <div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{$error}}.
  </div>
  @endif
</div>

{!! Form::open(['route' => ['backend.compliance.show.closed'], 'class'=>'form-inline', 'method' => 'POST']) !!}
<div style="padding:0px; margin:0px; border-top:1px !important;" >
  <div class="row" >

   <div class="form-group" id="first_date">
    {{Form::label('First Date', 'From:')}}
    {{Form::text('first_date','', ['class'=>'form-control less', 'placeholder' =>'Click here to pick the date'])}}
  </div>

  <div class="form-group" id="second_date">
    {{Form::label('Second Date', 'To:')}}
    {{Form::text('second_date','', ['class'=>'form-control less', 'placeholder' =>'Click here to pick the date'])}}
  </div>





</div>
</div>

{{--Buttons--}}
<br>
<div class="row">
  <div class="col-md-10" class="form-inline" >
    <div class="element-form">
      <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
      text-xs-right"></div>
      <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
        <div class="pull-left">

          {!! link_to('compliance/status',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

          {!! Form::button(trans('buttons.general.search'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

        </div>
      </div>
    </div>
  </div>

</div>
{!! Form::close() !!}

@stop

@if($parsed_json !== null)
@section('content-ades')
<div class="content content-switch content-form-layout">

  <h5>
    Closed Busines/es from {{date('jS F, Y',strtotime($date_one))}} to {{date('jS F, Y',strtotime($date_two))}}

  </h5>
  <br/>
  <div class="table-responsive">

    <table class="table table-bordered">
      <tbody>
        {{-- Employer Basic informations --}}

            {{-- <tr class="cancel_button site-btn">
              <th colspan="2" >Employer Basic Information</th>
          </tr>
          <tr>
              <td>Taxpayer Name</td>
              <td>{{$parsed_json['Payload']['PAYEAmount']['GeneralInformation']['BasicInformation']['TaxpayerName']}}</td>
          </tr>
          <tr>
              <td>TIN </td>
              <td>{{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerId']}}</td>
          </tr>
          <tr>
            <td>Date of Registration </td>
            <td>{{date('jS F, Y',strtotime($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['DateOfRegistration']))}}</td>
        </tr>
        <tr>
            <td>Number Of Employees</td>
            <td>{{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['NumberOfEmployees']}}</td>
        </tr>
        <tr>
            <td>Employer Status</td>
            <td>
                @if($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code'] == 'Active')
                <span class="btn btn-xs btn-success">
                    {{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code']}}
                </span>

                @else
                <span class="btn btn-xs btn-danger">
                    {{$parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code']}}
                </span>
                @endif
            </td>
          </tr> --}}

          {{-- Address informations starts --}}


          @foreach($parsed_json['Payload'] as $payload )

          <tr class="cancel_button site-btn">
            @if($payload['GeneralInformation']['BasicInformation']['TaxpayerName'] != '')
            <th colspan="2" > Taxpayer Name:{{$payload['GeneralInformation']['BasicInformation']['TaxpayerName']}}</th>
            @else
            <th colspan="2" > Name: 
              {{$payload['GeneralInformation']['BasicInformation']['FirstName'].' '.$payload['GeneralInformation']['BasicInformation']['MiddleName'].' '.$payload['GeneralInformation']['BasicInformation']['LastName']}} </th>
              @endif

            </tr>
            <tr>

              <td>TIN</td>
              <td>


                {{$payload['GeneralInformation']['BasicInformation']['TaxpayerId']}}


              </td>
            </tr>

            <tr>

            <td>Trading Name</td>
              <td>


                {{$payload['GeneralInformation']['BasicInformation']['TradingName']}}


              </td>
            </tr>
            <tr>
              <td>Date of registration</td>
              <td>


                {{$payload['GeneralInformation']['BasicInformation']['DateOfRegistration']}}

              </tr>

               <tr>
              <td>Date of deregistration</td>
              <td>


                {{$payload['GeneralInformation']['BasicInformation']['DateOfDeRegistration']}}

              </tr>

              <tr>
                <td>Number of employees</td>
                <td>
                  {{$payload['GeneralInformation']['BasicInformation']['NumberOfEmployees']}}
                </td>
              </tr>
              <tr>
                <td>Registration status</td>
                <td>@if($payload['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code'] == 'Active')
                  <span class="btn btn-xs btn-success">
                    {{$payload['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code']}}
                  </span>

                  @else
                  <span class="btn btn-xs btn-danger">
                    {{$payload['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code']}}
                  </span>
                  @endif</td>
                   
                </tr>
                    <tr>
                <td>Reason</td>
                <td>
                  {{$payload['GeneralInformation']['BasicInformation']['RegistrationStatus']['Reason']}}
                </td>
              </tr>
                @endforeach
                  
                 
                {{-- Address informations end --}}

                {{-- Contact Information --}}

    {{-- <tr class="cancel_button site-btn">
        <th colspan="2" >Employer Contact Information</th>
    </tr>
    <tr>
        <td>Telephone</td>
        <td>
            {{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Tel']}}
            /
            {{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Tel']}}

        </td>
    </tr>
    <tr>
      <td>Mobile</td>
      <td>{{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Mobile']}}</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>{{$parsed_json['Payload'][0]['GeneralInformation']['ContactInformation']['Email']}}</td>
  </tr> --}}

  {{-- Contact informations end --}}

</tbody>
</table>
</div> 
</div>
@stop

@endif

@push('after-script-end')
{{ Html::script(asset_url() . "/nextbyte/plugins/datepicker/js/bootstrap-datepicker.min.js") }}
<script type="text/javascript">
    $('#first_date input').datepicker({
        autoclose: true,
        orientation:'bottom',
        defaultViewDate:'-05y',
        format:'yyyymmdd',
    });

    $('#second_date input').datepicker({
        autoclose: true,
        orientation:'bottom',
        defaultViewDate:'-05y',
        format:'yyyymmdd',
    });

</script>

@endpush
