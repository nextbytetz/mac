@extends('layouts.backend.main', ['title' => trans('labels.backend.member.employer_registrations'), 'header_title' => trans('labels.backend.member.employer_registrations')])

@include('backend.includes.datatable_assets')

@section('content')
    {{----------Button Add New Employer -----------}}
    {{--<div class="row">--}}
    <div class="col-md-12" >
    <div class="pull-right">
    <a href="{!! route('backend.compliance.employer_registration.name_check') !!}"  class="btn btn-primary save_button" ><i class="icon fa fa-plus-circle"></i>&nbsp;@lang('labels.general.add_new')</a>
    </div>
    </div>
    <div>&nbsp;</div>
    {{--</div>--}}
    <div class="row">
        {{--search source--}}
        <div class = "col-md-3" id="source_div">
            {!! Form::label(trans('labels.backend.report.search_for_source')) !!}
            {!! Form::select('source', ['1' => 'WCF Branch', '2' => 'Online',], $source, ['style' => 'width:100%;border-radius:6px;', 'placeholder' => trans('labels.backend.report.search_for_source'),'class' => 'form-control search-select', 'id'=> 'source']) !!}
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}

        </div>
    </div>

@stop

@push('after-script-end')
{!! $dataTable->scripts() !!}
<script>
    $(function () {
        $("#source").on("change", function () {
            var $source = $(this).val();
            document.location.replace(base_url + "/compliance/employer_registration?source=" + $source);
        });
    });
</script>
@endpush
