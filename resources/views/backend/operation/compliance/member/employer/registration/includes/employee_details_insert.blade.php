
<div id="step-2" style=" height: auto !important;">
    <div  data-min="300" data-height="240">
        <div>
            <div class="step-content-wizzard" >
                <h2 class="StepTitle">@lang('labels.general.step') 2: @lang('labels.backend.member.employee_details')</h2>
                </br>
                {{--<div class="all-form-section">--}}
                {{--<div class="validation-form">--}}
                <div >

                    {{--HEADER FOR EMPLOYEE DETAILS----------------------}}
                    <div class="row">
                        <div class="col-md-12 light_grey_bg" style="height:25px">
                            <div class="form-inline" >
                                {{--employee_categories --}}
                                <div class="col-md-3" align="left">
                                    <div class="form-group">
                                        <label style="font-size: 15px"> @lang('labels.backend.member.employee_category')</label>
                                    </div>
                                </div>
                                {{--applicable--}}
                                <div class="col-md-3" align="left">
                                    <div class="form-group">
                                        <label style="font-size: 15px"> @lang('labels.general.applicable')</label>
                                    </div>
                                </div>

                                {{--female--}}
                                <div class="col-md-3" align="left">
                                    <div class="form-group" >
                                        <label style="font-size: 15px"> @lang('labels.general.female')</label>
                                    </div>
                                </div>


                                {{--male--}}
                                <div class="col-md-3" align="left">
                                    <div class="form-group">

                                        <label style="font-size: 15px"> @lang('labels.general.male')</label>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    {{--End of Header--}}

                    {{--INPUTS FIELD--}}

                    {{--Employee Categories--}}
                    @foreach ($employee_categories as $employee_category)

                        <div>&nbsp;</div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-inline" >
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label( 'employee_category_name', $employee_category->name, [  ])!!}
                                        </div>
                                    </div>
                                    {{--applicable - employee_category--}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!!  Form::select('employee_category' . $employee_category->id , ['0' => 'No', '1' => 'Yes'],  null, ['class' => 'form-control search-select employee_category' ,'placeholder'=> '', 'style'=>'width:120px', 'id'=>'employee_category'.$employee_category->id]) !!}
                                        </div>
                                    </div>




                                    {{--female--}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::input( 'text','female' . $employee_category->id, null, ['class' => 'form-control  number fillable'. $employee_category->id ,'style'=>'width:150px', 'id'=>'female'.$employee_category->id,'placeholder'=>trans('labels.general.count')]) !!}
                                            {!! $errors->first('female'. $employee_category->id, '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>

                                    </div>



                                    {{--male--}}
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            {!! Form::input( 'text','male' . $employee_category->id, null, ['class' => 'form-control  number fillable'. $employee_category->id ,'style'=>'width:150px', 'id'=>'male'.$employee_category->id , 'placeholder'=>trans('labels.general.count')])!!}
                                            {!! $errors->first('male'. $employee_category->id, '<span class="help-block label label-danger">:message</span>') !!}

                                        </div>

                                    </div>



                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>


        </div>
    </div>
    {{--</div>--}}
</div>




@push('employee-details-script-end')

<script  type="text/javascript">


    $(function () {

        $('.employee_category').each(function() {
            var $id = this.id;
            var $employee_category_id = $id.substr(17);

            employee_category_option('employee_category'+ $employee_category_id, 'fillable' + $employee_category_id);

            $("#employee_category" + $employee_category_id).change(function () {
                employee_category_option('employee_category'+ $employee_category_id, 'fillable' + $employee_category_id);
            });




        });

    });





    //Employee category options -> disable and hide
    function employee_category_option(employee_category, fillable) {
        var $employee_category = $("#" + employee_category).val();
        if ($employee_category == 1) {
            $("." + fillable ).prop("disabled", false);

        }
        else {
            $("." + fillable).prop("disabled", true);
        }

    }

</script>;

@endpush
