@extends('layouts.backend.main', ['title' => "Edit Employer", 'header_title' => "Edit Employer"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')

    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */

        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.compliance.employer.update_gen_info'], 'name' => 'edit_employer', 'class' => 'edit_employer', 'method' => 'post']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}
        {!! Form::hidden('action_type', 1) !!}



        {{--tin--}}
        <div class="row">
            <div class="col-md-12">
                <div class="element-form" id="parent_div" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.backend.member.parent_employer'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.parent_employer_description')  "></i></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::select('parent_id',$employers, $employer->parent_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control', 'id'=>'parent_id']) !!}
                            @if(isset($employer->parent_id))
                                {!! Form::checkbox('remove_parent_check', '1', 0, ['id' => 'remove_parent_check', 'class' => '']) !!}  Remove Parent
                            @endif
                            {!! Form::hidden('parent_employer_id' ) !!}
                            {!! $errors->first('parent_employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>

                {{--category--}}
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.employer_category'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::select('employer_category_cv_id', $employer_categories, $employer->employer_category_cv_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select'  , 'id' => 'employer_category_cv_id']) !!}
                            {!! $errors->first('employer_category_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--nature of business--}}
        <div class="row">
            <div class="col-md-12">


                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.nature_of_business'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::select('business_sector_cv_id', $business_sectors, ($employer->sectors()->count()) ? $employer->sectors->first()->id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                            {!! $errors->first('business_sector_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>


                {{--main_activities--}}
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.member.business_activities'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group text_content" >
                            {!! Form::textarea('business_activity', $employer->business_activity, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                            {!! $errors->first('business_activity', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <div class="element-form" id="treasury_div" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Pay through Hazina (Treasury):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::select('is_treasury', ['0' => 'No', '1' => 'Yes'], ($employer->is_treasury == true) ? 1 : 0, ['style' => 'width:100%', 'placeholder' => '',  'id' => 'is_treasury', 'class' => 'form-control search-select']) !!}
                            {!! $errors->first('is_treasury', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>

                {{--vote--}}
                {{--<div class="element-form"  >--}}
                {{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.member.vote'):</label></div>--}}
                {{--<div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">--}}
                {{--<div class="form-group">--}}
                {{--{!! Form::input( 'text','vote', null, ['class' => 'form-control']) !!}--}}
                {{--{!! $errors->first('vote', '<span class="help-block label label-danger">:message</span>') !!}--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>

        {{--Buttons--}}
        <div class="row">
            <div class="col-md-9" class="form-inline" >
                <div class="element-form">
                    <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                    <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                        <div class="pull-right">

                            {!! link_to_route('backend.compliance.employer.profile',trans('buttons.general.cancel'), [$employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();



            $("#parent_id").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });



            treasury_option();
            $("#employer_category_cv_id").on('change', function (e){
                treasury_option();
            });

            function treasury_option()
            {
                var category_id = $("#employer_category_cv_id").val();
                if(category_id == 36){
                    $("#treasury_div").show();
                    $("#is_treasury").prop("disabled", false);
                }else{
                    $("#treasury_div").hide();
                    $("#is_treasury").prop("disabled", true);
                }
            }


            /*remove parent trigger*/
            $('#remove_parent_check').change(function(e) {

                if(this.checked){
                    $("#parent_id").prop("disabled", true);
                    $("#parent_id").val(0).change();
                }else{
                    $("#parent_id").prop("disabled", false);
                }
            });




        });
    </script>

@endpush