<div id="step-1" style=" height: auto !important;" >
    <div  data-min="300" data-height="240">
        <div>
            <div class="step-content-wizzard" >
                <h2 class="StepTitle">@lang('labels.general.step') 1: @lang('labels.backend.member.employer_details')</h2>
                </br>
                {{--<div class="all-form-section">--}}
                {{--<div class="validation-form">--}}
                <div>

                    <div class="row">
                        {{--name--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.name'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group text_content">
                                            {!! Form::textarea('name', '', ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}

                                            {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{--Date Commenced--}}
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.employer.date_commenced'):</label></div>
                                    <div class="col-xs-4 col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                        {{--<div class="row">--}}
                                        <div class="form-group">
                                            <div class="form-inline">

                                                {!!  Form::selectRange('commenced_day', 1, 31, null, ['class' => 'form-control  search-select','style'=>'width:55px', 'placeholder' =>
                              'Day', 'id'=>'commenced_day']) !!}


                                                <span>      {!!  Form::selectMonth('commenced_month', null, ['class' => 'form-control search-select','style'=>'width:98px', 'placeholder' =>
                         'Month', 'id'=>'commenced_month']) !!}
                        </span>


                                                <span>      {!!  Form::selectRange('commenced_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(180)->format('Y'),null, ['class' => 'form-control search-select','style'=>'width:62px',
                        'placeholder' =>
                         'Year', 'id'=>'commenced_year']) !!}
                        </span>

                                            </div>
                                        </div>
                                        {!! Form::hidden('doc') !!}
                                        {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
                                        {{--</div>--}}
                                    </div>
                                </div>

                            </div>
                        </div>


                        {{--tin--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.tin_no'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','tin', null, ['class' => 'form-control number', 'id' => 'tin']) !!}
                                            <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
                                            {!! $errors->first('tin', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{--vote--}}
                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.member.vote'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','vote', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('vote', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--Parent ID -> Main Branch Employer--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" id="parent_div" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>@lang('labels.backend.member.parent_employer'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.parent_employer_description')  "></i></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::select('parent_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=>'parent_id']) !!}

                                            {!! Form::hidden('parent_employer_id') !!}
                                            {!! $errors->first('parent_employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        {{--employer category--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.employer_category'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::select('employer_category_cv_id', $employer_categories, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                                            {!! $errors->first('employer_category_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{--annual earning--}}
                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.annual_earning'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','annual_earning', null, ['class' => 'form-control money']) !!}
                                            {!! $errors->first('annual_earning', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--phone--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.phone'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','phone', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{--Telephone--}}
                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.telephone'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','telephone', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('telephone', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        {{--email--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.email'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','email', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{--box--}}
                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.po_box'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','box_no', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('box_no', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--fax--}}
                        <div class="row">
                            <div class="col-md-12">

                                {{--fax--}}
                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.fax'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','fax', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('fax', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                        {{--country id--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.country'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::select('country_id', $countries, 1, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'country_id']) !!}
                                            {!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{--region id--}}
                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.region'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::select('region_id', $regions, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                                            {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--distric id--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.district'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::select('district_id', $districts, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                                            {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{--location type id--}}
                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.location_type'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::select('location_type_id', $location_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'location_type']) !!}
                                            {!! $errors->first('location_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--street--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form surveyed"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.street'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','street', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('street', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>


                                {{--road--}}
                                <div class="element-form surveyed" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.road'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group text_content" >
                                            {!! Form::input( 'text','road', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('road', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                {{--plotno--}}
                                <div class="element-form surveyed"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.plot_no'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','plot_no', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('plot_no', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                {{--block_no--}}
                                <div class="element-form surveyed" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.block_no'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group text_content" >
                                            {!! Form::input( 'text','block_no', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('block_no', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>

                        {{--surveyed extra--}}
                        <div class="row">
                            <div class="col-md-12">

                                <div class="element-form surveyed"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.surveyed_extra'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::input( 'text','surveyed_extra', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('surveyed_extra', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>




                        {{--unsurveyed--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="element-form unsurveyed" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.unsurveyed_area'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group text_content" >
                                            {!! Form::textarea('unsurveyed_area', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                                            {!! $errors->first('unsurveyed_area', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                        {{--nature of business--}}
                        <div class="row">
                            <div class="col-md-12">

                                <div class="element-form"  >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.nature_of_business'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::select('business_sector_cv_id', $business_sectors, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                                            {!! $errors->first('business_sector_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>


                                {{--main_activities--}}
                                <div class="element-form" >
                                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.member.business_activities'):</label></div>
                                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group text_content" >
                                            {!! Form::textarea('business_activity', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                                            {!! $errors->first('business_activities', '<span class="help-block label label-danger">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>


            </div>
        </div>
        {{--</div>--}}
    </div>

</div>





@push('employer-details-script-end')

<script  type="text/javascript">
    $(function () {
        $(".surveyed").hide();
        $(".unsurveyed").hide();
//      $('#smartwizard').smartWizard();

        checkEmployerOption($("#tin").val());

        $('body').on('submit', 'form[name=create]', function(e) {
            e.preventDefault();
            // Validate date -- date commenced date
            var $day = $('#commenced_day').val();
            var $month = $('#commenced_month').val();
            var $year = $('#commenced_year').val();
            if (($year) && ($month) && ($day )) {
                $('input[name=doc]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=doc]").val("");
            }

            var $parent_id = $('#parent_id').val();
            if ($parent_id){
                $('input[name=parent_employer_id]').val($parent_id);
            }else{
                $('input[name=parent_employer_id]').val("");
            }

            this.submit();

        });


        location_type_option('location_type', 'surveyed','unsurveyed');
        $("#location_type").on('change', function (e){
            location_type_option('location_type', 'surveyed','unsurveyed');
        });


        $('#country_id').on('change', function (e) {
            $("#spin2").show();
            var country_id = e.target.value;
            $.get("{{ url('/') }}/getRegions?country_id=" + country_id, function (data) {
                $('#region_id').empty();
                $("#region_id").select2("val", "");
                $('#region_id').html(data);
                $("#spin2").hide();
            });
        });


        $('#region_id').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                $('#district_id').empty();
                $("#district_id").select2("val", "");
                $('#district_id').html(data);
                $("#spin2").hide();
            });
        });


        $('#tin').on('blur', function (e) {
            $("#spin1").show();
            var tin = e.target.value;
            checkEmployerOption(tin);
            $("#spin1").hide();
        });



    });



    // check if location type is selected -> survyed and unsurveyed
    function location_type_option(location_type, surveyed,unsurveyed) {

        var choice = $("#"+location_type).val();
        switch (choice) {
            case '1':
                $("." + surveyed).show();
                $("." + unsurveyed).hide();
                break;
            case '2':
                $("."+surveyed).hide();
                $("."+unsurveyed).show();
                break;
            default:
                $("." + surveyed).hide();
                $("." + unsurveyed).hide();
        }
    }


    function checkEmployerOption(tin) {
        tin = tin.replace(/[^\w\s]/gi, '');
        tin = tin.replace(/ /g, '');
        $.get("{{ url('/') }}/getParentEmployers?tin=" + tin, function (data) {
            $("#parent_id").empty();
            $("#parent_id").select2("val", "");
            $("#parent_id").html(data);

            if (data.length > 80) {
                $("#parent_div").show();
                $("#parent_id").prop("disabled", false);
                $('input[name=parent_employer_id]').prop("disabled", false);
            }else{
                $("#parent_div").hide();
                $("#parent_id").prop("disabled", true);
                $('input[name=parent_employer_id]').prop("disabled", true);
            }
        });
    }

</script>;

@endpush
