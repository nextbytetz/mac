
<div class="modal fade info-modal" id="instruction-modal" tabindex="-1" role="dialog" aria-labelledby="info-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('labels.backend.file.instruction')</h4>
            </div>
            <div class="modal-body" style="background-color: white">
                                <div class="row">
                    <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-pink"></i>
					    <i class="fa fa-info fa-stack-1x fa-inverse"></i>
					</span>
                    </div>
                    <div class='col-md-11'>
                        <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.title')</u></h4>
                        Microsoft Excel file xlsx, xls <br/>
                    <!--					@lang('labels.backend.file.format.file_helper')-->
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-orange"></i>
					    <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
					</span>
                    </div>
                    <div class='col-md-12'>
                        <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.content')</u></h4>
                        @lang('labels.backend.file.format.content_helper') <br/>
                        @lang('labels.backend.file.format.column')  &nbsp;<span class="label label-success">firstname</span>&nbsp;<span class="label label-success">middlename</span> <span class="label label-success">lastname</span>  <span class="label label-success">dob </span> <span class="label label-success">gender</span><span class="label label-success">basicpay</span> <span class="label label-success">allowance</span><span class="label label-success">gender</span>  <span class="label label-success">job_title</span> <span class="label label-success">employment_category</span> &nbsp;
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-yellow"></i>
					    <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
					</span>
                    </div>
                    <div class='col-md-10'>
                        <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.example')</u></h4>
                        @lang('labels.backend.file.format.example_helper') <br/><br/>
                        <img src="{{ asset_url() . '/nextbyte/img/upload_employee_sample.png' }}" height="auto" width="auto">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
             </div>
        </div>
    </div>
</div>







