
<legend class="grey_modal" >@lang('labels.backend.member.name_clearance')</legend>
</br>
<table class="display" cellspacing="0" width="100%" id ="name_clearance_table">
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.general.name')</th>
        <th>@lang('labels.general.tin_no')</th>
        <th>@lang('labels.backend.table.employer.date_commenced')</th>
        <th>@lang('labels.general.region')</th>
    </tr>
    </thead>
</table>

@push('name-clearance-script-end')

<script  type="text/javascript">
    $(function() {
        $('#name_clearance_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{!! route('backend.compliance.employer_registration.get_name_clearance', $employer->id) !!}',
                type: 'get',
                {{--data : {employer_id: "{!! $employer->id !!}"},  },--}}
            },
            columns: [
                { data: 'id', name: 'id', visible: false },
                { data: 'name' , name: 'name' },
                { data: 'tin' , name: 'tin', orderable: false, searchable: false },
                { data: 'doc_formatted' , name: 'doc', orderable: false, searchable: false },
                { data: 'region' , name: 'region', orderable: false, searchable: false },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = base_url + "/compliance/employer_registration/profile/"  + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }
        } );
    });
</script>;

@endpush