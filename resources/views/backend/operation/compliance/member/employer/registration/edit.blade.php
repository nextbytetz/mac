@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_employer_registration'), 'header_title' => trans('labels.backend.member.edit_employer_registration')])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/jQuery-Smart-Wizard/styles/smart_wizard.min.css") }}

@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    @if ($employer->approval_id <> 1)
        {!! Form::model($employer,['route' => ['backend.compliance.employer_registration.update',$employer->id],'method'=>'put', 'name' => 'update', 'enctype' => 'multipart/form-data']) !!}
    @else
        {!! Form::model($employer,['route' => ['backend.compliance.employer.update',$employer->id],'method'=>'put', 'name' => 'update', 'enctype' => 'multipart/form-data']) !!}
    @endif
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate(), ['class' =>'wcf_date']) !!}
    {!! Form::hidden('employer_id', $employer->id) !!}



    <div class="content content-wizard" style="padding:0px; margin:0px; border-top:1px !important;" >
        <div class="row" >

            <div class="col-md-12">
                {{--<h4 class="page-content-title">Form validation form wizard</h4>--}}
                {{--<p>Create & display form wizard with form validation.</p>--}}
                <div class="basic-wizard form-validation-wizzard">

                    <input type='hidden' name="issubmit" value="1">

                    <div id="wizard-validation" class="swMain">
                        <ul>
                            <li><a href="#step-1" data-toggle="tab" class="step" >
<span class="stepDesc">
@lang('labels.backend.member.employer_details')<br/>
<small>@lang('labels.backend.member.employer_details_summary')</small>
</span>
                                </a></li>
                            <li><a href="#step-2" data-toggle="tab" class="step">
<span class="stepDesc">
@lang('labels.backend.member.employee_details')<br/>
<small>@lang('labels.backend.member.employee_details_summary')</small>
</span>
                                </a></li>
                            <li><a href="#step-3" data-toggle="tab" class="step">
<span class="stepDesc">
@lang('labels.backend.member.upload_attachments')<br/>
<small>@lang('labels.backend.member.upload_attachments_summary')</small>
</span>
                                </a></li>

                        </ul>


                        {{--STEP 1--}}
                        @include('backend.operation.compliance.member.employer.registration.includes.employer_details_update')


                        {{--STEP 2--}}
                        @include('backend.operation.compliance.member.employer.registration.includes.employee_details_update')


                        {{--STEP 3--}}
                        @include('backend.operation.compliance.member.employer.registration.includes.attachments_upload_update')




                    </div>
                </div>

            </div>


        </div>
    </div>


    {{--Buttons--}}
    <br>
    <div class="row">
        <div class="col-md-10" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">
                        @if ($employer->approval_id == 1)
                            {!! link_to('compliance/employer/profile/' . $employer->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @else
                            {!! link_to('compliance/employer_registration/profile/' . $employer->id . '#general',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @endif


                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>

    </div>
    {!! Form::close() !!}

@stop


@push('after-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url() . "/nextbyte/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.min.js") }}

    {{ Html::script(asset_url() . "/nextbyte/plugins/form-wizard/js/wizard.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }}

    @stack('employer-details-script-end')

    @stack('employee-details-script-end')

    @stack('attachments-script-end')

    <script  type="text/javascript">

        $(function () {
            $(".search-select").select2();
            enableStep();


            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();




            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                    //var id = this.id;
                    //alert(id);
                } else {
                    location.hash = '#' + tab;
                }
            });



            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);

            });

            /*finish button*/
            $('.buttonFinish').on( 'click', function (e){
                $('form[name=update]').submit();
            });



        });




        /* start : mask all money input */
        $('.money').maskMoney({
            precision : 2,
            affixesStay : false
        });




        /* start : ensure only numbers are input on monetary boxes */
        function number_only(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }

        function enableStep(){

            $('.step').removeClass("selected").removeClass("disabled").addClass("done");
            $('.step').attr("isDone",1);

        }



    </script>;


@endpush

