{{-- Employer registration -> MORE LINKS OPTION--}}

<span class="dropdown"> <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" >
    @lang('buttons.general.more')
      </a>
  <span class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" >

      @if($employer->approval_id == 2)
          <a>
                  {{--Undo registered--}}
              {{ link_to_route('backend.compliance.employer_registration.undo', trans('labels.general.undo'), $employer->id, ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('labels.backend.compliance.undo_employer_registration_confirm'), 'class' => 'dropdown-item']) }}
      </a>
      @endif

          {{--Close--}}
          <a class="dropdown-item" href="{!! route('backend.compliance.employer_registration.index') !!}" >@lang('buttons.general.close')</a>

  </span>

</span>