@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance.employer_name_check'), 'header_title' => trans('labels.backend.compliance.employer_name_check')])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            {{----------Button proceed to add Employer -----------}}
            <div class="col-md-12" >
                <div class="pull-right">
                    <a href="{!! route('backend.compliance.employer_registration.create') !!}"  class="btn btn-success replace_button" >@lang('labels.general.proceed')&nbsp;<i class="icon fa fa-arrow-circle-right"></i></a>
                </div>
            </div>


            <div>&nbsp;</div>

            <table class="display" cellspacing="0" width="100%" id ="employers-table">
                <thead>
                <tr >
                    <th>@lang('labels.backend.table.name')</th>
                    <th>@lang('labels.general.tin_no')</th>
                    <th>@lang('labels.backend.table.employer.date_commenced')</th>
                    <th>@lang('labels.general.region')</th>
                    <th>@lang('labels.general.status')</th>
                    <th>@lang('labels.general.actions')</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#employers-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employer_registration.name_check.get_all_employer') !!}',
                type : 'get'
            },
            columns: [
                { data: 'name', name: 'name'},
                { data: 'tin' , name: 'tin'},
                { data: 'doc', name: 'doc'},
                { data: 'region_name', name: 'region_name'},
                { data: 'status', name: 'status' },
                { data: 'actions', name: 'actions' },

            ],

        });

    });







</script>;

@endpush
