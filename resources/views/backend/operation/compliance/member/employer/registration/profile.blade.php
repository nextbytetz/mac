@extends('layouts.backend.main', ['title' => trans('labels.backend.member.employer_registration_profile'), 'header_title' => trans('labels.backend.member.employer_registration_profile')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}

<style>
    /* start: File Icons CSS */
    #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
    #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
    #folder-tree .file-pdf { background-position: -32px 0 }
    #folder-tree .file-as { background-position: -36px 0 }
    #folder-tree .file-c { background-position: -72px -0px }
    #folder-tree .file-iso { background-position: -108px -0px }
    #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
    #folder-tree .file-cf { background-position: -162px -0px }
    #folder-tree .file-cpp { background-position: -216px -0px }
    #folder-tree .file-cs { background-position: -236px -0px }
    #folder-tree .file-sql { background-position: -272px -0px }
    #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
    #folder-tree .file-h { background-position: -488px -0px }
    #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
    #folder-tree .file-php { background-position: -108px -18px }
    #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
    #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
    #folder-tree .file-rb { background-position: -180px -18px }
    #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
    #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
    #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
    #folder-tree .file-js { background-position: -434px -18px }
    #folder-tree .file-css { background-position: -144px -0px }
    #folder-tree .file-fla { background-position: -398px -0px }
    /* end: File Icon CSS */
    /* start: upload progress bar css */
    .progress-bar {
        background-color: #12CC1A;
        height:20px;
        color: #FFFFFF;
        width:0%;
        -webkit-transition: width .3s;
        -moz-transition: width .3s;
        transition: width .3s;
    }
    .progress-div {
        border:#0FA015 1px solid;
        padding: 5px 0px;
        margin:30px 0px;
        border-radius:4px;
        text-align:center;
    }
    /* end: upload progress bar css */
    tr {
        border-bottom:1pt solid rgba(0, 0, 0, 0.12);
    }
</style>
@endpush


@section('content')


    <div class = "row">
        {{--{!! Form::model($employer, ['route' => ['backend.finance.receipt.dishonour', $employer->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}



        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

        {{--Tabs navigation--}}
        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        {{--General--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#general"
                               data-toggle="tab">@lang('labels.general.general')
                            </a>
                        </li>
                        {{--attachments--}}
                        <li class="nav-item">
                            <a class="nav-link"   href="#documents" data-toggle="tab">@lang('labels.general.document_centre')</a>
                        </li>
                        {{--nme clearance--}}
                        <li class="nav-item">
                            <a class="nav-link"   href="#name_clearance" data-toggle="tab">@lang('labels.backend.member.name_clearance')</a>
                        </li>


                    </ul>
                    <div class="nav_tab_contain tab-content">
                        <div id="general" class="nav_tab_pane tab-pane active in">
                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >


                                            @if (($employer->employeeTemps()->where('error',1)->count()))
                                                <span>
                                                                          {!! link_to_route('backend.compliance.employer_registration.download_error', trans('labels.general.download_error'), [$employer->id], ['class' => 'btn btn-secondary btn-sm pull-right text-red']) !!}
                                            </span>
                                            @endif

                                            @if ($employer->approval_id == 1)

                                                {{--issueance--}}
                                                <span>
                                        <a href="{!! route('backend.compliance.employer_registration.print_issuance',
                                        $employer->id)
                                        !!}"  class="btn btn-primary site-btn nav_button"  target="_blank"><i class="icon fa fa-newspaper-o"></i>&nbsp;@lang('buttons.backend.member.employer.issuance_reg_no')</a>
                                        </span>

                                                    {{--<span>--}}

                                                {{--{!!HTML::decode(link_to_route('backend.compliance.employer_registration.print_issuance',"<i class='icon fa fa-check-circle' aria-hidden='true'></i>&nbsp;" . trans('buttons.backend.member.employer.reissue_certificate'), $employer->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.compliance.reg_no_issuance_confirm'), 'class' => 'btn btn-primary site-btn nav_button '])) !!}--}}
                                                  {{--</span>--}}



                                                @if (!$employer->employerCertificateIssues()->count())
                                                    {{--issue certificate--}}
                                                    <span>
                                            <a href="{!! route('backend.compliance.employer_registration.print_certificate',
                                            $employer->id)
                                            !!}"  class="btn btn-primary site-btn nav_button"  target="_blank"><i class="icon fa fa-certificate"></i>&nbsp;@lang('buttons.backend.member.employer.issue_certificate')</a>
                                            </span>



                                                        {{--<span>--}}

                                                {{--{!!HTML::decode(link_to_route('backend.compliance.employer_registration.print_certificate',"<i class='icon fa fa-check-circle' aria-hidden='true'></i>&nbsp;" . trans('buttons.backend.member.employer.reissue_certificate'), $employer->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.compliance.issue_certificate_confirm'), 'class' => 'btn btn-primary site-btn nav_button '])) !!}--}}
                                                  {{--</span>--}}



                                                    @else
                                                    {{--Reissue Certificate--}}
                                                    <span>
                                            <a href="{!! route('backend.compliance.employer_registration.reprint_certificate',
                                            $employer->id)
                                            !!}"  class="btn btn-primary site-btn nav_button"  target="_blank"><i class="icon fa fa-certificate"></i>&nbsp;@lang('buttons.backend.member.employer.reissue_certificate')</a>
                                            </span>


                                                    {{--<span>--}}

                                                {{--{!!HTML::decode(link_to_route('backend.compliance.employer_registration.reprint_certificate',"<i class='icon fa fa-check-circle' aria-hidden='true'></i>&nbsp;" . trans('buttons.backend.member.employer.reissue_certificate'), $employer->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.compliance.reissue_certificate_confirm'), 'class' => 'btn btn-primary site-btn nav_button '])) !!}--}}
                                                  {{--</span>--}}





                                                @endif



                                            @endif


                                            {{--Approve--}}
                                            @if($check_workflow == 0 && $employer->approval_id == 2)
                                                <span>

                                                    {!!HTML::decode(link_to_route('backend.compliance.employer_registration.approve',"<i class='icon fa fa-check-circle' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.initiate'), $employer->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.compliance.approve_employer_registration_confirm'), 'class' => 'btn btn-primary site-btn nav_button '])) !!}
                                            </span>

                                            @endif


                                            @if(($check_workflow == 0 And $employer->source == 1) || $check_if_is_level1_pending == 1)
                                                {{--modify--}}
                                            <span>
                                                <a href="{!! route('backend.compliance.employer_registration.edit', $employer->id)
                                                !!}"  class="btn btn-primary site-btn nav_button">
                                                <i class="icon fa fa-pencil-square-o"></i>
                                                &nbsp;@lang('buttons.backend.member.employer.modify_registration')</a>
                                            </span>

                                            @endif


                                            {{--More Links--}}
                                            @include('backend.operation.compliance.member.employer.registration.includes.more_links')

                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{--main tab content--}}
                            <div class = "row">
                                <div class="col-md-12">

                                    <div class="col-md-9">
                                        {{--Counts--}}
                                        @if ($employer->source == 2)
                                            <legend></legend>
                                            <legend class="grey_info" >@lang('labels.backend.member.verification.requested')&nbsp;:&nbsp;
                                                <strong>
                                                    {{ $employer->user_formatted }}
                                                </strong>
                                            </legend>
                                            <legend></legend>
                                        @endif
                                        <legend class="grey_modal" >@lang('labels.backend.member.employee_counts_overview')</legend>

                                        {!! $employee_counts_datatable->with(['employer_id' => $employer->id])->render('backend.operation.compliance.member.employer.includes.employee_count') !!}


                                        <br/>
                                        {{--Show alert info if employer with this tin is already registered not from Unregistered--}}
                                        @if ($check_tin == 1 && $employer->approval_id <> 1)
                                            <div class="alert alert-info">
                                                @lang("strings.backend.member.tin_already_registered")
                                            </div>
                                        @endif


                                        {{--main workflow tracks--}}
                                        {{--Approved--}}
                                        @php
                                            $wfModule = $employer->wfModule;
                                            $workflowinput = ['resource_id' => $employer->id, 'wf_module_group_id'=> 6, 'type' => $wfModule->type];
                                        @endphp
                                        {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

                                    </div>
                                    <div class="col-md-3">
                                        {{--sidebar summary--}}
                                        @include('backend.operation.compliance.member.employer.includes.sidebar_summary')

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="documents" class="nav_tab_pane tab-pane">
                            {{--documents--}}
                            @include('backend.operation.compliance.member.employer.includes.document_centre')
                        </div>

                        <div id="name_clearance" class="nav_tab_pane tab-pane">
                            {{--name_clearance--}}
                            @include('backend.operation.compliance.member.employer.registration.includes.name_clearance')
                        </div>

                    </div>
                </div>
            </div>

        </div>

        {{--{!! Form::close() !!}--}}
    </div>
@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}

@stack('document-centre-script-end')
@stack('employee-counts-script-end')
@stack('name-clearance-script-end')
<script  type="text/javascript">

    $(function () {


        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });

</script>;

@endpush
