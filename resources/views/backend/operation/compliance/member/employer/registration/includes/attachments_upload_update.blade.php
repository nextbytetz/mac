
<div id="step-3" style=" height: auto !important;">
    <div  data-min="300" data-height="240">
        <div>
            <div class="step-content-wizzard" >
                <h2 class="StepTitle">@lang('labels.general.step') 3: @lang('labels.backend.member.upload_authorities_attachments')</h2>
                </br>
                {{--<div class="all-form-section">--}}
                {{--<div class="validation-form">--}}
                <div >

                             <div class="row">
                        <div class="col-md-12 light_grey_bg" style="height:25px">
                            <div class="form-inline" >
                                {{--document --}}
                                <div class="col-md-4" align="left">
                                    <div class="form-group">
                                        <label style="font-size: 15px"> @lang('labels.general.document')</label>
                                    </div>
                                </div>
                                {{--applicable--}}
                                <div class="col-md-2" align="left">
                                    <div class="form-group">
                                        <label style="font-size: 15px"> @lang('labels.general.applicable')</label>
                                    </div>
                                </div>


                                {{--upladed file file--}}
                                <div class="col-md-3" align="left">
                                    <div class="form-group">

                                        <label style="font-size: 15px"> @lang('labels.backend.member.uploaded_file')</label>

                                    </div>
                                </div>

                                {{--choose file--}}
                                <div class="col-md-3" align="left">
                                    <div class="form-group">

                                        <label style="font-size: 15px"> @lang('labels.general.choose_file')</label>

                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                    {{--End of Header--}}

                    {{--INPUTS FIELD--}}

                    {{--DOcuments--}}
                    @foreach ($documents as $document)

                        <div>&nbsp;</div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-inline" >
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {!! Form::label( 'document_name', $document->name, [  ])!!}
                                            {{--when list of employee--}}
                                            @if($document->id == 37)
                                                <span>
                                                {!! Form::button(trans('labels.general.info'), ['class' => 'btn btn-info site-btn', 'data-toggle' => 'modal'  ,'data-target' => '#instruction-modal']) !!}
                                                                              </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--applicable - document--}}
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {!!  Form::select('employer_document' . $document->id , ['0' => 'No', '1' => 'Yes'],  ($employer->documents()->where('document_id',$document->id)->count()) ? 1 : 0, ['class' => 'form-control search-select employer_document','placeholder'=> '', 'style'=>'width:120px', 'id'=>'employer_document'. $document->id ]) !!}
                                        </div>
                                    </div>




                                                       {{--uploaded file--}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label( 'uploaded_file'. $document->id, ($employer->documents()->where('document_id',$document->id)->count()) ? $employer->documents()->where('document_id',$document->id)->first()->pivot->name : ' ' , [  ])!!}
                                            {!! Form::hidden('uploaded_file'. $document->id, ($employer->documents()->where('document_id',$document->id)->count()) ? $employer->documents()->where('document_id',$document->id)->first()->pivot->name : null, []) !!}

                                        </div>

                                    </div>

                                    {{--choose--}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::file('document_file'. $document->id, ['class'=> 'document_file'. $document->id]) !!}

                                            {!! Form::hidden('document_file'. $document->id, null, []) !!}
                                            {!! $errors->first('document_file'. $document->id, '<span class="help-block label label-danger">:message</span>') !!}


                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach




                    {{--instruction modal--}}
                    @include('backend.operation.compliance.member.employer.registration.includes.employee_list_instruction')



                </div>
            </div>


        </div>
    </div>
    {{--</div>--}}
</div>




@push('attachments-script-end')

<script  type="text/javascript">


    $(function () {

        $('.employer_document').each(function() {
            var $id = this.id;
            var $employer_document_id = $id.substr(17);

           document_authority_option('employer_document'+ $employer_document_id, 'document_file' + $employer_document_id);

            $("#employer_document" + $employer_document_id).change(function () {
                document_authority_option('employer_document'+ $employer_document_id, 'document_file' + $employer_document_id);
            });



        });



    });




    //document_authority_option -> disable and hide
    function document_authority_option(employer_document, document_file) {
        var $employer_document = $("#" + employer_document).val();
        if ($employer_document == 1) {
            $("." + document_file ).prop("disabled", false);
            $("." + document_file).show();
        }
        else {
            $("." + document_file).hide();
            $("." + document_file ).prop("disabled", true);
        }

    }




</script>;

@endpush
