@extends('layouts.backend.main', ['title' => "Online Notification Application", 'header_title' => "Online Notification Application"])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12">
            {{--HEADER--}}
            @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

            <br/>
            <br/>
            <br/>

            @if ($employer->onlineIncidents()->count())
                {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
            @else
                <span class="underline" style="font-weight: 400;font-size: 14px;">No online notification application request for this employer</span>
            @endif

        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {!! $dataTable->scripts() !!}
@endpush