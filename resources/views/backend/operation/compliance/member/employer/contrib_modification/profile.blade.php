{{-- {{dd(unserialize($modification_request->receipt_codes))}} --}}
@extends('layouts.backend.main', ['title' => "Contribution Modification", 'header_title' => "Contribution Modification"])
@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style type="text/css">
.custom_filter:after {
    background-color: #F5F5F5;
    border: 1px solid #DDDDDD;
    border-radius: 4px 0 4px 0;
    color: #3c5ba4;
    content: "@lang('labels.backend.system.workflow.custom_filter')";
    left: -1px;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
}
.custom_filter {
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
    border-radius: 4px 4px 4px 4px;
    margin: 5px 0px;
    padding: 39px 19px 14px;
    position: relative;
}
</style>
@endpush
@section('content')

<div class="row">
    @include("backend/operation/compliance/member/employer/contrib_modification/includes/header_info")
</div>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tab-pills-image">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                        <i class="icon fa fa-info-circle" aria-hidden="true"></i>General Information
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="document_header" data-toggle="tab" href="#documents" role="tab">
                        <i class="icon fa fa-folder-open" aria-hidden="true"></i>Attachements
                    </a>
                </li>
                @if($modification_request->wf_ended && $modification_request->status != 4)
                <li class="nav-item">
                    <a class="nav-link" id="workflow_history_tab" data-toggle="tab" href="#workflow_history" role="tab">
                        <i class="icon fa fa-history" aria-hidden="true"></i>Workflow History
                    </a>
                </li>
                @endif
            </ul>
            <div class="nav_tab_contain tab-content">
                <div class="tab-pane active" id="general_information" role="tabpanel">
                   <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >
                            {{-- @if($modification_request->can_assign) --}}
                            <div class="pull-right" >
                                @if(empty($modification_request->status))
                                <span>
                                    {!! HTML::decode(link_to_route('backend.compliance.employer.contrib_modification.submit_for_review', "<i class='icon fa fa-paper-plane' aria-hidden='true'></i>&nbsp; Submit For Review",[$modification_request->id],['data-method' => 'confirm','data-type'=>'warning', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you want to submit for review and approval!', 'class' => 'btn btn-info site-btn nav_button'])) !!}
                                </span>
                                @endif

                                @if($modification_request->wf_done == 1 && $modification_request->status !=4)
                                <a target=""  href=" {{ route("backend.letter.process", [$modification_request->id, "EMPCONTRIBMOD"]) }}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-file"></i>&nbsp;Response Letter</a>
                                @endif

                                @if( ($modification_request->status == 0 && $modification_request->user_id == access()->user()->id) || ($modification_request->wf_done == 0 && $modification_request->status==3 && $modification_request->user_id == access()->user()->id))
                                <span>
                                    {!! HTML::decode(link_to_route('backend.compliance.employer.contrib_modification.edit', "<i class='icon fa fa-ellipsis-v' aria-hidden='true'></i>&nbsp; Edit Request",[$modification_request->id],['class' => 'btn btn-primary site-btn nav_button', 'style' => 'font-weight:bold;' ])) !!}
                                </span>
                                <span>
                                   <a href="#" class="btn btn-warning site-btn nav_button cancel_request"><i class="fa fa-trash"></i>&nbsp;Cancel Request</a>
                               </span>
                               @endif
                           </div>
                           {{-- @endif --}}
                       </div>
                   </div>
               </div>
               <div class = "row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <table class="table table-bordered mb-1 mt-2">
                            <thead>
                               <tr class="table-active table-success">
                                <td ><span class="h6">Receipt No#</span></td>
                                <td ><span class="h6">Amount</span></td>
                                <td ><span class="h6">{{$modification_request->status == 2 ? 'Old ' : ' '}} Description</span></td>
                                @if($modification_request->status == 2)
                                <td ><span class="h6">New description</span></td>
                                @endif
                            </tr>
                            <tbody>
                             <tr >
                                <td class="h6">{!!  $modification_request->rctno !!}</td>
                                <td class="h6">{!!  number_format($modification_request->amount,2) !!}</td> 
                                <td class="h6">{!! $modification_request->old_rct_description !!}</td> 
                                @if($modification_request->status == 2)
                                <td class="h6">{!! $modification_request->new_rct_description !!}</td>
                                @endif
                            </tr>

                        </tbody>
                    </table>
                    <div class="card-accordions">
                        <div id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <div class="card-header">
                                    <h6 class="mb-0">
                                        <a class="card-title" data-toggle="collapse" data-parent="#accordion" href="#general_change" aria-expanded="true">{{$modification_request->request_type == 1 ? 'Change Contribution Month(s)' : 'Redistribute Contribution Amount' }}</a>
                                    </h6>
                                </div>
                                <div id="general_change" class="expand collapse in" aria-expanded="true" style="">
                                    <div class="card-block">
                                        @if($modification_request->request_type == 1)
                                        @include("backend/operation/compliance/member/employer/contrib_modification/includes/modify_months")
                                        @elseif($modification_request->request_type == 2)
                                        @include("backend/operation/compliance/member/employer/contrib_modification/includes/redistribute_amount")
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 mt-1">
                        @if(!$modification_request->wf_ended && !empty($modification_request->status))
                        <legend class="grey_info mt-2" >Active Workflow</legend>
                        <div id="workflow" class="nav_tab_pane tab-pane">
                            @php
                            $workflowinput = ['resource_id' => $modification_request->id, 'wf_module_group_id'=> 37, 'type' => 0];
                            @endphp
                            {{-- @include("backend/includes/workflow/wf_track_html", $workflowinput) --}}
                            {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    @include("backend/operation/compliance/member/employer/contrib_modification/includes/summary")
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="documents" role="tabpanel">
        <div class="row">
            <div class="col-sm-12 pt-1">
               @include("backend/operation/compliance/member/employer/contrib_modification/includes/attachment")
           </div>
       </div>
       {{-- @include('backend.operation.compliance.member.employer.payroll_merging.document_center') --}}
   </div>
   @if($modification_request->wf_ended && $modification_request->status != 4)
   <div class="tab-pane" id="workflow_history" role="tabpanel">
    <div class="row">
        <div class="col-md-12 pills-height">
            <div data-plugin="scrollbar" data-height="1500">
                <div class="row">
                    <div class="col-md-8">
                        @if($modification_request->status != 4)
                        @if(!empty($modification_request->status))
                        <legend class="grey_info mt-2" >Workflow</legend>
                        <div id="plan_workflow" class="nav_tab_pane tab-pane">
                            @php
                            $workflowinput = ['resource_id' => $modification_request->id, 'wf_module_group_id'=> 37, 'type' => 0];
                            @endphp
                            {{-- @include("backend/includes/workflow/wf_track_html", $workflowinput) --}}
                            {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
                        </div>
                        @else
                        <div class="alert-left-border mt-2">
                            <div class="alert alert-primary alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    {{-- <span aria-hidden="true">&times;</span> --}}
                                </button>
                                <strong>No workflow history.</strong> This modification request has no workflow recorded.
                            </div>
                        </div>
                        @endif
                        @else
                        <div class="alert-left-border mt-2">
                            <div class="alert alert-primary alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                <strong>This modification request was cancelled.</strong>
                            </div>
                        </div>
                        @endif

                    </div>
                    <div class="col-md-4">
                       @include("backend/operation/compliance/member/employer/contrib_modification/includes/summary")
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
@endif
</div>
</div>
</div>
</div>
@endsection

<div class="modal" id="cancel_request_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form id="cancel_form" action="{{route('backend.compliance.employer.contrib_modification.cancel_request')}}">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Cancel Receipt # {{$modification_request->rctno}} Modification</h5>
        </div>
        <div class="modal-body">
         <div class="row">
             <div class="col-sm-12">
                <label class="h6">Cancel Reason:</label>
                <div class="form-group">
                    {!! Form::hidden("employer_id", $modification_request->employer_id) !!}
                    {!! Form::hidden("modification_request_id", $modification_request->id) !!}
                    <textarea name="cancel_reason" id="cancel_reason" class="form-control autosize" cols="30" rows="10"></textarea>
                    <strong><span class="cancel_reason_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <span class="submit_btn">
            <button type="button" class="btn btn-primary" id="btnSubmitCancel">Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </span>
        <span class="wait hidden">
            <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 1%; width:10%;" />  
            &nbsp; <span class="h6"> Please wait .... </span>
        </span>
    </div>
</div>
</div>
</form>
</div>


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script>
    $(function () {

        let $body = $('body');

        if (location.hash !== '') {
            let $linkhref = $('a[href="' + location.hash + '"]');
            $linkhref.tab('show');
            $linkhref.trigger('click');
        }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });

        $('.cancel_request').on('click',function(e){ 
            e.preventDefault();
            swal({
                title: "Dear! {{access()->user()->firstname}}",
                text: "<h6>Are you sure you want to cancel receipt # {{$modification_request->rctno}} modification request!?</h6>",
                type: "warning",
                html: true,
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "YES CANCEL",
                cancelButtonText: "NO DON'T CANCEL",
                closeOnConfirm: true,
                closeOnCancel: true,
            },
            function(isConfirm) {
               if (isConfirm) {
                 $('#cancel_form').trigger('reset');
                 $('#cancel_request_modal').modal('show');
             }
         });
        });

        $('#btnSubmitCancel').on('click', function(e) {
         e.preventDefault();
         let $form = $('#cancel_form');

         $('.submit_btn').addClass('hidden');
         $('.submit_errors').addClass('hidden');
         $('.submit_errors').text('');
         $("#btnSubmitCancel").prop('disabled', true);
         $(".wait").removeClass('hidden');


         let $options = {
            dataType : "json",
            type : "POST",
            url : $($form).attr("action"),
            success : function (data) {
                $("#btnSubmitCancel").prop('disabled', false);
                $(".submit_btn").removeClass('hidden');
                $(".wait").addClass('hidden');
                if (data.success) {
                  swal({
                    title: "Dear! {{access()->user()->firstname}}",
                    text: "<h6> The modification request succesfully cancelled  </h6>",
                    type: "success",
                    html: true,
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function(isConfirm) {
                    document.location.replace(base_url + "/compliance/employer/contrib_modification/index/{{$modification_request->employer_id}}");
                });        
              }else{
                if (data.errors) {
                 $.each(data.errors, function(key,valueObj){
                    $('.'+key+'_error').removeClass('hidden');
                    $('.'+key+'_error').text(valueObj);
                });
             } else {
                swal('Dear! {{access()->user()->firstname}}','The modification request failed to cancell','warning');
            }
        }
    },
    error: function ($data) {
        $("#btnSubmitCancel").prop('disabled', false);
        $(".submit_btn").removeClass('hidden');
        $(".wait").addClass('hidden');
        swal('Dear! {{access()->user()->firstname}}','An error seems to have occured! Please try again','warning');
    },
};

$($form).ajaxSubmit($options);
});


    });
</script>
@endpush