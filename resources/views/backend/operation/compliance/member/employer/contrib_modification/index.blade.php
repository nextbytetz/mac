@extends('layouts.backend.main', ['title' => 'Contribution Modification', 'header_title' => 'Contribuion Modification'])

@include('backend.includes.datatable_assets')

@section('content')

<div class = "row">
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
    {{--<br/>--}}
    <div class="col-md-12" >
        <br/>
        <div class="col-md-12" >

            <div class="pull-right">


                <div class="btn-group">
                    <a href="{!! route('backend.compliance.employer.contrib_modification.create', $employer->id) !!}"  class="btn btn-sm btn-primary save_button" ><i class="icon fa fa-plus"></i>&nbsp;New Request</a>
                </div>
            </div>



        </div>
    </div>


</div>

<br/>
{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="modification_table">
            <thead>
                <tr >
                    <th>Type Of Modification</th>
                    <th>Receipt To Modify</th>
                    <th>Receipt Description</th>
                    <th>Request Date</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>

    </div>
</div>
@stop


@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#modification_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employer.contrib_modification.datatable', $employer->id) !!}',
                type : 'get'
            },
            columns: [
            { data: 'request_type', name: 'request_type', orderable : true, searchable : true},
            { data: 'rctno', name: 'rctno', orderable : true, searchable : true},
            { data: 'old_rct_description', name: 'old_rct_description',  orderable : false, searchable : false},
            { data: 'date_received', name: 'date_received' ,orderable : true, searchable : true},
            { data: 'status', name: 'status' ,orderable : true, searchable : true},
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url +  "/compliance/employer/contrib_modification/profile/" + aData['id'] ;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        });

    });


</script>;

@endpush
