 <legend class="grey_modal" > Contribution Month(s) Modification</legend>
 <table class="display table table table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Old Month</th>
            <th>New Month</th>
            <th>Payment For</th>
        </tr>
    </thead>
    <tbody>
        @foreach($modification_request->months as $month_to_modify)
        <tr>
            <td>{{ \Carbon\Carbon::parse($month_to_modify->old_month)->format('F, Y')}}</td>
            <td>{{ \Carbon\Carbon::parse($month_to_modify->new_month)->format('F, Y')}}</td>
            <td>{{($month_to_modify->fin_code_id == 1) ? 'Interest' : 'Contribution'}}</td>
        </tr>
        @endforeach
    </tbody>
</table>