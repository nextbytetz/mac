<div class="row mt-2">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Request Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%" class="summary">
            <tr class="underline mb-1" >
                <td class="pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Employer:</h6>
                </td>
                <td>
                    <h6 style="font-weight: bold;">
                        {{$employer->name}}
                    </h6>
                </td>
            </tr>

            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Registration No#  :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">
                        {{$employer->reg_no}}
                    </h6>
                </td>
            </tr>

            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Receipt No#  :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">
                        {{$modification_request->rctno}}
                    </h6>
                </td>
            </tr>

            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Modification Type  :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">
                        {{$modification_request->request_type == 1 ? 'Change Contribution Month(s)' : 'Redistribute Contribution Amount' }}
                    </h6>
                </td>
            </tr>

            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Created By  :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!!  $modification_request->created_by_name !!}</h6>
                </td>
            </tr>

            <tr class="underline" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Created Date :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! $modification_request->created_date_formatted !!}</h6>
                </td>
            </tr>
            <tr class="underline" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Received Date :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! $modification_request->request_date_formatted !!}</h6>
                </td>
            </tr>



            @if($modification_request->iscancelled)
            <tr class="underline" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Cancel Date:</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! \Carbon\Carbon::parse($modification_request->updated_at)->format('d F, Y') !!}</h6>
                </td>
            </tr>
            <tr class="" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Cancel Reason:</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! $modification_request->cancel_reason !!}</h6>
                </td>
            </tr>
            @else
            <tr class="underline" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Request Reason:</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">{!! $modification_request->reason !!}</h6>
                </td>
            </tr>

            @endif

        </table>
    </div>
</div>