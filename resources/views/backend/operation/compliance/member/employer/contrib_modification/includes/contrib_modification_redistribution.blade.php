<div class="row col-md-12">
   <div class="col-md-4">
    <label class="required">Contribution Month(s)</label>
    <div class="form-group">
        <div class="form-inline">

            <div class="input-group" style="width:100%;">
                {!! Form::text('doc',  isset($employer->doc) ? short_date_format($employer->doc) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
            </div>
            <small class="help-block">
                <p>Date when amount was paid</p>
            </small>
            {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>
</div>

<div class="col-md-4">
    <label class="required">Month Count</label>
    <div class="form-group">
        {!! Form::input( 'number','month_number', [], ['class' => 'form-control ','id' => 'numbe_of_month', ]) !!}
        {!! $errors->first('month number', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>

<div class="col-md-4">
    <label class="required">Remark</label>
    <div class="form-group text_content">
        {!! Form::textarea('remark', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
        {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
    </div>  
</div>
</div>

{{-- <div class="row col-md-12">
   <div class="col-md-3">
    <label class="required">Contribution Month(s)</label>
    <div class="form-group">
        <div class="form-inline">

            <div class="input-group" style="width:100%;">
                {!! Form::text('doc',  isset($employer->doc) ? short_date_format($employer->doc) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
            </div>
            <small class="help-block">
                <p>Date when amount was paid</p>
            </small>
            {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>
</div>

<div class="col-md-3">
    <label class="required">Employee(s) Count</label>
    <div class="form-group">
        {!! Form::input( 'number','month_number', [], ['class' => 'form-control ','id' => 'numbe_of_month', ]) !!}
        {!! $errors->first('month number', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>

<div class="col-md-3">
    <label class="required">Contribution Amount</label>
    <div class="form-group text_content">
        {!! Form::text('contrib_amount', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
        {!! $errors->first('contrib_amount', '<span class="help-block label label-danger">:message</span>') !!}
    </div>  
</div>
<div class="col-md-3">
    <div class="form-group text_content">
        <label for="attachment" class="required">Attachment(s)</label>
        <input type="file" class="form-control change_attachment_field" name="attachment" id="attachment" required="required" />
        {!! $errors->first('attachment', '<span class="help-block label label-danger">:message</span>') !!}
    </div> 
</div> --}}
</div>




