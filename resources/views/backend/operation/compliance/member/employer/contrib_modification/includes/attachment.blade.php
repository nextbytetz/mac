
<div class = "row">
    <div class="col-md-12">
        @if(!empty($modification_request->request_attachment) || !empty($modification_request->receipt_attachment))

        <div class="col-md-4">

            <legend>Attached Documents
            </legend>

            <br/>

            {{----}}
            <div class="row">
                <div class="col-md-12">
                    <div class="element-form" >
                        @if(!empty($modification_request->request_attachment))
                        <i class="fa fa-file-pdf-o" ></i>
                        <a  style="color:dodgerblue;" class="doc_attached" id="request_attachment"  href="#">Request Letter / Email</a>
                        <br>                        <br>
                        @endif
                        @if(!empty($modification_request->receipt_attachment))
                        <i class="fa fa-file-pdf-o" ></i>
                        <a  style="color:dodgerblue;" class="doc_attached" id="receipt_attachment"  href="#">Receipt Attachment</a>
                        <br>
                        <br>
                        @endif
                    </div>

                </div>
            </div>

        </div>




        <div class="col-md-8">
            <div class = "row">
                <div class="col-md-12">
                    {{--Document Preview--}}
                    <legend>Preview</legend>
                    <br/>
                    <div id="document_frame" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>


                </div>

            </div>
        </div>
        @else
        <div class="alert-left-border mt-2">
            <div class="alert alert-primary alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    {{-- <span aria-hidden="true">&times;</span> --}}
                </button>
                <strong>No Attachement in this request</strong>.
            </div>
        </div>
        @endif
    </div>
</div>





@push('after-script-end')

<script  type="text/javascript">


    $(function () {

        $(".search-select").select2();

        $(".doc_attached").click(function($e) {
            $e.preventDefault();
            let $document_frame = $("#document_frame");
            let $doc_id = $(this).attr('id');
            previewAttachment().done(function ($data) {
                $document_frame.find("iframe").remove();
                // console.log($doc_id);
                let $url = '';
                if($doc_id == 'request_attachment') {
                    $url = $data.request_url 
                }
                if($doc_id == 'receipt_attachment') {
                    $url = $data.receipt_url 
                }

                if ($doc_id.length && $url !== '') {
                    let $iframe = $('<iframe src="' +$url+ '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                }
            });
        });


        function previewAttachment() {
         return $.ajax({
            url: base_url + "/compliance/employer/contrib_modification/attachment_preview/{{$modification_request->id}}",
            dataType : 'json',
            async : false,
            method : "POST"
        });
     }


 });
</script>;

@endpush
