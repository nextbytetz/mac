<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h6"><strong>{{$modification_request->request_type == 1 ? 'Change Contribution Month(s)' : 'Redistribute Contribution Amount' }} </strong></span>
    <span class="navbar-brand mb-0 h6">
        <small >
            Receipt No# &nbsp;:&nbsp;
            <span class="underline"> 
                {{-- <a href="{{route('backend.claim.investigations.profile',$profile->id)}}"> --}}
                    {!! $modification_request->rctno !!}        
                {{-- </a>  --}}
            </span> &nbsp;&nbsp;
            Status: {!! $modification_request->stage_label !!}
        </small>
    </span>
</nav>
<legend></legend>
<br/>