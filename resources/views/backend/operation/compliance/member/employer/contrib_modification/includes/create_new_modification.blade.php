<div class="row col-md-12">

 <div class="col-md-4">
    <label>Request Type:</label>
    <div class="form-group">
        <div class="form-inline">

            <div class="input-group" style="width:100%;">
                {!! Form::select('name_change_type_cv_id', $name_change_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'name_change_type']) !!}

            </div>   
            {!! $errors->first('name_change_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>
</div>

<div class="col-md-4">
    <label class="required">Receipt Number (Receipt):</label>
    <div class="form-group">
        {!! Form::number('rctno', null, ['class' => 'form-control ' ,  'id' => 'rctno',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
        {!! $errors->first('month number', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
<div class="col-md-4">
    <label class="required">Date received</label>
    <div class="form-group">
        <div class="form-inline">

            <div class="input-group" style="width:100%;">
                {!! Form::text('date_received',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
            </div>
            <small class="help-block">
                <p>Date when request was received</p>
            </small>
            {!! $errors->first('date_received', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>
</div>

</div>

<div class="row  col-md-12 name_optional_div" id="contribution_month_div">
    
        <div class="col-md-4">
            <label class="required">Old Value (Old Month.):</label>
            <div class="form-group">
                <div class="form-inline">

                    <div class="input-group" style="width:100%;">
                        {!! Form::text('doc',  isset($employer->doc) ? short_date_format($employer->doc) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="help-block">
                        <p>First contribution month</p>
                    </small>
                    {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <label class="required">New Value (new Month.):</label>
            <div class="form-group">
                <div class="form-inline">

                    <div class="input-group" style="width:100%;">
                        {!! Form::text('new_value',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="help-block">
                        <p>New Contribution Month to be change</p>
                    </small>
                    {!! $errors->first('date_received', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <label class="required">Remark</label>
            <div class="form-group text_content">
                {!! Form::textarea('remark', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
</div>

{{-- start of  redistribution --}}
<div class="row col-md-12 name_optional_div" id="contrib_redistribution_div">

 @include('backend/operation/compliance/member/employer/contrib_modification/create/includes/contrib_modification_redistribution')
</div>


<div class="row col-md-12 name_optional_div" id="new_name_div">
 @include('backend/operation/compliance/member/employer/contrib_modification/create/includes/contrib_modification_underpayment')
</div>


@push('after-script-end')
<!-- Custom javascript files for this page -->
{{--{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}--}}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}
<script>
    $(function() {
            // $(".search-select").select2({});

            nameChangeOption();
            $("#name_change_type").on('change', function (e){
                nameChangeOption();
            });


            function nameChangeOption()
            {
                var choice = $('#name_change_type').val();

                switch(choice){
                    case 'EMCMMONTH':
                    hide_show('hide_class', 'name_optional_div');
                    enable_disable('disable_class', 'name_optional_input');
                    /*show Change posted contribution month */
                    hide_show('show_id', 'contribution_month_div');
                    enable_disable('enable_id', 'contribution_month_id');
                    break;


                    case 'EMCMRED':
                    hide_show('hide_class', 'name_optional_div');
                    enable_disable('disable_class', 'name_optional_input');
                    /*show Change contributed amount */
                    hide_show('show_id', 'contrib_redistribution_div');
                    enable_disable('enable_id', 'contrib_redistribution_id');
                    break;


                    case 'EMCMUNDER':
                    hide_show('hide_class', 'name_optional_div');
                    enable_disable('disable_class', 'name_optional_input');
                    /*show modification underpayment*/
                    hide_show('show_id', 'new_name_div');
                    enable_disable('enable_id', 'new_name');
                    break;

                    default:
                    hide_show('hide_class', 'name_optional_div');
                    enable_disable('disable_class', 'name_optional_input');

                    break;

                }
            }

            /*Work for element id*/
            function enable_disable(action_type, element_id)
            {
                switch (action_type) {
                    case 'enable_id':
                    $("#"+element_id).prop("disabled", false);
                    break;
                    case 'disable_id':
                    $("#"+element_id).prop("disabled", true);
                    break;
                    case 'enable_class':
                    $("."+element_id).prop("disabled", false);
                    break;
                    case 'disable_class':
                    $("."+element_id).prop("disabled", true);
                    break;
                }
            }

            /*End - Enable_Disable*/

            /* start ---HIDE - SHOW attributes of elements*/

            /*Work for element id*/
            function hide_show(action_type, element_id)
            {
                switch (action_type) {
                    case 'hide_id':
                    $("#" + element_id).hide();
                    break;
                    case 'show_id':
                    $("#"+ element_id).show();
                    break;
                    case 'hide_class':
                    $("." + element_id).hide();
                    break;
                    case 'show_class':
                    $("."+ element_id).show();
                    break;
                }
            }
            $('.money').maskMoney({
                precision : 2,
                affixesStay : false
            });

            $('.numbe_month').click(function(e) {
                $('body').html("<input type='text' value='' name='contrib_month' class='form-group'>");

            });

            $("body").on("keyup", "input.search", function (e) {
              var inputValue = $(this).val(); 
              if(e.keyCode == 2) {
                alert(inputValue);
            }
        })


        });
    </script>

    @endpush