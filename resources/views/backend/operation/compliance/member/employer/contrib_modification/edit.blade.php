@extends('layouts.backend.main', ['title' => "Edit Modification Request", 'header_title' => "Edit Modification Request"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
<style>
</style>
@endpush

@section('content')
<div class="row ">
    <div class="col-md-12">
        @include("backend/operation/compliance/member/employer/contrib_modification/includes/header_info")
        <div>

            &nbsp;<br/></div>
            {!! Form::open(array('method'=>'POST','route' => ['backend.compliance.employer.contrib_modification.update_request', $modification_request->id], 'name' => 'edit_modification_request', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden("employer_id", $employer->id) !!}
            {{ method_field('PUT') }}
            <div class="card">
              <div class="card-header"></div>
              <div class="card-body pt-1 pl-1 pb-2">
                <div class="row">
                    <div class="col-sm-3"><label>Request Type:</label>
                        <div class="form-group">
                            <?php 
                            $request_type_name = $modification_request->request_type == 1 ? 'Change Contribution Month(s)': 'Redistribute Contribution Amount';
                            $request_types = [$modification_request->request_type=>$request_type_name]; 
                        ?>
                        <select name="request_type" id="request_type" class="form-control" style="width:100%; height: 100%;">
                            <option value="{{$modification_request->request_type}}">{{$request_type_name}}</option>
                        </select>
                        <input type="text" name="receipt_type" id="receipt_type" class="form-control hidden">
                        <strong><span class="request_type_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                </div>
                <div class="col-sm-3"><label>Receipt To Modify (Receipt No#):</label>
                    <div class="form-group">
                        <select name="receipt_id" id="rctno" class="form-control" style="width:100%; height: 100%;">
                            <option value="{{$modification_request->receipt_id}}">{{$modification_request->rctno}}</option>
                        </select>
                        <strong><span class="receipt_id_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                </div>
                <div class="col-sm-3">
                    <label class="required">Date Requested</label>
                    <div class="form-inline">
                        <div class="input-group" style="width:100%;">
                            {!! Form::text('request_date', $modification_request->date_received, ['placeholder' => '', 'class' => 'form-control datepicker3', 'autocomplete' => 'off']) !!}
                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        <strong><span class="request_date_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                   <div class="fileld-layout">
                    <label class="required">Reason</label>
                    <div class="form-group mt-1">
                      <div class="input-group">
                        {!! Form::textarea('reason', $modification_request->reason, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px; min-width:50%;', 'rows'=>'3']) !!}
                        <span class="help-block label label-danger submit_errors reason_error hidden mt-1"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
         <div class="fileld-layout">
          <label class="required">Request Attachement (Letter or Email in pdf/image) </label>
          <div class="form-group mt-1">
            <div class="input-group">
              <input type="file" name="request_attachment" id="request_attachment" class="form-control">
              <span class="help-block label label-danger submit_errors request_attachment_error hidden mt-1"></span>
          </div>
      </div>
  </div>
</div>
<div class="col-sm-3">
 <div class="fileld-layout">
  <label class="required">Receipt Attachement (pdf/image) </label>
  <div class="form-group mt-1">
    <div class="input-group">
      <input type="file" name="receipt_attachment" id="receipt_attachment" class="form-control">
      <span class="help-block label label-danger submit_errors request_attachment_error hidden mt-1"></span>
  </div>
</div>
</div>
</div>
</div>
<div class="row" id="receipt_details">
    <div class="col-sm-3"><label>Receipt Description:</label>
        <div class="form-group">
         {!! Form::textarea('rct_description', null, ['placeholder' => '', 'class' => 'form-control', 'id'=>'rct_description' ,'rows'=>'1', 'readonly'=>'readonly']) !!}
     </div>
 </div>
 <div class="col-sm-3"><label>Amount:</label>
    <div class="form-group">
        {!! Form::text('rct_amount', null, ['placeholder' => '', 'class' => 'form-control', 'id'=>'rct_amount' ,'autocomplete' => 'off', 'readonly'=>'readonly']) !!}
    </div>
</div>
<div class="col-sm-3"><label>Payment Date:</label>
    <div class="form-group">
        {!! Form::text('rct_date', null, ['placeholder' => '', 'class' => 'form-control', 'id'=>'rct_date' ,'autocomplete' => 'off', 'readonly'=>'readonly']) !!}
    </div>
</div>
<div class="col-sm-3"></div>
</div>
<div class="row hidden" id="border_rct">
    <div class="col-sm-12"><hr></div>
</div>
<span class="row hidden" id="change_month">

</span>
<span class="hidden" id="redistribute">
    <div class="row mb-1">
        <div class="col-sm-4">
            <input type="text" placeholder="number of months" name="number_of_fields" class="form-control" id="number_of_fields">
            <strong><p class="number_of_fields_error help-block label label-danger hidden submit_errors"></p></strong>
        </div>
        <div class="col-sm-2">
            <a class="add_number_of_fields" href="#"><i class="icon fa fa-3x fa-plus" aria-hidden="true" style="color:darkblue"></i></a>
        </div>
        <div class="col-sm-6">
           <div class="fileld-layout">
            <div class="form-group">
                <div class="form-inline">
                    <span>
                        Total amount distributed &nbsp;:&nbsp;&nbsp;
                    </span>
                    <span id = "total_entry" class="underline" style="font-weight: bold;">
                        0.00
                    </span>
                    <span id="contribution_summary_icon">

                    </span>
                </div>
            </div>
        </div>
    </div>

</div>
<hr>
<span class="redistribute_fields">

</span>

</span>
<div class="row mt-2 submit_btn">
    <div class="col-md-12">
        <input type="submit" value="SUBMIT" class="btn btn-primary" id="btnSubmitModification">
        <span class="wait hidden">
            <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 1%; width:10%;" />  
            &nbsp; <span class="h6"> Please wait .... </span>
        </span>
    </div>
</div>       
</div>
</div>
{!! Form::close() !!}
</div>
@endsection

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{-- {{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js") }} --}}

<script>
    $(function() {
        let $body = $('body');

        $(".search-select").select2({ });

        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();

        jQuery('.datepicker3').datetimepicker({
            timepicker:false,
            format:'Y-m-d',
            weeks: true,
            dayOfWeekStart: 1,
            lazyInit: true,
            scrollInput: false,
            maxDate: new Date(),
            minDate: new Date('2015-07-01')
        });

        $body.on('click',".datepicker2", function(){
            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'Y-m-d',
                weeks: true,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: 0,
                minDate: new Date('2015-07-01')
            });
        });

        $(".rct-select").select2({
         minimumInputLength: 3,
         ajax: {
          url: '{!! route('backend.compliance.employer.contrib_modification.return_rct', $employer->id) !!}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            data = data.map(function (item) {
              return {
                text: item.rctno,
                id: item.id,
                rctno: item.rctno,
                rct_type: item.receipt_type,
            };});
            return {
              results:  data
          };
      },
      cache: true }});

        $body.on('submit', 'form[name=edit_modification_request]', function(e) {
           e.preventDefault();
           let $form = this;
           $('.submit_errors').addClass('hidden');
           $('.submit_errors').text('');
           $($form).find("#btnSubmitModification").prop('disabled', true);
           $($form).find("#btnSubmitModification").addClass('hidden');
           $($form).find(".wait").removeClass('hidden');

           $body.find("input[name=receipt_type]").val({{$modification_request->receipt_type}});

           $body.find('.contrib_amount').each(function() {
            let this_amount = $(this).val();
            let this_name = $(this).attr('name');
            if (this_amount != '') {
               $body.find("input[name="+this_name+"]").val(this_amount.split(",").join(""));
           }
       });

           var $data = $($form).serialize();

           let $options = {
            data : $data,
            dataType : "json",
            type : "POST",
            url : $($form).attr("action"),
            success : function (data) {
                $($form).find("#btnSubmitModification").prop('disabled', false);
                $($form).find("#btnSubmitModification").removeClass('hidden');
                $($form).find(".wait").addClass('hidden');
                if (data.success) {
                  swal({
                    title: "Dear! {{access()->user()->firstname}}",
                    text: "<h6> Receipt # {{$modification_request->rctno}} Modification Request Successfully Updated </h6>",
                    type: "success",
                    html: true,
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function(isConfirm) {
                    document.location.replace(base_url + "/compliance/employer/contrib_modification/profile/" + data.request_id);
                });        
              }else{
                if (data.errors) {
                 $.each(data.errors, function(key,valueObj){
                    $('.'+key+'_error').removeClass('hidden');
                    $('.'+key+'_error').text(valueObj);
                });
             } else {
                swal('Dear! {{access()->user()->firstname}}','An error seems to have occurred!  Receipt # {{$modification_request->rctno}} Modification Request Failed To Update','warning');
            }
        }
    },
    error: function ($data) {
        $($form).find("#btnSubmitModification").prop('disabled', false);
        $($form).find("#btnSubmitModification").removeClass('hidden');
        $($form).find(".wait").addClass('hidden');
        swal('Dear! {{access()->user()->firstname}}','An error seems to have occurred. Kindly try again','warning');
    },
};

if ($('#request_type option:selected').val()) {
    switch(parseInt($('#request_type option:selected').val())){
        case 1:
        $($form).ajaxSubmit($options);
        break;
        case 2:
        if ($('#rct_amount').val() && $body.find('span.general_error').length) {
           let rct_total = parseFloat($('#rct_amount').val().split(",").join(""));
           let total_entry = parseFloat($('#total_entry').text().split(",").join(""));
           if (rct_total !== total_entry) {
            $($form).find("#btnSubmitModification").prop('disabled', false);
            $($form).find("#btnSubmitModification").removeClass('hidden');
            $($form).find(".wait").addClass('hidden');
            $body.find('.general_error').removeClass('hidden');
            $body.find('.general_error').text('The receipted amount and distributed amount doesn\'t tally. Kindly check and try again');
        }else{
          $($form).ajaxSubmit($options);
      }
  }else{
    $($form).ajaxSubmit($options);
}
break;
default:
}
}else{
    $($form).ajaxSubmit($options);
}

});

        receiptDetails();
        $('#request_type').on('change',function(e){ 
            returnFormFields();

        }); 

        $('.rct-select').on('change',function(e){
            receiptDetails();
        }); 


        function receiptDetails() {
            $('#receipt_details').addClass('hidden');
            $('#rct_description').val('{{$modification_request->old_rct_description}}');
            $('#rct_amount').val(addCommas('{{$modification_request->amount}}'));
            $('#rct_date').val('{{$modification_request->receipt->rct_date}}');
            $('#receipt_details').removeClass('hidden');
            returnRequestReceiptFields();
        }


        function returnFormFields() {
            $('#border_rct').addClass('hidden');
            $('#change_month').addClass('hidden');
            $('#redistribute').addClass('hidden');
            $('.redistribute_fields').empty();
            $('#change_month').empty();
            $('#underpayment').empty();
            switch({{$modification_request->request_type}}){
                case 1:
                $.ajax({
                    url : '{{url('/compliance/employer/contrib_modification/form_fields/'.$modification_request->receipt_id.'/'.$modification_request->request_type.'/'.$modification_request->receipt_type)}}',
                    type : 'GET',
                    datatype : 'json',
                    success:function(data){
                      if (data.success) {
                       $('#border_rct').removeClass('hidden');
                       $('#change_month').removeClass('hidden');
                       $('#redistribute').addClass('hidden');
                       $('#change_month').append(data.append_field);
                   }
               }});
                break;
                case 2:
                $('#border_rct').removeClass('hidden');
                $('#change_month').addClass('hidden');
                $('#redistribute').removeClass('hidden');
                break;
                default:
                $('#border_rct').addClass('hidden');
                $('#change_month').addClass('hidden');
                $('#redistribute').addClass('hidden');
            }
        }

        function returnRequestReceiptFields() {

            $('#border_rct').addClass('hidden');
            $('#change_month').addClass('hidden');
            $('#redistribute').addClass('hidden');
            $('#change_month').empty();
            $('.redistribute_fields').empty();

            $.ajax({
                url : '{{url('/compliance/employer/contrib_modification/return_request_fields/'.$modification_request->id)}}',
                type : 'GET',
                datatype : 'json',
                success:function(data){
                  if (data.success) {
                    switch({{$modification_request->request_type}}){
                        case 1:
                        $('#border_rct').removeClass('hidden');
                        $('#change_month').removeClass('hidden');
                        $('#redistribute').addClass('hidden');
                        $('#change_month').append(data.append_field);
                        break;
                        case 2:
                        $('#border_rct').removeClass('hidden');
                        $('#change_month').addClass('hidden');
                        $('#redistribute').removeClass('hidden');
                        $('.redistribute_fields').append(data.append_field);
                        break;
                        default:
                    }
                    checkAmount();
                }
            }});
        }

        $body.on('click', '.add_number_of_fields', function(e) {
           e.preventDefault();
           $('.general_error_2').remove();
           let number_of_fields = $body.find('#number_of_fields').val();
           let numItems =  $($body).find(".remove_field").length;
           if (number_of_fields) {
             $.ajax({
                url : '{{url('/compliance/employer/contrib_modification/add_form_fields')}}/'+number_of_fields,
                type : 'GET',
                datatype : 'json',
                success:function(data){
                  if (data.success) {
                     $('.redistribute_fields').append(data.append_field);
                 }
             }});}
         });

        $body.on('click', '.remove_field', function(e) {
            $($body).find(".row_"+$(this).attr('id')).remove();
        });

        $('body').off('keyup', '.contrib_amount').on('keyup', '.contrib_amount', function(e) {
            checkAmount();
        });

        $('body').off('keydown', '.namba').on('keydown', '.namba', function(e) {
            number_only(e);
        });

        function number_only(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            ((e.keyCode === 65 || e.keyCode == 86) && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
        return;
    }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function check_amount_match($receipt_amount, $entry_amount) {
        var $diff = parseFloat($receipt_amount).toFixed(2) - parseFloat($entry_amount).toFixed(2);
        $diff = parseFloat($diff).toFixed(2);
        switch(true) {
            case ($diff < -0.0):
            $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:red;'><i class='icon fa fa-2x fa-close' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>Total amount entered exceed the receipted amount</span>");
            break;
            case ($diff > 0.0):
            $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:#ffd303;'><i class='icon fa fa-2x fa-warning' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>Receipted amount is more than total distributed amount</span>");
            break;
            case ($diff >= -0.0 && $diff <= 0.0):
            $("#contribution_summary_icon").html(",&nbsp;&nbsp;<span style='color:green;'><i class='icon fa fa-2x fa-check-square-o' aria-hidden='true'></i></span>&nbsp;&nbsp;&nbsp;<span style='background-color: grey;color: white;font-weight: bold; padding : 2px;border-radius : 2px;'>@lang("exceptions.backend.finance.receipts.matched")</span>");
            break;
        }
    }

    function checkAmount() {
        var value;
        var sum = 0;
        $body.find('.contrib_amount').each(function() {
            value = $(this).val();
            if (value != '') {
                sum += parseFloat(value.split(",").join(""));
            }
        });
        $('#total_entry').text(addCommas(parseFloat(sum).toFixed(2)));
        let receipt_amount = $('#rct_amount').val() ? parseFloat($('#rct_amount').val().split(",").join("")).toFixed(2): 0.00;
        check_amount_match(receipt_amount, sum);
    }

});
</script>

@endpush
