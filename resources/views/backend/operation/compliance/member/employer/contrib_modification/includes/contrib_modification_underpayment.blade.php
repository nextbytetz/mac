<div class="row col-md-12">

     <div class="col-md-4">
       <div class="required"><label>Contribution Month(s)</label></div>
       <div class="form-group">
        <div class="form-inline">

            <div class="input-group" style="width:100%;">
                {!! Form::text('doc',  isset($employer->doc) ? short_date_format($employer->doc) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
            </div>
            <small class="help-block">
                <p>Date when amount was paid</p>
            </small>
            {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
    </div>
</div>

<div class="col-md-4">
    <label class="required">Employee(s) Count</label>
    <div class="form-group">
        {!! Form::number('new_name', null, ['class' => 'form-control name_optional_input' ,  'id' => 'new_name',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
        {!! $errors->first('month number', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
<div class="col-md-4">
    <label class="required">Amount:</label>
    <div class="form-group">
        {!! Form::text('amount', [], ['class' => 'form-control  money',  'id' => 'amount']) !!}
        {!! $errors->first('month number', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
    
</div>



