@if(!empty($modification_request->receipt_codes))
<legend class="grey_modal" > Current Amount Distribution </legend>
<table class="table table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr class="table-active table-success">
            <th>Payment For</th>
            <th>Contribution Month</th>
            <th>Contribution Amount</th>
            <th>Employee Count</th>
        </tr>
    </thead>
    <tbody>
        @foreach(unserialize($modification_request->receipt_codes) as $index => $rct_code)
        <tr>
            <td>{{($rct_code['fin_code_id'] == 1) ? 'Interest' : 'Contribution'}}</td>
            <td>{!! \Carbon\Carbon::parse($rct_code['contrib_month'])->format('F, Y') !!}</td>
            <td>{{!empty($rct_code['amount']) ? number_format($rct_code['amount'],2) : 0.00}}</td>
            <td>{{!empty($rct_code['employee_count']) ? $rct_code['employee_count'] : ' - '}}</td>
        </tr>
        @endforeach
    </tbody>
</table>


@endif

<legend class="grey_modal"> New Amount Redistribution </legend>
<table class="table table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr class="table-active table-info">
            <th>Payment For</th>
            <th>Contribution Month</th>
            <th>Contribution Amount</th>
            <th>Employee Count</th>
        </tr>
    </thead>
    <tbody>
        @foreach($modification_request->redistributions as $redistribution)
        <tr>
            <td>{{($redistribution->fin_code_id == 1) ? 'Interest' : 'Contribution'}}</td>
            <td>{!! \Carbon\Carbon::parse($redistribution->contrib_month)->format('F, Y') !!}</td>
            <td>{{number_format($redistribution->amount,2)}}</td>
            <td>{{empty($redistribution->member_count) ? '-': $redistribution->member_count}}</td>
        </tr>
        @endforeach
    </tbody>
</table>


