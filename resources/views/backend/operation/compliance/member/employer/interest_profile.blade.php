@extends('layouts.backend.main', ['title' => trans('labels.backend.table.interest'), 'header_title' => trans('labels.backend.table.interest')])

@include('backend.includes.datatable_assets')
@include('backend/includes/assets/sweetalert_assets')

@section('content')


    <div class = "row">
        {{--{!! Form::model($booking_interest, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <strong> {!! Form::label( 'name',  ($booking_interest->booking()->count()) ? $booking_interest->booking->employer->name : ' ', [ 'id'=> 'name'])
                !!}</strong>

                <small >
                    Reg #: {!! Form::label( 'reg_no',  ($booking_interest->booking()->count()) ? $booking_interest->booking->employer->reg_no : ' ', [ 'id'=> 'reg_no'])
                     !!}
                </small>
            </h5>
        </div>

        {{--Main Content--}}
        {{--menu bar--}}
        <div class = "row">
            <div class="col-md-12">
                {{--menu Bar--}}
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >

                            <div class="pull-right" >
                                @if($check_pending_level1)
                                    {{--<span>--}}
                                           {{--{!! HTML::decode(link_to_route('backend.compliance.employer.interest.adjusted_manually', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Adjusted Manually', $booking_interest->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to close workflow for this entry?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}--}}
                                    {{--</span>--}}
                                @endif

                                    {{--iNITIATE LETTER--}}
                                    @if($check_if_can_initiate_wf_letter == true &&  $booking_interest->status_if_letter_initiated_for_adjust == false)
                                        {{--<a target=""  href=" {{ route("backend.letter.process", [$booking_interest->id, "CLIEMPINTEADJ"]) }}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-file"></i>&nbsp;Response Letter</a>--}}
                                    @endif



                                    {{--adjust interest--}}
                                @if($booking_interest->isadjusted == 0 && $booking_interest->hasreceipt == 1 && $booking_interest->ispaid == 0 && ($check_pending_level1 == 1 || $check_workflow == 0))
                                    <span>
						        {{--<a href="{!! route('backend.compliance.employer.adjust_interest',$booking_interest->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-sliders"></i>&nbsp;@lang('buttons.general.adjust_interest')</a>--}}
					            	</span>
                                @endif
                                {{--close--}}
                                <span>

                                    {!! link_to('compliance/employer/profile/' . $booking_interest->booking->employer_id . '#interests',trans('buttons.general.close'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn nav_button icon fa fa-close',  ]) !!}
						</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div>&nbsp;</div>
        {{--interest summary--}}
        {{--[Includes => interest] interest Summary table--}}

        <div class="row">
            {{--left interest  summary--}}
            <div class="col-md-6">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered">
                        <tbody>
                        {{--rcv date--}}
                        <tr>
                            <td width="180px">@lang('labels.backend.finance.receipt.contrib_month')</td>
                            <th>{!! Form::label( 'rcv_date',
                            ($booking_interest->booking()->count()) ? $booking_interest->booking->rcv_date_formatted : ' ', [
                            'id'=>
                                'rcv_date'])
                                !!}  &nbsp;   <span> {!!
                                $booking_interest->contribution_status_label
                                !!}</span></th>
                        </tr>
                        {{--interest percent--}}
                        <tr>
                            <td>@lang('labels.backend.table.late_months')</td>
                            <th>{!! Form::label( 'late_months',
                            ($booking_interest->late_months > 0) ? $booking_interest->late_months : '0 ', [
                                'id'=>
                'late_months']) !!}</th>
                        </tr>
                        {{--amount--}}
                        <tr>
                            <td>@lang('labels.backend.table.interest')</td>
                            <th>{!! Form::label( 'interest',
                            $booking_interest->amount_formatted, [ 'id'=>
                            'interest']) !!}</th>
                        </tr>
                        {{--paid amount--}}
                        <tr>
                            <td>@lang('labels.backend.table.amount_paid')</td>
                            <th>{!! Form::label( 'amount_paid', $paid_amount ,[ 'id'=>
                                'amount_paid']) !!}</th>
                        </tr>
                        {{--remain amount--}}
                        <tr>
                            <td>@lang('labels.general.balance')</td>
                            <th>{!! Form::label( 'remain_amount', $remain_amount ,[ 'id'=>
                                'remain_amount']) !!}</th>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
            {{--right receipt summary--}}
            {{--Only if interest is adjusted dispalye this--}}
            @if(!empty($booking_interest->adjust_amount))
                <div class="col-md-6">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered" style="width:100%">
                            <tbody>
                            {{--header adjustment--}}
                            <tr>
                                <td align="center"><b>@lang('labels.backend.table.interest_adjustment_header')  &nbsp;   <span> {!!
                                $booking_interest->adjustment_status_label
                                !!}</span></b></td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table table-striped table-bordered">
                            <tbody>
                            {{--adjusted amount--}}
                            <tr>
                                <td width="180px">@lang('labels.backend.table.adjust_amount')</td>
                                <th>{!! Form::label( 'adjust_amount', (
                                $booking_interest->adjust_amount_formatted), [
                                'id'=>
                                'adjust_amount']) !!}</th>
                            </tr>
                            {{--Display old and New Interest Interchangebly when isadjusted --}}
                            {{--Old Interest--}}
                            @if ($booking_interest->isadjusted == 1)
                                <tr>
                                    <td width="180px">@lang('labels.backend.table.old_interest')</td>
                                    <th>{!! Form::label( 'old_interest', number_format( ( $booking_interest->amount - ( $booking_interest->adjust_amount == 0 ? 0 :  $booking_interest->adjust_amount) ), 2 , '.' , ',' ), ['id'=> 'old_interest']) !!}</th>
                                </tr>
                            @else
                                {{--New Interest--}}
                                <tr>
                                    <td width="180px">@lang('labels.backend.table.new_interest')</td>
                                    <th>{!! Form::label( 'new_interest',number_format( ( $booking_interest->amount + $booking_interest->adjust_amount ), 2 , '.' , ',' ), [
                                'id'=>
                                'new_interest']) !!}</th>
                                </tr>
                            @endif

                            @if ($booking_interest->is_board_waived == 1)
                                {{--old late months--}}
                                <tr>
                                    <td>@lang('labels.backend.table.old_late_months')</td>
                                    <th>{!! $booking_interest->old_late_months
                    !!}</th>
                                </tr>
                            @endif

                            {{--new payment date--}}
                            <tr>
                                <td>@lang('labels.backend.table.new_payment_date')</td>
                                <th>{!! Form::label( 'new_payment_date', ($booking_interest->new_payment_date)? $booking_interest->new_payment_date : ' ', [
                                'id'=>
                                'new_payment_date'])
                    !!}</th>
                            </tr>

                            <tr>
                                <td>@lang('labels.general.reason')</td>
                                <th>{!! Form::label( 'reason_type', ( $booking_interest->reason_type_cv_id) ? $booking_interest->reasonType->name : null, [ 'id'=>
                                'reason_type'])
                    !!}</th>
                            </tr>

                            {{--adjust note--}}
                            <tr>
                                <td>Remark</td>
                                <th>{!! Form::label( 'adjust_note', $booking_interest->adjust_note, [ 'id'=>
                                'adjust_note'])
                    !!}</th>
                            </tr>

                            {{--Leter--}}
                            @if($booking_interest->adjustmentResponseLetter()->count() > 0)
                                <tr>
                                    <td>Letter</td>
                                    <td>
                                        <a style="color:blue" href="{{ route('backend.letter.show', $booking_interest->adjustmentResponseLetter->id) }}" >{{ $booking_interest->adjustmentResponseLetter->reference }}</a></td>
                                </tr>
                            @endif

                            </tbody></table>

                    </div>
                </div>

            @endif
        </div>


        {{--Bottom tab navigation--}}

        <div class = "row">
            <div class="col-md-12">
                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.legend.workflow')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#bottom_tab-2" data-toggle="tab">@lang('labels.backend.legend.interest_other_details')</a>
                        </li>

                        {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="#doc_center" data-toggle="tab">Document Center</a>--}}
                        {{--</li>--}}
                    </ul>
                    <div class="nav_tab_contain tab-content">
                        {{--tab 1--}}
                        <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                            {{--main workflow tracks--}}
                            <br/>
                            {{--@php--}}
                            {{--$workflowinput = ['resource_id' => $booking_interest->id, 'wf_module_group_id'=> 2, 'type' => -1];--}}
                            {{--@endphp--}}
                            {{--{!! $workflowtrack->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}--}}
                        </div>

                        {{--tab 2--}}
                        {{--interest details summary  -> Other Details--}}
                        <div id="bottom_tab-2" class="nav_tab_pane tab-pane">
                            <div>&nbsp;</div>
                            @include('backend.operation.compliance.member.employer.includes.interest_details_summary')

                        </div>

                        {{--<div id="doc_center" class="nav_tab_pane tab-pane">--}}
                            {{--<div>&nbsp;</div>--}}
                            {{--@include('backend/system/document/general/includes/document_center_gen', ['resource_id' => $booking_interest->id, 'reference'=> 'DRINTEREADJ', 'allow_modify' => (($check_pending_level1 == 1 || $check_workflow == 0) ? true : false)])--}}

                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
        {{--{!! Form::close() !!}--}}
    </div>


@stop


@push('after-script-end')

    {{--<script  type="text/javascript">--}}

    {{--$(function() {--}}
    {{--$('#workflow-table').DataTable({--}}
    {{--processing: true,--}}
    {{--serverSide: true,--}}
    {{--stateSave: true,--}}
    {{--stateSaveCallback: function (settings, data) {--}}
    {{--localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));--}}
    {{--},--}}
    {{--stateLoadCallback: function (settings) {--}}
    {{--return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));--}}
    {{--},--}}
    {{--ajax:{--}}
    {{--url : '{!! route('backend.compliance.employer.get') !!}',--}}
    {{--type : 'get'--}}


    {{--},--}}
    {{--columns: [--}}
    {{--{ data: 'name' , name: 'name'},--}}
    {{--{ data: 'reg_no', name: 'reg_no'},--}}
    {{--{ data: 'date_of_commencement', name: 'date_of_commencement'},--}}
    {{--{ data: 'country', name: 'country' },--}}
    {{--{ data: 'region', name: 'region' }--}}
    {{--],--}}
    {{--"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {--}}
    {{--$(nRow).click(function() {--}}
    {{--document.location.href = "employer/"+ "profile/" + aData['id'] ;--}}
    {{--}).hover(function() {--}}
    {{--$(this).css('cursor','pointer');--}}
    {{--}, function() {--}}
    {{--$(this).css('cursor','auto');--}}
    {{--});--}}
    {{--}--}}

    {{--});--}}

    {{--});--}}
    {{--</script>;--}}

@endpush
