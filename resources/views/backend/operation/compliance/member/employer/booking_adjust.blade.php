@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance.booking_adjust'), 'header_title' => trans('labels.backend.compliance.booking_adjust')])

@section('after-styles-end')

@endsection

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($booking,['route' => ['backend.compliance.employer.booking_adjust', $booking->id],'method'=>'put',
   ]) !!}
    <div class="row">

        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <strong> {!! Form::label( 'name',$booking->employer->name , [ 'id'=> 'name'])
                !!}</strong>

                <small >
                    Reg #: {!! Form::label( 'reg_no',  $booking->employer->reg_no , [ 'id'=> 'reg_no'])
                     !!}
                </small>
            </h5>

        </div>
        <legend></legend>
        <div>&nbsp;</div>

        <div class="row">
            {{--current booking--}}
            <div class="col-md-9" >
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.compliance.current_booking'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text','amount', $booking->amount_formatted, ['class' => 'form-control', 'disabled' => true]) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            {{--new booking--}}
            <div class="col-md-9" >
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.compliance.new_booking_amount'):</label></div>
                    <div class="col-xs-7 col-lg-7 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text','new_booking_amount', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('new_booking_amount', '<span class="help-block label
                                             label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            {{--comments--}}
            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.table.comments'):</label></div>
                    <div class="col-xs-7 col-lg-7 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'comments', null, [ 'id'=> 'comments',  'class' =>'form-control']) !!}
                            {!! $errors->first('comments', '<span class="help-block label
                               label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--Buttons--}}
        <div class="row">
            <div class="col-md-6" class="form-inline" >
                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                    <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                        <div class="pull-right">

                            {!! link_to_route('backend.compliance.employer.booking_profile',trans('buttons.general.cancel'), [$booking->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')
<script  type="text/javascript">
    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();
    //todoc
    $('#text_content_todo').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();

</script>;


@endpush
