@if ($queries->count())
    <div data-plugin="custom-scroll" data-height="100">
        {{--data-height="100"--}}
        <table class="table table-striped table-bordered">
            <tbody>
            @foreach($queries->get() as $query)
                <tr>
                    <td><b>{{ $query->user->name }}</b></td>
                    <td>{{ $query->comments }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="alert-left-border">
        <div class="alert alert-primary alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>No Comments.</strong> This incident has no any associated comments.
        </div>
    </div>
@endif
