@extends('layouts.backend.main', ['title' => "Advance Payment Profile", 'header_title' => "Advance Payment Profile"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    <style>
        /* start: File Icons CSS */
        #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
        #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
        #folder-tree .file-pdf { background-position: -32px 0 }
        #folder-tree .file-as { background-position: -36px 0 }
        #folder-tree .file-c { background-position: -72px -0px }
        #folder-tree .file-iso { background-position: -108px -0px }
        #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
        #folder-tree .file-cf { background-position: -162px -0px }
        #folder-tree .file-cpp { background-position: -216px -0px }
        #folder-tree .file-cs { background-position: -236px -0px }
        #folder-tree .file-sql { background-position: -272px -0px }
        #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
        #folder-tree .file-h { background-position: -488px -0px }
        #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
        #folder-tree .file-php { background-position: -108px -18px }
        #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
        #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
        #folder-tree .file-rb { background-position: -180px -18px }
        #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
        #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
        #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
        #folder-tree .file-js { background-position: -434px -18px }
        #folder-tree .file-css { background-position: -144px -0px }
        #folder-tree .file-fla { background-position: -398px -0px }
        /* end: File Icon CSS */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12">
            {{--HEADER--}}
            @include("backend.operation.compliance.member.employer.includes.header_info", ['employer'=> $advance_payment->employer])
            <br/>
            <br/>
            <div class="row">
                <div class="col-md-9">
                    <legend class="grey_info" >@lang('labels.backend.member.verification.requested')&nbsp;:&nbsp;<strong>{{ $advance_payment->user_formatted }}</strong></legend>

                    {{--Document Center Here--}}
                    {{--<legend>@lang('labels.backend.member.verification.document_helper')</legend>--}}

                    <div class="row">
                        <div class="col-md-12">

                            <legend>Documents for advance payment request</legend>
                            <div id="folder-tree" class="nopadding"></div>

                            <br/>
                            <br/>
                        </div>
                    </div>

                    {{--Workflow Here--}}
                    @php
                        $workflowinput = ['resource_id' => $advance_payment->id, 'wf_module_group_id'=> 16, 'type' => -1];
                    @endphp
                    {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

                </div>
                <div class="col-md-3">
                    @include("backend/operation/compliance/member/employer/includes/advance_payment_summary")
                </div>
            </div>

        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}

    <script>
        $(function(){
            let $folder = $('#folder-tree');

            /************************** start: Document Library JS ***************************/
            $folder.jstree({
                'core': {
                    'data': {
                        'url': "{!! route('backend.compliance.employer.advance_payment_document_library', $advance_payment->id) !!}",
                        'data': function (node) {
                            return {'id': node.id};
                        }
                    },
                    'check_callback': function (o, n, p, i, m) {
                        if (m && m.dnd && m.pos !== 'i') {
                            return false;
                        }
                        if (o === "move_node" || o === "copy_node") {
                            if (this.get_node(n).parent === this.get_node(p).id) {
                                return false;
                            }
                        }
                        return true;
                    },
                    'force_text': true,
                    'themes': {
                        'responsive': true,
                        'variant': 'small',
                        'stripes': true
                    }
                },
                'sort': function (a, b) {
                    return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
                },
                'contextmenu': {
                    'items': function (node) {
                        var $menu = {};
                        if (this.get_type(node) === "file") {
                            $menu = {
                                "open" : {
                                    "label" : "{!! ucfirst(strtolower(trans("labels.general.open"))) !!}",
                                    "action" : function ($data) {
                                        window.open(base_url + "/compliance/employer/advance_payment/{!! $advance_payment->id !!}/" + node.id + "/open_document", '_blank');
                                    }
                                },
                                "download" : {
                                    "label" : "{!! trans("labels.general.download") !!}",
                                    "action" : function ($data) {
                                        window.open( base_url + "/compliance/employer/advance_payment/{!! $advance_payment->id !!}/" + node.id + "/download_document");
                                    }
                                }
                            };
                        }
                        return $menu;
                    }
                },
                'types': {
                    'default': {'icon': 'folder'},
                    'file': {'valid_children': [], 'icon': 'file'}
                },
                'unique': {
                    'duplicate': function (name, counter) {
                        return name + ' ' + counter;
                    }
                },
                'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']
            });
        });
    </script>

@endpush