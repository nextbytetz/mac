@extends('layouts.backend.main', ['title' => trans('labels.backend.table.write_off_interest'), 'header_title' => trans('labels.backend.table.write_off_interest')])

@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">
        {{--{!! Form::model($interest_write_off, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put'])--}}
         {{--!!}--}}
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <strong> {!! Form::label( 'name', $employer->name, [ 'id'=> 'name'])
                !!}</strong>

                <small >
                    Reg #: {!! Form::label( 'reg_no', $employer->reg_no, [ 'id'=> 'reg_no'])
                     !!}
                </small>
            </h5>
        </div>

        {{--Main Content--}}
        {{--menu bar--}}
        <div class = "row">
            <div class="col-md-12">
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >

                            <div class="pull-right" >

                                {{--Modify--}}
                                <span>
						        <a href="{!! route( 'backend.compliance.employer.written_off_interest_edit', $interest_write_off->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;@lang('buttons.general.modify')</a>
						</span>

                                {{--close--}}
                                <span>
						        <a href="{!! route( 'backend.compliance.employer.profile', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
						</span>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div>&nbsp;</div>
        {{--interest write off summary--}}

        {{--[Includes => interest] interest write off Summary table--}}

        <div class="row">
            {{--left interest  write off summary--}}
            <div class="col-md-6">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered">
                        <tbody>
                        {{--total interest--}}
                        <tr>
                            <td width="180px">@lang('labels.backend.table.total_interest')</td>
                            <th>{!! Form::label( 'total_interest',        number_format($interest_write_off->bookingInterests->sum('amount') , 2 , '.' , ',' )  , [ 'id'=>'total_interest'])
                                !!}</th>
                        </tr>

                        {{--late Months--}}
                        <tr>
                            <td>@lang('labels.backend.table.total_months')</td>
                            <th>{!! Form::label( 'late_months', $interest_write_off->bookingInterests()->count(), [
                                'id'=>'late_months']) !!}</th>
                        </tr>
                        {{--From Date--}}
                        <tr>
                            <td>@lang('labels.backend.table.from_date')</td>
                            <th>{!! Form::label( 'from_date',  Carbon\Carbon::parse($interest_write_off->bookingInterests->min('miss_month'))->format('d-M-Y')
                            , [
                            'id'=> 'from_date']) !!}</th>
                        </tr>
                        {{--To Date--}}
                        <tr>
                            <td>@lang('labels.backend.table.to_date')</td>
                            <th>{!! Form::label( 'to_date', Carbon\Carbon::parse($interest_write_off->bookingInterests->max('miss_month'))->format('d-M-Y') ,[ 'id'=> 'to_date']) !!}</th>
                        </tr>
                        </tbody></table>

                </div>
            </div>


            <div class="col-md-6">
                <div class="col-md-12">

                    <table class="table table-striped table-bordered">
                        <tbody>
                        {{--username--}}
                        <tr>
                            <td width="180px">@lang('labels.backend.table.written_off_by')</td>
                            <th>{!! Form::label( 'written_off_by', ($interest_write_off->user()->count()) ? $interest_write_off->user->username : ' ', [
                                'id'=>
                                'written_off_by']) !!}</th>
                        </tr>
                        {{--description--}}
                        <tr>
                            <td>@lang('labels.backend.table.comments')</td>
                            <th>{!! Form::label( 'comments', $interest_write_off->description, [
                                'id'=>
                                'comments'])
                    !!}</th>

                        </tbody></table>

                </div>
            </div>

        </div>


        {{--Bottom tab navigation--}}

        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active"  id="bottom_tab_1_header" href="#bottom_tab_1" data-toggle="tab">@lang('labels.backend.legend.workflow')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="bottom_tab_2_header" href="#bottom_tab_2" data-toggle="tab">@lang('labels.backend.table.written_off_interest')</a>
                        </li>
                    </ul>
                    <div class="nav_tab_contain tab-content">
                        <div id="bottom_tab_1" class="nav_tab_pane tab-pane active in">

                                                    {{--main workflow tracks--}}
                            <br/>
                            @php
                                $workflowinput = ['resource_id' => $interest_write_off->id, 'wf_module_group_id'=> 1, 'type' => -1];
                            @endphp
                            {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

                        </div>

                        <div id="bottom_tab_2" class="nav_tab_pane tab-pane">

                            {{--interests written offs--}}
                            @include('backend.operation.compliance.member.employer.includes.written_off_interest')

                        </div>

                    </div>


                </div>

            </div>

        </div>

        {{--{!! Form::close() !!}--}}
    </div>







@stop


@push('after-script-end')

@stack('written-off-interest-script-end')
{{--<script  type="text/javascript">--}}

{{--$(function() {--}}
{{--$('#workflow-table').DataTable({--}}
{{--processing: true,--}}
{{--serverSide: true,--}}
{{--stateSave: true,--}}
{{--stateSaveCallback: function (settings, data) {--}}
{{--localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));--}}
{{--},--}}
{{--stateLoadCallback: function (settings) {--}}
{{--return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));--}}
{{--},--}}
{{--ajax:{--}}
{{--url : '{!! route('backend.compliance.employer.get') !!}',--}}
{{--type : 'get'--}}


{{--},--}}
{{--columns: [--}}
{{--{ data: 'name' , name: 'name'},--}}
{{--{ data: 'reg_no', name: 'reg_no'},--}}
{{--{ data: 'date_of_commencement', name: 'date_of_commencement'},--}}
{{--{ data: 'country', name: 'country' },--}}
{{--{ data: 'region', name: 'region' }--}}
{{--],--}}
{{--"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {--}}
{{--$(nRow).click(function() {--}}
{{--document.location.href = "employer/"+ "profile/" + aData['id'] ;--}}
{{--}).hover(function() {--}}
{{--$(this).css('cursor','pointer');--}}
{{--}, function() {--}}
{{--$(this).css('cursor','auto');--}}
{{--});--}}
{{--}--}}

{{--});--}}

{{--});--}}
{{--</script>;--}}

@endpush
