@extends('layouts.backend.main', ['title' => "Online Notification Application Profile", 'header_title' => "Online Notification Application Profile"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{--{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}--}}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/7.33.1/css/sweetalert2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: File Icons CSS */
        #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
        #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
        #folder-tree .file-pdf { background-position: -32px 0 }
        #folder-tree .file-as { background-position: -36px 0 }
        #folder-tree .file-c { background-position: -72px -0px }
        #folder-tree .file-iso { background-position: -108px -0px }
        #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
        #folder-tree .file-cf { background-position: -162px -0px }
        #folder-tree .file-cpp { background-position: -216px -0px }
        #folder-tree .file-cs { background-position: -236px -0px }
        #folder-tree .file-sql { background-position: -272px -0px }
        #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
        #folder-tree .file-h { background-position: -488px -0px }
        #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
        #folder-tree .file-php { background-position: -108px -18px }
        #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
        #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
        #folder-tree .file-rb { background-position: -180px -18px }
        #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
        #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
        #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
        #folder-tree .file-js { background-position: -434px -18px }
        #folder-tree .file-css { background-position: -144px -0px }
        #folder-tree .file-fla { background-position: -398px -0px }
        /* end: File Icon CSS */
    </style>
@endpush

@php
    $canmerge = ($incident->status == 3 || $incident->status == 5);
@endphp

@if($canmerge)
    {{--start : start a modal for merging incident--}}
    <div class="modal fade bd-example-modal-lg"  tabindex="-1" id="merge_incident" role="dialog" aria-labelledby="merge_incident" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Merge Incident</h4>
                </div>
                {!! Form::open(['route' => ['backend.compliance.employer.merge_incident', $incident->id],  'id' => 'create_merge_incident', 'name' => 'create_merge_incident']) !!}
{{--                {!! Form::hidden('incident_id', $incident->id) !!}--}}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="padding-right:20px; border-right: 1px solid #ddd;">
                            <div class="fileld-layout">
                                <label class="required">File Name&nbsp;/&nbsp;Case Number&nbsp;<small>e.g. OAC 111, jitu kabeja e.t.c</small></label>
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::select('incident', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select-incident','id'=> 'search-select-incident', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary site-btn" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary site-btn"><i class='icon fa fa-arrow-right' aria-hidden='true'></i>&nbsp;Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--    end : start a modal for merging incident--}}
@endif

@section('content')
    <!-- Put the page specifically for this page here -->
    {{--HEADER--}}
    {{--@include("backend.operation.compliance.member.employer.includes.header_info", ['employer'=> $incident->employer])--}}
    <div class = "row">
        {{--{!! Form::model($employee, ['route' => ['backend.compliance.employee.profile', $employee->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        <div class="col-md-12 col-sm-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                <strong> {!! Form::label( 'name', $employee->firstname . " " . $employee->middlename . " " . $employee->lastname , [ 'id'=> 'name']) !!}</strong>
                <small>
                    @lang('labels.backend.table.member_#'): <span class="underline">{!! Form::label( 'reg_no', $employee->memberno, [ 'id'=> 'reg_no']) !!}</span>
                </small>
                <small>
                    Employer&nbsp;:&nbsp;<span class="underline">{{ $employer->name }}</span>
                </small>
            </h5>
        </div>
    </div>
    {{--<br/>--}}
    {{--<br/>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="divider15"></div>
            <div class="nav-tab-pills-image">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="notification_process_tab" data-toggle="tab" href="#general_information" role="tab">
                            <i class="icon fa fa-info" aria-hidden="true"></i>General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="general_information_tab" data-toggle="tab" href="#document_centre" role="tab">
                            <i class="icon fa fa-folder-open" aria-hidden="true"></i>Document Centre
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">

                <div class="tab-pane active" id="general_information" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12 pills-height">

                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >
                                            {{--Action Links--}}
                                            @if($canmerge)
                                                {{-- Incident Declined or Declined & Merged, Possibly Merge Notification --}}
                                                <span>
                                                    <a href="#"  class="btn btn-secondary site-btn nav_button" data-toggle="modal" data-target="#merge_incident" >
                                                        <i class="icon fa fa-dot-circle-o" aria-hidden="true"></i>&nbsp;Merge Incident
                                                    </a>
                                                </span>
                                            @endif
                                            @if ($incident->notification_report_id)
{{--                                                Notification Profile--}}
                                                <span>
                                                    <a href="{!! route('backend.claim.notification_report.profile', $incident->notification_report_id) !!}"  class="btn btn-primary site-btn nav_button" >
                                                        <i class="icon fa fa-level-up" aria-hidden="true"></i>&nbsp;Notification Profile</a>
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    <legend class="grey_info" >@lang('labels.backend.member.verification.requested')&nbsp;:&nbsp;<strong>{{ $incident->user_formatted }}</strong></legend>

                                    @if($incident->status == 1)
                                        <br/>
                                        {{--incident is approved, post comment and suggest documents to upload--}}
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{--Notification Stagings Logs--}}
                                                <legend class="grey_modal" >Corresponding</legend>
                                                <div class="label label-info">Notification <b>{{ $incident->NotificationReport->filename }}</b> </div>
                                                <legend></legend>
                                                <div id="comments_contents">
                                                    @include("backend.operation.compliance.member.employer.incident_comment", ['queries' => $incident->queries()])
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-row">
                                                    {!! Form::open(['role' => 'form', 'id' => 'incident_comment_form', 'route' => ['backend.compliance.employer.post_incident_comment', $incident->id]]) !!}
                                                        <div class="form-group col-md-10">
                                                            <label for="incident_comment">Comments:</label>
                                                            {!! Form::textarea('comments', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;', 'id' => 'incident_comment']) !!}
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label for="incident_comment_submit">&nbsp;</label>
                                                            <input type="submit" class="form-control btn btn-success btn-sm" value="Submit" id="incident_comment_submit">
                                                        </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>

                                        <legend>Additional Optional Documents</legend>
                                        <div  style="border: 1px dotted #000;padding: 2px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @foreach (array_chunk($documents->toArray(), count($documents)/4) as $docs)
                                                        <div class="col-md-3">
                                                            <ul style="margin:0;padding:0;list-style:none;">
                                                                @foreach($docs as $doc)
                                                                    <li>
                                                                        <input type="checkbox" value="{{$doc['id']}}" name="optional_docs[]" {{in_array($doc['id'], $opted_docs) ? 'checked' : ""}} id="document-{{ $doc['id'] }}" data-incident="{{ $incident->id }}" class="optional_docs" />
                                                                        <label for="document-{{ $doc['id'] }}"> {{ $doc['name'] }} </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{--Put Client Conversation Here--}}

                                    {{--Workflow Here--}}
                                    @php
                                        $workflowinput = ['resource_id' => $incident->id, 'wf_module_group_id'=> 17, 'type' => -1];
                                    @endphp
                                    {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

                                </div>
                                <div class="col-md-3">
                                    @include("backend/operation/compliance/member/employer/includes/incident_summary")
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="document_centre" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12 pills-height">

                            <div class="row">
                                <div class="col-md-4">

                                    <legend>Attached document</legend>
                                    <div id="folder-tree" class="nopadding"></div>

                                    <br/>
                                    <br/>
                                </div>
                                <div class="col-md-8">
                                    {{--Document Preview--}}
                                    <legend>Document Preview</legend>
                                    <br/>
                                    <div id="document_frame">
                                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/7.33.1/js/sweetalert2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script>

    $(function() {
        let $body = $('body');
        @if($canmerge)
            $('.search-select-incident').select2({
                dropdownParent: $('#merge_incident'),
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.claim.notification_report.search_registered_notification') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        $body.on('submit', 'form[name=create_merge_incident]', function ($e) {
            $e.preventDefault();
            let $form = this;
            $($form).find('i').removeClass("fa-arrow-right").addClass('fa-spinner fa-spin');
            $($form).find('button').prop("disabled", true);
            /* start: remove any printed error message in the input controls */
            $($form).find(':input').each(function () {
                $(this).closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
            });
            /* end: remove any printed error message in the input controls */
            let $options = {
                dataType : "json",
                type : "POST",
                url : $($form).attr("action"),
                beforeSend : function ($e) {
                    $($form).find(".btn-submit").prop('disabled', true);
                },
                success : function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    /*console.log($data);*/
                    if (!$data.success) {

                    } else {
                        /*document.location.href =  "";*/
                        location.reload();
                    }
                    $($form).find('i').removeClass("fa-spinner fa-spin").addClass('fa-arrow-right');
                    $($form).find('button').prop("disabled", false);
                },
                error: function ($data) {
                    $($form).find(".btn-submit").prop('disabled', false);
                    let errors = $.parseJSON($data.responseText);
                    /*console.log(errors);*/
                    $.each(errors, function($index, $value) {
                        $($form).find(':input[name^="' + $index + '"]').closest(".form-group").addClass("has-danger").find(".help-block").append("<small class='tag tag-danger'>" + $value + "</small>");
                        if ($index === 'general_error') { $($form).prepend('<div class="alert alert-danger general_error" role="alert">' + $value + '</div>'); $('.general_error').fadeOut(11000); }
                    });
                    $($form).find('i').removeClass("fa-spinner fa-spin").addClass('fa-arrow-right');
                    $($form).find('button').prop("disabled", false);
                },
                uploadProgress : function (event, position, total, percentComplete) {
                }
            };
            $($form).ajaxSubmit($options);
        });
        @endif

        var $folder = $('#folder-tree');
        autosize($("textarea.autosize"));

        $('#incident_comment_form').on('submit', function($e) {
            $e.preventDefault();
            var $form = this;
            var $options = {
                dataType : "html",
                type : "POST",
                url : $($form).attr("action"),
                success : function ($data) {
                    $("#comments_contents").html($data);
                    $("#incident_comment").val("");
                },
                error: function ($data) {

                }
            };
            // pass options to ajaxForm
            $($form).ajaxSubmit($options);
        });

        $('.optional_docs').change(function() {
            let $check = 0;
            let $documentId = $(this).val();
            if ($(this).is(":checked")) {
                $check = 1;
            } else {
                $check = 0;
            }
            optional_document($check, $documentId).done(function ($data) {
                /*console.log($data);*/
            });
        });

        /************************** start: Document Library JS ***************************/
        $folder.jstree({
            'core': {
                'data': {
                    'url': "{!! route('backend.compliance.employer.incident_document_library', $incident->id) !!}",
                    'data': function (node) {
                        return {'id': node.id};
                    }
                },
                'check_callback': function (o, n, p, i, m) {
                    if (m && m.dnd && m.pos !== 'i') {
                        return false;
                    }
                    if (o === "move_node" || o === "copy_node") {
                        if (this.get_node(n).parent === this.get_node(p).id) {
                            return false;
                        }
                    }
                    return true;
                },
                'force_text': false,
                'themes': {
                    'responsive': true,
                    'variant': 'small',
                    'stripes': true
                }
            },
            'sort': function (a, b) {
                return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            'contextmenu': {
                'items': function ($node) {
                    var $menu = {};
                    if (this.get_type($node) === "file") {
                        $menu = {
                            "validate" : {
                                "label" : "Validate",
                                "action" : function ($data) {
                                    /*console.log(node.original);*/
                                    if ($node.original.isverified === 0 || $node.original.isverified === 2) {
                                        Swal({
                                            title: $node.original.document_name,
                                            text: "Verifying Document",
                                            type: "info",
                                            showCloseButton: true,
                                            focusConfirm: false,
                                            showCancelButton: true,
                                            cancelButtonText: "Unverify",
                                            cancelButtonColor: "#dd733e",
                                            confirmButtonColor: "#5add42",
                                            confirmButtonText: "Verify",
                                            reverseButtons: true,
                                        }).then(($result) => {
                                            let $status = 0;
                                            let $send = 1;
                                            if ($result.value) {
                                                /*console.log("Verified");*/
                                                $status = 1;
                                            } else if ($result.dismiss === Swal.DismissReason.cancel) {
                                                /*console.log("Unverified");*/
                                                $status = 2;
                                            }
                                            verify_document($node.original.id, $send, $status).done(function ($data) {
                                                /*console.log($data);*/
                                                /* refresh jstree */
                                                if ($data.success) {
                                                    $folder.jstree("refresh");
                                                }
                                            });
                                        });
                                    } else {
                                        Swal('Document already verified.')
                                    }
                                }
                            }
                        };
                    }
                    return $menu;
                }
            },
            'types': {
                'default': {'icon': 'folder'},
                'file': {'valid_children': [], 'icon': 'file'}
            },
            'unique': {
                'duplicate': function (name, counter) {
                    return name + ' ' + counter;
                }
            },
            'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']
        });
        $folder.bind("click.jstree", function ($event, $data) {
            var $node = $($event.target).closest("li").attr("id");
            var $type = $folder.jstree().get_selected(true)[0].type;
            if ($type === 'default') {
                /*should only work on folders*/
                /*Document Folder Has Been Clicked*/
                $node = $node.split('/');
                $node = $node[$node.length - 1];
                if ($node.length > 0) {
                    //alert(1);
                } else {
                    //alert(2);
                }
            } else {
                /*preview the document here*/
                /*console.log($node);*/
                let $document_frame = $("#document_frame");
                get_current_document_from_path($node).done(function ($data) {
                    /*console.log($data);*/
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe allowfullscreen src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            }
        });
    });
    function get_current_document_from_path($node) {
        $refNode = $node;
        /*return $.post( base_url + "/claim/notification_report/current_base64_document/{!! $incident->id !!}/" + $node, {}, function( data ) {
    }, "json");*/
        return $.ajax({
            url: base_url + "/claim/notification_report/current_document_from_path/{!! $incident->id !!}/" + $node,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }
    function verify_document($node, $send, $status) {
        return $.ajax({
            url: base_url + "/claim/notification_report/verify_online_document/{!! $incident->id !!}/" + $node + "/" + $send + "/" + $status,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }
    function optional_document($check, $documentId) {
        return $.ajax({
            url: base_url + "/compliance/employer/{!! $incident->id !!}/incident_optional_docs/" + $check + "/" + $documentId,
            dataType : 'json',
            async : false,
            method : "POST"
        });
    }
</script>

@endpush