@extends('layouts.backend.main', ['title' => trans('labels.backend.member.interest_refund_overview'), 'header_title' => trans('labels.backend.member.interest_refund_overview')])

@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">
        {{--{!! Form::model($booking_interest, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

        {{--Main Content--}}
        {{--menu bar--}}
        <div class = "row">
            <div class="col-md-12">
                {{--menu Bar--}}
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >

                            <div class="pull-right" >

                                {{--Refund--}}
                                <span>
                                        <a href="{!! route('backend.compliance.booking_interest.refunding_page',
                                        $employer->id)
                                        !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-money"></i>&nbsp;@lang('labels.general.refund')</a>
                                        </span>


                                {{--close--}}
                                <span>

                                    {!! link_to('compliance/employer/profile/' . $employer->id. '#general',trans('buttons.general.close'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn nav_button icon fa fa-close',  ]) !!}
						</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div>&nbsp;</div>
        {{--interest summary--}}
        {{--[Includes => interest] interest Summary table--}}

        <div class="row">
            {{--interest Refunds Overview--}}
            <div class="col-md-9">
                @include('backend.operation.compliance.member.employer.receivable.interest.refund.includes.refund_overview')

            </div>

        </div>



        {{--{!! Form::close() !!}--}}
    </div>


@stop


@push('after-script-end')

{{--<script  type="text/javascript">--}}

{{--$(function() {--}}
{{--$('#workflow-table').DataTable({--}}
{{--processing: true,--}}
{{--serverSide: true,--}}
{{--stateSave: true,--}}
{{--stateSaveCallback: function (settings, data) {--}}
{{--localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));--}}
{{--},--}}
{{--stateLoadCallback: function (settings) {--}}
{{--return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));--}}
{{--},--}}
{{--ajax:{--}}
{{--url : '{!! route('backend.compliance.employer.get') !!}',--}}
{{--type : 'get'--}}


{{--},--}}
{{--columns: [--}}
{{--{ data: 'name' , name: 'name'},--}}
{{--{ data: 'reg_no', name: 'reg_no'},--}}
{{--{ data: 'date_of_commencement', name: 'date_of_commencement'},--}}
{{--{ data: 'country', name: 'country' },--}}
{{--{ data: 'region', name: 'region' }--}}
{{--],--}}
{{--"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {--}}
{{--$(nRow).click(function() {--}}
{{--document.location.href = "employer/"+ "profile/" + aData['id'] ;--}}
{{--}).hover(function() {--}}
{{--$(this).css('cursor','pointer');--}}
{{--}, function() {--}}
{{--$(this).css('cursor','auto');--}}
{{--});--}}
{{--}--}}

{{--});--}}

{{--});--}}
{{--</script>;--}}

@endpush
