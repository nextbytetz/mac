@extends('layouts.backend.main', ['title' => trans('labels.backend.member.adjust_interest'), 'header_title' => trans('labels.backend.member.adjust_interest')])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}

    {{ Form::model($interest_adjustment,['route' => ['backend.compliance.interest_adjustment.update', $interest_adjustment],'method'=>'put',
     'id' => 'update', 'name' => 'update']) }}
    {!! Form::hidden('receipt_id', $interest_adjustment->receipt_id ) !!}
    {!! Form::hidden('receipt_date', $interest_adjustment->receipt->rct_date ) !!}
    {!! Form::hidden('this_date', getTodayDate() ) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate()) !!}

    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

    <div><br/></div>
    <div><br/></div>
    <div><br/></div>
    <div class="row">


        {{--receipt no--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Receipt No.:</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text','received_amount',$receipt->rctno, ['class' => 'form-control', 'disabled' => true]) !!}

                        </div>
                    </div>
                </div>

            </div>
        </div>

        {{--Contribution amount--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.received_amount'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text','received_amount',number_2_format($receipt->amount), ['class' => 'form-control', 'disabled' => true]) !!}

                        </div>
                    </div>
                </div>

            </div>
        </div>
        {{--receipt description--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Receipt Description:</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label>{{$receipt->isPayForEmployer()}}</label>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        {{--Payment date--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Payment Date:</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <div class="form-inline">

                                <div class="input-group" style="width:100%;">
                                    {!! Form::text('payment_date_esc',  short_date_format($receipt->rct_date), ['placeholder' => '', 'class' => 'form-control datepicker2', 'autocomplete' => 'off', 'disabled']) !!}

                                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                </div>

                            </div>
                        </div>
                        {!! Form::hidden('payment_date', standard_date_format($receipt->rct_date)) !!}
                        {!! $errors->first('payment_date', '<span class="help-block label
    label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        {{--new payment date--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.new_payment_date'):</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <div class="form-inline">

                                <div class="input-group" style="width:100%;">
                                    {!! Form::text('new_payment_date',   short_date_format($interest_adjustment->new_payment_date), ['placeholder' => '', 'class' => 'form-control datepicker2', 'autocomplete' => 'off']) !!}

                                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                </div>

                            </div>
                        </div>

                        {!! $errors->first('new_payment_date', '<span class="help-block label
    label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            {{--Reason--}}
            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Reason:</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group" id = "">
                            {!! Form::select( 'reason_type_cv_id', $reason_types,$interest_adjustment->reason_type_cv_id, [ 'id'=> 'reason_type_cv_id',  'class' =>'form-control search-select', 'placeholder' => '']) !!}
                            {!! $errors->first('reason_type_cv_id', '<span class="help-block label
                               label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            {{--Reason--}}
            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Remark:</label></div>
                    <div class="col-xs-6 col-lg-6 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'remark', $interest_adjustment->remark, [ 'id'=> 'remark',  'class' =>'form-control']) !!}
                            {!! $errors->first('remark', '<span class="help-block label
                               label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--Buttons--}}
        <div class="row">
            <div class="col-md-6" class="form-inline" >
                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                    <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                        <div class="pull-right">

                            {!! link_to_route('backend.compliance.interest_adjustment.profile',trans('buttons.general.cancel'), [$interest_adjustment->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {!! Form::close() !!}
    {{--</section>--}}


@stop


@push('after-script-end')

    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script  type="text/javascript">
        $(function () {
            $('.search-select').select2();
        });
        $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();
        //todoc
        $('#text_content_todo').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();



        $('body').on('submit', 'form[name=adjust_interest]', function(e) {
            e.preventDefault();
            this.submit();
        });

    </script>;


@endpush
