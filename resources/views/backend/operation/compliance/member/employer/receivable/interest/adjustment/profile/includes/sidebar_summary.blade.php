<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Sidebar Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>


<div class="row">
    <div class="light_grey_bg">&nbsp;</div>

    <div class="light_grey_bg">
        <table  class="table table-bordered table-striped"  style="width:100%">
            <tbody>


            <tr>
                <td style="width:120px;">Receipt No.</td>
                <th>{{ $receipt->rctno }}</th>

            </tr>

            <tr>
                <td>Contributed Amount</td>
                <th>{{ number_2_format($receipt->amount )}}</th>
            </tr>
            <tr>
                <td>Contributed Amount</td>
                <th>{{  $receipt->isPayForEmployer() }}</th>

            </tr>
            <tr>
                <td>Payment Date</td>
                <th>{{ short_date_format($interest_adjustment->payment_date) }}</th>

            </tr>

            <tr>
                <td>New Payment Date</td>
                <th>{{ short_date_format($interest_adjustment->new_payment_date) }}</th>

            </tr>

            <tr>
                <td>Reason</td>
                <th>{{ ($interest_adjustment->reasonType->name) }}</th>

            </tr>

            <tr>
                <td>Remark</td>
                <td>{{ ($interest_adjustment->remark) }}</td>

            </tr>
            {{--Leter--}}
            @if($interest_adjustment->adjustmentResponseLetter()->count() > 0)
                <tr>
                    <td>Response Letter</td>
                    <td>
                        <a style="color:blue" href="{{ route('backend.letter.show', $interest_adjustment->adjustmentResponseLetter->id) }}" >{{ $interest_adjustment->adjustmentResponseLetter->reference }}</a></td>
                </tr>
            @endif

            </tbody>
        </table>
    </div>
</div>





<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Pending Tasks</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>


<div class="row">
    <div class="light_grey_bg">&nbsp;</div>

    <div class="light_grey_bg">
        {{--</table>--}}
        <ol>
            @foreach($pending_tasks as $pending_task)

                <li style="margin-left:10px;">
                    {!! $pending_task !!}
                </li>

            @endforeach

                {{--letter response--}}
                @if($interest_adjustment->status_if_letter_initiated_for_adjust == false &&  $check_if_can_initiate_wf_letter == true)
                    <li style="margin-left:10px;">
                        {!! 'Response letter not initiated yet ' !!}

                    </li>

                @endif

        </ol>
    </div>
</div>
