
{{--interest-writeoff-overview-table--}}

<legend class="grey_modal" >Interests</legend>
<table class="table table-bordered" id ="interest-refunded-table" style="width:100%" >
    <thead>
    <tr>
            <th>@lang('labels.backend.table.receipt.contrib_month')</th>
        <th>Late Months (Before Adjust)</th>
        <th>Interest (Before Adjust)</th>
        <th>@lang('labels.backend.table.booking.late_months')</th>
        <th>@lang('labels.backend.finance.receipt.interest')</th>
        <th>@lang('labels.backend.table.amount_paid')</th>
        <th>Refund Amount</th>
        <th>Rctno(s)</th>
    </tr>
    </thead>

</table>

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#interest-refunded-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: true,
            paging: true,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.booking_interest.get_interest_refunded', $interest_refund->id)
                !!}',
                type : 'get'
            },

            columns: [

                {data: 'contrib_month_formatted' , name: 'miss_month' ,searchable: true, orderable: true},
                { data: 'late_months_before', name: 'late_months_before', searchable: false, orderable: false},
                { data: 'amount_before', name: 'amount_before' , searchable: false, orderable: false},
                { data: 'late_months', name: 'late_months', searchable: false, orderable: false},
                { data: 'amount', name: 'amount' , searchable: false, orderable: false},
                { data: 'amount_paid', name: 'amount_paid' , searchable: false, orderable: false },
                { data: 'refunded_amount', name: 'refunded_amount' , searchable: false, orderable: false },
                { data: 'rctno', name: 'rctno' , searchable: false, orderable: false },


            ],

            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href =   url + "/compliance/employer/profile/interest/" + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }


        } );
    });
</script>;

@endpush