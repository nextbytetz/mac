<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            {!! Form::model($interest_refund,['route' => ['backend.compliance.booking_interest.refund.update_bank_details',$interest_refund->id],'method'=>'put',
'id' => 'update']) !!}


        </div>

        {{--Employee=========backed up===--}}
        {{--@if(count($notification_report->medicalExpenses()->where('member_type_id',2)))--}}
        <div class="col-md-12" >
            <legend class="grey_modal" id="employee" >@lang('labels.backend.compliance.employer')</legend>
            <div>&nbsp;</div>

            <table class="table table-striped table-bordered"  id="employee-bank-details-div" style="width:100%">
                <tbody>
                {{--Header--}}
                <tr>
                    <th>@lang('labels.general.name')</th>
                    <th>@lang('labels.backend.finance.receipt.bank')</th>
                    <th>@lang('labels.backend.table.branch')</th>
                    <th>@lang('labels.backend.table.accountno')</th>
                </tr>
                {{--employee details--}}
                {{--@foreach($notification_report->medicalExpenses()->where('member_type_id',2) as $medical_expense)--}}
                <tr>

                    <td>{!! $employer->name  !!}</td>


                    <td>{!!  Form::select('employer_bank_id', $banks, ($interest_refund->bankBranch()->count()) ? $interest_refund->bankBranch->bank->id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'employer_bank_select'])  !!}</td>
                    {!! $errors->first('employer_bank_id', '<span class="help-block label
                    label-danger">:message</span>') !!}



                    <td>   {!! Form::select('employer_bank_branch_id', ($interest_refund->bank_branch_id) ? $bank_branches : [], $interest_refund->bank_branch_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'employer_bank_branch_select' ]) !!}
                        {!! $errors->first('employer_bank_branch_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </td>


                    <td>{!! Form::input( 'text','employer_accountno', ($interest_refund->accountno) ?
                    $interest_refund->accountno : ' ', ['class' => 'form-control', 'id'=> 'accountno']) !!}
                        {!! $errors->first('employer_accountno', '<span class="help-block label label-danger">:message</span>') !!}</td>

                </tr>


                {{--@endforeach--}}

                </tbody></table>
        </div>
        {{--@endif--}}







        @if($check_workflow == 0 || $check_if_is_level1_pending == 1)

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-9" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.booking_interest.refund_profile',trans('buttons.general.cancel'), [$interest_refund->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </br>
        @endif
        {!! Form::close() !!}
    </div>
</div>
