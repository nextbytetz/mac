@extends('layouts.backend.main', ['title' => trans('labels.backend.member.interest_refund_profile'), 'header_title' => trans('labels.backend.member.interest_refund_profile')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')


    <div class = "row">
        {{--{!! Form::model($employer, ['route' => ['backend.finance.receipt.dishonour', $employer->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}

        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

        <div>&nbsp;</div>
        {{--Tabs navigation--}}
        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        {{--General--}}
                        <li class="nav-item">
                            <a class="nav-link active" href="#general"
                               data-toggle="tab">@lang('labels.general.general')
                            </a>
                        </li>
                        {{--bank details--}}
                        <li class="nav-item">
                            <a class="nav-link"  id = "tab_2_header"  href="#bank_details" data-toggle="tab">@lang('labels.backend.claim.bank_details')</a>
                        </li>

                    </ul>
                    <div class="nav_tab_contain tab-content">
                        <div id="general" class="nav_tab_pane tab-pane active in">
                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >

                                            {{--INITIATE--}}
                                            @if($check_workflow == 0)
                                                <span>

                                                {!!HTML::decode(link_to_route('backend.compliance.booking_interest.refund.initiate',"<i class='icon fa fa-check-circle' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.initiate'), $interest_refund->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'You are about to initiate Interest refund process! Are you Sure?', 'class' => 'btn btn-primary site-btn nav_button ']))
                                                 !!}
        </span>

                                            @endif

                                            @if($check_workflow == 0 || $check_if_is_level1_pending == 1)
                                                {{--Refund--}}
                                                <span>
                                        <a href="{!! route('backend.compliance.booking_interest.refund.edit',
                                        $interest_refund->id)
                                        !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;@lang('labels.general.modify')</a>
                                        </span>
                                                {{--UNDO--}}
                                                <span>

                                                {!!HTML::decode(link_to_route('backend.compliance.booking_interest.refund.undo',"<i class='icon fa fa-undo' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.undo'), $interest_refund->id, ['data-method' => 'confirm','data-type'=>'warning', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => 'You are about to undo Interest Refund! Are you Sure?', 'class' => 'btn btn-primary site-btn nav_button '])) !!}
        </span>
                                            @endif


                                            {{--close--}}
                                            <span>
                                        <a href="{!! route('backend.compliance.booking_interest.refund_overview',
                                        $employer->id)
                                        !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('labels.general.close')</a>
                                        </span>

                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{--interest Refunded--}}
                            <div class = "row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        </br>

                                        @include('backend.operation.compliance.member.employer.receivable.interest.refund.includes.interest_refunded')
                                    </div>
                                </div>
                            </div>

                            {{--SUMMARY--}}

                            <div class = "row">
                                <div class="col-md-12">
                                    </br>
                                    {{--SUMMARY--}}
                                    <table class="display" cellspacing="0" width="100%">
                                        <thead>
                                        <tr style="background-color: grey">
                                            <th style="width:160px">{!! strtoupper(trans('labels.general.total'))
                                            . ' ' . strtoupper(trans('labels.general.refund')) !!}:</th>
                                            <th>{!! number_2_format($interest_refund->amount) !!}</th>
                                            <th style="width:80px">{!! strtoupper(trans('labels.general.date'))
                                            !!}:</th>
                                            <th>{!! $interest_refund->created_at_formatted !!}</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>



                            {{--main tab content--}}
                            <div class = "row">
                                <div class="col-md-12">

                                    {{--main workflow tracks--}}
                                    @php
                                        $workflowinput = ['resource_id' => $interest_refund->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => -1];
                                    @endphp
                                    {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

                                </div>
                            </div>


                        </div>

                        <div id="bank_details" class="nav_tab_pane tab-pane">
                            {{--Bank_detail update--}}
                            @include('backend.operation.compliance.member.employer.receivable.interest.refund.includes.update_bank_details')
                        </div>
                    </div>
                </div>

            </div>

            {{--{!! Form::close() !!}--}}
        </div>
        @stop


        @push('after-script-end')
        {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
        {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--        {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
        <script  type="text/javascript">

            $(function () {
                $(".search-select").select2();

                if (location.hash !== '') {
                    $('a[href="' + location.hash + '"]').tab('show');
                    $('a[href="' + location.hash + '"]').trigger('click');
                }


                $('#employer_bank_select').on('change', function (e) {
                    $("#spin2").show();
                    var bank_id = e.target.value;
                    $.get("{{ url('/') }}/getbankbranch?bank_id=" + bank_id, function (data) {
                        $('#employer_bank_branch_select').empty();
                        $("#employer_bank_branch_select").select2("val", "");
                        $('#employer_bank_branch_select').html(data);
                        $("#spin2").hide();
                    });
                });
            });

        </script>;

    @endpush
