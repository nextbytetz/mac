{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="interests-adjusted-table">
            <thead>
            <tr >
                <th>Contrib Month</th>
                <th>Late Months</th>
                <th>Interest Amount</th>
                <th>New Late Months</th>
                <th>New Interest Amount</th>
            </tr>
            </thead>
        </table>

    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#interests-adjusted-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.interest_adjustment.get_interests_adjusted_dt', $interest_adjustment->id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'miss_month', name: 'miss_month', orderable : false, searchable : false},
                    { data: 'late_months', name: 'late_months', orderable : false, searchable : false},
                    { data: 'amount', name: 'amount', orderable : false, searchable : false},
                    { data: 'new_late_months', name: 'new_late_months', orderable : false, searchable : false},
                    { data: 'new_interest', name: 'new_interest', orderable : false, searchable : false},



                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/profile/interest/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush