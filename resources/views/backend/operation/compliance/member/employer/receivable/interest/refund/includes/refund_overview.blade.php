
{{--interest-writeoff-overview-table--}}

<legend class="grey_modal" >@lang('labels.backend.legend.interest_refund_overview')</legend>
<table class="table table-bordered" id ="interest-refund-overview-table" style="width:100%" >
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.general.amount')</th>
        <th>@lang('labels.general.date')</th>

    </tr>
    </thead>

</table>

@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#interest-refund-overview-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.booking_interest.get_refunds', $employer->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'status' , name: 'status', orderable : false, searchable : false},
                { data: 'amount_formatted' , name: 'amount', orderable : true, searchable : true },
                { data: 'created_at_formatted', name: 'created_at' , orderable : true, searchable : true}
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/compliance/booking_interest/refund_profile/"  + aData['id'];

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }


        } );
    });
</script>;

@endpush