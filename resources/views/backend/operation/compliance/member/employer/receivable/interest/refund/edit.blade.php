@extends('layouts.backend.main', ['title' => trans('labels.backend.member.modify_interest_refund'), 'header_title' => trans('labels.backend.member.modify_interest_refund')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
<style>
    .select2-results {
        min-height: 80px;
        max-height: 80px;
        overflow-y: auto;
    }
</style>
@endpush

@section('content')


    <div class = "row">
        {{--{!! Form::model($booking_interest, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

        {{--Main Content--}}
        {{--menu bar--}}
        <div class = "row">
            <div class="col-md-12">
                {{--menu Bar--}}
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >

                            <div class="pull-right" >


                                {{--close--}}
                                <span>
                                        <a href="{!! route('backend.compliance.booking_interest.refund_profile',
                                        $interest_refund->id)
                                        !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('labels.general.close')</a>
                                        </span>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div>&nbsp;</div>
        {{--interest summary--}}
        {{--[Includes => interest] interest Summary table--}}

        <div class="row">
            {{--interest Refunds Overview--}}
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <table class="display" cellspacing="0" width="100%" id ="interests-refund-table">
                            <thead>
                            <tr>
                                                            <th>@lang('labels.backend.table.receipt.contrib_month')</th>
                                <th>Late Months (Before Adjust)</th>
                                <th>Interest (Before Adjust)</th>
                                <th>@lang('labels.backend.table.booking.late_months')</th>
                                <th>@lang('labels.backend.finance.receipt.interest')</th>
                                <th>@lang('labels.backend.table.amount_paid')</th>
                                <th>@lang('labels.general.balance')</th>
                                <th>Rctno(s)</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>

        </div>


    </div>


@stop


@push('after-script-end')

{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

<script  type="text/javascript">

    $(function() {
        var url = "{!! url("/") !!}";
        var $table =   $('#interests-refund-table').DataTable({

            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.booking_interest.get_eligible_for_modifying_refund', ['employer' => $employer->id, 'interest_refund' =>
                $interest_refund->id ])
                !!}',
                type : 'post'

            },

            columns: [

                {data: 'contrib_month_formatted' , name: 'miss_month' ,searchable: true, orderable: true},
                { data: 'late_months_before', name: 'late_months_before', searchable: false, orderable: false},
                { data: 'amount_before', name: 'amount_before' , searchable: false, orderable: false},
                { data: 'late_months', name: 'late_months', searchable: false, orderable: false},
                { data: 'amount', name: 'amount' , searchable: false, orderable: false},
                { data: 'amount_paid', name: 'amount_paid' , searchable: false, orderable: false },
                { data: 'remain_amount', name: 'remain_amount' , searchable: false, orderable: false },
                { data: 'rctno', name: 'rctno' , searchable: false, orderable: false },
                { data: 'actions', name: 'actions' , searchable: false, orderable: false },

            ],


            'rowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:first-child)', nRow).click(function() {
                    document.location.href =   url + "/compliance/employer/profile/interest/" + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            },
        });



        // Handle button events
        $('#refund_selected').on('submit', function(e) {
            e.preventDefault();
            var $form = this;
            var $rows_selected = $table.column(0).checkboxes.selected();
            //Remove all previous selected
            $($form).find("input[name='id[]']").remove();
            // Iterate over all selected checkboxes
            $.each($rows_selected, function(index, rowId){
                // Create a hidden element
                $($form).append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'id[]')
                        .val(rowId)
                );
            });
            swal({
                title: "Confirm",
                text: "Are you sure to refund selected Interests?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function (confirmed) {
                if (confirmed) {
                    $form.submit();
                }
            });
        });


    });
</script>;

@endpush
