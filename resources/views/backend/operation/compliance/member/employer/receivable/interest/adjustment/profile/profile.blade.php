@extends('layouts.backend.main', ['title' => 'Interest Adjustment Profile' , 'header_title' => 'Interest Adjustment Profile'])

@include('backend/includes/assets/sweetalert_assets')
@include('backend.includes.datatable_assets')

@push('after-styles-end')
    <style>
    </style>
@endpush

@section('content')

    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $interest_adjustment->receipt->employer])

    <div class="row">

        <div class="col-md-12">
            <br/>
            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item ">
                        <a class="nav-link active" href="#general" data-toggle="tab">General</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="#documents" data-toggle="tab">Document Center</a>
                    </li>
                </ul>

                {{--Start: Tab Contents--}}
                <div class="nav_tab_contain tab-content">
                    <div id="general" class="nav_tab_pane tab-pane active in">

                        <div class = "row nav_tab_pane_header">
                            <div class="col-md-12">
                                <div class="pull-right">

                                    {{--iNITIATE LETTER--}}
                                    @if($check_if_can_initiate_wf_letter == true &&  $interest_adjustment->status_if_letter_initiated == false && $interest_adjustment->wf_done == 0)
                                        <a target=""  href=" {{ route("backend.letter.process", [$interest_adjustment->id, "CLIEMPINTEADJ"]) }}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-file"></i>&nbsp;Response Letter</a>
                                    @endif



                                    @if($check_workflow == 0)
                                        <span>
                                           {!! HTML::decode(link_to('#', "<i class=\"icon fa fa-confirm\"></i>&nbsp;Initiate Approval", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Interest Adjustment Approval Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $interest_adjustment->id, 'data-route' => route('backend.compliance.interest_adjustment.initiate_approval', $interest_adjustment->id)])) !!}
                                            </span>
                                    @endif


                                    @if($check_pending_level1 == 1 || $check_workflow == 0)
                                        <span>
                                           {!! HTML::decode(link_to_route('backend.compliance.interest_adjustment.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $interest_adjustment->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this entry?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                        {{--modify--}}
                                        <a href="{!! route('backend.compliance.interest_adjustment.edit', $interest_adjustment->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Modify</a>

                                    @endif
                                    <a href="{!! route('backend.finance.receipt.profile_by_request', $interest_adjustment->receipt_id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>


                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-9">
<legend style="background-color: lightgrey"> Interests Adjusted</legend>
                                @include('backend/operation/compliance/member/employer/receivable/interest/adjustment/profile/includes/get_interests_adjusted')

                                <div>&nbsp;</div>
                                {{--APprovals tabs history--}}
                                {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}

                            </div>

                            <div class="col-md-3">
                                @include('backend/operation/compliance/member/employer/receivable/interest/adjustment/profile/includes/sidebar_summary')
                            </div>
                        </div>
                    </div>


                    {{--documents--}}
                    <div id="documents" class="nav_tab_pane tab-pane">
                        @include('backend/system/document/general/includes/document_center_gen', ['resource_id' => $interest_adjustment->id, 'reference'=> 'DRINTEREADJ', 'allow_modify' => (($check_pending_level1 == 1 || $check_workflow == 0) ? true : false)])
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("backend.system.workflow.includes.initiate_modal")
@endsection

@push('after-script-end')
    <script>
        $(function() {
        });
    </script>
@endpush
