@include('backend.includes.datatable_assets')

<div class = "row">
  <div class="col-md-12" >
    <legend class="grey_modal">Contribution Modifications</legend>
    <div>&nbsp; </div>
    <div class="col-md-12">

    </div>
    {{--<div>&nbsp; </div>--}}
    &nbsp;


    <table class="display" cellspacing="0" width="100%" id ="employer_contribution_modifications-table">
      <thead>
        <tr >
          <th>Contribution Months</th>
          <th>Amount</th>
          <th>Description</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
    </table>

  </div>

</div>



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}}

 --}}
<script  type="text/javascript">

  $(function() {
   autosize($("textarea.autosize"));

   // var contribution_modification_datatable = $('#employer_contribution_modifications-table').DataTable({

    $('#employer_contribution_modifications-table').DataTable({

    processing: true,
    serverSide: true,
    stateSave: true,
    stateSaveCallback: function (settings, data) {
      localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));

    },
    stateLoadCallback: function (settings) {
      return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
    },
    ajax:{
     url : '{!! route('backend.compliance.employer.online.contribution_modifications',$employer->id) !!}',
     type : 'get'


   },
     columns: [
   { data: 'old_contrib_month', name: 'change_contribution_months.old_contrib_month'},
   { data: 'new_contrib_month', name:'change_contribution_months.new_contrib_month',searchable : false, orderable : false },
   { data: 'description', name: 'change_contribution_months.description'},
   { data: 'status', name:'status', searchable : false, orderable : false},
   { data: 'action', name:'action', searchable : false, orderable : false},
   ],
 });



});
</script>
@endpush