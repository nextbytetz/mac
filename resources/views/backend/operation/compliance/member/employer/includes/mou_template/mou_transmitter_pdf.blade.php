<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    <title></title>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/letter/standard.css") }}
</title>
</head>
<body style="margin-right: -15pt !important;">
<div>&nbsp;</div>
<div>&nbsp;</div>
<div style="text-align: center;"><strong><span style="font-size: 18pt; color: #236fa1;">WORKERS COMPENSATION FUND</span></strong></div>
<div>&nbsp;</div>
<table style="width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 33.3333%; text-align: left;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="width: 100%; vertical-align: top;" colspan="2">
                        <div style="text-align: left;">Telegraphic address "WCF"</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <div style="text-align: left;">Tel:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926107</div>
                        <div>+255 22 2926108</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Fax:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div>+255 22 2926109</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Email:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #3598db;"><a style="color: #3598db;" href="mailto:info@wcf.go.tz" target="_blank" rel="noopener">info@wcf.go.tz</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <div style="text-align: left;">Web:</div>
                    </td>
                    <td style="width: 70%; text-align: left;">
                        <div><span style="color: #000000;">www.wcf.go.tz</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 33.3333%; vertical-align: middle; text-align: center;"><img src="{{ public_url() }}/template/assets/nextbyte/img/wcf_big_logo_no_background.png" alt="wcf letter logo" width="215" height="105" /></td>
        <td style="width: 33.3333%; vertical-align: top; text-align: right;">
            <div>P.O. Box 79655</div>
            <div>GEPF House</div>
            <div>Plot No. 37</div>
            <div>Regent Estate</div>
            <div>Bagamoyo Road</div>
            <div>Dar es Salaam</div>
            
        </td>
    </tr>
    </tbody>
</table>
@php
    $box_no = (int) filter_var($employer->box_no, FILTER_SANITIZE_NUMBER_INT);
@endphp
<div>
    <table style="border-collapse: collapse; width: 100%; height: 35px;" border="0">
        <tbody>
        <tr>
            <td style="width: 50%; text-align: left;">
                <div><strong>In reply please quote:&nbsp;</strong></div>
                <div>&nbsp;</div>
                <div><strong>Re.No: {{$letter_reference}}</strong></div>
            </td>
            <td style="width: 50%; vertical-align: bottom; text-align: right;"><strong>{!! $departure_date !!}</strong></td>
        </tr>
        </tbody>
    </table>
</div>
<div>&nbsp;</div>

<table style="border-collapse: collapse; width: 100%;" border="0">
    <tbody>
    <tr>
        <td style="width: 100%; text-align: left;">
            <div>{{$employer->name}},</div>
            <div>P.O. Box {{$box_no}},</div>
            <div><strong>{{ strtoupper($employer->district_name) }}.</strong></div>
            <div>&nbsp;</div>
            <div><strong>RE: APPROVAL FOR PAYMENT OF ARREARS BY INSTALMENT AMOUNTING TO TZS {{number_format($portal_details['total'],2)}}. </strong></div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">
                Please refer to the above subject and your application for payment of arrears by instalment submitted on {!!(\Carbon\Carbon::parse($installment->created_at)->format('j<\s\u\p>S</\s\u\p> F Y'))!!}.
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">
                I wish to inform you that your request for payment of arrears by instalment amounting to TZS {{number_format($portal_details['total'],2)}} has been granted. You are therefore required to proceed with the payments as per signed Memorandum of Understanding whose copy is attached.
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important;" class="div_break">
               For further information, please contact the following:
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important; margin-left: 7% !important; text-indent: -1.9em !important;" class="div_break">
                1. Senior Compliance Officer, Ms. Melinda Matinyi through mobile number 0737344114 or email melinda.matinyi@wcf.go.tz; or
            </div>
            <div>&nbsp;</div>

            <div style="text-align: justify !important; margin-left: 7% !important; text-indent: -1.9em !important;" class="div_break">
                2. Compliance Officer, Mr. Joseph Joseph through mobile number 0737344043 or email joseph.joseph@wcf.go.tz.
            </div>
            <div>&nbsp;</div>
            <div style="text-align: justify !important;" class="div_break">
               Thank you for your cooperation. 
            </div>
            <div>&nbsp;</div>

            <div class="div_break">
            <div>&nbsp;&nbsp;<img src="{{ public_url() }}/template/assets/nextbyte/img/do_signature.png" width="140" height="50" /></div>
            <div>&nbsp; &nbsp; Anselim K. Peter</div>
           
            <div><strong>For: DIRECTOR GENERAL</strong></div>
            <div>&nbsp;</div>
            </div>
        </td>
    </tr>
    </tbody>
</table>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
<script type="text/javascript">
    @if ($print)
        window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    @endif
    $(function(){
        $(document).on("contextmenu",function(e){
            return false;
        });
    });
</script>
</body>
</html>