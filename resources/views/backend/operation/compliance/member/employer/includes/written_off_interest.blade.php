
{{--Written off Interests table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="written-off-interest-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.receipt.contrib_month')</th>
                <th>@lang('labels.backend.table.booking.late_months')</th>
                <th>@lang('labels.backend.finance.receipt.interest')</th>
            </tr>
            </thead>

        </table>
    </div>
</div>


@push('written-off-interest-script-end')

<script  type="text/javascript">
    $(function() {
    $("#bottom_tab_2_header").one("click", function(){

            $('#written-off-interest-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                paging: false,
                info:true,


                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.written_off_interests', $interest_write_off->id) !!}',
                    type : 'get'

                },
                columns: [
                    { data: 'miss_month' , name: 'miss_month' },
                    { data: 'late_months', name: 'late_months' },
                    { data: 'amount', name: 'amount' }
                ]

            } );

        });

    });
</script>;

@endpush