

<div class = "row" id="employee-counts-div">
    <div class="col-md-12"  >

        {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%', 'id' => 'employee_counts'], true) !!}


    </div>
</div>



@push('employee-counts-script-end')
{!! $dataTable->scripts() !!}
<script>
    $(function() {

    });

</script>
@endpush