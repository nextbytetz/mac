<div class = "row">
    <div class="col-md-12" >
        <legend class="grey_modal" >List of user/s managing notifications for this employer online</legend>
        <div>&nbsp; </div>
        <div class="col-md-12">

        </div>
        {{--<div>&nbsp; </div>--}}
        &nbsp;

        <table class="display" cellspacing="0" width="100%" id ="notification_online_users-table">
            <thead>
            <tr >
            <tr >
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Last Login</th>
                <th>Is Confirmed</th>
                <th>Status</th>
                <th>Is Validated</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>

    </div>

</div>

@push('after-script-end')
    <script  type="text/javascript">
        /*shall be enabled when online notification goes live*/
        $(document).ready(function() {
            let $notification_online_users_div = $('#notification_online_users-table');
            let $notification_online_users_table = $notification_online_users_div.DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer_notification.user',$employer->id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'firstname', name: 'firstname'},
                    { data: 'lastname', name: 'lastname'},
                    { data: 'email', name: 'email' },
                    { data: 'phone', name:'phone' },
                    { data: 'last_login_formatted', name:'last_login_formatted', searchable: false},
                    { data: 'confirmed_formatted', name:'confirmed_formatted', searchable: false},
                    { data: 'active_formatted', name: 'active_formatted', searchable: false},
                    { data: 'validated_formatted', name:'validated_formatted', searchable: false},
                    { data: 'action', name:'action'},
                ],
            });
            $notification_online_users_div.on('click','.btnResetPassword',function(e){
                e.preventDefault();
                $('#reset_user_id').val('');
                $("#reset_user_type").val("claim");
                $('#user_name').text('');
                $('#reset_password').val('');
                $('.error_password').addClass('hidden');
                $('.error_password').text('');

                let user_id = $(this).data('user');
                let user_name = $(this).data('user_name');
                swal({
                        title: "Attention! {{access()->user()->firstname}}",
                        text: "<h6>Are you sure want to reset this user's Password?</h6>",
                        type: "warning",
                        html: true,
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true,
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $('#reset_user_id').val(user_id);
                            $('#user_name').text(user_name);
                            $('#resetPasswordModal').modal('show');
                            //alert(user_name);
                        }
                    });
            });
            $notification_online_users_div.on('click','.btndeactivateNotification',function(e) {
                e.preventDefault();
                $.ajax({
                    url : '{{url('compliance/deactivate_notification_user_form/')}}/'+$(this).data('user'),
                    type : 'GET',
                    datatype : 'json',
                    success:function(data){
                        $('#deactivatenotificationuser').modal('show');
                        $('#notificationusername').val(data.onlineusername);
                        $('#notificationusermail').val(data.email);
                        $('#notificationuserphone').val(data.phone);
                        $('#notificationemployername').val(data.employername);
                        $('#notificationuser_id').val(data.user);
                        $('#notificationemployer_id').val(data.employer_id);
                    }

                });
            });
            $('#btnnotificationdeactivate').on('click',function(e){
                e.preventDefault();
                $.ajax({
                    url : '{{url('compliance/deactivate_notification_user')}}/'+$('#notificationuser_id').val(),
                    type : 'POST',
                    data:$('#deactivatednotificationuser').serialize(),
                    datatype : 'json',
                    success:function(data){
                        swal('User','Deactivated successfully','success');
                        $notification_online_users_table.ajax.reload(null, false);
                    }
                });
            });
        });
    </script>
@endpush