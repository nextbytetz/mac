
{{--employees table--}}

<div class = "row">
    <div class="col-md-12" >

        {{--<div>&nbsp; </div>--}}
        {{----------Button Add New Employee -----------}}
        <div class="col-md-12" >
            
        </div>



        &nbsp; <table class="display" cellspacing="0" width="100%" id ="employees-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.memberno')</th>
                <th>@lang('labels.backend.table.name')</th>
                <th>@lang('labels.backend.table.dob')</th>
                <th>@lang('labels.backend.table.category')</th>
            </tr>
            </thead>

        </table>


        <br/>
        {{--Total no of employees--}}

        <table class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th style="width:180px">@lang('labels.general.no_of_employees'): </th>
                <th>{!! number_0_format($member_count) !!}</th>
            </tr>
            </thead>
        </table>

    </div>
</div>

@push('employees-script-end')

<script  type="text/javascript">
    $(function() {
        $("#employee_header").one("click", function () {
            var url = "{!! url("/") !!}";
            $('#employees-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                info: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax: {
                    url: '{!! route('backend.compliance.employer.employees', $employer->id) !!}',
                    type: 'get'
                },
                columns: [
                    {data: 'memberno', name: 'memberno'},
                    {data: 'fullname', name: 'fullname', orderable: false, searchable: false, visible: true},
                    {data: 'dob', name: 'dob'},
                    {data: 'category', name: 'code_values.name', orderable: true, searchable: true, visible: true},
                    {data: 'firstname', name: 'firstname', orderable: false, searchable: true, visible: false},
                    {data: 'middlename', name: 'middlename', orderable: false, searchable: true, visible: false},
                    {data: 'lastname', name: 'lastname', orderable: false, searchable: true, visible: false}
                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function () {
                        document.location.href = url + "/compliance/employee/profile/"  + aData['id'];
                    }).hover(function () {
                        $(this).css('cursor', 'pointer');
                    }, function () {
                        $(this).css('cursor', 'auto');
                    });
                }

            });

        });
    });

</script>;

@endpush