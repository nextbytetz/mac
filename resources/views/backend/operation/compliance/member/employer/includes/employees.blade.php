
{{--employees table--}}

<div class="card">
    <div class="card-header h6">
     <label for="select">Payroll: &nbsp;</label>
     <select class="user-select" id="payroll_employee" name="payroll_employee" style="width:40%">
        @if(count($employer_user) == 1)
        <option value="{{$employer_user[0]->payroll_id}}" data-user_id="{{$employer_user[0]->id}}" selected="selected">{{ucwords(strtolower($employer_user[0]->name.' - '.$employer_user[0]->payroll_id))}}</option>
        @else
        <option value="" selected="selected">Click here to select</option>
        @foreach($employer_user as $user)
        <option value="{{$user->payroll_id}}" data-user_id="{{$user->id}}">{{ucwords(strtolower($user->name.' - '.$user->payroll_id))}}</option>
        @endforeach
        @endif
    </select>
    <br> <p class="error_user_select text-danger hidden"></p>
</div>
<div class="card-body pt-2">
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="employees-table">
                <thead>
                    <tr>
                        <th>@lang('labels.backend.table.memberno')</th>
                        <th>@lang('labels.backend.table.name')</th>
                        <th>@lang('labels.backend.table.dob')</th>
                        <th>@lang('labels.backend.table.category')</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>
</div>



@push('employees-script-end')

<script  type="text/javascript">
    $(function() {


       $('#employees-table').DataTable({});

       redrawEmployeestable();
       $('#payroll_employee').on('change',function(e){
          e.preventDefault();
          redrawEmployeestable();
      });  

       function redrawEmployeestable() {
        $('#employees-table').DataTable().clear().destroy();
        let payroll_employee = $('#payroll_employee option:selected').val();
        if (payroll_employee) {
          $('#employees-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            info: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{!!url('compliance/employer/online/get_employees/'.$employer->id) !!}/'+payroll_employee,
                type: 'get'
            },
            columns: [
            {data: 'memberno', name: 'memberno'},
            {data: 'fullname', name: 'fullname'},
            {data: 'dob', name: 'dob'},
            {data: 'emp_cate', name: 'emp_cate'},
            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function () {
                    document.location.href = url + "/compliance/employee/" + "profile/" + aData['id'];
                }).hover(function () {
                    $(this).css('cursor', 'pointer');
                }, function () {
                    $(this).css('cursor', 'auto');
                });
            }

        });
      }else{
        $('#employees-table').DataTable({});   
    }
}
});

</script>;

@endpush