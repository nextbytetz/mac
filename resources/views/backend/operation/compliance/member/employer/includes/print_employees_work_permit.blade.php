<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <meta charset="utf-8" />
    {{-- <title>@lang('labels.backend.registration.organization.print_certificate.title')</title> --}}
    <title>WORK PERMIT EMPLOYEES</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="WCF"/>
    {{-- <meta name="subject" content="{!! env('COMPANY') !!}, @lang('labels.backend.registration.organization.print_certificate.subject')"/> --}}
    <meta name="keywords" content="certificate"/>
    <meta name="date" content="2017-08-04"/>
    {{ Html::style(asset_url() . "/nextbyte/css/bill.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
    {{ Html::style(asset_url() . "/global/plugins/bootstrap/dist/css/bootstrap.min.css", ['rel' => 'stylesheet']) }}

    <style type="text/css">
    {{-- @page { margin: 20px 25px; } --}}
footer { position: fixed; bottom: 100px; left: 0px; right: 0px; font-family: Arial, sans-serif; /*background-color: lightblue; height: 50px; */}
/*p { page-break-after: always; }*/
/*p:last-child { page-break-after: never; }*/

</style>

</head>
<body>


   
    {{-- <footer>
        
        <p class="center copyright">Workers Compensation Fund &copy; {!! Carbon\Carbon::parse('now')->format('Y')!!}, All rights reserved.</p>
    </div>
</footer> --}}

<div id="certificate">
    <div class="center">
         <div class="col-md-3">
            <div class="pull-right" style="margin-left:500px;margin-bottom:30px">
                <i>Attachment 1</i>
            </div>
        </div>
        <div class="h4">WORKERS COMPENSATION FUND</div>
        <img src="{!! asset_url() . "/nextbyte/img/wcf_big_logo_no_background.png"!!}" style="height: 70px;">
        <p>LIST OF EMPLOYEES FOR {!! $employer->name_formatted !!} </p>
    </div>
    <div class="row">
        
        <table class="table table-bordered" style="font-size:10">
                <thead>
                <tr >
                    <tr >
                    <th scope="col">S/N</th>
                    <th scope="col">WCF NUMBER</th>
                    <th scope="col">EMPLOYEE NAME</th>
                    <th scope="col">TYPE OF CONTRACT</th>
                </tr>
                </thead>
                <?php $counter=0; ?>

                 @foreach($employees as $employee)
                <tr>
                <th>{{++$counter}} </th>
                <td>{{$employee->memberno}}</td>
                 <td>{{$employee->employee_name}}</td>
                 <td>{{$employee->employment_category}}</td>
                </tr>
                @endforeach
            </table>
         <div>&nbsp; </div>
        <p>Printed On : {!! Carbon\Carbon::parse('now')->format('d-M-Y')!!}</p>
        <p>Printed By : {{ucwords(access()->user()->firstname).' '.ucwords(access()->user()->middlename.' '.ucwords(access()->user()->lastname))}}</p>
        <br><br><br>

    </div>

    <script type="text/javascript">
        window.onload = function() {
            window.print();
            window.close();
        };
    //     window.onfocus=function(){ window.close();}

</script>
</body>
</html>