<html><head>
    <style type="text/css" href="{{asset_url().'/nextbyte/css/backend/boostrap_datatable/table.css'}}"></style>
     <style type="text/css">
    table {
      border-collapse: collapse;
    }

    table, th, td {
      border: 0.1px solid black;
    }
    .border-less{
     border: none !important;
   }
   .border-less-left-right{
     border-left: none !important;
     border-right: none !important;
   }
   .b-left-none{
     border-left: none !important;
   }
   .b-right-none{
     border-right: none !important;
   }
   .b-left-right-none{
     border-left: none !important;
     border-right: none !important;
   }
   .b-left-bottom-none{
     border-left: none !important;
     border-bottom: none !important;
   }

   .b-left-top-none{
     border-left: none !important;
     border-top: none !important;
   }
   .b-right-bottom-none{
     border-right: none !important;
     border-bottom: none !important;
   }
   .b-right-top-none{
     border-right: none !important;
     border-top: none !important;
   }
   .b-top-none{
     border-top: none !important;
   }
   .b-bottom-none{
     border-bottom: none !important;
   }
   .b-left-right-top-none{
     border-left: none !important;
     border-top: none !important;
     border-right: none !important;
   }

   .b-left-right-bottom-none{
     border-left: none !important;
     border-bottom: none !important;
     border-right: none !important;
   }
   .img-dimensions{
    max-width:337px;
    height:152px;

  }
  .text-center{
    text-align: center;
  }
  .text-left{
    text-align: left;
  }
  .text-right{
    text-align: right;
  }
  .mt-15px{
    margin-top: 15px;
  }
  .mt-10px{
    margin-top: 10px;
  }
  .bg-clay{
    background-color: #c4cace;
  }
  .text-blue{
    color: blue;
  }
  .width-100{
    width: 100%;
  }
  .width-25{
    width: 25%;
  }
  .width-max-25{
    max-width: 25%;
  }
  .bolder{
    font-weight: bolder;
  }
  .width-10px{
    width: 10px;
  }
  .max-width-70px{
    max-width: 70px;
  }
  .width-15px{
    width: 15px;
  }
  .auto-height{
    height: auto;
  }
  .width22p5{
    width: 22.5px;
  }
  .width-300px{
    width: 300px;
  }
  .width-4{
    width: 4%;
  }
  .width-5{
    width: 5%;
  }
  .width-48{
    width: 48%;
  }
  .width-24{
    width: 24%;
  }
  .width-20{
    width: 20%;
  }
  .width-80{
    width: 80%;
  }
  .width-294px{
    width: 294px;
  }
</style>
<body>
  <h4 class="text-right">WCF/COM7</h4>
  <h4 class='text-center'>
    <img src='{{asset_url().'/nextbyte/img/logo.png'}}' defer='' class='img-dimensions' alt='...'>
  </h4>

  <h3 class='text-center mt-15px'>PAYMENT BY INSTALMENT<br> REQUEST SUMMARY

  </h3>
  <table class='bg-clay text-left text-blue width-100'>
    <thead>
      <th class='bg-clay text-left text-blue width-100'>1.&nbsp; EMPLOYER'S DETAILS</th>
    </thead>
  </table>
  <table class='text-left width-100' style="text-align: left;width: 100%;">
    <tbody>
      <tr>
        <td class='bg-clay text-left width-25 bolder'>Employer's Name</td>
        <td class='width-25'>{{$employerProfile[1]['name']}}</td>
        <td class='bg-clay bolder text-left width-25'>WCF Reg. No.</td>
        <td class='width-25'>{{substr($employerProfile[1]['registrationNo'], -6)}}</td>
      </tr>
      <tr>
        <td class='bg-clay bolder text-left width-25'>Physical Address<br/>
          Street:<br/>
          Plot No:<br/>
          District:<br/></td>
          <td class='width-25'>{{$employerProfile[1]['addres']}}</td>
          <td class='bg-clay bolder width-25'>Contact Person</td>
          <td class='width-25'>{{$employerProfile[1]['contact_person']}}</td>
        </tr>
        <tr>
          <td class='bg-clay bolder width-25'>Mobile Number</td>
          <td class='width-25'>{{$employerProfile[1]['phone']}}</td>
          <td class='bg-clay bolder width-25'>Title:</td>
          <td class='width-25'>{{$employerProfile[1]['user_title']}}</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table class='table table-bordered mt-10px width-100'>
      <thead>
        <tr ><th class='table border-less text-left bg-clay text-blue'>2.&nbsp;DETAILS OF ACCOUNTING OFFICER/DIRECTORS/ TRUSTEES/OWNERS <i class='fa fa-arrow-down'></i></th>
        </tr></thead>
      </table>
      <table class='table table-bordered width-100 auto-height' id="table1">
        <tr>
          <th class='bg-clay width-15px'><b> S/N</b></th>
          <th class='bg-clay text-center'><b>FULL NAME</b></th>
          <th class='bg-clay text-center'><b>NATIONAL ID</b></th>
          <th class='bg-clay text-center'><b>TITLE</b></th>
          <th class='bg-clay text-center'><b>PHONE NO:</b></th>
          <th class='bg-clay text-center'><b>E-MAIL</b></th> 
        </tr>
        <?php $countLoop = 1;?>
        @foreach($accountants as $acc)
        <tr>
          <td class='width-10px'>{{$countLoop++}}</td>
          <td class='max-width-70px'>{{$acc->firstname.' '.$acc->middlename.' '.$acc->lastname}}</td>
          <td class='max-width-70px'>{{$acc->national_id}}</td>
          <td class='max-width-70px'>{{$acc->title}}</td>
          <td class='max-width-70px'>{{$acc->phone}}</td>
          <td class='max-width-70px'>{{$acc->email}}</td>
        </tr> 
        @endforeach        
      </table>
      <br>
      <table class="table table-bordered mt-10px width-100">
        <thead>
          <tr><th class="table border-less text-left bg-clay text-blue">3. &nbsp;DETAILS OF OUTSTANDING AMOUNT <i class="fa fa-arrow-down"></i></th>
          </tr></thead>
        </table>
        <table class="table table-bordered width-100 auto-height" id="table1">
         <thead>
          <tr>
            <th class='bg-clay width-15px'><b>S/N</b></th>
            <th class='bg-clay text-center'><b>ITEMS</b></th>
            <th class='bg-clay text-center'><b>DESCRIPTION</b></th>
            <th class='bg-clay text-center'><b>AMOUNT(TZS)</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th class='width-10px' >1</th>
            <th class='max-width-70px text-left'>Contributions</th>
            <td class='max-width-70px'>{{$months}}</td>
            <td class='max-width-70px text-right'>{{number_format($arrear_total,2)}}</td>
          </tr>
          <tr>
            <th class='width-10px'>2</th>
            <th class='max-width-70px text-left'>Interest</th>
            <td class='max-width-70px'>{{$months}}</td>
            <td class='max-width-70px text-right'>{{number_format($interest_total,2)}}</td>
          </tr>
          <tr>
            <th class='width-10px'></th>
            <th class='max-width-70px text-left'><b>Total</b></th>
            <td class='max-width-70px'></td>
            <td class='max-width-70px text-right'><b>{{number_format($arrear_total,2)}}</b></td>
          </tr>
        </tbody>
      </table>
      <br>
      <table class="table table-bordered mt-10px width-100">
        <thead>
          <tr class="table table-bordered"><th class="table border-less text-left bg-clay text-blue">4.&nbsp; PAYMENT DETAILS</th>
          </tr></thead>
        </table>

        <table class="table table-bordered width-100 auto-height" id="table1">
         <thead>
          <tr>
            <th class='bg-clay width-5'></th>
            <th class='bg-clay text-left width-300px'><b>PAYMENT OPTIONS</b></th>
            <th class='bg-clay text-left width-294px'>MONTHLY (Every month in 12 months)</th> 
          </tr>
          <tr>
            <td class='bg-clay width-5'><b></b></td>
            <td class='bg-clay text-left width-300px'><b>START DATE : {{$contribution_arrears_simplified[4]['start_commit_date']}}</b></td>
            <td class='bg-clay text-left width-294px'><b>END DATE : {{$contribution_arrears_simplified[5]['end_commit_date']}}</b></td>
          </tr>
        </thead>
      </table>
      <table class="table table-bordered width-100 auto-height" id="table1">
       <tbody>
        <tr>
          <th class='bg-clay width-5'><b>S/N</b></th>
          <th class='bg-clay width-48 text-left'>INSTALMENTS</th>
          <th class='bg-clay width-24'>INSTALMENT AMOUNT(TZS)</th>
          <th class='bg-clay width-24'>DUE DATE</th>
        </tr>

        @if(!empty($contribution_arrears_simplified))
        @if($contribution_arrears_simplified[1] == 'once' || $contribution_arrears_simplified[1] == 'twice' || $contribution_arrears_simplified[1] == 'thrice' || $contribution_arrears_simplified[1] == 'fourtimes' || $contribution_arrears_simplified[1] == 'fivetimes' || $contribution_arrears_simplified[1] == 'sixtimes' || $contribution_arrears_simplified[1] == 'seventimes' || $contribution_arrears_simplified[1] == 'eighttimes' || $contribution_arrears_simplified[1] == 'ninetimes'|| $contribution_arrears_simplified[1] == 'tentimes' || $contribution_arrears_simplified[1] == 'eleventimes' || $contribution_arrears_simplified[1] == 'twelvetimes')
        <?php
        $iter = 1;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['once'])) {
          foreach ($contribution_arrears_simplified[2]['once'] as $key => $values) {

            if ($values['payment_phase'] == 1) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }


          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_once = $arrear_amount;

        $iter = 2;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['twice'])) {
          foreach ($contribution_arrears_simplified[2]['twice'] as $key => $values) {

            if ($values['payment_phase'] == 2) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }

          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_twice = $arrear_amount;

        $iter = 3;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['thrice'])) {
          foreach ($contribution_arrears_simplified[2]['thrice'] as $key => $values) {

            if ($values['payment_phase'] == 3) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }

          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_thrice = $arrear_amount;

        $iter = 4;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['fourtimes'])) {
          foreach ($contribution_arrears_simplified[2]['fourtimes'] as $key => $values) {

            if ($values['payment_phase'] == 4) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_fourtimes = $arrear_amount;

        $iter = 5;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['fivetimes'])) {
          foreach ($contribution_arrears_simplified[2]['fivetimes'] as $key => $values) {

            if ($values['payment_phase'] == 5) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_fivetimes = $arrear_amount;

        $iter = 6;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['sixtimes'])) {
          foreach ($contribution_arrears_simplified[2]['sixtimes'] as $key => $values) {

            if ($values['payment_phase'] == 6) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_sixtimes = $arrear_amount;

        $iter = 7;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['seventimes'])) {
          foreach ($contribution_arrears_simplified[2]['seventimes'] as $key => $values) {

            if ($values['payment_phase'] == 7) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_seventimes = $arrear_amount;

        $iter = 8;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['eighttimes'])) {
          foreach ($contribution_arrears_simplified[2]['eighttimes'] as $key => $values) {

            if ($values['payment_phase'] == 8) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_eighttimes = $arrear_amount;

        $iter = 9;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['ninetimes'])) {
          foreach ($contribution_arrears_simplified[2]['ninetimes'] as $key => $values) {

            if ($values['payment_phase'] == 9) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_ninetimes = $arrear_amount;

        $iter = 10;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['tentimes'])) {
          foreach ($contribution_arrears_simplified[2]['tentimes'] as $key => $values) {

            if ($values['payment_phase'] == 10) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_tentimes = $arrear_amount;

        $iter = 11;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['eleventimes'])) {
          foreach ($contribution_arrears_simplified[2]['eleventimes'] as $key => $values) {

            if ($values['payment_phase'] == 11) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_eleventimes = $arrear_amount;

        $iter = 12;
        $contrib_month = '';
        $arrear_amount = 0;
        if (!empty($contribution_arrears_simplified[2]['twelvetimes'])) {
          foreach ($contribution_arrears_simplified[2]['twelvetimes'] as $key => $values) {

            if ($values['payment_phase'] == 12) {
              $payment_phase = $values['payment_phase'];
              $contrib_month .= \Carbon\Carbon::parse($values['contrib_month'])->format('F Y').',';
              $due_date = \Carbon\Carbon::parse($values['due_date'])->format('F Y');
              $arrear_amount += $values['arrear_amount'];

            }

          }    
          echo "<tr>";
          echo "<td class='width-4'>".$iter;
          echo "</td>";
          echo "<td class='width-48'>".$contrib_month;
          echo "</td>";
          echo "<td class='width-24 text-right'>".number_format($arrear_amount,2);
          echo "</td>";
          echo "<td class='width-24 text-center'>".$due_date;
          echo "</td>";
          echo "</tr>";
        }
        $arrear_total_twelvetimes = $arrear_amount;
        ?>
        @endif
        @endif
        <tr>
          <td></td>
          <td class='text-center'><b>Total</b></td>
          @if(!empty($contribution_arrears_simplified))
          {{-- @if($contribution_arrears_simplified[1] == 'once') --}}
          {{-- <td class='text-right'><b>{{number_format($arrear_total_once,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'twice')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'thrice')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'fourtimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_fourtimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'fivetimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_fivetimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'sixtimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_sixtimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'seventimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_seventimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'eighttimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_eighttimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'ninetimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_ninetimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'tentimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_tentimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'eleventimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_eleventimes,2)}}</b></td>
          @endif
          @if($contribution_arrears_simplified[1] == 'twelvetimes')
          <td class='text-right'><b>{{number_format($arrear_total_once + $arrear_total_twice + $arrear_total_thrice+$arrear_total_twelvetimes,2)}}</b></td>
          @endif --}}
          <td class="text-right"><b>{{number_format($arrear_total,2)}}</b></td>
          @endif
          <td></td>
        </tr>
      </tbody>
    </table>
    <br/>
    <br/>
    <p>
      <table class="table table-bordered mt-10px width-100">
       <thead>
         <tr>
           <th class='width-20 text-left bg-clay'>
             Date Applied 
             <td class='width-80'>&nbsp;&nbsp;&nbsp;{{$date_created}}</td>
           </th>
         </tr>
         <tr>
           <th class='width-20 text-left bg-clay'>
             Date Approved By WCF
             <td class='width-80'>&nbsp;&nbsp;&nbsp;{{$approval_date}}</td>
           </th>
         </tr>
       </thead>
     </table> </p>

   </body></html>