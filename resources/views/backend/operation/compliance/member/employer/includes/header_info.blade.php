<div class="col-md-12 col-sm-12">
    <h5 class="client-title">
        <i class="icon-circle"></i>
        <!-- Employer header detail -->
        <span>{!! $employer->status_label !!} </span>

        <strong> <a href="{!! route('backend.compliance.employer.profile',$employer->id) !!}"> {!! Form::label( 'name', $employer->name_formatted, [ 'id'=> 'name']) !!} </a></strong>

        <small >
            @lang('labels.backend.table.employer.reg_#'): {!! Form::label( 'reg_no', isset($employer->reg_no) ? (!empty($employer->reg_no) ? $employer->reg_no : ' ') : ' ', [ 'id'=> 'reg_no']) !!}
        </small>
    </h5>
    <legend></legend>
</div>
