@extends('layouts.backend.main', ['title' => 'Installment Request', 'header_title' => 'Installment Payment Request'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
    /* start: File Icons CSS */
    #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
    #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
    #folder-tree .file-pdf { background-position: -32px 0 }
    #folder-tree .file-as { background-position: -36px 0 }
    #folder-tree .file-c { background-position: -72px -0px }
    #folder-tree .file-iso { background-position: -108px -0px }
    #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
    #folder-tree .file-cf { background-position: -162px -0px }
    #folder-tree .file-cpp { background-position: -216px -0px }
    #folder-tree .file-cs { background-position: -236px -0px }
    #folder-tree .file-sql { background-position: -272px -0px }
    #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
    #folder-tree .file-h { background-position: -488px -0px }
    #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
    #folder-tree .file-php { background-position: -108px -18px }
    #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
    #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
    #folder-tree .file-rb { background-position: -180px -18px }
    #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
    #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
    #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
    #folder-tree .file-js { background-position: -434px -18px }
    #folder-tree .file-css { background-position: -144px -0px }
    #folder-tree .file-fla { background-position: -398px -0px }
    /* end: File Icon CSS */
</style>
@endpush

@section('content')
<!-- Put the page specifically for this page here -->
<div class="row">
    <div class="col-md-9">   
      <div class="row">
       <div class="col-md-12 col-sm-12">
        <h5 class="client-title">
            <i class="icon-circle"></i>
            <!-- Employer header detail -->
            <span>{!! $employer->status_label !!} </span>

            <strong> <a href="{!! route('backend.compliance.employer.profile',$installment->employer_id) !!}"> {!! Form::label( 'name', strtoupper($installment->employer), [ 'id'=> 'name']) !!} </a></strong>

            <small >
                @lang('labels.backend.table.employer.reg_#'): {!! Form::label( 'reg_no', isset($installment->reg_no) ? (!empty($installment->reg_no) ? $installment->reg_no : ' ') : ' ', [ 'id'=> 'reg_no']) !!}
            </small>
        </h5>
        <legend></legend>
    </div>

    <br/>
</div>
<div class="row">
    <div class="mou_template d-none" style="height:350px;">
        <object data="{!! route('backend.compliance.employer.online.mou_template',$installment->id) !!}" width="95%" height="100%" type="application/pdf">
            This browser does not support PDFs..
        </object>
    </div>
</div>
<div class="row">
    <div class="unsigned_mou d-none" style="height:350px;">
        <object data="{!! route('backend.compliance.employer.online.unsigned_mou',$installment->id) !!}" width="95%" height="100%" type="application/pdf">
            This browser does not support PDFs..
        </object>
    </div>
</div>
<div class="row">
    <div class="instalment_template d-none" style="height:350px;">
        <object data="{!! route('backend.compliance.employer.online.instalment_agreement_template',$installment->id) !!}" width="95%" height="100%" type="application/pdf">
            This browser does not support PDFs..
        </object>
    </div>
</div>
<div class="row">
    <div class="transmitter_template d-none" style="height:350px;">
        <object data="{!! route('backend.compliance.employer.online.transmitter_letter_template',$installment->id) !!}" width="95%" height="100%" type="application/pdf">
            This browser does not support PDFs..
        </object>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
       @php
       $workflowinput = ['resource_id' => $installment->id, 'wf_module_group_id'=> 26, 'type' => -1];
       @endphp
       {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}

   </div>

</div>
<div class="row">
    <div class="col-md-12">
        <legend>Instalments</legend>
        <table class="table table-bordered" cellspacing="0" width="100%" id ="instalment-table">
            <thead>
              <tr >
                <th>Instalments</th>
                {{-- <th>Contribution month  </th> --}}
                <th>Arrears Amount</th>
                <th>Status</th>
                <th>Due date</th>
                <th>Update due date</th>
                {{-- <th>Update due date</th> --}}
            </tr>
        </thead>
    </table>
</div>
</div>
</div>
<div class="col-md-3">
    <div class="row">

        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h5><b><span class="light_dark_color">Installment Request Summary</span></b></h5></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="light_grey_bg">&nbsp;</div>
        <div class="col-md-12 light_grey_bg">
            <h6 class="underline" style="font-weight: lighter;">User:</h6>
            <p style="font-weight: bold;">{!! $installment->user !!}</p>
            <h6 class="underline" style="font-weight: lighter;">Description :</h6>
            <p style="font-weight: bold;">{!! $installment->description !!}</p>
            <h6 class="underline" style="font-weight: lighter;">Payment Option :</h6>
            <p style="font-weight: bold;">{!! $installment->payment_option !!}</p>
            <h6 class="underline" style="font-weight: lighter;">Date Requested:</h6>
            <p style="font-weight: bold;">{!! \Carbon\Carbon::parse($data->created_at)->format('M d, Y H:i A') !!}</p>
        </div>
        @if($wf_definition->level > 5)
        {{-- {{dd(auth()->user())}} --}}
        <div class="col-md-12 light_grey_bg">
            {{-- @if((auth()->user()->unit_id == 15) && (auth()->user()->designation_id == 4)) --}}
            @if($wf_definition->level == 7)
            @if(!isset($installment->agreement_date))
            <div class="col-md-12 light_grey_bg"><button class="btn btn-sm btn-block btn-primary" id="add_agreement_date">Add MoU agreement date</button></div>
            @else
            <div class="col-md-12 light_grey_bg"><button class="btn btn-sm btn-block btn-primary" id="add_agreement_date">Update MoU agreement date</button></div>
            @endif
            @endif
            {{-- @endif --}}
        </div>
        @endif
        {{-- @if($wf_definition->level > 6) --}}
        {{-- {{dd(auth()->user())}} --}}
        <div class="col-md-12 light_grey_bg">
            {{-- @if((auth()->user()->unit_id == 15) && (auth()->user()->designation_id == 4)) --}}
            {{-- {{dd($wf_definition->level)}} --}}
            @if($wf_definition->level == 10)
            @if(!isset($installment->letter_date))
            <div class="col-md-12 light_grey_bg"><button class="btn btn-sm btn-block btn-primary" id="letter_details">Add letter details</button></div>
            @else
            <div class="col-md-12 light_grey_bg"><button class="btn btn-sm btn-block btn-primary" id="letter_details">Update letter details</button></div>
            @endif
            @endif
            {{-- @endif --}}
        </div>
        {{-- @endif --}}
        {{-- {{dd($wf_definition->level)}} --}}
        <div class="light_grey_bg">&nbsp;</div>
        <div class="col-md-12 light_grey_bg">
            @if($wf_definition->level >= 7)
            @if(isset($installment->agreement_date))
            <div class="col-md-5 light_grey_bg"><button class="btn btn-sm btn-block btn-info" id="unsigned_mou"><i class="fa fa-angle-double-up"></i> Unsigned MoU</button></div>
            @endif
            @endif
            @if($wf_definition->level >= 7)
            @if(!is_null($submitted_mou))
            <div class="col-md-5 light_grey_bg"><button class="btn btn-sm btn-block btn-info" id="signed_mou"><i class="fa fa-angle-double-up"></i> Signed MoU</button></div>
            @endif
            @endif
            
        </div>
        <div class="light_grey_bg">&nbsp;</div>
        <div class="col-md-12 light_grey_bg">
            <div class="col-md-7 light_grey_bg"><button class="btn btn-sm btn-block btn-info" id="instalment_agreement"><i class="fa fa-angle-double-up"></i> Instalment Summary</button></div>
            @if($wf_definition->level >= 10)
            <div class="col-md-5 light_grey_bg"><button class="btn btn-sm btn-block btn-info" id="transmitter_letter"><i class="fa fa-angle-double-up"></i> letter</button></div>
            @endif
        </div>
        <div class="light_grey_bg">&nbsp;</div>
        {{-- <div class="col-md-12 light_grey_bg">
            <div class="col-md-7 light_grey_bg"><button class="btn btn-sm btn-block btn-primary" id=""><i class="fa fa-angle-double-up"></i> Upload Mou</button></div>
        </div> --}}

    </div>
</div>
</div>

@include('backend.operation.compliance.member.employer.includes.modals.mou_agreement_modal')
@include('backend.operation.compliance.member.employer.includes.modals.mou_letter_modal')
@include('backend.operation.compliance.member.employer.includes.modals.instalment_due_dates_modal')
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
<script>
    $(function(){
        $('#e-table').DataTable({});
    });

    $('#instalment-table').DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        stateSaveCallback: function (settings, data) {
          localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
          return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
        url : "{!! route('backend.compliance.employer.online.instalment_details',$installment->id) !!}",
       type : 'get'

   },
   columns: [
   { data: 'payment_phase', name: 'payment_phase'},
   // { data: 'contrib_months', name: 'contrib_months'},
   { data: 'arrear_amount', name: 'arrear_amount'},
   { data: 'status', name: 'status'},
   { data: 'due_date', name:'due_date',searchable : false, orderable : false },
   { data: 'update_due_date', name:'update_due_date',searchable : false, orderable : false},
   ],
   fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    $('td', nRow).click(function() {
      {{-- document.location.href = '{!! url('manage/organization/instalment/') !!}/'+aData['Id'];  --}}
      let col = $(this).index()+1;
      let colText = $('#instalment-table thead th:nth-child('+col+')').text();
      if(colText == 'Update due date'){
          var phase = $(this).closest('tbody tr td').find('.control_no').attr('value');

          $.ajax({
           type: 'get',
           url: "{!! url('/compliance/employer/online/get_due_date/'.$installment->id) !!}/" + phase,
           success:function(data){
            console.log(data)
            if(data){
               $("#due_date").val(data)
                $("#phase").val(phase)
               console.log($("#due_date").val())
               $('#due_date_date_modal').modal('show')

           }
        }
        });         
      }
  }).hover(function() {
      $(this).css('cursor','pointer');
  }, function() {
      $(this).css('cursor','auto');
  });
} 

});


    $('.mou_template').hide()
    $('.instalment_template').hide()
    $('.transmitter_template').hide()
    $('.unsigned_mou').hide()

    $("#signed_mou").click(function () {
        $('.instalment_template').hide()
        $('.transmitter_template').hide()
        $('.unsigned_mou').hide()
        $('.mou_template').toggle()
    });


    $("#unsigned_mou").click(function () {
        $('.instalment_template').hide()
        $('.transmitter_template').hide()
        $('.mou_template').hide()
        $('.unsigned_mou').toggle()
    });

    $("#instalment_agreement").click(function () {
        $('.mou_template').hide()
        $('.transmitter_template').hide()
        $('.unsigned_mou').hide()
        $('.instalment_template').toggle()
    });

    $("#transmitter_letter").click(function () {
        $('.mou_template').hide()
        $('.instalment_template').hide()
        $('.unsigned_mou').hide()
        $('.transmitter_template').toggle()
    });

    $("#add_agreement_date").click(function () {
        $('#add_agreement_date_modal').modal('show')
    });

    $("#letter_details").click(function () {
        $('#add_letter_date_modal').modal('show')
    });

    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
      month = '0' + month.toString();
  if(day < 10)
      day = '0' + day.toString();

  var minDate = year + '-' + month + '-' + day;

  $('input[name="agreement_date"]').attr('min', minDate); 
  $('input[name="letter_date"]').attr('min', minDate); 

  $("#save_agreement_date").click(function () {
    let input = $('input[name="agreement_date"]').val()
    let instalment_id = "{{$installment->id}}"
    let url = "{!! route('backend.compliance.employer.online.save_mou_agreement_date',$installment->id) !!}"
    if (input) {
        let array = []
        let data = {name: 'agreement_date', value: input}
        let token = {name: '_token', value: "{{csrf_token()}}"}
        array.push(data)
        array.push(token)

        $.ajax({
         type: 'post',
         data : array,
         url: url,
         success:function(data){
            console.log(data)
            if(data.success){
                swal({
                    title: "Success",
                    text: data.success,
                    type: "success",
                    confirmButtonClass: 'btn btn-primary',
                    confirmButtonText: 'OK!'
                },function(){
                    location.reload();
                });
            }
        }
    });
    }
});

  $("#save_due_date").click(function () {
    let due_date = $('input[name="due_date"]').val()
    let phase = $('input[name="phase"]').val()
    let instalment_id = "{{$installment->id}}"
    let url = "{!! route('backend.compliance.employer.online.save_instalment_due_date',$installment->id) !!}"
    if (due_date) {
        let array = []
        let due = {name: 'due_date', value: due_date}
        let ph = {name: 'phase', value: phase}
        let token = {name: '_token', value: "{{csrf_token()}}"}
        array.push(due)
        array.push(ph)
        array.push(token)

        $.ajax({
         type: 'post',
         data : array,
         url: url,
         success:function(data){
            console.log(data)
            if(data.success){
                swal({
                    title: "Success",
                    text: data.success,
                    type: "success",
                    confirmButtonClass: 'btn btn-primary',
                    confirmButtonText: 'OK!'
                },function(){
                    location.reload();
                });
            }
        }
    });
    }
});

  $("#save_letter_details").click(function () {
    let date = $('input[name="letter_date"]').val()
    let reference = $('input[name="letter_reference"]').val()
    let instalment_id = "{{$installment->id}}"
    let url = "{!! route('backend.compliance.employer.online.save_letter_details',$installment->id) !!}"
    if (date && reference) {
        let array = []
        let letter_date = {name: 'letter_date', value: date}
        let letter_reference = {name: 'letter_reference', value: reference}
        let token = {name: '_token', value: "{{csrf_token()}}"}
        array.push(letter_date)
        array.push(letter_reference)
        array.push(token)

        $.ajax({
         type: 'post',
         data : array,
         url: url,
         success:function(data){
            console.log(data)
            if(data.success){
                swal({
                    title: "Success",
                    text: data.success,
                    type: "success",
                    confirmButtonClass: 'btn btn-primary',
                    confirmButtonText: 'OK!'
                },function(){
                    location.reload();
                });
            }
        }
    });
    }
});


</script>

@endpush