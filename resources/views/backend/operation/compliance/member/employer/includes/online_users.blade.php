<div class = "row">
  <div class="col-md-12" >
    <div class="nav_tab_pane_header">
      <div class="row">

      </div>
    </div>
    <legend class="grey_modal" >@lang('labels.backend.legend.employer_user')</legend>
    <div>&nbsp; </div>
    <div class="col-md-12">
      <div class="pull-right">
        <button class="btn btn-info btn-md btnAddUser" role="button" data-employerid="'.{{$employer->id}}.'">Add User </button>
      </div>
    </div>
    <div>&nbsp; </div>
    <table class="display" cellspacing="0" width="100%" id ="employeruser-table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Payroll</th>
          <th>Commencement</th>
          <th>Description</th>
          <th>Type</th>
          <th>Status</th>
          <th>Last Login</th>
          <th>Action</th>
        </tr>
      </thead>
    </table>

  </div>
</div>



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
  $(function() {
    let employer_id = {{$employer->id}};
    $(".selectReason").select2({
      dropdownParent: $('.modal-body', '#deactivateuser'),
    });
    
    let employeruser_table = $('#employeruser-table').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      dom: 'lBfrtip',
      buttons: [
      'colvis'
      ],
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
       url : '{!! route('backend.compliance.employer.online.users',$employer->id) !!}',
       type : 'get'

     },
     columns: [
     { data: 'name', name: 'users.name'},
     { data: 'email', name: 'users.email'},
     { data: 'phone', name: 'users.phone' },
     { data: 'payroll_id', name:'employer_user.payroll_id' },
     { data: 'doc', name:'employer_user.doc' },
     { data: 'payroll_description', name:'employer_user.payroll_description'},
     { data: 'user_type_cv_id', name: 'users.user_type_cv_id',visible:false},
     { data: 'status', name:'status'},
     { data: 'last_login', name:'users.last_login'},
     { data: 'action', name:'action', searchable: false, orderable: false,visible:false},
     ],
   });


    $('#employeruser-table').on('click','.btnResetPassword',function(e){
      e.preventDefault();
      $('#reset_user_id').val('');
      $('#user_name').text('');
      $('#reset_password').val('');
      $("#reset_user_type").val("");
      $('.error_password').addClass('hidden');
      $('.error_password').text('');

      let user_id = $(this).data('user');
      swal({
        title: "Attention! {{access()->user()->firstname}}",
        text: "<h6>Are you sure want to reset this user's Password?</h6>",
        type: "warning",
        html: true,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true,
      },
      function(isConfirm) {
        if (isConfirm) {
         $.ajax({
          url : '{{url('compliance/employer/online/return_user/')}}/'+user_id,
          type : 'GET',
          datatype : 'json',
          success:function(data){
              // console.log(result);
              if (data.error) {
                swal('Error','Something went Wrong! Please try again','error');
              } 
              else{
               $('#reset_user_id').val(data.user.id);
               $('#user_name').text(data.user.name);
               $('#resetPasswordModal').modal('show');
             }
           }

         });

       }

     });
    });


    $('#btnSubmitResetPw').on('click',function(e){
     $('.error_password').addClass('hidden');
     $('.error_password').text('');
     e.preventDefault();
     let reset_id = $('#reset_user_id').val();
     if(reset_id >= 1){
      $.ajax({
        url : '{{url('compliance/employer/online/reset_password/')}}',
        type : 'POST',
        data:$('#resetPasswordForm').serialize(),
        datatype : 'json',
        success:function(data){
          if(data.new_password){
            $('#resetPasswordModal').hide();
            swal({
              title: "{{access()->user()->firstname}}",
              text: "<h6>User's Password has been reseted to "+data.new_password+"</h6>",
              type: "success",
              html: true,
              showCancelButton: false,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "OK",
              cancelButtonText: "No",
              closeOnConfirm: true,
              closeOnCancel: true,
            },
            function(isConfirm) {
              if (isConfirm) {
                location.reload(null,false);
              }
            });
          }
          if (data.errors.reset_password){
            $('.error_password').removeClass('hidden');
            $('.error_password').text(data.errors.reset_password[0]);
          }
          if (data.errors.user_id){
            $('.error_password').removeClass('hidden');
            $('.error_password').text(data.errors.user_id);
          }

        }
      });
    }else{
      $('#resetPasswordModal').hide();
      swal({
        title: "Sorry! {{access()->user()->firstname}}",
        text: "<h6>Request Failed! Please try again</h6>",
        type: "error",
        html: true,
        showCancelButton: false,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "OK",
        closeOnConfirm: true,
        closeOnCancel: true,
      },
      function(isConfirm) {
        if (isConfirm) {
          location.reload(null,false);
        }
      });
    }

  });


    $('#employeruser-table').on('click','.btnchange',function(e){
      e.preventDefault();
      $.ajax({
        url : '{{url('compliance/employer/online/change_user_account')}}/'+$(this).data('user'),
        type : 'GET',
        datatype : 'json',
        success:function(data){
          swal('User Account type','successfully changed','success');
          employeruser_table.ajax.reload(null,false);
        }

      });


    });

    $('#employeruser-table').on('click','.btndeactivate',function(e) {
      e.preventDefault();
      $(".subBtns").removeClass('hidden');
      $(".waitLoad").addClass('hidden');
      $(".errors").addClass('hidden');
      $('#deactivateduser').trigger('reset');

      $.ajax({
        url : '{{url('compliance/employer/online/deactivate_user_form')}}/'+$(this).data('user')+'/'+$(this).data('employer'),
        type : 'GET',
        datatype : 'json',
        success:function(data){
          $('#deactivateuser').modal('show');
          $('#onlineusername').text(data.onlineusername);
          $('#usermail').text(data.email);
          $('#userphone').text(data.phone);
          $('#employername').text(data.employername);
          $('#user_id').val(data.user);
          $('#employer_id').val(data.employer_id);
        }

      });
    });

    $('#btnSubmitDeactivate').on('click',function(e){
      e.preventDefault();
      $(".subBtns").addClass('hidden');
      $(".waitLoad").removeClass('hidden');
      $(".errors").addClass('hidden');
      $.ajax({
        url : '{{url('compliance/employer/online/deactivate_user')}}/'+$('#user_id').val()+'/'+$('#employer_id').val(),
        type : 'POST',
        data:$('#deactivateduser').serialize(),
        datatype : 'json',
        success:function(data){
         $(".subBtns").removeClass('hidden');
         $(".waitLoad").addClass('hidden');
         if (data.errors) {
           $.each(data.errors, function(key,valueObj){
            $('.'+key+'_error').removeClass('hidden');
            $('.'+key+'_error').text(valueObj);
          });
         }
         if (data.success) {
           $('#deactivateuser').modal('hide');
           swal('User','Deactivated successfully','success');
           employeruser_table.ajax.reload(null,false);
         }
       },
       error: function ($data) {
        $(".subBtns").removeClass('hidden');
        $(".waitLoad").addClass('hidden');
        swal('Dear! {{access()->user()->firstname}}','An error seems to have occurred. Kindly try again','warning');
      }, 

    });


    });



    $('.btnAddUser').on('click',function(e){
      e.preventDefault();
      $('#payroll_options').empty();
      $('#addUserForm').trigger('reset');
      $('.error_payroll_options').addClass('hidden');
      $('.error_payroll_options').text('');
      $('.error_user_select').addClass('hidden');
      $('.error_user_select').text('');
      $.ajax({
        url : '{{url('/compliance/online_profile/get_payroll')}}/'+employer_id,
        type : 'GET',
        datatype : 'json',
        success:function(data){
          $('.select-user').val('').trigger('change');
          $('.user-name').val('');
          $('.user-email').val('');
          $('.user-phone').val('');
          $('#addUserForm').trigger('reset');
          $('#addUserModal').modal('show');
          if (data.payroll_id > 0) {
           $('#payroll_section').removeClass('hidden');
           for (let i = 1; i <= data.payroll_id; i++) {
            if (i==1) {
              $('#payroll_options').append('<br><input type="radio" name="payroll_id" value="'+i+'" checked="checked"> &nbsp; Payroll '+i+'');
            } else {
              $('#payroll_options').append('<br><input type="radio" name="payroll_id" value="'+i+'" > &nbsp; Payroll '+i+'');
            }
          }
        }

      }

    });
    });


    $(".select-user").select2({
     dropdownParent: $('.modal-body', '#addUserModal'),
     minimumInputLength: 3,
     ajax: {
      url: '{!! route('backend.compliance.online_profile.users') !!}',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        data = data.map(function (item) {
          return {
            text: item.name,
            id: item.id,
            name: item.name,
            phone: item.phone,
            email: item.email
          };
        });

        return {
          results:  data
        };

      },

      cache: true


    }
  });


    $('.select-user').on('change',function(e){ 
     let data=$('.select-user').select2('data')[0];

     if (data) {
       $('.user-name').val($('.select-user option:selected').text());
       $('.user-email').val(data.email);
       $('.user-phone').val(data.phone);
       $('.error_user_select').text('');
     }
     

   }); 


    $('#addUserFormSubmit').on('click',function(e){
      e.preventDefault();
      $('.error_payroll_options').addClass('hidden');
      $('.error_payroll_options').text('');
      $('.error_user_select').addClass('hidden');
      $('.error_user_select').text('');
      let frm = $('#addUserForm');
      if ($('.user-email').val() && employer_id) {
        $.ajax({
          url : '{{url('/compliance/online_profile/add_user')}}/'+employer_id,
          type : 'POST',
          data : frm.serialize(),
          datatype : 'json',
          success:function(data){
            if(data.errors){
              if (data.errors.payroll_id){
                $('.error_payroll_options').removeClass('hidden');
                $('.error_payroll_options').text(data.errors.payroll_id);
              }
              if (data.errors.user_id){
                $('.error_user_select').removeClass('hidden');
                $('.error_user_select').text(data.errors.user_id);
              }
            }
            else{
              if (data.success) {
               swal({
                title: "Dear! {{access()->user()->firstname}}",
                text: "<h6>"+$('.select-user option:selected').text()+" added successfully</h6>",
                type: "success",
                html: true,
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "OK",
                closeOnConfirm: true,
                closeOnCancel: true,
              },
              function(isConfirm) {
               $('#addUserModal').modal('hide');
               employeruser_table.ajax.reload(null,false);
             });

             } else {
               swal('Request Failed!','Something went wrong! Please try again','warning');
               employeruser_table.ajax.reload(null,false);
             }
           }
         } 

       });
      }else{
       $('.error_user_select').removeClass('hidden');
       $('.error_user_select').text('Please search and select user to add');
     }
   });


    $('#employeruser-table').on('click','.btnactivate',function(e) {
      e.preventDefault();
      
      let $name = $(this).data('name');
      let $email = $(this).data('email');
      let $phone = $(this).data('phone');
      let $user_id = $(this).data('user');
      $('#activateUserForm').trigger('reset');

      swal({
        title: "Dear! {{access()->user()->firstname}}",
        text: "<h6>Are you sure you want to activate this User!?</h6>",
        type: "warning",
        html: true,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "YES Activate",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true,
      },
      function(isConfirm) {
        $('.onlineusername').text($name);
        $('.usermail').text($email);
        $('.userphone').text($phone);
        $('#activate_user_id').val($user_id);
        $('#activateUserModal').modal('show');
      });
    });



    $('#btnSubmitActivate').on('click',function(e){
      e.preventDefault();
      $(".subBtns").addClass('hidden');
      $(".waitLoad").removeClass('hidden');
      $(".errors").addClass('hidden');
      $.ajax({
        url : '{{url('compliance/employer/online/activate_user')}}/'+$('#activate_user_id').val()+'/{{$employer->id}}',
        type : 'POST',
        data:$('#activateUserForm').serialize(),
        datatype : 'json',
        success:function(data){
         $(".subBtns").removeClass('hidden');
         $(".waitLoad").addClass('hidden');
         if (data.errors) {
           $.each(data.errors, function(key,valueObj){
            $('.'+key+'_error').removeClass('hidden');
            $('.'+key+'_error').text(valueObj);
          });
         }
         if (data.success) {
           $('#activateUserModal').modal('hide');
           swal('User','Activated successfully','success');
           employeruser_table.ajax.reload(null,false);
         }
       },
       error: function ($data) {
        $(".subBtns").removeClass('hidden');
        $(".waitLoad").addClass('hidden');
        swal('Dear! {{access()->user()->firstname}}','An error seems to have occurred. Kindly try again','warning');
      }, 

    });


    });




    $('#employeruser-table').on('click','.btnPayrollMerged',function(e) {
      e.preventDefault();
      $('a[href="#payroll_merging"]').tab('show');
      $('a[href="#payroll_merging"]').trigger('click');
    });


  });








</script>;

@endpush