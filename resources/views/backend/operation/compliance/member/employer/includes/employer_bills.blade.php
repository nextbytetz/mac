
{{--Contribution table--}}

<div class = "row">
    <div class="col-md-12" >
        <legend class="grey_modal" >@lang('labels.backend.legend.employer_bills')</legend>
        <div>&nbsp; </div>
        <div class="col-md-12">

        </div>
        {{--<div>&nbsp; </div>--}}
        &nbsp;

        <table class="display" cellspacing="0" width="100%" id ="bills-table">
            <thead>
                <tr >
                    <tr >
                        <th>Control No</th>
                        <th>Amount</th>
                        <th>Employees No</th>
                        <th>Description</th>
                        <th>Expire Date</th>
                        <th>Status</th>
                        <th>Genarated By</th>
                        {{-- <th>Contact</th> --}}
                        <th>Action</th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>


    @push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            $('#bills-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                   url : '{!! route('backend.compliance.employer.bills',$employer->id) !!}',
                   type : 'get'

               },
               columns: [
               { data: 'control_no', name: 'control_no'},
               { data: 'bill_amount', name: 'bills.bill_amount'},
               { data: 'member_count', name: 'bills.member_count' },
               { data: 'billed_item', name:'bills.billed_item' },
               { data: 'expire_date', name:'bills.expire_date'},
               { data: 'bill_status', name: 'bills.bill_status'},
               { data: 'generated_by', name:'generated_by',searchable:false,sortable:false},
               { data: 'action', name:'action'},
               { data: 'name', name:'users.name',searchable:true,visible:false},
               { data: 'name', name:'users.firstname',searchable:true,visible:false},
               { data: 'mobile_number', name:'bills.mobile_number',searchable:true,visible:false},
               ],


           });

        });







    </script>;

    @endpush