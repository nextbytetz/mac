
{{--contribution table--}}


<table class="display" cellspacing="0" width="100%" id ="inactive-receipts-table">
    <thead>
    <tr>

        <th>@lang('labels.backend.table.receipt.rctno')</th>
        <th>@lang('labels.backend.table.description')</th>
        <th>@lang('labels.backend.table.amount')</th>
        <th>@lang('labels.backend.finance.receipt.payment_type')</th>
        <th>@lang('labels.backend.table.payment_date')</th>
        <th>@lang('labels.backend.finance.receipt.captured_date')</th>
        <th>@lang('labels.backend.table.status')</th>
    </tr>
    </thead>

</table>



@push('inactive-receipts-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $("#tab_5_header").one("click", function() {
            $('#inactive-receipts-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                searching: false,
                paging: false,
                info: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax: {
                    url: '{!! route('backend.compliance.employer.inactive_receipts', $employer->id) !!}',
                    type: 'get'
                },
                columns: [
                    {data: 'rctno', name: 'rctno'},
                    {data: 'description', name: 'description'},
                    {data: 'amount', name: 'amount'},
                    {data: 'payment_type', name: 'payment_type'},
                    {data: 'rct_date', name: 'rct_date'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'status', name: 'status'}
                ],
            });
        });
    });
</script>;

@endpush