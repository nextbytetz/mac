<div class = "row">
  <div class="col-md-12" >
    <legend class="grey_modal" >Commitments</legend>
    <div>&nbsp; </div>
    <div class="col-md-12">

    </div>
    &nbsp;
    <table class="display" cellspacing="0" width="100%" id ="e-table">
      <thead>
        <tr >
          <th>Commitment Type</th>
          <th>User Submitted</th>
          <th>Date submitted</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>
    </table>

  </div>

</div>

<div id="update_commitment_modal" class="modal fade" role="dialog" data-focus="true" >
  <div class="modal-dialog modal-lg" role="document" style="background-color: white;">
    <form role="form" id="update_commitment_form">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Update Commitment: &nbsp;<span id="modal_name"></span></h5>
        </div>
        <div class="modal-body">
          <div class="row hidden" >
            <div class="col-md-5">
              <input type="text" name="commitment_id" id="commitment_id">
            </div>
          </div>
          <div class="row pb-2 pl-2" id="main_body">

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update_commitment_submit">Update</button>
          <button type="button" class="btn btn-secondary" id="" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>
  </div>
</div>


<div id="commitment_resolution_modal" class="modal fade" role="dialog" data-focus="true" >
  <div class="modal-dialog modal-lg" role="document" style="background-color: white;">
    <form role="form" id="update_commitment_form">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><span id="commitment_name"></span>&nbsp;</h5>
        </div>
        <div class="modal-body">
          <div class="row">
            <br>
            <div class="col-md-6">
             <label for="name">Resolution</label>
             <input id="commitment_resolution" name="name" class="form-control" readonly="readonly" />
             {{-- <textarea id="commitment_resolution" class="form-control" readonly="readonly"></textarea> --}}

           </div>
           {{-- <div class="col-md-6">&nbsp;</div> --}}
             <div class="col-md-6">
               <label for="email">Comments</label>
               <textarea id="commitment_comment" class="form-control" readonly="readonly"></textarea>
             </div>
           </div>
         </div>
       </div>
     </form>
   </div>
 </div>


 @push('after-script-end')
 {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}


 <script  type="text/javascript">

  $(function() {
    $('#e-table').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
       url : '{!! route('backend.compliance.employer.online.commitments',$employer->id) !!}',
       type : 'get'

     },
     columns: [
     { data: 'commitment_type', name: 'commitment_type'},
     { data: 'user_name', name: 'user_name'},
     { data: 'created_at', name:'created_at',searchable : false, orderable : false },
     { data: 'status', name:'status',searchable : false, orderable : false },
     { data: 'action', name:'action',searchable : false, orderable : false },
     ],
     rowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $('td', nRow).click(function() {
        let col = $(this).index()+1;
        let colText = $('#e-table thead th:nth-child('+col+')').text();
        // console.log('ii')
        if(colText){
          document.location.href = '{!! url('compliance/employer/online/view_commitment/') !!}/'+aData['id']; 
        }
      }).hover(function() {
        let col = $(this).index()+1;
        let colText = $('#e-table thead th:nth-child('+col+')').text();
        if(colText){
          $(this).css('cursor','pointer');                        
        }}, function() {
          $(this).css('cursor','auto');
        });
    }
  });


    $('#e-table').on('click','.update_commitment',function(e){
      e.preventDefault();
      let commitment_id = $(this).data('id');
      $.ajax({
        url : '{{url('compliance/employer/online/return_update_commitment_modal')}}/'+commitment_id,
        type : 'GET',
        datatype : 'json',
        success:function(data){
          if(data.success){
            $('#main_body').empty();
            $('#main_body').append(data.inputs);
            $('#modal_name').text(data.name);
            $('#commitment_id').val(commitment_id);
            $('#update_commitment_modal').modal('show');
          }else{
           $('#update_commitment_modal').modal('hide');
         }
       }
     });
    });


    $('#update_commitment_submit').on('click',function(e){
      e.preventDefault();
      let form = $('#update_commitment_form');
      $('.error_message').addClass('hidden');
      $.ajax({
        url : '{{url('compliance/employer/online/update_commitment')}}',
        type : 'POST',
        data: form.serialize(),
        datatype : 'json',
        success:function(data){
          if (data.success) {
           form.trigger('reset');
           $('#e-table').DataTable().ajax.reload(null,false);
           $('#update_commitment_modal').modal('hide');
           swal('Success','Susccess','success');

         }else{
          if (data.errors) {
            if (data.errors.comment){
              $('.error_comment').removeClass('hidden');
              $('.error_comment').text(data.errors.comment[0]);
            }
            if (data.errors.resolution){
              $('.error_resolution').removeClass('hidden');
              $('.error_resolution').text(data.errors.resolution[0]);
            }
            if (data.errors.commitment_id){
              form.trigger('reset');
              $('#update_commitment_modal').modal('hide');
            }
          } else {
            swal('Error','Request Failed! Please try again','error');
            $('#update_commitment_modal').modal('hide');
          }

        }
      }

    });
    });


    $('#e-table').on('click','.view_commitment_resolution',function(e){
      e.preventDefault();
      let commitment_id = $(this).data('id');
      $.ajax({
        url : '{{url('compliance/employer/online/return_commitment_resolution')}}/'+commitment_id,
        type : 'GET',
        datatype : 'json',
        success:function(data){
          if(data.success){
            $('#commitment_resolution').val(data.resolution);
            $('#commitment_comment').val(data.comment);
            $('#commitment_name').text(data.name);
            $('#commitment_resolution_modal').modal('show');
          }else{
           $('#commitment_resolution').val();
           $('#commitment_comment').val();
           $('#commitment_name').text();
           $('#commitment_resolution_modal').modal('hide');
         }
       }
     });
    });



  });


</script>
@endpush