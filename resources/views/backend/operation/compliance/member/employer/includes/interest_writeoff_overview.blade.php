
{{--interest-writeoff-overview-table--}}

<legend class="grey_modal" >@lang('labels.backend.legend.interest_writeoff_overview')</legend>
<table class="display" cellspacing="0" width="100%" id ="interest-writeoff-overview-table">
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.backend.table.min_date')</th>
        <th>@lang('labels.backend.table.max_date')</th>
        <th>@lang('labels.backend.table.total_months')</th>
        <th>@lang('labels.backend.table.amount')</th>
         </tr>
    </thead>

</table>

@push('interest-writeoff-overview-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#interest-writeoff-overview-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employer.write_off_interest_overview', $employer->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'status' , name: 'status', orderable : false, searchable : false},
                { data: 'min_date' , name: 'min_date', orderable : false, searchable : false },
                { data: 'max_date' , name: 'max_date' , orderable : false, searchable : false},
                { data: 'total_months' , name: 'total_months', orderable : false, searchable : false },
                { data: 'amount', name: 'amount' , orderable : false, searchable : false}
                            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).click(function() {
            document.location.href = url + "/compliance/employer/profile/interest_write_off_approve/"  + aData['id'];

            }).hover(function() {
            $(this).css('cursor','pointer');
            }, function() {
            $(this).css('cursor','auto');
            });
            }


        } );
    });
</script>;

@endpush