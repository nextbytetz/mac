
<div class="modal fade info-modal" id="add_letter_date_modal" tabindex="-1" role="dialog" aria-labelledby="info-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" style="text-align: center;">Letter details</h4>
            </div>
            <div class="modal-body" style="background-color: white">
                <div class="row">
                    <div class="col-md-12">
                     <div class="col-md-3">
                        <label>User:</label>
                    </div> 
                    <div class="col-md-9">
                        {{$installment->user}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                 <div class="col-md-3">
                    <label>Employer:</label>
                </div> 
                <div class="col-md-9">
                    {{$installment->employer}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
             <div class="col-md-3">
                <label>Description:</label>
            </div> 
            <div class="col-md-9">
                {{$installment->description}}
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
         <div class="col-md-3">
            <label>Letter date:</label>
        </div> 
        <div class="col-md-9">
            <input type="date" name="letter_date" value="{{isset($installment->letter_date) ? $installment->letter_date : ''}}" class="form-control">
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
     <div class="col-md-3">
        <label>Letter reference:</label>
    </div> 
    <div class="col-md-9">
        <input type="text" name="letter_reference" value="{{isset($installment->letter_reference) ? $installment->letter_reference : ''}}" class="form-control">
    </div>
</div>
</div>
</div>
<div class="modal-footer" style="background-color: white">
    <button type="button" id="save_letter_details" class="btn btn-primary">Save</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>







