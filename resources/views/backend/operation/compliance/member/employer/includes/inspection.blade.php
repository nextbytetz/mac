
{{--Employer Inspections--}}


<table class="display" cellspacing="0" width="100%" id ="employer-inspection-table">
    <thead>
    <tr>
        <th>@lang('labels.backend.compliance_menu.inspection.type')</th>
        <th>@lang('labels.backend.compliance_menu.inspection.task.visit_date')</th>
        <th>Staging</th>
        <th>Inspector</th>
        {{--<th>@lang('labels.backend.compliance_menu.inspection.task.finding')</th>
        <th>@lang('labels.backend.compliance_menu.inspection.task.resolution')</th>
        <th>@lang('labels.backend.compliance_menu.inspection.task.status')</th>--}}
    </tr>
    </thead>

</table>



@push('after-script-end')

<script  type="text/javascript">
    $(function() {
        $("#{{ $tab_header }}").one("click", function() {
            $('#employer-inspection-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                /*searching: false,*/
                paging: false,
                info: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax: {
                    url: '{!! route('backend.compliance.inspection.employer.get', $employer->id) !!}',
                    type: 'get'
                },
                columns: [
                    {data: 'inspection_type', name: 'a.name'},
                    {data: 'visit_date', name: 'employer_inspection_task.visit_date'},
                    {data: 'stage', name: 'b.name stage'},
                    {data: 'inspector', name: 'users.firstname'},
                    {data: 'id', name: 'employer_inspection_task.id', orderable: false, searchable: false, visible : false},
                ],
                'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                    $('td:not(:first-child)', $nRow).click(function() {
                        window.open(base_url + "/compliance/inspection/employer_task/" + $aData['id'], "_self");
                    }).hover(function() {
                        $(this).css('cursor', 'alias');
                    }, function() {
                        $(this).css('cursor', 'auto');
                    });
                }
            });
        });
    });
</script>;

@endpush