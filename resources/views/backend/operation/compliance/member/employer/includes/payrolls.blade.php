<div class = "row">
  <div class="col-md-12" >
    <legend class="grey_modal" >Employer Verified Payrolls</legend>
    <div>&nbsp; </div>
    <div class="col-md-12">

    </div>
    {{--<div>&nbsp; </div>--}}
    &nbsp;

    <table class="display" cellspacing="0" width="100%" id ="payrolls-table">
      <thead>
        <tr >
          <th>Payroll ID</th>
          <th>Payroll Type</th>
          <th>Payroll Description</th>
          <th>Commencement Date</th>
          <th>Registration Date</th>
          {{-- <th>Verification Status</th> --}}
          <th>Register User</th>
        </tr>
      </thead>
    </table>

  </div>

{{--   <div class="col-md-3">
    <div class="row">

      <div class="grey_modal">
        <table style="width:100%">
          <tr>
            <td  align="center"><h5><b><span class="light_dark_color">Payrolls Summary</span></b></h5></td>
          </tr>
        </table>
      </div>
    </div>

    <div class="row">
      <div class="light_grey_bg">&nbsp;</div>
      <div class="col-md-12 light_grey_bg" style="min-height: 70% !important;">
        <h6 class="underline" style="font-weight: lighter;">Number of Users :</h6>
        <p style="font-weight: bold;"></p>
        <h6 class="underline" style="font-weight: lighter;">Number Of Payrolls :</h6>
        <p style="font-weight: bold;"></p>
        <h6 class="underline" style="font-weight: lighter;">Approved Payrolls :</h6>
        <p style="font-weight: bold;"></p>
      </div>
    </div>
  </div> --}}

</div>



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}


<script  type="text/javascript">

  $(function() {


    var payrolls_table = $('#payrolls-table').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
       url : '{!! route('backend.compliance.employer.online.verified_payroll',$employer->id) !!}',
       type : 'get'
     },
     columns: [
     
     { data: 'payroll_id' , name: 'payroll_id'},
     { data: 'commencement_source', name: 'commencement_source',searchable: false, orderable: false},
     { data: 'payroll_description', name: 'payroll_description',searchable: false, orderable: false},
     { data: 'doc', name: 'doc',searchable: false, orderable: false},
     { data: 'created_at', name: 'created_at',searchable: false, orderable: false},
     // { data: 'status', name: 'status',searchable: false, orderable: false},
     { data: 'user', name: 'user', searchable: false, orderable: false},
     ],


   });;


  });
</script>
@endpush