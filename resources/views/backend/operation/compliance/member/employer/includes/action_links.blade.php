@if ($employer->employer_status == 3 || $employer->employer_status == 4)
    {{--Business is closed, may open it--}}
    {{--open business - only if was temporary closed--}}
    {{--@if($employer->getRecentEmployerClosure()->closure_type == 1)--}}
    <span>
    <a href="{!! route('backend.compliance.employer_closure.open.create', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" >
        <i class="icon fa fa-level-up" aria-hidden="true"></i>
    Open Business</a>
</span>
    {{--@endif--}}
@else
    {{--Business is active, may close it--}}
    {{--close business--}}
    <span>
    <a href="{!! route('backend.compliance.employer.closure.create', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" >
        <i class="icon fa fa-level-down" aria-hidden="true"></i>
    &nbsp;Close Business</a>
</span>
@endif



{{--modify--}}
@if($employer->approval_id  <> 1)
    <span>
    <a href="{!! route('backend.compliance.employer.edit', $employer->id)
    !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pencil-square-o"></i>&nbsp;@lang('buttons.general.modify')</a>
</span>
@else
    {{--Modify gen info--}}
    <span>
    <a href="{!! route('backend.compliance.employer.edit_gen_info', $employer->id)
    !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-pencil-square-o"></i>&nbsp;@lang('buttons.general.modify')</a>
</span>
@endif

{{--Write off Interest--}}
<span>
    <a href="{!! route('backend.compliance.employer.write_off_interest_page',
    $employer->id)
    !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-flag-o"></i>&nbsp;@lang('buttons.general.write_off_interest')</a>
</span>


{{--more Links--}}
@include('backend.operation.compliance.member.employer.includes.more_links')