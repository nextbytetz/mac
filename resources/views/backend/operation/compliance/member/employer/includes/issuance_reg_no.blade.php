<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint  lang="en-US">
<head>
    <meta charset="utf-8" />
    <title>@lang('labels.backend.member.print_issuance.title')</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Martin Luhanjo <m.luhanjo@nextbyte.co.tz>"/>
    <meta name="subject" content="{!! env('COMPANY') !!}, @lang('labels.backend.member.print_issuance.subject')"/>
    <meta name="keywords" content="certificate"/>
    <meta name="date" content="2017-08-04"/>
    {{ Html::style(asset_url() . "/nextbyte/css/backend/employer/issuance_reg_no.css", ['rel' => 'stylesheet', 'type' => 'text/css']) }}
</head>
<body>



<div id="certificate">
    <div class="center">



        <img id="wcf" src="{!! asset_url() . "/nextbyte/img/wcf_logo.jpg" !!}">

        <br/>
        <br/>

        <br/>
        <div class="h2">ISSUANCE OF EMPLOYERS' REGISTRATION NUMBER</div>
        <br/>
        <br/>
        <div>
            This is to acknowledge that your company has been submitting contributions to the Fund in accordance to the requirement of Workers Compensation Act [CAP 263 R.E. 2015] and that you
            are now a registered employer.
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="h4">Name of Employer   :   {!! $employer->name_formatted !!}</div>
        <br/>
        <br/>
        <div class="h4">Registration Number. {!! $employer->id_formatted !!}</div>

        <br/>

        <div>Your cooperation is truly appreciated and the Fund </br>
            is dedicated to provide adequate and equitable compensation.
        </div>
        <br/>

        <div><strong>Director General</strong></div>
        <div><strong>Workers Compensation Fund</strong></div>
        <div> Ground and 6<sup>th</sup> Floor</div>
        <div>GEPF House,Plot No. 37</div>
        <div>Regent Estate,Bagamoyo Road</div>
        <div>P.O BOX  79655</div>
        <div>Dar es Salaam</div>
        <div>Tel: +255 22 296107/8</div>
        <div>Fax: +255 22 2926109</div>
        <div>Email:  info@wcf.go.tz</div>
        <div>Website: www.wcf.go.tz</div>
        <div>Hotline: 0787 923 923</div>

        <br/>
        <br/>
        <br/>
    </div>
</div>


<script type="text/javascript">
    /*window.onload = function() {
        window.print();
        window.close();
    };*/
    window.onload = function() {
        window.print();
    };
    window.onafterprint = function(){
        window.close();
    };
    /* window.onfocus=function(){ window.close();} */
</script>
</body>
</html>
