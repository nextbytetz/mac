@extends('layouts.backend.main', ['title' => trans('labels.backend.member.load_employee_list'), 'header_title' => trans('labels.backend.member.load_employee_list')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')

{{ Html::style(asset_url() . "/nextbyte/plugins/formstone/css/upload.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/formstone/css/themes/light/upload.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}

<style>
.filelists {
    margin: 20px 0;
}

.filelists h5 {
    margin: 10px 0 0;
}

.filelists .cancel_all {
    color: red;
    cursor: pointer;
    clear: both;
    font-size: 10px;
    margin: 0;
    text-transform: uppercase;
}

.filelist {
    margin: 0;
    padding: 10px 0;
}

.filelist li {
    background: #fff;
    border-bottom: 1px solid #ECEFF1;
    font-size: 14px;
    list-style: none;
    padding: 5px;
    position: relative;
}

.filelist li:before {
    display: none !important;
}
/* main site demos */

.filelist li .bar {
    background: #eceff1;
    content: '';
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 0;
    z-index: 0;
    -webkit-transition: width 0.1s linear;
    transition: width 0.1s linear;
}

.filelist li .content_upload {
    display: block;
    overflow: hidden;
    position: relative;
    z-index: 1;
}

.filelist li .file {
    color: #455A64;
    float: left;
    display: block;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 50%;
    white-space: nowrap;
}

.filelist li .progress {
    color: #B0BEC5;
    display: block;
    float: right;
    font-size: 10px;
    text-transform: uppercase;
}

.filelist li .cancel {
    color: red;
    cursor: pointer;
    display: block;
    float: right;
    font-size: 10px;
    margin: 0 0 0 10px;
    text-transform: uppercase;
}

.filelist li.error .file {
    color: red;
}

.filelist li.error .progress {
    color: red;
}

.filelist li.error .cancel {
    display: none;
}
</style>

@endpush

@section('content')

<div class="nav_tab_pane_header">
    {{--Header Bar--}}
    <div class="row">
        <div class="col-md-12" >
            <div class="pull-right" >
                {{--load contribution--}}
                <span>
                    @if(($employer->employeeTemps()->count()))
                    @if ($employer->allowToLoadEmployeeLive() == 1)


                    {{--Undo wrongly loaded employees--}}
                    <span>
                        {!! HTML::decode(link_to_route('backend.compliance.employer_registration.undo_loaded_employees', "<i class='icon fa fa-undo' aria-hidden='true'></i>&nbsp;" . trans('buttons.general.undo'), $employer->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => trans('labels.backend.member.undo_loading_employees_confirm'), 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                    </span>




                    {{--Load employee live--}}
                    <a href="{!! route('backend.compliance.employer_registration.load_employee_list', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-check-circle"></i>&nbsp;@lang('labels.backend.member.load_employee_list')
                    </a>
                    @endif
                    @endif
                </span>



                {{--close--}}
                <span>
                    <a href="{!! route('backend.compliance.employer.profile', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;@lang('buttons.general.close')</a>
                </span>
            </div>
        </div>
    </div>
</div>
<br/>

{{--HEADER--}}
@include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

<br>
<br>

{{--error download--}}
<div class="row">
    <div class="col-md-12" >
        <div class="pull-right" >
            {{--download error--}}
            <span>
                @if(($employer->employeeTemps()->count()))
                {!! $employer->analysisStatus() !!}
                {!! $employer->linked_file_status_label !!}
                @if (($employer->employeeTemps()->where('error',1)->count()) || ($employer->documents()->where('error',1)->count()))
                <span>
                    {!! link_to_route('backend.compliance.employer_registration.download_error', trans('labels.general.download_error'), [$employer->id], ['class' => 'btn btn-secondary btn-sm pull-right text-red']) !!}
                </span>
                @endif
                @endif
            </span>
        </div>
    </div>
</div>




<div class = "row">
    @if($employer->isonline == 1)
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="emply">
            <thead>
                <tr>
                   <th>Firstname</th>
                   <th>Middlename</th>                
                   <th>Lastname</th>
                   <th>dob</th>                
                   <th>gender</th>
                   <th>job title</th>  
                   <th>Employment category</th>                             
                   <th>basicpay</th>
                   <th>grosspay</th> 
                   <th>Action</th>                        
               </tr>
           </thead>
       </table>
   </div>

   <div class="col-md-12" >
    <br><br>
    <a href="{{url('/compliance/generate_number/'.$employer->id)}}" class="btn btn-primary btn-md" id="generate">Generate WCF Number</a>
</div>

</div>




@else
<div class="col-md-12">
    <br/>
    <label class="control-label">@lang('labels.backend.finance.receipt.select_file')</label>
    {!! Form::model($employer, ['route' => ['backend.compliance.employer_registration.upload_employee_list',
    $employer->id], 'class' => 'form demo_form', 'role' => 'form', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="file_upload" data-upload-options='{"action":"{!! route('backend.compliance.employer_registration.upload_employee_list', $employer->id) !!}"}'></div>
    <div class="filelists">
        <h5>Complete</h5>
        <ol class="filelist complete">
        </ol>
        <h5>Queued</h5>
        <ol class="filelist queue">
        </ol>
        <span class="cancel_all">Cancel All</span>
    </div>
    {!! Form::close() !!}
</div>

</div>

<br/>
<div class="row">
    <div class="col-md-12" class="form-inline" >
    </div>
</div>


<br/>
<br/>
<h4>@lang('labels.backend.file.instruction')</h4>
<div class="row">
    <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-pink"></i>
           <i class="fa fa-info fa-stack-1x fa-inverse"></i>
       </span>
   </div>
   <div class='col-md-11'>
    <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.title')</u></h4>
    Microsoft Excel file xlsx <br/>
    <!--					@lang('labels.backend.file.format.file_helper')-->
</div>
</div>
<hr/>
<div class="row">
    <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-orange"></i>
           <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
       </span>
   </div>
   <div class='col-md-11'>
    <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.content')</u></h4>
    @lang('labels.backend.file.format.content_helper') <br/>
    @lang('labels.backend.file.format.column')  &nbsp;<span class="label label-success">firstname</span>&nbsp;<span class="label label-success">middlename</span> <span class="label label-success">lastname</span>  <span class="label label-success">dob </span> <span class="label label-success">basicpay</span> <span class="label label-success">allowance</span><span class="label label-success">gender</span>  <span class="label label-success">job_title</span> <span class="label label-success">employment_category</span> &nbsp;
</div>
</div>
<hr/>
<div class="row">
    <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-yellow"></i>
           <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
       </span>
   </div>
   <div class='col-md-6'>
    <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.example')</u></h4>
    @lang('labels.backend.file.format.example_helper') <br/><br/>
    <img src="{{ asset_url() . '/nextbyte/img/upload_employee_sample.png' }}" height="auto" width="auto">
</div>
</div>

@endif



@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/formstone/js/core.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/formstone/js/upload.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--{{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">
    $(function () {
        $(".file_upload").upload({
            maxSize: 1073741824,
            postKey: 'document_file37',
            beforeSend: onBeforeSend
        }).on("start.upload", onStart)
        .on("complete.upload", onComplete)
        .on("filestart.upload", onFileStart)
        .on("fileprogress.upload", onFileProgress)
        .on("filecomplete.upload", onFileComplete)
        .on("fileerror.upload", onFileError)
        .on("chunkstart.upload", onChunkStart)
        .on("chunkprogress.upload", onChunkProgress)
        .on("chunkcomplete.upload", onChunkComplete)
        .on("chunkerror.upload", onChunkError)
        .on("queued.upload", onQueued);

        $(".filelist.queue").on("click", ".cancel", onCancel);
        $(".cancel_all").on("click", onCancelAll);

        function onCancel(e) {
            console.log("Cancel");
            var index = $(this).parents("li").data("index");
            $(this).parents("form").find(".upload").upload("abort", parseInt(index, 10));
        }

        function onCancelAll(e) {
            console.log("Cancel All");
            $(this).parents("form").find(".upload").upload("abort");
        }

        function onBeforeSend(formData, file) {
            console.log("Before Send");
            formData.append("test_field", "test_value");
            // return (file.name.indexOf(".jpg") < -1) ? false : formData; // cancel all jpgs
            return formData;
        }

        function onQueued(e, files) {
            console.log("Queued");
            var html = '';
            for (var i = 0; i < files.length; i++) {
                html += '<li data-index="' + files[i].index + '"><span class="content_upload"><span class="file">' + files[i].name + '</span><span class="cancel">Cancel</span><span class="progress">Queued</span></span><span class="bar"></span></li>';
            }

            $(this).parents("form").find(".filelist.queue")
            .append(html);
        }

        function onStart(e, files) {
            console.log("Start");
            $(this).parents("form").find(".filelist.queue")
            .find("li")
            .find(".progress").text("Waiting");
        }

        function onComplete(e) {
            console.log(e);
            // All done!
        }

        function onFileStart(e, file) {
            console.log("File Start");
            $(this).parents("form").find(".filelist.queue")
            .find("li[data-index=" + file.index + "]")
            .find(".progress").text("0%");
        }

        function onFileProgress(e, file, percent) {
            console.log("File Progress");
            var $file = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");

            $file.find(".progress").text(percent + "%")
            $file.find(".bar").css("width", percent + "%");
        }

        function onFileComplete(e, file, response) {
            console.log("File Complete" + " " + response);
            if (response.trim() === "" || response.toLowerCase().indexOf("error") > -1) {
                $(this).parents("form").find(".filelist.queue")
                .find("li[data-index=" + file.index + "]").addClass("error")
                .find(".progress").text(response.trim());
            }
            else {
                var $target = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
                $target.find(".file").text(file.name);
                $target.find(".progress").remove();
                $target.find(".cancel").remove();
                $target.appendTo($(this).parents("form").find(".filelist.complete"));
            }
        }

        function onFileError(e, file, error) {
            console.log("File Error");
            $(this).parents("form").find(".filelist.queue")
            .find("li[data-index=" + file.index + "]").addClass("error")
            .find(".progress").text("Error: " + error);
        }

        function onChunkStart(e, file) {
            console.log("Chunk Start");
        }

        function onChunkProgress(e, file, percent) {
            console.log("Chunk Progress");
        }

        function onChunkComplete(e, file, response) {
            console.log("Chunk Complete");
        }

        function onChunkError(e, file, error) {
            console.log("Chunk Error");
        }
    });
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content")
    }
});


$(function() {
   var table = $('#emply').DataTable({
    processing: true,
    serverSide: true,
    ajax:{
        url : '{{url('/compliance/return_employees/'.$employer->id)}}', 
        type : 'get'
    },
    columns: [
    { data: 'firstname' , name: 'firstname'},
    { data: 'middlename', name: 'middlename'},
    { data: 'lastname', name: 'lastname'},
    { data: 'dob', name: 'dob' },
    { data: 'sex', name: 'sex' },
    { data: 'job_title', name: 'job_title' },
    { data: 'emp_cate', name: 'emp_cate' },
    { data: 'salary', name: 'salary' },
    { data: 'grosspay', name: 'grosspay' },
    { data: 'action', name: 'action' }
    ],


});


// generat wcf number
// if ( ! table.data().any() ) { //is empty
//     alert( 'Empty table' );
// }
//generat


 // edit employee ===============

 $('#emply').on('click','.btnEdit[data-edit]',function(e){
    e.preventDefault();
    var url = $(this).data('edit');
    swal({
        title: "Are you sure want to Edit this Employee?",
        type: "info",
        showCancelButton: true,
        confirmButtonClass: "btn-info",
        confirmButtonText: "Confirm",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    }, 
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                url : url,
                type : 'GET',
                datatype : 'json',
                success:function(data){
                    $('#edit_ID').val(data.id);
                    $('#firstname').val(data.firstname);
                    $('#middlename').val(data.middlename);
                    $('#lastname').val(data.lastname);
                    $('#dob').val(data.dob);
                    $('#sex').val(data.sex);
                    $('#salary').val(data.salary);
                    $('#grosspay').val(data.grosspay);
                    $('#job_title').val(data.job_title);
                    $('#emp_cate').val(data.emp_cate);
                    $('#mdlEditData').modal('show');
                }

            });
        }
    });
});



 $('#btnUpdate').on('click',function(e){
    e.preventDefault();
    var url = "/compliance/online_uploaded_employees/"+$('#edit_ID').val();
    var frm = $('#frmDataEdit');
    $.ajax({
        type :'PUT',
        url : url,
        dataType : 'json',
        // data : frm.serialize(),
        data: {
            // '_token': $('input[name=_token]').val(),
            'firstname': $("#firstname").val(),
            'lastname': $('#lastname').val(),
            'middlename': $('#middlename').val(),
            'sex': $('#sex').val(),
            'dob': $('#dob').val(),
            'job_title': $('#job_title').val(),
            'emp_cate': $('#emp_cate').val(),
            'grosspay': $('#grosspay').val(),
            'salary': $('#salary').val()
        },
        success:function(data){
            // console.log(data);
            if (data.errors) {

                if (data.errors.firstname) {
                    $('.error_firstname').removeClass('hidden');
                    $('.error_firstname').text(data.errors.firstname);
                }
                if (data.errors.middlename) {
                    $('.error_middlename').removeClass('hidden');
                    $('.error_middlename').text(data.errors.middlename);
                }
                if (data.errors.lastname) {
                    $('.error_lastname').removeClass('hidden');
                    $('.error_lastname').text(data.errors.lastname);
                }
                if (data.errors.sex) {
                    $('.error_sex').removeClass('hidden');
                    $('.error_sex').text(data.errors.sex);
                }
                if (data.errors.dob) {
                    $('.error_dob').removeClass('hidden');
                    $('.error_dob').text(data.errors.dob);
                }
                if (data.errors.salary) {
                    $('.error_salary').removeClass('hidden');
                    $('.error_salary').text(data.errors.salary);
                }
                if (data.errors.grosspay) {
                    $('.error_grosspay').removeClass('hidden');
                    $('.error_grosspay').text(data.errors.grosspay);
                }
                if (data.errors.job_title) {
                    $('.error_job_title').removeClass('hidden');
                    $('.error_job_title').text(data.errors.job_title);
                }
                if (data.errors.emp_cate) {
                    $('.error_emp_cate').removeClass('hidden');
                    $('.error_emp_cate').text(data.errors.emp_cate);
                }


            }
            if (data.success == true) {
                            // console.log(data);
                            $('.firstname').addClass('hidden');
                            $('.middlename').addClass('hidden');
                            $('.lastname').addClass('hidden');
                            $('.sex').addClass('hidden');
                            $('.dob').addClass('hidden');
                            $('.job_title').addClass('hidden');
                            $('.emp_cate').addClass('hidden');
                            $('.salary').addClass('hidden');
                            $('.grosspay').addClass('hidden');
                            frm.trigger('reset');
                            $('#mdlEditData').modal('hide');
                            swal('Success!','Employee Updated Successfully','success');
                            table.ajax.reload(null,false);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Please Reload to read Ajax');
                    }
                });
});


                //end edit employee ==========



            });

        </script>; 

        @endpush
