<div class = "row">
  <div class="col-md-12" >
    <legend class="grey_modal" >@lang('labels.backend.legend.contribution_arrears')</legend>
    <div>&nbsp; </div>
    <div class="col-md-12">

      <div class="pull-right">
        <button class="btn btn-info btn-md btnddarrear" role="button" data-employername="'.{{$employer->name}}.'" data-employerid="'.{{$employer->id}}.'">Add Arrear </button>
      </div>
    </div>
    
    &nbsp;

    <table class="display" cellspacing="0" width="100%" id ="contributionarrears-table">
      <thead>
        <tr >
          <th>contribution Month</th>
          {{--  <th>Delayed Months</th> --}}
          <th>Payroll</th>
          {{-- <th>Payroll ID</th> --}}
          {{-- <th>Status</th> --}}
          <th>Action</th>
        </tr>
      </thead>
    </table>

  </div>

</div>


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url() . "/nextbyte/plugins/maskmoney/js/maskmoney.min.js", ['type' => 'text/javascript']) }}

<script  type="text/javascript">
  $(function() {
   $('.money').maskMoney({
    precision : 2,
    affixesStay : false
  });

   var contributionarrears_table=
   $('#contributionarrears-table').DataTable({
    processing: true,
    serverSide: true,
    stateSave: true,
    stateSaveCallback: function (settings, data) {
      localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
    },
    stateLoadCallback: function (settings) {
      return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
    },
    ajax:{
     url : '{!! route('backend.compliance.employer.online.get_arrears',$employer->id) !!}',
     type : 'get'

   },
   columns: [
   { data: 'contrib_month' , name: 'contribution_arrears.contrib_month'},
   { data: 'payroll_user', name: 'payroll_user',searchable: false, orderable: false},
   // { data: 'phone', name: 'users.phone',visible: false,searchable: false, orderable: false},
   // { data: 'payroll_id', name: 'employer_user.payroll_id',visible: false,searchable: false, orderable: false},
   { data: 'action', name: 'action', searchable: false, orderable: false},
   ],


 });

   $('#contributionarrears-table').on('click','.removearrearbtn',function(e){
    e.preventDefault();
    var arrear = $(this).data('arrear');
    swal({
      title: "Attention! {{access()->user()->firstname}}",
      text: "<h6>This will Remove "+$(this).data('arrear_month')+" arrear from {{$employer->name}}!<br> <h5>Are you sure about this?</h6> ", 
      type: "info",
      html: true,
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "YES- REMOVE",
      cancelButtonText: "NO-CANCEL",
      closeOnConfirm: true,
      closeOnCancel: true,
    }, 
    function(isConfirm) {
      if (isConfirm) {
        $("#arrear_id").val(arrear);
        $("#removeArrearModal").modal('show');
      }
    });

  });



   $('#removeArrearSubmit').on('click',function(e){
    e.preventDefault();

    var myform = $('#removeArrearForm');
    var arrear = $('#arrear_id').val();

    $.ajax({
      url : '{{url('compliance/employer/online/clear_arrears/')}}/'+arrear,
      type : 'POST',
      data : myform.serialize(),
      datatype : 'json',
      success:function(data){
       if(data.success){
        swal({
          title: "{{access()->user()->firstname}}",
          text: "<h6>"+data.success.message+"</h5> ", 
          type: "success",
          html: true,
          showCancelButton: false,
          confirmButtonClass: "btn-success",
          confirmButtonText: "OK",
          closeOnConfirm: true,
          closeOnCancel: true,
        }, 
        function(isConfirm) {
          $("#arrear_id").val();
          $("#removeArrearModal").modal('hide');
          location.reload();
        });

      }
      if(data.errors){
        if (data.errors.message) {
          swal({
            title: "Sorry! {{access()->user()->firstname}}",
            text: "<h5> Request Failed! <br>"+data.errors.message+"</h5> ", 
            type: "error",
            html: true,
            showCancelButton: false,
            confirmButtonClass: "btn-success",
            confirmButtonText: "OK",
            closeOnConfirm: true,
            closeOnCancel: true,
          }, 
          function(isConfirm) {
            $("#arrear_id").val();
            myform.trigger('reset');
            $("#removeArrearModal").modal('hide');
            location.reload();
          });
        }else{
         $("#arrear_id").val();
         $("#removeArrearModal").modal('hide');
         myform.trigger('reset');
         swal('Request Failed!','Something went wrong! Please try again','error');
         location.reload();
       }
     }
   }

 });

  });




   $('.btnddarrear').on('click',function(e){
    e.preventDefault();
    $("#AddFields").empty();
    $('.error_contribmonth').addClass('hidden');
    $('.error_payroll').addClass('hidden');
    $('.error_reason_for_adding').addClass('hidden');
    $('#employer').val();      
    $('#contrib_month').val();
    $('#employer_id').val();
    $(".footerButtons").removeClass('hidden');
    $(".wait").addClass('hidden');
    swal({
      title: "{{access()->user()->firstname}}",
      text: "<h6>Are you sure you want to Add arrear to {{$employer->name}}?</h6> ", 
      type: "info",
      html: true,
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Confirm",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true,
    }, 
    function(isConfirm) {
      if (isConfirm) {
        var employer={{$employer->id}};
        $.ajax({ 
          url : '{{url('compliance/employer/online/add_arrears_form')}}/'+employer,
          type : 'GET',
          datatype : 'json',
          success:function(data){
           $('#employer').text('{{$employer->name}}');      
           $('#contrib_month').val();
           $('#employer_id').val({{$employer->id}});
           var reason_value = $('input[name=reason_for_adding]:checked', '#addarreaform').val();
           addFieldsOnChangeReason(reason_value);

           var payroll = data.payroll;
           if (payroll.length == 1) {
             $('#AddFields').append('<div class="row"><div class="col-xs-12"><input type="radio" checked="checked" value="'+data.payroll[0]['payroll_id']+'" name="payrollid"> '+data.payroll[0]['name']+' - '+data.payroll[0]['payroll_id']+'</div></div>');
           } else {
            for (var i = payroll.length - 1; i >= 0; i--) {
              $('#AddFields').append('<div class="row"><div class="col-xs-12"><input type="radio" value="'+data.payroll[i]['payroll_id']+'" name="payrollid"> '+data.payroll[i]['name']+' - '+data.payroll[i]['payroll_id']+'</div></div>');
            }
          }

          $('#addarrea').modal('show');

        }

      });

      }
    });
  });


   $(document).on('focusin', '.arrearcontribdate', function(){
     $(this).datepicker({
       defaultViewDate: {
        year: '{{\Carbon\Carbon::parse($employer->doc)->format('Y')}}',
        month: '{{\Carbon\Carbon::parse($employer->doc)->format('m')}}',
        day: '{{\Carbon\Carbon::parse($employer->doc)->format('d')}}',
      },
      autoclose: true,
      format: 'dd-mm-yyyy',
      startDate: "{{\Carbon\Carbon::parse($employer->doc)->format('d-m-Y')}}",
      endDate:  "-2m",
    })});

   $('#submitArrearForm').on('click',function(e){
    e.preventDefault();
    $('.error_contribmonth').addClass('hidden');
    $('.error_payroll').addClass('hidden');
    $('.error_reason_for_adding').addClass('hidden');
    $('.wait').removeClass('hidden');
    $('.footerButtons').addClass('hidden');
    var reason_value = $('input[name=reason_for_adding]:checked', '#addarreaform').val();

    if (reason_value == 'missing_month' || reason_value == 'forgot_employees' ) {
      var contribMonth =  $('input[name=contrib_month]', '#addarreaform').val();
      if (!contribMonth) {
        addFieldsOnChangeReason(reason_value);
      }
    }

    $.ajax({
      url : '{{url('compliance/employer/online/add_arrears/')}}/'+$('#employer_id').val(),
      type : 'POST',
      data:$('#addarreaform').serialize(),
      datatype : 'json',
      success:function(data){
       $('.wait').addClass('hidden');
       $('.footerButtons').removeClass('hidden');
       if(data.errors){
        if (data.errors.contrib_month) {
          $('.error_contribmonth').removeClass('hidden');
          $('.error_contribmonth').text(data.errors.contrib_month);
        }
        if (data.errors.payrollid) {
          $('.error_payroll').removeClass('hidden');
          $('.error_payroll').text(data.errors.payrollid);
        }
        if (data.errors.reason_for_adding) {
          $('.error_reason_for_adding').removeClass('hidden');
          $('.error_reason_for_adding').text(data.errors.reason_for_adding);
        }
        if (data.errors.message) {
          swal('Request Failed!',data.errors.message,'error');
        }
      }
      if(!data.errors){
         // myform.trigger('reset');
         contributionarrears_table.ajax.reload(null,false);
         swal('Arrears','added successfully','success');
         $('#addarrea').modal('hide');
       }

     }
   });

  });


   $('.reasonForAdding').click(function(e){
    addFieldsOnChangeReason(this.value);
  });



   function addFieldsOnChangeReason(reason_value) {
    $("#addRows").empty();
    switch(reason_value) {
      case 'wrong_commence':
      $("#addRows").empty();
      break;
      default:
      $('#addRows').append('<div class="row contrib_month"><div class="form-group"><div class="col-xs-4"><label for="contrib_month">Contribution Month:</label></div><div class="col-xs-8"><input type="text" class="form-control arrearcontribdate" name="contrib_month" id="contrib_month" placeholder="Click to Add date" /><p class="error_contribmonth text-danger hidden"></p></div></div></div>');

    } 
  }

});

$('#contributionarrears-table').on('click','.uploadArrearModalButton',function(e){
  e.preventDefault();
  $("#uploadArrearId").val();
  $('#uploadArrearEmployeesForm').trigger('reset');
  $('.wait').addClass('hidden');
  $('.footerButtons').removeClass('hidden');

  var arrear_id = $(this).data('arrear_id'); 
  var arrear_month = $(this).data('arrear_month');
  swal({
    title: "Dear {{access()->user()->firstname}}",
    text: "<h6>Are you sure you want to upload "+arrear_month+" arrear for {{$employer->name}}!</h6> ", 
    type: "warning",
    html: true,
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes Please",
    cancelButtonText: "No Cancel",
    closeOnConfirm: true,
    closeOnCancel: true,
  }, 
  function(isConfirm) {
    if (isConfirm) {
      $(".uploadArrearTitle").text('Upload '+arrear_month+' employees for {{$employer->name}}');
      $(".uploadArrearMonth").text(arrear_month);
      $('#uploadArrearId').val(arrear_id);

      $("#uploadArrearEmployeesModal").modal('show');
    } 
  });
});


$('#submitArrearEmployeesForm').on('click',function(e){
  e.preventDefault();

  $('.wait').removeClass('hidden');
  $('.footerButtons').addClass('hidden')

  $('.error_arrear_general').text();
  $('.error_div_general').addClass('hidden');

  $('.error_total_grosspay').text();
  $('.error_employee_count').text();
  $('.error_attachment').text();
  $('.error_total_grosspay').addClass('hidden');
  $('.error_employee_count').addClass('hidden');
  $('.error_attachment').addClass('hidden');


  var arrear_id =  $('#uploadArrearId').val();
  var upload_arrear_form = $('#uploadArrearEmployeesForm');

  var file_data = $('#attachment').prop('files')[0];
  var employee_count = $('#employee_count').val();
  var total_grosspay = $('#total_grosspay').val();

  var form_data = new FormData();
  form_data.append('attachment', file_data);
  form_data.append('employee_count', employee_count);
  form_data.append('total_grosspay', total_grosspay);
  form_data.append('arrear_id', arrear_id);


  $.ajax({
    url : '{{url('compliance/employer/online/upload_arrear/')}}/'+arrear_id,
    data: form_data,
    type: 'POST',
    contentType: false, 
    cache: false, 
    processData: false,
    success:function(data){
     if(data.success){
      swal({
        title: "Dear {{access()->user()->firstname}}",
        text: "<h6> Request submitted successfully</h5> ", 
        type: "success",
        html: true,
        showCancelButton: false,
        confirmButtonClass: "btn-success",
        confirmButtonText: "OK",
        closeOnConfirm: true,
        closeOnCancel: true,
      }, 
      function(isConfirm) {
       location.reload();
       $("#uploadArrearId").val();
       $("#uploadArrearEmployeesModal").modal('hide');

     });
    }else{
     if (data.errors) {
       $('.error_arrear_general').text('There was the problem on performing the requested action, please try again ');
       $('.error_div_general').removeClass('hidden');
       
       if (data.errors.total_grosspay) {
        $('.error_total_grosspay').removeClass('hidden');
        $('.error_total_grosspay').text(data.errors.total_grosspay);
      }

      if (data.errors.employee_count) {
        $('.error_employee_count').removeClass('hidden');
        $('.error_employee_count').text(data.errors.employee_count);
      }

      if (data.errors.attachment) {
        $('.error_attachment').removeClass('hidden');
        $('.error_attachment').text(data.errors.attachment);
      }

      if (data.errors.message) {
        swal('Sorry! {{access()->user()->firstname}}',data.errors.message,'error');
      }

    } else {
     swal('Sorry! {{access()->user()->firstname}}','An error has occured. Please try agaian','error');
   }
 }
 $('.wait').addClass('hidden');
 $('.footerButtons').removeClass('hidden');
}
});

  
});










</script>;

@endpush