<div class="row">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Summary</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.general.status') :</h6>
        <p style="font-weight: bold;">{!! $advance_payment->status_formatted !!}</p>
        <h6 class="underline" style="font-weight: lighter;">Period :</h6>
        <p style="font-weight: bold;">{!! $advance_payment->period !!}</p>
        <h6 class="underline" style="font-weight: lighter;">Employee Count :</h6>
        <p style="font-weight: bold;">{!! $advance_payment->employee_count !!}</p>
        <h6 class="underline" style="font-weight: lighter;">Start Month :</h6>
        <p style="font-weight: bold;">{!! $advance_payment->start_month_formatted !!}</p>
        <h6 class="underline" style="font-weight: lighter;">Description :</h6>
        <p style="font-weight: bold;">{!! $advance_payment->description !!}</p>
    </div>
</div>
