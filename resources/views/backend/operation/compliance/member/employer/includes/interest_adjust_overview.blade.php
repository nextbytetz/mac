
{{--interest-adjustment-overview-table--}}

<legend class="grey_modal" >@lang('labels.backend.legend.interest_adjustment_overview')</legend>
<br/>
<table class="display" cellspacing="0" width="100%" id ="interest-adjustment-overview-table">
    <thead>
    <tr>
        <th></th>
        <th>@lang('labels.backend.table.contrib_month')</th>
        <th>@lang('labels.backend.table.late_months')</th>
        <th>@lang('labels.backend.table.interest')</th>
        <th>@lang('labels.backend.table.adjust_amount')</th>
    </tr>
    </thead>

</table>

@push('interest-adjustment-overview-script-end')

<script  type="text/javascript">
    $(function() {
        var url = "{!! url("/") !!}";
        $('#interest-adjustment-overview-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: true,
            info:false,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax:{
                url : '{!! route('backend.compliance.employer.adjust_interest_overview', $employer->id) !!}',
                type : 'get'
            },
            columns: [
                { data: 'status' , name: 'status' ,orderable : false, searchable : false},
                { data: 'miss_month_formatted' , name: 'miss_month'},
                { data: 'late_months' , name: 'late_months'},
                { data: 'amount' , name: 'amount' },
                { data: 'adjust_amount', name: 'adjust_amount'}
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = url + "/compliance/employer/profile/interest/"  + aData['id'];

                }).hover(function() {
                    $(this).css('cursor','pointer');
                }, function() {
                    $(this).css('cursor','auto');
                });
            }

        } );
    });
</script>;

@endpush