
<div class="modal fade info-modal" id="due_date_date_modal" tabindex="-1" role="dialog" aria-labelledby="info-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" style="text-align: center;">Due date</h4>
            </div>
            <div class="modal-body" style="background-color: white">
                <div class="row">
                    <div class="col-md-12">
                       <div class="col-md-3">
                        <label>User:</label>
                    </div> 
                    <div class="col-md-9">
                        {{$installment->user}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <div class="col-md-3">
                    <label>Employer:</label>
                </div> 
                <div class="col-md-9">
                    {{$installment->employer}}
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-12">
               <div class="col-md-3">
                <label>Description:</label>
            </div> 
            <div class="col-md-9">
                {{$installment->description}}
            </div>
        </div>
    </div> --}}
    <hr>
    <div class="row">
        <div class="col-md-12">
           <div class="col-md-3">
            <label>Due date:</label>
        </div> 
        <div class="col-md-9">
            <input type="date" name="due_date" value="" class="form-control" id="due_date">
            <input type="hidden" name="phase" value="" id="phase">
        </div>
    </div>
</div>
</div>
<div class="modal-footer" style="background-color: white">
    <button type="button" id="save_due_date" class="btn btn-primary">Save</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>







