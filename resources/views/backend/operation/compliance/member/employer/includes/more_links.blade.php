{{-- Employer registration -> MORE LINKS OPTION--}}

<div class="dropdown" style="display: inline-block;"> <a class="btn btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown">
    @lang('buttons.general.more')
</a>
<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenuButton" >

    {{--      online services group--}}
    <li class="dropdown-submenu pull-left">
        <a href="#"  tabindex="-1" data-toggle="dropdown" class="dropdown-toggle dropdown-item" aria-haspopup="true" aria-expanded="false">Online Services</a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuButton">
            {{--    style="right: 0; left: auto;"--}}
            @if ($employer->source)
            <li>
                {{--online employer verification--}}
                {!! link_to_route('backend.compliance.employer.verification', trans('buttons.backend.member.employer.verification'), [$employer->id], ['class' => 'dropdown-item']) !!}
                <div class="dropdown-divider"></div>
            </li>
            @endif
            @if ($employer->isonline)
            <li class="dropdown-item">
                {!! link_to_route('backend.compliance.employer.advance_payment', 'Advance Payment Requests', [$employer->id], ['class' => 'dropdown-item']) !!}
                <div class="dropdown-divider"></div>
            </li>
            @endif
            @if ($employer->isonline == 1 && $employer->approval_id== 1 )
            <li>
                {!! link_to_route('backend.compliance.employer.online.profile', 'Manage Online Employer Profile', [$employer->id], ['class' => 'dropdown-item']) !!}
                <div class="dropdown-divider"></div>
            </li>
            @endif
            <li class="dropdown-item">
                {{--online notification application--}}
                {!! link_to_route('backend.compliance.employer.incident', 'Online Notification Application', [$employer->id], ['class' => 'dropdown-item']) !!}
            </li>
        </ul>
        <div class="dropdown-divider"></div>
    </li>

    {{--employer registration profile--}}
    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.employer_registration.profile', $employer->id) !!}" >@lang('buttons.backend.member.employer.registration_profile')</a>
        <div class="dropdown-divider"></div>
    </li>

    {{--load employee list--}}
    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.employer_registration.employee_list_file', $employer->id) !!}" >@lang('buttons.backend.member.employer.load_employee_list')</a>
        <div class="dropdown-divider"></div>
    </li>

    {{--Interest Refund--}}
    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.booking_interest.refund_overview', $employer->id) !!}" >@lang('buttons.backend.member.employer.interest_refund')</a>
        <div class="dropdown-divider"></div>
    </li>

    {{--Business closure--}}
    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.employer.closure.index', $employer->id) !!}" >Business De-Registrations</a>
        <div class="dropdown-divider"></div>
    </li>

    {{--Partcular changes--}}
    {{--@if(env('TESTING_MODE') == 1)--}}
    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.employer.change_particular.index', $employer->id) !!}" >Particular Changes</a>
        <div class="dropdown-divider"></div>
    </li>
    {{--@endif--}}

    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.employer.contrib_modification.index', $employer->id) !!}" >Contribution Modification</a>
        <div class="dropdown-divider"></div>
    </li>

    {{--      erroneous contribution--}}
    {{-- @if(env('TESTING_MODE'))
    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.employer.change_particular.index', $employer->id) !!}" >Erroneous Contribution</a>
        <div class="dropdown-divider"></div>
    </li>
    @endif --}}

    {{--close--}}
    <li>
        <a class="dropdown-item" href="{!! route('backend.compliance.employer.index') !!}" >@lang('buttons.general.close')</a>
    </li>


</ul>

</div>