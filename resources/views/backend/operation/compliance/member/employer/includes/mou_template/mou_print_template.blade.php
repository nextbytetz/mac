<html><title>Instalment MOU</title><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width">
  <style type="text/css" media="print">
    @page 
    {
      size: auto;   /* auto is the initial value */
      margin: 0mm;  /* this affects the margin in the printer settings */
    }
  </style>
  <style type="text/css">
    {{ Html::style(asset_url() . "/favicon/mac_favicon.ico", ['rel' => 'stylesheet icon', 'type' => 'image/x-icon']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon.png", ['rel' => 'apple-touch-icon']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-57x57.png", ['rel' => 'apple-touch-icon', 'sizes' => '57x57']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-72x72.png", ['rel' => 'apple-touch-icon', 'sizes' => '72x72']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-76x76.png", ['rel' => 'apple-touch-icon', 'sizes' => '76x76']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-114x114.png", ['rel' => 'apple-touch-icon', 'sizes' => '114x114']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-120x120.png", ['rel' => 'apple-touch-icon', 'sizes' => '120x120']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-144x144.png", ['rel' => 'apple-touch-icon', 'sizes' => '144x144']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-152x152.png", ['rel' => 'apple-touch-icon', 'sizes' => '152x152']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-180x180.png", ['rel' => 'apple-touch-icon', 'sizes' => '180x180']) }}
    {{ Html::style(asset_url() . "/global/css/bootstrap.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/icons_fonts/elegant_font/elegant.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/pages/global/css/global.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/global/css/components.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/global/css/components.min.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/layouts/layout-left-top-menu/css/layout.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/pages/login/login-v1/css/login.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/nextbyte/css/custom.css", ['rel' => 'stylesheet']) }}
  </style>

</head><body>   
  <div id="printMou" ><div class="container" style="margin: 8px;">
    <br><br>
    <table>
     <thead></thead>
     <tbody>
      <tr>
       <td colspan="2">This <strong>MEMORANDUM OF UNDERSTANDING (Agreement)</strong> (hereinafter referred to as the "Agreement” is made this <strong>{!!\Carbon\Carbon::parse($installment->agreement_date)->format('j<\s\u\p>S</\s\u\p> F Y')!!}</strong></td>
     </tr>
     <tr><td colspan="2"> &nbsp; &nbsp;</td></tr>
     <tr>
       <td style="text-align:center;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BETWEEN</strong></td>
     </tr>
     <tr><td colspan="2"> &nbsp;</td></tr>
     <tr>
      <td colspan="2">The Board of Trustees of the Workers Compensation Fund, a statutory body established under Section 5 of the Workers Compensation Act [Cap 263 R.E. 2015] whose address for the purpose hereof is P. O. Box 79655, Dar es Salaam (hereinafter referred to as “WCF") on the one part;</td>
    </tr>
    <tr><td colspan="2"> &nbsp;</td></tr>
    <tr>
     <td style="text-align:center"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND</strong></td>
   </tr>
   <tr><td colspan="2"> &nbsp;</td></tr>
    <?php
        $box_no = (int) filter_var($employer->box_no, FILTER_SANITIZE_NUMBER_INT);
    ?>
   <tr>
    <td colspan="2">{{$employer->name}} whose address for the purposes hereof is P.O. Box {{$box_no.' '.$employer->district_name}} (hereinafter referred to as the "THE EMPLOYER ") of the other part.</td>
  </tr>

  <tr><td colspan="2"> &nbsp;</td></tr>
  <tr>
   <td style="text-align:left"><strong>PREAMBLE</strong></td>
 </tr>
 <tr><td colspan="2"> &nbsp;</td></tr>
 <tr>
  <td colspan="2">The parties identified by their signatures appended to this Agreement have agreed to form this Agreement upon request by the employer and approval on the part of WCF for the payment of arrears of contributions by instalment basis.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
 <td style="text-align:left"><strong>WHEREAS;</strong></td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(a) The Employer is operating in Tanzania Mainland with a statutory obligation to contribute to the WCF as provided under the Workers Compensation Act [Cap 263 R.E. 2015] read together with the Workers Compensation Regulations G.N. No.185 of 2016, the Workers Compensation (Payment of Tariff) Regulations G.N. No.169 of 2015, the Workers Compensation (Payment of Tariff) Regulations G.N. 212A of 2016 and the Workers Compensation (Payment of Tariff) Regulations G.N. No. 229A of 2017.</td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(b) That as at {!!\Carbon\Carbon::parse($installment->agreement_date)->format('j<\s\u\p>S</\s\u\p> F Y')!!} the Employer is owing the WCF a total of TZS {{number_format($portal_details['total'],2)}} (Tanzania Shillings {{$portal_details['total_numbers_in_words']}} only) being outstanding amount(Exclude Interest) for the period from {{$portal_details['pay_description']}}. </td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(c) That on {!!\Carbon\Carbon::parse($installment->agreement_date)->format('j<\s\u\p>S</\s\u\p> F Y')!!} through Employers’ Online Self-Service Portal, the Employer requested for payment of outstanding amount(Exclude Interest) {{number_format($portal_details['total'],2)}} (Tanzania Shillings {{$portal_details['total_numbers_in_words']}} only) in installment basis within the period and amount as specified in the Instalment Payment Request Form which forms part of this Agreement.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(d) That in response to the Employer’s request, the WCF allowed the Employer to make payment of outstanding amount(Exclude Interest) by installments within the period and amount as specified in the Instalment Payment Request Form.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>NOW THEREFORE THE PARTIES MUTUALLY AGREE as follows;</strong></td>
</tr>
<tr>
  <td colspan="2"><strong>1.0 INTERPRETATION</strong></td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">1.1 For the purposes of this Agreement –</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(a)  “Act’’ means the Workers Compensation Act, [CAP 263 R.E. 2015] and its Regulations;</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(b) “Arrears of contributions” means unpaid statutory contributions which are compulsorily paid by the Employer under the Workers Compensation Act;</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(c) “Employer” means {{$employer->name}}.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">“Information’’ means any data, documents, reports, certified or authenticated copies thereof or other communications between the two parties; </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(e) “Interest” means a ten percentum (10%) of the unpaid amount following the date from which the payment should have been made to be recovered as debt owing to the WCF by the Employer;</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(f) “Parties” means {{$employer->name}}, and the Board of Trustees of the WORKERS COMPENSATION FUND;</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>2.0 COMMENCEMENT</strong></td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">2.1 This Agreement shall come into force on the {{$portal_details['first_due_date']}} and shall come to an end on the {{$portal_details['last_due_date']}}.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>3.0 DURATION</strong></td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">3.1 This Agreement shall remain in force for a period of twelve (12) months.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>4.0 OBJECTIVES OF THE AGREEMENT </strong></td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">4.1 These are; </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(a) To maintain a conducive working relationship between the WCF and its primary stakeholders by exhausting implementation of the Act and its Regulations. </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(b) To preserve the spirit of Alternative Dispute Resolution (ADR) as an amicable solution to avoid resorting to litigation at first instance.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(c) To provide the total amount of the accrued arears of contributions and interests on the part of the Employer.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(d) To provide the modality of payment of the accrued debt within time stipulated herein and agreed by the parties.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(e) To provide for the consequence of failure to adhering to the terms and conditions agreed under this Agreement.</td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>5.0 OBLIGATIONS OF THE PARTIES </strong></td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>5.1 INSTALMENT PAYMENT </strong></td>
</tr>


<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(a) That by this Agreement, the Employer has agreed to pay outstanding amount(Exclude Interest) to the tune of Tanzanian Shillings {{number_format($portal_details['total'],2)}} within the period starting from {{$portal_details['first_due_date']}} and shall come to an end on the {{$portal_details['last_due_date']}}. </td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(b) That the Employer has agreed to pay the WCF Tanzanian Shillings TZS {{number_format($portal_details['total'],2)}} (Tanzania Shillings {{$portal_details['total_numbers_in_words']}} only) being outstanding amount(Exclude Interest) for the period from {{$portal_details['pay_description']}} in {{$portal_details['count_in_words']}} ({{$portal_details['count']}}) instalments within the period and amount as specified in the <i>Instalment Payment Request Form</i>.

  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">i)  That the amount stated under <strong>Clause 5.1. (b)</strong>  shall be paid by the Employer on or before the agreed date specified in <i>Instalment Payment Request Form</i> and shall be paid together with or without affecting payment of the contribution due for that particular falling month, until the last instalment to be paid by the Employer on or before {{$portal_details['last_due_date']}} is effected.</td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">ii) That, all instalments to be paid by the Employer shall make a total amount of the outstanding debt of Tanzania Shillings {{number_format($portal_details['total'],2)}} (Tanzania Shillings {{$portal_details['total_numbers_in_words']}} only).
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">(c) That any money that may fall unpaid as agreed under clause <strong>5.1. (b)</strong> of this Agreement, when such money becomes due and payable shall attract an interest calculated at the rate of 10% per month.</td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">Employer may opt to pay the money referred under <strong>Clause 5.1. (a)</strong> above in less time than the time provided for under <strong>Clause 5.1. (b)</strong>.
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>5.2  MODE OF PAYMENT</strong></td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">All payments to the Fund under this Agreement must be made upon generation of a Control Number through the Employers’ Online Self-Service Portal. The Employer shall make use of the service available under the <strong><i>“Instalment Payment”</i></strong> Menu</strong>.
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>6.0 AMENDMENT</strong></td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">This Agreement may be amended from time to time to accommodate new development in the Act and its Regulations or any other sufficient reason as circumstances of its implementation may compel, provided that such amendment shall not come into force unless and until agreed upon by both parties in writing.
  </td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>7.0 CONCEQUENCES OF BREACH OF CONTRACT</strong></td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">In the event of breach of this Agreement by the Employer, the WCF reserves the right to institute criminal or civil proceedings according to the provisions of the Act read together with its Regulations within the Courts of competent jurisdiction in the United Republic of Tanzania.  
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>8.0  GOVERNING LAW</strong></td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">This Agreement shall be governed by the Laws of the United Republic of Tanzania.  
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>9.0 TERMINATION</strong></td>
</tr>
<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">This Agreement shall be terminated upon the following grounds; 
  </td>
</tr>
<tr>
  <td colspan="2">9.1  When the Employer fully pays the arrears of contributions to the WCF as per the terms of this Agreement and the WCF discharges the Employer in writing from the debt; or 
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">9.2  When the Employer fails to remit the arrears of contributions on the modality and time of making such instalments as agreed under this Agreement. 
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2"><strong>IN WITNESS WHEREOF</strong> the parties hereto have caused this Agreement to be executed under their respective hands on the date as herein below indicated:
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td colspan="2">Signed in duplicate on this <strong>{!!\Carbon\Carbon::parse($installment->agreement_date)->format('j<\s\u\p>S</\s\u\p> F Y')!!}</strong>.
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td><strong>{{'…………………………………........'}}</strong>.
  </td><td><strong>{{'…………………………………........'}}</strong>.</td>
</tr>
<tr>
  <td><strong>{{'…………. (Employers’ Rep.)'}}</strong>.
  </td>
  <td><strong>{{'Dr. JOHN K. MDUMA'}}</strong>
  </td>
</tr>

<tr>
  <td><strong>{{'………. (Title),'}}</strong>.
  </td>
  <td><strong>{{'Director General,'}}</strong>.
  </td>
</tr>

<tr>
  <td>Employer.
  </td>
  <td>Workers Compensations Fund (WCF), .
  </td>
</tr>

<tr>
  <td>{{'P. O. Box...'}}.
  </td>
  <td>{{'P.O. Box 79655, Dar es salaam'}}.
  </td>
</tr>

<tr>
  <td>TANZANIA.
  </td>
  <td>TANZANIA.
  </td>
</tr>

<tr><td colspan="2"> &nbsp;</td></tr>
<tr>
  <td><strong>WITNESSED BY: </strong>
  </td><td><strong>WITNESSED BY: </strong></td>
</tr>
<tr>
  <td><strong>{{'NAME…………………………….'}}</strong>.
  </td>
  <td><strong>{{'NAME:  ABRAHAM P. SIYOVELWA'}}</strong>.
  </td>
</tr>

<tr>
  <td><strong>{{'………. (Title),'}}</strong>.
  </td>
  </tr>

<tr>
  <td>{{'ADDRESS: '}}
  </td>
  <td><strong>ADDRESS:</strong>  P. O. Box 79655 Dar es salaam.
  </td>
</tr>

<tr>
  <td><strong>{{'QUALIFICATION: ………………'}}</strong>.
  </td>
  <td>{{'QUALIFICATION: Head of Legal Services Unit.'}}.
  </td>
</tr>

</tbody>
</table>
</div>
</div>
<script type="text/php">
    if (isset($pdf)) {
        $text = "page {PAGE_NUM} / {PAGE_COUNT}";
        $size = 10;
        $font = $fontMetrics->getFont("Verdana");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        $x = ($pdf->get_width() - $width) / 2;
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size);
    }
</script>
</body></html>
