<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.backend.member.verification.summary')</span></b></h5></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.general.status') :</h6>
        <p style="font-weight: bold;">{!! $verification->status_formatted !!}</p>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.member.verification.doc') :</h6>
        <p style="font-weight: bold;">{!! $verification->payroll_doc_formatted !!}</p>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.member.verification.tin') :</h6>
        <p style="font-weight: bold;">{{ $verification->tin }}&nbsp;:&nbsp;{!! $verification->tin_existing_label !!}</p>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.member.verification.reg_no') :</h6>
        <p style="font-weight: bold;">{!! $verification->reg_no !!}</p>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.member.verification.email') :</h6>
        <p style="font-weight: bold;">{!! $verification->email !!}</p>
        <h6 class="underline" style="font-weight: lighter;">@lang('labels.backend.member.verification.phone') :</h6>
        <p style="font-weight: bold;">{!! $verification->phone !!}</p>
        @if($verification->commencement_source > 1)
        <h6 class="underline" style="font-weight: lighter;"> Payroll Type :</h6>
        <p style="font-weight: bold;">{!! ($verification->commencement_source == 2) ? 'Branch or Another Payroll' : 'Project' !!}</p>
        <h6 class="underline" style="font-weight: lighter;"> Payroll Description :</h6>
        <p style="font-weight: bold;">{!! $verification->payroll_description_formatted !!}</p>
        @endif
    </div>
</div>
