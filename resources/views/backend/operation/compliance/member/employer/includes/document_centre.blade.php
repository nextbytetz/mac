<legend class="grey_modal" >@lang('labels.backend.member.document_helper')</legend>

<br/>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6 col-sm-6">
            <legend>{!! trans('labels.backend.claim.library') !!}</legend>
            <div id="folder-tree" class="nopadding"></div>
        </div>
          <br/>
        <br/>
    </div>
</div>
@push('document-centre-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<!-- Custom javascript files for this page -->
<script>
    $(function() {
        var $folder = $('#folder-tree');

        /************************** start: Document Library JS ***************************/
        $folder.jstree({
            'core': {
                'data': {
                    'url': "{!! route('backend.compliance.employer_registration.documents_library', $employer->id) !!}",
                    'data': function (node) {
                        return {'id': node.id};
                    }
                },
                'check_callback': function (o, n, p, i, m) {
                    if (m && m.dnd && m.pos !== 'i') {
                        return false;
                    }
                    if (o === "move_node" || o === "copy_node") {
                        if (this.get_node(n).parent === this.get_node(p).id) {
                            return false;
                        }
                    }
                    return true;
                },
                'force_text': true,
                'themes': {
                    'responsive': true,
                    'variant': 'small',
                    'stripes': true
                }
            },
            'sort': function (a, b) {
                return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            'contextmenu': {
                'items': function (node) {
                    var $menu = {};
                    if (this.get_type(node) === "file") {
                        $menu = {
                            "download" : {
                                "label" : "{!! trans("labels.general.download") !!}",
                                "action" : function ($data) {
                                    window.open( base_url + "/compliance/employer_registration/download_document/{!! $employer->id !!}/" + node.id);
                                }
                            },
                            "open" : {
                                "label" : "{!! ucfirst(strtolower(trans("labels.general.open"))) !!}",
                                "action" : function ($data) {
                                    window.open(base_url + "/compliance/employer_registration/open_document/{!! $employer->id !!}/" + node.id, '_blank');
                                }
                            }
                        }
                    }
                    return $menu;
                }
            },
            'types': {
                'default': {'icon': 'folder'},
                'file': {'valid_children': [], 'icon': 'file'}
            },
            'unique': {
                'duplicate': function (name, counter) {
                    return name + ' ' + counter;
                }
            },
            'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']
        });

    });
</script>
@endpush