<div class="row">
    <div class="col-md-12">
        <div class="grey_modal">
            <table style="width:100%">
                <tr>
                    <td  align="center"><h5><b><span class="light_dark_color">Incident Summary</span></b></h5></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 light_grey_bg">
        {{--<div class="light_grey_bg">--}}
            <br/>
            <h5 class="underline text-center">{!! $incident->status_label !!}</h5>
            <div class="underline">Employee Phone&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->phone Or "-" !!}</span></div>
            <div class="underline">Employer Phone&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->employer_phone Or "-" !!}</span></div>
            <div class="underline">Incident Type&nbsp;:&nbsp;<span style="font-weight: bold;">{!! $incident->incidentType->name !!}</span></div>

            <div class="underline">Incident Date&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $incident->incident_date_formatted }}&nbsp;&nbsp;</span></div>

            <div class="underline">Employee&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $incident->employee->name }}</span></div>
            <div class="underline">Employer&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $incident->employer->name }}</span></div>

            <div class="underline">Incident Description&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $incident->description Or "-" }}</span></div>

{{--            <div class="underline">Region&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $incident->district->region->name }}</span></div>--}}
            <div class="underline">Region&nbsp;:&nbsp;<span style="font-weight: bold;">{{ ($incident->district()
            ->count()) ? ($incident->district->region()->count()) ? $incident->district->region->name : ' ' : '' }}</span></div>

            <div class="underline">District&nbsp;:&nbsp;<span style="font-weight: bold;">{{ ($incident->district()
            ->count()) ? $incident->district->name  : '' }}</span></div>

            @if($incident->incidentType->id == 1)

                @php
                    $accident = $incident->accident;
                @endphp
                <div class="underline">Incident Time&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $accident->incident_time }}</span></div>

                <div class="underline">Incident Place&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $accident->place }}</span></div>

                <div class="underline">Activity&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $accident->activity Or "-" }}</span></div>

                <div class="underline">Accident Type&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $accident->accidentType->name }}</span></div>

                <div class="underline">Witness&nbsp;:&nbsp;
                    @if ($incident->witnesses()->count())
                        @php
                            $witnesses = $incident->witnesses;
                        @endphp
                        <ul>
                            @foreach ($witnesses as $witness)
                                <li>{{ $witness->name }}&nbsp;(&nbsp;{{ $witness->phone }}&nbsp;)</li>
                            @endforeach
                        </ul>

                    @else
                        -
                    @endif
                </div>

            @elseif($incident->incidentType->id == 2)
                @php
                    $disease = $incident->disease;
                @endphp
                <div class="underline">Disease Diagnosed&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $disease->name }}</span></div>

                <div class="underline">Hospital&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $disease->health_provider }}</span></div>

                <div class="underline">Medical Practitioner&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $disease->medical_practitioner }}</span></div>

            @elseif($incident->incidentType->id == 3)
                @php
                    $death = $incident->death;
                @endphp
                <div class="underline">Cause of Death&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $death->deathCause->name }}</span></div>

                <div class="underline">Hospital&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $death->health_provider }}</span></div>

                <div class="underline">Medical Practitioner&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $death->medical_practitioner }}</span></div>

                <div class="underline">Death Place&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $death->death_place }}</span></div>

                <div class="underline">Activity&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $death->activity }}</span></div>

                <div class="underline">Death Certificate&nbsp;:&nbsp;<span style="font-weight: bold;">{{ $death->certificate_number Or "-" }}</span></div>
            @endif

        {{--</div>--}}
    </div>
</div>