  <div class="row">

    <div class="grey_modal">
      <table style="width:100%">
        <tr>
          <td  align="center"><h5><b><span class="light_dark_color">Commitment Summary</span></b></h5></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="col-md-12 light_grey_bg">
      <h6 class="underline" style="font-weight: lighter;">Commitment type:</h6>
      <p style="font-weight: bold;">{!! $commitment->commitment_type !!}</p>
      <h6 class="underline" style="font-weight: lighter;">User Name :</h6>
      <p style="font-weight: bold;">{!! $commitment->firstname.' '.$commitment->lastname !!}</p>
      <h6 class="underline" style="font-weight: lighter;">User Email :</h6>
      <p style="font-weight: bold;">{!! $commitment->email !!}</p>
      <h6 class="underline" style="font-weight: lighter;">User Phone :</h6>
      <p style="font-weight: bold;">{!! $commitment->phone !!}</p>
      <h6 class="underline" style="font-weight: lighter;">Date Submitted:</h6>
      <p style="font-weight: bold;" class="pt-1">{!! \Carbon\Carbon::parse($commitment->created_at)->format('d<\s\up>S</\s\up> M, Y H:i A') !!}</p>
    </div>
  </div>