
{{--Booking table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="booking-table">
            <thead>
            <tr >
                <th>@lang('labels.backend.table.booking.rcv_date')</th>
                <th>@lang('labels.backend.table.booking.booked_amount')</th>
                <th>@lang('labels.backend.table.amount_paid')</th>
                <th>@lang('labels.backend.table.member_count')</th>
                <th>@lang('labels.backend.table.description')</th>
                <th>@lang('labels.backend.table.created_at')</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="1" style="text-align:right"></th>
                <th></th>
                {{--<th colspan="2" style="text-align:right"></th>--}}
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>


{{--SUMMARY--}}
<table class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>{!! strtoupper(trans('labels.general.total')) !!}:</th>
        <th>@lang('labels.general.booking')</th>
        <th>{!! $booking_summary['total_booking'] !!}</th>
        <th>@lang('labels.backend.table.amount_paid')</th>
        <th>{!! $booking_summary['total_paid'] !!}</th>
        <th>@lang('labels.backend.table.remain_amount')</th>
        <th>{!! $booking_summary['total_remain'] !!}</th>
    </tr>
    </thead>
</table>


@push('booking-script-end')

<script  type="text/javascript">
    $(function() {
    $("#tab_4_header").one("click", function(){
            $('#booking-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.bookings', $employer->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'rcv_date_formatted' , name: 'rcv_date' },
                    { data: 'amount', name: 'amount' },
                    { data: 'paid_amount', name: 'paid_amount' }, //added col
                    { data: 'member_count', name: 'member_count' },
                    { data: 'description', name: 'description' },
                    { data: 'created_at', name: 'created_at'}
                ],

//                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = "booking/" + aData['id'] ;
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//                },

                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 1 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

//                get Total for amount booked
                    // Update footer
                    function commaSeparateNumber(val){
                        while (/(\d+)(\d{3})/.test(val.toString())){
                            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                        return val;
                    }

                    $( api.column( 1 ).footer() ).html(
                        ''+ commaSeparateNumber(total.toFixed(2))
                    );

//                get total for amount paid

                    // Total over all pages
                    total = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    $( api.column( 2 ).footer() ).html(
                        ''+ commaSeparateNumber(total.toFixed(2))
                    );

                }
            });

        });
    });
</script>;

@endpush