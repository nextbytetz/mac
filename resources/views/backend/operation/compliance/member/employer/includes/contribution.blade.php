
{{--Contribution table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="contribution-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.receipt.contrib_month')</th>
                <th>@lang('labels.backend.table.receipt.rctno')</th>
                <th>@lang('labels.backend.table.receipt.rct_date')</th>
                <th>@lang('labels.backend.table.amount')</th>
                <th>@lang('labels.backend.table.member_count')</th>
                <th>@lang('labels.backend.table.linked_file')</th>
                <th>@lang('labels.backend.table.action')</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="3" style="text-align:right">Total:</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </tfoot>
        </table>
        <br/>
        <legend class="grey_modal" >@lang('labels.backend.member.employer.legacy_contribution')</legend>
<br/>
        <table class="display" cellspacing="0" width="100%" id ="legacy-contribution-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.receipt.contrib_month')</th>
                <th>@lang('labels.backend.table.receipt.rctno')</th>
                <th>@lang('labels.backend.table.receipt.rct_date')</th>
                <th>@lang('labels.backend.table.amount')</th>
                <th>@lang('labels.backend.table.member_count')</th>
            </tr>
            </thead>
            <tfoot>
            {{--<tr>
                <th colspan="3" style="text-align:right">Total:</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>--}}
            </tfoot>
        </table>

    </div>
</div>


@push('contribution-script-end')

<script  type="text/javascript">
    $(function() {
        $("#tab_2_header").one("click", function(){
            var url = "{!! url("/") !!}";
            $('#legacy-contribution-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.legacy_contributions', $employer->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'contrib_month_formatted' , name: 'contrib_month' },
                    { data: 'rctno', name: 'rctno' }, //edited col
                    { data: 'receipt_date', name: 'receipt_date' }, //added col
                    { data: 'amount', name: 'amount' },
                    { data: 'member_count', name: 'member_count' },
                ],
            });

            $('#contribution-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.contributions', $employer->id) !!}',
                    type : 'get'
                },
                columns: [
                    { data: 'contrib_month_formatted' , name: 'contrib_month' },
                    { data: 'receipt_id', name: 'receipt_id' }, //edited col
                    { data: 'rct_date', name: 'rct_date' }, //added col
                    { data: 'amount', name: 'amount' },
                    { data: 'member_count', name: 'member_count' },
                    { data: 'status', name: 'status'}, //added col
                    { data: 'action', name: 'action'}, //added col
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url + "/finance/receipt_code/" + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                },
                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    // Total over all pages
                    total = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    // Update footer
                    function commaSeparateNumber(val){
                        while (/(\d+)(\d{3})/.test(val.toString())){
                            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                        return val ;
                    }
                    $( api.column( 3 ).footer() ).html(
                        ''+ commaSeparateNumber(total.toFixed(2))
                    );
                }
            });
        });
    });
</script>;

@endpush