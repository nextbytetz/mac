{{--Interest Other Details--}}

<div class="row">
    {{--left interest  summary--}}
    <div class="col-md-6">
        <div class="col-md-12">
            <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                {{--rcv date--}}
                <tr>
                    <td width="180px">@lang('labels.backend.table.booked_amount')</td>
                    <th>{!! Form::label( 'booked_amount',
                            $booking_interest->booking->amount_formatted, [
                            'id'=>
                                'rcv_date'])
                                !!}  &nbsp; </th>
                </tr>
                {{--interest percent--}}
                <tr>
                    <td>@lang('labels.backend.table.received_amount')
                    </td>
                    <th>{!! Form::label( 'contribution_amount',
                           $contribution_paid_amount, [
                                'id'=>
                'contribution_amount']) !!}</th>

                </tr>

                {{--payment date--}}
                <tr>
                    <td>@lang('labels.backend.table.payment_date')</td>
                    <th>{!! Form::label( 'payment_date',
                          ($booking_interest->isadjusted == 1 && isset($booking_interest->payment_date_old)) ? $booking_interest->payment_date_old : $first_paid_date   , [ 'id'=>
                            'payment_date']) !!}</th>
                </tr>


                {{--amount--}}
                <tr>
                    <td>@lang('labels.backend.table.interest_percent')</td>
                    <th>{!! Form::label( 'interest_percent',
                            $booking_interest->interest_percent, [ 'id'=>
                            'interest_percent']) !!}</th>
                </tr>

                </tbody></table>

        </div>
    </div>

</div>
