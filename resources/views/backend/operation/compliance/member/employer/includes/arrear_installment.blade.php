<div class = "row">
  <div class="col-md-12" >
    <legend class="grey_modal" >Installment Payment Request </legend>
    <div>&nbsp; </div>
    <div class="col-md-12">

    </div>
    {{--<div>&nbsp; </div>--}}
    &nbsp;

    <table class="display" cellspacing="0" width="100%" id ="installment-table">
      <thead>
        <tr >
          <th>User</th>
          <th>Description</th>
          <th>Payment Option</th>
          <th>Number Of Months</th>
          <th>Date requested</th>
          <th>Status</th>
          {{-- <th>Action</th> --}}
        </tr>
      </thead>
    </table>

  </div>

</div>



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}


<script  type="text/javascript">

 $(function() {
  $('#installment-table').DataTable({
    processing: true,
    serverSide: true,
    stateSave: true,
    stateSaveCallback: function (settings, data) {
      localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
    },
    stateLoadCallback: function (settings) {
      return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
    },
    ajax:{
     url : '{!! route('backend.compliance.employer.online.installments',$employer->id) !!}',
     type : 'get'

   },
   columns: [
   { data: 'user', name: 'users.name'},
   { data: 'description', name: 'description'},
   { data: 'payment_option', name: 'payment_option'},
   { data: 'number_of_month', name: 'number_of_month'},
   { data: 'created_at', name:'created_at',searchable : false, orderable : false },
   { data: 'status', name:'status',searchable : false, orderable : false},
   ],
   fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    $('td', nRow).click(function() {
      document.location.href = '{!! url('compliance/employer/online/view_installment/') !!}/'+aData['id'];
    }).hover(function() {
      $(this).css('cursor','pointer');
    }, function() {
      $(this).css('cursor','auto');
    });
  } 

});
});



</script>
@endpush