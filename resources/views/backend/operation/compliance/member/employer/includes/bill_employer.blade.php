<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header h6">
       <label for="select">Payroll: &nbsp;</label>
       <select class="user-select" id="employer_payroll" name="employer_payroll" style="width:40%">
        @if(count($employer_user) == 1)
        <option value="{{$employer_user[0]->payroll_id}}" data-user_id="{{$employer_user[0]->id}}" selected="selected">{{ucwords(strtolower($employer_user[0]->name.' - '.$employer_user[0]->payroll_id))}}</option>
        @else
        <option value="" selected="selected">Click here to select</option>
        @foreach($employer_user as $user)
        <option value="{{$user->payroll_id}}" data-user_id="{{$user->id}}">{{ucwords(strtolower($user->name.' - '.$user->payroll_id))}}</option>
        @endforeach
        @endif
      </select>
      <br> <p class="error_user_select text-danger hidden"></p>
    </div>
    <div class="card-body">
     <div class="row">
      <div class="col-sm-12 p-2 ml-1 h6">
       Employer Payment <span id='payroll_name' class="reset_text"></span>
     </div>
   </div>
 </div>
 <div class="card-footer">
   <div class="col-sm-12 hidden" id="pending_bill_div"> 
     <h6 class="ml-1 mb-3">
      Employer has a pending Bill <span class="amount_control_no reset_text"> </span> 
    </h6> 
    <h6>
      <div class="row">
        <div class="col-sm-3 ml-2">Direct Deposit Form :</div>
        <div class="col-sm-8">
          <span class="direct_control_no wait_control_no reset_html"> </span> 
        </div>
      </div>              
    </h6>
    <br><br>

    <h6>
      <div class="row">
        <div class=" col-sm-3 ml-2">NMB Transfer Form :</div>
        <div class=" col-sm-8">
          <span class="nmb_control_no wait_control_no reset_html"> </span> 
        </div>
      </div>              
    </h6>

    <br><br>
    <h6>

      <div class="row">
        <div class=" col-sm-3 ml-2">CRDB transfer Form :</div>
        <div class=" col-sm-8">
          <span class="crdb_control_no wait_control_no reset_html"> </span> 
        </div>
      </div>  
    </h6>

  </div>

  <div id="payment_div" class="hidden col-md-12 pt-2">
    <form id="frmPay">
      {{ csrf_field() }}

      <div class="form-group row h6">
        <input class="form-control reset_value" name="payroll_id" type="text" id="bill_payroll_id"  hidden="hidden">
        <input class="form-control reset_value" name="user_id" id="bill_user_id" type="text" hidden="hidden">
        <input class="form-control" name="bill_employer_id" type="text" id="bill_employer_id" value="{{$employer->id}}"  hidden="hidden">

        <div class="col-sm-2">
          <input type="checkbox" class="bill_check" id="contribution_check" name="contribution_check" value="contribution_check" data-amount="" checked="checked">
        </div>
        <div class="col-sm-4 col-form-label">
          <label for="contribution_month_description">
            <span id="contribution_month_description"></span> : <span id="contrib_amount"></span>
          </label>
        </div>
        <div class="col-sm-4">
          <input class="form-control reset_value" name="month" type="text" id="month_for_contribution"  hidden="hidden">
          <input class="form-control reset_value" name="employees_count" id="contribution_employees_count" type="text" hidden="hidden">
          <input class="form-control reset_value" name="contribution" id="contribution_amount" type="text" id="contribution"  readonly="readonly">  
        </div>
      </div> 

      <div class="form-group row h6 hidden  pt-1" id='arrear_div'>
       <div class="col-sm-12"><hr></div>
       <div class="col-sm-2">
        <input type="checkbox" class="bill_check" id="arrear_check" name="arrear_check" value="arrear_check" data-amount="">
      </div>
      <div class="col-sm-4 col-form-label">
       <label for="arrear_description">
        <span id="arrear_description" class="reset_text"></span> : <span id="amount_of_arrear" class="reset_text"></span>
      </label>
    </div>

    <div class="col-sm-4">
      <input class="form-control reset_value" name="arrear_months" type="text" id="months_for_arrear"  hidden="hidden">
      <input class="form-control reset_value" name="arrear_amount" id="arrear_amount" type="text" readonly="readonly">  
    </div> 
    <div class="col-sm-12 mt-1">
      <hr>
    </div>
  </div> 
  <div class="form-group row pt-1">
    <div class="col-sm-6">&nbsp;</div>
    <div class="col-sm-4">
      <button class="btn btn-info btn-lg" id="btn_generate_cn"><i class="fa fa-cogs"></i> Generate Control Number <span class="total_bill"></span></button>
      <span class="wait hidden">
        <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  
        &nbsp; <span class="h6"> Please wait .... </span>
      </span>
    </div>
  </div> 

</form>
</div>
</div>
</div>
</div>
</div>
@push('document-centre-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script>
  $(function() {
    $(".user-select").select2({});
    selectedPayroll();


    $('#employer_payroll').on('change',function(e){
      e.preventDefault();
      selectedPayroll();
    });  

    $('.bill_check').change(function(){
      totalWrite();
    });


    $('#btn_generate_cn').on('click',function(e){
      e.preventDefault();

      let checked_check = [];
      $('.bill_check').each(function(){ 
        if($(this).is(':checked'))
        {
         checked_check.push($(this).val());
       }
     });

      let description ='';
      if (checked_check.length == 1) {
       if($.inArray("contribution_check", checked_check) !== -1){
        description =  $('#contribution_month_description').text();
      }
      if($.inArray("arrear_check", checked_check) !== -1){
        description = $('#arrear_description').text();
      }
    }

    if (checked_check.length == 2) {
      description = $('#contribution_month_description').text()+' and '+$('#arrear_description').text();
    }

    if (description == '') {
      swal('Request Failed!','Kindly select atleast one item for payment and try again','error');
    }else{
      swal({
        title: "Attention! {{access()->user()->firstname}}",
        text: "<h6>This will generate control number of "+$('.total_bill').text()+" for "+description+"</h6>",
        type: "warning",
        html: true,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Proceed",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true,
      },
      function(isConfirm) {
        if (isConfirm) {
         $('#btn_generate_cn').addClass('hidden');
         $('.wait').removeClass('hidden');
         var frm = $('#frmPay');
         $.ajax({
          url : '{{route("backend.compliance.employer.online.generate_employer_bill") }}',
          type : 'POST',
          datatype : 'json',
          data : frm.serialize(),
          success:function(data){
            if (data.success) {
             swal({
              title: "Success",
              text: "<h6>The control number has been generated successfully</h6>",
              type: "warning",
              html: true,
              showCancelButton: false,
              confirmButtonClass: "btn-success",
              confirmButtonText: "OK",
              closeOnConfirm: true,
              closeOnCancel: true,
            },
            function(isConfirm) {
              location.reload();
            });
           }else{
             $('#btn_generate_cn').removeClass('hidden');
             $('.wait').addClass('hidden');
             if (data.message) {
              swal('Attention! {{access()->user()->firstname}}!',data.message,'warning');
            }  
            if (data.errors) {
              swal('An error has Occured!','Kindly refresh and try again','error');
            }
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          location.reload(); 
        }
      });
       }

     });
    }
  });  


    function totalWrite() {
      let total = 0;
      $('.bill_check:checked').each(function(){ 
        /*iterate through each checked element.*/
        let amount = $(this).data('amount').replace(/,/g, '');
        total += isNaN(parseFloat(amount)) ? 0 : parseFloat(amount);
      });
      let jumla =  parseFloat(total).toFixed(2);
      let total_write = jumla.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      $(".total_bill").text(total_write);
    }


    function selectedPayroll() {
     let name = $('#employer_payroll option:selected').text();
     let payroll = $('#employer_payroll option:selected').val();
     let user_id = $('#employer_payroll option:selected').data('user_id');
     $('.reset_value').val();
     $('.reset_text').text('');
     $('.reset_html').empty();
     $('#arrear_check').prop('checked', false);
     $('#arrear_div').addClass('hidden');
     $('#payment_div').addClass('hidden');
     $('#pending_bill_div').addClass('hidden');
     if (payroll && name) {
      $('#bill_payroll_id').val(payroll);
      $('#bill_user_id').val(user_id);
      $('#payroll_name').text(' : '+name);
      $.ajax({
        url : '{{url('compliance/employer/online/payment')}}/'+{{$employer->id}}+'/'+payroll,
        type : 'GET',
        datatype : 'json',
        success:function(data){
          if (data.payment_data || data.pending_bill) {

            if (data.payment_data) {
              $('#payment_div').removeClass('hidden');
              $('#pending_bill_div').addClass('hidden');  
              $('#contribution_month_description').text();  

              let contribution = data.payment_data.contribution;
              let arrears = data.payment_data.arrears;
              if (contribution.month) {
                $('#contribution_month_description').text('Contribution of '+contribution.month+' for '+contribution.employees_count+' employees');  
                $('#month_for_contribution').val(contribution.month);  
                $('#contribution_employees_count').val(contribution.employees_count);  
                $('#contribution_amount').val(contribution.amount);  
                $('#contrib_amount').val(contribution.amount);
                $('#contribution_check').data('amount', contribution.amount);   
              }

              if (arrears.month_description) {
                $('#arrear_div').removeClass('hidden');
                if (arrears.months > 1) {
                  $('#arrear_description').text('Arrears of '+arrears.months+' months: '+arrears.month_description);
                } else {
                  $('#arrear_description').text('Arrear for '+arrears.month_description);
                }  
                $('#months_for_arrear').val(arrears.months_name);  
                $('#arrear_amount').val(arrears.amount);  
                $('#arrear_check').data('amount', arrears.amount);
              }

              totalWrite();
            }

            if (data.pending_bill) {
             $('#payment_div').addClass('hidden');
             $('#pending_bill_div').removeClass('hidden');
             let pending_bill = data.pending_bill;
             if (pending_bill.bill_amount) {
              if (pending_bill.bill_control_no != null) {
                $('.amount_control_no').text(' of Tzs. '+pending_bill.bill_amount+' being '+pending_bill.billed_item+'. Control No#: '+pending_bill.bill_control_no); 
                $('.direct_control_no').html('<a href="{{url('/finance/normal_bill/')}}/'+pending_bill.bill_id+'" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download Direct Deposit Form</a>');
                $('.nmb_control_no').html('<a href="{{url('/finance/nmb_transfer/')}}/'+pending_bill.bill_id+'" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download NMB Transfer Form</a>'); 
                $('.crdb_control_no').html('<a href="{{url('/finance/crdb_transfer/')}}/'+pending_bill.bill_id+'" class="btn btn-md btn-success"><i class="fa fa-download"></i> Download CRDB transfer Form</a>');  
              } else {
               $('.amount_control_no').text(' of Tzs. '+pending_bill.bill_amount+' being '+pending_bill.billed_item);
               $('.wait_control_no').html('<a class="btn btn-md btn-warning text-white btnWaitControlNo"><i class="fa fa-refresh"></i> Refresh To Get Control No#</a>'); 
             }
           }

         }
       }
     },
     error: function (xhr, ajaxOptions, thrownError) {
      location.reload(); 
    }
  });
    }else{
      $('#payroll_name').text(': Please select payroll to continue');
    }
  }

  $('.wait_control_no').click(function(e){
    location.reload();
  });


});
</script>
@endpush