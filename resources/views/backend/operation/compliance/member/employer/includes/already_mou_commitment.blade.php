@extends('layouts.backend.main', ['title' => 'Already Paid', 'header_title' => 'Already Paid Commitment'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
  /* start: File Icons CSS */
  #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
  #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
  #folder-tree .file-pdf { background-position: -32px 0 }
  #folder-tree .file-as { background-position: -36px 0 }
  #folder-tree .file-c { background-position: -72px -0px }
  #folder-tree .file-iso { background-position: -108px -0px }
  #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
  #folder-tree .file-cf { background-position: -162px -0px }
  #folder-tree .file-cpp { background-position: -216px -0px }
  #folder-tree .file-cs { background-position: -236px -0px }
  #folder-tree .file-sql { background-position: -272px -0px }
  #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
  #folder-tree .file-h { background-position: -488px -0px }
  #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
  #folder-tree .file-php { background-position: -108px -18px }
  #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
  #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
  #folder-tree .file-rb { background-position: -180px -18px }
  #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
  #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
  #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
  #folder-tree .file-js { background-position: -434px -18px }
  #folder-tree .file-css { background-position: -144px -0px }
  #folder-tree .file-fla { background-position: -398px -0px }
  /* end: File Icon CSS */
</style>
@endpush

@section('content')
<!-- Put the page specifically for this page here -->
<div class="row">
  <div class="col-md-9">   
    <div class="row">
     <div class="col-md-12 col-sm-12">
      <h5 class="client-title pb-2">
        <i class="icon-circle"></i>
        <!-- Employer header detail -->
        {{-- <span>{!! $employer->status_label !!} </span> --}}
        <a href="{!! url('compliance/employer/online/profile/'.$commitment->employer_id.'#commitments') !!}"><strong> {!! Form::label( 'name', strtoupper($commitment->employer), [ 'id'=> 'name']) !!}</strong> </a>
        <small >
          @lang('labels.backend.table.employer.reg_#'): {!! Form::label( 'reg_no', isset($commitment->reg_no) ? (!empty($commitment->reg_no) ? $commitment->reg_no : ' ') : ' ', [ 'id'=> 'reg_no']) !!}
        </small>
      </h5>
      <legend></legend>
    </div>

    <br/>
  </div>
  <div class="row">
    <div class="mou d-none" style="height:350px;">
      <object data="" width="95%" height="100%" type="application/pdf">
        This browser does not support PDFs..
      </object>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <table class="display" cellspacing="0" width="100%" id ="commitment-table">
        <thead>
          <tr>
            <th>Attachment</th>
            <th>Due date</th>
          </tr>
        </thead>
      </table>

    </div>
  </div>
</div>
<div class="col-md-3">
  @include('backend.operation.compliance.member.employer.includes.commitment_summary')
</div>
</div>
@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/jstree/js/jstree.min.js") }}
<script>
  $(function(){

    $('.mou').hide()

    $(".printing").click(function () {

    });


    $('#commitment-table').DataTable({
      processing: true,
      serverSide: true,
      stateSave: true,
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
      },
      ajax:{
       url : '{!! route('backend.compliance.employer.online.already_mou_datatable',$commitment->id) !!}',
       type : 'get'

     },
     columns: [
     { data: 'attachment', name: 'attachment',searchable : false, orderable : false },
     { data: 'due_date', name: 'due_date',searchable : false, orderable : false },

     ],
     rowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $('td', nRow).click(function() {
        // console.log(aData)
        let col = $(this).index()+1;
        let colText = $('#commitment-table thead th:nth-child('+col+')').text();
        if(colText == 'Attachment'){
          $('object').attr('data',aData['url']);
          $('.mou').toggle()
        }else{
          $('.mou').hide()
        }
      }).hover(function() {
        let col = $(this).index()+1;
        let colText = $('#e-table thead th:nth-child('+col+')').text();
        if(colText){
          $(this).css('cursor','pointer');                        
        }}, function() {
          $(this).css('cursor','auto');
        });
    }
  });


  });
</script>

@endpush