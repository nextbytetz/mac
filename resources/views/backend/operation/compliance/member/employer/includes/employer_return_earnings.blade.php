<div class = "row">
  <div class="col-md-12" >
    <legend class="grey_modal" >Annual Return of Earnings submission</legend>
    <div>&nbsp; </div>
    <div class="col-md-12">

    </div>
    {{--<div>&nbsp; </div>--}}
    &nbsp;

    <table class="display" cellspacing="0" width="100%" id ="employer_return_earnings-table">
      <thead>
        <tr >
          <th>Financial Year</th>
          <th>Payroll Number</th>
          <th>User Submitted</th>
          <th>Date submitted</th>
          <th>Submission Status</th>
          <th>Action</th>
        </tr>
      </thead>
    </table>

  </div>

</div>



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}


<script  type="text/javascript">

  $(function() {
   autosize($("textarea.autosize"));

   var annual_earning_table = $('#employer_return_earnings-table').DataTable({
    processing: true,
    serverSide: true,
    stateSave: true,
    stateSaveCallback: function (settings, data) {
      localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
    },
    stateLoadCallback: function (settings) {
      return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
    },
    ajax:{
     url : '{!! route('backend.compliance.employer.online.annual_earnings',$employer->id) !!}',
     type : 'get'

   },
   columns: [
   { data: 'fin_year', name: 'a.fin_year'},
   { data: 'payrollId', name: 'a.payrollId'},
   { data: 'user_submitted', name: 'u.name'},
   { data: 'submitted_date', name:'submitted_date',searchable : false, orderable : false },
   { data: 'status', name:'status',searchable : false, orderable : false},
   { data: 'action', name:'action',searchable : false, orderable : false},
   ],
 });


   annual_earning_table.on('click','.reverseAnnualReturn',function(e)
   {
    e.preventDefault();
    $('#reverseReturnForm').trigger('reset');
    $('.reason_title').text();
    $('#annual_earning_id').val();

    let annual_earning_id = $(this).data('annual_earning_id');
    let fin_year = $(this).data('fin_year');
    swal({
      title: "Attention! {{access()->user()->firstname}}",
      text: "<h6>Are you sure want to reverse this "+fin_year+" submission of annual earning?</h6>",
      type: "warning",
      html: true,
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: true,
      closeOnCancel: true,
    },
    function(isConfirm) {
      if (isConfirm) {
       $('.error_reason').addClass('hidden');
       $("#reverseReturnModal").modal('show');
       $('.reason_title').text(fin_year+' Annual earning submission');
       $('#annual_earning_id').val(annual_earning_id);
     }

   });
  });



   $('#reverseReturnSubmit').on('click',function(e){
    e.preventDefault();

    $('.wait').removeClass('hidden');
    $('.footerButtons').addClass('hidden')

    $('.error_reason').text();
    $('.error_reason').addClass('hidden');
    $.ajax({
      url : '{{url('compliance/employer/online/reverse_annual_earnings/')}}',
      type : 'POST',
      data:$('#reverseReturnForm').serialize(),
      datatype : 'json',
      success:function(data){
       if(data.success){
        swal({
          title: "Dear {{access()->user()->firstname}}",
          text: "<h6>The submission of annual earning has been reversed successfully!</h6>", 
          type: "success",
          html: true,
          showCancelButton: false,
          confirmButtonClass: "btn-success",
          confirmButtonText: "OK",
          closeOnConfirm: true,
          closeOnCancel: true,
        }, 
        function(isConfirm) {
          $('#reverseReturnModal').modal('hide');
          annual_earning_table.ajax.reload(null,false);
        });
      }else{
       if (data.errors) {
        if (data.errors.reverse_reason) {
          $('.error_reason').removeClass('hidden');
          $('.error_reason').text(data.errors.reverse_reason);
        }
        if (data.errors.message) {
          swal('Sorry! {{access()->user()->firstname}}',data.errors.message,'error');
        }
      } else {
       swal('Sorry! {{access()->user()->firstname}}','An error has occured. Please try agaian','error');
     }
   }
   $('.wait').addClass('hidden');
   $('.footerButtons').removeClass('hidden');
 }
});

  });


 });
</script>
@endpush