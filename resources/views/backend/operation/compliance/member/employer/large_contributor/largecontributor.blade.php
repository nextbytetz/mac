@extends('layouts.backend.main', ['title' => 'Large Contributors Registered Online', 'header_title' => 'Large Contributors Registered Online'])

@include('backend.includes.datatable_assets')

@section('content') 

{!! $dataTable->table(['class' => 'display', 'width' => '100%', 'íd' => 'dataTable']) !!}

@stop


@push('after-script-end')
{!! $dataTable->scripts() !!}


@endpush






