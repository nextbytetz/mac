@extends('layouts.backend.main', ['title' => 'Check Employer Status', 'header_title' => 'Check Employer Status'])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jQuery-Smart-Wizard/styles/smart_wizard.min.css") }}

@endpush

@section('content') {{-- //['backend.compliance.status'] --}}

{{-- {!! Form::open(['route' => '\Backend\Operation\Compliance\Member\EmployerController@employerStatusShow' ,'method'=>'post','name' => 'create']) !!}
--}}


<div style="padding:0px; margin:0px; border-top:1px !important;" >
    <div class="row" >

        <div class="col-md-12">
            @if(isset($error))
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{$error}}.
          </div>
          @endif
          <div class="element-form"  >
           
            </div>
        </div>

    </div>


</div>
</div>

{{--Buttons--}}
<br>
<div class="row">
    <div class="col-md-10" class="form-inline" >
        <div class="element-form">
            <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
            text-xs-right"></div>
            <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                <div class="pull-right">

                    {!! link_to('compliance/status',trans('buttons.general.cancel'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                    {!! Form::button(trans('buttons.general.search'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                </div>
            </div>
        </div>
    </div>

</div>
{!! Form::close() !!}

@stop


@section('content-ades')
<div class="content content-switch content-form-layout">
   
</div> 
</div>
@stop

@endif



