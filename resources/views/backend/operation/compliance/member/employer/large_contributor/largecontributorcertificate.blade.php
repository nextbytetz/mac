@extends('layouts.backend.main', ['title' => 'Large Contributors Certificates Issuance', 'header_title' => 'Large Contributors Certificates Issuance'])

@include('backend.includes.datatable_assets')

@section('content') 

{!! $dataTable->table(['class' => 'display', 'width' => '100%', 'íd' => 'dataTable']) !!}

@stop


@push('after-script-end')
{!! $dataTable->scripts() !!}


@endpush






