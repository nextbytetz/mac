@extends('layouts.backend.main', ['title' => trans('labels.backend.compliance.booking'), 'header_title' => trans('labels.backend.compliance.booking')])

@include('backend.includes.datatable_assets')

@section('content')


    <div class = "row">
        {{--{!! Form::model($booking, [ 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}
        <div class="col-md-8 col-sm-8">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <strong> {!! Form::label( 'name',  $booking->employer->name , [ 'id'=> 'name'])
                !!}</strong>

                <small >
                    Reg #: {!! Form::label( 'reg_no', $booking->employer->reg_no , [ 'id'=> 'reg_no'])
                     !!}
                </small>
            </h5>
        </div>

        {{--Main Content--}}
        {{--menu bar--}}
        <div class = "row">
            <div class="col-md-12">
                {{--menu Bar--}}
                <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >

                            <div class="pull-right" >


                                {{--Contribution Transfer--}}
                                {{--<span>--}}
						        {{--<a href="{!! route('backend.compliance.employer.adjust_interest',$booking->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-exchange"></i>&nbsp;@lang('buttons.backend.compliance.contribution_transfer')</a>--}}
						{{--</span>--}}


                                {{--Booking Adjustment--}}
                                <span>
						        <a href="{!! route('backend.compliance.employer.booking_adjust',$booking->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-sliders"></i>&nbsp;@lang('buttons.backend.compliance.booking_adjust')</a>
						</span>
                                {{--close--}}
                                <span>

                                    {!! link_to('compliance/employer/profile/' . $booking->employer_id . '#bookings',trans('buttons.general.close'),['id'=> 'cancel', 'class' => 'btn btn-primary site-btn nav_button icon fa fa-close',  ]) !!}
						</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div>&nbsp;</div>
        {{--Booking summary--}}
        {{--booking Summary table--}}

        <div class="row">
            {{--left booking  summary--}}
            <div class="col-md-6">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered">
                        <tbody>
                        {{--rcv date--}}
                        <tr>
                            <td width="180px">@lang('labels.backend.finance.receipt.contrib_month')</td>
                            <th>{!! Form::label( 'rcv_date',
                           $booking->rcv_date_formatted , [
                            'id'=>
                                'rcv_date'])
                                !!}    </th>
                        </tr>
                        {{--booked amount--}}
                        <tr>
                            <td>@lang('labels.backend.table.booked_amount')</td>
                            <th>{!! Form::label( 'booked_amount',  $booking->amount_formatted, [ 'id'=>'booked_amount']) !!}</th>
                        </tr>

                        {{--paid amount--}}
                        <tr>
                            <td>@lang('labels.backend.table.amount_paid')</td>
                            <th>{!! Form::label( 'amount_paid', $paid_amount ,[ 'id'=>
                                'amount_paid']) !!}</th>
                        </tr>
                        {{--remain amount--}}
                        <tr>
                            <td>@lang('labels.backend.table.remain_amount')</td>
                            <th>{!! Form::label( 'remain_amount', $remain_amount ,[ 'id'=>
                                'remain_amount']) !!}</th>
                        </tr>

                        </tbody></table>

                </div>
            </div>
            {{--right booking adjust summary--}}
            {{--Only if booking is adjusted dispalye this--}}
            @if(!empty($booking->adjust_amount))
                <div class="col-md-6">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered" style="width:100%">
                            <tbody>
                            {{--header adjustment--}}
                            <tr>
                                <td align="center"><b>@lang('labels.backend.compliance.booking_adjustment_summary_header')  &nbsp;   <span> {!!
                                $booking->adjustment_status_label
                                !!}</span></b></td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table table-striped table-bordered">
                            <tbody>
                            {{--adjusted amount--}}
                            <tr>
                                <td width="180px">@lang('labels.backend.table.adjust_amount')</td>
                                <th>{!! Form::label( 'adjust_amount', (
                                $booking->adjust_amount_formatted), [
                                'id'=>
                                'adjust_amount']) !!}</th>
                            </tr>
                            {{--Display old and New Interest Interchangebly when isadjusted --}}
                            {{--Old Interest--}}
                            @if ($booking->isadjusted == 1)
                                <tr>
                                    <td width="180px">@lang('labels.backend.compliance.old_booking_amount')</td>
                                    <th>{!! Form::label( 'old_booking_amount', number_format( ( $booking->amount - $booking->adjust_amount ), 2 , '.' , ',' ), ['id'=> 'old_booking_amount']) !!}</th>
                                </tr>
                            @else
                                {{--New booking--}}
                                <tr>
                                    <td width="180px">@lang('labels.backend.compliance.new_booking_amount')</td>
                                    <th>{!! Form::label( 'new_booking_amount',number_format( ( $booking->amount + $booking->adjust_amount ), 2 , '.' , ',' ), [
                                'id'=>
                                'new_booking_amount']) !!}</th>
                                </tr>
                            @endif


                            </tbody></table>

                    </div>
                </div>

            @endif
        </div>


        {{--Bottom tab navigation--}}

        <div class = "row">
            <div class="col-md-12">
                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#bottom_tab-1" data-toggle="tab">@lang('labels.backend.legend.workflow')</a>
                        </li>

                    </ul>
                    <div class="nav_tab_contain tab-content">
                        {{--tab 1--}}
                        <div id="bottom_tab-1" class="nav_tab_pane tab-pane active in">
                            {{--main workflow tracks--}}
                            @include('backend.includes.workflow_table_header')
                        </div>


                    </div>
                </div>
            </div>
        </div>
        {{--{!! Form::close() !!}--}}
    </div>


@stop


@push('after-script-end')

{{--<script  type="text/javascript">--}}

{{--$(function() {--}}
{{--$('#workflow-table').DataTable({--}}
{{--processing: true,--}}
{{--serverSide: true,--}}
{{--stateSave: true,--}}
{{--stateSaveCallback: function (settings, data) {--}}
{{--localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));--}}
{{--},--}}
{{--stateLoadCallback: function (settings) {--}}
{{--return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));--}}
{{--},--}}
{{--ajax:{--}}
{{--url : '{!! route('backend.compliance.employer.get') !!}',--}}
{{--type : 'get'--}}


{{--},--}}
{{--columns: [--}}
{{--{ data: 'name' , name: 'name'},--}}
{{--{ data: 'reg_no', name: 'reg_no'},--}}
{{--{ data: 'date_of_commencement', name: 'date_of_commencement'},--}}
{{--{ data: 'country', name: 'country' },--}}
{{--{ data: 'region', name: 'region' }--}}
{{--],--}}
{{--"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {--}}
{{--$(nRow).click(function() {--}}
{{--document.location.href = "employer/"+ "profile/" + aData['id'] ;--}}
{{--}).hover(function() {--}}
{{--$(this).css('cursor','pointer');--}}
{{--}, function() {--}}
{{--$(this).css('cursor','auto');--}}
{{--});--}}
{{--}--}}

{{--});--}}

{{--});--}}
{{--</script>;--}}

@endpush
