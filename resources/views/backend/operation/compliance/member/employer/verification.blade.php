@extends('layouts.backend.main', ['title' => trans('labels.backend.member.verification.title'), 'header_title' => trans('labels.backend.member.verification.list')])

@include('backend.includes.datatable_assets')
@push('after-styles-end')
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12">
            {{--HEADER--}}
            @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

            <br/>
            <br/>
            <br/>

            @if ($employer->onlineVerifications()->count())
                {!! $dataTable->table(['class' => 'display', 'cellspacing' => '0', 'width' => '100%' ,'id'=>'dataTable'],true) !!}
            @else
                <span class="underline" style="font-weight: 400;font-size: 14px;">@lang("labels.backend.member.verification.none")</span>
            @endif

        </div>
    </div>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{!! $dataTable->scripts() !!}
@endpush