@extends('layouts.backend.main', ['title' => "Open Business - Edit", 'header_title' => "Open Business - Edit"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.compliance.employer_closure.open.update', $closure_open->id], 'method' => 'put']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}
        {!! Form::hidden('close_date',  ($employer_closure) ? $employer_closure->close_date :  standard_date_format($employer->doc)) !!}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="fileld-layout">
                        <label>Reason for Opening Business</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::textarea('open_reason', $closure_open->reason, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                            </div>
                            <span class="help-block">
                                        <p>Specify reason(s) for employer opening a business</p>
                                    </span>
                            {!! $errors->first('open_reason', '<span class="help-block label label-danger">:message</span>') !!}

                        </div>

                    </div>

                </div>
            </div>


            <div class=" row">
                <div class="col-md-4">
                    <label class="required">Open Date</label>
                    <div class="form-group">

                        <div class="form-inline">

                            <div class="input-group" style="width:100%;">
                                {!! Form::text('open_date',  short_date_format($closure_open->open_date), ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            <span class="help-block">
                                        <p>Date of re-opening business</p>
                                    </span>
                        </div>
                        {!! $errors->first('open_date', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            <br/>

            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <div class="pull-right">
                        <input type="submit" class="btn btn-success btn-block btn-submit" value="Save" />
                    </div>
                </div>
            </div>

        </div>

        {!! Form::close() !!}
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function() {
            autosize($("textarea.autosize"));

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/
        });
    </script>
@endpush