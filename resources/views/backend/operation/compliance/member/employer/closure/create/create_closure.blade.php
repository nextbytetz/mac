@extends('layouts.backend.main', ['title' => "Business De-Registration", 'header_title' => "Business De-Registration"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')

    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */

        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>

        {!! Form::open(['route' => ['backend.compliance.employer.closure.store'], 'name' => 'employer_close_business', 'class' => 'employer_close_business', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}
        {!! Form::hidden('action_type', 1) !!}
        {!! Form::hidden('close_date_cut_off',$close_date_cut_off) !!}
        {!! Form::hidden('today',standard_date_format(getTodayDate())) !!}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">

                    <div class=" row filed-layout">
                        <div class="col-md-6">
                            <label class="required">Closure Type</label>
                            <div class="form-group" >
                                {!! Form::select('closure_type', ['1' => 'Temporary De-Registration', '2' => 'Permanent De-Registration'], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'closure_type', ]) !!}
                                {{--<span class="help-block">--}}
                                {{--<p>Date of closing the business</p>--}}
                                {{--</span>--}}
                                {!! $errors->first('closure_type', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                    <div class=" row filed-layout">
                        <div class="col-md-6">
                            <label class="required">Sub Type</label>
                            <div class="form-group" id="tempo_subtype_div">
                                {!! Form::select('sub_type_cv_id',$tempo_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select tempo_input_opt sub_type_cv_id', 'id' => 'tempo_subtype_id', ]) !!}

                                {!! $errors->first('sub_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                                <small class="help-block">
                                    Select Sub closure type where applicable.
                                </small>

                            </div>

                            <div class="form-group tempo_input_div" id="permanent_subtype_div">
                                {!! Form::select('sub_type_cv_id',$permanent_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select tempo_input_opt sub_type_cv_id', 'id' => 'permanent_subtype_id', ]) !!}

                                {!! $errors->first('sub_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                                <small class="help-block">
                                    Select Sub closure type where applicable.
                                </small>

                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout"  id="dormant_source_div">
                        <div class="col-md-6">
                            <label class="required">Authorities Source</label>
                            <div class="form-group">
                                {!! Form::select('dormant_source_cv_id',$dormant_sources, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'dormant_source_cv_id', ]) !!}

                                {!! $errors->first('dormant_source_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                                <small class="help-block">
                                    Select source for this dormant de-registration
                                </small>

                            </div>

                        </div>
                    </div>


                    <div class=" row filed-layout"  id="other_dormant_source_div">
                        <div class="col-md-6">
                            <label class="required">Other Authorities Source</label>
                            <div class="form-group">
                                {{--{{ Form::label('other_dormant_source', null, ['class' =>'required']) }}--}}
                                {{ Form::text('other_dormant_source',null,['class'=>'form-control', 'required', 'id' => 'other_dormant_source','placeholder' => '', 'autocomplete' => 'off']) }}
                                {!! $errors->first('other_dormant_source', '<span class="badge badge-danger">:message</span>') !!}
                            </div>

                        </div>
                    </div>


                    <div class=" row filed-layout">
                        <div class="col-md-6">
                            <label class="required">Close Date</label>
                            <div class="form-group">

                                <div class="form-inline">

                                    <div class="input-group" style="width:100%;">
                                        {!! Form::text('close_date',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off', 'id' => 'close_date']) !!}

                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>
                                    <small class="help-block">
                                        Date of closing the business i.e. when business stopped operating
                                    </small>

                                    {!! $errors->first('close_date', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout">
                        <div class="col-md-6">
                            <label class="required">Application Date</label>
                            <div class="form-group">

                                <div class="form-inline">

                                    <div class="input-group" style="width:100%;">
                                        {!! Form::text('application_date',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>
                                    <small class="help-block">
                                        Date of letter requesting business closure.
                                    </small>

                                    {!! $errors->first('application_date', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout tempo_input_div" id="specified_reopen_date_div">
                        <div class="col-md-6">
                            <label class="">Expected re-open Date</label>
                            <div class="form-group">

                                <div class="form-inline">

                                    <div class="input-group" style="width:100%;">
                                        {!! Form::text('specified_reopen_date',  null, ['placeholder' => '', 'class' => 'form-control datepicker2 tempo_input_opt', 'autocomplete' => 'off', 'id' => 'specified_reopen_date']) !!}

                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>
                                    <small class="help-block">
                                        Fill expected re-open date if specified by employer.
                                    </small>

                                    {!! $errors->first('specified_reopen_date', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="filed-layout">
                        <label class="required">Reason for Closing Business</label>
                        <div class="form-group">
                            <div class="input-group text_content">
                                {!! Form::textarea('reason', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                            </div>
                            <span class="help-block">
                                        <p>Specify reason(s) for closing a business</p>
                                    </span>
                            {!! $errors->first('reason', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>



                    <div class="filed-layout" id="domestic_div">
                        {{--<label>Is closure for Domestic employer</label>--}}
                        <div class="form-group">
                            <div class="input-group text_content">
                                {!! Form::checkbox('isdomestic', '1', false, ['id' => 'isdomestic', 'class' => '']) !!}  Is closure for Domestic employer?
                            </div>
                            <span class="help-block">
                                        <p style="font-size: 10px">Tick if is domestic employer i.e. employer wrongly contributed to WCF</p>
                                    </span>
                            {!! $errors->first('isdomestic', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>





                </div>

            </div>
            <br/>
            {{--<hr/>--}}
            {{--Buttons--}}
            <div class="row">
                <div class="col-md-4" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                        <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.employer.profile',trans('buttons.general.cancel'), [$employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();
            var cutoff_date = '{{ \Carbon\Carbon::parse($close_date_cut_off)->format('Y/n/j') }}';
            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: cutoff_date,
            });

            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
            });
            /*-----------End Date Process------------*/

            /*Hide tempo input*/
            $('.tempo_input_div').hide();

            domestic_option();
            tempo_input_option();
            $("#closure_type").on("change", function (e) {
                domestic_option();
                tempo_input_option();
                dormant_source_option();
            });


            // domestic option
            function domestic_option() {
                var choice = $("#closure_type").val();
                $('#isdomestic').prop('checked', false);
//            if is visit
                if (choice == '2') {
                    /*permanent*/
                    $('#domestic_div').show();
                } else {
                    /*Temporary*/
                    $('#domestic_div').hide();
                }
            }



            // tempo input option
            function tempo_input_option() {
                var choice = $("#closure_type").val();
                if (choice == '2') {
                    /*permanent*/
                    $('.tempo_input_div').hide();
                    $('.tempo_input_opt').val(null).change();
                    $('#permanent_subtype_div').show();
                    $('#permanent_subtype_id').prop('disabled', false);

                    //tempo
                    $('#tempo_subtype_div').hide();
                    $('#tempo_subtype_id').prop('disabled', true);

                } else if (choice == '1') {
                    /*Temporary*/
                    $('.tempo_input_div').show();

                    $('#tempo_subtype_div').show();
                    $('#tempo_subtype_id').prop('disabled', false);

                    //permanent
                    $('#permanent_subtype_div').hide();
                    $('#permanent_subtype_id').prop('disabled', true);

                }
            }



//dormant

            $('#dormant_source_div').hide();
            $("#tempo_subtype_id").on("change", function (e) {
                dormant_option();
                dormant_source_option();
                other_dormant_source_option();
            });


            dormant_option();
            function dormant_option() {
                var choice = $("#tempo_subtype_id").val();
                if (choice == 'ETCLDORMANT') {

                    //Dormant source
                    $('#dormant_source_div').show();
                    $('#dormant_source_cv_id').prop('disabled', false);

                    //reopen
                    $('#specified_reopen_date_div').hide();
                    // $('#specified_reopen_date').val('');
                    $('#specified_reopen_date').prop('disabled', true);


                } else {
                    //Dormant source
                    $('#dormant_source_div').hide();
                    $('#dormant_source_cv_id').prop('disabled', true);

                    //reopen
                    $('#specified_reopen_date_div').show();
                    // $('#specified_reopen_date').val('');
                    $('#specified_reopen_date').prop('disabled', false);
                }
            }


            // dormant_source_option();
            $("#dormant_source_cv_id").on("change", function (e) {
                dormant_source_option();
                other_dormant_source_option();
            });

            function dormant_source_option()
            {
                var choice = $("#dormant_source_cv_id").val();
                var temp_sub_type = $("#tempo_subtype_id").val();
                if (choice == 'AUSDREGWCF' && temp_sub_type == 'ETCLDORMANT') {
                    //Dormant source
                    $('#close_date').prop('disabled', true);
                    $('#close_date').val('Date will pick from Last contribution').change();

                }else {
                    $('#close_date').prop('disabled', false);
                    $('#close_date').val('').change();

                }
            }

            /*Other dormant source option*/
            other_dormant_source_option();
            function other_dormant_source_option()
            {
                var choice = $("#dormant_source_cv_id").val();
                var temp_sub_type = $("#tempo_subtype_id").val();
                if(choice == 'AUSDREGOTHE'  && temp_sub_type == 'ETCLDORMANT'){
                    hide_show('show_id','other_dormant_source_div');
                    enable_disable('enable_id','other_dormant_source');
                }else{
                    hide_show('hide_id','other_dormant_source_div');
                    enable_disable('disable_id','other_dormant_source');
                }
            }

        });
    </script>

@endpush