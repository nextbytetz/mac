
{{-- summary--}}
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Open Details- {!! $employer_closure_open->status_label !!} </legend>
            <table class="table table-striped table-bordered">
                <tbody>

                {{--close date--}}
                @if(isset($employer_closure))
                    <tr>
                        <td width="120px">Close Date</td>
                        <th>{!! Form::label( 'close_date',
                   $employer_closure->close_date_formatted, [])
                                !!} </th>
                    </tr>
                @endif
                {{--Approved  date--}}
                @if($employer_closure_open->wf_done == 1)
                    <tr>
                        <td>Approved Date</td>
                        <th>{!! Form::label( 'approved_date',
                  short_date_format( $employer_closure_open->wf_done_date), [])
                                !!} </th>
                    </tr>
                @endif

                {{--open  date--}}

                <tr>
                    <td width="120px">Re-Open Date</td>
                    <th>{!! Form::label( 'open_date',
                   $employer_closure_open->open_date_formatted, [])
                                !!} </th>
                </tr>


                {{--closure type--}}
                @if(isset($employer_closure))
                    <tr>
                        <td>Closure Type</td>
                        <th>
                            {!!   $employer_closure->close_type_name
                                        !!}</th>
                    </tr>
                @endif
                {{--Reason--}}
                <tr>
                    <td>Open Reason</td>
                    <td>
                        {!! $employer_closure_open->reason
                                    !!}</td>
                </tr>


                {{--created by--}}
                <tr>
                    <td>Created by</td>
                    <td>
                        {!! $employer_closure_open->user->username
                                    !!}</td>
                </tr>




                </tbody>
            </table>

        </div>
    </div>





</div>

@push('after-script-end')

    <script  type="text/javascript">


        $(function () {


        });
    </script>;

@endpush
