
<div class = "row">
    <div class="col-md-12">

        <div class="col-md-4">

            <legend>Documents Attached
                @if(($check_pending_level1 == 1 || $check_workflow == 0) && $employer_closure->is_legacy == 0)
                    <a class="pull-right" style="color:blue; font-size: 12px"  href="{!! route('backend.compliance.employer.closure.attach_document', $employer_closure->id) !!}">{!! 'Add Document' !!}</a>
                @endif
            </legend>

            <br/>

            {{--new Bank--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="element-form" >

                        {{--<ul>--}}
                        @foreach($docs_attached as $doc)
                            {{--<li>--}}
                            <i class="fa fa-file-pdf-o" ></i>
                            <a  style="color:dodgerblue;" class="doc_attached"  href="#" id="{!! 'doc'. $doc->pivot->id !!}">{!! ($doc->id != 70) ? $doc->name : $doc->pivot->description !!}</a>
                            @if(($check_pending_level1 == 1 || $check_workflow == 0) && $employer_closure->is_legacy == 0)
                                |

                                <a class="" style="color:grey"  href="{!! route('backend.compliance.employer.closure.edit_document', $doc->pivot->id) !!}">{!! 'Edit' !!}</a>
                                {{--<span style="font-size: 12px; color:#414141"> {!!   '' . ' : ' . $pollOption->votes  !!}   </span>--}}
                                {{--<span style="font-size: 12px; color:#414141"> {!!   $pollOption->vote_percent_label  !!} </span>--}}
                                {{--</li>--}}
                            @endif
                            <br/>
                        @endforeach
                        {{--</ul>--}}
                    </div>

                </div>
            </div>

        </div>




        <div class="col-md-8">
            <div class = "row">
                <div class="col-md-12">
                    {{--Document Preview--}}
                    <legend>Document Preview</legend>
                    <br/>
                    <div id="document_frame" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>





@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    <script  type="text/javascript">


        $(function () {

            $(".search-select").select2();





            /*Documents which pending to be used list*/
            $(".doc_attached").click(function() {
                   var $doc_id = this.id;
                var $pivot_id = $doc_id.substr(3);
                      let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });



            function get_current_document($doc_pivot_id) {

                return $.ajax({
                    url: base_url + "/compliance/employer/closure/preview/document/"+ $doc_pivot_id,
                    dataType : 'json',
                    async : false,
                    method : "POST"
                });
            }


        });
    </script>;

@endpush
