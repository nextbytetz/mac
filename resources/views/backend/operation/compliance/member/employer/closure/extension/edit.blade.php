@extends('layouts.backend.main', ['title' => 'Edit Extension' , 'header_title' => 'Edit Extension'])

@include('backend/includes/assets/datetimepicker')

@push('after-styles-end')
    <style>
    </style>
@endpush

@section('content')




    {{ Form::model($employer_closure_extension,['route' => ['backend.compliance.employer_closure.extension.update',$employer_closure_extension->id], 'method'=>'put','autocomplete' => 'off', 'id' => 'update', 'name' => 'edit', 'class' => 'form-horizontal needs-validation', 'novalidate',]) }}

    {{ Form::hidden('action_type', 2, []) }}
    {{ Form::hidden('today', getTodayDate(), []) }}
    <section class="card">
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer_closure_extension->employerClosure->closedEmployer])

<div>&nbsp;</div>

        <div class="card-body">
            {{--<p>{!! getLanguageBlock('lang.auth.mandatory-field') !!}</p>--}}

            <div class="row">
                          <div class="col-md-3">
                    <div class="form-group ">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        {{ Form::label('application_date', 'Application Date', ['class' =>'required']) }}
                                        <div class="input-group">
                                            {{ Form::text('application_date',short_date_format($employer_closure_extension->application_date),['class'=>'form-control datepicker2', 'required', 'id' => 'application_date','placeholder' => '', 'autocomplete' => 'off']) }}
                                            <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                                        </div>
                                        {!! $errors->first('application_date', '<span class="badge badge-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        {{ Form::label('new_reopen_date', 'New Expected Reopen Date', ['class' =>'']) }}
                                        <div class="input-group">
                                            {{ Form::text('new_reopen_date',short_date_format($employer_closure_extension->new_reopen_date),['class'=>'form-control datepicker2', 'required', 'id' => 'new_reopen_date','placeholder' => '', 'autocomplete' => 'off']) }}
                                            <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                                        </div>
                                        {!! $errors->first('new_reopen_date', '<span class="badge badge-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group text_content">
                                        {{ Form::label('remark', 'Remark', ['class' =>'']) }}
                                        {{ Form::textarea('remark',$employer_closure_extension->remark,['class'=>'form-control', 'id' => 'remark','placeholder' => '', 'autocomplete' => 'off']) }}
                                        {!! $errors->first('remark', '<span class="badge badge-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-3">
                    <div class="element-form">
                        <div class="form-group pull-right">
                            {{ link_to_route('backend.compliance.employer_closure.extension.profile',trans('buttons.general.cancel'),[$employer_closure_extension->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) }}
                            {{ Form::button(trans('buttons.general.submit'), ['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit', 'style' => 'border-radius: 5px;']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{ Form::close() }}
@endsection

@push('after-script-end')
    <script>
        $(function() {
        });
    </script>
@endpush
