{{--create Form--}}

{{--main contents--}}
<br/>



{{--follow up type--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.follow_up_type'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('follow_up_type_cv_id', $follow_up_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'follow_type_id']) !!}

                    {!! $errors->first('follow_up_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>

{{--contact--}}
<div class="row" id="contact_div">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.contact'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.contact_tooltip_description')  "></i></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact', null, ['class' => 'form-control', 'id'=>'contact_id']) !!}

                    {!! $errors->first('contact', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>


{{--contact person--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.contact_person'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact_person', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_person', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

{{--date_of_follow_up--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.date_of_follow_up'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                {{--<div class="row">--}}
                <div class="form-group">

                    <div class="form-inline">

                        <div class="input-group" style="width:100%;">
                            {!! Form::text('date_of_follow_up',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        {{--<span class="help-block">--}}
                                        {{--<p>Date of closing the business</p>--}}
                                    {{--</span>--}}
                    </div>
                </div>
                          {!! $errors->first('date_of_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>

{{--description--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Follow up @lang('labels.general.descriptions'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group text_content">
                    {!! Form::textarea('description', ($isstatus_followup == 0) ? null : 'Status followup if has reopened or still closed', ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                    {!! $errors->first('description', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

@if($isstatus_followup == 0)
{{--feedback--}}
<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.feedback'):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <div class="form-group text_content" >
                        {!! Form::textarea( 'feedback', null, [ 'id'=> 'address',  'class' =>'form-control']) !!}
                        {!! $errors->first('feedback', '<span class="help-block label
                           label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@else

<div class="row">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Follow Up Outcome:</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('followup_outcome', ['1' => 'Still Closed', '2' => 'Re-Opened'], [], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', '']) !!}
                    {!! $errors->first('followup_outcome', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endif

{{--ATtach file input--}}
<div class="row" id="doc_form_div">
    <div class="col-md-9">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Attach Document (PDF):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::file('document_file') !!}
                    {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>


