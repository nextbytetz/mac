@extends('layouts.backend.main', ['title' => 'Employer Temporary Closure For Status Follow Ups', 'header_title' => 'Employer Temporary Closure For Status Follow Ups'])

@include('backend.includes.datatable_assets')

@section('content')



    <br/>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="employer-closures-table">
                <thead>
                <tr >
                    <th>Employer Name</th>
                    <th>Reg No.</th>
                    <th>Close Date</th>
                    <th>Application Date</th>
                    <th>Approved Date</th>
                    <th>Last Follow up Date</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#employer-closures-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.closure.get_tempo_closed_followup_for_dt') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'employer_name' , name: 'e.name', orderable : true, searchable : true},
                    { data: 'employer_regno', name: 'e.reg_no', orderable : true, searchable : true},
                    { data: 'close_date_formatted', name: 'close_date',  orderable : false, searchable : false},
                    { data: 'application_date_formatted', name: 'application_date',  orderable : false, searchable : false},
                    { data: 'approved_date_formatted', name: 'approved_date',  orderable : false, searchable : false},
                    { data: 'followup_ref_date_formatted', name: 'followup_ref_date',  orderable : false, searchable : false},
                ],

                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/closure/profile/" + aData['closure_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
