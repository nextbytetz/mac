@extends('layouts.backend.main', ['title' => "Sync Eoffice Closure to Employer", 'header_title' => "Sync Eoffice Closure to Employer"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')

    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */

        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}

        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.compliance.employer.closure.sync_closure_employer_page', $closure_alert->id], 'name' => '', 'class' => '', 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
        {{--{!! Form::hidden('employer_id', $employer->id) !!}--}}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">

                    <div class=" row filed-layout">
                        <div class="col-md-12">
                            <label class="">Subject</label>
                            <div class="form-group">
                                {!! Form::text('subject',  $closure_alert->subject, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'disabled']) !!}

                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout">
                        <div class="col-md-12">
                            <label class="">Doc Date</label>
                            <div class="form-group">
                                {!! Form::text('doc_date',  short_date_format($closure_alert->doc_date), ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'disabled']) !!}

                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout">
                        <div class="col-md-12">
                            <label class="">Doc Receive Date</label>
                            <div class="form-group">
                                {!! Form::text('doc_receive_date',  short_date_format($closure_alert->doc_receive_date), ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'disabled']) !!}

                            </div>
                        </div>
                    </div>



                    {{--<div class=" row filed-layout">--}}
                    {{--<div class="col-md-6">--}}
                    {{--<label class="">Folio</label>--}}
                    {{--<div class="form-group">--}}
                    {{--{!! Form::text('folio', $closure_alert->folio, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'disabled']) !!}--}}

                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}


                    <div class=" row filed-layout">
                        <div class="col-md-12">
                            <label class="">From</label>
                            <div class="form-group">
                                {!! Form::text('from', $closure_alert->from, ['placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'disabled']) !!}

                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout">
                        <div class="col-md-12">
                            <label class="required">Employer</label>
                            <div class="form-group">
                                {!! Form::select('employer_id', [], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control employer-select', ]) !!}
                                {!! $errors->first('employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                    <div class=" row filed-layout">
                        <div class="col-md-12">
                            <label class="required">Closure Type</label>
                            <div class="form-group">
                                {!! Form::select('closure_type', ['1' => 'Temporary Closure', '2' => 'Permanent Closure'], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', ]) !!}
                                {{--<span class="help-block">--}}
                                {{--<p>Date of closing the business</p>--}}
                                {{--</span>--}}
                                {!! $errors->first('closure_type', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout">
                        <div class="col-md-12">
                            <label class="required">Close Date</label>
                            <div class="form-group">

                                <div class="form-inline">

                                    <div class="input-group" style="width:100%;">
                                        {!! Form::text('close_date',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>
                                    <span class="help-block">
                                        <p>Date of closing the business</p>
                                    </span>

                                    {!! $errors->first('close_date', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row filed-layout">
                        <div class="col-md-12">
                            <label class="required">Reason for Closing Business</label>
                            <div class="form-group">
                                <div class="input-group text_content">
                                    {!! Form::textarea('reason', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                                </div>
                                <span class="help-block">
                                        <p>Specify reason(s) for closing a business</p>
                                    </span>
                                {!! $errors->first('reason', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-8">
                    {{--DOC AREA--}}
                    <legend>Document Review: Closure Letter</legend>
                    <br/>
                    <div id="document_frame" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>

                </div>


            </div>
            <br/>
            {{--<hr/>--}}
            {{--Buttons--}}
            <div class="row">
                <div class="col-md-4" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                        <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.employer.closure.eoffice_alerts',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/



            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        if (data.total_count === 0) {
                            $employer_unavailable = 1;
                        } else {
                            $employer_unavailable = 0;
                        }
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:select", function(e) {
                $employer_unavailable = 0;
                $('.employer_unavailable_check').removeAttr('checked');
            });

        });
    </script>

@endpush