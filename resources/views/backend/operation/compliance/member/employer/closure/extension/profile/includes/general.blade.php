<legend style="background-color: lightgray; color: grey;"> {{ 'General Information' }}</legend>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped" id="general_info">
            <tbody>
            <tr>
                <td width ="140">De-Registration :</td>
                <td><a style="color: blue;" href="{!! route('backend.compliance.employer.closure.profile', $employer_closure_extension->employer_closure_id) !!}">{{ $employer_closure_extension->employerClosure->close_type_name  }}</a></td>
            </tr>

            <tr>
                <td width ="140">Application Date :</td>
                <td>{{ short_date_format($employer_closure_extension->application_date) }}</td>
            </tr>
            <tr>
                <td >New Expected Reopen Date :</td>
                <td>{{ short_date_format($employer_closure_extension->new_reopen_date )}}</td>
            </tr>

            {{--Leter--}}
            @if($employer_closure_extension->closureExtensionLetter()->count() > 0)
                <tr>
                    <td>Letter</td>
                    <td>
                        <a style="color:blue" href="{{ route('backend.letter.show', $employer_closure_extension->closureExtensionLetter->id) }}" >{{ $employer_closure_extension->closureExtensionLetter->reference }}</a></td>
                </tr>
            @endif
            <tr>
                <td >Remark :</td>
                <td>{{ $employer_closure_extension->remark }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>