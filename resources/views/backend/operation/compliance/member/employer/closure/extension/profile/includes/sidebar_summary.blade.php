<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Pending Tasks</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>


<div class="row">
    <div class="light_grey_bg">&nbsp;</div>

    <div class="light_grey_bg">
        {{--<table style="width:100%">--}}
        {{--@if(!$check_if_first_payroll)--}}
        {{--new Pensioners--}}
        {{--<tr>--}}
        {{--<td style="padding-left: 5px" width="140px">New Pensioners <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="New pensioners to be paid for the first time on--}}
        {{--this payroll"></i>:</td>--}}
        {{--<td  height="20px"> <b>{!! $data_summary['new_pensioners'] !!}</b></td>--}}
        {{--<td > <a href="#" data-toggle="modal" data-target="#new_pensioners_modal" id="new_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a> </td>--}}
        {{--</tr>--}}



        {{--</table>--}}
        <ol>
            @foreach($pending_tasks as $pending_task)

                <li style="margin-left:10px;">
                    {!! $pending_task !!}
                </li>

            @endforeach

            {{--letter response--}}
                @if($employer_closure_extension->status_if_letter_initiated == false &&  $check_if_can_initiate_wf_letter == true)
                    <li style="margin-left:10px;">
                        {!! 'Response letter not initiated yet ' !!}

                    </li>

                @endif

        </ol>
    </div>
</div>
