@extends('layouts.backend.main', ['title' => "Confirm Employer Payrolls", 'header_title' => "Confirm Employer Payrolls"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>
        {{ Form::model($employer_closure,['route' => ['backend.compliance.employer.closure.confirm_payrolls', $employer_closure->id],'method'=>'put',
         'id' => 'update', 'name' => 'update']) }}

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="fileld-layout">
                        <label>Choose Payroll(s) closed</label>
                        <div class="form-group">
                            {{ Form::select('payroll_ids[]',$payroll_ids,isset($employer_closure->payroll_ids) ? json_decode($employer_closure->payroll_ids) : null,['class'=>'form-control search-select', 'required', 'id' => 'payroll_ids', 'autocomplete' => 'off', 'multiple']) }}
                            {!! $errors->first('payroll_ids', '<span class="badge badge-danger">:message</span>') !!}

                        </div>

                    </div>

                </div>
            </div>


           <br/>

            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.employer.closure.profile',trans('buttons.general.cancel'), [$employer_closure->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>

        </div>

        {!! Form::close() !!}
    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});

        });
    </script>
@endpush