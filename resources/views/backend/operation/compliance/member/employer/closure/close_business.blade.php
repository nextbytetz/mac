@extends('layouts.backend.main', ['title' => "Close Business", 'header_title' => "Close Business"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */
        .progress-bar {
            background-color: #12CC1A;
            height:20px;
            color: #FFFFFF;
            width:0%;
            -webkit-transition: width .3s;
            -moz-transition: width .3s;
            transition: width .3s;
        }
        .progress-div {
            border:#0FA015 1px solid;
            padding: 5px 0px;
            margin:30px 0px;
            border-radius:4px;
            text-align:center;
        }
        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.compliance.employer.close_business.post', $employer->id], 'name' => 'employer_close_business', 'class' => 'employer_close_business', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="fileld-layout">
                        <label>Reason for Closing Business</label>
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::textarea('reason', null, ['class' => 'form-control autosize',  'style' => 'border-radius: 3px;']) !!}
                            </div>
                            <span class="help-block">
                                        <p>Specify reason(s) for closing a business</p>
                                    </span>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="filed-layout">
                        <label class="required">Close Date</label>
                        <div class="form-group">
                            <div class="form-inline">
                                        <span>
                                            {!!  Form::selectRange('close_day',1 , 31, null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' => 'Day']) !!}
                                        </span>
                                <span>
                                            {!!  Form::selectMonth('close_month', null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Month']) !!}
                                        </span>
                                <span>
                                            {!!  Form::selectRange('close_year',  \Carbon\Carbon::now()->format('Y'), 2015 , null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' => 'Year']) !!}
                                        </span>
                                <span>
                                            {!! Form::hidden('today_date', \Carbon\Carbon::now()->format("Y-n-j")) !!}
                                    {!! Form::hidden('close_date') !!}
                                        </span>
                            </div>
                            <span class="help-block">
                                        <p>Date of closing the business</p>
                                    </span>
                        </div>
                    </div>

                    <div class=" row fileld-layout">
                        <div class="col-md-6">
                        <label class="required">Closure Type</label>
                        <div class="form-group">
                            {!! Form::select('closure_type', ['1' => 'Temporary Closure', '2' => 'Permanent Closure'], null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', ]) !!}
                            <span class="help-block">
                                        {{--<p>Date of closing the business</p>--}}
                                    </span>

                        </div>
                        </div>
                    </div>

                    <div class="fileld-layout">
                        <label>Closing Business Letter</label>
                        <div class="form-group">
                            {!! Form::file('employer_close_file') !!}
                            <span class="help-block">
                                        <p>Upload Employer Letter confirming closing business</p>
                                    </span>
                        </div>
                        {{-- start : Displaying the progress of uploading file--}}
                        <div class="progress-div">
                            <div class="progress-bar"></div>
                        </div>
                        {{-- end : Displaying the progress of uploading file--}}
                    </div>
                </div>

            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <input type="submit" class="btn btn-success btn-block btn-submit" value="Save" />
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});
            autosize($("textarea.autosize"));
            /* start: Submitting Form and perform validation on the server side */
            $('body').on('submit', 'form.employer_close_business', function (e) {
                e.preventDefault();
                var $form = this;
                swal({
                    title: "Warning",
                    text: "Are you sure to close this business?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        var $close_day = $($form).find("select[name=close_day]").val();
                        var $close_month = $($form).find("select[name=close_month]").val();
                        var $close_year = $($form).find("select[name=close_year]").val();
                        if ($close_day && $close_month && $close_year) {
                            $($form).find("input[name=close_date]").val($close_year + '-' + $close_month + '-' + $close_day);
                        } else {
                            $($form).find("input[name=close_date]").val("");
                        }
                        /* start: remove any printed error message in the input controls */
                        $($form).find(':input').each(function () {
                            var $name = $(this).attr('name');
                            $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();

                        });
                        /* end: remove any printed error message in the input controls */
                        var $options = {
                            dataType : "json",
                            type : "POST",
                            url : $($form).attr("action"),
                            beforeSend : function (e) {
                                $($form).find(".progress-bar").width('0%');
                                $($form).find(".btn-submit").prop('disabled', true);
                            },
                            success : function (data) {
                                $($form).find(".btn-submit").prop('disabled', false);
                                document.location.href =  base_url + "/compliance/employer/profile/{{ $employer->id }}";
                            },
                            error: function (data) {
                                $($form).find(".progress-bar").width('0%');
                                $($form).find(".btn-submit").prop('disabled', false);
                                var errors = $.parseJSON(data.responseText);
                                /* console.log(errors); */
                                $.each(errors, function(index, value) {
                                    $($form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                    $($form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                    $($form).find("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                                  });
                            },
                            uploadProgress : function (event, position, total, percentComplete) {
                                $($form).find(".progress-bar").width(percentComplete + '%');
                                $($form).find(".progress-bar").html('<div class="progress-status">' + percentComplete +' %</div>')
                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
        });
    </script>
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush