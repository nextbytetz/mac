@extends('layouts.backend.main', ['title' => 'Edit Closure Document', 'header_title' => 'Edit Closure Document'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.employer.closure.update_document'], 'name' => 'employer_close_business', 'class' => 'attach_doc', 'method' => 'put','enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('employer_closure_id', $employer_closure->id, ['class' =>'']) !!}
    {!! Form::hidden('close_date', $employer_closure->close_date, ['class' =>'']) !!}
    {!! Form::hidden('document_id', $uploaded_document->pivot->document_id, ['class' =>'']) !!}
    {!! Form::hidden('doc_pivot_id', $uploaded_document->pivot->id, ['class' =>'']) !!}
    {!! Form::hidden('action_type', 2, ['class' =>'']) !!}
    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
    <br/>

    {{--<div>--}}

    <br/>
    {{--</div>--}}

    <br/>


    {{--document types--}}
    <div class="row">
        <div class="pull-right" >
            {{--<br/>--}}
            <span>
            {!! HTML::decode(link_to_route('backend.compliance.employer.closure.delete_document', "<i class='icon fa fa-delete' aria-hidden='true'></i>&nbsp;" . 'Delete', $uploaded_document->pivot->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to delete this Document?', 'class' => 'btn btn-primary site-btn delete_button'])) !!}
            </span>
        </div>
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Document type:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! $uploaded_document->name . ' - ' .  $uploaded_document->pivot->name!!}

                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--document title--}}
    @if($uploaded_document->pivot->document_id == 70)
        <div class="row option_div" id="doc_title_div">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Document title:</label></div>
                    <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text','document_title', $uploaded_document->pivot->description, ['class' => 'form-control option_input', 'id' => 'document_title']) !!}
                            {!! $errors->first('document_title', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif

    {{--Attach file input--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Attach document (PDF):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::file('document_file') !!}
                        {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>




    {{--date_of_reference--}}
    <div class="row option_div" id="date_ref_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Date of reference:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">

                        <div class="form-inline">

                            <div class="input-group" style="width:100%;">
                                {!! Form::text('date_reference',  isset($uploaded_document->date_reference) ? short_date_format($uploaded_document->date_reference) : null, ['placeholder' => '', 'class' => 'form-control option_input datepicker1', 'autocomplete' => 'off', 'id' => 'date_reference']) !!}

                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            {{--<span class="help-block">--}}
                            {{--<p>Date of closing the business</p>--}}
                            {{--</span>--}}
                        </div>
                    </div>
                    {!! $errors->first('date_reference', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>
    </div>



    <br/>

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.employer.closure.profile',trans('buttons.general.cancel'), [$employer_closure->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}


    <script>
        $(function () {
            var document_type = '{!! $uploaded_document->pivot->document_id !!}';
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                // maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            documentOption();
            $("#document_type").on("change", function (e) {
                documentOption();
            });





            //Document type option
            function documentOption() {

                var choice = document_type;

                /*Hide and disable all*/
                $(".option_div").hide();
                $(".option_input").prop('disabled', true);
                switch (choice){
                    case '72':
                        $("#date_ref_div").show();
                        $("#date_reference").prop('disabled', false);
                        break;

                    case '70':
                        $("#doc_title_div").show();
                        $("#document_title").prop('disabled', false);
                        break;
                    default:
                        $("#date_ref_div").hide();
                        $("#date_reference").prop('disabled', true);

                        /*Doc title*/
                        $("#doc_title_div").hide();
                        $("#document_title").prop('disabled', true);
                        break;
                }

            }

        });

    </script>;


@endpush