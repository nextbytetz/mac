@extends('layouts.backend.main', ['title' => 'Employer De-Registrations', 'header_title' => 'Employer De-Registrations'])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        {{--<br/>--}}
        <div class="col-md-12" >
            <br/>
            <div class="col-md-12" >

                <div class="pull-right">





                    @if ($employer->employer_status == 3 || $employer->employer_status == 4)
                        {{--Business is closed, may open it--}}

                        <span>
    <a href="{!! route('backend.compliance.employer_closure.open.create', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" >
        <i class="icon fa fa-level-up" aria-hidden="true"></i>
    Open Business</a>
</span>


                    @else
                        {{--Business is active, may close it--}}
                        <span>
    <a href="{!! route('backend.compliance.employer.closure.create', $employer->id) !!}"  class="btn btn-primary site-btn save_button" >
        <i class="icon fa fa-level-down" aria-hidden="true"></i>
    &nbsp;Close Business</a>
</span>
                    @endif



                </div>



            </div>
        </div>


    </div>

    <br/>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="employer-closures-table">
                <thead>
                <tr >
                    <th>Reason</th>
                    <th>Close Type</th>
                    <th>Close date</th>
                    <th>Status</th>
                    <th>Approved Date</th>
                    <th>Re-Open Date</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#employer-closures-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.closure.get_for_datatable', $employer->id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'reason', name: 'reason', orderable : true, searchable : true},
                    { data: 'close_type_name' , name: 'close_type_name', orderable : false, searchable : false},
                    { data: 'close_date_formatted', name: 'close_date',  orderable : true, searchable : true},
                    { data: 'status', name: 'status',  orderable : false, searchable : false},
                    { data: 'approved_date_formatted', name: 'wf_done_date' ,orderable : true, searchable : true},
                    { data: 'open_date_formatted', name: 'open_date' ,orderable : true, searchable : true},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/closure/profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
