@extends('layouts.backend.main', ['title' => "Fill Status Follow up", 'header_title' => "Fill Status Follow up"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')

    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */

        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>

        {!! Form::model($employer_closure,['route' => ['backend.compliance.employer.closure.store_status_followup', $employer_closure],'method'=>'put',  'id' => 'update', 'name' => 'update']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}
        {!! Form::hidden('employer_closure_id', $employer_closure->id) !!}
        {!! Form::hidden('today_date', getTodayDate()) !!}
        {!! Form::hidden('action_type', 2) !!}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">


                    <div class=" row filed-layout">
                        <div class="col-md-6">
                            <label class="required">Follow up Date</label>
                            <div class="form-group">

                                <div class="form-inline">

                                    <div class="input-group" style="width:100%;">
                                        {!! Form::text('followup_date',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                    </div>
                                    <small class="help-block">
                                        <p>Follow up date to check status of business</p>
                                    </small>
                                    {!! $errors->first('followup_date', '<span class="help-block label label-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class=" row filed-layout">
                        <div class="col-md-6">
                            <label class="required">Follow Up Outcome</label>
                            <div class="form-group">
                                {!! Form::select('followup_outcome', ['1' => 'Still Closed', '2' => 'Re-Opened'], [], ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', '']) !!}
                                {{--<span class="help-block">--}}
                                {{--<p>Date of closing the business</p>--}}
                                {{--</span>--}}
                                {!! $errors->first('followup_outcome', '<span class="help-block label label-danger">:message</span>') !!}

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <br/>
            {{--<hr/>--}}
            {{--Buttons--}}
            <div class="row">
                <div class="col-md-4" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                        <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.employer.closure.profile',trans('buttons.general.cancel'), [$employer_closure->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/

        });
    </script>

@endpush