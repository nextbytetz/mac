@extends('layouts.backend.main', ['title' => trans('labels.backend.member.edit_follow_up'), 'header_title' => trans('labels.backend.member.edit_follow_up')])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::model($follow_up,['route' => ['backend.compliance.employer.closure.follow_up.update'],'method'=>'put',  'id' => 'update', 'name' => 'update', 'enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('follow_up_id', $follow_up->id, ['class' =>'']) !!}
    {!! Form::hidden('employer_closure_id', $follow_up->employer_closure_id, ['class' =>'']) !!}
    {!! Form::hidden('close_date', $employer_closure->close_date, ['class' =>'']) !!}
    {!! Form::hidden('isstatus_followup', $isstatus_followup, ['class' =>'']) !!}
    {!! Form::hidden('action_type', 2, ['class' =>'']) !!}
    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
    <br/>

    {{--<div>--}}
    <div class="pull-right" >
        <br/>
        {!! HTML::decode(link_to_route('backend.compliance.employer.closure.follow_up.delete', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Delete', $follow_up->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to delete this follow up?', 'class' => 'btn btn-primary site-btn delete_button'])) !!}
        </span>
    </div>

    <br/>
    {{--</div>--}}

    <div class="row">

        {{--<div class ="col-md-12">--}}

        <div class ="col-md-6">
    {{--Create form--}}
    @include("backend/operation/compliance/member/employer/closure/follow_up/edit/includes/input_form",['follow_up'=>$follow_up])

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.employer.closure.profile',trans('buttons.general.cancel'), [$follow_up->employer_closure_id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

        </div>

        @if(isset($document_attached))
            {{--doc review--}}
            <div class ="col-md-6">

                <div class = "row">
                    <div class="col-md-12">
                        {{--Document Preview--}}
                        <legend>Document Preview</legend>

                        <ol>
                            <li>
                                <a  id="{!! 'doc'. $document_attached->id !!}" class="doc_attached" style="color: blue" href="#">View Document</a>
                            </li>

                        </ol>
                        <br/>
                        <div id="document_frame" style="text-align: center;">
                            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                        </div>


                    </div>

                </div>
            </div>
        @endif

    </div>

    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    {{--<script  type="text/javascript">--}}



    <script>
        $(function () {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });



            /*Documents which pending to be used list*/
            $(".doc_attached").click(function() {
                var $doc_id = this.id;
                var $pivot_id = $doc_id.substr(3);
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });


            function get_current_document($doc_pivot_id) {
                return $.ajax({
                    url: base_url + "/compliance/employer/closure/preview/follow_up_document/" + $doc_pivot_id,
                    dataType: 'json',
                    async: false,
                    method: "POST"
                });
            }

            });
        // Follow up type option
        function follow_up_type_option(follow_type_id, contact_div, contact_id) {

            var choice = $("#" + follow_type_id).val();
//            if is visit
            if (choice == 93 || choice == 408) {
                $("#" + contact_div).hide();
                $('#doc_form_div').show();
            } else {
//            phone call
                if (choice == 91) {
                    $("#" + contact_id).attr('placeholder', 'Phone #');
                } else if (choice == 92) {
                    $("#" + contact_id).attr('placeholder', 'Email@yahoo.com');
                }
                $("#" + contact_div).show();
                $('#doc_form_div').hide();

            }
        }


    </script>;


@endpush