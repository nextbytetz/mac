<div class = "row">
    <div class="col-md-12" >
        <div class="col-md-12" >
            <div class="pull-right">


                <div class="btn-group">

                    @if(($check_pending_level1 == 1 || $check_workflow == 0) && $employer_closure->is_legacy == 0)
                    <a href="{!! route('backend.compliance.employer.closure.follow_up.create', $employer_closure->id) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-plus"></i>&nbsp;Add New</a>
                        @endif
                </div>
            </div>



        </div>
    </div>


</div>

<br/>
{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="closure-follow-ups-table">
            <thead>
            <tr >
                <th>Description</th>
                <th>Feedback</th>
                <th>Follow up Type</th>
                <th>Contact</th>
                <th>Contact Person</th>
                <th>Follow up Date</th>
                <th>Staff</th>
            </tr>
            </thead>
        </table>

    </div>
</div>




@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#closure-follow-ups-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.closure.get_follow_ups_for_datatable', $employer_closure->id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'description', name: 'description', orderable : true, searchable : true},
                    { data: 'feedback' , name: 'feedback', orderable : false, searchable : false},
                    { data: 'follow_up_type', name: 'follow_up_type',  orderable : false, searchable : false},
                    { data: 'contact', name: 'contact',  orderable : true, searchable : true},
                    { data: 'contact_person', name: 'contact_person',  orderable : true, searchable : true},
                    { data: 'date_of_follow_up_formatted', name: 'date_of_follow_up' ,orderable : true, searchable : true},
                    { data: 'staff', name: 'staff',  orderable : false, searchable : false},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/closure/follow_up/edit/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
