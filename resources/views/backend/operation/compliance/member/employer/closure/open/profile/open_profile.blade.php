@extends('layouts.backend.main', ['title' => 'Business Open Profile', 'header_title' => 'Business Open Profile' .  (($employer_closure)  ? ( ' - ' .$employer_closure->close_type_name ) : '')  ])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>


        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row">
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])


    </div>
    <br/>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>
                    {{--document--}}

                        <li class="nav-item">
                            <a class="nav-link" href="#documents" id="document_header"
                               data-toggle="tab">Document Center
                            </a>
                        </li>



                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >


                                        @if($check_workflow == 0)
                                            <span>
                                           {!! HTML::decode(link_to('#', "<i class=\"icon fa fa-confirm\"></i>&nbsp;Initiate", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Closure Approval Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $employer_closure_open->id, 'data-route' => route('backend.compliance.employer_closure.open.initiate_approval', $employer_closure_open->id)])) !!}
                                            </span>

                                        @endif


                                        @if($check_pending_level1 == 1 || $check_workflow == 0)

                                            <span>
                                           {!! HTML::decode(link_to_route('backend.compliance.employer_closure.open.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $employer_closure_open->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this business open?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                            {{--modify--}}
                                            <a href="{!! route('backend.compliance.employer_closure.open.edit', $employer_closure_open->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Modify</a>

                                        @endif




                                        {{--close--}}

                                        <a href="{!! route('backend.compliance.employer.profile', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/>
                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--general info--}}
                                    @include('backend/operation/compliance/member/employer/closure/open/profile/includes/general')
                                    {{--disease adjust overview--}}
                                    <div>&nbsp;</div>
                                    {{--APprovals tabs history--}}
                                    {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/compliance/member/employer/closure/open/profile/includes/sidebar_summary')
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--documents--}}
                    <div id="documents" class="nav_tab_pane tab-pane">
                        @include('backend/system/document/general/includes/document_center_gen', ['resource_id' => $employer_closure_open->id, 'reference'=> 'DREMCLREOP', 'allow_modify' => (($check_pending_level1 == 1 || $check_workflow == 0) ? true : false)])
                    </div>


                    {{--susendeed payments--}}
                    {{--<div id="suspended_payments" class="nav_tab_pane tab-pane">--}}
                    {{--@include('backend/operation/payroll/pension_administration/profile/includes/monthly_pension_tab/get_monthly_pensions' ,['member_type_id' => 5, 'resource_id' =>--}}
                    {{--$pensioner->id, 'notification_report_id' => $pensioner->notification_report_id])--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

    </div>

    @include("backend.system.workflow.includes.initiate_modal")
    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {


            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });
        });
    </script>;

@endpush
