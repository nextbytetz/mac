@extends('layouts.backend.main', ['title' => 'Business De-Registration Profile', 'header_title' => 'Profile - ' .  $employer_closure->close_type_name])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
<style>


    /* end: upload progress bar css */
    tr {
        border-bottom:1pt solid rgba(0, 0, 0, 0.12);
    }
</style>
@endpush

@section('content')


<div class = "row">
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])


</div>
<br/>
{{--Tabs navigation--}}
<div class = "row">
    <div class="col-md-12">

        <div class="basic_nav_pills nav_basic_tab">
            <ul class="nav nav-tabs">
                {{--General--}}
                <li class="nav-item ">
                    <a class="nav-link active" href="#general"
                    data-toggle="tab">@lang('labels.general.general')
                </a>
            </li>
            {{--document--}}
            <li class="nav-item">
                <a class="nav-link" href="#documents" id="document_header"
                data-toggle="tab">Document Center
            </a>
        </li>
        {{--follow ups--}}
        <li class="nav-item">
            <a class="nav-link" href="#followups" id="follow_ups_header"
            data-toggle="tab">Follow Ups
        </a>
    </li>
    {{--WCP-1   --}}
    @if($employer_closure->closure_type == 1)
    <li class="nav-item">
        <a class="nav-link" href="#wcp" id="wcp_header"
        data-toggle="tab">WCP-1
    </a>
</li>
@endif
{{--suspended_payments--}}
{{--<li class="nav-item">--}}
    {{--<a class="nav-link" href="#suspended_payments" id="suspended_header"--}}
    {{--data-toggle="tab">Suspended Payments--}}
{{--</a>--}}
{{--</li>--}}

</ul>
<div class="nav_tab_contain tab-content">

    <div id="general" class="nav_tab_pane tab-pane active in">
        <div class="nav_tab_pane_header">
            <div class="row ">
                <div class="col-md-12" >

                    <div class="pull-right" >

                        {{--Todo <deploy_extension>--}}
                        @if(env('TESTING_MODE') == 1)
                        @if($employer_closure->checkIfEligibleForExtension($employer_closure->employer_id))
                        {{--Extensions--}}
                        <a href="{!! route('backend.compliance.employer_closure.extension.create', $employer_closure->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Add Extension</a>

                        @endif
                        @endif

                        @if($employer_closure->status_follow_up_eligibility == true)
                        <a target=""  href=" {{ route("backend.compliance.employer.closure.fill_status_followup", $employer_closure->id) }}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Fill Status Follow Up </a>
                        @endif

                        {{--Confirm Payroll at level for letter--}}
                        @if($check_if_can_initiate_wf_letter == true)
                        <a target=""  href=" {{ route("backend.compliance.employer.closure.open_page_confirm_payrolls", [$employer_closure->id]) }}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-list-alt"></i>&nbsp;Confirm Payrolls</a>
                        @endif

                        @if($check_if_can_initiate_wf_letter == true &&  $employer_closure->status_if_letter_initiated == false && $employer_closure->check_if_need_response_letter == true)
                        <a target=""  href=" {{ route("backend.letter.process", [$employer_closure->id, "CLIEMPCLOSURE"]) }}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-file"></i>&nbsp;Response Letter</a>



                        @endif


                        @if($employer_closure->is_legacy == 0)

                        @if($employer_closure->source == 2)
                        @if($employer_closure->online_status == 1)
                        @include('backend/operation/compliance/member/employer/closure/profile/includes/online_button_options')
                        @endif
                        @else
                        @if($check_workflow == 0)
                        <span>
                           {!! HTML::decode(link_to('#', "<i class=\"icon fa fa-confirm\"></i>&nbsp;Initiate", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Closure Approval Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $employer_closure->id, 'data-route' => route('backend.compliance.employer.closure.initiate_approval', $employer_closure->id)])) !!}
                       </span>

                       @endif


                       @if($check_pending_level1 == 1 || $check_workflow == 0)

                       <span>
                           {!! HTML::decode(link_to_route('backend.compliance.employer.closure.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $employer_closure->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this closure?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                       </span>

                       {{--modify--}}
                       <a href="{!! route('backend.compliance.employer.closure.edit', $employer_closure->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Modify</a>

                       @endif
                       @endif
                       @endif

                       {{--close--}}
                       <a href="{!! route('backend.compliance.employer.closure.index', $employer->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>

                   </div>
               </div>
           </div>
       </div>

       <br/>
       {{--main tab content--}}
       <div class = "row">
        <div class="col-md-12">

            <div class="col-md-9">
                {{--general info--}}
                @include('backend/operation/compliance/member/employer/closure/profile/includes/general')
                {{--disease adjust overview--}}
                <div>&nbsp;</div>
                {{--APprovals tabs history--}}
                {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}
            </div>
            <div class="col-md-3">
                {{--sidebar summary--}}
                @include('backend/operation/compliance/member/employer/closure/profile/includes/sidebar/sidebar_summary')
            </div>
        </div>
    </div>
</div>
{{--documents--}}
<div id="documents" class="nav_tab_pane tab-pane">
    @include('backend/operation/compliance/member/employer/closure/profile/includes/document_center')
</div>
{{--followups--}}
<div id="followups" class="nav_tab_pane tab-pane">
    @include('backend/operation/compliance/member/employer/closure/profile/includes/get_closure_follow_ups')
</div>

<div id="wcp" class="nav_tab_pane tab-pane">
    @include('backend/operation/compliance/member/employer/closure/profile/includes/wcp1')
</div>

{{--susendeed payments--}}
{{--<div id="suspended_payments" class="nav_tab_pane tab-pane">--}}
    {{--@include('backend/operation/payroll/pension_administration/profile/includes/monthly_pension_tab/get_monthly_pensions' ,['member_type_id' => 5, 'resource_id' =>--}}
    {{--$pensioner->id, 'notification_report_id' => $pensioner->notification_report_id])--}}
{{--</div>--}}
</div>
</div>
</div>

</div>

@if($employer_closure->source == 2)
@include('backend/operation/compliance/member/employer/closure/profile/includes/online_remarks_modal')
@endif

@include("backend.system.workflow.includes.initiate_modal")
{{--{!! Form::close() !!}--}}
{{-- {!! route('backend.compliance.employer.closure.edit', $employer_closure->id) !!} --}}
@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
<script  type="text/javascript">


    $(function () {


        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });
</script>;

@endpush
