
@push('after-styles-end')
    {{ Html::style(asset_url() . "/global/plugins/jstree/dist/themes/default/style.css", ['rel' => 'stylesheet']) }}
    {{--{{ Html::style(asset_url() . "/global/plugins/material-design-iconic-font/dist/css/material-design-iconic-font.min.css", ['rel' => 'stylesheet']) }}--}}
{{----}}

@endpush


<div class = "row">
    <div class="col-md-12">

        <div class="col-md-4">

            <legend>Documents Attached
                {{--@if($check_pending_level1 == 1 || $check_workflow == 0)--}}
                <a class="pull-right" style="color:blue; font-size: 12px"  href="{!! route('backend.compliance.employer.closure.attach_wcp1_document', $employer_closure->id) !!}">{!! 'Add Document' !!}</a>
                {{--@endif--}}
            </legend>

            <br/>

            {{--new Bank--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="element-form" >

                        {{--<ul>--}}
                        {{--@foreach($wcp_attached as $doc)--}}
                            {{--<li>--}}
                            {{--<i class="fa fa-file-pdf-o" ></i>--}}
                            {{--<a  style="color:dodgerblue;" class="wcp_attached"  href="#" id="{!! 'doc'. $doc->pivot->id !!}">{!! $doc->name !!}</a>--}}
                            {{--@if($check_pending_level1 == 1 || $check_workflow == 0)--}}
                            {{--|--}}

                            {{--<a class="" style="color:grey"  href="{!! route('backend.compliance.employer.closure.edit_document', $doc->pivot->id) !!}">{!! 'Edit' !!}</a>--}}
                            {{--<span style="font-size: 12px; color:#414141"> {!!   '' . ' : ' . $pollOption->votes  !!}   </span>--}}
                            {{--<span style="font-size: 12px; color:#414141"> {!!   $pollOption->vote_percent_label  !!} </span>--}}
                            {{--</li>--}}
                            {{--@endif--}}
                            {{--<br/>--}}
                        {{--@endforeach--}}
                        {{--</ul>--}}






                        <div id="">


                                @foreach(array_reverse($wcp1_months) as $month)

                                        {{--<ul>--}}

                                            @if(($employer_closure->getWcp1DocAttachedByMonth($month)->isNotEmpty()))
                                            @foreach($employer_closure->getWcp1DocAttachedByMonth($month) as $doc)
                                                {{--<li>--}}
                                                <i class="fa fa-file-pdf-o" ></i>
                                                <a  style="color:dodgerblue;" class="wcp_attached"  href="#" id="{!! 'doc'. $doc->pivot->id !!}">{!! $month !!}</a>
                                                {{--@if($check_pending_level1 == 1 || $check_workflow == 0)--}}
                                                |

                                                <a class="" style="color:grey"  href="{!! route('backend.compliance.employer.closure.edit_document', $doc->pivot->id) !!}">{!! 'Edit' !!}</a>

                                                {{--</li>--}}
                                                {{--@endif--}}
                                                <br/>
                                            @endforeach

                                                @else

                                                {{--<li>--}}
                                                    <i class="fa fa-file-pdf-o" ></i>
                                                    <a  style="color:grey;" class=""  href="#" id="{!! 'doc'!!}">{!! $month !!}</a>

                                                {{--</li>--}}
                                                {{--@endif--}}
                                                <br/>
                                            @endif
                                        {{--</ul>--}}

                                @endforeach


                        </div>





                    </div>

                </div>
            </div>









        </div>




        <div class="col-md-8">
            <div class = "row">
                <div class="col-md-12">
                    {{--Document Preview--}}
                    <legend>Document Preview</legend>
                    <br/>
                    <div id="document_frame_wcp" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>





@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

    {{ Html::script(asset_url(). "/global/plugins/jstree/dist/jstree.min.js") }}
    {{ Html::script(asset_url(). "/global/js/global/treeview.js") }}

    <script  type="text/javascript">


        $(function () {

            $(".search-select").select2();


            /*Documents which pending to be used list*/
            $(".wcp_attached").click(function() {
                var $doc_id = this.id;
                     var $pivot_id = $doc_id.substr(3);
                let $document_frame = $("#document_frame_wcp");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });






            function get_current_document($doc_pivot_id) {

                return $.ajax({
                    url: base_url + "/compliance/employer/closure/preview/document/"+ $doc_pivot_id,
                    dataType : 'json',
                    async : false,
                    method : "POST"
                });
            }


        });
    </script>;

@endpush
