@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_new_follow_up'), 'header_title' => trans('labels.backend.member.add_new_follow_up')])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.employer.closure.follow_up.store'], 'name' => 'employer_close_business', 'class' => 'employer_close_business', 'method' => 'post','enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('employer_closure_id', $employer_closure->id, ['class' =>'']) !!}
    {!! Form::hidden('close_date', $employer_closure->close_date, ['class' =>'']) !!}
    {!! Form::hidden('isstatus_followup', $isstatus_followup, ['class' =>'']) !!}
    {!! Form::hidden('action_type', 1, ['class' =>'']) !!}
    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
    <br/>

{{--<div>--}}

    <br/>
{{--</div>--}}

    {{--Create form--}}
    @include("backend/operation/compliance/member/employer/closure/follow_up/create/includes/input_form",['employer_closure'=>$employer_closure])

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.employer.closure.profile',trans('buttons.general.cancel'), [$employer_closure->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}



    <script>
        $(function () {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });


        });

        // Follow up type option
        function follow_up_type_option(follow_type_id, contact_div, contact_id) {

            var choice = $("#" + follow_type_id).val();
//            if is visit
            if (choice == 93 || choice == 410) {
                $("#" + contact_div).hide();
                $('#doc_form_div').show();
            } else {
//            phone call
                if (choice == 91) {
                    $("#" + contact_id).attr('placeholder','Phone #');
                }else if(choice == 92){
                    $("#" + contact_id).attr('placeholder','Email@yahoo.com');
                }
                $("#" + contact_div).show();
                $('#doc_form_div').hide();

            }
        }

    </script>;


@endpush