<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Pending Tasks</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

@if($employer_closure->is_legacy == 0)
    <div class="row">
        <div class="light_grey_bg">&nbsp;</div>

        <div class="light_grey_bg">
            {{--<table style="width:100%">--}}
            {{--@if(!$check_if_first_payroll)--}}
            {{--new Pensioners--}}
            {{--<tr>--}}
            {{--<td style="padding-left: 5px" width="140px">New Pensioners <i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="New pensioners to be paid for the first time on--}}
            {{--this payroll"></i>:</td>--}}
            {{--<td  height="20px"> <b>{!! $data_summary['new_pensioners'] !!}</b></td>--}}
            {{--<td > <a href="#" data-toggle="modal" data-target="#new_pensioners_modal" id="new_pensioners_link"   style="color: dodgerblue" class="pull-center underline"> View </a> </td>--}}
            {{--</tr>--}}



            {{--</table>--}}
            <ol>
                @foreach($docs_pending as $doc)

                    <li style="margin-left:10px;">
                        {!! $doc->name . ' not attached' !!}

                    </li>


                @endforeach

                @if($employer_closure->status_if_letter_initiated == false &&  $check_if_can_initiate_wf_letter == true && $employer_closure->check_if_need_response_letter == true)
                        <li style="margin-left:10px;">
                            {!! 'Response letter not initiated yet ' !!}

                        </li>

                @endif

                    @if( $check_if_can_initiate_wf_letter == true && $employer_closure->ispayroll_confirmed == 0)
                        <li style="margin-left:10px;">
                            {!! 'Payroll(s) not yet confirmed!' !!}

                        </li>

                    @endif

            </ol>
        </div>
    </div>
@endif