<div class="modal hide fade" id="online_remarks_modal" role="dialog" aria-labelledby="online_remarks_modal" aria-hidden="true">
    <div class="modal-dialog white_modal" role="document">
        <div class="modal-content" id="modal-content">
            @include('backend/operation/compliance/member/employer/closure/profile/includes/online_remarks_form')
        </div>
    </div>
</div>

@push('after-script-end')
<script  type="text/javascript">
    $(function() {
        var $body = $('body');
        $body.on('click', 'button#reverse_button', function ($e) {
            $e.preventDefault();
            /*alert($(this).attr("data-description"));*/
            var $form = $('form[name=online_remarks_form]');
            var $modal = $('#online_remarks_modal');
            $form.attr('action', $(this).attr("data-route"));
            $form.find("input[name^='online_remarks']").val('');
            $modal.modal('show');
        });
        $body.on('submit', 'form[name=online_remarks_form]', function(e) {
            e.preventDefault();
            var $form = this;
            swal({
                title: "Warning",
                text: "This will reverse to online User! Are you sure you want to continue?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function (confirmed) {
                if (confirmed) {
                    $form.submit();
                }
            });
        });
    });
</script>
@endpush