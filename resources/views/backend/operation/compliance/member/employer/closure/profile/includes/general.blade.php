
{{-- summary--}}
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Closure Details- {!! $employer_closure->status_label !!} </legend>
            <table class="table table-striped table-bordered">
                <tbody>

                {{--close date--}}
                <tr>
                    <td width="140">Close Date</td>
                    <th>{!! Form::label( 'close_date',
                   $employer_closure->close_date_formatted, [])
                                !!} </th>
                </tr>

                @if(isset($employer_closure->application_date))
                    <tr>
                        <td width="120px">Application Date</td>
                        <th>{!! Form::label( 'application_date',
                short_date_format($employer_closure->application_date), [])
                                !!} </th>
                    </tr>
                @endif

                @if(isset($employer_closure->specified_reopen_date))
                    <tr>
                        <td width="120px">Expected Re-open Date</td>
                        <th>{!! Form::label( 'specified_reopen_date',
                short_date_format($employer_closure->specified_reopen_date), [])
                                !!} </th>
                    </tr>
                @endif

                {{--Approved  date--}}
                @if($employer_closure->wf_done == 1)
                    <tr>
                        <td>Approved Date</td>
                        <th>{!! Form::label( 'approved_date',
                   $employer_closure->approved_date_formatted, [])
                                !!} </th>
                    </tr>
                @endif

                {{--open  date--}}
                @if(($employer_closure->open()->count() > 0))
                    <tr>
                        <td width="120px">Re-Open Date</td>
                        <th>{!! Form::label( 'open_date',
                   $employer_closure->open_date_formatted, [])
                                !!} @if($employer_closure->open()->count() > 0) <a style="color: blue;" href="{!! route('backend.compliance.employer_closure.open.profile', $employer_closure->open->id) !!}">Business Open Approval - Click to view</a> @endif  </th>
                    </tr>
                @endif


                {{--closure type--}}
                <tr>
                    <td>Closure Type</td>
                    <th>
                        {!!   $employer_closure->close_type_name
                                    !!}</th>
                </tr>

                @if(isset($employer_closure->sub_type_cv_id))
                    <tr>
                        <td>Closure Sub Type</td>
                        <th>
                            {!!   $employer_closure->subType->name
                                        !!}</th>
                    </tr>
                @endif

                @if(isset($employer_closure->dormant_source_cv_id))
                    <tr>
                        <td>Authority Dormant Source</td>
                        <th>
                            {!!   $employer_closure->dormantSource->name
                                        !!}</th>
                    </tr>

                    @if(isset($employer_closure->other_dormant_source))
                        <tr>
                            <td>Other Authority Dormant Source</td>
                            <th>
                                {!!   $employer_closure->other_dormant_source
                                            !!}</th>
                        </tr>
                    @endif

                    <tr>
                        <td>Last Contribution</td>
                        <th>
                            {!!   number_2_format($employer_closure->last_contribution)
                                        !!}</th>
                    </tr>

                @endif

                {{--Reason--}}
                <tr>
                    <td>Reason</td>
                    <td>
                        {!! $employer_closure->reason
                                    !!}</td>
                </tr>

                {{--ARrears--}}
                <tr>
                    <td>{!! $employer_closure->isdomestic == 0 ? 'Arrears' : 'Amount for Refund i.e. Total contribution' !!}</td>
                    <td>
                        {!! number_2_format($employer_closure->arrears)
                                    !!}</td>
                </tr>
                @if($employer_closure->isdomestic == 0)
                    {{--MIssing months--}}
                    <tr>
                        <td>Missing months</td>
                        <td>
                            {!! number_0_format($employer_closure->missing_months)
                                        !!}</td>
                    </tr>
                @endif

                @if($employer_closure->isdomestic == 0)
                    {{--Interest--}}
                    <tr>
                        <td>Interests</td>
                        <td>
                            {!! number_2_format($employer_closure->interest_amount)
                                        !!}</td>
                    </tr>
                @endif

                @if($employer_closure->payroll_ids)
                    {{--Interest--}}
                    <tr>
                        <td>Payroll(s)</td>
                        <td>
                            {!! ($employer_closure->payroll_ids)
                                        !!}</td>
                    </tr>
                @endif

                {{--Domestic closure--}}
                @if($employer_closure->isdomestic == 1)
                    <tr>
                        <td>Domestic closure</td>
                        <td>
                            {!! ($employer_closure->isdomestic == 1) ? 'Yes' : 'No'
                                        !!}</td>
                    </tr>
                @endif


                {{--Leter--}}
                @if($employer_closure->closureLetter()->count() > 0)
                    <tr>
                        <td>Letter</td>
                        <td>
                            <a style="color:blue" href="{{ route('backend.letter.show', $employer_closure->closureLetter->id) }}" >{{ $employer_closure->closureLetter->reference }}</a></td>
                    </tr>
                @endif

                @if($employer_closure->closureExtensions()->count() > 0)
                <tr>
                    <td >Extensions</td>
                    <td>
                        <ul>
                            @foreach($employer_closure->closureExtensions()->orderBy('employer_closure_extensions.id', 'desc')->get() as $closureExtension)
                                <li>
                                    <a style="color: blue;" href="{!! route('backend.compliance.employer_closure.extension.profile', $closureExtension->id) !!}">{{ 'Applied on: ' . short_date_format($closureExtension->application_date)  }}</a>
                                </li>
                            @endforeach
                        </ul>


                    </td>
                </tr>
                @endif
                {{--created by--}}
                <tr>
                    <td>Created by</td>
                    <td>
                        {!! $employer_closure->user->username
                                    !!}</td>
                </tr>

                @if(isset($employer_closure->followup_outcome))
                    <tr>
                        <td>Last Status Follow up Date</td>
                        <td>
                            {!! short_date_format($employer_closure->followup_ref_date) .' by ' . $employer_closure->followupUser->username
                                        !!}</td>
                    </tr>
                @endif


                </tbody>
            </table>

        </div>
    </div>





</div>

@push('after-script-end')

    <script  type="text/javascript">


        $(function () {


        });
    </script>;

@endpush
