@extends('layouts.backend.main', ['title' => 'Create Extension' , 'header_title' => 'Extension'])

@include('backend/includes/assets/datetimepicker')

@push('after-styles-end')
    <style>
    </style>
@endpush

@section('content')

    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer_closure->closedEmployer])
    {{ Form::open(['route' => 'backend.compliance.employer_closure.extension.store', 'autocomplete' => 'off','method' => 'post', 'name' => 'create', 'class' => 'needs-validation', 'novalidate' , ]) }}
    {{ Form::hidden('employer_closure_id', $employer_closure->id, []) }}
    {{ Form::hidden('prev_reopen_date', $employer_closure->specified_reopen_date, []) }}
    {{ Form::hidden('action_type', 1, []) }}
    {{ Form::hidden('today', getTodayDate(), []) }}
    {{--<p>{!!'Fields marked with <span style="color: red;">*</span> are mandatory'!!}</p>--}}
    <div class="row">
        <div class="col-md-3">
            <div class="form-group ">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('application_date', 'Application Date', ['class' =>'required']) }}
                                <div class="input-group">
                                    {{ Form::text('application_date',null,['class'=>'form-control datepicker2', 'required', 'id' => 'application_date','placeholder' => '', 'autocomplete' => 'off']) }}
                                    <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                                </div>

                                {!! $errors->first('application_date', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('new_reopen_date', 'New Expected Reopen Date', ['class' =>'']) }}
                                <div class="input-group">
                                    {{ Form::text('new_reopen_date',null,['class'=>'form-control datepicker2', 'required', 'id' => 'new_reopen_date','placeholder' => '', 'autocomplete' => 'off']) }}
                                    <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                                </div>

                                {!! $errors->first('new_reopen_date', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group text_content">
                                {{ Form::label('remark', 'Remark', ['class' =>'']) }}
                                {{ Form::textarea('remark',null,['class'=>'form-control', 'id' => 'remark','placeholder' => '', 'autocomplete' => 'off']) }}
                                {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-3">
            <div class="element-form">
                <div class="form-group pull-right">
                    {{ link_to_route('backend.compliance.employer.closure.profile',trans('buttons.general.cancel'),[$employer_closure->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) }}
                    {{ Form::button(trans('buttons.general.submit'), ['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit', 'style' => 'border-radius: 5px;']) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@push('after-script-end')
    <script>
        $(function() {
        });
    </script>
@endpush
