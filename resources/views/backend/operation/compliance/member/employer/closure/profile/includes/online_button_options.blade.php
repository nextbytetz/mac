   @if(($check_workflow == 0 || $check_pending_level1 == 1))
   <button class="btn btn-primary site-btn" id="reverse_button" ><i class="icon fa fa-backward"></i>&nbsp;Reverse</button>
   @endif

   @if($check_workflow == 0)
   <span>
     {!! HTML::decode(link_to('#', "<i class=\"icon fa fa-confirm\"></i>&nbsp;Initiate", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Closure Approval Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $employer_closure->id, 'data-route' => route('backend.compliance.employer.closure.initiate_approval', $employer_closure->id)])) !!}
   </span>
   @endif