@extends('layouts.backend.main', ['title' => trans('labels.backend.home'), 'header_title' => "Associate Closed Business from TRA"])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- Employer header detail -->
                {{--<span>{!! $employer->status_label !!} </span>--}}

                <strong> {!! Form::label( 'name', $closed_business->name, [ 'id'=> 'name']) !!}</strong>

                <small >
                    TIN #: {!! Form::label( 'reg_no', $closed_business->tin) !!}
                </small>
                <small>
                    Deregistration Date : {!! Form::label( 'deregistration_date', $closed_business->deregistrationdate_label) !!}
                </small>
            </h5>
            <legend></legend>
        </div>

        <div class="col-md-12 col-sm-12">
            {!! Form::open(['route' => ['backend.compliance.closed_business.associate', $closed_business->id]]) !!}
            <p>
                Search Employer from the existing employer database to associate with this employer record from TRA.
            </p>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="grid-column">
                        {!! Form::select('employer', [], null, ['class' => 'employer-select', 'style' => 'width:100%']) !!}
                        {!! $errors->first('employer', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {!! Form::submit(trans('labels.general.continue'), ['class' => 'btn btn-success btn-save btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script>
    $(function () {
        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
    });
</script>
@endpush