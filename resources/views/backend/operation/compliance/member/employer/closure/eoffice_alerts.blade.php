@extends('layouts.backend.main', ['title' => 'Employer Business Closures from E-office', 'header_title' => 'Employer Business Closures from E-office'])

@include('backend.includes.datatable_assets')

@section('content')



    <br/>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="employer-closures-eoffice-table">
                <thead>
                <tr >
                    <th>From</th>
                    <th>Subject</th>
                    <th>Document date</th>
                    <th>Doc Receive Date</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#employer-closures-eoffice-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.closure.get_pending_eoffice_for_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'from' , name: 'from', orderable : true, searchable : true},
                    { data: 'subject', name: 'subject', orderable : true, searchable : true},
                            { data: 'doc_date', name: 'doc_date',  orderable : true, searchable : true},
                    { data: 'doc_receive_date', name: 'doc_receive_date',  orderable : true, searchable : true},
                ],

                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/closure/open_sync_closure_employer_page/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
