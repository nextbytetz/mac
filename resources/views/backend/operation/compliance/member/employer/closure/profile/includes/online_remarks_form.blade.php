<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="modal-title">Reverse Remarks</h4>
</div>
<div class="modal-body" style="font-size: 14px !important;">
    {!! Form::open(['url' => route('backend.compliance.employer.closure.reverse_online', $employer_closure->id), 'name' => 'online_remarks_form', 'id' => 'online_remarks_form']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="field-layout">
                <div class="form-group">
                    <label for="online_remarks">Remarks</label>
                    {!! Form::textarea('online_remarks', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px;', 'id' => 'online_remarks']) !!}
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn  site-btn btn-secondary" data-dismiss="modal">@lang("buttons.general.close")</button>
        <button type="submit" class="btn btn-primary btn-submit">Continue</button>
    </div>
    {!! Form::close() !!}
</div>