
{{--Interest table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="interest-table">
            <thead>
            <tr>
                <th>@lang('labels.backend.table.receipt.contrib_month')</th>
                <th>@lang('labels.backend.table.booking.late_months')</th>
                <th>@lang('labels.backend.finance.receipt.interest')</th>
                <th>@lang('labels.backend.table.amount_paid')</th>
                <th>@lang('labels.general.balance')</th>
                <th>@lang('labels.backend.table.contribution_status')</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="2" style="text-align:right">@lang('labels.backend.table.page_total'):</th>
                <th></th>
                {{--<th colspan="2" style="text-align:right">Total:</th>--}}
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
{{--SUMMARY--}}
<table class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>{!! strtoupper(trans('labels.general.total')) !!}:</th>
        <th>@lang('labels.backend.table.interest')</th>
        <th>{!! $interest_summary['total_interest'] !!}</th>
        <th>@lang('labels.backend.table.amount_paid')</th>
        <th>{!! $interest_summary['total_paid'] !!}</th>
        <th>@lang('labels.general.balance')</th>
        <th>{!! $interest_summary['total_remain'] !!}</th>
    </tr>
    </thead>
</table>
@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            $("#interests_header").one("click", function(){

                $('#interest-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.compliance.employer.interests', $employer->id) !!}',
                        type : 'get'
                    },
                    columns: [
                        { data: 'contrib_month_formatted' , name: 'miss_month' },
                        { data: 'late_months', name: 'late_months' },
                        { data: 'amount', name: 'amount' },
                        { data: 'amount_paid', name: 'amount_paid' },
                        { data: 'remain_amount', name: 'remain_amount' },
                        { data: 'status', name: 'status' },
                    ],

                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = "interest/" + aData['id'] ;
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    },

                    footerCallback: function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                            .column( 2 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Update footer
                        function commaSeparateNumber(val){
                            while (/(\d+)(\d{3})/.test(val.toString())){
                                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                            }
                            return val ;
                        }

                        $( api.column( 2 ).footer() ).html(
                            ''+ commaSeparateNumber(total.toFixed(2))
                        );

//                    get Total for amount paid

                        // Total over all pages
                        total = api
                            .column( 3 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
                        $( api.column( 3 ).footer() ).html(
                            '' + commaSeparateNumber(total.toFixed(2))
                        );

//                    get Total for amount remained

                        // Total over all pages
                        total = api
                            .column( 4 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
                        $( api.column( 4 ).footer() ).html(
                            '' + commaSeparateNumber(total.toFixed(2))
                        );

                    }
                } );

            });

        });
    </script>;

@endpush