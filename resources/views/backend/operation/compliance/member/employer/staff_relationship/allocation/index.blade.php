@extends('layouts.backend.main', ['title' => 'Staff Employer Allocations', 'header_title' => 'Staff Employer Allocations'])

@include('backend.includes.datatable_assets')

@section('content')


    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="allocations-table">
                <thead>
                <tr >
                    <th>Title</th>
                    <th>No of employers</th>
                    <th>No of staff</th>
                    <th>Start_date</th>
                    <th>End Date</th>
                    <th>Approval Status</th>
                    <th>Run Status</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#allocations-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.staff_relation.get_allocations_for_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'title', name: 'title', orderable : false, searchable : false},
                    { data: 'no_of_employers' , name: 'no_of_employers', orderable : false, searchable : false},
                    { data: 'no_of_staff', name: 'close_date',  orderable : false, searchable : false},
                    { data: 'start_date_formatted', name: 'start_date',  orderable : true, searchable : true},
                    { data: 'end_date_formatted', name: 'end_date' ,orderable : true, searchable : true},
                    { data: 'approval_status', name: 'approval_status' ,orderable : false, searchable : false},
                    { data: 'run_status', name: 'run_status' ,orderable : false, searchable : false},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/staff_relation/allocation_profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
