@extends('layouts.backend.main', ['title' => 'Employers Debt Management', 'header_title' => 'Employers Debt Management'])


@section('content')

    <div style="color:#fff">
        {{--left div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">

                {{--item 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.configuration') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-cog"> </i><large>&nbsp;&nbsp;Configure
                                    Allocation Parameters  </large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Configure allocation parameters</p> </li>
                    </a>

                </ul>

                <br/>
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.allocations') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;All Staff Employer Allocations</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">View all Allocations of staff-employer </p> </li>
                    </a>

                </ul>

                <br/>
                {{----}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.create_allocation_large') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;Allocate Large employers to staff</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Allocate large employers</p> </li>
                    </a>

                </ul>

                <br/>
                {{--Item1 1--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.create_allocation') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;Allocate Non Large employers to staff
                                </large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Allocate non large employers to staff</p> </li>
                    </a>

                </ul>

                {{--item 2--}}




                <br/>
                {{----}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.create_allocation_bonus') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;Allocate Bonus employers to staff</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Allocate staff to dormant / bonus employers</p> </li>
                    </a>

                </ul>


                <br/>
                {{----}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.create_allocation_treasury') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;Allocate Treasury employers to staff</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Allocate staff to treasury employers</p> </li>
                    </a>
                </ul>

                <br/>
                {{----}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.create_allocation_intern') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;Allocate Employers to Intern staff</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Allocate employers to Intern Staff</p> </li>
                    </a>
                </ul>



                <br/>
                {{----}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.create_allocation_staff_region') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-users"> </i><large>&nbsp;&nbsp;Configure staff for regions</large></h6>
                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Configure staff for regions</p> </li>
                    </a>
                </ul>
                <br/>
                @if(env('TESTING_MODE') == 1)
                    &nbsp; <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.employer.staff_relation.open_test_simulation') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tachometer"> </i><large>&nbsp;&nbsp;Test Simulation</large></h6>
                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">For Test Only (UAT)</p> </li>
                        </a>

                    </ul>
                @endif

            </div>
        </div>


        {{--right div--}}
        <div class="col-sm-6 col-md-6">
            <div class="list-group">


                @permission('staff_employer_assign')
                {{--Item 2--}}
                <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.reallocation_page') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-reply-all"> </i><large>&nbsp;&nbsp;Re-allocate staff to employers</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Re-allocate staff to employers</p> </li>
                    </a>

                </ul>
                @endauth

                @permission('staff_employer_approve_allocation')
                {{--Item 2--}}
                &nbsp;   <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.replace_page') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-reply-all"> </i><large>&nbsp;&nbsp;Replace /Remove employers</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Replace/ Remove employers</p> </li>
                    </a>

                </ul>
                @endauth

                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.pending_follow_ups_for_review') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-check"> </i><large>&nbsp;&nbsp;Follow Ups pending for Review</large> @if($pending_follow_ups > 0)  <label class="label label-info text-white">&nbsp; {!! $pending_follow_ups !!} &nbsp; </label> @endif</h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Retrieve all follow ups pending for review. i.e. follow ups (visitation) of employers</p> </li>
                    </a>

                </ul>

                @permission('review_staff_employer_followup')
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.assigned_pending_for_review') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-check"> </i><large>&nbsp;&nbsp;Assigned - Follow Ups for Review</large> @if($my_pending_follow_ups > 0)  <label class="label label-info text-white">&nbsp; {!! $my_pending_follow_ups !!} &nbsp; </label> @endif</h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Retrieve all assigned follow ups pending for review. i.e. follow ups (visitation) of employers</p> </li>
                    </a>

                </ul>
                @endauth

                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.reversed_for_correction') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-check"> </i><large>&nbsp;&nbsp; Follow Ups reversed for Correction</large> @if($my_pending_follow_ups_for_correction > 0)  <label class="label label-info text-white">&nbsp; {!! $my_pending_follow_ups_for_correction !!} &nbsp; </label> @endif</h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Retrieve all reversed follow ups pending for correction. i.e. follow ups (visitation) of employers</p> </li>
                    </a>


                         &nbsp; <ul class="list-unstyled">
                        <a href="{!! route('backend.compliance.employer.staff_relation.followup.staff_missing_target') !!}">
                            <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-check"> </i><large>&nbsp;&nbsp;Staff Follow up target Missing</large></h6>

                                <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding"> Staff Follow up target missing reminders</p> </li>
                        </a>

                    </ul>

                {{--Item 3--}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[access()->id(), 0, 0]) !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-building"> </i><large>&nbsp;&nbsp;My Employers</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">My employers - Allocated employers for debt management</p> </li>
                    </a>

                </ul>

                {{--Item 3--}}
                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.all_employers_for_active_allocations') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-building"> </i><large>&nbsp;&nbsp;All Allocated Employers</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">All allocated employers - Allocated employers for debt management</p> </li>
                    </a>

                </ul>





                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.performance_dashboard') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tachometer"> </i><large>&nbsp;&nbsp;Staff Performance Dashboard</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Staff contribution collection performance</p> </li>
                    </a>

                </ul>



                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.performance_dashboard.bonus_employers') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tachometer"> </i><large>&nbsp;&nbsp;Staff Bonus Performance Dashboard</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Staff Bonus performance</p> </li>
                    </a>

                </ul>


                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.performance_dashboard.treasury_employers') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tachometer"> </i><large>&nbsp;&nbsp;Staff Treasury Performance Dashboard</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Staff Treasury performance</p> </li>
                    </a>
                </ul>


                &nbsp; <ul class="list-unstyled">
                    <a href="{!! route('backend.compliance.employer.staff_relation.performance_intern_dashboard') !!}">
                        <li  class="border-less" class="list-group-item" >  <h6 class="list-group-item-heading ng-binding"><i class="icon fa fa-tachometer"> </i><large>&nbsp;&nbsp;Staff Performance Dashboard - Intern</large></h6>

                            <p id = "bottom_border_custom"  style="color:grey;" class="list-group-item-text ng-binding">Staff contribution collection performance for Intern</p> </li>
                    </a>

                </ul>



            </div>
        </div>


    </div>





@stop

@push('after-script-end')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             $("#site-header-title").hide();
             */
            $("#preview").click(function() {

            });
        });
    </script>;

@endpush
