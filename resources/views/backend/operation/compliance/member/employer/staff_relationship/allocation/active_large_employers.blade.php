@extends('layouts.backend.main', ['title' => 'Allocate Active Large Contributors', 'header_title' => 'Allocate Active Large Contributors'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.store_allocation_large'], 'name' => 'employer_close_business', 'class' => 'attach_doc', 'method' => 'post','enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {{--{!! Form::hidden('staff_employer_config_id', $staff_employer_config->id, ['class' =>'']) !!}--}}
    {!! Form::hidden('end_of_last_allocation', $cut_off_date, ['class' =>'']) !!}

    {{--HEADER--}}



    {{--document types--}}    {{--<div>--}}

    {{--<br/>--}}
    {{--</div>--}}

    {{--<br/>--}}

    {{--Attach file input--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required "><label>Start date:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                        <div class="form-inline">

                            <div class="input-group" style="width:100%;">
                                {!! Form::text('start_date',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            <small class="help-block">
                                Start date of period for this relation allocation
                            </small>


                        </div>

                        {!! $errors->first('start_date', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required "><label>End date:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="form-inline">

                            <div class="input-group" style="width:100%;">
                                {!! Form::text('end_date',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            <small class="help-block">
                                End date of period for this relation allocation
                            </small>

                            {!! $errors->first('end_date', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--no of employers--}}
    {{--<div class="row" id="">--}}
    {{--<div class="col-md-9">--}}
    {{--<div class="element-form" >--}}
    {{--<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>No of employers:</label></div>--}}
    {{--<div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">--}}
    {{--<div class="row">--}}
    {{--<div class="form-group">--}}
    {{--{!! Form::text('no_of_employers', $no_of_employers, ['placeholder' => '', 'class' => 'form-control number', 'autocomplete' => 'off', 'id' => 'non_large_employers_per_staff']) !!}--}}
    {{--<small class="help-block">--}}
    {{--No of employers target to be allocated--}}
    {{--</small>--}}
    {{--{!! $errors->first('no_of_employers', '<span class="help-block label label-danger">:message</span>') !!}--}}
    {{--</div>--}}

    {{--</div>--}}
    {{--</div>--}}

    {{--</div>--}}
    {{--</div>--}}



    {{--no of staff--}}
    {{--<div class="row" id="">--}}
    {{--<div class="col-md-9">--}}
    {{--<div class="element-form" >--}}
    {{--<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>No of staff:</label></div>--}}
    {{--<div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">--}}
    {{--<div class="row">--}}
    {{--<div class="form-group">--}}
    {{--{!! Form::text('no_of_staff', $no_of_staff, ['placeholder' => '', 'class' => 'form-control number', 'autocomplete' => 'off', 'id' => 'non_large_employers_per_staff']) !!}--}}
    {{--<small class="help-block">--}}
    {{--No of compliance staff target to be allocated--}}
    {{--</small>--}}
    {{--{!! $errors->first('no_of_staff', '<span class="help-block label label-danger">:message</span>') !!}--}}
    {{--</div>--}}

    {{--</div>--}}
    {{--</div>--}}

    {{--</div>--}}
    {{--</div>--}}


    {{--For large employers--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Staff:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('staff[]', $compliance_staff, $compliance_staff_ids, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'staff', 'multiple']) !!}

                        {!! $errors->first('staff', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    <br/>

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.employer.staff_relation.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}


    <script>
        $(function () {

            $(".search-select").select2({});

            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);
            });


            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                // minDate: today_date,
            });
            /*-----------End Date Process------------*/


            /* start : ensure only numbers are input on monetary boxes */
            function number_only(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });

    </script>;


@endpush






