@extends('layouts.backend.main', ['title' => 'Staff Bonus Performance', 'header_title' => 'Staff Bonus Performance'])

@include('backend.includes.datatable_assets')

@section('content')


    {{--</div>--}}
    {{--</div>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="alert-left-border">
                <div class="alert alert-primary alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Note.</strong> Performance is based on number of employers converted to active status from dormant rational to allocated employers. Employer become active when they start contributing.
                </div>
            </div>
        </div>
    </div>
    <br/>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
                <tbody class="tbody_per_region table_display_block" >

                <tr style="color: white; background-color: lightskyblue" >
                    <th>Sn</th>
                    <th>Staff</th>
                    <th>No of Employers Allocated</th>
                    <th>No of Employers Activated</th>
                    <th>Performance %</th>
                    <th>Action</th>
                </tr>



                <?php
                $i = 1;
                ?>

                @foreach($staff_performances as $staff)

                    <tr>
                        <td>{!! $i   !!}</td>
                        <td>{!! $staff->fullname !!}</td>
                        <td style="text-align:right;">{!! number_0_format($staff->allocated_employers) !!}</td>
                        <td style="text-align:right;">{!! number_0_format( $staff->activated_employers)  !!}</td>
                        <th style="text-align:right;">{!! number_2_format( $staff->performance ) . ' % '!!}</th>
                        <th><a style="color:blue"  href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations', [$staff->user_id, 1,0]) !!}" >View</a></th>
                    </tr>

                    <?php
                    $i++;
                    ?>

                @endforeach


                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td></td>
                    <td>TOTAL</td>
                    <th style="text-align:right;">{!! number_0_format($total_summary->total_allocated_employers) !!}</th>
                    <th style="text-align:right;">{!! number_2_format( $total_summary->total_activated_employers)  !!}</th>
                               <th style="text-align:right;">{!! (($total_summary->total_allocated_employers > 0) ? number_2_format(($total_summary->total_activated_employers * 100)/($total_summary->total_allocated_employers)) : 0) . ' %' !!}</th>
                    <th></th>
                </tr>
                </tbody>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";


        });


    </script>;

@endpush
