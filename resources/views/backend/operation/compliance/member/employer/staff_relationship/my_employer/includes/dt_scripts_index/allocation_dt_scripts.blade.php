


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var  with_contact = '{!! $with_contact !!}';
            var  dt_script_id = '{!! $dt_script_id !!}';
            var url = "{!! url("/") !!}";
            $('#allocations-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,

                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{

                    url :getRouteForDt(dt_script_id),
                    type : 'post'
                },
                columns: [
                    {data: 'staff', name: 'main.users.firstname', searchable: true, orderable: true},
                    {data: 'employer', name: 'main.employers.name', searchable: true, orderable: true},
                    {data: 'region_name', name: 'main.regions.name', searchable: true, orderable: true},
                        @if($with_contact == 1)
                    {data: 'contact_person', name: 'contact_person', searchable: false, orderable: false},
                    {data: 'contact_email', name: 'contact_email', searchable: false, orderable: false},
                    {data: 'contact_phone', name: 'contact_phone', searchable: false, orderable: false},
                        @endif
                    {data: 'allocation_start_date', name: 'main.staff_employer.start_date', searchable: true, orderable: true},
                    {data: 'allocation_end_date', name: 'main.staff_employer.end_date', searchable: true, orderable: true},
                    {data: 'contributing_category', name: 'contributing_category', searchable: false, orderable: true},
                    // {data: 'receivable_amount', name: 'receivable_amount', searchable: true, orderable: true},
                    {data: 'missing_months', name: 'missing_months', searchable: false, orderable: true},
                    {data: 'no_of_follow_ups', name: 'no_of_follow_ups', searchable: true, orderable: true},
                    {data: 'attendance_status', name: 'attendance_status', searchable: false, orderable: true},
                    {data: 'isonline', name: 'isonline', searchable: false, orderable: true},
                    // {data: 'relation_status', name: 'relation_status', searchable: false, orderable: false},
                    {data: 'employer_status', name: 'main.employers.employer_status', searchable: false, orderable: true},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/staff_relation/staff_employer_profile/" + aData['staff_employer_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });


            /*on click*/
            if(with_contact == 0){
                $('#with_contact_div').hide();
            }else{
                $('#without_contact_div').hide();
            }

            $("#with_contact_check").on("change", function (e) {

                if ($('#with_contact_check').prop('checked') == true) {
                    $('#with_contact_div').show();
                    $('#without_contact_div').hide();
                } else {

                    $('#without_contact_div').show();
                    $('#with_contact_div').hide();
                }
            });


            function getRouteForDt(dt_script_id)
            {

                switch(dt_script_id)
                {
                    case '1':
                                             return '{!! route('backend.compliance.employer.staff_relation.get_employers_for_active_allocations', [$user_id, $isbonus,$with_contact ]) !!}';
                        break;

                    case '2':
                        return '{!! route('backend.compliance.employer.staff_relation.get_employers_for_general', [$user_id, $dt_script_id ]) !!}';
                        break;
                }

            }

            // $("#without_contact_check").on("change", function (e) {
            //
            //     if ($('#without_contact_check').prop('checked') == true) {
            //
            //         $('#with_contact_div').hide();
            //         $('#without_contact_div').show();
            //     } else {
            //         $('#without_contact_div').hide();
            //         $('#with_contact_div').show();
            //     }
            // });

        });


    </script>;

@endpush
