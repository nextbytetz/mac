

{{--sidebar employer summary table--}}


{{--<div>&nbsp;</div>--}}

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">@lang('labels.general.summary_detail')</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%" class="summary">
            <tr>
                <td style="padding-left: 5px" width="105px">@lang('labels.general.source'):</td>
                <td  height="20px"><b>{!! $employer->source_formatted !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px" width="105px">@lang('labels.general.registered_date'):</td>
                <td  height="20px"><b>{!! $employer->created_at !!}</b></td>

            </tr>
            {{--created by--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.created_by'):</td>
                <td>
                    <b>
                        @if ($employer->source == 2)
                            {{ $employer->user_formatted }}
                        @else
                            {!! Form::label( 'created_by', ($employer->created_by) ? $employer->user->username : ' ', [ 'id'=> 'created_by'])
                            !!}
                        @endif

                    </b>
                </td>
            </tr>

            <tr>
                <td style="padding-left: 5px" width="105px">Operation Status:</td>
                <td  height="20px"><b>{!! $employer->employer_status_formatted !!}</b></td>

            </tr>
            <tr>
                <td style="padding-left: 5px" width="105px">@lang('labels.general.name'):</td>
                <td  height="20px"><b>{!! Form::label( 'name', $employer->name_formatted, [ 'id'=> 'name']) !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.employer.reg_no'):</td>
                <td><b>{!! Form::label( 'reg_no', ($employer->reg_no) ? (!empty($employer->reg_no) ? $employer->reg_no : ' ') : ' ', [ 'id'=> 'reg_no']) !!}</b></td>

            </tr>

            {{--parentemployer--}}
            @if(isset($employer->parent_id))
                <tr>
                    <td style="padding-left: 5px">@lang('labels.backend.member.parent_employer'):</td>
                    <td><b>
                            {!! Form::label( 'parent_id', ($employer->parentEmployer()->count()) ? $employer->parentEmployer->name_formatted : "-", [ 'id'=> 'parent_id'])
                            !!}</b></td>
                </tr>
            @endif

            {{--tin--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.tin_no'):</td>
                <td><b>
                        {!! Form::label( 'tin', ($employer->tin) ? (!empty($employer->tin) ? $employer->tin : ' ') : ' ', [ 'id'=> 'tin'])
                        !!}</b></td>
            </tr>
            {{--Tin verification status--}}
            {{--<tr>--}}
            {{--<td style="padding-left: 5px">@lang('labels.general.tin_verification'):</td>--}}
            {{--<td><b>--}}
            {{--{!! $employer->tin_verification_label--}}
            {{--!!}</b></td>--}}
            {{--</tr>--}}

            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.employer.date_commenced'):</td>
                <td ><b>{!! Form::label( 'date_of_commencement', ($employer->doc) ?    $employer->date_of_commencement_formatted : ' ', [ 'id'=>
                'date_of_commencement'])
                !!}</b></td>

            </tr>
            {{--Annual Earning--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.annual_earning'):</td>
                <td><b>{!! Form::label( 'annual_earning', ($employer->annual_earning) ? $employer->annual_earning_formatted : ' ', [ 'id'=> 'annual_earning'])
                        !!}</b></td>
            </tr>

            {{--phone--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.phone'):</td>
                <td><b>{!! Form::label( 'phone', ($employer->phone) ? $employer->phone : ' ', [ 'id'=> 'phone'])
                        !!}</b></td>

            </tr>


            {{--telephone--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.telephone'):</td>
                <td><b>{!! Form::label( 'telephone', ($employer->telephone) ? $employer->telephone : ' ', [ 'id'=> 'telephone'])
                        !!}</b></td>

            </tr>

            {{--email--}}
            <tr style="word-wrap: break-word; word-break: break-all;">
                <td style="padding-left: 5px " width="105px">@lang('labels.general.email'):</td>
                <td><b>{!! Form::label( 'email', ($employer->email) ? $employer->email : ' ', [ 'id'=> 'email'])
                        !!}</b></td>

            </tr>



            {{--box_no--}}
            <tr>
                <td style="padding-left: 5px " width="105px">@lang('labels.general.po_box'):</td>
                <td><b>{!! Form::label( 'box_no', ($employer->box_no) ? $employer->box_no : ' ', [ 'id'=> 'box_no'])
                        !!}</b></td>

            </tr>






            {{--Employer Category--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.employer_category'):</td>
                <td><b>{!! Form::label( 'employer_category', ($employer->employerCategory()->count()) ? $employer->employerCategory->name : ' ', [ 'id'=> 'employer_category'])
                        !!}</b></td>
            </tr>
            {{--vote--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.vote'):</td>
                <td><b>
                        {!! Form::label( 'vote', ($employer->vote) ? (!empty($employer->vote) ? $employer->vote : ' ') : ' ', [ 'id'=> 'vote'])
                        !!}</b></td>

            </tr>

            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.district'):</td>
                <td><b>{!! Form::label( 'district', ($employer->district()->count()) ? $employer->district->name : ' ', [ 'id'=> 'district'])  !!}</b></td>
            </tr>
            {{--region--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.table.region'):</td>
                <td><b>{!! Form::label( 'region', ($employer->district()->count()) ? (($employer->district->region()->count()) ? $employer->district->region->name : ' ') : (($employer->region()->count()) ? $employer->region->name : ' '), [ 'id'=> 'region'])
                        !!}</b></td>
            </tr>
            {{--country--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.country'):</td>
                <td><b>{!! Form::label( 'country', (($employer->district()->count()) ? (($employer->district->region()->count()) ? $employer->district->region->country->name : ' ') : (($employer->region()->count()) ? $employer->region->country->name : ' ')), [ 'id'=> 'country'])
                        !!}</b></td>

            </tr>

            {{--location--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.general.location'):</td>
                <td><b>{!! Form::label( 'location', ($employer->location_type_id) ? (($employer->location_type_id == 1) ? $employer->location_surveyed : $employer->location_unsurveyed) : ' ', [ 'id'=> 'location'])
                        !!}</b></td>

            </tr>

            {{--sector--}}
            @foreach($employer->sectors as $sector)
                <tr>
                    <td style="padding-left: 5px">@lang('labels.general.sector'):</td>
                    <td><b>{!! Form::label( 'sector', $sector->name, [ 'id'=> 'sector'])
                        !!}</b></td>

                </tr>
            @endforeach


            {{--business acitivyir--}}
            <tr>
                <td style="padding-left: 5px">@lang('labels.backend.member.business_activities'):</td>
                <td><b>{!! Form::label( 'business_activity', ($employer->business_activity) ? $employer->business_activity : ' ', [ 'id'=> 'business_activity'])
                        !!}</b></td>

            </tr>

            {{--treasury--}}
            @if($employer->is_treasury == true)
                <tr>
                    <td style="padding-left: 5px">Treasury:</td>
                    <td><b>{!! $employer->treasury_label
                        !!}</b></td>

                </tr>
            @endif
        </table>
    </div>
</div>






@push('after-script-end')

    <script  type="text/javascript">

    </script>;

@endpush