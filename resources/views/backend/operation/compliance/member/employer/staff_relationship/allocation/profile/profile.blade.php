@extends('layouts.backend.main', ['title' => 'Staff Employer Allocation Profile', 'header_title' => 'Staff Employer Allocation Profile '])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>


        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>


                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >



                                        @if($staff_employer_allocation->isapproved == 1)

                                            @if(comparable_date_format($staff_employer_allocation->end_date) <= comparable_date_format(\Carbon\Carbon::now()) && $staff_employer_allocation->status == 1)
                                                <span>
                                           {!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.complete_allocation', "<i class='icon fa fa-check' aria-hidden='true'></i>&nbsp;" . 'Complete',  $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to complete this allocation?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
               </span>
                                            @endif

                                            @if($staff_employer_allocation->category == 1)
                                                {{--<a href="{!! route('backend.compliance.employer.staff_relation.allocate_medium_for_large_staff', $staff_employer_allocation->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-user"></i>&nbsp;Allocate Medium Employers</a>--}}
                                                {{--TODO REMOVE this after rectification--}}
                                                {{--<span>--}}
                                                {{--{!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.allocate_medium_for_large_staff', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Allocate Medium Employers', $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to allocate medium employers to staff for large?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}--}}
                                                {{--</span>--}}
                                            @endif


                                            @if($staff_employer_allocation->category == 5)
                                                <a href="{!! route('backend.compliance.employer.staff_relation.upload_excel_page_intern', $staff_employer_allocation->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-upload"></i>&nbsp;Assign Intern Staff (Excel)</a>

                                                {{--TODO Remove this function temporary--}}
                                                {!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.undo_allocation', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this allocation?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}


                                            @endif



                                            @if($staff_employer_allocation->category == 2 || $staff_employer_allocation->category == 3)
                                                <a href="{!! route('backend.compliance.employer.staff_relation.open_allocate_new_staff_page', $staff_employer_allocation->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-user"></i>&nbsp;Allocate New Staff</a>
                                                {{--<a href="{!! route('backend.compliance.employer.staff_relation.allocations') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-exchange"></i>&nbsp;Replace Employers</a>--}}

                                                {{--TODO REMOVE this after rectification--}}
                                                {{--<span>--}}
                                                {{--{!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.replace_small_employers', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Replace Small Employers', $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to replace employers with medium plus for this allocation?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}--}}
                                                {{--</span>--}}
                                            @endif

                                            @if($staff_employer_allocation->category == 3)
                                                {{--<a href="{!! route('backend.compliance.employer.staff_relation.allocate_medium_for_large_staff', $staff_employer_allocation->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-user"></i>&nbsp;Allocate Medium Employers</a>--}}
                                                {{--TODO REMOVE this after rectification--}}
                                                {{--<span>--}}
                                                {{--{!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.reallocate_bonus_for_unassigned_staff', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Reallocate for unassigned staff', $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to re-allocate bonus employers for unassigned staff', 'class' => 'btn btn-primary site-btn nav_button'])) !!}--}}
                                                {{--</span>--}}
                                            @endif

                                            {{--TODO- NEED to remove this function until is worked on--}}
                                            {{--<span>--}}
                                            {{--{!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.revoke_allocation', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Revoke Allocation', $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to revoke this allocation?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}--}}
                                            {{--</span>--}}

                                        @endif

                                        @if($staff_employer_allocation->isapproved == 0)
                                            <span>
                                           {!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.approve_allocation', "<i class='icon fa fa-check' aria-hidden='true'></i>&nbsp;" . 'Approve',  $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to approve this allocation?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
               </span>
                                            <span>

                                                @if($staff_employer_allocation->category == 5)
                                                    <a href="{!! route('backend.compliance.employer.staff_relation.upload_excel_page_intern', $staff_employer_allocation->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-upload"></i>&nbsp;Assign Intern Staff (Excel)</a>

                                                @endif


                                                {!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.undo_allocation', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $staff_employer_allocation->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this allocation?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>
                                        @endif


                                        <a href="{!! route('backend.compliance.employer.staff_relation.edit_allocation', $staff_employer_allocation->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Modify</a>

                                        {{--close--}}
                                        <a href="{!! route('backend.compliance.employer.staff_relation.allocations') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/>
                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    {{--general info--}}
                                    @include('backend/operation/compliance/member/employer/staff_relationship/allocation/profile/includes/general')

                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/compliance/member/employer/staff_relationship/allocation/profile/includes/sidebar_summary')
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include("backend.system.workflow.includes.initiate_modal")
    {{--{!! Form::close() !!}--}}
@include('backend/operation/compliance/member/employer/staff_relationship/allocation/profile/includes/modal_remove_staff')
@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {


            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });


            $('#staff-allocated-overview-table tbody').on('click', '.remove_staff', function () {
var element_id = this.id;
var user_id = element_id.substr(12);
                // var data = table.row( this ).data();
                // //
                // $('#staff_name').text(data.fullname);
                var currentRow=$(this).closest("tr");
              var staff_name =  currentRow.find("td:eq(0)").text();

                $('.staff_name').text(staff_name);
                $('#user_id_remove').val(user_id);
                $("#remove_staff_modal").modal("show");

            } );

            $("#close_remove_staff_modal").click(function(){
//code here
                $("#remove_staff_modal").modal("hide");
            });
        });
    </script>;

@endpush
