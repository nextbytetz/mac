{{--create Form--}}

{{--main contents--}}
<br/>



{{--follow up type--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.follow_up_type'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::select('follow_up_type_cv_id', $follow_up_types, $follow_up->followUpType->reference, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'follow_type_id']) !!}
                    {!! $errors->first('follow_up_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>

@if(!is_null($follow_up->commitment_request_id))
<div class="row" id="commitment_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Attend Commitment</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select one of the commitment you attended."></i></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <select name="commitment_attend" class="form-control search-select" style="width:100%;" id="commitment_attend_id">
                        @foreach($commitment_array as $key => $value) 
                        <option value="{{$key}}" {{$value->commitment_request_id == $key ? 'selected' : ''}}>{!! $value !!}</option>
                        @endforeach
                    </select>

                    {!! $errors->first('commitment_attend_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>
@endif

{{--contact--}}
<div class="row" id="contact_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.general.contact'):</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" @lang('labels.backend.member.contact_tooltip_description')  "></i></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact', $follow_up->contact, ['class' => 'form-control', 'id'=>'contact_id']) !!}

                    {!! $errors->first('contact', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>


{{--contact person--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.contact_person'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','contact_person', $follow_up->contact_person, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_person', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

{{--date_of_follow_up--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.backend.member.date_of_follow_up'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                {{--<div class="row">--}}
                <div class="form-group">

                    <div class="form-inline">

                        <div class="input-group" style="width:100%;">
                            {!! Form::text('date_of_follow_up',  short_date_format($follow_up->date_of_follow_up), ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}

                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        {{--<span class="help-block">--}}
                        {{--<p>Date of closing the business</p>--}}
                        {{--</span>--}}
                    </div>
                </div>
                {!! $errors->first('date_of_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>



{{--feedback--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>@lang('labels.general.feedback'):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-group text_content" >
                        {!! Form::select('feedback_cv_id', $feedback, $follow_up->feedback->reference, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'feedback_cv_id']) !!}
                        {!! $errors->first('feedback_cv_id', '<span class="help-block label
                           label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{--remark--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right" id="remark_div"><label>Remark:</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group text_content">
                    {!! Form::textarea('remark', $follow_up->remark, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                    {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

{{--date_of_reminder--}}
<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Reminder date:</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" Date to remind officer for checking if employer comply with feedback provided "></i></div>


            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                {{--<div class="row">--}}
                <div class="form-group">

                    <div class="form-inline">

                        <div class="input-group" style="width:100%;">
                            {!! Form::text('reminder_date',  (isset($follow_up->date_of_reminder) ? short_date_format($follow_up->date_of_reminder) : null), ['placeholder' => '', 'class' => 'form-control datepicker2', 'autocomplete' => 'off', 'id' => 'reminder_date']) !!}

                            <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                        </div>
                        {{--<span class="help-block">--}}
                        {{--<p>Date of closing the business</p>--}}
                        {{--</span>--}}
                    </div>
                </div>
                {!! $errors->first('reminder_date', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>


<div class="row" id="reminder_email_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Employer reminder email:</label><i class="icon fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Email of employer to be used for this reminder. "></i></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','reminder_email', $follow_up->reminder_email, ['class' => 'form-control', 'id'=>'reminder_email']) !!}

                    {!! $errors->first('reminder_email', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>



{{--Re-attach file input--}}
<div class="row" id="doc_form_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Re-attach Document (PDF):</label></div>
            <div class="col-xs-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::file('document_file') !!}
                    {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>

        </div>

    </div>
</div>