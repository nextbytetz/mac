@extends('layouts.backend.main', ['title' => 'Staff Collection Performance - Intern', 'header_title' => 'Staff Collection Performance - Intern'])

@include('backend.includes.datatable_assets')

@section('content')



    <div class="row">
        <div class="col-md-12">
            <div class="alert-left-border">
                <div class="alert alert-primary alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Note.</strong> Performance is based on amount of contribution collected from allocated employers rational to target receivable amount from allocated employers.
                </div>
            </div>
        </div>
    </div>

    <div class = "row">
        {{--<div class="col-md-12" >--}}
        {{--<div class="col-md-12" >--}}
        <div class="pull-right">
            <div class="btn-group">
                <a  id="extended_button" href="#"  class="btn btn-xs btn-info" ><i class="icon fa fa-tachometer"></i>&nbsp;View Extended Performance</a>
                {{--@if(($check_pending_level1 == 1 || $check_workflow == 0) && $employer_closure->is_legacy == 0)--}}
                <a href="{!! route('backend.report.configurable.refresh_by_name', 'staff_employer_contrib_performance_intern') !!}"  class="btn btn-xs btn-primary" ><i class="icon fa fa-plus"></i>&nbsp;Refresh Data</a>
                {{--@endif--}}
            </div>
        </div>
    </div>
    {{--</div>--}}
    {{--</div>--}}

    <br/>

    {{--@include('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/collection_dashboard/includes/basic')--}}

    @include('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/collection_intern_dashboard/includes/extended')

@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";

            $('.extended_field').hide();
            $('#extended_button').click(function () {
                $('.extended_field').show();
            });


        });


    </script>;

@endpush
