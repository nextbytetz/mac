
<legend style="background-color: lightgrey">Follow ups Made  -  {{ $follow_ups->count() }}</legend>

<table class="table table-striped table-bordered  table-sm  existing-followups-new-table" id="existing-followups-new-table">
    <tbody>
    <tr style="background-color: #ebebeb">
        <td>{{ 'Employer'}}</td>
        <td >{{ 'Follow Up Type'}}</td>
        <td >{{ 'Contact' }}</td>
        <td >{{ 'Contact Person' }}</td>
        <td >{{ 'Feedback' }}</td>
        <td  class="" >{{ 'Remark' }}</td>
        <td  class="" >{{ 'Reminder Date' }}</td>
        <td>{{ 'Reminder email' }}</td>
        {{--<td >{{ 'Attachment' }}</td>--}}
        <td>{{ 'Action' }}</td>
    </tr>
    </tbody>
    {{--<tbody>--}}
    {{--</tbody>--}}
    <tfoot>


    {{--Add new--}}
    @foreach($follow_ups as $follow_up)
        <tr class = "form_input_text_sm">
            <td width="10%"> {{ $follow_up->employer->name  }}  </td>
            <td width="10%">
                {{ $follow_up->followUpType->name  }}
            </td>
            <td width="10%" class="">
                {{ $follow_up->contact  }}
            </td>
            <td width="10%">
                {{ $follow_up->contact_person  }}
            </td>
            <td width="10%">
                {{ $follow_up->feedBack->name  }}
            </td>
            <td width="13%" class="">
                {{ $follow_up->remark  }}
            </td>
            <td width="10%">
                {{ short_date_format($follow_up->date_of_reminder)  }}
            </td>
            <td width="10%" class="">
                {{ $follow_up->reminder_email  }}

            </td>

            <td width="8%" >    <a  target="_blank" id="edit_followup" href="{{ route('backend.compliance.employer.staff_relation.edit_follow_up', $follow_up->id)  }}"  class="btn btn-xs  btn-info "><i class="icon fa fa-plus"></i> {{ 'Edit' }}</a>

            </td>
        </tr>
    @endforeach


    </tfoot>
</table>

