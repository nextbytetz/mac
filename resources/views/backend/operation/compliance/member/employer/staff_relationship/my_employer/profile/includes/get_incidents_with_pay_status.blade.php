
{{--Interest table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="incidents-table">
            <thead>
            <tr>
                <th>Employee</th>
                <th>Incident type</th>
                <th>Incident Date</th>
                <th>Pay Status</th>
            </tr>
            </thead>

        </table>
    </div>
</div>

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            $("#incidents_header").one("click", function(){
                var url = "{!! url("/") !!}";
                $('#incidents-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.compliance.employer.get_incidents_for_dt', $employer->id) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'employee_name' , name: 'ee.firstname', searchable: true, orderable : true },
                        { data: 'incident_type_name', name: 't.name' , searchable: true, orderable : true },
                        { data: 'incident_date', name: 'incident_date', searchable: true, orderable : true  },
                        { data: 'pay_status', name: 'pay_status' , searchable: false, orderable : false },
                        { data: 'employee_name' , name: 'ee.lastname', searchable: true, orderable : true , visible: false},
                    ],

                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).click(function() {
                            document.location.href = url + "/claim/notification_report/profile/"  + aData['notification_report_id'];
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                        });
                    },

                } );

            });

        });
    </script>;

@endpush