@extends('layouts.backend.main', ['title' => 'Review Follow up', 'header_title' => 'Review Follow up'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.store_follow_up_review', $follow_up->id], 'name' => '', 'class' => '', 'method' => 'put',]) !!}
    {{--main contents--}}
    {!! Form::hidden('follow_up_id', $follow_up->id, ['class' =>'']) !!}
    {!! Form::hidden('staff_employer_id', $staff_employer->id, ['class' =>'']) !!}
    {!! Form::hidden('start_date', $staff_employer->start_date, ['class' =>'']) !!}
    {!! Form::hidden('end_date', $staff_employer->end_date, ['class' =>'']) !!}
    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
    <br/>

    {{--<div>--}}

    <br/>
    <div class="row">

        {{--<div class ="col-md-12">--}}

        <div class ="col-md-6">

    {{--Create form--}}
    @include("backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/includes/review_input_form")

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-9" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.employer.staff_relation.staff_employer_profile',trans('buttons.general.cancel'), [$staff_employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                                {!! Form::button('Submit',['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>




        @if(isset($visit_form))
            {{--doc review--}}
            <div class ="col-md-6">

                <div class = "row">
                    <div class="col-md-12">
                        {{--Document Preview--}}
                        <legend>Document Preview</legend>

                        <ol>
                            <li>
                                <a  id="{!! 'doc'. $visit_form->id !!}" class="doc_attached" style="color: blue" href="#">View visitation form</a>
                            </li>

                        </ol>
                        <br/>
                        <div id="document_frame" style="text-align: center;">
                            {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                        </div>


                    </div>

                </div>
            </div>
        @endif


    </div>



    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}


    <script>
        $(function () {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();


            $('.review_input').prop('disabled', true);

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                minDate: today_date,
            });
            /*-----------End Date Process------------*/

            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });



            /*Documents which pending to be used list*/
            $(".doc_attached").click(function() {
                var $doc_id = this.id;
                var $pivot_id = $doc_id.substr(3);
                let $document_frame = $("#document_frame");
                get_current_document($pivot_id).done(function ($data) {
                    $document_frame.find("iframe").remove();
                    let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
                    $document_frame.append($iframe);
                });
            });






            function get_current_document($doc_pivot_id) {
                return $.ajax({
                    url: base_url + "/compliance/employer/staff_relation/preview/document/"+ $doc_pivot_id,
                    dataType : 'json',
                    async : false,
                    method : "POST"
                });
            }

        });

        // Follow up type option
        function follow_up_type_option(follow_type_id, contact_div, contact_id) {

            var choice = $("#" + follow_type_id).val();
//            if is visit
            if (choice == 93) {
                $("#" + contact_div).hide();
            } else {
//            phone call
                if (choice == 91) {
                    $("#" + contact_id).attr('placeholder','Phone #');
                }else if(choice == 92){
                    $("#" + contact_id).attr('placeholder','Email@yahoo.com');
                }
                $("#" + contact_div).show();

            }
        }

    </script>;


@endpush