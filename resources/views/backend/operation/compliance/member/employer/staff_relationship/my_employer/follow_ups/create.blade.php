@extends('layouts.backend.main', ['title' => trans('labels.backend.member.add_new_follow_up'), 'header_title' => trans('labels.backend.member.add_new_follow_up')])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.store_follow_up'], 'name' => 'employer_close_business', 'class' => '', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('employer_id',$employer->id, ['class' =>'']) !!}
    {!! Form::hidden('staff_employer_id',isset( $staff_employer) ?  $staff_employer->id : null, ['class' =>'']) !!}
    {!! Form::hidden('start_date', isset( $staff_employer) ? $staff_employer->start_date : null, ['class' =>'']) !!}
    {!! Form::hidden('end_date', isset( $staff_employer) ? $staff_employer->end_date : null, ['class' =>'']) !!}
    {!! Form::hidden('action_type', 1, ['class' =>'']) !!}
    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
    <br/>

    {{--<div>--}}

    <br/>
    {{--</div>--}}

    {{--Create form--}}
    @include("backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/includes/create_input_form")


    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        @if(isset($staff_employer))
                        {!! link_to_route('backend.compliance.employer.staff_relation.staff_employer_profile',trans('buttons.general.cancel'), [$staff_employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
@else
                            {!! link_to_route('backend.compliance.employer.profile',trans('buttons.general.cancel'), [$employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                        @endif
                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}



    <script>
        $(function () {
            $(".search-select").select2({
                html: true
            });

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                minDate: today_date,
            });
            /*-----------End Date Process------------*/

            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });

            feedback_option();
            $("#feedback_cv_id").on("change", function (e) {
                feedback_option();
            });

            reminder_email_option();
            $("#reminder_date").on("change", function (e) {
                reminder_email_option();
            });

        });

        // Follow up type option
        function follow_up_type_option(follow_type_id, contact_div, contact_id) {

            var choice = $("#" + follow_type_id).val();
//            if is visit
            if (choice == 'FUVSIT' || choice == 'FULETT') {
                $("#" + contact_div).hide();
                $('#doc_form_div').show();
            } else {
//            phone call
                if (choice == 'FUPHONC') {
                    $("#" + contact_id).attr('placeholder','Phone #');
                }else if(choice == 'FUEMAIL'){
                    $("#" + contact_id).attr('placeholder','Email@yahoo.com');
                }
                $("#" + contact_div).show();
                $('#doc_form_div').hide();
            }
        }



        // feedback option
        function feedback_option() {

            var choice = $("#feedback_cv_id" ).val();
//            if is others
            if (choice == 'SEFOTH') {
                $("#remark_div").addClass("required");

            } else {
                $("#remark_div").removeClass("required");
            }
        }


        function reminder_email_option()
        {
            var choice = $('#reminder_date').val();
            if(choice.length > 0){
                $('#reminder_email_div').show();
                $('#reminder_email').prop('disabled', false);
            }else{
                $('#reminder_email_div').hide();
                $('#reminder_email').prop('disabled', true);
            }
        }


    </script>;


@endpush