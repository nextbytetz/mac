@extends('layouts.backend.main', ['title' => 'Follow ups for Review - Assigned', 'header_title' => 'Follow ups for Review - Assigned'])

@include('backend.includes.datatable_assets')

@section('content')



    <br/>

    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="allocations-table">
                <thead>
                <tr >
                    <th>Staff</th>
                    <th>Employer</th>
                    <th>Remark</th>
                    <th>Feedback</th>
                    <th>Followup Type</th>
                     <th>Contact</th>
                    <th>Date of Follow up</th>
                    <th>Assigned Reviewer</th>

                </tr>
                </thead>
            </table>

        </div>
    </div>


@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#allocations-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.staff_relation.get_my_for_review_datatable') !!}',
                    type : 'post'
                },
                columns: [
                    {data: 'staff', name: 'staff', searchable: true, orderable: true},
                    {data: 'employer_name', name: 'employer_name', searchable: true, orderable: true},
                    {data: 'remark', name: 'remark', searchable: true, orderable: true},
                    {data: 'feedback', name: 'feedback', searchable: true, orderable: true},
                    {data: 'follow_up_type', name: 'follow_up_type', searchable: true, orderable: true},
                    {data: 'contact', name: 'contact', searchable: true, orderable: true},
                    {data: 'date_of_follow_up', name: 'date_of_follow_up', searchable: true, orderable: true},
                    {data: 'assigned_reviewer', name: 'assigned_reviewer', searchable: true, orderable: true},

                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/staff_relation/follow_up/review_page/" + aData['follow_up_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
