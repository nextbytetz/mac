@extends('layouts.backend.main', ['title' => 'Staff-Employer Profile', 'header_title' => 'Staff-Employer Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>


        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')

    <div class = "row">
        {{--{!! Form::model($employer, ['route' => ['backend.finance.receipt.dishonour', $employer->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}

        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

        <div>&nbsp;</div>

        {{--Tabs navigation--}}
        <div class = "row">
            <div class="col-md-12">

                <div class="basic_nav_pills nav_basic_tab">
                    <ul class="nav nav-tabs">
                        {{--General--}}
                        <li class="nav-item">
                            <a class="nav-link active" id="general_header"   href="#general"
                               data-toggle="tab">@lang('labels.general.general')
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link "  id="followups_header"  href="#followups"
                               data-toggle="tab">Follow Ups
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link"  id="bookings_header"  href="#bookings"
                               data-toggle="tab">Bookings
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link " id="contributions_header"  href="#contributions"
                               data-toggle="tab">Contributions
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " id="arrears_header"  href="#arrears"
                               data-toggle="tab">Missing months (Arrears)
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " id="interests_header"  href="#interests"
                               data-toggle="tab">Interests
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link " id="dishonoured_header"  href="#dishonoured"
                               data-toggle="tab">Dishonoured Cheques
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link " id="incidents_header"  href="#incidents"
                               data-toggle="tab">Claims
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " id="inspection_header"  href="#inspections"
                               data-toggle="tab">Inspections
                            </a>
                        </li>


                    </ul>
                    <div class="nav_tab_contain tab-content">

                        <div id="general" class="nav_tab_pane tab-pane active in">
                            <div class="nav_tab_pane_header">
                                <div class="row">
                                    <div class="col-md-12" >

                                        <div class="pull-right" >
                                            @if($staff_employer->isattended == 0)
                                                {{--<span>--}}
                                                {{--{!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.update_attendance_status', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Mark as Attended', [$staff_employer->id, 1], ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to mark as attended this employer?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}--}}
                                                      {{--</span>--}}
                                                @else
                                                <span>
                                                     {!! HTML::decode(link_to_route('backend.compliance.employer.staff_relation.update_attendance_status', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Mark as Unattended', [$staff_employer->id, 0], ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to mark as unattended this employer?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                                      </span>
                                            @endif
                                            {{--close--}}
                                            <a href="{!! route('backend.compliance.employer.staff_relation.allocations') !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br/>
                            {{--main tab content--}}
                            <div class = "row">
                                <div class="col-md-12">

                                    <div class="col-md-9">
                                        {{--general info--}}
                                        @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/general')

                                    </div>
                                    <div class="col-md-3">
                                        {{--sidebar summary--}}
                                        @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/sidebar_summary')
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="followups" class="nav_tab_pane tab-pane">
                            {{--follow ups--}}
                            @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/follow_ups')
                            {{--@include('backend/operation/compliance/member/employer/staff_relationship/general_followups/includes/follow_ups_by_employer_dt', ['employer_id' => $employer->id])--}}
                        </div>

                        <div id="bookings" class="nav_tab_pane tab-pane">
                            {{--bookings--}}
                            @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/bookings')
                        </div>

                        <div id="contributions" class="nav_tab_pane tab-pane">
                            {{--contributions--}}
                            @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/contributions')
                        </div>

                        <div id="arrears" class="nav_tab_pane tab-pane">
                            {{--contributions--}}
                            @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/missing_months')
                        </div>
                        <div id="interests" class="nav_tab_pane tab-pane">
                            {{--interests--}}
                            @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/interests')
                        </div>
                        <div id="dishonoured" class="nav_tab_pane tab-pane">
                            {{--interests--}}
                            @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/dishonoured_cheques')
                        </div>

                        <div id="incidents" class="nav_tab_pane tab-pane">
                            {{--interests--}}
                            @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/includes/get_incidents_with_pay_status')
                        </div>
                        <div id="inspections" class="nav_tab_pane tab-pane">
                            {{--inspections--}}
                            @include('backend.operation.compliance.member.employer.includes.inspection', ['tab_header' => 'inspection_header'])
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>


@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {


            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });
        });
    </script>;

@endpush
