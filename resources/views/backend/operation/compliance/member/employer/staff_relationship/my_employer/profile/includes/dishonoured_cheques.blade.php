
{{--Booking table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="dishonoured-table">
            <thead>
            <tr >
                <th>Contrib Month</th>
                <th>Amount</th>
                <th>Old Cheque</th>
                <th>of Bank</th>
                <th>Receipt date</th>
                <th>Rctno</th>
                <th>New cheque</th>
                <th>of Bank</th>
                <th>status</th>
                <th>Reason</th>
            </tr>
            </thead>

        </table>
    </div>
</div>




@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            $("#dishonoured_header").one("click", function(){
                $('#dishonoured-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.compliance.get_dishonoured_cheques_for_dt', $employer->id) !!}',
                        type : 'post'
                    },
                    columns: [
                        { data: 'contrib_month' , name: 'contrib_month' , 'orderable' : false, 'searchable' : false},
                        { data: 'amount_formatted', name: 'amount' ,  'orderable' : false, 'searchable' : false},
                        { data: 'old_cheque_no', name: 'old_cheque_no' ,  'orderable' : true, 'searchable' : true}, //added col
                        { data: 'old_bank_id', name: 'old_bank_id' ,  'orderable' : false, 'searchable' : false},
                        { data: 'receipt_date', name: 'receipt_date' ,  'orderable' : false, 'searchable' : false},
                        { data: 'rct_no', name: 'rct_no',  'orderable' : false, 'searchable' : false},
                        { data: 'new_cheque_no', name: 'new_cheque_no',  'orderable' : false, 'searchable' : false},
                        { data: 'new_bank_id', name: 'new_bank_id' ,  'orderable' : false, 'searchable' : false},
                        { data: 'status', name: 'status',  'orderable' : false, 'searchable' : false },
                        { data: 'dishonour_reason', name: 'dishonour_reason',  'orderable' : false, 'searchable' : false},

                    ],



                });

            });
        });
    </script>;

@endpush