@extends('layouts.backend.main', ['title' => 'Test Simulation', 'header_title' => 'Test Simulation'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')


    <legend>Simulate Follow up Reminders</legend>
    <br/>

    <a style="color:blue;" href="{{ route('backend.compliance.employer.staff_relation.test_simulation',1) }}" >1.  Weekly Follow Up Reminders</a>

    <br/>
    <br/>
    <legend>Simulate Compliment Notification to Staff</legend>
    <br/>

    <a style="color:blue;" href="{{ route('backend.compliance.employer.staff_relation.test_simulation',2) }}" >1.  Intern Collection</a>
    <br/>
    <a style="color:blue;" href="{{ route('backend.compliance.employer.staff_relation.test_simulation',3) }}" >2.  100% Verifications</a>
    <br/>
    <a style="color:blue;" href="{{ route('backend.compliance.employer.staff_relation.test_simulation',4) }}" >3.  Dormant Activation</a>

    <br/>
    <br/>
    <legend>Simulate Others</legend>
    <a style="color:blue;" href="{{ route('backend.compliance.employer.staff_relation.test_simulation',5) }}">1.  Inbox & Task (Temp De-registrations)</a>
@stop



@push('after-script-end')

    <script>
        $(function () {

            $(".search-select").select2({});




        });

    </script>;


@endpush