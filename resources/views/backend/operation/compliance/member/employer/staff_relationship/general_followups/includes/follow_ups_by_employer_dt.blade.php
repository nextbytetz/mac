<div class = "row">
    <div class="col-md-12" >
        <div class="col-md-12" >

<br/>
            <div class="pull-left">


                {{--<div class="btn-group">--}}
                    {{--<a style="color: blue;" href="{!! route('backend.compliance.employer.staff_relation.export_employer_follow_ups',[0 ]) !!}"  class="" >Export to excel</a>--}}

                {{--</div>--}}
            </div>

            <div class="pull-right">


                <div class="btn-group">

                    {{--@if(($check_pending_level1 == 1 || $check_workflow == 0) && $employer_closure->is_legacy == 0)--}}
                    {{--<a href="{!! route('backend.compliance.employer.staff_relation.create_follow_up_general', ['employer' => $employer_id]) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-plus"></i>&nbsp;Add New</a>--}}
                    {{--@endif--}}
                </div>
            </div>



        </div>
    </div>


</div>

<br/>
{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="relation-follow-ups-table">
            <thead>
            <tr >
                <th>Remark</th>
                <th>Feedback</th>
                <th>Follow up Type</th>
                <th>Contact</th>
                <th>Contact Person</th>
                <th>Follow up Date</th>
                <th>Staff</th>
                {{--<th>Reminder date</th>--}}
            </tr>
            </thead>
        </table>

    </div>
</div>




@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $("#followups_header").one("click", function(){
            $('#relation-follow-ups-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.staff_relation.get_by_employer_for_datatable', $employer_id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'remark', name: 'remark', orderable : true, searchable : true},
                    { data: 'feedback' , name: 'feedbacks.name', orderable : true, searchable : true},
                    { data: 'follow_up_type', name: 'follow_up_types.name',  orderable : false, searchable : false},
                    { data: 'contact', name: 'contact',  orderable : true, searchable : true},
                    { data: 'contact_person', name: 'contact_person',  orderable : true, searchable : true},
                    { data: 'date_of_follow_up', name: 'date_of_follow_up' ,orderable : true, searchable : true},
                    { data: 'staff', name: 'users.firstname',  orderable : true, searchable : true},
                    // { data: 'date_of_reminder', name: 'date_of_reminder',  orderable : false, searchable : false},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/staff_relation/follow_up/edit/" + aData['follow_up_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });
        });


    </script>;

@endpush
