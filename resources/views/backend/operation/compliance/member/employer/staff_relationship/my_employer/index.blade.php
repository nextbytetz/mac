@extends('layouts.backend.main', ['title' => $title, 'header_title' => $title])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        <div class="col-md-12" >
            <div class="col-md-12" >


                <div class="pull-left">


                    <div class="btn-group">
                        <a style="color: blue;" href="{!! route('backend.compliance.employer.staff_relation.export_employers_allocated',[$user_id,$isbonus,$with_contact, $dt_script_id ]) !!}"  class="" >Export to excel</a>
                        |
                            <div class="dropdown" style="display: inline-block;"> <a class="btn btn-xs btn-primary site-btn nav_button dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown">
                                    Actions
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenuButton" >


                                    <li>
                                        <a href="{!! route('backend.compliance.employer.staff_relation.add_multiple_page') !!}"  class=""  > <i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="{!! 'Add Multiple Followups on single page'!!}"></i> Multiple Followups</a>

                                        <div class="dropdown-divider"></div>
                                    </li>


                                    <li>
                                        <i data-toggle="tooltip" data-placement="top" title="{!! 'Employers not contributed due month! Follow up to avoid late submissions and penalties. Due month - ' . (\Carbon\Carbon::now()->subMonthsNoOverflow(1)->format('M Y'))   !!}"  ></i>
                                        <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_general',[$user_id,2 ]) !!}"   >
                                            <i class="icon fa fa-eye" data-toggle="tooltip" data-placement="top" title="{!! 'View employers not contributed due month! Follow up to avoid late submissions and penalties. Due month - ' . (\Carbon\Carbon::now()->subMonthsNoOverflow(1)->format('M Y'))   !!}"></i> Not Contributed</a>
                                        <div class="dropdown-divider"></div>
                                    </li>

                                </ul>

                            </div>
                    </div>
                </div>

                <div class="pull-right">
                    {!! Form::checkbox('with_contact', '1', ($with_contact == 1) ? true : false, ['id' => 'with_contact_check', 'class' => 'with_contact_check']) !!}&nbsp;With Contact
                </div>


                <div class="pull-right" id="without_contact_div">

                    {{--<div class="pull-right">--}}
                    {{--</div>--}}
                    <div class="btn-group">
                        @if($user->relationEmployers()->where('isbonus', 0)->where('isactive', 1)->count() > 0 )
                            <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[$user_id,0, 0 ]) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-building"></i>&nbsp;Large/Non large Employers</a>
                        @endif
                        @if($user->relationEmployers()->where('isbonus', 1)->where('isactive', 1)->count() > 0 )
                            <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[$user_id,1, 0 ]) !!}"  class="btn btn-xs btn-success " ><i class="icon fa fa-building"></i>&nbsp;Bonus Employers</a>
                        @endif
                        @if($user->relationEmployers()->where('isbonus', 2)->where('isactive', 1)->count() > 0 )
                            <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[$user_id,2 , 0]) !!}"  class="btn btn-xs btn-info " ><i class="icon fa fa-building"></i>&nbsp;Treasury Employers</a>
                        @endif

                    </div>
                </div>

                <div class="pull-right" id="with_contact_div">

                    {{--{!! Form::checkbox('with_out_contact', '1', ($with_contact == 1) ? true : false, ['id' => 'with_contact_check', 'class' => '']) !!}  With Contact--}}

                    <div class="btn-group">
                        @if($user->relationEmployers()->where('isbonus', 0)->where('isactive', 1)->count() > 0 )
                            <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[$user_id,0, 1]) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-building"></i>&nbsp;Large/Non large Employers</a>
                        @endif
                        @if($user->relationEmployers()->where('isbonus', 1)->where('isactive', 1)->count() > 0 )
                            <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[$user_id,1, 1 ]) !!}"  class="btn btn-xs btn-success " ><i class="icon fa fa-building"></i>&nbsp;Bonus Employers</a>
                        @endif
                        @if($user->relationEmployers()->where('isbonus', 2)->where('isactive', 1)->count() > 0 )
                            <a href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[$user_id,2 , 1]) !!}"  class="btn btn-xs btn-info " ><i class="icon fa fa-building"></i>&nbsp;Treasury Employers</a>
                        @endif

                    </div>
                </div>



            </div>
        </div>


    </div>

    <br/>

    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="allocations-table">
                <thead>
                <tr >
                    <th>Staff</th>
                    <th>Employer</th>
                    <th>Region</th>
                    @if($with_contact == 1)
                        <th>Contact Person</th>
                        <th>Email</th>
                        <th>Phone</th>
                    @endif
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Contributing Category</th>
                    {{--<th>Receivable Amount</th>--}}
                    <th>Missing months</th>
                    <th>Follow ups</th>
                    <th>Attendance Status</th>
                    <th>Online Status</th>
                    {{--<th>Allocation Status</th>--}}
                    <th>Employer Status</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

    <br/>

    <table class="display" cellspacing="0" width="100%" >
        <thead>
        <tr >
            <th width="120px">Total follow ups : </th>
            <th width="80px">{!! number_0_format($no_of_follow_ups) !!}</th>
                <th width="120px">Previous Missing Target : </th>
                <th width="80px">{!! number_0_format($followups_target_data['prev_missing_target']) !!}</th>
                <th width="120px">Current Week Target : </th>
                <th width="80px">{!! number_0_format($followups_target_data['current_week_followups'])  . '/' . number_0_format($followups_target_data['actual_follow_ups_target']) !!}</th>
                <th width="120px">Weekly Target : </th>
                <th width="80px">{!! number_0_format($followups_target_data['weekly_target'])   !!}</th>

        </tr>
        </thead>
    </table>






@stop



{{--Scripts - Allocation dt--}}
@include('backend/operation/compliance/member/employer/staff_relationship/my_employer/includes/dt_scripts_index/allocation_dt_scripts')