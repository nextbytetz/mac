
{{-- summary--}}
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Employers Contributions Performance Overview - {!!  short_date_format($staff_employer->start_date) . ' To ' . short_date_format($staff_employer->end_date)  !!} </legend>
            <table class="table table-striped table-bordered">
                <tbody>

                    <tr>
                        <td width="200">Staff Allocated</td>
                        <th>{!!    $user->name  . ' (' . $allocation->title . ')'   !!} </th>
                    </tr>


                    <tr>
                        <td width="200">Contribution Collection</td>
                        <th>{!!      number_2_format($collection_summary['contribution_collection'] )   !!} </th>
                    </tr>


                    <tr>
                        <td width="">Interest Collection</td>
                        <th>{!!      number_2_format($collection_summary['interest_collection'])    !!} </th>
                    </tr>

                    <tr>
                        <th width="">Total Collection</th>
                        <th>{!!     number_2_format($collection_summary['contribution_collection'] +    $collection_summary['interest_collection'] )   !!} </th>
                    </tr>


                </tbody>
            </table>

        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Employers Contributions Receivable Overview</legend>
            <table class="table table-striped table-bordered">
                <tbody>


                    {{--Contributions--}}
                    <tr>
                        <th width="200px">Contribution</th>
                        <th></th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>Total Contribution</td>
                        <th>{!! number_2_format($general_contrib_summary['contribution']) !!}</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>Total Receivable</td>
                        <th>{!! number_2_format($general_contrib_summary['receivable']) !!}</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>Missing months</td>
                        <th>   {!! $general_contrib_summary['missing_months'] !!}</th>
                        <th></th>
                    </tr>

                    {{--interests--}}

                    <tr>
                        <th width="200px">Interest</th>
                        <th></th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>Total Interest</td>
                        <th>{!! $interest_summary['total_interest'] !!}</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>Total Paid</td>
                        <th>{!! $interest_summary['total_paid'] !!}</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>Total balance</td>
                        <th>{!! $interest_summary['total_remain'] !!}</th>
                        <th></th>
                    </tr>


                </tbody>
            </table>

        </div>
    </div>
</div>
@if(count($commitment_request_followups) > 0)
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <legend style="background-color: lightgrey;text-align:center">Employers Commitment Overview</legend>
            <table class="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <th colspan="2">Commitments</th>
                    </tr>
                    @foreach($commitment_request_followups as $c)
                    <tr>
                        <td>{{$c->name}}</td> @php
                        $staff_commitment_status = new App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository();
                        $status = $staff_commitment_status->commitmentStatus($c->status);

                        @endphp
                        <th>{!! $status !!}</th>
                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
    </div>
</div>
@endif

@push('after-script-end')

<script  type="text/javascript">


    $(function () {


    });
</script>;

@endpush
