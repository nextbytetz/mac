<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Sidebar Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    {{--<div class="light_grey_bg">&nbsp;</div>--}}

    <div class="light_grey_bg">


        <table class="table table-striped table-bordered">
            <tbody>

            <tr>
                <td>Start Date</td>
                <td>
                    {!! short_date_format($staff_employer_allocation->start_date)
                                !!}</td>
            </tr>
            <tr>
                <td>End Date</td>

                <td>
                    {!! short_date_format($staff_employer_allocation->end_date)
                                !!}</td>
            </tr>

            <tr>
                <td>No of employers (Target)</td>
                <td>
                    {!! number_0_format($staff_employer_allocation->no_of_employers)
                                !!}</td>
            </tr>


            <tr>
                <td>No of employers (Allocated)</td>
                <td>
                    {!! number_0_format($staff_employer_allocation->no_of_employers_allocated)
                                !!}</td>
            </tr>


            <tr>
                <td>No of staff</td>
                <td>
                    {!! number_0_format($staff_employer_allocation->no_of_staff)
                                !!}</td>
            </tr>


            </tbody>
        </table>

    </div>
</div>