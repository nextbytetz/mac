@extends('layouts.backend.main', ['title' => "Bulk Assign Employers - Intern", 'header_title' => "Bulk Assign Employers - Intern"])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */
        .progress-bar {
            background-color: #12CC1A;
            height:20px;
            color: #FFFFFF;
            width:0%;
            -webkit-transition: width .3s;
            -moz-transition: width .3s;
            transition: width .3s;
        }
        .progress-div {
            border:#0FA015 1px solid;
            padding: 5px 0px;
            margin:30px 0px;
            border-radius:4px;
            text-align:center;
        }
        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

    <div class="row">
        {{--HEADER--}}

        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.upload_excel_intern'], 'name' => 'upload_bulk', 'class' => 'upload_bulk', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

        {!! Form::hidden('staff_employer_allocation_id', $staff_employer_allocation->id, ['class' =>'']) !!}
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-6">
                    <div class="element-form" >
                        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 required"><label>Staff:</label> </div>
                        <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                            <div class="form-group">
                                {!! Form::select('staff', $staff_for_intern, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'staff']) !!}

                                {!! $errors->first('staff', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <br/>

            <div class="row">
                <div class="col-md-12">
                    <div class="fileld-layout">
                        <label>Upload Excel file</label>
                        <div class="form-group">
                            {!! Form::file('document_file', ['class' => 'btn btn-secondary btn-save btn-block']) !!}
                            <span class="help-block">
                                <p>File containing information for intern assignment</p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="row">--}}
            {{--<div class="col-md-3">--}}
            {{--<div class="pull-right">--}}
            {{--<input type="submit" class="btn btn-success btn-block" value="Upload" />--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-3" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.employer.staff_relation.allocation_profile',trans('buttons.general.cancel'), [$staff_employer_allocation->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--</div>--}}
            <br/>
            <hr/>

            <h4>@lang('labels.backend.file.instruction')</h4>
            <div class="row">
                <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-pink"></i>
           <i class="fa fa-info fa-stack-1x fa-inverse"></i>
       </span>
                </div>
                <div class='col-md-11'>
                    <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.title')</u></h4>
                    Microsoft Excel file xlsx <br/>
                <!--					@lang('labels.backend.file.format.file_helper')-->
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class='col-md-1'>
       <span class="fa-stack fa-2x">
           <i class="fa fa-square fa-stack-2x text-orange"></i>
           <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
       </span>
                </div>
                <div class='col-md-11'>
                    <h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.content')</u></h4>
                    @lang('labels.backend.file.format.content_helper') <br/>
                    @lang('labels.backend.file.format.column')  &nbsp;&nbsp;<span class="label label-success">employer_regno</span> <span class="label label-success">intern</span>
                </div>
            </div>
            <hr/>
            {{--<div class="row">--}}
            {{--<div class='col-md-1'>--}}
            {{--<span class="fa-stack fa-2x">--}}
            {{--<i class="fa fa-square fa-stack-2x text-yellow"></i>--}}
            {{--<i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>--}}
            {{--</span>--}}
            {{--</div>--}}
            {{--<div class='col-md-6'>--}}
            {{--<h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.example')</u></h4>--}}
            {{--@lang('labels.backend.file.format.example_helper') <br/><br/>--}}
            {{--<img src="{{ asset_url() . '/nextbyte/img/merge_employers.png' }}" height="auto" width="auto">--}}
            {{--</div>--}}
            {{--</div>--}}
            <br/>


        </div>


        {!! Form::close() !!}

    </div>

@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});
            /* start: Submitting Form and perform validation on the server side */
            $('body').on('submit', 'form.upload_unmerge_employers', function (e) {
                e.preventDefault();
                var $form = this;
                swal({
                    title: "Warning",
                    text: "Are you sure to upload this file?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function (confirmed) {
                    if (confirmed) {
                        /* start: remove any printed error message in the input controls */
                        $($form).find(':input').each(function () {
                            var $name = $(this).attr('name');
                            $($form).find("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("textarea[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                            $($form).find("select[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                        });
                        /* end: remove any printed error message in the input controls */
                        var $options = {
                            dataType : "json",
                            type : "POST",
                            url : $($form).attr("action"),
                            beforeSend : function (e) {
                                $($form).find(".progress-bar").width('0%');
                                $($form).find(".btn-submit").prop('disabled', true);
                            },
                            success : function (data) {
                                $($form).find(".btn-submit").prop('disabled', false);
                                swal({
                                    title: "Message",
                                    text: "File Uploaded. All employers will be demerged as specified in the uploaded file.",
                                    type: "success",
                                    showCancelButton: false,
                                    cancelButtonText: "Cancel",
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: true
                                }, function (confirmed) {
                                    /*location.reload()*/
                                    /*window.open(base_url + "/compliance/employer/");*/
                                    document.location.href =  base_url + "/compliance/employer";
                                });
                            },
                            error: function (data) {
                                $($form).find(".progress-bar").width('0%');
                                $($form).find(".btn-submit").prop('disabled', false);
                                var errors = $.parseJSON(data.responseText);
                                /* console.log(errors); */
                                $.each(errors, function(index, value) {
                                    $($form).find("input[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                    $($form).find("textarea[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                    $($form).find("select[name=" + index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + value + "</p>");
                                });
                            },
                            uploadProgress : function (event, position, total, percentComplete) {
                                $($form).find(".progress-bar").width(percentComplete + '%');
                                $($form).find(".progress-bar").html('<div class="progress-status">' + percentComplete +' %</div>')
                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
        });
    </script>
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
@endpush