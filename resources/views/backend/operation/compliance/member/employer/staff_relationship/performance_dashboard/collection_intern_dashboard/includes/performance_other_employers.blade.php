{{--Table--}}


<div class = "row">
    <div class="col-md-12" >
        <legend>Collection Performance - Other employers</legend>
        <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
            <tbody class="tbody_per_region table_display_block" >

            <tr style="color: white; background-color: #33774a" >
                <th>Sn</th>
                {{--<th></th>--}}
                <th>No of Employers</th>
                <th>Percent - Online verified</th>
                <th>Overall Target Receivable</th>
                <th>Overall Actual Collection</th>
                 <th>Performance %</th>
                {{--<th>Action</th>--}}
            </tr>




                <tr>
                    <td>{!! '1'  !!}</td>
                    {{--<td>{!! ''!!}</td>--}}
                    <td>{!! number_0_format($performance_other_employers->no_of_employers) !!}</td>
                    <td>{!! ($performance_other_employers->no_of_employers > 0) ?  number_2_format(($performance_other_employers->online_users * 100) / $performance_other_employers->no_of_employers) : 0 !!} %</td>
                    <td>{!! number_2_format( $performance_other_employers->receivable)  !!}</td>
                    <td>{!! number_2_format($performance_other_employers->contribution)  !!}</td>
                    <th>{!! ($performance_other_employers->contribution > 0) ?  number_2_format(($performance_other_employers->contribution * 100) / $performance_other_employers->receivable) : 0 !!} %</th>
                    {{--<th><a style="color:blue"  href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations', [$staff->user_id, 0,0]) !!}" >View</a></th>--}}
                </tr>




            </tbody>
        </table>

    </div>
</div>