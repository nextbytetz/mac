<div class="modal hide fade" id="remove_staff_modal" role="dialog" aria-labelledby="remove_staff_modal" aria-hidden="true">
    <div class="modal-dialog white_modal" role="document">
        <div class="modal-content" id="modal-content">

            <div class="swal2-modal swal2-show" tabindex="-1"
                 style="width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 344px;">

                <h2 class="swal2-title">Are you sure?</h2>
                <div class="swal2-content" style="display: block;">Are you sure you wish to remove <label style="font-weight: bold"   class="staff_name"> </label> from this allocation?. This action can not be reversed.</div>
                <hr class="swal2-spacer" style="display: block;">
                {{--form--}}
                {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.remove_staff_from_allocation'], 'name' => 'remove_staff', 'class' => '', 'method' => 'get']) !!}
                    {!! Form::hidden('user_id',null, ['class' =>'', 'id' => 'user_id_remove']) !!}
                {!! Form::hidden('staff_employer_allocation_id',$staff_employer_allocation->id, ['class' =>'', 'id' => 'staff_employer_allocation_id']) !!}
                <button type="submit" class="swal2-confirm btn btn-primary flat-buttons waves-effect waves-button">Yes,
                    Remove ! <label style="font-weight: bold"   class="staff_name"> </label>
                </button>
                <button type="button"  id="close_remove_staff_modal"  class="swal2-cancel btn btn-danger flat-buttons waves-effect waves-button"
                         style="display: inline-block;">No, cancel!
                </button>

                <span class="swal2-close" style="display: none;">×</span></div>
            {!! Form::close() !!}

        </div>
    </div>
</div>