@extends('layouts.backend.main', ['title' => 'Update Staff Region Allocation', 'header_title' => 'Update Staff Region Allocation'])

@include('backend.includes.datatable_assets')

@section('content')

    {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.store_allocation_staff_region'], 'name' => '', 'class' => '', 'method' => 'post']) !!}
    <div class = "row">
        <div class="col-md-6" >

            <table class="display" cellspacing="0" width="100%" >
                <thead>
                <tr >
                    <th width="120px">Staff </th>
                    <th width="80px">{!! 'Responsible region' !!}</th>
                </tr>


                </thead>


                <tbody>


                @foreach($users as $user)

                    <tr >
                        <td width="120px">{!! $user->name !!} </td>
                        <td width="80px">{!! Form::select('region'. $user->id, $regions,($user->allocatedRegions()->where('staff_region.unit_id', 5)->count() > 0) ? $user->allocatedRegions()->where('staff_region.unit_id', 5)->first()->pivot->region_id : (isset($user->office->region_id) ? $user->office->region_id : []), ['style' => 'width:100%','class' => 'form-control search-select', 'id'=> 'region'  , '']) !!}  <hr class="h1"></td>
                    </tr>

                @endforeach
                </tbody>
            </table>

            <br/>
            {{--Buttons--}}
            <div class="row">
                <div class="col-md-9" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.employer.staff_relation.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

  {!! Form::close() !!}



@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";


        });


    </script>;

@endpush
