
{{--Booking table--}}

<div class = "row">
    <div class="col-md-12" >
        {{--<div>&nbsp; </div>--}}
        &nbsp; <table class="display" cellspacing="0" width="100%" id ="arrears-table">
            <thead>
            <tr >
                <th>@lang('labels.backend.table.booking.rcv_date')</th>
                <th>@lang('labels.backend.table.booking.booked_amount')</th>

            </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="1" style="text-align:right"></th>
                <th></th>
                {{--<th colspan="2" style="text-align:right"></th>--}}

            </tr>
            </tfoot>
        </table>
    </div>
</div>


{{--SUMMARY--}}
<table class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        {{--<th>{!! strtoupper(trans('labels.general.total')) !!}:</th>--}}
        <th>Missing months</th>
        <th>{!! $general_contrib_summary['missing_months'] !!}</th>
        <th>Total receivable (Arrears)</th>
        <th>{!! number_2_format($general_contrib_summary['receivable']) !!}</th>

    </tr>
    </thead>
</table>


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            $("#arrears_header").one("click", function(){
                $('#arrears-table').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    },
                    ajax:{
                        url : '{!! route('backend.compliance.employer.get_bookings_for_missing_months', $employer->id) !!}',
                        type : 'get'
                    },
                    columns: [
                        { data: 'rcv_date_formatted' , name: 'rcv_date' },
                        { data: 'booked_amount', name: 'booked_amount' },

                    ],



                });

            });
        });
    </script>;

@endpush