@extends('layouts.backend.main', ['title' => "Staff Re-Allocation", 'header_title' => "Staff Re-Allocation"])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/datatables/css/checkboxes/dataTables.checkboxes.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
    <style>
        .custom_filter:after {
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3c5ba4;
            content: "@lang('labels.backend.system.workflow.custom_filter')";
            /* font-size: 12px;
            font-weight: bold; */
            left: -1px;
            padding: 3px 7px;
            position: absolute;
            top: -1px;
        }
        .custom_filter {
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
            border-radius: 4px 4px 4px 4px;
            margin: 5px 0px;
            padding: 39px 19px 14px;
            position: relative;
        }
    </style>
@endpush


@section('content')

    {{--Custom filter--}}
    <div class="custom_filter">
        {!! Form::open(['role' => 'form', 'id' => 'search-form']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="form-row">
                    {{--Allocation Category--}}
                    <div class="form-group col-md-4 allocation_select">
                        <label for="category">Allocation Category</label>
                        {!! Form::select('category', $allocation_categories, null, ['class' => 'form-control search-select unallocated_opt', 'id' => 'category', 'placeholder' => '']) !!}
                    </div>

                    {{--Allocated User--}}
                    <div class="form-group col-md-4 allocated_user_select">
                        <label for="allocated_user">Assigned User:</label>
                        {!! Form::select('user_id', $users, [], ['class' => 'form-control search-select unallocated_opt', 'placeholder' => '', 'id' => 'allocated_user']) !!}
                    </div>

                    {{--Employer Name--}}
                    <div class="form-group col-md-4 allocation_select">
                        <label for="employer">Employer Name</label>
                        {!! Form::select('employer', [], null, ['class' => 'form-control employer-select unallocated_opt', 'id' => 'employer']) !!}
                    </div>


                        <div class="form-group col-md-4 unallocated_select">
                            <label for="category">Unallocated Employers by Category
                                <i class="icon fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="{!! 'Unassigned employers who are active, not duplicate and contributors' !!}"></i>
                            </label>
                            {!! Form::select('unallocated_contrib_category_id', $employer_contrib_categories, null, ['class' => 'form-control search-select', 'id' => 'unallocated_contrib_category_id', 'placeholder' => '']) !!}
                        </div>


                        <div class="form-group col-md-4">
                            <label for="category">Region
                            </label>
                            {!! Form::select('region_id', $regions, null, ['class' => 'form-control search-select', 'id' => 'region_id', 'placeholder' => '']) !!}
                        </div>


                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="button" class="btn btn-secondary site-btn" id="clear_filter" value="Clear" />
                        <input type="submit" class="btn btn-success btn-sm btn-submit" value="@lang('buttons.general.search')" />
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <legend></legend>
    <br/>
    {{--@permission("resource_allocation")--}}
    {{--Assign to--}}
    <div class="row">
        <div class="col-md-12">
            <div class="form-row">
                {!! Form::open(['role' => 'form', 'id' => 'assign-user', 'route' => 'backend.compliance.employer.staff_relation.reallocate']) !!}
                <div class="form-group offset-md-6 col-md-4">
                    <span class="assign_user_select">
                    <label for="assigned_user"><b>Assign To:</b></label>
                        {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
                </span>
                </div>
                {{--<div class="col-md-3 assign_user_select">
                    <label for="assigned_user">Assign To:</label>
                    {!! Form::select('assigned_user', $users, null, ['class' => 'form-control search-select', 'id' => 'assigned_user', 'placeholder' => '']) !!}
                </div>--}}
                <div class="form-group col-md-2">
                    <label for="allocate_submit">&nbsp;</label>
                    <input type="submit" class="form-control btn btn-success btn-sm" value="Submit" id="allocate_submit">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--@endauth--}}

    {{--Resource Datatable--}}
    <div class="row">
        <div class="col-md-12">
            <table class="display" id = "resource-allocation-table" width="100%">
                <thead>
                <tr >
                    <th></th>
                    <th>Staff</th>
                    <th>Employer</th>
                    <th>Start Date</th>
                    <th>End Date</th>

                </tr>
                </thead>
            </table>
        </div>
    </div>

@stop



@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/datatables/js/checkboxes/dataTables.checkboxes.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

    <script>

        $(function() {
            $(".search-select").select2({
                allowClear: true,
                debug: true,
                placeholder: ""
            });


            /* start : Searching Employer */
            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                allowClear: true,
                debug: true,
                placeholder: "",
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            }).on("select2:selecting", function($e) {
                var $selected = $e.params.args.data.id;
            });
            /* end : Searching Employer */

            /* end : Searching Employee */
            $( "#clear_filter" ).click(function() {
                /* Clear the Filter Form */
                clearAllInputs();
            });

            /*clear all inputs on filter*/
            function clearAllInputs()
            {
                $("#employer").val(null).trigger('change.select2');
                $("#category").val(null).trigger('change.select2');
                $("#allocated_user").val(null).trigger('change.select2');
                $("#unallocated_contrib_category_id").val(null).trigger('change.select2');
                $("#region_id").val(null).trigger('change.select2');
            }

            var $oTable = $('#resource-allocation-table').DataTable({
                /*dom : 'Bfrtip',*/
                buttons : ['reload', 'colvis'],
                initComplete : function () {
                    $oTable.buttons().container().insertBefore('#resource-allocation-table');
                },
                drawCallback : function () {

                },
                processing: true,
                serverSide: true,
                info : true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax: {
                    url: "{!! route("backend.compliance.employer.staff_relation.get_allocations_for_reallocation_dt") !!}",
                    method : "PUT",
                    data: function ($d) {
                        $d.category = $('select[name=category]').val();
                        $d.employer = $('select[name=employer]').val();
                        $d.user_id = $('select[name=user_id]').val();
                        $d.unallocated_contrib_category_id = $('select[name=unallocated_contrib_category_id]').val();
                        $d.region_id = $('select[name=region_id]').val();
                    }
                },
                columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                select: {
                    'style': 'multi'
                },
                columns: [
                    {
                        orderable: false,
                        searchable: false,
                        data: 'staff_employer_id',
                        name : 'staff_employer_id'
                    },

                    {data: 'staff', name: 'users.firstname', searchable: true, orderable: true},
                    {data: 'employer', name: 'employers.name', searchable: true, orderable: true},
                    {data: 'allocation_start_date', name: 'staff_employer.start_date', searchable: true, orderable: true},
                    {data: 'allocation_end_date', name: 'staff_employer.end_date', searchable: true, orderable: true},

                ],
                // 'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
                //     $('td:not(:first-child)', $nRow).click(function() {
                //         window.open(base_url + "/claim/notification_report/profile/" + $aData['id'], "_self");
                //     }).hover(function() {
                //         $(this).css('cursor', 'alias');
                //     }, function() {
                //         $(this).css('cursor', 'auto');
                //     });
                // }
            });

            $('#search-form').on('submit', function($e) {
                resetForUnallocated();
                $oTable.draw();
                /*alert($('select[name=category]').val());
                return false;*/
                $e.preventDefault();
            });

            /*Reset other inputs for unallocated*/
            function resetForUnallocated()
            {
                var unallocated_category = $("#unallocated_contrib_category_id").val();
                if(unallocated_category != '' && unallocated_category != null){

                    // clearAllInputs();
                    $('.unallocated_opt').val(null).change();
                    $("#unallocated_contrib_category_id").val(unallocated_category).change();
                }

            }

            @auth
            $('#assign-user').on('submit', function($e) {
                $e.preventDefault();
                var $form = this;
                var $rowsSelected = $oTable.column(0).checkboxes.selected();
                //Remove all previous selected
                $($form).find("input[name='id[]']").remove();
                // Iterate over all selected checkboxes
                $.each($rowsSelected, function($index, $rowId) {
                    // Create a hidden element
                    $($form).append($('<input>').attr('type', 'hidden').attr('name', 'id[]').val($rowId));
                });
                swal({
                    title: "Warning",
                    text: "Are you sure to allocate selected files to the selected assigned user",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancel",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    closeOnConfirm: true
                }, function ($confirmed) {
                    if ($confirmed) {
                        //$form.submit();
                        //Do the ajax submission ...
                        //route : backend.claim.notification_report.allocation.assign
                        var $options = {
                            dataType : "json",
                            data : {category : $('select[name=category]').val(),
                                unallocated_contrib_category_id : $('select[name=unallocated_contrib_category_id]').val()},
                            type : "PUT",
                            url : $($form).attr("action"),
                            success : function (data) {
                                if (data.success) {
                                    $oTable.draw();
                                    $oTable.column(0).checkboxes.deselectAll();
                                    $.amaran({
                                        'theme'     :'awesome success',
                                        'content'   :{
                                            title : "Success",
                                            message: data.message,
                                            info:'',
                                            icon: 'fa fa-check-square-o'
                                        },
                                        'position'  :'bottom left',
                                        'outEffect' :'slideBottom',
                                        'inEffect'  :'slideLeft'
                                    });
                                } else {
                                    alert(data.message);
                                    /*swal({ title : "Error Assigning User to Resource(s)", text : data.message});*/
                                }
                            },
                            error: function (data) {

                            }
                        };
                        // pass options to ajaxForm
                        $($form).ajaxSubmit($options);
                    }
                });
            });
            @endauth

        });

    </script>;
@endpush