@extends('layouts.backend.main', ['title' => 'All Allocated Employers', 'header_title' => 'All Allocated Employers'])

@include('backend.includes.datatable_assets')

@section('content')

    <div class="pull-left">
        <div class="btn-group">
            <a style="color: blue;" href="{!! route('backend.compliance.employer.staff_relation.export_all_allocated_employers') !!}"  class="" >Export to excel</a>
        </div>

    </div>

    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="allocations-table">
                <thead>
                <tr >
                    <th>Staff</th>
                    <th>Employer</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Contributing Category</th>
                    {{--<th>Receivable Amount</th>--}}
                    <th>Missing months</th>
                    <th>Follow ups</th>
                    <th>Attendance Status</th>
                    <th>Online Status</th>
                    {{--<th>Allocation Status</th>--}}
                    <th>Employer Status</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

    <br/>

    {{--<table class="display" cellspacing="0" width="100%" >--}}
        {{--<thead>--}}
        {{--<tr >--}}
            {{--<th width="120px">Total follow ups : </th>--}}
            {{--<th width="80px">{!! number_0_format($no_of_follow_ups) !!}</th>--}}
            {{--<th></th>--}}
        {{--</tr>--}}
        {{--</thead>--}}
    {{--</table>--}}

@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#allocations-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.staff_relation.get_all_employers_for_active_allocations') !!}',
                    type : 'post'
                },
                columns: [
                    {data: 'staff', name: 'users.firstname', searchable: true, orderable: true},
                    {data: 'employer', name: 'employers.name', searchable: true, orderable: true},
                    {data: 'allocation_start_date', name: 'staff_employer.start_date', searchable: true, orderable: true},
                    {data: 'allocation_end_date', name: 'staff_employer.end_date', searchable: true, orderable: true},
                    {data: 'contributing_category', name: 'employer_contributing_categories.name', searchable: true, orderable: true},
                    // {data: 'receivable_amount', name: 'receivable_amount', searchable: true, orderable: true},
                    {data: 'missing_months', name: 'missing_months', searchable: false, orderable: true},
                    {data: 'no_of_follow_ups', name: 'no_of_follow_ups', searchable: true, orderable: true},
                    {data: 'attendance_status', name: 'attendance_status', searchable: false, orderable: false},
                    {data: 'isonline', name: 'isonline', searchable: false, orderable: false},
                    // {data: 'relation_status', name: 'relation_status', searchable: false, orderable: false},
                    {data: 'employer_status', name: 'employers.employer_status', searchable: false, orderable: true},
                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/staff_relation/staff_employer_profile/" + aData['staff_employer_id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
