

<legend style="background-color: lightgrey">Add New</legend>


<table class="table table-striped table-bordered  table-sm  followups-new-table" id="followups-new-table">
    <thead>
    <tr style="background-color: #ebebeb">
        <td  class="required" >{{ 'Employer'}}</td>
        <td  class="required" >{{ 'Follow Up Type'}}</td>
        <td  class="contact_div"  >{{ 'Contact' }}</td>
        <td  class="required" >{{ 'Contact Person' }}</td>
        <td  class="required" >{{ 'Feedback' }}</td>
        <td  class="" >{{ 'Remark' }}</td>
        <td  class="" >{{ 'Reminder Date' }}</td>
        <td  class="reminder_email_div" >{{ 'Reminder email' }}</td>
        <td  class="doc_form_div" >{{ 'Attachment' }}</td>
        <td>{{ 'Action' }}</td>
    </tr>
    </thead>
    {{--<tbody>--}}
    {{--</tbody>--}}
    <tfoot>


    {!! Form::hidden('action_type', 1, ['class' =>'']) !!}
    {{--Add new--}}
    <tr>
        <td width="12%">                 {!! Form::select('staff_employer_id', [], old('staff_employer_id'), [ 'placeholder' => '', 'required','class' => 'form-control employer-select', 'id'=> 'staff_employer_id']) !!}
            {!! $errors->first('staff_employer_id', '<span class="help-block label label-danger">:message</span>') !!} </td>
        <td width="10%">
            {!! Form::select('follow_up_type_cv_id', $follow_up_types, old('follow_up_type_cv_id'), [ 'placeholder' => '', 'required', 'class' => 'form-control search-select', 'id'=> 'follow_type_id']) !!}
            {!! $errors->first('follow_up_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
        </td>
        <td width="10%" class="contact_div">
            {!! Form::input( 'text','contact', null, ['class' => 'form-control form_input_text_sm', 'id'=>'contact_id']) !!}
            {!! $errors->first('contact', '<span class="help-block label label-danger">:message</span>') !!}

        </td>
        <td width="10%">
            {!! Form::input( 'text','contact_person', null, ['class' => 'form-control form_input_text_sm', 'required']) !!}
            {!! $errors->first('contact_person', '<span class="help-block label label-danger">:message</span>') !!}
        </td>
        <td width="10%">
            {!! Form::select('feedback_cv_id', $feedbacks, null, ['placeholder' => '','class' => 'form-control  search-select', 'id'=> 'feedback_cv_id', 'required']) !!}
            {!! $errors->first('feedback_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
        </td>
        <td width="12%" class="text_content">
            {!! Form::textarea('remark', null, ['class' => 'form-control form_input_text_sm' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
            {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
        </td>
        <td width="10%">
            {!! Form::text('reminder_date',  null, ['placeholder' => '', 'class' => 'form-control form_input_text_sm datepicker2', 'autocomplete' => 'off', 'id' => 'reminder_date']) !!}
            {!! $errors->first('reminder_date', '<span class="help-block label label-danger">:message</span>') !!}
        </td>
        <td width="10%" class="reminder_email_div">
            {!! Form::input( 'text','reminder_email', null, ['class' => 'form-control form_input_text_sm', 'id'=>'reminder_email']) !!}
            {!! $errors->first('reminder_email', '<span class="help-block label label-danger">:message</span>') !!}
        </td>
        <td width="9%" class="doc_form_div">
            {!! Form::file('document_file') !!}
            {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
        </td>
        <td width="8%" >   {{-- <a id="add_followup" href="#add_followup"  class="btn btn-xs  btn-info "><i class="icon fa fa-plus"></i> {{ 'Add' }}</a>--}}
        {!! Form::button(trans('buttons.general.add'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}
        </td>
    </tr>



    </tfoot>
</table>


@push('after-scripts')
    {{--{{ Html::script(url('js/examples/examples.modals.js')) }}--}}
    <script>
        $(function() {
            var url = "{{ url("/") }}";
            var current_qty = 0;
            $(".select2").select2();


        });


    </script>
@endpush

