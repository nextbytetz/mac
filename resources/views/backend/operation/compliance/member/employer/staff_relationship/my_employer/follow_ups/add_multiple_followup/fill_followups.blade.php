@extends('layouts.backend.main', ['title' => 'Fill Follow Ups', 'header_title' => 'Fill Follow Ups'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

    <style>
        .form_input_text_sm{
            font-size: 11px !important;
        }
        .scroll_table_par {
            height:500px;
            overflow-y:auto;
            overflow-x:auto;
            width: 100%;
        }
    </style>
@endpush

@section('content')
    {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.store_multiple'], 'name' => 'employer_close_business', 'class' => '', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {{ Form::label('date_of_follow_up', 'Date of Followup', ['class' =>'required']) }}
                <div class="input-group">
                    {{--not the first time--}}
                    <div class="input-group">
                        {{ Form::text('date_of_follow_up',!is_null(request()->input('date_of_follow_up')) ?  short_date_format(request()->input('date_of_follow_up')) : null,['class'=>'form-control datepicker1', 'required', 'id' => 'date_of_follow_up','placeholder' => '', 'autocomplete' => 'off']) }}
                        {!! $errors->first('date_of_follow_up', '<span class="help-block label label-danger">:message</span>') !!}
                        <span class="input-group-addon">
<span class="input-group-text">
<i class="icon fa fa-calendar"></i>
</span>
</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/add_multiple_followup/new_entry')
    {{--<div data-plugin="custom-scroll" data-height="500">--}}
    <div class="scroll_table_par" {{--data-plugin="custom-scroll" data-height="500"--}}>
    <div id="existing-followups-content">

    </div>
    </div>
    {{--<br/>--}}

    {!! Form::close() !!}


@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}



    <script>
        $(function () {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });


            jQuery('.datepicker2').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                minDate: today_date,
            });
            /*-----------End Date Process------------*/

            follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            $("#follow_type_id").on("change", function (e) {
                follow_up_type_option('follow_type_id', 'contact_div','contact_id');
            });

            feedback_option();
            $("#feedback_cv_id").on("change", function (e) {
                feedback_option();
            });

            reminder_email_option();
            $("#reminder_date").on("change", function (e) {
                reminder_email_option();
            });

        });

        // Follow up type option
        function follow_up_type_option(follow_type_id, contact_div, contact_id) {

            var choice = $("#" + follow_type_id).val();
//            if is visit
            if (choice == 'FUVSIT' || choice == 'FULETT') {
                $("." + contact_div).hide();
                enable_disable('disable_id', 'contact_id');
                $('.doc_form_div').show();
            } else {
//            phone call
                if (choice == 'FUPHONC') {
                    $("#" + contact_id).attr('placeholder','Phone #');
                }else if(choice == 'FUEMAIL'){
                    $("#" + contact_id).attr('placeholder','Email@yahoo.com');
                }
                $("." + contact_div).show();
                $('.doc_form_div').hide();
                enable_disable('enable_id', 'contact_id');
            }
        }



        // feedback option
        function feedback_option() {

            var choice = $("#feedback_cv_id" ).val();
//            if is others
            if (choice == 'SEFOTH') {
                $("#remark_div").addClass("required");

            } else {
                $("#remark_div").removeClass("required");
            }
        }


        function reminder_email_option()
        {
            var choice = $('#reminder_date').val();
            if(choice.length > 0){
                $('.reminder_email_div').show();
                $('#reminder_email').prop('disabled', false);
            }else{
                $('.reminder_email_div').hide();
                $('#reminder_email').prop('disabled', true);
            }
        }

        $("#date_of_follow_up").change(function(){
            loadExistingFollowupsContent();
        });

        /*Load Sales content*/
        loadExistingFollowupsContent();
        function loadExistingFollowupsContent() {
            var date_of_follow_up = element_id_value('date_of_follow_up');
            // if(isfuel == 1){
            if(date_of_follow_up != '' && date_of_follow_up != null){

                $.get("{{ route('backend.compliance.employer.staff_relation.load_existing_follow_ups_user') }}", {'date_of_follow_up': date_of_follow_up}, function (data) {
                    $("#existing-followups-content").empty();
                    $(data).prependTo("#existing-followups-content");
                }, "html").done()

            }


        }


        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{{ route('backend.compliance.employer.staff_relation.get_employers_by_user_select') }}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.staff_employer_id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });


    </script>;


@endpush