{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
            <tbody class="tbody_per_region table_display_block" >

            <tr style="color: white; background-color: lightskyblue" >
                <th>Sn</th>
                <th>Staff</th>
                <th>No of Employers</th>
                <th>Overall Target Receivable</th>
                <th>Overall Actual Collection</th>
                {{--<th>Target Interest</th>--}}
                {{--<th>Actual Interest Collection</th>--}}
                <th>Performance %</th>
                <th>Action</th>
            </tr>



            <?php
            $i = 1;
            ?>

            @foreach($staff_performances as $staff)

                <tr>
                    <td>{!! $i   !!}</td>
                    <td>{!! $staff->fullname !!}</td>
                    <td>{!! number_0_format($staff->no_of_employers) !!}</td>
                    <td>{!! number_2_format( $staff->target_receivable)  !!}</td>
                    <td>{!! number_2_format($staff->contribution)  !!}</td>
                    {{--<td>{!! number_2_format( $staff->target_interest)  !!}</td>--}}
                    {{--<td>{!! number_2_format($staff->interest_paid)  !!}</td>--}}
                    <th>{!! number_2_format( $staff->performance ) . ' % '!!}</th>
                    <th><a style="color:blue"  href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations', [$staff->user_id, 0,0]) !!}" >View</a></th>
                </tr>

                <?php
                $i++;
                ?>

            @endforeach


            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td>TOTAL</td>
                <th>{!! number_0_format($total_summary->no_of_employers) !!}</th>
                <th>{!! number_2_format( $total_summary->total_receivable)  !!}</th>
                <th>{!! number_2_format($total_summary->total_contribution)  !!}</th>
                <th>{!!  (($total_summary->total_receivable > 0) ?  number_2_format(($total_summary->total_contribution * 100)/($total_summary->total_receivable)) : 0) . ' %' !!}</th>
                <th></th>
            </tr>
            </tbody>
        </table>

    </div>
</div>