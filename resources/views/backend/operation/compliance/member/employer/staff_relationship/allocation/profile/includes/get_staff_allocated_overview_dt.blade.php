


{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="staff-allocated-overview-table">
            <thead>
            <tr >
                <th>Staff</th>
                <th>No of Employers Allocated</th>
                <th>No of Active Employers</th>
                <th>No of Dormant Employers</th>
                <th>No of Closed Employers</th>
                <th>No of Duplicate Employers</th>
                <th>Actions</th>
                {{--<th>Actions</th>--}}
                {{--<th>Actions</th>--}}
            </tr>
            </thead>
        </table>

    </div>
</div>




@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#staff-allocated-overview-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.staff_relation.get_staff_allocated_overview_dt', $staff_employer_allocation->id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'fullname', name: 'fullname', orderable : true, searchable : true},
                    { data: 'no_of_employers' , name: 'no_of_employers', orderable : true, searchable : true},
                    { data: 'no_of_employers_active', name: 'no_of_employers_active',  orderable : true, searchable : true},
                    { data: 'no_of_employers_dormant', name: 'no_of_employers_dormant',  orderable : true, searchable : true},
                    { data: 'no_of_employers_closed', name: 'no_of_employers_closed',  orderable : true, searchable : true},
                    { data: 'no_of_employers_duplicate', name: 'no_of_employers_duplicate',  orderable : true, searchable : true},
                    /*hidden*/
                    // { data: 'id', name: 'id',  orderable : false, searchable : false, visible: false},
                    // { data: 'staff_employer_allocation_id', name: 'staff_employer_allocation_id',  orderable : false, searchable : false},
                    /*end hidden*/
                    { data: 'actions', name: 'actions',  orderable : false, searchable : false}

                                   ],
                {{--"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {--}}
                    {{--$(nRow).click(function() {--}}
                        {{--document.location.href = url +  "/compliance/employer/staff_relation/my_employers_for_active_allocations/" + aData['id'] + '/' + '{!! $staff_employer_allocation->isbonus !!}' +'/0';--}}
                    {{--}).hover(function() {--}}
                        {{--$(this).css('cursor','pointer');--}}
                    {{--}, function() {--}}
                        {{--$(this).css('cursor','auto');--}}
                    {{--});--}}
                {{--}--}}

            });

        });


    </script>;

@endpush
