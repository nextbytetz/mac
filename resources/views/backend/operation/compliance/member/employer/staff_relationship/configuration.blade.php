@extends('layouts.backend.main', ['title' => 'Allocation Configuration', 'header_title' => 'Allocation Configuration'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.employer.staff_relation.update_configuration'], 'name' => 'employer_close_business', 'class' => 'attach_doc', 'method' => 'put','enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('staff_employer_config_id', $staff_employer_config->id, ['class' =>'']) !!}

    {{--HEADER--}}



    {{--document types--}}    {{--<div>--}}

    {{--<br/>--}}
    {{--</div>--}}

    {{--<br/>--}}



    {{--Attach file input--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Main region percent %:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::text('allocation_percent', $staff_employer_config->allocation_percent, ['placeholder' => '', 'class' => 'form-control number', 'autocomplete' => 'off', 'id' => 'allocation_percent']) !!}
                        {!! $errors->first('allocation_percent', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>




    {{--no of employers per staff non large employers--}}
    <div class="row" id="date_ref_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>No of employers per staff (Non large):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">
                        {!! Form::text('non_large_employers_per_staff', $staff_employer_config->non_large_employers_per_staff, ['placeholder' => '', 'class' => 'form-control number', 'autocomplete' => 'off', 'id' => 'non_large_employers_per_staff']) !!}
                        {!! $errors->first('non_large_employers_per_staff', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>

                </div>
            </div>

        </div>
    </div>

    {{--lareg employers--}}

    <div class="row" id="">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>No of employers per staff (large):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">
                        {!! Form::text('large_employers_per_staff', $staff_employer_config->large_employers_per_staff, ['placeholder' => '', 'class' => 'form-control number', 'autocomplete' => 'off', 'id' => 'non_large_employers_per_staff']) !!}
                        {!! $errors->first('large_employers_per_staff', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>

                </div>
            </div>

        </div>
    </div>
        <div class="row" id="">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Follow up Weekly Target (Officers):</label></div>
                    <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                        {{--<div class="row">--}}
                        <div class="form-group">
                            {!! Form::text('followup_weekly_target', $staff_employer_config->followup_weekly_target, ['placeholder' => '', 'class' => 'form-control number', 'autocomplete' => 'off', 'id' => 'followup_weekly_target']) !!}
                            {!! $errors->first('followup_weekly_target', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                    </div>
                </div>

            </div>
        </div>


        <div class="row" id="">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Follow up Weekly Target (Intern):</label></div>
                    <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                        {{--<div class="row">--}}
                        <div class="form-group">
                            {!! Form::text('followup_weekly_target_intern', $staff_employer_config->followup_weekly_target_intern, ['placeholder' => '', 'class' => 'form-control number', 'autocomplete' => 'off', 'id' => 'followup_weekly_target_intern']) !!}
                            {!! $errors->first('followup_weekly_target', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="row" id="">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Follow up Next Date Reminder:</label></div>
                    <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                        {{--<div class="row">--}}
                        <div class="form-group">
                            @if(!isset($staff_employer_config->followup_reminder_next_date))
                                {!! Form::text('followup_reminder_next_date',null, ['placeholder' => '', 'class' => 'form-control number datepicker2', 'autocomplete' => 'off', 'id' => 'followup_reminder_next_date']) !!}
                                {!! $errors->first('followup_reminder_next_date', '<span class="help-block label label-danger">:message</span>') !!}
                            @else
                                {!! Form::text('followup_reminder_next_date_disabled',short_date_format($staff_employer_config->followup_reminder_next_date), ['placeholder' => '', 'class' => 'form-control number datepicker2', 'autocomplete' => 'off', 'id' => 'followup_reminder_next_date_disabled', 'disabled']) !!}

                                {!! Form::hidden('followup_reminder_next_date',  $staff_employer_config->followup_reminder_next_date, ['class' =>'']) !!}
                            @endif

                        </div>

                    </div>
                </div>

            </div>
        </div>


    <div class="row" id="date_ref_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Compliance officers (To be assigned employers):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">
                        {!! Form::select('compliance_officers[]', $compliance_staff,$compliance_officers, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'staff'  , 'multiple']) !!}
                        {!! $errors->first('compliance_officers', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div class="row" id="date_ref_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Staff for Large contributors:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">
                        {!! Form::select('staff_for_large_contributors[]', $compliance_staff,$staff_for_large_contrib, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'staff'  , 'multiple']) !!}
                        {!! $errors->first('staff_for_large_contributors', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="row" id="">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Intern Staff:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    {{--<div class="row">--}}
                    <div class="form-group">
                        {!! Form::select('intern_staff[]', $staff_for_intern,$intern_staff, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'staff'  , 'multiple']) !!}
                        {!! $errors->first('intern_staff', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>

                </div>
            </div>

        </div>
    </div>

    <br/>

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.employer.staff_relation.index',trans('buttons.general.cancel'), [],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}


    <script>
        $(function () {

            $(".search-select").select2({});


            /*Number*/
            $('body').off('keydown', '.number').on('keydown', '.number', function(e) {
                number_only(e);
            });


            /* start : ensure only numbers are input on monetary boxes */
            function number_only(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }

        });

    </script>;


@endpush