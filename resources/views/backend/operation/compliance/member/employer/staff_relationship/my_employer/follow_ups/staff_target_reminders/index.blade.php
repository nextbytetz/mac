@extends('layouts.backend.main', ['title' => 'Staff Missing Followups Target Reminders', 'header_title' => 'Staff Missing Followups Target Reminders'])

@include('backend.includes.datatable_assets')

@section('content')

    <div class="pull-left">
        <div class="btn-group">
            <a style="color: blue;" href="{!! route('backend.compliance.employer.staff_relation.followup.staff_missing_target.export_excel') !!}"  class="" >Export to excel</a>
        </div>

    </div>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="allocations-table">
                <thead>
                <tr >
                    <th>Staff</th>
                    <th>Total missing target</th>
                    <th>Week start date</th>
                    <th>Week end date</th>
                    <th>No. of weeks</th>
                    <th>Total days worked</th>
                    <th>Elevation level</th>
                </thead>
            </table>

        </div>
    </div>

    <br/>

    <table class="display" cellspacing="0" width="100%" >
        <thead>
        <tr >
            <th width="120px"> </th>
            <th width="120px"> </th>

        </tr>
        </thead>
    </table>

@stop

@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var  with_contact = '{!! $with_contact !!}';
            var url = "{!! url("/") !!}";
            $('#allocations-table').DataTable({
                processing: true,
                serverSide: false,
                stateSave: true,

                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.staff_relation.followup.staff_missing_target.get_list_reminders') !!}',
                    type : 'post'
                },
                columns: [
                    {data: 'staff_name', name: 'u.firstname', searchable: true, orderable: true},
                    {data: 'total_missing_target', name: 'total_missing_target', searchable: true, orderable: true},
                    {data: 'week_start_date', name: 'week_start_date', searchable: true, orderable: true},
                    {data: 'week_end_date', name: 'week_end_date', searchable: false, orderable: false},
                    {data: 'no_of_weeks', name: 'no_of_weeks', searchable: false, orderable: false},
                    {data: 'total_days_worked', name: 'total_days_worked', searchable: false, orderable: false},
                    {data: 'elevation_level', name: 'elevation_level', searchable: true, orderable: true},
                ],

                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/staff_relation/my_employers_for_active_allocations/" + aData['user_id'] + '/0/0' ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });




        });


    </script>;

@endpush
