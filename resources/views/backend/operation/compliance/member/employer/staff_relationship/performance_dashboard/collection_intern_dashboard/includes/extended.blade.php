{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="table table-striped table-bordered table-fixed"  style="width:100%" >
            <tbody class="tbody_per_region table_display_block" >

            <tr style="color: white; background-color: lightskyblue" >
                <th>Sn</th>
                <th>Staff</th>
                <th>No of Employers</th>
                {{--Extended--}}
                <th class="">Percent - Online verified</th>
                <th class="extended_field">Arrears Before Allocation</th>
                <th class="extended_field">Contrib Collection (Arrears)</th>
                <th class="extended_field">Receivable Current</th>
                <th class="extended_field">Collection Current</th>
                {{--extended--}}
                <th>Overall Target Receivable</th>
                <th>Overall Actual Collection</th>
                <th>Performance %</th>
                <th>Action</th>
            </tr>



            <?php
            $i = 1;
            ?>

            @foreach($staff_performances as $staff)

                <tr>
                    <td style="text-align:right;">{!! $i   !!}</td>
                    <td>{!! $staff->fullname !!}</td>
                    <td style="text-align:right;">{!! number_0_format($staff->no_of_employers) !!}</td>
                    {{--extended--}}
                    <td class="" style="text-align:right;">{!! (($staff->no_of_employers > 0) ?  number_2_format(($staff->online_employers * 100)/($staff->no_of_employers)) : 0) . ' %' !!}</td>
                    <td class="extended_field" style="text-align:right;">{!! number_2_format( $staff->arrears_before)  !!}</td>
                    <td class="extended_field" style="text-align:right;">{!! number_2_format($staff->arrears_contribution)   . '<br/> <b style="font-size: 10px"> ('.  (($staff->arrears_before > 0) ?  number_2_format(($staff->arrears_contribution * 100)/($staff->arrears_before)) : 0) . ' %'   . ') </b>' !!}</td>
                    <td class="extended_field" style="text-align:right;">{!! number_2_format( $staff->receivable_current)  !!}</td>
                    <td class="extended_field" style="text-align:right;">{!! number_2_format($staff->contribution_current) . '  <br/>  <b style="font-size: 10px">  ('.  (($staff->receivable_current > 0) ?  number_2_format(($staff->contribution_current * 100)/($staff->receivable_current)) : 0) . ' %'   . ')  </b>'   !!}</td>
                    {{--extended--}}
                    <td style="text-align:right;">{!! number_2_format( $staff->target_receivable)  !!}</td>
                    <td style="text-align:right;">{!! number_2_format($staff->contribution)  !!}</td>
                    {{--<td>{!! number_2_format( $staff->target_interest)  !!}</td>--}}
                    {{--<td>{!! number_2_format($staff->interest_paid)  !!}</td>--}}
                    <th style="text-align:right;"> {!! number_2_format( $staff->performance ) . ' % '!!}</th>
                    <th><a style="color:blue"  href="{!! route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations', [$staff->user_id, 0,0]) !!}" >View</a></th>
                </tr>

                <?php
                $i++;
                ?>

            @endforeach


            <tr>
                <td></td>
                <td></td>
                <td></td>
                {{--Extended--}}
                <td class=""></td>
                <td class="extended_field"></td>
                <td class="extended_field"></td>
                <td class="extended_field"></td>
                <td class="extended_field"></td>
                {{--Extended--}}
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td>TOTAL</td>
                <th style="text-align:right;">{!! number_0_format($total_summary->no_of_employers) !!}</th>
                {{--extended--}}
                <th class="" style="text-align:right;">{!! (($total_summary->no_of_employers > 0) ?  number_2_format(($total_summary->total_online_employers * 100)/($total_summary->no_of_employers)) : 0) . ' %' !!}</th>
                <th class="extended_field" style="text-align:right;">{!! number_2_format( $total_summary->total_arrears_before)  !!}</th>
                <th class="extended_field" style="text-align:right;">{!! number_2_format( $total_summary->total_arrears_contribution)  . '<br/>  <b style="font-size: 10px">  ('.  (($total_summary->total_arrears_before > 0) ?  number_2_format(($total_summary->total_arrears_contribution * 100)/($total_summary->total_arrears_before)) : 0) . ' %'   . ') </b>'  !!}</th>
                <th class="extended_field" style="text-align:right;">{!! number_2_format( $total_summary->total_receivable_current)  !!}</th>
                <th class="extended_field" style="text-align:right;">{!! number_2_format( $total_summary->total_contribution_current) . '<br/>  <b style="font-size: 10px">('.  (($total_summary->total_receivable_current > 0) ?  number_2_format(($total_summary->total_contribution_current * 100)/($total_summary->total_receivable_current)) : 0) . ' %'   . ') </b>'  !!}</th>
                {{--extended--}}
                <th style="text-align:right;">{!! number_2_format($total_summary->total_receivable)  !!}</th>
                <th style="text-align:right;">{!! number_2_format($total_summary->total_contribution)  !!}</th>
                <th style="text-align:right;">{!!  (($total_summary->total_receivable > 0) ?  number_2_format(($total_summary->total_contribution * 100)/($total_summary->total_receivable)) : 0) . ' %' !!}</th>
                <th></th>
            </tr>
            </tbody>
        </table>

    </div>
</div>