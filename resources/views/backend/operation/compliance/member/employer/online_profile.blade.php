@extends('layouts.backend.main', ['title' => trans('labels.backend.member.employer.title.index'), 'header_title' => trans('labels.backend.member.employer.header.index_online')])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
    /* start: File Icons CSS */
    #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
    #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
    #folder-tree .file-pdf { background-position: -32px 0 }
    #folder-tree .file-as { background-position: -36px 0 }
    #folder-tree .file-c { background-position: -72px -0px }
    #folder-tree .file-iso { background-position: -108px -0px }
    #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
    #folder-tree .file-cf { background-position: -162px -0px }
    #folder-tree .file-cpp { background-position: -216px -0px }
    #folder-tree .file-cs { background-position: -236px -0px }
    #folder-tree .file-sql { background-position: -272px -0px }
    #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
    #folder-tree .file-h { background-position: -488px -0px }
    #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
    #folder-tree .file-php { background-position: -108px -18px }
    #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
    #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
    #folder-tree .file-rb { background-position: -180px -18px }
    #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
    #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
    #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
    #folder-tree .file-js { background-position: -434px -18px }
    #folder-tree .file-css { background-position: -144px -0px }
    #folder-tree .file-fla { background-position: -398px -0px }
    /* end: File Icon CSS */
    /* start: upload progress bar css */
    .progress-bar {
        background-color: #12CC1A;
        height:20px;
        color: #FFFFFF;
        width:0%;
        -webkit-transition: width .3s;
        -moz-transition: width .3s;
        transition: width .3s;
    }
    .progress-div {
        border:#0FA015 1px solid;
        padding: 5px 0px;
        margin:30px 0px;
        border-radius:4px;
        text-align:center;
    }
    /* end: upload progress bar css */
    tr {
        border-bottom:1pt solid rgba(0, 0, 0, 0.12);
    }
</style>
@endpush

@section('content')


<div class = "row">
    {{--{!! Form::model($employer, ['route' => ['backend.finance.receipt.dishonour', $employer->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}

    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

    <div>&nbsp;</div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                        data-toggle="tab">@lang('labels.general.general')
                    </a>
                </li>
                {{--Bills Generated--}}
                <li class="nav-item">
                    <a class="nav-link"  id = "tab_2_header"  href="#bills" data-toggle="tab">
                        {{-- @lang('labels.general.bills') --}} Bills
                    </a>
                </li>
                {{--Online users--}}
                <li class="nav-item">
                    <a class="nav-link" id = "tab_3_header" href="#online_users" data-toggle="tab">
                        @lang('labels.general.online_user')
                    </a>
                </li>

                {{--Employees--}}
                <li class="nav-item">
                    <a class="nav-link" id = "employee_header" href="#employees" data-toggle="tab">
                        @lang('labels.backend.compliance.employees')
                    </a>
                </li>


                {{--Control number request--}}
                <li class="nav-item">
                    <a class="nav-link" id = "document_header" href="#control_number" data-toggle="tab">
                        {{-- @lang('labels.general.control_number_request') --}} Payment
                    </a>
                </li>

                {{--Notification Online Users--}}
                <li class="nav-item">
                    <a class="nav-link" id = "tab_4_header" href="#notification_online_users" data-toggle="tab">
                        Notification Users
                    </a>
                </li>
                {{--Notification Online Users--}}

                <li class="nav-item">
                    <a class="nav-link" id = "tab_5_header" href="#installments" data-toggle="tab"> Installments</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id = "document_header" href="#commitments" data-toggle="tab">Commitments</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id = "tab_4_header" href="#employer_return_earnings" data-toggle="tab">Annual Earnings</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id = "tab_4_header"
                    href="#payroll_merging" data-toggle="tab">Payroll Merging</a>
                </li>
            </ul>
            <br>
            <div class="nav_tab_contain tab-content">
                <div id="general" class="nav_tab_pane tab-pane active in">


                    {{--main tab content--}}
                    <div class = "row">
                        <div class="col-md-12">

                            <div class="col-md-9">
                                @include('backend.operation.compliance.member.employer.includes.contribution_arrears')
                                <div>&nbsp;</div>
                            </div>
                            <div class="col-md-3">
                                @include('backend.operation.compliance.member.employer.includes.sidebar_summary')
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bills" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.employer_bills')
                </div>
                <div id="online_users" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.online_users')
                </div>
                <div id="employees" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.employees')

                </div>
                <div id="control_number" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.bill_employer')
                </div>
                <div id="notification_online_users" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.notification_online_users')
                </div>
                <div id="commitments" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.arrear_commitments')
                </div>
                <div id="installments" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.arrear_installment')
                </div>
                <div id="employer_return_earnings" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.includes.employer_return_earnings')
                </div>

                <div id="payroll_merging" class="nav_tab_pane tab-pane">
                    @include('backend.operation.compliance.member.employer.payroll_merging.payroll_merging')
                </div>
            </div>
        </div>
    </div>

</div>

{{--{!! Form::close() !!}--}}
</div>
</div>
@include("backend.includes.partials.deactivate_employer_user_modal")
@include("backend.includes.partials.deactivate_notification_user_modal")
@include('backend.includes.partials.reset_password')
@include("backend.includes.partials.add_user_modal")
@include('backend.includes.partials.add_arrear_modal')
@stop


@push('after-script-end')
@stack('booking-script-end')
@stack('contribution-script-end')
@stack('interest-adjustment-overview-script-end')
@stack('interest-writeoff-overview-script-end')
@stack('interest-script-end')
@stack('inactive-receipts-script-end')
@stack('document-centre-script-end')
@stack('employees-script-end')
<script  type="text/javascript">

    $(function () {


        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });

</script>;

@endpush
