@extends('layouts.backend.main', ['title' => 'Change Particular Profile', 'header_title' => 'Change Particular Profile'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
    <style>


        /* end: upload progress bar css */
        tr {
            border-bottom:1pt solid rgba(0, 0, 0, 0.12);
        }
    </style>
@endpush

@section('content')


    <div class = "row">
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer_particular_change->employer])


    </div>
    <br/>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                           data-toggle="tab">@lang('labels.general.general')
                        </a>
                    </li>
                    {{--document--}}
                    <li class="nav-item">
                        <a class="nav-link" href="#documents" id="document_header"
                           data-toggle="tab">Document Center
                        </a>
                    </li>

                </ul>
                <div class="nav_tab_contain tab-content">

                    <div id="general" class="nav_tab_pane tab-pane active in">
                        <div class="nav_tab_pane_header">
                            <div class="row">
                                <div class="col-md-12" >

                                    <div class="pull-right" >
                                        @if($check_workflow == 0)
                                            <span>
                                           {!! HTML::decode(link_to('#', "<i class=\"icon fa fa-confirm\"></i>&nbsp;Initiate", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'id' => "initiate_workflow", 'data-description' => 'Initiate Workflow', 'data-group' => 10, 'data-type' => 0, 'data-resource' => $employer_particular_change->id, 'data-route' => route('backend.compliance.employer.change_particular.initiate_wf', $employer_particular_change->id)])) !!}
                                            </span>
                                        @endif

                                        @if($check_pending_level1 == 1 || $check_workflow == 0)
                                            <span>
                                           {!! HTML::decode(link_to_route('backend.compliance.employer.change_particular.undo', "<i class='icon fa fa-reply' aria-hidden='true'></i>&nbsp;" . 'Undo', $employer_particular_change->id, ['data-method' => 'confirm','data-type'=>'success', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.confirm'), 'data-trans-text' => 'Are you sure you wish to undo this change of particular?', 'class' => 'btn btn-primary site-btn nav_button'])) !!}
                                            </span>

                                            {{--modify--}}
                                            <a href="{!! route('backend.compliance.employer.change_particular.edit', $employer_particular_change->id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-edit"></i>&nbsp;Modify</a>

                                        @endif


                                        {{--close--}}

                                        <a href="{!! route('backend.compliance.employer.change_particular.index', $employer_particular_change->employer_id) !!}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-close"></i>&nbsp;Close</a>

                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--<br/>--}}
                        {{--main tab content--}}
                        <div class = "row">
                            <div class="col-md-12">

                                <div class="col-md-9">
                                    <br/>
                                    <h5 style="background-color: lightgrey;"><b><span class="light_dark_color"> &nbsp; General Information - Change Request</span></b></h5>
                                    {{--<br/>--}}
                                    {{--notice summary--}}
                                    @include('backend/operation/compliance/member/employer/particular_change/profile/includes/notice_summary')


                                    {{--general summary--}}
                                    @include('backend/operation/compliance/member/employer/particular_change/profile/includes/general/general_summary')

                                    {{--general info--}}
                                    @include('backend/operation/compliance/member/employer/particular_change/profile/includes/general_info')
                                    {{--Remark--}}

                                    {{--APprovals tabs history--}}
                                    {!! $workflow_track->with($workflow_input)->render('backend.includes.workflow_track', $workflow_input) !!}
                                </div>
                                <div class="col-md-3">
                                    {{--sidebar summary--}}
                                    @include('backend/operation/compliance/member/employer/particular_change/profile/includes/sidebar_summary')
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--documents--}}
                    <div id="documents" class="nav_tab_pane tab-pane">
                        @include('backend/operation/compliance/member/employer/particular_change/profile/includes/document_center')
                    </div>

                </div>
            </div>
        </div>

    </div>

    @include("backend.system.workflow.includes.initiate_modal")
    {{--{!! Form::close() !!}--}}

@stop


@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
    {{--    {{ Html::script(asset_url(). "/nextbyte/js/backend/backend.js") }}--}}
    <script  type="text/javascript">


        $(function () {

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            if (location.hash !== '') {
                $('a[href="' + location.hash + '"]').tab('show');
                $('a[href="' + location.hash + '"]').trigger('click');
            }


            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tab = $(e.target).attr('href').substr(1);
                if (history.pushState) {
                    history.pushState(null, null, '#' + tab);
                } else {
                    location.hash = '#' + tab;
                }
            });


            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });


        });
    </script>;

@endpush
