

<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>New Registration Authority:</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">

                    {{--{!! $errors->first('registration_authority_cv_id', '<span class="help-block label label-danger">:message</span>') !!}--}}
                    @foreach(unserialize($change_repo->getChangeType($employer_particular_change->id, 'EMTCREGA', null)->first()->new_values) as $key => $new_value)
                        {{--<strong> {!! $new_value !!}</strong>--}}

                        {!! Form::select('registration_authority_cv_id', $reg_authorities, $new_value, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control', 'id'=> 'registration_authority_cv_id', 'disabled']) !!}
                    @endforeach


                </div>
            </div>
        </div>

    </div>
</div>


@if($check_pending_level1 == 1 || $check_workflow == 0)
    <br/>
    Note: <b style="font-style: italic; font-size: 10px;"> Attach Relevant Document(s) </b>
@endif