<br/>

<div class="row">

    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Pending Tasks</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>


<ol>
    @if($check_pending_level1 == 1 || $check_workflow == 0)
    @foreach($pending_docs as $doc)

    <li style="margin-left:10px;">
        {!! $doc . ' not attached' !!}

    </li>


    @endforeach
    @endif
</ol>

@if($employer_particular_change->source == 2)

<div class="row mt-2">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Online Payroll (User) </span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <div class="table-responsive">
            <table style="width:100%" class="summary">
                <tr class="underline mb-1" >
                    <td class="pl-1" width="50%">
                        <h6 style="font-weight: lighter;">Payroll No#:</h6>
                    </td>
                    <td>
                        <h6 style="font-weight: bold;">
                            {{$employer_particular_change->payroll_id}}
                        </h6>
                    </td>
                </tr>

                @if($employer_particular_change->user->description)
                <tr class="underline pb-1" >
                    <td class="pt-1 pl-1" width="50%">
                        <h6 style="font-weight: lighter;">Payroll Description :</h6>
                    </td>
                    <td class="pt-1">
                        <h6 style="font-weight: bold;">
                            {{$employer_particular_change->onlineUser->description}}
                        </h6>
                    </td>
                </tr>
                @endif  
                <tr class="underline pb-1" >
                    <td class="pt-1 pl-1" width="50%">
                        <h6 style="font-weight: lighter;">Username :</h6>
                    </td>
                    <td class="pt-1">
                        <h6 style="font-weight: bold;">
                            {{$employer_particular_change->onlineUser->name}}
                        </h6>
                    </td>
                </tr>

                <tr class="underline pb-1" >
                    <td class="pt-1 pl-1" width="50%">
                        <h6 style="font-weight: lighter;">Phone :</h6>
                    </td>
                    <td class="pt-1">
                        <h6 style="font-weight: bold;">
                            {{$employer_particular_change->onlineUser->phone}}
                        </h6>
                    </td>
                </tr>

                <tr class="underline pb-1" >
                    <td class="pt-1 pl-1" width="50%">
                        <h6 style="font-weight: lighter;">Email :</h6>
                    </td>
                    <td class="pt-1">
                        <h6 style="font-weight: bold;">
                            {{$employer_particular_change->onlineUser->email}}
                        </h6>
                    </td>
                </tr>



            </table>
        </div>
    </div>
</div>

@endif
