

{{--General info--}}
<br/>
<div class="row">
    {{--left general  summary--}}
    <div class="col-md-12">
        <div class="col-md-6">
            <legend id="old_values"  style="background-color: lightgrey;text-align:center">Old Values</legend>
            <table class="table table-striped table-bordered" id="old_values_table">
                <tbody>

                @foreach(unserialize($change_repo->getChangeType($employer_particular_change->id, 'EMTCGEN', null)->first()->old_values) as $key => $old_value)
                    <tr>
                        <td width="130px">{!! ucfirst(str_replace('_',' ',str_replace('_id','',$key))) !!}:</td>

                        @if($key == 'doc')
                            <th>{!! short_date_format($old_value)  !!} </th>
                        @elseif($key == 'district_id' || $key == 'district')

                            <th>{!! (isset($old_value) && strlen($old_value) > 0) ? (new \App\Repositories\Backend\Location\DistrictRepository())->find( $old_value)->name : '' !!} </th>

                        @elseif($key == 'region_id' || $key == 'region')
                            <th>{!!  (isset($old_value) && strlen($old_value) > 0)  ? (new \App\Repositories\Backend\Sysdef\RegionRepository())->find( $old_value)->name   : '' !!} </th>

                        @elseif($key == 'country_id')
                            <th>{!! isset($old_value) ? (new \App\Repositories\Backend\Location\CountryRepository())->find( $old_value)->name  : '' !!} </th>

                        @elseif($key == 'location_type_id')
                            <th>{!!  isset($old_value) ? (new \App\Repositories\Backend\Location\LocationTypeRepository())->find( $old_value)->name  : '' !!} </th>
                        @elseif($key == 'employer_category_cv_id')
                            <th>{!!  isset($old_value) ? (new \App\Repositories\Backend\Sysdef\CodeValueRepository())->find( $old_value)->name  : '' !!} </th>
                        @else
                            <th>{!! $old_value !!} </th>
                        @endif
                    </tr>
                @endforeach



                </tbody>
            </table>

        </div>

        <div class="col-md-6">
            <legend id="new_values"  style="background-color: lightgrey;text-align:center">New Values</legend>
            <table class="table table-striped table-bordered" id="new_values_table">
                <tbody>

                @foreach(unserialize($change_repo->getChangeType($employer_particular_change->id, 'EMTCGEN', null)->first()->new_values) as $key => $new_value)
                    <tr>
                        <td width="130px">{!! ucfirst(str_replace('_',' ',str_replace('_id','',$key))) !!}:</td>
                        @if($key == 'doc')
                            <th>{!! short_date_format($new_value) !!}
                                @if($change_repo->checkWhenDocChangedNoContributionBeforeNewDate($employer_particular_change->employer_id, $new_value) == false && $employer_particular_change->wf_done == 0)
                                <img  data-toggle="tooltip" data-placement="top" title="There are contribution(s) before this new Date of commencement. Please check!' " style="width: 20px;height: 20px;" class="img-fluid" src="{{asset_url(). '/nextbyte/img/alert.gif'}}">
                            @endif
                            </th>
                        @elseif($key == 'district_id' || $key == 'district')
                            <th>{!! (isset($new_value) && strlen($new_value) > 0) ? (new \App\Repositories\Backend\Location\DistrictRepository())->find( $new_value)->name : '' !!} </th>

                        @elseif($key == 'region_id' || $key == 'region')
                            <th>{!!  (isset($new_value) && strlen($new_value) > 0)  ? (new \App\Repositories\Backend\Sysdef\RegionRepository())->find( $new_value)->name : '' !!} </th>

                        @elseif($key == 'country_id')
                            <th>{!! isset($new_value) ? (new \App\Repositories\Backend\Location\CountryRepository())->find( $new_value)->name : '' !!} </th>

                        @elseif($key == 'location_type_id')
                            <th>{!!  isset($new_value) ? (new \App\Repositories\Backend\Location\LocationTypeRepository())->find( $new_value)->name : '' !!} </th>
                        @elseif($key == 'employer_category_cv_id')
                            <th>{!!  isset($new_value) ? (new \App\Repositories\Backend\Sysdef\CodeValueRepository())->find( $new_value)->name  : '' !!} </th>
                        @else
                            <th>{!! $new_value !!} </th>
                        @endif
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>
</div>



@push('after-script-end')

    <script  type="text/javascript">


        $(function () {

            // $("#old_values_table").hide();
            // $("#old_values").click(function(){
            //     $("#old_values_table").slideToggle("slow");
            // });

        });
    </script>;

@endpush








