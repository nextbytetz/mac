

<div class="card-accordions">
    <div id="accordion" role="tablist" aria-multiselectable="true">

        @if($change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', null)->count() > 0 )
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#name_change" aria-expanded="false">Name Change</a>
                    </h5>
                </div>
                <div id="name_change" class="expand" aria-expanded="true" style="">
                    <div class="card-block">
                        @include('backend/operation/compliance/member/employer/particular_change/profile/includes/general/name_change')
                    </div>
                </div>
            </div>
        @endif

            @if($change_repo->getChangeType($employer_particular_change->id, 'EMTCGEN', null)->count() > 0 )
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#general_change" aria-expanded="false">General Change</a>
                    </h5>
                </div>
                <div id="general_change" class="expand" aria-expanded="false">
                    <div class="card-block">
                        @include('backend/operation/compliance/member/employer/particular_change/profile/includes/general/general_change')
                    </div>
                </div>
            </div>
        @endif

            @if($change_repo->getChangeType($employer_particular_change->id, 'EMTCREGA', null)->count() > 0 )
            <div class="card">
                <div class="card-header" role="tab" id="headingThree">
                    <h5 class="mb-0">
                        <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#registration_change" aria-expanded="false">Registration Authority Change</a>
                    </h5>
                </div>
                <div id="registration_change" class="expand" aria-expanded="false">
                    <div class="card-block">
                        @include('backend/operation/compliance/member/employer/particular_change/profile/includes/general/registration_authority_change')
                    </div>
                </div>
            </div>

        @endif
    </div>
</div>

