


{{--3--}}
<div class=" row filed-layout">
    <div class="col-md-12">
        <div class="col-md-4">
            <label class="required">Tin</label>
            <div class="form-group">
                {!! Form::input( 'tin','tin', $employer->tin, ['class' => 'form-control']) !!}
                {!! $errors->first('tin', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

        <div class="col-md-4">
            <label class="required">Date of Commencement</label>
            <div class="form-group">
                <div class="form-inline">

                    <div class="input-group" style="width:100%;">
                        {!! Form::text('doc',  isset($employer->doc) ? short_date_format($employer->doc) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <small class="help-block">
                        <p>Date of commencement of business</p>
                    </small>
                    {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>



        <div class="col-md-4">
            <label class="">Vote</label>
            <div class="form-group">
                {!! Form::input( 'text','vote', $employer->vote, ['class' => 'form-control']) !!}
                {!! $errors->first('vote', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>




{{--4--}}
<div class=" row filed-layout">
    <div class="col-md-12">
        {{--<div class="col-md-4">--}}
            {{--<label class="">Vote</label>--}}
            {{--<div class="form-group">--}}
                {{--{!! Form::input( 'text','telephone', $employer->vote, ['class' => 'form-control']) !!}--}}
                {{--{!! $errors->first('vote', '<span class="help-block label label-danger">:message</span>') !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>




