@extends('layouts.backend.main', ['title' => 'Attach Documents', 'header_title' => 'Attach Documents'])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {!! Form::open(['route' => ['backend.compliance.employer.change_particular.store_document'], 'name' => 'employer_particular_change', 'class' => 'attach_doc', 'method' => 'post','enctype' => 'multipart/form-data']) !!}
    {{--main contents--}}
    {!! Form::hidden('employer_particular_change_id', $employer_particular_change->id, ['class' =>'']) !!}
    {!! Form::hidden('action_type', 1, ['class' =>'']) !!}
    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
    <br/>

    {{--<div>--}}

    <br/>
    {{--</div>--}}



    <br/>
    {{--document types--}}
    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Document type:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::select('document_id', $document_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'document_type']) !!}

                        {!! $errors->first('document_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


    {{--document title--}}
    <div class="row option_div" id="doc_title_div">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Document title:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::input( 'text','document_title', null, ['class' => 'form-control option_input', 'id' => 'document_title']) !!}
                        {!! $errors->first('document_title', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--ATtach file input--}}
    <div class="row ">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Attach document (PDF):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::file('document_file') !!}
                        {!! $errors->first('document_file', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>



    {{--Return on this page--}}

    <div class="row">
        <div class="col-md-9">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>&nbsp;</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        {!! Form::checkbox('return_page', '1', false, ['id' => 'return_page', 'class' => 'return_page']) !!}  Return to this page
                    </div>
                </div>
            </div>

        </div>
    </div>


    <br/>

    {{--Buttons--}}
    <div class="row">
        <div class="col-md-6" class="form-inline" >
            <div class="element-form">
                <div class="col-xl-2 col-lg-2 col-sm-12 col-md-3 text-xs-right"></div>
                <div class="col-xl-7 col-lg-7 col-sm-12 col-md-9 col-xs-12">
                    <div class="pull-right">

                        {!! link_to_route('backend.compliance.employer.change_particular.profile',trans('buttons.general.cancel'), [$employer_particular_change->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                        {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>


    {!! Form::close() !!}
    {{--</section>--}}

@stop



@push('after-script-end')
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    {{--<script  type="text/javascript">--}}



    <script>
        $(function () {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                // maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            documentOption();
            $("#document_type").on("change", function (e) {
                documentOption();
            });




            //Document type option
            function documentOption() {

                var choice = $("#document_type").val();

                /*Hide and disable all*/
                $(".option_div").hide();
                $(".option_input").prop('disabled', true);

                switch (choice){
                    // case '85':
                    //     $("#date_ref_div").show();
                    //     $("#date_reference").prop('disabled', false);
                    //     break;

                    case '86':
                        $("#doc_title_div").show();
                        $("#document_title").prop('disabled', false);
                        break;
                    default:
                        $("#date_ref_div").hide();
                        $("#date_reference").prop('disabled', true);

                        /*Doc title*/
                        $("#doc_title_div").hide();
                        $("#document_title").prop('disabled', true);
                        break;
                }

            }

        });

    </script>;


@endpush