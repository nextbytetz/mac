@extends('layouts.backend.main', ['title' => 'Employer Particular Changes', 'header_title' => 'Employer Particular Changes'])

@include('backend.includes.datatable_assets')

@section('content')

    <div class = "row">
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        {{--<br/>--}}
        <div class="col-md-12" >
            <br/>
            <div class="col-md-12" >

                <div class="pull-right">


                    <div class="btn-group">
                        <a href="{!! route('backend.compliance.employer.change_particular.create', $employer->id) !!}"  class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-plus"></i>&nbsp;Add New</a>
                    </div>
                </div>



            </div>
        </div>


    </div>

    <br/>
    {{--Table--}}
    <div class = "row">
        <div class="col-md-12" >
            <table class="display" cellspacing="0" width="100%" id ="employer-changes-table">
                <thead>
                <tr >
                    <th>Remark</th>
                    <th>Status</th>
                    <th>Approved Date</th>
                    <th>Payroll</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>
@stop


@push('after-script-end')

    <script  type="text/javascript">
        $(function() {
            var url = "{!! url("/") !!}";
            $('#employer-changes-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                },
                ajax:{
                    url : '{!! route('backend.compliance.employer.change_particular.get_by_employer', $employer->id) !!}',
                    type : 'post'
                },
                columns: [
                    { data: 'remark', name: 'remark', orderable : true, searchable : true},
                    { data: 'status', name: 'status',  orderable : false, searchable : false},
                    { data: 'approved_date_formatted', name: 'wf_done_date' ,orderable : true, searchable : true},
                    { data: 'payroll_id', name: 'wf_done_date' ,orderable : true, searchable : true},


                ],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = url +  "/compliance/employer/change_particular/profile/" + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                }

            });

        });


    </script>;

@endpush
