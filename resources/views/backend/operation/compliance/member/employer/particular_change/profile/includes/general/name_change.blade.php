
{{--Name changes--}}

{{--@if($employer_particular_change->changeTypes()->where('particular_change_type_cv_id', 433)->first()->name_change_type_cv_id == 438)--}}
@if($change_repo->checkIfNameChangeTypeExists($employer_particular_change->id, 'EMNCTNA'))
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-left "><label>Name change type:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                        {{--<strong>{!! $employer_particular_change->changeTypes()->where('particular_change_type_cv_id', 433)->where('name_change_type_cv_id', 438)->first()->nameChangeType->name  !!}</strong>--}}
               <strong>{!! $change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', 'EMNCTNA')->first()->nameChangeType->name  !!}</strong>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="row">
        {{--left general  summary--}}
        <div class="col-md-12">
            <div class="col-md-6">
                <legend id="old_values"  style="background-color: lightgrey;text-align:center">Old Value</legend>
                <table class="table table-striped table-bordered" id="old_values_table">
                    <tbody>

                    @foreach(unserialize($change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', 'EMNCTNA')->first()->old_values) as $key => $old_value)
                        <tr>
                            <th>{!! $old_value !!} </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

            <div class="col-md-6">
                <legend id="new_values"  style="background-color: lightgrey;text-align:center">New Value</legend>
                <table class="table table-striped table-bordered" id="new_values_table">
                    <tbody>
                    @foreach(unserialize($change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', 'EMNCTNA')->first()->new_values) as $key => $new_value)
                        <tr>
                            <th>{!! $new_value !!} </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endif


{{--Merger employer--}}


{{--@if($employer_particular_change->changeTypes()->where('particular_change_type_cv_id', 433)->first()->name_change_type_cv_id == 436)--}}
@if($change_repo->checkIfNameChangeTypeExists($employer_particular_change->id, 'EMNCTME'))
    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Name change type:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                        <strong>{!! $change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', 'EMNCTME')->first()->nameChangeType->name  !!}</strong>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Employer to Merge to (Old regno):</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                        {{--{!! $errors->first('registration_authority_cv_id', '<span class="help-block label label-danger">:message</span>') !!}--}}
                        @foreach(unserialize($change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', 'EMNCTME')->first()->new_values) as $key => $new_value)
                            {{--<strong> {!! $new_value !!}</strong>--}}

                            {!! Form::text('merge_employer_id', $employer_repo->findWithoutScopeOrThrowException($new_value)->name, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control', 'id'=> 'merge_employer_id', 'disabled']) !!}
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </div>


@endif

{{--Other--}}

{{--@if($employer_particular_change->changeTypes()->where('particular_change_type_cv_id', 433)->first()->name_change_type_cv_id == 437)--}}
@if($change_repo->checkIfNameChangeTypeExists($employer_particular_change->id, 'EMNCTSPL'))

    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-left "><label>Name change type:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">

                        <strong>{!! $change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', 'EMNCTSPL')->first()->nameChangeType->name  !!}</strong>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="element-form" >
                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-left "><label>Employer to split:</label></div>
                <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group text_content">

                        {{--{!! $errors->first('registration_authority_cv_id', '<span class="help-block label label-danger">:message</span>') !!}--}}
                        @foreach(unserialize($change_repo->getChangeType($employer_particular_change->id, 'EMTCNAME', 'EMNCTSPL')->first()->new_values) as $key => $new_value)
                            {{--<strong> {!! $new_value !!}</strong>--}}
                            {!! Form::text('split_employer_id', $employer_repo->findWithoutScopeOrThrowException($new_value)->name_with_reg_no, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control', 'id'=> 'split_employer_id', 'disabled']) !!}
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </div>

@endif


@if($check_pending_level1 == 1 || $check_workflow == 0)
    <br/>
    Note: <b style="font-style: italic; font-size: 10px;"> Attach Board Resolution and/or Any Other Relevant Document(s) </b>
@endif