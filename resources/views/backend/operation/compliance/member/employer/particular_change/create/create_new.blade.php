@extends('layouts.backend.main', ['title' => "Change Employer Particulars", 'header_title' => "Change Employer Particulars"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')

    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */

        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.compliance.employer.change_particular.store'], 'name' => 'employer_change_particular', 'class' => 'employer_change_particular', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}
        {!! Form::hidden('action_type', 1) !!}
        {!! Form::hidden('this_date', getTodayDate()) !!}

        <legend style="background-color: #e2e2e2;text-align:left">General Information </legend>

        <br/>


        <div class="col-md-8">



            <div class="card-accordions">
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#name_change" aria-expanded="false">1. Name Change</a>
                            </h5>
                        </div>
                        <div id="name_change" class="expand" aria-expanded="true" style="">
                            <div class="card-block">
                                @include('backend/operation/compliance/member/employer/particular_change/create/includes/name_change')
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#general_change" aria-expanded="false">2. General Change</a>
                            </h5>
                        </div>
                        <div id="general_change" class="expand" aria-expanded="false">
                            <div class="card-block">
                                @include('backend/operation/compliance/member/employer/particular_change/create/includes/general_change')
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#address_change" aria-expanded="false">3. General: Address Change</a>
                            </h5>
                        </div>
                        <div id="address_change" class="expand" aria-expanded="false">
                            <div class="card-block">
                                @include('backend/operation/compliance/member/employer/particular_change/create/includes/address_change')
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#contact_change" aria-expanded="false">4. General: Contact Change</a>
                            </h5>
                        </div>
                        <div id="contact_change" class="expand" aria-expanded="false">
                            <div class="card-block">
                                @include('backend/operation/compliance/member/employer/particular_change/create/includes/contact_change')
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <h5 class="mb-0">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#registration_change" aria-expanded="false">5. Registration Authority Change</a>
                            </h5>
                        </div>
                        <div id="registration_change" class="expand" aria-expanded="false">
                            <div class="card-block">
                                @include('backend/operation/compliance/member/employer/particular_change/create/includes/registration_change')
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>


        <div class="col-md-4">
            {{--7--}}
            <div class=" row filed-layout">

                <div class="col-md-12">

                    <div class="col-md-12">
                        <label class="required">Date received</label>
                        <div class="form-group">
                            <div class="form-inline">

                                <div class="input-group" style="width:100%;">
                                    {!! Form::text('date_received',  null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                                </div>
                                <small class="help-block">
                                    <p>Date when request was received</p>
                                </small>
                                {!! $errors->first('date_received', '<span class="help-block label label-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    {{--<br/>--}}
                    <div class="col-md-12">
                        <label class="required">Remark</label>
                        <div class="form-group text_content">
                            {!! Form::textarea('remark', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                            {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                    <div class="col-md-4">

                    </div>

                    <div class="col-md-4">

                    </div>
                </div>


                <br/>
                {{--<hr/>--}}
                {{--Buttons--}}
                <div class="row">
                    <div class="col-md-12" class="form-inline" >
                        <div class="element-form">
                            <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                            <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                                <div class="pull-right">

                                    {!! link_to_route('backend.compliance.employer.change_particular.index',trans('buttons.general.cancel'), [$employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                                    {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        {!! Form::close() !!}


    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            location_type_option('location_type', 'surveyed','unsurveyed');
            $("#location_type").on('change', function (e){
                location_type_option('location_type', 'surveyed','unsurveyed');
            });



            $('#country_id').on('change', function (e) {
                $("#spin2").show();
                var country_id = e.target.value;
                $.get("{{ url('/') }}/getRegions?country_id=" + country_id, function (data) {
                    $('#region_id').empty();
                    $("#region_id").select2("val", "");
                    $('#region_id').html(data);
                    $("#spin2").hide();
                });
            });


            $('#region_id').on('change', function (e) {
                $("#spin2").show();
                var region_id = e.target.value;
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district_id').empty();
                    $("#district_id").select2("val", "");
                    $('#district_id').html(data);
                    $("#spin2").hide();
                });
            });


            $(".employer-select").select2({
                minimumInputLength: 3,
                multiple: false,
                ajax: {
                    url: "{!! route('backend.compliance.employers') !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term || "",
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                };
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });


            // check if location type is selected -> survyed and unsurveyed
            function location_type_option(location_type, surveyed,unsurveyed) {

                var choice = $("#"+location_type).val();
                switch (choice) {
                    case '1':
                        $("." + surveyed).show();
                        $("." + unsurveyed).hide();
                        break;
                    case '2':
                        $("."+surveyed).hide();
                        $("."+unsurveyed).show();
                        break;
                    default:
                        $("." + surveyed).hide();
                        $("." + unsurveyed).hide();
                }
            }

        });
    </script>

@endpush