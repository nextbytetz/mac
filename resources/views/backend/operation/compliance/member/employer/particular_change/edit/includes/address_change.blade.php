


{{--3--}}
<div class=" row filed-layout">
    <div class="col-md-12">
        <div class="col-md-4">
            <label class="">P.O Box</label>
            <div class="form-group">
                {!! Form::input( 'text','box_no',  isset($general_new_values['box_no']) ? $general_new_values['box_no']: $employer->box_no, ['class' => 'form-control']) !!}
                {!! $errors->first('box_no', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>


        <div class="col-md-4">
            <label class="required">Country</label>
            <div class="form-group">
                {!! Form::select('country_id', $countries, 1, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'country_id']) !!}
                {!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>


        <div class="col-md-4">
            <label class="required">Region</label>
            <div class="form-group">
                {!! Form::select('region_id', $regions,   isset($general_new_values['region_id']) ? $general_new_values['region_id']: (($employer->region_id) ?  $employer->region_id : ''), ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>




{{--4--}}
<div class=" row filed-layout">
    <div class="col-md-12">

        {{--</div>--}}
        <div class="col-md-4">
            <label class="required">District</label>
            <div class="form-group">
                {!! Form::select('district_id', $districts, isset($general_new_values['district_id']) ? $general_new_values['district_id']:  (($employer->district_id) ? $employer->district_id : null), ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

        <div class="col-md-4">
            <label class="required">Location Types</label>
            <div class="form-group">
                {!! Form::select('location_type_id', $location_types, isset($general_new_values['location_type_id']) ? $general_new_values['location_type_id']: $employer->location_type_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'location_type']) !!}
                {!! $errors->first('location_type_id', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
    </div>
</div>

{{--5--}}
<div class=" row filed-layout">
    <div class="col-md-12">

        <div class="col-md-4 surveyed">
            <label class="">Street</label>
            <div class="form-group">
                {!! Form::input( 'text','street', isset($general_new_values['street']) ? $general_new_values['street']: $employer->street, ['class' => 'form-control']) !!}
                {!! $errors->first('street', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
        {{--</div>--}}
        <div class="col-md-4 surveyed">
            <label class="">Road</label>
            <div class="form-group">
                {!! Form::input( 'text','road', isset($general_new_values['road']) ? $general_new_values['road']: $employer->road, ['class' => 'form-control']) !!}
                {!! $errors->first('road', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

        <div class="col-md-4 surveyed">
            <label class="">Plot no</label>
            <div class="form-group">
                {!! Form::input( 'text','plot_no',isset($general_new_values['plot_no']) ? $general_new_values['plot_no']: $employer->plot_no, ['class' => 'form-control']) !!}
                {!! $errors->first('plot_no', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>
    </div>
</div>



{{--7--}}
<div class=" row filed-layout">
    <div class="col-md-12">


        <div class="col-md-4 surveyed">
            <label class="">Block No.</label>
            <div class="form-group">
                {!! Form::input( 'text','block_no', isset($general_new_values['block_no']) ? $general_new_values['block_no']: $employer->block_no, ['class' => 'form-control']) !!}
                {!! $errors->first('block_no', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

        <div class="col-md-4 surveyed">
            <label class="">Surveyed Extra</label>
            <div class="form-group">
                {!! Form::input( 'text','surveyed_extra', isset($general_new_values['surveyed_extra']) ? $general_new_values['surveyed_extra']: $employer->surveyed_extra, ['class' => 'form-control']) !!}
                {!! $errors->first('surveyed_extra', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

        <div class="col-md-4 unsurveyed">
            <label class="">Unsurveyed Area</label>
            <div class="form-group text_content">
                {!! Form::textarea('unsurveyed_area', isset($general_new_values['unsurveyed_area']) ? $general_new_values['unsurveyed_area']: $employer->unsurveyed_area, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                {!! $errors->first('unsurveyed_area', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>


        <div class="col-md-4">
            {{--<label class="required">Location Types</label>--}}
            {{--<div class="form-group">--}}

            {{--</div>--}}
        </div>
    </div>
</div>




