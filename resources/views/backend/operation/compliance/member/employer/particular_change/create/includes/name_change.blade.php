<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>Name change type:</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('name_change_type_cv_id', $name_change_types, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'name_change_type']) !!}
                    {!! $errors->first('name_change_type_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row name_optional_div" id="merge_employer_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Employer to merge (Old Reg No.):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('merge_employer_id', [], old('merge_employer_id'), ['style' => 'width:100%','class' => 'form-control employer-select name_optional_input', 'id'=> 'merge_employer_id']) !!}

                    {!! $errors->first('merge_employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row name_optional_div" id="split_employer_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Employer to split:</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('split_employer_id', $duplicate_employers_merged, old('merge_employer_id'), ['style' => 'width:100%','class' => 'form-control search-select name_optional_input', 'id'=> 'split_employer_id', 'placeholder' => '']) !!}

                    {!! $errors->first('split_employer_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>



<div class="row name_optional_div" id="new_name_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>New Name:</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group ">
                    {!! Form::text('new_name', null, ['class' => 'form-control name_optional_input' ,  'id' => 'new_name',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                    {!! $errors->first('new_name', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>


<div class="row name_optional_div" id="other_name_change_div">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Other (Please specify):</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group text_content">
                    {!! Form::textarea('other_name_change', null, ['class' => 'form-control name_optional_input' , 'id' => 'other_name_change',   'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                    {!! $errors->first('other_name_change', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>




@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}--}}
    <script>
        $(function() {
            // $(".search-select").select2({});

            nameChangeOption();
            $("#name_change_type").on('change', function (e){
                nameChangeOption();
            });


            function nameChangeOption()
            {
                var choice = $('#name_change_type').val();

                switch(choice){
                    case 'EMNCTME':
                        hide_show('hide_class', 'name_optional_div');
                        enable_disable('disable_class', 'name_optional_input');
                        /*show merge employer*/
                        hide_show('show_id', 'merge_employer_div');
                        enable_disable('enable_id', 'merge_employer_id');
                        break;


                    case 'EMNCTSPL':
                        hide_show('hide_class', 'name_optional_div');
                        enable_disable('disable_class', 'name_optional_input');
                        /*show merge employer*/
                        hide_show('show_id', 'split_employer_div');
                        enable_disable('enable_id', 'split_employer_id');
                        break;


                    case 'EMNCTNA':
                        hide_show('hide_class', 'name_optional_div');
                        enable_disable('disable_class', 'name_optional_input');
                        /*show merge employer*/
                        hide_show('show_id', 'new_name_div');
                        enable_disable('enable_id', 'new_name');
                        break;

                    case 'EMNCTRN':
                        hide_show('hide_class', 'name_optional_div');
                        enable_disable('disable_class', 'name_optional_input');
                        /*show merge employer*/
                        hide_show('show_id', 'other_name_change_div');
                        enable_disable('enable_id', 'other_name_change');
                        break;

                    default:
                        hide_show('hide_class', 'name_optional_div');
                        enable_disable('disable_class', 'name_optional_input');

                        break;

                }
            }

            /*Work for element id*/
            function enable_disable(action_type, element_id)
            {
                switch (action_type) {
                    case 'enable_id':
                        $("#"+element_id).prop("disabled", false);
                        break;
                    case 'disable_id':
                        $("#"+element_id).prop("disabled", true);
                        break;
                    case 'enable_class':
                        $("."+element_id).prop("disabled", false);
                        break;
                    case 'disable_class':
                        $("."+element_id).prop("disabled", true);
                        break;
                }
            }

            /*End - Enable_Disable*/

            /* start ---HIDE - SHOW attributes of elements*/

            /*Work for element id*/
            function hide_show(action_type, element_id)
            {
                switch (action_type) {
                    case 'hide_id':
                        $("#" + element_id).hide();
                        break;
                    case 'show_id':
                        $("#"+ element_id).show();
                        break;
                    case 'hide_class':
                        $("." + element_id).hide();
                        break;
                    case 'show_class':
                        $("."+ element_id).show();
                        break;
                }
            }


        });
    </script>

@endpush