@extends('layouts.backend.main', ['title' => "Change Employer Particulars", 'header_title' => "Change Employer Particulars"])

@include('backend.includes.assets.datetimepicker')
@push('after-styles-end')

    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
    <style>
        /* start: upload progress bar css */

        /* end: upload progress bar css */
    </style>
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->
    <div class="row">
        {{--HEADER--}}
        @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])
        <div>&nbsp;<br/></div>
        {!! Form::open(['route' => ['backend.compliance.employer.change_particular.store'], 'name' => 'employer_change_particular', 'class' => 'employer_change_particular', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}
        {!! Form::hidden('action_type', 1) !!}
        {!! Form::hidden('this_date', getTodayDate()) !!}

        <legend style="background-color: #e2e2e2;text-align:left">General Information </legend>

        <br/>
        <div class=" row filed-layout">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="required">Name</label>
                    <div class="form-group text_content">
                        {!! Form::textarea('name', $employer->name, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                {{--</div>--}}
                <div class="col-md-4">
                    <label class="required">Tin</label>
                    <div class="form-group">
                        {!! Form::input( 'text','tin', $employer->tin, ['class' => 'form-control number', 'id'=> 'tin', 'disabled']) !!}
                        <i class="fa fa-spinner fa-spin" id = "spin1" style='display: none'></i>
                        {!! $errors->first('tin', '<span class="help-block label label-danger">:message</span>') !!}
                        {!! Form::hidden('tin', $employer->tin) !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <label class="required">Doc</label>
                    <div class="form-group">
                        <div class="form-inline">

                            <div class="input-group" style="width:100%;">
                                {!! Form::text('doc',  isset($employer->doc) ? short_date_format($employer->doc) : null, ['placeholder' => '', 'class' => 'form-control datepicker1', 'autocomplete' => 'off']) !!}
                                <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                            </div>
                            <small class="help-block">
                                <p>Date of commencement of business</p>
                            </small>
                            {!! $errors->first('doc', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--2--}}
        <div class=" row filed-layout">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="required">Phone</label>
                    <div class="form-group">
                        {!! Form::input( 'text','phone', $employer->phone, ['class' => 'form-control']) !!}
                        {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                {{--</div>--}}
                <div class="col-md-4">
                    <label class="">Telephone</label>
                    <div class="form-group">
                        {!! Form::input( 'text','telephone', $employer->telephone, ['class' => 'form-control']) !!}
                        {!! $errors->first('telephone', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <label class="required">Email</label>
                    <div class="form-group">
                        {!! Form::input( 'text','email', $employer->email, ['class' => 'form-control']) !!}
                        {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>



        {{--3--}}
        <div class=" row filed-layout">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="">P.O Box</label>
                    <div class="form-group">
                        {!! Form::input( 'text','box_no', $employer->box_no, ['class' => 'form-control']) !!}
                        {!! $errors->first('box_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                {{--</div>--}}
                <div class="col-md-4">
                    <label class="">Fax</label>
                    <div class="form-group">
                        {!! Form::input( 'text','fax', $employer->fax, ['class' => 'form-control']) !!}
                        {!! $errors->first('fax', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <label class="required">Country</label>
                    <div class="form-group">
                        {!! Form::select('country_id', $countries, 1, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'country_id']) !!}
                        {!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>




        {{--4--}}
        <div class=" row filed-layout">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="required">Region</label>
                    <div class="form-group">
                        {!! Form::select('region_id', $regions, ($employer->region_id) ?  $employer->region_id : '', ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'region_id']) !!}
                        {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                {{--</div>--}}
                <div class="col-md-4">
                    <label class="required">District</label>
                    <div class="form-group">
                        {!! Form::select('district_id', $districts, ($employer->district_id) ? $employer->district_id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select','id'=> 'district_id']) !!}
                        {!! $errors->first('district_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <label class="required">Location Types</label>
                    <div class="form-group">
                        {!! Form::select('location_type_id', $location_types, $employer->location_type_id, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id' => 'location_type']) !!}
                        {!! $errors->first('location_type_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        {{--5--}}
        <div class=" row filed-layout">
            <div class="col-md-12">

                <div class="col-md-4 surveyed">
                    <label class="">Street</label>
                    <div class="form-group">
                        {!! Form::input( 'text','street', $employer->street, ['class' => 'form-control']) !!}
                        {!! $errors->first('street', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                {{--</div>--}}
                <div class="col-md-4 surveyed">
                    <label class="">Road</label>
                    <div class="form-group">
                        {!! Form::input( 'text','road', $employer->road, ['class' => 'form-control']) !!}
                        {!! $errors->first('road', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-4 surveyed">
                    <label class="">Plot no</label>
                    <div class="form-group">
                        {!! Form::input( 'text','plot_no', $employer->plot_no, ['class' => 'form-control']) !!}
                        {!! $errors->first('plot_no', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>



        {{--7--}}
        <div class=" row filed-layout">
            <div class="col-md-12">
                <div class="col-md-4 surveyed">
                    <label class="">Surveyed Extra</label>
                    <div class="form-group">
                        {!! Form::input( 'text','surveyed_extra', $employer->surveyed_extra, ['class' => 'form-control']) !!}
                        {!! $errors->first('surveyed_extra', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-4 unsurveyed">
                    <label class="">Unsurveyed Area</label>
                    <div class="form-group text_content">
                        {!! Form::textarea('unsurveyed_area', $employer->unsurveyed_area, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                        {!! $errors->first('unsurveyed_area', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>
                {{--</div>--}}
                <div class="col-md-4">
                    <label class="required">Nature of Business</label>
                    <div class="form-group">
                        {!! Form::select('business_sector_cv_id', $business_sectors, ($employer->sectors()->count()) ? $employer->sectors->first()->id : null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select']) !!}
                        {!! $errors->first('business_sector_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-4">
                    {{--<label class="required">Location Types</label>--}}
                    {{--<div class="form-group">--}}

                    {{--</div>--}}
                </div>
            </div>
        </div>

        {{--7--}}
        <div class=" row filed-layout">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="required">Remark</label>
                    <div class="form-group text_content">
                        {!! Form::textarea('remark', null, ['class' => 'form-control' , 'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal; !important']) !!}
                        {!! $errors->first('remark', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-md-4">

            </div>

            <div class="col-md-4">

            </div>
        </div>
        {{--</div>--}}


        {{--</div>--}}

        {{--</div>--}}


        <br/>
        {{--<hr/>--}}
        {{--Buttons--}}
        <div class="row">
            <div class="col-md-6" class="form-inline" >
                <div class="element-form">
                    <div class="col-xl-5 col-lg-3 col-sm-12 col-md-3
                    text-xs-right"></div>
                    <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                        <div class="pull-right">

                            {!! link_to_route('backend.compliance.employer.change_particular.index',trans('buttons.general.cancel'), [$employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}
                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>



        {!! Form::close() !!}

    </div>
@endsection

@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
    <script>
        $(function() {
            $(".search-select").select2({});

            $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

            /*------------Start Date Process ---------*/
            var today_date = new Date;
            var dd = today_date.getDate();
            var mm = today_date.getMonth() + 1; //January is 0!
            var yyyy = today_date.getFullYear();

            today_date = yyyy + '/' + mm + '/' + dd;

            jQuery('.datepicker1').datetimepicker({
                timepicker:false,
                format:'d-M-Y',
                weeks: false,
                dayOfWeekStart: 1,
                lazyInit: true,
                scrollInput: false,
                maxDate: today_date,
            });
            /*-----------End Date Process------------*/

            location_type_option('location_type', 'surveyed','unsurveyed');
            $("#location_type").on('change', function (e){
                location_type_option('location_type', 'surveyed','unsurveyed');
            });



            $('#country_id').on('change', function (e) {
                $("#spin2").show();
                var country_id = e.target.value;
                $.get("{{ url('/') }}/getRegions?country_id=" + country_id, function (data) {
                    $('#region_id').empty();
                    $("#region_id").select2("val", "");
                    $('#region_id').html(data);
                    $("#spin2").hide();
                });
            });


            $('#region_id').on('change', function (e) {
                $("#spin2").show();
                var region_id = e.target.value;
                $.get("{{ url('/') }}/getDistricts?region_id=" + region_id, function (data) {
                    $('#district_id').empty();
                    $("#district_id").select2("val", "");
                    $('#district_id').html(data);
                    $("#spin2").hide();
                });
            });


            // check if location type is selected -> survyed and unsurveyed
            function location_type_option(location_type, surveyed,unsurveyed) {

                var choice = $("#"+location_type).val();
                switch (choice) {
                    case '1':
                        $("." + surveyed).show();
                        $("." + unsurveyed).hide();
                        break;
                    case '2':
                        $("."+surveyed).hide();
                        $("."+unsurveyed).show();
                        break;
                    default:
                        $("." + surveyed).hide();
                        $("." + unsurveyed).hide();
                }
            }

        });
    </script>

@endpush