<div class="row">
    <div class="col-md-12">
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right "><label>Registration Authority:</label></div>
            <div class="col-xs-5 col-lg-5 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::select('registration_authority_cv_id', $registration_authorities, null, ['style' => 'width:100%', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'registration_authority_cv_id']) !!}

                    {!! $errors->first('registration_authority_cv_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>
</div>





@push('after-script-end')
    <!-- Custom javascript files for this page -->
    {{--{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}--}}
    <script>
        $(function() {
            // $(".search-select").select2({});



        });
    </script>

@endpush