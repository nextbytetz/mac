


{{--3--}}
<div class=" row filed-layout">
    <div class="col-md-12">
        <div class="col-md-4">
            <label class="required">Phone</label>
            <div class="form-group">
                {!! Form::input( 'text','phone', isset($general_new_values['phone']) ? $general_new_values['phone']: $employer->phone, ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>


        <div class="col-md-4">
            <label class="">Telephone</label>
            <div class="form-group">
                {!! Form::input( 'text','telephone', isset($general_new_values['telephone']) ? $general_new_values['telephone']: $employer->telephone, ['class' => 'form-control']) !!}
                {!! $errors->first('telephone', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>


        <div class="col-md-4">
            <label class="required">Email</label>
            <div class="form-group">
                {!! Form::input( 'text','email', isset($general_new_values['email']) ? $general_new_values['email']: $employer->email, ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>




{{--4--}}
<div class=" row filed-layout">
    <div class="col-md-12">

        {{--</div>--}}
        <div class="col-md-4">
            <label class="">Fax</label>
            <div class="form-group">
                {!! Form::input( 'text','fax', isset($general_new_values['fax']) ? $general_new_values['fax']: $employer->fax, ['class' => 'form-control']) !!}
                {!! $errors->first('fax', '<span class="help-block label label-danger">:message</span>') !!}
            </div>
        </div>

    </div>
</div>




