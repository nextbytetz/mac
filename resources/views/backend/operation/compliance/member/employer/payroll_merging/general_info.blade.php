<div class="card-accordions">
    <div id="accordion" role="tablist" aria-multiselectable="true">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#general_change" aria-expanded="false">{{$merge_request->merge_type == 1 ? 'Payroll Merge' : 'Payrolls To Combine'}}</a>
                </h5>
            </div>
            <div id="general_change" class="expand" aria-expanded="false">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-6">
                            <legend id="old_values"  style="background-color: lightgrey;text-align:center">Old Payrolls</legend>
                            <table class="table table-striped table-bordered" id="old_values_table">
                                <tbody>
                                    @foreach(unserialize($merge_request->old_users) as $key => $old_value)
                                    <tr>
                                        <td width="40px">{!! ucfirst(str_replace('_',' ',str_replace('_id','',$key+1))) !!}:</td>
                                        <th>{!!$merge_request->getOldUserFormattedAttribute($key) !!} </th>
                                        <td>
                                            <a href="#" data-payroll="{{$merge_request->getOldUSerPayroll($key)}}" class="show_merge_summary small underline">
                                                (<span class="show-text" style="color: green;">Show Summary</span>)
                                            </a> 
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <legend id="new_values"  style="background-color: lightgrey;text-align:center">Merge With Payrolls</legend>
                            <table class="table table-striped table-bordered" id="new_values_table">
                                <tbody>
                                    
                                    {{-- @foreach(unserialize($merge_request->new_user) as $key => general_info) --}}
                                    <tr>
                                        <td width="40px">{!! ucfirst(str_replace('_',' ',str_replace('_id','',1))) !!}:</td>
                                        <th>{!! $merge_request->new_user_formatted !!} </th>
                                        <td>
                                            <a href="#" data-payroll="{{$merge_request->getNewUserPayrollAttribute(0)}}" class="show_merge_summary small underline">
                                                (<span class="show-text" style="color: green;">Show Summary</span>)
                                            </a> 
                                        </td>
                                    </tr>
                                    {{-- @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('after-script-end')

<script  type="text/javascript">


    $(function () {
     $('.show_merge_summary').on('click', function ($e) {
        $e.preventDefault();
        $('#payroll_summary').empty();
        $('.payroll_summary_title').text('');
        if ($(this).data('payroll')) {
            $('.payroll_summary_title').text('Payroll '+$(this).data('payroll')+' As of Submission Date');
            payrollSummary($(this).data('payroll'),1);
            setTimeout(function(){
                $('#show_merge_summary_modal').modal('show');
            }, 200);
        }

    });

     $('.show_approval_summary').on('click', function ($e) {
        $e.preventDefault();
        $('#payroll_summary').empty();
        $('.payroll_summary_title').text('');
        if ($(this).data('payroll')) {
            $('.payroll_summary_title').text('Payroll '+$(this).data('payroll')+' After Merging Approval');
            payrollSummary($(this).data('payroll'),2);
            setTimeout(function(){
                $('#show_merge_summary_modal').modal('show');
            }, 200);
        }
    });


     $('#payroll_summary').on('click', '.view_summary_details', function(event){
        event.preventDefault();
        let details_type = $(this).attr('type');
        let details_field = $(this).attr('field');
        let details_payroll_id  = $(this).attr('payroll');

        $('#summary_details_body').empty();
        $('.summary_details_title').text('');
        if (details_payroll_id && details_field && details_type) {
            payrollSummaryDetails(details_payroll_id, details_type, details_field);
        }

    });

     function payrollSummary(payroll_id, type) {
      $.ajax({
        url : '{{url("compliance/employer/payroll_merge/".$merge_request->id."/payroll_summary/")}}/'+payroll_id+'/'+type,
        type : 'GET',
        datatype : 'json',
        success:function(data){
          if (data.summary) {
            $('#payroll_summary').append(data.summary);
        } 
    }});
  }

  function payrollSummaryDetails(payroll_id, type, what_to_return) {
      $.ajax({
        url : '{{url("compliance/employer/payroll_merge/".$merge_request->id."/summary_details/")}}/'+payroll_id+'/'+type+'/'+what_to_return,
        type : 'GET',
        datatype : 'json',
        success:function(data){
            if (data.details) {
                $('#summary_details_body').append('<table class="table table-striped table-bordered" id="summary_details">'+data.details+'</table>');
                $('.summary_details_title').text(data.title);
                $('#view_summary_details_modal').modal('show');
                $('#summary_details').DataTable({});
            } 
        }
    });
  }

});
</script>;

@endpush
