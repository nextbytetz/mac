
<div class = "row">
    <div class="col-md-12">

        <div class="col-md-4">

            <legend>Documents Attached
            </legend>

            <br/>

            {{----}}
            <div class="row">
                <div class="col-md-12">
                    <div class="element-form" >
                        <i class="fa fa-file-pdf-o" ></i>
                        <a  style="color:dodgerblue;" class="doc_attached"  href="#">Request Attachment</a>
                    </div>

                </div>
            </div>

        </div>




        <div class="col-md-8">
            <div class = "row">
                <div class="col-md-12">
                    {{--Document Preview--}}
                    <legend>Document Preview</legend>
                    <br/>
                    <div id="document_frame" style="text-align: center;">
                        {{--<iframe id="document_preview" name="document_preview" src="" width='100%' height='600px'></iframe>--}}
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>





@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">


    $(function () {

        $(".search-select").select2();

        let $document_frame = $("#document_frame");
        get_attached_document().done(function ($data) {
            $document_frame.find("iframe").remove();
            let $iframe = $('<iframe src="' + $data.url + '" frameborder="0"  width=\'100%\' height=\'600px\'></iframe>');
            $document_frame.append($iframe);
        });

        function get_attached_document() {

            return $.ajax({
                url: base_url + "/compliance/employer/payroll_merge/preview_document/{{$merge_request->id}}",
                dataType : 'json',
                async : false,
                method : "POST"
            });
        }


    });
</script>;

@endpush
