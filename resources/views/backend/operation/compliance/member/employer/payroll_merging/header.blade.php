<nav class="navbar navbar-light bg-light" style="background-color: #e3f2fd;">
    <span class="navbar-brand mb-0 h6">
        <strong>
           <a href="{{ route('backend.compliance.employer.online.profile', $merge_request->employer->id) }}#payroll_merging">{{$merge_request->employer->name}}</a> - Payroll Merge:
       </strong>
   </span>
   <span class="navbar-brand mb-0 h6">
    <small >
        &nbsp;&nbsp;
        {{($merge_request->merge_type == 1) ? 'Wrong Verification' : 'Payroll Combine' }} 
        &nbsp;
        Status: {!! $merge_request->status_label !!}
    </small>
</span>
</nav>
<legend></legend>
<br/>
