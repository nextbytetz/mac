

<div class="modal hide fade" id="show_merge_summary_modal" role="dialog" aria-labelledby="show_merge_summary_modal" aria-hidden="true">
   <div class="modal-dialog white_modal" role="document">
      <div class="modal-content" id="modal-content">
         <div class="modal-header">
            <span class="modal-title h5 payroll_summary_title"></span>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <table class="table table-striped table-bordered" id="payroll_summary">
            </table>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal hide fade" id="view_summary_details_modal" role="dialog" aria-labelledby="view_summary_details_modal" aria-hidden="true">
   <div class="modal-dialog  modal-lg white_modal" role="document">
      <div class="modal-content" id="modal-content">
         <div class="modal-header">
            <span class="modal-title h5 summary_details_title"></span>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" id="summary_details_body">
           
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

