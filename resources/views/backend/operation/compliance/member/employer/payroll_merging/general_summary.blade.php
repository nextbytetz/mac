<div class="row mb-1">
    <div class="col-md-12">
        <table class="table table-striped table-bordered" id="old_values_table">
            <tbody>
                <tr class="h6">
                    <th width="20%">Date Received:</th>
                    <td>{!! short_date_format($merge_request->created_at) !!} </td>
                    @if($merge_request->wf_done == 1 && $merge_request->status==2)
                    <td></td>
                    @endif
                </tr>
                @if($merge_request->merge_type == 2)
                <tr class="h6">
                    <th width="20%">User Payroll:</th>
                    <td>{!! $merge_request->new_user_formatted !!}</td>
                </tr>
                @endif

                <tr class="h6">
                    <th width="20%">Remark:</th>
                    <td>{!! $merge_request->merge_reason !!} </td>
                    @if($merge_request->wf_done == 1 && $merge_request->status==2)
                    <td></td>
                    @endif
                </tr>

                @if($merge_request->wf_done == 1 && $merge_request->status==2)
                <tr class="h6">
                    <th width="20%">Approval Date:</th>
                    <td>{!! $merge_request->approval_date !!} </td>
                    <td>
                        <a href="#" data-payroll="{{$merge_request->getNewUserPayrollAttribute($key)}}" class="show_approval_summary small underline">
                            (<span class="show-text" style="color: green;">Show Summary</span>)
                        </a> 
                    </td>
                </tr>
                @else
                <tr class="h6">
                    <th width="20%">After Approval:</th>
                    <td>
                        <a href="#" data-payroll="{{$merge_request->getNewUserPayrollAttribute($key)}}" class="show_approval_summary small underline">
                            (<span class="show-text" style="color: green;">Show Summary</span>)
                        </a> 
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
