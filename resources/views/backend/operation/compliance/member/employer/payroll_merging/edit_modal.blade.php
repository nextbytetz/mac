<div class="modal hide fade" id="edit_merge_modal" role="dialog" aria-labelledby="edit_merge_modal" aria-hidden="true">
  <div class="modal-dialog modal-lg white_modal" role="document">
    <div class="modal-content" id="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h6 class="modal-title" id="modal-title">Edit Merge {{$merge_request->employer->name}} Payroll(s)</h6>
      </div>
      <div class="modal-body" style="font-size: 14px !important;">
        {!! Form::open(['url' => route('backend.compliance.employer.payroll_merge.store'), 'name' => 'edit_merge_form', 'id' => 'edit_merge_form']) !!}
        {!! Form::hidden('employer_id', $merge_request->employer->id) !!}

        <div class="row">
          <div class="col-md-6">
            <div class="field-layout">
              <div class="form-group">
                <label for="comments">Select Merge type</label>

                <select name="merge_type" class="search-select-merge" style="width: 100%">
                  <option value="" selected="selected"></option>
                  <option value="1">Wrong Verification</option>
                  <option value="2">Payroll Combination</option>
                </select>
                <span class="help-block label label-danger error_field merge_type_error hidden mt-1"></span>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="filed-layout doc">
              <label class="required">Date Of Commencement</label>
              <div class="form-group">
                <div class="input-group">
                  <input type="text" value="{{$merge_request->doc}}" class="form-control letter_date" name="doc" id="doc" autocomplete="off" />
                  <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block label label-danger error_field doc_error hidden mt-1"></span>
              </div>
            </div>
          </div>
        </div>

        <div class="row hidden_div">
          <div class="col-md-6">
            <div class="field-layout">
              <div class="form-group keep-payrol">
                <label for="comments">Select Payroll to Keep</label>

                <select name="new_payroll" class="search-select-payroll1" style="width: 100%">
                  @foreach($online_users as $keep_user)
                  @if($keep_user->ismanage)
                  <option value="{{$keep_user->id}}" selected="{{$keep_user->id == (unserialize($merge_request->new_user)[0]) ? 'selected' : ''}}">{{$keep_user->name.' - '.$keep_user->payroll_id}}</option>
                  @endif
                  @endforeach
                </select>
                <span class="help-block label label-danger error_field new_payroll_error hidden mt-1"></span>
              </div>
              <div class="form-group manage-payrol">
                <label for="comments">Search user to manage new payroll</label>

                <select name="new_payroll_user" class="search-select-manage" style="width: 100%">
                  <option value="" selected="selected"></option>
                  <option value="[]"></option>
                </select>
                <span class="help-block label label-danger error_field new_payroll_user_error hidden mt-1"></span>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="field-layout">
              <div class="form-group">
                <label for="comments">Select Payroll to <span class="combine-remove"></span> (or deactivate)</label>
                <select name="old_payroll[]" class="search-select-payroll" style="width: 100%" multiple='true'>
                </select>
                <span class="help-block label label-danger error_field old_payroll_error hidden mt-1"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="row user-details">

        </div>
        <div class="row">
          <div class="col-md-6">
           <div class="fileld-layout">
            <label class="required">Merge Reason</label>
            <div class="form-group mt-1">
              <div class="input-group">
                {!! Form::textarea('merge_reason', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px; min-width:50%;', 'rows'=>'3']) !!}
                <span class="help-block label label-danger error_field merge_reason_error hidden mt-1"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
         <div class="fileld-layout">
          <label class="required">Request Letter / Email (Attachement pdf/image) </label>
          <div class="form-group mt-1">
            <div class="input-group">
              <input type="file" name="document_file50" id="request_letter" required="required" class="form-control">
              <span class="help-block label label-danger error_field document_file50_error hidden mt-1"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="filed-layout">
          <label class="required mb-1">Request Letter / Email Date</label>
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control letter_date" name="letter_date" id="letter_date" />
              <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
            </div>
            <span class="help-block label label-danger error_field letter_date_error hidden mt-1"></span>
          </div>
        </div>

      </div>
      <div class="col-md-6">
       <div class="fileld-layout">
        <label>Request Letter Reference </label>
        <div class="form-group mt-1">
          <div class="input-group">
            <input type="text" name="letter_reference" id="letter_reference" class="form-control">
            <span class="help-block label label-danger error_field letter_reference_error hidden mt-1"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
   <span class="footerButtons">
    <button type="button" class="btn  btn-secondary btn-submit" data-dismiss="modal">@lang("buttons.general.close")</button>
    <button type="submit" class="btn btn-primary btn-submit" >Continue</button>
  </span>
  <span class="wait hidden">
    <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  
    &nbsp; <span class="h6"> Please wait .... </span>
  </span>

</div>
{!! Form::close() !!}
</div>

</div>
</div>
</div>


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}


<script  type="text/javascript">

  $(function () {

    $(".search-select").select2({});

    $(".search-select-merge").select2({
      dropdownParent: $('#edit_merge_modal'),
    });

    $(".search-select-payroll1").select2({
      dropdownParent: $('#edit_merge_modal'),
    });

    // (unserialize($merge_request->new_user))[0]

    $(".search-select-payroll").select2({
      multiple: true,
      dropdownParent: $('#edit_merge_modal'),
    });

    $('.letter_date').datepicker({
     defaultViewDate: {
      year: '{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y')}}',
      month: '{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('m')}}',
      day: '{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d')}}',
    },
    autoclose: true,
    format: 'dd-mm-yyyy',
    endDate:  "+1d",
  });


    $('.btnEditMerge').on('click', function ($e) {  
      swal({
        title: "Dear! {{access()->user()->firstname}}",
        text: "<h6> Are you sure you want to edit this payroll merging request!?</h6>",
        type: "warning",
        html: true,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "YES",
        cancelButtonText: "NO",
        closeOnConfirm: true,
        closeOnCancel: true,
      },
      function(isConfirm) {
        if (isConfirm) {
          $('#edit_merge_modal').modal('show');
          onChangePayrollOne();
        }
      }); 
    });

    $('.search-select-payroll1').on('change', function ($e) { 
      onChangePayrollOne();
    });


    function onChangePayrollOne() {
      let keep_payroll = $('.search-select-payroll1 option:selected').val();
      $html = '';
      @foreach($online_users as $remove_user)
      $html += '<option value="{{$remove_user->id}}">{{$remove_user->name." - ".$remove_user->payroll_id}}</option>';
      @endforeach

      $(".search-select-payroll").empty();
      $(".search-select-payroll").append($html);
      if (keep_payroll) {
       $('.search-select-payroll option[value=' + keep_payroll + ']').remove();
     }

   }

   $('.search-select-merge').on('change', function ($e) { 
     let merge_type = $('.search-select-merge option:selected').val();
     $('.hidden_div').show();
     if (merge_type == 1) {
      $('.keep-payrol').show();
      $('.manage-payrol').hide();
      $('.user-details').empty()
      $(".search-select-manage").val(null).trigger('change.select2');
      $('.doc').hide();
      $('.combine-remove').text('remove');

    }else{
      $('.keep-payrol').hide();
      $('.manage-payrol').show();
      $(".search-select-payroll1").val(null).trigger('change.select2');
      $(".search-select-payroll").val(null).trigger('change.select2');
      $('.doc').show();
      $('.combine-remove').text('combine');
      getAllPayrols();
    }
  });

   /* start : Searching users */
   $(".search-select-manage").select2({
    dropdownParent: $('#edit_merge_modal'),
    minimumInputLength: 3,
    multiple: false,
    allowClear: true,
    debug: true,
    placeholder: "",
    ajax: {
      url: "{!! route('backend.compliance.online_profile.users') !!}",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term || "",
          page: params.page || 1
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            };
          }),
          pagination: {
            more: true
          }
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) {
      return markup;
    }
  }).on("select2:selecting", function($e) {
    var $selected = $e.params.args.data.id;
    getUserDetails($e.params.args.data.id)
  });

  function getUserDetails(user_id){
    $.ajax({
     type: 'get',
     url: "{!! url('compliance/online_profile/user_details') !!}/" + user_id,
     success:function(data){
      if(data){
        $('.user-details').empty()
        $('.user-details').append(data)
        getAllPayrols()
      }
    }
  });
  }

  function getAllPayrols() {
    $html = '';
    @foreach($online_users as $remove_user)
    $html += '<option value="{{$remove_user->id}}">{{$remove_user->name." - ".$remove_user->payroll_id}}</option>';
    @endforeach

    $(".search-select-payroll").empty();
    $(".search-select-payroll").append($html);
  }


});
</script>
@endpush
