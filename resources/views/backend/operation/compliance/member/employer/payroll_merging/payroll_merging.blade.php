<div class = "row">
  <div class="col-md-12" >
    <div class="nav_tab_pane_header">
      <div class="row">
        <div class="col-md-12" >
          @permission('merge_employer_payroll')
          <div class="pull-right" >
            {{--Action Links--}}
            @if(count($online_users) > 1)
            <span>
              <button class="btn btn-primary site-btn nav_button" id="merge_payroll_btn" ><i class="icon fa fa fa-thumb-tack" ></i>
                &nbsp;Merge Employer Payrolls
              </button>
            </span>
            @endif
          </div>
          @endauth
        </div>
      </div>
    </div>
    <legend class="grey_modal" >Payroll Merging</legend>
    <div>&nbsp; </div>
    <table class="display" cellspacing="0" width="100%" id ="payroll_merge_table">
      <thead>
        <tr>
          <th>Remark</th>
          <th>Status</th>
          <th>User</th>
          <th>Date Inititated</th>
          <th>Date Approved</th>
        </tr>
      </thead>
    </table>

  </div>
</div>


<div class="modal hide fade" id="merge_payroll_modal" role="dialog" aria-labelledby="merge_payroll_modal" aria-hidden="true">
  <div class="modal-dialog modal-lg white_modal" role="document">
    <div class="modal-content" id="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h6 class="modal-title" id="modal-title">Merge {{$employer->name}} Payroll(s)</h6>
      </div>
      <div class="modal-body" style="font-size: 14px !important;">
        {!! Form::open(['url' => route('backend.compliance.employer.payroll_merge.store'), 'name' => 'merge_payroll_form', 'id' => 'merge_payroll_form']) !!}
        {!! Form::hidden('employer_id', $employer->id) !!}

        <div class="row">
          <div class="col-md-6">
            <div class="field-layout">
              <div class="form-group">
                <label for="comments">Select Merge type</label>

                <select name="merge_type" class="search-select-merge" style="width: 100%">
                  <option value="" selected="selected"></option>
                  <option value="1">Wrong Verification</option>
                  <option value="2">Payroll Combination</option>
                </select>
                <span class="help-block label label-danger error_field merge_type_error hidden mt-1"></span>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="filed-layout doc">
              <label class="required">Date Of Commencement</label>
              <div class="form-group">
                <div class="input-group">
                  <input type="text" class="form-control letter_date" name="doc" id="doc" autocomplete="off" />
                  <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <span class="help-block label label-danger error_field doc_error hidden mt-1"></span>
              </div>
            </div>
          </div>
        </div>

        <div class="row hidden_div">
          <div class="col-md-6">
            <div class="field-layout">
              <div class="form-group keep-payrol">
                <label for="comments">Select Payroll to Keep</label>

                <select name="new_payroll" class="search-select-payroll1" style="width: 100%">
                  <option value="" selected="selected"></option>
                  @foreach($online_users as $keep_user)
                  @if($keep_user->ismanage)
                  <option value="{{$keep_user->id}}">{{$keep_user->name.' - '.$keep_user->payroll_id}}</option>
                  @endif
                  @endforeach
                </select>
                <span class="help-block label label-danger error_field new_payroll_error hidden mt-1"></span>
              </div>
              <div class="form-group manage-payrol">
                <label for="comments">Search user to manage new payroll</label>

                <select name="new_payroll_user" class="search-select-manage" style="width: 100%">
                  <option value="" selected="selected"></option>
                  <option value="[]"></option>
                </select>
                <span class="help-block label label-danger error_field new_payroll_user_error hidden mt-1"></span>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="field-layout">
              <div class="form-group">
                <label for="comments">Select Payroll to <span class="combine-remove"></span> (or deactivate)</label>
                <select name="old_payroll[]" class="search-select-payroll" style="width: 100%" multiple='true'>
                </select>
                <span class="help-block label label-danger error_field old_payroll_error hidden mt-1"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="row user-details">

        </div>
        <div class="row">
          <div class="col-md-6">
           <div class="fileld-layout">
            <label class="required">Merge Reason</label>
            <div class="form-group mt-1">
              <div class="input-group">
                {!! Form::textarea('merge_reason', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;border-radius: 3px; min-width:50%;', 'rows'=>'3']) !!}
                <span class="help-block label label-danger error_field merge_reason_error hidden mt-1"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
         <div class="fileld-layout">
          <label class="required">Request Letter / Email (Attachement pdf/image) </label>
          <div class="form-group mt-1">
            <div class="input-group">
              <input type="file" name="document_file50" id="request_letter" required="required" class="form-control">
              <span class="help-block label label-danger error_field document_file50_error hidden mt-1"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="filed-layout">
          <label class="required mb-1">Request Letter / Email Date</label>
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control letter_date" name="letter_date" id="letter_date" />
              <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
            </div>
            <span class="help-block label label-danger error_field letter_date_error hidden mt-1"></span>
          </div>
        </div>

      </div>
      <div class="col-md-6">
       <div class="fileld-layout">
        <label>Request Letter Reference </label>
        <div class="form-group mt-1">
          <div class="input-group">
            <input type="text" name="letter_reference" id="letter_reference" class="form-control">
            <span class="help-block label label-danger error_field letter_reference_error hidden mt-1"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
   <span class="footerButtons">
     <button type="button" class="btn  site-btn btn-secondary" data-dismiss="modal">@lang("buttons.general.close")</button>
     <button type="submit" class="btn btn-primary btn-submit" >Continue</button>
   </span>
   <span class="wait hidden">
    <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 3%; width:20%;" />  
    &nbsp; <span class="h6"> Please wait .... </span>
  </span>

</div>
{!! Form::close() !!}
</div>

</div>
</div>
</div>


<!-- =========================== Exception modal  ================================= -->
<div class="modal fade in" tabindex="-1" role="dialog" id="mdlExcelExptions">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12">
            <h6 class="alert bg-danger alert-icon-right alert-dismissible fade in mb-2">Merge Failed! Some contributions appear in multiple payrolls as shown below.</h6>
            <table class="table table-sm">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Payroll</th>
                  <th scope="col">Months</th>
                </tr>
              </thead>
              <tbody id="errorExceptions">
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn btn-lg btn-success float-sm-right" data-dismiss="modal"><i class="fa fa-thumbs-up"> </i> &nbsp;OK&nbsp;</button> 
          </div>
        </div>
     {{--  <div class="modal-footer">
        <button type="button" class="btn btn-md btn-success" data-dismiss="modal">OK</button>       
      </div> --}}
    </div>
  </div>
</div>
</div>


<!-- =========================== Excel Error / Exception modal end ================================= -->


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
<script  type="text/javascript">
  $(function() {
    let employer_id = {{$employer->id}};
    $("#mdlExcelExptions").appendTo("body");
    $('.hidden_div').hide();
    $('.keep-payrol').hide();
    $('.manage-payrol').hide();
    $('.doc').hide();

    $(".search-select-merge").select2({
      dropdownParent: $('#merge_payroll_modal'),
    });

    $(".search-select-payroll1").select2({
      dropdownParent: $('#merge_payroll_modal'),
    });

    $(".search-select-payroll").select2({
      multiple: true,
      dropdownParent: $('#merge_payroll_modal'),
    });

    $('.letter_date').datepicker({
     defaultViewDate: {
      year: '{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y')}}',
      month: '{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('m')}}',
      day: '{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d')}}',
    },
    autoclose: true,
    format: 'dd-mm-yyyy',
    endDate:  "+1d",
  });

    let payroll_merge_table = $('#payroll_merge_table').DataTable({
     processing: true,
     serverSide: true,
     stateSave: true,
     stateSaveCallback: function (settings, data) {
      localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
    },
    stateLoadCallback: function (settings) {
      return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
    },
    ajax:{
      url : '{!! route('backend.compliance.employer.payroll_merge.merge_requests',$employer->id) !!}',
      type : 'get'
    },
    columns: [
    { data: 'merge_reason', name: 'merge_reason'},
    { data: 'status', name: 'status'},
    { data: 'user_id', name: 'user_id' },
    { data: 'created_at', name:'created_at'},
    { data: 'wf_done_date', name:'wf_done_date'},
    ],
    'rowCallback': function ($nRow, $aData, $iDisplayIndex, $iDisplayIndexFull) {
      $('td', $nRow).click(function() {
        window.open(base_url + "/compliance/employer/payroll_merge/profile/" + $aData['id'], "_self");
      }).hover(function() {
        $(this).css('cursor', 'alias');
      }, function() {
        $(this).css('cursor', 'auto');
      });
    }
  });

    let $body = $('body');
    $body.on('click', 'button#merge_payroll_btn', function ($e) {
      $e.preventDefault();
      swal({
        title: "Dear! {{access()->user()->firstname}}",
        text: "<h6> Are you sure you want to merge {{strtoupper($employer->name)}} Payrolls?</h6>",
        type: "warning",
        html: true,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        cancelButtonText: "No Cancel",
        confirmButtonText: "Yes Continue",
        closeOnConfirm: true,
        closeOnCancel: true,
      },
      function(isConfirm) {
       let $modal = $('#merge_payroll_modal');
       if (isConfirm) {
        let $form = $('form[name=merge_payroll_form]');
        $form.trigger("reset");
        $($form).find(".footerButtons").prop('disabled', false);
        $($form).find(".footerButtons").removeClass('hidden');
        $($form).find(".wait").addClass('hidden');
        $modal.modal('show');
        onChangePayrollOne();
      }else{
        $modal.modal('hide');
      }});
    });


    $body.on('submit', 'form[name=merge_payroll_form]', function(e) {
     e.preventDefault();
     let $form = this;
     $('.error_field').addClass('hidden');
     $('.error_field').text('');
     $($form).find(".footerButtons").prop('disabled', true);
     $($form).find(".footerButtons").addClass('hidden');
     $($form).find(".wait").removeClass('hidden');
     
     let $options = {
      dataType : "json",
      type : "POST",
      url : $($form).attr("action"),
      success : function (data) {
        $($form).find(".footerButtons").prop('disabled', false);
        $($form).find(".footerButtons").removeClass('hidden');
        $($form).find(".wait").addClass('hidden');

        if (data.success) {
          $('#merge_payroll_modal').modal('hide');
          swal({
            title: "Dear! {{access()->user()->firstname}}",
            text: "<h6> Merging Request submitted successfull </h6>",
            type: "success",
            html: true,
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "OK",
            closeOnConfirm: true,
            closeOnCancel: true,
          },
          function(isConfirm) {
            document.location.replace(base_url + "/compliance/employer/payroll_merge/profile/" + data.request_id);
          });        
        }else if(data.arrear_payrolls){
          swal('Attention! {{access()->user()->firstname}}', data.arrear_payrolls ,'warning');
        }else if (data.errors.exceptions_found) {
          $('#mdlExcelExptions').modal('show');
          $('#errorExceptions').empty();
          var a = data.errors.exceptions_found;
          var rw = 1;
          $.each(a, function( index, value ) {
            $.each(value, function( key, month ) {
              $('#errorExceptions').append('<tr> <th scope="row">'+rw+'</th> <td>'+index+' and '+data.errors.keep_payroll+'</td> <td>'+month+'</td> </tr>');
              rw++;             
            });
          });
        }else{
         swal('Attention! {{access()->user()->firstname}}','Merging failed','warning');
       }
     },
     error: function ($data) {
      $($form).find(".footerButtons").prop('disabled', false);
      $($form).find(".footerButtons").removeClass('hidden');
      $($form).find(".wait").addClass('hidden');
      let $errors = $.parseJSON($data.responseText);
      $.each($errors, function( index, value ) {
        $('.'+index+'_error').removeClass('hidden');
        $('.'+index+'_error').text(value);
      });
    },
  };
  $($form).ajaxSubmit($options);

});


    $('.search-select-payroll1').on('change', function ($e) { 
      onChangePayrollOne();
    });


    function onChangePayrollOne() {
      let keep_payroll = $('.search-select-payroll1 option:selected').val();
      $html = '';
      @foreach($online_users as $remove_user)
      $html += '<option value="{{$remove_user->id}}">{{$remove_user->name." - ".$remove_user->payroll_id}}</option>';
      @endforeach

      $(".search-select-payroll").empty();
      $(".search-select-payroll").append($html);
      if (keep_payroll) {
       $('.search-select-payroll option[value=' + keep_payroll + ']').remove();
     }

   }

   $('.search-select-merge').on('change', function ($e) { 
     let merge_type = $('.search-select-merge option:selected').val();
     $('.hidden_div').show();
     if (merge_type == 1) {
      $('.keep-payrol').show();
      $('.manage-payrol').hide();
      $('.user-details').empty()
      $(".search-select-manage").val(null).trigger('change.select2');
      $('.doc').hide();
      $('.combine-remove').text('remove')

    }else{
      $('.keep-payrol').hide();
      $('.manage-payrol').show();
      $(".search-select-payroll1").val(null).trigger('change.select2');
      $(".search-select-payroll").val(null).trigger('change.select2');
      $('.doc').show();
      $('.combine-remove').text('combine')
      getAllPayrols()
    }
  });

   /* start : Searching users */
   $(".search-select-manage").select2({
    dropdownParent: $('#merge_payroll_modal'),
    minimumInputLength: 3,
    multiple: false,
    allowClear: true,
    debug: true,
    placeholder: "",
    ajax: {
      url: "{!! route('backend.compliance.online_profile.users') !!}",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term || "",
          page: params.page || 1
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            };
          }),
          pagination: {
            more: true
          }
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) {
      return markup;
    }
  }).on("select2:selecting", function($e) {
    var $selected = $e.params.args.data.id;
    getUserDetails($e.params.args.data.id)
  });

  function getUserDetails(user_id){
    $.ajax({
     type: 'get',
     url: "{!! url('compliance/online_profile/user_details') !!}/" + user_id,
     success:function(data){
      if(data){
        $('.user-details').empty()
        $('.user-details').append(data)
        getAllPayrols()
      }
    }
  });
  }

  function getAllPayrols() {
    $html = '';
    @foreach($online_users as $remove_user)
    $html += '<option value="{{$remove_user->id}}">{{$remove_user->name." - ".$remove_user->payroll_id}}</option>';
    @endforeach

    $(".search-select-payroll").empty();
    $(".search-select-payroll").append($html);
  }

});

</script>
@endpush