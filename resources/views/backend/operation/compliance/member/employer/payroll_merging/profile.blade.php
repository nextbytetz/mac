@extends('layouts.backend.main', ['title' => 'Payroll Merging Request', 'header_title' => 'Payroll Merging Request'])

@include('backend.includes.datatable_assets')


@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}

<style>
/* end: upload progress bar css */
tr {
    border-bottom:1pt solid rgba(0, 0, 0, 0.12);
}
</style>
@endpush

@section('content')


<div class = "row">
    @include("backend.operation.compliance.member.employer.payroll_merging.header")
</div>
<br/>
{{--Tabs navigation--}}
<div class = "row">
    <div class="col-md-12">

        <div class="basic_nav_pills nav_basic_tab">
            <ul class="nav nav-tabs">
                {{--General--}}
                <li class="nav-item">
                    <a class="nav-link active" href="#general"
                    data-toggle="tab">@lang('labels.general.general')
                </a>
            </li>
            {{--document--}}
            <li class="nav-item">
                <a class="nav-link" href="#documents" id="document_header"
                data-toggle="tab">Document Center
            </a>
        </li>
        {{-- <li class="nav-item">
            <a class="nav-link" href="#response_letter" id="document_header"
            data-toggle="tab">Response Letter
        </a>
    </li> --}}

</ul>
<div class="nav_tab_contain tab-content">

    <div id="general" class="nav_tab_pane tab-pane active in">
        <div class="nav_tab_pane_header">
            <div class="row">
                <div class="col-md-12" >

                    <div class="pull-right" >
                       {{--  @if(($merge_request->status == 3) && empty($merge_request->wf_done) && ($merge_request->user_id == access()->user()->id))
                        <a target=""  href="#"  class="btn btn-primary site-btn nav_button btnEditMerge" >
                            <i class="icon fa fa-edit"></i>&nbsp;Edit Request
                        </a>
                        @endif --}}

                        @if($merge_request->wf_done == 1)
                        <a target=""  href=" {{ route("backend.letter.process", [$merge_request->id, "CLIEMPPAYROLLMRG"]) }}"  class="btn btn-primary site-btn nav_button" ><i class="icon fa fa-file"></i>&nbsp;Response Letter</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class = "row">
            <div class="col-md-9">
                {{--general summary--}}
                @include('backend.operation.compliance.member.employer.payroll_merging.general_summary')

                {{--general info--}}
                @include('backend.operation.compliance.member.employer.payroll_merging.general_info')

                @if(!empty($merge_request->status))
                <legend class="grey_info mt-2" >Workflow</legend>
                <div id="plan_workflow" class="nav_tab_pane tab-pane">
                    @php
                    $workflowinput = ['resource_id' => $merge_request->id, 'wf_module_group_id'=> 35, 'type' => 0];
                    @endphp
                    {{-- @include("backend/includes/workflow/wf_track_html", $workflowinput) --}}
                    {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
                </div>
                @else
                <div class="alert-left-border mt-2">
                    <div class="alert alert-primary alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        </button>
                        <strong>No workflow history.</strong> This request has no associated workflow recorded.
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-3">
                @include('backend.operation.compliance.member.employer.payroll_merging.sidebar_summary')
            </div>
        </div>
    </div>
    {{--documents--}}
    <div id="documents" class="nav_tab_pane tab-pane">
        @include('backend.operation.compliance.member.employer.payroll_merging.document_center')
    </div>
   {{--  <div id="response_letter" class="nav_tab_pane tab-pane">
        {{'Hello World'}}
        @include('backend/operation/compliance/member/employer/particular_change/profile/includes/document_center')
    </div> --}}

</div>
</div>
</div>

</div>
@stop
@include("backend.system.workflow.includes.initiate_modal")
@include('backend.operation.compliance.member.employer.payroll_merging.payroll_summary_modal')
@include('backend.operation.compliance.member.employer.payroll_merging.edit_modal')

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script  type="text/javascript">


    $(function () {

        $('.text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
            $(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();

        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }
        $("#workflow_modal").prependTo("body");

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });


        $(".employer-select").select2({
            minimumInputLength: 3,
            multiple: false,
            ajax: {
                url: "{!! route('backend.compliance.employers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term || "",
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        }),
                        pagination: {
                            more: true
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });


    });
</script>;

@endpush
