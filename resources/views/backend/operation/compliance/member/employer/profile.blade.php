@extends('layouts.backend.main', ['title' => 'Employer', 'header_title' => 'Employer'])

@include('backend.includes.datatable_assets')

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/jstree/css/default/style.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/css/file_format.css") }}
<style>
    /* start: File Icons CSS */
    #folder-tree .folder { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') right bottom no-repeat; }
    #folder-tree .file { background:url('{!! asset_url() !!}/nextbyte/img/file_sprite.png') 0 0 no-repeat; }
    #folder-tree .file-pdf { background-position: -32px 0 }
    #folder-tree .file-as { background-position: -36px 0 }
    #folder-tree .file-c { background-position: -72px -0px }
    #folder-tree .file-iso { background-position: -108px -0px }
    #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
    #folder-tree .file-cf { background-position: -162px -0px }
    #folder-tree .file-cpp { background-position: -216px -0px }
    #folder-tree .file-cs { background-position: -236px -0px }
    #folder-tree .file-sql { background-position: -272px -0px }
    #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
    #folder-tree .file-h { background-position: -488px -0px }
    #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
    #folder-tree .file-php { background-position: -108px -18px }
    #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
    #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
    #folder-tree .file-rb { background-position: -180px -18px }
    #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
    #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
    #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
    #folder-tree .file-js { background-position: -434px -18px }
    #folder-tree .file-css { background-position: -144px -0px }
    #folder-tree .file-fla { background-position: -398px -0px }
    /* end: File Icon CSS */
    /* start: upload progress bar css */
    .progress-bar {
        background-color: #12CC1A;
        height:20px;
        color: #FFFFFF;
        width:0%;
        -webkit-transition: width .3s;
        -moz-transition: width .3s;
        transition: width .3s;
    }
    .progress-div {
        border:#0FA015 1px solid;
        padding: 5px 0px;
        margin:30px 0px;
        border-radius:4px;
        text-align:center;
    }
    /* end: upload progress bar css */
    tr {
        border-bottom:1pt solid rgba(0, 0, 0, 0.12);
    }
</style>
@endpush

@section('content')


<div class = "row">
    {{--{!! Form::model($employer, ['route' => ['backend.finance.receipt.dishonour', $employer->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}--}}

    {{--HEADER--}}
    @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer])

    <div>&nbsp;</div>
    {{--Tabs navigation--}}
    <div class = "row">
        <div class="col-md-12">

            <div class="basic_nav_pills nav_basic_tab">
                <ul class="nav nav-tabs">
                    {{--General--}}
                    <li class="nav-item">
                        <a class="nav-link active" href="#general"
                        data-toggle="tab">@lang('labels.general.general')
                    </a>
                </li>
                {{--contributions--}}
                <li class="nav-item">
                    <a class="nav-link"  id = "tab_2_header"  href="#contributions" data-toggle="tab">@lang('labels.general.contributions')</a>
                </li>
                {{--interests--}}
                <li class="nav-item">
                    <a class="nav-link" id = "tab_3_header"
                    href="#interests" data-toggle="tab">@lang('labels.general.interests')</a>
                </li>
                {{--bookings--}}
                <li class="nav-item">
                    <a class="nav-link" id = "tab_4_header"
                    href="#bookings" data-toggle="tab">@lang('labels.general.bookings')</a>
                </li>
                {{--inactive receipts--}}
                <li class="nav-item">
                    <a class="nav-link" id = "tab_5_header"
                    href="#inactive_recceipts" data-toggle="tab">@lang('labels.backend.finance.receipt.inactive_receipts')</a>
                </li>
                {{--Employer Inspections--}}
                <li class="nav-item">
                    <a class="nav-link" id = "tab_6_header"
                    href="#inspection" data-toggle="tab">@lang('labels.backend.compliance_menu.inspection.title')</a>
                </li>
                {{--Employees--}}
                <li class="nav-item">
                    <a class="nav-link" id = "employee_header"
                    href="#employees" data-toggle="tab">@lang('labels.backend.compliance.employees')</a>
                </li>


                {{--Attachments--}}
                <li class="nav-item">
                    <a class="nav-link" id = "document_header"
                    href="#documents" data-toggle="tab">@lang('labels.general.document_centre')</a>
                </li>

                {{--follow ups--}}
                <li class="nav-item">
                    <a class="nav-link" id = "followups_header"
                    href="#follow_ups" data-toggle="tab">Follow Ups</a>
                </li>

                {{--employees--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#tab-5" data-toggle="tab">@lang('labels.general.employees')</a>--}}
                {{--</li>--}}
                {{--inspections--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#tab-6" data-toggle="tab">@lang('labels.general.inspections')</a>--}}
                {{--</li>--}}
            </ul>
            <div class="nav_tab_contain tab-content">
                <div id="general" class="nav_tab_pane tab-pane active in">
                    <div class="nav_tab_pane_header">
                        <div class="row">
                            <div class="col-md-12" >

                                <div class="pull-right" >
                                    {{--Action Links--}}
                                    @include('backend.operation.compliance.member.employer.includes.action_links')

                                </div>
                            </div>
                        </div>
                    </div>


                    {{--main tab content--}}
                    <div class = "row">
                        <div class="col-md-12">

                            <div class="col-md-9">
                                {{--interest waiving overview--}}
                                @include('backend.operation.compliance.member.employer.includes.interest_writeoff_overview')
                                {{--interest adjust overview--}}
                                <div>&nbsp;</div>
                                @include('backend.operation.compliance.member.employer.includes.interest_adjust_overview')
                            </div>
                            <div class="col-md-3">
                                {{--sidebar summary--}}
                                @include('backend.operation.compliance.member.employer.includes.sidebar_summary')
                            </div>
                        </div>
                    </div>
                </div>
                <div id="contributions" class="nav_tab_pane tab-pane">
                    {{--contributions--}}
                    @include('backend.operation.compliance.member.employer.includes.contribution')
                </div>
                <div id="interests" class="nav_tab_pane tab-pane">
                    {{--interests--}}
                    @include('backend.operation.compliance.member.employer.includes.interest')
                </div>
                <div id="bookings" class="nav_tab_pane tab-pane">
                    {{--bookings--}}
                    @include('backend.operation.compliance.member.employer.includes.booking')
                </div>
                <div id="inactive_recceipts" class="nav_tab_pane tab-pane">
                    {{--inactive receipts--}}
                    @include('backend.operation.compliance.member.employer.includes.inactive_receipts')
                </div>
                <div id="inspection" class="nav_tab_pane tab-pane">
                    {{--Employer Inspection History--}}
                    @include('backend.operation.compliance.member.employer.includes.inspection', ['tab_header' => 'tab_6_header'])
                </div>
                <div id="employees" class="nav_tab_pane tab-pane">
                    {{--Employees--}}
                    @include('backend.operation.compliance.member.employer.includes.employees_all')

                    {{-- @include('backend.operation.compliance.member.employer.includes.employees') --}}
                </div>
                <div id="documents" class="nav_tab_pane tab-pane">
                    {{--Employer Document / Attachments--}}
                    @include('backend.operation.compliance.member.employer.includes.document_centre')
                </div>

                <div id="follow_ups" class="nav_tab_pane tab-pane">
                    {{--Employer Document / Attachments--}}
                    @include('backend/operation/compliance/member/employer/staff_relationship/general_followups/includes/follow_ups_by_employer_dt', ['employer_id' => $employer->id])
                </div>
            </div>
        </div>
    </div>

</div>

{{--{!! Form::close() !!}--}}
</div>
@stop


@push('after-script-end')
@stack('booking-script-end')
@stack('contribution-script-end')
@stack('interest-adjustment-overview-script-end')
@stack('interest-writeoff-overview-script-end')
@stack('interest-script-end')
@stack('inactive-receipts-script-end')
@stack('document-centre-script-end')
@stack('employees-script-end')
<script  type="text/javascript">

    $(function () {


        if (location.hash !== '') {
            $('a[href="' + location.hash + '"]').tab('show');
            $('a[href="' + location.hash + '"]').trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });
    });

</script>;

@endpush
