@extends('layouts.backend.main', ['title' => 'Contribution & Interest Rates', 'header_title' => 'Contribution & Interest Rates'])

@include('backend.includes.assets.datetimepicker')
@include('backend.includes.datatable_assets')
@push('after-styles-end')

{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
<style type="text/css">
div#ui-datepicker-div {
    z-index: 999999999999 !important;
}
</style>
@endpush

@section('content')

<div class="row">
    <div class="custom_filter">
        {!! Form::open(['role' => 'form', 'id' => 'search-form', 'method'=>'GET', 'route' => ['backend.compliance.employer.rates.datatable']]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="form-row">
                    {{--Incident Type--}}
                    <div class="form-group col-md-4 allocation_select">
                        <label for="pay_type">Payment Type</label>
                        {!! Form::select('fin_code_id', [0 => 'All', 2 =>'Contribution', 1=> 'Interest'], null, ['class' => 'form-control search-select', 'id' => 'fin_code_id']) !!}
                    </div>
                    {{--Employee Name--}}
                    <div class="form-group col-md-4 allocation_select">
                        <label for="employer_category_cv_id">Applicable For</label>
                        {!! Form::select('employer_category_cv_id', [0 => 'All', 36=> 'Public Sector', 37 => 'Private Sector'], null, ['class' => 'form-control search-select', 'id' => 'employer_category_cv_id']) !!}
                    </div>
                    {{--Employer Name--}}
                    <div class="form-group col-md-4 allocation_select">
                        <label for="employer">Status</label>
                        {!! Form::select('status', [0=> 'All', 2=> 'Approved', 1 => 'On-Progress', 3=>'Cancelled / Rejected'], null, ['class' => 'form-control search-select', 'id' => 'status']) !!}
                    </div>

                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <button type="button" class="btn btn-secondary site-btn" id="clear_filter">Clear</button>
                        <button type="submit" class="btn btn-success btn-sm btn-submit">@lang('buttons.general.search')</button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<div class = "row">
    {{-- @include("backend.operation.compliance.member.employer.includes.header_info",['employer'=> $employer]) --}}
    {{--<br/>--}}
    <div class="col-md-12" >
        <br/>
        <div class="col-md-12" >
            <div class="pull-right">
                <div class="btn-group">
                    <a href="#"  class="btn btn-sm btn-primary save_button" id="new_rate" ><i class="icon fa fa-plus"></i>&nbsp;New Rate</a>
                </div>
            </div>
        </div>
    </div>
</div>

<br/>
{{--Table--}}
<div class = "row">
    <div class="col-md-12" >
        <table class="display" cellspacing="0" width="100%" id ="rate_table">
            <thead>
                <tr >
                    <th>Description</th>
                    <th>Rate</th>
                    {{-- <th>Applicable To</th> --}}
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>

    </div>
</div>

</div>
<div class="modal" id="new_rate_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <form id="new_rate_form" action="{{route('backend.compliance.employer.rates.post')}}">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">New Contribution / Interest Rate</h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6"><label class="required">Rate For:</label>
                    <div class="form-group">
                        <?php $request_types = [1 => 'Interest Rate', 2 => 'Contribution Rate']; ?>
                        {!! Form::select('rate_type',$request_types, null, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'request_type']) !!}
                        <strong><span class="rate_type_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                </div>

                <div class="col-sm-6"><label class="required">Applicable to:</label>
                    <div class="form-group">
                        <?php $applicable_to = [37 => 'Private Employer', 36 => 'Public Employer']; ?>
                        {!! Form::select('employer_category',$applicable_to, null, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control search-select', 'id'=> 'employer_category']) !!}
                        <strong><span class="employer_category_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                </div>
                <div class="col-sm-12">
                    <strong><span class="general_error help-block label label-danger hidden submit_errors mb-1"></span></strong>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6"><label>Current Rate:</label>
                    <div class="form-group">
                        {!! Form::text('current_rate',null, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control', 'id'=> 'current_rate', 'readonly'=>'readonly']) !!}
                        <strong><span class="current_rate_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                </div>
                <div class="col-sm-6"><label>Current Rate Started At:</label>
                    <div class="form-group">
                        {!! Form::text('current_start_date',null, ['style' => 'width:100%;','class' => 'form-control', 'id'=> 'current_start_date', 'readonly'=>'readonly']) !!}
                        <strong><span class="current_start_date_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                </div>
            </div>

            <div class="row mt-1">
                <div class="col-sm-6"><label class="required">New Rate (in percentage example 0.5 or 1):</label>
                    <div class="form-group">
                        {!! Form::text('new_rate',null, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control', 'id'=> 'new_rate']) !!}
                        <strong><span class="new_rate_error help-block label label-danger hidden submit_errors"></span></strong>
                    </div>
                </div>

                <div class="col-sm-6"><label class="required">Start Period:</label>
                 <div class="form-inline">
                    <div class="input-group" style="width:100%;">
                        {!! Form::text('start_date',null, ['class' => 'form-control datepicker3', 'id' => 'start_date']) !!}
                        <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                    </div>
                    <strong><span class="start_date_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label class="required">Description:</label>
                <div class="form-group">
                    <textarea name="description" id="description" class="form-control autosize" cols="10" rows="2"></textarea>
                    <strong><span class="description_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>
            <div class="col-sm-6">
               <div class="fileld-layout">
                <label class="required">Attachement </label>
                <div class="form-group mt-1">
                    <div class="input-group">
                      <input type="file" name="request_attachment" id="request_attachment" required="required" class="form-control">
                      <strong><span class="help-block label label-danger submit_errors request_attachment_error hidden mt-1"></span></strong>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal-footer">
    <span class="submit_btn">
        <button type="button" class="btn btn-primary" id="btnSubmitRequest">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </span>
    <span class="wait hidden">
        <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 1%; width:10%;" />  
        &nbsp; <span class="h6"> Please wait .... </span>
    </span>
</div>
</div>
</form>
</div>
</div>




@stop


@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}

<script  type="text/javascript">
    $(function() {
       jQuery('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          startDate: new Date('2015-07-01')
      });

       // $(document).on('focusin', '.arrearcontribdate', function(){
       //     $(this).datepicker({
       //     })});

       let $oTable = $('#rate_table').DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        stateSaveCallback: function (settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
        },
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
        },
        ajax:{
            url : '{!! route('backend.compliance.employer.rates.datatable') !!}',
            type : 'get',
            data: function ($data) {
               $data = gatherDataDt($data, 'search-form');
           }
       },
       columns: [
       { data: 'name', name: 'name', orderable : false, searchable : false},
       { data: 'rate', name: 'rate', orderable : true, searchable : false},
       { data: 'start_date', name: 'start_date' ,orderable : false, searchable : false},
       { data: 'end_date', name: 'end_date' ,orderable : false, searchable : false},
       { data: 'wf_status', name: 'status' ,orderable : false, searchable : false},
       ],
       "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).click(function() {
            document.location.href = base_url +  "/compliance/employer/rates/profile/" + aData['id'] ;
        }).hover(function() {
            $(this).css('cursor','pointer');
        }, function() {
            $(this).css('cursor','auto');
        });
    }

});


       $('#new_rate').on('click', function(event) {
        event.preventDefault();
            // alert(123);
            $('#new_rate_form').trigger('reset');
            $('#new_rate_modal').modal('show');
        });


       $('#btnSubmitRequest').on('click', function(e) {
           e.preventDefault();
           let $form = $('#new_rate_form');
           $('.submit_errors').addClass('hidden');
           $('.submit_errors').text(''); 
           $("#btnSubmitRequest").prop('disabled', true);
           $(".submit_btn").addClass('hidden');
           $(".wait").removeClass('hidden');
           let $options = {
            dataType : "json",
            type : "POST",
            url : $($form).attr("action"),
            success : function (data) {
                $("#btnSubmitRequest").prop('disabled', false);
                $(".submit_btn").removeClass('hidden');
                $(".wait").addClass('hidden');
                if (data.success) {
                  swal({
                    title: "Dear! {{access()->user()->firstname}}",
                    text: "<h6> New Rate Successfully Saved </h6>",
                    type: "success",
                    html: true,
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function(isConfirm) {
                    document.location.replace(base_url + "/compliance/employer/rates/profile/" + data.request_id);
                });        
              }else{
                if (data.errors) {
                   $.each(data.errors, function(key,valueObj){
                    $('.'+key+'_error').removeClass('hidden');
                    $('.'+key+'_error').text(valueObj);
                });
               } else {
                swal('Dear! {{access()->user()->firstname}}','New Rate Failed To Save! Kindly try again','warning');
            }
        }
    },
    error: function ($data) {
        $("#btnSubmitRequest").prop('disabled', false);
        $(".submit_btn").removeClass('hidden');
        $(".wait").addClass('hidden');
        swal('Dear! {{access()->user()->firstname}}','An error seems to have occurred. Kindly try again','warning');
    },
};

$($form).ajaxSubmit($options);});

       $( "#clear_filter" ).click(function() {
        $(".search-select").val(null).trigger('change.select2');
    });

       $('#search-form').on('submit', function($e) {
        $e.preventDefault();
        $oTable.draw();
    });

       returnCurrentRate();
       $('#request_type').on('change',function(e){ 
        returnCurrentRate();

    }); 

       $('#employer_category').on('change',function(e){
        returnCurrentRate();
    }); 


       function returnCurrentRate() {
        let fin_code = $('#request_type').val();
        let employer_category = $('#employer_category').val();
        $('#current_rate').val('');
        $('#current_start_date').val('');
        if (fin_code && employer_category) {
           $.ajax({
            url : '{{url('/compliance/employer/rates/current_rate')}}/'+fin_code+'/'+employer_category,
            type : 'GET',
            datatype : 'json',
            success:function(data){
                if (data.current_rate) {
                    $('#current_rate').val(data.current_rate);
                    $('#current_start_date').val(data.current_start_date);
                }
            }});}
       }

       function gatherDataDt($data, $id) {
        $('#' + $id).find(':input').each(function () {
            let $name = $(this).attr('name');
            $data[$name] = $(this).val();
        });
        return $data;}



    });


</script>;

@endpush
