<div class="row mt-2">
    <div class="grey_modal">
        <table style="width:100%">
            <tr>
                <td  align="center"><h5><b><span class="light_dark_color">Request Summary</span></b></h5></td>

            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="light_grey_bg">&nbsp;</div>
    <div class="light_grey_bg">
        <table style="width:100%" class="summary">

            <tr class="underline pb-1" >
                <td class="pt-1 pl-1" width="50%">
                    <h6 style="font-weight: lighter;">Item to change  :</h6>
                </td>
                <td class="pt-1">
                    <h6 style="font-weight: bold;">
                     {{$rate->rate_name}}
                 </h6>
             </td>
         </tr>

         <tr class="underline" >
            <td class="pt-1 pl-1" width="50%">
                <h6 style="font-weight: lighter;">Rate changes:</h6>
            </td>
            <td class="pt-1">
                <h6 style="font-weight: bold;">{!! !empty($rate->parent->rate) ? 'From '.$rate->parent->rate.'% to '.$rate->rate :  $rate->rate !!}%</h6>
            </td>
        </tr>

        <tr class="underline pb-1" >
            <td class="pt-1 pl-1" width="50%">
                <h6 style="font-weight: lighter;">Applicable to  :</h6>
            </td>
            <td class="pt-1">
                <h6 style="font-weight: bold;">
                 {{$rate->sector_name}} Employers
             </h6>
         </td>
     </tr>

     <tr class="underline pb-1" >
        <td class="pt-1 pl-1" width="50%">
            <h6 style="font-weight: lighter;">Created by  :</h6>
        </td>
        <td class="pt-1">
            <h6 style="font-weight: bold;">{!!  trim($rate->user->name) !!}</h6>
        </td>
    </tr>

    <tr class="underline" >
        <td class="pt-1 pl-1" width="50%">
            <h6 style="font-weight: lighter;">Request description:</h6>
        </td>
        <td class="pt-1">
            <h6 style="font-weight: bold;">{!! $rate->description !!}</h6>
        </td>
    </tr>

    <tr class="{{$modification_request->iscancelled ? 'underline' :''}}" >
        <td class="pt-1 pl-1" width="50%">
            <h6 style="font-weight: lighter;">Created date :</h6>
        </td>
        <td class="pt-1">
            <h6 style="font-weight: bold;">{!! $rate->created_date_formatted !!}</h6>
        </td>
    </tr>


    @if($modification_request->iscancelled)
    <tr class="underline" >
        <td class="pt-1 pl-1" width="50%">
            <h6 style="font-weight: lighter;">Cancel date:</h6>
        </td>
        <td class="pt-1">
            <h6 style="font-weight: bold;">{!! \Carbon\Carbon::parse($modification_request->updated_at)->format('d F, Y') !!}</h6>
        </td>
    </tr>
    <tr class="" >
        <td class="pt-1 pl-1" width="50%">
            <h6 style="font-weight: lighter;">Cancel reason:</h6>
        </td>
        <td class="pt-1">
            <h6 style="font-weight: bold;">{!! $modification_request->cancel_reason !!}</h6>
        </td>
    </tr>

    @endif

</table>
</div>
</div>