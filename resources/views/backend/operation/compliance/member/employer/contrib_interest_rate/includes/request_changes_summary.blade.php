
<table class="table table-bordered table-striped mb-1 mt-2">
  <tr>
    <th colspan="2" style="text-align: center; vertical-align: middle;">Change Item</th>
    <th>New</th>
    <th>Old</th>
  </tr>
  <tr class="table-info">
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$rate->rate.'%'}}</td>
    <td>{{!empty($rate->parent->rate) ? $rate->parent->rate.'%': 'N/A'}}</td>
  </tr>
  <tr class="table-success">
    <td>Start Date</td>
    <td>{{$rate->start_date_formatted}}</td>
    <td>{{!empty($rate->parent->start_date_formatted) ? $rate->parent->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($rate->start_date_formatted)->subMonth())->format('F, Y'): 'N/A'}} </td>
  </tr>
</table>