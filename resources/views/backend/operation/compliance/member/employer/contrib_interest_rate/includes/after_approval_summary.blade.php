 <table class="table table-bordered table_rates table-striped mb-1 mt-2">
  <tr>
    <th style="text-align: center; vertical-align: middle;"></th>
    <th>Item</th>
    <th>New</th>
    <th>Old</th>
</tr>
@if($public_contrib_rate)

@if($rate->fin_code_id == 2 && $rate->employer_category_cv_id == 36)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$public_contrib_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$rate->rate.'%'}}</td> 
    <td>{{$public_contrib_rate->rate.'%'}}</td>
</tr>
<tr >
    <td>Start Date</td>
    <td>{{$rate->start_date_formatted}}</td>
    <td>{{$public_contrib_rate->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($rate->start_date_formatted)->subMonth())->format('F, Y')}}</td>
</tr>

@else
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$public_contrib_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$public_contrib_rate->rate.'%'}}</td>
    <td>{{$public_contrib_rate->rate.'%'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$public_contrib_rate->start_date_formatted}}</td>
    <td>{{$public_contrib_rate->start_date_formatted}} </td>
</tr>

@endif
@endif



@if($private_contrib_rate)

@if($rate->fin_code_id == 2 && $rate->employer_category_cv_id == 37)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$private_contrib_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$rate->rate.'%'}}</td>
    <td>{{$private_contrib_rate->rate.'%'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$rate->start_date_formatted}}</td>
    <td>{{$private_contrib_rate->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($rate->start_date_formatted)->subMonth())->format('F, Y')}}</td>
</tr>
@else
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$private_contrib_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$private_contrib_rate->rate.'%'}}</td>
    <td>{{$private_contrib_rate->rate.'%'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$private_contrib_rate->start_date_formatted}}</td>
    <td>{{$private_contrib_rate->start_date_formatted}}</td>
</tr>

@endif
@endif


@if($public_interest_rate)

@if($rate->fin_code_id == 1 && $rate->employer_category_cv_id == 36)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$public_interest_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$rate->rate.'%'}}</td>
    <td>{{$public_interest_rate->rate.'%'}}</td>
</tr>
<tr >
    <td>Start Date</td>
    <td>{{$rate->start_date_formatted}}</td>
    <td>{{$public_interest_rate->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($rate->start_date_formatted)->subMonth())->format('F, Y')}}</td>
</tr>
@else
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$public_interest_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$public_interest_rate->rate.'%'}}</td>
    <td>{{$public_interest_rate->rate.'%'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$public_interest_rate->start_date_formatted}}</td>
    <td>{{$public_interest_rate->start_date_formatted}}</td>
</tr>

@endif
@endif

@if($private_interest_rate)
@if($rate->fin_code_id == 1 && $rate->employer_category_cv_id == 37)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$private_interest_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$rate->rate.'%'}}</td>
    <td>{{$private_interest_rate->rate.'%'}}</td>
</tr>
<tr >
    <td>Start Date</td>
    <td>{{$rate->start_date_formatted}}</td>
    <td>{{$private_interest_rate->start_date_formatted .' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($rate->start_date_formatted)->subMonth())->format('F, Y')}}</td>
</tr>
@else
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$private_interest_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$private_interest_rate->rate.'%'}}</td>
    <td>{{$private_interest_rate->rate.'%'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$private_interest_rate->start_date_formatted}}</td>
    <td>{{$private_interest_rate->start_date_formatted}}</td>
</tr>
@endif
@endif
</table>