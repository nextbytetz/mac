{{-- @php

$history_array = json_decode($rate->history,true);
$current_summary = [];
foreach ($history_array as $key => $value) {
    if (($value['fin_code_id'] == 2) && ($value['employer_category_cv_id'] == 36 )) {
     $current_summary ['public_contrib'] = ['rate' =>  $value['rate'],'start_date' =>  $value['start_date']];
 }

 if (($value['fin_code_id'] == 1) && ($value['employer_category_cv_id'] == 36 )) {
     $current_summary ['public_interest'] = ['rate' =>  $value['rate'],'start_date' =>  $value['start_date']];
 }

 if (($value['fin_code_id'] == 1) && ($value['employer_category_cv_id'] == 37 )) {
     $current_summary ['private_interest'] = ['rate' =>  $value['rate'],'start_date' =>  $value['start_date']];
 }

 if (($value['fin_code_id'] == 2) && ($value['employer_category_cv_id'] == 37 )) {
     $current_summary ['private_contrib'] = ['rate' =>  $value['rate'],'start_date' =>  $value['start_date']];
 }
}
dd($current_summary);
@endphp


<table class="table table-bordered table_rates table-striped mb-1 mt-2">
    <tr>
        <td>Sn</td>
        <td rowspan="2"></td>
        <td></td>
        <td colspan="2">Applicable Rate</td>
    </tr>
    <tr>
        <td></td>
        <td>Rate/Period</td>
        <td>{{$current_summary['public_contrib'] ? \Carbon\Carbon::parse($current_summary['public_contrib']['start_date'])->format('F, Y') : '-'}}</td>
        <td>July 2021 – Todate</td>
    </tr>
    <tr>
        <td>1</td>
        <td>Contribution</td>
        <td>{{$current_summary['public_contrib'] ? $current_summary['public_contrib']['rate'].'%' : '-'}}</td>
        <td>0.6%</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Interest</td>
        <td>10%</td>
        <td>N/A</td>
    </tr>
</table>




 --}}

<table class="table table-bordered table_rates table-striped mb-1 mt-2">
  <tr>
    <th style="text-align: center; vertical-align: middle;"></th>
    <th>Item</th>
    <th>Current</th>
    <th>Previous</th>
</tr>
@if($public_contrib_rate)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$public_contrib_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$public_contrib_rate->rate.'%'}}</td>
    <td>{{!empty($public_contrib_rate->parent->rate) ? $public_contrib_rate->parent->rate.'%': 'N/A'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$public_contrib_rate->start_date_formatted}}</td>
    <td>{{!empty($public_contrib_rate->parent->start_date_formatted) ? $public_contrib_rate->parent->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($public_contrib_rate->start_date_formatted)->subMonth())->format('F, Y'): 'N/A'}} </td>
</tr>
@endif
@if($private_contrib_rate)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$private_contrib_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$private_contrib_rate->rate.'%'}}</td>
    <td>{{!empty($private_contrib_rate->parent->rate) ? $private_contrib_rate->parent->rate.'%': 'N/A'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$private_contrib_rate->start_date_formatted}}</td>
    <td>{{!empty($private_contrib_rate->parent->start_date_formatted) ? $private_contrib_rate->parent->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($private_contrib_rate->start_date_formatted)->subMonth())->format('F, Y'): 'N/A'}} </td>
</tr>
@endif


@if($public_interest_rate)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$public_interest_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$public_interest_rate->rate.'%'}}</td>
    <td>{{!empty($public_interest_rate->parent->rate) ? $public_interest_rate->parent->rate.'%': 'N/A'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$public_interest_rate->start_date_formatted}}</td>
    <td>{{!empty($public_interest_rate->parent->start_date_formatted) ? $public_interest_rate->parent->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($public_interest_rate->start_date_formatted)->subMonth())->format('F, Y'): 'N/A'}} </td>
</tr>
@endif
@if($private_interest_rate)
<tr >
    <td rowspan="2" class="text-center" style="text-align: center; vertical-align: middle; line-height: 90px; background-color: white !important;">{{$private_interest_rate->long_name}}</td>
    <td>Rate</td>
    <td>{{$private_interest_rate->rate.'%'}}</td>
    <td>{{!empty($private_interest_rate->parent->rate) ? $private_interest_rate->parent->rate.'%': 'N/A'}}</td>

</tr>
<tr >
    <td>Start Date</td>
    <td>{{$private_interest_rate->start_date_formatted}}</td>
    <td>{{!empty($private_interest_rate->parent->start_date_formatted) ? $private_interest_rate->parent->start_date_formatted.' - '.\Carbon\Carbon::parse(\Carbon\Carbon::parse($private_interest_rate->start_date_formatted)->subMonth())->format('F, Y'): 'N/A'}} </td>
</tr>
@endif
</table>