@extends('layouts.backend.main', ['title' => $rate->rate_name, 'header_title' => $rate->rate_name])
@include('backend.includes.datatable_assets')
@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/sweetalert.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/sweetalert/css/google.css") }}
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
<style type="text/css">
.custom_filter:after {
    background-color: #F5F5F5;
    border: 1px solid #DDDDDD;
    border-radius: 4px 0 4px 0;
    color: #3c5ba4;
    content: "@lang('labels.backend.system.workflow.custom_filter')";
    left: -1px;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
}
.custom_filter {
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
    border-radius: 4px 4px 4px 4px;
    margin: 5px 0px;
    padding: 39px 19px 14px;
    position: relative;
}
</style>
@endpush
@section('content')
{{-- {{dd($rate->parent)}} --}}
<div class="row">
    @include("backend/operation/compliance/member/employer/contrib_interest_rate/includes/header_info")
</div>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tab-pills-image">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" id="general_information_tab" data-toggle="tab" href="#general_information" role="tab">
                        <i class="icon fa fa-info-circle" aria-hidden="true"></i>General Information
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="document_header" data-toggle="tab" href="#documents" role="tab">
                        <i class="icon fa fa-folder-open" aria-hidden="true"></i>Attachments
                    </a>
                </li>
            </ul>
            <div class="nav_tab_contain tab-content">
                <div class="tab-pane active" id="general_information" role="tabpanel">
                   <div class="nav_tab_pane_header">
                    <div class="row">
                        <div class="col-md-12" >
                            {{-- @if($modification_request->can_assign) --}}
                            <div class="pull-right" >
                                @if(empty($rate->wf_status))
                                <span>
                                    {!! HTML::decode(link_to_route('backend.compliance.employer.rates.submit_for_approval', "<i class='icon fa fa-paper-plane' aria-hidden='true'></i>&nbsp; Submit For Review",[$rate->id],['data-method' => 'confirm','data-type'=>'warning', 'data-trans-button-cancel' => trans('buttons.general.no'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => "Dear ".access()->user()->firstname, 'data-trans-text' => 'Are you sure you want to submit for review and approval!', 'class' => 'btn btn-info site-btn nav_button'])) !!}
                                </span>
                                @endif
                                @if( ($rate->wf_status == 0 && $rate->user_id == access()->user()->id) || ($rate->wf_done == 0 && $rate->wf_status==3 && $rate->user_id == access()->user()->id))
                                <span>
                                    <a href="#" id="edit_rate_request" class="btn btn-primary site-btn nav_button" style="font-weight:bold;">
                                        <i class='icon fa fa-ellipsis-v' aria-hidden='true'></i>&nbsp; Edit Request
                                    </a>
                                </span>
                                {{-- <span>
                                 <a href="#" class="btn btn-warning site-btn nav_button cancel_request"><i class="fa fa-trash"></i>&nbsp;Cancel Request</a>
                             </span> --}}
                             @endif
                         </div>
                         {{-- @endif --}}
                     </div>
                 </div>
             </div>
             <div class = "row">
                <div class="col-md-12">
                    <div class="col-md-8">

                        <div class="card-accordions mt-2">
                            <div id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="card">
                                    <div class="card-header">
                                        <h6 class="mb-0">
                                            <a class="card-title" data-toggle="collapse" data-parent="#accordion" href="#changes_summary" aria-expanded="true">Changes Summary
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="changes_summary" class="expand collapse in" aria-expanded="true" style="">
                                        <div class="card-block">
                                            @include("backend/operation/compliance/member/employer/contrib_interest_rate/includes/request_changes_summary")
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card-accordions mt-2">
                            <div id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="card">
                                    <div class="card-header">
                                        <h6 class="mb-0">
                                            <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#current_summary" aria-expanded="false">Before Approval (Current Rates) Summary
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="current_summary" class="expand collapse" aria-expanded="false" style="">
                                        <div class="card-block">
                                            @include("backend/operation/compliance/member/employer/contrib_interest_rate/includes/current_summary")
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-accordions mt-2">
                            <div id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="card">
                                    <div class="card-header">
                                        <h6 class="mb-0">
                                            <a class="card-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#after_summary" aria-expanded="false">After Approval Summary
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="after_summary" class="expand collapse" aria-expanded="false" style="">
                                        <div class="card-block">
                                            @include("backend/operation/compliance/member/employer/contrib_interest_rate/includes/after_approval_summary")
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mb-2 mt-1">
                            @if(!empty($rate->wf_status))
                            <legend class="grey_info mt-2" >Workflow</legend>
                            <div id="workflow" class="nav_tab_pane tab-pane">
                                @php
                                $workflowinput = ['resource_id' => $rate->id, 'wf_module_group_id'=> 38, 'type' => 0];
                                @endphp
                                {{-- @include("backend/includes/workflow/wf_track_html", $workflowinput) --}}
                                {!! $workflowtrack->with($workflowinput)->render('backend.includes.workflow_track', $workflowinput) !!}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        @include("backend/operation/compliance/member/employer/contrib_interest_rate/includes/summary")
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="documents" role="tabpanel">
            <div class="row">
                <div class="col-sm-12 pt-1">
                   @include("backend/operation/compliance/member/employer/contrib_interest_rate/includes/attachment")
               </div>
           </div>
       </div>
   </div>
</div>
</div>

<div class="modal" id="cancel_request_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form id="cancel_form" action="{{route('backend.compliance.employer.contrib_modification.cancel_request')}}">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Cancel Receipt # {{$modification_request->rctno}} Modification</h5>
        </div>
        <div class="modal-body">
         <div class="row">
             <div class="col-sm-12">
                <label class="h6">Cancel Reason:</label>
                <div class="form-group">
                    {!! Form::hidden("employer_id", $modification_request->employer_id) !!}
                    {!! Form::hidden("modification_request_id", $modification_request->id) !!}
                    <textarea name="cancel_reason" id="cancel_reason" class="form-control autosize" cols="30" rows="10"></textarea>
                    <strong><span class="cancel_reason_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <span class="submit_btn">
            <button type="button" class="btn btn-primary" id="btnSubmitCancel">Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </span>
        <span class="wait hidden">
            <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 1%; width:10%;" />  
            &nbsp; <span class="h6"> Please wait .... </span>
        </span>
    </div>
</div>
</div>
</form>
</div>

@include("backend/operation/compliance/member/employer/contrib_interest_rate/includes/edit_modal")


</div>


@endsection



@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/autosize/js/autosize.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/sweetalert/js/sweetalert.min.js") }}
{{ Html::script(asset_url(). "/nextbyte/plugins/forms/js/jquery.form.min.js") }}

<script>
    $(function () {

        let $body = $('body');

        if (location.hash !== '') {
            let $linkhref = $('a[href="' + location.hash + '"]');
            $linkhref.tab('show');
            $linkhref.trigger('click');
        }


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tab = $(e.target).attr('href').substr(1);
            if (history.pushState) {
                history.pushState(null, null, '#' + tab);
            } else {
                location.hash = '#' + tab;
            }
        });

        $('.cancel_request').on('click',function(e){ 
            e.preventDefault();
            swal({
                title: "Dear! {{access()->user()->firstname}}",
                text: "<h6>Are you sure you want to cancel receipt # {{$modification_request->rctno}} modification request!?</h6>",
                type: "warning",
                html: true,
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "YES CANCEL",
                cancelButtonText: "NO DON'T CANCEL",
                closeOnConfirm: true,
                closeOnCancel: true,
            },
            function(isConfirm) {
               if (isConfirm) {
                 $('#cancel_form').trigger('reset');
                 $('#cancel_request_modal').modal('show');
             }
         });
        });

        $('#btnSubmitCancel').on('click', function(e) {
         e.preventDefault();
         let $form = $('#cancel_form');

         $('.submit_btn').addClass('hidden');
         $('.submit_errors').addClass('hidden');
         $('.submit_errors').text('');
         $("#btnSubmitCancel").prop('disabled', true);
         $(".wait").removeClass('hidden');


         let $options = {
            dataType : "json",
            type : "POST",
            url : $($form).attr("action"),
            success : function (data) {
                $("#btnSubmitCancel").prop('disabled', false);
                $(".submit_btn").removeClass('hidden');
                $(".wait").addClass('hidden');
                if (data.success) {
                  swal({
                    title: "Dear! {{access()->user()->firstname}}",
                    text: "<h6> The modification request succesfully cancelled  </h6>",
                    type: "success",
                    html: true,
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function(isConfirm) {
                    document.location.replace(base_url + "/compliance/employer/contrib_modification/index/{{$modification_request->employer_id}}");
                });        
              }else{
                if (data.errors) {
                 $.each(data.errors, function(key,valueObj){
                    $('.'+key+'_error').removeClass('hidden');
                    $('.'+key+'_error').text(valueObj);
                });
             } else {
                swal('Dear! {{access()->user()->firstname}}','The modification request failed to cancell','warning');
            }
        }
    },
    error: function ($data) {
        $("#btnSubmitCancel").prop('disabled', false);
        $(".submit_btn").removeClass('hidden');
        $(".wait").addClass('hidden');
        swal('Dear! {{access()->user()->firstname}}','An error seems to have occured! Please try again','warning');
    },
};

$($form).ajaxSubmit($options);
});



    });
</script>
@endpush