<div class="modal" id="edit_rate_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <form id="edit_rate_form" action="{{route('backend.compliance.employer.rates.update',$rate->id)}}">
     {{ method_field('PUT') }}
     <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit {{$rate->rate_name}}</h5>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-6"><label class="required">Rate For:</label>
                <div class="form-group">
                    <?php $request_types = [1 => 'Interest Rate', 2 => 'Contribution Rate']; ?>
                    {!! Form::select('rate_type',$request_types, $rate->fin_code_id, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control', 'id'=> 'request_type']) !!}
                    <strong><span class="rate_type_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>

            <div class="col-sm-6"><label class="required">Applicable to:</label>
                <div class="form-group">
                    <?php $applicable_to = [37 => 'Private Employer', 36 => 'Public Employer']; ?>
                    {!! Form::select('employer_category',$applicable_to, $rate->employer_category_cv_id, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control ', 'id'=> 'employer_category']) !!}
                    <strong><span class="employer_category_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>
            <div class="col-sm-12">
                <strong><span class="general_error help-block label label-danger hidden submit_errors mb-1"></span></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6"><label>Current Rate:</label>
                <div class="form-group">
                    {!! Form::text('current_rate',$rate->parent->rate, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control', 'id'=> 'current_rate', 'readonly'=>'readonly']) !!}
                    <strong><span class="current_rate_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>
            <div class="col-sm-6"><label>Current Rate Started At:</label>
                <div class="form-group">
                    {!! Form::text('current_start_date',$rate->parent->start_date, ['style' => 'width:100%;','class' => 'form-control', 'id'=> 'current_start_date', 'readonly'=>'readonly']) !!}
                    <strong><span class="current_start_date_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>
        </div>

        <div class="row mt-1">
            <div class="col-sm-6"><label class="required">New Rate (in percentage example 0.5 or 1):</label>
                <div class="form-group">
                    {!! Form::text('new_rate',$rate->rate, ['style' => 'width:100%;', 'placeholder' => '','class' => 'form-control', 'id'=> 'new_rate']) !!}
                    <strong><span class="new_rate_error help-block label label-danger hidden submit_errors"></span></strong>
                </div>
            </div>

            <div class="col-sm-6"><label class="required">Start Period:</label>
               <div class="form-inline">
                <div class="input-group" style="width:100%;">
                    {!! Form::text('start_date',$rate->start_date, ['class' => 'form-control datepicker3', 'id' => 'start_date']) !!}
                    <span class="input-group-addon"><i class="icon fa fa-calendar"></i></span>
                </div>
                <strong><span class="start_date_error help-block label label-danger hidden submit_errors"></span></strong>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <label class="required">Description:</label>
            <div class="form-group">
                <textarea name="description" id="description" class="form-control autosize" cols="10" rows="2">{{$rate->description}}</textarea>
                <strong><span class="description_error help-block label label-danger hidden submit_errors"></span></strong>
            </div>
        </div>
        <div class="col-sm-6">
         <div class="fileld-layout">
            <label>Attachement </label>
            <div class="form-group mt-1">
                <div class="input-group">
                  <input type="file" name="request_attachment" id="request_attachment" required="required" class="form-control">
                  <strong><span class="help-block label label-danger submit_errors request_attachment_error hidden mt-1"></span></strong>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
<div class="modal-footer">
    <span class="submit_btn">
        <button type="button" class="btn btn-primary" id="btnSubmitEdit">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </span>
    <span class="wait hidden">
        <img src="{!! asset_url() . '/nextbyte/img/ajax-loading.gif' !!}" style="height: 1%; width:10%;" />  
        &nbsp; <span class="h6"> Please wait .... </span>
    </span>
</div>
</div>
</form>
</div>
</div>



@push('after-script-end')


<script  type="text/javascript">
    $(function() {
        jQuery('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          startDate: new Date('2015-07-01')
      });

        $('#edit_rate_request').on('click', function(event) {
            event.preventDefault();

            swal({
                title: "Dear! {{access()->user()->firstname}}",
                text: "<h6>Are you sure you want to edit this request!?</h6>",
                type: "warning",
                html: true,
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "YES",
                cancelButtonText: "NO",
                closeOnConfirm: true,
                closeOnCancel: true,
            },
            function(isConfirm) {
               if (isConfirm) {
                $('#edit_rate_modal').modal('show'); }});});

        $('#request_type').on('change',function(e){ 
            returnCurrentRate();

        }); 

        $('#employer_category').on('change',function(e){
            returnCurrentRate();
        }); 


        function returnCurrentRate() {
            let fin_code = $('#request_type').val();
            let employer_category = $('#employer_category').val();
            $('#current_rate').val('');
            $('#current_start_date').val('');
            if (fin_code && employer_category) {
             $.ajax({
                url : '{{url('/compliance/employer/rates/current_rate')}}/'+fin_code+'/'+employer_category,
                type : 'GET',
                datatype : 'json',
                success:function(data){
                    if (data.current_rate) {
                        $('#current_rate').val(data.current_rate);
                        $('#current_start_date').val(data.current_start_date);
                    }
                }});}
         }


         $('#btnSubmitEdit').on('click', function(e) {
           e.preventDefault();
           let $form = $('#edit_rate_form');
           $('.submit_errors').addClass('hidden');
           $('.submit_errors').text(''); 
           $("#btnSubmitEdit").prop('disabled', true);
           $(".submit_btn").addClass('hidden');
           $(".wait").removeClass('hidden');
           let $options = {
            dataType : "json",
            type : "POST",
            url : $($form).attr("action"),
            success : function (data) {
                $("#btnSubmitEdit").prop('disabled', false);
                $(".submit_btn").removeClass('hidden');
                $(".wait").addClass('hidden');
                if (data.success) {
                  swal({
                    title: "Dear! {{access()->user()->firstname}}",
                    text: "<h6> Rate Change Request Successfully Edited </h6>",
                    type: "success",
                    html: true,
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function(isConfirm) {
                    document.location.replace(base_url + "/compliance/employer/rates/profile/" + data.request_id);
                });        
              }else{
                if (data.errors) {
                   $.each(data.errors, function(key,valueObj){
                    $('.'+key+'_error').removeClass('hidden');
                    $('.'+key+'_error').text(valueObj);
                });
               } else {
                swal('Dear! {{access()->user()->firstname}}','Failed To Edit! Kindly try again','warning');
            }
        }
    },
    error: function ($data) {
        $("#btnSubmitEdit").prop('disabled', false);
        $(".submit_btn").removeClass('hidden');
        $(".wait").addClass('hidden');
        swal('Dear! {{access()->user()->firstname}}','An error seems to have occurred. Kindly try again','warning');
    },
};

$($form).ajaxSubmit($options);
});




     });


 </script>;

 @endpush



