@extends('layouts.backend.main', ['title' => trans('labels.backend.table.write_off_interest'), 'header_title' => trans('labels.backend.table.write_off_interest')])

@push('after-styles-end')
    {{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')

    {!! Form::model($employer,['route' => ['backend.compliance.employer.write_off_interest', $employer->id],
    'method'=>'put',
    'name' => 'write_off_interest']) !!}
    {!! Form::hidden('this_date', getTodayDate(), ['class' =>'this_date']) !!}
    {!! Form::hidden('wcf_start_date', getWCFLaunchDate(), ['class' =>'wcf_date']) !!}
    <div class="row">

        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!--employer header detail -->
                <strong> {!! Form::label( 'name', $employer->name, [ 'id'=> 'name'])
                !!}</strong>

                <small >
                    Reg #: {!! Form::label( 'reg_no', $employer->reg_no, [ 'id'=> 'reg_no'])
                     !!}
                </small>
            </h5>
            <legend></legend>
            <div>&nbsp;</div>
        </div>


        <div class="row">
            {{--From Date--}}
            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.min_date'):</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-7 col-lg-7 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="form-inline">

                                <span>      {!!  Form::selectMonth('min_month',null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' =>
                         'Month' ,'id'=>'min_month']) !!}
                        </span>

                                <span>      {!!  Form::selectRange('min_year',Carbon\Carbon::now()->format('Y'),2015,null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year','id'=>'min_year']) !!}
                        </span>

                            </div>
                        </div>
                        {!! Form::hidden('min_date') !!}
                        {!! $errors->first('min_date', '<span class="help-block label
                                               label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            {{--To Date--}}
            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.max_date'):</label> <span class="required_asterik">*</span></div>
                    <div class="col-xs-7 col-lg-7 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="form-inline">
                                                   <span> {!!  Form::selectMonth('max_month',null, ['class' => 'form-control search-select','style'=>'width:100px', 'placeholder' =>  'Month','id'=>'max_month']) !!}
                        </span>
                                <span> {!!  Form::selectRange('max_year',Carbon\Carbon::now()->format('Y'),2015,null, ['class' => 'form-control search-select', 'style'=>'width:65px',
                        'placeholder' =>
                         "Year", 'id'=>'max_year']) !!}
                        </span>

                            </div>
                            {!! Form::hidden('max_date') !!}
                            {!! $errors->first('max_date', '<span class="help-block label
                  label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.comments'):</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-6 col-lg-6 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'comments', null, [ 'id'=> 'comments',  'class' =>'form-control']) !!}
                            {!! $errors->first('comments', '<span class="help-block label
                               label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>

            </div>

            {{--Buttons--}}
            <div class="row">
                <div class="col-md-6" class="form-inline" >
                    <div class="element-form">
                        <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                        <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                            <div class="pull-right">

                                {!! link_to_route('backend.compliance.employer.profile',trans('buttons.general.cancel'), [$employer->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                                {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {!! Form::close() !!}
    {{--</section>--}}
@stop

@push('after-script-end')
{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

<script  type="text/javascript">
    $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
        $(this).height(0).height(this.scrollHeight);
    }).find( 'textarea' ).change();

    $(function() {
        $(".search-select").select2({});
        $('body').on('submit', 'form[name=write_off_interest]', function(e) {
            e.preventDefault();
            // Validate date -- from date
            var $day = 28;
            var $month = $('#min_month').val();
            var $year = $('#min_year').val();
            if (($year) && ($month) && ($day)) {
                $('input[name=min_date]').val($year + '-' + $month + '-' + $day);
            }else {
                $("input[name=min_date]").val("");
            }
            // Validate date -- to date
            var $max_day = 28;
            var $max_month = $('#max_month').val();
            var $max_year = $('#max_year').val();
            if (($max_year) && ($max_month) && ($max_day)) {
                $('input[name=max_date]').val($max_year + '-' + $max_month + '-' + $max_day);
            } else{
                $("input[name=max_date]").val("");
            }

            this.submit();

        });



    });
</script>;


@endpush
