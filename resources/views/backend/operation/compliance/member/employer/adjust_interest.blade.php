@extends('layouts.backend.main', ['title' => trans('labels.backend.member.adjust_interest'), 'header_title' => trans('labels.backend.member.adjust_interest')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    {{--<section id="content-wrapper">--}}
    {{--<div style="color-:#fff">--}}
    {!! Form::model($booking_interest,['route' => ['backend.compliance.employer.adjust_interest', $booking_interest->id],'method'=>'put',
    'id' => 'adjust_interest','name' => 'adjust_interest']) !!}
    {!! Form::hidden('this_date', getTodayDate() ) !!}
    {!! Form::hidden('wcf_start_date', getWCFStartDate()) !!}
    <div class="row">

        <div class="col-md-12">
            <h5 class="client-title">
                <i class="icon-circle"></i>
                <!-- ngIf: client.subStatus.description -->
                <strong> {!! Form::label( 'name', ($booking_interest->booking()->count()) ? $booking_interest->booking->employer->name : ' ', [ 'id'=> 'name'])
                !!}</strong>

                <small >
                    Reg #: {!! Form::label( 'reg_no',  ($booking_interest->booking()->count()) ? $booking_interest->booking->employer->reg_no : ' ', [ 'id'=> 'reg_no'])
                     !!}
                </small>
            </h5>

            <legend></legend>
            <div>&nbsp;</div>
        </div>


        {{--Contribution amount--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.received_amount'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input( 'text','received_amount',number_format( $booking_interest->booking->receiptCodes->sum('amount') , 2 , '.' , ',' ), ['class' => 'form-control', 'disabled' => true]) !!}

                        </div>
                    </div>
                </div>

                {{--payment date--}}
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.payment_date'):</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            {!! Form::input('text', 'payment_date',$booking_interest->booking->receiptCodes->first()->receipt->rct_date_formatted, ['class' => 'form-control', 'disabled' => true ]) !!}
                            {!! $errors->first('payment_date', '<span class="help-block label label-danger">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="row">
            {{--current interest--}}
            <div class="col-md-9" >
        <div class="element-form" >
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.current_interest'):</label></div>
            <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    {!! Form::input( 'text','amount', $booking_interest->amount_formatted, ['class' => 'form-control', 'disabled' => true]) !!}

                </div>
            </div>
        </div>
            </div>
        </div>

        {{--new payment date--}}
        <div class="row">
            <div class="col-md-9">
                <div class="element-form" >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right"><label>@lang('labels.backend.table.new_payment_date'):</label><span class="required_asterik">*</span></div>
                    <div class="col-xs-6 col-lg-6 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                            <div class="form-inline">

                                          <span>      {!!  Form::selectRange('payment_day',1,31,($booking_interest->new_payment_date) ? \Carbon\Carbon::parse($booking_interest->new_payment_date)->format('d') : null, ['class' => 'form-control search-select','style'=>'width:60px', 'placeholder' =>
                         'Day', 'id'=>'payment_day']) !!}
                        </span>

                                <span>      {!!  Form::selectMonth('payment_month',($booking_interest->new_payment_date) ? \Carbon\Carbon::parse($booking_interest->new_payment_date)->format('m') : null, ['class' => 'form-control search-select','style'=>'width:102px', 'placeholder' =>
                         'Month', 'id'=>'payment_month']) !!}

                        </span>

                                <span>      {!!  Form::selectRange('payment_year',Carbon\Carbon::now()->format('Y'),Carbon\Carbon::now()->subYears(50)->format('Y'),($booking_interest->new_payment_date) ? \Carbon\Carbon::parse($booking_interest->new_payment_date)->format('Y') : null, ['class' => 'form-control search-select','style'=>'width:65px',
                        'placeholder' =>
                         'Year','id'=>'payment_year']) !!}
                        </span>
                            </div>
                        </div>
                        {!! Form::hidden('new_payment_date') !!}
                        {!! $errors->first('new_payment_date', '<span class="help-block label
    label-danger">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            {{--Reason--}}
            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Reason:</label></div>
                    <div class="col-xs-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group" id = "">
                            {!! Form::select( 'reason_type_cv_id', $reason_types,null, [ 'id'=> 'reason_type_cv_id',  'class' =>'form-control search-select', 'placeholder' => '']) !!}
                            {!! $errors->first('reason_type_cv_id', '<span class="help-block label
                               label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            {{--Reason--}}
            <div class="col-md-9" >
                <div class="element-form"  >
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-4 text-xs-right required"><label>Remark:</label></div>
                    <div class="col-xs-6 col-lg-6 ol-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" id = "text_content">
                            {!! Form::textarea( 'comments', $booking_interest->adjust_note, [ 'id'=> 'comments',  'class' =>'form-control']) !!}
                            {!! $errors->first('comments', '<span class="help-block label
                               label-danger">:message</span>') !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--Buttons--}}
        <div class="row">
            <div class="col-md-6" class="form-inline" >
                <div class="element-form">
                    <div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
                    <div class="col-xl-8 col-lg-8 col-sm-12 col-md-9 col-xs-12">
                        <div class="pull-right">

                            {!! link_to_route('backend.compliance.employer.interest_profile',trans('buttons.general.cancel'), [$booking_interest->id],['id'=> 'cancel', 'class' => 'btn btn-primary site-btn cancel_button', ]) !!}

                            {!! Form::button(trans('buttons.general.submit'),['class' => 'btn btn-primary site-btn save_button', 'type'=>'submit']) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
        {!! Form::close() !!}
        {{--</section>--}}


        @stop


        @push('after-script-end')

{{ Html::script(asset_url(). "/nextbyte/plugins/select2/js/select2.min.js") }}

        <script  type="text/javascript">
            $(function () {
                $('.search-select').select2();
            });
            $('#text_content').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();
            //todoc
            $('#text_content_todo').on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();



            $('body').on('submit', 'form[name=adjust_interest]', function(e) {
                e.preventDefault();
                // Validate date -- new payment date
                var $day = $('#payment_day').val();
                var $month = $('#payment_month').val();
                var $year = $('#payment_year').val();
                if (($year) && ($month) && ($day)) {
                    $('input[name=new_payment_date]').val($year + '-' + $month + '-' + $day);
                }else {
                    $("input[name=new_payment_date]").val("");
                }
                this.submit();
            });

        </script>;


    @endpush
