@extends('layouts.frontend.main', ['title' => trans('labels.frontend.login.title')])

@section('content')
    <span class="login-mac-logo">
            <img  class="logo_img" src="{!! asset_url(). '/nextbyte/img/mac_legacy_logo.png' !!}">
        <br/>
        <br/>
        <hr/>
    </span>

    <div class="login-page">

        <div class="main-login-contain">

            <div style="background-color: white; color: black;">
                @include('includes.partials.messages')
            </div>
            <div class="login-circul text-center" style="align-items: center;">
                <i class="icon_lock_alt login-icon-circul"></i>
            </div>
            <div class="login-form">
                {!! Form::open(['url' => 'login', 'id' => 'form-validation', 'autocomplete' => 'off']) !!}
                <div class="login_input" style="margin-bottom: 10px">
                    {!! Form::input('text', 'username', null, ['class' => 'login-form-border', 'id' => 'username', 'placeholder' => trans('labels.frontend.login.username'), 'autocomplete' => 'off']) !!}
                    <span class="login-right-icon"><i class="icon icon_mail"></i></span>
                </div>
                <div style="margin-bottom: 5px">
                    {!! $errors->first('username', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
                <div class="login_input" style="margin-bottom: 10px">
                    {!! Form::input('password', 'password', null, ['class' => 'login-form-border', 'id' => 'password', 'placeholder' => trans('labels.frontend.login.password'), 'autocomplete' => 'off']) !!}
                    <span class="login-right-icon"><i class="icon icon_key"></i></span>
                </div>
                <div style="margin-bottom: 5px">
                    {!! $errors->first('password', '<span class="help-block label label-danger">:message</span>') !!}
                </div>
                <div class="checkbox checkbox-login-v1" @if (!env('TESTING_MODE', 0)) style="display: none;" @endif>
                    <div class="checkbox-squared">
                        {!! Form::input('checkbox', 'remember', null, ['id' => 'checkbox-squared1', 'autocomplete' => 'off']) !!}
                        <label for="checkbox-squared1"></label>
                        <span>@lang('labels.frontend.login.remember?')</span>
                    </div>
                </div>
                {!! Form::button(trans('labels.general.signin'), ['class' => 'btn btn-primary btn-login', 'type'=>'submit']) !!}
                {{--                <div class="goto-login">
                                        <div class="sign-up-login">
                                            <i class="arrow_back"></i>
                                            Go to <a href="register_v1-2.html">Sign up</a>
                                        </div>
                                        <div class="forgot-password-login">
                                            <a href="forgot_password_v1-2.html">
                                                Forgot password?
                                            </a>
                                        </div>
                                    </div>--}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
