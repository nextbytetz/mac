<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="bootstrap default admin template">
    <meta name="viewport" content="width=device-width">
    <title>{!! env('APP_NAME') !!} - {!! $title !!}</title>

    {{ Html::style(asset_url() . "/favicon/mac_favicon.ico", ['rel' => 'stylesheet icon', 'type' => 'image/x-icon']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon.png", ['rel' => 'apple-touch-icon']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-57x57.png", ['rel' => 'apple-touch-icon', 'sizes' => '57x57']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-72x72.png", ['rel' => 'apple-touch-icon', 'sizes' => '72x72']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-76x76.png", ['rel' => 'apple-touch-icon', 'sizes' => '76x76']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-114x114.png", ['rel' => 'apple-touch-icon', 'sizes' => '114x114']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-120x120.png", ['rel' => 'apple-touch-icon', 'sizes' => '120x120']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-144x144.png", ['rel' => 'apple-touch-icon', 'sizes' => '144x144']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-152x152.png", ['rel' => 'apple-touch-icon', 'sizes' => '152x152']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-180x180.png", ['rel' => 'apple-touch-icon', 'sizes' => '180x180']) }}

    {{ Html::style(asset_url() . "/global/css/bootstrap.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/icons_fonts/elegant_font/elegant.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/pages/global/css/global.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/global/css/components.min.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/layouts/layout-left-top-menu/css/layout.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/pages/login/login-v1/css/login.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/nextbyte/css/custom.css", ['rel' => 'stylesheet']) }}

    @stack('after-styles-end')

</head>
<body>
<div class="login-background">

    @yield('content')

</div>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
{{ Html::script(asset_url() . "/global/plugins/tether/dist/js/tether.min.js") }}
{{ Html::script(asset_url() . "/global/plugins/bootstrap/dist/js/bootstrap.min.js") }}
{{ Html::script(asset_url() . "/pages/global/js/global.min.js") }}

@stack('after-script-end')

</body>

</html>