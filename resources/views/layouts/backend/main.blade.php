<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="{!! env('META_CONTENT') !!}">
    <meta name="viewport" content="width=device-width">
    <meta name="_token" content="{{ csrf_token() }}" />
    <title>{!! env('APP_NAME') !!} - {!! $title !!}</title>

    {{ Html::style(asset_url() . "/favicon/mac_favicon.ico", ['rel' => 'stylesheet icon', 'type' => 'image/x-icon']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon.png", ['rel' => 'apple-touch-icon']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-57x57.png", ['rel' => 'apple-touch-icon', 'sizes' => '57x57']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-72x72.png", ['rel' => 'apple-touch-icon', 'sizes' => '72x72']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-76x76.png", ['rel' => 'apple-touch-icon', 'sizes' => '76x76']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-114x114.png", ['rel' => 'apple-touch-icon', 'sizes' => '114x114']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-120x120.png", ['rel' => 'apple-touch-icon', 'sizes' => '120x120']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-144x144.png", ['rel' => 'apple-touch-icon', 'sizes' => '144x144']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-152x152.png", ['rel' => 'apple-touch-icon', 'sizes' => '152x152']) }}
    {{ Html::style(asset_url() . "/favicon/apple-touch-icon-180x180.png", ['rel' => 'apple-touch-icon', 'sizes' => '180x180']) }}

    {{ Html::style(asset_url() . "/global/plugins/bootstrap/dist/css/bootstrap.min.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/global/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css", ['rel' => 'stylesheet']) }}


    {{--{{ Html::style(asset_url() . "/global/plugins/bootstrap/dist/css/bootstrap-theme.min.css", ['rel' => 'stylesheet']) }}--}}

    {{--{{ Html::style(asset_url() . "/global/plugins/jquery-ui/css/jquery-ui.min.css", ['rel' => 'stylesheet']) }}--}}
    {{ Html::style(asset_url() . "/global/plugins/jquery-ui/theme/pepper_grinder/css/jquery-ui.min.css", ['rel' => 'stylesheet']) }}

    {{--{{ Html::style(asset_url() . "/nextbyte/plugins/bootstrap/version_3/css/bootstrap.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/nextbyte/plugins/bootstrap/version_3/css/bootstrap-theme.min.css", ['rel' => 'stylesheet']) }}--}}

    {{ Html::style(asset_url() . "/icons_fonts/elegant_font/elegant.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/layouts/layout-left-top-menu/css/color/light/color-default.css", ['rel' => 'stylesheet', 'id' => 'site-color']) }}
    {{ Html::style(asset_url() . "/global/plugins/switchery/dist/switchery.min.css", ['rel' => 'stylesheet']) }}
    {{ Html::style(asset_url() . "/global/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/global/plugins/font-awesome/css/font-awesome.min.css", ['rel' => 'stylesheet']) }}

    {{-- start : Toastr Notifications CSS  (Mandatory for notifications)--}}
    {{ Html::style(asset_url() . "/global/plugins/toastr/build/toastr.min.css") }}
    {{ Html::style(asset_url() . "/global/plugins/AmaranJS/dist/css/amaran.min.css") }}
    {{ Html::style(asset_url() . "/global/plugins/AmaranJS/dist/css/animate.min.css") }}
    {{-- end : Toastr Notifications CSS --}}

    {{ Html::style(asset_url() . "/global/css/components.min.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/layouts/layout-left-top-menu/css/layout.css", ['rel' => 'stylesheet']) }}

    {{ Html::style(asset_url() . "/nextbyte/css/custom.css", ['rel' => 'stylesheet']) }}

    @stack('after-styles-end')

</head>




<body class="nav-light">

<div class="loader-overlay">

    <div class="loader-preview-area">

        <div class="spinners">
            <div class="loader">
                <div class="rotating-plane"></div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper">

    @include('backend.includes.top_bar')
    @include('backend.includes.search_overlay')

    <section id="main" class="container-fluid">
        <div class="row">

            @include('backend.includes.left_bar')

            <section id="content-wrapper"  >

                @if (env('TESTING_MODE', 0))
                    <div class="site-content-title">
                        <p style="background-color: darkred; color: white; font-weight: bold; text-align: center; max-height: 23px; border-radius: 3px;">
                            TESTING MODE
                        </p>
                    </div>
                @endif

                @if (access()->isSubstituted())
                    <div class="site-content-title">
                        <p style="background-color: #268b3c; color: white; font-weight: bold; text-align: center; max-height: 23px; border-radius: 3px;">
                            You are currently being substituted by <span style="color : antiquewhite;">{{ access()->user()->substitutingUser->name }}</span>, <a href="{{ route("backend.users.self_revoke_substitute", access()->id()) }}"  style="color : antiquewhite;text-decoration: underline;">Revoke</a>
                        </p>
                    </div>
                @endif

                @if (access()->substitutingCount())
                    <div class="site-content-title">
                        <p style="background-color: #5a6a8b; color: white; font-weight: bold; text-align: center; max-height: 23px; border-radius: 3px;">
                            You are currently substituting <span style="color : antiquewhite;">{{ access()->substitutingUsersList() }}</span>
                        </p>
                    </div>
                @endif


                <div id= "site-header-title" class="site-content-title">
                    <h2 class="float-xs-left content-title-main">{!! $header_title !!}</h2>

                    {!! Breadcrumbs::renderIfExists() !!}

                </div>

            </section>
            <section id="content-wrapper">

                @include('includes.partials.messages')

                <div class="content content-switch content-form-layout">

                                @yield('content')
                </div>
                @yield('content-ades')
            </section>

        </div>
    </section>

    <footer id="footer">
        Copyright&copy; {!! Carbon\Carbon::now()->format('Y') !!}, {!! env('COMPANY') !!}.
    </footer>

</div>
@include('backend.includes.assets.datetimepicker')
@include('backend.includes.assets.timepicki')
@include('backend.includes.assets.tinymce')
@include("backend.includes.partials.edit_employee_model")
@include("backend.finance.govt_intergration.treasury.includes.data_fail_modal")

@include('backend.includes.partials.annual_earning_modal')
<script>
    var base_url = "{!! url("/") !!}";
</script>

{{ Html::script(asset_url() . "/global/plugins/jquery/dist/jquery.min.js") }}
{{--{{ Html::script(asset_url() . "/global/plugins/jquery-ui/js/jquery-ui.min.js") }}--}}
{{ Html::script(asset_url() . "/global/plugins/jquery-ui/theme/pepper_grinder/js/jquery-ui.min.js") }}
{{ Html::script(asset_url() . "/global/plugins/tether/dist/js/tether.min.js") }}
{{--{{ Html::script(asset_url() . "/global/plugins/popper/popper.min.js") }}--}}
{{ Html::script(asset_url() . "/global/plugins/bootstrap/dist/js/bootstrap.min.js") }}
{{ Html::script(asset_url() . "/global/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js") }}
{{ Html::script(asset_url() . "/global/plugins/switchery/dist/switchery.min.js") }}
{{ Html::script(asset_url() . "/global/plugins/screenfull.js/dist/screenfull.min.js") }}
{{--{{ Html::script(asset_url() . "/global/plugins/classie/classie.js") }}--}}
{{ Html::script(asset_url() . "/global/js/site.min.js") }}
{{ Html::script(asset_url() . "/global/js/site-settings.min.js") }}
{{ Html::script(asset_url() . "/layouts/layout-left-top-menu/js/layout.js") }}
{{ Html::script(asset_url() . "/nextbyte/js/backend/helpers.js") }}
{{--{{ Html::script(asset_url() . "/nextbyte/plugins/socket/js/socket.io.min.js") }}--}}

{{ Html::script(asset_url() . "/global/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}



{{-- start : Toastr Notifications JS (Mandatory for notifications)--}}
{{ Html::script(asset_url(). "/global/plugins/toastr/build/toastr.min.js") }}
{{ Html::script(asset_url(). "/global/plugins/AmaranJS/dist/js/jquery.amaran.min.js") }}
{{-- end : Toastr Notifications JS --}}

{{-- start : For sound notification)--}}
{{ Html::script(asset_url(). "/nextbyte/sounds/howler.min.js") }}
{{-- end : For Sound Notification --}}

<script type="text/javascript">
    //Date picker
    $('#dob').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        defaultDate: '1995-01-01'
    })
</script>


<script>
    /*    var $server_addr = "{!! $_SERVER['SERVER_ADDR'] !!}";
    var $server_port = "{!! env("SOCKET_PORT") !!}";
    var $socket = io.connect("http://" + $server_addr + ":" + $server_port);
    var $sound = new Howl({
        src: ["{!! asset_url() . '/nextbyte/sounds/notification_jiggle.wav' !!}"]
    });
    $socket.emit('client_info', {user_id : {!! access()->id() !!}});
    $socket.on('message', function (data) {
        $sound.play();
        $.amaran({
            'theme' : 'colorful',
            'clearAll' : true,
            'sticky' : true,
            'closeButton' : true,
            'content' : {
                bgcolor:'#42c902',
                color:'#ffffff',
                message : data,
            },
            'position'  :'bottom left',
            'outEffect' :'slideBottom',
            'inEffect'  :'slideLeft'
        });
    });*/
</script>

{{ Html::script(asset_url(). '/nextbyte/js/backend/backend.js') }}
{{ Html::script(asset_url(). '/nextbyte/js/custom.js') }}

@stack('after-script-end')
</body>

</html>