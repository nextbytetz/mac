<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="{!! env('META_CONTENT') !!}">
    <meta name="viewport" content="width=device-width">
    <title>{!! env('APP_NAME') !!} - {!! $title !!}</title>

    {{ Html::style(route("guide"), ['rel' => 'canonical']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/plugins/font-awesome/3.2.1/css/font-awesome.min.css", ['rel' => 'stylesheet', 'media' => 'all', 'type' => 'text/css']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/css/bootstrap.css", ['rel' => 'stylesheet', 'media' => 'screen']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/css/responsive.css", ['rel' => 'stylesheet', 'media' => 'screen']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/css/prettify.css", ['rel' => 'stylesheet', 'media' => 'screen']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/css/prettyPhoto.css", ['rel' => 'stylesheet', 'media' => 'screen']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/css/idea.css", ['rel' => 'stylesheet', 'media' => 'screen']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/css/style.css", ['rel' => 'stylesheet', 'media' => 'screen']) }}
    {{ Html::style(asset_url() . "/nextbyte/guide/css/contact-form.css", ['rel' => 'stylesheet', 'media' => 'screen']) }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/jquery.js") }}
    {{--{{ Html::script(asset_url() . "/nextbyte/guide/js/jquery.scrollTo.js") }}--}}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/jquery-migrate-1.1.0.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/prettify.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/bootstrap-affix.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/jquery.prettyPhoto.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/TMForm.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/modal.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/bootstrap-filestyle.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/sForm.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/jquery.countdown.min.js") }}
    {{ Html::script(asset_url() . "/nextbyte/guide/js/scripts.js") }}



</head>

<body>
    @yield('content')

</body>

</html>
