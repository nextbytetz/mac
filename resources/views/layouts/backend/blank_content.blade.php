@extends('layouts.backend.main', ['title' => trans('labels.backend.home'), 'header_title' => trans('labels.general.welcome')])

@push('after-styles-end')
{{ Html::style(asset_url() . "/nextbyte/plugins/select2/css/select2.min.css") }}
@endpush

@section('content')
    <!-- Put the page specifically for this page here -->

@endsection

@push('after-script-end')
<!-- Custom javascript files for this page -->

@endpush