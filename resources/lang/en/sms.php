<?php

return [
    'employer' => [
        'verification' => [
            'approved' => 'Dear :name,\nYour employer verification with registration number :reg_no has been approved. Please log in to the online system to proceed with self-service.\nThank you',
            'rejected' => 'Dear :name,\nYour employer verification with registration number :reg_no has been subject to queries. Please log in to the online system for more information.\nThank you',
        ],
        'registration' => [
            'approved' => 'Dear :name,\nYour employer registration for :organization has been approved. Please log in to the online system to proceed with self-service.\nThank you',
            'rejected' => 'Dear :name,\nYour registration for :organization has been subject to queries.  Please log in to the online system for more information.\nThank you',
        ],
    ],
];