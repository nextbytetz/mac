<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [

        'finance' => [

            'currency' => [
                'created' => 'Success, Currency added',
                'deleted' => 'Success, Currency deleted',
                'updated' => 'Success, Currency Updated',
            ],
            'receipt' => [
                'created' => 'Success, Receipt added',
                'cancelled' => 'Success, Receipt Cancelled',
                'updated' => 'Success, Receipt Updated',
                'dishonoured' => 'Success, Receipt Dishonoured',
                'replace_cheque' => 'Success, Cheque No. Replaced',
                'linked_file_created' => 'Success, Linked file Uploaded',
                'contribution' => [
                    'updated' => 'Success, Contribution month has been updated',
                ],
            ],

            'payment_processed' => 'Success, Payments have been processed',
            'payment_voucher_trans_processed' => 'Success, Payment Voucher Transaction have been processed',
            'payment_voucher_paid' => 'Success, Payment Voucher has been paid',

        ],
        'roles' => [
            'created' => 'Success, Role has been created',
            'deleted' => 'Success, Role has been deleted',
            'updated' => 'Success, Role has been updated',
        ],
        'users' => [
            'created' => 'Success, User has been created',
            'deleted' => 'Success, User has been deleted',
            'updated' => 'Success, User has been updated',
        ],
        'inspection' => [
            'created' => 'Success, Inspection has been created',
            'deleted' => 'Success, Inspection has been deleted',
            'updated' => 'Success, Inspection has been updated',
            'cancelled' => 'Success, Inspection has been cancelled',
            'completed' => 'Success, Inspection has been completed',
            'profile' => [
                'created' => 'Success, Inspection Profile has been created',
                'deleted' => 'Success, Inspection Profile has been deleted',
                'updated' => 'Success, Inspection Profile has been updated',
            ],
        ],
        'inspection_task' => [
            'created' => 'Success, Inspection Task has been created',
            'deleted' => 'Success, Inspection Task has been deleted',
            'updated' => 'Success, Inspection Task has been updated',
            'assigned' => 'Success, All staffs has been assigned to employers',
            'employer' => [
                'updated' => 'Success, Inspection Task for this employer has been updated'
            ],
        ],
        'case'=>[
            'created' => 'Success, Case has been created',
            'deleted' => 'Success, Case has been deleted',
            'added' => 'Success, Case has been added',
            'updated' => 'Success, Case has been updated',
            'closed' => 'Success, Case has been closed',

            'lawyers' => [
                'created' => 'Success, Lawyer has been created',
                'deleted' => 'Success, Lawyer has been deleted',
                'updated' => 'Success, Lawyer has been updated',
            ],
            'hearing' => [
                'created' => 'Success, Case hearing has been created',
                'deleted' => 'Success, Case hearing has been deleted',
                'updated' => 'Success, Case hearing has been updated',
            ],
            'mentioning' => [
                'created' => 'Success, Case mentioning has been Added',
                'deleted' => 'Success, Case mentioning has been deleted',
                'updated' => 'Success, Case mentioning been updated',
            ],
        ],
        'contribution' => [
            'track_created' => 'Success, Contribution track created',
            'track_updated' => 'Success, Contribution track updated',
            'contribution_loaded' => 'Success, Contribution loaded',
        ],
        'compliance' => [
            'booking_adjusted' => 'Success, Booking Adjusted',

        ],
        'member' => [

            'employment_history_created' => 'Success, Employment History Created',
            'employment_history_updated' => 'Success, Employment History Updated',
            'employment_history_deleted' => 'Success, Employment History Deleted',

            'dependent_created' => 'Success, Dependent Created',
            'dependent_updated' => 'Success, Dependent Updated',

            'employee_created' => 'Success, Employee Created',
            'employee_updated' => 'Success, Employee Updated',
            'employee_deleted' => 'Success, Employee Deleted',

            'employer_created' => 'Success, Employer Created',
            'employer_updated' => 'Success, Employer Updated',
            'employer_deleted' => 'Success, Employer Deleted',


            'follow_up_created' => 'Success, Follow Up Created',
            'follow_up_updated' => 'Success, Follow Up Updated',
            'follow_up_deleted' => 'Success, Follow Up Deleted',

            'assign_staff' => 'Success, Staff Assigned',
            'assigned_staff_updated' => 'Success, Assigned Staff Updated',

            'unregistered_employer_created' => 'Success, Unregistered Employer Created',
            'unregistered_employer_updated' => 'Success, Unregistered Employer Updated',
            'unregistered_employer_deleted' => 'Success, Unregistered Employer Deleted',
            'unregistered_employer_mark_as_registered' => 'Success, Unregistered Employer Marked as Registered',

            'employer_registration_created' => 'Success, Employer Registration Created',
            'employer_registration_updated' => 'Success, Employer Registration Updated',
            'employer_registration_deleted' => 'Success, Employer Registration Deleted',

            'employer_registration_approved' => 'Success, Employer Registration Approved',
            'employer_registration_undone' => 'Success, Employer Registration Undone',

            'employee_contribution_created' => 'Success, Employee Contribution Created',
            'employee_contribution_updated' => 'Success, Employee Contribution Updated',

            'loading_employee' => 'Success, Employee are loaded into the system',

            'undo_loaded_employees' => 'Success, Undone loaded employees',

        ],

        'claim' => [

            'old_compensation_created' => 'Success, Old Compensation Created',
            'old_compensation_updated' => 'Success, Old Compensation Updated',
            'old_compensation_deleted' => 'Success, Old Compensation Deleted',

            'notification_report_created' => 'Success, New Notification Report Created',
            'notification_report_updated' => 'Success, Notification Report Updated',
            'notification_report_undone' => 'Success, Notification Report Undone',

            'medical_expense_created' => 'Success, Medical Expense Created',
            'medical_expense_updated' => 'Success, Medical Expense Updated',

            'investigators_assigned' => 'Success, Investigator assigned',
            'investigator_changed' => 'Success, Investigator changed',
            'investigator_deleted' => 'Success, Investigator deleted',

            'activate_investigation' => 'Success, Investigation Activated',
            'cancel_investigation' => 'Success, Investigation Cancelled',

            'investigation_report_feedback_updated' => 'Success, Investigation Report Feedback Updated',
            'health_provider_service_created' => 'Success, Health Provider Service Created',
            'health_provider_service_updated' => 'Success, Health Provider Service Updated',

            'current_employee_state_created' => 'Success, Current Employee State Created',
            'current_employee_state_updated' => 'Success, Current Employee State Updated',

            'approve_notification_to_claim' => 'Success, Notification report approved to Claim',
            'claim_assessed' => 'Success, Claim Assessed',

            'medical_practitioner_created' => 'Success, Medical Practitioner Created',
            'medical_practitioner_updated' => 'Success, Medical Practitioner Updated',
            'medical_practitioner_deleted' => 'Success, Medical Practitioner  deleted',

            'health_provider_created'  => 'Success, Health Provider Created',
            'health_provider_updated' => 'Success, Health Provider  Updated',
            'health_provider_deleted' => 'Success, Health Provider  deleted',

            'notification_report_rejected' => 'Success, Notification report rejected; Workflow has been initiated',
            'notification_report_verified' => 'Success, Notification Report Verified',
            'witness_created'  => 'Success, Witness Created',
            'witness_updated' => 'Success, Witness Updated',
            'witness_deleted' => 'Success, Witness deleted',

            'bank_details_updated' => 'Success, Bank Details Updated',

            'insurance_created' => 'Success, Insurance Created',
            'insurance_updated' => 'Success, Insurance Updated',
            'insurance_deleted' => 'Success, Insurance Deleted',

            'notification_contribution_track_updated' => 'Success, Notifiction Contribution Track Updated',

            'monthly_earning_updated' => 'Success, Monthly Earning Updated'

        ],
        'notification' => [
            'document_updated' => 'Document Register Updated',
        ],

        'payroll' => [
            'payroll_run' => 'Success, Payroll process has been initiated on the background, you will be notified on completion.',

        ],

        'interest' => [
            'adjust_interested' => 'Success, Interest Adjusted, Workflow has been initiated',
            'write_off_interested' => 'Success, Interest(s) written off, Workflow has been initiated',
            'track_updated' => 'Success, Contribution track updated',
            'interest_written_off_updated' => 'Success, Interest Write Off Updated',

            'interest_refund_created' => 'Success, Interest refund created',
            'interest_refund_updated' => 'Success, Interest refund updated',
            'interest_refund_undone' => 'Success, Interest refund undone',
        ],
        'workflow' => [
            'assigned' => 'Success, Selected workflow has been assigned to you',
            'updated' => 'Success, workflow has been updated',
        ],

        'system'=>[
            'code_value_created' => 'Success, Code Value created',
            'code_value_updated' => 'Success, Code Value updated',
            'code_value_deleted' => 'Success, Code Value deleted'
        ],

        'organization'=>[
            'fiscal_year_created' => 'Success, Fiscal Year created',
            'fiscal_year_updated' => 'Success, Fiscal Year updated',

        ],

    ],
];
