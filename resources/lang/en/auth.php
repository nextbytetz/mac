<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => 'These credentials do not match our records.',
    'general_error' => 'You do not have sufficient permission to perform the requested action.',
    'workflow_error' => 'You do not have workflow level access to perform the requested action',
    'socialite'     => [
        'unacceptable' => ':provider is not an acceptable login type.',
    ],
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'unknown'  => 'An unknown error occurred',
];
