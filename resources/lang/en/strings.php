<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Strings Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in strings throughout the system.
      | Regardless where it is placed, a string can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
        'access' => [
            'permissions' => [
                'edit_explanation' => 'If you performed operations in the hierarchy section without refreshing this page, you will need to refresh to reflect the changes here.',
                'groups' => [
                    'hierarchy_saved' => 'Hierarchy successfully saved.',
                ],
                'sort_explanation' => 'This section allows you to organize your permissions into groups to stay organized. Regardless of the group, the permissions are still individually assigned to each role.',
            ],
            'users' => [
                'delete_user_confirm' => 'Are you sure you want to delete this user permanently? Anywhere in the application that references this user\'s id will most likely error. Proceed at your own risk. This can not be un-done.',
                'if_confirmed_off' => '(If confirmed is off)',
                'restore_user_confirm' => 'Restore this user to its original state?',
                'group' => [
                    'delete' => 'Are you sure you want to delete all selected user(s)',
                    'approve' => 'Are you sure you want to approve all seleted user(s)',
                ],
            ],
        ],
        'dashboard' => [
            'title' => 'Administrative Dashboard',
            'welcome' => 'Welcome',
        ],
        'member' => [
            'tin_already_registered' => 'Employer with the same Tin Already Registered! Please Check!',

        ],
        'workflow' => [
            'description' => '',
            'forwarded' => 'This workflow record has already been forwarded. Please select a valid workflow process.',
            'archived' => 'This workflow record has been archived, please choose to un-archive to forward it.',
            'participated' => 'You have already participated in approving this workflow in another level. You can only participate in one level.',
            'assign?' => 'Would you like to assign this process to yourself?',
            'miss_level_access' => 'You don\'t have access to participate in this workflow level',
            'miss_restrictive_access' => "You are restricted to participate in this workflow level",
            'miss_access_to_assess_claim' => 'To perform Claim Assessment will require appropriate workflow level access',
        ],
        'general' => [
            'all_rights_reserved' => 'All Rights Reserved.',
            'are_you_sure' => 'Are you sure?',
            'boilerplate_link' => 'Laravel 5 Boilerplate',
            'continue' => 'Continue',
            'member_since' => 'Member since',
            'search_placeholder' => 'Search...',
            'see_all' => [
                'messages' => 'See all messages',
                'notifications' => 'View all',
                'tasks' => 'View all tasks',
            ],
            'status' => [
                'online' => 'Online',
                'offline' => 'Offline',
            ],
            'you_have' => [
                'messages' => '{0} You don\'t have messages|{1} You have 1 message|[2,Inf] You have :number messages',
                'notifications' => '{0} You don\'t have notifications|{1} You have 1 notification|[2,Inf] You have :number notifications',
                'tasks' => '{0} You don\'t have tasks|{1} You have 1 task|[2,Inf] You have :number tasks',
            ],
            'delete_message' => 'Are you sure you want to delete this item?',
        ],


    ],

    'frontend' => [

    ],

];
