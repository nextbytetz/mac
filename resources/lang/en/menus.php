
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */
    'general' => [

    ],

    'backend' => [
        'units' => 'Units',
        'employers' => [
            'title' => 'Members',
            'employees' => 'Employees',
            'employers' => 'Employers',
        ],
        'finance' => [
            'title' => 'Finance',
            'receipts' => 'Receipts',
        ],
        'compliance' => [
            'title' => 'Compliance',
            'inspection' => [
                'title' => 'Inspection Manager',
                'view' => 'View Inspection Report',
            ],
        ],
        'legal' => [
            'title' => 'Legal',
            'create' => 'Create Case',
            'notheard'=>'Not Heard',
            'lawyers'=>'Lawyers',
            'cases'=>'Cases'

        ],
        'claims' => [
            'title' => 'Claims',
            'search' => 'Search Claim',
            'track' => 'Track Claim',
        ],
        'payroll' => [
            'title' => 'Payroll',

        ],
        'reports' => [
            'title' => 'Reports',
            'all' => 'All',
        ],
        'admin' => [
            'title' => 'Admin',
            'users' => 'Users',
            'organization' => 'Organization',
            'system' => 'System',
        ],
        'osh' => [
            'title' => 'Prevention',
        ],
        'investment' => [
            'title' => 'Investment',
        ],
    
        'mis' => 'M.I.S',
        'documentation' => [
            'title' => 'Documentation',
            'manual' => 'User Manual',
        ],
        'help' => [
            'title' => 'Help',
            'about' => 'About',
        ],
    ],

    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'sw'    => 'Swahili',
            'en'    => 'English',
        ],
    ],
];
