<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'finance' => [
            'receipt' => [
                'cancel_receipt' => 'Cancel Receipt',
            ],
            'legacy_receipt' => [
                'edit_receipt' => 'Edit Receipt',
                'edit_months' => 'Edit Months'
            ],

            'process_payment' => ' Process Payment',
            'process_voucher' => ' Process Voucher',
                    ],

        'member' => [
            'new_notification' => 'New Notification',

            'employer' => [
                'receive_contribution' => 'Receive Contribution',
                'add_employee' => 'Add Employee',
                'issue_certificate'=> 'Issue Certificate',
                'reissue_certificate'=> 'Re-Issue Certificate',
                'modify_registration'=> 'Modify Registration',
                'self_staff_assign' => 'Self Staff Assign',
                'assign_staff' => 'Assign Staff',
                'change_staff_assignment' => 'Change Staff Assignment',
                'issuance_reg_no' => 'Issuance Reg No.',
                'registration_profile'=> 'Registration Profile',
                'mark_as_registered' => 'Mark As Registered',
                'verification' => 'Online Verification',
                'online_employer_profile' => 'Manage Online Employer Profile',
                'load_employee_list' => 'Load Employees List',
                'interest_refund' => 'Interest Refund',
            ],

            'employee' => [
               'edit' => 'Edit',
                'add_contribution' => 'Add Contribution'

            ],

        ],
        'legal' => [
            'case' => [
                'add_hearing' => 'Add Case Proceeding',
                'add_mentioning' => 'Add Case Mentioning',
                'close' => 'Close Case',
            ],
        ],
        'claim' => [
            'modify_notification' => 'Modify Notification',
            'assign_investigator' => 'Assign Investigators',
            'add_medical_expense' => 'Add Medical Expense',
            'acknowledge' => 'Acknowledge',
            'print_investigation_report' => 'Print Investigation Report',
            'document_centre' => 'Document Centre',
            'document_register' => 'Document Register',
            'claim_assessment' => 'Claim Assessment',
            'add_witness' => 'Add New Witness',
            'edit_witness' => 'Edit Witness',
            'doctor_modification' => 'Doctors Modification',
            'contribution_tracks' => 'Contribution Tracks',

        ],
        'compliance' => [
            'booking_adjust' => 'Adjust Booking',
            'contribution_transfer' => 'Contribution Transfer',
        ],
    ],
    'general' => [
        'cancel' => 'Cancel',
        'close' => 'Close',
        'confirm' => 'Yes',
        'reject' => 'Reject',
        'undo'=>'Undo',
        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit'   => 'Edit',
            'update' => 'Update',
            'view'   => 'View',
            'generate' => 'Generate',
        ],
        'save' => 'Save',
        'submit' => 'Submit',
        'print' => 'Print',
        'dishonour' => 'Dishonour',
        'upload' => 'Upload',
        'upload_linked_file' => 'Upload Contribution File',
        'upload_bulk'=> 'Upload Bulk',
        'load_contribution' => 'Load Contribution',
        'enable_billing' => 'Enable Billing',
        'disable_billing' => 'Disable Billing',
        'load' => 'Review and Load',
        'replace_cheque' => 'Replace Cheque',
        'add_track' => 'Add Track',
        'add_new' => 'Add New',
        'adjust_interest' => 'Adjust Interest',
        'write_off_interest' => 'Write Off Interest',
        'online_approval' => 'Online Approval',
        'approve' => 'Approve',
        'no' => 'No',
        'assess' => 'Assess',
        'view' => 'View',
        'more' => 'More',
        'modify' => 'Modify',
        'register' => 'Register',
        'verify' => 'Verify',
        'pay' => 'Pay',
        'investigate' => 'Investigate',
        'cancel_investigation'=> 'Cancel Investigation',
        'auth' => [
            'confirm_account' => 'Confirm Account',
            'reset_password'  => 'Reset Password',
        ],
        'search' => 'Search',
        'initiate'=> 'Initiate',
        'add' => 'Add',
        'remove' => 'Remove',
    ],
];
