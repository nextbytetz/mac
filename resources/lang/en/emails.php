<?php

return [
    'employer' => [
        'verification' => [
            'approved' => [
                'subject' => 'Employer Verification Approved',
                'line_1' => 'Your employer verification has been approved.',
                'line_2' => "Request to verify employer with registration number <span style='border-bottom: 1px dashed #999;text-decoration: none;color : #030b06;'>:reg_no</span> received on <b>:date</b> ha been approved on <b>:approve_date</b>. Employer has been added to your profile.",
                'line_3' => 'Now you can get all self services from your online employer profile. For further more help and support, please contant us.',
                'button' => 'View Employer Profile',
            ],
            'rejected' => [
                'subject' => 'Queried Employer Verification',
                'line_1' => 'Your employer verification has been queried',
                'line_2' => "Request to verify employer with registration number <span style='border-bottom: 1px dashed #999;text-decoration: none;color : #030b06;'>:reg_no</span> received on <b>:date</b> are subject to some queries",
                'line_3' => 'You will find all the reviews from the WCF Staff on the dashboard of the employer verification. For further more help and support, please contact us.',
                'button' => 'View Verification Profile',
            ],
        ],
        'registration' => [
            'approved' => [
                'subject' => 'Employer Registration Approved',
                'line_1' => 'Your Employer Registration has been approved',
                'line_2' => "Information for <span style='border-bottom: 1px dashed #999;text-decoration: none;color : #030b06;'>:name</span> received on <b>:date</b> has been approved on <b>:approve_date</b>",
                'line_3' => 'Now you can get all self services from your online employer profile. For further more help and support, please contact us.',
                'button' => 'View Employer Profile',
            ],
            'rejected' => [
                'subject' => 'Queried Employer Registration',
                'line_1' => 'Your employer registration has been queried',
                'line_2' => "Information for <span style='border-bottom: 1px dashed #999;text-decoration: none;color : #030b06;'>:name</span> received on <b>:date</b> are subject to some queries.",
                'line_3' => 'You will find all the reviews from the WCF Staff on the dashboard of the employer profile. For further more help and support, please contact us.',
                'button' => 'View Employer Profile',
            ],
        ],
    ],
    'thanks' => 'Thanks',
    'wcf' => 'Workers Compensation Fund',
];