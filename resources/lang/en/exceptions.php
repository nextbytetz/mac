<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'roles' => [
                'already_exists'    => 'That role already exists. Please choose a different name.',
                'cant_delete_admin' => 'You can not delete the Administrator role.',
                'create_error'      => 'There was a problem creating this role. Please try again.',
                'delete_error'      => 'There was a problem deleting this role. Please try again.',
                'has_users'         => 'You can not delete a role with associated users, you may need to remove users from this role first',
                'needs_permission'  => 'You must select at least one permission for this role.',
                'not_found'         => 'That role does not exist.',
                'update_error'      => 'There was a problem updating this role. Please try again.',
            ],
            'users' => [
                'update_error' => 'There was a problem updating this user. Please try again.',
                'role_needed' => 'You must choose at least one role.',
                'permission_needed' => 'You must choose at least one role or permission',
                'user_not_found' => 'User Not Found! Please check',
                'admin_needed' => 'There must be at least one administrator, none will exist if we allow this edit',
            ],
        ],
        'finance' => [
            'fin_code_group_not_found' => 'Fin Code Group Not Found',
            'bank_not_found' => 'Bank Not Found',
            'bank_branch_not_found' => 'Bank Branch Not Found',
            'payment_voucher_not_found' => 'Payment Voucher Not Found',
            'payment_voucher_transaction_not_found' => 'Payment Voucher Transaction Not Found',
            'payroll_proc_not_found' => 'Payroll Proc Not Found',
            'monthly_payroll_already_run' => 'Monthly Payroll Already Run This Month',
            'currencies' => [
                'already_exists'    => 'That currency already exists. Please choose a different name.',
                'cant_delete_admin' => 'You can not delete the Administrator role.',
                'create_error'      => 'There was a problem creating this currency. Please try again.',
                'delete_error'      => 'There was a problem deleting this currency. Please try again.',
                'needs_permission'  => 'You must select at least one permission for this role.',
                'not_found'         => 'That currency does not exist.',
                'update_error'      => 'There was a problem updating this currency. Please try again.',
            ],
            'receipts' => [
                'already_exists'    => 'That receipt already exists. Please choose a different receipt.',
                'cant_delete_admin' => 'You can not delete the Administrator role.',
                'create_error'      => 'There was a problem creating this receipt. Please try again.',
                'delete_error'      => 'There was a problem deleting this receipt. Please try again.',
                'needs_permission'  => 'You must select at least one permission for this role.',
                'not_found'         => 'That receipt does not exist.',
                'not_found_cancelled'  => 'That receipt does not exist or has already been cancelled.',
                'update_error'      => 'There was a problem updating this receipt. Please try again.',
                'cancel_receipt_period' => 'You can not cancel this receipt has already exceeded period allowed to cancel receipt',
                'no_cheque'      => 'This receipt does not have cheque. Cant be dishonored! Please Check!',
                'dishonoured_cheque_not_found' => 'Dishonoured Cheque not found',
                'already_dishonored' =>  'This receipt has already dishonored! Please Check!',
                'contribution' => [
                    'upload' => 'No file was uploaded, please make sure that you have selected a file before clicking upload',
                ],
                'max_entries' => "Only total of :total contributions can be registered",
                "select_month" => "Contribution month and year must be selected",
                'greater' => 'Bank amount is greater than the total contributions',
                'less' => 'Bank amount is less than the total contributions',
                'matched' => 'Amount tallied',
                'duplicate_month' => 'Contribution month already picked, choose another',
                'date_threshold' => 'Contribution date exceeded maximum allowed limit of :max months from now',
                'not_tallied' => 'Amount paid in bank does not tally with the total contribution entries, submission cancelled.',
                'small_amount' => 'Amount paid in the bank can not be less than :amount',
            ],
            'receipt_codes' => [
                'already_exists'    => 'That receipt code already exists. Please choose a different receipt.',
                'not_found'         => 'That receipt code does not exist.',
                'not_found_cancelled'  => 'That receipt does not exist or has already been cancelled.',
                'update_error'      => 'There was a problem updating this receipt. Please try again.',
                'linked_file_not_allowed'   => 'File attached not allowed! Choose excel file only!',
            ],
            'booking' => [
                'already_exists'    => 'That Booking already exists. Please choose a different booking.',
                'not_found'         => 'That Booking does not exist.',
                'not_found_cancelled'  => 'That booking does not exist or has already been cancelled.',
                'update_error'      => 'There was a problem updating this booking. Please try again.',
                'interest_not_found'         => 'That Booking Interest write off does not exist.',
                'interest_write_off_not_found'         => 'That Booking Interest does not exist.',
                'interest_not_found_date_range'  => 'There is no eligible interest to write off in the date range',
                'interest_not_eligible_date_range'  => 'There are  ineligible interest(s) to be written off in the date range! Either already paid or written off',
                'nothing_is_selected'  => 'Nothing is selected! Please Check!',

            ],
            'booking_interest' => [
                'already_exists'    => 'That Booking interest already exists. Please choose a different booking.',
                'not_found'         => 'That Booking interest does not exist.',
                'not_refundable'    => 'This Interest is not refundable! Please check!'
            ],
        ],
        'upload' => [
            'column_missing2' => "<div>Column <span class='tag tag-success'>:column</span> is missing in your uploaded excel file, please add it.</div>",
            'column_missing' => "Column :column is missing in your uploaded excel file, please add it.",
            'entries' => 'invalid entry :entry on column :column',
            'upload_error'=> 'There is a problem with uploading list of employee document! Please check instruction before uploading.',
            'list_of_employee_error'=> 'List of Employee Error'
        ],

        'compliance' => [
            'employer_not_found' => 'Employer not found',
            'unregistered_employer_not_found' => 'Unregistered Employer not found',
            'employee_not_found' => 'Employee not found',
            'employee_already_exist' => 'Employee already exist! Please check!',
            'dependent_not_found' => 'Dependent not found',
            'contribution_not_found' => 'Contribution not found',
            'contribution_already_exist' => 'Contribution Already Exist',
            'contribution_track_not_found' => 'Contribution track not found',
            'contribution_track_dont_belong' => 'Contribution track dont belong to this user! Please check!',
            'max_date_greater' => 'Max date must be greater than min date! Please check!',
            'dates_exceed_this_month' => 'Dates must not be later than this month! Please check!',
            'unregistered_employer_follow_up_not_found' => 'Unregistered Employer Follow up not found',
            'user_not_assigned_to_this_employer' => 'User not assigned to this unregistered employer',
            'unregistered_employer_not_assigned' => 'Unregistered Employer not yet assigned to staff! Please check!',
            'tin_not_filled' => 'Tin must be filled to proceed! Please check!',
            'employer_with_tin_exist' => 'Employer with this Tin already exist! Please check if is already registered!',
            'employer_need_to_finish_registration' => 'Employer Need to Complete Registration from Compliance before proceeding with Receipting',
            'unregistered_employer_already_registered' => 'Unregistered Employer Already registered, can not be amended! Please check!',
            'employer_with_vote_exist' => 'Employer with this Vote already exist! Please check!!',
            'employer_with_name_exist' => 'Employer with this Name already exist! Please check!!',
            'employee_count_not_found' => 'Employee Count not found',
            'employee_count_not_filled' => 'Employee Count not filled',
            'tin_certificate_not_attached' => 'Tin Certificate not attached',
            'brela_certificate_not_attached' => 'Brela Certificate not attached',
            'list_of_employee_not_attached' => 'List of Employees not attached',
            'employee_list_doc_has_error' => 'List of Employees Document has Error! Please Check!',

            'unregistered_followup_inspection_not_found' => 'Unregistered Follow up Inspection not found',
            'unregistered_followup_inspection_task_not_found' => 'Unregistered Follow up Inspection Task not found',
            'unregistered_inspection_profile_not_found' => 'Unregistered Inspection profile not found',
            'profile_employer_count_validation' => 'Selected profile has less employers than required number',

            'inspection_has_active_task' => 'Inspection has active task(s), deactivate them first before proceeding',
            'inspection_not_yet_completed' => 'Inspection is not fully completed! Please check!',
            'bank_detail_not_found' => 'Bank Detail not found',
            'inspection_task_not_found' => 'Inspection Task not Found.',
            'inspection_not_found' => 'Inspection not Found.',
            'upload' => 'No file was uploaded, please make sure that you have selected a file before clicking upload',
            'age_limit_not_correct' => 'Age for this dependent is not correct',
            'gender_not_correct' => 'Gender Incorrect! Please check!'

        ],
        'claim' => [
            'notification_report_not_found' => 'Notification report not found',
            'death_not_found' => 'Death Notification not found',
            'disease_not_found' => 'Disease Notification not found',
            'accident_not_found' => 'Accident Notification not found',
            'witness_not_found' => 'Witness Not Found',
            'accident_witness_not_found' => 'Accident Witness Not Found',
            'accident_type_not_found' => 'Accident Type not found',
            'insurance_not_found' => 'Insurance not found',
            'employment_history_not_found' => 'Employment History not found',
            'employer_not_filled' => 'Employer not filled',
            'pensioner_not_found' => 'Pensioner not found',
            'dependent_not_found' => 'Dependent not found',
            'incident_type_not_found' => 'Incident Type not found',
            'benefit_type_not_found' => 'Benefit Type Not Found',
            'claim_not_found' => 'Claim Not Found',
            'claim_compensation_not_found' => 'Claim Compensation Not Found',
            'employee_old_compensation_not_found' => 'Employee Old Compensation not found',
            'dependent_type_not_found' => 'Dependent Type not found',
            'reporting_date_not_correct' => 'Reporting Date should be greater or equal to incident date',
            'receipt_date_not_correct' => 'Notification Date should be greater or equal to incident date and reporting date to employer',
            'disease_know_how_not_found' => 'How this occupational disease was discovered not found',
            'death_cause_not_found' => 'Disease Cause not found',
            'disability_state_checklist_not_found' => 'Disability State Checklist not found',
            'health_state_checklist_not_found' => 'Health State Checklist not found',
            'member_type_not_found' => 'Member Type not found',
            'notification_health_state_not_found' => 'Notification Health State not found',
            'notification_disability_state_not_found' => 'Notification Disability State not found',
            'notification_disability_state_already_exist' => 'Notification Disability State Already exist for this medical expense',
            'medical_expense_not_found' => 'Medical Expense not found',
            'medical_expense_already_exist' => 'Medical Expense Already Exist! Please Check',
            'investigator_not_found' => 'Investigator not found',
            'investigators_cant_be_the_same' => 'Investigators can not be the same',
            'investigation_report_update_by_assigned_investigators' => 'Investigation report can only be updated by Assigned Investigators',
            'investigators_already_assigned' => 'Investigator already assigned for this Investigation',
            'investigation_feedback_not_found' => 'Investigation feedback not found',
            'investigation_question_not_found' => 'Investigation Question not found',
            'health_provider_not_found' => 'Health Provider not found',
            'medical_practitioner_not_found' => 'Medical Practitioner not found',
            'notification_health_provider_not_found' => 'Notification Health Provider not found',
            'notification_health_provider_practitioner_not_found' => 'Notification Health Provider Practitioner not found',
            'health_service_checklist_not_found' => 'Health Service Checklist not found',
            'notification_health_provider_service_not_found' => 'Notification Health Provider Service not found',
            'rank_notification_health_provider_exist' => 'Rank for this notification health provider already exist! Please check',
            'percentage_pd_dont_exceed_100' => 'Percent of Permanent Disability (PD%) should not exceed 100',
            'hours_worked_per_day_dont_exceed_8' => 'Hours worked per day should not exceed 8',
            'percentage_ed_ld_dont_exceed_100' => 'Percentage for Excuse Duty (ED) / Light Duty (LD) should not exceed 100%',
            'notification_disability_state_assessment_not_found' => 'Notification Disability State Assessment Not Found',
            'assistant_not_allowed_more_than_two'=> 'Not allowed to have two pensionable assistants at the same time! Please check!',

            //validate approval to claim
            'monthly_earning_not_found' => 'Monthly Earning (Contribution) before incident date not found! Please check!',
            'investigation_report_not_updated' => 'Investigation Report not updated! Please check!',
            'notification_report_not_verified' => 'Notification Report not verified! Please check!',
            'health_services_not_added' => 'Health Services (Medical Care) not added! Please check!',
            'dependents_not_added' => 'Dependents not added! Please check!',
            'bank_details_not_updated' => 'Bank Details not updated! Please check!',
            //assessment level validation
            'assessment_confirm' => 'Claim Assessment not done! Please Check!',
            'nature_of_incident_not_modified' => 'Nature of incident must be modified at this level! Modify to Proceed with approval',
            'document_not_found' => 'Document not found',
            'notification_contribution_track_not_found' => 'Notification Contribution Track not found',
            'track_is_already_updated' => 'Track is already updated! Please Check!',
            'not_in_your_department' => 'Track not in your department level! Please Check!',
            'documents_not_uploaded' => 'Document(s) not uploaded! Please Make sure all mandatory documents are attached!',
            'police_report_not_uploaded' => 'Police report document not uploaded! Please Check!',
            'spouse_marriage_document_not_uploaded' => 'Spouse Marriage Certificate not uploaded! Please Check!',

            'contribution_tracks_not_completed'=> 'Contribution Tracks not completed! Please Check!',

        ],

        'payroll' => [
            'payroll_run_not_found' => 'Payroll Run Not found',
        ],

        'sysdef' => [
            'location_type_not_found' => 'Location Type not found',
            'identity_type_not_found' => 'Identity Type not found',
            'job_title_not_found' => 'Job Title not found',
            'country_not_found' => 'Country not found',
            'gender_not_found' => 'Gender not found',
            'district_not_found' => 'District not found',
            'date_range_not_found' => 'Date Range not found',
        ],

        'organization' => [
            'fiscal_year_not_found' => 'Fiscal Year not found',

        ],
        'workflow' => [
            'missing_contribution_file' => 'At least one of the registered contribution(s) is missing an uploaded file. Make sure to upload all the contribution schedule for all the registered contribution(s).',
            'file_not_uploaded' => 'Contribution file(s) have not yet been full uploaded, please wait or make sure that all contribution has been loaded.',
            'level_error' => 'This action is not at its appropriate workflow level, please complete the necessary workflow levels to reach this level',
            'level_not_assigned' => 'This action is at its appropiate workflow level but not assigned to any user.',
            'can_not_initiate_action' => 'This action can  be initiated at pending level 1.',
            'module_not_found' => 'Workflow module not found',
            'verification_exist' => 'This employer has already been verified to another user, please check the verification list',
        ],
        'contribution' => [
            'amount_mismatch' => 'Error Loading Contribution! Contribution amount registered is not equal to total imported amount from the uploaded contribution file',
        ],
        'inspection' => [
            'profile' => [
                'no_employer' => 'There is no employer in the selected inspection profile, make sure that the selected inspection profile has employers'
            ],
            'task' => [
                'many_regions' => 'Can not assign staff to employers, there are :region regions(s) to be involved for the selected employers, but only :staff selected for this task',

            ],
        ],
        'legal' => [
            'no_lawyer' => 'There is no lawyer found',
            'has_hearings' => 'Can not delete lawyer with associated case hearings',
            'has_mentions' => 'Can not delete lawyer with associated case mentioning',
        ],
        'storage' => [
            'remove_error' => 'Can not remove file in this context',
            'rename_error' => 'Can not rename file in this context',
            'name_error' => 'Invalid name : :name',
            'path_error' => 'Path already exists : :new',
            'list_error' => 'Could not list path : :dir',
            'name_exist' => 'Folder :name already exists',
            'path_exist' => 'Path does not exist: :path',
            'base_error' => 'Base directory does not exist',
            'select_document' => 'Please select a document type from the document library list',
        ],
    ],
    'frontend' => [
        'session_expired' => 'Your session has expired, please log in to continue',
        'token_mismatch' => 'Sorry, we could not verify your request. Please try again or reload this page to continue.',
    ],
    'general' => [
        'error' => 'There was the problem on performing the requested action, please try again',
        'unauthorized' => 'Unauthorized, session expired. Please log in to continue',
        'date_exceed_today' => 'Date exceed todays date',
        'date_is_before_wcf_start' => 'Date is before WCF Start! Date Not Valid',
        'gratuity_percentage_exceed_100' => 'Total dependents Gratuity Percentages exceed 100%',
        'pension_percentage_exceed_100' => 'Total dependents Pension Percentages exceed 100%',
        'funeral_grant_percentage_exceed_100' => 'Total dependents Funeral Grant Percentages exceed 100%',
        'funeral_grant_percentage_dont_reach_100' => 'Total dependents Funeral Grant Percentages distribution does not reach 100%! Please Check',
        'nothing_to_process'=> 'Nothing to process! Please check!',
        'can_not_delete_foreign_key'=> 'This item has already been referenced, can not be deleted. Foreign key constraint fails!',
        "phone" => "The :attribute field contains an invalid number.",
        'exists' => "This value already exists",
    ],
];
