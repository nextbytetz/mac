<?php
/**
/**
 * Finance
 */
Route::group([
//    'prefix' => 'investment',
    'as' => 'investment.',
    'namespace' => 'Report',
], function() {

    Route::group([ 'prefix' => 'report', 'as' => 'report.'], function() {


        /**
         * Receipt Performance report
         */
         Route::get('corporate_bond_schedule', 'InvestmentReportController@corporateBond')->name('schedule.corporate_bond');

        Route::get('divident_schedule', 'InvestmentReportController@dividentSchedule')->name('schedule.divident');

        Route::get('yrs_treasury_bond_schedule', 'InvestmentReportController@yrsTreasuryBond')->name('yrs_treasury_bond_schedule');

        Route::get('fixed_deposits_schedule', 'InvestmentReportController@fixedDeposit')->name('schedule.fixed_deposits');

        Route::get('interest_call_account', 'InvestmentReportController@interestCallAccount')->name('interest_call_account');

        Route::get('listed_equities', 'InvestmentReportController@listedEquities')->name('listed.equities');

        Route::get('treasury_bill_schedule', 'InvestmentReportController@treasuryBill')->name('schedule.treasury_bill');

        Route::get('treasury_bond_schedule', 'InvestmentReportController@treasuryBond')->name('schedule.treasury_bond');

        Route::get('utt_bond_fund', 'InvestmentReportController@uttBondFund')->name('utt_bond_fund');
           //start 
        Route::get('market_value_changes', 'InvestmentReportController@marketValueChange')->name('market_value_changes');

        Route::get('investment_portfolio', 'InvestmentReportController@investmentPortfolio')->name('investment_portfolio');

        Route::get('compliance_listed_stock', 'InvestmentReportController@complianceListedStock')->name('compliance_listed_stock');

        Route::get('compliance_deposit_holding', 'InvestmentReportController@complianceDepositHolding')->name('compliance_deposit_holding');

        Route::get('budget_actual_amount', 'InvestmentReportController@budgetActualAmount')->name('budget_actual_amount');

        Route::get('maturing_actual_expected_maturing', 'InvestmentReportController@maturingActualExpected')->name('maturing_actual_expected_maturing');

        Route::get('income_from_investment', 'InvestmentReportController@incomeFromInvestment')->name('income_from_investment');

        Route::get('average_return_fund', 'InvestmentReportController@averageReturnFund')->name('average_return_fund');
       
    });
});