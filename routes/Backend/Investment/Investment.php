<?php
/**
/**
 * Investment portfolio 
 */
Route::group([
	'namespace' => 'Investment',
], function() {



    // Route::group(['prefix' => 'investment',  'as' => 'investment.', 'middleware'=>'access.routeNeedsPermission:view_investment_menu'], function() {
	Route::group(['prefix' => 'investment',  'as' => 'investment.', 'middleware'=>'access.routeNeedsPermission:view_investment_menu'], function() {

		Route::get('/', function () {
			return view('backend.investment.menu');
		})->name('menu');
		

		Route::get('dashboard', 'InvestmentController@dashboard')->name('dashboard');
		Route::get('select_investment_type', 'InvestmentController@returnInvestmentTypes')->name('select_investment_type'); 
		Route::get('choose_type', 'InvestmentController@chooseTypeForm')->name('choose_type'); 
		Route::post('choose_type', 'InvestmentController@chooseType')->name('choose_type');
		Route::get('bond_calculator', 'InvestmentController@bondCalculate')->name('bond_calculator');


		Route::post('fixed_deposit', 'InvestmentController@fixedDeposit')->name('fixed_deposit'); 
		Route::post('fixed_deposit_update', 'InvestmentController@fixedDepositUpdate')->name('fixed_deposit_update'); 
		Route::get('get_fixed_deposit/{financial_year_id}', 'InvestmentController@getFixedDeposit')->name('get_fixed_deposit'); 
		Route::post('fixed_deposit_store_update', 'InvestmentController@fixedDepositStoreUpdate')->name('fixed_deposit_store_update'); 

		Route::post('treasury_bills', 'InvestmentController@treasuryBills')->name('treasury_bills'); 
		Route::get('get_treasury_bills/{financial_year_id}', 'InvestmentController@getTreasuryBills')->name('get_treasury_bills'); 
		Route::post('treasury_bills_update', 'InvestmentController@treasuryBillsUpdate')->name('treasury_bills_update');
		Route::post('treasury_bills_store_update', 'InvestmentController@treasuryBillsStoreUpdate')->name('treasury_bills_store_update');

		Route::post('treasury_bonds', 'InvestmentController@treasuryBonds')->name('treasury_bonds'); 
		Route::get('get_treasury_bonds/{financial_year_id}', 'InvestmentController@getTreasuryBonds')->name('get_treasury_bonds');
		Route::post('treasury_bonds_update', 'InvestmentController@treasuryBondsUpdate')->name('treasury_bonds_update');
		Route::post('treasury_bonds_store_update', 'InvestmentController@treasuryBondsStoreUpdate')->name('treasury_bonds_store_update');

		Route::post('corporate_bonds', 'InvestmentController@corporateBonds')->name('corporate_bonds'); 
		Route::get('get_corporate_bonds/{financial_year_id}', 'InvestmentController@getCorporateBonds')->name('get_corporate_bonds'); 
		Route::post('corporate_bonds_update', 'InvestmentController@corporateBondsUpdate')->name('corporate_bonds_update');
		Route::post('corporate_bonds_store_update', 'InvestmentController@corporateBondsStoreUpdate')->name('corporate_bonds_store_update');  


		Route::get('show/{investment_type}/{investment_allocation_id}', 'InvestmentController@show')->name('show');
		Route::get('employers', 'InvestmentController@getEmployers')->name('employers');

		// summary
		Route::group(['prefix' => 'summary',  'as' => 'summary.'], function() {
			Route::get('/', 'InvestmentSummaryController@index')->name('index');
			Route::get('datatable/{financial_year_id}', 'InvestmentSummaryController@datatable')->name('datatable');
			Route::get('investment_type/{financial_year_id}/{budget_alocation_id}/{code_value_id}', 'InvestmentSummaryController@investmentType')->name('investment_type');
			Route::get('investment_type_datatable/{financial_year_id}/{budget_alocation_id}/{code_value_id}', 'InvestmentSummaryController@investmentTypeDatatable')->name('investment_type_datatable');
			Route::get('show/{investment_id}/{code_value_id}/{financial_year_id}', 'InvestmentSummaryController@show')->name('show');
			Route::get('show_datatable/{investment_id}/{code_value_id}', 'InvestmentSummaryController@showDatatable')->name('show');
			Route::get('show_fixed_datatable/{investment_id}', 'InvestmentSummaryController@showFixedDatatable')->name('show_fixed_datatable');
			Route::get('treasury_bonds_datatable/{investment_id}', 'InvestmentSummaryController@showTBondsDatatable')->name('treasury_bonds_datatable');
			Route::get('treasury_bills_datatable/{investment_id}', 'InvestmentSummaryController@showTBillsDatatable')->name('treasury_bills_datatable');
			Route::get('corporate_bonds_datatable/{investment_id}', 'InvestmentSummaryController@showCBondsDatatable')->name('corporate_bonds_datatable');
			Route::get('calculate', 'InvestmentSummaryController@calculate')->name('calculate');
			Route::get('coupon_tenure/{days}', 'InvestmentSummaryController@couponTenure')->name('coupon_tenure');
		}); 


		// budget
		Route::group(['prefix' => 'budget',  'as' => 'budget.'], function() {
			Route::get('/', 'InvestmentBudgetController@index')->name('index');
			Route::get('show/{investment_budget_id}', 'InvestmentBudgetController@show')->name('show'); 
			Route::post('set', 'InvestmentBudgetController@set')->name('set'); 
			Route::get('datatable', 'InvestmentBudgetController@datatable')->name('datatable'); 
			Route::get('edit/{investment_budget_id}', 'InvestmentBudgetController@edit')->name('edit_budget'); 
			Route::put('update/{investment_budget_id}', 'InvestmentBudgetController@update')->name('update'); 
			Route::get('budget_datatable/{investment_budget_id}', 'InvestmentBudgetController@budgetDatatable')->name('budget_datatable');
			Route::get('calculator','InvestmentBudgetController@bondCalculator')->name('calculator'); 
			Route::get('return_allocated_budget/{fin_year_id}/{code_value_id}', 'InvestmentBudgetController@returnAllocatedBudget')->name('return_allocated_budget'); 

			//workflow
			Route::get('recommend_budget/{investment_budget_id}', 'InvestmentBudgetController@recommendBudget')->name('recommend_budget'); 


		});


		Route::group(['prefix' => 'loan',  'as' => 'loan.'], function() {
			Route::get('/', 'InvestmentController@loanShow')->name('index');
			Route::get('create/{investment_allocation_id?}', 'InvestmentController@loanCreate')->name('create');
			Route::post('save', 'InvestmentController@loanSave')->name('save');
			Route::get('datatable/{budget_alocation_id}', 'InvestmentController@loansDatatable')->name('datatable');
			Route::get('show/{loan_id}', 'InvestmentController@loanShow')->name('show');

			Route::get('repayment_datatable/{loan_id}', 'InvestmentController@loanRepaymentDatatable')->name('repayment_datatable');
			Route::post('save_repayment', 'InvestmentController@saveLoanRepayment')->name('save_repayment');

		});


		Route::group(['prefix' => 'equity',  'as' => 'equity.'], function() {
			Route::get('index/{budget_allocation_id?}', 'InvestmentEquityController@index')->name('index');
			Route::post('save_issuer', 'InvestmentEquityController@saveIssuer')->name('save_issuer');
			Route::get('create/{investment_allocation_id?}', 'InvestmentEquityController@create')->name('create');
			Route::get('datatable/{budget_allocation_id?}', 'InvestmentEquityController@datatable')->name('datatable');
			Route::get('show/{equity_id}', 'InvestmentEquityController@show')->name('show');
			Route::get('updates_datatable/{equity_id}', 'InvestmentEquityController@updatesDatatable')->name('updates_datatable');
			Route::get('lots_datatable/{equity_id}', 'InvestmentEquityController@lotsDatatable')->name('lots_datatable');
			Route::post('save', 'InvestmentEquityController@save')->name('save');
			Route::get('return_lot_number/{issuer_id}', 'InvestmentEquityController@returnLotNumber')->name('return_lot_number');
			Route::post('update_price', 'InvestmentEquityController@updatePrice')->name('update_price');
			Route::get('sales_datatable/{issuer_id}', 'InvestmentEquityController@salesDatatable')->name('sales_datatable');
			Route::post('sell', 'InvestmentEquityController@sell')->name('sell');
			Route::get('dividend_datatable/{equity_id}', 'InvestmentEquityController@dividendDatatable')->name('dividend_datatable');
			Route::post('add_dividend', 'InvestmentEquityController@addDividend')->name('add_dividend');
		});


		Route::group(['prefix' => 'collective_scheme',  'as' => 'collective_scheme.'], function() {
			Route::get('index/{budget_allocation_id?}', 'CollectiveSchemeController@index')->name('index');
			Route::post('add_scheme', 'CollectiveSchemeController@addScheme')->name('add_scheme');
			Route::get('datatable/{budget_allocation_id?}', 'CollectiveSchemeController@datatable')->name('datatable');
			Route::get('show/{scheme_id}', 'CollectiveSchemeController@show')->name('show');
			Route::get('updates_datatable/{scheme_id}', 'CollectiveSchemeController@updatesDatatable')->name('updates_datatable');
			Route::post('update_price', 'CollectiveSchemeController@updateValue')->name('update_price');
			Route::get('create/{investment_allocation_id?}', 'CollectiveSchemeController@create')->name('create');
			Route::post('save', 'CollectiveSchemeController@save')->name('save');
			Route::get('return_schemes/{manager_id}', 'CollectiveSchemeController@returnSchemes')->name('return_schemes');
			Route::get('return_lot_number/{scheme_id}', 'CollectiveSchemeController@returnLotNumber')->name('return_lot_number');
			Route::get('lots_datatable/{scheme_id}', 'CollectiveSchemeController@lotsDatatable')->name('lots_datatable');
			Route::get('sales_datatable/{scheme_id}', 'CollectiveSchemeController@salesDatatable')->name('sales_datatable');
			Route::get('dividend_datatable/{scheme_id}', 'CollectiveSchemeController@dividendDatatable')->name('dividend_datatable');
			Route::post('sell', 'CollectiveSchemeController@sell')->name('sell');
			Route::post('add_dividend', 'CollectiveSchemeController@addDividend')->name('add_dividend');

		});



		Route::group(['prefix' => 'call_account',  'as' => 'call_account.'], function() {
			Route::get('index', 'InvestmentController@callShow')->name('index');
			Route::get('create', 'InvestmentController@callCreate')->name('create');
			Route::post('save', 'InvestmentController@callSave')->name('save');
			Route::get('datatable', 'InvestmentController@callAccountDatatable')->name('datatable');
			Route::get('show/{call_id}', 'InvestmentController@callShow')->name('show');

		});

			// Route::get('show/{equity_id}', 'CollectiveScheme@show')->name('show');
			// Route::get('datatable/{budget_allocation_id?}', 'CollectiveScheme@datatable')->name('datatable');
			// Route::get('updates_datatable/{equity_id}', 'CollectiveScheme@updatesDatatable')->name('updates_datatable');
			// Route::get('sales_datatable/{equity_id}', 'CollectiveScheme@salesDatatable')->name('sales_datatable');
			// Route::get('dividend_datatable/{equity_id}', 'CollectiveScheme@dividendDatatable')->name('dividend_datatable');
			// Route::post('update_price', 'CollectiveScheme@updatePrice')->name('update_price');
			// Route::post('sell', 'CollectiveScheme@sell')->name('sell');
			// Route::post('save_dividend', 'CollectiveScheme@saveDividend')->name('save_dividend');


		

	});
});