<?php
/**
/**
 * System
 */
Route::group([
    'namespace' => 'System',
], function() {

    Route::group(['prefix' => 'system', 'as' => 'system.'], function () {

        Route::get('/', function () {
            return view('backend.system.menu');
        })->name('menu');

        /**
         *
         * Codes ROUTES ===================================
         */
        Route::resource('code', 'CodeController', ['except' => ['show']]);

        /**
         * code values===========
         */
        Route::get('code/values/{code}', 'CodeController@getCodeValues')->name('code.code_values');
        Route::get('code/value/create/{code}', 'CodeController@codeValueCreate')->name('code.create_value');
        Route::post('code/values/store', 'CodeController@codeValueStore')->name('code.store_value');

        Route::get('code/value/{code}/edit', 'CodeController@codeValueEdit')->name('code.edit_value');
        Route::put('code/value/update/{code}', 'CodeController@codeValueUpdate')->name('code.update_value');

        Route::delete('code/value/{code}/delete', 'CodeController@codeValueDelete')->name('code.delete_value');

        //End code values-------

//--End codes routes ---------------------------

    });

    Route::group(['prefix' => 'workflow', 'as' => 'workflow.'], function () {
        Route::get('defaults', 'WorkflowController@defaults')->name('defaults');
        Route::get('pending', 'WorkflowController@pending')->name('pending');
        Route::get('pending/get', 'WorkflowController@getPending')->name('pending.get');
        Route::get('my_pending', 'WorkflowController@myPending')->name('my_pending');
        Route::get('attended', 'WorkflowController@attended')->name('attended');
        Route::get('archived', 'WorkflowController@archived')->name('archived');
        Route::get('progress', 'WorkflowController@progress')->name('progress');
        Route::patch('update/{definition}', 'WorkflowController@updateDefinitionUsers')->name('update');
        Route::get('get_users/{definition}', 'WorkflowController@getUsers')->name('get_users');
        Route::get('monthly_user_review', 'WorkflowController@getUsersReview')->name('users_review');

        /**
         *  wf_tracks
         */
        Route::post('get_wf_tracks_html/{resource_id}/{wf_module_group_id}/{type}', 'WorkflowController@getWfTracksForHtml')->name('wf_tracks.get_html');
        Route::post('get_wf_tracks/{resource_id}/{wf_module_group_id}/{type}', 'WorkflowController@getWfTracksForDatatable')->name('wf_tracks.get');
        Route::get('get_completed_wf_tracks/{resource_id}/{wf_module_group_id}/{type}', 'WorkflowController@getCompletedWfTracks')->name('wf_completed_tracks.get');
        Route::get('get_list_wf_tracks/{resource_id}/{wf_module_group_id}/{type}', 'WorkflowController@getListWfTracks')->name('wf_list_tracks.get');
        Route::post('archive_workflow/{resource_id}/{wf_module_group_id}/{type}/{action}', 'WorkflowController@archiveWorkflow')->name('archive_workflow')->where('action', '[0-1]+');
        Route::get('edit_archive_workflow/{wf_archive}', 'WorkflowController@editArchiveWorkflow')->name('edit_archive_workflow');
        Route::post('can_archive_workflow/{resource_id}/{wf_module_group_id}/{type}', 'WorkflowController@canArchiveWorkflow')->name('can_archive_workflow');
        Route::post('post_archive_workflow/{wf_track}/{action}', 'WorkflowController@postArchiveWorkflow')->name('post_archive_workflow');
        Route::post('update_archive_workflow/{wf_archive}', 'WorkflowController@updateArchiveWorkflow')->name('update_archive_workflow');
        Route::get('get_deactivated_wf_tracks/{resource_id}/{wf_module_group_id}', 'WorkflowController@getDeactivatedWfTracksForDataTable')->name('wf_tracks.get_deactivated');
        Route::get('get_deactivated_claim_wf_tracks/{resource_id}', 'WorkflowController@getDeactivatedClaimWfTracksForDataTable')->name('wf_tracks.get_deactivated_claim');
        Route::post('workflow_modal_content/', 'WorkflowController@getWorkflowModalContent')->name('workflow_modal_content');
        Route::post('update_workflow/{wf_track}', 'WorkflowController@updateWorkflow')->name('update_workflow');
        Route::post('next_level_designation/{wf_track}/{action}/', 'WorkflowController@nextLevelDesignation')->name('next_level_designation');

        Route::post('initiate', 'WorkflowController@initiate')->name('initiate');

        /**
         * Allocations
         */
        Route::get('allocation', 'WorkflowController@allocation')->name('allocation');
        //assign allocation
        Route::put('assign_allocation', 'WorkflowController@assignAllocation')->name('assign_allocation');

            // reversed workflows   
        Route::get('reversed', 'WorkflowController@reversed')->name('reversed');
        Route::get('reversed_pending/get', 'WorkflowController@getReversedPending')->name('reversed_pending.get');


        // subordinatesPending
        Route::get('subordinates', 'WorkflowController@subordinatesSummaryView')->name('subordinates');
        Route::get('subordinates/get', 'WorkflowController@subordinatesSummaryDatatable')->name('subordinates.get');
        Route::get('subordinate_pending/{user_id}', 'WorkflowController@subordinatePending')->name('subordinate_pending');
        Route::get('subordinate_pending/{user_id}/get', 'WorkflowController@getSubordinatePending')->name('subordinate_pending.get');

    });

    Route::group(['prefix' => 'notification', 'as' => 'notification.'], function () {
        Route::get('/', 'NotificationController@index')->name('index');
        Route::get('get', 'NotificationController@get')->name('get');
        Route::post('read/{id}', 'NotificationController@read')->name('read');
        Route::get('fetch_next/{offset}', 'NotificationController@fetchNext')->name('fetch.next');
    });

    Route::group(['prefix' => 'checker', 'as' => 'checker.'], function () {
        Route::get('/', 'CheckerController@index')->name('index');
        Route::put('get_datatable/{code_value_id}', 'CheckerController@getForDatatable')->name('get_datatable');
    });

    Route::group(['prefix' => 'letter', 'as' => 'letter.'], function () {
        Route::get('preview/{letter}/', 'LetterController@preview')->name('preview')->where(['letter' => '[0-9]+']);
        Route::get('preview_legacy/{reference}/{resource}', 'LetterController@previewLegacy')->name('preview_legacy');
        Route::get('open/{letter}', 'LetterController@open')->name('open')->where(['letter' => '[0-9]+']);
        Route::get('live_print/{letter}', 'LetterController@livePrint')->name('live_print')->where(['letter' => '[0-9]+']);
        Route::get('make_pdf/{letter}', 'LetterController@makePdf')->name('make_pdf')->where(['letter' => '[0-9]+']);
        Route::get('confirm/{resource}/{reference}', 'LetterController@confirm')->name('confirm')->where(['resource' => '[0-9]+']);
        Route::get('create/{resource}/{reference}/{option?}', 'LetterController@create')->name('create')->where(['resource' => '[0-9]+']);
        Route::get('show/{letter}', 'LetterController@show')->name('show')->where(['letter' => '[0-9]+']);
        Route::get('edit/{letter}', 'LetterController@edit')->name('edit')->where(['letter' => '[0-9]+']);
        Route::post('update/{letter}', 'LetterController@update')->name('update');
        Route::post('updatedispatch/{letter}', 'LetterController@updateDispatch')->name('updatedispatch');
        Route::get('process/{resource}/{reference}/{option?}', 'LetterController@process')->name('process')->where(['resource' => '[0-9]+']);
        Route::post('store/{resource}/{reference}', 'LetterController@store')->name('store');
        Route::post('printed/{letter}', 'LetterController@printed')->name('printed')->where(['letter' => '[0-9]+']);
    });

    Route::group(['prefix' => 'document', 'as' => 'document.'], function () {
        Route::post('check_if_others', 'DocumentController@checkIfOthers')->name('check_if_others');
        Route::get('{resource}/{reference}/{url_selector?}/attach', 'DocumentController@attach')->name('attach');
        Route::get('{doc_resource_id}/{reference}/edit', 'DocumentController@edit')->name('edit');
        Route::post('{doc_resource_id}/preview', 'DocumentController@preview')->name('preview');
        Route::get('{doc_resource_id}/{reference}/delete', 'DocumentController@delete')->name('delete');
        Route::post('store', 'DocumentController@store')->name('store');
        Route::put('update', 'DocumentController@update')->name('update');
        /*Doc general*/
        Route::get('{resource}/{reference}/attach/general', 'DocumentController@attachDocumentGen')->name('attach_doc_gen');
        Route::post('store_doc_gen', 'DocumentController@storeDocumentGen')->name('store_doc_gen');
        Route::get('{doc_pivot}/{resource}/{reference}/edit_doc/general', 'DocumentController@editDocumentGen')->name('edit_doc_gen');
        Route::put('update_doc_gen', 'DocumentController@updateDocumentGen')->name('update_doc_gen');
        Route::post('{doc_pivot}/{resource}/{reference}/preview/general', 'DocumentController@previewDocGen')->name('preview_doc_gen');
        Route::get('{doc_pivot}/{resource}/{reference}/delete/general', 'DocumentController@deleteDocumentGen')->name('delete_doc_gen');
    });
    
});
