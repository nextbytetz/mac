<?php

/**
/**
 * Finance
 */
Route::group([
    'namespace' => 'Access',
], function() {

    /*
     * For DataTables
    */
    Route::get('users/get', 'UserTableController')->name('users.get');
    Route::get('role/get', 'RoleTableController')->name('role.get');

    /*
     * For Select Input Search
     */
    Route::get('users/search', 'UserController@search')->name('users.search');

    /*
     * User Resource
    */
    Route::resource('user', 'UserController', ['except' => ['show']]);

    /*
     * Role Resource
    */
    Route::resource('role', 'RoleController', ['except' => ['show']]);

    /**
     * My substitute Rotes
     */
    Route::get('users/substitute/{user}', 'UserController@getSubstitute')->name('users.substitute');
    Route::post('users/substitute', 'UserController@postSubstitute')->name('users.substitute.post');
    Route::delete('users/revoke_substitute/{user}', 'UserController@revokeSubstitute')->name('users.revoke_substitute');
});