<?php
/**
/**
 * Finance
 */
Route::group([
    'prefix' => 'finance',
    'as' => 'finance.',
    'namespace' => 'Finance',
], function() {


    Route::get('/', function () {
        return view('backend.finance.menu');
    })->name('menu');


    Route::resource('currency', 'CurrencyController', ['except' => ['show']]);
    Route::get('get', 'CurrencyController@get')->name('finance.currency.get');
    //  Route::get('get', 'DatatablesController@get')->name('finance.currency.get');


//    RECEIPT ROUTING -------------------------
    Route::resource('receipt', 'ReceiptController', ['except' => ['show']]);
    //    load receipt for action
    Route::get('get', 'ReceiptController@get')->name('receipt.get');

    // start : Third party payers
    Route::resource('thirdparty', 'ThirdPartyController');
    Route::group(['prefix' => 'thirdparty',  'as' => 'thirdparty.'], function() {
        Route::post('get', 'ThirdPartyController@get')->name('get');
        Route::post('get/receipts/{id}', 'ThirdPartyController@getReceipt')->name('get.receipts');
        Route::get('status_change/{thirdparty}/{status}', 'ThirdPartyController@statusChange')->name('status_change')->where(['status' => '[0,1]']);;
    });
    //end : Third party payers

    /*  Receipt PROFILE ============== */
    Route::get('receipt/{receipt}/profile', 'ReceiptController@profile')->name('receipt.profile');
    /**
     * Specific receipt routes.
     */
    Route::get('receipt/employer', 'ReceiptController@chooseEmployer')->name('receipt.employer');
    Route::post('receipt/employer', 'ReceiptController@postChooseEmployer')->name('receipt.employer.post');
    Route::post('receipt/contribution_entries', 'ReceiptController@contributionEntries')->name('receipt.contribution_entries');
    Route::post('receipt/booked_amount', 'ReceiptController@bookedAmount')->name('receipt.booked_amount');
    Route::post('receipt/interest_due_amount', 'ReceiptController@interestDueAmount')->name('receipt.interest_due_amount');
    //Route::post('receipt/employer', 'ReceiptController@postChooseEmployer')->name('receipt.employer.post');
    Route::get('receipt/administrative', 'ReceiptController@createAdministrative')->name('receipt.administrative');
    Route::post('receipt/administrative', 'ReceiptController@storeAdministrative')->name('receipt.administrative.post');
    Route::get('receipt/{receipt}/print', 'ReceiptController@printReceipt')->name('receipt.print');
    Route::get('receipt_code/{receipt_code}/edit', 'ReceiptCodeController@edit')->name("receipt_code.edit");
    Route::post('receipt_code/{receipt_code}/edit', 'ReceiptCodeController@update')->name("receipt_code.edit.post");
    Route::get('receipt/{receipt}/edit_months', 'ReceiptController@editMonths')->name("receipt.edit_months");
    Route::get('receipt/{receipt}/edit_months_permission', 'ReceiptController@editMonthsPermission')->name("receipt.edit_months_permission");
    Route::post('receipt/{receipt}/edit_months', 'ReceiptController@updateMonths')->name("receipt.edit_months.post");
    Route::get('receipt/{receipt}/reconcile', 'ReceiptController@reconcile')->name("receipt.reconcile");

//    GET contributions by this receipt
    Route::get('get_contributions/{receipt}', 'ReceiptController@getContributionsByReceiptForDataTable')->name('receipt.contributions');
    //    GET interests by this receipt
    Route::get('get_interests/{receipt}', 'ReceiptController@getInterestByReceiptForDatatable')->name('receipt.interests');

    //====================


//    Cancel receipt routing
    Route::get('cancel_reason/{receipt}', 'ReceiptController@cancelReason')->name('receipt.cancel_reason');
    Route::put('cancel/{receipt}', 'ReceiptController@cancel')->name('receipt.cancel');

//     Dishonour cheque
    Route::get('dishonour_reason/{receipt}', 'ReceiptController@dishonourReason')->name('receipt.dishonour_reason');
    Route::put('dishonour/{receipt}', 'ReceiptController@dishonour')->name('receipt.dishonour');
//    replace cheque
    Route::get('replace_cheque/{receipt}', 'ReceiptController@newCheque')->name('receipt.new_cheque');
    Route::put('replace_cheque/{receipt}', 'ReceiptController@replaceCheque')->name('receipt.replace_cheque');

//    Recall Receipt
    Route::get('recall_search', 'ReceiptController@recallSearch')->name('receipt.recall_search');
    Route::get('recall', 'ReceiptController@recall')->name('receipt.recall');



//    search receipt by receipt no by request
    Route::get('receipt/profile/{receipt}', 'ReceiptController@profileByRequest')->name('receipt.profile_by_request');

    //    RECEIPT CODE ROUTING -------------------------
    Route::resource('receipt_code', 'ReceiptCodeController', ['except' => ['edit', 'create', 'index',  'store']]);
    Route::post('receipt_code/check_existing', 'ReceiptCodeController@checkExisting')->name('receipt_code.check_existing');
//    get contribution tracks
    Route::get('get_contribution_tracks/{receipt}', 'ReceiptCodeController@getContributionsTracksForDataTable')->name('receipt_code.contribution_tracks');
    Route::get('get_member_contributions/{receipt}', 'ReceiptCodeController@getMemberContributionsForDataTable')->name('receipt_code.member_contributions');

//    Store linked files
    Route::get('receipt_code/{receipt_code}/upload', 'ReceiptCodeController@linkedFile')->name('receipt_code.linked_file');
    Route::post('receipt_code/{receipt_code}/upload/store', 'ReceiptCodeController@linkedFileStore')->name('receipt_code.store_linked_file');

    Route::post('upload_progress/{receipt_code}', 'ReceiptCodeController@uploadProgress')->name("upload_progress");
    Route::get('receipt_code/error/{receipt_code}', 'ReceiptCodeController@downloadError')->name("receipt_code.download_error");

    //Register Contribution
    Route::get('contribution/register/{receipt_code}', 'ReceiptCodeController@registerContribution')->name('contribution.register');


    /**
     * FINANCE CONTROLLER ROUTES ==============================
     */

    //ERP routes starts here
    Route::get('payment/pv', 'FinanceController@postApErpApi');
    Route::get('payment/posted_gl', 'FinanceController@getErpPostedGl');
    Route::get('search_supplier', 'ReceiptController@viewSupplier')->name('search.view');
    Route::get('search_supplier_employee', 'ReceiptController@viewSupplierEmployee')->name('search.view.employee');
    Route::get('erp_menu', 'ReceiptController@returnErpMenu')->name('erp.menu.view');
    Route::get('create_supplier_form/{org_id}', 'ReceiptController@returnSupplierForm')->name('supplier_form');
    Route::get('create_supplier_employee_form/{employee_id}', 'ReceiptController@returnEmployeeSupplier')->name('employee_supplier_form');
    Route::post('store_supplier', 'ReceiptController@storeSupplier')->name('supplier_create');
    Route::post('store_supplier_employee', 'ReceiptController@storeSupplierEmployee')->name('supplier_create_employee');
    Route::get('post_supplier_erp', 'ReceiptController@postSupplierERP')->name('post_supplier');
    /*Update invoices from erp*/
    Route::get('bulk_update_erp_paid_invoice', 'FinanceController@bulkUpdatePaidErpInvoices')->name('bulk_update_erp_paid_invoice');

    Route::get('view_successfullygl_erp', 'ReceiptController@viewSuccessfullyGltoERP')->name('successfull.gl.erp');

   //ERP routes Ends here

      //ERMS routes starts here
    Route::get('erms_menu', 'ErmsIntegrationController@returnErmsMenu')->name('erms.menu.view');
    Route::get('erms/claims_payable', 'ErmsIntegrationController@claimsPayableView')->name('erms.claims_payable');
    Route::get('erms/get_claims_payable', 'ErmsIntegrationController@claimsPayableDatatable')->name('erms.get_claims_payable');
    Route::post('erms/claims_payable/post/{id}', 'ErmsIntegrationController@postClaimsPayable')->name('erms.claims_payable.post');
    Route::get('erms/claims_payable/post_all', 'ErmsIntegrationController@postAllClaimsPayable')->name('erms.claims_payable.post_all');

   //ERMS routes Ends here



    Route::get('payment/profile', 'FinanceController@payment_profile')->name('payment.profile');
    Route::get('payment/get_pending_process_compensation', 'FinanceController@getPendingProcessCompensations')->name('payment.get_pending_process_compensations');
    //pending funeral grants
    Route::get('payment/get_pending_funeral_grants', 'FinanceController@getPendingFuneralGrants')->name('payment.get_pending_funeral_grants');
    //pending survivor benefits
    Route::get('payment/get_pending_survivor_gratuity', 'FinanceController@getPendingSurvivorGratuity')->name('payment.get_pending_survivor_gratuity');

    /*Pending Interest Refunds*/
    Route::get('payment/get_pending_interest_refunds', 'FinanceController@getPendingInterestRefunds')->name('payment.get_pending_interest_refunds');


    //Pending pv trans individual
    Route::get('payment/get_pending_payment_voucher_trans_individual', 'FinanceController@getPendingPaymentVoucherTransIndividual')->name('payment.get_pending_payment_vouchers_trans_individual');
    //  get Pending payment vouchers not paid
    Route::get('payment/get_pending_payment_vouchers', 'FinanceController@getPendingPaymentVouchers')->name('payment.get_pending_payment_vouchers');

    //Pending pv trans batch
    Route::get('payment/get_pending_payment_vouchers_batch', 'FinanceController@getPendingPaymentVoucherTransBatch')->name('payment.get_pending_payment_vouchers_trans_batch');
    /*process payment*/
    Route::get('payment/process', 'FinanceController@processPayment')->name('payment.process');
    /*process payment voucher trans*/
    Route::get('payment/process_voucher_trans/{process_type}', 'FinanceController@processVoucherTransactions')->name('payment.process_voucher_transactions');

    /* Pay/Dispatch Payment Voucher*/
    Route::get('payment/pay_voucher/{payment_voucher}', 'FinanceController@PayVoucher')->name('payment.pay_voucher');

    /*Print Payment Voucher*/
    Route::get('payment/voucher/{payment_voucher}/print', 'FinanceController@printPaymentVoucher')->name('payment.print_voucher');

    /*Get Pending payment voucher transactions to be exported to ERP*/
    Route::get('payment/get_pv_trans_for_export', 'FinanceController@getPendingExportPvTranForDataTable')->name('payment.get_pv_trans_for_export');
    /*Export to ERP*/
    Route::get('payment/export_pv_tran_to_erp', 'FinanceController@exportPvTranToErp')->name('payment.export_pv_tran_to_erp');
    /***
     *
     * LEGACY RECEIPTS MODULE  ROUTES =========================================
     *
     */

    // LEGACY RECEIPT = = = = =  ==
    Route::resource('legacy_receipt', 'LegacyReceiptController', ['except' => ['show']]);
    /* Choose Employer*/
    Route::get('legacy_receipt/employer', 'LegacyReceiptController@chooseEmployer')->name('legacy_receipt.employer');
    /* Post choose employer -- Create Receipt */
    Route::post('legacy_receipt/employer', 'LegacyReceiptController@postChooseEmployer')->name('legacy_receipt.employer.post');
    /* Receipt Profile */
    Route::get('legacy_receipt/profile/{legacy_receipt}', 'LegacyReceiptController@profile')->name('legacy_receipt.profile');
    /* Edit receipt()months -> codes distributions*/
    Route::get('legacy_receipt/{legacy_receipt}/edit_months', 'LegacyReceiptController@editMonths')->name("legacy_receipt.edit_months");
    Route::post('legacy_receipt/{legacy_receipt}/update_months', 'LegacyReceiptController@updateMonths')->name("legacy_receipt.update_months");


    /*  GET contributions by this receipt */
    Route::get('get_legacy_contributions/{legacy_receipt}', 'LegacyReceiptController@getContributionsByReceiptForDataTable')->name('legacy_receipt.contributions');
    /*  GET interests by this receipt */
    Route::get('get_legacy_interests/{legacy_receipt}', 'LegacyReceiptController@getInterestByReceiptForDatatable')->name('legacy_receipt.interests');

    /* Cancel receipt routing */
    Route::get('cancel_reason_legacy/{legacy_receipt}', 'LegacyReceiptController@cancelReason')->name('legacy_receipt.cancel_reason');
    Route::put('cancel_legacy/{legacy_receipt}', 'LegacyReceiptController@cancel')->name('legacy_receipt.cancel');

    /* Dishonour cheque */
    Route::get('dishonour_reason_legacy/{legacy_receipt}', 'LegacyReceiptController@dishonourReason')->name('legacy_receipt.dishonour_reason');
    Route::put('dishonour_legacy/{legacy_receipt}', 'LegacyReceiptController@dishonour')->name('legacy_receipt.dishonour');
    /* replace cheque */
    Route::get('replace_cheque_legacy/{legacy_receipt}', 'LegacyReceiptController@newCheque')->name('legacy_receipt.new_cheque');
    Route::put('replace_cheque_legacy/{legacy_receipt}', 'LegacyReceiptController@replaceCheque')->name('legacy_receipt.replace_cheque');
    /* Print receipt */
    Route::get('legacy_receipt/{legacy_receipt}/print', 'LegacyReceiptController@printReceipt')->name('legacy_receipt.print');



    /* LEGACY RECEIPT => CONTRIBUTION AND RECEIPT CODES */
    Route::get('legacy_contributions_received', 'LegacyReceiptController@receivedContributions')->name('legacy_receipt.received_contributions');

    /* Receipt code */
    Route::resource('legacy_receipt_code', 'LegacyReceiptCodeController', ['except' => ['show']]);



    /* Contribution entries when receipting */
    Route::post('legacy_receipt/contribution_entries', 'LegacyReceiptController@contributionEntries')->name('legacy_receipt.contribution_entries');

    /* Upload manual contribution for financial year 2015/16 and 2016/17 view */
    Route::get('legacy_receipt/upload_manual_contribution', 'LegacyReceiptController@uploadManualContribution')->name('legacy_receipt.upload_manual_contribution');
    /* Upload manual contribution post */
    Route::post('legacy_receipt/upload_manual_contribution/store', 'LegacyReceiptController@uploadManualContributionList')->name('legacy_receipt.upload_manual_contribution.post');

    /*  LEGACY RECEIPT CODE ROUTING ====================== */
    Route::resource('legacy_receipt_code', 'LegacyReceiptCodeController', ['except' => ['edit', 'create', 'index',  'store']]);

    /*   Store linked files */
    Route::get('legacy_receipt_code/{legacy_receipt_code}/upload', 'LegacyReceiptCodeController@linkedFile')->name('legacy_receipt_code.linked_file');
    Route::post('legacy_receipt_code/{legacy_receipt_code}/upload/store', 'LegacyReceiptCodeController@linkedFileStore')->name('legacy_receipt_code.store_linked_file');
    /* Upload progress */
    Route::post('upload_progress/{legacy_receipt_code}', 'LegacyReceiptCodeController@uploadProgress')->name("legacy_receipt_code.upload_progress");
    /* Download error*/
    Route::get('legacy_receipt_code/error/{legacy_receipt_code}', 'LegacyReceiptCodeController@downloadError')->name("legacy_receipt_code.download_error");
    /* Edit legacy receipt code => contribution */
    Route::get('legacy_receipt_code/{legacy_receipt_code}/edit', 'LegacyReceiptCodeController@edit')->name("legacy_receipt_code.edit");
    Route::post('legacy_receipt_code/update/{legacy_receipt_code}', 'LegacyReceiptCodeController@update')->name("legacy_receipt_code.update");

    /* Register Contribution */
    Route::get('contribution_legacy/register/{legacy_receipt_code}', 'LegacyReceiptCodeController@registerContribution')->name('contribution_legacy.register');
    /* Uploaded Member contributions */
    Route::get('legacy_receipt/get_member_contributions/{legacy_receipt}', 'LegacyReceiptCodeController@getMemberContributionsForDataTable')->name('legacy_receipt_code.member_contributions');
    /* end of legacy receipt------------ */


    /* GePG Routes */

    Route::get('search_employer', 'ReceiptController@viewEmployer')->name('search');
    Route::get('non_contributions_payments', 'ReceiptController@nonContributions')->name('non.contributions');
    Route::get('get_employers', 'ReceiptController@getEmployers')->name('get.employers');
    Route::post('generate_bill', 'ReceiptController@generateBill')->name('generatebill');
    // Route::post('generate_bill', function(){
    //    return 'hi';
    // }); 
    Route::post('control_no/administrative', 'ReceiptController@generateAdministrativeControlNo')->name('generate.administrative.controlno');
    // Route::get('get_gepg_gfs_code', 'ReceiptController@getGepgGfsCode')->name('gepg.gfs.code');
    Route::get('bill/{bill_id}/administrative_order_forms', 'ReceiptController@administrativeOrderForms')->name('view.administrative.order_form');

    Route::get('resend_bill', 'ReceiptController@resendBill')->name('resend_bill');

    Route::get('generate_bill_form/{org_id}', 'ReceiptController@returnControlNumberForm')->name('generate_bill_form');

    Route::get('normal_bill/{bill_id}', 'ReceiptController@downloadNormalBill');
    Route::get('crdb_transfer/{bill_id}', 'ReceiptController@downloadCrdbTransfer');
    Route::get('nmb_transfer/{bill_id}', 'ReceiptController@downloadNmbTransfer');

    Route::get('reconcile', 'ReceiptController@returnReconciliation')->name('reconcilliation');
    Route::get('controlno', 'ReceiptController@controlNumberReceipts')->name('controlno');
    Route::get('pending', 'ReceiptController@controlNumberPending')->name('pending');
    Route::get('view_successfull_trx', 'ReceiptController@viewSuccessfullTrx')->name('view.successfull.trx');

    Route::get('view_failed_trx', 'ReceiptController@viewFailedTrx')->name('view.failed.trx');

    Route::get('view_daily_GePG_trx', 'ReceiptController@viewSuccessfullGePGTrx')->name('view.successfull.GePG.trx');


    Route::get('view_contribution_refund', 'ReceiptController@viewContribRefund')->name('view.contrib.refund');
    Route::get('post_contribution_refund/{refund_id?}', 'ReceiptController@refundToERP')->name('post.contrib.refund');


        //Ministry of Finance Public Servants routes
    Route::group([ 'prefix' => 'treasury', 'as' => 'treasury.'], function() {
       Route::get('menu', 'GotIntergrationController@returnTreasuryMenu')->name('menu.view');
       Route::get('payment/summary', 'GotIntergrationController@returnSummaryView')->name('payment.summary');
       Route::get('failed_summary/{contrib_month}', 'GotIntergrationController@returnFailedSummaryDetails')->name('failed_summary');

       //drill down
       Route::get('summary/{contrib_month}/{funding_source}', 'GotIntergrationController@summaryDrillDown')->name('summary.drill_down');
       Route::get('summary_datatable/{contrib_month}/{funding_source}', 'GotIntergrationController@summaryDrillDownDatatable')->name('summary.drill_down_datatable');

       Route::get('votes/{vote_code}/{contrib_month}/{funding_source}', 'GotIntergrationController@votesDrillDown')->name('votes.drill_down');
       Route::get('votes_datatable/{vote_code}/{contrib_month}/{funding_source}', 'GotIntergrationController@votesDrillDownDatatable')->name('votes.drill_down_datatable');

       Route::get('funding_summary', 'GotIntergrationController@saveContributionSummary')->name('funding.summary');


       Route::post('generate_bill', 'GotIntergrationController@generateTreasuryBill')->name('generate_bill');
       Route::get('bill/{bill_id}/{type}', 'GotIntergrationController@returnBillDatatable')->name('payment.bills');

       Route::get('update_bl', 'GotIntergrationController@updateEmployer_id');

       Route::get('return_employees_header', 'GotIntergrationController@employeesHeaderGot')->name('return.employees.header');
       Route::get('return_contribution_header', 'GotIntergrationController@contributionHeader')->name('return.contribution.header');
   });
    // Route::get('post_bulk_contribution_refund', 'ReceiptController@refundBulkToERP')->name('post.bulk.contrib.refund');

    //NOT USED ANYMORE
    // Route::get('get_successfull_trx', 'ReceiptController@getSuccessfullTrx')->name('get.successfull.trx');
    // Route::get('get_failed_trx', 'ReceiptController@getFailedTrx')->name('get.failed.trx');

    // Route::get('controlno', 'ReceiptController@requestControlNumber')->name('controlno');


    /*End of GePG routes */


    /*  End of Legacy Receipts routes ---------------- */




    //End Finance Controller -----------------------------
});

