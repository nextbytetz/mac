<?php
/**
/**
 * Finance
 */
Route::group([
//    'prefix' => 'finance',
    'as' => 'finance.',
    'namespace' => 'Report',
], function() {

    Route::group([ 'prefix' => 'report', 'as' => 'report.'], function() {


        /**
         * Receipt Performance report
         */
         Route::get('receipting_staff_performance', 'FinanceReportController@receiptPerformance')->name('receipt.performance');


        Route::get('general_receipt_summary', 'FinanceReportController@generalReceiptSummary')->name('receipt.general_receipt_summary');
        Route::post('get/receipts', 'FinanceReportController@getReceipts')->name('receipts');

        Route::get('dishonoured_cheques', 'FinanceReportController@dishonouredCheques')->name('dishonoured_cheques');
        Route::get('cancelled_receipts', 'FinanceReportController@cancelledReceipt')->name('cancelled_receipts');
        Route::get('receipt_finance_codes', 'FinanceReportController@receiptByFinanceCode')->name('receipts_by_finance_codes');
        Route::get('receipts', 'FinanceReportController@getReceipts')->name('receipts');
        Route::get('receipt_general_report', 'FinanceReportController@receiptGeneralReport')->name('receipt_general_report');
        //Payment Vouchers
        Route::get('payment_vouchers', 'FinanceReportController@paymentVouchers')->name('payment_vouchers');
        Route::get('category/{category_id}', 'GeneralReportController@index')->name('index');

        Route::get('successfullControlNo_trx', 'FinanceReportController@successfullyCNReport')->name('successfully_control_numbers');
        Route::get('daily_reconciliation', 'FinanceReportController@dailyReconciliation')->name('daily.reconciliation');
//        Route::get('/dishonoured_cheques', 'FinanceReportController@dishonouredCheques')->name('dishonoured_cheques');
//        Route::get('/cancelled_receipts', 'FinanceReportController@cancelledReceipt')->name('cancelled_receipts');
//        Route::get('/receipt_finance_codes', 'FinanceReportController@receiptByFinanceCode')->name('receipts_by_finance_codes');
//        Route::get('/receipts', 'FinanceReportController@getReceipts')->name('receipts');
//        Route::get('/{category_id}', 'GeneralReportController@index')->name('index');
    });
});