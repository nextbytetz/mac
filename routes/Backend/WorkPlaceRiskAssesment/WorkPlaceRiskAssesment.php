<?php
/**
/**
 * WRA
 */
Route::group([
    'namespace' => 'WorkPlaceRiskAssesment',
], function() {



    Route::group(['prefix' => 'workplace_risk_assesment',  'as' => 'workplace_risk_assesment.', 'middleware'=>'access.routeNeedsPermission:view_osh_menu'], function() {

        Route::get('/', function () {
            return view('backend.workplace_risk_assesment.menu');
        })->name('menu');


        Route::group(['prefix' => 'audit',  'as' => 'audit.'], function() {

            Route::get('/', 'OshAuditController@index')->name('index');
            Route::get('activate_period/{osh_audit_period}', 'OshAuditController@activatePeriod')->name('activate_period'); 


            //approved audit list
            Route::get('checklist_view', 'OshAuditController@checklistView')->name('checklist_view');

            //feedback
            Route::get('update_feedback/{osh_audit_employer_id}', 'OshAuditController@updateFeedbackForm')->name('update_feedback'); 
            Route::post('update_feedback', 'OshAuditController@updateFeedback')->name('update_feedback'); 
            Route::post('update_feedback_general_info', 'OshAuditController@updateFeedbackGeneralInfo')->name('update_feedback_general_info'); 
            

            Route::get('{osh_audit_id}/profile', 'OshAuditController@auditListProfile')->name('profile');
            Route::get('{osh_audit_period_id}/employer_profile/{employer_id}', 'OshAuditController@employerAuditProfile')->name('employer_profile');

            Route::get('update_checklist/{osh_audit_employer_id}', 'OshAuditController@updateChecklistForm')->name('update_checklist'); 
            Route::post('update_checklist', 'OshAuditController@updateChecklist')->name('update_checklist'); 
            Route::get('profile_notifications/{osh_audit_employer_id}/{type}', 'OshAuditController@profileNotificationsDatatable')->name('profile_notifications');
            Route::get('{osh_audit_employer_id}/questions/{code_value_id}', 'OshAuditController@getCodeValueQuestions')->name('questions');

            //initiate wf
            // Route::get('submit_period_list/{osh_audit_period_id}', 'OshAuditController@submitPeriodList')->name('submit_period_list');
            Route::post('submit_period_list', 'OshAuditController@submitPeriodList')->name('submit_period_list');
            
            //assign staff
            Route::get('checklist_view', 'OshAuditController@checklistView')->name('checklist_view');
            Route::put('assign_staff', 'OshAuditController@assignStaffAudit')->name('assign_staff');
            Route::get('assigned_list', 'OshAuditController@userAssignedList')->name('assigned_list');



            //redesigned osh audit
            Route::get('view_audit_list/{osh_audit_employer_id}/{fatal}/{lti}/{non_conveyance}', 'OshAuditController@viewAuditList')->name('view_audit_list'); 
            Route::get('return_quater_datatable/{osh_audit_period_id}/{fatal}/{lti}/{non_conveyance}/{quater}', 'OshAuditController@returnQuaterAuditListDatatable')->name('return_quater_datatable'); 
            Route::get('return_quater_submitted_datatable/{osh_audit_period_id}/{quater}', 'OshAuditController@returnSubmittedQuaterAuditListDatatable')->name('return_quater_submitted_datatable'); 



            
        });



        Route::group(['prefix' => 'data_management',  'as' => 'data_management.'], function() {
            // get employees datatable
            Route::get('employees_demographics/', 'OshDataManagementController@employeeDemographic')->name('employee_demographic');
            // get specific notification employee
            Route::get('employee/{employee_id}/{notification_report_id}', 'OshDataManagementController@employee')->name('employee_data');
            Route::get('employer/{employer_id}', 'OshDataManagementController@employer')->name('employer_details');
            Route::get('employers/', 'OshDataManagementController@employerParticulars')->name('employer_particulars');
            Route::get('menu', 'OshDataManagementController@dataManagementMenu')->name('data_management_menu');
            Route::get('employee_datatable', 'OshDataManagementController@getEmployeeDataTable')->name('employee_datatable');
            Route::get('employer_data/datatable', 'OshDataManagementController@getEmployerDatatable')->name('employer_data_datatable');
            Route::get('claims', 'OshDataManagementController@oshClaims')->name('osh_claims');
            Route::get('claim_datatable', 'OshDataManagementController@getClaimsDataTable')->name('claim_datatable');
            Route::get('get_accidents/{accident_id}', 'OshDataManagementController@getAccident')->name('get_accidents');
            Route::get('get_disease_organs/{organ_id}', 'OshDataManagementController@getDeseasesByOrgan')->name('get_disease_organs');
            Route::get('get_disease_agents/{agent_id}', 'OshDataManagementController@getDeseasesByAgent')->name('get_disease_agents');
        });
          //osh reports

        Route::group(['prefix' => 'reports',  'as' => 'reports.'], function() {
            Route::get('report_menu', 'OshReportController@reportMenu')->name('report_menu');
            Route::get('osh_table_reports', 'OshReportController@tableReport')->name('osh_table_reports');
            Route::get('osh_report_summary/{report_id}', 'OshReportController@oshEmployerReport')->name('osh_report_summary');
            Route::get('osh_graphical_reports', 'OshReportController@graphicalReport')->name('osh_graphical_reports'); 

            // special for sectors and categories
            
            Route::get('osh_sector_category_summary/{report_id}', 'OshReportController@oshEmployerSectorCategory')->name('osh_sector_category_summary');

                    // backend.workplace_risk_assesment..osh_reports.osh_reportoshEmployerReport
        });


    });

});