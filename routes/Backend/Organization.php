<?php
/**
/**
 * Organization
 */
Route::group([
    'prefix' => 'organization',
    'namespace' => 'Organization',
    'as' => 'organization.',
], function() {


    Route::get('/', function () {
        return view('backend.organization.menu');
    })->name('menu');




    /* Fiscal Year  */
    Route::resource('fiscal_year', 'FiscalYearController',  ['except' => ['show']]);
    Route::get('fiscal_year/get', 'FiscalYearController@get')->name('fiscal_year.get'); //All fiscal years

    /* End of Fiscal Years */
});

