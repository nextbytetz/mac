<?php

Route::group([
    'as' => 'report.',
    'namespace' => 'Report',
    'prefix' => 'report',
], function() {

    Route::group(['prefix' => 'configurable', 'as' => 'configurable.'], function () {
        Route::get('build/{id?}', 'ReportController@buildConfigurable')->name('build');
        Route::get('refresh/{id?}', 'ReportController@refreshConfigurable')->name('refresh');
        Route::get('refresh_by_name/{name}', 'ReportController@refreshMviewByName')->name('refresh_by_name');
    });

    Route::get('cr/{cr}', 'ReportController@viewConfigurable')->name('cr');
    Route::get('sla/{sla}', 'ReportController@viewSla')->name('sla');
    //Route::get('cr/cr_parent/{cr}/{attribute}/{attribute_value}', 'ReportController@viewConfigurable')->name('cr');
    Route::post('cr_headers/{cr}', 'ReportController@getConfigurableHeaders')->name('cr_headers');
    Route::post('cr_datatable/{cr}', 'ReportController@getConfigurableDatatable')->name('cr_datatable');
    Route::post('cr_refresh_dt/{cr}', 'ReportController@refreshConfigurableDt')->name('cr_refresh_dt');
    Route::post('cr_build_dt/{cr}', 'ReportController@buildConfigurableDt')->name('cr_build_dt');
    Route::post('cr_download_dt/{cr}', 'ReportController@downloadConfigurableDt')->name('cr_download_dt');
    Route::post('cr_download_all/{cr}', 'ReportController@downloadConfigurableAll')->name('cr_download_all');
    Route::post('cr_reversed_datatable/{cr}', 'ReportController@getReversedConfigurableDatatable')->name('cr_reversed_datatable');
    Route::post('cr_subordinates_datatable/{cr}/{user_id}', 'ReportController@getSubordinatesConfigurableDatatable')->name('cr_subordinates_datatable');

    Route::group(['prefix' => 'sla', 'as' => 'sla.'], function () {
        Route::get('/', 'ReportController@sla')->name('index');
    });

});

