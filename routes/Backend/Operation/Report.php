<?php
/**
/**
 * Finance
 */
Route::group([
//    'prefix' => 'operation',
    'as' => 'operation.',
    'namespace' => 'Report',
], function() {

    Route::group([ 'prefix' => 'report', 'as' => 'report.'], function() {


        /**
         * COMPLIANCE REPORTS ===============================
         */


        /**
         * load contribution staff performance report
         */
        Route::get('load_contribution_staff_performance', 'ComplianceReportController@staffContributionPerformance')->name('contribution.performance');

        /*
         *
         * Load contribution aging
         */
        Route::get('load_contribution_aging', 'ComplianceReportController@loadContributionAging')->name('load_contribution.aging');



        /*
         *
         * Booking amount against Contribution (amount paid)
         */
        Route::get('booking_contribution_variance', 'ComplianceReportController@bookingContributionVariance')->name('booking_contribution.variance');
        Route::get('contribution_receivable', 'ComplianceReportController@contributionReceivable')->name('contribution_receivable');


        /*
         *
         * Interest Raised vs Paid amount variance
         */
        Route::get('interest_raised_paid_variance', 'ComplianceReportController@interestRaisedPaidVariance')->name('interest_raised_paid.variance');


        /*
   *
   * New Employees registered
   */
        Route::get('new_employees_registered', 'ComplianceReportController@newEmployeesRegistered')->name('new_employees.registered');



        /*
       *
       * Employers Report
       */
        Route::get('employer_registration', 'ComplianceReportController@employerRegistrationReport')->name('employer.registrations');


        /*
        *
        * Employer Overall Report
        */
        Route::get('employer_overall_report', 'ComplianceReportController@employerOverallReport')->name('employer.overall_report');


        /*
    *
    * Unregistered employers Report
    */
        Route::get('unregistered_employers_report', 'ComplianceReportController@unregisteredEmployersReport')->name('employer.unregistered');


        /*
   *
   * Employers Certificate Issue Report
   */
        Route::get('employer_registration_certificate_issue', 'ComplianceReportController@employerCertificateIssueReport')->name('employer.certificate_issues');

        /*
   *
   * Employees by employers
   */
        Route::get('employees_by_employer', 'ComplianceReportController@employeesByEmployer')->name('employer.employees');


        /*
   *
   * Employer interest overpayment
   */
        Route::get('employer_interest_overpayment', 'ComplianceReportController@employerInterestOverpayment')->name('employer_interest_overpayment');

        /*
         * Large Contributor Report
         */
        Route::get('large_contributor', 'ComplianceReportController@largeContributor')->name('large_contributor');

        /*
          * Agents and  employers manages online reports
          */
        Route::get('agents_employers', 'ComplianceReportController@agentEmployer')->name('agents_employers');

        Route::get('Unsuccessfully_cn_month', 'ComplianceReportController@unsuccessfullyCNReport')->name('unsuccessfully_cn');

        Route::get('registered_not_contributing', 'ComplianceReportController@registeredNotContributing')->name('registered_not_contributing');

        Route::get('online_not_control_number', 'ComplianceReportController@onlineNoControlNumber')->name('online_not_control_number');

        /*Contributing employers*/

        Route::get('contributing_employers', 'ComplianceReportController@ContributingEmployers')->name('contributing_employers');

        Route::get('closed_business', 'ComplianceReportController@closedBusiness')->name('closed_business');
        Route::get('closed_business_extension', 'ComplianceReportController@closedBusinessExtension')->name('closed_business_extension');
        Route::get('deregistration_need_action_after_followup', 'ComplianceReportController@closedBusinessNeedActionAfterStatusFollowup')->name('deregistration_need_action_after_followup');
        Route::get('dormant_employers', 'ComplianceReportController@dormantEmployers')->name('dormant_employers');
        Route::get('employer_verifications', 'ComplianceReportController@employerVerifications')->name('employer_verifications');
        Route::get('erroneous_refund_payment', 'ComplianceReportController@erroneousRefundPayment')->name('erroneous_refund_payment');
        Route::get('advance_contribution_register', 'ComplianceReportController@advanceContribution')->name('advance_contribution_register');
        Route::get('contribution_reconciliations', 'ComplianceReportController@contributionReconciliations')->name('contribution_reconciliations');
        Route::get('approved_pending_contributions', 'ComplianceReportController@approvedPendingContributions')->name('approved_pending_contributions');
        Route::get('staff_contrib_collection_performance', 'ComplianceReportController@staffContributionCollectionPerformance')->name('staff_contrib_collection_performance');
        Route::get('booked_interests', 'ComplianceReportController@bookedInterests')->name('booked_interests');
        Route::get('unpaid_interests', 'ComplianceReportController@unpaidInterests')->name('unpaid_interests');
        Route::get('contrib_receivable_non_contributors', 'ComplianceReportController@contribRcvableForNonContributors')->name('contrib_receivable_non_contributors');
        Route::get('booked_interest_non_contributors', 'ComplianceReportController@bookedInterestForNonContributors')->name('booked_interest_non_contributors');
        Route::get('run_booking_jobs/{action_type}', 'ComplianceReportController@runBookingJobs')->name('run_booking_jobs');
        Route::get('previous_receivables_new_employers', 'ComplianceReportController@contribRcvableNewEmployers')->name('previous_receivables_new_employers');
        Route::get('rcvable_non_contrib_whole_year', 'ComplianceReportController@contribRcvableNonContributorWholeYear')->name('rcvable_non_contrib_whole_year');
        Route::get('employer_contributions', 'ComplianceReportController@employerContributions')->name('employer_contributions');
        Route::get('summary_contrib_quarterly', 'ComplianceReportController@summaryContribQuarterly')->name('summary_contrib_quarterly');
        Route::get('contribution_age_analysis', 'ComplianceReportController@receivableAgeAnalysis')->name('contribution_age_analysis');
        Route::get('contrib_rcvable_closed_business', 'ComplianceReportController@contribRcvableClosedBusiness')->name('contrib_rcvable_closed_business');
        Route::get('contrib_rcvable_general', 'ComplianceReportController@contribRcvableGeneralReport')->name('contrib_rcvable_general');
        Route::get('contrib_rcvable_open_balance', 'ComplianceReportController@contribRcvableOpenBalance')->name('contrib_rcvable_open_balance');
        Route::get('contrib_rcvable_legacy_bookings', 'ComplianceReportController@legacyBookingsReport')->name('contrib_rcvable_legacy_bookings');
        Route::get('receipt_contributions', 'ComplianceReportController@receiptContributions')->name('receipt_contributions');
        Route::get('change_of_monthly_earning', 'ComplianceReportController@changeOfMonthlyEarning')->name('change_of_monthly_earning');
        Route::get('summary_contrib_age_analysis', 'ComplianceReportController@summaryContribAgeAnalysis')->name('summary_contrib_age_analysis');
        Route::get('summary_contrib_income_rcvable', 'ComplianceReportController@summaryContribIncomeRcvable')->name('summary_contrib_income_rcvable');
        Route::get('update_receivable_mviews', 'ComplianceReportController@updateReceivableReportMViews')->name('update_receivable_mviews');
        Route::get('update_receivable_age_analysis_mviews', 'ComplianceReportController@updateReceivableAgeAnalysisReportMViews')->name('update_receivable_age_analysis_mviews');

        Route::get('receipts_contrib_summary_income', 'ComplianceReportController@receiptsForContribIncomeSummary')->name('receipts_contrib_summary_income');
        Route::get('receipts_prev_contrib_summary_income', 'ComplianceReportController@receiptsForPrevContribIncomeSummary')->name('receipts_prev_contrib_summary_income');
        Route::get('receipts_prev_contrib_booked', 'ComplianceReportController@receiptsForPrevContribIncomeSummaryBooked')->name('receipts_prev_contrib_booked');
        /*Debt management*/
        Route::get('staff_monthly_collection_performance', 'ComplianceReportController@staffMonthlyCollectionPerformance')->name('staff_monthly_collection_performance');
        Route::get('staff_employer_follow_ups', 'ComplianceReportController@staffEmployerFollowUps')->name('staff_employer_follow_ups');
        Route::get('employer_change_request', 'ComplianceReportController@changeParticulars')->name('employer_change_request');
        Route::get('employer_return_earnings', 'ComplianceReportController@employerReturnEarnings')->name('employer_return_earnings');
        Route::get('staff_employer_monthly_collection', 'ComplianceReportController@staffEmployerMonthlyCollectionReport')->name('staff_employer_monthly_collection');
        Route::get('employer_contrib_ext_audit', 'ComplianceReportController@employerContributionsForExternalAudit')->name('employer_contrib_ext_audit');

       //Inspection Profile 
        Route::get('employer_inspection_profile', 'ComplianceReportController@generalInspectionProfileDetails')->name('employer_inspection_profile');
        Route::get('allocated_staff', 'ComplianceReportController@allocatedStaffDetail')->name('allocated_staff');
        Route::get('inspection_stage', 'ComplianceReportController@inspectionStage')->name('inspection_stage');
        //--END --compliance ----------------------



        /**
         * CLAIM REPORTS===================================
         *
         */

        /**
         *
         * Notification report received
         */
        Route::get('notification_report_received', 'ClaimReportController@notificationReportReceived')->name('notification_report.received');
        //  Initial Expense
        Route::get('notification_report_initial_expense', 'ClaimReportController@notificationReportInitialExpense')->name('notification_report.initial_expense');

        //  Processed claims
        Route::get('processed_claims', 'ClaimReportController@processedClaims')->name('notification_report.processed');

        /**
        *
        *Claim Accrual reports
        *  
        */
        // Route::get('claim_accrual_report', 'ClaimAccrualReportController@claimAccrualReports')->name('claim_accrual_report');
        Route::get('accrued_booking', 'ClaimAccrualReportController@claimAccrualReportBooking')->name('accrued_booking');
        Route::get('accrued_assessment', 'ClaimAccrualReportController@claimAccrualReportAssessment')->name('accrued_assessment');
        Route::get('claim_accrual_report', 'ClaimAccrualReportController@generalClaimAccrualReport')->name('claim_accrual_report');
        Route::post('get', 'ClaimAccrualReportController@claimAccrualReportBooking')->name('accrued_booking.get');
         /**
        *
        *Incestigation Plan reports
        *  
        */
         Route::get('general_investigation_plan', 'InvestigationPlanReportController@generalInvestigationPlanReport')->name('general_investigation_plan');
         Route::get('planned_unplanned_investigation', 'InvestigationPlanReportController@bothInvestigationPlan')->name('planned_unplanned_investigation');
         Route::get('investigated_report_written', 'InvestigationPlanReportController@investigatedReportWritten')->name('investigated_report_written');
         Route::get('investigated_report_unwritten', 'InvestigationPlanReportController@investigatedReportUnWritten')->name('investigated_report_unwritten');
         Route::get('un_planned_investigation', 'InvestigationPlanReportController@unPlannedInvestigation')->name('un_planned_investigation');
         Route::get('investigation_reports', 'InvestigationPlanReportController@investigationReports')->name('investigation_reports');
         
         /**
         *
         *Claim Follou UPS
         */
         Route::get('notification_follow_ups_report', 'NottificationFollowUpsReportController@generalNotificationFollowUpREport')->name('notification_follow_ups_report');

        //-EDN ---CLAIM REPORTS----------------------------




        /**
         * PAYROLL REPORTS ===================
         */

        //monthly pension payroll
        Route::get('monthly_pension_payroll', 'PayrollReportController@monthlyPensionPayroll')->name('payroll.monthly_pension');
        Route::get('payroll_beneficiaries', 'PayrollReportController@payrollBeneficiaries')->name('payroll.payroll_beneficiaries');
        Route::get('payroll_verifications', 'PayrollReportController@payrollVerifications')->name('payroll.payroll_verifications');
        Route::get('pension_statement', 'PayrollReportController@pensionStatement')->name('payroll.pension_statement');
        Route::get('payroll_child_extensions', 'PayrollReportController@payrollChildExtensions')->name('payroll.payroll_child_extensions');
        Route::get('payroll_bank_updates', 'PayrollReportController@payrollBankUpdates')->name('payroll.payroll_bank_updates');
        Route::get('payroll_payments_summary', 'PayrollReportController@payrollPaymentsSummary')->name('payroll.payroll_payments_summary');
        //--End --Payroll ---------------
        

       
    });



});

