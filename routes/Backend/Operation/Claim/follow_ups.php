<?php
/**
/**
 * follow_ups
 */
Route::group([
	'namespace' => 'Operation\Claim',
	'as' => 'claim.',
	'prefix' => 'claim'
], function() {
	Route::group(['prefix' => 'follow_ups',  'as' => 'follow_ups.'], function() {
		Route::get('create_follow_up/{notification_report_id}', 'ClaimFollowupController@createFollowUp')->name('create_follow_up');
		Route::post('store_follow_up', 'ClaimFollowupController@storeFollowUp')->name('store_follow_up');
		Route::get('get_follow_up_datatable/{notification_report_id}', 'ClaimFollowupController@getClaimFollowupDatatable')->name('get_follow_up_datatable');
		Route::get('edit/{claim_followup_id}/{follow_up_id}', 'ClaimFollowupController@editFollowUp')->name('edit');
		Route::post('update_follow_up/{follow_up_id}', 'ClaimFollowupController@updateFollowUp')->name('update_follow_up');
		Route::get('delete_follow_up/{follow_up_id}', 'ClaimFollowupController@deleteFollowUp')->name('delete_follow_up');
		Route::get('get_follow_ups', 'ClaimFollowupController@getFollowUps')->name('get_follow_ups');
		Route::get('my_follow_ups/{user_id}', 'ClaimFollowupController@myFollowUps')->name('my_follow_ups');
		Route::get('get_main_follow_up_datatable', 'ClaimFollowupController@mainFollowUps')->name('get_main_follow_up_datatable');
		Route::get('user_follow_ups/{user_id}', 'ClaimFollowupController@userFollowUps')->name('user_follow_ups');

		Route::get('get_user_follow_ups/{user_id}', 'ClaimFollowupController@getUserFollowUps')->name('get_user_follow_ups');
		Route::get('export_follow_up_perfomance', 'ClaimFollowupController@exportFollowUpPerfomance')->name('export_follow_up_perfomance');

		/* review */

		Route::get('review_history_follow_up/{notification_report_id}', 'ClaimFollowupController@reviewHistory')->name('review_history_follow_up');
		Route::get('approve_follow_up/{notification_report_id}', 'ClaimFollowupController@approve')->name('approve_follow_up');
		Route::get('reverse_follow_up/{notification_report_id}', 'ClaimFollowupController@reverse')->name('reverse_follow_up');
		Route::get('resubmit_follow_up/{notification_report_id}', 'ClaimFollowupController@resubmit')->name('resubmit_follow_up');
		Route::get('complete_follow_up/{notification_report_id}', 'ClaimFollowupController@complete')->name('complete_follow_up');

		/* followup reports */
		Route::get('get_users_followup_updates/{date}', 'ClaimFollowupController@updatesCsv')->name('get_users_followup_updates');

		
	});
});